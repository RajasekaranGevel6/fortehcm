﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// BatchQuestionEntry.cs
// File that represents the user interface forBatch Question Entry page
// This will helps to upload a set of questions
#endregion Header

#region Directives

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Ionic.Zip;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;


#endregion Directives

namespace Forte.HCM.UI.InterviewQuestions
{
    /// <summary> 
    /// Class that defines the user interface layout and functionalities for
    /// BatchQuestionEntry page. This page helps to insert bulk of questions in 
    /// to the database.      
    /// </summary>
    public partial class InterviewBatchQuestionEntry : PageBase
    {
        #region Events Handlers
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set default button and focus
                Page.Form.DefaultButton = InterviewBatchQuestionEntry_uploadButton.UniqueID;

                Page.Form.DefaultFocus = InterviewBatchQuestionEntry_fileUpload.UniqueID;

                InterviewBatchQuestionEntry_fileUpload.Focus();

                Master.SetPageCaption("Batch Question Entry");

                if (!IsPostBack)
                {
                    //Assign the page number in viewstate
                    ViewState["PAGE_NUMBER"] = 1;

                    //Assign the invalid grid page number
                    ViewState["INVALID_QUESTIONS_PAGE_NUMBER"] = 1;

                    InterviewBatchQuestionEntry_isSavedHiddenField.Value = "0";

                    InterviewBatchQuestionEntry_saveButton.Enabled = true;

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(userID);

                    Session["USERNAME"] = userDetail.FirstName;

                    // Add handler to download template.
                    BatchQuestionEntry_downloadTemplateLinkButton.Attributes.Add
                        ("onclick", "javascript:DownloadInterviewBatchTemplate();");
                }
                InterviewBatchQuestionEntry_topSuccessMessageLabel.Attributes["Tag"] = "0";

                InterviewBatchQuestionEntry_topErrorMessageLabel.Text = string.Empty;
                InterviewBatchQuestionEntry_topSuccessMessageLabel.Text = string.Empty;
                InterviewBatchQuestionEntry_bottomSuccessMessageLabel.Text = string.Empty;
                InterviewBatchQuestionEntry_bottomErrorMessageLabel.Text = string.Empty;
                InterviewBatchQuestionEntry_inCompleteQuestionsTabPanel_errorMessageLabel
                 .Text = string.Empty;
                InterviewBatchQuestionEntry_questionsTabPanel_errorMessageLabel.Text
                    = string.Empty;

                InterviewBatchQuestionEntry_pageNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                    (InterviewBatchQuestionEntry_pageNavigator_PageNumberClick);


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when page number is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        void InterviewBatchQuestionEntry_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                UpdateOldInvalidQuestionsToSession(Convert.ToInt32(ViewState["INVALID_QUESTIONS_PAGE_NUMBER"]));
                ViewState["INVALID_QUESTIONS_PAGE_NUMBER"] = e.PageNumber;
                BindInvalidQuestion(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on datalist row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewBatchQuestionEntry_questionsDataList_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                LinkButton InterviewBatchQuestionEntry_qstImageLinkButton =
                    (LinkButton)e.Row.FindControl("InterviewBatchQuestionEntry_qstImageLinkButton");
                HtmlTableRow tr = (HtmlTableRow)e.Row.FindControl("InterviewBatchQuestionEntry_imageLinkTR");
                if (InterviewBatchQuestionEntry_qstImageLinkButton.Visible)
                {

                    string xlsFilePath = ViewState["PATH_NAME"].ToString();
                    string imageAbsolutePath = Path.Combine("../InterviewBatchQuestionExcels/", Path.GetFileNameWithoutExtension(xlsFilePath)) + "/" + InterviewBatchQuestionEntry_qstImageLinkButton.Text;
                    InterviewBatchQuestionEntry_qstImageLinkButton.Attributes.Add("onclick",
                        "javascript: return ShowQuestionImage('" + imageAbsolutePath + "')");

                    tr.Style.Add("display", "");
                }
                else
                    tr.Style.Add("display", "none");


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called on datalist row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewBatchQuestionEntry_inCompleteQuestionsDataList_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                HiddenField InterviewBatchQuestionEntry_testAreaHiddenField =
                    (HiddenField)e.Row.FindControl("InterviewBatchQuestionEntry_testAreaHiddenField");
                DropDownList InterviewBatchQuestionEntry_testAreaDropDownList =
                    (DropDownList)e.Row.FindControl("InterviewBatchQuestionEntry_testAreaDropDownList");
                DropDownList InterviewBatchQuestionEntry_complexityDropDownList =
                    (DropDownList)e.Row.FindControl("InterviewBatchQuestionEntry_complexityDropDownList");
                ImageButton InterviewBatchQuestionEntry_complexityImageButton =
                    (ImageButton)e.Row.FindControl("InterviewBatchQuestionEntry_complexityImageButton");
                HiddenField InterviewBatchQuestionEntry_complexityHiddenField =
                     (HiddenField)e.Row.FindControl("InterviewBatchQuestionEntry_complexityDropDownListHiddenField");

                if (InterviewBatchQuestionEntry_testAreaDropDownList != null)
                {
                    InterviewBatchQuestionEntry_testAreaDropDownList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
                    InterviewBatchQuestionEntry_testAreaDropDownList.DataTextField = "AttributeName";
                    InterviewBatchQuestionEntry_testAreaDropDownList.DataValueField = "AttributeID";
                    InterviewBatchQuestionEntry_testAreaDropDownList.DataBind();
                    InterviewBatchQuestionEntry_testAreaDropDownList.Items.Insert(0, "--Select--");

                    InterviewBatchQuestionEntry_testAreaDropDownList.ClearSelection();
                    if (InterviewBatchQuestionEntry_testAreaDropDownList.Items.FindByText(
                        InterviewBatchQuestionEntry_testAreaHiddenField.Value.Trim()) != null)
                        InterviewBatchQuestionEntry_testAreaDropDownList.Items.FindByText(
                            InterviewBatchQuestionEntry_testAreaHiddenField.Value.Trim()).Selected = true;
                }
                if (InterviewBatchQuestionEntry_complexityDropDownList != null)
                {
                    InterviewBatchQuestionEntry_complexityDropDownList.DataSource =
                        new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
                    InterviewBatchQuestionEntry_complexityDropDownList.DataTextField = "AttributeName";
                    InterviewBatchQuestionEntry_complexityDropDownList.DataValueField = "AttributeID";
                    InterviewBatchQuestionEntry_complexityDropDownList.DataBind();
                    InterviewBatchQuestionEntry_complexityDropDownList.Items.Insert(0, "--Select--");

                    InterviewBatchQuestionEntry_complexityDropDownList.ClearSelection();

                    if (InterviewBatchQuestionEntry_complexityDropDownList.Items.FindByText(
                        InterviewBatchQuestionEntry_complexityHiddenField.Value.Trim()) != null)
                        InterviewBatchQuestionEntry_complexityDropDownList.Items.FindByText(
                            InterviewBatchQuestionEntry_complexityHiddenField.Value.Trim()).Selected = true;
                }

                ImageButton InterviewBatchQuestionEntry_categoryImageButton = ((ImageButton)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_categoryImageButton"));
                TextBox InterviewBatchQuestionEntry_categoryTextBox = ((TextBox)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_categoryTextBox"));
                TextBox InterviewBatchQuestionEntry_subjectTextBox = ((TextBox)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_subjectTextBox"));
                HiddenField InterviewBatchQuestionEntry_subjectIdHiddenField = ((HiddenField)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_subjectIdHiddenField"));
                InterviewBatchQuestionEntry_categoryImageButton.Attributes.Add("onclick",
                    "return LoadCategorySubjectLookUpforReadOnly('" + InterviewBatchQuestionEntry_categoryTextBox.ClientID +
                    "','" + InterviewBatchQuestionEntry_subjectTextBox.ClientID + "','"
                        + InterviewBatchQuestionEntry_subjectIdHiddenField.ClientID + "','" +
                        ((HiddenField)e.Row.FindControl("InterviewBatchQuestionEntry_categoryNameHiddenField")).ClientID + "','" +
                        ((HiddenField)e.Row.FindControl("InterviewBatchQuestionEntry_subjectNameHiddenField")).ClientID + "');");

                ImageButton InterviewBatchQuestionEntry_authorImageButton = ((ImageButton)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_authorImageButton"));
                TextBox InterviewBatchQuestionEntry_authorTextBox = ((TextBox)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_authorTextBox"));
                HiddenField InterviewBatchQuestionEntry_authorHiddenField = ((HiddenField)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_authorHiddenField"));
                InterviewBatchQuestionEntry_authorImageButton.Attributes.Add("onclick",
                    "return LoadUserName('" + InterviewBatchQuestionEntry_authorHiddenField.ClientID + "','"
                    + InterviewBatchQuestionEntry_authorHiddenField.ClientID + "','"
                    + InterviewBatchQuestionEntry_authorTextBox.ClientID + "')");
                InterviewBatchQuestionEntry_authorTextBox.Attributes.Add("onclick", "javascript:return false;");
                InterviewBatchQuestionEntry_authorTextBox.Attributes.Add("onkeydown", "javascript:return false;");

                HiddenField InterviewBatchQuestionEntry_qstImageHiddenField = ((HiddenField)e.Row.FindControl
                    ("InterviewBatchQuestionEntry_qstImageHiddenField"));

                string xlsFilePath = ViewState["PATH_NAME"].ToString();
                string imageAbsolutePath = Path.Combine("/InterviewBatchQuestionExcels/", Path.GetFileNameWithoutExtension(xlsFilePath)) + "/" + InterviewBatchQuestionEntry_qstImageHiddenField.Value;

                LinkButton InterviewBatchQuestionEntry_addQuestionImageLinkButton =
                    (LinkButton)e.Row.FindControl("InterviewBatchQuestionEntry_addQuestionImageLinkButton");
                InterviewBatchQuestionEntry_addQuestionImageLinkButton.Attributes.Add
                    ("onclick", "javascript:return ShowQuestionImagePopup('" + imageAbsolutePath + "','" + InterviewBatchQuestionEntry_addQuestionImageLinkButton.ClientID + "');");

                HtmlTableRow InterviewBatchQuestionEntry_addImageLinkTR =
                    (HtmlTableRow)e.Row.FindControl("InterviewBatchQuestionEntry_addImageLinkTR");

                if (InterviewBatchQuestionEntry_addQuestionImageLinkButton.Visible)
                {
                    InterviewBatchQuestionEntry_addImageLinkTR.Style.Add("display", "");
                }
                else
                    InterviewBatchQuestionEntry_addImageLinkTR.Style.Add("display", "none");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when upload button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewBatchQuestionEntry_uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear the questions in the valid question sessions
                Session["INTERVIEW_VALID_QUESTIONS"] = null;

                //Clear the questions in the invalid question sessions
                Session["INTERVIEW_INVALID_QUESTIONS"] = null;

                //Clear the record label text
                InterviewBatchQuestionEntry_recordLabel.Text = string.Empty;

                //Make the total question count as 0
                ViewState["TOTAL_QUESTION_COUNT"] = 0;

                //Make the page number as 1
                ViewState["PAGE_NUMBER"] = 1;

                //Clear the path name from view state
                ViewState["PATH_NAME"] = string.Empty;

                //Set the isSavedHiddenField to default value of 0
                InterviewBatchQuestionEntry_isSavedHiddenField.Value = "0";

                //Make the question tab as active tab
                InterviewBatchQuestionEntry_mainTabContainer.ActiveTabIndex = 0;

                //Save file in the server and get the file name
                string fileName = SaveFileAs();

                // Load the images in dictionary
                if (InterviewBatchQuestionEntry_zipFileUpload.HasFile)
                    ExtractQuestionImages(fileName);

                //If file name is empty return
                if (fileName == string.Empty)
                    return;

                //Save the file name in viewstate
                ViewState["PATH_NAME"] = fileName;

                //Get the sheet name and the total question in each sheet
                GetSheetName(fileName);

                //Load the questions from excel
                LoadExcelFile(fileName);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);

                InterviewBatchQuestionEntry_uploadExcelDiv.Style["display"] = "block";

                InterviewBatchQuestionEntry_resultDiv.Style["display"] = "none";

                InterviewBatchQuestionEntry_resultDiv.Visible = false;
                InterviewBatchQuestionEntry_uploadExcelDiv.Visible = true;
            }
        }

        /// <summary>
        /// This method is called to extract the images in BatchQuestionExcels folder
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void ExtractQuestionImages(string xlsFilePath)
        {
            string zipFileName = Path.GetFileNameWithoutExtension(xlsFilePath);
            string zipFilePath = Path.Combine(Path.GetDirectoryName(xlsFilePath), zipFileName);
            Directory.CreateDirectory(zipFilePath);
            var zip = ZipFile.Read(InterviewBatchQuestionEntry_zipFileUpload.FileContent);
            zip.ExtractAll(zipFilePath, ExtractExistingFileAction.OverwriteSilently);
        }

        /// <summary>
        /// Handler method that will be called when the  reset button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">    
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewBatchQuestionEntry_resetLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                ResetValues();
                Response.Redirect("../InterviewQuestions/InterviewBatchQuestionEntry.aspx?m=0&s=0", false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when the datalist is binded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewBatchQuestionEntry_subject_categoryDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            /*  try
               {
                   if (e.Item.ItemType != ListItemType.Item
                      && e.Item.ItemType != ListItemType.AlternatingItem)
                       return;

                   RadioButton selectRadioButton = (RadioButton)
                       e.Item.FindControl("InterviewBatchQuestionEntry_selectRadioButton");

                   //To add a javascript that will make only one item selectable
                   selectRadioButton.Attributes.Add("onclick", "return CheckOtherIsCheckedByGVID(this,'" +
                       e.Item.Parent.ClientID + "')");
               }
               catch (Exception exception)
               {
                   Logger.ExceptionLog(exception);

                   // Show error messages to the user
                   base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                       InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
               }*/
        }

        /// <summary>
        /// Handler that will be called on subjectcategoryQuestionDataList is binded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewBatchQuestionEntry_subjectcategoryQuestionDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                    return;

                RadioButton Subject_selectQuestionRadioButton = (RadioButton)
                    e.Item.FindControl("InterviewBatchQuestionEntry_selectQuestionRadioButton");

                Subject_selectQuestionRadioButton.Attributes.
                    Add("onclick", "return CheckOtherIsCheckedByGVID(this,'" +
                    e.Item.Parent.ClientID + "')");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the next link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// 
        protected void InterviewBatchQuestionEntry_nextLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                UpdateOldInvalidQuestionsToSession(Convert.ToInt32(ViewState["INVALID_QUESTIONS_PAGE_NUMBER"]));
                LoadNextBatchQuestions();
                ViewState["INVALID_QUESTIONS_PAGE_NUMBER"] = 1;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called on row command on InterviewBatchQuestionEntry_questionGridView
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewCommandEventArgs"/> that holds the event data
        /// </param>
        protected void InterviewBatchQuestionEntry_questionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteQuestion")
                {
                    InterviewBatchQuestionEntry_confirmMsgControl_messageValidLabel.Text =
                        Resources.HCMResource.InterviewBatchQuestionEntry_SingleQuestionDelete;

                    InterviewBatchQuestionEntry_deletepopupExtender.Show();

                    InterviewBatchQuestionEntry_questionIDHiddenField.Value = "V" + e.CommandArgument.ToString();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler that is called on row command on
        /// InterviewBatchQuestionEntry_inCompleteQuestionsGridView
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewCommandEventArgs"/>that holds the event args
        /// </param>
        protected void InterviewBatchQuestionEntry_inCompleteQuestionsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteQuestion")
                {
                    InterviewBatchQuestionEntry_confirmMsgControl_messageValidLabel.Text =
                        Resources.HCMResource.InterviewBatchQuestionEntry_SingleQuestionDelete;

                    InterviewBatchQuestionEntry_deletepopupExtender.Show();

                    InterviewBatchQuestionEntry_questionIDHiddenField.Value = "I" + e.CommandArgument.ToString();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler that is called when yes button of the confirm msg is clicked to 
        /// delete a question or set of questions
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void InterviewBatchQuestionEntry_confirmMsgControl_yesValidButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                //Get the Questions details as in the grid 
                Session["INTERVIEW_VALID_QUESTIONS"] = saveBatchQuestion();

                //Checks which question has to be removed.
                //V denotes to remove a valid single question
                if (InterviewBatchQuestionEntry_questionIDHiddenField.Value.ToString().Substring(0, 1) == "V")
                {
                    List<QuestionDetail> questions = Session["INTERVIEW_VALID_QUESTIONS"] as List<QuestionDetail>;
                    QuestionDetail questionDetail = questions.Find(delegate(QuestionDetail question)
                    {
                        return question.QuestionID.ToString() == InterviewBatchQuestionEntry_questionIDHiddenField.Value.ToString().Substring(1);
                    });

                    questions.Remove(questionDetail);
                    InterviewBatchQuestionEntry_questionGridView.DataSource = questions;
                    InterviewBatchQuestionEntry_questionGridView.DataBind();
                    Session["INTERVIEW_VALID_QUESTIONS"] = questions;
                }

                //I denotes the option to delete a invalid single question
                else if (InterviewBatchQuestionEntry_questionIDHiddenField.Value.
                    ToString().Substring(0, 1) == "I")
                {
                    List<QuestionDetail> questions = GetCurrentInvalidQuestions();

                    QuestionDetail questionDetail = questions.Find(delegate(QuestionDetail question)
                    {
                        return question.QuestionID.ToString() == InterviewBatchQuestionEntry_questionIDHiddenField.Value.ToString().Substring(1);
                    });

                    questions.Remove(questionDetail);

                    Session["INTERVIEW_INVALID_QUESTIONS"] = questions;

                    InterviewBatchQuestionEntry_pageNavigator.Reset();

                    ViewState["INVALID_QUESTIONS_PAGE_NUMBER"] = 1;

                    BindInvalidQuestion(1);

                    InterviewBatchQuestionEntry_pageNavigator.TotalRecords = questions.Count;
                    InterviewBatchQuestionEntry_pageNavigator.PageSize = GridPageSize;
                }
                //AV denote the option to delet all valid questions
                else if (InterviewBatchQuestionEntry_questionIDHiddenField.Value == "AV")
                {
                    List<QuestionDetail> questions = Session["INTERVIEW_VALID_QUESTIONS"] as List<QuestionDetail>;

                    questions.Clear();

                    InterviewBatchQuestionEntry_questionGridView.DataSource = questions;

                    InterviewBatchQuestionEntry_questionGridView.DataBind();

                    Session["INTERVIEW_VALID_QUESTIONS"] = questions;

                    LoadNextBatchQuestions();
                }
                //AI denote the option to delete all invalid questions      
                else if (InterviewBatchQuestionEntry_questionIDHiddenField.Value == "AI")
                {
                    List<QuestionDetail> questions = Session["INTERVIEW_INVALID_QUESTIONS"] as List<QuestionDetail>;

                    int lastNumber = int.Parse(ViewState["INVALID_QUESTIONS_PAGE_NUMBER"].ToString()) * GridPageSize;

                    int index = lastNumber - GridPageSize;

                    questions.RemoveRange(index, InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count);
                    Session["INTERVIEW_INVALID_QUESTIONS"] = questions;

                    InterviewBatchQuestionEntry_pageNavigator.TotalRecords = questions.Count;

                    InterviewBatchQuestionEntry_pageNavigator.PageSize = GridPageSize;

                    InterviewBatchQuestionEntry_pageNavigator.Reset();

                    ViewState["INVALID_QUESTIONS_PAGE_NUMBER"] = 1;

                    BindInvalidQuestion(1);

                    if (questions.Count != 0)
                        return;

                    LoadNextBatchQuestions();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when valid questions,
        /// remove all link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void InterviewBatchQuestionEntry_removeQuestionLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (InterviewBatchQuestionEntry_questionGridView.Rows.Count == 0)
                {
                    InterviewBatchQuestionEntry_questionsTabPanel_errorMessageLabel.Text =
                            "There are no more records to remove";
                    return;
                }

                InterviewBatchQuestionEntry_questionIDHiddenField.Value = "AV";

                InterviewBatchQuestionEntry_confirmMsgControl_messageValidLabel.Text =
                    Resources.HCMResource.InterviewBatchQuestionEntry_MultiValidQuestionDelete;

                InterviewBatchQuestionEntry_deletepopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when invalid 
        /// questions remove all button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void InterviewBatchQuestionEntry_removeInvalidQuestionLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count == 0)
                {
                    InterviewBatchQuestionEntry_inCompleteQuestionsTabPanel_errorMessageLabel.Text =
                                 "There are no more records to remove";
                    return;
                }

                InterviewBatchQuestionEntry_questionIDHiddenField.Value = "AI";

                InterviewBatchQuestionEntry_confirmMsgControl_messageValidLabel.Text =
                       Resources.HCMResource.InterviewBatchQuestionEntry_MultiInValidQuestionDelete;

                InterviewBatchQuestionEntry_deletepopupExtender.Show();

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when save button is clicked 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void InterviewBatchQuestionEntry_saveButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (InterviewBatchQuestionEntry_questionGridView.Rows.Count == 0)
                {
                    base.ShowMessage(InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                        InterviewBatchQuestionEntry_topErrorMessageLabel,
                    Resources.HCMResource.InterviewBatchQuestionEntry_PleaseEnterAQuestion);
                    return;
                }

                List<QuestionDetail> questionDetails = new List<QuestionDetail>();

                //Get the question details from the grid
                questionDetails = saveBatchQuestion();

                //Check whether the questions details are valid
                if (CheckIsValidData(questionDetails, "Valid"))
                {
                    // Assign Disclaimer message to the disclaimer popup
                    InterviewBatchQuestionEntry_savePopupExtenderControl.Message =
                        Resources.HCMResource.InterviewBatchQuestionEntry_DisclaimerMessage;
                    InterviewBatchQuestionEntry_disclaimerSavepopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler that is called when confirm message ok Button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void InterviewBatchQuestionEntry_saveQuestionOkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                InterviewBatchQuestionEntry_isSavedHiddenField.Value = "1";

                if (InterviewBatchQuestionEntry_saveRadioButton.Checked)
                {
                    InterviewBatchQuestionEntry_saveButton_Click(sender, e);
                    return;
                }

                LoadNextBatchQuestions();

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when invalid 
        /// questions save button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void InterviewBatchQuestionEntry_invalidSaveButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count == 0)
                {
                    base.ShowMessage(InterviewBatchQuestionEntry_bottomErrorMessageLabel, InterviewBatchQuestionEntry_topErrorMessageLabel, Resources.HCMResource.InterviewBatchQuestionEntry_PleaseEnterAQuestion);
                    return;
                }

                List<QuestionDetail> invalidQuestion = new List<QuestionDetail>();

                invalidQuestion = GetInvalidQuestion();

                if (CheckIsValidData(invalidQuestion, "Invalid"))
                {
                    // Assign Disclaimer message to the disclaimer popup
                    InterviewBatchQuestionEntry_saveInvalidPopupExtenderControl.Message =
                        Resources.HCMResource.InterviewBatchQuestionEntry_DisclaimerMessage;
                    InterviewBatchQuestionEntry_disclaimerSaveInvalidpopupExtender.Show();
                }
                InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataSource = invalidQuestion;
                InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataBind();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when save button is clicked        
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void DisclaimerControl_acceptButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<QuestionDetail> questionDetails = new List<QuestionDetail>();

                //Get the question details from the grid
                questionDetails = saveBatchQuestion();

                //Check whether there is invalid questions . if yes avtivate that tab


                //Save the questions to the database
                SaveQuestionsToDatabase(questionDetails);

                //Assign the hidden value to 1, as the question is saved
                InterviewBatchQuestionEntry_isSavedHiddenField.Value = "1";

                questionDetails = new List<QuestionDetail>();

                InterviewBatchQuestionEntry_questionGridView.DataSource = questionDetails;
                InterviewBatchQuestionEntry_questionGridView.DataBind();


                if (InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count > 0)
                {
                    base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                   InterviewBatchQuestionEntry_bottomErrorMessageLabel, "Incomplete questions are still pending");
                    InterviewBatchQuestionEntry_mainTabContainer.ActiveTabIndex = 1;
                }
                else
                {
                    //Load next set of questions
                    InterviewBatchQuestionEntry_nextLinkButton_Click(sender, e);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when save button is clicked        
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void InterviewBatchQuestionEntry_acceptInvalidQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<QuestionDetail> questionDetails = new List<QuestionDetail>();

                //Get the question details from the grid
                questionDetails = GetInvalidQuestion();

                //Save the questions to the database
                SaveQuestionsToDatabase(questionDetails);

                List<QuestionDetail> inValidQuestions = new List<QuestionDetail>();

                inValidQuestions = Session["INTERVIEW_INVALID_QUESTIONS"] as List<QuestionDetail>;

                foreach (QuestionDetail questions in questionDetails)
                {
                    inValidQuestions.RemoveAll(delegate(QuestionDetail question)
                    {
                        return question.QuestionID == questions.QuestionID;
                    });
                }

                Session["INTERVIEW_INVALID_QUESTIONS"] = inValidQuestions;

                //Load next set of questions
                LoadNextBatchQuestions();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Method that used to save the batch questions into the database
        /// </summary>
        /// <param name="questionDetails">
        /// A<see cref="List<QuestionDetail>"/>that holds the list of questions
        /// </param>
        private void SaveQuestionsToDatabase(List<QuestionDetail> questionDetails)
        {
            //Remove the invalid choice options from the options 
            /*  foreach (QuestionDetail question in questionDetails)
              {
                  question.AnswerChoices.RemoveAll(delegate(AnswerChoice answer)
                  {
                      return answer.Choice.Trim().Length == 0;
                  });

                  //Set the number of choices of questions to the 
                  //count of the choices 
                  question.NoOfChoices = Convert.ToInt16(question.AnswerChoices.Count);
              }*/

            string questionIds = string.Empty;

            QuestionBLManager questionBLManager = new QuestionBLManager();

            foreach (QuestionDetail question in questionDetails)
            {
                question.SelectedSubjectIDs = question.Subjects[0].SubjectID.ToString();

                questionBLManager.SaveInterviewQuestion(question);

                questionIds += question.QuestionKey + ", ";
            }

            questionIds = questionIds.Remove(questionIds.Length - 2, 2);

            ShowMessage(InterviewBatchQuestionEntry_topSuccessMessageLabel
             , InterviewBatchQuestionEntry_bottomSuccessMessageLabel,
             string.Format(Resources.HCMResource.SingleQuestionEntry_AddedSuccessfully,
             questionIds.Split(',').Length));
        }

        #endregion Events Handlers

        #region Private Methods

        /// <summary>
        /// This method gets the value from the grid view and updates
        /// the session of invalid question details
        /// </summary>
        /// <param name="PreviousPageNumber">
        /// A <see cref="integer"/> that contains the page number
        /// of previous records.
        /// </param>
        private void UpdateOldInvalidQuestionsToSession(int PreviousPageNumber)
        {
            List<QuestionDetail> inValidQuestions = null;
            inValidQuestions = Session["INTERVIEW_INVALID_QUESTIONS"] as List<QuestionDetail>;
            if (inValidQuestions == null || inValidQuestions.Count == 0)
                return;
            if (PreviousPageNumber == 0)
                PreviousPageNumber = 1;
            int firstRecord = (PreviousPageNumber * GridPageSize) - GridPageSize;
            if (InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count != 0)
            {
                inValidQuestions.RemoveRange(firstRecord,
                    InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count);
                inValidQuestions.InsertRange(firstRecord, GetInvalidQuestion());
                Session["INTERVIEW_INVALID_QUESTIONS"] = null;
                Session["INTERVIEW_INVALID_QUESTIONS"] = inValidQuestions;
            }
        }

        /// <summary>
        /// Method that is used to read the questions from the excel
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/>that holds the file name
        /// </param>
        private void LoadExcelFile(string fileName)
        {
            if (Session["USERNAME"] == null)
            {
                ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel, Resources.HCMResource.InterviewBatchquestionEntry_InvalidUserName);

                return;
            }
            QuestionExcelBatchReader excelBatchReader = new QuestionExcelBatchReader(fileName);

            //ViewState["TOTAL_QUESTION_COUNT"] = excelBatchReader.GetCount("Sheet1$");

            List<QuestionDetail> questionDetails =
             excelBatchReader.GetInterviewQuestions
             (int.Parse(ViewState["PAGE_NUMBER"].ToString()), GridPageSize,
             Session["USERNAME"].ToString().Trim(), ViewState["CURRENT_SHEET_NAME"].ToString(), userID, fileName, base.tenantID);

            GetValidQuestion(questionDetails);

            DisplayRecordLabel(ViewState["TOTAL_QUESTION_COUNT"].ToString(), questionDetails.Count);

        }

        /// <summary>
        /// Method that is used to separate valid and invalid 
        /// questions taken from the excel.
        /// </summary>
        /// <param name="questions">
        /// A<see cref="List<QuestionDetail>"/>
        /// that holds the list of questions.
        /// </param>
        private void GetValidQuestion(List<QuestionDetail> questions)
        {
            List<QuestionDetail> validQuestions = new List<QuestionDetail>();

            List<QuestionDetail> inValidQuestions = new List<QuestionDetail>();

            //For the first time store the list of invalid questions in session    
            if (Session["INTERVIEW_INVALID_QUESTIONS"] != null)
            {
                inValidQuestions = Session["INTERVIEW_INVALID_QUESTIONS"] as List<QuestionDetail>;
            }

            inValidQuestions.AddRange(questions.FindAll(delegate(QuestionDetail question)
            {
                return question.IsValid == false || question.CorrectAnswer == "" || question.CorrectAnswer == null ||
                    question.Question == "" || question.Question == null;
            }));

            validQuestions = questions.FindAll(delegate(QuestionDetail question)
            {
                return question.IsValid == true
                    && (question.CorrectAnswer != null || question.CorrectAnswer != "")
                    && !Utility.IsNullOrEmpty(question.Question)
                 ;
            });


            InterviewBatchQuestionEntry_questionGridView.DataSource = validQuestions;
            InterviewBatchQuestionEntry_questionGridView.DataBind();
            Session["INTERVIEW_VALID_QUESTIONS"] = validQuestions;


            Session["INTERVIEW_INVALID_QUESTIONS"] = inValidQuestions;
            InterviewBatchQuestionEntry_pageNavigator.TotalRecords = inValidQuestions.Count;
            InterviewBatchQuestionEntry_pageNavigator.PageSize = GridPageSize;

            InterviewBatchQuestionEntry_pageNavigator.Reset();
            BindInvalidQuestion(1);
        }

        /// <summary>
        /// Method that is used to bind the invalid questions in the grid
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the current page number
        /// </param>
        private void BindInvalidQuestion(int pageNumber)
        {
            List<QuestionDetail> inValidQuestions = new List<QuestionDetail>();
            int lastRecord = pageNumber * GridPageSize;
            int firstRecord = lastRecord - GridPageSize;
            inValidQuestions = Session["INTERVIEW_INVALID_QUESTIONS"] as List<QuestionDetail>;
            if (inValidQuestions.Count == 0)
            {
                InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataSource = inValidQuestions;
                InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataBind();
                return;
            }

            int remainingRecord = inValidQuestions.Count - ((pageNumber - 1) * GridPageSize);

            lastRecord = lastRecord - firstRecord;

            lastRecord = lastRecord > remainingRecord ?
                inValidQuestions.Count % GridPageSize : lastRecord;

            //Get the certain number of questions from invalid questions
            InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataSource =
                inValidQuestions.GetRange(firstRecord, lastRecord);

            InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataBind();
        }

        /// <summary>
        /// Method that is used to display the question record details
        /// </summary>
        /// <param name="totalCount">
        /// A<see cref="string"/>that holds the totalCount of the question
        /// </param>
        /// <param name="questionCount">
        /// A<see cref="int"/>that holds the questionCount displayed per page
        /// </param>
        private void DisplayRecordLabel(string totalCount, int questionCount)
        {
            int firstNumber;

            int lastNumber;

            if ((int.Parse(ViewState["PAGE_NUMBER"].ToString()) == 1))
            {
                firstNumber = ((int.Parse(ViewState["PAGE_NUMBER"].ToString())
                                    - 1) * GridPageSize) + 1;
                lastNumber = questionCount;
            }
            else
            {
                firstNumber = ((int.Parse(ViewState["PAGE_NUMBER"].ToString())
                                    - 1) * GridPageSize) + 1;

                lastNumber = firstNumber - 1 + questionCount;
            }

            //To display the number of questions displayed in the page
            InterviewBatchQuestionEntry_recordLabel.Text = firstNumber + " to "
                            + lastNumber + " of " + totalCount + " Records";
        }

        /// <summary>
        /// This method helps to save the file in the server.
        /// </summary>
        private string SaveFileAs()
        {
            string fileName = InterviewBatchQuestionEntry_fileUpload.FileName;

            // Check if file name is null or empty.
            if (fileName == null || fileName.Trim().Length == 0)
            {
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                    Resources.HCMResource.InterviewBatchQuesionEntry_PleaseProvideAExcelFile);
                return string.Empty;
            }

            // Check if file type is not of the desired one.
            string extension = Path.GetExtension(fileName);
            if (extension == null || extension.Trim().Length == 0 ||
                (extension.ToLower().Trim() != ".xls" && extension.ToLower().Trim() != ".xlsx"))
            {
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                    Resources.HCMResource.InterviewBatchQuesionEntry_PleaseProvideAExcelFile);
                return string.Empty;
            }

            // Check if content type is not of the desired one.
            string contentType = InterviewBatchQuestionEntry_fileUpload.PostedFile.ContentType;
            if (contentType == null || contentType.Trim().Length == 0 ||
                (contentType.ToLower().Trim() != "application/vnd.ms-excel" &&
                contentType.ToLower().Trim() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                contentType.ToLower().Trim() != "application/ms-excel"))
            {
                base.ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                    Resources.HCMResource.InterviewBatchQuesionEntry_PleaseProvideAExcelFile);
                return string.Empty;
            }

            // Create a directory in the server to keep the uploaded files.
            Directory.CreateDirectory(Server.MapPath("~/") + "\\InterviewBatchQuestionExcels");

            //To save excel file as 
            string saveAsPath = Server.MapPath("~/") + "InterviewBatchQuestionExcels\\"
                + userID + "_" + DateTime.Now.TimeOfDay.Milliseconds + "_" + fileName;

            InterviewBatchQuestionEntry_fileUpload.SaveAs(saveAsPath);

            InterviewBatchQuestionEntry_uploadExcelDiv.Style["display"] = "none";

            InterviewBatchQuestionEntry_resultDiv.Style["display"] = "block";

            InterviewBatchQuestionEntry_resultDiv.Visible = true;

            return saveAsPath;
        }

        /// <summary>
        /// Method to save the invalid questions
        /// </summary>
        private List<QuestionDetail> GetInvalidQuestion()
        {
            QuestionDetail question;

            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            foreach (GridViewRow row in InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows)
            {
                question = new QuestionDetail();

                Label questionIDLabel = (Label)row.FindControl
                    ("InterviewBatchQuestionEntry_incompleteQuestionNoLabel");

                if (!Utility.IsNullOrEmpty(questionIDLabel.Text))
                {
                    question.QuestionID = int.Parse(questionIDLabel.Text.Trim());
                }

                question.Question = ((TextBox)row.FindControl
                    ("InterviewBatchQuestionEntry_incompleteQuestionTextBox")).Text.Trim();

                //question.CategoryID = int.Parse(((HiddenField)row.FindControl
                //    ("InterviewBatchQuestionEntry_categoryIDHiddenField")).Value);
                question.CategoryName = ((HiddenField)row.FindControl("InterviewBatchQuestionEntry_categoryNameHiddenField")).Value.Trim();
                question.SubjectName = ((HiddenField)row.FindControl("InterviewBatchQuestionEntry_subjectNameHiddenField")).Value.Trim();
                //if (!Utility.IsNullOrEmpty(((TextBox)
                //   row.FindControl("InterviewBatchQuestionEntry_categoryTextBox")).Text.Trim()))
                //{
                //    question.CategoryName = ((TextBox)
                //    row.FindControl("InterviewBatchQuestionEntry_categoryTextBox")).Text.Trim();
                //}

                //if (!Utility.IsNullOrEmpty(((TextBox)
                //    row.FindControl("InterviewBatchQuestionEntry_subjectTextBox")).Text.Trim()))
                //{
                //    question.SubjectName = ((TextBox)
                //    row.FindControl("InterviewBatchQuestionEntry_subjectTextBox")).Text.Trim();
                //}


                question.Subjects = new List<Subject>();
                question.Subjects.Add(new Subject());

                if (!Utility.IsNullOrEmpty(((HiddenField)
                    row.FindControl("InterviewBatchQuestionEntry_subjectIdHiddenField")).Value.Trim()))
                {
                    question.Subjects[0].SubjectID = int.Parse(((HiddenField)
                   row.FindControl("InterviewBatchQuestionEntry_subjectIdHiddenField")).Value.Trim());

                    question.Subjects[0].IsSelected = true;

                    question.SubjectID = int.Parse(((HiddenField)
                   row.FindControl("InterviewBatchQuestionEntry_subjectIdHiddenField")).Value.Trim());
                }

                DropDownList testAreaDropDownList = (DropDownList)row.
                    FindControl("InterviewBatchQuestionEntry_testAreaDropDownList");
                question.TestAreaID = testAreaDropDownList.SelectedValue.Trim();
                question.TestAreaName = testAreaDropDownList.SelectedItem.Text.Trim();

                DropDownList complexityDropDown = (DropDownList)row.
                    FindControl("InterviewBatchQuestionEntry_complexityDropDownList");

                question.Complexity = complexityDropDown.SelectedValue.Trim();
                question.ComplexityName = complexityDropDown.SelectedItem.Text.Trim();

                question.Tag = ((TextBox)row.FindControl
                    ("InterviewBatchQuestionEntry_tagTextBox")).Text.Trim();

                question.CreditsEarned = 0.00M;

                TextBox userTextbox = (TextBox)
                  row.FindControl("InterviewBatchQuestionEntry_authorTextBox");

                HiddenField hiddenField = (HiddenField)
                    row.FindControl("InterviewBatchQuestionEntry_authorHiddenField");

                if (!Utility.IsNullOrEmpty(userTextbox))
                {
                    question.AuthorName = userTextbox.Text;

                    question.Author = int.Parse(hiddenField.Value);

                    question.CreatedBy = int.Parse(hiddenField.Value);

                    question.ModifiedBy = int.Parse(hiddenField.Value);
                }

                TextBox answerTextbox = (TextBox)
                  row.FindControl("InterviewBatchQuestionEntry_invalidQuestionanswerNamerTextBox");

                question.CorrectAnswer = answerTextbox.Text;


                int options = 1;

                question.AnswerChoices = new List<AnswerChoice>();

                question.AnswerChoices.Add(new AnswerChoice(answerTextbox.Text.Trim()
                    , options, true));


                /*    DataList answerDataList = (DataList)row.
                        FindControl("InterviewBatchQuestionEntry_subjectcategoryQuestionDataList");
                    question.NoOfChoices = Convert.ToInt16(answerDataList.Controls.Count);
                    int options = 1;

                    question.AnswerChoices = new List<AnswerChoice>();

                    foreach (Control control in answerDataList.Controls)
                    {
                        RadioButton radioButton =
                            (RadioButton)control.FindControl("InterviewBatchQuestionEntry_selectQuestionRadioButton");
                        TextBox textBox =
                            (TextBox)control.FindControl("InterviewBatchQuestionEntry_answerQuestionTextBox");

                        if (radioButton.Checked)
                        {
                            question.Answer = Convert.ToInt16(options);
                        }

                        //if (textBox.Text.Trim().Length == 0)
                        //    continue;

                        question.AnswerChoices.Add(new AnswerChoice
                            (textBox.Text.Trim(), options, radioButton.Checked));

                        options = options + 1;
                        question.CreatedDate = DateTime.Now.Date;
                        question.ModifiedDate = DateTime.Now.Date;
                    }*/

                //question.NoOfChoices = Convert.ToInt16(question.AnswerChoices.Count);
                HiddenField InterviewBatchQuestionEntry_qstImageHiddenField =
                        (HiddenField)row.FindControl("InterviewBatchQuestionEntry_qstImageHiddenField");

                string xlsFilePath = Path.GetDirectoryName(ViewState["PATH_NAME"].ToString());
                string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(ViewState["PATH_NAME"].ToString()));

                string questionImageName = InterviewBatchQuestionEntry_qstImageHiddenField.Value;
                question.HasImage = false;
                question.ImageName = questionImageName;
                if (question.ImageName != "")
                {
                    question.HasImage = true;
                    if (File.Exists(Path.Combine(imagesFolder, question.ImageName)))
                    {
                        try
                        {
                            FileStream fs = File.OpenRead(Path.Combine(imagesFolder, question.ImageName));
                            if (fs.Length > 102400) // if file size greater than 100 KB
                            {
                                question.IsValid = false;
                                question.InvalidQuestionRemarks += "MAXIMUM OF 100KB IMAGE CAN BE UPLOADED, ";
                            }
                            else
                            {
                                byte[] data = new byte[fs.Length];
                                fs.Read(data, 0, data.Length);
                                question.QuestionImage = data;
                            }
                            fs.Close();
                        }
                        catch
                        {
                            question.IsValid = false;
                            question.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";

                        }
                    }
                    else
                    {
                        question.IsValid = false;
                        question.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";
                    }
                }

                question.InvalidQuestionRemarks = ((Label)row.FindControl
           ("InterviewBatchQuestionEntry_remarksReadOnlyLabel")).Text.Trim();

                questionDetails.Add(question);
            }

            return questionDetails;
        }

        /// <summary>
        /// Method to check whether the questions are valid or not
        /// </summary>
        /// <param name="questions">
        /// A<see cref="List<QuestionDetail>"/>list of question details
        /// </param>
        /// <param name="options">
        /// A<see cref="string"/>that holds the options to check
        /// </param>
        private bool CheckIsValidData(List<QuestionDetail> questions, string options)
        {
            string errorMessage = string.Empty;
            System.Text.StringBuilder errorText = null;
            try
            {
                if (options != "Invalid")
                {
                    foreach (QuestionDetail question in questions)
                    {
                        if (Utility.IsNullOrEmpty(errorText))
                            errorText = new System.Text.StringBuilder();
                        //To check whether the question is null
                        if (question.Question.Trim().Length == 0)
                        {
                            errorText.Append(Resources.HCMResource.InterviewBatchQuestionEntry_PleaseEnterQuestion);
                            errorText.Append(", ");
                            //ShowMessage(InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                            //InterviewBatchQuestionEntry_topErrorMessageLabel, string.Format("Question - " + question.QuestionID +
                            //" " + Resources.HCMResource.InterviewBatchQuestionEntry_PleaseEnterQuestion));
                        }

                        //To check whether the anser options are valid or not 
                        List<AnswerChoice> answers = new List<AnswerChoice>();
                        answers = (question.AnswerChoices.FindAll(delegate(AnswerChoice answer)
                        {
                            return answer.Choice.Trim().Length == 0;
                        }));



                        AnswerChoice correctAnswer = question.AnswerChoices.Find(delegate(AnswerChoice answer)
                        {
                            return answer.ChoiceID == question.Answer;
                        });
                        if (correctAnswer != null && correctAnswer.Choice.Trim().Length == 0)
                        {
                            errorText.Append(Resources.HCMResource.InterviewBatchQuestionEntry_PleaseEnterCorrectChoice);
                            errorText.Append(", ");
                            //ShowMessage(BatchQuestionEntry_bottomErrorMessageLabel,
                            //    BatchQuestionEntry_topErrorMessageLabel, string.Format("Question - " +
                            //  question.QuestionID + " " + Resources.HCMResource.
                            //  BatchQuestionEntry_PleaseEnterCorrectChoice));
                        }
                        if (question.CategoryName == null || question.SubjectName == null ||
                            question.CategoryName.Trim().Length == 0 || question.SubjectName.Trim().Length == 0)
                        {
                            errorText.Append("Please enter category  and subject, ");
                            //ShowMessage(InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                            //    InterviewBatchQuestionEntry_topErrorMessageLabel, string.Format("Question - " +
                            //  question.QuestionID + " " + "Please enter category  and subject"));
                        }
                        if (errorText.ToString().Length > 1)
                            ShowMessage(InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                                InterviewBatchQuestionEntry_topErrorMessageLabel, string.Format("Question - " +
                              question.QuestionID + " " + errorText.ToString().TrimEnd(' ').TrimEnd(',')));
                        errorText = null;
                    }
                }
                //To check the selected subjects and categories
                if (options == "Invalid")
                {
                    errorMessage = new QuestionBLManager().CheckValidInterviewQuestions(questions);
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel.Text = "";
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel.Text = "";
                    ShowMessage(InterviewBatchQuestionEntry_bottomErrorMessageLabel, errorMessage);
                    ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel, errorMessage);
                }
                //If the questions are valid and no error message is there to display
                //then save all questions
                if (InterviewBatchQuestionEntry_topSuccessMessageLabel.Text.Trim().Length == 0 &&
                    InterviewBatchQuestionEntry_bottomErrorMessageLabel.Text.Trim().Length == 0 &&
                    errorMessage.Trim().Length == 0)
                    return true;
                else
                    return false;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(errorText)) errorText = null;
            }
        }

        /// <summary>
        /// Method to save the list of questions
        /// </summary>
        private List<QuestionDetail> saveBatchQuestion()
        {
            QuestionDetail question;
            List<QuestionDetail> validQuestions = new List<QuestionDetail>();

            foreach (GridViewRow row in InterviewBatchQuestionEntry_questionGridView.Rows)
            {
                question = new QuestionDetail();

                Label questionIdLabel = ((Label)row.FindControl("InterviewBatchQuestionEntry_questionNoLabel"));

                if (questionIdLabel.Text != "")
                {
                    question.QuestionID = int.Parse(questionIdLabel.Text);
                }

                question.Question = ((TextBox)row.FindControl
                    ("InterviewBatchQuestionEntry_questionTextBox")).Text.Trim();

                question.CategoryName = ((Label)row.FindControl
                    ("InterviewBatchQuestionEntry_categoryQuestionReadOnlyLabel")).Text.Trim();

                question.CategoryID = int.Parse(((HiddenField)row.FindControl
                    ("InterviewBatchQuestionEntry_categoryIDHiddenField")).Value.Trim());

                question.SubjectName = ((Label)row.FindControl(
                "InterviewBatchQuestionEntry_subjectQuestionReadOnlyLabel")).Text.Trim();

                question.SubjectID = int.Parse(((HiddenField)row.FindControl
                    ("InterviewBatchQuestionEntry_subjectIDHiddenField")).Value.Trim());

                Subject subject = new Subject();
                question.Subjects = new List<Subject>();
                question.Subjects.Add(subject);

                question.Subjects[0].IsSelected = true;
                question.Subjects[0].SubjectID = int.Parse(((HiddenField)
                    row.FindControl("InterviewBatchQuestionEntry_subjectIDHiddenField")).Value.Trim());

                question.TestAreaID = ((HiddenField)row.FindControl
                    ("InterviewBatchQuestionEntry_testAreaIDHiddenField")).Value.Trim();

                question.ComplexityName = ((Label)row.FindControl
                    ("InterviewBatchQuestionEntry_complexityQuestionLabel")).Text.Trim();

                question.Complexity = ((HiddenField)row.FindControl
                    ("InterviewBatchQuestionEntry_complexityIDHiddenField")).Value.Trim();
                question.Tag = ((Label)row.FindControl
                    ("InterviewBatchQuestionEntry_tagQuestionReadOnlyLabel")).Text.Trim();

                question.TestAreaName = ((Label)row.FindControl
                    ("InterviewBatchQuestionEntry_testAreaQuestionLabel")).Text.Trim();

                question.AuthorName = ((Label)row.FindControl
                    ("InterviewBatchQuestionEntry_authorQuestionReadOnlyLabel")).Text.Trim();

                question.CreditsEarned = 0.00M;

                question.Author = userID;
                question.CreatedBy = userID;
                question.ModifiedBy = userID;
                question.CorrectAnswer = ((TextBox)row.FindControl
                    ("InterviewBatchQuestionEntry_answerTextBox")).Text.Trim();

                /*  DataList answerDataList = (DataList)row.
                      FindControl("InterviewBatchQuestionEntry_subject_categoryDataList");
                  question.NoOfChoices = Convert.ToInt16(answerDataList.Controls.Count);*/

                int options = 1;

                question.AnswerChoices = new List<AnswerChoice>();

                //foreach (Control control in answerDataList.Controls)
                //{

                TextBox textBox =
                    (TextBox)row.FindControl("InterviewBatchQuestionEntry_answerTextBox");

                

                question.AnswerChoices.Add(new AnswerChoice(textBox.Text.Trim()
                    , options, true));


                question.CreatedDate = DateTime.Now.Date;
                question.ModifiedDate = DateTime.Now.Date;

                // }
                //question.NoOfChoices = Convert.ToInt16(question.AnswerChoices.Count);
                string xlsFilePath = Path.GetDirectoryName(ViewState["PATH_NAME"].ToString());
                string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(ViewState["PATH_NAME"].ToString()));

                string questionImageName = ((LinkButton)row.FindControl
                    ("InterviewBatchQuestionEntry_qstImageLinkButton")).Text.Trim();
                question.HasImage = false;
                question.ImageName = questionImageName;
                if (question.ImageName != "")
                {
                    question.HasImage = true;
                    if (File.Exists(Path.Combine(imagesFolder, question.ImageName)))
                    {
                        try
                        {
                            FileStream fs = File.OpenRead(Path.Combine(imagesFolder, question.ImageName));
                            if (fs.Length > 102400) // if file size greater than 100 KB
                            {
                                question.IsValid = false;
                                question.InvalidQuestionRemarks += "IMAGE SIZE EXCEEDED ABOVE 100KB, ";
                            }
                            else
                            {
                                byte[] data = new byte[fs.Length];
                                fs.Read(data, 0, data.Length);
                                question.QuestionImage = data;
                            }
                            fs.Close();
                        }
                        catch
                        {
                            question.IsValid = false;
                            question.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";

                        }
                    }
                    else
                    {
                        question.IsValid = false;
                        question.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";
                    }
                }
                validQuestions.Add(question);
            }
            return validQuestions;
        }

        /// <summary>
        /// Method to load the next batch of questions
        /// </summary>
        private void LoadNextBatchQuestions()
        {
            // If the valid questions are not saved , it will show
            //a modal popup extender to save the valid questions
            if (InterviewBatchQuestionEntry_questionGridView.Rows.Count > 0 &&
                InterviewBatchQuestionEntry_isSavedHiddenField.Value != "1")
            {
                InterviewBatchQuestionEntry_questionModalPopupExtender.Show();
                return;
            }

            //To check the last question of the file
            double records = Convert.ToDouble(ViewState["TOTAL_QUESTION_COUNT"].ToString()) / GridPageSize;

            //If particular quesiton is last question , 
            //gets the question from the next sheet
            if (records <= int.Parse(ViewState["PAGE_NUMBER"].ToString()))
            {
                List<string> sheetNames = Session["SHEET_NAME"] as List<string>;

                //Store the current sheet name
                string sheetName = ViewState["CURRENT_SHEET_NAME"].ToString();

                //Gets the total question count for the sheet
                //Get the index of the current sheet
                int sheetIndex = sheetNames.FindIndex(delegate(string sheet)
                {
                    return sheet == sheetName;
                });

                if (sheetIndex + 1
                    != sheetNames.Count)
                {
                    //Get the next sheet name
                    GetSheetQuestionCount(sheetNames[sheetIndex + 1],
                        ViewState["PATH_NAME"].ToString());
                    //Reset page number
                    ViewState["PAGE_NUMBER"] = 1;
                }

                if (int.Parse(ViewState["TOTAL_QUESTION_COUNT"].ToString()) == 0
                    || sheetIndex + 1 == sheetNames.Count)
                {
                    InterviewBatchQuestionEntry_pageNavigator.MoveToPage(1);
                    ShowMessage(InterviewBatchQuestionEntry_bottomErrorMessageLabel,
                        Resources.HCMResource.InterviewBatchQuestionEntry_LastRecord);
                    ShowMessage(InterviewBatchQuestionEntry_topErrorMessageLabel,
                        Resources.HCMResource.InterviewBatchQuestionEntry_LastRecord);
                    List<QuestionDetail> nullDataSource = new List<QuestionDetail>();
                    InterviewBatchQuestionEntry_questionGridView.DataSource
                        = nullDataSource;
                    InterviewBatchQuestionEntry_questionGridView.DataBind();

                    //List<QuestionDetail> invalidQuestions = Session["INTERVIEW_INVALID_QUESTIONS"]
                    //    as List<QuestionDetail>;

                    //InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataSource = invalidQuestions;
                    //InterviewBatchQuestionEntry_inCompleteQuestionsGridView.DataBind();

                    ViewState["INVALID_QUESTIONS_PAGE_NUMBER"] = 1;

                    BindInvalidQuestion(1);

                    if (InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count > 0)
                    {
                        return;
                    }

                    ResetValues();

                    InterviewBatchQuestionEntry_bottomErrorMessageLabel.Text = string.Empty;

                    InterviewBatchQuestionEntry_topErrorMessageLabel.Text = string.Empty;

                    ShowMessage(InterviewBatchQuestionEntry_bottomSuccessMessageLabel,
                    InterviewBatchQuestionEntry_topSuccessMessageLabel,
                    Resources.HCMResource.
                    InterviewBatchQuestionEntry_AllQuestionsProcessed);

                    return;
                }
            }
            else
            {
                ViewState["PAGE_NUMBER"] = int.Parse(ViewState["PAGE_NUMBER"].ToString()) + 1;
            }
            LoadExcelFile(ViewState["PATH_NAME"].ToString());

            InterviewBatchQuestionEntry_isSavedHiddenField.Value = "0";
        }

        /// <summary>
        /// Method to get the current sheet name 
        /// </summary>
        /// <param name="fileName">
        /// A<see cref="string"/>that stores the name of the file
        /// </param>
        private void GetSheetName(string fileName)
        {
            //Get the list of sheet name in the excel 
            List<string> sheetName = new ExcelBatchReader(fileName).
                GetSheetNames();

            //Stores the sheet details in session
            Session["SHEET_NAME"] = sheetName;

            //Store the current sheet name
            ViewState["CURRENT_SHEET_NAME"] = sheetName[0];

            //Gets the total question count for the sheet
            GetSheetQuestionCount(sheetName[0], fileName);
        }

        /// <summary>
        /// Method to get the question count in current sheet
        /// </summary>
        /// <param name="sheetName">
        /// A<see cref="string"/>that holds the sheet name
        /// </param>
        /// <param name="fileName">
        /// A<see cref="string"/>that holds the file name
        /// </param>
        private void GetSheetQuestionCount(string sheetName, string fileName)
        {
            //Get the total question count of the first sheet
            //and store it in view state
            ViewState["TOTAL_QUESTION_COUNT"] = new ExcelBatchReader
                                              (fileName).GetCount(sheetName);

            //Checks if the number of question in the current sheet is zero,
            //if it is not then it will save the question count in session

            if (int.Parse(ViewState["TOTAL_QUESTION_COUNT"].ToString()) != 0)
            {
                ViewState["CURRENT_SHEET_NAME"] = sheetName;
                return;
            }

            //If the current sheet total question is zero.it checks the next sheet

            //Gets the list of sheet name from session 
            List<string> sheetNames = Session["SHEET_NAME"] as List<string>;

            //Get the index of the current sheet
            int sheetIndex = sheetNames.FindIndex(delegate(string sheet)
            {
                return sheet == sheetName;
            });
            //if the current sheet is the last sheet , it will return
            if (sheetNames.Count - 1 == sheetIndex)
                return;

            //else it will find count for next sheet
            GetSheetQuestionCount(sheetNames[sheetIndex + 1], fileName);
        }

        /// <summary>
        /// Mathod that is used to get the current invalid questions
        /// </summary>
        /// <returns>
        /// A<see cref=List<QuestionDetail>/>that has
        /// the invalid questions
        /// </returns>
        private List<QuestionDetail> GetCurrentInvalidQuestions()
        {
            List<QuestionDetail> questions = Session["INTERVIEW_INVALID_QUESTIONS"] as List<QuestionDetail>;

            List<QuestionDetail> currentQuestion = new List<QuestionDetail>();

            currentQuestion = GetInvalidQuestion();

            int lastNumber = int.Parse(ViewState
                 ["INVALID_QUESTIONS_PAGE_NUMBER"].ToString()) * GridPageSize;

            int index = lastNumber - GridPageSize;

            questions.RemoveRange(index, InterviewBatchQuestionEntry_inCompleteQuestionsGridView.Rows.Count);

            questions.InsertRange(index, currentQuestion);

            return questions;
        }

        /// <summary>
        /// Method to reset the values 
        /// </summary>
        private void ResetValues()
        {
            //Clear the questions in the valid question sessions
            Session["INTERVIEW_VALID_QUESTIONS"] = null;

            //Clear the questions in the invalid question sessions
            Session["INTERVIEW_INVALID_QUESTIONS"] = null;

            //Make the upload excel div as visible true
            InterviewBatchQuestionEntry_uploadExcelDiv.Style["display"] = "block";

            //Make the upload result div as visible false
            InterviewBatchQuestionEntry_resultDiv.Style["display"] = "none";

            //Clear the record label text
            InterviewBatchQuestionEntry_recordLabel.Text = string.Empty;

            //Make the total question count as 0
            ViewState["TOTAL_QUESTION_COUNT"] = 0;

            //Make the page number as 1
            ViewState["PAGE_NUMBER"] = 1;

            //Clear the path name from view state
            ViewState["PATH_NAME"] = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
    }
}



