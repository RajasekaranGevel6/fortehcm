<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="InterviewSingleQuestionEntry.aspx.cs" Inherits="Forte.HCM.UI.InterviewQuestions.InterviewSingleQuestionEntry" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/DisclaimerControl.ascx" TagName="DisclaimerControl"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="InterviewSingleQuestionEntry_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <script type="text/javascript" language="javascript">

        // This method used for avoiding multiple radio button selection
        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById("<%= InterviewSingleQuestionEntry_answerChoicesGridView.ClientID %>");
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {
                if (items[i].id != CurrentRdbID && items[i].type == "radio") {
                    if (items[i].checked) {
                        items[i].checked = false;
                        items[i].parentElement.parentElement.style.backgroundColor = 'white';
                        items[i].parentElement.parentElement.style.color = 'black';
                    }
                }
            }
        }
        function ShowAddImagePanel(addImageTRID, addImageBtnID) {
            var addImageTR = document.getElementById(addImageTRID);
            addImageTR.style.display = "";
            var addImageBtn = document.getElementById(addImageBtnID);
            addImageBtn.style.display = "none";

            return false;
        }
       
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                            <asp:Literal ID="InterviewSingleQuestionEntry_headerLiteral" runat="server"></asp:Literal>
                        </td>
                        <td style="width: 28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 22%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Button ID="InterviewSingleQuestionEntry_topSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="InterviewSingleQuestionEntry_saveButton_Click" TabIndex="12" />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Button ID="InterviewSingleQuestionEntry_topPreviewButton" runat="server" SkinID="sknButtonId"
                                            Text="Preview" OnClick="InterviewSingleQuestionEntry_previewButton_Click" TabIndex="13" />
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="InterviewSingleQuestionEntry_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="InterviewSingleQuestionEntry_resetLinkButton_Click"
                                            TabIndex="14" />
                                    </td>
                                    <td style="width: 4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td style="width: 18%" align="left">
                                        <asp:LinkButton ID="InterviewSingleQuestionEntry_topCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" TabIndex="15" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SinleQuestionEntry_topMessageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="InterviewSingleQuestionEntry_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewSingleQuestionEntry_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="InterviewSingleQuestionEntry_categorySubjectControl" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="panel_bg">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr runat="server" id="InterviewSingleQuestionEntry_editModeTR" visible="false">
                                    <td valign="bottom">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 50%">
                                                    <div style="float: left">
                                                        <asp:Label ID="InterviewSingleQuestionEntry_questionIDLabel" runat="server" Text="Question ID "
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        &nbsp;<asp:Label ID="InterviewSingleQuestionEntry_questionIDValueLabel" runat="server" Text=""
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                                <td style="width: 50%">
                                                    <div style="float: left">
                                                        <asp:Label ID="InterviewSingleQuestionEntry_lastModifiedDateLabel" runat="server" Text="Last Modified Date "
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        &nbsp;<asp:Label ID="InterviewSingleQuestionEntry_lastModifiedDateValueLabel" runat="server"
                                                            Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="tr_height_2">
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <asp:Literal ID="InterviewSingleQuestionEntry_questionLiteral" runat="server" Text="Question"></asp:Literal>
                                        <span class='mandatory'>*</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg" style="padding: 3px;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="InterviewSingleQuestionEntry_questionTextBox" runat="server" TextMode="MultiLine"
                                                        Columns="150" Height="50" MaxLength="1500" SkinID="sknMultiLineTextBox" onkeyup="CommentsCount(1500,this)"
                                                        onchange="CommentsCount(1500,this)" TabIndex="1"></asp:TextBox>
                                                    <asp:LinkButton ID="InterviewSingleQuestionEntry_addImageLinkButton" runat="server" SkinID="sknActionLinkButton" Text="Add Image" />
                                                </td>
                                            </tr>
                                            <tr id="InterviewSingleQuestionEntry_AddQuestionImageTR" runat="server" style="display:none">
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="InterviewSingleQuestionEntry_namePopHeadLabel" runat="server" Text="Select Image"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 65%; text-align: right">
                                                                <%-- <asp:FileUpload ID="SingleQuestionEntry_fileUpload" runat="server" Width="100%" />--%>
                                                                <ajaxToolKit:AsyncFileUpload ID="InterviewSingleQuestionEntry_fileUpload" runat="server" Width="250px" />
                                                            </td>
                                                            <td style="width: 5%">
                                                                <asp:ImageButton ID="InterviewSingleQuestionEntry_helpImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                    ToolTip="Add Image" OnClientClick="javascript:return false;"/>
                                                            </td>
                                                            <td style="width: 10%; text-align: left">
                                                                <asp:Button ID="InterviewSingleQuestionEntry_addImageButton" runat="server" Text="Add" SkinID="sknButtonId"
                                                                    OnClick="InterviewSingleQuestion_questionImageButtonClick" ToolTip="Click to add image" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="InterviewSingleQuestionEntry_DisplayQuestionImageTR" runat="server"  style="display:none">
                                                <td >
                                                    <table>
                                                        <tr>
                                                            <td >
                                                            <asp:Image runat="server" ID="InterviewSingleQuestionEntry_questionImage" />
                                                            </td>
                                                            <td>
                                                            <asp:LinkButton ID="InterviewSingleQuestionEntry_deleteLinkButton" runat="server" Text="Delete" SkinID="sknActionLinkButton"  OnClick="SingleQuestion_deleteQstLnkButtonClick" ToolTip="Click to delete image" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <asp:Literal ID="InterviewSingleQuestionEntry_answerChoicesLiteral" runat="server" Text="Answer Choices<span class='mandatory'>&nbsp;*</span>"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <asp:UpdatePanel ID="InterviewSingleQuestionEntry_choicesUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:GridView ID="InterviewSingleQuestionEntry_answerChoicesGridView" runat="server" AllowPaging="true"
                                                                AllowSorting="true" AutoGenerateColumns="false" OnRowCommand="InterviewSingleQuestionEntry_answerChoicesGridView_RowCommand"
                                                                OnRowDataBound="InterviewSingleQuestionEntry_answerChoicesGridView_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="InterviewSingleQuestionEntry_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                                ToolTip="Remove Choice" CommandName="DeleteRow" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--<asp:TemplateField ItemStyle-Width="7%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                        HeaderText="Answer">
                                                                        <ItemTemplate>
                                                                            <asp:RadioButton ID="InterviewSingleQuestionEntry_correctAnswerRadioButton" runat="server"
                                                                                onclick="CheckOtherIsCheckedByGVID(this)" TabIndex="3" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="Choices">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="InterviewSingleQuestionEntry_answerTextBox" runat="server" TextMode="MultiLine"
                                                                                Columns="130" MaxLength="500" Height="30px" SkinID="sknMultiLineTextBox" Text='<%# Eval("Choice") %>'
                                                                                onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" TabIndex="2"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="InterviewSingleQuestionEntry_isCorrectAnswerHiddenField" runat="server"
                                                                                Value='<%# Eval("IsCorrect") %>' />
                                                                            <asp:HiddenField ID="InterviewSingleQuestionEntry_questionOptionIdHiddenField" runat="server"
                                                                                Value='<%# Eval("QuestionGenId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="padding-left: 6px; height: 20px;">
                                                            <asp:LinkButton ID="InterviewSingleQuestionEntry_addAnswerLinkButton" SkinID="sknAddLinkButton"
                                                                ToolTip="Add New Choice" runat="server" Text="Add" TabIndex="4" OnClick="InterviewSingleQuestionEntry_addAnswerLinkButton_Click"></asp:LinkButton>
                                                            <asp:HiddenField ID="InterviewSingleQuestionEntry_deletedChoicesHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="InterviewSingleQuestionEntry_categorySubjectControlUpdatePanel" runat="server"
                                            UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <uc1:CategorySubjectControl ID="InterviewSingleQuestionEntry_categorySubjectControl" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_inner_body_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 10%">
                                                    <asp:Label ID="InterviewSingleQuestionEntry_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                </td>
                                                <td class="checkbox_list_bg" align="left" style="width: 90%">
                                                    <asp:RadioButtonList ID="InterviewSingleQuestionEntry_testAreaRadioButtonList" runat="server"
                                                        RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" TextAlign="Right"
                                                        Width="100%" TabIndex="5">
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_inner_body_bg" align="left">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 10%">
                                                    <asp:Label ID="InterviewSingleQuestionEntry_complexityLabel" runat="server" Text="Complexity"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                </td>
                                                <td style="width: 20%">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:DropDownList ID="InterviewSingleQuestionEntry_complexityDropDownList" runat="server"
                                                            Width="133px" TabIndex="6">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="InterviewSingleQuestionEntry_complexityImageButton" SkinID="sknHelpImageButton"
                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the complexity of the question here" />
                                                    </div>
                                                </td>
                                                <td style="width: 6%">
                                                    <asp:Label ID="InterviewSingleQuestionEntry_questionLabel" runat="server" Text="Tags" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 34%">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:TextBox ID="InterviewSingleQuestionEntry_tagsTextBox" runat="server" MaxLength="100"
                                                            onkeyup="CommentsCount(100,this)" TabIndex="7" Width="250px" onchange="CommentsCount(100,this)"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="InterviewSingleQuestionEntry_tagsImageButton" SkinID="sknHelpImageButton"
                                                            runat="server" ImageAlign="Middle" TabIndex="8" OnClientClick="javascript:return false;"
                                                            ToolTip="Please enter tags to a question here that represents additional information related to a question which helps in finding exact match during search" />
                                                    </div>
                                                </td>
                                                <td style="width: 7%">
                                                    <asp:Label ID="InterviewSingleQuestionEntry_authorHeadLabel" runat="server" Text="Author"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 33%" valign="middle">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:TextBox ID="InterviewSingleQuestionEntry_authorTextBox" runat="server" MaxLength="50"
                                                            onkeyup="CommentsCount(50,this)" onchange="CommentsCount(50,this)" TabIndex="9"
                                                            ReadOnly="true"></asp:TextBox>
                                                        <asp:HiddenField ID="InterviewSingleQuestionEntry_authorIdHiddenField" runat="server" />
                                                        <asp:HiddenField ID="InterviewSingleQuestionEntry_dummyAuthorId" runat="server" />
                                                        <asp:HiddenField ID="InterviewSingleQuestionEntry_createdByHiddenField" runat="server" />
                                                        <asp:HiddenField ID="InterviewSingleQuestionEntry_createdDateHiddenField" runat="server" />
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="InterviewSingleQuestionEntry_authorImageButton" SkinID="sknbtnSearchicon"
                                                            runat="server" ImageAlign="Middle" ToolTip="Click here to select the question author"
                                                            TabIndex="10" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" align="left">
                                                    <asp:Label ID="InterviewSingleQuestionEntry_creditHeadLabel" runat="server" Text="Credits Earned (in $)"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    <asp:TextBox ID="InterviewSingleQuestionEntry_creditTextBox" MaxLength="9" runat="server"
                                                        TabIndex="11" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="InterviewSingleQuestionEntry_bottomMessageUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="InterviewSingleQuestionEntry_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewSingleQuestionEntry_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="InterviewSingleQuestionEntry_categorySubjectControl" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text">
                        </td>
                        <td style="width: 28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 22%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Button ID="InterviewSingleQuestionEntry_bottomSaveButton" runat="server" Text="Save"
                                            OnClick="InterviewSingleQuestionEntry_saveButton_Click" SkinID="sknButtonId" TabIndex="16" />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Button ID="InterviewSingleQuestionEntry_bottomPreviewButton" runat="server" SkinID="sknButtonId"
                                            Text="Preview" OnClick="InterviewSingleQuestionEntry_previewButton_Click" TabIndex="17" />
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="InterviewSingleQuestionEntry_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="InterviewSingleQuestionEntry_resetLinkButton_Click"
                                            TabIndex="18" />
                                    </td>
                                    <td style="width: 4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td style="width: 18%" align="left">
                                        <asp:LinkButton ID="InterviewSingleQuestionEntry_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" TabIndex="19" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <%-- Question Preview Popup Extender --%>
                <asp:Panel ID="InterviewSingleQuestionEntry_questionPreviewPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_question_detail">
                    <asp:HiddenField ID="InterviewSingleQuestionEntry_hiddenFieldButton" runat="server" />
                    <uc2:QuestionDetailPreviewControl ID="InterviewSingleQuestionEntry_questionPreviewControl"
                        runat="server" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="InterviewSingleQuestionEntry_questionPreviewModalPopupExtender"
                    runat="server" PopupControlID="InterviewSingleQuestionEntry_questionPreviewPanel" BackgroundCssClass="modalBackground"
                    TargetControlID="InterviewSingleQuestionEntry_hiddenFieldButton">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="InterviewSingleQuestionEntry_deletePopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_remove">
                    <uc3:ConfirmMsgControl ID="InterviewSingleQuestionEntry_deletePopupExtenderControl" runat="server"
                        Message="Are you sure want to delete this choice?" Title="Delete Choice" />
                </asp:Panel>
                <div id="InterviewSingleQuestionEntry_hiddenDeleteButtonDIV" runat="server" style="display: none;">
                    <asp:Button ID="InterviewSingleQuestionEntry_hiddenDeletePopupModalButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="InterviewSingleQuestionEntry_deletePopupExtender" runat="server"
                    PopupControlID="InterviewSingleQuestionEntry_deletePopupPanel" TargetControlID="InterviewSingleQuestionEntry_hiddenDeletePopupModalButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <%--Disclaimer Popup--%>
                <asp:Panel ID="InterviewSingleQuestionEntry_disclaimerPopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_disclaimer">
                    <uc4:DisclaimerControl ID="InterviewSingleQuestionEntry_disclaimerControl" runat="server"
                        Title="Disclaimer" />
                    <div style="padding-left: 10px">
                        <asp:Button ID="InterviewSingleQuestionEntry_disclaimerAcceptButton" runat="server" SkinID="sknButtonId"
                            Text="Accept" Width="64px" OnClick="DisclaimerControl_acceptButton_Click" />
                        &nbsp;
                        <asp:LinkButton ID="DisclaimerControl_cancelLinkButton" Text="Cancel" runat="server"
                            SkinID="sknPopupLinkButton"></asp:LinkButton>
                    </div>
                </asp:Panel>
                <div id="InterviewSingleQuestionEntry_hiddensavePopupModalDiv" runat="server" style="display: none">
                    <asp:Button ID="InterviewSingleQuestionEntry_hiddensavePopupModalButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="InterviewSingleQuestionEntry_disclaimerPopupExtender"
                    runat="server" PopupControlID="InterviewSingleQuestionEntry_disclaimerPopupPanel" TargetControlID="InterviewSingleQuestionEntry_hiddensavePopupModalButton"
                    BackgroundCssClass="popupcontrol_addImage">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="InterviewSingleQuestionEntry_addImagePanel" runat="server" CssClass="popupcontrol_addImage">
                    <div style="display: none">
                        <asp:Button ID="InterviewSingleQuestionEntry_addImageHiddenButton" runat="server" Text="Hidden" />
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="popup_td_padding_10">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Literal ID="InterviewSingleQuestionEntry_addImageLiteral" runat="server" Text="Add Image"></asp:Literal>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="InterviewSingleQuestionEntry_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                                            OnClick="CloseQuestionImagePopUpClick" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_10" width="100%">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="popupcontrol_addImage_inner_bg">
                                    <tr>
                                        <td style="vertical-align: top">
                                            <table width="100%" style="padding: 2px" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="msg_align" colspan="4">
                                                        <asp:Label ID="InterviewSingleQuestionEntry_ErrorMsgLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td colspan="4">
                                                        * JPG, GIF, PNG files are supported<br />
                                                        * Maximum of 1MB can be uploaded
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_5">
                                <asp:LinkButton ID="ResumeUploader_topCancelButton" runat="server" Text="Cancel"
                                    SkinID="sknPopupLinkButton" OnClick="CloseQuestionImagePopUpClick"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="InterviewSingleQuestionEntry_addImageModalPopupExtender"
                    runat="server" PopupControlID="InterviewSingleQuestionEntry_addImagePanel" TargetControlID="InterviewSingleQuestionEntry_addImageHiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
