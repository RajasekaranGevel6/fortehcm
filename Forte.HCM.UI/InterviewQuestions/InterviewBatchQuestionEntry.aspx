﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="InterviewBatchQuestionEntry.aspx.cs" Inherits="Forte.HCM.UI.InterviewQuestions.InterviewBatchQuestionEntry" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/DisclaimerControl.ascx" TagName="DisclaimerControl"
    TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" runat="server" ID="InterviewBatchQuestionEntry_body">
    <script type="text/javascript" language="javascript">

        // Function that will open the download window.
        function DownloadInterviewBatchTemplate() 
        {
            window.open('../Common/Download.aspx?type=BIQT', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }

        //Method that is called when the options radio button list is clicked
        function CheckOtherIsCheckedByGVID(spanChk, gridId) {
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById(gridId);
            var items = Parent.getElementsByTagName('input');
            //Loops through the options in list
            for (i = 0; i < items.length; i++) {
                if (items[i].id != CurrentRdbID && items[i].type == "radio") {
                    //If one item is selected then make other item non selectable
                    if (items[i].checked) {
                        items[i].checked = false;
                        items[i].parentElement.parentElement.style.backgroundColor = 'white';
                        items[i].parentElement.parentElement.style.color = 'black';
                    }
                }
            }
        }

        function ShowQuestionImage(path) {
            window.open(path);
            return false;
        }

        // Function that shows the add question image popup
        function ShowQuestionImagePopup(path, ctrlId) {
            var height = 250;
            var width = 450;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes;toolbars=no,menubar=no,location=no";

            var queryStringValue = "../popup/AddQuestionImage.aspx?imagePath=" + path + "&ctrlId=" + ctrlId;

            //window.open(queryStringValue, window.self, sModalFeature);
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="InterviewBatchQuestionEntry_headerLiteral" runat="server" Text="Batch Interview Question Entry"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="InterviewBatchQuestionEntry_topResetLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Reset" OnClick="InterviewBatchQuestionEntry_resetLinkButton_Click"
                                            CausesValidation="false" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="InterviewBatchQuestionEntry_topCancelLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <table width="100%">
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="InterviewBatchQuestionEntry_topSuccessMessageLabel" runat="server"
                                Tag="12" SkinID="sknSuccessMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="InterviewBatchQuestionEntry_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="SearchQuestion_stateHiddenField" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <div id="InterviewBatchQuestionEntry_uploadExcelDiv" runat="server" style="display: block">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="td_height_8">
                            </td>
                        </tr>
                        <tr>
                            <td class="header_bg">
                                <asp:Literal ID="InterviewBatchQuestionEntry_fileSelectionLiteral" runat="server"
                                    Text="File Selection"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="grid_body_bg">
                                <table border="0" cellpadding="2" cellspacing="5" width="70%">
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions_title">
                                            Before performing a batch upload of your questions, please ensure that
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            1. You are using the ForteHCM supplied excel template. Click
                                            <asp:LinkButton runat="server" ID="BatchQuestionEntry_downloadTemplateLinkButton"
                                                CssClass="batch_upload_download_template_link_btn" Font-Size ="15px"> here </asp:LinkButton>
                                            to download the template.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            2. The excel sheet name does not contain any special characters and spaces
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            3. You upload all your images as a separate zipped file
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            4. The excel file is closed before you click on the upload button
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 45%">
                                            <asp:Label ID="InterviewBatchQuestionEntry_fileNameLabel" runat="server" Text="Excel File (that contains the questions)"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            <span class="mandatory">*</span>
                                        </td>
                                        <td valign="middle">
                                            <div style="float: left; padding-right: 5px;">
                                                <asp:FileUpload ID="InterviewBatchQuestionEntry_fileUpload" runat="server" />
                                            </div>
                                            <div style="float: left;">
                                                <asp:ImageButton ID="InterviewBatchQuestionEntry_helpImageButton" SkinID="sknHelpImageButton"
                                                    runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the excel file that contains the questions" />
                                            </div>
                                        </td>
                                        <%--   <td>
                                            <asp:Button ID="InterviewBatchQuestionEntry_uploadButton" runat="server" Text="Upload" SkinID="sknButtonId"
                                                OnClick="InterviewBatchQuestionEntry_uploadButton_Click" />
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:RegularExpressionValidator ID="InterviewBatchQuestionEntry_regularExpressionValidataor"
                                                runat="server" ControlToValidate="InterviewBatchQuestionEntry_fileUpload" ErrorMessage="Only excel files are allowed"
                                                ValidationExpression="^.+\.((xls|xlsx))$">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="InterviewBatchQuestionEntry_zipUploadLabel" runat="server" Text="Zip File (that contains the supported images)"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </td>
                                        <td valign="middle">
                                            <div style="float: left; padding-right: 5px;">
                                                <asp:FileUpload ID="InterviewBatchQuestionEntry_zipFileUpload" runat="server" />
                                            </div>
                                            <div style="float: left;">
                                                <asp:ImageButton ID="InterviewBatchQuestionEntry_zipHelpImage" SkinID="sknHelpImageButton"
                                                    runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the zip file that contains the images" /></div>
                                        </td>
                                        <%-- <td>
                                            <asp:Button ID="InterviewBatchQuestionEntry_zipUploadButton" runat="server" Text="Upload Zip File" SkinID="sknButtonId"
                                                OnClick="InterviewBatchQuestionEntry_zipUploadButton_Click" />
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:RegularExpressionValidator ID="InterviewBatchQuestionEntry_zipUploadRegEx" runat="server"
                                                ControlToValidate="InterviewBatchQuestionEntry_zipFileUpload" ErrorMessage="Only zip files are allowed"
                                                ValidationExpression="^.+\.((zip))$">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right">
                                            <asp:Button ID="InterviewBatchQuestionEntry_uploadButton" runat="server" Text="Upload"
                                                SkinID="sknButtonId" OnClick="InterviewBatchQuestionEntry_uploadButton_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="InterviewBatchQuestionEntry_resultDiv" runat="server" visible="false">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="msg_align_center">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="InterviewBatchQuestionEntry_recordLabel" runat="server" Tag="12" Text=""
                                                SkinID="sknRecordCount"></asp:Label>
                                            <span class="signin_text">|</span>
                                            <asp:LinkButton ID="InterviewBatchQuestionEntry_nextLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Next" OnClick="InterviewBatchQuestionEntry_nextLinkButton_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ajaxToolKit:TabContainer ID="InterviewBatchQuestionEntry_mainTabContainer" runat="server"
                                    ActiveTabIndex="0">
                                    <ajaxToolKit:TabPanel ID="InterviewBatchQuestionEntry_questionsTabPanel" HeaderText="Questions"
                                        runat="server">
                                        <HeaderTemplate>
                                            Questions</HeaderTemplate>
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="5%" width="100%">
                                                <tr>
                                                    <td class="msg_align">
                                                        <asp:Label ID="InterviewBatchQuestionEntry_questionsTabPanel_errorMessageLabel" runat="server"
                                                            SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="InterviewBatchQuestionEntry_removeQuestionLinkButton" runat="server"
                                                                        SkinID="sknActionLinkButton" Text="Remove&nbsp;All" OnClick="InterviewBatchQuestionEntry_removeQuestionLinkButton_Click" />
                                                                </td>
                                                                <td style="width: 5%">
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="InterviewBatchQuestionEntry_saveButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Save" OnClick="InterviewBatchQuestionEntry_saveButton_Click" /><asp:HiddenField
                                                                            ID="InterviewBatchQuestionEntry_isSavedHiddenField" runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView ID="InterviewBatchQuestionEntry_questionGridView" runat="server" AutoGenerateColumns="False"
                                                OnRowDataBound="InterviewBatchQuestionEntry_questionsDataList_RowDataBound" ShowHeader="False"
                                                SkinID="sknNewGridView" OnRowCommand="InterviewBatchQuestionEntry_questionGridView_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                <tr>
                                                                    <td valign="middle" style="width: 8%">
                                                                        <div style="float: left; width: 35%;">
                                                                            <asp:ImageButton ID="InterviewBatchQuestionEntry_questionsGridView_deleteImageButton"
                                                                                runat="server" SkinID="sknDeleteImageButton" ToolTip="Delete Question" CommandName="DeleteQuestion"
                                                                                CommandArgument='<%# Eval("QuestionID")%>' /></div>
                                                                        <div style="float: left;">
                                                                            <asp:Image ID="InterviewBatchQuestionEntry_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                ToolTip="Question" />
                                                                            <asp:Label ID="InterviewBatchQuestionEntry_questionNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server" ReadOnly="true" Text='<%# Eval("QuestionID") %>'>
                                                                            </asp:Label></div>
                                                                    </td>
                                                                    <td colspan="6" style="width: 92%">
                                                                        <asp:TextBox ID="InterviewBatchQuestionEntry_questionTextBox" runat="server" SkinID="sknMultiLineTextBox"
                                                                            TextMode="MultiLine" Width="91%" Height="40" Text='<%# Eval("Question") %>' MaxLength="500"
                                                                            onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="InterviewBatchQuestionEntry_imageLinkTR">
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_imageNameHeadLabel" runat="server" Text="Image Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="5">
                                                                        <asp:LinkButton ID="InterviewBatchQuestionEntry_qstImageLinkButton" SkinID="sknActionLinkButton"
                                                                            runat="server" Text='<%# Eval("ImageName") %>' Visible='<%# Eval("HasImage") %>'> 
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_answerNameHeadLabel" runat="server" Text="Answer"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="6" style="width: 98%">
                                                                        <asp:TextBox ID="InterviewBatchQuestionEntry_answerTextBox" SkinID="sknMultiLineTextBox"
                                                                            runat="server" TextMode="MultiLine" Text='<%# Eval("CorrectAnswer") %>' Width="80%"
                                                                            Height="30px" MaxLength="200" onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_categoryQuestionLabel" runat="server"
                                                                            Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_categoryQuestionReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_categoryIDHiddenField" runat="server"
                                                                            Value='<%# Eval("CategoryID") %>' />
                                                                    </td>
                                                                    <td style="width: 6%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_subjectQuestionLabel" runat="server" Text="Subject"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_subjectQuestionReadOnlyLabel" runat="server"
                                                                            Text='<%# Eval("SubjectName") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_subjectIDHiddenField" runat="server"
                                                                            Value='<%# Eval("SubjectID") %>' />
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_testAreaQuestionHeadLabel" runat="server"
                                                                            Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_testAreaQuestionLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_testAreaIDHiddenField" runat="server"
                                                                            Value='<%# Eval("TestAreaID") %>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_complexityQuestionHeadLabel" runat="server"
                                                                            Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_complexityQuestionLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("ComplexityName") %>' />
                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_complexityIDHiddenField" runat="server"
                                                                            Value='<%# Eval("Complexity") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_tagQuestionHeadLabel" runat="server" Text="Tag"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_tagQuestionReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("Tag") %>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_authorQuestionHeadLabel" runat="server"
                                                                            Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_authorQuestionReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" ReadOnly="true" Text='<%# Eval("AuthorName") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </ajaxToolKit:TabPanel>
                                    <ajaxToolKit:TabPanel ID="InterviewBatchQuestionEntry_inCompleteQuestionsTabPanel"
                                        HeaderText="Incomplete Questions" runat="server">
                                        <HeaderTemplate>
                                            Incomplete Questions</HeaderTemplate>
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="5%" width="100%">
                                                <tr>
                                                    <td class="msg_align">
                                                        <asp:Label ID="InterviewBatchQuestionEntry_inCompleteQuestionsTabPanel_errorMessageLabel"
                                                            runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="InterviewBatchQuestionEntry_removeInvalidQuestionLinkButton"
                                                                        runat="server" SkinID="sknActionLinkButton" Text="Remove&nbsp;All" OnClick="InterviewBatchQuestionEntry_removeInvalidQuestionLinkButton_Click" />
                                                                </td>
                                                                <td style="width: 5%">
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="InterviewBatchQuestionEntry_invalidSaveButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Save" OnClick="InterviewBatchQuestionEntry_invalidSaveButton_Click" /><asp:HiddenField
                                                                            ID="InterviewBatchQuestionEntry_isInvalidSavedHiddenField" runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView ID="InterviewBatchQuestionEntry_inCompleteQuestionsGridView" runat="server"
                                                AutoGenerateColumns="false" OnRowDataBound="InterviewBatchQuestionEntry_inCompleteQuestionsDataList_RowDataBound"
                                                SkinID="sknNewGridView" ShowFooter="false" ShowHeader="false" OnRowCommand="InterviewBatchQuestionEntry_inCompleteQuestionsGridView_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="2">
                                                                <tr>
                                                                    <td valign="middle" style="width: 8%">
                                                                        <div style="float: left; width: 35%;">
                                                                            <asp:ImageButton ID="InterviewBatchQuestionEntry_questionsGridView_deleteImageButton"
                                                                                runat="server" SkinID="sknDeleteImageButton" ToolTip="Delete Question" CommandName="DeleteQuestion"
                                                                                CommandArgument='<%# Eval("QuestionID")%>' /></div>
                                                                        <div style="float: left;">
                                                                            <asp:Image ID="InterviewBatchQuestionEntry_incompleteQuestionImage" runat="server"
                                                                                SkinID="sknQuestionImage" ToolTip="Question" />
                                                                            <asp:Label ID="InterviewBatchQuestionEntry_incompleteQuestionNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server" ReadOnly="true" Text='<%# Eval("QuestionID") %>'>
                                                                            </asp:Label></div>
                                                                    </td>
                                                                    <td colspan="3" style="width: 92%">
                                                                        <asp:TextBox ID="InterviewBatchQuestionEntry_incompleteQuestionTextBox" runat="server"
                                                                            TextMode="MultiLine" Width="91%" Height="40" SkinID="sknMultiLineTextBox" Text='<%# Eval("Question") %>'
                                                                            MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="InterviewBatchQuestionEntry_addImageLinkTR">
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:LinkButton ID="InterviewBatchQuestionEntry_addQuestionImageLinkButton" runat="server"
                                                                            Text="Add Image" SkinID="sknActionLinkButton" ToolTip="Add Question Image" Visible='<%# Eval("HasImage") %>'>
                                                                        </asp:LinkButton>
                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_qstImageHiddenField" runat="server"
                                                                            Value='<%# Eval("ImageName") %>'></asp:HiddenField>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_invalidQuestionanswerNameHeadLabel" runat="server"
                                                                            Text="Answer" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3" style="width: 98%">
                                                                        <asp:TextBox ID="InterviewBatchQuestionEntry_invalidQuestionanswerNamerTextBox" SkinID="sknMultiLineTextBox"
                                                                            runat="server" TextMode="MultiLine" Text='<%# Eval("CorrectAnswer") %>' Width="80%"
                                                                            Height="30px" MaxLength="200" onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td style="width: 55%" valign="middle">
                                                                        <div style="float: left; width: 92%;" class="grouping_border_bg">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="InterviewBatchQuestionEntry_categoryLabel" runat="server" Text="Category"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 43%">
                                                                                        <asp:TextBox ID="InterviewBatchQuestionEntry_categoryTextBox" runat="server" Text='<%# Eval("CategoryName") %>'
                                                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="InterviewBatchQuestionEntry_subjectLabel" runat="server" Text="Subject"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 22%">
                                                                                        <asp:TextBox ID="InterviewBatchQuestionEntry_subjectTextBox" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_subjectIdHiddenField" runat="server"
                                                                                            Value='<%# Eval("SubjectID") %>' />
                                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_categoryNameHiddenField" runat="server"
                                                                                            Value='<%# Eval("CategoryName") %>' />
                                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_subjectNameHiddenField" runat="server"
                                                                                            Value='<%# Eval("SubjectName") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div style="float: left; padding-left: 5px; padding-top: 5px;">
                                                                            <asp:ImageButton ID="InterviewBatchQuestionEntry_categoryImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ToolTip="Click here to select category and subject" /></div>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_testAreaLabel" runat="server" Text="Interview Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="InterviewBatchQuestionEntry_testAreaDropDownList" runat="server"
                                                                            Width="133px">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_testAreaHiddenField" runat="server"
                                                                            Value='<%# Eval("TestAreaName") %>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td style="width: 55%">
                                                                        <div style="float: left; padding-right: 5px; width: 92%;" class="grouping_bg">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="InterviewBatchQuestionEntry_complexityLabel" runat="server" Text="Complexity"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 43%">
                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                            <asp:DropDownList ID="InterviewBatchQuestionEntry_complexityDropDownList" runat="server"
                                                                                                Width="133px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:HiddenField ID="InterviewBatchQuestionEntry_complexityDropDownListHiddenField"
                                                                                                runat="server" Value='<%#Eval("ComplexityName") %>' />
                                                                                        </div>
                                                                                        <div style="float: left;">
                                                                                            <asp:ImageButton ID="InterviewBatchQuestionEntry_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                runat="server" ImageAlign="Middle" CommandArgument='<%# Eval("Complexity") %>'
                                                                                                OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" /></div>
                                                                                    </td>
                                                                                    <td style="width: 16%">
                                                                                        <asp:Label ID="InterviewBatchQuestionEntry_tagHeadLabel" runat="server" Text="Tag"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 22%">
                                                                                        <asp:TextBox ID="InterviewBatchQuestionEntry_tagTextBox" runat="server" Text='<%# Eval("Tag") %>'
                                                                                            MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_authorHeadLabel" runat="server" Text="Author"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="InterviewBatchQuestionEntry_authorTextBox" runat="server" Text='<%#Eval("AuthorName") %>'>
                                                                            </asp:TextBox>
                                                                            <asp:HiddenField ID="InterviewBatchQuestionEntry_authorHiddenField" runat="server"
                                                                                Value='<%#Eval("Author") %>' />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="InterviewBatchQuestionEntry_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the question author" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <asp:Label ID="InterviewBatchQuestionEntry_remarksHeadLabel" runat="server" Text="Remarks "
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="InterviewBatchQuestionEntry_remarksReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                                        runat="server" ReadOnly="true" Text='<%#Eval("InvalidQuestionRemarks") %>'>                                                                                        
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <uc1:PageNavigator ID="InterviewBatchQuestionEntry_pageNavigator" runat="server" />
                                        </ContentTemplate>
                                    </ajaxToolKit:TabPanel>
                                </ajaxToolKit:TabContainer>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="InterviewBatchQuestionEntry_removepopupPanel" runat="server" Style="display: none"
                                    CssClass="popupcontrol_confirm_remove">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                            <asp:Label ID="InterviewBatchQuestionEntry_confirmMsgControl_titleLabel" runat="server"
                                                                Text="Remove Confirm"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%" valign="top" align="right">
                                                            <asp:ImageButton ID="InterviewBatchQuestionEntry_confirmMsgControl_questionCloseImageButton"
                                                                runat="server" SkinID="sknCloseImageButton" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                    <tr>
                                                        <td align="left" class="popup_td_padding_10">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="InterviewBatchQuestionEntry_confirmMsgControl_messageValidLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text="Are you sure want to remove all the questions?"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr valign="bottom">
                                                                    <td align="right" style="text-align: center">
                                                                        <asp:Button ID="InterviewBatchQuestionEntry_confirmMsgControl_yesValidButton" runat="server"
                                                                            SkinID="sknButtonId" Text="Yes" Width="64px" OnClick="InterviewBatchQuestionEntry_confirmMsgControl_yesValidButton_Click" />
                                                                        <asp:Button ID="InterviewBatchQuestionEntry_confirmMsgControl_noValidButton" runat="server"
                                                                            SkinID="sknButtonId" Text="No" Width="45px" OnClientClick="javascript:return false;" />
                                                                        <asp:HiddenField ID="InterviewBatchQuestionEntry_questionIDHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <div style="display: none">
                                    <asp:Button ID="InterviewBatchQuestionEntry_hiddenButton" runat="server" />
                                </div>
                                <ajaxToolKit:ModalPopupExtender ID="InterviewBatchQuestionEntry_deletepopupExtender"
                                    runat="server" PopupControlID="InterviewBatchQuestionEntry_removepopupPanel"
                                    TargetControlID="InterviewBatchQuestionEntry_hiddenButton" BackgroundCssClass="modalBackground"
                                    CancelControlID="InterviewBatchQuestionEntry_confirmMsgControl_noValidButton"
                                    DynamicServicePath="" Enabled="True">
                                </ajaxToolKit:ModalPopupExtender>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <table width="100%">
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="InterviewBatchQuestionEntry_bottomSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="InterviewBatchQuestionEntry_bottomErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="InterviewBatchQuestionEntry_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="InterviewBatchQuestionEntry_resetLinkButton_Click"
                                            CausesValidation="false" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="InterviewBatchQuestionEntry_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Panel ID="InterviewBatchQuestionEntry_questionPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_next">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="popup_td_padding_10">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Literal ID="InterviewBatchQuestionEntry_titleLiteral" runat="server" Text="Confirmation Message"></asp:Literal>
                                        </td>
                                        <td style="width: 50%" valign="top" align="right">
                                            <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                                                SkinID="sknCloseImageButton" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_10">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                                    <tr>
                                        <td class="popup_td_padding_10" align="left">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="InterviewBatchQuestionEntry_messageLabel" SkinID="sknLabelFieldText"
                                                            runat="server" Text="The questions present in 'Questions' tab is not saved.<br \><br \>Select your option to continue."></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_20">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_padding_left_20">
                                                        <asp:RadioButton ID="InterviewBatchQuestionEntry_saveRadioButton" runat="server"
                                                            Text="Save and load next" GroupName="confirm" Checked="true" ForeColor="#148EC0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_padding_left_20">
                                                        <asp:RadioButton ID="InterviewBatchQuestionEntry_clearRadioButton" runat="server"
                                                            Text="Clear all and load next" GroupName="confirm" ForeColor="#148EC0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_8">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_5">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="InterviewBatchQuestionEntry_okButton" runat="server" SkinID="sknButtonId"
                                                Text="OK" Width="64px" OnClick="InterviewBatchQuestionEntry_saveQuestionOkButton_Click" />
                                        </td>
                                        <td style="padding-left: 10px">
                                            <asp:LinkButton ID="InterviewBatchQuestionEntry_cancelLinkButton" runat="server"
                                                SkinID="sknCancelLinkButton" Text="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div id="InterviewBatchQuestionEntry_questionModalPopupHiddenDiv" runat="server"
                    style="display: none">
                    <asp:Button ID="InterviewBatchQuestionEntry_modalPopUpHiddenButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="InterviewBatchQuestionEntry_questionModalPopupExtender"
                    runat="server" PopupControlID="InterviewBatchQuestionEntry_questionPanel" TargetControlID="InterviewBatchQuestionEntry_modalPopUpHiddenButton"
                    BackgroundCssClass="modalBackground" CancelControlID="InterviewBatchQuestionEntry_cancelLinkButton">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="InterviewBatchQuestionEntry_saveValidPopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_disclaimer">
                    <uc3:DisclaimerControl ID="InterviewBatchQuestionEntry_savePopupExtenderControl"
                        runat="server" Title="Disclaimer" />
                    <div style="padding-left: 11px">
                        <asp:Button ID="DisclaimerControl_acceptButton" runat="server" SkinID="sknButtonId"
                            Text="Accept" Width="64px" OnClick="DisclaimerControl_acceptButton_Click" />
                        &nbsp;
                        <asp:LinkButton ID="DisclaimerControl_cancelLinkButton" Text="Cancel" runat="server"
                            SkinID="sknPopupLinkButton"></asp:LinkButton>
                    </div>
                </asp:Panel>
                <div id="InterviewBatchQuestionEntry_hiddensavePopupModalDiv" runat="server" style="display: none">
                    <asp:Button ID="InterviewBatchQuestionEntry_hiddensavePopupModalButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="InterviewBatchQuestionEntry_disclaimerSavepopupExtender"
                    runat="server" PopupControlID="InterviewBatchQuestionEntry_saveValidPopupPanel"
                    TargetControlID="InterviewBatchQuestionEntry_hiddensavePopupModalButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="InterviewBatchQuestionEntry_saveInValidPopupPanel" runat="server"
                    Style="display: none" CssClass="popupcontrol_confirm_disclaimer">
                    <uc3:DisclaimerControl ID="InterviewBatchQuestionEntry_saveInvalidPopupExtenderControl"
                        runat="server" Title="Disclaimer" />
                    <div style="padding-left: 11px">
                        <asp:Button ID="DisclaimerControl_acceptInvalidButton" runat="server" SkinID="sknButtonId"
                            Text="Accept" Width="64px" OnClick="InterviewBatchQuestionEntry_acceptInvalidQuestionButton_Click" />
                        &nbsp;
                        <asp:LinkButton ID="DisclaimerControl_cancelInvalidLinkButton" Text="Cancel" runat="server"
                            SkinID="sknPopupLinkButton"></asp:LinkButton>
                    </div>
                </asp:Panel>
                <div id="InterviewBatchQuestionEntry_hiddensaveInvalidPopupModalDiv" runat="server"
                    style="display: none">
                    <asp:Button ID="InterviewBatchQuestionEntry_hiddensaveInvalidPopupModalButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="InterviewBatchQuestionEntry_disclaimerSaveInvalidpopupExtender"
                    runat="server" PopupControlID="InterviewBatchQuestionEntry_saveInValidPopupPanel"
                    TargetControlID="InterviewBatchQuestionEntry_hiddensaveInvalidPopupModalButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
