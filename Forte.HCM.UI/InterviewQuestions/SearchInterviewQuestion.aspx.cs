﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.

// SerachQuestion.cs
// File that represents the SearchInterviewQuestion class that defines the user interface
// layout and functionalities for the SearchInterviewQuestion page. This page helps in 
// searching for users by providing search criteria for Category,subject,Keyword,
// TestArea,Complexity,etc.. This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.InterviewQuestions
{
    /// <summary>                                                                         
    /// Class that represents the user interface layout and functionalities
    /// for the SearchInterviewQuestion page. This page helps in searching for users by
    /// providing search criteria for Category,subject,Keyword,TestArea,Complexity,
    /// etc.. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchInterviewQuestion : PageBase
    {
        #region Private Members

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Members

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Method will call for every postback for retaining the 
                // maximized hidden field value.
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    if (Support.Utility.IsNullOrEmpty(SearchInterviewQuestion_authorIdHiddenField.Value))
                        SearchInterviewQuestion_authorIdHiddenField.Value = base.userID.ToString();
                    //bind the all default values like TestArea,complexity
                    LoadValues();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                        Session["CATEGORY_SUBJECTS"] = null;
                    }

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION]
                            as QuestionDetailSearchCriteria);

                    // Reload the maximize hidden value.
                    CheckAndSetExpandorRestore();
                }

                // Set default button.
                Page.Form.DefaultButton = SearchInterviewQuestion_topSearchButton.UniqueID;

                // Clear the success & error messages in Top and Bottom
                SearchInterviewQuestion_topSuccessMessageLabel.Text = string.Empty;
                SearchInterviewQuestion_bottomSuccessMessageLabel.Text = string.Empty;
                SearchInterviewQuestion_topErrorMessageLabel.Text = string.Empty;
                SearchInterviewQuestion_bottomErrorMessageLabel.Text = string.Empty;

                // Paging Event 
                SearchInterviewQuestion_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (SearchInterviewQuestion_pagingNavigator_PageNumberClick);

                // Category subject Control Message Event
                SearchInterviewQuestion_searchCategorySubjectControl.ControlMessageThrown += new
                    CategorySubjectControl.ControlMessageThrownDelegate
                    (SearchInterviewQuestion_searchCategorySubjectControl_ControlMessageThrown);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        private void SearchInterviewQuestion_searchCategorySubjectControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                        SearchInterviewQuestion_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(SearchInterviewQuestion_topSuccessMessageLabel,
                        SearchInterviewQuestion_bottomSuccessMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchInterviewQuestion_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;

                //Bind the Search Result based on the Pageno.
                LoadQuestion(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void SearchInterviewQuestion_questionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    SearchInterviewQuestion_QuestionDetailPreviewControl.LoadInterviewQuestionDetails
                        (e.CommandArgument.ToString(), 0);
                    SearchInterviewQuestion_QuestionDetailPreviewControl.Title = "Interview Question Detail";
                    SearchInterviewQuestion_QuestionDetailPreviewControl.SetFocus();
                    SearchInterviewQuestion_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "active")
                {
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchInterviewQuestion_Activate_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Title = "Activate Question";
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchInterviewQuestiont_confirmPopupExtenderControl.DataBind();
                    SearchInterviewQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "inactive")
                {
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchInterviewQuestion_DeActivate_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Title = "Deactivate Question";
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchInterviewQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "deletes")
                {
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchInterviewQuestion_Delete_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Title = "Delete Question";
                    SearchInterviewQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchInterviewQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "editquestion")
                {
                    Response.Redirect("InterviewSingleQuestionEntry.aspx?m=0&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_INTERVIEW_QUESTION + "&questionKey=" + e.CommandArgument, false);
                }
                SearchInterviewQuestion_QuestionKey.Value = e.CommandArgument.ToString();
                SearchInterviewQuestion_Action.Value = e.CommandName.ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchInterviewQuestion_questionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                SearchInterviewQuestion_stateExpandHiddenField.Value = "0";
                SearchInterviewQuestion_testDraftExpandLinkButton.Text = "Expand All";
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                SearchInterviewQuestion_bottomPagingNavigator.Reset();
                LoadQuestion(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void SearchInterviewQuestion_resetLinkButton_Click(object sender, EventArgs e)
        {
            #region Not used now


            //try
            //{
            //    SearchInterviewQuestion_questionGridView.DataSource = null;
            //    SearchInterviewQuestion_questionGridView.DataBind();
            //    SearchInterviewQuestion_bottomPagingNavigator.TotalRecords = 0;
            //    ResetFormControlValues(this);
            //    if (!Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
            //        ViewState["SORT_ORDER"] = "A";

            //    if (!Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
            //        ViewState["SORT_FIELD"] = "QUESTIONKEY";

            //}
            //catch (Exception exp)
            //{
            //    Logger.ExceptionLog(exp);
            //    SearchInterviewQuestion_topErrorMessageLabel.Text = exp.Message;
            //    SearchInterviewQuestion_bottomErrorMessageLabel.Text = exp.Message;
            //}
            //finally
            //{
            //    SearchInterviewQuestion_questionDiv.Visible = false;
            //}
            #endregion

            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchInterviewQuestion_questionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton SearchInterviewQuestion_activeImageButton = (ImageButton)e.Row.FindControl(
                    "SearchInterviewQuestion_activeImageButton");
                ImageButton SearchInterviewQuestion_deleteImageButton = (ImageButton)e.Row.FindControl(
                    "SearchInterviewQuestion_deleteImageButton");
                ImageButton SearchInterviewQuestion_inactiveImageButton = (ImageButton)e.Row.FindControl(
                    "SearchInterviewQuestion_inactiveImageButton");
                ImageButton SearchInterviewQuestion_editImageButton = (ImageButton)e.Row.FindControl(
                    "SearchInterviewQuestion_editImageButton");
                HiddenField SearchInterviewQuestion_statusHiddenField = (HiddenField)e.Row.FindControl(
                    "SearchInterviewQuestion_statusHiddenField");
                HiddenField CorrectAnswerHiddenField = (HiddenField)e.Row.FindControl(
                    "CorrectAnswerHiddenField");
                HiddenField TestIncludedHiddenField = (HiddenField)e.Row.FindControl(
                    "TestIncludedHiddenField");
                HiddenField DataAccessRightsHiddenField = (HiddenField)e.Row.FindControl(
                    "DataAccessRightsHiddenField");


                if (Convert.ToInt32(TestIncludedHiddenField.Value) > 0)
                {
                    SearchInterviewQuestion_deleteImageButton.Visible = false;
                }

                if (SearchInterviewQuestion_statusHiddenField.Value == "Active")
                {
                    SearchInterviewQuestion_activeImageButton.Visible = true;
                }
                else
                {
                    SearchInterviewQuestion_editImageButton.Visible = false;
                    SearchInterviewQuestion_inactiveImageButton.Visible = true;
                }

                if ((DataAccessRightsHiddenField.Value == "V"))
                {
                    SearchInterviewQuestion_editImageButton.Visible = false;
                    SearchInterviewQuestion_inactiveImageButton.Visible = false;
                    SearchInterviewQuestion_deleteImageButton.Visible = false;
                    SearchInterviewQuestion_activeImageButton.Visible = false;
                }

                LinkButton SearchInterviewQuestion_questionIdLinkButton =
                    (LinkButton)e.Row.FindControl("SearchInterviewQuestion_questionIdLinkButton");
                HtmlContainerControl SearchInterviewQuestion_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("SearchInterviewQuestion_detailsDiv");
                HtmlAnchor SearchInterviewQuestion_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("SearchInterviewQuestion_focusDownLink");

                SearchInterviewQuestion_questionIdLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                SearchInterviewQuestion_detailsDiv.ClientID + "','" + SearchInterviewQuestion_focusDownLink.ClientID + "')");

                Label SearchInterviewQuestion_questionLabel =
                    (Label)e.Row.FindControl("SearchInterviewQuestion_questionLabel");

                PlaceHolder SearchInterviewQuestion_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl(
                    "SearchInterviewQuestion_answerChoicesPlaceHolder");
                SearchInterviewQuestion_answerChoicesPlaceHolder.Controls.Add
                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetInterviewQuestionOptions
                    (SearchInterviewQuestion_questionIdLinkButton.Text.ToString()),false));

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchInterviewQuestion_questionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (SearchInterviewQuestion_questionGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchInterviewQuestion_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SearchInterviewQuestion_simpleLinkButton.Text.ToLower() == "simple")
                {
                    SearchInterviewQuestion_simpleLinkButton.Text = "Advanced";
                    SearchInterviewQuestion_simpleSearchDiv.Visible = true;
                    SearchInterviewQuestion_advanceSearchDiv.Visible = false;
                }
                else
                {
                    SearchInterviewQuestion_simpleLinkButton.Text = "Simple";
                    SearchInterviewQuestion_simpleSearchDiv.Visible = false;
                    SearchInterviewQuestion_advanceSearchDiv.Visible = true;
                }
       
                LoadQuestion(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchInterviewQuestion_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchInterviewQuestion_bottomPagingNavigator.Reset();
                ViewState["PAGENUMBER"] = "1";
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "QUESTIONKEY";

                LoadQuestion(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void SearchInterviewQuestiont_okClick(object sender, EventArgs e)
        {
            try
            {
                QuestionBLManager questionBLManager = new QuestionBLManager();
                string SearchQuestion_Action = SearchInterviewQuestion_Action.Value.ToString();
                string SearchInterviewQuestionKey = SearchInterviewQuestion_QuestionKey.Value.Trim().ToString();
                if (SearchQuestion_Action == "active")
                {
                    questionBLManager.ActivateInterviewQuestion(SearchInterviewQuestionKey, base.userID);
                    base.ShowMessage(SearchInterviewQuestion_topSuccessMessageLabel,
                    SearchInterviewQuestion_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchInterviewQuestion_Activate, SearchInterviewQuestionKey));
                }
                else if (SearchQuestion_Action == "inactive")
                {
                    questionBLManager.DeactivateInterviewQuestion(SearchInterviewQuestionKey, base.userID);
                    base.ShowMessage(SearchInterviewQuestion_topSuccessMessageLabel,
                     SearchInterviewQuestion_bottomSuccessMessageLabel,
                     string.Format(Resources.HCMResource.SearchInterviewQuestion_InActivate, SearchInterviewQuestionKey));
                }
                else if (SearchQuestion_Action == "deletes")
                {
                    questionBLManager.DeleteInterviewQuestion(SearchInterviewQuestionKey, base.userID);
                    base.ShowMessage(SearchInterviewQuestion_topSuccessMessageLabel,
                    SearchInterviewQuestion_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchInterviewQuestion_Delete, SearchInterviewQuestionKey));
                }
                LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void SearchInterviewQuestion_cancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// It will clear all the session values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchInterviewQuestion_cancelLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect("~/InterviewHome.aspx", false);
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadQuestion(int pageNumber)
        {
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();
            List<Subject> categorySubjects = null;

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (SearchInterviewQuestion_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    SearchInterviewQuestion_categoryTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewQuestion_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    SearchInterviewQuestion_subjectTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewQuestion_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    SearchInterviewQuestion_keywordsTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewQuestion_keywordsTextBox.Text.Trim();
                questionDetailSearchCriteria.IsSimple = true;

                questionDetailSearchCriteria.CurrentPage = pageNumber;
                questionDetailSearchCriteria.IsMaximized =
                    SearchInterviewQuestion_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                questionDetailSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                questionDetailSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
                questionDetailSearchCriteria.Author = base.userID;

            }
            else
            {
                questionDetailSearchCriteria.IsAdvanced = true;
                string SelectedSubjectIDs = "";

                if (!Utility.IsNullOrEmpty(SearchInterviewQuestion_searchCategorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();

                    foreach (Subject subject in SearchInterviewQuestion_searchCategorySubjectControl.SubjectDataSource)
                    {
                        if (categorySubjects == null)
                            categorySubjects = new List<Subject>();

                        Subject subjectCat = new Subject();

                        if (subject.IsSelected)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                        // Get all the subjects and categories and then assign to the list.
                        subjectCat.CategoryID = subject.CategoryID;
                        subjectCat.CategoryName = subject.CategoryName;
                        subjectCat.SubjectID = subject.SubjectID;
                        subjectCat.SubjectName = subject.SubjectName;
                        subjectCat.IsSelected = subject.IsSelected;
                        categorySubjects.Add(subjectCat);
                    }

                    //Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in SearchInterviewQuestion_searchCategorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }

                // Keep the categories and subjects in Session
                Session["CATEGORY_SUBJECTS"] = categorySubjects;

                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    SearchInterviewQuestion_questionIDTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewQuestion_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    SearchInterviewQuestion_keywordTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewQuestion_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.ActiveFlag =
                                    SearchInterviewQuestion_statusDropDownList.SelectedValue.ToString() == "" ?
                                    null : SearchInterviewQuestion_statusDropDownList.SelectedValue.ToString();
                //questionDetailSearchCriteria.CreditsEarned =
                //                    SearchInterviewQuestion_creditTextBox.Text.Trim() == "" ?
                //                    0 : Convert.ToDecimal(SearchInterviewQuestion_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.AuthorName = SearchInterviewQuestion_authorTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewQuestion_authorIdHiddenField.Value;

                questionDetailSearchCriteria.Author = base.userID;
               

                questionDetailSearchCriteria.CurrentPage = pageNumber;
                questionDetailSearchCriteria.IsMaximized =
                    SearchInterviewQuestion_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                questionDetailSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                questionDetailSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            }

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION] = questionDetailSearchCriteria;

            questionDetailSearchCriteria.TenantID = base.tenantID;

            List<QuestionDetail> questionDetail =
                new BL.QuestionBLManager().GetInterviewQuestions(questionDetailSearchCriteria,
                    base.GridPageSize, pageNumber, questionDetailSearchCriteria.SortExpression,
                    questionDetailSearchCriteria.SortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                SearchInterviewQuestion_questionGridView.DataSource = null;
                SearchInterviewQuestion_questionGridView.DataBind();
                SearchInterviewQuestion_bottomPagingNavigator.TotalRecords = 0;
                SearchInterviewQuestion_questionDiv.Visible = false;
                SearchInterviewQuestion_testDraftExpandLinkButton.Visible = false;
            }
            else
            {
                SearchInterviewQuestion_questionGridView.DataSource = questionDetail;
                SearchInterviewQuestion_questionGridView.DataBind();

                SearchInterviewQuestion_bottomPagingNavigator.PageSize = base.GridPageSize;
                SearchInterviewQuestion_bottomPagingNavigator.TotalRecords = totalRecords;
                SearchInterviewQuestion_questionDiv.Visible = true;
                SearchInterviewQuestion_testDraftExpandLinkButton.Visible = true;
            }
        }

        /// <summary>
        /// Javascript Event Handler Declared here.
        /// </summary>
        private void JavaScriptHandler()
        {
            GridRestore();

            SearchInterviewQuestion_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + SearchInterviewQuestion_dummyAuthorID.ClientID + "','"
                + SearchInterviewQuestion_authorIdHiddenField.ClientID + "','"
                + SearchInterviewQuestion_authorTextBox.ClientID + "','QA')");
        }

        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            try
            {
                SearchInterviewQuestion_complexityCheckBoxList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY,
                    Constants.SortTypeConstants.ASCENDING);
                SearchInterviewQuestion_complexityCheckBoxList.DataTextField = "AttributeName";
                SearchInterviewQuestion_complexityCheckBoxList.DataValueField = "AttributeID";
                SearchInterviewQuestion_complexityCheckBoxList.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {
            try
            {
                SearchInterviewQuestion_testAreaCheckBoxList.DataSource =
                   new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA,
                   Constants.SortTypeConstants.ASCENDING);
                SearchInterviewQuestion_testAreaCheckBoxList.DataTextField = "AttributeName";
                SearchInterviewQuestion_testAreaCheckBoxList.DataValueField = "AttributeID";
                SearchInterviewQuestion_testAreaCheckBoxList.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewQuestion_topErrorMessageLabel,
                SearchInterviewQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Get Selected Test Area IDs and its used in the Search.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < SearchInterviewQuestion_testAreaCheckBoxList.Items.Count; i++)
            {
                if (SearchInterviewQuestion_testAreaCheckBoxList.Items[i].Selected)
                {
                    testAreaID.Append(SearchInterviewQuestion_testAreaCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return testAreaID;
        }

        /// <summary>
        /// Get Selected Complexity IDs and its used in the Search.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < SearchInterviewQuestion_complexityCheckBoxList.Items.Count; i++)
            {
                if (SearchInterviewQuestion_complexityCheckBoxList.Items[i].Selected)
                {
                    complexityID.Append(SearchInterviewQuestion_complexityCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return complexityID;
        }

        /// <summary>
        /// Expand & Restore the Search Result grid, while clicking the Grid right corner Image.
        /// </summary>
        private void GridRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchInterviewQuestion_restoreHiddenField.Value) &&
             SearchInterviewQuestion_restoreHiddenField.Value == "Y")
            {
                SearchInterviewQuestion_searchCriteriasDiv.Style["display"] = "none";
                SearchInterviewQuestion_searchResultsUpSpan.Style["display"] = "block";
                SearchInterviewQuestion_searchResultsDownSpan.Style["display"] = "none";
                SearchInterviewQuestion_questionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchInterviewQuestion_searchCriteriasDiv.Style["display"] = "block";
                SearchInterviewQuestion_searchResultsUpSpan.Style["display"] = "none";
                SearchInterviewQuestion_searchResultsDownSpan.Style["display"] = "block";
                SearchInterviewQuestion_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            SearchInterviewQuestion_searchResultsUpSpan.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                SearchInterviewQuestion_questionDiv.ClientID + "','" +
                SearchInterviewQuestion_searchCriteriasDiv.ClientID + "','" +
                SearchInterviewQuestion_searchResultsUpSpan.ClientID + "','" +
                SearchInterviewQuestion_searchResultsDownSpan.ClientID + "','" +
                SearchInterviewQuestion_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
            SearchInterviewQuestion_searchResultsDownSpan.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                SearchInterviewQuestion_questionDiv.ClientID + "','" +
                SearchInterviewQuestion_searchCriteriasDiv.ClientID + "','" +
                SearchInterviewQuestion_searchResultsUpSpan.ClientID + "','" +
                SearchInterviewQuestion_searchResultsDownSpan.ClientID + "','" +
                SearchInterviewQuestion_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="questionDetailSearchCriteria">
        /// A <see cref="QuestionDetailSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(QuestionDetailSearchCriteria questionDetailSearchCriteria)
        {
            string[] testAreaIDs = null;
            string[] complexities = null;

            // Check if the simple link button is clicked.
            if (questionDetailSearchCriteria.IsSimple)
            {
                SearchInterviewQuestion_simpleLinkButton.Text = "Advanced";
                SearchInterviewQuestion_simpleSearchDiv.Visible = true;
                SearchInterviewQuestion_advanceSearchDiv.Visible = false;

                if (questionDetailSearchCriteria.CategoryName != null)
                    SearchInterviewQuestion_categoryTextBox.Text = questionDetailSearchCriteria.CategoryName;

                if (questionDetailSearchCriteria.SubjectName != null)
                    SearchInterviewQuestion_subjectTextBox.Text = questionDetailSearchCriteria.SubjectName;

                if (questionDetailSearchCriteria.Tag != null)
                    SearchInterviewQuestion_keywordsTextBox.Text = questionDetailSearchCriteria.Tag;

                SearchInterviewQuestion_restoreHiddenField.Value =
                    questionDetailSearchCriteria.IsMaximized == true ? "Y" : "N";

                ViewState["SORT_FIELD"] = questionDetailSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = questionDetailSearchCriteria.SortDirection;

                // Apply search
                LoadQuestion(questionDetailSearchCriteria.CurrentPage);

                SearchInterviewQuestion_bottomPagingNavigator.MoveToPage
                    (questionDetailSearchCriteria.CurrentPage);
            }
            else
            {
                SearchInterviewQuestion_simpleLinkButton.Text = "Simple";
                SearchInterviewQuestion_simpleSearchDiv.Visible = false;
                SearchInterviewQuestion_advanceSearchDiv.Visible = true;

                if (!Utility.IsNullOrEmpty(questionDetailSearchCriteria.TestAreaID))
                {
                    testAreaIDs = questionDetailSearchCriteria.TestAreaID.Split(new char[] { ',' });

                    // Make selection test area.
                    for (int idx = 0; idx < testAreaIDs.Length; idx++)
                        SearchInterviewQuestion_testAreaCheckBoxList.Items.FindByValue(testAreaIDs[idx]).Selected = true;
                }

                if (!Utility.IsNullOrEmpty(questionDetailSearchCriteria.Complexities))
                {
                    complexities = questionDetailSearchCriteria.Complexities.Split(new char[] { ',' });

                    // Make selection of complexities.
                    for (int idx = 0; idx < complexities.Length; idx++)
                        SearchInterviewQuestion_complexityCheckBoxList.Items.FindByValue(complexities[idx]).Selected = true;
                }

                if (questionDetailSearchCriteria.QuestionKey != null)
                    SearchInterviewQuestion_questionIDTextBox.Text = questionDetailSearchCriteria.QuestionKey;

                if (questionDetailSearchCriteria.Tag != null)
                    SearchInterviewQuestion_keywordTextBox.Text = questionDetailSearchCriteria.Tag;

                if (questionDetailSearchCriteria.ActiveFlag != null)
                    SearchInterviewQuestion_statusDropDownList.SelectedValue = questionDetailSearchCriteria.ActiveFlag;

                //if (questionDetailSearchCriteria.CreditsEarned.ToString() != null)
                //    SearchInterviewQuestion_creditTextBox.Text = questionDetailSearchCriteria.CreditsEarned.ToString();

                if (questionDetailSearchCriteria.AuthorName != null)
                    SearchInterviewQuestion_authorTextBox.Text = questionDetailSearchCriteria.AuthorName;

                if (Session["CATEGORY_SUBJECTS"] != null)
                {
                    QuestionDetail questionDetail = new QuestionDetail();
                    questionDetail.Subjects = Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Fetch unique categories from the list.
                    var distinctCategories = (Session["CATEGORY_SUBJECTS"] as List<Subject>).GroupBy
                        (x => x.CategoryID).Select(x => x.First());

                    // This datasource is used to make the selection of the subjects
                    // which are already selected by the user to search.
                    SearchInterviewQuestion_searchCategorySubjectControl.SubjectsToBeSelected =
                        Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Bind distinct categories in the category datalist
                    SearchInterviewQuestion_searchCategorySubjectControl.CategoryDataSource =
                        distinctCategories.ToList<Subject>();

                    // Bind subjects for the categories which are bind previously.
                    SearchInterviewQuestion_searchCategorySubjectControl.SubjectDataSource =
                        distinctCategories.ToList<Subject>();
                }

                SearchInterviewQuestion_restoreHiddenField.Value =
                    questionDetailSearchCriteria.IsMaximized == true ? "Y" : "N";
                ViewState["SORT_FIELD"] = questionDetailSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = questionDetailSearchCriteria.SortDirection;

                // Apply search
                LoadQuestion(questionDetailSearchCriteria.CurrentPage);
                SearchInterviewQuestion_bottomPagingNavigator.MoveToPage
                    (questionDetailSearchCriteria.CurrentPage);
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(SearchInterviewQuestion_restoreHiddenField.Value) &&
                         SearchInterviewQuestion_restoreHiddenField.Value == "Y")
            {
                SearchInterviewQuestion_searchCriteriasDiv.Style["display"] = "none";
                SearchInterviewQuestion_searchResultsUpSpan.Style["display"] = "block";
                SearchInterviewQuestion_searchResultsDownSpan.Style["display"] = "none";
                SearchInterviewQuestion_questionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchInterviewQuestion_searchCriteriasDiv.Style["display"] = "block";
                SearchInterviewQuestion_searchResultsUpSpan.Style["display"] = "none";
                SearchInterviewQuestion_searchResultsDownSpan.Style["display"] = "block";
                SearchInterviewQuestion_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }

            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION]))
                if (!Utility.IsNullOrEmpty(SearchInterviewQuestion_restoreHiddenField.Value))
                    ((QuestionDetailSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION]).IsMaximized =
                        SearchInterviewQuestion_restoreHiddenField.Value == "Y" ? true : false;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
            JavaScriptHandler();

            // Set default focus field.
            Page.Form.DefaultFocus = SearchInterviewQuestion_categoryTextBox.UniqueID;
            SearchInterviewQuestion_categoryTextBox.Focus();
            SearchInterviewQuestion_simpleSearchDiv.Visible = true;
            SearchInterviewQuestion_advanceSearchDiv.Visible = false;

            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;
            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "QUESTIONKEY";
            if (Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";

            Master.SetPageCaption(Resources.HCMResource.SearchInterviewQuestion_Title);
        }

        #endregion Protected Overridden Methods
    }
}