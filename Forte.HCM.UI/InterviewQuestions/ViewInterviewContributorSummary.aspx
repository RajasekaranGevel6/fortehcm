<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewInterviewContributorSummary.aspx.cs"
    MasterPageFile="~/MasterPages/InterviewMaster.Master" Inherits="Forte.HCM.UI.InterviewQuestions.ViewInterviewContributorSummary" %>

<%@ Register TagPrefix="uc1" Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" %>
<%@ Register Src="../CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/SingleSeriesChartControl.ascx" TagName="SingleChartControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" ID="ViewInterviewContributorSummary_bodyContent"
    runat="server">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript">
        function TrimUserId(TextBoxId, HiddenId, BHiddenId) {
            var TextBoxValue = document.getElementById(TextBoxId).value;
            try {
                if (TextBoxValue == "")
                    return;
                document.getElementById(BHiddenId).value = "";
                document.getElementById(HiddenId).value = "";
                if (TextBoxValue.indexOf('|') < 0)
                    return;
                var UId = TextBoxValue.substring(TextBoxValue.indexOf('|') + 2);
                var ValidInteger = /^\d+$/.test(UId);
                if (!ValidInteger)
                    return;
                document.getElementById(HiddenId).value = UId;
                document.getElementById(TextBoxId).value = TextBoxValue.substring(0, TextBoxValue.indexOf('|') - 1);
                document.getElementById(BHiddenId).value = "SName";
                __doPostBack('', '');
            }
            catch (err) {
                alert(err.description);
            }
        }
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="ViewInterviewContributorSummary_headerLiteral" runat="server" Text="View Contributor Summary"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="ViewInterviewContributorSummary_topResetLinkButton" runat="server" Text="Reset"
                                            OnClick="ViewInterviewContributorSummary_resetLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="ViewInterviewContributorSummary_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ViewInterviewContributorSummary_topSuccessMessageLabel" Text="success" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ViewInterviewContributorSummary_topErrorMessageLabel" Text="error" runat="server"
                    SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="ViewInterviewContributorSummary_headerUdpatePanel" runat="server">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                        <tr>
                                            <td style="width: 12%">
                                                <asp:Label ID="ViewInterviewContributorSummary_questionAuthorLabel" runat="server" Text="Interview Question Author"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 14%">
                                                <div style="float: left; padding-right: 5px;">
                                                    <asp:TextBox ID="ViewInterviewContributorSummary_questionAuthorIDTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                    <%--                                                    <ajaxToolKit:AutoCompleteExtender ID="ViewContributionSummary_questionAuthorAutoCompleteExtender"
                                                        runat="server" TargetControlID="ViewInterviewContributorSummary_questionAuthorIDTextBox"
                                                        ServicePath="~/AutoComplete.asmx" ServiceMethod="GetUserNameList" MinimumPrefixLength="1"
                                                        CompletionListElementID="pnl" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionSetCount="12" EnableCaching="true">
                                                    </ajaxToolKit:AutoCompleteExtender>--%>
                                                    <asp:HiddenField ID="ViewInterviewContributorSummary_authorIDHiddenField" runat="server" />
                                                    <asp:HiddenField ID="ViewInterviewContributorSummary_browserHiddenField" runat="server" />
                                                    <asp:Panel ID="pnl" runat="server">
                                                    </asp:Panel>
                                                </div>
                                                <div id="ViewInterviewContributorSummary_searchImageDIV" style="display: block; float: left;"
                                                    runat="server">
                                                    <asp:ImageButton ID="ViewInterviewContributorSummary_searchImage" runat="server" SkinID="sknbtnSearchicon"
                                                        ToolTip="Click here to select the interview question author" Visible="false" />&nbsp;
                                                </div>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 48%">
                                                <div id="ViewInterviewContributorSummary_loadDetailsButton_div" runat="server" style="display: none">
                                                    <asp:Button ID="ViewInterviewContributorSummary_loadDetailsButton" runat="server" SkinID="sknButtonId"
                                                        Text="Load Details" OnClick="ViewInterviewContributorSummary_loadDetailsButton_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ViewInterviewContributorSummary_loadDetailsButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top">
                            <asp:UpdatePanel ID="ViewInterviewContributorSummary_authorSummaryUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="ViewInterviewContributorSummary_questionSummaryDIV" style="display: block; width: 100%"
                                        runat="server">
                                        <div style="float: left; width: 670px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="header_bg">
                                                        <asp:Literal ID="ViewInterviewContributorSummary_graphicalSummaryHeaderLiteral" runat="server"
                                                            Text="Graphical Summary"></asp:Literal>
                                                        <asp:Label ID="ViewInterviewContributorSummary_graphicalSummaryHeaderHelpLabel" runat="server"
                                                            SkinID="sknLabelText" Text=" - Click graph to zoom">                                                
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_body_bg" style="height: 208px;" valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 10%">
                                                                    <uc3:SingleChartControl ID="ViewInterviewContributorSummary_totalQuestionsAuthoredChart" runat="server" />
                                                                    <asp:Label ID="ViewInterviewContributorSummary_totalQuestionAuthored_noDataLabel" runat="server"
                                                                        SkinID="sknErrorMessage"></asp:Label>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <uc3:SingleChartControl ID="ViewInterviewContributorSummary_questionUsageSummaryChart" runat="server" />
                                                                    <asp:Label ID="ViewInterviewContributorSummary_questionUsageSummary_noDataLabel" runat="server"
                                                                        SkinID="sknErrorMessage"></asp:Label>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <uc3:SingleChartControl ID="ViewInterviewContributorSummary_creditsEarnedChart" runat="server" />
                                                                    <asp:Label ID="ViewInterviewContributorSummary_creditsEarned_noDataLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_8">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="float: right; width: 29%;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="header_bg">
                                                        <asp:Literal ID="ViewInterviewContributorSummary_questionSummaryLiteral" runat="server" Text="Interview Question Summary"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_body_bg" style="height: 208px;" valign="top">
                                                        <table width="100%" border="0" cellspacing="5" cellpadding="3">
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_contributorSinceLabel" runat="server" Text="Contributor Since"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_contributorSinceValueLabel" runat="server"
                                                                        ReadOnly="true" Text="" SkinID="sknLabelFieldText" Width="90%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_noOfQuestionsAuthoredLabel" runat="server"
                                                                        Text="Questions Authored" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 13%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_noOfQuestionsAuthoredValueLabel" runat="server"
                                                                        Text="" ReadOnly="true" SkinID="sknLabelFieldText" Width="60%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_creditsEarnedLabel" runat="server" Text="Credits Earned (in $)"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 13%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_creditsEarnedValueLabel" runat="server" Text=""
                                                                        SkinID="sknLabelFieldText" Width="60%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_noOfCategoriesContributedLabel" runat="server"
                                                                        Text="Categories Contributed" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <asp:HyperLink ID="ViewInterviewContributorSummary_noOfCategoriesContibutedHyperLink" runat="server"
                                                                        Text="" ToolTip="Click here to view list of categories contributed"></asp:HyperLink>
                                                                    <asp:Label ID="ViewInterviewContributorSummary_noOfCategoriesContributedValueLabel" runat="server"
                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_noOfSubjectsContributedLabel" runat="server"
                                                                        Text="Subjects Contributed" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 13%">
                                                                    <asp:HyperLink ID="ViewInterviewContributorSummary_noOfSubjectsContributedHyperLink" runat="server"
                                                                        Text="" ToolTip="Click here to view list of subjects contributed"></asp:HyperLink>
                                                                    <asp:Label ID="ViewInterviewContributorSummary_noOfSubjectsContributedValueLabel" runat="server"
                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_questionUsageLabel" runat="server" Text="Interview Question Usage Count"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 13%">
                                                                    <asp:Label ID="ViewInterviewContributorSummary_questionUsageValueLabel" runat="server" Text=""
                                                                        ReadOnly="true" SkinID="sknLabelFieldText" Width="60%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="ViewInterviewContributorSummary_questionDetailsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="ViewInterviewContributorSummary_questionDetailHeaderLiteral" runat="server"
                                            Text="Interview Question Details"></asp:Literal>
                                        <asp:Label ID="ViewInterviewContributorSummary_questionDetailHeaderHelpLabel" runat="server"
                                            SkinID="sknLabelText" Text=" - Click column headers to sort">                                                
                                        </asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="ViewInterviewContributorSummary_questionDetailsUparrowSpan" style="display: block;"
                                            runat="server">
                                            <asp:Image ID="ViewInterviewContributorSummary_questionDetailsUpArrow" runat="server" SkinID="sknMaximizeImage" />
                                        </span><span id="ViewInterviewContributorSummary_questionDetailsDownarrowSpan" style="display: none;"
                                            runat="server">
                                            <asp:Image ID="ViewInterviewContributorSummary_questionDetailsDownArrow" runat="server" SkinID="sknMinimizeImage" />
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel ID="ViewInterviewContributorSummary_updatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <div id="ViewInterviewContributorSummary_questionDetailsDIV" style="height: 200px; display: block;
                                                    overflow: auto;" runat="server">
                                                    <asp:GridView ID="ViewInterviewContributorSummary_questionDetailsGridView" runat="server"
                                                        AllowSorting="True" AutoGenerateColumns="False" OnRowDataBound="ViewInterviewContributorSummary_questionDetailsGridView_OnRowDataBound"
                                                        OnSorting="ViewInterviewContributorSummary_questionDetailsGridView_Sorting" OnRowCommand="ViewInterviewContributorSummary_questionDetailsGridView_RowCommand"
                                                        OnRowCreated="ViewInterviewContributorSummary_questionDetailsGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Question ID" SortExpression="QUESTIONKEY" ItemStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="ViewInterviewContributorSummary_questionLinkButton" runat="server" CommandName="view"
                                                                        CommandArgument='<%# Eval("QuestionKey") %>' Text='<%# Eval("QuestionKey") %>'
                                                                        ToolTip="Question Details"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle Font-Underline="False" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" SortExpression="CREATEDDATE DESC"
                                                                ItemStyle-Width="25%" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Interview Included" SortExpression="TESTATTEMPTED DESC" ItemStyle-CssClass="td_padding_right_20"
                                                                HeaderStyle-CssClass="td_padding_right_20" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="right">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="ViewInterviewContributorSummary_testIncludedLinkButton" runat="server"
                                                                        Text='<%# Eval("TestIncluded") %>' Visible="false" ToolTip="Click here to view the list of interview included details"></asp:LinkButton>
                                                                    <asp:Label ID="ViewInterviewContributorSummary_testIncludedLabel" runat="server" Text='<%# Eval("TestIncluded") %>'
                                                                        Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Administered" HeaderText="Administered" SortExpression="TESTADMINISTERED DESC"
                                                                ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                ItemStyle-Width="12%" ItemStyle-HorizontalAlign="right"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Credit Earned  (in $)" SortExpression="CREDITSEARNED DESC"
                                                                ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="right">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewInterviewContributorSummary_creditsearnedLabel" runat="server" Text='<%# GetCorrectAmount(Convert.ToDecimal(Eval("CreditsEarned"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="STATUS" ItemStyle-Width="15%">
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="ViewInterviewContributorSummary_questionDetailBottomPageNavigator"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="ViewInterviewContributorSummary_questionPanel" runat="server" Style="display: none"
                                                    CssClass="popupcontrol_question_detail">
                                                    <div style="display: none">
                                                        <asp:Button ID="ViewInterviewContributorSummary_hiddenButton" runat="server" Text="Hidden" /></div>
                                                    <uc2:QuestionDetailPreviewControl ID="ViewInterviewContributorSummary_questionDetailPreviewControl"
                                                        runat="server" />
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="ViewInterviewContributorSummary_questionModalPopupExtender"
                                                    runat="server" PopupControlID="ViewInterviewContributorSummary_questionPanel" TargetControlID="ViewInterviewContributorSummary_hiddenButton"
                                                    BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ViewInterviewContributorSummary_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                    Text="success"></asp:Label><asp:Label ID="ViewInterviewContributorSummary_bottomErrorMessageLabel"
                        runat="server" SkinID="sknErrorMessage" Text="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="right">
                            <table border="0" cellspacing="5" cellpadding="0">
                                <tr>
                                    <td align="right">
                                        <asp:LinkButton ID="ViewInterviewContributorSummary_bottomResetLinkButton" runat="server"
                                            Text="Reset" OnClick="ViewInterviewContributorSummary_resetLinkButton_Click" SkinID="sknActionLinkButton" />
                                    </td>
                                    <td align="center">
                                        |
                                    </td>
                                    <td align="left">
                                        <asp:LinkButton ID="ViewInterviewContributorSummary_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="ViewInterviewContributorSummary_isMaximizedHiddenField" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
