﻿
#region Directives

using System;
using System.Web.Services;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using System.Collections.Specialized;
using AjaxControlToolkit;
using System.Linq;

#endregion Directives

namespace Forte.HCM.UI
{
    /// <summary>
    /// Summary description for Autocomplete
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Autocomplete : WebService
    {
        #region Service Methods

        /// <summary>
        /// Service method that gets recommended categories from the DB.
        /// </summary>
        /// <param name="prefixText">The 'like' text to search in DB.</param>
        /// <param name="count"></param>
        /// <returns>
        /// A list of <see cref="string"/> that contains the categories.
        /// </returns>
        [WebMethod(Description = "Service method to get the categories list for auto complete recommendation",EnableSession=true)]
        public string[] GetCategoryList(string prefixText, int count)
        {
            try
            {
                int userID = 0;
                if (Session["USER_DETAIL"] != null)
                {
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                    userID = userDetail.UserID;
                }
                return new CommonBLManager().GetCategoryList(prefixText, userID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// Service method that gets recommended user names from the DB.
        /// </summary>
        /// <param name="prefixText">The 'like' text to search in DB.</param>
        /// <param name="count"></param>
        /// <returns>
        /// A list of <see cref="string"/> that contains the user firstname and lastname
        /// with user id.
        /// </returns>
        [WebMethod(Description = "Service method to get the username(s) for auto complete recommendation")]
        public string[] GetUserNameList(string prefixText, int count)
        {
            try
            {
                return new CommonBLManager().GetUserNameList(prefixText);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw;
            }
        }
        /// <summary>
        /// Service method that gets recommended position profile keyword  from the DB.
        /// </summary>
        /// <param name="prefixText">The 'like' text to search in DB.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        [WebMethod(Description = "Service method to get the Position Profile Keyword(s) for auto complete recommendation")]
        public string[] GetPositionProfileKeyword(string prefixText, int count)
        {
            try
            {
                return new PositionProfileBLManager().GetPositionProfileKeywordDictionary(prefixText, SkillType.Vertical, null);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw;
            }
        }

        /// <summary>
        /// Gets the skill category keywords.
        /// </summary>
        /// <param name="prefixText">The prefix text.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        [WebMethod(Description = "Service method to get the Position Profile skill category  Keyword(s) for auto complete recommendation")]
        public string[] GetSkillCategoryKeywords(string prefixText, int count)
        {
            try
            {
                return new PositionProfileBLManager().GetPositionProfileSkillCategroyKeyword(prefixText);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw;
            }
        }

        /// <summary>
        /// Gets the position skill name by category.
        /// </summary>
        /// <param name="prefixText">The prefix text.</param>
        /// <param name="count">The count.</param>
        /// <param name="contextKey">The context key.</param>
        /// <returns></returns>
        [WebMethod(Description = "Service method to get the Position Profile Keyword(s) for auto complete recommendation")]
        public string[] GeSkillNameKeywordByCategory(string prefixText, int count, string contextKey)
        {
            try
            {
                return new PositionProfileBLManager().GetPositionProfileKeywordDictionary(prefixText, SkillType.Technical, contextKey);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw;
            }
        }

        [WebMethod(Description = "Service method to get the sub module list based on the module id selected")]
        public CascadingDropDownNameValue[] GetSubModuleByModuleID(string knownCategoryValues,
        string category)
        {
            try
            {
                string[] moduleID = knownCategoryValues.Split(':', ';');

                int number = int.Parse(moduleID[1]);


                List<SubModule> moduleList = new AuthenticationBLManager().GetSubModuleBasedOnModule(number);

                List<CascadingDropDownNameValue> values =
                new List<CascadingDropDownNameValue>();

                foreach (SubModule item in moduleList)
                {
                    values.Add(new CascadingDropDownNameValue(item.SubModuleName, item.SubModuleID.ToString()));
                }
                return values.ToArray();

            }

            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw;
            }

        }

        /// <summary>
        /// Gets the category list.
        /// </summary>
        /// <param name="prefixText">The prefix text.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
          [WebMethod(EnableSession = true)]
        public string[] GetClientList(string prefixText,int count)
        {
            try
            {
                return new CommonBLManager().GetClientList(prefixText,count, Convert.ToInt16(Session["TENANT_ID"]));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

          /// <summary>
          /// Gets the skill list.
          /// </summary>
          /// <param name="prefixText">The prefix text.</param>
          /// <param name="count">The count.</param>
          /// <returns></returns>
          [WebMethod(EnableSession = true)]
          public string[] GetSkillList(string prefixText)
          {
              try
              {
                  return new CommonBLManager().GetSkillSearch(prefixText);
              }
              catch (Exception exp)
              {
                  Logger.ExceptionLog(exp);
                  throw exp;
              }
          }
        #endregion Service Methods
    }
}
