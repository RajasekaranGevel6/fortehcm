﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OverviewMaster.Master" 
CodeBehind="OTMOverview.aspx.cs" Inherits="Forte.HCM.UI.Overview.OTMOverview" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="OTMOverview_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="left" valign="top" style="width:70%" class="td_padding_10">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="overview_title" >
                            Online Testing Module
                        </td>
                    </tr>
                    <tr>
                        <td class="overview_content">
                            The online testing module is intended to play an important role in the ForteHCM process methodology by providing a platform for users to create, schedule and administer online assessments to candidates through use of intuitive and user-friendly techniques and tools
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" valign="top" style="width: 30%" class="overview">
            </td>
        </tr>
    </table>
</asp:Content>