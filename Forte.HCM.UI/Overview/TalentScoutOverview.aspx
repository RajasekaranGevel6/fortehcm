﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OverviewMaster.Master"
CodeBehind="TalentScoutOverview.aspx.cs" Inherits="Forte.HCM.UI.Overview.TalentScoutOverview" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="TalentScoutOverview_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="left" valign="top" style="width:70%" class="td_padding_10">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="overview_title" >
                            TalentScout
                        </td>
                    </tr>
                    <tr>
                        <td class="overview_content">
                            The TalentScout utility is intended to provide an integrated platform for talent seekers to perform searches across multiple sources and obtain standardized, ranked and informative results, in a simple, user friendly front-end interface, through use of an intuitive search engine powered by proprietary algorithms.
                        </td>
                    </tr>
                    <tr>
                        <td class="overview_content">
                            TalentScout is derived from ‘talent scouting’, a term popularly used in the sporting and music industry where evaluators travel to several places with the intent of identifying and evaluating talent. Akin to ‘talent scouting’, the TalentScout utility’s search engine reaches out to multiple user-specified sources in identifying talent, and then assesses normalized performance scores of such talent in ranking them on a unified platform.
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" valign="top" style="width: 30%" class="overview">
            </td>
        </tr>
    </table>
</asp:Content>
