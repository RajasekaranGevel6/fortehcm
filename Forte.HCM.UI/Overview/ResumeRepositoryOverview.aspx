﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OverviewMaster.Master"
    CodeBehind="ResumeRepositoryOverview.aspx.cs" Inherits="Forte.HCM.UI.Overview.ResumeRepositoryOverview" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="ResumeRepositoryOverview_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="left" valign="top" style="width: 70%" class="td_padding_10">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="overview_title">
                            Resume Repository
                        </td>
                    </tr>
                    <tr>
                        <td class="overview_content">
                            The Resume Repository module helps to deal with the shared database as well as local database to process candidate information. It helps in creating candidates, uploading resumes, etc.
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" valign="top" style="width: 30%" class="overview">
            </td>
        </tr>
    </table>
</asp:Content>
