﻿using System;
using System.Web.UI;

namespace Forte.HCM.UI.Overview
{
    public partial class PositionProfileOverview : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Overview");
        }
    }
}