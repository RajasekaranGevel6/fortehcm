﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OverviewMaster.Master"
    CodeBehind="PositionProfileOverview.aspx.cs" Inherits="Forte.HCM.UI.Overview.PositionProfileOverview" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="PositionProfileOverview_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="left" valign="top" style="width:70%" class="td_padding_10">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="overview_title" >
                            Position Profile
                        </td>
                    </tr>
                    <tr>
                        <td class="overview_content">
                            The Position Profile registration system is intended to be used by hiring managers/delivery managers/corporate recruiters to record information pertaining to a position that needs to be filled. This information includes essential characteristics of the position as well as constructs on which prospective candidates will need to be evaluated for the position. This module will therefore serve as the first step in the sourcing/screening process as soon as information about a new job requisition is received. Information recorded within the position profile registration will serve as inputs to the systems/processes such as TalentScout, Test Authoring, etc.
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" valign="top" style="width: 30%" class="overview">
            </td>
        </tr>
    </table>
</asp:Content>
