﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    CodeBehind="SiteAdminHome.aspx.cs" Inherits="Forte.HCM.UI.SiteAdminHome" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="SiteAdminHome_bodyContent" runat="server" ContentPlaceHolderID="SiteAdminMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="center" class="title_home_icon">
                <h2>
                    Super Admin Home
                </h2>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="App_Themes/DefaultTheme/Images/Site_Admin.png" alt="Super Admin" height="226px"
                    width="234px" />
            </td>
        </tr>
    </table>
</asp:Content>
