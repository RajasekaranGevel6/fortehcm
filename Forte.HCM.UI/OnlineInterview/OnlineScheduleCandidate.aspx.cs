﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineScheduleCandidate.cs
// File that represents the user interface for the create a new online interview 
// creation. This will interact with the OnlineInterviewBLManager to insert online interview 
// creation details to the database.

#endregion Header

#region Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ExternalService;
using System.Data;
using System.Reflection;
using Forte.HCM.SmptClientService;
using System.Configuration;

#endregion Directives

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for OnlineInterviewCreation page. This is used
    /// to create a online interview creation by providing interview name, 
    /// position profie, list of skill associated etc.
    /// Also this page provides some set of links in the online interview creation 
    /// to view the assessor, question set, schedule candidate screens
    /// This class is inherited from
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class OnlineScheduleCandidate : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(CandidateInterviewSessionDetail candidateInterviewDetail,
            EntityType entityType);

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </param>
        /// <param name="chatRoomKey">
        /// A <see cref="String"/> that holds the chat room key.
        /// </param>
        /// <param name="candidateScheduleID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegateAssessor(List<AssessorDetail> assessorDetail,
            string chatRoomKey, int candidateScheduleID, EntityType entityType);

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate details.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegateCandidate(CandidateDetail candidateDetail,
            EntityType entityType);

        /// <summary>
        /// A <see cref="string"/> that hold the interview key.
        /// </summary>
        private string interviewKey = string.Empty;

        /// <summary>
        /// A <see cref="string"/> that hold the interview key.
        /// </summary>
        private string displayMode = string.Empty;

        /// <summary>
        /// A <see cref="string"/> that hold the candidate interview id.
        /// </summary>
        private int candidateInterviewID = 0;

        #endregion Private Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that is called when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Clear controls
                ClearMessageControls();

                //Set the page title
                Master.SetPageCaption("Schedule Candidate");

                Page.Form.DefaultButton = OnlineScheduleCandidate_autoCalculateLinkButton.UniqueID;

                // Check if interview key is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["interviewkey"]))
                {
                    // Get interview key.
                    interviewKey = Request.QueryString["interviewkey"].ToString();
                }

                // Check if display mode value is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["displaymode"]))
                {
                    // Get display mode.
                    displayMode = Request.QueryString["displaymode"].ToString();
                }

                // Check if candidate interview id is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["candidateinterviewid"]))
                {
                    // Get candidate interview id
                    candidateInterviewID = 
                        Convert.ToInt32(Request.QueryString["candidateinterviewid"].ToString());
                }

                if (!IsPostBack)
                {
                    //Clear session values
                    Session["REQUESTED_CHOICE_LIST"] = null;
                    Session["SKILL_ASSOCIATED"] = null;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "AVAILABILITY_DATE";

                    OnlineScheduleCandidate_seacrchImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                           + OnlineScheduleCandidate_candidateNameTextBox.ClientID + "','"
                           + OnlineScheduleCandidate_emailTextBox.ClientID + "','"
                           + OnlineScheduleCandidate_candidateIdHiddenField.ClientID
                           + "')");

                    OnlineInterviewSessionDetail onlineInteviewDetail = null;
                    onlineInteviewDetail = new OnlineInterviewBLManager().
                        GetOnlineInterviewDetailByKey(interviewKey);

                    int associatedPositionID = 0;
                    if (onlineInteviewDetail != null)
                    {
                        associatedPositionID = onlineInteviewDetail.PositionProfileID;
                    }

                    // Assign handler for 'load position profile candidate' icon.
                    OnlineScheduleCandidate_seacrchPPImageButton.Attributes.Add("onclick", "return LoadPositionProfileCandidates('"
                       + OnlineScheduleCandidate_candidateNameTextBox.ClientID + "','"
                       + OnlineScheduleCandidate_emailTextBox.ClientID + "','"
                       + OnlineScheduleCandidate_candidateIdHiddenField.ClientID + "','"
                       + associatedPositionID + "')");


                    OnlineScheduleCandidate_newButton.Attributes.Add("onclick",
                   "return ShowCandidatePopup('" + OnlineScheduleCandidate_candidateNameTextBox.ClientID + "','" +
                        OnlineScheduleCandidate_emailTextBox.ClientID + "','" + OnlineScheduleCandidate_candidateIdHiddenField.ClientID + "')");

                    OnlineScheduleCandidate_searchSkillButton.Attributes.Add("onclick",
                       "return LoadSubjectLookUp('" + OnlineScheduleCandidate_categoryIDHiddenField.ClientID + "','" +
                       OnlineScheduleCandidate_skillTextBox.ClientID + "','" +
                       OnlineScheduleCandidate_skillIDHiddenField.ClientID + "','" + OnlineScheduleCandidate_skillHiddenField.ClientID + "','" +
                       OnlineScheduleCandidate_refreshSearchButton.ClientID + "')");

                    //Load from and to time
                    LoadFromToTime();

                    OnlineScheduleCandidate_addAssessorButton.Attributes.Add("onclick", "return ShowSearchAssessor('" + 
                        OnlineScheduleCandidate_addAssessorIDHiddenField.ClientID +
                       "','" + OnlineScheduleCandidate_addAssessorNameHiddenField.ClientID + "','" +
                       OnlineScheduleCandidate_addAssessorRefreshButton.ClientID + "','Y');");

                    //Load reminder settings
                    LoadScheduleReminder();

                    ShowScheduleOption();
                }


                //Expand or restore
                ExpandRestore();
                ExpandRestoreSearchResults();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

         /// <summary>
        /// Handles the Click event of the OnlineInterviewQuestionSet_searchQuestionImageButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void OnlineScheduleCandidate_timeSlotGridView_deleteChoiceImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            if (OnlineScheduleCandidate_timeSlotGridView.Rows.Count == 0) return;
 
              
            HiddenField OnlineScheduleCandidate_timeSlotGridView_interviewerId_HiddenField =
                  ((ImageButton)sender).FindControl("OnlineScheduleCandidate_timeSlotGridView_interviewerId_HiddenField")
                  as HiddenField;

            if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_timeSlotGridView_interviewerId_HiddenField)) return;
  
              List<AssessorTimeSlotDetail> selectedTimeSlots = Session["REQUESTED_CHOICE_LIST"] as List<AssessorTimeSlotDetail>;

              if (selectedTimeSlots==null || selectedTimeSlots.Count ==0) return;
                
              selectedTimeSlots.RemoveAll(x => x.AssessorID == Convert.ToInt32(OnlineScheduleCandidate_timeSlotGridView_interviewerId_HiddenField.Value));

              Session["REQUESTED_CHOICE_LIST"] = selectedTimeSlots;  
               
              OnlineScheduleCandidate_timeSlotGridView.DataSource = selectedTimeSlots;
              OnlineScheduleCandidate_timeSlotGridView.DataBind();
        }

        protected void OnlineScheduleCandidate_timeSlotGridView_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {     
                 GridViewRow gridRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                if(gridRow==null) return ;

                Label OnlineScheduleCandidate_timeSlotGridView_DateLabel = (Label)gridRow.FindControl("OnlineScheduleCandidate_timeSlotGridView_DateLabel");

                if (OnlineScheduleCandidate_timeSlotGridView_DateLabel == null) return;

                Label OnlineScheduleCandidate_timeSlotGridView_FromLabel = (Label)gridRow.FindControl("OnlineScheduleCandidate_timeSlotGridView_FromLabel");

                if (OnlineScheduleCandidate_timeSlotGridView_FromLabel == null) return;

                string fromTime = OnlineScheduleCandidate_timeSlotGridView_FromLabel.Text.Substring(0, 8);
  
                List<AssessorTimeSlotDetail> selectedTimeSlots = Session["REQUESTED_CHOICE_LIST"] as List<AssessorTimeSlotDetail>;

                if (selectedTimeSlots == null) return;

                selectedTimeSlots.RemoveAll(x => x.AvailabilityDate  == DateTime.ParseExact(OnlineScheduleCandidate_timeSlotGridView_DateLabel.Text,"MM/dd/yyyy",null) && x.TimeSlotTextFrom == fromTime);

                Session["REQUESTED_CHOICE_LIST"] = selectedTimeSlots;

                OnlineScheduleCandidate_timeSlotGridView.DataSource = selectedTimeSlots;
                OnlineScheduleCandidate_timeSlotGridView.DataBind();

                 //OnlineScheduleCandidate_timeSlotGridView.DeleteRow(gridRow.RowIndex); 

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                SearchTimeSlot(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method to refresh the grid
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// It will bind the selected skill to gridview
        /// </remarks>
        protected void OnlineScheduleCandidate_refreshSearchButton_Click
          (object sender, EventArgs e)
        {
            try
            {
                // Check if skill is entered.
                if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillHiddenField.Value.Trim()))
                {
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, "No skill entered to add");
                    return;
                }

                BindSubjects(OnlineScheduleCandidate_skillHiddenField.Value.ToString().Trim());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the OnlineScheduleCandidate_CancelLinkButton 
        /// control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void OnlineScheduleCandidate_cancelLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/OnlineInterviewSchduleCandidate.aspx?m=3&s=0&interviewkey=" +
                interviewKey + "&parentpage=PP_REVIEW", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the OnlineScheduleCandidate_CancelImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/>
        /// instance containing the event data.</param>
        protected void OnlineScheduleCandidate_cancelImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                //
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        
        /// <summary>
        /// Handler method that will be called when send request button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_sendRequestButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                int candidateID = 0;
                
                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_candidateIdHiddenField.Value))
                    candidateID = Convert.ToInt32(OnlineScheduleCandidate_candidateIdHiddenField.Value);

                List<AssessorTimeSlotDetail> timeSlots = null;

                if (Session["REQUESTED_CHOICE_LIST"] == null)
                {
                    timeSlots = new List<AssessorTimeSlotDetail>();
                }
                else
                    timeSlots = Session["REQUESTED_CHOICE_LIST"] as List<AssessorTimeSlotDetail>;

                if (timeSlots != null && timeSlots.Count > 0 && candidateID != 0)
                {
                    int candidateInterviewGenID = 0;

                    if (!IsValidInterviewSkill())
                    {
                        base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Skill weightage should be equal to 100");
                        return;
                    }

                    OnlineCandidateSessionDetail scheduleCandidate =
                        new OnlineCandidateSessionDetail();

                    scheduleCandidate.InterviewKey = interviewKey;
                    scheduleCandidate.CandidateID = candidateID;

                    scheduleCandidate.SessionStatus = "SESS_REQ";
                    scheduleCandidate.ChatRoomName =
                        new OnlineInterviewBLManager().GenerateChatRoomKey();

                    scheduleCandidate.CandidateInterviewID = candidateInterviewID;

                    if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_remainderRadioButtonList.SelectedValue))
                    {
                        scheduleCandidate.ReminderType = OnlineScheduleCandidate_remainderRadioButtonList.SelectedValue;
                    }

                    List<SkillDetail> skillList = new List<SkillDetail>();

                    skillList = ConstructInterviewSkillDetail();

                    // Get slot expiry time in minutes
                    int slotExpiryInMinutes = 0;

                    if (!Utility.IsNullOrEmpty(ConfigurationManager.AppSettings["SLOT_EXPIRY_TIME_IN_MINUTES"]))
                    {
                        slotExpiryInMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["SLOT_EXPIRY_TIME_IN_MINUTES"].ToString());
                    }

                    new OnlineInterviewBLManager().SaveCandidateRequestDetail(timeSlots, slotExpiryInMinutes, skillList, scheduleCandidate,
                       interviewKey, candidateID, base.userID, out candidateInterviewGenID);

                    CandidateInterviewSessionDetail candidateInterviewDetail = new CandidateInterviewSessionDetail();

                    candidateInterviewDetail = new OnlineInterviewBLManager().
                        GetCandidateOnlineInterviewDetail(candidateInterviewGenID);

                    //Send mail to the requested user
                    // Sent alert mail to the associated user asynchronously.
                    AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendSlotRequestToCandidate);
                    IAsyncResult result = taskDelegate.BeginInvoke(candidateInterviewDetail,
                        EntityType.CandidateSlotApproval, new AsyncCallback(SendSlotRequestToCandidateCallBack),
                        taskDelegate);

                    Session["REQUESTED_CHOICE_LIST"] = null;
                    Response.Redirect("~/OnlineInterview/OnlineInterviewSchduleCandidate.aspx?m=3&s=0&interviewkey=" +
                        interviewKey + "&msg=ES&parentpage=PP_REVIEW", false);
                }
                else
                {
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Mandatory fields cannot be empty");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when send request button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_scheduleButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (!IsValidImmediateSchedule())
                {
                    return;
                }

                int candidateID = 0;
                int scheduledCandidateInterviewID = 0;

                string chatRoomKey = string.Empty;

                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_candidateIdHiddenField.Value))
                    candidateID = Convert.ToInt32(OnlineScheduleCandidate_candidateIdHiddenField.Value);

                OnlineCandidateSessionDetail scheduleCandidate = new OnlineCandidateSessionDetail();

                scheduleCandidate.InterviewKey = interviewKey;
                scheduleCandidate.CandidateID = candidateID;

                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_scheduleDateTextBox.Text))
                {
                    scheduleCandidate.InterviewDate =
                        Convert.ToDateTime(OnlineScheduleCandidate_scheduleDateTextBox.Text);
                }
                scheduleCandidate.SessionStatus = "SESS_SCHD";

                if (OnlineScheduleCandidate_scheduleFromDropDownList.SelectedIndex != 0)
                {
                    scheduleCandidate.TimeSlotFrom =
                        OnlineScheduleCandidate_scheduleFromDropDownList.SelectedItem.Text.Trim();
                    scheduleCandidate.TimeSlotIDFrom = Convert.ToInt32(OnlineScheduleCandidate_scheduleFromDropDownList.SelectedValue);
                }

                if (OnlineScheduleCandidate_scheduleToDropDownList.SelectedIndex != 0)
                {
                    scheduleCandidate.TimeSlotTo =
                        OnlineScheduleCandidate_scheduleToDropDownList.SelectedItem.Text.Trim();
                    scheduleCandidate.TimeSlotIDTo = Convert.ToInt32(OnlineScheduleCandidate_scheduleToDropDownList.SelectedValue);
                }

                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_remainderRadioButtonList.SelectedValue))
                {
                    scheduleCandidate.ReminderType = OnlineScheduleCandidate_remainderRadioButtonList.SelectedValue;
                }

                // Get assessor detail
                List<AssessorDetail> assessorDetails = new List<AssessorDetail>();
                assessorDetails = ConstructSelectedAssessor();

                List<SkillDetail> skillList = new List<SkillDetail>();

                skillList = ConstructInterviewSkillDetail();

                //Reminder
                InterviewReminderDetail reminderDetail = new InterviewReminderDetail();
                reminderDetail.IntervalID = OnlineScheduleCandidate_remainderRadioButtonList.SelectedValue;
                reminderDetail.ReminderDate = Convert.ToDateTime(OnlineScheduleCandidate_scheduleDateTextBox.Text);
                reminderDetail.UserID = base.userID;
                reminderDetail.InterviewStartTime = OnlineScheduleCandidate_scheduleFromDropDownList.SelectedItem.Text.Trim();

                string msg = string.Empty;

                if (candidateInterviewID != 0)
                {
                    scheduledCandidateInterviewID = candidateInterviewID;
                    scheduleCandidate.CandidateInterviewID = candidateInterviewID;

                    // Resschedule Candidate
                    new OnlineInterviewBLManager().ReScheduleOnlineInterviewCandidate(scheduleCandidate, reminderDetail,
                        skillList, assessorDetails, base.userID);

                    if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_chatRoomKeyHiddenField.Value))
                    {
                        chatRoomKey = OnlineScheduleCandidate_chatRoomKeyHiddenField.Value;
                    }
                    msg = "RS";
                }
                else
                {
                    if (!new OnlineInterviewBLManager().GetScheduleAvailableTimes(scheduleCandidate))
                    {
                        base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Candidate is not available. Already scheduled with interview date/time");

                        return;
                    }

                    foreach (AssessorDetail assessorDetail in assessorDetails)
                    {

                        if (!new OnlineInterviewBLManager().IsAssessorAvailableForScheduledTime(assessorDetail.AssessorId, 
                            scheduleCandidate.InterviewDate, scheduleCandidate.TimeSlotIDFrom, scheduleCandidate.TimeSlotIDTo))
                        {
                            base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                            OnlineScheduleCandidate_bottomErrorMessageLabel, 
                            string.Format("Assessor {0} is already scheduled with interview date/time",assessorDetail.FirstName));
                            return;
                        }
                    }

                    chatRoomKey = new OnlineInterviewBLManager().GenerateChatRoomKey();
                    scheduleCandidate.ChatRoomName = chatRoomKey;

                    // Schedule Candidate
                    new OnlineInterviewBLManager().ScheduleOnlineInterviewCandidate(scheduleCandidate, reminderDetail, skillList,
                        assessorDetails, base.userID, out scheduledCandidateInterviewID);
                    msg = "SC";
                }

                List<AssessorDetail> assessors = new List<AssessorDetail>();

                assessors = new OnlineInterviewBLManager().
                    GetOnlineInterviewScheduledAssessorByCandidateID(scheduledCandidateInterviewID);

                // Send mail to the requested assessor
                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegateAssessor taskDelegate = new AsyncTaskDelegateAssessor(SendScheduledInfoToAssessor);
                IAsyncResult result = taskDelegate.BeginInvoke(assessors, chatRoomKey, scheduledCandidateInterviewID,
                    EntityType.OnlineInterviewScheduledToAssessor, new AsyncCallback(SendScheduledInfoToAssessorCallBack), taskDelegate);

                CandidateDetail candidateDetail = new CandidateDetail();
                candidateDetail.FirstName = OnlineScheduleCandidate_candidateNameTextBox.Text;
                candidateDetail.EMailID = OnlineScheduleCandidate_emailTextBox.Text;
                candidateDetail.ChatRoomKey = chatRoomKey;
                candidateDetail.CandidateInterviewID = scheduledCandidateInterviewID;

                // Send mail to the scheduled candidate
                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegateCandidate taskCandidateDelegate = new AsyncTaskDelegateCandidate(SendScheduledInfoToCandidate);
                IAsyncResult resultCandidate = taskCandidateDelegate.BeginInvoke(candidateDetail,
                    EntityType.OnlineInterviewScheduledToCandidate, new AsyncCallback(SendScheduledInfoToCandidateCallBack), taskCandidateDelegate);

                Response.Redirect("~/OnlineInterview/OnlineInterviewSchduleCandidate.aspx?m=3&s=0&interviewkey=" +
                    interviewKey + "&msg=" + msg + "&parentpage=PP_REVIEW", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called at the time of textchanged event is fired
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineScheduleCandidate_skillTextBox_TextChanged(object sender,
            EventArgs e)
        {
            try
            {
                // Check if skill is entered.
                if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillTextBox.Text) &&
                    Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillHiddenField.Value.Trim()))
                {
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, "No skill added");

                    return;
                }

                string skill = string.Empty;

                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillTextBox.Text))
                    skill = OnlineScheduleCandidate_skillTextBox.Text.Trim();
                else if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillHiddenField.Value.Trim()))
                    skill = OnlineScheduleCandidate_skillHiddenField.Value.ToString().Trim();

                BindSubjects(skill);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when search button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_searchButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (!IsValidSearchCriteria())
                {
                    return;
                }

                SearchTimeSlot(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when request time add image button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/>that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_requestTimeAddImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            try
            {

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when the from time slot dropdown
        /// list item is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will select the next possible (1 r) advance time slot in 
        /// the 'to' time slot.
        /// </remarks>
        protected void OnlineScheduleCandidate_fromDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Check if a valid time slot is selected or not.
                if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_fromDropDownList.SelectedValue))
                    return;

                int selectedID = Convert.ToInt32(OnlineScheduleCandidate_fromDropDownList.SelectedValue);
                // Increment the ID to point to the next time slot.
                selectedID++;

                // Set the selected ID to the 'to' time slot.
                if (OnlineScheduleCandidate_toDropDownList.Items.FindByValue(selectedID.ToString()) != null)
                    OnlineScheduleCandidate_toDropDownList.SelectedValue = selectedID.ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the from time slot dropdown
        /// list item is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will select the next possible (1 r) advance time slot in 
        /// the 'to' time slot.
        /// </remarks>
        protected void OnlineScheduleCandidate_scheduleFromDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Check if a valid time slot is selected or not.
                if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_scheduleFromDropDownList.SelectedValue))
                    return;

                int selectedID = Convert.ToInt32(OnlineScheduleCandidate_scheduleFromDropDownList.SelectedValue);
                // Increment the ID to point to the next time slot.
                selectedID++;

                // Set the selected ID to the 'to' time slot.
                if (OnlineScheduleCandidate_scheduleToDropDownList.Items.FindByValue(selectedID.ToString()) != null)
                    OnlineScheduleCandidate_scheduleToDropDownList.SelectedValue = selectedID.ToString();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when 
        /// add request time slot image button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event args.
        /// </param>
        protected void OnlineScheduleCandidate_addRequestTimeImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            try
            {
                if (!IsCheckSelectedDate())
                {
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select time slot from search results");

                    return;
                }

                if (!IsSimilarDateSelected())
                {
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "While selecting multiple assessors, please select the same timeslot on the same day to create a choice");
                    return;
                }

                if (OnlineScheduleCandidate_fromDropDownList.SelectedValue != "--Select--" &&
                    OnlineScheduleCandidate_toDropDownList.SelectedValue != "--Select--")
                {
                    AssessorTimeSlotDetail selectedSlots = GetTimeSlotIDFromAndTo();

                    if (selectedSlots != null && selectedSlots.TimeSlotIDFrom > 0
                        && selectedSlots.TimeSlotIDTo > 0)
                    {
                        OnlineScheduleCandidate_addRequestFromDropDownList.SelectedIndex = selectedSlots.TimeSlotIDFrom;
                        OnlineScheduleCandidate_addRequestToDropDownList.SelectedIndex = selectedSlots.TimeSlotIDTo;
                    }

                    OnlineSchduleCandidate_assignTimeModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void OnlineScheduleCandidate_searchTimeSlotGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                OnlineScheduleCandidate_pageNavigator.Reset();
                SearchTimeSlot(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel request time button is clicked
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void OnlineSchduleCandidate_cancelRequestButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel request time button is clicked
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void OnlineSchduleCandidate_addRequestTimeButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (OnlineScheduleCandidate_addRequestFromDropDownList.SelectedValue != "--Select--" &&
                   OnlineScheduleCandidate_addRequestToDropDownList.SelectedValue != "--Select--")
                {
                    //Construct date assign choice dates
                    List<AssessorTimeSlotDetail> selectedChoiceList = new List<AssessorTimeSlotDetail>();

                    selectedChoiceList = ConstructChoiceList(false);

                    OnlineScheduleCandidate_timeSlotGridView.DataSource = selectedChoiceList;
                    OnlineScheduleCandidate_timeSlotGridView.DataBind();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel request time button is clicked
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void ScheduleCandidate_cancelReasonCancelImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            try
            {
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the skills grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void OnlineScheduleCandidate_skillAssociatedGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteSkill")
                    return;

                int skillID = 0;

                if (!Utility.IsNullOrEmpty(e.CommandArgument))
                {
                    skillID = Convert.ToInt32(e.CommandArgument);
                }

                
                // Retrieve the skill details list from session.
                List<SkillDetail> skillDetails = null;
                if (Session["SKILL_ASSOCIATED"] != null)
                {
                    skillDetails = Session["SKILL_ASSOCIATED"] as List<SkillDetail>;
                }

                if (skillDetails == null || skillID == 0)
                {
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "No skill found to delete");

                    return;
                }

                if (!Utility.IsNullOrEmpty(interviewKey) && candidateInterviewID!=0)
                {
                    // Show confirmation popup
                    OnlineScheduleCandidate_deleteSkillIDHiddenField.Value = skillID.ToString();

                    ImageButton sourceButton = (ImageButton)e.CommandSource;
                    GridViewRow row = (GridViewRow)sourceButton.Parent.Parent;

                    Label skillName = (Label)row.FindControl("OnlineScheduleCandidate_skillAssociatedGridView_skillNameLabel");

                    OnlineScheduleCandidate_skillDeleteConfirmMsgControl.Message =
                        string.Format("Are you sure to delete the skill {0}?", skillName.Text.Trim());

                    OnlineScheduleCandidate_skillDeletePopupPanel.Style.Add("height", "200px");
                    OnlineScheduleCandidate_skillDeleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                    OnlineScheduleCandidate_skillDeleteConfirmMsgControl.Title = "Warning";

                    OnlineScheduleCandidate_skillDeleteModalPopupExtender.Show();
                    return;
                }

                DeleteAssociatedSkillFromList(skillID);
                OnlineScheduleCandidate_skillChangesCheckBox.Enabled = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the skills grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_createTimeSlotLinkButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                OnlineScheduleCandidate_requestTimeSlotSectionDiv.Style["display"] = "none";
                OnlineScheduleCandidate_createTimeSlotSectionDiv.Style["display"] = "block";

                OnlineScheduleCandidate_sendRequestButton.Visible = false;
                OnlineScheduleCandidate_scheduleButton.Visible = true;

                OnlineScheduleCandidate_searchSlotsDiv.Style["display"] = "none";
                OnlineScheduleCandidate_searchResultsSlotsDiv.Style["display"] = "none";

                OnlineScheduleCandidate_createTimeSlotLinkButton.CssClass = "request_tab_no_link_button";
                OnlineScheduleCandidate_requestTimeSlotLinkButton.CssClass = "request_tab_link_button";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the skills grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_requestTimeSlotLinkButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                OnlineScheduleCandidate_requestTimeSlotSectionDiv.Style["display"] = "block";
                OnlineScheduleCandidate_createTimeSlotSectionDiv.Style["display"] = "none";

                OnlineScheduleCandidate_sendRequestButton.Visible = true;
                OnlineScheduleCandidate_scheduleButton.Visible = false;

                OnlineScheduleCandidate_searchSlotsDiv.Style["display"] = "block";
                OnlineScheduleCandidate_searchResultsSlotsDiv.Style["display"] = "block";

                OnlineScheduleCandidate_createTimeSlotLinkButton.CssClass = "request_tab_link_button";
                OnlineScheduleCandidate_requestTimeSlotLinkButton.CssClass = "request_tab_no_link_button";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when add new assessor button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_addAssessorRefreshButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_addAssessorIDHiddenField.Value))
                {
                    OnlineScheduleCandidate_interviewAssessorCheckBoxList.Items.Add(
                        new ListItem(OnlineScheduleCandidate_addAssessorNameHiddenField.Value,
                            OnlineScheduleCandidate_addAssessorIDHiddenField.Value));

                    OnlineScheduleCandidate_interviewAssessorCheckBoxList.DataBind();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when auto calculate link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineScheduleCandidate_autoCalculateLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                AutoPopulateSkillWeightage();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void OnlineScheduleCandidate_skillAssociatedGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void OnlineScheduleCandidate_skillDeleteConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                int skillID = 0;

                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_deleteSkillIDHiddenField.Value))
                {
                    skillID = Convert.ToInt32(OnlineScheduleCandidate_deleteSkillIDHiddenField.Value);
                }

                // Delete from  ONLINE_INTERVIEW_SCHEDULED_SKILL table
                new OnlineInterviewBLManager().DeleteOnlineInterviewScheduledSkill(candidateInterviewID, skillID);

                // Delete skill form list
                DeleteAssociatedSkillFromList(skillID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void OnlineScheduleCandidate_skillDeleteConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                OnlineScheduleCandidate_deleteSkillIDHiddenField.Value = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                    OnlineScheduleCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method to get the selected from and to slots
        /// </summary>
        /// <returns></returns>
        private AssessorTimeSlotDetail GetTimeSlotIDFromAndTo()
        {
            int selectedTimeSlotIDFrom = 0;
            int selectedTimeSlotIDTo = 0;

            int gridLoopCnt = 0;

            foreach (GridViewRow row in OnlineScheduleCandidate_searchTimeSlotGridView.Rows)
            {
                CheckBox OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox = ((CheckBox)row.FindControl
                       ("OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox"));

                if (OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox.Checked)
                {
                    //Assign the search from and to time slots
                    HiddenField OnlineScheduleCandidate_searchTimeSlotGridView_timeFromIDHiddenField =
                       ((HiddenField)row.FindControl("OnlineScheduleCandidate_searchTimeSlotGridView_timeFromIDHiddenField"));

                    HiddenField OnlineScheduleCandidate_searchTimeSlotGridView_timeToIDHiddenField =
                       ((HiddenField)row.FindControl("OnlineScheduleCandidate_searchTimeSlotGridView_timeToIDHiddenField"));

                    if (gridLoopCnt == 0 &&
                        !Utility.IsNullOrEmpty(OnlineScheduleCandidate_searchTimeSlotGridView_timeFromIDHiddenField.Value))
                    {
                        selectedTimeSlotIDFrom =
                            Convert.ToInt32(OnlineScheduleCandidate_searchTimeSlotGridView_timeFromIDHiddenField.Value);
                    }

                    if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_searchTimeSlotGridView_timeToIDHiddenField.Value))
                    {
                        selectedTimeSlotIDTo =
                            Convert.ToInt32(OnlineScheduleCandidate_searchTimeSlotGridView_timeToIDHiddenField.Value);
                    }

                    gridLoopCnt++;
                }
            }

            AssessorTimeSlotDetail selectedSlots = new AssessorTimeSlotDetail();
            selectedSlots.TimeSlotIDFrom=selectedTimeSlotIDFrom;
            selectedSlots.TimeSlotIDTo = selectedTimeSlotIDTo;

            return selectedSlots;
        }

        /// <summary>
        /// Method to delete the skill from skill list
        /// </summary>
        /// <param name="skillID"></param>
        private void DeleteAssociatedSkillFromList(int skillID)
        {
            List<SkillDetail> skillDetail = null;

            if (Session["SKILL_ASSOCIATED"] == null)
                skillDetail = new List<SkillDetail>();
            else
                skillDetail = Session["SKILL_ASSOCIATED"] as List<SkillDetail>;

            // Delete the skill from skill list
            skillDetail.RemoveAll(s => s.SkillID == skillID);

            // Keep the skills in session.
            if (skillDetail.Count == 0)
                Session["SKILL_ASSOCIATED"] = null;
            else
                Session["SKILL_ASSOCIATED"] = skillDetail;

            // Assign the skills to the grid view.
            OnlineScheduleCandidate_skillAssociatedGridView.DataSource = skillDetail;
            OnlineScheduleCandidate_skillAssociatedGridView.DataBind();
            
            //Auto Populate Skill Weightage
            AutoPopulateSkillWeightage();

            // Show success message.
            base.ShowMessage(OnlineScheduleCandidate_topSuccessMessageLabel, 
                OnlineScheduleCandidate_bottomSuccessMessageLabel, "Skill deleted successfully");
        }

        /// <summary>
        /// Method to show the schedule option
        /// </summary>
        private void ShowScheduleOption()
        {
            if (displayMode == "ES")
            {
                //Clear the session variables
                Session["REQUESTED_CHOICE_LIST"] = null;

                OnlineScheduleCandidate_sectionTitleLiteral.Text = "Schedule Candidate - Edit Slots";
                OnlineScheduleCandidate_addRequestTimeImageButton.Visible = true;
                OnlineScheduleCandidate_createTimeSlotLinkButton.Visible = false;
                OnlineScheduleCandidate_sendRequestButton.Visible = true;
                OnlineScheduleCandidate_scheduleButton.Visible = false;

                OnlineScheduleCandidate_requestTimeSlotSectionDiv.Style["display"] = "block";
                OnlineScheduleCandidate_createTimeSlotSectionDiv.Style["display"] = "none";

                OnlineCandidateSessionDetail onlineCandidateScheduleDetail = new OnlineCandidateSessionDetail();
                onlineCandidateScheduleDetail = new OnlineInterviewBLManager().GetCandidateScheduledDetail(candidateInterviewID);

                if (onlineCandidateScheduleDetail != null)
                {
                    OnlineScheduleCandidate_seacrchImageButton.Visible = false;
                    OnlineScheduleCandidate_newButton.Visible = false;
                    OnlineScheduleCandidate_seacrchPPImageButton.Visible = false;

                    OnlineScheduleCandidate_candidateNameTextBox.Text = onlineCandidateScheduleDetail.FirstName;
                    OnlineScheduleCandidate_emailTextBox.Text = onlineCandidateScheduleDetail.EmailId;
                    OnlineScheduleCandidate_candidateIdHiddenField.Value = onlineCandidateScheduleDetail.CandidateID.ToString();
                    OnlineScheduleCandidate_chatRoomKeyHiddenField.Value = onlineCandidateScheduleDetail.ChatRoomName;
                    OnlineScheduleCandidate_slotKeyHiddenField.Value = onlineCandidateScheduleDetail.SlotKey;

                    if (!Utility.IsNullOrEmpty(onlineCandidateScheduleDetail.ReminderType))
                    {
                        OnlineScheduleCandidate_remainderRadioButtonList.SelectedValue =
                            onlineCandidateScheduleDetail.ReminderType;
                    }
                }

                List<AssessorTimeSlotDetail> reAssignTimeSlots =
                    new List<AssessorTimeSlotDetail>();

                AssessorTimeSlotDetail assessorSlots = new AssessorTimeSlotDetail();

                assessorSlots = new OnlineInterviewBLManager().
                    GetCandidateSlotDetailByCandidateID(candidateInterviewID);

                if (assessorSlots != null && assessorSlots.assessorTimeSlotDetails != null)
                {
                    foreach (AssessorTimeSlotDetail assessorTimeSlots in
                        assessorSlots.assessorTimeSlotDetails)
                    {
                        // Instantiate time slot list.
                        if (reAssignTimeSlots == null)
                            reAssignTimeSlots = new List<AssessorTimeSlotDetail>();

                        // Create a time slot object.
                        AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                        timeSlot.TimeSlotTextFrom = assessorTimeSlots.TimeSlotTextFrom;
                        timeSlot.TimeSlotTextTo = assessorTimeSlots.TimeSlotTextTo;

                        timeSlot.TimeSlotIDFrom =
                            Convert.ToInt32(assessorTimeSlots.TimeSlotIDFrom);

                        timeSlot.TimeSlotIDTo =
                            Convert.ToInt32(assessorTimeSlots.TimeSlotIDTo);

                        if (!Utility.IsNullOrEmpty(assessorTimeSlots.AvailabilityDate))
                        {
                            timeSlot.AvailabilityDate =
                                Convert.ToDateTime(assessorTimeSlots.AvailabilityDate);
                        }

                        int requestedSlotID = 0;
                        requestedSlotID = assessorTimeSlots.RequestDateGenID;

                        if (assessorSlots != null &&
                            assessorSlots.assessorDetail != null)
                        {
                            var slotAssessor = Enumerable.Where(assessorSlots.assessorDetail,
                                t => t.RequestedSlotID == requestedSlotID);

                            if (assessorSlots != null)
                            {
                                string assessorNames = string.Join(", ", slotAssessor.ToList().Select(x => x.AssessorName).ToArray());
                                timeSlot.Assessor = assessorNames;

                                timeSlot.assessorDetail = slotAssessor.ToList();
                            }
                        }
                        reAssignTimeSlots.Add(timeSlot);
                    }

                    Session["REQUESTED_CHOICE_LIST"] = reAssignTimeSlots;
                }

                if (Session["REQUESTED_CHOICE_LIST"] != null)
                {
                    OnlineScheduleCandidate_timeSlotGridView.DataSource = reAssignTimeSlots;
                    OnlineScheduleCandidate_timeSlotGridView.DataBind();
                }

                //Load the interview skill
                LoadSkillSet(candidateInterviewID);
            }
            else if (displayMode == "RS")
            {
                OnlineScheduleCandidate_sectionTitleLiteral.Text = "Reschedule Candidate";
                OnlineScheduleCandidate_addRequestTimeImageButton.Visible = false;
                
                OnlineCandidateSessionDetail onlineCandidateScheduleDetail = new OnlineCandidateSessionDetail();
                onlineCandidateScheduleDetail = new OnlineInterviewBLManager().GetCandidateScheduledDetail(candidateInterviewID);

                if (onlineCandidateScheduleDetail != null)
                {
                    OnlineScheduleCandidate_seacrchImageButton.Visible = false;
                    OnlineScheduleCandidate_newButton.Visible = false;
                    OnlineScheduleCandidate_seacrchPPImageButton.Visible = false;

                    OnlineScheduleCandidate_candidateNameTextBox.Text = onlineCandidateScheduleDetail.FirstName;
                    OnlineScheduleCandidate_emailTextBox.Text = onlineCandidateScheduleDetail.EmailId;
                    OnlineScheduleCandidate_candidateIdHiddenField.Value = onlineCandidateScheduleDetail.CandidateID.ToString();
                    OnlineScheduleCandidate_chatRoomKeyHiddenField.Value = onlineCandidateScheduleDetail.ChatRoomName;

                    if (!Utility.IsNullOrEmpty(onlineCandidateScheduleDetail.InterviewDate))
                    {
                        OnlineScheduleCandidate_scheduleDateTextBox.Text =
                            onlineCandidateScheduleDetail.InterviewDate.ToShortDateString();
                    }

                    OnlineScheduleCandidate_scheduleFromDropDownList.SelectedValue =
                        onlineCandidateScheduleDetail.TimeSlotIDFrom.ToString();
                    OnlineScheduleCandidate_scheduleToDropDownList.SelectedValue =
                        onlineCandidateScheduleDetail.TimeSlotIDTo.ToString();

                    if (!Utility.IsNullOrEmpty(onlineCandidateScheduleDetail.ReminderType))
                    {
                        OnlineScheduleCandidate_remainderRadioButtonList.SelectedValue = 
                            onlineCandidateScheduleDetail.ReminderType;
                    }

                    BindInterviewAssessor();
                    DisplayScheduledAssessorCheck();

                    //Hide search results
                    OnlineScheduleCandidate_searchSlotsDiv.Style["display"] = "none";
                    OnlineScheduleCandidate_searchResultsSlotsDiv.Style["display"] = "none";

                    OnlineScheduleCandidate_requestTimeSlotSectionDiv.Style["display"] = "none";
                    OnlineScheduleCandidate_createTimeSlotSectionDiv.Style["display"] = "block";

                    OnlineScheduleCandidate_sendRequestButton.Visible = false;
                    OnlineScheduleCandidate_scheduleButton.Visible = true;

                    OnlineScheduleCandidate_skillAssociated_restoreHiddenField.Value = "Y";

                    //Expand or restore
                    ExpandRestore();

                    OnlineScheduleCandidate_createTimeSlotLinkButton.CssClass = "request_tab_no_link_button";
                    OnlineScheduleCandidate_requestTimeSlotLinkButton.CssClass = "request_tab_link_button";

                    OnlineScheduleCandidate_requestTimeSlotLinkButton.Visible = false;

                }
                LoadSkillSet(candidateInterviewID);
            }
            else
            {
                ClearControls();
                OnlineScheduleCandidate_sectionTitleLiteral.Text = "Schedule Candidate";

                OnlineScheduleCandidate_seacrchImageButton.Visible = true;
                OnlineScheduleCandidate_newButton.Visible = true;
                OnlineScheduleCandidate_seacrchPPImageButton.Visible = true;

                OnlineScheduleCandidate_createTimeSlotLinkButton.Visible = true;
                OnlineScheduleCandidate_requestTimeSlotLinkButton.Visible = true;

                //Hide search results
                OnlineScheduleCandidate_searchSlotsDiv.Style["display"] = "none";
                OnlineScheduleCandidate_searchResultsSlotsDiv.Style["display"] = "none";

                OnlineScheduleCandidate_requestTimeSlotSectionDiv.Style["display"] = "none";
                OnlineScheduleCandidate_createTimeSlotSectionDiv.Style["display"] = "block";

                OnlineScheduleCandidate_sendRequestButton.Visible = false;
                OnlineScheduleCandidate_scheduleButton.Visible = true;

                //Expand or restore
                
                OnlineScheduleCandidate_createTimeSlotLinkButton.CssClass = "request_tab_no_link_button";
                OnlineScheduleCandidate_requestTimeSlotLinkButton.CssClass = "request_tab_link_button";

                LoadSkillSet(0);
                BindInterviewAssessor();
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>                                         
        private void ExpandRestore()
        {
            if (OnlineScheduleCandidate_skillAssociated_restoreHiddenField.Value == "Y")
            {
                OnlineScheduleCandidate_skillAssociatedDiv.Style["display"] = "block";
                OnlineScheduleCandidate_skillAssociatedUpSpan.Style["display"] = "none";
                OnlineScheduleCandidate_skillAssociatedDownSpan.Style["display"] = "block";
            }
            else
            {
                OnlineScheduleCandidate_skillAssociatedDiv.Style["display"] = "none";
                OnlineScheduleCandidate_skillAssociatedUpSpan.Style["display"] = "block";
                OnlineScheduleCandidate_skillAssociatedDownSpan.Style["display"] = "none";
            }
            OnlineScheduleCandidate_skillAssociatedTR.Attributes.Add("onclick",
                "ExpandOrRestoreSkillAssociated('" +
                OnlineScheduleCandidate_skillAssociatedDiv.ClientID + "','" +
                OnlineScheduleCandidate_skillAssociatedUpSpan.ClientID + "','" +
                OnlineScheduleCandidate_skillAssociatedDownSpan.ClientID + "','" +
                OnlineScheduleCandidate_skillAssociated_restoreHiddenField.ClientID + "')");
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>                                         
        private void ExpandRestoreSearchResults()
        {
            if (OnlineScheduleCandidate_searchSlots_restoreHiddenField.Value == "Y")
            {
                OnlineScheduleCandidate_candidateDetailDIV.Style["display"] = "block";
                OnlineScheduleCandidate_searchSlotsUpSpan.Style["display"] = "none";
                OnlineScheduleCandidate_searchSlotsDownSpan.Style["display"] = "block";
            }
            else
            {
                OnlineScheduleCandidate_candidateDetailDIV.Style["display"] = "none";
                OnlineScheduleCandidate_searchSlotsUpSpan.Style["display"] = "block";
                OnlineScheduleCandidate_searchSlotsDownSpan.Style["display"] = "none";
            }
            OnlineScheduleCandidate_searchSlotsTR.Attributes.Add("onclick",
                "ExpandOrRestoreSearchResults('" +
                OnlineScheduleCandidate_candidateDetailDIV.ClientID + "','" +
                OnlineScheduleCandidate_searchSlotsUpSpan.ClientID + "','" +
                OnlineScheduleCandidate_searchSlotsDownSpan.ClientID + "','" +
                OnlineScheduleCandidate_searchSlots_restoreHiddenField.ClientID + "')");
        }

        /// <summary>
        /// Method to bind the interview assessor
        /// </summary>
        private void BindInterviewAssessor()
        {
            List<AssessorDetail> assessorDetail = new List<AssessorDetail>();

            assessorDetail = new OnlineInterviewBLManager().
                GetOnlineInterviewAssessorByInterviewKey(interviewKey);

            OnlineScheduleCandidate_interviewAssessorCheckBoxList.DataSource = null;
            OnlineScheduleCandidate_interviewAssessorCheckBoxList.DataBind();

            if (assessorDetail != null)
            {
                OnlineScheduleCandidate_interviewAssessorCheckBoxList.DataSource = assessorDetail;
                OnlineScheduleCandidate_interviewAssessorCheckBoxList.DataTextField = "AssessorName";
                OnlineScheduleCandidate_interviewAssessorCheckBoxList.DataValueField = "AssessorId";
                OnlineScheduleCandidate_interviewAssessorCheckBoxList.DataBind();
            }
        }


        /// <summary>
        /// Method that construct and return the list of selected times
        /// </summary>
        /// <returns>Returns the list of selected times</returns>
        private List<AssessorDetail> ConstructSelectedAssessor()
        {
            List<AssessorDetail> assessorDetails = new List<AssessorDetail>();
            foreach (ListItem cBox in OnlineScheduleCandidate_interviewAssessorCheckBoxList.Items)
            {
                AssessorDetail assessorDetail = new AssessorDetail();
                if (cBox.Selected)
                {
                    assessorDetail.AssessorId = Convert.ToInt32(cBox.Value);
                    assessorDetail.FirstName = cBox.Text;
                    assessorDetails.Add(assessorDetail);
                }
            }
            return assessorDetails;
        }

        /// <summary>
        /// Method to display scheduled assessor to be checked
        /// </summary>
        private void DisplayScheduledAssessorCheck()
        {
            List<AssessorDetail> assessors = new List<AssessorDetail>();

            assessors = new OnlineInterviewBLManager().
                GetOnlineInterviewScheduledAssessorByCandidateID(candidateInterviewID);

            if (assessors != null)
            {
                foreach (ListItem cBox in OnlineScheduleCandidate_interviewAssessorCheckBoxList.Items)
                {
                    int index = assessors.FindIndex(item => item.AssessorId == Convert.ToInt32(cBox.Value));
                    if (index >= 0)
                    {
                        cBox.Selected = true;
                    }
                }
            }
        }

        /// <summary>
        /// Method to construct the list of choices
        /// </summary>
        private List<AssessorTimeSlotDetail> ConstructChoiceList(bool isSinglSelect)
        {
            DateTime selectedDate = new DateTime();

            //selectedDate = DateTime.ParseExact(DateTime.Today.ToString("MM/dd/yyyy"), "MM/dd/yyyy", null);

            List<AssessorTimeSlotDetail> timeSlots = null;

            if (Session["REQUESTED_CHOICE_LIST"] == null)
            {
                timeSlots = new List<AssessorTimeSlotDetail>();
            }
            else
                timeSlots = Session["REQUESTED_CHOICE_LIST"] as List<AssessorTimeSlotDetail>;

            List<AssessorDetail> assessorDetails = new List<AssessorDetail>();

            // Create a time slot object.
            AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

            // Instantiate time slot list.
            if (timeSlots == null)
                timeSlots = new List<AssessorTimeSlotDetail>();

            foreach (GridViewRow row in OnlineScheduleCandidate_searchTimeSlotGridView.Rows)
            {
                // Instantiate assessor slot list.
                if (assessorDetails == null)
                    assessorDetails = new List<AssessorDetail>();

                // Create a assessor object
                AssessorDetail assessorDetail = new AssessorDetail();

                CheckBox OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox = ((CheckBox)row.FindControl
                       ("OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox"));

                if (OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox.Checked)
                {
                    Label OnlineScheduleCandidate_searchTimeSlotGridView_interviewerNameLabel =
                        ((Label)row.FindControl("OnlineScheduleCandidate_searchTimeSlotGridView_interviewerNameLabel"));

                    HiddenField OnlineScheduleCandidate_searchTimeSlotGridView_assessorIDHiddenField =
                        ((HiddenField)row.FindControl("OnlineScheduleCandidate_searchTimeSlotGridView_assessorIDHiddenField"));

                    Label OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel =
                        ((Label)row.FindControl("OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel"));

                    if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_searchTimeSlotGridView_assessorIDHiddenField.Value))
                    {
                        assessorDetail.AssessorId =
                            Convert.ToInt32(OnlineScheduleCandidate_searchTimeSlotGridView_assessorIDHiddenField.Value);
                    }

                    assessorDetail.AssessorName = OnlineScheduleCandidate_searchTimeSlotGridView_interviewerNameLabel.Text;

                    if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel.Text))
                    {
                        //selectedDate = Convert.ToDateTime(OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel.Text);
                        selectedDate = DateTime.ParseExact(OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel.Text, "MM/dd/yyyy", null);
                    }

                    OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox.Checked = false;

                    if (!Utility.IsNullOrEmpty(assessorDetails) && assessorDetails.Count != 0
                       && (assessorDetails.Any(res => res.AssessorId == assessorDetail.AssessorId)))
                        continue;

                    assessorDetails.Add(assessorDetail);
                }
            }

            timeSlot.AvailabilityDate = selectedDate;

            if (isSinglSelect)
            {
                timeSlot.TimeSlotTextFrom = OnlineScheduleCandidate_fromDropDownList.SelectedItem.Text;
                timeSlot.TimeSlotTextTo = OnlineScheduleCandidate_toDropDownList.SelectedItem.Text;

                if (OnlineScheduleCandidate_fromDropDownList.SelectedIndex != 0)
                {
                    timeSlot.TimeSlotIDFrom =
                        Convert.ToInt32(OnlineScheduleCandidate_fromDropDownList.SelectedItem.Value);
                }

                if (OnlineScheduleCandidate_toDropDownList.SelectedIndex != 0)
                {
                    timeSlot.TimeSlotIDTo =
                        Convert.ToInt32(OnlineScheduleCandidate_toDropDownList.SelectedItem.Value);
                }
            }
            else
            {
                timeSlot.TimeSlotTextFrom = OnlineScheduleCandidate_addRequestFromDropDownList.SelectedItem.Text;
                timeSlot.TimeSlotTextTo = OnlineScheduleCandidate_addRequestToDropDownList.SelectedItem.Text;

                if (OnlineScheduleCandidate_addRequestFromDropDownList.SelectedIndex != 0)
                {
                    timeSlot.TimeSlotIDFrom =
                        Convert.ToInt32(OnlineScheduleCandidate_addRequestFromDropDownList.SelectedItem.Value);
                }

                if (OnlineScheduleCandidate_addRequestToDropDownList.SelectedIndex != 0)
                {
                    timeSlot.TimeSlotIDTo =
                        Convert.ToInt32(OnlineScheduleCandidate_addRequestToDropDownList.SelectedItem.Value);
                }
            }

            if (assessorDetails != null)
            {
                string assessorNames = string.Join(", ", assessorDetails.Select(x => x.AssessorName).ToArray());

                timeSlot.Assessor = assessorNames;
                timeSlot.assessorDetail = assessorDetails;
            }

            timeSlots.Add(timeSlot);

            Session["REQUESTED_CHOICE_LIST"] = timeSlots;

            return timeSlots;
        }

        /// <summary>
        /// Method to clear the grid and page controls
        /// </summary>
        private void ClearControls()
        {
            Session["REQUESTED_CHOICE_LIST"] = null;

            OnlineScheduleCandidate_timeSlotGridView.DataSource = null;
            OnlineScheduleCandidate_timeSlotGridView.DataBind();

            OnlineScheduleCandidate_candidateNameTextBox.Text = string.Empty;
            OnlineScheduleCandidate_emailTextBox.Text = string.Empty;
            OnlineScheduleCandidate_candidateIdHiddenField.Value = string.Empty;

            OnlineScheduleCandidate_searchTimeSlotGridView.DataSource = null;
            OnlineScheduleCandidate_searchTimeSlotGridView.DataBind();

            OnlineScheduleCandidate_scheduleDateTextBox.Text = string.Empty;


            OnlineScheduleCandidate_scheduleFromDropDownList.SelectedIndex = 0;
            OnlineScheduleCandidate_scheduleToDropDownList.SelectedIndex = 0;
        }

        /// <summary>
        /// Method to validate whether user have checked date or not
        /// </summary>
        /// <returns></returns>
        private bool IsCheckSelectedDate()
        {
            bool rtnFlag = false;

            foreach (GridViewRow row in OnlineScheduleCandidate_searchTimeSlotGridView.Rows)
            {
                CheckBox OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox = ((CheckBox)row.FindControl
                       ("OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox"));

                if (OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox.Checked)
                {
                    rtnFlag = true;
                    break;
                }
            }

            return rtnFlag;

        }

        /// <summary>
        /// Method to validate whether similar dates are selected or not
        /// </summary>
        /// <returns></returns>
        private bool IsSimilarDateSelected()
        {
            bool isValid = true;

            int i = 0;
            DateTime selectedDate = new DateTime();

            foreach (GridViewRow row in OnlineScheduleCandidate_searchTimeSlotGridView.Rows)
            {
                CheckBox OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox = ((CheckBox)row.FindControl
                       ("OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox"));

                if (OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox.Checked)
                {
                    Label OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel =
                        ((Label)row.FindControl("OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel"));

                    if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel.Text))
                    {
                        if (i == 0)
                        {
                            //selectedDate = Convert.ToDateTime(OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel.Text,);
                            selectedDate = DateTime.ParseExact(OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel.Text,"MM/dd/yyyy",null); 
                        }
                        else
                        {
                            if (selectedDate !=
                                DateTime.ParseExact(OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel.Text, "MM/dd/yyyy", null))
                            {
                                isValid = false;
                                break;
                            }
                        }
                        i++;
                    }
                }
            }

            return isValid;
        }

        /// <summary>
        /// Method to validate the search criteria
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        private bool IsValidSearchCriteria()
        {
            bool isValid = true;

            if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_fromDate_TextBox.Text.Trim()) ||
                DateTime.Parse(OnlineScheduleCandidate_fromDate_TextBox.Text) < DateTime.Today)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                       OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select valid from date");
                isValid = false;
            }

            if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_toDate_TextBox.Text.Trim()) ||
                DateTime.Parse(OnlineScheduleCandidate_toDate_TextBox.Text) < DateTime.Today)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                       OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select valid to date");
                isValid = false;
            }

            if (OnlineScheduleCandidate_fromDropDownList.SelectedIndex == 0)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                       OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select valid from time");
                isValid = false;
            }

            if (OnlineScheduleCandidate_toDropDownList.SelectedIndex == 0)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                       OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select valid to time");
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// This function clears controls text on page load
        /// </summary>
        private void ClearMessageControls()
        {
            OnlineScheduleCandidate_topErrorMessageLabel.Text = string.Empty;
            OnlineScheduleCandidate_bottomErrorMessageLabel.Text = string.Empty;
            OnlineScheduleCandidate_topSuccessMessageLabel.Text = string.Empty;
            OnlineScheduleCandidate_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method to validate scheduled time is valid/available
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        private bool IsScheduledTimeAvailable()
        {
            bool isValid = true;


            return isValid;
        }

        /// <summary>
        /// Method to validate whether immediate schedule 
        /// inputs are valid or not
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        private bool IsValidImmediateSchedule()
        {
            bool isValid = true;

            if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_candidateIdHiddenField.Value) ||
                Convert.ToInt32(OnlineScheduleCandidate_candidateIdHiddenField.Value) == 0)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select candidate");
                isValid = false;
            }


            if (Utility.IsNullOrEmpty(OnlineScheduleCandidate_scheduleDateTextBox.Text.Trim()) ||
                DateTime.Parse(OnlineScheduleCandidate_scheduleDateTextBox.Text) < DateTime.Today)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select valid schedule date");
                isValid = false;
            }

            if (OnlineScheduleCandidate_scheduleFromDropDownList.SelectedIndex == 0)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select valid from time");
                isValid = false;
            }

            if (OnlineScheduleCandidate_scheduleToDropDownList.SelectedIndex == 0)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select valid to time");
                isValid = false;
            }

            List<AssessorDetail> assessorDetails = new List<AssessorDetail>();
            assessorDetails = ConstructSelectedAssessor();

            if (assessorDetails == null || assessorDetails.Count == 0)
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Please select assessor(s) to schedule");
                isValid = false;
            }

            if (!IsValidInterviewSkill())
            {
                base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, "Skill weightage should be equal to 100");
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Method to load from to time
        /// </summary>
        private void LoadFromToTime()
        {
            AssessorBLManager blManager = new AssessorBLManager();

            // Set time slot settings.
            OnlineScheduleCandidate_fromDropDownList.DataSource = blManager.
                GetTimeSlotSettings(TimeSlotType.From);
            OnlineScheduleCandidate_fromDropDownList.DataTextField = "Name";
            OnlineScheduleCandidate_fromDropDownList.DataValueField = "ID";
            OnlineScheduleCandidate_fromDropDownList.DataBind();
            OnlineScheduleCandidate_fromDropDownList.Items.Insert(0, new ListItem("--Select--", ""));

            OnlineScheduleCandidate_toDropDownList.DataSource = blManager.
                GetTimeSlotSettings(TimeSlotType.To);
            OnlineScheduleCandidate_toDropDownList.DataTextField = "Name";
            OnlineScheduleCandidate_toDropDownList.DataValueField = "ID";
            OnlineScheduleCandidate_toDropDownList.DataBind();
            OnlineScheduleCandidate_toDropDownList.Items.Insert(0, new ListItem("--Select--", ""));

            OnlineScheduleCandidate_addRequestFromDropDownList.DataSource = blManager.
                GetTimeSlotSettings(TimeSlotType.From);
            OnlineScheduleCandidate_addRequestFromDropDownList.DataTextField = "Name";
            OnlineScheduleCandidate_addRequestFromDropDownList.DataValueField = "ID";
            OnlineScheduleCandidate_addRequestFromDropDownList.DataBind();
            OnlineScheduleCandidate_addRequestFromDropDownList.Items.Insert(0, new ListItem("--Select--", ""));

            OnlineScheduleCandidate_addRequestToDropDownList.DataSource = blManager.
                GetTimeSlotSettings(TimeSlotType.To);
            OnlineScheduleCandidate_addRequestToDropDownList.DataTextField = "Name";
            OnlineScheduleCandidate_addRequestToDropDownList.DataValueField = "ID";
            OnlineScheduleCandidate_addRequestToDropDownList.DataBind();
            OnlineScheduleCandidate_addRequestToDropDownList.Items.Insert(0, new ListItem("--Select--", ""));


            OnlineScheduleCandidate_scheduleFromDropDownList.DataSource = blManager.
               GetTimeSlotSettings(TimeSlotType.From);
            OnlineScheduleCandidate_scheduleFromDropDownList.DataTextField = "Name";
            OnlineScheduleCandidate_scheduleFromDropDownList.DataValueField = "ID";
            OnlineScheduleCandidate_scheduleFromDropDownList.DataBind();
            OnlineScheduleCandidate_scheduleFromDropDownList.Items.Insert(0, new ListItem("--Select--", ""));

            OnlineScheduleCandidate_scheduleToDropDownList.DataSource = blManager.
               GetTimeSlotSettings(TimeSlotType.To);
            OnlineScheduleCandidate_scheduleToDropDownList.DataTextField = "Name";
            OnlineScheduleCandidate_scheduleToDropDownList.DataValueField = "ID";
            OnlineScheduleCandidate_scheduleToDropDownList.DataBind();
            OnlineScheduleCandidate_scheduleToDropDownList.Items.Insert(0, new ListItem("--Select--", ""));

        }

        
        /// <summary>
        /// Method to load the skill list
        /// </summary>
        private void LoadSkillSet(int candidateInterviewID)
        {
            List<SkillDetail> skillDetail = new List<SkillDetail>();

            if (candidateInterviewID != 0)
            {
                skillDetail = new OnlineInterviewBLManager().
                    GetOnlineInterviewScheduledSkills(candidateInterviewID);
            }
            else
            {
                skillDetail = new OnlineInterviewBLManager().
                    GetSkillByOnlineInterviewKey(interviewKey);
            }

            if (skillDetail != null)
            {
                OnlineScheduleCandidate_skillAssociatedGridView.DataSource = skillDetail;
                OnlineScheduleCandidate_skillAssociatedGridView.DataBind();

                Session["SKILL_ASSOCIATED"] = skillDetail;
            }
        }

        
        /// <summary>
        /// Method to search available time slot
        /// </summary>
        private void SearchTimeSlot(int pageNumber)
        {
            int totalRecords = 0;
            

            DateTime fromDate = Convert.ToDateTime("01/01/0001");
            DateTime toDate = Convert.ToDateTime("01/01/0001");

            if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_fromDate_TextBox.Text))
            {
                fromDate = Convert.ToDateTime(OnlineScheduleCandidate_fromDate_TextBox.Text);
            }

            if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_toDate_TextBox.Text))
            {
                toDate = Convert.ToDateTime(OnlineScheduleCandidate_toDate_TextBox.Text);
            }

            AssessorTimeSlotSearchCriteria timeSlotCriteria = new AssessorTimeSlotSearchCriteria();
            timeSlotCriteria.FromDate = fromDate;
            timeSlotCriteria.ToDate = toDate;
            timeSlotCriteria.TimeSlotIDFrom = Convert.ToInt32(OnlineScheduleCandidate_fromDropDownList.SelectedValue);
            timeSlotCriteria.TimeSlotIDTo = Convert.ToInt32(OnlineScheduleCandidate_toDropDownList.SelectedValue);

            List<AssessorTimeSlotDetail> assessorSlots = new List<AssessorTimeSlotDetail>();

            assessorSlots = new AssessorBLManager().GetAssessorAvailableDates(timeSlotCriteria, base.tenantID, pageNumber,
                base.GridPageSize, ViewState["SORT_FIELD"].ToString(), (SortType)ViewState["SORT_ORDER"], out totalRecords);

            if (assessorSlots == null || assessorSlots.Count == 0)
            {
                OnlineScheduleCandidate_searchTimeSlotGridView.DataSource = null;
                OnlineScheduleCandidate_searchTimeSlotGridView.DataBind();
            }
            else
            {
                var assessorSlotsFiltered = Enumerable.Where(assessorSlots,
                    t => t.IsAssessorAvailable.Trim() == "Y");

                if (assessorSlotsFiltered != null)
                {
                    OnlineScheduleCandidate_searchTimeSlotGridView.DataSource = assessorSlotsFiltered;
                    OnlineScheduleCandidate_searchTimeSlotGridView.DataBind();
                    OnlineScheduleCandidate_pageNavigator.PageSize = base.GridPageSize;
                    OnlineScheduleCandidate_pageNavigator.TotalRecords = totalRecords;
                }
                else
                {
                    OnlineScheduleCandidate_searchTimeSlotGridView.DataSource = null;
                    OnlineScheduleCandidate_searchTimeSlotGridView.DataBind();
                }
            }

            OnlineScheduleCandidate_searchSlots_restoreHiddenField.Value = "Y";
            ExpandRestoreSearchResults();

        }


        /// <summary>
        /// Method to bind the subjects skill
        /// </summary>
        private void BindSubjects(string skills)
        {
            List<SkillDetail> skillDetails = null;

            if (Session["SKILL_ASSOCIATED"] == null)
            {
                skillDetails = new List<SkillDetail>();
            }
            else
                skillDetails = Session["SKILL_ASSOCIATED"] as List<SkillDetail>;

            string[] arraySkill = skills.Trim().Split(',');

            foreach (string skill in arraySkill)
            {
                if (skill.Trim().Length == 0)
                    continue;

                // Retrieve skill detail.
                SkillDetail skillDetail = new AssessorBLManager().GetSkillDetail
                    (skill.Trim());

                // Check if skill detail is null.
                if (skillDetail == null)
                {
                    continue;
                }

                // Check if skill detail already selected.
                var findSkill = from sk in skillDetails
                                where sk.SkillID == skillDetail.SkillID
                                select sk;

                if (findSkill != null && findSkill.Count() > 0)
                {
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                        OnlineScheduleCandidate_bottomErrorMessageLabel, 
                        string.Format("Skill '{0}' already added", findSkill.First().SkillName));
                    continue;
                }

                // Add to the skills list.
                skillDetails.Add(skillDetail);
            }

            // Keep the skills in session.
            Session["SKILL_ASSOCIATED"] = skillDetails;

            // Assign the skills to the grid view.
            OnlineScheduleCandidate_skillAssociatedGridView.DataSource = skillDetails;
            OnlineScheduleCandidate_skillAssociatedGridView.DataBind();

            // Clear skills text box.
            OnlineScheduleCandidate_skillTextBox.Text = string.Empty;

            // Set the focus to skills text box.
            OnlineScheduleCandidate_skillTextBox.Focus();

            // Clear hidden fields.
            OnlineScheduleCandidate_skillIDHiddenField.Value = string.Empty;

            AutoPopulateSkillWeightage();

        }

        /// <summary>
        /// Method that send email to the requested 
        /// candidates with list of choice slots.
        /// </summary>
        /// <param name="candidateInterviewDetail">
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the interview details
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendSlotRequestToCandidate(CandidateInterviewSessionDetail candidateInterviewDetail,
            EntityType entityType)
        {
            try
            {
                // Send email.
                //new EmailHandler().SendMail(entityType, candidateDetail);
                new SmptEmailHandler().OnlineInterviewSlotRequestSendMail(entityType, candidateInterviewDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Method that sends mail to the scheduled assessor
        /// </summary>
        /// <param name="assessorDetails">
        /// A <see cref="AssessorDetail"/> that holds the assessor
        /// detail.
        /// </param>
        /// <param name="chatRoomKey">
        /// A <see cref="String"/> that holds the chat room key
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendScheduledInfoToAssessor(List<AssessorDetail> assessorDetails,
            string chatRoomKey, int candidateScheduleID, EntityType entityType)
        {
            try
            {
                // Send email.
                foreach (AssessorDetail assessorDetail in assessorDetails)
                {
                    assessorDetail.CandidateInterviewID = candidateScheduleID;
                    assessorDetail.ChatRoomKey = chatRoomKey;
                    new EmailHandler().SendMail(entityType, assessorDetail);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that sends mail to the scheduled candidate
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendScheduledInfoToCandidate(CandidateDetail candidateDetail,
            EntityType entityType)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(entityType, candidateDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Metod that returns the skill details
        /// </summary>
        /// <returns>
        /// The returns the associated skill
        /// </returns>
        private List<SkillDetail> ConstructInterviewSkillDetail()
        {
            List<SkillDetail> skills = null;

            foreach (GridViewRow row in OnlineScheduleCandidate_skillAssociatedGridView.Rows)
            {
                // Instantiate skill list.
                if (skills == null)
                    skills = new List<SkillDetail>();

                // Create a skill object.
                SkillDetail skill = new SkillDetail();

                TextBox OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox = ((TextBox)row.FindControl
                       ("OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox"));

                HiddenField OnlineScheduleCandidate_skillAssociatedGridView_skillIDHiddenField = ((HiddenField)row.FindControl
                    ("OnlineScheduleCandidate_skillAssociatedGridView_skillIDHiddenField"));

                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillAssociatedGridView_skillIDHiddenField.Value))
                {
                    skill.SkillID = Convert.ToInt32(OnlineScheduleCandidate_skillAssociatedGridView_skillIDHiddenField.Value);
                }

                if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox.Text))
                {
                    skill.Weightage = Convert.ToInt32(OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox.Text);
                }

                skills.Add(skill);
            }
            return skills;
        }

        /// <summary>
        /// Method to validate the interview skill assigned
        /// </summary>
        /// <returns>
        /// Returns true if interview skill weightage is 100
        /// </returns>
        private bool IsValidInterviewSkill()
        {
            bool isValidWeightage = true;

            int weightage = 0;
            if (OnlineScheduleCandidate_skillAssociatedGridView != null &&
                OnlineScheduleCandidate_skillAssociatedGridView.Rows.Count != 0)
            {
                foreach (GridViewRow row in OnlineScheduleCandidate_skillAssociatedGridView.Rows)
                {
                    TextBox OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox = ((TextBox)row.FindControl
                           ("OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox"));

                    if (!Utility.IsNullOrEmpty(OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox.Text) &&
                        OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox.Text != "0")
                    {
                        weightage += Convert.ToInt32(OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox.Text);
                    }
                    else
                    {
                        isValidWeightage = false;
                        break;
                    }
                }
            }

            if (weightage != 100)
            {
                isValidWeightage = false;
            }

            return isValidWeightage;
        }

        /// <summary>
        /// Method to load the reminder settings
        /// </summary>
        private void LoadScheduleReminder()
        {
            List<AttributeDetail> reminderList =
                new AttributeBLManager().GetAttributesByType("INT_REM_SE", "E");

            if (reminderList != null)
            {
                OnlineScheduleCandidate_remainderRadioButtonList.DataSource = reminderList;
                OnlineScheduleCandidate_remainderRadioButtonList.DataValueField = "AttributeID";
                OnlineScheduleCandidate_remainderRadioButtonList.DataTextField = "AttributeName";
                OnlineScheduleCandidate_remainderRadioButtonList.DataBind();
            }
        }

        /// <summary>
        /// Method to autopopulate the skill weightage
        /// </summary>
        private void AutoPopulateSkillWeightage()
        {
            //Here is the logic for calculate weight
            DataTable dtTestSearchCriteria = null;
            dtTestSearchCriteria = BuildTableFromGridView();
            int TotalWeightage = 0;
            LoadWeightages(ref dtTestSearchCriteria, dtTestSearchCriteria.Rows.Count, out TotalWeightage);
            int index = 0;
            foreach (GridViewRow gridViewRow in OnlineScheduleCandidate_skillAssociatedGridView.Rows)
            {
                TextBox Weightage = (TextBox)gridViewRow.FindControl("OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox");
                Weightage.Text = dtTestSearchCriteria.Rows[index]["Weightage"].ToString();
                index++;
            }
        }


        /// <summary>
        /// Builds data table from test segment grid view
        /// </summary>
        /// <returns>Data table that contains user given input for test segment grid</returns>
        private DataTable BuildTableFromGridView()
        {
            DataTable dttestDrft = null;
            try
            {
                dttestDrft = new DataTable();
                AddTestSegmentColumns(ref dttestDrft);

                DataRow drNewRow = null;
                int Weightage = 0;
                int SNo = 1;
                foreach (GridViewRow gridViewRow in OnlineScheduleCandidate_skillAssociatedGridView.Rows)
                {
                    drNewRow = dttestDrft.NewRow();
                    drNewRow["SNO"] = SNo++.ToString();

                    ((TextBox)gridViewRow.FindControl("OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox")).Text = "";

                    int.TryParse(((TextBox)gridViewRow.FindControl("OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox")).Text, out Weightage);
                    if (Weightage > 0)
                        drNewRow["Weightage"] = Weightage;

                    dttestDrft.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                return dttestDrft;
            }
            finally
            {
                if (dttestDrft != null) dttestDrft = null;
            }
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(OnlineScheduleCandidate_topErrorMessageLabel,
                         OnlineScheduleCandidate_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateAutomaticInterviewTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }


        #endregion Private Methods

        #region Protected Methods


        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Methods

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendSlotRequestToCandidateCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="resultAssessor">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendScheduledInfoToAssessorCallBack(IAsyncResult resultAssessor)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegateAssessor caller = (AsyncTaskDelegateAssessor)resultAssessor.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(resultAssessor);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="resultCandidate">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendScheduledInfoToCandidateCallBack(IAsyncResult resultCandidate)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegateCandidate caller = (AsyncTaskDelegateCandidate)resultCandidate.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(resultCandidate);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        #endregion Asynchronous Method Handlers

       
}
}