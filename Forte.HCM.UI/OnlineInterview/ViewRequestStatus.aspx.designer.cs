﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.OnlineInterview {
    
    
    public partial class ViewRequestStatus {
        
        /// <summary>
        /// ViewRequestStatus_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ViewRequestStatus_headerLiteral;
        
        /// <summary>
        /// ViewRequestStatus_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewRequestStatus_topResetLinkButton;
        
        /// <summary>
        /// ViewRequestStatus_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewRequestStatus_topCancelLinkButton;
        
        /// <summary>
        /// ViewRequestStatus_topMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ViewRequestStatus_topMessageUpdatePanel;
        
        /// <summary>
        /// ViewRequestStatus_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_topSuccessMessageLabel;
        
        /// <summary>
        /// ViewRequestStatus_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_topErrorMessageLabel;
        
        /// <summary>
        /// ViewRequestStatus_searchCriteriasDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ViewRequestStatus_searchCriteriasDiv;
        
        /// <summary>
        /// ViewRequestStatus_criteriaUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ViewRequestStatus_criteriaUpdatePanel;
        
        /// <summary>
        /// ViewRequestStatus_statusLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_statusLabel;
        
        /// <summary>
        /// ViewRequestStatus_statusDropDownList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ViewRequestStatus_statusDropDownList;
        
        /// <summary>
        /// ViewRequestStatus_requestStatusHelpImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewRequestStatus_requestStatusHelpImageButton;
        
        /// <summary>
        /// ViewRequestStatus__initatedByLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus__initatedByLabel;
        
        /// <summary>
        /// ViewRequestStatus_initiatedByTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ViewRequestStatus_initiatedByTextBox;
        
        /// <summary>
        /// ViewRequestStatus_initiatedByImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewRequestStatus_initiatedByImageButton;
        
        /// <summary>
        /// ViewRequestStatus_initiatedByHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ViewRequestStatus_initiatedByHiddenField;
        
        /// <summary>
        /// ViewRequestStatus_initiatedDateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_initiatedDateLabel;
        
        /// <summary>
        /// ViewRequestStatus_initiatedDateTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ViewRequestStatus_initiatedDateTextBox;
        
        /// <summary>
        /// ViewRequestStatus_initiatedDateImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewRequestStatus_initiatedDateImageButton;
        
        /// <summary>
        /// ViewRequestStatus_initiatedDateMaskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender ViewRequestStatus_initiatedDateMaskedEditExtender;
        
        /// <summary>
        /// ViewRequestStatus_initiatedDateMaskedEditValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditValidator ViewRequestStatus_initiatedDateMaskedEditValidator;
        
        /// <summary>
        /// ViewRequestStatus_initiatedDateCalendarExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CalendarExtender ViewRequestStatus_initiatedDateCalendarExtender;
        
        /// <summary>
        /// ViewRequestStatus_sessionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_sessionLabel;
        
        /// <summary>
        /// ViewRequestStatus_sessionTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ViewRequestStatus_sessionTextBox;
        
        /// <summary>
        /// ViewRequestStatus_sessionImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewRequestStatus_sessionImageButton;
        
        /// <summary>
        /// ViewRequestStatus_sessionKeyHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ViewRequestStatus_sessionKeyHiddenField;
        
        /// <summary>
        /// ViewRequestStatus_assessorLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_assessorLabel;
        
        /// <summary>
        /// ViewRequestStatus_assessorTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ViewRequestStatus_assessorTextBox;
        
        /// <summary>
        /// ViewRequestStatus_assessorImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewRequestStatus_assessorImageButton;
        
        /// <summary>
        /// ViewRequestStatus_assessorIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ViewRequestStatus_assessorIDHiddenField;
        
        /// <summary>
        /// ViewRequestStatus_requestedDateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_requestedDateLabel;
        
        /// <summary>
        /// ViewRequestStatus_requestedDateTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ViewRequestStatus_requestedDateTextBox;
        
        /// <summary>
        /// ViewRequestStatus_requestedDateImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewRequestStatus_requestedDateImageButton;
        
        /// <summary>
        /// ViewRequestStatus_requestedDateMaskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender ViewRequestStatus_requestedDateMaskedEditExtender;
        
        /// <summary>
        /// ViewRequestStatus_requestedDateMaskedEditValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditValidator ViewRequestStatus_requestedDateMaskedEditValidator;
        
        /// <summary>
        /// ViewRequestStatus_requestedDateCustomCalendarExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CalendarExtender ViewRequestStatus_requestedDateCustomCalendarExtender;
        
        /// <summary>
        /// ViewRequestStatus_skillLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_skillLabel;
        
        /// <summary>
        /// ViewRequestStatus_skillTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ViewRequestStatus_skillTextBox;
        
        /// <summary>
        /// ViewRequestStatus_commentsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_commentsLabel;
        
        /// <summary>
        /// ViewRequestStatus_commentsTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ViewRequestStatus_commentsTextBox;
        
        /// <summary>
        /// ViewRequestStatus_availabilityRequestButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ViewRequestStatus_availabilityRequestButton;
        
        /// <summary>
        /// ViewRequestStatus_searchButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ViewRequestStatus_searchButton;
        
        /// <summary>
        /// ViewRequestStatus_searchResultsTR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow ViewRequestStatus_searchResultsTR;
        
        /// <summary>
        /// ViewRequestStatus_requestsResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ViewRequestStatus_requestsResultsLiteral;
        
        /// <summary>
        /// ViewRequestStatus_sortHelpLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_sortHelpLabel;
        
        /// <summary>
        /// ViewRequestStatus_searchResultsUpSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ViewRequestStatus_searchResultsUpSpan;
        
        /// <summary>
        /// ViewRequestStatus_searchResultsUpImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ViewRequestStatus_searchResultsUpImage;
        
        /// <summary>
        /// ViewRequestStatus_searchResultsDownSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ViewRequestStatus_searchResultsDownSpan;
        
        /// <summary>
        /// ViewRequestStatus_searchResultsDownImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ViewRequestStatus_searchResultsDownImage;
        
        /// <summary>
        /// ViewRequestStatus_restoreHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ViewRequestStatus_restoreHiddenField;
        
        /// <summary>
        /// ViewRequestStatus_isMaximizedHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ViewRequestStatus_isMaximizedHiddenField;
        
        /// <summary>
        /// ViewRequestStatus_assessmentDetailsGridViewUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ViewRequestStatus_assessmentDetailsGridViewUpdatePanel;
        
        /// <summary>
        /// ViewRequestStatus_timeSlotsGridViewDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ViewRequestStatus_timeSlotsGridViewDiv;
        
        /// <summary>
        /// ViewRequestStatus_timeSlotsGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView ViewRequestStatus_timeSlotsGridView;
        
        /// <summary>
        /// ViewRequestStatus_pagingNavigator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PageNavigator ViewRequestStatus_pagingNavigator;
        
        /// <summary>
        /// ViewRequestStatus_bottomMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ViewRequestStatus_bottomMessageUpdatePanel;
        
        /// <summary>
        /// ViewRequestStatus_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_bottomSuccessMessageLabel;
        
        /// <summary>
        /// ViewRequestStatus_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewRequestStatus_bottomErrorMessageLabel;
        
        /// <summary>
        /// ViewRequestStatus_bottomResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewRequestStatus_bottomResetLinkButton;
        
        /// <summary>
        /// ViewRequestStatus_bottomCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewRequestStatus_bottomCancelLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.InterviewMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.InterviewMaster)(base.Master));
            }
        }
    }
}
