<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScheduleOnlineInterviewCandidate.aspx.cs"
    Inherits="Forte.HCM.UI.OnlineInterview.ScheduleOnlineInterviewCandidate" 
    MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ Register Src="~/CommonControls/TestScheduleControl.ascx" TagName="TestScheduleControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="ScheduleOnlineInterviewCandidate_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">

    <script language="javascript" type="text/javascript">
        // Validate the schedule popup. If candidate name or expiry date is empty, display the message to user.
        function VaildateSchedulePopup(nameTxt, expiryDateTxt, errorMsgLbl) {
            if (document.getElementById(nameTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Candidate name cannot be empty";
                return false;
            }
            else if (document.getElementById(expiryDateTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Expiry date cannot be empty";
                return false;
            }
        }

        // Pressing tab key from 'Test Session Id' textbox should 
        // load the test session and schedule candidate details.
        function TestSessionKeyDown(event, showButtonCtrl) {
            if (event.which || event.keyCode) {
                if ((event.which == 9) || (event.keyCode == 9)) {
                    var ctrlId = showButtonCtrl.toString();
                    document.getElementById(showButtonCtrl).click();
                    return;
                }
            }
        }

        function LoadInterviewOnlineRecommendAssessor(flag, key, attemptId, from, ctrlId) {
            var height = 590;
            var width = 800;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
            var queryStringValue = "../popup/ShowOnlineRecommendAssessor.aspx?source=lookup&key=" + key + "&flag=" + flag + "&attemptId=" + attemptId + "&from=" + from + "&ctrlId=" + ctrlId;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top">
                <asp:UpdatePanel ID="ScheduleOnlineInterviewCandidate_mainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="3" cellpadding="0">
                            <tr>
                                <td class="header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="72%" class="header_text_bold">
                                                <asp:Literal ID="ScheduleOnlineInterviewCandidate_headerLiteral" runat="server" Text="Schedule Online Interview Candidate"></asp:Literal>
                                            </td>
                                            <td width="28%" align="right">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                    <tr>
                                                        <td width="81%" align="right">
                                                            <asp:LinkButton ID="ScheduleOnlineInterviewCandidate_topResetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="ScheduleOnlineInterviewCandidate_resetLinkButton_Click" />
                                                        </td>
                                                        <td width="4%" align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:LinkButton ID="ScheduleOnlineInterviewCandidate_topCancelButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ScheduleOnlineInterviewCandidate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ScheduleOnlineInterviewCandidate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tab_body_bg">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <div id="ScheduleOnlineInterviewCandidate_testSessionDetailsDiv" runat="server" style="display: block">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_testSessionIdHeadLabel" runat="server" Text="Online Interview Session ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox" runat="server"></asp:TextBox></div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ScheduleOnlineInterviewCandidate_onlineSessionImageButton" runat="server" ImageAlign="Middle"
                                                                                    SkinID="sknbtnSearchicon" ToolTip="Click here to search session ID" Visible="false" /></div>
                                                                        </td>
                                                                        <td style="width: 14%">
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_candidateSessionIdHeadLabel" runat="server" Text="Candidate Session ID(s)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ScheduleOnlineInterviewCandidate_candidateSessionIdTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ScheduleOnlineInterviewCandidate_candidateSessionHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter one or more candidate session ID's separated by commas"
                                                                                    Height="16px" /></div>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:Button ID="ScheduleOnlineInterviewCandidate_showButton" runat="server" Text="Show Details"
                                                                                OnClick="ScheduleOnlineInterviewCandidate_showButton_Click" SkinID="sknButtonId" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div id="ScheduleOnlineInterviewCandidate_testDetailsDiv" runat="server" style="display: none">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class=" header_bg">
                                                                                <table cellpadding="0" cellspacing="0" width="98%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 93%" align="left">
                                                                                            <asp:Literal ID="ScheduleOnlineInterviewCandidate_testDetailsLiteral" runat="server" Text="Interview Session Details"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="grid_body_bg">
                                                                                <table cellpadding="0" cellspacing="5" width="100%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_sessionNameHeadLabel" runat="server" 
                                                                                            Text="Session Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 10%" colspan="3">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_sessionNameLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_positionProfileHeadLabel" runat="server" Text="Position Profile"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_positionProfileLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                     </tr>
                                                                                    
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_skillsHeadLabel" runat="server" 
                                                                                            Text="Skills/Areas" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 10%" colspan="7">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_skillsLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                     </tr>
                                                                                    
                                                                                     <tr>       
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_sessionAuthorHeadLabel" runat="server" Text="Sesssion Author"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_sessionAuthorLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 9%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 17%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_emailLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                         <td style="width: 9%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_noofsessionsHeadLabel" runat="server" Text="Number Of Sessions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 17%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_noofsessionsLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                         <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_expiryDateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_descriptionHeadLabel" runat="server" Text="Session Description"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_descriptionLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 11%">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_instructionsHeadLabel" runat="server" Text="Interview Instructions"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3">
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_instructionsLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 112px; overflow: auto;">
                                                                                                <asp:Literal ID="ScheduleOnlineInterviewCandidate_testDescLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 112px; overflow: auto;">
                                                                                                <asp:Literal ID="ScheduleOnlineInterviewCandidate_instructionsLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text="Candidates are requested to abide to the rules and regulations of the interview. Since cyber proctoring is enabled the scores will be calculated only based on the test tracking details."></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="ScheduleOnlineInterviewCandidate_testDetailsGridDiv" runat="server" style="display: none">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr id="ScheduleOnlineInterviewCandidate_searchTestResultsTR" runat="server">
                                                            <td class="header_bg">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td id="ScheduleOnlineInterviewCandidate_candidateSessionHeader" runat="server">
                                                                            <asp:Literal ID="ScheduleOnlineInterviewCandidate_candidateSessionDetailsLiteral" runat="server"
                                                                                Text="Candidate Session Details"></asp:Literal>
                                                                        </td>
                                                                        <td align="right" id="ScheduleOnlineInterviewCandidate_searchResultsTR" runat="server">
                                                                            <span id="ScheduleOnlineInterviewCandidate_searchResultsUpSpan" style="display: none;" runat="server">
                                                                                <asp:Image ID="ScheduleOnlineInterviewCandidate_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                            </span><span id="ScheduleOnlineInterviewCandidate_searchResultsDownSpan" style="display: block;"
                                                                                runat="server">
                                                                                <asp:Image ID="ScheduleOnlineInterviewCandidate_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg">
                                                                <div id="ScheduleOnlineInterviewCandidate_candidateSessionGridViewDIV" runat="server" style="height: 200px;
                                                                    overflow: auto;">
                                                                    <asp:UpdatePanel ID="ScheduleOnlineInterviewCandidate_candidateSessionGridViewUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="ScheduleOnlineInterviewCandidate_candidateSessionGridView" runat="server" AutoGenerateColumns="false"
                                                                                AllowSorting="True" OnRowCommand="ScheduleOnlineInterviewCandidate_candidateSessionGridView_RowCommand"
                                                                                OnRowDataBound="ScheduleOnlineInterviewCandidate_candidateSessionGridView_RowDataBound" OnSorting="ScheduleOnlineInterviewCandidate_candidateSessionGridView_Sorting"
                                                                                OnRowCreated="ScheduleOnlineInterviewCandidate_candidateSessionGridView_RowCreated" SkinID="sknWrapHeaderGrid">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="ScheduleOnlineInterviewCandidate_scheduleImageButton" runat="server" 
                                                                                                SkinID="sknScheduleImageButton"
                                                                                                ToolTip="Schedule Candidate" CommandName="schedule" 
                                                                                                CommandArgument='<%# Eval("CandidateTestSessionID") %>' 
                                                                                                Visible='<%# !IsScheduled(Eval("Status").ToString()) %>' />
                                                                                            <asp:ImageButton ID="ScheduleOnlineInterviewCandidate_rescheduleImageButton" runat="server" 
                                                                                                SkinID="sknViewAssessorImageButton"
                                                                                                ToolTip="View/Change Assessor/Time Slots" CommandName="ViewAssessor"
                                                                                                CommandArgument='<%# Eval("CandidateTestSessionID") %>' 
                                                                                                Visible='<%# IsScheduled(Eval("Status").ToString()) %>' />
                                                                                            <asp:ImageButton ToolTip="View Schedule" ID="ScheduleOnlineInterviewCandidate_ViewScheduleImageButton"
                                                                                                runat="server" SkinID="sknViewScheduleImageButton"
                                                                                                CommandName="viewschedule" Visible='<%# IsScheduled(Eval("Status").ToString()) %>'
                                                                                                 CommandArgument='<%# Eval("CandidateTestSessionID") %>'/>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Session ID" SortExpression="CandidateSessionId"
                                                                                        ItemStyle-Width="12%" HeaderStyle-CssClass="td_padding_right_20">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_candidateSessionLabel" runat="server" Text='<%# Eval("CandidateTestSessionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Name" SortExpression="CandidateName" ItemStyle-Width="22%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_candidateLabel" ToolTip='<%# Eval("CandidateFullName") %>'
                                                                                                runat="server" Text='<%# Eval("CandidateFirstName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email ID" SortExpression="EmailId" HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_emailLabel" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Status" SortExpression="Status" HeaderStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_statusLabel" runat="server" Text='<%# GetStatus(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).Status) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ScheduleOnlineInterviewCandidate_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg" align="right">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="72%" class="header_text_bold">
                                            </td>
                                            <td width="28%" align="right">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                    <tr>
                                                        <td width="81%" align="right">
                                                            <asp:LinkButton ID="ScheduleOnlineInterviewCandidate_bottonResetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="ScheduleOnlineInterviewCandidate_resetLinkButton_Click" />
                                                        </td>
                                                        <td width="4%" align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:LinkButton ID="ScheduleOnlineInterviewCandidate_bottonCancelLinkButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleOnlineInterviewCandidate_schedulePanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_scheduler">
                                        <div style="display: none;">
                                            <asp:Button ID="ScheduleOnlineInterviewCandidate_hiddenButton" runat="server" Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="ScheduleOnlineInterviewCandidate_questionResultLiteral" runat="server" Text="Schedule"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="ScheduleOnlineInterviewCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                        <tr>
                                                            <td align="left" class="popup_td_padding_10">
                                                                <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="4">
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_scheduleErrorMsgLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_candidateNamePopHeadLabel" runat="server" Text="Candidate Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class='mandatory'>*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>&nbsp;<asp:ImageButton ID="ScheduleOnlineInterviewCandidate_candidateNameImageButton"
                                                                                    SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to search for candidates"/>&nbsp;<asp:ImageButton ID="ScheduleOnlineInterviewCandidate_positionProfileCandidateImageButton"
                                                                                    SkinID="sknBtnSearchPositinProfileCandidateIcon" runat="server" ImageAlign="Middle" ToolTip="Click here to search for candidates associated with the position profile"/>&nbsp;
                                                                                    <asp:Button ID="ScheduleOnlineInterviewCandidate_Popup_createCandidateButton" Text="New" runat="server" SkinID="sknButtonId" ToolTip="Click here to create a new candidate"/> 
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_Popup_emailHeadLabel" runat="server" Text="Email"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="ScheduleOnlineInterviewCandidate_Popup_emailTextBox" runat="server" Text="" ReadOnly="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_scheduleDatePopLabel" runat="server" Text="Online Interview Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class='mandatory'>*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 40%">
                                                                                        <asp:TextBox ID="ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox" runat="server" MaxLength="10"
                                                                                            AutoCompleteType="None" Text=""></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ScheduleOnlineInterviewCandidate_Popup_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:MaskedEditExtender ID="ScheduleOnlineInterviewCandidate_MaskedEditExtender" runat="server"
                                                                                TargetControlID="ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox" Mask="99/99/9999"
                                                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="ScheduleOnlineInterviewCandidate_MaskedEditValidator" runat="server"
                                                                                ControlExtender="ScheduleOnlineInterviewCandidate_MaskedEditExtender" ControlToValidate="ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox"
                                                                                EmptyValueMessage="Expiry Date is required" InvalidValueMessage="Expiry Date is invalid"
                                                                                Display="None" TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="ScheduleOnlineInterviewCandidate_customCalendarExtender" runat="server"
                                                                                TargetControlID="ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox" CssClass="MyCalendar"
                                                                                Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="ScheduleOnlineInterviewCandidate_Popup_calendarImageButton" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_sessionExpiryDateHeadLabel" runat="server" Text="Session Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_Popup_sessionExpiryDateValueLabel" runat="server"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_5">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="left" style="width: 20px; padding-right: 5px">
                                                                <asp:HiddenField ID="ScheduleOnlineInterviewCandidate_candidateIdHiddenField" runat="server" />
                                                                <asp:Button ID="ScheduleOnlineInterviewCandidate_saveButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Save" OnClick="ScheduleOnlineInterviewCandidate_saveButton_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="ScheduleOnlineInterviewCandidate_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                                                                    runat="server" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleOnlineInterviewCandidate_scheduleModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleOnlineInterviewCandidate_schedulePanel" TargetControlID="ScheduleOnlineInterviewCandidate_hiddenButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleOnlineInterviewCandidate_hiddenViewSchedulePanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_question_detail">
                                        <div style="display: none;">
                                            <asp:Button ID="ScheduleOnlineInterviewCandidate_hiddenViewScheduleButton" runat="server" Text="Hidden" /></div>
                                        <uc1:TestScheduleControl ID="ScheduleOnlineInterviewCandidate_viewTestSchedule" runat="server" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleOnlineInterviewCandidate_viewScheduleModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleOnlineInterviewCandidate_hiddenViewSchedulePanel" TargetControlID="ScheduleOnlineInterviewCandidate_hiddenViewScheduleButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleOnlineInterviewCandidate_unscheduleCandidatePanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_confirm_remove">
                                        <div style="display: none;">
                                            <asp:Button ID="ScheduleOnlineInterviewCandidate_hiddenUnscheduleButton" runat="server" Text="Hidden" /></div>
                                        <uc2:ConfirmMsgControl ID="ScheduleOnlineInterviewCandidate_inactiveConfirmMsgControl" runat="server"
                                            Title="Unschedule" Type="YesNo" Message="Are you sure do you want to cancel the scheduled online interview session?"
                                            OnOkClick="ScheduleOnlineInterviewCandidate_OkClick" />
                                        /></asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleOnlineInterviewCandidate_unscheduleCandidateModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleOnlineInterviewCandidate_unscheduleCandidatePanel" TargetControlID="ScheduleOnlineInterviewCandidate_hiddenUnscheduleButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleOnlineInterviewCandidate_cancelTestPanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_cancel_session">
                                        <div style="display: none">
                                            <asp:Button ID="ScheduleOnlineInterviewCandidate_cancelReasonHiddenButton" runat="server" Text="Hidden" />
                                        </div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Label ID="ScheduleOnlineInterviewCandidate_cancelReasonLiteral" runat="server" Text="Cancelled Online Interview Session Reason"></asp:Label>
                                                            </td>
                                                            <td style="width: 25%" align="right">
                                                                <asp:ImageButton ID="ScheduleOnlineInterviewCandidate_cancelReasonCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div runat="server" id="ScheduleOnlineInterviewCandidate_cancelTestReasonDiv"  class="label_field_text" style="height: 70px;
                                                                                width: 400px; overflow: auto;">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_5">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:LinkButton ID="ScheduleOnlineInterviewCandidate_cancelReasonCancellationButton" runat="server"
                                                                    Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleOnlineInterviewCandidate_cancelSessionModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleOnlineInterviewCandidate_cancelTestPanel" TargetControlID="ScheduleOnlineInterviewCandidate_cancelReasonHiddenButton"
                                        CancelControlID="ScheduleOnlineInterviewCandidate_cancelReasonCancellationButton" BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleOnlineInterviewCandidate_RetakeValidationPopupPanel" runat="server" Style="display: none;
                                        height: 202px;" CssClass="popupcontrol_confirm">
                                        <div id="ScheduleOnlineInterviewCandidate_RetakeValidationDiv" style="display: none">
                                            <asp:Button ID="ScheduleOnlineInterviewCandidate_RetakeValidation_hiddenButton" runat="server" />
                                        </div>
                                        <uc2:ConfirmMsgControl ID="ScheduleOnlineInterviewCandidate_RetakeValidation_ConfirmMsgControl"
                                            runat="server" Title="Request to Retake" Type="OkSmall" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleOnlineInterviewCandidate_RetakeValidation_ModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleOnlineInterviewCandidate_RetakeValidationPopupPanel"
                                        TargetControlID="ScheduleOnlineInterviewCandidate_RetakeValidation_hiddenButton" BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="ScheduleOnlineInterviewCandidate_isMaximizedHiddenField" runat="server" />
                                    <asp:HiddenField ID="ScheduleOnlineInterviewCandidate_candidateNameHiddenField" runat="server" />
                                    <asp:HiddenField ID="ScheduleOnlineInterviewCandidate_reScheduleCandidateIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="ScheduleOnlineInterviewCandidate_sessionExpiryDateHiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
