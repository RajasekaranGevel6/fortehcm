﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;  
using Forte.HCM.UI.CommonControls;
using System.Web.UI.HtmlControls;


namespace Forte.HCM.UI.OnlineInterview
{
    public partial class OnlineInterviewAssessor : PageBase
    {
        #region Private Variables
        /// <summary>
        /// A <see cref="System.String"/> constant that holds the 
        /// view state name of the sort direction
        /// </summary>
        private const string SORTDIRECTION_VIEWSTATE = "GRIDVIEWSORT_DIRECTION";

        /// <summary>
        ///  A <see cref="System.String"/> constant that holds the 
        /// view state name of the sort expression
        /// </summary>
        private const string SORTEXPRESSION_VIEWSTATE = "GRIDVIEWSORT_EXPRESSION";
        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion

        List<AssessorTimeSlotDetail> assessorList = null;

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {     
            Master.SetPageCaption("Online Interview Assessor Assignment");

            OnlineInterview_topSuccessLabel.Text = string.Empty;
            OnlineInterview_bottomSuccessLabel.Text = string.Empty;

            OnlineInterview_topErrorLabel.Text = string.Empty;
            OnlineInterview_bottomErrorLabel.Text = string.Empty;

            try
            {
                if (!IsPostBack)
                {

                    DateTime currentDate = DateTime.Now; 

                    Session["Tenant_id"] = base.tenantID;

                    Session["USER_SELECTED_DATES"] = string.Empty;

                    ViewState["INDIVIDUAL_REQUESTED_DATES"] = string.Empty;

                    OnlineInterview_addAssessor_Button.Attributes.Add("onclick", "return ShowSearchAssessor('" + OnlineInterview_addedAssessorIdHiddenField.ClientID +
                       "','" + OnlineInterview_assessorNameTextBox.ClientID + "','" +
                       OnlineInterview_refreshGridButton.ClientID + "','Y');");

                    // Keep the date in session.
                    Session["MY_AVAILABILITY_DATE"] = currentDate;

                    OnlineInterview_selectedDateLabel.Text = currentDate.ToString("dddd, dd MMMM yyyy");

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "ACTIVE_POSITION_ASSIGNMENTS"; 

                    //InterviewList();
                    GetInterviewAssessorList(Request.QueryString["interviewkey"].ToString(), 1, base.GridPageSize);
                    // Load all recruiters
                    LoadRecruiterDetail("RA", 1);
                    ViewState["ASSESSOR_LOAD_OPTION"] = "RA";
                    SetAddAssessorVisibility();

                    // Load all corporate users 
                    LoadUsersValues();
                }

                // Subscribe to assessor list paging event.
                OnlineInterview_assessorList_assessorsPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(
                    OnlineInterview_assessorList_assessorsPageNavigator_PageNumberClick);
                 
            }
            catch (Exception exp)
            { 
                base.ShowMessage(OnlineInterview_topErrorLabel, 
                    OnlineInterview_bottomErrorLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        protected void OnlineInterview_assessorDefaultLoad_Button_Click(object sender, EventArgs e)
        {
            try 
            {
                //Page_Load(new object(), new EventArgs());
               
                GetInterviewAssessorList(Request.QueryString["interviewkey"].ToString(), 1, base.GridPageSize);
                // Load all recruiters
                if (ViewState["ASSESSOR_LOAD_OPTION"] != null)
                    LoadRecruiterDetail(ViewState["ASSESSOR_LOAD_OPTION"].ToString(), 1); 
                    //LoadRecruiterDetail("RC", 1);

                SetAddAssessorVisibility();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_bottomErrorLabel,
                    OnlineInterview_topErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterview_assessorList_GridView_viewAssessorRadioButton_CheckedChanged
            (object sender, EventArgs e)
        {

            try 
            {
                OnlineInterview_initiatedBy_GridView.DataSource = null;
                OnlineInterview_initiatedBy_GridView.DataBind();
                OnlineInterview_assessorList_Calendar.VisibleDate = DateTime.Now;

                ViewState["ASSESSOR_VACATION_DATE"] = string.Empty;
                ViewState["ASSESSOR_AVAILABLE_DATES"] = string.Empty;

                OnlineInterview_selectedDateLabel.Text = DateTime.Today.ToString("dddd, dd MMMM yyyy");

                foreach (GridViewRow row in OnlineInterview_assessorList_GridView.Rows)
                {
                    RadioButton OnlineInterview_assessorList_GridView_viewAssessorRadioButton =
                   (RadioButton)row.FindControl("OnlineInterview_assessorList_GridView_viewAssessorRadioButton");

                    OnlineInterview_assessorList_GridView_viewAssessorRadioButton.Checked = false;
                }

                //Set the clicked row
                RadioButton radioButton = (RadioButton)sender;
                GridViewRow currentRow = (GridViewRow)radioButton.NamingContainer;

                RadioButton OnlineInterview_assessorList_GridView_viewAssessorRadioButtonCheck =
                    (RadioButton)currentRow.FindControl("OnlineInterview_assessorList_GridView_viewAssessorRadioButton");
                OnlineInterview_assessorList_GridView_viewAssessorRadioButtonCheck.Checked = true;

                OnlineInterview_assessorList_Calendar.SelectedDates.Clear();

                SetAddAssessorVisibility();

              HiddenField OnlineInterview_assessorList_GridView_userIDHiddenField =
                  (HiddenField)currentRow.FindControl("OnlineInterview_assessorList_GridView_userIDHiddenField");

                
                if (Utility.IsNullOrEmpty(OnlineInterview_assessorList_GridView_userIDHiddenField.Value)) return;

                int monthId = OnlineInterview_assessorList_Calendar.VisibleDate.Month; 

                ViewState["CURRENT_ASSESSOR_ID"] = OnlineInterview_assessorList_GridView_userIDHiddenField.Value; 

                GetAssessorAvailableDates(Convert.ToInt32( ViewState["CURRENT_ASSESSOR_ID"]),monthId);
                
                /* List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                   new OnlineInterviewAssessorBLManager().GetAssessorAvailableDates(Convert.ToInt32(OnlineInterview_assessorList_GridView_userIDHiddenField.Value),
                   monthId, 1, base.GridPageSize, out totalRecords);

                if (Utility.IsNullOrEmpty(assessorAvailabilityDates)) return;

                ViewState["ASSESSOR_AVAILABLE_DATES"] = assessorAvailabilityDates; 

                if (Utility.IsNullOrEmpty(assessorAvailabilityDates[0].VacationDates)) return;

                ViewState["ASSESSOR_VACATION_DATE"] = assessorAvailabilityDates[0].VacationDates.ToString();*/

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_bottomErrorLabel,
                    OnlineInterview_topErrorLabel, exp.Message);
            }
        }

        private void OnlineInterview_assessorList_assessorsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        { } 

        protected void OnlineInterview_nextDate_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["MY_AVAILABILITY_DATE"] == null) 
                    return; 

                DateTime actualDate = (DateTime)Session["MY_AVAILABILITY_DATE"];
                 

                // Construct new date.
                DateTime newDate = ((DateTime)Session["MY_AVAILABILITY_DATE"]).AddDays(1);

                // Keep the date in session.
                Session["MY_AVAILABILITY_DATE"] = newDate;

                OnlineInterview_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");


                DateTime dtime = Convert.ToDateTime(OnlineInterview_selectedDateLabel.Text); 

                GetAsssessorInterviews(Convert.ToInt32(ViewState["CURRENT_ASSESSOR_ID"]),dtime.Month, dtime);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                /*base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);*/
            }
        }

        protected void OnlineInterview_previousDate_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["MY_AVAILABILITY_DATE"] == null)
                    return;

                DateTime actualDate = (DateTime)Session["MY_AVAILABILITY_DATE"];


                // Construct new date.
                DateTime newDate = ((DateTime)Session["MY_AVAILABILITY_DATE"]).AddDays(-1);

                // Keep the date in session.
                Session["MY_AVAILABILITY_DATE"] = newDate;

                OnlineInterview_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");


                DateTime dtime = Convert.ToDateTime(OnlineInterview_selectedDateLabel.Text);

                GetAsssessorInterviews(Convert.ToInt32(ViewState["CURRENT_ASSESSOR_ID"]), dtime.Month, dtime);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                /* base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);*/
            }
        } 

        protected void OnlineInterview_assessorOption_RadioButtonList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            { 
                string selectedOptionValue=OnlineInterview_assessorOption_RadioButtonList.SelectedValue;
                ViewState["ASSESSOR_LOAD_OPTION"] = selectedOptionValue;
                // Load all recruiters
                LoadRecruiterDetail(selectedOptionValue, 1);

                GetInterviewAssessorList(Request.QueryString["interviewkey"].ToString(), 1, base.GridPageSize);

                SetAddAssessorVisibility();

                OnlineInterview_initiatedBy_GridView.DataSource = null;
                OnlineInterview_initiatedBy_GridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_bottomErrorLabel,
                    OnlineInterview_topErrorLabel, exp.Message);
            }
        }

            protected void OnlineInterview_addNewUser_Button_Click
            (object sender, EventArgs e)
        {
            try 
            {
                OnlineInterview_addNewUserModalPopupExtender.Show();
            } 
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                    OnlineInterview_bottomErrorLabel, exp.Message);
            }               
        }

        protected void OnlineInterview_refreshGridButton_Click(object sender, EventArgs e)
        {
            try 
            {
                string s = OnlineInterview_addedAssessorIdHiddenField.Value;

                AddingAssessor(Convert.ToInt32(OnlineInterview_addedAssessorIdHiddenField.Value),
                    Request.QueryString["interviewkey"].ToString());

                //RebindGridView();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                    OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterview_assessorList_GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try 
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton OnlineInterview_assessorList_GridView_assessorNameLinkButton = (LinkButton)
                      e.Row.FindControl("OnlineInterview_assessorList_GridView_assessorNameLinkButton");

                    int recruiterID = 0;
                    HiddenField OnlineInterview_assessorList_GridView_userIDHiddenField = (HiddenField)
                        e.Row.FindControl("OnlineInterview_assessorList_GridView_userIDHiddenField");

                    if (!Utility.IsNullOrEmpty(OnlineInterview_assessorList_GridView_userIDHiddenField.Value))
                        recruiterID = Convert.ToInt32(OnlineInterview_assessorList_GridView_userIDHiddenField.Value);

                    // Add click handler.
                    OnlineInterview_assessorList_GridView_assessorNameLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowAssessorProfile('" + recruiterID + "');");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                   OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterview_assessorList_GridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(Request.QueryString["interviewkey"])) return;

                string interviewKey = Request.QueryString["interviewkey"].ToString();

                if (e.CommandName.Trim() == "AssignAssessor")
                {
                    AddingAssessor(Convert.ToInt32(e.CommandArgument), interviewKey);

                    /*AssessorTimeSlotDetail assessorTimeSlot = new AssessorTimeSlotDetail();
                    assessorTimeSlot.AssessorID = Convert.ToInt32 (e.CommandArgument);
                    assessorTimeSlot.CandidateSessionKey = interviewKey;
                    assessorTimeSlot.InitiatedBy = base.userID; 

                    new OnlineInterviewAssessorBLManager().InsertOnlineInterviewAssessor(assessorTimeSlot);

                    GetInterviewAssessorList(interviewKey, 1, base.GridPageSize);*/

                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;

                    ImageButton OnlineInterview_assessorList_GridView_addAssessorImageButton =
                        (ImageButton)row.FindControl("OnlineInterview_assessorList_GridView_addAssessorImageButton");

                    OnlineInterview_assessorList_GridView_addAssessorImageButton.Visible = false; 

                }
            }
            catch (Exception exp) 
            {   
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                   OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }
         
        protected void OnlineInterview_assessorList_DataList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try  
            { 

                if (e.CommandName == "DeleteAssessor")
                {
                    ViewState["ASSESSOR_ID"] = e.CommandArgument.ToString(); 
                    OnlineInterview_deleteAssessorConfirmMsgControl.Message = "Are you sure to delete the assessor?";
                    OnlineInterview_deleteAssessorConfirmMsgControl.Type = MessageBoxType.YesNo;
                    OnlineInterview_deleteAssessorConfirmMsgControl.Title = "Confirmation";

                    OnlineInterview_deleteAssessorModalPopupExtender.Show();
                } 
                else if (e.CommandName == "RequestDate")
                {
                    HiddenField OnlineInterview_assessorList_DataList_assessorGenIdHiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterview_assessorList_DataList_assessorGenIdHiddenField");

                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                                           "ASS_REQDATE", "<script language='javascript'>AesssorRequest('" + e.CommandArgument.ToString() + "','" + OnlineInterview_assessorDefaultLoad_Button.ClientID + "','"+ OnlineInterview_assessorList_DataList_assessorGenIdHiddenField.Value
                                           +"');</script>", false);  
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                   OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterview_deleteAssessorConfirmMsgControl_okClick
      (object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(ViewState["ASSESSOR_ID"])) return;

                new OnlineInterviewAssessorBLManager().DeleteOnlineInterviewAssessor(
                        Convert.ToInt32(ViewState["ASSESSOR_ID"]), Request.QueryString["interviewkey"].ToString());

                GetInterviewAssessorList(Request.QueryString["interviewkey"].ToString(), 1, base.GridPageSize);

                base.ShowMessage(OnlineInterview_topSuccessLabel,
                 OnlineInterview_bottomSuccessLabel, "Deleted successfully");

                SetAddAssessorVisibility(); 

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                  OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }
         
        protected void OnlineInterview_assessorList_DataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try 
            {
                int totalRecords=0;
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                     System.Web.UI.WebControls.Calendar cla = (System.Web.UI.WebControls.Calendar)e.Item.
                      FindControl("OnlineInterview_assessorList_DataList_Calendar");

                     cla.VisibleDate  = DateTime.Now;

                    HiddenField  assessorGenIdHiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterview_assessorList_DataList_assessorGenIdHiddenField");

                    HiddenField assessorIdHiddenField = (HiddenField)e.Item.
                        FindControl("OnlineInterview_assessorList_DataList_assessorIdHiddenField");

                    HyperLink OnlineInterview_assessorList_DataList_assessorName_HyperLink =
                        (HyperLink)e.Item.FindControl("OnlineInterview_assessorList_DataList_assessorName_HyperLink"); 
                     
                    OnlineInterview_assessorList_DataList_assessorName_HyperLink.NavigateUrl =
                        "../Assessor/MyAvailability.aspx?m=5&s=1&parentpage=ONLINE_ASS&assessorid=" + assessorIdHiddenField.Value + ""+
                    "&interviewid=" + Request.QueryString["interviewkey"].ToString();

                    GridView gridView = (GridView)e.Item.FindControl("OnlineInterview_assessorTimeSlot_GridView");

                    if (gridView == null) return;
                    DataListItem listItem = e.Item;
                    AssessorTimeSlotDetail assessorDates = (AssessorTimeSlotDetail)listItem.DataItem;

                    ViewState["SELECTED_ASSESSOR_CALENDAR"] = cla.VisibleDate.Month;

                    List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                        new OnlineInterviewAssessorBLManager().GetOnlineAssessorTimeSlotDates(assessorDates.AssessorID, cla.VisibleDate.Month, 1, base.GridPageSize, out totalRecords);

                    gridView.DataSource = null;
                    gridView.DataBind();
                      
                    if (assessorAvailabilityDates != null)
                    {
                        gridView.DataSource = assessorAvailabilityDates;
                        gridView.DataBind(); 

                        foreach (AssessorTimeSlotDetail ass in assessorAvailabilityDates)
                        {  
                            cla.SelectedDates.Add(ass.AvailabilityDate); 

                            if (ViewState[assessorGenIdHiddenField.Value + "_DATES"] == null)
                                ViewState[assessorGenIdHiddenField.Value + "_DATES"] =Convert.ToString(ass.AvailabilityDate ) + ",";
                            else
                                ViewState[assessorGenIdHiddenField.Value + "_DATES"] =ViewState[assessorGenIdHiddenField.Value + "_DATES"].ToString()
                                    + "," + Convert.ToString(ass.AvailabilityDate);
                        }  
                    } 
                     
                    List<SkillDetail> skillDetail = new OnlineInterviewBLManager().GetSkillByOnlineInterviewKey(Request.QueryString["interviewkey"].ToString()); 

                    CheckBoxList OnlineInterview_skill_CheckBoxList = (CheckBoxList)e.Item.FindControl("OnlineInterview_skill_CheckBoxList");

                    if (OnlineInterview_skill_CheckBoxList == null) return;

                    OnlineInterview_skill_CheckBoxList.DataSource = null;
                    OnlineInterview_skill_CheckBoxList.DataBind();

                    if (skillDetail == null) return;

                    OnlineInterview_skill_CheckBoxList.DataSource = skillDetail;
                    OnlineInterview_skill_CheckBoxList.DataBind();

                    CheckAssessorSkill(Convert.ToInt32(assessorGenIdHiddenField.Value), OnlineInterview_skill_CheckBoxList); 
                    
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                   OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }
       
        protected void OnlineInterview_saveButton_Click(object sender, EventArgs e)
        {
            try 
            {

                if ( OnlineInterview_assessorList_DataList  ==null || OnlineInterview_assessorList_DataList.Items.Count == 0) return;

                //Getting skill gridview
                foreach (DataListItem dataLstItem in OnlineInterview_assessorList_DataList.Items)
                {
                    CheckBoxList OnlineInterview_skill_CheckBoxList = (CheckBoxList)dataLstItem.FindControl("OnlineInterview_skill_CheckBoxList");

                   // GridView OnlineInterview_skill_GridView = (GridView)dataLstItem.FindControl("OnlineInterview_skill_GridView");
                     
                    //Getting assessor table auto gen id
                    HiddenField OnlineInterview_assessorList_DataList_assessorGenIdHiddenField =
                        (HiddenField)dataLstItem.FindControl("OnlineInterview_assessorList_DataList_assessorGenIdHiddenField");

                    //Getting assessor id
                    HiddenField OnlineInterview_assessorList_DataList_assessorIdHiddenField =
                        (HiddenField)dataLstItem.FindControl("OnlineInterview_assessorList_DataList_assessorIdHiddenField");

                    if (OnlineInterview_skill_CheckBoxList == null) break;

                    //if (OnlineInterview_skill_GridView == null) break;

                    //Getting checked skills in skill gridview
                    for (int checkedItem = 0; checkedItem < OnlineInterview_skill_CheckBoxList.Items.Count; checkedItem++)
                    { 
                        if (OnlineInterview_skill_CheckBoxList.Items[checkedItem].Selected)
                        {
                            AssessorTimeSlotDetail assessorSkill = new AssessorTimeSlotDetail();

                            assessorSkill.ID = Convert.ToInt32(OnlineInterview_assessorList_DataList_assessorGenIdHiddenField.Value);
                            assessorSkill.SkillID = Convert.ToInt32(OnlineInterview_skill_CheckBoxList.Items[checkedItem].Value);
                            assessorSkill.InitiatedBy = base.userID;

                            new OnlineInterviewAssessorBLManager().InsertOnlineInterviewAssessorSkill(assessorSkill); 
                        }
                    }

                    Response.Redirect("~/OnlineInterview/OnlineInterviewSchduleCandidate.aspx?m=3&s=0&interviewkey=" + Request.QueryString["interviewkey"].ToString() +"&parentpage=S_OITSN", false);
                } 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                   OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }
         
        protected void OnlineInterview_assessorList_Calendar_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Session["MY_AVAILABILITY_DATE"] = OnlineInterview_assessorList_Calendar.SelectedDate;

                OnlineInterview_selectedDateLabel.Text = OnlineInterview_assessorList_Calendar.SelectedDate.ToString("dddd, dd MMMM yyyy");

                GetAsssessorInterviews(Convert.ToInt32(ViewState["CURRENT_ASSESSOR_ID"]),OnlineInterview_assessorList_Calendar.VisibleDate.Month,
                    OnlineInterview_assessorList_Calendar.SelectedDate);                 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                   OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterview_assessorList_Calendar_DayRender(object sender, DayRenderEventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(ViewState["ASSESSOR_AVAILABLE_DATES"]))
                {
                    List<AssessorTimeSlotDetail> availabilityDates = null;

                    availabilityDates = ViewState["ASSESSOR_AVAILABLE_DATES"] as List<AssessorTimeSlotDetail>;

                    foreach (AssessorTimeSlotDetail ass in availabilityDates)
                    {
                        if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus =="Approved" && ass.Scheduled =="S" )
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(153,221,150);
                            e.Cell.ToolTip = "Scheduled date";
                            //89, 193, 238
                        }
                        else if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Approved"  && ass.Scheduled == "N")
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(112, 146, 190);
                            e.Cell.ToolTip = "Request Approved but not yet scheduled";
                        }
                        else if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Initiated"  && ass.Scheduled == "N")
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(255, 212, 85);
                            e.Cell.ToolTip = "Request initiated";
                        }
                    }
                }


                if (!Utility.IsNullOrEmpty(ViewState["ASSESSOR_VACATION_DATE"]))
                {

                    string[] vacationDates = ViewState["ASSESSOR_VACATION_DATE"].ToString().Split(',');

                    if (vacationDates == null) return;

                    foreach (string vDate in vacationDates)
                    {
                        //if (e.Day.Date == DateTime.Parse(vDate, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None))
                        if (e.Day.Date.ToShortDateString() ==  vDate )
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(242, 147, 120);
                            e.Day.IsSelectable = false;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_bottomErrorLabel,
                    OnlineInterview_topErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterview_assessorList_DataList_Calendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            //Set the clicked row
            Calendar calendarControl = (Calendar)sender;
            DataListItem currentItem = (DataListItem)calendarControl.NamingContainer;
            if (currentItem == null) return;

            GridView gridView = (GridView)currentItem.FindControl("OnlineInterview_assessorTimeSlot_GridView");

            if (gridView == null) return;

            HiddenField assessorIdHiddenField =
               (HiddenField)currentItem.FindControl("OnlineInterview_assessorList_DataList_assessorIdHiddenField");

            HiddenField assessorGenIdHiddenField =
               (HiddenField)currentItem.FindControl("OnlineInterview_assessorList_DataList_assessorGenIdHiddenField");

            int totalRecords = 0;

            List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                new OnlineInterviewAssessorBLManager().GetOnlineAssessorTimeSlotDates(Convert.ToInt32(assessorIdHiddenField.Value),
                e.NewDate.Month, 1, base.GridPageSize, out totalRecords);

            gridView.DataSource = null;
            gridView.DataBind();

            if (assessorAvailabilityDates != null)
            {
                gridView.DataSource = assessorAvailabilityDates;
                gridView.DataBind();

                foreach (AssessorTimeSlotDetail ass in assessorAvailabilityDates)
                {
                    calendarControl.SelectedDates.Add(ass.AvailabilityDate);
                    if (ViewState[assessorGenIdHiddenField.Value + "_DATES"] == null)
                        ViewState[assessorGenIdHiddenField.Value + "_DATES"] = Convert.ToString(ass.AvailabilityDate) + ",";
                    else
                        ViewState[assessorGenIdHiddenField.Value + "_DATES"] = ViewState[assessorGenIdHiddenField.Value + "_DATES"].ToString()
                            + "," + Convert.ToString(ass.AvailabilityDate);
                }
            }
        }

        protected void OnlineInterview_assessorList_Calendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            try
            {
                GetAssessorAvailableDates(Convert.ToInt32(ViewState["CURRENT_ASSESSOR_ID"]),
                    OnlineInterview_assessorList_Calendar.VisibleDate.Month);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                       OnlineInterview_bottomErrorLabel, exp.Message);
            }

        } 

        protected void OnlineInterview_assessorTimeSlot_GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HtmlGenericControl Assessor_timeDetailsDiv = (HtmlGenericControl)e.Row.FindControl("Assessor_timeDetailsDiv");

                    HtmlGenericControl Assessor_defaultTimeDetailsDiv = (HtmlGenericControl)e.Row.FindControl("Assessor_defaultTimeDetailsDiv");

                    HiddenField OnlineInterview_assessorTimeSlot_GridView_AvailableDate_GenIdHiddenField =
                        (HiddenField)e.Row.FindControl("OnlineInterview_assessorTimeSlot_GridView_AvailableDate_GenIdHiddenField");

                    GridView AssessorTimeSlot_PopupGridView = (GridView)e.Row.FindControl("AssessorTimeSlot_PopupGridView");

                    LinkButton OnlineInterview_assessorTimeSlot_GridView_AvailableDateLinkButton =
                        (LinkButton)e.Row.FindControl("OnlineInterview_assessorTimeSlot_GridView_AvailableDateLinkButton");

                    HtmlAnchor AssessorTimeSlot_Popup_focusDownLink = (HtmlAnchor)e.Row.FindControl("AssessorTimeSlot_Popup_focusDownLink");

                    if (Assessor_timeDetailsDiv == null || OnlineInterview_assessorTimeSlot_GridView_AvailableDate_GenIdHiddenField == null ||
                       AssessorTimeSlot_PopupGridView == null || OnlineInterview_assessorTimeSlot_GridView_AvailableDateLinkButton == null
                        || AssessorTimeSlot_Popup_focusDownLink==null) return;

                    List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                           new OnlineInterviewAssessorBLManager().GetAssessorTimeSlots(
                           Convert.ToInt32(OnlineInterview_assessorTimeSlot_GridView_AvailableDate_GenIdHiddenField.Value));

                    if (assessorAvailabilityDates == null || assessorAvailabilityDates.Count == 0)
                    {
                        OnlineInterview_assessorTimeSlot_GridView_AvailableDateLinkButton.Attributes.Add("onclick", "return DefaultDivExpandCollapse('" + Assessor_defaultTimeDetailsDiv.ClientID + "')");
                        return;
                    }

                    AssessorTimeSlot_PopupGridView.DataSource = assessorAvailabilityDates;
                    AssessorTimeSlot_PopupGridView.DataBind();

                    OnlineInterview_assessorTimeSlot_GridView_AvailableDateLinkButton.Attributes.Add("onclick", "return divexpandcollapse('" + Assessor_timeDetailsDiv.ClientID + "')");

                    /*OnlineInterview_assessorTimeSlot_GridView_AvailableDateLinkButton.Attributes.Add("onclick", "return AssessorTimeDetails('" +
                        Assessor_timeDetailsDiv.ClientID + "','" + AssessorTimeSlot_Popup_focusDownLink.ClientID + "')"); */
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                       OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void OnlineInterview_assessorList_GridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                string filterBy = OnlineInterview_assessorOption_RadioButtonList.SelectedValue;

                //PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "none";

                // Reset and show records for first page.
                OnlineInterview_assessorList_assessorsPageNavigator.Reset();

                LoadRecruiterDetail(filterBy, 1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_topErrorLabel,
                       OnlineInterview_bottomErrorLabel, exp.Message);
            }
        }

        #endregion

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Private Methods 

        /// <summary>
        /// Thisfunction adds assessor to particular interview key aganist
        /// </summary>
        /// <param name="assessorId">
        /// <see cref="System.Int32"/>
        /// </param>
        /// <param name="interviewKey">
        /// <see cref="System.String"/>
        /// </param>
        private void AddingAssessor(int assessorId,string interviewKey)
        {
            AssessorTimeSlotDetail assessorTimeSlot = new AssessorTimeSlotDetail();
            assessorTimeSlot.AssessorID = assessorId;
            assessorTimeSlot.CandidateSessionKey = interviewKey;
            assessorTimeSlot.InitiatedBy = base.userID;

            new OnlineInterviewAssessorBLManager().InsertOnlineInterviewAssessor(assessorTimeSlot);

            GetInterviewAssessorList(interviewKey, 1, base.GridPageSize);
        }

        private void GetAsssessorInterviews(int assessorId,int monthId, DateTime requestDate)
        {
            OnlineInterview_initiatedBy_GridView.DataSource = null;
            OnlineInterview_initiatedBy_GridView.DataBind();

            List<InterviewDetail> interviewDetailList = new OnlineInterviewAssessorBLManager().
                   GetAssessorInterviewDetail(assessorId, requestDate);

            if (interviewDetailList != null)
            {
                foreach (InterviewDetail idetail in interviewDetailList)
                {
                    if (idetail.TimeSlotTextFrom == null)
                        idetail.TimeSlotTextFrom = "Any Time";
                }

                OnlineInterview_initiatedBy_GridView.DataSource = interviewDetailList;
                OnlineInterview_initiatedBy_GridView.DataBind();
            }

            int totalRecords = 0;

            List<AssessorTimeSlotDetail> assessorAvailabilityDates =
              new OnlineInterviewAssessorBLManager().GetAssessorAvailableDates(assessorId,monthId, 1, base.GridPageSize, out totalRecords);

            if (Utility.IsNullOrEmpty(assessorAvailabilityDates)) return;

            foreach (AssessorTimeSlotDetail ass in assessorAvailabilityDates)
            {
                OnlineInterview_assessorList_Calendar.SelectedDates.Add(ass.AvailabilityDate);
            }
        }

        private void CheckAssessorSkill(int assessorGenId, CheckBoxList checkedLstBox)
        {
            List<int> skillList = new OnlineInterviewAssessorBLManager().
                GetOnlineInterviewAssessorSkill(assessorGenId);

            if (skillList == null) return;

              //Getting checked skills in skill gridview
            for (int i = 0; i < checkedLstBox.Items.Count; i++)
            {
                if (skillList.Contains(Convert.ToInt32(checkedLstBox.Items[i].Value)))
                    checkedLstBox.Items[i].Selected = true;
            }  
        }

        private void GetInterviewAssessorList(string interviewkey,int pageNo,int pageSize)
        {
            int totalRecords = 0;

              assessorList =  new OnlineInterviewAssessorBLManager().
                  GetOnlineInterviewAssessorList(interviewkey,pageNo,pageSize,out totalRecords); 

            OnlineInterview_assessorList_DataList.DataSource = null;
            OnlineInterview_assessorList_DataList.DataBind();
            OnlineInterview_assessorList_DataList.DataSource = assessorList;
            OnlineInterview_assessorList_DataList.DataBind(); 
        }

        private void SetAddAssessorVisibility()
        {
            //if (assessorList == null) return;
             
            List<AssessorTimeSlotDetail> assslist = (List<AssessorTimeSlotDetail>)OnlineInterview_assessorList_DataList.DataSource;

            if (assslist == null) return;

            if (assslist.Count == 0) return;

            foreach (GridViewRow row in OnlineInterview_assessorList_GridView.Rows)
            {
                HiddenField OnlineInterview_assessorList_GridView_userIDHiddenField =
                    (HiddenField)row.FindControl("OnlineInterview_assessorList_GridView_userIDHiddenField"); 

                ImageButton OnlineInterview_assessorList_GridView_addAssessorImageButton =
                       (ImageButton)row.FindControl("OnlineInterview_assessorList_GridView_addAssessorImageButton");

                if (OnlineInterview_assessorList_GridView_userIDHiddenField == null ||
                    OnlineInterview_assessorList_GridView_addAssessorImageButton == null) break;

                foreach (AssessorTimeSlotDetail assessorDetail in assslist)
                {
                    if(assessorDetail.AssessorID == Convert.ToInt32(OnlineInterview_assessorList_GridView_userIDHiddenField.Value))
                        OnlineInterview_assessorList_GridView_addAssessorImageButton.Visible = false;  
                } 
            }

            OnlineInterview_assessorList_UpdatePanel.Update();

        }

        /// <summary>
        /// Method to get the recruiter list against tenant id
        /// </summary>
        private void LoadRecruiterDetail(string filterBy, int pageNumber)
        {
            int totalRecords = 0; 
 
            List<RecruiterDetail> recruiterDetails = null;

            if (filterBy == "RS")
            {
                recruiterDetails = new List<RecruiterDetail>();
                recruiterDetails = new OnlineInterviewBLManager().
                    GetOnlineInterviewRecruiterSkill(base.tenantID, Request.QueryString["interviewkey"].ToString(),
                    pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"], out totalRecords);
            }
            else if (filterBy == "RC")
            {
                recruiterDetails = new List<RecruiterDetail>();
                recruiterDetails = new OnlineInterviewBLManager().
                    GetOnlineInterviewRecruiterClient(base.tenantID, Request.QueryString["interviewkey"].ToString(),
                    pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"], out totalRecords);
            } 
            else
            {
                recruiterDetails = new List<RecruiterDetail>();
                recruiterDetails = new PositionProfileBLManager().GetAllRecruiter(base.tenantID,
                    pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"], out totalRecords);
            }
            
            if (recruiterDetails != null && recruiterDetails.Count > 0)
            {
                OnlineInterview_assessorList_assessorsPageNavigator.Visible = true;
                OnlineInterview_assessorList_assessorsPageNavigator.TotalRecords = totalRecords;
                OnlineInterview_assessorList_assessorsPageNavigator.PageSize = base.GridPageSize;

                OnlineInterview_assessorList_GridView.DataSource = recruiterDetails;
                OnlineInterview_assessorList_GridView.DataBind();
            }
            else
            {
                OnlineInterview_assessorList_assessorsPageNavigator.Visible = false;
                OnlineInterview_assessorList_GridView.DataSource = null;
                OnlineInterview_assessorList_GridView.DataBind();
            }
        }

        private void LoadUsersValues()
        {
            ViewState[SORTEXPRESSION_VIEWSTATE] = "EMAIL";
            ViewState[SORTDIRECTION_VIEWSTATE] = SortDirection.Ascending;
            BindCorporateChildUsers();
        }
        /// <summary>
        /// A Method that checks and binds the logged in corporate account users
        /// </summary>
        private void BindCorporateChildUsers()
        {
            List<UserRegistrationInfo> userRegistrationInfo = null;

            try
            {
                userRegistrationInfo = new UserRegistrationBLManager().GetSelectedCorporateAccountUsers(base.tenantID,
                    ViewState[SORTEXPRESSION_VIEWSTATE].ToString(), (SortType)ViewState[SORTDIRECTION_VIEWSTATE]);

                if ((userRegistrationInfo == null || userRegistrationInfo.Count == 0) && base.isSiteAdmin == false)
                {
                    return;
                }
                string strAdminRole = Enum.GetName(typeof(SubscriptionRolesEnum), 3);
                var CheckIsAdminUser = System.Linq.Enumerable.Where(userRegistrationInfo, p => p.SubscriptionRole == strAdminRole);
                if ((CheckIsAdminUser == null || CheckIsAdminUser.ToList().Count == 0) && base.isSiteAdmin == false)
                {

                    return;
                }
                var AdminUser = CheckIsAdminUser.ToList();

                if (AdminUser.Count > 0)
                {
                    UserRegistrationInfo userRegistrationInfoNew = new UserRegistrationInfo();
                    userRegistrationInfoNew.UserID = Convert.ToInt32(AdminUser[0].TenantID);
                    userRegistrationInfoNew.Company = AdminUser[0].Company;
                    userRegistrationInfoNew.Phone = AdminUser[0].Phone;
                    userRegistrationInfoNew.Title = AdminUser[0].Title;
                    OnlineInterview_CorporateUserSignUp.InsertNewDataSource = userRegistrationInfoNew;

                    string strNormalUserRole = Enum.GetName(typeof(SubscriptionRolesEnum), 4);
                    var NormalUsers = System.Linq.Enumerable.Where(userRegistrationInfo, p => p.SubscriptionRole == strNormalUserRole);

                    OnlineInterview_addNewUser_Button.Visible = true;
                }
                else
                {
                    OnlineInterview_addNewUser_Button.Visible = false;
                }
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        private void GetVocationDates(int assessorId)
        {
            string vocationDates = string.Empty;
            vocationDates = new AssessorBLManager().GetAssessorNonAvailabilityDates(assessorId); 

            ViewState["VOCATION_DATES"] = vocationDates; 
        }

        private void GetAssessorAvailableDates(int assessorId,int month)
        {
            int totalRecords = 0;

            List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                  new OnlineInterviewAssessorBLManager().GetAssessorAvailableDates(assessorId,
                  month, 1, base.GridPageSize, out totalRecords);

            if (Utility.IsNullOrEmpty(assessorAvailabilityDates)) return;

            ViewState["ASSESSOR_AVAILABLE_DATES"] = assessorAvailabilityDates;

            if (Utility.IsNullOrEmpty(assessorAvailabilityDates[0].VacationDates)) return;

            ViewState["ASSESSOR_VACATION_DATE"] = assessorAvailabilityDates[0].VacationDates.ToString();
        }

        #endregion

        #region Public Methods
        public void ShowSignUpPopUp()
        {
            OnlineInterview_addNewUserModalPopupExtender.Show();
        }

        protected void ShowRecommendAssessor_addNewUserLinkButton_Click(object sender, EventArgs e)
        {
            OnlineInterview_addNewUserModalPopupExtender.Show();
        }

        public void ShowSuccessMessage(string Message)
        {
            base.ShowMessage(OnlineInterview_topErrorLabel,
                    OnlineInterview_bottomErrorLabel, Message);
        }

        public void RebindGridView()
        {
            // Load all recruiters
            OnlineInterview_assessorOption_RadioButtonList.SelectedIndex = 2;

            LoadRecruiterDetail("RA", 1);
        }
        public void ShowErrMessage(string Message)
        {
            base.ShowMessage(OnlineInterview_topErrorLabel,
                  OnlineInterview_bottomErrorLabel, Message);
        }
        #endregion   

        #region commented calendar code
        /*protected void OnlineInterview_assessorList_DataList_Calendar_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                //Set the clicked row
                System.Web.UI.WebControls.Calendar dataListcalendar = (System.Web.UI.WebControls.Calendar)sender;

                DataListItem currentRow = (DataListItem)dataListcalendar.NamingContainer;

                if (currentRow == null) return;

                HiddenField  assessorGenIdHiddenField =
                   (HiddenField)currentRow.FindControl("OnlineInterview_assessorList_DataList_assessorGenIdHiddenField");

                System.Web.UI.WebControls.Calendar dlCalendar = (System.Web.UI.WebControls.Calendar)currentRow.
                    FindControl("OnlineInterview_assessorList_DataList_Calendar");

                if (dlCalendar == null) return;

                if (assessorGenIdHiddenField == null) return; 

                if (ViewState[assessorGenIdHiddenField.Value + "_DATES"] != null)
                    ViewState["INDIVIDUAL_REQUESTED_DATES"] = ViewState[assessorGenIdHiddenField.Value + "_DATES"] + "," + Convert.ToString(dataListcalendar.SelectedDate);
                 else
                    ViewState["INDIVIDUAL_REQUESTED_DATES"] = dataListcalendar.SelectedDate.ToString();  
                 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_bottomErrorLabel,
                    OnlineInterview_topErrorLabel, exp.Message); 
            }
        }
      
        protected void OnlineInterview_assessorList_DataList_Calendar_DayRender(object sender, DayRenderEventArgs e)
        {
            try 
            { 
             

                if (Utility.IsNullOrEmpty(ViewState["INDIVIDUAL_REQUESTED_DATES"])) return;

                string[] requestedDates = ViewState["INDIVIDUAL_REQUESTED_DATES"].ToString().Split(',');

                if (requestedDates == null) return;

                foreach (string rDate in requestedDates)
                {
                    if (Utility.IsNullOrEmpty(rDate)) break;

                    //if (e.Day.Date.ToString() == DateTime.Parse(rDate, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None))
                    if (e.Day.Date.ToString() == rDate.Trim() )
                    {
                        e.Cell.BackColor = System.Drawing.Color.FromArgb(242, 147, 120); 
                    }
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterview_bottomErrorLabel,
                    OnlineInterview_topErrorLabel, exp.Message);
            }
        }  */
        #endregion

      
}
}