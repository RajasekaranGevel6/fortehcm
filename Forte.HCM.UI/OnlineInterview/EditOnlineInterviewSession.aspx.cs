
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateOnlineInterviewTestSession.cs
// File that represents the user interface for the create a new online interview 
// session. This will interact with the OnlineInterviewBLManager to insert online interview 
// session details to the database.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Text;
using System.Data;
using System.Web.UI;

#endregion Directives

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for CreateOnlineInterviewTestSession page. This is used
    /// to create a online interview session by providing number of candidates,
    /// session description, interview instructions etc. 
    /// Also this page provides some set of links in the online interview session
    /// grid for viewing candidate detail, test session and cancel the
    /// cancel the candidate online interview session. This class is inherited from
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class EditOnlineInterviewSession : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Variables

        #region Event Handlers

        DataTable dttempTable = null;
        /// <summary>
        /// Handler that will fire when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = EditOnlineInterviewSession_topSaveButton.UniqueID;
                Master.SetPageCaption("Edit Online Interview Session");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = EditOnlineInterviewSession_sessionNameTextBox.UniqueID;
                    EditOnlineInterviewSession_sessionNameTextBox.Focus();

                    TrAssessor.Visible = true;
                    if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                        ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                        ViewState["SORT_EXPRESSION"] = "InterviewSessionID";

                    if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                        ViewState["PAGE_NUMBER"] = "1";

                    if (Request.QueryString["interviewtestkey"] != null)
                    {
                        EditOnlineInterviewSession_TestKeyHiddenField.Value =
                            Request.QueryString["interviewtestkey"].ToString();
                    }

                    EditOnlineInterviewSession_SearchSkillButton.Attributes.Add("onclick",
                       "return LoadSkillLookup('" + EditOnlineInterviewSession_CaegoryIDHiddenField.ClientID + "','" +
                       EditOnlineInterviewSession_skillTextBox.ClientID + "','" +
                       EditOnlineInterviewSession_SkillIDHiddenField.ClientID + "','" + 
                       EditOnlineInterviewSession_NewSkillButton.ClientID + "')");

                    //LoadTestDetail(EditOnlineInterviewSession_TestKeyHiddenField.Value);
                    EditOnlineInterviewSession_positionProfileImageButton.Attributes.Add("onclick",
                    "return ShowPositionProfileLooKUpToAppendTextBoxContent('" + 
                    EditOnlineInterviewSession_positionProfileTextBox.ClientID + "','" +
                    EditOnlineInterviewSession_skillTextBox.ClientID + "','" +
                    EditOnlineInterviewSession_positionProfileIDHiddenField.ClientID + "','" + 
                    EditOnlineInterviewSession_NewSkillButton.ClientID + "')");
                    Session["ASSESSOR"] = null;
                    Session["SKILL"] = null;
                }
                
                // Add handler for email button.
                EditOnlineInterviewSession_emailImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowEmailCandidateSessions('EMAIL_ONLINE_INTERVIEW_SESSION')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                    EditOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will help to validate input fields before they saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditOnlineInterviewSession_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                // Validate the data
                if (!IsValidData())
                    return;

                OnlineInterviewSessionDetail onlineInterviewSessionDetail = PreviewOnlineInterviewSession();
                EditOnlineInterviewSession_viewTestSessionSave_UserControl.Mode = "view";

                EditOnlineInterviewSession_viewTestSessionSave_UserControl.DataSource = onlineInterviewSessionDetail;
                EditOnlineInterviewSession_viewTestSessionSave_modalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                    EditOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to delete the skill matrix
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditOnlineInterviewSession_skillGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            HiddenField SubjectValue_HiddenFiled = (HiddenField)EditOnlineInterviewSession_skillGridView.
                Rows[e.RowIndex].FindControl("EditOnlineInterviewSession_skillGridView_skillIDHiddenField");

            if (string.IsNullOrEmpty(SubjectValue_HiddenFiled.Value)) return;

            dttempTable = (DataTable)ViewState["AssessorSkills"];

            foreach (DataRow _dr in dttempTable.Rows)
            {
                if (SubjectValue_HiddenFiled.Value.ToString().Trim() ==
                    Convert.ToString(_dr["SkillID"]))
                {
                    dttempTable.Rows.Remove(_dr);
                    dttempTable.AcceptChanges();
                    break;
                }
            }

            ViewState["AssessorSkills"] = dttempTable;

            EditOnlineInterviewSession_skillGridView.DataSource = dttempTable;
            EditOnlineInterviewSession_skillGridView.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditOnlineInterviewSession_NewSkillButton_Click
          (object sender, ImageClickEventArgs e)
        {
            ClearLabelMessage();
            if (EditOnlineInterviewSession_skillTextBox.Text.Trim() == "")
            {
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                        EditOnlineInterviewSession_bottomErrorMessageLabel,
                        "No skill entered to add");
                return;
            }
            dttempTable = new DataTable();

            if (ViewState["AssessorSkills"] == null)
            {
                dttempTable.Columns.Add("Category", typeof(string));
                dttempTable.Columns.Add("SkillID", typeof(int));
                dttempTable.Columns.Add("Skill", typeof(string));
                dttempTable.AcceptChanges();
            }
            else
                dttempTable = (DataTable)ViewState["AssessorSkills"];

            string[] searchedSkill= null;

            searchedSkill = EditOnlineInterviewSession_skillTextBox.Text.ToString().Split(',');

            
            foreach (string skill in searchedSkill)
            {
                bool skillExist = false;
                AddManual_Skill(skill);
                string categoryID_HiddenField = EditOnlineInterviewSession_CaegoryIDHiddenField.Value.ToString().Trim();
                string sbjectID_HiddenField = EditOnlineInterviewSession_SkillIDHiddenField.Value.ToString();

                if (dttempTable.Rows.Count > 0)
                {
                    
                    foreach (DataRow drow in dttempTable.Rows)
                    {
                        if (Convert.ToString(drow[0]) == Convert.ToString(categoryID_HiddenField)
                            && Convert.ToInt32(drow[1]) == Convert.ToInt32(sbjectID_HiddenField))
                        {
                            this.EditOnlineInterviewSession_skillTextBox.Focus();
                            base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel, 
                                string.Concat(skill," already added"));
                            skillExist = true;
                        }
                    }
                }
                if (!skillExist)
                {
                    DataRow dr = dttempTable.NewRow();
                    dr[0] = categoryID_HiddenField;
                    dr[1] = sbjectID_HiddenField;
                    dr[2] = skill;

                    dttempTable.Rows.Add(dr);
                    dttempTable.AcceptChanges();

                    ViewState["AssessorSkills"] = dttempTable;
                }
            }
            

            EditOnlineInterviewSession_skillTextBox.Text = "";
            EditOnlineInterviewSession_CaegoryIDHiddenField.Value = "";
            EditOnlineInterviewSession_SkillIDHiddenField.Value = "";

            EditOnlineInterviewSession_skillGridView.DataSource = null;
            EditOnlineInterviewSession_skillGridView.DataBind();

            EditOnlineInterviewSession_skillGridView.DataSource = dttempTable;
            EditOnlineInterviewSession_skillGridView.DataBind();
        }

        /// <summary>
        /// 
        /// </summary> 
        /// <param name="skill">
        /// <see cref="string"/>
        /// </param>
        private void AddManual_Skill(string skill)
        {
            DataTable dttemp = new InterviewBLManager().getSubjectCategoryID(skill);
            if (dttemp.Rows.Count != 0)
            {
                EditOnlineInterviewSession_CaegoryIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][0]);
                EditOnlineInterviewSession_SkillIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][1]);
                EditOnlineInterviewSession_skillTextBox.Text = Convert.ToString(dttemp.Rows[0][2]);
            }
        }

        /// <summary>
        /// Handler that will call when clicking on create button which shows
        /// the given information before its getting inserted in the database.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditOnlineInterviewSession_previewTestSessionControl_createButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear success/error label message
                ClearLabelMessage();

                OnlineInterviewSessionDetail onlineInterviewSessionDetail = ConstructOnlineInterviewSessionDetail();
                CreditRequestDetail creditRequestDetail = new CreditRequestDetail();

                //Add time slots
                List<AssessorTimeSlotDetail> slots = Session["AVAILABILITY_REQUEST_LIST"] as List<AssessorTimeSlotDetail>;

                //Assessor list and selected skill id
                List<AssessorDetail> assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;
                
                ////creditRequestDetail.Amount = decimal.Parse(EditOnlineInterviewSession_creditValueLabel.Text);

                string interviewSessionKey = null;
                string candidateSessionIDs = null;
                bool isMailSent = false;

                new OnlineInterviewBLManager().SaveOnlineInterviewSession(onlineInterviewSessionDetail,
                    slots, assessorDetails, base.userID,
                    out interviewSessionKey, out candidateSessionIDs, out isMailSent);

                // Keep the TestSessionID in Viewstate. 
                // This will be used by ScheduleCandidate button event handler
                ViewState["ONLINE_INTERVIEW_SESSION_ID"] = interviewSessionKey;

                base.ShowMessage(EditOnlineInterviewSession_topSuccessMessageLabel,
                    EditOnlineInterviewSession_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.
                    CreateOnlineInterviewSession_OnlineInteviewSessionAndCandidateSessionsCreatedSuccessfully,
                    interviewSessionKey, candidateSessionIDs));

                //To hide add assessor option once online interview session created
                if (!string.IsNullOrEmpty(interviewSessionKey))
                    TrAssessor.Visible = false;
                // If the mail sent failed, then show the error message.
                //if (isMailSent == false)
                //{
                //    base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                //        EditOnlineInterviewSession_bottomErrorMessageLabel,
                //         "<br>Mail cannot be sent to the session author. Contact your administrator");
                //}

                // Set visible false once the session is created.
                EditOnlineInterviewSession_topSaveButton.Visible = false;

                // Enable the schedule candidate button
                EditOnlineInterviewSession_topScheduleCandidateButton.Visible = true;

                // Show the email session button.
                EditOnlineInterviewSession_emailImageButton.Visible = true;

                // Keep the created test session details in session, so that it can
                // be used in the email popup.
                Session["EMAIL_ONLINE_INTERVIEW_SESSION_SUBJECT"] = string.Format
                    ("Online interview session '{0}' created", interviewSessionKey);
                Session["EMAIL_ONLINE_INTERVIEW_SESSION_MESSAGE"] = string.Format
                    ("Dear Viewer,\n\nSub: Online interview session '{0}' created.\n\nThe following candidate sessions are associated:\n{1}",
                    interviewSessionKey, candidateSessionIDs);
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                    EditOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }
        protected void EditOnlineInterviewSession_addAssessorGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
        /// <summary>
        /// Handler that will perform reset/clear the input fields.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditOnlineInterviewSession_resetButton_Click(object sender, EventArgs e)
        {
            EditOnlineInterviewSession_sessionNameTextBox.Text = string.Empty;
            EditOnlineInterviewSession_positionProfileTextBox.Text = string.Empty;
            EditOnlineInterviewSession_skillTextBox.Text = string.Empty;
            EditOnlineInterviewSession_sessionDescTextBox.Text = string.Empty;
            EditOnlineInterviewSession_instructionsTextBox.Text = string.Empty;
            EditOnlineInterviewSession_sessionNoTextBox.Text = "0";
            EditOnlineInterviewSession_expiryDateTextBox.Text = string.Empty;
            EditOnlineInterviewSession_assessorDetailsGridView.DataSource = null;
            EditOnlineInterviewSession_assessorDetailsGridView.DataBind();
            EditOnlineInterviewSession_assessorDetailsDiv.Visible = false;
            ClearLabelMessage();
        }

        /// <summary>
        /// Handler that performs to redirect to schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void EditOnlineInterviewSession_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/OnlineInterview/ScheduleOnlineInterviewCandidate.aspx?m=3&s=1&interviewSessionKey=" 
                + ViewState["ONLINE_INTERVIEW_SESSION_ID"], false);
        }

        #endregion Event Handlers

        
        #region Private Methods

        /// <summary>
        /// A Method that gets the position profile keywords by removing there
        /// weightages
        /// </summary>
        /// <param name="KeyWordWithWeightages">
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// among with their weightages
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// by removing their weightages
        /// </returns>
        private string GetPositionProfileKeys(string KeyWordWithWeightages)
        {
            StringBuilder sbKeywords = null;
            string[] strSplitKeywords = null;
            try
            {
                strSplitKeywords = KeyWordWithWeightages.Split(',');
                sbKeywords = new StringBuilder();
                for (int i = 0; i < strSplitKeywords.Length; i++)
                    if (strSplitKeywords[i].IndexOf('[') != -1)
                        sbKeywords.Append(strSplitKeywords[i].Substring(0, strSplitKeywords[i].IndexOf('[')) + ",");
                    else
                        sbKeywords.Append(strSplitKeywords[i] + ",");
                return sbKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (strSplitKeywords != null) strSplitKeywords = null;
                if (sbKeywords != null) sbKeywords = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Method that will preview the online interview session details once the user clicked 
        /// on the save button.
        /// </summary>
        /// <returns>Returns the <see cref="onlineInterviewSessionDetail"/> object</returns>
        private OnlineInterviewSessionDetail PreviewOnlineInterviewSession()
        {
            // Initialize the test session detail object
            OnlineInterviewSessionDetail onlineInterviewSessionDetail = new OnlineInterviewSessionDetail();

            // Get the information from the controls and assign to the object members

            onlineInterviewSessionDetail.OnlineInterviewSessionName = EditOnlineInterviewSession_sessionNameTextBox.Text;
            onlineInterviewSessionDetail.NumberOfCandidateSessions
                = Convert.ToInt32(EditOnlineInterviewSession_sessionNoTextBox.Text);

            onlineInterviewSessionDetail.PositionProfileName = EditOnlineInterviewSession_positionProfileTextBox.Text;

            // Set the time to maximum in expiry date.
            if(EditOnlineInterviewSession_expiryDateTextBox.Text!=string.Empty)
                onlineInterviewSessionDetail.ExpiryDate = (Convert.ToDateTime(EditOnlineInterviewSession_expiryDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

            onlineInterviewSessionDetail.OnlineInterviewSessionDescription = EditOnlineInterviewSession_sessionDescTextBox.Text;
            onlineInterviewSessionDetail.OnlineInterviewInstruction = EditOnlineInterviewSession_instructionsTextBox.Text;
            onlineInterviewSessionDetail.Skills = EditOnlineInterviewSession_skillTextBox.Text;
            onlineInterviewSessionDetail.CreatedBy = base.userID;
            onlineInterviewSessionDetail.ModifiedBy = base.userID;

            return onlineInterviewSessionDetail;
        }

        /// <summary>
        /// This method gets the seconds values and updated the time format as hh:mm:yyyy
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string GetTimeFormat(int seconds)
        {
            return Utility.ConvertSecondsToHoursMinutesSeconds(seconds);
        }


        /// <summary>
        /// This method returns the OnlineInterivewSessionDetails instance with all the values 
        /// given as input by user.
        /// </summary>
        /// <returns></returns>
        private OnlineInterviewSessionDetail ConstructOnlineInterviewSessionDetail()
        {
            // Initialize test session detail object
            OnlineInterviewSessionDetail onlineInterviewSessionDetail = new OnlineInterviewSessionDetail();

            // Check whether a test key is empty or not before assigning to 

            // Set session name name
           onlineInterviewSessionDetail.OnlineInterviewSessionName = EditOnlineInterviewSession_sessionNameTextBox.Text;

            // Set number of candidate session (session count)
            onlineInterviewSessionDetail.NumberOfCandidateSessions =
                Convert.ToInt32(EditOnlineInterviewSession_sessionNoTextBox.Text);


            // Set position profile ID.
            if (!Utility.IsNullOrEmpty(EditOnlineInterviewSession_positionProfileIDHiddenField.Value))
                onlineInterviewSessionDetail.PositionProfileID =
                    Convert.ToInt32(EditOnlineInterviewSession_positionProfileIDHiddenField.Value);
            else
                onlineInterviewSessionDetail.PositionProfileID = 0;

            // Set Skills/Areas
            onlineInterviewSessionDetail.Skills = EditOnlineInterviewSession_skillTextBox.Text;
            // Set expiry date
            if(EditOnlineInterviewSession_expiryDateTextBox.Text!=string.Empty)
                onlineInterviewSessionDetail.ExpiryDate = Convert.ToDateTime(EditOnlineInterviewSession_expiryDateTextBox.Text);

            // Set created by
            onlineInterviewSessionDetail.CreatedBy = base.userID;

            // Set modified by
            onlineInterviewSessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            onlineInterviewSessionDetail.OnlineInterviewInstruction =
                EditOnlineInterviewSession_instructionsTextBox.Text.ToString().Trim();

            // Set session descriptions
            onlineInterviewSessionDetail.OnlineInterviewSessionDescription =
                EditOnlineInterviewSession_sessionDescTextBox.Text.ToString().Trim();

            if (Session["ASSESSOR"] != null)
            {
                List<AssessorDetail> assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;
                //for (int i = 0; i < assessorDetails.Count; i++)
                //{
                //    onlineInterviewSessionDetail.AssessorIDs += assessorDetails[i].UserID + ",";
                //}
                //onlineInterviewSessionDetail.AssessorIDs = onlineInterviewSessionDetail.AssessorIDs.ToString().Remove(onlineInterviewSessionDetail.AssessorIDs.Length - 1);
            }
            return onlineInterviewSessionDetail;
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            EditOnlineInterviewSession_topSuccessMessageLabel.Text = string.Empty;
            EditOnlineInterviewSession_bottomSuccessMessageLabel.Text = string.Empty;
            EditOnlineInterviewSession_topErrorMessageLabel.Text = string.Empty;
            EditOnlineInterviewSession_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isAssessorInvalid = false;
            // Validate session description text field
            if (EditOnlineInterviewSession_sessionNameTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                    EditOnlineInterviewSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateInterviewTestSession_SessionNameCannotBeEmpty);
            }

            //// Validate skills/areas text field
            //if (EditOnlineInterviewSession_skillTextBox.Text.Trim().Length == 0)
            //{
            //    isValidData = false;
            //    base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
            //        EditOnlineInterviewSession_bottomErrorMessageLabel,
            //        Resources.HCMResource.CreateInterviewTestSession_skillsCannotBeEmpty);
            //}
            
            // Validate number of session field
            if (Convert.ToInt32(EditOnlineInterviewSession_sessionNoTextBox.Text) <= 0 ||
                (Convert.ToInt32(EditOnlineInterviewSession_sessionNoTextBox.Text) > 30))
            {
                isValidData = false;
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                    EditOnlineInterviewSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateInterviewTestSession_SessionCountCannotBeZero);
            }

            EditOnlineInterviewSession_MaskedEditValidator.Validate();

            if (EditOnlineInterviewSession_expiryDateTextBox.Text.Trim().Length != 0)
            {
                // This will check if the date is valid date. i.e. 99/99/9999
                if (!EditOnlineInterviewSession_MaskedEditValidator.IsValid)
                {
                    isValidData = false;
                    base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                    EditOnlineInterviewSession_bottomErrorMessageLabel,
                    EditOnlineInterviewSession_MaskedEditValidator.ErrorMessage);
                }
                else
                {
                    // Check if the expiry date is less than current date
                    if (DateTime.Parse(EditOnlineInterviewSession_expiryDateTextBox.Text) < DateTime.Today)
                    {
                        isValidData = false;
                        base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                        EditOnlineInterviewSession_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateInterviewTestSession_ExpiryDateCannotBeLessThanCurrentDate);
                    }
                }
            }

            if (Session["ASSESSOR"] != null)
            {
                if ((Session["ASSESSOR"] as List<AssessorDetail>).Count == 0)
                    isAssessorInvalid = true;
            }
            else
                isAssessorInvalid = true;

            if (isAssessorInvalid)
            {
                isValidData = false;
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                EditOnlineInterviewSession_bottomErrorMessageLabel,
                "Assessor cannot be empty");
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Method to call add assessor and assign to grid values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditOnlineInterviewSession_addAssessorLinkButton_Click(object sender, EventArgs e)
        {
            if (Session["ASSESSOR"] == null)
                return;
            else
            {
                List<AssessorDetail> slots = Session["ASSESSOR"] as List<AssessorDetail>;
                EditOnlineInterviewSession_assessorDetailsGridView.DataSource = Session["ASSESSOR"] as List<AssessorDetail>;
                EditOnlineInterviewSession_assessorDetailsGridView.DataBind();
                EditOnlineInterviewSession_assessorDetailsDiv.Visible = true;
            }
        }
        #endregion Protected Methods
        protected void EditOnlineInterviewSession_recommendAssessorLinkButton_Click(object sender, EventArgs e)
        {
            ClearLabelMessage();
            Session["SESSION_SUBJECT_TABLE"] = null;
            DataTable dtSkillSubject = new DataTable();
            dtSkillSubject = (DataTable)ViewState["AssessorSkills"];
            Session["SESSION_SUBJECT_TABLE"] = (DataTable)ViewState["AssessorSkills"];
            if (dtSkillSubject!=null && dtSkillSubject.Rows.Count!=0)
            {
                string OpenAssessorWindow = string.Empty;
                OpenAssessorWindow = "SearchInterviewOnlineRecommendAssessor('SEARCH',null,1,null,'" + EditOnlineInterviewSession_refreshGridButton.ClientID + "')";
                ScriptManager.RegisterStartupScript(EditOnlineInterviewSession_assessorLinkUpdatePanel,
                    EditOnlineInterviewSession_assessorLinkUpdatePanel.GetType(), "OpenAssessorWindow", OpenAssessorWindow, true);
            }
            else
            {
                base.ShowMessage(EditOnlineInterviewSession_topErrorMessageLabel,
                EditOnlineInterviewSession_bottomErrorMessageLabel,
                "Please select skills/areas");
            }
        }

        /// <summary>
        /// Assign Selected Assessors to the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditOnlineInterviewSession_refreshGridButton_Click(object sender, EventArgs e)
        {
            EditOnlineInterviewSession_assessorDetailsGridView.DataSource = Session["ASSESSOR"] as List<AssessorDetail>;
            EditOnlineInterviewSession_assessorDetailsGridView.DataBind();
            EditOnlineInterviewSession_assessorDetailsDiv.Visible = true;
        }
    }
}