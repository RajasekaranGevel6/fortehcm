﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="OnlineScheduleCandidate.aspx.cs" Inherits="Forte.HCM.UI.OnlineInterview.OnlineScheduleCandidate" %>

<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/OnlineInterviewMenuControl.ascx" TagName="OnlineInterviewMenuControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="OnlineScheduleCandidate_bodyContent" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <script language="javascript" type="text/javascript">
        // Handler method that will be called when the maximize or restore
        // button is clicked. This will maximize or restore the panel to
        // show or hide full view.
        function ExpandOrRestoreSkillAssociated(ctrlExpand, expandImage, compressImage, isMaximizedHidden) {
            if (document.getElementById(ctrlExpand) != null) {
                if (document.getElementById(isMaximizedHidden).value == "Y") {
                    document.getElementById(ctrlExpand).style.display = "none";
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(compressImage).style.display = "none";
                    document.getElementById(isMaximizedHidden).value = "N";
                }
                else {
                    document.getElementById(ctrlExpand).style.display = "block";
                    document.getElementById(expandImage).style.display = "none";
                    document.getElementById(compressImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "Y";
                }
            }
            return false;
        }

        function ExpandOrRestoreSearchResults(ctrlSearchExpand, expandSearchImage, compressSearchImage, isSearchMaximizedHidden) {
            if (document.getElementById(ctrlSearchExpand) != null) {
                if (document.getElementById(isSearchMaximizedHidden).value == "Y") {
                    document.getElementById(ctrlSearchExpand).style.display = "none";
                    document.getElementById(expandSearchImage).style.display = "block";
                    document.getElementById(compressSearchImage).style.display = "none";
                    document.getElementById(isSearchMaximizedHidden).value = "N";
                }
                else {
                    document.getElementById(ctrlSearchExpand).style.display = "block";
                    document.getElementById(expandSearchImage).style.display = "none";
                    document.getElementById(compressSearchImage).style.display = "block";
                    document.getElementById(isSearchMaximizedHidden).value = "Y";
                }
            }
            return false;
        }

        function ShowLoadingImage() {
            document.getElementById("<%=OnlineScheduleCandidate_skillTextBox.ClientID%>").className = "position_profile_client_bg";
        }

        function HideLoadingImage() {
            document.getElementById("<%=OnlineScheduleCandidate_skillTextBox.ClientID%>").className = "";
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding-bottom: 0px">
                <uc3:OnlineInterviewMenuControl ID="OnlineScheduleCandidate_menuControl" runat="server"
                    Visible="true" PageType="SC" />
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="OnlineScheduleCandidate_topSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="OnlineScheduleCandidate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineScheduleCandidate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr runat="server">
            <td class="header_bg" align="center">
                <table cellpadding="0" cellspacing="0" width="99%" border="0">
                    <tr>
                        <td style="width: 50%" align="left" class="header_text_bold">
                            <asp:Literal ID="OnlineScheduleCandidate_sectionTitleLiteral" runat="server" Text="Schedule Candidate"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td class="non_tab_body_bg">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="left">
                                <tr>
                                    <td width="10%" align="left">
                                        <asp:Label ID="OnlineScheduleCandidate_candidateNameLabel" runat="server" Text="Candidate Name"
                                            SkinID="sknHomePageLabel"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                    </td>
                                    <td width="25%" align="left">
                                        <asp:HiddenField ID="OnlineScheduleCandidate_candidateIdHiddenField" runat="server" />
                                        <asp:TextBox ID="OnlineScheduleCandidate_candidateNameTextBox" runat="server" SkinID="sknHomePageTextBox"
                                            Width="200px" ReadOnly="true"> </asp:TextBox>
                                    </td>
                                    <td width="3%" align="center">
                                        <asp:ImageButton ID="OnlineScheduleCandidate_seacrchImageButton" runat="server" SkinID="sknbtnSearchIcon" />
                                    </td>
                                    <td width="3%" align="center">
                                        <asp:ImageButton ID="OnlineScheduleCandidate_seacrchPPImageButton" runat="server"
                                            SkinID="sknBtnSearchPositinProfileCandidateIcon" />
                                    </td>
                                    <td width="5%">
                                        <asp:Button ID="OnlineScheduleCandidate_newButton" runat="server" Text="New" SkinID="sknButtonId" />
                                    </td>
                                    <td>
                                        <div style="visibility: hidden">
                                            <asp:TextBox ID="OnlineScheduleCandidate_emailTextBox" runat="server" ReadOnly="true"
                                                Text=""> </asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- Search available Slots -->
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%" class="header_text_bold">
                                                    <asp:Literal ID="OnlineScheduleCandidate_searchSlot_headerLiteral" runat="server"></asp:Literal>
                                                </td>
                                                <td align="right">
                                                    <asp:UpdatePanel ID="OnlineScheduleCandidate_slotSectionUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="OnlineScheduleCandidate_createTimeSlotLinkButton" CssClass="request_tab_no_link_button"
                                                                Text="Create Time Slot" runat="server" OnClick="OnlineScheduleCandidate_createTimeSlotLinkButton_Click">
                                                            </asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                            <asp:LinkButton ID="OnlineScheduleCandidate_requestTimeSlotLinkButton" CssClass="request_tab_link_button"
                                                                Text="Request Time Slot" runat="server" OnClick="OnlineScheduleCandidate_requestTimeSlotLinkButton_Click">
                                                            </asp:LinkButton>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- Search Part -->
                                <tr>
                                    <td class="tab_body_bg">
                                        <asp:UpdatePanel ID="OnlineScheduleCandidate_scheduleSlotSectionUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="OnlineScheduleCandidate_searchSlotsDiv" runat="server">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="panel_bg">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="td_height_5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="panel_inner_body_bg">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td width="8%" align="left">
                                                                                                    <asp:Label ID="OnlineScheduleCandidate_fromDateTitle_Label" runat="server" Text="From Date"
                                                                                                        SkinID="sknHomePageLabel"></asp:Label><span class="mandatory">*</span>
                                                                                                </td>
                                                                                                <td width="12%" align="left">
                                                                                                    <asp:TextBox ID="OnlineScheduleCandidate_fromDate_TextBox" runat="server" SkinID="sknHomePageTextBox"
                                                                                                        Width="100px">
                                                                                                    </asp:TextBox>
                                                                                                </td>
                                                                                                <td width="8%" align="left">
                                                                                                    <asp:ImageButton ID="OnlineScheduleCandidate_fromDateImageButton" runat="server"
                                                                                                        SkinID="sknCalendarImageButton" />
                                                                                                    <ajaxToolKit:MaskedEditExtender ID="OnlineScheduleCandidate_fromDateMaskedEditExtender"
                                                                                                        runat="server" TargetControlID="OnlineScheduleCandidate_fromDate_TextBox" Mask="99/99/9999"
                                                                                                        MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                                        MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                                                    <ajaxToolKit:MaskedEditValidator ID="OnlineScheduleCandidate_fromDateMaskedEditValidator"
                                                                                                        runat="server" ControlExtender="OnlineScheduleCandidate_fromDateMaskedEditExtender"
                                                                                                        ControlToValidate="OnlineScheduleCandidate_fromDate_TextBox" EmptyValueMessage="Date is required"
                                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                                    <ajaxToolKit:CalendarExtender ID="OnlineScheduleCandidate_fromDateCalendarExtender"
                                                                                                        runat="server" TargetControlID="OnlineScheduleCandidate_fromDate_TextBox" CssClass="MyCalendar"
                                                                                                        Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="OnlineScheduleCandidate_fromDateImageButton" />
                                                                                                </td>
                                                                                                <td width="6%" align="left">
                                                                                                    <asp:Label ID="OnlineScheduleCandidate_toDateTitle_Label" runat="server" Text="To Date"
                                                                                                        SkinID="sknHomePageLabel"></asp:Label><span class="mandatory">*</span>
                                                                                                </td>
                                                                                                <td width="12%" align="left">
                                                                                                    <asp:TextBox ID="OnlineScheduleCandidate_toDate_TextBox" runat="server" SkinID="sknHomePageTextBox"
                                                                                                        Width="100px">
                                                                                                    </asp:TextBox>
                                                                                                </td>
                                                                                                <td width="8%" align="left">
                                                                                                    <asp:ImageButton ID="OnlineScheduleCandidate_toDateImageButton" runat="server" SkinID="sknCalendarImageButton" />
                                                                                                    <ajaxToolKit:MaskedEditExtender ID="MaskedEditExtOnlineScheduleCandidate_toDateMaskedEditExtenderender"
                                                                                                        runat="server" TargetControlID="OnlineScheduleCandidate_toDate_TextBox" Mask="99/99/9999"
                                                                                                        MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                                        MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                                                    <ajaxToolKit:MaskedEditValidator ID="OnlineScheduleCandidate_toDateMaskedEditValidator"
                                                                                                        runat="server" ControlExtender="MaskedEditExtOnlineScheduleCandidate_toDateMaskedEditExtenderender"
                                                                                                        ControlToValidate="OnlineScheduleCandidate_toDate_TextBox" EmptyValueMessage="Date is required"
                                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                                    <ajaxToolKit:CalendarExtender ID="OnlineScheduleCandidate_toDateCalendarExtender"
                                                                                                        runat="server" TargetControlID="OnlineScheduleCandidate_toDate_TextBox" CssClass="MyCalendar"
                                                                                                        Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="OnlineScheduleCandidate_toDateImageButton" />
                                                                                                </td>
                                                                                                <td style="width: 7%; height: 23px" align="left">
                                                                                                    <asp:Label ID="OnlineScheduleCandidate_fromLabel" runat="server" Text="From" EnableViewState="false"
                                                                                                        SkinID="sknHomePageLabel"></asp:Label>
                                                                                                    <span class="mandatory">*</span>
                                                                                                </td>
                                                                                                <td style="width: 10%" align="left">
                                                                                                    <asp:DropDownList ID="OnlineScheduleCandidate_fromDropDownList" runat="server" AutoPostBack="true"
                                                                                                        OnSelectedIndexChanged="OnlineScheduleCandidate_fromDropDownList_SelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td style="width: 7%" align="left">
                                                                                                    <asp:Label ID="OnlineScheduleCandidate_toLabel" runat="server" Text="To" SkinID="sknHomePageLabel"
                                                                                                        EnableViewState="false"></asp:Label><span class="mandatory">*</span>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="OnlineScheduleCandidate_toDropDownList" runat="server">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <asp:UpdatePanel ID="OnlineScheduleCandidate_searchUpdatePanel" runat="server">
                                                                                            <ContentTemplate>
                                                                                                <asp:Button ID="OnlineScheduleCandidate_searchButton" runat="server" Text="Search"
                                                                                                    SkinID="sknButtonId" OnClick="OnlineScheduleCandidate_searchButton_Click" />
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="OnlineScheduleCandidate_searchResultsSlotsDiv" runat="server">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr1" runat="server">
                                                                        <td class="header_bg">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td align="left" class="header_text_bold">
                                                                                        <asp:Literal ID="OnlineScheduleCandidate_searchResultsLiteral" runat="server" Text="Search Results">
                                                                                        </asp:Literal>&nbsp;<asp:Label ID="OnlineScheduleCandidate_sortHelpLabel" runat="server"
                                                                                            SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:ImageButton ID="OnlineScheduleCandidate_addRequestTimeImageButton" runat="server"
                                                                                            SkinID="sknPPAddImageButton" ToolTip="Add To Choice List" OnClick="OnlineScheduleCandidate_addRequestTimeImageButton_Click" />
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr id="OnlineScheduleCandidate_searchSlotsTR" runat="server">
                                                                                                <td>
                                                                                                    <span id="OnlineScheduleCandidate_searchSlotsUpSpan" runat="server" style="display: block;">
                                                                                                        <asp:Image ID="OnlineScheduleCandidate_searchSlotsUpImage" runat="server" SkinID="sknMinimizeImage"
                                                                                                            ToolTip="Click here to show" />
                                                                                                    </span><span id="OnlineScheduleCandidate_searchSlotsDownSpan" style="display: none;"
                                                                                                        runat="server">
                                                                                                        <asp:Image ID="OnlineScheduleCandidate_searchSlotsDownImage" runat="server" SkinID="sknMaximizeImage"
                                                                                                            ToolTip="Click here to hide" />
                                                                                                    </span>
                                                                                                    <asp:HiddenField ID="OnlineScheduleCandidate_searchSlots_restoreHiddenField" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="grid_body_bg">
                                                                            <div id="OnlineScheduleCandidate_candidateDetailDIV" runat="server" style="height: 120px;
                                                                                overflow: auto; display: none">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:UpdatePanel ID="OnlineScheduleCandidate_searchTimeSlotGridViewUpdatePanel" runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <asp:GridView ID="OnlineScheduleCandidate_searchTimeSlotGridView" runat="server"
                                                                                                        AllowSorting="true" OnSorting="OnlineScheduleCandidate_searchTimeSlotGridView_Sorting"
                                                                                                        AutoGenerateColumns="False" Width="100%">
                                                                                                        <EmptyDataRowStyle CssClass="error_message_text" />
                                                                                                        <EmptyDataTemplate>
                                                                                                            <table style="width: 100%">
                                                                                                                <tr>
                                                                                                                    <td style="height: 50px">
                                                                                                                        No slots found to display
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </EmptyDataTemplate>
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:CheckBox ID="OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotCheckBox"
                                                                                                                        runat="server" SkinID="sknMyRecordCheckBox" />
                                                                                                                    <asp:HiddenField ID="OnlineScheduleCandidate_searchTimeSlotGridView_assessorIDHiddenField"
                                                                                                                        Value='<%# Eval("AssessorID") %>' runat="server" />
                                                                                                                    <asp:HiddenField ID="OnlineScheduleCandidate_searchTimeSlotGridView_timeFromIDHiddenField"
                                                                                                                        Value='<%# Eval("TimeSlotIDFrom") %>' runat="server" />
                                                                                                                    <asp:HiddenField ID="OnlineScheduleCandidate_searchTimeSlotGridView_timeToIDHiddenField"
                                                                                                                        Value='<%# Eval("TimeSlotIDTo") %>' runat="server" />
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="5%" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Available Date" SortExpression="AVAILABILITY_DATE">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotDateLabel"
                                                                                                                        runat="server" Text='<%# Eval("AvailabilityDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="15%" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="From Time">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotFromLabel"
                                                                                                                        runat="server" Text='<%# Eval("TimeSlotTextFrom") %>'></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="15%" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="To Time">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="OnlineScheduleCandidate_searchTimeSlotGridView_timeSlotToLabel" runat="server"
                                                                                                                        Text='<%# Eval("TimeSlotTextTo") %>'></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="15%" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Interviewer Name" SortExpression="ASSESSOR_NAME">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="OnlineScheduleCandidate_searchTimeSlotGridView_interviewerNameLabel"
                                                                                                                        runat="server" Text='<%# Eval("Assessor") %>'></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="18%" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Accepted Date">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="OnlineScheduleCandidate_searchTimeSlotGridView_acceptedDateLabel"
                                                                                                                        runat="server" Text='<%# Eval("AcceptedDate","{0:MM/dd/yyyy hh:mm ss}") %>' SkinID="sknHomePageLabel"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="20%" />
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:AsyncPostBackTrigger ControlID="OnlineScheduleCandidate_searchButton" />
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_5">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right">
                                                                                            <uc1:PageNavigator ID="OnlineScheduleCandidate_pageNavigator" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <!-- Choice list -->
                                                    <tr>
                                                        <td>
                                                            <div id="OnlineScheduleCandidate_requestTimeSlotSectionDiv" runat="server" style="display: block">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr2" runat="server">
                                                                        <td class="header_bg">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                                        <asp:Literal ID="OnlineScheduleCandidate_choiceListLiteral" runat="server" Text="Request Time Slot">
                                                                                        </asp:Literal>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="grid_body_bg">
                                                                            <div id="Div1" runat="server" style="height: 100px; overflow: auto;">
                                                                                <asp:UpdatePanel ID="OnlineScheduleCandidate_timeSlotGridViewUpdatePanel" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:GridView ID="OnlineScheduleCandidate_timeSlotGridView" runat="server" AutoGenerateColumns="False"
                                                                                            Width="100%" 
                                                                                            OnRowCommand ="OnlineScheduleCandidate_timeSlotGridView_RowCommand" >
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="OnlineScheduleCandidate_timeSlotGridView_deleteChoiceImageButton" SkinID="sknPPDeleteImageButton"
                                                                                                                 ToolTip="Delete Choice" CommandName ="Remove" runat="server"  />
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="5%" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="OnlineScheduleCandidate_timeSlotGridView_ChoiceLabel" Text='<%# String.Format("Choice {0}",Container.DataItemIndex + 1)  %>'
                                                                                                            runat="server"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="10%" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Date">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="OnlineScheduleCandidate_timeSlotGridView_DateLabel" runat="server"
                                                                                                            Text='<%# Eval("AvailabilityDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="15%" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Requested Time">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="OnlineScheduleCandidate_timeSlotGridView_FromLabel" runat="server"
                                                                                                            SkinID="sknHomePageLabel" Text='<%# String.Format("{0} To {1}",Eval("TimeSlotTextFrom"),Eval("TimeSlotTextTo")) %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="25%" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Interviewer Name">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="OnlineScheduleCandidate_timeSlotGridView_interviewerId_HiddenField" runat="server" Value ='<%# Eval("AssessorId") %>' />
                                                                                                        <asp:Label ID="OnlineScheduleCandidate_timeSlotGridView_interviewerName_LinkButton"
                                                                                                            runat="server" Text='<%# Eval("Assessor") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="OnlineScheduleCandidate_addRequestTimeImageButton" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <!-- End Choice list -->
                                                    <!-- Create Time Slot -->
                                                    <tr>
                                                        <td>
                                                            <div id="OnlineScheduleCandidate_createTimeSlotSectionDiv" runat="server" style="display: none">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr3" runat="server">
                                                                        <td class="header_bg">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                                        <asp:Literal ID="Literal1" runat="server" Text="Create Time Slot"></asp:Literal>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Button ID="OnlineScheduleCandidate_addAssessorButton" runat="server" Text="Add Assessor"
                                                                                                        SkinID="sknButtonId" />
                                                                                                    <div style="display: none">
                                                                                                        <asp:Button ID="OnlineScheduleCandidate_addAssessorRefreshButton" runat="server"
                                                                                                            Text="Refresh" SkinID="sknButtonId" OnClick="OnlineScheduleCandidate_addAssessorRefreshButton_Click" />
                                                                                                    </div>
                                                                                                    <asp:HiddenField runat="server" ID="OnlineScheduleCandidate_addAssessorNameHiddenField" />
                                                                                                    <asp:HiddenField runat="server" ID="OnlineScheduleCandidate_addAssessorIDHiddenField" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="grid_body_bg">
                                                                            <div id="Div2" runat="server" style="height: 100px; overflow: auto;">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td style="width: 40%" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td align="left" width="25%">
                                                                                                        <asp:Label ID="OnlineScheduleCandidate_scheduleDateLabel" runat="server" SkinID="sknHomePageLabel"
                                                                                                            Text="Schedule Date"></asp:Label>
                                                                                                        <span class="mandatory">*</span>
                                                                                                    </td>
                                                                                                    <td align="left" width="7%">
                                                                                                        <asp:TextBox ID="OnlineScheduleCandidate_scheduleDateTextBox" runat="server" SkinID="sknHomePageTextBox"
                                                                                                            Width="100px">
                                                                                                        </asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left" width="8%" colspan="2" style="padding-left: 2px">
                                                                                                        <asp:ImageButton ID="OnlineScheduleCandidate_scheduleDateImageButton" runat="server"
                                                                                                            SkinID="sknCalendarImageButton" />
                                                                                                        <ajaxToolKit:MaskedEditExtender ID="OnlineScheduleCandidate_scheduleDateMaskedEditExtender"
                                                                                                            runat="server" AcceptNegative="Left" DisplayMoney="Left" ErrorTooltipEnabled="True"
                                                                                                            Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                                            OnInvalidCssClass="MaskedEditError" TargetControlID="OnlineScheduleCandidate_scheduleDateTextBox" />
                                                                                                        <ajaxToolKit:MaskedEditValidator ID="OnlineScheduleCandidate_scheduleDateMaskedEditValidator"
                                                                                                            runat="server" ControlExtender="OnlineScheduleCandidate_scheduleDateMaskedEditExtender"
                                                                                                            ControlToValidate="OnlineScheduleCandidate_scheduleDateTextBox" Display="None"
                                                                                                            EmptyValueBlurredText="*" EmptyValueMessage="Date is required" InvalidValueBlurredMessage="*"
                                                                                                            InvalidValueMessage="Date is invalid" TooltipMessage="Input a date" ValidationGroup="MKE" />
                                                                                                        <ajaxToolKit:CalendarExtender ID="OnlineScheduleCandidate_scheduleDateCalendarExtender"
                                                                                                            runat="server" CssClass="MyCalendar" Format="MM/dd/yyyy" PopupButtonID="OnlineScheduleCandidate_scheduleDateImageButton"
                                                                                                            PopupPosition="BottomLeft" TargetControlID="OnlineScheduleCandidate_scheduleDateTextBox" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_height_10">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 20%;" align="left">
                                                                                                        <asp:Label ID="OnlineScheduleCandidate_scheduleFromLabel" runat="server" Text="From"
                                                                                                            EnableViewState="false" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                        <span class="mandatory">*</span>
                                                                                                    </td>
                                                                                                    <td style="width: 15%" align="left">
                                                                                                        <asp:DropDownList ID="OnlineScheduleCandidate_scheduleFromDropDownList" runat="server"
                                                                                                            AutoPostBack="true" OnSelectedIndexChanged="OnlineScheduleCandidate_scheduleFromDropDownList_SelectedIndexChanged">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>
                                                                                                    <td style="width: 15%" align="left">
                                                                                                        <asp:Label ID="OnlineScheduleCandidate_scheduleToLabel" runat="server" Text="To"
                                                                                                            SkinID="sknHomePageLabel" EnableViewState="false"></asp:Label><span class="mandatory">*</span>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:DropDownList ID="OnlineScheduleCandidate_scheduleToDropDownList" runat="server">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_height_5">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td style="width: 60%" align="left" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td align="center" class="interview_assessor_slot_grid_body_bg" valign="top">
                                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <asp:CheckBoxList ID="OnlineScheduleCandidate_interviewAssessorCheckBoxList" Width="100%"
                                                                                                                        RepeatColumns="3" runat="server">
                                                                                                                    </asp:CheckBoxList>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <!-- End Create Time Slot -->
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="OnlineScheduleCandidate_createTimeSlotLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="OnlineScheduleCandidate_requestTimeSlotLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <!-- End Search Part -->
                            </table>
                        </td>
                    </tr>
                    <!-- End available Slots -->
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- Skill Association -->
                    <tr>
                        <td valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr id="SearchUserInformation_searchTestResultsTR" runat="server">
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 114px" align="left">
                                                    <asp:Label runat="server" SkinID="sknLabelFieldHeaderText" Text="Skill Associated"
                                                        ID="OnlineScheduleCandidate_skillAssociatedLabel"></asp:Label>
                                                </td>
                                                <td>
                                                    <div style="float: left;">
                                                        <asp:TextBox ID="OnlineScheduleCandidate_skillTextBox" runat="server" Width="350px"
                                                            Columns="65" Style="font-size: 90%; font-family: Tahoma, Arial, sans-serif; color: gray;
                                                            font-style: normal; text-align: left;" AutoPostBack="true" OnTextChanged="OnlineScheduleCandidate_skillTextBox_TextChanged"> </asp:TextBox>
                                                    </div>
                                                    <div style="float: left; padding: 2px; 0px; 0px; 2px">
                                                        <asp:Image ID="OnlineScheduleCandidate_skillAddImage" SkinID="sknSkillAdImage" runat="server" />
                                                    </div>
                                                    <ajaxToolKit:TextBoxWatermarkExtender ID="OnlineScheduleCandidate_newAssessorTextBoxWatermarkExtender"
                                                        runat="server" TargetControlID="OnlineScheduleCandidate_skillTextBox" WatermarkText="Enter/Select skill name"
                                                        WatermarkCssClass="assessor_skill_water_mark">
                                                    </ajaxToolKit:TextBoxWatermarkExtender>
                                                    <ajaxToolKit:AutoCompleteExtender ID="OnlineScheduleCandidate_skillAutoCompleteExtender"
                                                        runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetSkillList"
                                                        TargetControlID="OnlineScheduleCandidate_skillTextBox" MinimumPrefixLength="1"
                                                        CompletionListElementID="pnl" CompletionListCssClass="interview_client_completion_list"
                                                        CompletionListItemCssClass="interview_client_completion_list_item" OnClientPopulating="ShowLoadingImage"
                                                        OnClientPopulated="HideLoadingImage" EnableCaching="true" CompletionSetCount="12"
                                                        OnClientShowing="ShowLoadingImage" OnClientHiding="HideLoadingImage" CompletionListHighlightedItemCssClass="interview_client_completion_list_highlight">
                                                    </ajaxToolKit:AutoCompleteExtender>
                                                    <asp:Panel ID="pnl" runat="server">
                                                        &nbsp;</asp:Panel>
                                                </td>
                                                <td align="right" style="text-align: right">
                                                    <asp:UpdatePanel ID="OnlineScheduleCandidate_autoCalculateUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="OnlineScheduleCandidate_autoCalculateLinkButton" runat="server"
                                                                Text="Auto Calculate" OnClick="OnlineScheduleCandidate_autoCalculateLinkButton_Click"
                                                                SkinID="sknActionLinkButton" ToolTip="Click here to auto populate"></asp:LinkButton>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td align="right">
                                                    <asp:Button ID="OnlineScheduleCandidate_searchSkillButton" Text="Search Skill" SkinID="sknButtonId"
                                                        ToolTip="Click here to search & add skill" runat="server" />
                                                </td>
                                                <td style="width: 50px">
                                                    <div style="display: none">
                                                        <asp:Button ID="OnlineScheduleCandidate_refreshSearchButton" runat="server" Text="Refresh"
                                                            OnClick="OnlineScheduleCandidate_refreshSearchButton_Click" />
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr id="OnlineScheduleCandidate_skillAssociatedTR" runat="server">
                                                            <td>
                                                                <span id="OnlineScheduleCandidate_skillAssociatedUpSpan" runat="server" style="display: block;">
                                                                    <asp:Image ID="OnlineScheduleCandidate_skillAssociatedUpImage" runat="server" SkinID="sknMinimizeImage"
                                                                        ToolTip="Click here to show" />
                                                                </span><span id="OnlineScheduleCandidate_skillAssociatedDownSpan" style="display: none;"
                                                                    runat="server">
                                                                    <asp:Image ID="OnlineScheduleCandidate_skillAssociatedDownImage" runat="server" SkinID="sknMaximizeImage"
                                                                        ToolTip="Click here to hide" />
                                                                </span>
                                                                <asp:HiddenField ID="OnlineScheduleCandidate_skillAssociated_restoreHiddenField"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="clear: both; display: none" id="OnlineScheduleCandidate_skillAssociatedDiv"
                                            runat="server">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="grid_body_bg">
                                                        <div style="clear: both;" runat="server">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:UpdatePanel ID="OnlineScheduleCandidate_skillAssociatedGridViewUpdatePanel"
                                                                            runat="server">
                                                                            <ContentTemplate>
                                                                                <div style="clear: both; height: 150px; overflow: auto;">
                                                                                    <asp:GridView ID="OnlineScheduleCandidate_skillAssociatedGridView" runat="server"
                                                                                        AutoGenerateColumns="False" AllowSorting="False" OnRowCommand="OnlineScheduleCandidate_skillAssociatedGridView_RowCommand">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderStyle-Width="10%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="OnlineScheduleCandidate_skillAssociatedGridView_deleteSkillImageButton"
                                                                                                        runat="server" SkinID="sknPPDeleteImageButton" CommandArgument='<%# Eval("SkillID") %>'
                                                                                                        ToolTip="Delete Skill" CssClass="showCursor" CommandName="DeleteSkill" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Skill" HeaderStyle-Width="25%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="OnlineScheduleCandidate_skillAssociatedGridView_skillNameLabel" runat="server"
                                                                                                        Text='<%# Eval("SkillName") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Weightage" HeaderStyle-Width="15%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox ID="OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox"
                                                                                                        runat="server" MaxLength="3" Width="25%" Text='<%# Eval("Weightage") %>'></asp:TextBox>
                                                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="OnlineScheduleCandidate_weightageTextBoxExtender"
                                                                                                        runat="server" FilterType="Custom,Numbers" TargetControlID="OnlineScheduleCandidate_skillAssociatedGridView_weightageTextBox"
                                                                                                        ValidChars=".">
                                                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:HiddenField ID="OnlineScheduleCandidate_skillAssociatedGridView_skillIDHiddenField"
                                                                                                        runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SkillID")%>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                                <asp:HiddenField ID="OnlineScheduleCandidate_categoryIDHiddenField" runat="server" />
                                                                                <asp:HiddenField ID="OnlineScheduleCandidate_skillIDHiddenField" runat="server" />
                                                                                <asp:HiddenField ID="OnlineScheduleCandidate_skillHiddenField" runat="server" />
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="OnlineScheduleCandidate_skillChangesMadeUpdatePanel" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:CheckBox ID="OnlineScheduleCandidate_skillChangesCheckBox" runat="server" SkinID="sknMyRecordCheckBox"
                                                                                    Checked="false" Enabled="false" Text="Is the change is specific for this candidate or is to be made to the interview" />
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="OnlineScheduleCandidate_skillAssociatedGridView" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- End Skill Association -->
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- Reminder Section -->
                    <tr>
                        <td>
                            <div id="OnlineScheduleCandidate_reminderSectionDiv" runat="server" style="display: block">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr runat="server">
                                        <td class="header_bg">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="OnlineScheduleCandidate_reminderLiteral" runat="server" Text="Set Reminder"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <div runat="server" style="height: 30px; overflow: auto;">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 40%" valign="top">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="td_height_10">
                                                                        <asp:RadioButtonList ID="OnlineScheduleCandidate_remainderRadioButtonList" runat="server"
                                                                            SkinID="sknSkillRadioButtonList" AutoPostBack="false" RepeatColumns="8">
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <!-- End Reminder Section -->
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="OnlineScheduleCandidate_scheduleUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left" style="width: 100px">
                                                <asp:Button ID="OnlineScheduleCandidate_sendRequestButton" runat="server" Text="Send Request"
                                                    SkinID="sknButtonId" ValidationGroup="a" OnClick="OnlineScheduleCandidate_sendRequestButton_Click" />
                                                <asp:Button ID="OnlineScheduleCandidate_scheduleButton" runat="server" Text="Schedule"
                                                    SkinID="sknButtonId" OnClick="OnlineScheduleCandidate_scheduleButton_Click" />
                                            </td>
                                            <td align="left">
                                                <asp:LinkButton ID="OnlineScheduleCandidate_cancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="OnlineScheduleCandidate_cancelLinkButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="OnlineScheduleCandidate_createTimeSlotLinkButton" />
                                    <asp:AsyncPostBackTrigger ControlID="OnlineScheduleCandidate_requestTimeSlotLinkButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="OnlineScheduleCandidate_assignTimeUpdatePanel" UpdateMode="Always"
                    runat="server">
                    <ContentTemplate>
                        <span style="display: none">
                            <asp:Button ID="OnlineSchduleCandidate_assignTimeHiddenButton" runat="server" />
                        </span>
                        <asp:Panel ID="OnlineSchduleCandidate_assignTimePanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_request_time">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" class="popup_header_text" valign="middle">
                                                    <asp:Label ID="ScheduleCandidate_cancelReasonLiteral" runat="server" Text="Select Time"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:ImageButton ID="ScheduleCandidate_cancelReasonCancelImageButton" runat="server"
                                                        SkinID="sknCloseImageButton" OnClick="ScheduleCandidate_cancelReasonCancelImageButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table border="0" cellpadding="0" cellspacing="0" class="popupcontrol_question_inner_bg"
                                            width="100%">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td style="width: 5%; height: 23px" align="left">
                                                                <asp:Label ID="OnlineScheduleCandidate_addRequestFromLabel" runat="server" Text="From"
                                                                    EnableViewState="false" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 8%" align="left">
                                                                <asp:DropDownList ID="OnlineScheduleCandidate_addRequestFromDropDownList" runat="server"
                                                                    Width="90%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 5%" align="right">
                                                                <asp:Label ID="OnlineScheduleCandidate_addRequestToLabel" runat="server" Text="To"
                                                                    SkinID="sknLabelFieldHeaderText" EnableViewState="false"></asp:Label><span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 8%" align="right">
                                                                <asp:DropDownList ID="OnlineScheduleCandidate_addRequestToDropDownList" runat="server"
                                                                    Width="90%">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" style="padding-left: 5px">
                                                    <asp:Button ID="OnlineSchduleCandidate_addRequestTimeButton" OnClick="OnlineSchduleCandidate_addRequestTimeButton_Click"
                                                        Text="Add" runat="server" SkinID="sknButtonId" />
                                                    <asp:LinkButton ID="OnlineSchduleCandidate_cancelRequestButton" runat="server"
                                                        SkinID="sknPopupLinkButton" OnClick="OnlineSchduleCandidate_cancelRequestButton_Click"
                                                        Text="Cancel"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineSchduleCandidate_assignTimeModalPopupExtender"
                            runat="server" PopupControlID="OnlineSchduleCandidate_assignTimePanel"
                            TargetControlID="OnlineSchduleCandidate_assignTimeHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <!-- Delete associated interview skill confirmation popup -->
        <tr>
            <td>
                <asp:UpdatePanel ID="OnlineScheduleCandidate_skillDeleteUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="OnlineScheduleCandidate_skillDeleteHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="OnlineScheduleCandidate_skillDeletePopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="OnlineScheduleCandidate_skillDeleteConfirmMsgControl"
                                runat="server" OnOkClick="OnlineScheduleCandidate_skillDeleteConfirmMsgControl_okClick"
                                OnCancelClick="OnlineScheduleCandidate_skillDeleteConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="OnlineScheduleCandidate_skillDeleteHiddenFiled" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineScheduleCandidate_skillDeleteModalPopupExtender"
                            runat="server" PopupControlID="OnlineScheduleCandidate_skillDeletePopupPanel"
                            TargetControlID="OnlineScheduleCandidate_skillDeleteHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="OnlineScheduleCandidate_bottomSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="OnlineScheduleCandidate_candidateInterviewIDHiddenField" runat="server" />
                        <asp:HiddenField ID="OnlineScheduleCandidate_chatRoomKeyHiddenField" runat="server" />
                        <asp:HiddenField ID="OnlineScheduleCandidate_slotKeyHiddenField" runat="server" />
                        <asp:HiddenField ID="OnlineScheduleCandidate_deleteSkillIDHiddenField" runat="server" />
                        <asp:Label ID="OnlineScheduleCandidate_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineScheduleCandidate_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
