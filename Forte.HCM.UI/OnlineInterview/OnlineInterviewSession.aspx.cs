﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineInterviewSession.aspx.cs
// File that represents the user interface layout and functionalities
// for the OnlineInterviewSession page. This page helps in searching  
// online interview and perform actions against that.  

#endregion Header

#region Directives                                                             

using System;
using System.Collections;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;


using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the OnlineInterviewSession page. This page helps in searching  
    /// online interview  and perform actions against that. This class
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class OnlineInterviewSession : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Search Interview");

                Page.Form.DefaultButton = OnlineInterviewSession_searchButton.UniqueID;

                OnlineInterviewSession_searchResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    OnlineInterviewSession_interviewDetailGridViewDiv.ClientID + "','" +
                    OnlineInterviewSession_searchCriteriasDiv.ClientID + "','" +
                    OnlineInterviewSession_searchResultsUpSpan.ClientID + "','" +
                    OnlineInterviewSession_searchResultsDownSpan.ClientID + "','" +
                    OnlineInterviewSession_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
                
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    // Subscribes to client side event handlers.
                    SubscribeClientSideEventHandlers();

                    ViewState["SORT_ORDER"] = SortType.Descending;
                    ViewState["SORT_FIELD"] = "CREATEDDATE";

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_ONLINE_INTERVIEW] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_ONLINE_INTERVIEW]
                            as OnlineInterviewSessionSearchCriteria);
                }

                // Subscribes to paging event.
                OnlineInterviewSession_pagingNavigator.PageNumberClick += new 
                    PageNavigator.PageNumberClickEventHandler(OnlineInterviewSession_pagingNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that holds the event data.
        /// </param>
        private void OnlineInterviewSession_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Search sessions for the given page number.
                SearchInerview(e.PageNumber);

                ViewState["PAGENUMBER"] = e.PageNumber;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the search button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewSession_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Search sessions for the given page number.
                ClearLabelMessage();
                SearchInerview(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// It will clear all the session values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnlineInterviewSession_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/InterviewHome.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the reset link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewSession_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                base.ClearSearchCriteriaSession();
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void OnlineInterviewSession_resultsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (OnlineInterviewSession_resultsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void OnlineInterviewSession_resultsGridView_RowCommand
           (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditInterview")
                {
                    Response.Redirect(string.Format("~/OnlineInterview/OnlineInterviewCreation.aspx?m=3&s=0&interviewkey={0}&parentpage=S_OITSN",
                        e.CommandArgument), false);
                }
                else if (e.CommandName == "CopyInterview")
                {
                    OnlineInterviewSession_interviewSaveAsControl.UserID = base.userID;
                    OnlineInterviewSession_interviewSaveAsControl.InterviewKey = e.CommandArgument.ToString().Trim();
                    OnlineInterviewSession_interviewSaveAs_ModalPopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void OnlineInterviewSession_resultsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                OnlineInterviewSession_pagingNavigator.Reset();

                // Search interview for the given page number.
                SearchInerview(1);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void OnlineInterviewSession_resultsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                    OnlineInterviewSession_bottomErrorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            return true;
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="interviewSearchCriteria">
        /// A <see cref="TestSearchCriteria"/> that contains the interview search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(OnlineInterviewSessionSearchCriteria interviewSearchCriteria)
        {
            if (interviewSearchCriteria.Interviewkey != null)
                OnlineInterviewSession_interviewKeyTextBox.Text = interviewSearchCriteria.Interviewkey.Trim();

            if (interviewSearchCriteria.InterviewName != null)
                OnlineInterviewSession_interviewNameTextBox.Text = interviewSearchCriteria.InterviewName.Trim();

            if (interviewSearchCriteria.PositionProfileName != null)
            {
                OnlineInterviewSession_positionProfileTextBox.Text = interviewSearchCriteria.PositionProfileName.Trim();
                OnlineInterviewSession_positionProfileIDHiddenField.Value = interviewSearchCriteria.PositionProfileID.ToString();
            }

            if (interviewSearchCriteria.InterviewCreator != null)
            {
                OnlineInterviewSession_interviewCreatorTextBox.Text = interviewSearchCriteria.InterviewCreator.Trim();
                OnlineInterviewSession_interviewCreatorIDHiddenField.Value = interviewSearchCriteria.InterviewCreatorID.ToString();
            }

            OnlineInterviewSession_isMaximizedHiddenField.Value = interviewSearchCriteria.IsMaximized == true ? "Y" : "N";
            ViewState["SORT_FIELD"] = interviewSearchCriteria.SortExpression;
            ViewState["SORT_ORDER"] = interviewSearchCriteria.SortDirection;

            // Apply search
            SearchInerview(interviewSearchCriteria.CurrentPage);

            // Highlight the last page number which is stored in session
            // when the page is launched from somewhere else.
            OnlineInterviewSession_pagingNavigator.MoveToPage(interviewSearchCriteria.CurrentPage);
        }


        /// <summary>
        /// Method that subscribes to the client side event handlers
        /// </summary>
        private void SubscribeClientSideEventHandlers()
        {
            // Position profile.
            OnlineInterviewSession_positionProfileImageButton.Attributes.Add("onclick",
                "return LoadPositionProfileName('" + OnlineInterviewSession_positionProfileTextBox.ClientID + "','" +
                OnlineInterviewSession_positionProfileIDHiddenField.ClientID + "');");

            OnlineInterviewSession_interviewCreatorImageButton.Attributes.Add("onclick",
                      "return SearchCorporateUsers('" + OnlineInterviewSession_interviewCreatorIDHiddenField.ClientID +
                      "','" + OnlineInterviewSession_interviewCreatorTextBox.ClientID + "');");
        }

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void SearchInerview(int pageNumber)
        {
            int totalRecords = 0;
            OnlineInterviewSessionSearchCriteria interviewSearchCriteria = 
                new OnlineInterviewSessionSearchCriteria();
            
            interviewSearchCriteria.SearchTenantID = base.tenantID;

            interviewSearchCriteria.Interviewkey = OnlineInterviewSession_interviewKeyTextBox.Text.Trim() == "" ?
                null : OnlineInterviewSession_interviewKeyTextBox.Text.Trim();
            interviewSearchCriteria.InterviewName = OnlineInterviewSession_interviewNameTextBox.Text.Trim() == "" ?
                null : OnlineInterviewSession_interviewNameTextBox.Text.Trim();


            if (OnlineInterviewSession_interviewCreatorTextBox.Text.Trim() != string.Empty)
                interviewSearchCriteria.InterviewCreator = OnlineInterviewSession_interviewCreatorTextBox.Text.Trim();
            else
                interviewSearchCriteria.InterviewCreator = null;

            if (OnlineInterviewSession_interviewCreatorIDHiddenField.Value.Trim().Length != 0)
                interviewSearchCriteria.InterviewCreatorID = int.Parse(OnlineInterviewSession_interviewCreatorIDHiddenField.Value.Trim());
            else
                interviewSearchCriteria.InterviewCreatorID = 0;

            if (OnlineInterviewSession_positionProfileIDHiddenField.Value == null ||
                OnlineInterviewSession_positionProfileIDHiddenField.Value.Trim().Length == 0)
            {
                interviewSearchCriteria.PositionProfileID = 0;
            }
            else
            {
                interviewSearchCriteria.PositionProfileID = Convert.ToInt32(OnlineInterviewSession_positionProfileIDHiddenField.Value);
            }
            interviewSearchCriteria.PositionProfileName = OnlineInterviewSession_positionProfileTextBox.Text;


            interviewSearchCriteria.CurrentPage = pageNumber;
            interviewSearchCriteria.IsMaximized =
                OnlineInterviewSession_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

            interviewSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
            interviewSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

            
            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_ONLINE_INTERVIEW] = interviewSearchCriteria;

            List<OnlineInterviewSessionDetail> inerviewDetail = new List<OnlineInterviewSessionDetail>();
            inerviewDetail = new OnlineInterviewBLManager().GetOnlineInterviewDetail(interviewSearchCriteria, pageNumber, base.GridPageSize,
                out totalRecords, ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]));

            if (inerviewDetail == null || inerviewDetail.Count == 0)
            {
                base.ShowMessage(OnlineInterviewSession_topErrorMessageLabel,
                        OnlineInterviewSession_bottomErrorMessageLabel,
                        Resources.HCMResource.Common_Empty_Grid);
                OnlineInterviewSession_resultsGridView.DataSource = null;
                OnlineInterviewSession_resultsGridView.DataBind();
                OnlineInterviewSession_pagingNavigator.TotalRecords = 0;
                OnlineInterviewSession_interviewDetailGridViewDiv.Visible = false;
            }
            else
            {
                OnlineInterviewSession_resultsGridView.DataSource = inerviewDetail;
                OnlineInterviewSession_resultsGridView.DataBind();
                OnlineInterviewSession_pagingNavigator.PageSize = base.GridPageSize;
                OnlineInterviewSession_pagingNavigator.TotalRecords = totalRecords;
                OnlineInterviewSession_interviewDetailGridViewDiv.Visible = true;
            }
        }

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            OnlineInterviewSession_topSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewSession_bottomSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewSession_topErrorMessageLabel.Text = string.Empty;
            OnlineInterviewSession_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(OnlineInterviewSession_isMaximizedHiddenField.Value) &&
                OnlineInterviewSession_isMaximizedHiddenField.Value == "Y")
            {
                OnlineInterviewSession_searchCriteriasDiv.Style["display"] = "none";
                OnlineInterviewSession_searchResultsUpSpan.Style["display"] = "block";
                OnlineInterviewSession_searchResultsDownSpan.Style["display"] = "none";
                OnlineInterviewSession_interviewDetailGridViewDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                OnlineInterviewSession_searchCriteriasDiv.Style["display"] = "block";
                OnlineInterviewSession_searchResultsUpSpan.Style["display"] = "none";
                OnlineInterviewSession_searchResultsDownSpan.Style["display"] = "block";
                OnlineInterviewSession_interviewDetailGridViewDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods

        #region Public Methods                                                 
        
        /// <summary>
        /// Method that will show feature pop up extender
        /// </summary>
        public void ShowSaveModalPopUp()
        {
            OnlineInterviewSession_interviewSaveAsControl.UserID = base.userID;
            OnlineInterviewSession_interviewSaveAs_ModalPopupExtender.Show();
        }

        #endregion Public Methods
    }
}