﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewRequestStatus.aspx.cs
// File that represents the user interface layout and functionalities
// for the SearchTimeSlotRequest page. This page helps in viewing and 
// processing the time slot requests made by users. 

#endregion Header

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchTimeSlotRequest page. This page helps in viewing and 
    /// processing the time slot requests made by users. This class inherits 
    /// the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ViewRequestStatus : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> that holds the status that indicates whether
        /// to display page as search time slots or pending requests.
        /// </summary>
        /// <remarks>
        /// The value 'P' indicates pending requests and empty indicates search 
        /// time slots.
        /// </remarks>
        private string type = string.Empty;

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("View Time Slot Request Status");
                
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    // Add handler to maximize/restore image click.
                    ViewRequestStatus_searchResultsTR.Attributes.Add("onclick",
                       "ExpandOrRestore('" +
                       ViewRequestStatus_timeSlotsGridViewDiv.ClientID + "','" +
                       ViewRequestStatus_searchCriteriasDiv.ClientID + "','" +
                       ViewRequestStatus_searchResultsUpSpan.ClientID + "','" +
                       ViewRequestStatus_searchResultsDownSpan.ClientID + "','" +
                       ViewRequestStatus_isMaximizedHiddenField.ClientID + "','" +
                       RESTORED_HEIGHT + "','" +
                       EXPANDED_HEIGHT + "')");

                    // Load the initiated by field by default by the logged in 
                    // user ID.
                    ViewRequestStatus_initiatedByHiddenField.Value = base.userID.ToString();

                    // Get user detail.
                    UserDetail userDetail = new CommonBLManager().GetUserDetail(base.userID);

                    if (userDetail != null)
                    {
                        ViewRequestStatus_initiatedByTextBox.Text = userDetail.FirstName;
                    }

                    // Show the initiated by select icon for corporate admin users only.
                    if (base.isCustomerAdmin)
                    {
                        ViewRequestStatus_initiatedByImageButton.Visible = true;

                        // Add handler to initiated by image button.
                        ViewRequestStatus_initiatedByImageButton.Attributes.Add("onclick",
                           "return SearchCorporateUsers('" + ViewRequestStatus_initiatedByHiddenField.ClientID +
                           "','" + ViewRequestStatus_initiatedByTextBox.ClientID + "');");
                    }
                    else
                    {
                        ViewRequestStatus_initiatedByImageButton.Visible = false;
                    }

                    // Add handler to assessor image button.
                    ViewRequestStatus_assessorImageButton.Attributes.Add("onclick",
                       "return ShowSearchAssessor('" + ViewRequestStatus_assessorIDHiddenField.ClientID +
                       "','" + ViewRequestStatus_assessorTextBox.ClientID + "','', 'N');");

                    // Add handler to session key image button.
                    ViewRequestStatus_sessionImageButton.Attributes.Add("onclick", "javascript:return ShowOnlineInterviewSession('"
                        + ViewRequestStatus_sessionKeyHiddenField.ClientID + "','" + ViewRequestStatus_sessionTextBox.ClientID + "')");

                    // Load values.
                    LoadValues();

                    ViewState["SORT_ORDER"] = SortType.Descending;
                    ViewState["SORT_FIELD"] = "REQUEST_STATUS";
                }

                // Subscribes to paging event.
                ViewRequestStatus_pagingNavigator.PageNumberClick += new 
                    PageNavigator.PageNumberClickEventHandler(ViewRequestStatus_pagingNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that holds the event data.
        /// </param>
        private void ViewRequestStatus_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load time slots for specific page number.
                LoadTimeSlotRequests(e.PageNumber);
                ViewState["PAGENUMBER"] = e.PageNumber;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the search button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ViewRequestStatus_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();
                ViewRequestStatus_pagingNavigator.Reset();
                ViewState["PAGENUMBER"] = "1";
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "REQUEST_STATUS";

                // Load time slots for first page.
                LoadTimeSlotRequests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the availability request button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is called when the save button is clicked in the 
        /// availability request window. This will just show a success message 
        /// to the user.
        /// </remarks>
        protected void ViewRequestStatus_availabilityRequestButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewRequestStatus_topSuccessMessageLabel.Text = string.Empty;
                ViewRequestStatus_bottomSuccessMessageLabel.Text = string.Empty;

                // Show a success message.
                base.ShowMessage(ViewRequestStatus_topSuccessMessageLabel,
                    ViewRequestStatus_bottomSuccessMessageLabel, "Time slot requested successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// It will clear all the session values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewRequestStatus_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/InterviewHome.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the reset link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ViewRequestStatus_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                base.ClearSearchCriteriaSession();
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void ViewRequestStatus_timeSlotsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (ViewRequestStatus_timeSlotsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void ViewRequestStatus_timeSlotsGridView_RowCommand
           (object sender, GridViewCommandEventArgs e)
        {
            try
            {
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void ViewRequestStatus_timeSlotsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {

                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                ViewRequestStatus_pagingNavigator.Reset();

                // Load time slots for first page.
                LoadTimeSlotRequests(1);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ViewRequestStatus_timeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HtmlContainerControl ViewRequestStatus_timeSlotsGridView_detailsDiv =
                        (HtmlContainerControl)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_detailsDiv");

                    HtmlAnchor ViewRequestStatus_timeSlotsGridView_detailsViewFocusDownLink =
                        (HtmlAnchor)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_detailsViewFocusDownLink");

                    LinkButton ViewRequestStatus_timeSlotsGridView_skillsLinkButton =
                        (LinkButton)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_skillsLinkButton");

                    // Add handler to show/hide expanded view link button.
                    ViewRequestStatus_timeSlotsGridView_skillsLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                        ViewRequestStatus_timeSlotsGridView_detailsDiv.ClientID + "','" + ViewRequestStatus_timeSlotsGridView_detailsViewFocusDownLink.ClientID + "')");

                    HiddenField ViewRequestStatus_timeSlotsGridView_skillIDHiddenField =
                        (HiddenField)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_skillIDHiddenField");

                    HiddenField ViewRequestStatus_timeSlotsGridView_assessorIDHiddenField =
                        (HiddenField)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_assessorIDHiddenField");

                    HiddenField ViewRequestStatus_timeSlotsGridView_sessionKeyHiddenField =
                        (HiddenField)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_sessionKeyHiddenField");

                    ImageButton ViewRequestStatus_timeSlotsGridView_requestTimeSlotImageButton =
                        (ImageButton)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_requestTimeSlotImageButton");

                    // Add handler to request time slot image button.
                    ViewRequestStatus_timeSlotsGridView_requestTimeSlotImageButton.Attributes.Add("onclick",
                        "return ShowRequestSessionTimeSlot('" + ViewRequestStatus_timeSlotsGridView_assessorIDHiddenField.Value +
                        "','" + ViewRequestStatus_timeSlotsGridView_skillIDHiddenField.Value + "','" +
                        ViewRequestStatus_timeSlotsGridView_sessionKeyHiddenField.Value + "','" + 
                        ViewRequestStatus_availabilityRequestButton.ClientID + "');");

                    ImageButton ViewRequestStatus_timeSlotsGridView_emailAssessorImageButton =
                       (ImageButton)e.Row.FindControl("ViewRequestStatus_timeSlotsGridView_emailAssessorImageButton");

                    // Add handler to email assessor image button.
                    ViewRequestStatus_timeSlotsGridView_emailAssessorImageButton.Attributes.Add("onclick",
                        "return ShowEmailAssessor('" + ViewRequestStatus_timeSlotsGridView_assessorIDHiddenField.Value + "','VRS');");
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            // Load status dropdown list.
            ViewRequestStatus_statusDropDownList.Items.Clear();
            ViewRequestStatus_statusDropDownList.Items.Add(new ListItem("All", ""));
            ViewRequestStatus_statusDropDownList.Items.Add(new ListItem
                (Constants.TimeSlotRequestStatus.PENDING_NAME, Constants.TimeSlotRequestStatus.PENDING_CODE));
            ViewRequestStatus_statusDropDownList.Items.Add(new ListItem
                (Constants.TimeSlotRequestStatus.ACCEPTED_NAME, Constants.TimeSlotRequestStatus.ACCEPTED_CODE));
            ViewRequestStatus_statusDropDownList.Items.Add(new ListItem
                (Constants.TimeSlotRequestStatus.REJECTED_NAME, Constants.TimeSlotRequestStatus.REJECTED_CODE));

            if (type == "P")
            {
                // By default select the 'Pending' value.
                ViewRequestStatus_statusDropDownList.SelectedValue = Constants.
                    TimeSlotRequestStatus.PENDING_CODE;
            }
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            return true;
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the time slots for the given page number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holdst the page number.
        /// </param>
        private void LoadTimeSlotRequests(int pageNumber)
        {
            int totalRecords = 0;

            // Construct search criteria.
            AssessorTimeSlotSearchCriteria criteria = new AssessorTimeSlotSearchCriteria();

            criteria.SchedulerID = base.userID;

            if (!Utility.IsNullOrEmpty(ViewRequestStatus_statusDropDownList.SelectedValue))
                criteria.RequestStatus = ViewRequestStatus_statusDropDownList.SelectedValue.Trim();
            else
                criteria.RequestStatus = string.Empty;

            criteria.RequestedDate = ViewRequestStatus_requestedDateTextBox.Text.Trim() == string.Empty ?
                DateTime.MinValue : Convert.ToDateTime(ViewRequestStatus_requestedDateTextBox.Text.Trim());

            criteria.InitiatedDate = ViewRequestStatus_initiatedDateTextBox.Text.Trim() == string.Empty ?
                DateTime.MinValue : Convert.ToDateTime(ViewRequestStatus_initiatedDateTextBox.Text.Trim());

            if (!Utility.IsNullOrEmpty(ViewRequestStatus_sessionKeyHiddenField.Value))
                criteria.SessionKey = ViewRequestStatus_sessionKeyHiddenField.Value.Trim();
            else
                criteria.SessionKey = null;

            if (!Utility.IsNullOrEmpty(ViewRequestStatus_assessorIDHiddenField.Value))
                criteria.AssessorID = Convert.ToInt32(ViewRequestStatus_assessorIDHiddenField.Value);

            criteria.Skill = ViewRequestStatus_skillTextBox.Text.Trim() == string.Empty ?
                null : ViewRequestStatus_skillTextBox.Text.Trim();

            criteria.Comments = ViewRequestStatus_commentsTextBox.Text.Trim() == string.Empty ?
                null : ViewRequestStatus_commentsTextBox.Text.Trim();

            List<AssessorTimeSlotDetail> timeSlots = new AssessorBLManager().GetSchedulerTimeSlotRequest
                (criteria, pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                (SortType)ViewState["SORT_ORDER"], out totalRecords);

            if (timeSlots == null || timeSlots.Count == 0)
            {
                base.ShowMessage(ViewRequestStatus_topErrorMessageLabel,
                    ViewRequestStatus_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }

            ViewRequestStatus_timeSlotsGridView.DataSource = timeSlots;
            ViewRequestStatus_timeSlotsGridView.DataBind();
            ViewRequestStatus_pagingNavigator.TotalRecords = totalRecords;
            ViewRequestStatus_pagingNavigator.PageSize = base.GridPageSize;
        }

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            ViewRequestStatus_topSuccessMessageLabel.Text = string.Empty;
            ViewRequestStatus_bottomSuccessMessageLabel.Text = string.Empty;
            ViewRequestStatus_topErrorMessageLabel.Text = string.Empty;
            ViewRequestStatus_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(ViewRequestStatus_isMaximizedHiddenField.Value) &&
                ViewRequestStatus_isMaximizedHiddenField.Value == "Y")
            {
                ViewRequestStatus_searchCriteriasDiv.Style["display"] = "none";
                ViewRequestStatus_searchResultsUpSpan.Style["display"] = "block";
                ViewRequestStatus_searchResultsDownSpan.Style["display"] = "none";
                ViewRequestStatus_timeSlotsGridViewDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                ViewRequestStatus_searchCriteriasDiv.Style["display"] = "block";
                ViewRequestStatus_searchResultsUpSpan.Style["display"] = "none";
                ViewRequestStatus_searchResultsDownSpan.Style["display"] = "block";
                ViewRequestStatus_timeSlotsGridViewDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Method that search for time slots.
        /// </summary>
        private void SearchTimeSlots()
        {
           
        }

        #endregion Private Methods
    }
}