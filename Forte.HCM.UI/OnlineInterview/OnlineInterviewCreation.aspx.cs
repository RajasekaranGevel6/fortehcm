#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineInterviewCreation.cs
// File that represents the user interface for the create a new online interview 
// creation. This will interact with the OnlineInterviewBLManager to insert online interview 
// creation details to the database.

#endregion Header

#region Directives

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for OnlineInterviewCreation page. This is used
    /// to create a online interview creation by providing interview name, 
    /// position profie, list of skill associated etc.
    /// Also this page provides some set of links in the online interview creation 
    /// to view the assessor, question set, schedule candidate screens
    /// This class is inherited from
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class OnlineInterviewCreation : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <see cref="string"/> that hold the interview key.
        /// </summary>
        private string interviewKey = string.Empty;

        #endregion Private Variables

        #region Event Handlers
       
        /// <summary>
        /// Handler method that is called when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Clear Message Controls
                ClearLabelMessage();
                
                Page.Form.DefaultButton = OnlineInterviewCreation_topSaveButton.UniqueID;
                
                //Set the page title
                Master.SetPageCaption("Interview Creation");

                // Check if interview key is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["interviewkey"]))
                {
                    // Get interview key.
                    interviewKey = Request.QueryString["interviewkey"].ToString();
                    OnlineInterviewCreation_interviewKeyHiddenField.Value = interviewKey;
                }
                else if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_interviewKeyHiddenField.Value))
                {
                    // Get interview key.
                    interviewKey = OnlineInterviewCreation_interviewKeyHiddenField.Value.Trim();
                }

                if (!Utility.IsNullOrEmpty(interviewKey))
                {
                    OnlineInterviewCreation_questionSetLinkButton.Enabled = true;
                    OnlineInterviewCreation_changeAssessorLinkButton.Enabled = true;
                    OnlineInterviewCreation_scheduleCandidatesLinkButton.Enabled = true;
                    OnlineInterviewCreation_saveAsImageButton.Visible = true;
                }

                if (!IsPostBack)
                {
                    // Clear session.
                    Session["SKILL_LIST"] = null;

                    //Set interview copied successfull message
                    if (!Utility.IsNullOrEmpty(Request.QueryString["copied"]) && 
                        Request.QueryString["copied"].Trim().ToUpper()=="Y")
                    {
                        // Show success message.
                        base.ShowMessage(OnlineInterviewCreation_topSuccessMessageLabel,
                            "Interview copied successfully");
                    }

                    //Show/Hide interview id
                    OnlineInterviewCreation_positionProfileIDLabel.Visible = false;
                    OnlineInterviewCreation_interviewIDLabelValue.Visible = false;
                    OnlineInterviewCreation_interviewIDLabel.Visible = false;

                    // Set default focus field
                    Page.Form.DefaultFocus = OnlineInterviewCreation_interviewNameTextBox.UniqueID;
                    OnlineInterviewCreation_interviewNameTextBox.Focus();

                    OnlineInterviewCreation_positionProfileIDLabelValue.Visible = false;

                    int positionProfileID = 0;

                    if (!Utility.IsNullOrEmpty(interviewKey))
                    {
                        //Load the interview
                        BindInterviewDetail();

                        OnlineInterviewCreation_interviewIDLabel.Visible = true;
                        OnlineInterviewCreation_interviewIDLabelValue.Visible = true;

                        if (!Utility.IsNullOrEmpty(positionProfileID) && 
                            positionProfileID != 0)
                        {
                            OnlineInterviewCreation_positionProfileImageButton.Visible = false;
                            OnlineInterviewCreation_positionProfileHelpImageButton.Visible = false;
                            OnlineInterviewCreation_positionProfileTextBox.Enabled = false;
                        }
                    }

                    OnlineInterviewCreation_searchSkillImageButtonButton.Attributes.Add("onclick",
                       "return LoadSubjectLookUp('" + OnlineInterviewCreation_categoryIDHiddenField.ClientID + "','" +
                       OnlineInterviewCreation_skillTextBox.ClientID + "','" +
                       OnlineInterviewCreation_skillIDHiddenField.ClientID + "','" + OnlineInterviewCreation_skillHiddenField.ClientID + "','" +
                       OnlineInterviewCreation_refreshSearchButton.ClientID + "')");

                    OnlineInterviewCreation_positionProfileImageButton.Attributes.Add("onclick",
                    "return ShowPositionProfileLooKUp('" +
                    OnlineInterviewCreation_positionProfileTextBox.ClientID + "','" +
                    OnlineInterviewCreation_skillHiddenField.ClientID + "','" +
                    OnlineInterviewCreation_positionProfileIDHiddenField.ClientID + "','" +
                    OnlineInterviewCreation_positionProfileClientRequestIDHiddenField.ClientID + "','" +
                    OnlineInterviewCreation_refreshSearchButton.ClientID + "')");

                    //Skill Set
                    AssignSkillSet();

                    //Assessor List
                    AssignAssessorSet(1,base.GridPageSize);

                    //Candidate List
                    AssignCandidateSet();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    OnlineInterviewCreation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will help to validate input fields before they saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that contains the event data.
        /// </param>
        protected void OnlineInterviewCreation_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate the data
                if (!IsValidData())
                    return;

                string interviewKeyOut = string.Empty;

                OnlineInterviewSessionDetail onlineInterviewSessionDetail =
                    ConstructOnlineInterviewSessionDetail();

                List<SkillDetail> skillDetails= ConstructSkillDetail();

                new OnlineInterviewBLManager().SaveOnlineInterview(onlineInterviewSessionDetail,
                    skillDetails, base.userID, out interviewKeyOut);

                if (Utility.IsNullOrEmpty(OnlineInterviewCreation_interviewKeyHiddenField.Value))
                {
                    OnlineInterviewCreation_interviewKeyHiddenField.Value = interviewKeyOut;

                    OnlineInterviewCreation_displayInterviewSavedConfirmMsgControl.Message =
                        string.Format("Online Interview {0} Saved.<br><br>Do you want to continue to assign question set?",
                        interviewKeyOut);

                    OnlineInterviewCreation_displayInterviewSavedPopupPanel.Style.Add("height", "200px");
                    OnlineInterviewCreation_displayInterviewSavedConfirmMsgControl.Type = MessageBoxType.YesNo;
                    OnlineInterviewCreation_displayInterviewSavedConfirmMsgControl.Title = "Confirm Navigation";

                    OnlineInterviewCreation_displayInterviewSavedModalPopupExtender.Show();
                }
                else
                {
                    Response.Redirect("~/OnlineInterview/OnlineInterviewQuestionSet.aspx?m=3&s=0&interviewkey="
                          + interviewKeyOut + "&parentpage=S_OITSN", false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    OnlineInterviewCreation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void OnlineInterviewCreation_displayInterviewSavedConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/OnlineInterviewQuestionSet.aspx?m=3&s=0&interviewkey="
                            + interviewKey +"&parentpage=S_OITSN", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    OnlineInterviewCreation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method to refresh the grid
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// It will bind the selected skill to gridview
        /// </remarks>
        protected void OnlineInterviewCreation_refreshSearchButton_Click
          (object sender, EventArgs e)
        {
            try
            {
                 // Check if skill is entered.
                if(Utility.IsNullOrEmpty(OnlineInterviewCreation_skillHiddenField.Value.Trim()))
                {
                    base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                        "No skill entered to add");
                    return;
                }

                // Set position profile client request number
                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_positionProfileTextBox.Text))
                {
                    OnlineInterviewCreation_positionProfileIDLabel.Visible = true;
                    OnlineInterviewCreation_positionProfileIDLabelValue.Visible = true;
                    OnlineInterviewCreation_positionProfileIDLabelValue.Text = 
                        OnlineInterviewCreation_positionProfileClientRequestIDHiddenField.Value;
                }

                BindSubjects(OnlineInterviewCreation_skillHiddenField.Value.ToString().Trim());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    OnlineInterviewCreation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will perform reset/clear the input fields.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewCreation_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                OnlineInterviewCreation_interviewNameTextBox.Text = string.Empty;
                OnlineInterviewCreation_positionProfileTextBox.Text = string.Empty;
                OnlineInterviewCreation_skillTextBox.Text = string.Empty;
                OnlineInterviewCreation_instructionTextBox.Text = string.Empty;
                OnlineInterviewCreation_remarkTextBox.Text = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    OnlineInterviewCreation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called at the time of textchanged event is fired
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewCreation_skillTextBox_TextChanged(object sender,
            EventArgs e)
        {
            try
            {
                // Check if skill is entered.
                if (Utility.IsNullOrEmpty(OnlineInterviewCreation_skillTextBox.Text) && 
                    Utility.IsNullOrEmpty(OnlineInterviewCreation_skillHiddenField.Value.Trim()))
                {
                    base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                        "No skill added");
                    return;
                }

                string skill = string.Empty;

                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillTextBox.Text))
                    skill = OnlineInterviewCreation_skillTextBox.Text.Trim();
                else if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillHiddenField.Value.Trim()))
                    skill = OnlineInterviewCreation_skillHiddenField.Value.ToString().Trim();

                BindSubjects(skill);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    OnlineInterviewCreation_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the skills grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void OnlineInterviewCreation_skillsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteSkill")
                    return;

                int skillID = 0;

                if (!Utility.IsNullOrEmpty(e.CommandArgument))
                {
                    skillID = Convert.ToInt32(e.CommandArgument);
                }

                // Retrieve the skill details list from session.
                List<SkillDetail> skillDetails = null;
                if (Session["SKILL_LIST"] != null)
                {
                    skillDetails = Session["SKILL_LIST"] as List<SkillDetail>;
                }

                if (skillDetails == null || skillID == 0)
                {
                    base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, "No skill found to delete");
                    return;
                }

                if (!Utility.IsNullOrEmpty(interviewKey))
                {
                    // Show confirmation popup
                    OnlineInterviewCreation_deleteSkillIDHiddenField.Value = skillID.ToString();

                    ImageButton sourceButton = (ImageButton)e.CommandSource;
                    GridViewRow row = (GridViewRow)sourceButton.Parent.Parent;

                    Label skillName = (Label)row.FindControl("OnlineInterviewCreation_skillsGridView_skillNameLabel");

                    OnlineInterviewCreation_skillDeleteConfirmMsgControl.Message = 
                        string.Format("Are you sure to delete the skill {0}?",skillName.Text.Trim());
                    
                    OnlineInterviewCreation_skillDeletePopupPanel.Style.Add("height", "200px");
                    OnlineInterviewCreation_skillDeleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                    OnlineInterviewCreation_skillDeleteConfirmMsgControl.Title = "Warning";

                    OnlineInterviewCreation_skillDeleteModalPopupExtender.Show();
                    return;
                }

                DeleteSkillFromList(skillID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void OnlineInterviewCreation_skillDeleteConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                int skillID = 0;

                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_deleteSkillIDHiddenField.Value))
                {
                    skillID = Convert.ToInt32(OnlineInterviewCreation_deleteSkillIDHiddenField.Value);
                }

                // Delete from  ONLINE_INTERVIEW_SKILL table
                new OnlineInterviewBLManager().DeleteOnlineInterviewSkill(interviewKey, skillID);

                // Delete skill form list
                DeleteSkillFromList(skillID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void OnlineInterviewCreation_skillDeleteConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                OnlineInterviewCreation_deleteSkillIDHiddenField.Value = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the skills grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void OnlineInterviewCreation_skillsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when saveas image button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewCreation_saveAsImageButton_Click(object sender, 
            ImageClickEventArgs e)
        {
            try
            {
                ShowSaveModalPopUp();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when question set link is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewCreation_questionSetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/OnlineInterviewQuestionSet.aspx?m=3&s=0&interviewkey="
                           + interviewKey + "&parentpage=S_OITSN", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when change/add assessor link is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewCreation_changeAssessorLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/OnlineInterviewAssessor.aspx?m=3&s=0&interviewkey="
                          + interviewKey + "&parentpage=S_OITSN", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when schedule candidate link is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewCreation_scheduleCandidatesLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/OnlineInterviewSchduleCandidate.aspx?m=3&s=0&interviewkey="
                          + interviewKey + "&parentpage=S_OITSN", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when schedule candidate link is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewCreation_autoCalculateLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                AutoPopulateSkillWeightage();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when the item data bound  
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will assign the assessor profile link
        /// </remarks>
        protected void OnlineInterviewCreation_assessorListDataList_ItemDataBound(object sender,
           DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                        e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Set the assessor profile link
                    LinkButton OnlineInterviewCreation_assessorListDataList_nameLinkButton =
                        (LinkButton)e.Item.FindControl("OnlineInterviewCreation_assessorListDataList_nameLinkButton");

                    HiddenField OnlineInterviewCreation_assessorListDataList_assessorIDHiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterviewCreation_assessorListDataList_assessorIDHiddenField");

                    OnlineInterviewCreation_assessorListDataList_nameLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowAssessorProfile('" + OnlineInterviewCreation_assessorListDataList_assessorIDHiddenField.Value.Trim() + "');");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the item data bound  
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will assign the candidate dashboard link
        /// </remarks>
        protected void OnlineInterviewCreation_candidateAssociatedDataList_ItemDataBound(object sender,
           DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                        e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Set the assessor profile link
                    HyperLink OnlineInterviewCreation_candidateAssociatedDataList_candidateHyperLink =
                        (HyperLink)e.Item.FindControl("OnlineInterviewCreation_candidateAssociatedDataList_candidateHyperLink");

                    HiddenField OnlineInterviewCreation_candidateAssociatedDataList_candidateIDHiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterviewCreation_candidateAssociatedDataList_candidateIDHiddenField");

                    if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_candidateAssociatedDataList_candidateIDHiddenField.Value) && 
                        Convert.ToInt32(OnlineInterviewCreation_candidateAssociatedDataList_candidateIDHiddenField.Value)!=0)
                    {
                        OnlineInterviewCreation_candidateAssociatedDataList_candidateHyperLink.NavigateUrl =
                                "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&puid=" + 
                                OnlineInterviewCreation_candidateAssociatedDataList_candidateIDHiddenField.Value;
                    }  
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    exp.Message);
            }
        }

        #endregion Event Handlers
        
        #region Private Methods

        /// <summary>
        /// Method to delete the skill from skill list
        /// </summary>
        /// <param name="skillID"></param>
        private void DeleteSkillFromList(int skillID)
        {
            List<SkillDetail> skillDetail = null;

            if (Session["SKILL_LIST"] == null)
                skillDetail = new List<SkillDetail>();
            else
                skillDetail = Session["SKILL_LIST"] as List<SkillDetail>;

            // Delete the skill from skill list
            skillDetail.RemoveAll(s => s.SkillID == skillID);

            // Keep the skills in session.
            if (skillDetail.Count == 0)
                Session["SKILL_LIST"] = null;
            else
                Session["SKILL_LIST"] = skillDetail;

            // Assign the skills to the grid view.
            OnlineInterviewCreation_skillsGridView.DataSource = skillDetail;
            OnlineInterviewCreation_skillsGridView.DataBind();
            LoadSkillWeigtageSkillLevel();

            //Auto Populate Skill Weightage
            AutoPopulateSkillWeightage();

            // Show success message.
            base.ShowMessage(OnlineInterviewCreation_topSuccessMessageLabel, 
                OnlineInterviewCreation_bottomSuccessMessageLabel, "Skill deleted successfully");
        }

        /// <summary>
        /// Method to assign the existing skill set
        /// </summary>
        private void AssignSkillSet()
        {
            List<SkillDetail> skillDetail = new List<SkillDetail>();

            if (!Utility.IsNullOrEmpty(interviewKey))
            {
                skillDetail = new OnlineInterviewBLManager().
                    GetSkillByOnlineInterviewKey(interviewKey);

                if (skillDetail != null && skillDetail.Count > 0)
                {
                    OnlineInterviewCreation_skillDataList.DataSource = skillDetail;
                    OnlineInterviewCreation_skillDataList.DataBind();
                    OnlineInterviewCreation_noQuestionSetLabel.Visible = false;
                    OnlineInterviewCreation_noQuestionSetImage.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to assign the assessor set
        /// </summary>
        /// <param name="pageNo">The page no</param>
        /// <param name="pageSize">The page size</param>
        private void AssignAssessorSet(int pageNo, int pageSize)
        {
            int totalRecords = 0;
            List<AssessorTimeSlotDetail> assessorList = new List<AssessorTimeSlotDetail>();

            if (!Utility.IsNullOrEmpty(interviewKey))
            {
                assessorList = new OnlineInterviewAssessorBLManager().
                      GetOnlineInterviewAssessorList(interviewKey, pageNo, pageSize, out totalRecords);

                if (assessorList != null)
                {
                    OnlineInterviewCreation_assessorListDataList.DataSource = assessorList;
                    OnlineInterviewCreation_assessorListDataList.DataBind();
                    OnlineInterviewCreation_noAssessorImage.Visible = false;
                    OnlineInterviewCreation_noAssessorLabel.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to bind the associated candidate list
        /// </summary>
        private void AssignCandidateSet()
        {
            List<CandidateDetail> candidateDetail = new List<CandidateDetail>();

            if (!Utility.IsNullOrEmpty(interviewKey))
            {
                candidateDetail = new OnlineInterviewBLManager().
                      GetOnlineInterviewCandidates(interviewKey);

                if (candidateDetail != null)
                {
                    OnlineInterviewCreation_candidateAssociatedDataList.DataSource = candidateDetail;
                    OnlineInterviewCreation_candidateAssociatedDataList.DataBind();
                    OnlineInterviewCreation_noCandidateImage.Visible = false;
                    OnlineInterviewCreation_noCandidateLabel.Visible = false;
                }
            }
        }

        /// <summary>
        /// A Method that gets the position profile keywords by removing there
        /// weightages
        /// </summary>
        /// <param name="KeyWordWithWeightages">
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// among with their weightages
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// by removing their weightages
        /// </returns>
        private string GetPositionProfileKeys(string KeyWordWithWeightages)
        {
            StringBuilder sbKeywords = null;
            string[] strSplitKeywords = null;
            try
            {
                strSplitKeywords = KeyWordWithWeightages.Split(',');
                sbKeywords = new StringBuilder();
                for (int i = 0; i < strSplitKeywords.Length; i++)
                    if (strSplitKeywords[i].IndexOf('[') != -1)
                        sbKeywords.Append(strSplitKeywords[i].Substring(0, strSplitKeywords[i].IndexOf('[')) + ",");
                    else
                        sbKeywords.Append(strSplitKeywords[i] + ",");
                return sbKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (strSplitKeywords != null) strSplitKeywords = null;
                if (sbKeywords != null) sbKeywords = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Method that will preview the online interview creation details
        /// on the save button.
        /// </summary>
        /// <returns>Returns the <see cref="onlineInterviewSessionDetail"/> object</returns>
        private OnlineInterviewSessionDetail PreviewOnlineInterviewSession()
        {
            // Initialize the interview creation object
            OnlineInterviewSessionDetail onlineInterviewSessionDetail = 
                new OnlineInterviewSessionDetail();

            onlineInterviewSessionDetail.InterviewName = 
                OnlineInterviewCreation_interviewNameTextBox.Text;

            onlineInterviewSessionDetail.PositionProfileName = 
                OnlineInterviewCreation_positionProfileTextBox.Text;

            if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_positionProfileIDHiddenField.Value))
            {
                onlineInterviewSessionDetail.PositionProfileID = 
                    Convert.ToInt32(OnlineInterviewCreation_positionProfileIDHiddenField.Value);
            }

            onlineInterviewSessionDetail.InterviewInstruction = OnlineInterviewCreation_instructionTextBox.Text;
            onlineInterviewSessionDetail.InterviewRemarks = OnlineInterviewCreation_remarkTextBox.Text;
            

            return onlineInterviewSessionDetail;
        }

        
        /// <summary>
        /// This method returns the OnlineInterivewSessionDetails instance with all the values 
        /// given as input by user.
        /// </summary>
        /// <returns></returns>
        private OnlineInterviewSessionDetail ConstructOnlineInterviewSessionDetail()
        {
            // Initialize test session detail object
            OnlineInterviewSessionDetail onlineInterviewSessionDetail = 
                new OnlineInterviewSessionDetail();

            onlineInterviewSessionDetail.Interviewkey = interviewKey;
           
            onlineInterviewSessionDetail.InterviewName = 
                OnlineInterviewCreation_interviewNameTextBox.Text;

            // Set position profile ID.
            if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_positionProfileIDHiddenField.Value))
            {
                onlineInterviewSessionDetail.PositionProfileID =
                    Convert.ToInt32(OnlineInterviewCreation_positionProfileIDHiddenField.Value);
            }
            
            onlineInterviewSessionDetail.InterviewInstruction =
                OnlineInterviewCreation_instructionTextBox.Text.ToString().Trim();

            onlineInterviewSessionDetail.InterviewRemarks =
                OnlineInterviewCreation_remarkTextBox.Text.ToString().Trim();

            return onlineInterviewSessionDetail;
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            OnlineInterviewCreation_topSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewCreation_bottomSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewCreation_topErrorMessageLabel.Text = string.Empty;
            OnlineInterviewCreation_bottomErrorMessageLabel.Text = string.Empty;
        }


        /// <summary>
        /// Method to bind the subjects skill
        /// </summary>
        private void BindSubjects(string skills)
        {
            List<SkillDetail> skillDetails = null;

            if (Session["SKILL_LIST"] == null)
            {
                skillDetails = new List<SkillDetail>();
            }
            else
                skillDetails = Session["SKILL_LIST"] as List<SkillDetail>;

            string[] arraySkill = skills.Trim().Split(',');

            foreach (string skill in arraySkill)
            {
                if (skill.Trim().Length == 0)
                    continue;

                // Retrieve skill detail.
                SkillDetail skillDetail = new AssessorBLManager().GetSkillDetail
                    (skill.Trim());

                // Check if skill detail is null.
                if (skillDetail == null)
                {
                    continue;
                }

                // Check if skill detail already selected.
                var findSkill = from sk in skillDetails
                                where sk.SkillID == skillDetail.SkillID
                                select sk;

                if (findSkill != null && findSkill.Count() > 0)
                {
                    base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                        string.Format("Skill '{0}' already added", findSkill.First().SkillName));

                    continue;
                }

                // Add to the skills list.
                skillDetails.Add(skillDetail);
            }

            // Keep the skills in session.
            Session["SKILL_LIST"] = skillDetails;

            // Assign the skills to the grid view.
            OnlineInterviewCreation_skillsGridView.DataSource = skillDetails;
            OnlineInterviewCreation_skillsGridView.DataBind();
            LoadSkillWeigtageSkillLevel();

            // Clear skills text box.
            OnlineInterviewCreation_skillTextBox.Text = string.Empty;

            // Set the focus to skills text box.
            OnlineInterviewCreation_skillTextBox.Focus();

            // Clear hidden fields.
            OnlineInterviewCreation_categoryIDHiddenField.Value = string.Empty;
            OnlineInterviewCreation_skillIDHiddenField.Value = string.Empty;

            AutoPopulateSkillWeightage();
        }

        
        /// <summary>
        /// Method to bind the interview details
        /// </summary>
        private void BindInterviewDetail()
        {
            OnlineInterviewSessionDetail onlineInteviewDetail = null;
            onlineInteviewDetail = new OnlineInterviewBLManager().
                GetOnlineInterviewDetailByKey(interviewKey);

            if (onlineInteviewDetail == null)
                return;

            OnlineInterviewCreation_interviewNameTextBox.Text=onlineInteviewDetail.InterviewName;
            OnlineInterviewCreation_positionProfileTextBox.Text=onlineInteviewDetail.PositionProfileName;
            OnlineInterviewCreation_positionProfileIDHiddenField.Value=onlineInteviewDetail.PositionProfileID.ToString();
            OnlineInterviewCreation_instructionTextBox.Text=onlineInteviewDetail.InterviewInstruction;
            OnlineInterviewCreation_remarkTextBox.Text = onlineInteviewDetail.InterviewRemarks;
            OnlineInterviewCreation_interviewIDLabelValue.Text = interviewKey;

            if (onlineInteviewDetail.skillDetail!=null &&
                onlineInteviewDetail.skillDetail.Count>0)
            {
                // Assign the skills to the grid view.
                Session["SKILL_LIST"] = onlineInteviewDetail.skillDetail;
                OnlineInterviewCreation_skillsGridView.DataSource = onlineInteviewDetail.skillDetail;
                OnlineInterviewCreation_skillsGridView.DataBind();
                LoadSkillWeigtageSkillLevel();
            }
        }

        /// <summary>
        /// Metod that returns the skill details
        /// </summary>
        /// <returns></returns>
        private List<SkillDetail> ConstructSkillDetail()
        {
            int weightage = 0;
            List<SkillDetail> skills = null;

            foreach (GridViewRow row in OnlineInterviewCreation_skillsGridView.Rows)
            {
                // Instantiate skill list.
                if (skills == null)
                    skills = new List<SkillDetail>();

                // Create a skill object.
                SkillDetail skill = new SkillDetail();

                TextBox OnlineInterviewCreation_skillsGridView_weightageTextBox = ((TextBox)row.FindControl
                       ("OnlineInterviewCreation_skillsGridView_weightageTextBox"));

                HiddenField OnlineInterviewCreation_skillsGridView_skillIDHiddenField = ((HiddenField)row.FindControl
                    ("OnlineInterviewCreation_skillsGridView_skillIDHiddenField"));

                DropDownList OnlineInterviewCreation_skillsGridView_skillLevelDropDownList = ((DropDownList)row.FindControl
                    ("OnlineInterviewCreation_skillsGridView_skillLevelDropDownList"));

                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillsGridView_weightageTextBox.Text))
                {
                    weightage += Convert.ToInt32(OnlineInterviewCreation_skillsGridView_weightageTextBox.Text);
                }

                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillsGridView_skillIDHiddenField.Value))
                {
                    skill.SkillID = Convert.ToInt32(OnlineInterviewCreation_skillsGridView_skillIDHiddenField.Value);
                }

                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillsGridView_weightageTextBox.Text))
                {
                    skill.Weightage = Convert.ToInt32(OnlineInterviewCreation_skillsGridView_weightageTextBox.Text);
                }

                skill.SkillLevel = OnlineInterviewCreation_skillsGridView_skillLevelDropDownList.SelectedValue;

                skills.Add(skill);
            }
            return skills;
        }

        /// <summary>
        /// Bind position profile status
        /// </summary>
        private void BindSkillLevel(DropDownList ddlCntrl)
        {
            ddlCntrl.DataSource = new AttributeBLManager().
                GetAttributesByType(Constants.AttributeTypes.SKILL_LEVEL, "V");
            ddlCntrl.DataTextField = "AttributeName";
            ddlCntrl.DataValueField = "AttributeID";
            ddlCntrl.DataBind();
        }

        /// <summary>
        /// Method to load skill weigtage/level
        /// </summary>
        private void LoadSkillWeigtageSkillLevel()
        {
            foreach (GridViewRow row in OnlineInterviewCreation_skillsGridView.Rows)
            {

                HiddenField OnlineInterviewCreation_skillsGridView_weightageHiddenField = ((HiddenField)row.FindControl
                    ("OnlineInterviewCreation_skillsGridView_weightageHiddenField"));

                HiddenField OnlineInterviewCreation_skillsGridView_skillLeveHiddenField = ((HiddenField)row.FindControl
                    ("OnlineInterviewCreation_skillsGridView_skillLeveHiddenField"));

                TextBox OnlineInterviewCreation_skillsGridView_weightageTextBox = ((TextBox)row.FindControl
                       ("OnlineInterviewCreation_skillsGridView_weightageTextBox"));

                DropDownList OnlineInterviewCreation_skillsGridView_skillLevelDropDownList = ((DropDownList)row.FindControl
                    ("OnlineInterviewCreation_skillsGridView_skillLevelDropDownList"));

                BindSkillLevel(OnlineInterviewCreation_skillsGridView_skillLevelDropDownList);

                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillsGridView_weightageHiddenField.Value))
                {
                    OnlineInterviewCreation_skillsGridView_weightageTextBox.Text = 
                        OnlineInterviewCreation_skillsGridView_weightageHiddenField.Value.Trim();
                }

                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillsGridView_skillLeveHiddenField.Value))
                {
                    OnlineInterviewCreation_skillsGridView_skillLevelDropDownList.SelectedValue = 
                        OnlineInterviewCreation_skillsGridView_skillLeveHiddenField.Value.Trim();
                }
            }
        }

        /// <summary>
        /// Method to autopopulate the skill weightage
        /// </summary>
        private void AutoPopulateSkillWeightage()
        {
            if (OnlineInterviewCreation_skillsGridView != null && OnlineInterviewCreation_skillsGridView.Rows.Count > 0)
            {
                //Here is the logic for calculate weight
                DataTable dtTestSearchCriteria = null;
                dtTestSearchCriteria = BuildTableFromGridView();
                int TotalWeightage = 0;
                LoadWeightages(ref dtTestSearchCriteria, dtTestSearchCriteria.Rows.Count, out TotalWeightage);
                int index = 0;
                foreach (GridViewRow gridViewRow in OnlineInterviewCreation_skillsGridView.Rows)
                {
                    TextBox Weightage = (TextBox)gridViewRow.FindControl("OnlineInterviewCreation_skillsGridView_weightageTextBox");
                    Weightage.Text = dtTestSearchCriteria.Rows[index]["Weightage"].ToString();
                    index++;
                }
            }
        }


        /// <summary>
        /// Builds data table from test segment grid view
        /// </summary>
        /// <returns>Data table that contains user given input for test segment grid</returns>
        private DataTable BuildTableFromGridView()
        {
            DataTable dttestDrft = null;
            try
            {
                dttestDrft = new DataTable();
                AddTestSegmentColumns(ref dttestDrft);

                DataRow drNewRow = null;
                int Weightage = 0;
                int SNo = 1;
                foreach (GridViewRow gridViewRow in OnlineInterviewCreation_skillsGridView.Rows)
                {
                    drNewRow = dttestDrft.NewRow();
                    drNewRow["SNO"] = SNo++.ToString();

                    ((TextBox)gridViewRow.FindControl("OnlineInterviewCreation_skillsGridView_weightageTextBox")).Text="";

                    int.TryParse(((TextBox)gridViewRow.FindControl("OnlineInterviewCreation_skillsGridView_weightageTextBox")).Text, out Weightage);
                    if (Weightage > 0)
                        drNewRow["Weightage"] = Weightage;

                    dttestDrft.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                return dttestDrft;
            }
            finally
            {
                if (dttestDrft != null) dttestDrft = null;
            }
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                         OnlineInterviewCreation_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticInterviewTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isValidWeightage = true;
            // Validate session description text field
            if (OnlineInterviewCreation_interviewNameTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                    OnlineInterviewCreation_bottomErrorMessageLabel,"Please enter interview name");
            }

            int weightage = 0;
            if (OnlineInterviewCreation_skillsGridView != null &&
                OnlineInterviewCreation_skillsGridView.Rows.Count != 0)
            {
                foreach (GridViewRow row in OnlineInterviewCreation_skillsGridView.Rows)
                {
                    TextBox OnlineInterviewCreation_skillsGridView_weightageTextBox = ((TextBox)row.FindControl
                           ("OnlineInterviewCreation_skillsGridView_weightageTextBox"));

                    if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_skillsGridView_weightageTextBox.Text) &&
                        OnlineInterviewCreation_skillsGridView_weightageTextBox.Text != "0")
                    {
                        weightage += Convert.ToInt32(OnlineInterviewCreation_skillsGridView_weightageTextBox.Text);
                    }
                    else
                    {
                        isValidWeightage = false;
                    }
                }
                if (weightage != 100 || isValidWeightage==false)
                {
                    isValidData = false;
                    base.ShowMessage(OnlineInterviewCreation_topErrorMessageLabel,
                        OnlineInterviewCreation_bottomErrorMessageLabel, "Skill weightage should be equal to 100");
                }
            }

            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Methods

        #region Public Methods
        /// <summary>
        /// Method that will show feature pop up extender
        /// </summary>
        public void ShowSaveModalPopUp()
        {
            OnlineInterviewCreation_interviewSaveAsControl.UserID = base.userID;
            OnlineInterviewCreation_interviewSaveAsControl.InterviewKey = interviewKey;
            OnlineInterviewCreation_interviewSaveAs_ModalPopupExtender.Show();
        }
        #endregion Public Methods

        
}
}