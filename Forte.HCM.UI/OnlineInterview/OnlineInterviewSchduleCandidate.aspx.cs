﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ExternalService;


namespace Forte.HCM.UI.OnlineInterview
{
    public partial class OnlineInterviewSchduleCandidate : PageBase
    {
        #region Private Constants
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "0px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "400px";
        #endregion Private Constants

        #region Private Variables

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </param>
        /// <param name="chatRoomKey">
        /// A <see cref="String"/> that holds the chat room key.
        /// </param>
        /// <param name="candidateScheduleID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegateAssessor(List<AssessorDetail> assessorDetail,
            string chatRoomKey, int candidateScheduleID, EntityType entityType);

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate details.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegateCandidate(CandidateDetail candidateDetail,
            EntityType entityType);

        #endregion Private Variables

        #region Event Handlers
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["INTERVIEW_SEARCH_CRITERIA"] != null)
                LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
        }

        /// <summary>
        /// Handler method that will be called when the page loading
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/> that holds the even data
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Online Interview Schedule");

                ClearControls();

                OnlineInterviewSchduleCandidate_candidateListTR.Attributes.Add("onclick",
                        "ExpandOrRestoreDiv('" +
                        OnlineInterviewSchduleCandidate_dataListDiv.ClientID + "','" +
                        OnlineInterviewSchduleCandidate_gridViewDiv.ClientID + "','" +
                        OnlineInterviewSchduleCandidate_candidateListUpSpan.ClientID + "','" +
                        OnlineInterviewSchduleCandidate_candidateListDownSpan.ClientID + "','" +
                        OnlineInterviewSchduleCandidate_candidateList_isMaximizedHiddenField.ClientID + "')");

                if (!IsPostBack)
                {
                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "ONLINE_INTERVIEW_DATE";

                    ViewState["PAGE_NUMBER"] = 1;
                    ViewState["PAGE_SIZE"] = base.GridPageSize;

                    SetSearchInterviewParam(1, ViewState["SORT_FIELD"].ToString(), SortType.Ascending);

                    LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
                    DisplayScheduledMsg();
                }

                // Subscribe to assessor list paging event.
                OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(
                    OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator_PageNumberClick);

                DivDisplayMethod();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGE_NUMBER"] = e.PageNumber;

                InterviewSearchCriteria searchInterview = Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria;

                searchInterview.CurrentPage = e.PageNumber;

                Session["INTERVIEW_SEARCH_CRITERIA"] = searchInterview;

                LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);

                DivDisplayMethod();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the schedule candidate button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/> that holds the even data
        /// </param>
        protected void OnlineInterviewSchduleCandidate_scheduledCandidate_Button_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/OnlineScheduleCandidate.aspx?m=3&s=0&interviewkey=" +
                        Request.QueryString["interviewkey"].ToString() + "&parentpage=S_OITSN", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the showhidden button clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/> that holds the even data
        /// </param>
        protected void OnlineInterviewSchduleCandidate_showInterview_HiddenButton_Click(object sender, EventArgs e)
        {
            try
            {
                InterviewSearchCriteria searchInterview =
                        Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria;

                searchInterview.InterviewSessionStatus = SelectedSesstionStatus();

                searchInterview.Keyword = SearchText();

                Session["INTERVIEW_SEARCH_CRITERIA"] = searchInterview;

                LoadOnlineInterviews(searchInterview);

                DivDisplayMethod();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the datagrid data binding 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/> that holds the even data
        /// </param>
        protected void OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HtmlGenericControl div = (HtmlGenericControl)e.Row.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_InterivewerName_Div");

                    if (div == null) return;

                    HyperLink OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink =
                        (HyperLink)e.Row.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink");

                    Label OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_Status_Label =
                        (Label)e.Row.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_Status_Label");

                    ImageButton OnlineInterviewSchduleCandidate_approveSlotsImageButton =
                        (ImageButton)e.Row.FindControl("OnlineInterviewSchduleCandidate_approveSlotsImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_rescheduleImageButton =
                        (ImageButton)e.Row.FindControl("OnlineInterviewSchduleCandidate_rescheduleImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_unscheduleImageButton =
                        (ImageButton)e.Row.FindControl("OnlineInterviewSchduleCandidate_unscheduleImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_editSlotsImageButton =
                        (ImageButton)e.Row.FindControl("OnlineInterviewSchduleCandidate_editSlotsImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_deleteRequestedSlotsImageButton =
                        (ImageButton)e.Row.FindControl("OnlineInterviewSchduleCandidate_deleteRequestedSlotsImageButton");

                    HyperLink OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink =
                        (HyperLink)e.Row.FindControl("OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink");

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_sessionKey_HiddenField =
                        (HiddenField)e.Row.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_sessionKey_HiddenField");

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_GenId_HiddenField =
                        (HiddenField)e.Row.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_GenId_HiddenField");

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_chatId_HiddenField =
                        (HiddenField)e.Row.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_chatId_HiddenField");

                    if (OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink != null)
                        OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink.NavigateUrl
                            = "~/OnlineInterview/OnlineInterviewAssessorSummary.aspx?m=3&s=0&parentpage=MENU&interviewkey=" +
                            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_sessionKey_HiddenField.Value + "&interviewid=" +
                            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_GenId_HiddenField.Value + "&chatroomid=" + OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_chatId_HiddenField.Value.Trim();

                    if (OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink != null)
                    {
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink.Attributes.Add("cursor", "pointer");
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink.Attributes.
                            Add("onclick", "javascript:return false");
                    }

                    OnlineInterviewSchduleCandidate_approveSlotsImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_rescheduleImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_unscheduleImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_editSlotsImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_deleteRequestedSlotsImageButton.Visible = false;

                    OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink.Visible = true;

                    if (!Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_Status_Label.Text) &&
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_Status_Label.Text.Trim() == "Requested")
                    {
                        OnlineInterviewSchduleCandidate_approveSlotsImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_editSlotsImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink.Visible = false;
                        OnlineInterviewSchduleCandidate_deleteRequestedSlotsImageButton.Visible = true;
                    }

                    if (!Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_Status_Label.Text) &&
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_Status_Label.Text.Trim() == "Scheduled")
                    {
                        OnlineInterviewSchduleCandidate_rescheduleImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_unscheduleImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink.Visible = false;
                    }

                    OnlineCandidateSessionDetail dataRowItem = (OnlineCandidateSessionDetail)e.Row.DataItem;

                    if (dataRowItem.AssessorName == null) return;

                    if (!string.IsNullOrEmpty(dataRowItem.AssessorName))
                        div.Controls.Add(SetAssessorList(dataRowItem.AssessorName));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }

        }

        
        /// <summary>
        /// Handler method that will be called when the link button pressed
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="CommandEventArgs"/> that holds the even data
        /// </param> 
        protected void OnlineInterviewSchduleCandidate_LinkButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(e.CommandArgument)) return;

                InterviewSearchCriteria searchInterview =
                        Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria;

                if (ViewState["SORT_FIELD"].ToString() == (e.CommandArgument.ToString()))
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;

                    searchInterview.SortDirection = ((SortType)searchInterview.SortDirection) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                {
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    searchInterview.SortDirection = SortType.Ascending;
                }

                ViewState["SORT_FIELD"] = (e.CommandArgument.ToString());

                searchInterview.SortExpression = (e.CommandArgument.ToString()); ;

                Session["INTERVIEW_SEARCH_CRITERIA"] = searchInterview;

                LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
                DivDisplayMethod();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the datalist data binding 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="DataListItemEventArgs"/> that holds the even data
        /// </param>
        protected void OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                            e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl div = (HtmlGenericControl)e.Item.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_InterivewerName_Div");

                    /*HyperLink OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink =
    (HyperLink)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink");*/

                    Label OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatus_Label =
                        (Label)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatus_Label");

                    ImageButton OnlineInterviewSchduleCandidate_scheduledCandidateList_approveSlotsImageButton =
                        (ImageButton)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_approveSlotsImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_scheduledCandidateList_rescheduleImageButton =
                        (ImageButton)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_rescheduleImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_scheduledCandidateList_deleteRequestedSlotsImageButton =
                        (ImageButton)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_deleteRequestedSlotsImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_scheduledCandidateList_unscheduleImageButton =
                        (ImageButton)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_unscheduleImageButton");

                    ImageButton OnlineInterviewSchduleCandidate_scheduledCandidateList_editSlotsImageButton =
                        (ImageButton)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_editSlotsImageButton");

                    HyperLink OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink =
                        (HyperLink)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink");

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_sessionKey_HiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_sessionKey_HiddenField");

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_GenId_HiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GenId_HiddenField");

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_chatId_HiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_chatId_HiddenField");


                    if (OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink != null)
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink.NavigateUrl
                            = "~/OnlineInterview/OnlineInterviewAssessorSummary.aspx?m=3&s=0&parentpage=MENU&interviewkey=" +
                            OnlineInterviewSchduleCandidate_scheduledCandidateList_sessionKey_HiddenField.Value + "&interviewid=" +
                            OnlineInterviewSchduleCandidate_scheduledCandidateList_GenId_HiddenField.Value + "&chatroomid=" + OnlineInterviewSchduleCandidate_scheduledCandidateList_chatId_HiddenField.Value.Trim();

                    OnlineInterviewSchduleCandidate_scheduledCandidateList_approveSlotsImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_scheduledCandidateList_rescheduleImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_scheduledCandidateList_unscheduleImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_scheduledCandidateList_editSlotsImageButton.Visible = false;
                    OnlineInterviewSchduleCandidate_scheduledCandidateList_deleteRequestedSlotsImageButton.Visible = false;

                    OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink.Visible = true;

                    if (!Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatus_Label.Text) &&
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatus_Label.Text.Trim() == "Requested")
                    {
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_approveSlotsImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_editSlotsImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink.Visible = false;
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_deleteRequestedSlotsImageButton.Visible = true;
                    }

                    if (!Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatus_Label.Text) &&
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatus_Label.Text.Trim() == "Scheduled")
                    {
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_rescheduleImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_unscheduleImageButton.Visible = true;
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink.Visible = false;
                    }

                    if (div == null) return;

                    /*HyperLink OnlineInterviewSchduleCandidate_scheduledCandidateList_ViewDetail_HyperLink =
                  (HyperLink)e.Item.FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_ViewDetail_HyperLink");

                    if (OnlineInterviewSchduleCandidate_scheduledCandidateList_ViewDetail_HyperLink != null)
                    {
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_ViewDetail_HyperLink.Attributes.Add("cursor", "pointer");
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_ViewDetail_HyperLink.Attributes.
                            Add("onclick", "javascript:return false");
                    }*/

                    OnlineCandidateSessionDetail dataRowItem = (OnlineCandidateSessionDetail)e.Item.DataItem;

                    if (dataRowItem.AssessorName == null) return;

                    if (!string.IsNullOrEmpty(dataRowItem.AssessorName))
                        div.Controls.Add(SetAssessorList(dataRowItem.AssessorName));


                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="ImageClickEventArgs"/> that holds the even data
        /// </param>
        protected void OnlineInterviewSchduleCandidate_search_ImageButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                InterviewSearchCriteria searchInterview =
                        Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria;

                searchInterview.InterviewSessionStatus = SelectedSesstionStatus();

                searchInterview.Keyword = SearchText();

                Session["INTERVIEW_SEARCH_CRITERIA"] = searchInterview;

                LoadOnlineInterviews(searchInterview);

                DivDisplayMethod();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the View More button pressed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data
        /// </param>
        protected void OnlineInterviewSchduleCandidate_viewMore_Button_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["PAGE_SIZE"] = Convert.ToInt32(ViewState["PAGE_SIZE"]) + base.GridPageSize;

                InterviewSearchCriteria searchInterview =
                      Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria;

                searchInterview.PageSize = searchInterview.PageSize + base.GridPageSize;

                Session["INTERVIEW_SEARCH_CRITERIA"] = searchInterview;

                LoadOnlineInterviewsOnDataList(searchInterview);

                DivDisplayMethod();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when approve slots button is called
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewSchduleCandidate_approveSlotButton_Click(object sender, EventArgs e)
        {
            try
            {
                int acceptedID = 0;
                int candidateInterviewID = 0;

                foreach (GridViewRow row in OnlineInterviewSchduleCandidate_approveSlotGridView.Rows)
                {
                    RadioButton OnlineInterviewSchduleCandidate_approveSlotGridView_ApproveRadioButton =
                        ((RadioButton)row.FindControl("OnlineInterviewSchduleCandidate_approveSlotGridView_ApproveRadioButton"));

                    if (OnlineInterviewSchduleCandidate_approveSlotGridView_ApproveRadioButton.Checked)
                    {
                        HiddenField OnlineInterviewSchduleCandidate_approveSlotGridView_AcceptedIDHiddenField =
                            ((HiddenField)row.FindControl("OnlineInterviewSchduleCandidate_approveSlotGridView_AcceptedIDHiddenField"));

                        if (!Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_approveSlotGridView_AcceptedIDHiddenField.Value))
                        {
                            acceptedID =
                                Convert.ToInt32(OnlineInterviewSchduleCandidate_approveSlotGridView_AcceptedIDHiddenField.Value);
                        }
                        break;
                    }
                }

                if (!Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_candidateInterviewIDHiddenField.Value))
                {
                    candidateInterviewID =
                        Convert.ToInt32(OnlineInterviewSchduleCandidate_candidateInterviewIDHiddenField.Value);
                }

                if (acceptedID == 0 || candidateInterviewID == 0)
                {
                    base.ShowMessage(OnlineInterviewSchduleCandidate_approveSlotsErrorMsgLabel,
                    "Select slot to approve");
                    OnlineInterviewSchduleCandidate_approveSlotsModalPopupExtender.Show();
                    return;
                }

                string chatRoomkey = new OnlineInterviewBLManager().
                    ApproveSlotIDAndSchedule(candidateInterviewID, acceptedID, base.userID);

                List<AssessorDetail> assessors = new List<AssessorDetail>();

                assessors = new OnlineInterviewBLManager().
                    GetOnlineInterviewScheduledAssessorByCandidateID(candidateInterviewID);

                // Send mail to the requested assessor
                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegateAssessor taskDelegate = new AsyncTaskDelegateAssessor(SendScheduledInfoToAssessor);
                IAsyncResult result = taskDelegate.BeginInvoke(assessors, chatRoomkey, candidateInterviewID,
                    EntityType.OnlineInterviewScheduledToAssessor,
                    new AsyncCallback(SendScheduledInfoToAssessorCallBack), taskDelegate);

                CandidateDetail candidateDetail = new CandidateDetail();
                candidateDetail.FirstName = OnlineInterviewSchduleCandidate_candidateFirstNameHiddenField.Value;
                candidateDetail.EMailID = OnlineInterviewSchduleCandidate_candidateEmailHiddenField.Value;
                candidateDetail.ChatRoomKey = chatRoomkey;
                candidateDetail.CandidateInterviewID = candidateInterviewID;

                // Send mail to the scheduled candidate
                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegateCandidate taskCandidateDelegate = new AsyncTaskDelegateCandidate(SendScheduledInfoToCandidate);
                IAsyncResult resultCandidate = taskCandidateDelegate.BeginInvoke(candidateDetail,
                    EntityType.OnlineInterviewScheduledToCandidate, new AsyncCallback(SendScheduledInfoToCandidateCallBack), taskCandidateDelegate);

                //Reload the grid
                LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);

                base.ShowMessage(OnlineInterviewSchduleCandidate_topSuccessLabel,
                   OnlineInterviewSchduleCandidate_bottopSuccessLabel, "Slot approved and scheduled");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }
       
        /// <summary>
        /// Check whether the clicked button is schedule/reschedule/unschedule/viewschedule
        /// and display the Modal popup accordingly.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_slotKey_Label = ((ImageButton)e.CommandSource).Parent.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_slotKey_Label") as HiddenField;

                // Schedule Candidate
                if (e.CommandName == "ApproveSlots")
                {
                    string slotKey = string.Empty;

                    if (Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_slotKey_Label.Value))
                        return;

                    AssessorTimeSlotDetail assessorSlotDetail = new AssessorTimeSlotDetail();

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_firstName_Label = ((ImageButton)e.CommandSource).Parent.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_firstName_Label") as HiddenField;

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_emailId_Label = ((ImageButton)e.CommandSource).Parent.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_emailId_Label") as HiddenField;

                    OnlineInterviewSchduleCandidate_candidateFirstNameHiddenField.Value =
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_firstName_Label.Value;
                    OnlineInterviewSchduleCandidate_candidateEmailHiddenField.Value =
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_emailId_Label.Value;

                    assessorSlotDetail = new AssessorBLManager().GetCandidateSlotDetail(
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_slotKey_Label.Value.Trim());

                    if (assessorSlotDetail != null && assessorSlotDetail.assessorTimeSlotDetails != null)
                    {
                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataSource = null;
                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataBind();

                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataSource =
                            assessorSlotDetail.assessorTimeSlotDetails;
                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataBind();
                    }

                    if (assessorSlotDetail != null && assessorSlotDetail.onlineInterviewDetail != null)
                    {
                        OnlineInterviewSchduleCandidate_candidateInterviewIDHiddenField.Value =
                            assessorSlotDetail.onlineInterviewDetail.CandidateInterviewID.ToString();
                    }

                    OnlineInterviewSchduleCandidate_approveSlotsModalPopupExtender.Show();
                }
                // Reschedule
                if (e.CommandName == "ReSchedule")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                       Response.Redirect("~/OnlineInterview/OnlineScheduleCandidate.aspx?m=3&s=0&interviewkey=" +
                       Request.QueryString["interviewkey"].ToString() + "&candidateinterviewid=" +
                       Convert.ToInt32(e.CommandArgument.ToString()) + "&displaymode=RS&parentpage=S_OITSN", false);
                    }
                }
                // Unschedule
                if (e.CommandName == "UnSchedule")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                        ViewState["CANDIDATE_INTERVIEW_ID"] = e.CommandArgument.ToString();
                    }
                    OnlineInterviewSchduleCandidate_unscheduleCandidateModalPopupExtender.Show();
                }
                // Delete Slots
                if (e.CommandName == "DeleteSlots")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                        ViewState["CANDIDATE_INTERVIEW_ID"] = e.CommandArgument.ToString();
                    }
                    OnlineInterviewSchduleCandidate_deleteRequestedSlotsModalPopupExtender.Show();
                }
                // Edit Time Slots
                if (e.CommandName == "EditSlots")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                        Response.Redirect("~/OnlineInterview/OnlineScheduleCandidate.aspx?m=3&s=0&interviewkey=" +
                        Request.QueryString["interviewkey"].ToString() + "&candidateinterviewid=" +
                        Convert.ToInt32(e.CommandArgument.ToString()) + "&displaymode=ES&parentpage=S_OITSN", false);
                    }
                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewSchduleCandidate_unScheduleCandidateOkClick(object sender, EventArgs e)
        {
            try
            {
                int candidateInterviewID = 0;

                if (!Utility.IsNullOrEmpty(ViewState["CANDIDATE_INTERVIEW_ID"]))
                {
                    candidateInterviewID = Convert.ToInt32(ViewState["CANDIDATE_INTERVIEW_ID"].ToString());
                }

                if (candidateInterviewID != 0)
                {
                    new OnlineInterviewBLManager().
                        UnScheduleOlineInterviewCandidate(candidateInterviewID);

                    base.ShowMessage(OnlineInterviewSchduleCandidate_topSuccessLabel,
                        OnlineInterviewSchduleCandidate_bottopSuccessLabel,
                        "Candidate unscheduled successfully");

                    LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as
                        InterviewSearchCriteria);
                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewSchduleCandidate_deleteRequestedSlotsOkClick(object sender, EventArgs e)
        {
            try
            {
                int candidateInterviewID = 0;

                if (!Utility.IsNullOrEmpty(ViewState["CANDIDATE_INTERVIEW_ID"]))
                {
                    candidateInterviewID = Convert.ToInt32(ViewState["CANDIDATE_INTERVIEW_ID"].ToString());
                }

                if (candidateInterviewID != 0)
                {
                    new OnlineInterviewBLManager().
                        UnScheduleOlineInterviewCandidate(candidateInterviewID);

                    base.ShowMessage(OnlineInterviewSchduleCandidate_topSuccessLabel,
                        OnlineInterviewSchduleCandidate_bottopSuccessLabel,
                        "Candidate requested slots deleted successfully");

                    LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as
                        InterviewSearchCriteria);
                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }


        #endregion

        #region Private Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assessorsList"></param>
        /// <returns></returns>
        private Table SetAssessorList(string assessorsList)
        {
            Table tbl = new Table();

            if (assessorsList == null) return tbl;

            TableRow tblRow = new TableRow();

            TableCell tblCell = new TableCell();

            tblCell.VerticalAlign = VerticalAlign.Middle;
            tblCell.HorizontalAlign = HorizontalAlign.Left;

            string[] assessorArray = assessorsList.Split(',');

            foreach (string assessorValue in assessorArray)
            {
                if (string.IsNullOrEmpty(assessorValue)) continue;

                string[] assessors = assessorValue.Split('/');

                HyperLink hlink = new HyperLink();

                hlink.Text = assessors[1].ToString();
                hlink.Style.Add("cursor", "pointer");
                hlink.ToolTip = "Click here to view the assessor details";
                hlink.Attributes.Add("onclick", "javascript:return ShowAssessorProfile('" + assessors[0].ToString() + "');");

                Label lblSpace = new Label();
                lblSpace.Text = "&nbsp;&nbsp;&nbsp;";
                tblCell.Controls.Add(hlink);
                tblCell.Controls.Add(lblSpace);
            }

            tblRow.Cells.Add(tblCell);

            tbl.Rows.Add(tblRow);

            return tbl;
        }

        /// <summary>
        /// Loading the interview session aganist the interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A<see cref="System.String"/> that holds the online interview key
        /// </param>
        /// <param name="status">
        /// A <see cref="System.String"/> that holds the session status
        /// </param>
        /// <param name="searchText">
        /// A <see cref="System.String"/> that holds the searhc criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="System.Int32"/> that holds the page no
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="System.Int32"/>that holds the page size
        /// </param>
        /// <param name="sortingKey">
        /// A <see cref="System.String"/> that holds the ascendin/desending value
        /// </param>
        /// <param name="sortByDirection">
        /// A <see cref="DataObjects.SortType"/> that holds the ascendin/desending flag
        /// </param>
        private void LoadOnlineInterviews(InterviewSearchCriteria interviewSearch)
        {
            int totalRecords = 0;
            OnlineInterviewSchduleCandidate_displayCount_Label.Text = string.Empty;

            List<OnlineCandidateSessionDetail> onlineInterviewCandidateList =
                new OnlineInterviewAssessorBLManager().GetOnlineInterviews(interviewSearch, out totalRecords);

            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView.DataSource = null;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView.DataBind();

            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataSource = null;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataBind();

            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator.Reset();

            if (onlineInterviewCandidateList == null)
            {
                OnlineInterviewSchduleCandidate_viewMore_Button.Enabled = false;
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                    OnlineInterviewSchduleCandidate_bottomErrorLabel, "No records found to display");
                return;
            }
            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator.TotalRecords = totalRecords;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator.PageSize = base.GridPageSize;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator.MoveToPage(interviewSearch.CurrentPage);

            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView.DataSource = onlineInterviewCandidateList;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView.DataBind();

            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataSource = onlineInterviewCandidateList;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataBind();

            OnlineInterviewSchduleCandidate_viewMore_Button.Enabled = false;

            if (onlineInterviewCandidateList.Count > interviewSearch.CurrentPage)
                OnlineInterviewSchduleCandidate_viewMore_Button.Enabled = true;

            OnlineInterviewSchduleCandidate_displayCount_Label.Text =
                "Display " + onlineInterviewCandidateList.Count + " of " + totalRecords;
        }

        /// <summary>
        /// Loading the interview session aganist the interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A<see cref="System.String"/> that holds the online interview key
        /// </param>
        /// <param name="status">
        /// A <see cref="System.String"/> that holds the session status
        /// </param>
        /// <param name="searchText">
        /// A <see cref="System.String"/> that holds the searhc criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="System.Int32"/> that holds the page no
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="System.Int32"/>that holds the page size
        /// </param>
        /// <param name="sortingKey">
        /// A <see cref="System.String"/> that holds the ascendin/desending value
        /// </param>
        /// <param name="sortByDirection">
        /// A <see cref="DataObjects.SortType"/> that holds the ascendin/desending flag
        /// </param>
        private void LoadOnlineInterviewsOnDataList(InterviewSearchCriteria searchInterview)
        {
            int totalRecords = 0;
            OnlineInterviewSchduleCandidate_displayCount_Label.Text = string.Empty;

            List<OnlineCandidateSessionDetail> onlineInterviewCandidateList =
                new OnlineInterviewAssessorBLManager().GetOnlineInterviews(searchInterview, out totalRecords);

            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataSource = null;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataBind();

            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataSource = onlineInterviewCandidateList;
            OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList.DataBind();

            OnlineInterviewSchduleCandidate_viewMore_Button.Enabled = false;

            if (onlineInterviewCandidateList.Count > searchInterview.PageSize)
                OnlineInterviewSchduleCandidate_viewMore_Button.Enabled = true;

            OnlineInterviewSchduleCandidate_displayCount_Label.Text =
                "Display " + onlineInterviewCandidateList.Count + " of " + totalRecords;

        }

        /// <summary>
        /// This function clears controls text on page load
        /// </summary>
        private void ClearControls()
        {
            OnlineInterviewSchduleCandidate_topErrorLabel.Text = string.Empty;
            OnlineInterviewSchduleCandidate_bottomErrorLabel.Text = string.Empty;
            OnlineInterviewSchduleCandidate_bottopSuccessLabel.Text = string.Empty;
            OnlineInterviewSchduleCandidate_topSuccessLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method to display the schedule candidate message
        /// </summary>
        private void DisplayScheduledMsg()
        {
            if(!Utility.IsNullOrEmpty(Request.QueryString["msg"]))
            {
                string msg = Request.QueryString["msg"].ToString().Trim();
                string msgContent = string.Empty;

                if (msg == "RS")
                {
                    msgContent = "Candidate rescheduled successfully";
                }
                else if (msg == "ES")
                {
                    msgContent = "Slot requested";
                }
                else if (msg == "SC")
                {
                    msgContent = "Candidate scheduled successfully";
                }
                base.ShowMessage(OnlineInterviewSchduleCandidate_topSuccessLabel,
                    OnlineInterviewSchduleCandidate_bottopSuccessLabel, msgContent);
            }
        }   

        /// <summary>
        /// Returns the status code of selecting appropritate text
        /// </summary>
        /// <returns></returns>
        private string SelectedSesstionStatus()
        {
            string sessionStatus = null;

            if (OnlineInterviewSchduleCandidate_interviewStatus_DropDownList.SelectedValue != "0"
                && OnlineInterviewSchduleCandidate_interviewStatus_DropDownList.SelectedValue != "ALL")
                sessionStatus = OnlineInterviewSchduleCandidate_interviewStatus_DropDownList.SelectedValue;

            return sessionStatus;
        }

        /// <summary>
        /// Returns the search text
        /// </summary>
        /// <returns></returns>
        private string SearchText()
        {
            if (Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_interviewSearch_TextBox.Text.Trim())) return null;

            return OnlineInterviewSchduleCandidate_interviewSearch_TextBox.Text.Trim();
        }

        private void DivDisplayMethod()
        {
            if (Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_candidateList_isMaximizedHiddenField.Value) ||
                  OnlineInterviewSchduleCandidate_candidateList_isMaximizedHiddenField.Value == "N")
            {
                OnlineInterviewSchduleCandidate_gridViewDiv.Style.Add("display", "block");
                OnlineInterviewSchduleCandidate_dataListDiv.Style.Add("display", "none");
            }
            else
            {
                OnlineInterviewSchduleCandidate_gridViewDiv.Style.Add("display", "none");
                OnlineInterviewSchduleCandidate_dataListDiv.Style.Add("display", "block");
            }
        }

        private void SetSearchInterviewParam(int pageNumber, string sortExpression, SortType sortType)
        {
            Session["INTERVIEW_SEARCH_CRITERIA"] = null;

            InterviewSearchCriteria interviewSearch = new InterviewSearchCriteria();

            interviewSearch.InterviewSessionKey = Request.QueryString["interviewkey"].ToString();
            interviewSearch.InterviewSessionStatus = SelectedSesstionStatus();
            interviewSearch.Keyword = SearchText();
            interviewSearch.SearchTenantID = base.tenantID;
            interviewSearch.CurrentPage = pageNumber;
            interviewSearch.PageSize = base.GridPageSize;
            interviewSearch.SortExpression = sortExpression;
            interviewSearch.SortDirection = sortType;

            Session["INTERVIEW_SEARCH_CRITERIA"] = interviewSearch;
        }

        /// <summary>
        /// Method that sends mail to the scheduled assessor
        /// </summary>
        /// <param name="assessorDetails">
        /// A <see cref="AssessorDetail"/> that holds the assessor
        /// detail.
        /// </param>
        /// <param name="chatRoomKey">
        /// A <see cref="String"/> that holds the chat room key
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendScheduledInfoToAssessor(List<AssessorDetail> assessorDetails,
            string chatRoomKey, int candidateScheduleID, EntityType entityType)
        {
            try
            {
                // Send email.
                foreach (AssessorDetail assessorDetail in assessorDetails)
                {
                    assessorDetail.CandidateInterviewID = candidateScheduleID;
                    assessorDetail.ChatRoomKey = chatRoomKey;
                    new EmailHandler().SendMail(entityType, assessorDetail);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that sends mail to the scheduled candidate
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendScheduledInfoToCandidate(CandidateDetail candidateDetail,
            EntityType entityType)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(entityType, candidateDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }

        #endregion

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="resultAssessor">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendScheduledInfoToAssessorCallBack(IAsyncResult resultAssessor)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegateAssessor caller = (AsyncTaskDelegateAssessor)resultAssessor.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(resultAssessor);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="resultCandidate">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendScheduledInfoToCandidateCallBack(IAsyncResult resultCandidate)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegateCandidate caller = (AsyncTaskDelegateCandidate)resultCandidate.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(resultCandidate);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }


        #endregion Asynchronous Method Handlers

        protected void OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_slotKey_Label = ((ImageButton)e.CommandSource).Parent.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_slotKey_Label") as HiddenField;

                // Schedule Candidate
                if (e.CommandName == "ApproveSlots")
                {
                    string slotKey = string.Empty;

                    if (Utility.IsNullOrEmpty(OnlineInterviewSchduleCandidate_scheduledCandidateList_slotKey_Label.Value))
                        return;

                    AssessorTimeSlotDetail assessorSlotDetail = new AssessorTimeSlotDetail();

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_firstName_Label = ((ImageButton)e.CommandSource).Parent.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_firstName_Label") as HiddenField;

                    HiddenField OnlineInterviewSchduleCandidate_scheduledCandidateList_emailId_Label = ((ImageButton)e.CommandSource).Parent.
                        FindControl("OnlineInterviewSchduleCandidate_scheduledCandidateList_emailId_Label") as HiddenField;

                    OnlineInterviewSchduleCandidate_candidateFirstNameHiddenField.Value =
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_firstName_Label.Value;
                    OnlineInterviewSchduleCandidate_candidateEmailHiddenField.Value =
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_emailId_Label.Value;

                    assessorSlotDetail = new AssessorBLManager().GetCandidateSlotDetail(
                        OnlineInterviewSchduleCandidate_scheduledCandidateList_slotKey_Label.Value.Trim());

                    if (assessorSlotDetail != null && assessorSlotDetail.assessorTimeSlotDetails != null)
                    {
                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataSource = null;
                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataBind();

                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataSource =
                            assessorSlotDetail.assessorTimeSlotDetails;
                        OnlineInterviewSchduleCandidate_approveSlotGridView.DataBind();
                    }

                    if (assessorSlotDetail != null && assessorSlotDetail.onlineInterviewDetail != null)
                    {
                        OnlineInterviewSchduleCandidate_candidateInterviewIDHiddenField.Value =
                            assessorSlotDetail.onlineInterviewDetail.CandidateInterviewID.ToString();
                    }

                    OnlineInterviewSchduleCandidate_approveSlotsModalPopupExtender.Show();
                }
                // Reschedule
                if (e.CommandName == "ReSchedule")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                        Response.Redirect("~/OnlineInterview/OnlineScheduleCandidate.aspx?m=3&s=0&interviewkey=" +
                        Request.QueryString["interviewkey"].ToString() + "&candidateinterviewid="+
                        Convert.ToInt32(e.CommandArgument.ToString()) + "&displaymode=RS&parentpage=S_OITSN", false);
                    }
                }
                // Unschedule
                if (e.CommandName == "UnSchedule")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                        ViewState["CANDIDATE_INTERVIEW_ID"] = e.CommandArgument.ToString();
                    }
                    OnlineInterviewSchduleCandidate_unscheduleCandidateModalPopupExtender.Show();
                }
                // Delete Slots
                if (e.CommandName == "DeleteSlots")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                        ViewState["CANDIDATE_INTERVIEW_ID"] = e.CommandArgument.ToString();
                    }
                    OnlineInterviewSchduleCandidate_deleteRequestedSlotsModalPopupExtender.Show();
                }
                // Edit Time Slots
                if (e.CommandName == "EditSlots")
                {
                    if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    {
                        Response.Redirect("~/OnlineInterview/OnlineScheduleCandidate.aspx?m=3&s=0&interviewkey=" +
                        Request.QueryString["interviewkey"].ToString() + "&candidateinterviewid=" +
                        Convert.ToInt32(e.CommandArgument.ToString()) + "&displaymode=ES&parentpage=S_OITSN", false);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewSchduleCandidate_topErrorLabel,
                   OnlineInterviewSchduleCandidate_bottomErrorLabel, exp.Message);
            }
        }
    }
}
