﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineInterviewQuestionSet.aspx.cs"
    Inherits="Forte.HCM.UI.OnlineInterview.OnlineInterviewQuestionSet" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ Register Src="~/CommonControls/OnlineInterviewMenuControl.ascx" TagName="OnlineInterviewMenuControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/InterviewQuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/OnlineInterviewSummaryControl.ascx" TagName="OnlineInterviewSummaryControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="OnlineInterviewQuestionSet_bodyContent" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
        function ShowZoomedChart(sessionName) {
            var height = 620;
            var width = 950;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/ZoomedChart.aspx?sessionName=" + sessionName;
            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }

        
        $(document).ready(function () {

            // Handler method when import quetion image is clicked
            $("#<%=OnlineInterviewQuestionSet_toggleImage.ClientID %>").click(function () {
                if ($('#OnlineInterviewCreation_importQuestionDiv').is(':visible')) {
                    $("#OnlineInterviewCreation_importQuestionDiv").hide();
                    $("#<%=OnlineInterviewQuestionSet_toggleImage.ClientID %>").attr("title", "Show");
                } else {
                    $("#OnlineInterviewCreation_importQuestionDiv").show();
                    $("#<%=OnlineInterviewQuestionSet_toggleImage.ClientID %>").attr("title", "Hide");
                }
                return false;
            });

            // Handler method for when import interview section is clicked
            $('#<%= OnlineInterviewQuestionSet_importInterviewLinkButton.ClientID %>').live('click', function () {
                $("#OnlineInterviewQuestionSet_recommendedInterviewSectionDiv").hide();
                $("#OnlineInterviewQuestionSet_importInterviewSectionDiv").show();

                //Remove and add new class
                $("#<%=OnlineInterviewQuestionSet_importInterviewLinkButton.ClientID %>").removeClass("qusetionset_tab_no_link_button");
                $("#<%=OnlineInterviewQuestionSet_importInterviewLinkButton.ClientID %>").addClass("qusetionset_tab_link_button");

                $("#<%=OnlineInterviewQuestionSet_recommendedQuestionLinkButton.ClientID %>").removeClass("qusetionset_tab_link_button");
                $("#<%=OnlineInterviewQuestionSet_recommendedQuestionLinkButton.ClientID %>").addClass("qusetionset_tab_no_link_button");

                document.getElementById('<%=OnlineInterviewQuestionSet_searchTypeHiddenField.ClientID %>').value = "I";

                return false;
            });

            // Handler method for when recommended questions section is clicked
            $('#<%= OnlineInterviewQuestionSet_recommendedQuestionLinkButton.ClientID %>').live('click', function () {
                $("#OnlineInterviewQuestionSet_importInterviewSectionDiv").hide();
                $("#<%=OnlineInterviewQuestionSet_searchResultsDiv.ClientID %>").hide();
                $("#<%=OnlineInterviewQuestionSet_questionMoveDiv.ClientID %>").hide();
                $("#OnlineInterviewQuestionSet_recommendedInterviewSectionDiv").show();

                //Remove and add new class
                $("#<%=OnlineInterviewQuestionSet_recommendedQuestionLinkButton.ClientID %>").removeClass("qusetionset_tab_no_link_button");
                $("#<%=OnlineInterviewQuestionSet_recommendedQuestionLinkButton.ClientID %>").addClass("qusetionset_tab_link_button");

                $("#<%=OnlineInterviewQuestionSet_importInterviewLinkButton.ClientID %>").removeClass("qusetionset_tab_link_button");
                $("#<%=OnlineInterviewQuestionSet_importInterviewLinkButton.ClientID %>").addClass("qusetionset_tab_no_link_button");


                document.getElementById('<%=OnlineInterviewQuestionSet_searchTypeHiddenField.ClientID %>').value = "R";

                return false;
            });

            // Handler method when automated option is clicked
            $('#<%= OnlineInterviewQuestionSet_automatedSelectionRadioButton.ClientID %>').live('click', function () {
                $("#<%=OnlineInterviewQuestionSet_keywordSearchDiv.ClientID %>").hide();
            });

            // Handler method when manual question option is clicked
            $('#<%= OnlineInterviewQuestionSet_manualSelectionRadioButton.ClientID %>').live('click', function () {
                $("#<%=OnlineInterviewQuestionSet_keywordSearchDiv.ClientID %>").show();
            });

        });

        
        function ShowAddImagePanel() {
            var addImageTR = document.getElementById('<%=OnlineInterviewQuestionSet_addQuestionImageTR.ClientID%>');
            addImageTR.style.display = "";
            var addImageBtn = document.getElementById('<%=OnlineInterviewQuestionSet_addImageLinkButton.ClientID%>');
            addImageBtn.style.display = "none";
            return false;
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <uc1:OnlineInterviewMenuControl ID="OnlineInterviewQuestionSet_menuControl" runat="server"
                    Visible="true" PageType="QS" />
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="OnlineInterviewQuestionSet_topSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="OnlineInterviewQuestionSet_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewQuestionSet_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="non_tab_body_bg">
                <div runat="server" id="OnlineInterviewQuestionSet_displayQuestionSectionDiv" style="display: none">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <!-- Search Question Section -->
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="interview_skill_header_bg">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <div style="text-align: right">
                                                                        <div style="float: right; width: 30px;">
                                                                            <asp:ImageButton ID="OnlineInterviewQuestionSet_toggleImage" ToolTip="Show"
                                                                                runat="server" SkinID="sknMoveDown_ArrowImageButton" />
                                                                        </div>
                                                                        <div style="float: right">
                                                                            <asp:Label runat="server" SkinID="sknPPHeaderLabel" Text="Import Question" ID="OnlineInterviewCreation_skillLabel"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div id="OnlineInterviewCreation_importQuestionDiv" style="display: none">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="interview_skill_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td class="td_height_10">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 130px;">
                                                                                                <asp:LinkButton ID="OnlineInterviewQuestionSet_importInterviewLinkButton" CssClass="qusetionset_tab_link_button"
                                                                                                    Text="Import Interview" runat="server">
                                                                                                </asp:LinkButton>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:LinkButton ID="OnlineInterviewQuestionSet_recommendedQuestionLinkButton" CssClass="qusetionset_tab_no_link_button"
                                                                                                    Text="Recommended Question" runat="server">
                                                                                                </asp:LinkButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image ID="OnlineInterviewQuestionSet_lineBreakImage" SkinID="sknPPLineImage"
                                                                                        runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_10">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div id="OnlineInterviewQuestionSet_importInterviewSectionDiv" style="display: block">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td style="width: 100px">
                                                                                                    <asp:Label ID="OnlineInterviewQuestionSet_interviewNameHeaderLabel" Text="Interview Name"
                                                                                                        SkinID="sknLabelFieldHeaderText" runat="server"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 350px">
                                                                                                    <asp:UpdatePanel ID="OnlineInterviewCreation_searchedInterviewUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <div style="float: left">
                                                                                                                <asp:TextBox ID="OnlineInterviewCreation_searchedInterviewTextBox" runat="server"
                                                                                                                    Width="300px"> </asp:TextBox>
                                                                                                            </div>
                                                                                                            <div style="float: left; padding-left: 0px">
                                                                                                                <asp:ImageButton ID="OnlineInterviewCreation_searchInterviwImageButton" SkinID="sknSearchIconImageButton"
                                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select online interview" />
                                                                                                            </div>
                                                                                                            <asp:HiddenField ID="OnlineInterviewCreation_searchedInterviewKeyHiddenField" runat="server" />
                                                                                                            <div style="display: none">
                                                                                                                <asp:Button ID="OnlineInterviewCreation_searchedInterviewSkillButton" OnClick="OnlineInterviewCreation_searchedInterviewSkillButton_Click"
                                                                                                                    runat="server" />
                                                                                                            </div>
                                                                                                        </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                                <td style="text-align: left">
                                                                                                    <asp:UpdatePanel ID="OnlineInterviewQuestionSet_filterSkillUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <div runat="server" style="display: none" id="OnlineInterviewQuestionSet_importedInterviewSkillDiv">
                                                                                                                <div style="float: left; width: 50px;">
                                                                                                                    <asp:Label ID="OnlineInterviewQuestionSet_skillHeaderLabel" Text="Skill" SkinID="sknLabelFieldHeaderText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </div>
                                                                                                                <div style="float: left">
                                                                                                                    <asp:DropDownList ID="OnlineInterviewQuestionSet_skillDropDownList" runat="server">
                                                                                                                    </asp:DropDownList>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="OnlineInterviewCreation_searchInterviwImageButton" />
                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div id="OnlineInterviewQuestionSet_recommendedInterviewSectionDiv" style="display: none">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="td_height_5">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 150px">
                                                                                                    <asp:RadioButton ID="OnlineInterviewQuestionSet_automatedSelectionRadioButton" Text="Automated Selection"
                                                                                                        runat="server" Checked="true" AutoPostBack="false" GroupName="RecommendedQuestion" />
                                                                                                </td>
                                                                                                <td style="width: 150px">
                                                                                                    <asp:RadioButton ID="OnlineInterviewQuestionSet_manualSelectionRadioButton" Text="Manual Selection"
                                                                                                        runat="server" AutoPostBack="false" GroupName="RecommendedQuestion" />
                                                                                                </td>
                                                                                                <td align="left">
                                                                                                    <div style="display:none" runat="server" ID="OnlineInterviewQuestionSet_keywordSearchDiv">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td style="width: 80px">
                                                                                                                <asp:Label ID="OnlineInterviewQuestionSet_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="OnlineInterviewQuestionSet_keywordsTextBox" Width="250px" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="OnlineInterviewQuestionSet_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_5">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="text-align: left;display:none" colspan="2">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <div>
                                                                                                                <div style="float: left; width: 75px;">
                                                                                                                    <asp:Label ID="OnlineInterviewQuestionSet_automatedSkillLabel" Text="Skill" SkinID="sknLabelFieldHeaderText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </div>
                                                                                                                <div style="float: left">
                                                                                                                    <asp:DropDownList ID="OnlineInterviewQuestionSet_automatedSkillDropDownList" runat="server">
                                                                                                                    </asp:DropDownList>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:HiddenField ID="OnlineInterviewQuestionSet_searchTypeHiddenField" runat="server" />
                                                                                    <asp:UpdatePanel ID="OnlineInterviewQuestionSet_searchImageButtonUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:ImageButton ID="OnlineInterviewQuestionSet_searchImageButton" SkinID="sknSearchImageButton"
                                                                                                runat="server" ToolTip="Search Question" OnClick="OnlineInterviewQuestionSet_searchImageButton_Click" />
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_10">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Search data list -->
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:UpdatePanel ID="OnlineInterviewQuestionSet_searchResultsDataListUpdatePanel"
                                                                                        runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div runat="server" id="OnlineInterviewQuestionSet_searchResultsDiv" style="display: none">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="interview_skill_header_bg">
                                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label runat="server" SkinID="sknLabelFieldHeaderText" Text="Search Results"
                                                                                                                            ID="OnlineInterviewQuestionSet_searchResultsHeaderLabel"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="interview_skill_body_bg" align="left">
                                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <div style="clear:both;float:left;width: 100%; overflow: auto; height: 150px;" runat="server" id="content">
                                                                                                                            <asp:DataList ID="OnlineInterviewQuestionSet_searchResultsDataList" Width="100%"
                                                                                                                                runat="server">
                                                                                                                                <HeaderTemplate>
                                                                                                                                    <tr>
                                                                                                                                        <td style="clear:both;" class="skill_question_header_bg">
                                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                <tr>
                                                                                                                                                    <td style="width: 50px">
                                                                                                                                                        &nbsp;
                                                                                                                                                    </td>
                                                                                                                                                    <td style="text-align: left; width: 700px">
                                                                                                                                                        <asp:Label ID="OnlineInterviewQuestionSet_questionHeaderLabel" runat="server" SkinID="sknLabelHeaderText"
                                                                                                                                                            Text="Question"></asp:Label>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </table>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </HeaderTemplate>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                                                                                        <tr>
                                                                                                                                            <td class="skill_question_inner_bg">
                                                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                    <tr>
                                                                                                                                                        <td style="width: 50px">
                                                                                                                                                            <asp:CheckBox ID="OnlineInterviewQuestionSet_searchResultsDataList_selectCheckBox"
                                                                                                                                                                runat="server" />
                                                                                                                                                            <asp:HiddenField ID="OnlineInterviewQuestionSet_searchResultsDataList_questionKeyHiddenField"
                                                                                                                                                                Value='<%# Eval("QuestionKey") %>' runat="server" />
                                                                                                                                                            <asp:HiddenField ID="OnlineInterviewQuestionSet_searchResultsDataList_complexityNameHiddenField"
                                                                                                                                                                Value='<%# Eval("ComplexityName") %>' runat="server" />
                                                                                                                                                            <asp:HiddenField ID="OnlineInterviewQuestionSet_searchResultsDataList_complexityHiddenField"
                                                                                                                                                                Value='<%# Eval("Complexity") %>' runat="server" />
                                                                                                                                                            <asp:HiddenField ID="OnlineInterviewQuestionSet_searchResultsDataList_testAreaIDHiddenField"
                                                                                                                                                                Value='<%# Eval("TestAreaID") %>' runat="server" />
                                                                                                                                                            <asp:HiddenField ID="OnlineInterviewQuestionSet_searchResultsDataList_testAreaNameHiddenField"
                                                                                                                                                                Value='<%# Eval("TestAreaName") %>' runat="server" />
                                                                                                                                                            <asp:HiddenField ID="OnlineInterviewQuestionSet_searchResultsDataList_questionRelationIDHiddenField"
                                                                                                                                                                Value='<%# Eval("QuestionRelationId") %>' runat="server" />
                                                                                                                                                        </td>
                                                                                                                                                        <td style="text-align: left; width: 700px">
                                                                                                                                                            <asp:LinkButton ID="OnlineInterviewQuestionSet_searchResultsDataList_questionLinkButton"
                                                                                                                                                                Text='<%# Eval("Question") %>' runat="server" 
                                                                                                                                                             OnClick="OnlineInterviewQuestionSet_searchResultsDataList_questionLinkButton_Click" ToolTip="Preview Question" SkinID="sknQustionLinkButton" />
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </ItemTemplate>
                                                                                                                                <FooterTemplate>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_searchResultsDataList_noRecordLabel" Text="No question found"
                                                                                                                                                Visible="false" CssClass="skill_no_question_error_message" runat="server"></asp:Label>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </FooterTemplate>
                                                                                                                            </asp:DataList>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <uc4:PageNavigator ID="OnlineInterviewQuestionSet_searchResultsDataListPageNavigator" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:AsyncPostBackTrigger ControlID="OnlineInterviewQuestionSet_searchImageButton" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- End search data list -->
                                                                            <tr>
                                                                                <td class="td_height_10">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:UpdatePanel ID="OnlineInterviewQuestionSet_questionMoveUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div runat="server" id="OnlineInterviewQuestionSet_questionMoveDiv" style="display: none">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td style="width: 80px">
                                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_selectSkillLabel" SkinIDs="sknLabelHeaderText"
                                                                                                                Text="Select Skill" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 150px">
                                                                                                            <asp:DropDownList ID="OnlineInterviewQuestionSet_moveQuestionSkillDropDownList" runat="server">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Button ID="OnlineInterviewQuestionSet_questionMoveButton" SkinID="sknButtonId"
                                                                                                                Text="Move" OnClick="OnlineInterviewQuestionSet_questionMoveButton_Click" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:AsyncPostBackTrigger ControlID="OnlineInterviewQuestionSet_searchImageButton" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_height_20">
                            </td>
                        </tr>
                        <!-- End Search Question Section-->
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="OnlineInterviewQuestionSet_sessionDetailsUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <div id="OnlineInterviewQuestionSet_sessionDetailsDIV" runat="server">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:UpdatePanel ID="OnlineInterviewQuestionSet_categoryDataListUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:DataList ID="OnlineInterviewQuestionSet_categoryDataList" runat="server" Width="100%"
                                                                    DataKeyField="SkillID" OnItemDataBound="OnlineInterviewQuestionSet_categoryDataList_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td class="interview_skill_header_bg">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr class="categoryPanelTab">
                                                                                            <td align="left" style="width: 620px">
                                                                                                <div style="float: left">
                                                                                                    <asp:Label ID="OnlineInterviewQuestionSet_categoryLabel" runat="server" Text='<%# Eval("SkillName") %>'></asp:Label>
                                                                                                </div>
                                                                                                <div style="float: left">
                                                                                                    <asp:Label ID="OnlineInterviewQuestionSet_weightageLabel" runat="server"></asp:Label>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td style="width: 150px">
                                                                                                <asp:UpdatePanel ID="OnlineInterviewQuestionSet_addNewQuestionUpdatePanel" runat="server">
                                                                                                    <ContentTemplate>
                                                                                                        <asp:ImageButton ID="OnlineInterviewQuestionSet_addNewQuestionImageButton" CommandArgument='<%# Eval("SkillID") + "~" + Eval("SkillName") %>'
                                                                                                            SkinID="sknAddQuestionImageButton" runat="server" OnClick="OnlineInterviewQuestionSet_addNewQuestionImageButton_Click" />
                                                                                                    </ContentTemplate>
                                                                                                </asp:UpdatePanel>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ImageButton ID="OnlineInterviewQuestionSet_searchQuestionImageButton" SkinID="sknSearchQuestionImageButton"
                                                                                                    CommandArgument='<%# Eval("SkillID") + "~" + Eval("SkillName") %>' OnClick="OnlineInterviewQuestionSet_searchQuestionImageButton_Click"
                                                                                                    runat="server" />
                                                                                                <asp:HiddenField ID="OnlineInterviewQuestionSet_dataListIndexHiddenField" runat="server" />
                                                                                                <asp:HiddenField ID="OnlineInterviewQuestionSet_categoryDataList_skillIDHiddenField"
                                                                                                    Value='<%# Eval("SkillID") %>' runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="interview_skill_body_bg">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="td_height_10">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div style="clear:both;width: 100%; overflow: auto; height: 150px;" runat="server">
                                                                                                    <asp:DataList ID="OnlineInterviewQuestionSet_categoryQuestionDataList" Width="100%"
                                                                                                        runat="server" OnItemDataBound="OnlineInterviewQuestionSet_categoryQuestionDataList_ItemDataBound">
                                                                                                        <HeaderTemplate>
                                                                                                            <tr>
                                                                                                                <td class="skill_question_header_bg">
                                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td style="text-align: left; width: 750px">
                                                                                                                                <asp:Label ID="OnlineInterviewQuestionSet_questionHeaderLabel" runat="server" SkinID="sknLabelHeaderText"
                                                                                                                                    Text="Question"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:Label ID="OnlineInterviewQuestionSet_mandatoryHeaderLabel" runat="server" SkinID="sknLabelHeaderText"
                                                                                                                                    Text="Mandatory"></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="skill_question_inner_bg">
                                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td valign="top" align="left" style="width: 25px">
                                                                                                                                    <asp:ImageButton ID="OnlineInterviewQuestionSet_deleteQuestionImageButton" SkinID="sknPPDeleteImageButton"
                                                                                                                                        ToolTip="Delete Question" runat="server" OnClick="OnlineInterviewQuestionSet_deleteQuestionImageButton_Click" />
                                                                                                                                </td>
                                                                                                                                <td valign="top" align="left" style="width: 40px">
                                                                                                                                    <asp:ImageButton ID="OnlineInterviewQuestionSet_previewQuestionImageButton" SkinID="sknInteviewQuestionPreviewImageButton"
                                                                                                                                        runat="server" ToolTip="Preview Question" OnClick="OnlineInterviewQuestionSet_previewQuestionImageButton_Click" />
                                                                                                                                </td>
                                                                                                                                <td style="text-align: left; width: 700px">
                                                                                                                                    <asp:UpdatePanel ID="OnlineInterviewQuestionSet_questionUpdatePanel" runat="server">
                                                                                                                                        <ContentTemplate>
                                                                                                                                            <asp:LinkButton ID="OnlineInterviewQuestionSet_questionLinkButton" runat="server"
                                                                                                                                                SkinID="sknQustionLinkButton" ToolTip="Preview Question" OnClick="OnlineInterviewQuestionSet_questionLinkButton_Click" Text='<%# Eval("Question") %>' />
                                                                                                                                            <div id='<%# Eval("QuestionRelationId") %>' class="QuestionID">
                                                                                                                                            </div>
                                                                                                                                        </ContentTemplate>
                                                                                                                                    </asp:UpdatePanel>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:HiddenField ID="OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField"
                                                                                                                                        Value='<%# Eval("SkillID") %>' runat="server" />
                                                                                                                                    <asp:HiddenField ID="OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField"
                                                                                                                                        Value='<%# Eval("QuestionRelationId") %>' runat="server" />
                                                                                                                                    <asp:HiddenField ID="OnlineInterviewQuestionSet_categoryQuestionDataList_questionKeyHiddenField"
                                                                                                                                        Value='<%# Eval("QuestionKey") %>' runat="server" />
                                                                                                                                    <asp:HiddenField ID="OnlineInterviewQuestionSet_categoryQuestionDataList_isMandatoryHiddenField"
                                                                                                                                        Value='<%# Eval("IsMandatory") %>' runat="server" />
                                                                                                                                    <asp:CheckBox ID="OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox" AutoPostBack="true"
                                                                                                                                        OnCheckedChanged="OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox_Click"
                                                                                                                                        runat="server" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                        <FooterTemplate>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="OnlineInterviewQuestionSet_noRecordLabel" Text="No question added"
                                                                                                                        Visible="false" CssClass="skill_no_question_error_message" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </FooterTemplate>
                                                                                                    </asp:DataList>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_20">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_10">
                                                    </td>
                                                </tr>
                                                <!-- Question Set Summary -->
                                                <tr>
                                                    <td width="100%">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="OnlineInterviewQuestionSet_byQuestion_automatedControlUpdatePanel"
                                                                        runat="server">
                                                                        <ContentTemplate>
                                                                            <uc3:OnlineInterviewSummaryControl ID="OnlineInterviewQuestionSet_questionSummaryUserControl"
                                                                                runat="server" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- End Question Set Summary -->
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" style="width: 120px">
                                                                    <asp:Button ID="OnlineInterviewQuestionSet_SaveButton" runat="server" SkinID="sknButtonId"
                                                                        TabIndex="13" Height="26px" Text="Save and Continue" OnClick="OnlineInterviewQuestionSet_SaveButton_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="OnlineInterviewQuestionSet_cancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                        Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <!-- Preview Question -->
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="OnlineInterviewQuestionSet_questionDetailPreviewUpdatePanel"
                                    runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="OnlineInterviewQuestionSet_questionPanel" runat="server" Style="display: none"
                                            CssClass="skill_question_summary">
                                            <asp:HiddenField ID="OnlineInterviewQuestionSet_previewQuestionAddHiddenField" runat="server" />
                                            <uc2:QuestionDetailSummaryControl ID="OnlineInterviewQuestionSet_questionDetailSummaryControl"
                                                runat="server" Title="Question Details Summary" OnCancelClick="OnlineInterviewQuestionSet_cancelClick" />
                                        </asp:Panel>
                                        <div style="display: none">
                                            <asp:Button ID="OnlineInterviewQuestionSet_questionModalTargeButton" runat="server" />
                                        </div>
                                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewQuestionSet_questionModalPopupExtender"
                                            runat="server" PopupControlID="OnlineInterviewQuestionSet_questionPanel" TargetControlID="OnlineInterviewQuestionSet_questionModalTargeButton"
                                            BackgroundCssClass="modalBackground">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <!-- End Preview Question -->
                        <!-- Add new question popup -->
                        <tr>
                            <td valign="top">
                                <asp:UpdatePanel ID="OnlineInterviewQuestionSet_additionalQuestion" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="OnlineInterviewQuestionSet_addQuestionPanel" runat="server" CssClass="popupcontrol_addQuestion">
                                            <div style="display: none;">
                                                <asp:Button ID="OnlineInterviewQuestionSet_addQuestionhiddenButton" runat="server" /></div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="popup_td_padding_10">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                    <asp:Literal ID="OnlineInterviewQuestionSet_addQuestionLiteral" runat="server" Text="Add Question"></asp:Literal>
                                                                </td>
                                                                <td style="width: 50%" valign="top">
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:ImageButton ID="OnlineInterviewQuestionSet_addQuestionTopCancelImageButton"
                                                                                    runat="server" SkinID="sknCloseImageButton" OnClick="OnlineInterviewQuestionSet_addQuestionTopCancelImageButton_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popup_td_padding_10" valign="top">
                                                        <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                            <tr>
                                                                <td align="left" class="popup_td_padding_10">
                                                                    <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <table cellpadding="0" cellspacing="5">
                                                                                    <tr>
                                                                                        <td class="msg_align" colspan="4">
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_addQuestionErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_addQuestionQuestLabel" runat="server" Text="Question"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            <span class='mandatory'>*</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding: 0px;">
                                                                                                <asp:TextBox ID="OnlineInterviewQuestionSet_addQuestionQuestTextBox" runat="server"
                                                                                                    MaxLength="3000" Height="52px" Width="498px" TextMode="MultiLine" Wrap="true"></asp:TextBox>
                                                                                                <asp:LinkButton ID="OnlineInterviewQuestionSet_addImageLinkButton" runat="server"
                                                                                                    SkinID="sknActionLinkButton" Text="Add Image" ToolTip="Click here to add image"
                                                                                                    OnClientClick="javascript: return ShowAddImagePanel();" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="OnlineInterviewQuestionSet_addQuestionImageTR" runat="server" style="display: none">
                                                                                        <td>
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_selectImageLabel" runat="server" Text="Select Image"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td style="width: 250px">
                                                                                                        <ajaxToolKit:AsyncFileUpload ID="OnlineInterviewQuestionSet_questionImageUpload"
                                                                                                            runat="server" OnUploadedComplete="OnlineInterviewQuestionSet_questionImageOnUploadComplete"
                                                                                                            ClientIDMode="AutoID" />
                                                                                                    </td>
                                                                                                    <td style="width: 30px">
                                                                                                        <asp:ImageButton ID="OnlineInterviewQuestionSet_helpImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                                            ToolTip="Add Image" OnClientClick="javascript:return false;" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Button ID="OnlineInterviewQuestionSet_questionImageButton" runat="server" Text="Add"
                                                                                                            SkinID="sknButtonId" OnClick="OnlineInterviewQuestionSet_questionImageButtonClick"
                                                                                                            ToolTip="Click to add image" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_addQuestionAnswerLabel" runat="server"
                                                                                                Text="Answer" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            <span class='mandatory'>*</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="OnlineInterviewQuestionSet_addQuestionAnswerTextBox" runat="server"
                                                                                                MaxLength="500" Height="52px" Width="498px" TextMode="MultiLine"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="OnlineInterviewQuestionSet_isQuestionRepository" runat="server"
                                                                                                Text="Save question to the repository<br>(Please leave this box unchecked if this question is meant only for this interview and you do not wish to add it to the permanent question database)"
                                                                                                Checked="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_addQuestioncomplexityLabel" runat="server"
                                                                                                Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div>
                                                                                                <asp:DropDownList ID="OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList"
                                                                                                    runat="server" Width="133px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:ImageButton ID="OnlineInterviewQuestionSet_addQuestioncomplexityImageButton"
                                                                                                    SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                                                    ToolTip="Please select the complexity of the question here" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_addQuestionLabel" runat="server" Text="Interview Area"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td class="checkboxaddquestion_list_bg">
                                                                                                        <asp:RadioButtonList ID="OnlineInterviewQuestionSet_addQuestionRadioButtonList" runat="server"
                                                                                                            RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5" TextAlign="Right"
                                                                                                            Width="100%" TabIndex="5">
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_addQuestion_byQuestion_categoryLabel" runat="server"
                                                                                                Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox"
                                                                                                            ReadOnly="true" runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="OnlineInterviewQuestionSet_addQuestion_byQuestion_subjectLabel" runat="server"
                                                                                                            Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        <asp:HiddenField ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField"
                                                                                                            runat="server" Value='<%# Eval("Cat_sub_id") %>' />
                                                                                                        <asp:HiddenField ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryNameHiddenField"
                                                                                                            runat="server" Value='<%# Eval("Category") %>' />
                                                                                                        <asp:HiddenField ID="CreateAutomaticInterviewAddQuestion_byQuestion_subjectNameHiddenField"
                                                                                                            runat="server" Value='<%# Eval("Subject") %>' />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox" ReadOnly="true"
                                                                                                            runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:ImageButton ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryImageButton"
                                                                                                            SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="OnlineInterviewQuestionSet_tagsLabel" runat="server" Text="Tags"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="OnlineInterviewQuestionSet_tagsTextBox" runat="server" MaxLength="100"
                                                                                                Width="500px"></asp:TextBox>
                                                                                            <asp:ImageButton ID="OnlineInterviewQuestionSet_tagsImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                                ToolTip="Please enter tags separated by commas" OnClientClick="javascript:return false;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <table cellpadding="0" cellspacing="5" class="tab_body_bg">
                                                                                    <tr id="OnlineInterviewQuestionSet_DisplayQuestionImageTR" runat="server" style="display: none">
                                                                                        <td>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Image runat="server" ID="OnlineInterviewQuestionSet_questionImage" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:LinkButton ID="OnlineInterviewQuestionSet_deleteLinkButton" runat="server" Text="Delete"
                                                                                                            SkinID="sknActionLinkButton" OnClick="OnlineInterviewQuestionSet_deleteLinkButtonClick"
                                                                                                            ToolTip="Click to delete image" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popup_td_padding_5">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td align="left" style="width: 20px; padding-left: 5px; padding-right: 2px">
                                                                    <asp:HiddenField ID="OnlineInterviewQuestionSet_skillIDHiddenField" runat="server" />
                                                                    <asp:HiddenField ID="OnlineInterviewQuestionSet_skillNameHiddenField" runat="server" />
                                                                    <asp:Button ID="OnlineInterviewQuestionSet_addQuestionSaveButton" runat="server"
                                                                        SkinID="sknButtonId" Text="Save" OnClick="OnlineInterviewQuestionSet_addQuestionSaveButton_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="OnlineInterviewQuestionSet_addQuestioCloseLinkButton" SkinID="sknPopupLinkButton"
                                                                        runat="server" Text="Cancel" OnClick="OnlineInterviewQuestionSet_addQuestioCloseLinkButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewQuestionSet_addQuestioModalPopupExtender"
                                            runat="server" PopupControlID="OnlineInterviewQuestionSet_addQuestionPanel" TargetControlID="OnlineInterviewQuestionSet_addQuestionhiddenButton"
                                            BackgroundCssClass="modalBackground">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <!-- End Add new question popup -->
                        <tr>
                            <td class="td_height_5">
                            </td>
                        </tr>
                    </table>
                </div>
                <div runat="server" id="OnlineInterviewQuestionSet_noSkillMsgDiv" class="no_skill_error_message"
                    style="display: none">
                    No skill associated with interview.
                </div>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="OnlineInterviewQuestionSet_bottomSuccessErrorMsgUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Label ID="OnlineInterviewQuestionSet_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewQuestionSet_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
