﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    AutoEventWireup="true" CodeBehind="OnlineInterviewSchduleCandidate.aspx.cs" Inherits="Forte.HCM.UI.OnlineInterview.OnlineInterviewSchduleCandidate" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="~/CommonControls/OnlineInterviewMenuControl.ascx" TagName="OnlineInterviewMenuControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="InterviewMaster_body" runat="server">
    <script type="text/javascript">
        // Function that shows the create candidate popup
        function ShowCandidatePopup(nameCtrl, emailCtrl, idCtrl) {
            var height = 540;
            var width = 980;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/AddCandidate.aspx?namectrl=" + nameCtrl;
            queryStringValue += "&emailctrl=" + emailCtrl;
            queryStringValue += "&idctrl=" + idCtrl;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

        function ExpandOrRestoreCandidateDataList(ctrlExpand, ctrlHide, expandImage, compressImage,
            isMaximizedHidden, restoredHeight, expandedHeight) {
            if (document.getElementById(ctrlExpand) != null) {
                document.getElementById(expandImage).style.display = "none";
                document.getElementById(compressImage).style.display = "none";
                if (document.getElementById(ctrlExpand).style.height.toString() == restoredHeight) {

                    document.getElementById(ctrlExpand).style.height = expandedHeight;
                    /*document.getElementById(ctrlHide).style.display = "none";*/
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "Y";
                }
                else {
                    document.getElementById(ctrlExpand).style.height = restoredHeight;
                    /*document.getElementById(ctrlHide).style.display = "block";*/
                    document.getElementById(compressImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "N";
                }
            }
            return false;
        }

        function ExpandOrRestoreDiv(ctrlExpand, ctrlHide, expandImage, compressImage,
            isMaximizedHidden) {
            if (document.getElementById(ctrlExpand) != null) {

                if (document.getElementById(ctrlExpand).style.display == "block") {

                    document.getElementById(ctrlHide).style.display = "block";
                    document.getElementById(compressImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "N";

                    document.getElementById(ctrlExpand).style.display = "none";
                    document.getElementById(expandImage).style.display = "none";
                }
                else {
                    document.getElementById(ctrlHide).style.display = "none";
                    document.getElementById(compressImage).style.display = "none";
                    document.getElementById(isMaximizedHidden).value = "Y";

                    document.getElementById(ctrlExpand).style.display = "block";
                    document.getElementById(expandImage).style.display = "block";
                }
            }
            return false;
        }

        function SessionStatusChanges() {
            document.getElementById("<%= OnlineInterviewSchduleCandidate_showInterview_HiddenButton.ClientID%>").click();
            return false;
        }

        function GridRadioCheck(rb) {
            var gv = document.getElementById("<%=OnlineInterviewSchduleCandidate_approveSlotGridView.ClientID%>");
            var rbs = gv.getElementsByTagName("input");

            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }
    </script>
    <asp:UpdatePanel ID="OnlineInterviewSchduleCandidate_mainUpdatePanel" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <uc1:OnlineInterviewMenuControl runat="server" ID="OnlineInterview_MenuControl" PageType="SC"
                            Visible="true" />
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="OnlineInterviewSchduleCandidate_topMessageUpdatePanel" runat="server"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="OnlineInterviewSchduleCandidate_topErrorLabel" runat="server" Text=""
                                    SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="OnlineInterviewSchduleCandidate_topSuccessLabel" runat="server" Text=""
                                    SkinID="sknSuccessMessage"></asp:Label>
                                <asp:HiddenField ID="OnlineInterviewSchduleCandidate_candidateFirstNameHiddenField"
                                    runat="server" />
                                <asp:HiddenField ID="OnlineInterviewSchduleCandidate_candidateEmailHiddenField" runat="server" />
                                <asp:HiddenField ID="OnlineInterviewSchduleCandidate_candidateInterviewIDHiddenField"
                                    runat="server" />
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="non_tab_body_bg">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td class="panel_bg">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="5%">
                                                <asp:Label ID="OnlineInterviewSchduleCandidate_sortByLabel" runat="server" Text="Sort By"
                                                    SkinID="sknLabelFieldDashboard"></asp:Label>
                                            </td>
                                            <td width="9%" align="center">
                                                <asp:LinkButton ID="OnlineInterviewSchduleCandidate_scheduledDate_LinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" CommandArgument="SCHEDULED_DATE" CommandName="ScheduledDate"
                                                    OnCommand="OnlineInterviewSchduleCandidate_LinkButton_Command">Scheduled Date</asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button" width="2%">
                                                |
                                            </td>
                                            <td width="9%" align="center">
                                                <asp:LinkButton ID="OnlineInterviewSchduleCandidate_scheduledBy_LinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" CommandArgument="CREATED_BY" CommandName="CreatedBy"
                                                    OnCommand="OnlineInterviewSchduleCandidate_LinkButton_Command">Scheduled By</asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button" width="2%">
                                                |
                                            </td>
                                            <td width="9%" align="center">
                                                <asp:LinkButton ID="OnlineInterviewSchduleCandidate_interviewDate_LinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" CommandArgument="ONLINE_INTERVIEW_DATE" CommandName="InterviewDate"
                                                    OnCommand="OnlineInterviewSchduleCandidate_LinkButton_Command">Interview Date</asp:LinkButton>
                                            </td>
                                            <td width="3%">
                                                &nbsp;
                                            </td>
                                            <td align="left" width="4%">
                                                <asp:Label ID="OnlineInterviewSchduleCandidate_interviewStatus_Label" runat="server"
                                                    Text="Status " SkinID="sknLabelFieldDashboard"></asp:Label>
                                            </td>
                                            <td align="left" width="10%">
                                                <asp:DropDownList ID="OnlineInterviewSchduleCandidate_interviewStatus_DropDownList"
                                                    runat="server" Width="110px" SkinID="sknSubjectDropDown" onchange="javascript:return SessionStatusChanges();">
                                                    <asp:ListItem Value="0">[Select]</asp:ListItem>
                                                    <asp:ListItem Value="ALL">All</asp:ListItem>
                                                    <asp:ListItem Value="SESS_SCHD">Scheduled</asp:ListItem>
                                                    <asp:ListItem Value="SESS_COMP">Completed</asp:ListItem>
                                                    <asp:ListItem Value="SESS_REQ">Requested</asp:ListItem>
                                                </asp:DropDownList>
                                                <div style="display: none;">
                                                    <asp:Button ID="OnlineInterviewSchduleCandidate_showInterview_HiddenButton" runat="server"
                                                        Text="Show" OnClick="OnlineInterviewSchduleCandidate_showInterview_HiddenButton_Click" />
                                                </div>
                                            </td>
                                            <td align="right" width="6%">
                                                <asp:Label ID="OnlineInterviewSchduleCandidate_interviewSearch_Label" runat="server"
                                                    Text="Search " SkinID="sknLabelFieldDashboard"></asp:Label>
                                            </td>
                                            <td align="left" width="18%">
                                                <asp:TextBox ID="OnlineInterviewSchduleCandidate_interviewSearch_TextBox" runat="server"
                                                    SkinID="sknHomePageTextBox" Width="150px"></asp:TextBox>
                                            </td>
                                            <td width="3%" align="left">
                                                <asp:ImageButton ID="OnlineInterviewSchduleCandidate_search_ImageButton" runat="server"
                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/search_icon.gif" ToolTip="Click here to search the interview"
                                                    OnClick="OnlineInterviewSchduleCandidate_search_ImageButton_Click" />
                                            </td>
                                            <td width="20%" align="right">
                                                <asp:UpdatePanel ID="OnlineInterviewSchduleCandidate_scheduleCandidateUpdatePanel"
                                                    runat="server">
                                                    <ContentTemplate>
                                                        <asp:Button ID="OnlineInterviewSchduleCandidate_scheduledCandidate_Button" runat="server"
                                                            Text="Schedule Candidate" SkinID="sknButtonId" OnClick="OnlineInterviewSchduleCandidate_scheduledCandidate_Button_Click" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="panel_inner_body_bg">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" width="99%">
                                                    <tr id="OnlineInterviewSchduleCandidate_candidateListTR" runat="server">
                                                        <td class="header_bg" align="center">
                                                            <table cellpadding="0" cellspacing="0" width="99%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                        <asp:Literal ID="OnlineInterviewSchduleCandidate_candidateListLiteral" runat="server"
                                                                            Text="Candidate List"></asp:Literal>
                                                                        &nbsp;<asp:Label ID="OnlineInterviewSchduleCandidate_sortHelpLabel" runat="server"
                                                                            SkinID="sknLabelText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="OnlineInterviewSchduleCandidate_candidateListUpSpan" runat="server" style="display: none;">
                                                                            <asp:Image ID="OnlineInterviewSchduleCandidate_candidateListUpImage" runat="server"
                                                                                SkinID="sknMinimizeImage" ToolTip="Click here to view in grid" />
                                                                        </span><span id="OnlineInterviewSchduleCandidate_candidateListDownSpan" style="display: block;"
                                                                            runat="server">
                                                                            <asp:Image ID="OnlineInterviewSchduleCandidate_candidateListDownImage" runat="server"
                                                                                SkinID="sknMaximizeImage" ToolTip="Click here to view in thumnail" />
                                                                        </span>
                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_candidateList_restoreHiddenField"
                                                                            runat="server" />
                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_candidateList_isMaximizedHiddenField"
                                                                            runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="height: 100%; width: 100%; overflow: hidden; display: none" runat="server"
                                                    id="OnlineInterviewSchduleCandidate_gridViewDiv">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:UpdatePanel ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_UpdatePanel"
                                                                    runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView"
                                                                            runat="server" SkinID="sknWrapHeaderGrid" AutoGenerateColumns="False" EnableModelValidation="True"
                                                                            Width="100%" OnRowCommand="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_RowCommand"
                                                                            OnRowDataBound="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="OnlineInterviewSchduleCandidate_approveSlotsImageButton" runat="server"
                                                                                            CommandName="ApproveSlots" SkinID="sknAcceptTimeSlot" ToolTip="Approve Slots"
                                                                                            Visible="true" />
                                                                                        <asp:ImageButton ID="OnlineInterviewSchduleCandidate_editSlotsImageButton" runat="server"
                                                                                            CommandName="EditSlots" CommandArgument='<%# Eval("CandidateInterviewID") %>'
                                                                                            SkinID="sknScheduleImageButton" ToolTip="Edit Requested Slots" Visible="true" />
                                                                                        <asp:ImageButton ID="OnlineInterviewSchduleCandidate_deleteRequestedSlotsImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                                            ToolTip="Delete Requested Slots" CommandName="DeleteSlots" CommandArgument='<%# Eval("CandidateInterviewID") %>' />
                                                                                        <asp:ImageButton ID="OnlineInterviewSchduleCandidate_rescheduleImageButton" runat="server"
                                                                                            CommandArgument='<%# Eval("CandidateInterviewID") %>' CommandName="ReSchedule"
                                                                                            SkinID="sknRescheduleImageButton" ToolTip="Reschedule Candidate" Visible="true" />
                                                                                        <asp:ImageButton ID="OnlineInterviewSchduleCandidate_unscheduleImageButton" runat="server"
                                                                                            CommandArgument='<%# Eval("CandidateInterviewID") %>' CommandName="UnSchedule"
                                                                                            SkinID="sknUnscheduleImageButton" ToolTip="Unschedule Candidate" />
                                                                                        <asp:HyperLink ID="OnlineInterviewSchduleCandidate_candidateRatingSummaryHyperLink"
                                                                                            runat="server" Target="_blank" ToolTip="Candidate Rating Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                        </asp:HyperLink>
                                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_slotKey_Label"
                                                                                            Value='<%# Eval("SlotKey") %>' runat="server" />
                                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_firstName_Label"
                                                                                            Value='<%# Eval("FirstName") %>' runat="server" />
                                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_emailId_Label"
                                                                                            Value='<%# Eval("EmailId") %>' runat="server" />
                                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_GenId_HiddenField"
                                                                                            Value='<%# Eval("CandidateInterviewID") %>' runat="server" />
                                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_sessionKey_HiddenField"
                                                                                            Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                                                        <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_chatId_HiddenField"
                                                                                            Value='<%# Eval("ChatRoomName") %>' runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Candidate Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_CandidateName_Label"
                                                                                            runat="server" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Scheduled By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ScheduledBy_Label"
                                                                                            runat="server" Text='<%# Eval("ScheduledBy") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Interview Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_InterviewDate_Label"
                                                                                            runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("InterviewDate"))) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Interviewer(s) Name">
                                                                                    <ItemTemplate>
                                                                                        <div runat="server" id="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_InterivewerName_Div"
                                                                                            style="width: 200px">
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Status">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_Status_Label"
                                                                                            runat="server" Text='<%# Eval("SessionStatus") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="View" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:HyperLink ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_ViewDetail_HyperLink"
                                                                                            runat="server" SkinID="sknURLHyperLink" Visible="false">Details</asp:HyperLink>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <uc2:PageNavigator runat="server" ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GridView_PageNavigator" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td align="left" style="width: 120px">
                                                                            <asp:LinkButton ID="OnlineInterviewSchduleCandidate_cancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                                Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="height: 100%; width: 100%; display: block; overflow: auto" runat="server"
                                                    id="OnlineInterviewSchduleCandidate_dataListDiv">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="OnlineInterviewSchduleCandidate_displayCount_Label" runat="server"
                                                                    SkinID="sknLabelAnswerText">Display 20 of 25</asp:Label>&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100%">
                                                                <asp:UpdatePanel ID="OnlineInterviewSchduleCandidate_scheduledCandidateDataList_UpdatePanel"
                                                                    runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:DataList ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList"
                                                                            runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Width="100%" CellPadding="1"
                                                                            CellSpacing="5"  
                                                                            OnItemDataBound="OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_ItemDataBound" 
                                                                            onitemcommand="OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_ItemCommand">
                                                                            <ItemTemplate>
                                                                                <table cellpadding="5" cellspacing="0" border="0" width="440px" align="left" style="border-top: 1px solid #ccc">
                                                                                    <tr>
                                                                                        <td class="panel_body_bg" style="height: 16px">
                                                                                            <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_GenId_HiddenField"
                                                                                                Value='<%# Eval("CandidateInterviewID") %>' runat="server" />
                                                                                            <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_sessionKey_HiddenField"
                                                                                                Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                                                            <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_CandidteNameLabel"
                                                                                                runat="server" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top" class="panel_body_bg">
                                                                                            <table cellpadding="5" cellspacing="0" border="0" width="440px">
                                                                                                <tr>
                                                                                                    <td width="30%">
                                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_ScheduledByTitle_Label"
                                                                                                            runat="server" Text="Scheduled By" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                    </td>
                                                                                                    <td width="70%">
                                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_ScheduledByName_Label"
                                                                                                            runat="server" Text='<%# Eval("ScheduledBy") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_InterviewDateTitle_Label"
                                                                                                            runat="server" Text="Interview Date" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_InterviewDate_Label"
                                                                                                            runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("InterviewDate"))) %>'
                                                                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewerNameTitle_Label"
                                                                                                            runat="server" Text="Interviewer(s) Name" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                    </td>
                                                                                                    <td valign="top" align="left">
                                                                                                        <div runat="server" id="OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_InterivewerName_Div"
                                                                                                            style="width: 280px;">
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatusTitle_Label"
                                                                                                            runat="server" Text="Status" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_InterivewStatus_Label"
                                                                                                            runat="server" Text='<%# Eval("SessionStatus") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" align="right">
                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_slotKey_Label"
                                                                                                                        Value='<%# Eval("SlotKey") %>' runat="server" />
                                                                                                                    <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_firstName_Label"
                                                                                                                        Value='<%# Eval("FirstName") %>' runat="server" />
                                                                                                                    <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_emailId_Label"
                                                                                                                        Value='<%# Eval("EmailId") %>' runat="server" />
                                                                                                                    <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_GenId_HiddenField"
                                                                                                                        Value='<%# Eval("CandidateInterviewID") %>' runat="server" />
                                                                                                                    <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_sessionKey_HiddenField"
                                                                                                                        Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                                                                                    <asp:HiddenField ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_chatId_HiddenField"
                                                                                                                        Value='<%# Eval("ChatRoomName") %>' runat="server" />
                                                                                                                </td>
                                                                                                                <td style="width:5%">
                                                                                                                    <asp:ImageButton ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_approveSlotsImageButton" runat="server"
                                                                                                                        CommandName="ApproveSlots" SkinID="sknAcceptTimeSlot" ToolTip="Approve Slots"
                                                                                                                        Visible="true" />
                                                                                                                </td>
                                                                                                                <td style="width:5%">
                                                                                                                    <asp:ImageButton ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_editSlotsImageButton" runat="server"
                                                                                                                        CommandName="EditSlots" CommandArgument='<%# Eval("CandidateInterviewID") %>'
                                                                                                                        SkinID="sknScheduleImageButton" ToolTip="Edit Requested Slots" Visible="true" />
                                                                                                                </td>
                                                                                                                <td style="width:5%">
                                                                                                                    <asp:ImageButton ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_rescheduleImageButton" runat="server"
                                                                                                                        CommandArgument='<%# Eval("CandidateInterviewID") %>' CommandName="ReSchedule"
                                                                                                                        SkinID="sknRescheduleImageButton" ToolTip="Reschedule Candidate" Visible="true" />
                                                                                                                </td>
                                                                                                                <td style="width:5%">
                                                                                                                     <asp:ImageButton ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_deleteRequestedSlotsImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                                                                        ToolTip="Delete Requested Slots" CommandName="DeleteSlots" Visible="true" CommandArgument='<%# Eval("CandidateInterviewID") %>' />
                                                                                                                </td>
                                                                                                                <td style="width:5%">
                                                                                                                    <asp:ImageButton ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_unscheduleImageButton" runat="server"
                                                                                                                        CommandArgument='<%# Eval("CandidateInterviewID") %>' CommandName="UnSchedule"
                                                                                                                        SkinID="sknUnscheduleImageButton" ToolTip="Unschedule Candidate" />
                                                                                                                </td>
                                                                                                                <td style="width:5%">
                                                                                                                    <asp:HyperLink ID="OnlineInterviewSchduleCandidate_scheduledCandidateList_candidateRatingSummaryHyperLink"
                                                                                                                        runat="server" Target="_blank" ToolTip="Candidate Rating Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                                                    </asp:HyperLink>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table> 
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:DataList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="OnlineInterviewSchduleCandidate_viewMore_Button" runat="server" Text="View More"
                                                                    SkinID="sknButtonCandidate" OnClick="OnlineInterviewSchduleCandidate_viewMore_Button_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Approve slots popup -->
                <tr>
                    <td>
                        <asp:Panel ID="OnlineInterviewSchduleCandidate_approveSlotsPanel" runat="server"
                            CssClass="popupcontrol_approve_slots" Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="OnlineInterviewSchduleCandidate_approveSlotsHiddenButton" runat="server"
                                    Text="Hidden" />
                            </div>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" class="popup_header_text" style="width: 50%" valign="middle">
                                                    <asp:Literal ID="OnlineInterviewSchduleCandidate_approveSlotsLiteral" runat="server"
                                                        Text="Approve Slot"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table align="right" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="OnlineInterviewSchduleCandidate_topCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table border="0" cellpadding="0" cellspacing="0" class="popupcontrol_question_inner_bg"
                                            width="100%">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <div style="overflow: auto; height: 150px;">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="5" class="tab_body_bg"
                                                            width="100%">
                                                            <tr>
                                                                <td class="msg_align" colspan="4">
                                                                    <asp:Label ID="OnlineInterviewSchduleCandidate_approveSlotsErrorMsgLabel" runat="server"
                                                                        SkinID="sknErrorMessage"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="OnlineInterviewSchduleCandidate_approveSlotGridView" runat="server"
                                                                        AutoGenerateColumns="False" Width="100%">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:RadioButton ID="OnlineInterviewSchduleCandidate_approveSlotGridView_ApproveRadioButton"
                                                                                        onclick="GridRadioCheck(this);" runat="server" />
                                                                                    <asp:HiddenField ID="OnlineInterviewSchduleCandidate_approveSlotGridView_AcceptedIDHiddenField"
                                                                                        Value='<%# Eval("RequestDateGenID") %>' runat="server" />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="5%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="OnlineInterviewSchduleCandidate_approveSlotGridView_ChoiceLabel" Text='<%# String.Format("Choice {0}",Container.DataItemIndex + 1)  %>'
                                                                                        runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="20%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Date">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="OnlineInterviewSchduleCandidate_approveSlotGridView_DateLabel" runat="server"
                                                                                        Text='<%# Eval("AvailabilityDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="30%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Requested Time">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="OnlineInterviewSchduleCandidate_approveSlotGridView_FromLabel" runat="server"
                                                                                        SkinID="sknHomePageLabel" Text='<%# String.Format("{0} To {1}",Eval("TimeSlotTextFrom"),Eval("TimeSlotTextTo")) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" style="padding-left: 5px; width: 80px">
                                                    <asp:Button ID="OnlineInterviewSchduleCandidate_approveSlotButton" runat="server"
                                                        OnClick="OnlineInterviewSchduleCandidate_approveSlotButton_Click" SkinID="sknButtonId"
                                                        Text="Approve" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="OnlineInterviewSchduleCandidate_bottomCloseLinkButton" runat="server"
                                                        SkinID="sknPopupLinkButton" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewSchduleCandidate_approveSlotsModalPopupExtender"
                            runat="server" BackgroundCssClass="modalBackground" PopupControlID="OnlineInterviewSchduleCandidate_approveSlotsPanel"
                            TargetControlID="OnlineInterviewSchduleCandidate_approveSlotsHiddenButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <!-- End slots-->
                <!-- Unschedule popup -->
                <tr>
                    <td>
                        <asp:Panel ID="OnlineInterviewSchduleCandidate_unscheduleCandidatePanel" runat="server"
                            CssClass="popupcontrol_confirm_remove" Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="OnlineInterviewSchduleCandidate_hiddenUnscheduleButton" runat="server"
                                    Text="Hidden" />
                            </div>
                            <uc4:ConfirmMsgControl ID="OnlineInterviewSchduleCandidate_inactiveConfirmMsgControl"
                                runat="server" Message="Are you sure do you want to cancel the scheduled interview?"
                                OnOkClick="OnlineInterviewSchduleCandidate_unScheduleCandidateOkClick" Title="Unschedule"
                                Type="YesNo" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewSchduleCandidate_unscheduleCandidateModalPopupExtender"
                            runat="server" BackgroundCssClass="modalBackground" PopupControlID="OnlineInterviewSchduleCandidate_unscheduleCandidatePanel"
                            TargetControlID="OnlineInterviewSchduleCandidate_hiddenUnscheduleButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <!-- End unschedule popup -->
                <!-- Delete Requested Slots Popup -->
                <tr>
                    <td>
                        <asp:Panel ID="OnlineInterviewSchduleCandidate_deleteRequestedSlotsPanel" runat="server"
                            CssClass="popupcontrol_confirm_remove" Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="OnlineInterviewSchduleCandidate_hiddenDeleteRequestedSlotsButton"
                                    runat="server" Text="Hidden" />
                            </div>
                            <uc4:ConfirmMsgControl ID="OnlineInterviewSchduleCandidate_deleteRequestedSlotsConfirmMsgControl"
                                runat="server" Message="Are you sure do you want to delete the requested slots?"
                                OnOkClick="OnlineInterviewSchduleCandidate_deleteRequestedSlotsOkClick" Title="Delete Requested Slots"
                                Type="YesNo" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewSchduleCandidate_deleteRequestedSlotsModalPopupExtender"
                            runat="server" BackgroundCssClass="modalBackground" PopupControlID="OnlineInterviewSchduleCandidate_deleteRequestedSlotsPanel"
                            TargetControlID="OnlineInterviewSchduleCandidate_hiddenDeleteRequestedSlotsButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <!-- Delete Requested Slots Popup -->
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="OnlineInterviewSchduleCandidate_bottomUpdatePanel" runat="server"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="OnlineInterviewSchduleCandidate_bottomErrorLabel" runat="server" Text=""
                                    SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="OnlineInterviewSchduleCandidate_bottopSuccessLabel" runat="server"
                                    Text="" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
