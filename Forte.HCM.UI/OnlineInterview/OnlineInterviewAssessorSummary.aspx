﻿<%@ Page Title="Online Interview Assessor Summary" Language="C#" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    AutoEventWireup="true" CodeBehind="OnlineInterviewAssessorSummary.aspx.cs" Inherits="Forte.HCM.UI.OnlineInterview.OnlineInterviewAssessorSummary" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="InterviewMaster_body" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="../VideoStreaming/history/history.css" />
    <script type="text/javascript" src="../VideoStreaming/AC_OETags.js" language="javascript"></script>
    <script type="text/javascript" src="../VideoStreaming/history/history.js" language="javascript"></script>
    <script type="text/javascript">
        var requiredMajorVersion = 9;
        var requiredMinorVersion = 0;
        var requiredRevision = 28; 
    </script>
    <script type="text/javascript">
        function OpenDownloadPopUp(type, interviewkey, chatroomid, interviewid, rpttype) { 

            window.open('../Common/Download.aspx?interviewkey='
            + interviewkey + '&type=' + type + '&chatroomid=' + chatroomid + '&interviewid=' + interviewid + '&rpttype=' + rpttype
            + '', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }
        function CheckQuestionVisible(tbl) {
            var rowNum = 0;
            var clienttbl = document.getElementById(tbl);

            for (rowNum = 0; rowNum < clienttbl.rows.length; rowNum++) {
                var row = clienttbl.rows[rowNum];

                if (row.style.display == 'none')
                    row.style.display = 'table-row';
                else if (row.style.display == 'table-row')
                    row.style.display = 'none';
            }
            return false;
        }

        function ShowTable(clntTable, placeholder) {

            var i = 0;
            if (placeholder != null) {
                var clntcontrols = placeholder.split(',');
                if (clntcontrols != null) {
                    for (i = 0; i < clntcontrols.length; i++) {
                        document.getElementById(clntcontrols[i]).style.display = 'none';
                    }
                }
            }

            var tempClntTable = document.getElementById(clntTable);

            if (tempClntTable == null) return false;

            if (tempClntTable.style.display == 'none')
                tempClntTable.style.display = 'table';
            else if (tempClntTable.style.display == 'table')
                tempClntTable.style.display = 'none';

            document.getElementById("<%= OnlineInterviewCandidateAssessorSummary_assessmentSubjectScore_LinkButton.ClientID %>").innerHTML = 'Hide';
            document.getElementById("<%= OnlineInterviewCandidateAssessorSummary_assessmentSubjectScore_LinkButton.ClientID %>").title = 'Click here to view all the skills rating by assessor wise';

            return false;
        }

        function ShowAllScore(linkBtn, placeholder) {

            var i = 0;
            if (placeholder != null) {
                var clntcontrols = placeholder.split(',');
                if (clntcontrols != null) {
                    for (i = 0; i < clntcontrols.length; i++) {

                        if (document.getElementById(linkBtn).innerHTML == 'Hide') {
                            document.getElementById(clntcontrols[i]).style.display = 'none';
                        }
                        else {
                            document.getElementById(clntcontrols[i]).style.display = 'table';
                        }

                    }
                }
            }

            if (document.getElementById(linkBtn).innerHTML == 'Show All') {
                document.getElementById(linkBtn).innerHTML = 'Hide';
                document.getElementById(linkBtn).title = 'Click here to hide all the skills rating by assessor wise';
            }
            else {
                document.getElementById(linkBtn).innerHTML = 'Show All';
                document.getElementById(linkBtn).title = 'Click here to view all the skills rating by assessor wise';
            }
            return false;
        }

        function OnSuccess(response) {
            alert(response.d);
        }

        function checkVisibilty(tbl) {
            alert(document.getElementById(tbl));
            return false;
        }
    </script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="OnlineInterviewCandidateAssessorSummary_headerLiteral" runat="server"
                                Text="Candidate Interview Response"></asp:Literal>
                        </td>
                        <td align="right">
                            <asp:Button ID="OnlineInterviewCandidateAssessorSummary_topDownloadButton" runat="server"
                                Text="Download" ToolTip="Click here to download" SkinID="sknButtonId" OnClick="OnlineInterviewCandidateAssessorSummary_topDownloadButton_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="OnlineInterviewCandidateAssessorSummary_topMessageUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Label ID="OnlineInterviewCandidateAssessorSummary_topErrorLabel" runat="server"
                            Text="" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewCandidateAssessorSummary_topSuccessLabel" runat="server"
                            Text="" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview" />
                        <asp:AsyncPostBackTrigger ControlID="OnlineInterviewCandidateAssessorSummary_topDownloadButton" />
                        <asp:AsyncPostBackTrigger ControlID="OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview" />
                        <asp:AsyncPostBackTrigger ControlID ="OnlineInterviewCandidateAssessorSummary_reportButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
         
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tab_body_bg">
                            <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top" align="left" style="width: 21%">
                                        <div style="width: 186px; overflow: auto; height: 215px; background-color: #fff;">
                                            <script language="JavaScript" type="text/javascript">
                                                                <!--
                                                // Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
                                                var hasProductInstall = DetectFlashVer(6, 0, 65);

                                                // Version check based upon the values defined in globals
                                                var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

                                                if (hasProductInstall && !hasRequestedVersion) {
                                                    // DO NOT MODIFY THE FOLLOWING FOUR LINES
                                                    // Location visited after installation is complete if installation is required
                                                    var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
                                                    var MMredirectURL = window.location;
                                                    document.title = document.title.slice(0, 47) + " - Flash Player Installation";
                                                    var MMdoctitle = document.title;

                                                    AC_FL_RunContent(
		                                                                "src", "playerProductInstall",
		                                                                "FlashVars", "URL=<%= videoURL %>&MMredirectURL=" + MMredirectURL + '&MMplayerType=' + MMPlayerType + '&MMdoctitle=' + MMdoctitle + "",
		                                                                "width", "186",
		                                                                "height", "215",
		                                                                "align", "middle",
		                                                                "id", "OFIStream",
		                                                                "quality", "high",
		                                                                "bgcolor", "#ffffff",
		                                                                "name", "OFIStream",
		                                                                "allowScriptAccess", "sameDomain",
		                                                                "type", "application/x-shockwave-flash",
		                                                                "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                                                );
                                                } else if (hasRequestedVersion) {
                                                    // if we've detected an acceptable version
                                                    // embed the Flash Content SWF when all tests are passed
                                                    AC_FL_RunContent(
			                                                                "src", "../VideoStreaming/OFIStream",
			                                                                "FlashVars", "URL=<%= videoURL %>&QuestionID=<%= candidateID %>",
			                                                                "width", "186",
			                                                                "height", "215",
			                                                                "align", "left",
			                                                                "id", "OFIStream",
			                                                                "quality", "high",
			                                                                "bgcolor", "#ffffff",
			                                                                "name", "OFIStream",
			                                                                "allowScriptAccess", "sameDomain",
			                                                                "type", "application/x-shockwave-flash",
			                                                                "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                                                );
                                                } else {  // flash is too old or we can't detect the plugin
                                                    var alternateContent = 'Alternate HTML content should be placed here. '
  	                                                                + 'This content requires the Adobe Flash Player. '
   	                                                                + '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
                                                    document.write(alternateContent);  // insert non-flash content
                                                }
                                                                // -->
                                            </script>
                                            <noscript>
                                                <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="OFIStream" width="186px"
                                                    height="215px" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
                                                    <param name="movie" value="OFIStream.swf" />
                                                    <param name="quality" value="high" />
                                                    <param name="bgcolor" value="#ffffff" />
                                                    <param name="allowScriptAccess" value="sameDomain" />
                                                    <embed src="../VideoStreaming/OFIStream.swf" quality="high" bgcolor="#ffffff" width="186px"
                                                        height="215px" name="OFIStream" align="middle" play="true" loop="false" quality="high"
                                                        allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer">
			                                                             </embed>
                                                </object>
                                            </noscript>
                                        </div>
                                    </td>
                                    <td align="left" valign="top" style="width: 44%; padding: 15px">
                                        <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:LinkButton runat="server" ID="OnlineInterviewCandidateAssessorSummary_candidateNameLinkButton"
                                                        Text="Candidate Name" SkinID="sknAssessorActionLinkButton" ToolTip="Click here to view candidate profile"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_interviewNameLabel"
                                                        Text="Interview Name" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_interviewCompletedLabel"
                                                        Text="Interview Completed on 14-Oct-2012" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 35%">
                                                    <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_totalWeightageLabel"
                                                        Text="Total Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_totalWeightageValeuLabel"
                                                        SkinID="sknLabelRatingScoreFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="left" valign="top" style="width: 35%; padding: 15px">
                                        <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="OnlineInterviewCandidateAssessorSummary_positionProfileNameLinkButton"
                                                        Text="DBD 1" SkinID="sknAssessorActionLinkButton" Style="word-wrap: break-word;
                                                        white-space: normal" ToolTip="Click here to view position profile details"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="OnlineInterviewCandidateAssessorSummary_positionProfile_ClientNameLinkButton"
                                                        Text="I - CAR" SkinID="sknLabelFieldTextLinkButton" ToolTip="Click here to view client details"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="header_bg" style="width: 50%; text-align: left; padding-left: 10px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td style="width: 75%" align="left">
                                                    <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_assessmentSubjectScore_Label"
                                                        Text="Assessor(s) Subject Score"></asp:Label>
                                                </td>
                                                <td align="right" style="padding-right: 10px">
                                                    <asp:LinkButton ID="OnlineInterviewCandidateAssessorSummary_assessmentSubjectScore_LinkButton"
                                                        SkinID="sknActionLinkButton" runat="server" Text="Show All" ToolTip="Click here to view all the skills rating by assessor wise" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 1%">
                                    </td>
                                    <td class="header_bg" style="width: 49%; text-align: left; padding-left: 10px">
                                        <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_assessmentSubjectScoreComents_Label"
                                            Text="Assessor(s) Score & Comments"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left">
                                        <asp:UpdatePanel ID="OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_UpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <asp:GridView runat="server" ID="OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview"
                                                    AutoGenerateColumns="False" EnableModelValidation="True" OnRowDataBound="OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Subject">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton"
                                                                    runat="server" Text='<%# Eval("SKILL_NAME") %>' ToolTip="Click here view the skill rating"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Subject Weighted Score">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillScoreLabel"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:UpdatePanel ID="OnlineInterviewCandidateAssessorSummary_asssessorScore_UpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <asp:GridView runat="server" ID="OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview"
                                                    AutoGenerateColumns="False" EnableModelValidation="True" OnRowDataBound="OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_RowDataBound"
                                                    SkinID="sknWrapHeaderGrid">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Assessor Name">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_assessorIdHiddenField"
                                                                    runat="server" Value='<%# Eval("ASSESSOR_ID") %>' />
                                                                <asp:Label runat="server" ID="OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_assessorNameLabel"
                                                                    Text='<%# Eval("ASSESSOR_NAME") %>' SkinID="sknLabelAnswerText"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Weighted Score">
                                                            <ItemTemplate>
                                                                <asp:Label ID="OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_WeightedScoreLabel"
                                                                    runat="server" SkinID="sknLabelAnswerText"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="100%">
                                                                        <div runat="server" id="OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_AssessorCommentsDiv"
                                                                            style="word-wrap: break-word; white-space: normal; width: 100%; position: relative">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="OnlineInterviewCandidateAssessorSummary_pdfTypeUpdatePanel"
                                runat="server">
                                <ContentTemplate>
                                    <asp:HiddenField ID="OnlineInterviewCandidateAssessorSummary_pdfTypeHiddenField"
                                        runat="server" />
                                    <asp:Panel ID="OnlineInterviewCandidateAssessorSummary_pdfTypePanel" runat="server"
                                        CssClass="popupcontrol_confirm_remove" Style="display: none; width: 450px; height: 270px">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="Forte_CorporateUserSignUp_messagetitleLiteral" runat="server">Assessment Report</asp:Literal>
                                                            </td>
                                                            <td style="width: 25%" valign="top" align="right">
                                                                <asp:ImageButton ID="OnlineInterviewCandidateAssessorSummary_topCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_SignUp_type_bg"
                                                        style="height: 150px">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="OnlineInterviewCandidateAssessorSummary_pdfTypeRadioButtonList" runat="server" SkinID="sknSkillRadioButtonList">
                                                                    <asp:ListItem Text="Overall Score and Assessor(s) Comments Only" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Overall Score and Subject-Level Scores" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Overall Score and Question-Level Scores" Value="3"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td style="width:15%;" align="left" >
                                                                <asp:Button ID="OnlineInterviewCandidateAssessorSummary_reportButton" runat="server" Text="Report"
                                                                    SkinID="sknButtonId"  OnClick="OnlineInterviewCandidateAssessorSummary_reportButton_Click"   />
                                                            </td>
                                                            <td  style="width:15%;" align="left">
                                                                <asp:LinkButton ID="OnlineInterviewCandidateAssessorSummary_cancelLinkButton" runat="server"
                                                                    Text="Cancel" SkinID="sknPopupLinkButton" >
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewCandidateAssessorSummary_pdfTypeModalPopupExtender"
                                        runat="server" PopupControlID="OnlineInterviewCandidateAssessorSummary_pdfTypePanel"
                                        TargetControlID="OnlineInterviewCandidateAssessorSummary_pdfTypeHiddenField"
                                        BackgroundCssClass="modalBackground" CancelControlID="OnlineInterviewCandidateAssessorSummary_cancelLinkButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="OnlineInterviewCandidateAssessorSummary_topDownloadButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align="left" id="OnlineInterviewCandidateAssessorSummary_tableDiv" runat="server"
                                        valign="top">
                                        <asp:LinkButton ID="LinkButton1" runat="server"></asp:LinkButton>
                                        <asp:UpdatePanel ID="OnlineInterviewCandidateAssessorSummary_tablePlaceHolder_UpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <asp:PlaceHolder ID="OnlineInterviewCandidateAssessorSummary_tablePlaceHolder" runat="server">
                                                    <br />
                                                </asp:PlaceHolder>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
