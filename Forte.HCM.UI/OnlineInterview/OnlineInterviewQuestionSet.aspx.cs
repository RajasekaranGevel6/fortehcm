﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineInterviewQuestionSet.cs
// File that represents the user interface for the create online interview question set
// creation. This will interact with the OnlineInterviewBLManager to maintain the online interview 
// question set details to the database.

#endregion Header

#region Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.Support;
using System.Data;
using System.IO;
using Forte.HCM.BL;
using Forte.HCM.DataObjects;
using System.Text;
using System.Web.UI.DataVisualization.Charting;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.Common.DL;
using Forte.HCM.EventSupport;


#endregion Directives

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for OnlineInterviewQuestionSet page. This is used
    /// to create online interview question set against the skill selected
    /// This class is inherited from
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class OnlineInterviewQuestionSet : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <see cref="string"/> that hold the interview key.
        /// </summary>
        private string interviewkey = string.Empty;

        #endregion Private Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that is called when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Clear Controls
                ClearLabelMessage();

                Page.Form.DefaultButton = OnlineInterviewQuestionSet_SaveButton.UniqueID;

                //Set the page title
                Master.SetPageCaption("Question Set");

                // Check if interview key is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["interviewkey"]))
                {
                    // Get interview key.
                    interviewkey = Request.QueryString["interviewkey"].ToString();
                }

                if (!IsPostBack)
                {
                    //Clear the session values
                    Session["SKILL_QUESTIONS"] = null;

                    
                    //Set search online interview popup
                    OnlineInterviewCreation_searchInterviwImageButton.Attributes.Add("onclick",
                    "return SearchOnlineInterview('" +
                    OnlineInterviewCreation_searchedInterviewTextBox.ClientID + "','" +
                    OnlineInterviewCreation_searchedInterviewKeyHiddenField.ClientID + "','" +
                    OnlineInterviewCreation_searchedInterviewSkillButton.ClientID + "')");

                    //Load existing skill and its questions
                    LoadSkillQuestion();

                    //Load recommend questions skill drop down list
                    LoadDropDownSkill(interviewkey, 
                        OnlineInterviewQuestionSet_automatedSkillDropDownList, true);

                }

                // Subscribe to assessor list paging event.
                OnlineInterviewQuestionSet_searchResultsDataListPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(
                    OnlineInterviewQuestionSet_searchResultsDataListPageNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void OnlineInterviewQuestionSet_searchResultsDataListPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                SearchQuestionSet(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                     OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the item data bound  
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will filter data by categorywise and append to the nested datalist
        /// </remarks>
        protected void OnlineInterviewQuestionSet_categoryDataList_ItemDataBound(object sender,
           DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                        e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Set search question popup
                    ImageButton OnlineInterviewQuestionSet_searchQuestionImageButton =
                        (ImageButton)e.Item.FindControl("OnlineInterviewQuestionSet_searchQuestionImageButton");

                    Label OnlineInterviewQuestionSet_categoryLabel =
                        (Label)e.Item.FindControl("OnlineInterviewQuestionSet_categoryLabel");

                    string searchKeyword = string.Empty;

                    if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_categoryLabel.Text))
                    {
                        searchKeyword = OnlineInterviewQuestionSet_categoryLabel.Text.ToString().Trim();
                    }

                    OnlineInterviewQuestionSet_searchQuestionImageButton.Attributes.Add("onclick",
                    "return SearchOnlineInterviewQuestions('" + searchKeyword + "');");

                    //Assign the datalist index
                    HiddenField OnlineInterviewQuestionSet_dataListIndexHiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterviewQuestionSet_dataListIndexHiddenField");

                    OnlineInterviewQuestionSet_dataListIndexHiddenField.Value = e.Item.ItemIndex.ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    exp.Message);
            }
        }

        protected void OnlineInterviewQuestionSet_addQuestionTopCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ResetQuestionImage();
        }

        /// <summary>
        /// Handler that helps to show the preview of the saved image
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>   
        protected void OnlineInterviewQuestionSet_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
                {
                    OnlineInterviewQuestionSet_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE";
                    OnlineInterviewQuestionSet_addQuestionImageTR.Style.Add("display", "none");
                    OnlineInterviewQuestionSet_DisplayQuestionImageTR.Style.Add("display", "");
                    OnlineInterviewQuestionSet_addImageLinkButton.Style.Add("display", "none");
                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewQuestionSet_deleteLinkButtonClick(object sender, EventArgs e)
        {
            try
            {
                ResetQuestionImage();
                OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                return;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handles the Clicked event of the OnlineInterviewQuestionSet_addQuestionSaveButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnlineInterviewQuestionSet_addQuestionSaveButton_Click
                (object sender, EventArgs e)
        {
            try
            {
                Session["NEWSEARCH_QUESTIONLIST"] = null;
                if (OnlineInterviewQuestionSet_addQuestionQuestTextBox.Text.Length == 0)
                {
                    OnlineInterviewQuestionSet_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel, "Enter the question");

                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();

                    return;
                }
                if (OnlineInterviewQuestionSet_addQuestionAnswerTextBox.Text.Length == 0)
                {
                    OnlineInterviewQuestionSet_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel, "Enter the answer");

                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.SelectedItem.Text == "--Select--")
                {
                    OnlineInterviewQuestionSet_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel, "Select the complexity");

                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                    return;
                }

                if (CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox.Text.Length == 0)
                {
                    OnlineInterviewQuestionSet_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel, "Select the category and subject");

                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox.Text.Length == 0)
                {
                    OnlineInterviewQuestionSet_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel, "Select the category and subject");

                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                    return;
                }

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_skillIDHiddenField.Value))
                {
                    int skillID = 0;
                    int selectedIndex = 0;
                    string skillName = string.Empty;

                    skillID = Convert.ToInt32(OnlineInterviewQuestionSet_skillIDHiddenField.Value);
                    skillName = OnlineInterviewQuestionSet_skillNameHiddenField.Value;

                    selectedIndex = GetParentDataListIndex(skillID);

                    if (skillID != 0 && !Utility.IsNullOrEmpty(skillName))
                    {
                        AddQusetionSet(selectedIndex, skillID, skillName);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void OnlineInterviewQuestionSet_addQuestioCloseLinkButton_Click(object sender, EventArgs e)
        {
            ResetQuestionImage();
        }

        /// <summary>
        /// Handler that helps to save the posted file in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnlineInterviewQuestionSet_questionImageOnUploadComplete(object sender,
            AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            System.Threading.Thread.Sleep(500);
            if (OnlineInterviewQuestionSet_questionImageUpload.PostedFile != null)
            {
                if (OnlineInterviewQuestionSet_questionImageUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                {
                    base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel,
                        Resources.HCMResource.BatchQuestionEntry_imageSizeExceeded);
                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Check if the photo is of the desired extension.
                string extension = Path.GetExtension(OnlineInterviewQuestionSet_questionImageUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel, "Only gif/png/jpg/jpeg files are allowed");
                    OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Read the bytes from the posted file.
                HttpPostedFile postedQstImage = OnlineInterviewQuestionSet_questionImageUpload.PostedFile;
                int nFileLen = postedQstImage.ContentLength;
                byte[] imgData = new byte[nFileLen];

                postedQstImage.InputStream.Read(imgData, 0, nFileLen);
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = imgData;

                OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                return;
            }
            else
            {
                base.ShowMessage(OnlineInterviewQuestionSet_addQuestionErrorLabel, "Please select valid image to upload");
                OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
                return;
            }
        }

        /// <summary>
        /// Handles the Click event of the OnlineInterviewQuestionSet_addNewQuestionImageButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnlineInterviewQuestionSet_addNewQuestionImageButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Get skill id and skill name
                ImageButton skillIDImageButton = (ImageButton)sender;

                HiddenField OnlineInterviewQuestionSet_dataListIndexHiddenField = 
                    ((ImageButton)sender).FindControl("OnlineInterviewQuestionSet_dataListIndexHiddenField") as HiddenField;

                if (!Utility.IsNullOrEmpty(skillIDImageButton.CommandArgument))
                {
                    OnlineInterviewQuestionSet_skillIDHiddenField.Value = string.Empty;
                    OnlineInterviewQuestionSet_skillNameHiddenField.Value = string.Empty;

                    // Get command arguments
                    string[] arrArgument = skillIDImageButton.CommandArgument.Split(new char[] { '~' });

                    OnlineInterviewQuestionSet_skillIDHiddenField.Value = arrArgument[0].ToString();
                    OnlineInterviewQuestionSet_skillNameHiddenField.Value = arrArgument[1].ToString();
                }

                OnlineInterviewQuestionSet_addQuestionErrorLabel.Text = string.Empty;
                OnlineInterviewQuestionSet_addQuestionQuestTextBox.Text = string.Empty;
                OnlineInterviewQuestionSet_questionImage.ImageUrl = string.Empty;
                OnlineInterviewQuestionSet_addQuestionAnswerTextBox.Text = string.Empty;
                OnlineInterviewQuestionSet_isQuestionRepository.Checked = false;
                CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox.Text = string.Empty;
                CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox.Text = string.Empty;
                OnlineInterviewQuestionSet_tagsTextBox.Text = string.Empty;

                // Binding test area to radiobutton list.
                OnlineInterviewQuestionSet_addQuestionRadioButtonList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
                OnlineInterviewQuestionSet_addQuestionRadioButtonList.DataTextField = "AttributeName";
                OnlineInterviewQuestionSet_addQuestionRadioButtonList.DataValueField = "AttributeID";
                OnlineInterviewQuestionSet_addQuestionRadioButtonList.DataBind();
                OnlineInterviewQuestionSet_addQuestionRadioButtonList.Items[0].Selected = true;

                //Bind complexities/attributes based on the attribute type
                OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
                OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.DataTextField = "AttributeName";
                OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.DataValueField = "AttributeID";
                OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.DataBind();

                OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.Items.Insert(0, "--Select--");
                OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.SelectedIndex = 0;

                CreateAutomaticInterviewAddQuestion_byQuestion_categoryImageButton.Attributes.Add("onclick",
                "return LoadCategorySubjectLookUpforReadOnly('" + CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox.ClientID +
                "','" + CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox.ClientID + "','" +
                CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField.ClientID + "','" +
                CreateAutomaticInterviewAddQuestion_byQuestion_categoryNameHiddenField.ClientID + "','" +
                CreateAutomaticInterviewAddQuestion_byQuestion_subjectNameHiddenField.ClientID + "');");

                OnlineInterviewQuestionSet_addQuestioModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handles the Click event of the OnlineInterviewQuestionSet_searchQuestionImageButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnlineInterviewQuestionSet_searchQuestionImageButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["NEWSEARCH_QUESTIONLIST"] == null)
                    return;

                HiddenField OnlineInterviewQuestionSet_dataListIndexHiddenField = 
                    ((ImageButton)sender).FindControl("OnlineInterviewQuestionSet_dataListIndexHiddenField") 
                    as HiddenField;

                int selectedIndex = 0;

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_dataListIndexHiddenField.Value))
                {
                    selectedIndex = Convert.ToInt32(OnlineInterviewQuestionSet_dataListIndexHiddenField.Value);
                }

                //Get skill id and name
                ImageButton objImageButton = (ImageButton)sender;

                int skillID = 0;
                string skillName = string.Empty;

                if (!Utility.IsNullOrEmpty(objImageButton.CommandArgument))
                {
                    // Get command arguments
                    string[] arrArgument = objImageButton.CommandArgument.Split(new char[] { '~' });

                    skillID = Convert.ToInt32(arrArgument[0]);
                    skillName = arrArgument[1].ToString();

                    AddQusetionSet(selectedIndex, skillID, skillName);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the OnlineInterviewQuestionSet_searchQuestionImageButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void OnlineInterviewQuestionSet_deleteQuestionImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            try
            {
                int selectedIndex = 0;
                int skillID = 0;
                int questionRelationID = 0;

                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField =
                   ((ImageButton)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField")
                   as HiddenField;

                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField =
                ((ImageButton)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField")
                    as HiddenField;

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField.Value))
                {
                    skillID = 
                        Convert.ToInt32(OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField.Value.Trim());
                }

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField.Value))
                {
                    questionRelationID = 
                        Convert.ToInt32(OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField.Value.Trim());
                }

                if (skillID != 0 && questionRelationID!=0)
                {
                    selectedIndex = GetParentDataListIndex(skillID);

                    List<QuestionDetail> questions = null;
                    
                    if (Session["SKILL_QUESTIONS"] == null)
                        questions = new List<QuestionDetail>();
                    else
                        questions = Session["SKILL_QUESTIONS"] as List<QuestionDetail>;

                    if (questions != null && questions.Count > 0)
                    {
                        questions.RemoveAll(x => x.SkillID == skillID && x.QuestionRelationId == questionRelationID);

                        //Delete from skill question table also
                        new OnlineInterviewBLManager().DeleteOnlineInterviewSkillQuesionByQuestionKey(interviewkey,
                        skillID, questionRelationID);

                        Session["SKILL_QUESTIONS"] = questions;

                        AssignSkillQuestions(selectedIndex, skillID);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }


          /// <summary>
        /// Handles the Click event of the OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox_Click(object sender,
            EventArgs e)
        {
            try
            {

                QuestionDetail updateQuestion = new QuestionDetail();

                //Get current check box status
                CheckBox OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox = (CheckBox)sender;

                updateQuestion.IsMandatory = OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox.Checked ? "Y" : "N";
                
                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField =
                   ((CheckBox)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField")
                   as HiddenField;

                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField =
                   ((CheckBox)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField")
                   as HiddenField;

                int skillID = 0;
                int questionRelationID = 0;

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField.Value))
                {
                    skillID = Convert.ToInt32(OnlineInterviewQuestionSet_categoryQuestionDataList_skillIDHiddenField.Value);
                    updateQuestion.SkillID = skillID;
                }

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField.Value))
                {
                    questionRelationID = Convert.ToInt32(OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField.Value);
                    updateQuestion.QuestionRelationId = questionRelationID;
                }

                if (skillID != 0 && questionRelationID != 0)
                {
                    IDbTransaction transaction = null;
                    
                    transaction = new TransactionManager().Transaction;

                    //Save the selected interview questions
                    new OnlineInterviewBLManager().InsertOnlineInterviewSkillQuestion(updateQuestion,
                        interviewkey, base.userID, transaction);

                    //Commit the transaction
                    transaction.Commit();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of 
        /// the OnlineInterviewQuestionSet_categoryQuestionDataList_ItemDataBound control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataListItemEventArgs"/> instance containing the event data.</param>
        protected void OnlineInterviewQuestionSet_categoryQuestionDataList_ItemDataBound(object sender,
           DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                        e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Get the mandatory value
                    HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_isMandatoryHiddenField =
                        (HiddenField)e.Item.FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_isMandatoryHiddenField");

                    //Assign the mandatory value
                    CheckBox OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox =
                        (CheckBox)e.Item.FindControl("OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox");

                    if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_categoryQuestionDataList_isMandatoryHiddenField.Value) &&
                        OnlineInterviewQuestionSet_categoryQuestionDataList_isMandatoryHiddenField.Value.Trim().ToUpper()=="Y")
                    {
                        OnlineInterviewQuestionSet_isQuestionMandatoryCheckBox.Checked = true;
                    }
                }

                if (e.Item.ItemType == ListItemType.Footer)
                {
                    if (((DataList)sender).Items.Count == 0)
                    {
                        Label OnlineInterviewQuestionSet_noRecordLabel = 
                            (Label)e.Item.FindControl("OnlineInterviewQuestionSet_noRecordLabel");
                        OnlineInterviewQuestionSet_noRecordLabel.Visible = true;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void OnlineInterviewQuestionSet_cancelClick(object sender, EventArgs e)
        {
            try
            {
                OnlineInterviewQuestionSet_previewQuestionAddHiddenField.Value = "";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever preview question image button
        /// is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewQuestionSet_previewQuestionImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            try
            {
                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_questionKeyHiddenField =
                   ((ImageButton)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_questionKeyHiddenField")
                   as HiddenField;

                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField =
                ((ImageButton)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField")
                    as HiddenField;

                ShowQuestionPreview(OnlineInterviewQuestionSet_categoryQuestionDataList_questionKeyHiddenField.Value,
                    OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField.Value);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to show the question preview
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewQuestionSet_questionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_questionKeyHiddenField =
                   ((LinkButton)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_questionKeyHiddenField")
                   as HiddenField;

                HiddenField OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField =
                ((LinkButton)sender).FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField")
                    as HiddenField;

                ShowQuestionPreview(OnlineInterviewQuestionSet_categoryQuestionDataList_questionKeyHiddenField.Value,
                     OnlineInterviewQuestionSet_categoryQuestionDataList_questionRelationIDHiddenField.Value);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to show the question preview
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void OnlineInterviewQuestionSet_searchResultsDataList_questionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenField OnlineInterviewQuestionSet_searchResultsDataList_questionKeyHiddenField =
                   ((LinkButton)sender).FindControl("OnlineInterviewQuestionSet_searchResultsDataList_questionKeyHiddenField")
                   as HiddenField;

                HiddenField OnlineInterviewQuestionSet_searchResultsDataList_questionRelationIDHiddenField =
                ((LinkButton)sender).FindControl("OnlineInterviewQuestionSet_searchResultsDataList_questionRelationIDHiddenField")
                    as HiddenField;

                ShowQuestionPreview(OnlineInterviewQuestionSet_searchResultsDataList_questionKeyHiddenField.Value,
                     OnlineInterviewQuestionSet_searchResultsDataList_questionRelationIDHiddenField.Value);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    exp.Message);
            }
        }

        
        /// <summary>
        /// Handler method event is raised when save button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewQuestionSet_SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/OnlineInterviewAssessor.aspx?m=3&s=0&interviewkey="
                          + interviewkey + "&parentpage=S_OITSN", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised when save button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewCreation_searchedInterviewSkillButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(OnlineInterviewCreation_searchedInterviewKeyHiddenField.Value))
                {
                    LoadDropDownSkill(OnlineInterviewCreation_searchedInterviewKeyHiddenField.Value.Trim(),
                        OnlineInterviewQuestionSet_skillDropDownList,true);

                    OnlineInterviewQuestionSet_importedInterviewSkillDiv.Style["display"] = "block";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when search image button is clicked.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnlineInterviewQuestionSet_searchImageButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                SearchQuestionSet(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void SearchQuestionSet(int pageNumber)
        {
            int totalRecords = 0;

            OnlineInterviewQuestionSet_searchResultsDataListPageNavigator.Visible = true;
            if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_searchTypeHiddenField.Value) &&
                    OnlineInterviewQuestionSet_searchTypeHiddenField.Value.ToString().Trim() == "R")
            {
                List<QuestionDetail> recommendedQuestion = new List<QuestionDetail>();
                string keywords = string.Empty;

                if (OnlineInterviewQuestionSet_automatedSelectionRadioButton.Checked)
                {
                    OnlineInterviewQuestionSet_searchResultsDataListPageNavigator.Visible = false;
                    List<SkillDetail> skillDetail = new List<SkillDetail>();

                    skillDetail = new OnlineInterviewBLManager().
                                   GetSkillByOnlineInterviewKey(interviewkey);

                    keywords = string.Join(",", skillDetail.Select(s => s.SkillName).ToArray());

                    recommendedQuestion = LoadRecommendedQuestion(keywords);

                    if (recommendedQuestion != null)
                    {
                        OnlineInterviewQuestionSet_searchResultsDataList.DataSource = null;
                        OnlineInterviewQuestionSet_searchResultsDataList.DataBind();

                        OnlineInterviewQuestionSet_searchResultsDataList.DataSource = recommendedQuestion;
                        OnlineInterviewQuestionSet_searchResultsDataList.DataBind();

                        LoadDropDownSkill(interviewkey,
                            OnlineInterviewQuestionSet_moveQuestionSkillDropDownList, false);

                        OnlineInterviewQuestionSet_searchResultsDiv.Style["display"] = "block";
                        OnlineInterviewQuestionSet_questionMoveDiv.Style["display"] = "block";
                    }
                }
                else
                {
                    keywords = OnlineInterviewQuestionSet_keywordsTextBox.Text.ToString().Trim();
                    
                    List<QuestionDetail> manualQuestionDetails = new List<QuestionDetail>();
                    QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

                    questionDetailSearchCriteria.Tag = keywords.Trim() == "" ? null : keywords.Trim();
                    questionDetailSearchCriteria.ActiveFlag = null;
                    questionDetailSearchCriteria.Author = base.userID;

                    manualQuestionDetails = new InterviewBLManager().GetInterviewSearchQuestions(questionDetailSearchCriteria,
                        base.GridPageSize, pageNumber, "QUESTIONKEY", "A", out totalRecords);

                    if (manualQuestionDetails != null)
                    {
                        OnlineInterviewQuestionSet_searchResultsDataList.DataSource = null;
                        OnlineInterviewQuestionSet_searchResultsDataList.DataBind();

                        OnlineInterviewQuestionSet_searchResultsDataList.DataSource = manualQuestionDetails;
                        OnlineInterviewQuestionSet_searchResultsDataList.DataBind();

                        OnlineInterviewQuestionSet_searchResultsDataListPageNavigator.PageSize = base.GridPageSize;
                        OnlineInterviewQuestionSet_searchResultsDataListPageNavigator.TotalRecords = totalRecords;

                        LoadDropDownSkill(interviewkey,
                            OnlineInterviewQuestionSet_moveQuestionSkillDropDownList, false);

                        OnlineInterviewQuestionSet_searchResultsDiv.Style["display"] = "block";
                        OnlineInterviewQuestionSet_questionMoveDiv.Style["display"] = "block";
                    }
                }
            }
            else
            {
                string searchedInterviewKey =
                    OnlineInterviewCreation_searchedInterviewKeyHiddenField.Value.Trim();

                int selectedSkillID = 0;

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_skillDropDownList.SelectedValue) &&
                    OnlineInterviewQuestionSet_skillDropDownList.SelectedValue.Trim() != "Show All")
                {
                    selectedSkillID = Convert.ToInt32(OnlineInterviewQuestionSet_skillDropDownList.SelectedValue);
                }

                if (!Utility.IsNullOrEmpty(searchedInterviewKey))
                {
                    List<QuestionDetail> importedInterviewQuestion = new List<QuestionDetail>();
                    //importedInterviewQuestion = new OnlineInterviewBLManager().
                    //    GetOnlineInterviewSkillQuestion(searchedInterviewKey, selectedSkillID);

                    importedInterviewQuestion = new OnlineInterviewBLManager().
                        GetOnlineInterviewSearchQuestion(searchedInterviewKey, selectedSkillID, pageNumber,
                    base.GridPageSize, out totalRecords);

                    if (importedInterviewQuestion != null)
                    {
                        OnlineInterviewQuestionSet_searchResultsDataList.DataSource = null;
                        OnlineInterviewQuestionSet_searchResultsDataList.DataBind();

                        OnlineInterviewQuestionSet_searchResultsDataList.DataSource = importedInterviewQuestion;
                        OnlineInterviewQuestionSet_searchResultsDataList.DataBind();

                        OnlineInterviewQuestionSet_searchResultsDataListPageNavigator.PageSize = base.GridPageSize;
                        OnlineInterviewQuestionSet_searchResultsDataListPageNavigator.TotalRecords = totalRecords;

                        LoadDropDownSkill(interviewkey,
                            OnlineInterviewQuestionSet_moveQuestionSkillDropDownList, false);

                        OnlineInterviewQuestionSet_searchResultsDiv.Style["display"] = "block";
                        OnlineInterviewQuestionSet_questionMoveDiv.Style["display"] = "block";
                    }
                }
            }
        }


        /// <summary>
        /// Handler method event is raised when move button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void OnlineInterviewQuestionSet_questionMoveButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (!IsValidMoveQuesion())
                    return;

                string skillName = string.Empty;
                int selectedIndex = 0;

                int skillID = 0;

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_moveQuestionSkillDropDownList.SelectedValue))
                {
                    skillID = Convert.ToInt32(OnlineInterviewQuestionSet_moveQuestionSkillDropDownList.SelectedValue);
                    skillName = OnlineInterviewQuestionSet_moveQuestionSkillDropDownList.SelectedItem.Text;
                }

                selectedIndex = GetParentDataListIndex(skillID);

                List<QuestionDetail> skillQuestions = null;

                if (Session["SKILL_QUESTIONS"] == null)
                    skillQuestions = new List<QuestionDetail>();
                else
                    skillQuestions = Session["SKILL_QUESTIONS"] as List<QuestionDetail>;


                QuestionBLManager questionBLManager = new QuestionBLManager();

                IDbTransaction transaction = null;
                transaction = new TransactionManager().Transaction;

                foreach (DataListItem dl in OnlineInterviewQuestionSet_searchResultsDataList.Items)
                {
                    // Create a question object.
                    QuestionDetail searchedQuestion = new QuestionDetail();

                    CheckBox OnlineInterviewQuestionSet_searchResultsDataList_selectCheckBox =
                        (CheckBox)dl.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_selectCheckBox");

                    if (OnlineInterviewQuestionSet_searchResultsDataList_selectCheckBox.Checked)
                    {

                        LinkButton OnlineInterviewQuestionSet_searchResultsDataList_questionLinkButton =
                        (LinkButton)dl.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_questionLinkButton");

                        HiddenField OnlineInterviewQuestionSet_searchResultsDataList_questionKeyHiddenField =
                            (HiddenField)dl.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_questionKeyHiddenField");

                        HiddenField OnlineInterviewQuestionSet_searchResultsDataList_complexityNameHiddenField =
                            (HiddenField)dl.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_complexityNameHiddenField");

                        HiddenField OnlineInterviewQuestionSet_searchResultsDataList_complexityHiddenField =
                            (HiddenField)dl.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_complexityHiddenField");

                        HiddenField OnlineInterviewQuestionSet_searchResultsDataList_testAreaIDHiddenField =
                            (HiddenField)dl.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_testAreaIDHiddenField");

                        HiddenField OnlineInterviewQuestionSet_searchResultsDataList_testAreaNameHiddenField =
                            (HiddenField)dl.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_testAreaNameHiddenField");
    
                        searchedQuestion.SkillID = skillID;
                        searchedQuestion.SkillName = skillName;
                        searchedQuestion.QuestionKey = OnlineInterviewQuestionSet_searchResultsDataList_questionKeyHiddenField.Value.Trim();
                        searchedQuestion.Question = OnlineInterviewQuestionSet_searchResultsDataList_questionLinkButton.Text.Trim();
                        searchedQuestion.ComplexityName = OnlineInterviewQuestionSet_searchResultsDataList_complexityNameHiddenField.Value.Trim();
                        searchedQuestion.Complexity = OnlineInterviewQuestionSet_searchResultsDataList_complexityHiddenField.Value.Trim();
                        searchedQuestion.TestAreaID = OnlineInterviewQuestionSet_searchResultsDataList_testAreaIDHiddenField.Value.Trim();
                        searchedQuestion.TestAreaName = OnlineInterviewQuestionSet_searchResultsDataList_testAreaNameHiddenField.Value.Trim();
                        searchedQuestion.IsMandatory = "N";
                        searchedQuestion.QuestionRelationId = questionBLManager.GetQuestionRelationID(searchedQuestion.QuestionKey);
                        searchedQuestion.CategoryName = questionBLManager.GetCategory(searchedQuestion.QuestionKey);
                        searchedQuestion.SubjectName = questionBLManager.GetSubject(searchedQuestion.QuestionKey);

                        //Save the selected interview questions
                        if (!Utility.IsNullOrEmpty(searchedQuestion.QuestionKey))
                        {
                            int indexMove = skillQuestions.FindIndex(item => item.QuestionRelationId == searchedQuestion.QuestionRelationId &&
                                item.SkillID == searchedQuestion.SkillID);

                            if (indexMove == -1)
                            {
                                skillQuestions.Add(searchedQuestion);

                                //Save the selected interview questions
                                new OnlineInterviewBLManager().InsertOnlineInterviewSkillQuestion(searchedQuestion,
                                    interviewkey, base.userID, transaction);
                            }
                        }
                    }
                }

                //Commit the transaction
                transaction.Commit();

                //Append the newly added search questions to session
                Session["SKILL_QUESTIONS"] = skillQuestions;

                AssignSkillQuestions(selectedIndex, skillID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method thats shows the question preview details
        /// </summary>
        /// <param name="questionKey">The question key</param>
        /// <param name="questionRelationID">The question relation id</param>
        private void ShowQuestionPreview(string questionKey, string questionRelationID)
        {
            if (!Utility.IsNullOrEmpty(questionKey) && !Utility.IsNullOrEmpty(questionRelationID) 
                && Convert.ToInt32(questionRelationID) != 0)
            {
                OnlineInterviewQuestionSet_questionDetailSummaryControl.LoadQuestionDetails
                  (questionKey, Convert.ToInt32(questionRelationID));

                OnlineInterviewQuestionSet_previewQuestionAddHiddenField.Value = null;
                OnlineInterviewQuestionSet_questionDetailSummaryControl.ShowAddButton = false;
                OnlineInterviewQuestionSet_questionModalPopupExtender.Show();
            }
        }


        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            OnlineInterviewQuestionSet_topSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewQuestionSet_bottomSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewQuestionSet_topErrorMessageLabel.Text = string.Empty;
            OnlineInterviewQuestionSet_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method to assign the added/deleted questions to datalist
        /// </summary>
        /// <param name="selectedIndex">The child data list id</param>
        /// <param name="skillID">The skill ID</param>
        private void AssignSkillQuestions(int selectedIndex, int skillID)
        {
            List<QuestionDetail> skillQuestions = null;

            if (Session["SKILL_QUESTIONS"] == null)
                skillQuestions = new List<QuestionDetail>();
            else
                skillQuestions = Session["SKILL_QUESTIONS"] as List<QuestionDetail>;

            //filter by skill id
            var skillQuestion = Enumerable.Where(skillQuestions, s => s.SkillID == skillID);

            // findControl function to get the nested datalist control
            DataList nestedDataList = (DataList)OnlineInterviewQuestionSet_categoryDataList.Items[selectedIndex].
                FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList");

            if (skillQuestion != null)
            {
                nestedDataList.DataSource = skillQuestion;
                nestedDataList.DataBind();
                LoadInterviewSummary(skillQuestions);
            }
        }

        
        /// <summary>
        /// Method to reset the question image
        /// </summary>
        private void ResetQuestionImage()
        {
            Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            OnlineInterviewQuestionSet_questionImage.ImageUrl = string.Empty;
            OnlineInterviewQuestionSet_addQuestionImageTR.Style.Add("display", "none");
            OnlineInterviewQuestionSet_DisplayQuestionImageTR.Style.Add("display", "none");
            OnlineInterviewQuestionSet_addImageLinkButton.Style.Add("display", "");
        }

        /// <summary>
        /// Saves the new question.
        /// </summary>
        public void AddQusetionSet(int selectedIndex, int skillID, string skillName)
        {
            List<QuestionDetail> questions = null;
            List<QuestionDetail> quest = null;
            QuestionDetail questionDetail = null;

            if (Session["SKILL_QUESTIONS"] == null)
                questions = new List<QuestionDetail>();
            else
                questions = Session["SKILL_QUESTIONS"] as List<QuestionDetail>;

            QuestionBLManager questionBLManager = new QuestionBLManager();

            IDbTransaction transaction = null;

            if (Session["NEWSEARCH_QUESTIONLIST"] != null)
            {
                if (Session["NEWSEARCH_QUESTIONLIST"] == null)
                    quest = new List<QuestionDetail>();
                else
                    quest = Session["NEWSEARCH_QUESTIONLIST"] as List<QuestionDetail>;

                
                transaction = new TransactionManager().Transaction;

                for (int i = 0; i < quest.Count; i++)
                {
                    questionDetail = new QuestionDetail();
                    questionDetail.SkillID = skillID;
                    questionDetail.SkillName = skillName;
                    questionDetail.QuestionKey = quest[i].QuestionKey;
                    questionDetail.Question = quest[i].Question;
                    questionDetail.ComplexityName = quest[i].Complexity;
                    questionDetail.Complexity = quest[i].ComplexityID;
                    questionDetail.Author = quest[i].Author;
                    questionDetail.TestAreaID = quest[i].TestAreaID;
                    questionDetail.TestAreaName = quest[i].TestAreaName;
                    questionDetail.Tag = "";
                    questionDetail.CreatedBy = quest[i].CreatedBy;
                    questionDetail.CreatedDate = DateTime.Now;
                    questionDetail.ModifiedBy = quest[i].ModifiedBy;
                    questionDetail.IsMandatory = "N";
                    questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
                    questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
                    questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);

                    int index = questions.FindIndex(item => 
                        item.QuestionRelationId == questionDetail.QuestionRelationId && 
                        item.SkillID == questionDetail.SkillID);

                    if (index == -1)
                    {
                        questions.Add(questionDetail);
                        Session["SKILL_QUESTIONS"] = questions.ToList();

                        //Save the selected interview questions
                        new OnlineInterviewBLManager().InsertOnlineInterviewSkillQuestion(questionDetail,
                            interviewkey, base.userID, transaction);
                    }
                }
                //Commit the transaction
                transaction.Commit();
            }
            else
            {
                transaction = new TransactionManager().Transaction;

                questionDetail = new QuestionDetail();
                questionDetail = ConstructQuestionDetails();
                new QuestionBLManager().SaveInterviewQuestion(questionDetail);
                questionDetail.SkillID = skillID;
                questionDetail.SkillName = skillName;
                questionDetail.IsMandatory = "N";
                questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
                questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
                questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);

                questions.Add(questionDetail);
                Session["SKILL_QUESTIONS"] = questions.ToList();

                //Save the selected interview questions
                new OnlineInterviewBLManager().InsertOnlineInterviewSkillQuestion(questionDetail,
                    interviewkey, base.userID, transaction);

                //Commit the transaction
                transaction.Commit();

                ResetQuestionImage();
            }

            AssignSkillQuestions(selectedIndex, skillID);
        }

        /// <summary>
        /// This method loads the chart details to the test statistics object.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object by adding chart controls data.</param>
        private void LoadChartDetails(ref List<QuestionDetail> questionDetails,
            ref TestStatistics testStatistics)
        {
            ChartData chartData = null;
            List<ChartData> testAreaChartDatum = null;
            List<ChartData> complexityChartDatum = null;
            try
            {
                var complexityGroups =
                                          from q in questionDetails
                                          group q by q.ComplexityName into g
                                          select new { ComplexityName = g.Key, ComplexityCount = g.Count() };
                complexityChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var complexity in complexityGroups)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = complexity.ComplexityName;
                    chartData.ChartYValue = complexity.ComplexityCount;
                    complexityChartDatum.Add(chartData);
                }
                chartData = null;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.ComplexityStatisticsChartData))
                    testStatistics.ComplexityStatisticsChartData = new SingleChartData();
                testStatistics.ComplexityStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.ComplexityStatisticsChartData.ChartData = complexityChartDatum;
                testStatistics.ComplexityStatisticsChartData.ChartLength = 140;
                testStatistics.ComplexityStatisticsChartData.ChartWidth = 300;
                testStatistics.ComplexityStatisticsChartData.ChartTitle = "Complexity Statistics";
                var testAreaCounts =
                        from t in questionDetails
                        group t by t.TestAreaName into g
                        select new { TestAreaName = g.Key, TestAreaCOunt = g.Count() };
                testAreaChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var testArea in testAreaCounts)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = testArea.TestAreaName;
                    chartData.ChartYValue = testArea.TestAreaCOunt;
                    testAreaChartDatum.Add(chartData);
                }
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.TestAreaStatisticsChartData))
                    testStatistics.TestAreaStatisticsChartData = new SingleChartData();
                testStatistics.TestAreaStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.TestAreaStatisticsChartData.ChartData = testAreaChartDatum;
                testStatistics.TestAreaStatisticsChartData.ChartLength = 140;
                testStatistics.TestAreaStatisticsChartData.ChartWidth = 300;
                testStatistics.TestAreaStatisticsChartData.ChartTitle = "Interview Area Statistics";
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(chartData)) chartData = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testAreaChartDatum)) testAreaChartDatum = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(complexityChartDatum)) complexityChartDatum = null;
            }
        }

        /// <summary>
        /// This method will load all the test statistics user control data.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object.</param>
        private void LoadTestStatisticsObject(ref List<QuestionDetail> questionDetails, 
            ref TestStatistics testStatistics)
        {
            TestDetail testDetail = null;
            try
            {
                testStatistics.NoOfQuestions = questionDetails.Count;
                LoadChartDetails(ref questionDetails, ref testStatistics);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        // <summary>
        /// This Method gives the summary of the result questions.
        /// Summary will be like 
        /// Category Name, subject, test area, complexity, total questions
        /// </summary>
        /// <param name="ResultQuestionDetails">Question List that o/p to user</param>
        /// <returns>List of strings with each string contains
        /// category & subject & Test Area & Complexity & 
        /// Toal Number of Questions shown to the user</returns>
        private TestStatistics AutomatedSummaryList(List<QuestionDetail> ResultQuestionDetails)
        {
            StringBuilder CategoryNames = new StringBuilder();
            TestStatistics testStatistics = null;
            int TotalCategory = 0;
            try
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics))
                    testStatistics = new TestStatistics();

                for (int i = 0; i < ResultQuestionDetails.Count; i++)
                {
                    if (CategoryNames.ToString().IndexOf("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() + " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() + " COMP: " + ResultQuestionDetails[i].Complexity.Trim()) >= 0)
                        continue;
                    TotalCategory = ResultQuestionDetails.FindAll(
                        p => "CAT: " + p.CategoryName.Trim() + " SUB: " + p.SubjectName.Trim() +
                    " TA: " + p.TestAreaName.Trim() +
                    " COMP: " + p.Complexity.Trim() == "CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim()).Count;
                    CategoryNames.Append("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim());
                    CategoryNames.Append(",");
                    TotalCategory = 0;
                }
                testStatistics.NoOfQuestions = ResultQuestionDetails.Count;
                return testStatistics;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CategoryNames)) CategoryNames = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics)) testStatistics = null;
            }
        }


        /// <summary>
        /// Constructs the question details.
        /// </summary>
        /// <returns></returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail.Question = OnlineInterviewQuestionSet_addQuestionQuestTextBox.Text.Trim();

            if (OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.SelectedItem.Value != null &&
                OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = OnlineInterviewQuestionSet_addQuestioncomplexityDropDownList.SelectedItem.Text;
            }

            //Set question type

            if (OnlineInterviewQuestionSet_isQuestionRepository.Checked)
                questionDetail.IsActiveFlag = "Y";
            else
                questionDetail.IsActiveFlag = "N";

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            AnswerChoice answerChoice = null;


            answerChoice = new AnswerChoice();
            answerChoice.ChoiceID = 1;
            answerChoice.Choice = OnlineInterviewQuestionSet_addQuestionAnswerTextBox.Text.Trim();
            answerChoices.Add(answerChoice);

            questionDetail.AnswerChoices = answerChoices;
            questionDetail.Author = Convert.ToInt32(base.userID);

            // Additional settings (Test AreaId).
            questionDetail.TestAreaID = OnlineInterviewQuestionSet_addQuestionRadioButtonList.SelectedValue;
            questionDetail.TestAreaName = OnlineInterviewQuestionSet_addQuestionRadioButtonList.SelectedItem.Text;

            questionDetail.Tag = OnlineInterviewQuestionSet_tagsTextBox.Text;

            questionDetail.CreatedBy = Convert.ToInt32(base.userID);
            questionDetail.CreatedDate = DateTime.Now;

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);
            //   questionDetail.Subjects = SingleQuestionEntry_categorySubjectControl.SubjectDataSource;

            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs = CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField.Value.ToString();


            if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;

            return questionDetail;
        }

        /// <summary>
        /// Method to load the skills and question against the interview
        /// </summary>
        public void LoadSkillQuestion()
        {
            List<SkillDetail> skillDetail = new List<SkillDetail>();

            skillDetail = new OnlineInterviewBLManager().GetSkillByOnlineInterviewKey(interviewkey);

            if (skillDetail != null && skillDetail.Count > 0)
            {
                OnlineInterviewQuestionSet_displayQuestionSectionDiv.Style["display"] = "block";
                OnlineInterviewQuestionSet_noSkillMsgDiv.Style["display"] = "none";
                //Bind the parent data list
                OnlineInterviewQuestionSet_categoryDataList.DataSource = skillDetail;
                OnlineInterviewQuestionSet_categoryDataList.DataBind();

                //Bind the child list

                List<QuestionDetail> skillQuestion = new List<QuestionDetail>();
                skillQuestion = new OnlineInterviewBLManager().GetOnlineInterviewSkillQuestion(interviewkey,0);

               
                Session["SKILL_QUESTIONS"] = skillQuestion;

                // foreach loop over each item of DataList control
                foreach (DataListItem Item in OnlineInterviewQuestionSet_categoryDataList.Items)
                {
                    BindNestedDataList(Item.ItemIndex);
                }

                if (skillQuestion != null)
                {
                    LoadInterviewSummary(skillQuestion);
                }
            }
            else
            {
                OnlineInterviewQuestionSet_displayQuestionSectionDiv.Style["display"] = "none";
                OnlineInterviewQuestionSet_noSkillMsgDiv.Style["display"] = "block";
            }
        }

        /// <summary>
        /// Method to load the interview summary
        /// </summary>
        /// <param name="skillQuestion"></param>
        private void LoadInterviewSummary(List<QuestionDetail> skillQuestion)
        {
            TestStatistics testStatistics = AutomatedSummaryList(skillQuestion);
            LoadTestStatisticsObject(ref skillQuestion, ref testStatistics);
            OnlineInterviewQuestionSet_questionSummaryUserControl.DataSource = testStatistics;
            OnlineInterviewQuestionSet_questionSummaryUserControl.Visible = true;

            //Get distinct skills and its count

            QuestionDetail skillSums = null;
            List<QuestionDetail> skillSummary = null;

            var distinctSkillSummary = (from a in skillQuestion
                                        group a by new { a.SkillName, a.SkillID }
                                            into g
                                            select new
                                            {
                                                SkillName = g.Key.SkillName,
                                                SkillID = g.Key.SkillID,
                                                Count = g.Count()

                                            }).ToList();

            skillSummary = new List<QuestionDetail>();
            if (Utility.IsNullOrEmpty(skillSums))
                skillSums = new QuestionDetail();

            foreach (var skill in distinctSkillSummary)
            {
                skillSums = new QuestionDetail();
                skillSums.SkillID = skill.SkillID;
                skillSums.SkillName = skill.SkillName;
                skillSums.SkillCount = skill.Count;
                skillSummary.Add(skillSums);
            }

            OnlineInterviewQuestionSet_questionSummaryUserControl.skillSet = skillSummary;
        }

        /// <summary>
        /// Method to load the skills and question against the interview
        /// </summary>
        public void LoadSearchQuestion()
        {
            List<QuestionDetail> questionDetails = null;

            if (Session["SKILL_QUESTIONS"] == null)
                questionDetails = new List<QuestionDetail>();
            else
                questionDetails = Session["SKILL_QUESTIONS"] as List<QuestionDetail>;

            OnlineInterviewQuestionSet_searchResultsDataList.DataSource = questionDetails;
            OnlineInterviewQuestionSet_searchResultsDataList.DataBind();
        }

        /// <summary>
        /// Method to bind the nested datalist
        /// </summary>
        /// <param name="ItemIndex">ItemIndex</param>
        private void BindNestedDataList(int ItemIndex)
        {
            int skillID = Convert.ToInt32(OnlineInterviewQuestionSet_categoryDataList.DataKeys[ItemIndex]);

            List<QuestionDetail> skillQuestions = new List<QuestionDetail>();

            if (Session["SKILL_QUESTIONS"] == null)
                skillQuestions = new List<QuestionDetail>();
            else
                skillQuestions = Session["SKILL_QUESTIONS"] as List<QuestionDetail>;
             
            //filter by skill id
            var questionFilteredList = Enumerable.Where(skillQuestions, s => s.SkillID == skillID);

            // findControl function to get the nested datalist control
            DataList nestedDataList = (DataList)OnlineInterviewQuestionSet_categoryDataList.Items[ItemIndex].
                FindControl("OnlineInterviewQuestionSet_categoryQuestionDataList");

            if (questionFilteredList != null)
            {
                nestedDataList.DataSource = questionFilteredList;
                nestedDataList.DataBind();
            }
        }

        /// <summary>
        /// Method to load the list of skills available against the interview key
        /// </summary>
        /// <param name="interviewkey">The interview key</param>
        /// <param name="dl">The drop down list control</param>
        /// <param name="addShowAll">To include show all in the </param>
        private void LoadDropDownSkill(string interviewkey, DropDownList dl,
            bool addShowAll)
        {
            List<SkillDetail> skillDetail = new List<SkillDetail>();

            skillDetail = new OnlineInterviewBLManager().
                GetSkillByOnlineInterviewKey(interviewkey);

            if (skillDetail != null)
            {
                dl.DataSource = skillDetail;
                dl.DataTextField = "SkillName";
                dl.DataValueField = "SkillID";
                dl.DataBind();

                if (addShowAll)
                {
                    dl.Items.Insert(0, "Show All");
                    dl.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        private bool IsValidMoveQuesion()
        {
            bool isValidQuestion = false;

            foreach (DataListItem li in OnlineInterviewQuestionSet_searchResultsDataList.Items)
            {
                CheckBox OnlineInterviewQuestionSet_searchResultsDataList_selectCheckBox =
                    (CheckBox)li.FindControl("OnlineInterviewQuestionSet_searchResultsDataList_selectCheckBox");

                if (OnlineInterviewQuestionSet_searchResultsDataList_selectCheckBox.Checked)
                {
                    isValidQuestion = true;
                    break;
                }
            }

            if (!isValidQuestion)
            {
                base.ShowMessage(OnlineInterviewQuestionSet_topErrorMessageLabel,
                    OnlineInterviewQuestionSet_bottomErrorMessageLabel,
                    "No question selected to move");
            }

            return isValidQuestion;
        }
        
        /// <summary>
        /// Method to get the selected skills index in parent datalist
        /// </summary>
        /// <param name="skillID">The skill ID</param>
        /// <returns>
        /// That returns the index of parent data list
        /// </returns>
        private int GetParentDataListIndex(int skillID)
        {
            int selectedIndex = 0;
            foreach (DataListItem dlParent in OnlineInterviewQuestionSet_categoryDataList.Items)
            {
                HiddenField OnlineInterviewQuestionSet_categoryDataList_skillIDHiddenField =
                   (HiddenField)dlParent.FindControl("OnlineInterviewQuestionSet_categoryDataList_skillIDHiddenField");

                if (!Utility.IsNullOrEmpty(OnlineInterviewQuestionSet_categoryDataList_skillIDHiddenField.Value))
                {
                    if (Convert.ToInt32(OnlineInterviewQuestionSet_categoryDataList_skillIDHiddenField.Value) == skillID)
                    {
                        HiddenField OnlineInterviewQuestionSet_dataListIndexHiddenField =
                            (HiddenField)dlParent.FindControl("OnlineInterviewQuestionSet_dataListIndexHiddenField");

                        selectedIndex = Convert.ToInt32(OnlineInterviewQuestionSet_dataListIndexHiddenField.Value);
                        break;
                    }
                }
            }
            return selectedIndex;
        }

        /// <summary>
        /// Method to get the list of manual list questions
        /// </summary>
        public List<QuestionDetail> LoadManualQuestion(string keyword,int pageNumber)
        {
            int TotalRecords;

            List<QuestionDetail> questionDetails = questionDetails = new List<QuestionDetail>();
            TestSearchCriteria interviewSearchCriteria = null;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(interviewSearchCriteria))
                interviewSearchCriteria = new TestSearchCriteria();

            interviewSearchCriteria.Keyword = keyword.ToString().Trim();
            interviewSearchCriteria.TestStartIndex = pageNumber;
            interviewSearchCriteria.TestEndIndex = 3;

            questionDetails = new OnlineInterviewBLManager().
                GetInterviewQuestionDetailsforAutomatedTest(interviewSearchCriteria,
                base.userID, base.tenantID, out TotalRecords);

            return questionDetails;
        }

        /// <summary>
        /// Method to get the list of automated list questions
        /// </summary>
        public List<QuestionDetail> LoadRecommendedQuestion(string keywords)
        {
            List<QuestionDetail> questionDetails = questionDetails = new List<QuestionDetail>();
            List<QuestionDetail> returnQuestionDetails = new List<QuestionDetail>();
           
            TestSearchCriteria interviewSearchCriteria = null;

            if (keywords != null)
            {
                foreach (string keyword in keywords.Split(new char[] { ',' }))
                {
                    int TotalRecords;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(interviewSearchCriteria))
                        interviewSearchCriteria = new TestSearchCriteria();

                    interviewSearchCriteria.Keyword = keyword.ToString().Trim();
                    interviewSearchCriteria.TestStartIndex = 1;
                    interviewSearchCriteria.TestEndIndex = 5;

                    questionDetails = new OnlineInterviewBLManager().
                        GetInterviewQuestionDetailsforAutomatedTest(interviewSearchCriteria,
                        base.userID, base.tenantID, out TotalRecords);

                    if (questionDetails != null)
                    {
                        foreach(QuestionDetail questionSearched in questionDetails)
                        {
                            returnQuestionDetails.Add(questionSearched);
                        }
                    }
                }
            }

            return returnQuestionDetails;
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            OnlineInterviewQuestionSet_topErrorMessageLabel.Text = "Done";
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Methods
    }
}