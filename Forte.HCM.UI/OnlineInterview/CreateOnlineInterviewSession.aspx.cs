#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateOnlineInterviewTestSession.cs
// File that represents the user interface for the create a new online interview 
// session. This will interact with the OnlineInterviewBLManager to insert online interview 
// session details to the database.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Text;
using System.Data;
using System.Web.UI;

#endregion Directives

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for CreateOnlineInterviewTestSession page. This is used
    /// to create a online interview session by providing number of candidates,
    /// session description, interview instructions etc. 
    /// Also this page provides some set of links in the online interview session
    /// grid for viewing candidate detail, test session and cancel the
    /// cancel the candidate online interview session. This class is inherited from
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CreateOnlineInterviewSession : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        private DataTable dttempTable = null;

        #endregion Private Variables

        #region Event Handlers
       
        /// <summary>
        /// Handler method that is called when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = CreateOnlineInterviewSession_topSaveButton.UniqueID;
                Master.SetPageCaption("Create Online Interview Session");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = CreateOnlineInterviewSession_sessionNameTextBox.UniqueID;
                    CreateOnlineInterviewSession_sessionNameTextBox.Focus();

                    TrAssessor.Visible = true;
                    if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                        ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                        ViewState["SORT_EXPRESSION"] = "InterviewSessionID";

                    if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                        ViewState["PAGE_NUMBER"] = "1";

                    if (Request.QueryString["interviewtestkey"] != null)
                    {
                        CreateOnlineInterviewSession_TestKeyHiddenField.Value =
                            Request.QueryString["interviewtestkey"].ToString();
                    }

                    CreateOnlineInterviewSession_SearchSkillButton.Attributes.Add("onclick",
                       "return LoadSkillLookup('" + CreateOnlineInterviewSession_CaegoryIDHiddenField.ClientID + "','" +
                       CreateOnlineInterviewSession_skillTextBox.ClientID + "','" +
                       CreateOnlineInterviewSession_SkillIDHiddenField.ClientID + "','" + 
                       CreateOnlineInterviewSession_NewSkillButton.ClientID + "')");

                    //LoadTestDetail(CreateOnlineInterviewSession_TestKeyHiddenField.Value);
                    CreateOnlineInterviewSession_positionProfileImageButton.Attributes.Add("onclick",
                    "return ShowPositionProfileLooKUpToAppendTextBoxContent('" + 
                    CreateOnlineInterviewSession_positionProfileTextBox.ClientID + "','" +
                    CreateOnlineInterviewSession_skillTextBox.ClientID + "','" +
                    CreateOnlineInterviewSession_positionProfileIDHiddenField.ClientID + "','" + 
                    CreateOnlineInterviewSession_NewSkillButton.ClientID + "')");
                    Session["ASSESSOR"] = null;
                    Session["SKILL"] = null;
                }
                
                // Add handler for email button.
                CreateOnlineInterviewSession_emailImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowEmailCandidateSessions('EMAIL_ONLINE_INTERVIEW_SESSION')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will help to validate input fields before they saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateOnlineInterviewSession_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                // Validate the data
                if (!IsValidData())
                    return;

                OnlineInterviewSessionDetail onlineInterviewSessionDetail = PreviewOnlineInterviewSession();
                CreateOnlineInterviewSession_viewTestSessionSave_UserControl.Mode = "view";

                CreateOnlineInterviewSession_viewTestSessionSave_UserControl.DataSource = onlineInterviewSessionDetail;
                CreateOnlineInterviewSession_viewTestSessionSave_modalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to delete the skill matrix
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateOnlineInterviewSession_skillGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                HiddenField SubjectValue_HiddenFiled = (HiddenField)CreateOnlineInterviewSession_skillGridView.
                    Rows[e.RowIndex].FindControl("CreateOnlineInterviewSession_skillGridView_skillIDHiddenField");

                if (string.IsNullOrEmpty(SubjectValue_HiddenFiled.Value)) return;

                dttempTable = (DataTable)ViewState["AssessorSkills"];

                foreach (DataRow _dr in dttempTable.Rows)
                {
                    if (SubjectValue_HiddenFiled.Value.ToString().Trim() ==
                        Convert.ToString(_dr["SkillID"]))
                    {
                        dttempTable.Rows.Remove(_dr);
                        dttempTable.AcceptChanges();
                        break;
                    }
                }

                ViewState["AssessorSkills"] = dttempTable;

                CreateOnlineInterviewSession_skillGridView.DataSource = dttempTable;
                CreateOnlineInterviewSession_skillGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateOnlineInterviewSession_NewSkillButton_Click
          (object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearLabelMessage();
                if (CreateOnlineInterviewSession_skillTextBox.Text.Trim() == "")
                {
                    base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                            CreateOnlineInterviewSession_bottomErrorMessageLabel,
                            "No skill entered to add");
                    return;
                }
                dttempTable = new DataTable();

                if (ViewState["AssessorSkills"] == null)
                {
                    dttempTable.Columns.Add("Category", typeof(string));
                    dttempTable.Columns.Add("SkillID", typeof(int));
                    dttempTable.Columns.Add("Skill", typeof(string));
                    dttempTable.AcceptChanges();
                }
                else
                    dttempTable = (DataTable)ViewState["AssessorSkills"];

                string[] searchedSkill = null;

                searchedSkill = CreateOnlineInterviewSession_skillTextBox.Text.ToString().Split(',');


                foreach (string skill in searchedSkill)
                {
                    bool skillExist = false;
                    AddManual_Skill(skill);
                    string categoryID_HiddenField = CreateOnlineInterviewSession_CaegoryIDHiddenField.Value.ToString().Trim();
                    string sbjectID_HiddenField = CreateOnlineInterviewSession_SkillIDHiddenField.Value.ToString();

                    if (dttempTable.Rows.Count > 0)
                    {

                        foreach (DataRow drow in dttempTable.Rows)
                        {
                            if (Convert.ToString(drow[0]) == Convert.ToString(categoryID_HiddenField)
                                && Convert.ToInt32(drow[1]) == Convert.ToInt32(sbjectID_HiddenField))
                            {
                                this.CreateOnlineInterviewSession_skillTextBox.Focus();
                                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                                    string.Concat(skill, " already added"));
                                skillExist = true;
                            }
                        }
                    }
                    if (!skillExist)
                    {
                        DataRow dr = dttempTable.NewRow();
                        dr[0] = categoryID_HiddenField;
                        dr[1] = sbjectID_HiddenField;
                        dr[2] = skill;

                        dttempTable.Rows.Add(dr);
                        dttempTable.AcceptChanges();

                        ViewState["AssessorSkills"] = dttempTable;
                    }
                }


                CreateOnlineInterviewSession_skillTextBox.Text = "";
                CreateOnlineInterviewSession_CaegoryIDHiddenField.Value = "";
                CreateOnlineInterviewSession_SkillIDHiddenField.Value = "";

                CreateOnlineInterviewSession_skillGridView.DataSource = null;
                CreateOnlineInterviewSession_skillGridView.DataBind();

                CreateOnlineInterviewSession_skillGridView.DataSource = dttempTable;
                CreateOnlineInterviewSession_skillGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary> 
        /// <param name="skill">
        /// <see cref="string"/>
        /// </param>
        private void AddManual_Skill(string skill)
        {
            DataTable dttemp = new InterviewBLManager().getSubjectCategoryID(skill);
            if (dttemp.Rows.Count != 0)
            {
                CreateOnlineInterviewSession_CaegoryIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][0]);
                CreateOnlineInterviewSession_SkillIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][1]);
                CreateOnlineInterviewSession_skillTextBox.Text = Convert.ToString(dttemp.Rows[0][2]);
            }
        }

        /// <summary>
        /// Handler that will call when clicking on create button which shows
        /// the given information before its getting inserted in the database.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateOnlineInterviewSession_previewTestSessionControl_createButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear success/error label message
                ClearLabelMessage();

                OnlineInterviewSessionDetail onlineInterviewSessionDetail = ConstructOnlineInterviewSessionDetail();
                CreditRequestDetail creditRequestDetail = new CreditRequestDetail();

                //Add time slots
                List<AssessorTimeSlotDetail> slots = Session["AVAILABILITY_REQUEST_LIST"] as List<AssessorTimeSlotDetail>;

                //Assessor list and selected skill id
                List<AssessorDetail> assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;
                
                ////creditRequestDetail.Amount = decimal.Parse(CreateOnlineInterviewSession_creditValueLabel.Text);

                string interviewSessionKey = null;
                string candidateSessionIDs = null;
                bool isMailSent = false;

                new OnlineInterviewBLManager().SaveOnlineInterviewSession(onlineInterviewSessionDetail,
                    slots, assessorDetails, base.userID,
                    out interviewSessionKey, out candidateSessionIDs, out isMailSent);

                // Keep the TestSessionID in Viewstate. 
                // This will be used by ScheduleCandidate button event handler
                ViewState["ONLINE_INTERVIEW_SESSION_ID"] = interviewSessionKey;

                base.ShowMessage(CreateOnlineInterviewSession_topSuccessMessageLabel,
                    CreateOnlineInterviewSession_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.
                    CreateOnlineInterviewSession_OnlineInteviewSessionAndCandidateSessionsCreatedSuccessfully,
                    interviewSessionKey, candidateSessionIDs));

                //To hide add assessor option once online interview session created
                if (!string.IsNullOrEmpty(interviewSessionKey))
                    TrAssessor.Visible = false;
                // If the mail sent failed, then show the error message.
                //if (isMailSent == false)
                //{
                //    base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                //        CreateOnlineInterviewSession_bottomErrorMessageLabel,
                //         "<br>Mail cannot be sent to the session author. Contact your administrator");
                //}

                // Set visible false once the session is created.
                CreateOnlineInterviewSession_topSaveButton.Visible = false;

                // Enable the schedule candidate button
                CreateOnlineInterviewSession_topScheduleCandidateButton.Visible = true;

                // Show the email session button.
                CreateOnlineInterviewSession_emailImageButton.Visible = true;

                // Keep the created test session details in session, so that it can
                // be used in the email popup.
                Session["EMAIL_ONLINE_INTERVIEW_SESSION_SUBJECT"] = string.Format
                    ("Online interview session '{0}' created", interviewSessionKey);
                Session["EMAIL_ONLINE_INTERVIEW_SESSION_MESSAGE"] = string.Format
                    ("Dear Viewer,\n\nSub: Online interview session '{0}' created.\n\nThe following candidate sessions are associated:\n{1}",
                    interviewSessionKey, candidateSessionIDs);
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateOnlineInterviewSession_addAssessorGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        /// <summary>
        /// Handler that will perform reset/clear the input fields.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateOnlineInterviewSession_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateOnlineInterviewSession_sessionNameTextBox.Text = string.Empty;
                CreateOnlineInterviewSession_positionProfileTextBox.Text = string.Empty;
                CreateOnlineInterviewSession_skillTextBox.Text = string.Empty;
                CreateOnlineInterviewSession_sessionDescTextBox.Text = string.Empty;
                CreateOnlineInterviewSession_instructionsTextBox.Text = string.Empty;
                CreateOnlineInterviewSession_sessionNoTextBox.Text = "0";
                CreateOnlineInterviewSession_expiryDateTextBox.Text = string.Empty;
                CreateOnlineInterviewSession_assessorDetailsGridView.DataSource = null;
                CreateOnlineInterviewSession_assessorDetailsGridView.DataBind();
                CreateOnlineInterviewSession_assessorDetailsDiv.Visible = false;
                ClearLabelMessage();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that performs to redirect to schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void CreateOnlineInterviewSession_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/OnlineInterview/ScheduleOnlineInterviewCandidate.aspx?m=3&s=1&interviewSessionKey="
                    + ViewState["ONLINE_INTERVIEW_SESSION_ID"], false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the recommend assessor link button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateOnlineInterviewSession_recommendAssessorLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();
                Session["SESSION_SUBJECT_TABLE"] = null;
                DataTable dtSkillSubject = new DataTable();
                dtSkillSubject = (DataTable)ViewState["AssessorSkills"];
                Session["SESSION_SUBJECT_TABLE"] = (DataTable)ViewState["AssessorSkills"];
                if (dtSkillSubject != null && dtSkillSubject.Rows.Count != 0)
                {
                    string OpenAssessorWindow = string.Empty;
                    OpenAssessorWindow = "SearchInterviewOnlineRecommendAssessor('SEARCH',null,1,null,'" + CreateOnlineInterviewSession_refreshGridButton.ClientID + "')";
                    ScriptManager.RegisterStartupScript(CreateOnlineInterviewSession_assessorLinkUpdatePanel,
                        CreateOnlineInterviewSession_assessorLinkUpdatePanel.GetType(), "OpenAssessorWindow", OpenAssessorWindow, true);
                }
                else
                {
                    base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel,
                    "Please select skills/areas");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Assign Selected Assessors to the grid
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateOnlineInterviewSession_refreshGridButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateOnlineInterviewSession_assessorDetailsGridView.DataSource = Session["ASSESSOR"] as List<AssessorDetail>;
                CreateOnlineInterviewSession_assessorDetailsGridView.DataBind();
                CreateOnlineInterviewSession_assessorDetailsDiv.Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers
        
        #region Private Methods

        /// <summary>
        /// A Method that gets the position profile keywords by removing there
        /// weightages
        /// </summary>
        /// <param name="KeyWordWithWeightages">
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// among with their weightages
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// by removing their weightages
        /// </returns>
        private string GetPositionProfileKeys(string KeyWordWithWeightages)
        {
            StringBuilder sbKeywords = null;
            string[] strSplitKeywords = null;
            try
            {
                strSplitKeywords = KeyWordWithWeightages.Split(',');
                sbKeywords = new StringBuilder();
                for (int i = 0; i < strSplitKeywords.Length; i++)
                    if (strSplitKeywords[i].IndexOf('[') != -1)
                        sbKeywords.Append(strSplitKeywords[i].Substring(0, strSplitKeywords[i].IndexOf('[')) + ",");
                    else
                        sbKeywords.Append(strSplitKeywords[i] + ",");
                return sbKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (strSplitKeywords != null) strSplitKeywords = null;
                if (sbKeywords != null) sbKeywords = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Method that will preview the online interview session details once the user clicked 
        /// on the save button.
        /// </summary>
        /// <returns>Returns the <see cref="onlineInterviewSessionDetail"/> object</returns>
        private OnlineInterviewSessionDetail PreviewOnlineInterviewSession()
        {
            // Initialize the test session detail object
            OnlineInterviewSessionDetail onlineInterviewSessionDetail = new OnlineInterviewSessionDetail();

            // Get the information from the controls and assign to the object members

            onlineInterviewSessionDetail.OnlineInterviewSessionName = CreateOnlineInterviewSession_sessionNameTextBox.Text;
            onlineInterviewSessionDetail.NumberOfCandidateSessions
                = Convert.ToInt32(CreateOnlineInterviewSession_sessionNoTextBox.Text);

            onlineInterviewSessionDetail.PositionProfileName = CreateOnlineInterviewSession_positionProfileTextBox.Text;

            // Set the time to maximum in expiry date.
            if(CreateOnlineInterviewSession_expiryDateTextBox.Text!=string.Empty)
                onlineInterviewSessionDetail.ExpiryDate = (Convert.ToDateTime(CreateOnlineInterviewSession_expiryDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

            onlineInterviewSessionDetail.OnlineInterviewSessionDescription = CreateOnlineInterviewSession_sessionDescTextBox.Text;
            onlineInterviewSessionDetail.OnlineInterviewInstruction = CreateOnlineInterviewSession_instructionsTextBox.Text;
            onlineInterviewSessionDetail.Skills = CreateOnlineInterviewSession_skillTextBox.Text;
            onlineInterviewSessionDetail.CreatedBy = base.userID;
            onlineInterviewSessionDetail.ModifiedBy = base.userID;

            return onlineInterviewSessionDetail;
        }

        /// <summary>
        /// This method gets the seconds values and updated the time format as hh:mm:yyyy
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string GetTimeFormat(int seconds)
        {
            return Utility.ConvertSecondsToHoursMinutesSeconds(seconds);
        }


        /// <summary>
        /// This method returns the OnlineInterivewSessionDetails instance with all the values 
        /// given as input by user.
        /// </summary>
        /// <returns></returns>
        private OnlineInterviewSessionDetail ConstructOnlineInterviewSessionDetail()
        {
            // Initialize test session detail object
            OnlineInterviewSessionDetail onlineInterviewSessionDetail = new OnlineInterviewSessionDetail();

            // Check whether a test key is empty or not before assigning to 

            // Set session name name
           onlineInterviewSessionDetail.OnlineInterviewSessionName = CreateOnlineInterviewSession_sessionNameTextBox.Text;

            // Set number of candidate session (session count)
            onlineInterviewSessionDetail.NumberOfCandidateSessions =
                Convert.ToInt32(CreateOnlineInterviewSession_sessionNoTextBox.Text);


            // Set position profile ID.
            if (!Utility.IsNullOrEmpty(CreateOnlineInterviewSession_positionProfileIDHiddenField.Value))
                onlineInterviewSessionDetail.PositionProfileID =
                    Convert.ToInt32(CreateOnlineInterviewSession_positionProfileIDHiddenField.Value);
            else
                onlineInterviewSessionDetail.PositionProfileID = 0;

            // Set Skills/Areas
            onlineInterviewSessionDetail.Skills = CreateOnlineInterviewSession_skillTextBox.Text;
            // Set expiry date
            if(CreateOnlineInterviewSession_expiryDateTextBox.Text!=string.Empty)
                onlineInterviewSessionDetail.ExpiryDate = Convert.ToDateTime(CreateOnlineInterviewSession_expiryDateTextBox.Text);

            // Set created by
            onlineInterviewSessionDetail.CreatedBy = base.userID;

            // Set modified by
            onlineInterviewSessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            onlineInterviewSessionDetail.OnlineInterviewInstruction =
                CreateOnlineInterviewSession_instructionsTextBox.Text.ToString().Trim();

            // Set session descriptions
            onlineInterviewSessionDetail.OnlineInterviewSessionDescription =
                CreateOnlineInterviewSession_sessionDescTextBox.Text.ToString().Trim();

            if (Session["ASSESSOR"] != null)
            {
                List<AssessorDetail> assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;
                //for (int i = 0; i < assessorDetails.Count; i++)
                //{
                //    onlineInterviewSessionDetail.AssessorIDs += assessorDetails[i].UserID + ",";
                //}
                //onlineInterviewSessionDetail.AssessorIDs = onlineInterviewSessionDetail.AssessorIDs.ToString().Remove(onlineInterviewSessionDetail.AssessorIDs.Length - 1);
            }
            return onlineInterviewSessionDetail;
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            CreateOnlineInterviewSession_topSuccessMessageLabel.Text = string.Empty;
            CreateOnlineInterviewSession_bottomSuccessMessageLabel.Text = string.Empty;
            CreateOnlineInterviewSession_topErrorMessageLabel.Text = string.Empty;
            CreateOnlineInterviewSession_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isAssessorInvalid = false;
            // Validate session description text field
            if (CreateOnlineInterviewSession_sessionNameTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateInterviewTestSession_SessionNameCannotBeEmpty);
            }

            //// Validate skills/areas text field
            //if (CreateOnlineInterviewSession_skillTextBox.Text.Trim().Length == 0)
            //{
            //    isValidData = false;
            //    base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
            //        CreateOnlineInterviewSession_bottomErrorMessageLabel,
            //        Resources.HCMResource.CreateInterviewTestSession_skillsCannotBeEmpty);
            //}
            
            // Validate number of session field
            int sessions = 0;
            bool validSession = int.TryParse(CreateOnlineInterviewSession_sessionNoTextBox.Text, out sessions);
            if (validSession == false || sessions <= 0 || sessions > 30)
            {
                isValidData = false;
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateInterviewTestSession_SessionCountCannotBeZero);
            }

            CreateOnlineInterviewSession_MaskedEditValidator.Validate();

            if (CreateOnlineInterviewSession_expiryDateTextBox.Text.Trim().Length != 0)
            {
                // This will check if the date is valid date. i.e. 99/99/9999
                if (!CreateOnlineInterviewSession_MaskedEditValidator.IsValid)
                {
                    isValidData = false;
                    base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel,
                    CreateOnlineInterviewSession_MaskedEditValidator.ErrorMessage);
                }
                else
                {
                    // Check if the expiry date is less than current date
                    if (DateTime.Parse(CreateOnlineInterviewSession_expiryDateTextBox.Text) < DateTime.Today)
                    {
                        isValidData = false;
                        base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                        CreateOnlineInterviewSession_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateInterviewTestSession_ExpiryDateCannotBeLessThanCurrentDate);
                    }
                }
            }

            if (Session["ASSESSOR"] != null)
            {
                if ((Session["ASSESSOR"] as List<AssessorDetail>).Count == 0)
                    isAssessorInvalid = true;
            }
            else
                isAssessorInvalid = true;

            if (isAssessorInvalid)
            {
                isValidData = false;
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                CreateOnlineInterviewSession_bottomErrorMessageLabel,
                "Assessor cannot be empty");
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Method to call add assessor and assign to grid values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateOnlineInterviewSession_addAssessorLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ASSESSOR"] == null)
                    return;
                else
                {
                    List<AssessorDetail> slots = Session["ASSESSOR"] as List<AssessorDetail>;
                    CreateOnlineInterviewSession_assessorDetailsGridView.DataSource = Session["ASSESSOR"] as List<AssessorDetail>;
                    CreateOnlineInterviewSession_assessorDetailsGridView.DataBind();
                    CreateOnlineInterviewSession_assessorDetailsDiv.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateOnlineInterviewSession_topErrorMessageLabel,
                    CreateOnlineInterviewSession_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Protected Methods
    }
}