
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ScheduleOnlineInterviewCandidate.aspx.cs
// This page allows the recruiter to schedule the online interview to candidates. 
// They can reschedule, schedule, unschedule the candidate session tests.

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.OnlineInterview
{
    /// <summary>
    /// This page allows the recruiter to schedule the online interview to candidates. 
    /// They can reschedule, schedule, unschedule the candidate session tests.
    /// </summary>
    public partial class ScheduleOnlineInterviewCandidate : PageBase
    {
        #region Private Members                                                

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "100%";
        private string _tsid = string.Empty;

        #endregion Private Members

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when a page is loaded. During that time,
        /// it'll take the responsibility to implement the default settings
        /// such as page title, default button, and calling javascript
        /// functions etc.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set page title
                Master.SetPageCaption("Schedule Online Interview Candidate");
                CheckAndSetExpandorRestore();

                // Set default button.
                Page.Form.DefaultButton = ScheduleOnlineInterviewCandidate_showButton.UniqueID;
                  UserDetail userDetail =new UserDetail();
                  userDetail = (UserDetail)Session["USER_DETAIL"];

                
                ScheduleOnlineInterviewCandidate_onlineSessionImageButton.Visible = true;
                

                if (!Page.IsPostBack)
                {
                    LoadValues();

                    // Set default focus
                    ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Focus();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING))
                    {
                        base.ClearSearchCriteriaSession();
                    }
                    else
                    {
                        // If any querystring is passed(i.e. interviewSessionKey)
                        if (!Utility.IsNullOrEmpty(Request.QueryString["interviewSessionKey"]))
                        {
                            _tsid = Convert.ToString(Request.QueryString["interviewSessionKey"]);

                            // If test session id is passed in Querystring, then display all the informations
                            // with respect to testSessionId.
                            ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text = _tsid;
                            ScheduleOnlineInterviewCandidate_testDetailsDiv.Style["display"] = "block";
                            ScheduleOnlineInterviewCandidate_testDetailsGridDiv.Style["display"] = "block";
                            ScheduleOnlineInterviewCandidate_candidateSessionHeader.Style["display"] = "block";

                            // Load the candidate session details grid.
                            LoadOnlineInterviewAndCandidateSessions();
                        }
                        else
                        {
                            if (Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] != null)
                            {
                                // if redirected from any child page, fill the search criteria
                                // and apply the search.
                                FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]
                                    as TestScheduleSearchCriteria);

                                // Reload the maximize hidden value.
                                CheckAndSetExpandorRestore();
                            }
                        }
                    }

                    // Load the search test session popup automatically, when 
                    // navigating from talentscout.
                    // Check if page is navigated from talentscout and position profile is found
                    if (Request.QueryString["parentpage"] != null &&
                        Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                        Request.QueryString["positionprofileid"] != null)
                    {
                        // Get position profile ID.
                        int positionProfileID = 0;
                        if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                        {
                            // Load the position profile popup.
                            ScriptManager.RegisterStartupScript
                                (this, this.GetType(), "LoadTestSessionWithPositionProfile",
                                "javascript: LoadTestSessionWithPositionProfile('"
                                + ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.ClientID + "','" + 
                                positionProfileID + "')", true);
                        }
                    }
                }

                // Clear the error and success messages on every postback.
                ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel.Text =
                    ScheduleOnlineInterviewCandidate_bottomSuccessMessageLabel.Text =
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel.Text =
                    ScheduleOnlineInterviewCandidate_topSuccessMessageLabel.Text = "";
                ScheduleOnlineInterviewCandidate_Popup_createCandidateButton.Attributes.Add("onclick",
                "return ShowCandidatePopup('" + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.ClientID + "','" + ScheduleOnlineInterviewCandidate_Popup_emailTextBox.ClientID + "','" + ScheduleOnlineInterviewCandidate_candidateIdHiddenField.ClientID + "')");
                
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// While unscheduling the candidate, it asks the user to confirm. Once he confirms to 
        /// unschedule, it triggers the below method to specify the button type 'Yes'/'No'
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ConfirmMessageEventArgs"/> that contains the event data.
        /// </param>
        /// <remarks>
        /// This parameter has a property ButtonType which specify the Actiontype
        /// </remarks>
        void ScheduleOnlineInterviewCandidate_inactiveConfirmMsgControl_ConfirmMessageThrown
            (object sender, ConfirmMessageEventArgs e)
        {
            try
            {
                if (e.ButtonType ==Forte.HCM.DataObjects.ButtonType.Yes)
                {
                    string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                    string attemptId = ViewState["ATTEMPT_ID"].ToString();

                    bool isMailSent = true;

                    // Update the session status as 'Not Scheduled'
                    new TestConductionBLManager().UpdateSessionStatus
                        (candidateSessionId, Convert.ToInt32(attemptId),
                        Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                        Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID, out isMailSent);

                    // TODO : Instead of loading the whole grid, update only that row.
                    //LoadValues();
                    LoadOnlineInterviewAndCandidateSessions();
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleOnlineInterviewCandidate_OkClick(object sender, EventArgs e)
        {
            try
            {
                string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());

                bool isMailSent = true;
                bool isUnscheduled = true;

                // Get candidate details for the given candidate test session id and attempt id.
                CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerBLManager().
                         GetCandidateTestSession(candidateSessionId, attemptId);

                // Update the session status as 'Not Scheduled'
                new TestConductionBLManager().UpdateSessionStatus(candidateTestSessionDetail,
                     Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                     Constants.CandidateSessionStatus.NOT_SCHEDULED,
                     base.userID, isUnscheduled, out isMailSent);

                // Update candidate activity log. Handled in the trigger[TRUPDATE_SESSION_CANDIDATE]
                //if (candidateTestSessionDetail.CandidateID != null && candidateTestSessionDetail.CandidateID.Trim().Length > 0)
                //{
                //    new CommonBLManager().  InsertCandidateActivityLog
                //        (0, Convert.ToInt32(candidateTestSessionDetail.CandidateID), "Unscheduled from test", base.userID, 
                //        Constants.CandidateActivityLogType.CANDIDATE_UNSCHEDULED);
                //}

                if (isMailSent == false)
                {
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // TODO : Instead of loading the whole grid, update only that row.
                LoadOnlineInterviewAndCandidateSessions();
                base.ShowMessage(ScheduleOnlineInterviewCandidate_topSuccessMessageLabel,
                         ScheduleOnlineInterviewCandidate_bottomSuccessMessageLabel,
                         Resources.HCMResource.ScheduleCandidate_UnScheduledStatus);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This handler gets triggered on clicking the reset button.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleOnlineInterviewCandidate_resetLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("ScheduleOnlineInterviewCandidate.aspx?m=2&s=1", false);
        }

        /// <summary>
        /// Show button will display the test session details and its corresponding 
        /// candidate session keys.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        public void ScheduleOnlineInterviewCandidate_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check the user input, if it is valid, get the test session details.
                if (!IsValidData())
                {
                    ScheduleOnlineInterviewCandidate_testDetailsDiv.Style["display"] = "none";
                    ScheduleOnlineInterviewCandidate_testDetailsGridDiv.Style["display"] = "none";
                    return;
                }

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CandidateSessionId";

                // Load the candidate session details grid.
                LoadOnlineInterviewAndCandidateSessions();
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);

                if (exp.Message.Contains("Online Interview Session Id not found"))
                {
                    ScheduleOnlineInterviewCandidate_testDetailsDiv.Style["display"] = "none";
                    ScheduleOnlineInterviewCandidate_testDetailsGridDiv.Style["display"] = "none";
                }
            }
        }
        /// <summary>
        /// Display the schedule/unschedule/reschedule/viewresults image buttons 
        /// according to the status field
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleOnlineInterviewCandidate_candidateSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                //Label ScheduleOnlineInterviewCandidate_statusLabel =
                //    (Label)e.Row.FindControl("ScheduleOnlineInterviewCandidate_statusLabel");
                //ImageButton ScheduleOnlineInterviewCandidate_scheduleImageButton =
                //    (ImageButton)e.Row.FindControl("ScheduleOnlineInterviewCandidate_scheduleImageButton");
                //ImageButton ScheduleOnlineInterviewCandidate_rescheduleImageButton =
                //    (ImageButton)e.Row.FindControl("ScheduleOnlineInterviewCandidate_rescheduleImageButton");
                //ImageButton ScheduleOnlineInterviewCandidate_unscheduleImageButton =
                //    (ImageButton)e.Row.FindControl("ScheduleOnlineInterviewCandidate_unscheduleImageButton");
                //ImageButton ScheduleOnlineInterviewCandidate_viewShedulerImageButton =
                //    (ImageButton)e.Row.FindControl("ScheduleOnlineInterviewCandidate_viewShedulerImageButton");
                //ImageButton ScheduleOnlineInterviewCandidate_viewResultImageButton =
                //    (ImageButton)e.Row.FindControl("ScheduleOnlineInterviewCandidate_viewResultImageButton");
                //ImageButton ScheduleOnlineInterviewCandidate_retakeRequestImageButton =
                //    (ImageButton)e.Row.FindControl("ScheduleOnlineInterviewCandidate_retakeRequestImageButton");
                //ImageButton ScheduleOnlineInterviewCandidate_viewCancelReasonImageButton =
                //    (ImageButton)e.Row.FindControl("ScheduleOnlineInterviewCandidate_cancelReasonDisplayImageButton");

                //// Initialize all the image buttons visibility as false.
                //ScheduleOnlineInterviewCandidate_viewResultImageButton.Visible = false;
                //ScheduleOnlineInterviewCandidate_viewShedulerImageButton.Visible = false;
                //ScheduleOnlineInterviewCandidate_scheduleImageButton.Visible = false;
                //ScheduleOnlineInterviewCandidate_rescheduleImageButton.Visible = false;
                //ScheduleOnlineInterviewCandidate_unscheduleImageButton.Visible = false;
                //ScheduleOnlineInterviewCandidate_retakeRequestImageButton.Visible = false;
                //ScheduleOnlineInterviewCandidate_viewCancelReasonImageButton.Visible = false;

                //// Check the status and display the image buttons accordingly.
                //if (ScheduleOnlineInterviewCandidate_statusLabel.Text == "Not Scheduled")
                //    ScheduleOnlineInterviewCandidate_scheduleImageButton.Visible = true;

                //else if (ScheduleOnlineInterviewCandidate_statusLabel.Text == "Scheduled")
                //{
                //    ScheduleOnlineInterviewCandidate_viewShedulerImageButton.Visible = true;
                //    ScheduleOnlineInterviewCandidate_rescheduleImageButton.Visible = true;
                //    ScheduleOnlineInterviewCandidate_unscheduleImageButton.Visible = true;
                //}

                //else if (ScheduleOnlineInterviewCandidate_statusLabel.Text == "Cancelled")
                //    ScheduleOnlineInterviewCandidate_viewCancelReasonImageButton.Visible = true;

                //else if (ScheduleOnlineInterviewCandidate_statusLabel.Text == "In Progress")
                //    ScheduleOnlineInterviewCandidate_viewShedulerImageButton.Visible = true;

                //if (ScheduleOnlineInterviewCandidate_statusLabel.Text == "Completed" ||
                //    ScheduleOnlineInterviewCandidate_statusLabel.Text == "Quit" ||
                //    ScheduleOnlineInterviewCandidate_statusLabel.Text == "Elapsed")
                //{
                //    ScheduleOnlineInterviewCandidate_viewResultImageButton.Visible = true;
                //    ScheduleOnlineInterviewCandidate_viewShedulerImageButton.Visible = true;

                //    // Check the retake request field and display the RetakeRequest image.
                //    HiddenField retakeRequestHiddenField =
                //        e.Row.FindControl("ScheduleOnlineInterviewCandidate_requestRetakeHiddenField") as HiddenField;

                //    string retakeRequest = retakeRequestHiddenField.Value;

                //    if (retakeRequest == "Y")
                //        ScheduleOnlineInterviewCandidate_retakeRequestImageButton.Visible = true;
                //}

                //e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                //e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Check whether the clicked button is schedule/reschedule/unschedule/viewschedule
        /// and display the Modal popup accordingly.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleOnlineInterviewCandidate_candidateSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;
                // Schedule Candidate
                if (e.CommandName == "schedule")
                {
                    Session["SESSION_SUBJECTIDS"] = null;
                    string onlineInterviewSessionKey = string.Empty;
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    onlineInterviewSessionKey = ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text.Trim();
                    string OpenAssessorWindow = string.Empty;
                    OpenAssessorWindow = "SearchInterviewOnlineRecommendAssessor('SESSIONKEY','" + onlineInterviewSessionKey + "',1,'" + ViewState["CANDIDATE_SESSIONID"] + "','" + ScheduleOnlineInterviewCandidate_showButton.ClientID + "')";
                    ScriptManager.RegisterStartupScript(ScheduleOnlineInterviewCandidate_candidateSessionGridViewUpdatePanel,
                        ScheduleOnlineInterviewCandidate_candidateSessionGridViewUpdatePanel.GetType(), "OpenAssessorWindow", OpenAssessorWindow, true);
                }
                // View Schedule ViewAssessor
                else if (e.CommandName == "viewschedule")
                {
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;

                    string OpenViewScheduleWindow = string.Empty;
                    OpenViewScheduleWindow = "ShowViewOnlineSchedule('" + ViewState["CANDIDATE_SESSIONID"] + "')";
                    ScriptManager.RegisterStartupScript(ScheduleOnlineInterviewCandidate_candidateSessionGridViewUpdatePanel,
                        ScheduleOnlineInterviewCandidate_candidateSessionGridViewUpdatePanel.GetType(),
                        "OpenViewScheduleWindow", OpenViewScheduleWindow, true);
                }
                // View Assessor / Add new time slots
                else if (e.CommandName == "ViewAssessor")
                {
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    string onlineInterviewSessionKey = string.Empty;
                    string OpenViewAssessorWindow = string.Empty;
                    Session["SESSION_SUBJECT_TABLE"] = null;
                    onlineInterviewSessionKey = ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text.Trim();
                    OpenViewAssessorWindow = "SearchInterviewOnlineRecommendAssessor('CANDIDATEKEY','" + onlineInterviewSessionKey + "',1,'" + ViewState["CANDIDATE_SESSIONID"] + "','" + ScheduleOnlineInterviewCandidate_showButton.ClientID + "')";
                    ScriptManager.RegisterStartupScript(ScheduleOnlineInterviewCandidate_candidateSessionGridViewUpdatePanel,
                        ScheduleOnlineInterviewCandidate_candidateSessionGridViewUpdatePanel.GetType(), "OpenViewAssessorWindow", OpenViewAssessorWindow, true);
                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on Save button in Schedule popup will trigger this method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleOnlineInterviewCandidate_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate the scheduled date and display the error message if any.
                ScheduleOnlineInterviewCandidate_MaskedEditValidator.Validate();

                if (!ScheduleOnlineInterviewCandidate_MaskedEditValidator.IsValid)
                {
                    ScheduleOnlineInterviewCandidate_scheduleErrorMsgLabel.Text =
                        ScheduleOnlineInterviewCandidate_MaskedEditValidator.ErrorMessage;
                    ScheduleOnlineInterviewCandidate_scheduleModalPopupExtender.Show();
                    return;
                }

                // Scheduled date should be between current date and test session's expiry date.
                TestSessionDetail testSession = Cache["SEARCH_DATATABLE"] as TestSessionDetail;

                if (testSession != null)
                {
                    if (DateTime.Parse(ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox.Text) > testSession.ExpiryDate
                        || DateTime.Parse(ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox.Text) < DateTime.Today)
                    {
                        ScheduleOnlineInterviewCandidate_scheduleErrorMsgLabel.Text =
                            Resources.HCMResource.ScheduleCandidate_ScheduledDateInvalid;
                        ScheduleOnlineInterviewCandidate_scheduleModalPopupExtender.Show();

                        // Since the candidate name and email textboxes are readonly, its values are not 
                        // maintained on postback. Use Request[controlName.UniqueID] to get the value.
                        ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.Text =
                            Request[ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.UniqueID].Trim();
                        ScheduleOnlineInterviewCandidate_Popup_emailTextBox.Text =
                            Request[ScheduleOnlineInterviewCandidate_Popup_emailTextBox.UniqueID].Trim();
                        return;
                    }
                }

                if (ViewState["CANDIDATE_SESSIONID"] == null || ViewState["ATTEMPT_ID"] == null)
                    return;

                TestScheduleDetail testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = ScheduleOnlineInterviewCandidate_candidateIdHiddenField.Value;
                testSchedule.CandidateTestSessionID = ViewState["CANDIDATE_SESSIONID"].ToString();
                testSchedule.AttemptID = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                testSchedule.EmailId = Request[ScheduleOnlineInterviewCandidate_Popup_emailTextBox.UniqueID].Trim();

                // Set the time to maximum in expiry date.
                testSchedule.ExpiryDate = (Convert.ToDateTime(
                    ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox.Text)).Add(new TimeSpan(23, 59, 59));

                bool isMailSent = true;
                if (!Utility.IsNullOrEmpty(ViewState["STATUS"]) &&
                    ViewState["STATUS"].ToString().ToUpper() == "RESCHEDULE")
                {
                    testSchedule.IsRescheduled = true;

                    if (!Utility.IsNullOrEmpty(ScheduleOnlineInterviewCandidate_reScheduleCandidateIDHiddenField.Value))
                        testSchedule.CandidateID = ScheduleOnlineInterviewCandidate_reScheduleCandidateIDHiddenField.Value;

                    new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID, base.tenantID, out isMailSent);

                    // Once the data is updated show the success message.
                    base.ShowMessage(ScheduleOnlineInterviewCandidate_topSuccessMessageLabel,
                            ScheduleOnlineInterviewCandidate_bottomSuccessMessageLabel,
                            Resources.HCMResource.ScheduleCandidate_ReScheduledSuccesss);

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleOnlineInterviewCandidate_topErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }
                else if (!Utility.IsNullOrEmpty(ViewState["STATUS"]) &&
                    ViewState["STATUS"].ToString().ToUpper() == "SCHEDULE")
                {
                    if (testSession == null || testSchedule == null)
                    {
                        base.ShowMessage(ScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                            ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel, 
                            "Your current session seems to be expired. Reload the page and try again.");
                        return;
                    }
                    //check whether the same candidate is assigned for test session id
                    string isCandidateAlreadyAssignedSchedulerName = new TestSchedulerBLManager().CheckCandidateAlreadyAssigned(testSession.TestSessionID, testSchedule.CandidateID);

                    if (isCandidateAlreadyAssignedSchedulerName == null ||
                        isCandidateAlreadyAssignedSchedulerName.Trim().Length == 0)
                    {
                        if (new AdminBLManager().IsFeatureUsageLimitExceeds(this.tenantID, Constants.FeatureConstants.ONLINE_TEST_ADMINISTERED))
                        {
                            ShowMessage(ScheduleOnlineInterviewCandidate_scheduleErrorMsgLabel, "You have reached the maximum limit based your subscription plan. Cannot schedule more candidates");
                            ScheduleOnlineInterviewCandidate_scheduleModalPopupExtender.Show();
                            return;
                        }

                        new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID, base.tenantID, out isMailSent);
                        base.ShowMessage(ScheduleOnlineInterviewCandidate_topSuccessMessageLabel,
                            ScheduleOnlineInterviewCandidate_bottomSuccessMessageLabel,
                            Resources.HCMResource.ScheduleCandidate_ScheduledSuccess);
                    }
                    else
                    {
                        base.ShowMessage(ScheduleOnlineInterviewCandidate_scheduleErrorMsgLabel,
                            "Already Candidate " + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.Text +
                            " has been scheduled in this online interview session");
                        ScheduleOnlineInterviewCandidate_scheduleModalPopupExtender.Show();
                        return;
                    }

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleOnlineInterviewCandidate_topErrorMessageLabel.Text =
                               Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }
                else // Retake a test
                {
                    new TestSchedulerBLManager().RetakeTest(testSchedule, base.userID, out isMailSent,base.tenantID);
                    base.ShowMessage(ScheduleOnlineInterviewCandidate_topSuccessMessageLabel,
                        ScheduleOnlineInterviewCandidate_bottomSuccessMessageLabel,
                        Resources.HCMResource.ScheduleCandidate_ReTakeSuccesss);

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleOnlineInterviewCandidate_topErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }

                // TODO : Instead of loading the whole grid, update only that row.
                LoadOnlineInterviewAndCandidateSessions();
                ViewState["STATUS"] = null;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Methods                                              

        /// <summary>
        /// Return the proper status message according to the DB session values.
        /// </summary>
        /// <param name="pStatus">
        /// A <see cref="string"/> that contains the test status
        /// </param>
        /// <returns>
        /// It will return the test status.
        /// </returns>
        protected string GetStatus(string pStatus)
        {
            string status = "";
            switch (pStatus.Trim())
            {
                case Constants.CandidateSessionStatus.ELAPSED:
                    status = "Elapsed";
                    break;
                case Constants.CandidateSessionStatus.IN_PROGRESS:
                    status = "In Progress";
                    break;
                case Constants.CandidateSessionStatus.NOT_SCHEDULED:
                    status = "Not Scheduled";
                    break;
                case Constants.CandidateSessionStatus.QUIT:
                    status = "Quit";
                    break;
                case Constants.CandidateSessionStatus.SCHEDULED:
                    status = "Scheduled";
                    break;
                case Constants.CandidateSessionStatus.CANCELLED:
                    status = "Cancelled";
                    break;
                case Constants.CandidateSessionStatus.COMPLETED:
                    status = "Completed";
                    break;
                default:
                    status = "Not Scheduled";
                    break;
            }
            return status;
        }

        /// <summary>
        /// This method returns true when CyberProctoring is enabled as well as 
        /// the candidate session is completed.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status of the candidate.
        /// </param>
        /// <returns></returns>
        protected bool IsCyberProctoring(string status)
        {
            TestSessionDetail testSession = Cache["SEARCH_DATATABLE"] as TestSessionDetail;
            if (testSession != null)
                return (status.Trim() == Constants.CandidateSessionStatus.COMPLETED
                    && testSession.IsCyberProctoringEnabled) ? true : false;
            else
                return false;
        }

        /// <summary>
        /// This method returns true when a candidate is scheduled 
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status of the candidate scheduled or not.
        /// </param>
        /// <returns></returns>
        protected bool IsScheduled(string status)
        {
            if (!Utility.IsNullOrEmpty(status) && status.Trim().ToUpper() == "SESS_SCHD")
                return true;
            else
                return false;
        }

        /// <summary>
        /// This method clears the textboxes in Schedule/Reschedule popup.
        /// </summary>
        protected void ClearSchedulePopup()
        {
            ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.Text = "";
            ScheduleOnlineInterviewCandidate_Popup_emailTextBox.Text = "";
            ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox.Text = "";
            ScheduleOnlineInterviewCandidate_scheduleErrorMsgLabel.Text = "";
        }

        /// <summary>
        /// This method clears the test session and candidate session details
        /// </summary>
        protected void ClearSessionValues()
        {
            try
            {
                ScheduleOnlineInterviewCandidate_testDetailsDiv.Style["display"] = "none";
                ScheduleOnlineInterviewCandidate_testDetailsGridDiv.Style["display"] = "none";
                ScheduleOnlineInterviewCandidate_candidateSessionHeader.Style["display"] = "none";

                // Store the scheduled date in view state
                ScheduleOnlineInterviewCandidate_sessionNameLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_positionProfileLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_emailLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_sessionAuthorLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_noofsessionsLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_expiryDateLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_positionProfileLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_instructionsLiteral.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_descriptionLabel.Text = string.Empty;
                ScheduleOnlineInterviewCandidate_instructionsLabel.Text = string.Empty;

                Cache["SEARCH_DATATABLE"] = "";
                ViewState["TEST_SESSION_ID"] = "";
                ViewState["CANDIDATE_SESSION_IDS"] = "";
                ScheduleOnlineInterviewCandidate_candidateSessionGridView.DataSource = null;
                ScheduleOnlineInterviewCandidate_candidateSessionGridView.DataBind();
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Protected Methods

        #region Sorting Related Methods                                        

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void ScheduleOnlineInterviewCandidate_candidateSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                LoadOnlineInterviewAndCandidateSessions();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ScheduleOnlineInterviewCandidate_candidateSessionGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                            (ScheduleOnlineInterviewCandidate_candidateSessionGridView,
                            (string)ViewState["SORT_FIELD"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            (SortType)ViewState["SORT_ORDER"]);
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Sorting Related Methods

        #region Private Methods                                                

        /// <summary>
        /// 
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(ScheduleOnlineInterviewCandidate_isMaximizedHiddenField.Value) &&
                ScheduleOnlineInterviewCandidate_isMaximizedHiddenField.Value == "Y")
            {
                ScheduleOnlineInterviewCandidate_testSessionDetailsDiv.Style["display"] = "none";
                ScheduleOnlineInterviewCandidate_searchResultsUpSpan.Style["display"] = "block";
                ScheduleOnlineInterviewCandidate_searchResultsDownSpan.Style["display"] = "none";
                ScheduleOnlineInterviewCandidate_candidateSessionGridViewDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                ScheduleOnlineInterviewCandidate_testSessionDetailsDiv.Style["display"] = "block";
                ScheduleOnlineInterviewCandidate_searchResultsUpSpan.Style["display"] = "none";
                ScheduleOnlineInterviewCandidate_searchResultsDownSpan.Style["display"] = "block";
                ScheduleOnlineInterviewCandidate_candidateSessionGridViewDIV.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ScheduleOnlineInterviewCandidate_isMaximizedHiddenField.Value))
                    ((TestScheduleSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]).IsMaximized =
                        ScheduleOnlineInterviewCandidate_isMaximizedHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadOnlineInterviewAndCandidateSessions()
        {
            try
            {
                TestScheduleSearchCriteria scheduleCandidateSearchCriteria = new TestScheduleSearchCriteria();

                // Check if the search criteria fields are empty.
                // If this is empty, pass null. Or else, pass its value.
                scheduleCandidateSearchCriteria.TestSessionID =
                    ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.CandidateSessionIDs =
                    ScheduleOnlineInterviewCandidate_candidateSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : ScheduleOnlineInterviewCandidate_candidateSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.IsMaximized =
                    ScheduleOnlineInterviewCandidate_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                scheduleCandidateSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                scheduleCandidateSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

                // Keep the search criteria in Session if the page is launched from 
                // somewhere else.
                Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] = scheduleCandidateSearchCriteria;

                OnlineInterviewSessionDetail onlineInterviewSession = new OnlineInterviewBLManager().
                    GetOnlineInterviewSessionDetail(scheduleCandidateSearchCriteria, 
                        scheduleCandidateSearchCriteria.SortExpression, 
                        scheduleCandidateSearchCriteria.SortDirection);

                if (onlineInterviewSession != null)
                {
                    UserDetail userDetail = new UserDetail();
                    userDetail = (UserDetail)Session["USER_DETAIL"];
                    
                    ScheduleOnlineInterviewCandidate_testDetailsDiv.Style["display"] = "block";
                    ScheduleOnlineInterviewCandidate_testDetailsGridDiv.Style["display"] = "block";
                    ScheduleOnlineInterviewCandidate_candidateSessionHeader.Style["display"] = "block";

                    // Store the scheduled date in view state
                    ScheduleOnlineInterviewCandidate_sessionNameLabel.Text = onlineInterviewSession.OnlineInterviewSessionName;
                    ScheduleOnlineInterviewCandidate_emailLabel.Text = onlineInterviewSession.SessionAuthorEmail;
                    ScheduleOnlineInterviewCandidate_sessionAuthorLabel.Text = onlineInterviewSession.SessionAuthor;
                    ScheduleOnlineInterviewCandidate_testDescLiteral.Text = onlineInterviewSession.OnlineInterviewSessionDescription;
                    ScheduleOnlineInterviewCandidate_expiryDateLabel.Text = GetDateFormat(onlineInterviewSession.ExpiryDate);
                    ScheduleOnlineInterviewCandidate_positionProfileLabel.Text = onlineInterviewSession.PositionProfileName;
                    ScheduleOnlineInterviewCandidate_instructionsLiteral.Text = onlineInterviewSession.OnlineInterviewInstruction;
                    ScheduleOnlineInterviewCandidate_skillsLabel.Text = onlineInterviewSession.Skills;
                    ScheduleOnlineInterviewCandidate_noofsessionsLabel.Text = onlineInterviewSession.NumberOfCandidateSessions.ToString();
                    Cache["SEARCH_DATATABLE"] = onlineInterviewSession;
                    ViewState["TEST_SESSION_ID"] = ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text.Trim();
                    ViewState["CANDIDATE_SESSION_IDS"] = ScheduleOnlineInterviewCandidate_candidateSessionIdTextBox.Text.Trim();
                    ScheduleOnlineInterviewCandidate_candidateSessionGridView.DataSource = onlineInterviewSession.CandidateOnlineInterviewSessions;
                    ScheduleOnlineInterviewCandidate_candidateSessionGridView.DataBind();

                    // Remove 'onclick' handler for 'load position profile candidate' icon.
                    if (ScheduleOnlineInterviewCandidate_positionProfileCandidateImageButton.Attributes["onclick"] != null)
                        ScheduleOnlineInterviewCandidate_positionProfileCandidateImageButton.Attributes.Remove("onclick");

                    // Assign handler for 'load position profile candidate' icon.
                    ScheduleOnlineInterviewCandidate_positionProfileCandidateImageButton.Attributes.Add("onclick", "return LoadPositionProfileCandidates('"
                       + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.ClientID + "','"
                       + ScheduleOnlineInterviewCandidate_Popup_emailTextBox.ClientID + "','"
                       + ScheduleOnlineInterviewCandidate_candidateIdHiddenField.ClientID + "','"
                       + onlineInterviewSession.PositionProfileID + "')");
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                    ScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
                ClearSessionValues();
            }
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        private void FillSearchCriteria(TestScheduleSearchCriteria scheduleSearchCriteria)
        {
            if (scheduleSearchCriteria.TestSessionID != null)
                ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text = scheduleSearchCriteria.TestSessionID;

            if (scheduleSearchCriteria.EMail != null)
                ScheduleOnlineInterviewCandidate_candidateSessionIdTextBox.Text = scheduleSearchCriteria.CandidateSessionIDs;

            ScheduleOnlineInterviewCandidate_isMaximizedHiddenField.Value =
                    scheduleSearchCriteria.IsMaximized == true ? "Y" : "N";

            ViewState["SORT_ORDER"] = scheduleSearchCriteria.SortDirection;
            ViewState["SORT_FIELD"] = scheduleSearchCriteria.SortExpression;

            // Apply search
            LoadOnlineInterviewAndCandidateSessions();
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// This method checks the user input. If both testSession and CandidateSession IDs 
        /// are not given, then return false.
        /// </summary>
        /// <returns>
        /// </returns>
        protected override bool IsValidData()
        {
            if (ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text != "" ||
                ScheduleOnlineInterviewCandidate_candidateSessionIdTextBox.Text != "")
            {
                // This method passes the list of candidateSession ids separated by comma and
                // it returns the candidate session keys which are not existing in test session.
                List<string> notMatchedCandSessionKeys =
                    new OnlineInterviewBLManager().ValidateOnlineInterviewSessionInput
                    (ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Text.Trim(),
                    ScheduleOnlineInterviewCandidate_candidateSessionIdTextBox.Text.Trim());

                if (notMatchedCandSessionKeys != null && notMatchedCandSessionKeys.Count > 0)
                {
                    base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                        ScheduleOnlineInterviewCandidate_topErrorMessageLabel, "No matching records found");
                    return false;
                }
                else
                    return true;
            }
            base.ShowMessage(ScheduleOnlineInterviewCandidate_bottomErrorMessageLabel,
                ScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                "Please enter Online Interview Session ID or Candidate Session ID(s)");
            return false;
        }

        /// <summary>
        /// This method loads the test session and scheduled candidate details.
        /// </summary>
        protected override void LoadValues()
        {
            // Assign the client side onclick events to header
            ScheduleOnlineInterviewCandidate_searchTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                ScheduleOnlineInterviewCandidate_candidateSessionGridViewDIV.ClientID + "','" +
                ScheduleOnlineInterviewCandidate_testSessionDetailsDiv.ClientID + "','" +
                ScheduleOnlineInterviewCandidate_searchResultsUpSpan.ClientID + "','" +
                ScheduleOnlineInterviewCandidate_searchResultsDownSpan.ClientID + "','" +
                ScheduleOnlineInterviewCandidate_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            ScheduleOnlineInterviewCandidate_candidateNameImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.ClientID + "','"
                + ScheduleOnlineInterviewCandidate_Popup_emailTextBox.ClientID + "','"
                + ScheduleOnlineInterviewCandidate_candidateIdHiddenField.ClientID
                + "')");

           
            // Check if page is navigated from talentscout and position profile is found
            if (Request.QueryString["parentpage"] != null &&
                Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                Request.QueryString["positionprofileid"] != null)
            {
                // Get position profile ID.
                int positionProfileID = 0;
                if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                {
                    ScheduleOnlineInterviewCandidate_onlineSessionImageButton.Attributes.Add("onclick", "return LoadTestSessionWithPositionProfile('"
                    + ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.ClientID + "','" + 
                    positionProfileID + "')");
                }
                else
                {
                    ScheduleOnlineInterviewCandidate_onlineSessionImageButton.Attributes.Add("onclick", "return LoadOnlineInterviewSession('"
                    + ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.ClientID + "')");
                }
            }
            else
            {
                ScheduleOnlineInterviewCandidate_onlineSessionImageButton.Attributes.Add("onclick", "return LoadOnlineInterviewSession('"
                    + ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.ClientID + "')");
            }

            ScheduleOnlineInterviewCandidate_saveButton.Attributes.Add("onclick", "return VaildateSchedulePopup('"
                + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.ClientID + "','"
                + ScheduleOnlineInterviewCandidate_scheduleDatePopupTextBox.ClientID
                + "','" + ScheduleOnlineInterviewCandidate_scheduleErrorMsgLabel.ClientID + "')");

            ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox.Attributes.Add("onkeydown", "TestSessionKeyDown(event,'"
                + ScheduleOnlineInterviewCandidate_showButton.ClientID + "')");

            // Assign default sort field and order keys.
            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;

            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "CandidateSessionID";
        }
        #endregion Protected Overridden Methods
    }
}