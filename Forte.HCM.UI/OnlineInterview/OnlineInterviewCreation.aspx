<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineInterviewCreation.aspx.cs"
    MasterPageFile="~/MasterPages/InterviewMaster.Master" Inherits="Forte.HCM.UI.OnlineInterview.OnlineInterviewCreation" %>

<%@ Register Src="~/CommonControls/CandidateDetailControl.ascx" TagName="CandidateDetail"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/OnlineInterviewMenuControl.ascx" TagName="OnlineInterviewMenuControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewSaveAsControl.ascx" TagName="InterviewSaveAs"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="OnlineInterviewCreation_bodyContent" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <script type="text/javascript">
        function ClearPositionProfile() {
            document.getElementById("<%= OnlineInterviewCreation_positionProfileTextBox.ClientID%>").value = '';
            document.getElementById("<%= OnlineInterviewCreation_positionProfileIDHiddenField.ClientID%>").value = '';
            return false;
        }

        function ShowLoadingImage() {
            document.getElementById("<%=OnlineInterviewCreation_skillTextBox.ClientID%>").className = "position_profile_client_bg";
        }
        function HideLoadingImage() {
            document.getElementById("<%=OnlineInterviewCreation_skillTextBox.ClientID%>").className = "";
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding-bottom: 0px">
                <uc3:OnlineInterviewMenuControl ID="OnlineInterviewCreation_menuControl" runat="server"
                    Visible="true" PageType="IC" />
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="OnlineInterviewCreation_topSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="OnlineInterviewCreation_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewCreation_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="non_tab_body_bg">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="OnlineInterviewCreation_sessionDetailsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="OnlineInterviewCreation_sessionDetailsDIV" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 120px">
                                                                                        <asp:Label ID="OnlineInterviewCreation_interviewNameLabel" runat="server" Text="Inteview Name"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        <span class="mandatory">&nbsp;*</span>
                                                                                    </td>
                                                                                    <td style="width: 550px">
                                                                                        <asp:TextBox ID="OnlineInterviewCreation_interviewNameTextBox" runat="server" MaxLength="100"
                                                                                            AutoCompleteType="None" Columns="70" TabIndex="1"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 120px">
                                                                                        <asp:Label ID="OnlineInterviewCreation_interviewIDLabel" runat="server" Text="Inteview ID"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="OnlineInterviewCreation_interviewIDLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 120px">
                                                                                        <asp:Label ID="CreateOnlineInterviewSession_positionProfileLabel" runat="server"
                                                                                            SkinID="sknLabelFieldHeaderText" Text="Position Profile"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 550px">
                                                                                        <div style="float: left;">
                                                                                            <asp:TextBox ID="OnlineInterviewCreation_positionProfileTextBox" MaxLength="50" ReadOnly="true"
                                                                                                Columns="70" runat="server" TabIndex="2">
                                                                                            </asp:TextBox>
                                                                                        </div>
                                                                                        <div style="padding-left: 0px; float: left; height: 16px;">
                                                                                            <asp:ImageButton ID="OnlineInterviewCreation_positionProfileImageButton" SkinID="sknSearchIconImageButton"
                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile to associate with online interview"
                                                                                                TabIndex="3" />
                                                                                            <asp:ImageButton ID="OnlineInterviewCreation_positionProfileHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Click here to select position profile to associate with online interview"
                                                                                                TabIndex="4" />
                                                                                        </div>
                                                                                        <asp:HiddenField ID="OnlineInterviewCreation_positionProfileIDHiddenField" runat="server" />
                                                                                    </td>
                                                                                    <td style="width: 120px">
                                                                                        <asp:Label ID="OnlineInterviewCreation_positionProfileIDLabel" runat="server" Text="Position Profile ID"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="OnlineInterviewCreation_positionProfileIDLabelValue" SkinID="sknLabelFieldText"
                                                                                            runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="OnlineInterviewCreation_positionProfileClientRequestIDHiddenField"
                                                                                            runat="server"></asp:HiddenField>
                                                                                        <asp:HiddenField ID="OnlineInterviewCreation_positionProfilekeyWordHiddenField" runat="server">
                                                                                        </asp:HiddenField>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_20">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="interview_skill_header_bg">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td style="width: 114px">
                                                                                                                <asp:Label runat="server" SkinID="sknLabelFieldHeaderText" Text="Skill/Areas" ID="OnlineInterviewCreation_skillLabel"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:TextBox ID="OnlineInterviewCreation_skillTextBox" runat="server" Width="350px"
                                                                                                                        Columns="65" Style="font-size: 90%; font-family: Tahoma, Arial, sans-serif; color: gray;
                                                                                                                        font-style: normal; text-align: left;" AutoPostBack="true" OnTextChanged="OnlineInterviewCreation_skillTextBox_TextChanged">
                                                                                                                    </asp:TextBox>
                                                                                                                </div>
                                                                                                                <div style="float: left; padding: 2px; 0px; 0px; 2px">
                                                                                                                    <asp:Image ID="OnlineInterviewCreation_skillAddImage" SkinID="sknSkillAdImage" runat="server" />
                                                                                                                </div>
                                                                                                                <ajaxToolKit:TextBoxWatermarkExtender ID="OnlineInterviewCreation_newAssessorTextBoxWatermarkExtender"
                                                                                                                    runat="server" TargetControlID="OnlineInterviewCreation_skillTextBox" WatermarkText="Enter/Select skill name"
                                                                                                                    WatermarkCssClass="assessor_skill_water_mark">
                                                                                                                </ajaxToolKit:TextBoxWatermarkExtender>
                                                                                                                <ajaxToolKit:AutoCompleteExtender ID="OnlineInterviewCreation_skillAutoCompleteExtender"
                                                                                                                    runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetSkillList"
                                                                                                                    TargetControlID="OnlineInterviewCreation_skillTextBox" MinimumPrefixLength="1"
                                                                                                                    CompletionListElementID="pnl" CompletionListCssClass="interview_client_completion_list"
                                                                                                                    CompletionListItemCssClass="interview_client_completion_list_item" OnClientPopulating="ShowLoadingImage"
                                                                                                                    OnClientPopulated="HideLoadingImage" EnableCaching="true" CompletionSetCount="12"
                                                                                                                    OnClientShowing="ShowLoadingImage" OnClientHiding="HideLoadingImage" CompletionListHighlightedItemCssClass="interview_client_completion_list_highlight">
                                                                                                                </ajaxToolKit:AutoCompleteExtender>
                                                                                                                <asp:Panel ID="pnl" runat="server">
                                                                                                                    &nbsp;</asp:Panel>
                                                                                                            </td>
                                                                                                            <td style="width: 50px">
                                                                                                                <div style="display: none">
                                                                                                                    <asp:Button ID="OnlineInterviewCreation_refreshSearchButton" runat="server" SkinID="sknAddVectorGroupImageButton"
                                                                                                                        Text="Refresh" OnClick="OnlineInterviewCreation_refreshSearchButton_Click" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td align="right" style="text-align:right">
                                                                                                                <asp:UpdatePanel ID="OnlineInterviewCreation_autoCalculateUpdatePanel" runat="server">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:LinkButton ID="OnlineInterviewCreation_autoCalculateLinkButton" runat="server"
                                                                                                                            Text="Auto Calculate" OnClick="OnlineInterviewCreation_autoCalculateLinkButton_Click"
                                                                                                                            SkinID="sknActionLinkButton" ToolTip="Click here to auto populate"></asp:LinkButton>
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                            <td align="right" class="interview-search-skill">
                                                                                                                <asp:ImageButton ID="OnlineInterviewCreation_searchSkillImageButtonButton" 
                                                                                                                    SkinID="sknSearchSkillImageButton" ToolTip="Click here to search & add skill" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="interview_skill_body_bg">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="td_height_20">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:UpdatePanel ID="CreateAssessor_skillsUpdatePanel" runat="server">
                                                                                                                    <ContentTemplate>
                                                                                                                        <div style="height: 200px; overflow: auto;">
                                                                                                                            <asp:GridView ID="OnlineInterviewCreation_skillsGridView" runat="server" AutoGenerateColumns="False"
                                                                                                                                AllowSorting="False" OnRowCommand="OnlineInterviewCreation_skillsGridView_RowCommand"
                                                                                                                                OnRowDataBound="OnlineInterviewCreation_skillsGridView_RowDataBound" SkinID="sknOnlieInterviewGrid">
                                                                                                                                <Columns>
                                                                                                                                    <asp:TemplateField HeaderStyle-Width="10%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:ImageButton ID="OnlineInterviewCreation_skillsGridView_deleteSkillImageButton"
                                                                                                                                                runat="server" SkinID="sknPPDeleteImageButton" CommandArgument='<%# Eval("SkillID") %>'
                                                                                                                                                ToolTip="Delete Skill" CssClass="showCursor" CommandName="DeleteSkill" />&nbsp;
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                    <asp:TemplateField HeaderText="Category" HeaderStyle-Width="25%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:Label ID="OnlineInterviewCreation_skillsGridView_categoryNameLabel" runat="server"
                                                                                                                                                Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                    <asp:TemplateField HeaderText="Skill" HeaderStyle-Width="25%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:Label ID="OnlineInterviewCreation_skillsGridView_skillNameLabel" runat="server"
                                                                                                                                                Text='<%# Eval("SkillName") %>'></asp:Label>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                    <asp:TemplateField HeaderText="Weightage" HeaderStyle-Width="15%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:TextBox ID="OnlineInterviewCreation_skillsGridView_weightageTextBox" runat="server"
                                                                                                                                                MaxLength="3" Width="25%"></asp:TextBox>
                                                                                                                                            <ajaxToolKit:FilteredTextBoxExtender ID="OnlineInterviewCreation_weightageTextBoxExtender"
                                                                                                                                                runat="server" FilterType="Custom,Numbers" TargetControlID="OnlineInterviewCreation_skillsGridView_weightageTextBox"
                                                                                                                                                ValidChars=".">
                                                                                                                                            </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                    <asp:TemplateField HeaderText="Skill Level" HeaderStyle-Width="25%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:DropDownList ID="OnlineInterviewCreation_skillsGridView_skillLevelDropDownList"
                                                                                                                                                runat="server">
                                                                                                                                            </asp:DropDownList>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                    <asp:TemplateField>
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:HiddenField ID="OnlineInterviewCreation_skillsGridView_skillIDHiddenField" runat="server"
                                                                                                                                                Value='<%# DataBinder.Eval(Container.DataItem, "SkillID")%>' />
                                                                                                                                            <asp:HiddenField ID="OnlineInterviewCreation_skillsGridView_weightageHiddenField"
                                                                                                                                                runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Weightage")%>' />
                                                                                                                                            <asp:HiddenField ID="OnlineInterviewCreation_skillsGridView_skillLeveHiddenField"
                                                                                                                                                runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SkillLevel")%>' />
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                </Columns>
                                                                                                                            </asp:GridView>
                                                                                                                        </div>
                                                                                                                        <asp:HiddenField ID="OnlineInterviewCreation_categoryIDHiddenField" runat="server" />
                                                                                                                        <asp:HiddenField ID="OnlineInterviewCreation_skillIDHiddenField" runat="server" />
                                                                                                                        <asp:HiddenField ID="OnlineInterviewCreation_skillHiddenField" runat="server" />
                                                                                                                    </ContentTemplate>
                                                                                                                    <Triggers>
                                                                                                                        <asp:AsyncPostBackTrigger ControlID="OnlineInterviewCreation_autoCalculateLinkButton" />
                                                                                                                    </Triggers>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="interview_instruction_panel_bg">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" align="center" width="95%">
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 50%;">
                                                                            <asp:Label ID="OnlineInterviewCreation_sessionDescHeadLabel" runat="server" Text="Interview Instruction"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 50%;">
                                                                            <asp:Label ID="OnlineInterviewCreation_instructionsHeadLabel" runat="server" Text="Remarks"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 50%;">
                                                                            <asp:TextBox ID="OnlineInterviewCreation_instructionTextBox" runat="server" TextMode="MultiLine"
                                                                                SkinID="sknMultiLineTextBox" MaxLength="8000" Rows="5" onkeyup="CommentsCount(8000,this)"
                                                                                onchange="CommentsCount(8000,this)" Width="420px" TabIndex="8"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 50%;">
                                                                            <asp:TextBox ID="OnlineInterviewCreation_remarkTextBox" runat="server" TextMode="MultiLine"
                                                                                MaxLength="8000" Rows="5" Width="450px" onkeyup="CommentsCount(8000,this)" onchange="CommentsCount(8000,this)"
                                                                                TabIndex="9"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <!-- Question Set -->
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="OnlineInterviewCreation_questionSetLabel" runat="server" Text="Question Set"
                                                                    CssClass="interview_menu_title"></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <asp:LinkButton ID="OnlineInterviewCreation_questionSetLinkButton" runat="server"
                                                                    Text="Create/Change Question Set" ToolTip="Create/Change Question Set" CssClass="interview_edi_edit_link"
                                                                    OnClick="OnlineInterviewCreation_questionSetLinkButton_Click" Enabled="false">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Image ID="OnlineInterviewCreation_questionSetLineBreakImage" SkinID="sknPPLineImage"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="interview_details_panel_bg" style="height: 25px; padding-left: 10px">
                                                                <asp:DataList ID="OnlineInterviewCreation_skillDataList" runat="server" RepeatColumns="4"
                                                                    Width="100%" RepeatDirection="Horizontal">
                                                                    <ItemTemplate>
                                                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                                                            <tr>
                                                                                <td style="width: 100px; text-align: left; padding-top: 10px;">
                                                                                    <asp:Label ID="OnlineInterviewCreation_skillNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text='<%# Eval("SkillName") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="OnlineInterviewCreation_skillCountLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text='<%# String.Format("({0})", Eval("SkillQuestionCount"))  %>' ToolTip="Total Questions">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                                <asp:Image SkinID="sknNoRecordImage" runat="server" ID="OnlineInterviewCreation_noQuestionSetImage"
                                                                    Visible="true" />&nbsp;
                                                                <asp:Label ID="OnlineInterviewCreation_noQuestionSetLabel" runat="server" Text="Question(s) set not found"
                                                                    CssClass="interview_no_record" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- Assessor Set -->
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="OnlineInterviewCreation_assessorHeaderLabel" runat="server" Text="Assessors"
                                                                    CssClass="interview_menu_title"></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <asp:LinkButton ID="OnlineInterviewCreation_changeAssessorLinkButton" runat="server"
                                                                    Text="Change/Assign Assessor" CssClass="interview_edi_edit_link" OnClick="OnlineInterviewCreation_changeAssessorLinkButton_Click"
                                                                    ToolTip="Change/Assign Assessor" Enabled="false">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Image ID="OnlineInterviewCreation_assessorSetLineBreakImage" SkinID="sknPPLineImage"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="interview_details_panel_bg" style="height: 25px; padding-left: 10px">
                                                                <asp:DataList ID="OnlineInterviewCreation_assessorListDataList" runat="server" RepeatColumns="4"
                                                                    Width="100%" RepeatDirection="Vertical" OnItemDataBound="OnlineInterviewCreation_assessorListDataList_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <table cellpadding="1" cellspacing="1" border="0" align="left" width="100%">
                                                                            <tr>
                                                                                <td style="width: 100px; text-align: left; padding-top: 10px;">
                                                                                    <asp:LinkButton ID="OnlineInterviewCreation_assessorListDataList_nameLinkButton"
                                                                                        Text='<%# Eval("Assessor") %>' SkinID="sknActionLinkButton" runat="server"></asp:LinkButton>
                                                                                    <asp:HiddenField ID="OnlineInterviewCreation_assessorListDataList_assessorIDHiddenField"
                                                                                        runat="server" Value='<%# Eval("AssessorID")  %>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                                <asp:Image SkinID="sknNoRecordImage" runat="server" ID="OnlineInterviewCreation_noAssessorImage"
                                                                    Visible="true" />&nbsp;
                                                                <asp:Label ID="OnlineInterviewCreation_noAssessorLabel" runat="server" Text="Assessor(s) not found"
                                                                    CssClass="interview_no_record" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- Candidates -->
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="OnlineInterviewCreation_candidateHeaderLabel" runat="server" Text="Candidates"
                                                                    CssClass="interview_menu_title"></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <asp:LinkButton ID="OnlineInterviewCreation_scheduleCandidatesLinkButton" runat="server"
                                                                    Text="Schedule Candidate" CssClass="interview_edi_edit_link" OnClick="OnlineInterviewCreation_scheduleCandidatesLinkButton_Click"
                                                                    ToolTip="Schedule Candidate" Enabled="false">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Image ID="OnlineInterviewCreation_scheduleCandidateLineBreakImage" SkinID="sknPPLineImage"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="interview_details_panel_bg" style="height: 25px; padding-left: 10px">
                                                                <asp:DataList ID="OnlineInterviewCreation_candidateAssociatedDataList" runat="server"
                                                                    RepeatColumns="4" Width="100%" RepeatDirection="Vertical" 
                                                                    OnItemDataBound="OnlineInterviewCreation_candidateAssociatedDataList_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <table cellpadding="1" cellspacing="1" border="0" align="left" width="100%">
                                                                            <tr>
                                                                                <td style="width: 100px; text-align: left; padding-top: 10px;">
                                                                                    <asp:HyperLink ID="OnlineInterviewCreation_candidateAssociatedDataList_candidateHyperLink" runat="server" 
                                                                                    Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'
                                                                                    ToolTip='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>' Target="_blank" 
                                                                                    SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                                                    <asp:HiddenField ID="OnlineInterviewCreation_candidateAssociatedDataList_candidateIDHiddenField" 
                                                                                        runat="server" Value='<%# Eval("CandidateInfoID")  %>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                                <asp:Image SkinID="sknNoRecordImage" runat="server" ID="OnlineInterviewCreation_noCandidateImage" />&nbsp;
                                                                <asp:Label ID="OnlineInterviewCreation_noCandidateLabel" runat="server" Text="Candidate(s) not found"
                                                                    CssClass="interview_no_record" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="OnlineInterviewCreation_candidatesLabel" runat="server" Text="" CssClass="interview_menu_title"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left" style="width:120px">
                                                                <asp:HiddenField ID="OnlineInterviewCreation_interviewKeyHiddenField" runat="server" />
                                                                <asp:HiddenField ID="OnlineInterviewCreation_deleteSkillIDHiddenField" runat="server" />
                                                                <asp:Button ID="OnlineInterviewCreation_topSaveButton" runat="server" OnClick="OnlineInterviewCreation_saveButton_Click"
                                                                    SkinID="sknButtonId" TabIndex="13" Height="26px" Text="Save and Continue" />
                                                            </td>
                                                            <td>
                                                                 <asp:LinkButton ID="OnlineInterviewCreation_cancelLinkButton" runat="server"
                                                                    Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"/>
                                                            </td>
                                                            <td align="right">
                                                                <asp:ImageButton ID="OnlineInterviewCreation_saveAsImageButton" runat="server" OnClick="OnlineInterviewCreation_saveAsImageButton_Click"
                                                                    SkinID="sknInterviewSaveAsImageButton" Visible="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="OnlineInterviewCreation_displayInterviewSavedUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="OnlineInterviewCreation_displayInterviewSavedHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="OnlineInterviewCreation_displayInterviewSavedPopupPanel" runat="server"
                            Style="display: none" CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="OnlineInterviewCreation_displayInterviewSavedConfirmMsgControl"
                                runat="server" OnOkClick="OnlineInterviewCreation_displayInterviewSavedConfirmMsgControl_okClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewCreation_displayInterviewSavedModalPopupExtender"
                            runat="server" PopupControlID="OnlineInterviewCreation_displayInterviewSavedPopupPanel"
                            TargetControlID="OnlineInterviewCreation_displayInterviewSavedHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="OnlineInterviewCreation_copyInterviewUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="float: left;">
                            <asp:Panel ID="OnlineInterviewCreation_interviewSaveAsPanel" runat="server" Style="display: none;
                                height: 250px;" CssClass="popupcontrol_interview_saveas">
                                <div id="OnlineInterviewCreation_interviewSaveAsDiv" style="display: none">
                                    <asp:Button ID="OnlineInterviewCreation_interviewSaveAs_hiddenButton" runat="server" />
                                </div>
                                <uc4:InterviewSaveAs ID="OnlineInterviewCreation_interviewSaveAsControl" runat="server"
                                    Title="Create a Copy" Type="InterviewScoreConfirmType" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewCreation_interviewSaveAs_ModalPopupExtender"
                                BehaviorID="OnlineInterviewCreation_interviewSaveAs_ModalPopupExtender" runat="server"
                                PopupControlID="OnlineInterviewCreation_interviewSaveAsPanel" TargetControlID="OnlineInterviewCreation_interviewSaveAs_hiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <!-- Delete interview skill confirmation popup -->
        <tr>
            <td>
                <asp:UpdatePanel ID="OnlineInterviewCreation_skillDeleteUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="OnlineInterviewCreation_skillDeleteHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="OnlineInterviewCreation_skillDeletePopupPanel" runat="server"
                            Style="display: none" CssClass="popupcontrol_confirm_remove">
                            <uc2:confirmmsgcontrol id="OnlineInterviewCreation_skillDeleteConfirmMsgControl"
                                runat="server" onokclick="OnlineInterviewCreation_skillDeleteConfirmMsgControl_okClick"
                                oncancelclick="OnlineInterviewCreation_skillDeleteConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="OnlineInterviewCreation_skillDeleteHiddenFiled"
                                runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewCreation_skillDeleteModalPopupExtender"
                            runat="server" PopupControlID="OnlineInterviewCreation_skillDeletePopupPanel"
                            TargetControlID="OnlineInterviewCreation_skillDeleteHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="OnlineInterviewCreation_bottomSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="OnlineInterviewCreation_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewCreation_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
