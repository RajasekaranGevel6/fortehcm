﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/InterviewMaster.Master" AutoEventWireup="true"
    Title="Online Interview Assessor Assignment" CodeBehind="OnlineInterviewAssessor.aspx.cs"
    Inherits="Forte.HCM.UI.OnlineInterview.OnlineInterviewAssessor" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CorporateUserSignUp.ascx" TagName="CorporateUserSignUp"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/OnlineInterviewMenuControl.ascx" TagName="OnlineInterviewMenuControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="DeleteAssessorConfirmMsgControl"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="OnlineInterview_Content" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <script language="javascript" type="text/javascript">
        function AesssorRequest(userID, btnCnrl, genId) {
            var height = 550;
            var width = 850;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
            var queryStringValue = "../Popup/DateRequest.aspx" +
                "?userid=" + userID +
                "&btncnrl=" + btnCnrl +
                "&genid=" + genId;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            if (div.style.display == "none") {
                div.style.display = "block";
            } else {
                div.style.display = "none";
            }
            return false;
        }

        function DefaultDivExpandCollapse(divname) {
            var div = document.getElementById(divname);
            if (div.style.display == "none") {
                div.style.display = "block";
            } else {
                div.style.display = "none";
            }
            return false;
        }
    </script>
    <asp:UpdatePanel ID="OnlineInterview_mainUpdatePanel" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td>
                        <uc3:OnlineInterviewMenuControl ID="OnlineInterviewAssessor_menuControl" runat="server"
                            Visible="true" PageType="AS" />
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="OnlineInterview_topMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="OnlineInterview_topErrorLabel" runat="server" Text="" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="OnlineInterview_topSuccessLabel" runat="server" Text="" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="non_tab_body_bg">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:RadioButtonList ID="OnlineInterview_assessorOption_RadioButtonList" runat="server"
                                                    SkinID="sknSkillRadioButtonList" AutoPostBack="true" OnSelectedIndexChanged="OnlineInterview_assessorOption_RadioButtonList_SelectedIndexChanged">
                                                    <asp:ListItem Value="RC">Show only assessors that have interviewed for this client in the past</asp:ListItem>
                                                    <asp:ListItem Value="RS">Show only assessors that have interviewed for relevant skills in the past</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="RA">Show all assessors</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_10">
                                    <div style="visibility: hidden">
                                        <asp:Button ID="OnlineInterview_assessorDefaultLoad_Button" runat="server" Text="Load"
                                            OnClick="OnlineInterview_assessorDefaultLoad_Button_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="66%" class="panel_bg">
                                                <asp:Label ID="OnlineInterview_seletionOptionText_Label" runat="server" Text="Show all"></asp:Label>
                                            </td>
                                            <td width="17%" class="header_bg_selectAll">
                                                <asp:Button ID="OnlineInterview_addNewUser_Button" runat="server" Text="Add New User"
                                                    SkinID="sknButtonId" OnClick="OnlineInterview_addNewUser_Button_Click" />
                                                <div id="OnlineInterview_addNewUser_hiddenDiv" style="display: none;">
                                                    <asp:Button ID="OnlineInterview_addNewUserHiddenButton" runat="server" />
                                                </div>
                                                <ajaxToolKit:ModalPopupExtender ID="OnlineInterview_addNewUserModalPopupExtender"
                                                    runat="server" PopupControlID="OnlineInterview_addNewUserPanel" TargetControlID="OnlineInterview_addNewUserHiddenButton"
                                                    BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="OnlineInterview_addNewUserPanel" runat="server" Style="display: none"
                                                    Height="400px" CssClass="popupcontrol_subscription_types">
                                                    <uc2:CorporateUserSignUp ID="OnlineInterview_CorporateUserSignUp" runat="server" />
                                                </asp:Panel>
                                            </td>
                                            <td width="17%" class="header_bg_selectAll">
                                                <asp:Button ID="OnlineInterview_addAssessor_Button" runat="server" Text="Add Assessor"
                                                    SkinID="sknButtonId" />
                                                <div style="display: none">
                                                    <asp:Button ID="OnlineInterview_refreshGridButton" runat="server" Text="Refresh"
                                                        SkinID="sknButtonId" OnClick="OnlineInterview_refreshGridButton_Click" />
                                                    <asp:TextBox runat="server" ID="OnlineInterview_assessorNameTextBox"></asp:TextBox>
                                                </div>
                                                <asp:HiddenField runat="server" ID="OnlineInterview_addedAssessorIdHiddenField" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="non_tab_body_bg">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="40%" valign="top">
                                                <asp:UpdatePanel ID="OnlineInterview_assessorList_UpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:GridView ID="OnlineInterview_assessorList_GridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                                        AutoGenerateColumns="False" Width="100%"
                                                                        OnSorting="OnlineInterview_assessorList_GridView_Sorting" 
                                                                        OnRowDataBound="OnlineInterview_assessorList_GridView_RowDataBound"
                                                                        OnRowCommand="OnlineInterview_assessorList_GridView_RowCommand">
                                                                        <EmptyDataRowStyle CssClass="error_message_text" />
                                                                        <EmptyDataTemplate>
                                                                            <table style="width: 100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        No assessors to display
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </EmptyDataTemplate>
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:RadioButton ID="OnlineInterview_assessorList_GridView_viewAssessorRadioButton"
                                                                                        runat="server" ToolTip="Click here to view assessor assignments" CommandArgument='<%# Eval("UserID") %>'
                                                                                        AutoPostBack="true" OnCheckedChanged="OnlineInterview_assessorList_GridView_viewAssessorRadioButton_CheckedChanged" />
                                                                                    <asp:ImageButton ID="OnlineInterview_assessorList_GridView_addAssessorImageButton"
                                                                                        runat="server" SkinID="sknPPAddRecruiterImageButton" CommandName="AssignAssessor"
                                                                                        CommandArgument='<%# Eval("UserID") %>' ToolTip="Assign Assessor" />
                                                                                    <asp:HiddenField ID="OnlineInterview_assessorList_GridView_userIDHiddenField" Value='<%# Eval("UserID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="OnlineInterview_assessorList_GridView_assessorNameHiddenField"
                                                                                        Value='<%# Eval("RecruiterFirstName") %>' runat="server" />
                                                                                    <asp:HiddenField ID="OnlineInterview_assessorList_GridView_assessorEMailHiddenField"
                                                                                        Value='<%# Eval("RecruiterEMail") %>' runat="server" />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="15%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Name" SortExpression="RECRUITER_NAME">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="OnlineInterview_assessorList_GridView_assessorNameLinkButton"
                                                                                        runat="server" Text='<%# Eval("RecruiterName")  %>' ToolTip="Click here to view assessor profile"></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <uc1:PageNavigator ID="OnlineInterview_assessorList_assessorsPageNavigator" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorOption_RadioButtonList" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td width="20%" align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="panel_bg" align="center" width="100%">
                                                            <asp:Label ID="OnlineInterview_calendarTitle_Label" runat="server" Text="Calendar"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="panel_body_bg">
                                                            <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:UpdatePanel ID="OnlineInterview_assessorList_Calendar_UpdatePanel" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:Calendar ID="OnlineInterview_assessorList_Calendar" runat="server" BackColor="White"
                                                                                    BorderColor="Black" BorderWidth="1px" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black"
                                                                                    Height="168px" Width="170px" SelectionMode="Day" OtherMonthDayStyle-Font-Underline="false"
                                                                                    DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                                    DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                                    TodayDayStyle-Font-Underline="false" OnSelectionChanged="OnlineInterview_assessorList_Calendar_SelectionChanged"
                                                                                    OnDayRender="OnlineInterview_assessorList_Calendar_DayRender" OnVisibleMonthChanged="OnlineInterview_assessorList_Calendar_VisibleMonthChanged">
                                                                                    <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                                    <SelectedDayStyle Font-Underline="false" BackColor="#59C1EE" ForeColor="Black"></SelectedDayStyle>
                                                                                    <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                                    <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                                                </asp:Calendar>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorList_GridView" EventName="RowCommand" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #E1D455" title="Request Initiated Dates">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="OnlineInterview_initiatedDates_Label" runat="server" Text="Request Initiated Dates"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #7092BE" title="Available Dates">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="OnlineInterview_selectedDates_Label" runat="server" Text="Available Dates"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #99DD96" title="Dates With Schedule">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="OnlineInterview_scheduledDates_Label" runat="server" Text="Dates With Schedule"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #F29378" title="Vacation Dates">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="OnlineInterview_vocationDates_Label" runat="server" Text="Vacation Dates"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="40%" valign="top">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="panel_bg">
                                                            <asp:Label ID="Label1" runat="server" Text="Slots"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="panel_inner_body_bg">
                                                            <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <div style="height: 26px; overflow: auto; padding: 5px">
                                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:ImageButton ID="OnlineInterview_previousDate" runat="server" ToolTip="Go to the previous date"
                                                                                            SkinID="sknPreviousDateImage" OnClick="OnlineInterview_previousDate_Click" Width="16px" />
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:Label ID="OnlineInterview_selectedDateLabel" runat="server" Text="" SkinID="sknLabelSelectedDateTextSmall"></asp:Label>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:ImageButton ID="OnlineInterview_nextDate" runat="server" ToolTip="Go to the next date"
                                                                                            SkinID="sknNextDateImage" OnClick="OnlineInterview_nextDate_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="OnlineInterview_initiatedBy_GridViewUpdatePanel" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:GridView ID="OnlineInterview_initiatedBy_GridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                                                    AutoGenerateColumns="False">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Interview Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="OnlineInterview_initiatedBy_GridView_InterviewName_Label" runat="server"
                                                                                                    Text='<%# Eval("InterviewName")  %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Initiated By">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="OnlineInterview_initiatedBy_GridView_InitiatedBy_Label" runat="server"
                                                                                                    Text='<%# Eval("TestAuthor")  %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="From">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="OnlineInterview_initiatedBy_GridView_FromTimeLabel" runat="server"
                                                                                                    SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextFrom") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="To">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="OnlineInterview_initiatedBy_GridView_ToTimeLabel" runat="server" SkinID="sknHomePageLabel"
                                                                                                    Text='<%# Eval("TimeSlotTextTo") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorList_Calendar" />
                                                                                <asp:AsyncPostBackTrigger ControlID="OnlineInterview_nextDate" />
                                                                                <asp:AsyncPostBackTrigger ControlID="OnlineInterview_previousDate" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_10">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%">
                                    <asp:UpdatePanel ID="OnlineInterview_assessorListUpdatePanel" runat="server">
                                        <ContentTemplate>
                                            <asp:DataList ID="OnlineInterview_assessorList_DataList" runat="server" BorderStyle="Solid"
                                                CellPadding="1" CellSpacing="1" GridLines="Both" RepeatColumns="1" Width="100%"
                                                OnItemCommand="OnlineInterview_assessorList_DataList_ItemCommand" OnItemDataBound="OnlineInterview_assessorList_DataList_ItemDataBound">
                                                <ItemTemplate>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="panel_bg" width="100%" align="left" colspan="3">
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td width="50%" align="left">
                                                                            <asp:HiddenField ID="OnlineInterview_assessorList_DataList_assessorEmailHiddenField"
                                                                                runat="server" />
                                                                            <asp:HiddenField ID="OnlineInterview_assessorList_DataList_assessorGenIdHiddenField"
                                                                                runat="server" Value='<%# Eval("ID")  %>' />
                                                                            <asp:HiddenField ID="OnlineInterview_assessorList_DataList_assessorIdHiddenField"
                                                                                runat="server" Value='<%# Eval("AssessorID")  %>' />
                                                                            <asp:HyperLink ID="OnlineInterview_assessorList_DataList_assessorName_HyperLink"
                                                                                SkinID="sknLabelFieldTextHyperLink" runat="server" ToolTip="Asssesor Name" Target="_blank"
                                                                                Text='<%# Eval("Assessor")  %>'></asp:HyperLink>
                                                                        </td>
                                                                        <td width="47%" align="right">
                                                                            <asp:ImageButton ID="OnlineInterview_assessorList_DataList_Request_ImageButton" runat="server"
                                                                                SkinID="sknCalendarImageButton" ToolTip="Request Slot" CommandName="RequestDate"
                                                                                CommandArgument='<%# Eval("AssessorID")  %>' />
                                                                            <asp:Button ID="OnlineInterview_assessorList_DataList_Request_Button" runat="server"
                                                                                Text="Request Slot" SkinID="sknButtonId" UseSubmitBehavior="false" Visible="false" />
                                                                        </td>
                                                                        <td width="3%" align="right">
                                                                            <asp:ImageButton ID="OnlineInterview_assessorList_DataList_deleteRequest_ImageButton"
                                                                                runat="server" SkinID="sknDeleteImageButton" ToolTip="Click here to delete the assessor"
                                                                                CommandName="DeleteAssessor" CommandArgument='<%# Eval("AssessorID")  %>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="25%" align="left" valign="top">
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td class="panel_bg">
                                                                            Calendar
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="non_tab_body_bg">
                                                                            <asp:Calendar ID="OnlineInterview_assessorList_DataList_Calendar" runat="server"
                                                                                BackColor="White" BorderColor="Black" BorderWidth="1px" Font-Names="Verdana"
                                                                                Font-Size="8pt" ForeColor="Black" Height="168px" Width="170px" OtherMonthDayStyle-Font-Underline="false"
                                                                                DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                                DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                                TodayDayStyle-Font-Underline="false" SelectionMode="None" OnVisibleMonthChanged="OnlineInterview_assessorList_DataList_Calendar_VisibleMonthChanged">
                                                                                <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                                <SelectedDayStyle Font-Underline="false" BackColor="#7092BE" ForeColor="White"></SelectedDayStyle>
                                                                                <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                                <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                                            </asp:Calendar>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="1%">
                                                                &nbsp;
                                                            </td>
                                                            <td width="74%" align="left" valign="top">
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td class="panel_bg">
                                                                            Slots
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="non_tab_body_bg">
                                                                            <div style="overflow: auto; height: 168px" runat="server" id="OnlineInterview_assessorTimeSlotDiv"
                                                                                visible="true">
                                                                                <asp:GridView ID="OnlineInterview_assessorTimeSlot_GridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                                                    OnRowDataBound="OnlineInterview_assessorTimeSlot_GridView_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Date">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="OnlineInterview_assessorTimeSlot_GridView_AvailableDate_GenIdHiddenField"
                                                                                                    runat="server" Value='<%# Eval("ID") %>' />
                                                                                                <asp:LinkButton ID="OnlineInterview_assessorTimeSlot_GridView_AvailableDateLinkButton"
                                                                                                    runat="server" SkinID="sknActionLinkButton" Text='<%#  GetDateFormat(Convert.ToDateTime(Eval("AvailabilityDate"))) %>'
                                                                                                    CommandName="2" CommandArgument='<%# Eval("AssessorID") %>' ToolTip="Click here to view the time slots"><%--TimeSlotDate--%></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Status">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_StatusLabel" runat="server"
                                                                                                    Text='<%# Eval("Scheduled") %>' SkinID="sknHomePageLabel"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td colspan="100%">
                                                                                                        <div id="Assessor_timeDetailsDiv" runat="server" class="table_outline_bg" style="display: none;
                                                                                                            position: relative; left: 15px; overflow: auto; width: 90%">
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:GridView ID="AssessorTimeSlot_PopupGridView" runat="server" SkinID="sknWrapHeaderGrid">
                                                                                                                            <Columns>
                                                                                                                                <asp:TemplateField HeaderText="From">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupFromTimeLabel" runat="server"
                                                                                                                                            SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextFrom") %>'></asp:Label>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:TemplateField HeaderText="To">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupToTimeLabel" runat="server"
                                                                                                                                            SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextTo") %>'></asp:Label>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:TemplateField HeaderText="Status">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupStatusLabel" runat="server"
                                                                                                                                            SkinID="sknHomePageLabel" Text='<%# Eval("RequestStatus") %>'></asp:Label>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                            </Columns>
                                                                                                                        </asp:GridView>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <a href="#SearchQuestion_focusmeLink" id="AssessorTimeSlot_Popup_focusDownLink" runat="server">
                                                                                                                        </a><a href="#" id="SearchQuestion_focusmeLink" runat="server"></a>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="100%">
                                                                                                        <div id="Assessor_defaultTimeDetailsDiv" runat="server" class="table_outline_bg"
                                                                                                            style="display: none; position: relative; left: 15px; overflow: auto; width: 90%;
                                                                                                            text-align: center">
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td height="30px" align="justify">
                                                                                                                        <asp:Label ID="Assessor_defaultTimeDetailLabel" runat="server" Text="Available any time"
                                                                                                                            SkinID="sknSuccessMessage"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="padding: 5px;">
                                                                            <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_SkillTitleLabel" runat="server"
                                                                                Text="Skills" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBoxList ID="OnlineInterview_skill_CheckBoxList" runat="server" DataTextField="SkillName"
                                                                                SkinID="sknSkillsCheckBoxList" DataValueField="SkillID" RepeatColumns="5" RepeatDirection="Horizontal"
                                                                                TextAlign="Right" CellPadding="5" CellSpacing="5">
                                                                            </asp:CheckBoxList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorList_GridView" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                        <asp:HiddenField ID="OnlineInterview_deleteAssessorHiddenField" runat="server" />
                        <asp:Panel ID="OnlineInterview_deleteAssessorPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc4:DeleteAssessorConfirmMsgControl ID="OnlineInterview_deleteAssessorConfirmMsgControl"
                                runat="server" OnOkClick="OnlineInterview_deleteAssessorConfirmMsgControl_okClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OnlineInterview_deleteAssessorModalPopupExtender"
                            runat="server" PopupControlID="OnlineInterview_deleteAssessorPanel" TargetControlID="OnlineInterview_deleteAssessorHiddenField"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td align="left" style="width: 120px">
                                    <asp:Button ID="OnlineInterview_saveButton" runat="server" SkinID="sknButtonId" Text="Save and Continue"
                                        OnClick="OnlineInterview_saveButton_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="OnlineInterviewAssessor_cancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                        Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="OnlineInterview_bottomMessage_UpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="OnlineInterview_bottomErrorLabel" runat="server" Text="" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="OnlineInterview_bottomSuccessLabel" runat="server" Text="" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
