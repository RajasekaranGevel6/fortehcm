﻿#region Directives
using System;
using System.IO;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

using iTextSharp.text;
using iTextSharp.text.pdf;
#endregion

namespace Forte.HCM.UI.OnlineInterview
{
    public partial class OnlineInterviewAssessorSummary : PageBase
    {
        #region Private Variables

        public string videoURL = string.Empty;
        public string candidateID = string.Empty;

        #endregion

        #region Handler Methods

        protected void Page_Init(object sender, EventArgs e)
        {
            ClearControls();
            LoadTable();
            AssessorRatingTable();
        }

        /// <summary>
        /// The event called when the page load
        /// </summary>
        /// <param name="sender">
        /// A<see cref="System.Object"/> that holds the sender the event
        /// </param>
        /// <param name="e">
        /// A<see cref="System.EventArgs"/>that holds the event result set
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Online Interview Assessor Summary");
            OnlineInterviewCandidateAssessorSummary_topErrorLabel.Text = string.Empty;

            try
            {
                if (!IsPostBack)
                { 
                    if (Request.QueryString["interviewkey"] == null) return;
                    if (Request.QueryString["interviewid"] == null) return;
                    if (Request.QueryString["chatroomid"] == null) return;

                    if (!Utility.IsNullOrEmpty(Request.QueryString["interviewkey"]))
                    {
                        string serverURL = ConfigurationManager.AppSettings["INTERVIEW_VIDEO_STREAMING_SERVER_URL"].ToString();
                        LoadHeaderDetail(); 
                        videoURL = string.Concat(serverURL, Request.QueryString["interviewkey"], "/" + Request.QueryString["chatroomid"]);

                        candidateID = new OnlineInterviewAssessorBLManager().GetCandidateUserName(Convert.ToInt32(Request.QueryString["interviewid"]));
                        //AssessorRatingTable();
                       
                    }
                }
                else
                {
                    // SetQuestionVisibleOnPagePostBack();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCandidateAssessorSummary_topErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HtmlGenericControl OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_AssessorCommentsDiv = (HtmlGenericControl)e.Row.
                        FindControl("OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_AssessorCommentsDiv");

                    Label OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_WeightedScoreLabel = (Label)e.Row.
                        FindControl("OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_WeightedScoreLabel");

                    DataRowView drv = (DataRowView)e.Row.DataItem;

                    if (drv == null) return;

                    DataRow dr = drv.Row;

                    if (OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_AssessorCommentsDiv != null)
                        OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_AssessorCommentsDiv.InnerText = Convert.ToString(dr["COMMENTS"]);

                    if (OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_WeightedScoreLabel != null)
                        OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview_WeightedScoreLabel.Text =
                            string.Format("{0:0.00}%", Convert.ToDecimal(dr["OVERALL_RATING"]));

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCandidateAssessorSummary_topErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "SKILL_SELECT")
                {
                    if (ViewState["SUMMARY_DATA"] == null) return;

                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;

                    LinkButton OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton =
                            (LinkButton)row.FindControl("OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton");

                    ViewState["SELECTED_SKILL_NAME"] = OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton.Text;
                    ViewState["SELECTED_SKILL_ID"] = e.CommandArgument.ToString();

                    Table tbl = (Table)OnlineInterviewCandidateAssessorSummary_tablePlaceHolder.FindControl(e.CommandArgument.ToString() + "ID");

                    if (tbl != null)
                    {
                        //tbl.Attributes.CssStyle.Value
                        if(tbl.Attributes.CssStyle.Value.Contains("block"))
                            tbl.Attributes.Add("style", "width:100%;display:none");
                        else
                            tbl.Attributes.Add("style","width:100%;display:block");
                    } 
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCandidateAssessorSummary_topErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow) return;

                Label OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillScoreLabel = (Label)e.Row.
                    FindControl("OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillScoreLabel");

                LinkButton OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton =
                            (LinkButton)e.Row.FindControl("OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton");

                DataRowView drv = (DataRowView)e.Row.DataItem;

                if (drv == null) return;

                DataRow dr = drv.Row;

                if (OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillScoreLabel != null)
                    OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillScoreLabel.Text =
                        string.Format("{0:0.00}%", Convert.ToDecimal(dr["SUBJECT_WEIGHTAGE_RATING"]));

                Table tbl = (Table)OnlineInterviewCandidateAssessorSummary_tablePlaceHolder.FindControl(dr["SKILL_ID"].ToString() + "ID"); 


                if (tbl == null || OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton == null) return;

                OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview_SkillLinkButton.Attributes.Add("onclick", "return ShowTable('" + tbl.ClientID + "','" + SkillTablesID()+ "');");
             
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCandidateAssessorSummary_topErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterviewCandidateAssessorSummary_topDownloadButton_Click(object sender, EventArgs e)
        {
            try
            {  
                OnlineInterviewCandidateAssessorSummary_pdfTypeModalPopupExtender.Show(); 

                /*if (Request.QueryString["interviewkey"] == null || Request.QueryString["chatroomid"] == null)
                {
                    base.ShowMessage(OnlineInterviewCandidateAssessorSummary_topErrorLabel, "Assessment data not found");
                    return;
                } 

                DataTable assessmentTable = new DataTable();

                CandidateInterviewSessionDetail sessionDetail = new OnlineInterviewAssessorBLManager().GetOnlineInterviewDetail
                        (Request.QueryString["interviewkey"].ToString(), Request.QueryString["chatroomid"].ToString());

                PDFDocumentWriter pdfWriter = new PDFDocumentWriter();

                string fileName = sessionDetail.CandidateName + "_" + "InterviewAssessmentSummary.pdf";

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                Document doc = new Document(PDFDocumentWriter.paper,
                    PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                    PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

                string logoPath = Server.MapPath("~/");

                iTextSharp.text.Image imgBackground = iTextSharp.text.Image.GetInstance(logoPath + "Images/AssessmentBackground.jpg");

                imgBackground.ScaleToFit(1000, 132);
                imgBackground.Alignment = iTextSharp.text.Image.UNDERLYING;
                imgBackground.SetAbsolutePosition(50, 360);

                PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(logoPath, "Candidate Online Interview Assessment Report"));
                pageHeader.SpacingAfter = 10f;

                //Construct Table Structures    
                PdfPTable tableHeaderDetail = new PdfPTable(1);
                PdfPCell cellBorder = new PdfPCell(new Phrase(""));
                cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cellBorder.Border = 0;
                tableHeaderDetail.WidthPercentage = 90;
                cellBorder.PaddingTop = 20f; 

                cellBorder.AddElement(pdfWriter.OnlineInterviewTableHeader(sessionDetail, Convert.ToInt32(Request.QueryString["interviewid"]),
                    GetCandidateImageFile(sessionDetail.CandidateInformationID), out assessmentTable));

                tableHeaderDetail.AddCell(cellBorder);
                tableHeaderDetail.SpacingAfter = 30f;
                //Response Output
                PdfWriter.GetInstance(doc, Response.OutputStream);

                doc.Open();
                doc.Add(pageHeader);
                doc.Add(imgBackground);
                doc.Add(tableHeaderDetail);
                doc.Add(pdfWriter.OnlineInterviewTableDetail(assessmentTable));
                doc.Close();*/
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewCandidateAssessorSummary_topErrorLabel, exp.Message);
            }
        }

        protected void OnlineInterviewCandidateAssessorSummary_reportButton_Click(object sender, EventArgs e)
        {  
            if (Request.QueryString["interviewkey"] == null || Request.QueryString["chatroomid"] == null)
            {
                base.ShowMessage(OnlineInterviewCandidateAssessorSummary_topErrorLabel, "Assessment data not found");
                OnlineInterviewCandidateAssessorSummary_pdfTypeModalPopupExtender.Show();
                return;
            }
             string downloadType = "OIPDF";
             string rptType = OnlineInterviewCandidateAssessorSummary_pdfTypeRadioButtonList.SelectedItem.Value; 

             string scrpt = "<script language='javascript'> OpenDownloadPopUp('" + downloadType + "','" +
                 Request.QueryString["interviewkey"] + "','" + Request.QueryString["chatroomid"] + "','" + Request.QueryString["interviewid"] + "','" + rptType + "');</script>";

             ScriptManager.RegisterStartupScript(this, this.GetType(), "rpt", scrpt, false);
               
        }
        #endregion

        #region Private Functions

        private string SkillTablesID()
        {       
            DataSet ds = ViewState["SUMMARY_DATA"] as DataSet;
            string controlIds = null;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                Table tbl = (Table)OnlineInterviewCandidateAssessorSummary_tablePlaceHolder.FindControl(dr["SKILL_ID"].ToString() + "ID");

                if (tbl == null) continue;
                controlIds += tbl.ClientID +","; 
            }

            if(controlIds!=null)
                controlIds = controlIds.Remove(controlIds.LastIndexOf(',') );

            return controlIds ;
        }
        /// <summary>
        /// Clears the controls values 
        /// </summary>
        private void ClearControls()
        {
            OnlineInterviewCandidateAssessorSummary_candidateNameLinkButton.Text = string.Empty;
            OnlineInterviewCandidateAssessorSummary_interviewNameLabel.Text = string.Empty;
            OnlineInterviewCandidateAssessorSummary_interviewCompletedLabel.Text = string.Empty;
            OnlineInterviewCandidateAssessorSummary_totalWeightageValeuLabel.Text = string.Empty;
            OnlineInterviewCandidateAssessorSummary_totalWeightageLabel.Text = string.Empty;
            OnlineInterviewCandidateAssessorSummary_positionProfileNameLinkButton.Text = string.Empty;
            OnlineInterviewCandidateAssessorSummary_positionProfile_ClientNameLinkButton.Text = string.Empty;
        }

        /// <summary>
        /// Load the interview detail aganist the interview key.
        /// </summary>
        private void LoadHeaderDetail()
        {
            CandidateInterviewSessionDetail sessionDetail = new OnlineInterviewAssessorBLManager().GetOnlineInterviewDetail
                        (Request.QueryString["interviewkey"].ToString(), Request.QueryString["chatroomid"].ToString());// "QW6UXZP7");

            if (Utility.IsNullOrEmpty(sessionDetail)) return;

            OnlineInterviewCandidateAssessorSummary_candidateNameLinkButton.Text = sessionDetail.CandidateName;
            OnlineInterviewCandidateAssessorSummary_interviewNameLabel.Text = sessionDetail.InterviewTestName;
            OnlineInterviewCandidateAssessorSummary_interviewCompletedLabel.Text = "Interview Completed on " + sessionDetail.DateCompleted.ToShortDateString();

            OnlineInterviewCandidateAssessorSummary_positionProfileNameLinkButton.Text = sessionDetail.PositionProfileName;
            OnlineInterviewCandidateAssessorSummary_positionProfile_ClientNameLinkButton.Text = sessionDetail.ClientName;

            if (sessionDetail.PositionProfileID != 0)
            {
                OnlineInterviewCandidateAssessorSummary_positionProfileNameLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewPositionProfile('" + sessionDetail.PositionProfileID + "');");
            }

            if (sessionDetail.ClientID != 0)
            {
                OnlineInterviewCandidateAssessorSummary_positionProfile_ClientNameLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewClient('" + sessionDetail.ClientID + "');");
            }

            // Assign handler to candidate profile page.
            OnlineInterviewCandidateAssessorSummary_candidateNameLinkButton.Attributes.Add("onclick",
                    "javascript:return OpenViewCandidateProfilePopup('" + sessionDetail.CandidateInformationID + "');");

            //OnlineInterviewCandidateAssessorSummary_totalWeightageValeuLabel.Text = "";
            OnlineInterviewCandidateAssessorSummary_totalWeightageLabel.Text = "Total Weighted Score";
        } 
         
        /// <summary>
        /// Method that constructs and load the table.
        /// </summary>
        private void LoadTable()
        {
            OnlineInterviewCandidateAssessorSummary_tablePlaceHolder.Controls.Clear();
            OnlineInterviewCandidateAssessorSummary_totalWeightageValeuLabel.Text = string.Empty;
            OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview.DataSource = null;
            OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview.DataBind();

            OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview.DataSource = null;
            OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview.DataBind();

            if (Request.QueryString["interviewid"] == null) return;

            int onlineInterviewId = Convert.ToInt32(Request.QueryString["interviewid"]);

            DataSet summaryData = new AssessmentSummaryBLManager().
                GetOnlineInterviewAssessorySummary(onlineInterviewId);

            if (summaryData == null) return;

            ViewState["SUMMARY_DATA"] = summaryData;

            decimal totalScore = 0m;
            decimal totalWeightedScore = 0m;

            foreach (DataRow dr in summaryData.Tables[1].Rows)
            {
                // Get rating summary table.
                DataTable table = new OnlineInterviewDataManager().GetRatingSummaryTable(summaryData, Convert.ToInt32(dr["SKILL_ID"]), out totalScore, out totalWeightedScore); 
                // Assign rating summary table.            
                OnlineInterviewCandidateAssessorSummary_tablePlaceHolder.Controls.Add(GetTable(table, dr["SKILL_NAME"].ToString(), Convert.ToInt32(dr["SKILL_ID"]))); 
            } 
             
            DataTable dtAssessorSummary = summaryData.Tables[0];

            DataTable dtSkillSummary = summaryData.Tables[1];

            OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview.DataSource = dtAssessorSummary;
            OnlineInterviewCandidateAssessorSummary_asssessorScore_Gridview.DataBind();

            OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview.DataSource = dtSkillSummary;
            OnlineInterviewCandidateAssessorSummary_asssessmentSubjectScore_Gridview.DataBind();
            if (summaryData.Tables[5] != null && summaryData.Tables[5].Rows.Count > 0)
                if(!Utility.IsNullOrEmpty(summaryData.Tables[5].Rows[0][0]))
                    OnlineInterviewCandidateAssessorSummary_totalWeightageValeuLabel.Text = string.Format("{0:N2}%", Convert.ToDecimal(summaryData.Tables[5].Rows[0][0]));
                else
                    OnlineInterviewCandidateAssessorSummary_totalWeightageValeuLabel.Text = string.Empty;

            if(!Utility.IsNullOrEmpty(SkillTablesID()) )
                OnlineInterviewCandidateAssessorSummary_assessmentSubjectScore_LinkButton.Attributes.Add("onclick", "return ShowAllScore('" + OnlineInterviewCandidateAssessorSummary_assessmentSubjectScore_LinkButton.ClientID + "','" + SkillTablesID() + "')");
             
        }

        /// <summary>
        /// Method that retrieves the table that comprises of the header and
        /// rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref=""/> that holds the data for the table to be 
        /// constructed.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the table.
        /// </returns>
        private Table GetTable(DataTable table, string headerText,int skillId)
        {
            Table htmlTable = new Table();

            htmlTable.ID = skillId.ToString() + "ID";

            htmlTable.CssClass = "candidate_assessor_summary_table";
            htmlTable.CellPadding = 0;
            htmlTable.CellSpacing = 0;
            htmlTable.Style.Add("width", "100%");

            htmlTable.Style.Add("display","none");  

            // Create skill header.
            TableHeaderRow tableHeaderRow = new TableHeaderRow();
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = table.Columns.Count;
            headerCell.Text ="<br />"+ headerText;
            headerCell.CssClass = "candidate_assessor_summary_table_cell_subject";
            tableHeaderRow.Cells.Add(headerCell);
            htmlTable.Rows.Add(tableHeaderRow);
            // Create header.
            TableRow headerRow = new TableRow();

            // Add the header row cells.
            headerRow.Cells.AddRange(GetTableHeaderCells(table.Columns));

            // Add the header row to the table.
            htmlTable.Rows.Add(headerRow);

            DataSet ds = ViewState["SUMMARY_DATA"] as DataSet;

            // Create rows.
            foreach (DataRow row in table.Rows)
            {
                TableRow tableRow = new TableRow();

                // Create row.
                tableRow.Cells.AddRange(GetTableRowCells(row, table.Columns.Count - 1));

                int dollarIndex = row["TITLE"].ToString().Trim().LastIndexOf('$');

                if (row["FLAG"].ToString() == "QF")
                {
                    tableRow.Style.Add("display", "none");
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        if (dr["QUESTION_KEY"].ToString().Trim() == row["TITLE"].ToString().Trim().Substring(dollarIndex + 1))
                        {
                            dr["VISIBLE"] = "T";
                        }
                    }
                }
                else if (row["FLAG"].ToString() == "QT")
                {
                    tableRow.Style.Add("display", "table-row");
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        if (dr["QUESTION_KEY"].ToString().Trim() == row["TITLE"].ToString().Trim().Substring(dollarIndex + 1))
                        {
                            dr["VISIBLE"] = "F";
                        }
                    }
                }

                if (row["FLAG"].ToString() == "QT" || row["FLAG"].ToString() == "QF")
                {
                    tableRow.ID = row["TITLE"].ToString().Trim().Substring(dollarIndex + 1);
                    tableRow.Style.Add("width", "100%");
                }


                ds.AcceptChanges();

                // Add the row to the table.
                htmlTable.Rows.Add(tableRow); 
            }

            ViewState["SUMMARY_DATA"] = ds;

            return htmlTable;
        }

        /// <summary>
        /// Method that retrieves the tables header that comprises of the 
        /// header cells collection.
        /// </summary>
        /// <param name="columns">
        /// A <see cref="DataColumnCollection"/> that holds the data for the 
        /// columns to be constructed.
        /// </param>
        /// <returns>
        /// An array of <see cref="TableHeaderCell"/> that holds the table 
        /// header row.
        /// </returns>
        private TableHeaderCell[] GetTableHeaderCells(DataColumnCollection columns)
        {
            TableHeaderCell[] tableHeaderCells = new TableHeaderCell[columns.Count - 1];

            //TableHeaderCell[] tableHeaderCells = new TableHeaderCell[columns.Count];

            //int accessorColumnWidth = 80 / (tableHeaderCells.Length - 2);
            decimal accessorColumnWidth = 55 / (tableHeaderCells.Length - 1);

            for (int columnIndex = 0; columnIndex < tableHeaderCells.Length; columnIndex++)
            {
                tableHeaderCells[columnIndex] = new TableHeaderCell();
                tableHeaderCells[columnIndex].VerticalAlign = VerticalAlign.Middle;
                tableHeaderCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;

                if (columnIndex == 0)
                {
                    // Title column. 
                    tableHeaderCells[columnIndex].Style.Add("width", "45%");
                    tableHeaderCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_title_header";
                    tableHeaderCells[columnIndex].Text = "Assessor";
                }
                else
                {
                    // Assessor column. 
                    tableHeaderCells[columnIndex].Style.Add("width", accessorColumnWidth.ToString() + "%");
                    tableHeaderCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_assessor_header";

                    LinkButton linkButton = new LinkButton();
                    linkButton.CssClass = "candidate_assessor_summary_table_cell_assessor_header_link_button";
                    linkButton.Text = columns[columnIndex].Caption;


                    if (!Utility.IsNullOrEmpty(columns[columnIndex].ColumnName) &&
                      Convert.ToInt32(columns[columnIndex].ColumnName) >= 100000)
                    {
                        linkButton.ToolTip = "Click here to view external assessor profile";
                        //linkButton.Attributes.Add("onclick", "javascript:return ShowExternalAssessorProfile('" + columns[columnIndex].ColumnName + "','" + CandidateAssessorSummary_convertToRegisteredUserButton.ClientID + "');");
                    }
                    else
                    {
                        linkButton.ToolTip = "Click here to view assessor profile";
                        linkButton.Attributes.Add("onclick", "javascript:return ShowAssessorProfile('" + columns[columnIndex].ColumnName + "');");
                    }

                    // Add the link to the cell.
                    tableHeaderCells[columnIndex].Controls.Add(linkButton);
                }
            }

            return tableHeaderCells;
        }

        /// <summary>
        /// Method that retrieves the tables row that comprises of the cells
        /// collection.
        /// </summary>
        /// <param name="row">
        /// A <see cref="DataRow"/> that holds data for the row to be
        /// constructed.
        /// </param>
        /// <param name="columnsCount">
        /// A <see cref="int"/> that holds the columns count.
        /// </param>
        /// <returns>
        /// An array of <see cref="TableCell"/> that holds the table row.
        /// </returns>
        private TableCell[] GetTableRowCells(DataRow row, int columnsCount)
        {
            TableCell[] tableCells = new TableCell[columnsCount];

            for (int columnIndex = 0; columnIndex < columnsCount; columnIndex++)
            {
                tableCells[columnIndex] = new TableCell();

                if (columnIndex == 0)
                {
                    //Assign first Columns
                    if (row["FLAG"].ToString().Trim().ToUpper() == "S" || row["FLAG"].ToString().Trim().ToUpper() == "L")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "QF" || row["FLAG"].ToString().Trim().ToUpper() == "QT")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_question";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                }

                tableCells[columnIndex].VerticalAlign = VerticalAlign.Middle;
                tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Left;

                if (columnIndex == 0)
                {
                    // Check if the row is a question.
                    if (row["FLAG"].ToString().Trim().ToUpper() == "QF" || row["FLAG"].ToString().Trim().ToUpper() == "QT")
                    {
                        // Construct question and question key
                        string question = row[columnIndex].ToString().Trim();
                        string questionKey = null;

                        int dollarIndex = row[columnIndex].ToString().Trim().LastIndexOf('$');
                        if (dollarIndex != -1)
                        {
                            try
                            {
                                question = row[columnIndex].ToString().Trim().Substring(0, dollarIndex);
                                questionKey = row[columnIndex].ToString().Trim().Substring(dollarIndex + 1);
                            }
                            catch (Exception exp)
                            {
                                Logger.ExceptionLog(exp);
                            }
                        }

                        LinkButton linkButton = new LinkButton();
                        linkButton.CssClass = "candidate_assessor_summary_table_cell_question_link_button";
                        linkButton.Text = TrimContent(question, Convert.ToInt32(ConfigurationManager.AppSettings["QUESTION_DISPLAY_LENGTH"]));
                        linkButton.ToolTip = question;
                        linkButton.Attributes.Add("onclick", "javascript:return ShowViewInterviewQuestion('" + questionKey + "');");

                        // Add the link to the cell.
                        tableCells[columnIndex].Controls.Add(linkButton);
                    }
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                    {
                        tableCells[columnIndex].Text = "Overall Subject Score";
                        tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Left;
                    }
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                    {
                        tableCells[columnIndex].Text = row[columnIndex].ToString().Trim();
                        tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Left;
                    }
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "L")
                    {  
                        LinkButton linkButton = new LinkButton();
                        linkButton.ID = Guid.NewGuid().ToString();
                        linkButton.CssClass = "candidate_assessor_summary_table_cell_assessor_header_link_button";
                        linkButton.Text = row[columnIndex].ToString().Trim();
                        linkButton.ToolTip = "Click here to view the questions rating";
                         // Add the link to the cell.
                        tableCells[columnIndex].Controls.Add(linkButton); 
                    }
                }
                else
                {
                    tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_assessor";

                    // Weighted subject score column.
                    if (!Utility.IsNullOrEmpty(row[columnIndex]))
                    {
                        // Assign total weighted subject score.
                        if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        {
                            decimal value = 0m;
                            decimal.TryParse(row[columnIndex].ToString().Trim(), out value);
                            tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;
                            if (value < 0)
                                tableCells[columnIndex].Text = "Not Rated";
                            else
                                tableCells[columnIndex].Text = string.Format("{0:N2}%", value);
                        }
                        // Assign weight.
                        else if (row["FLAG"].ToString().Trim().ToUpper() == "QF" || row["FLAG"].ToString().Trim().ToUpper() == "QT")
                        {
                            decimal value = 0;
                            decimal.TryParse(row[columnIndex].ToString().Trim(), out value);
                            tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;

                            if (value <= 0)
                                tableCells[columnIndex].Text = "Not Rated";
                            else
                                tableCells[columnIndex].Text = string.Format("{0:N2}%", value);
                        }
                        else if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                        {
                            tableCells[columnIndex].Text = row[columnIndex].ToString().Trim();
                            tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Left;
                        }
                    }
                }
            }

            return tableCells;
        } 

        /// <summary>
        /// Method that retrieves the image path for the given candidate ID.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the candidate image path.
        /// </returns>
        private string GetCandidateImageFile(int candidateID)
        {
            string path = null;

            bool isExists = false;
            if (candidateID > 0)
            {
                path = Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];
                if (File.Exists(path))
                    isExists = true;
                else
                    isExists = false;
            }

            if (isExists)
                return path;
            else
            {     // Show the 'not available image'.
                return Server.MapPath("~/") +
                      ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                      "\\" + "photo_not_available.jpg";
            }
        }

         /// <summary>
        /// Set the event for question link button
        /// </summary>
        private void AssessorRatingTable()
        {  
            foreach (Control ctrl in OnlineInterviewCandidateAssessorSummary_tablePlaceHolder.Controls)
            {
                Table tbl = (Table)ctrl;
                
                if (tbl == null) continue;

                foreach (TableRow dr in tbl.Rows)
                {
                    foreach (TableCell tc in dr.Cells)
                    {
                        if (tc.Controls.Count > 0)
                        {
                            Control cellCtrl = tc.Controls[0];
                            if (cellCtrl != null)
                            {
                                LinkButton questionLinkButton = cellCtrl as LinkButton;
                                if (questionLinkButton.Text == "Questions")
                                {
                                    questionLinkButton.Attributes.Add("onclick", "return CheckQuestionVisible('" + tbl.ClientID + "');"); 
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion
        
        
    }
}