﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineInterviewSession.aspx.cs"
    Inherits="Forte.HCM.UI.OnlineInterview.OnlineInterviewSession" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewSaveAsControl.ascx" TagName="InterviewSaveAs"
    TagPrefix="uc1" %>
<asp:Content ID="OnlineInterviewSession_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="OnlineInterviewSession_headerLiteral" runat="server" Text="Search Interview"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="OnlineInterviewSession_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="OnlineInterviewSession_resetLinkButton_Click"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="OnlineInterviewSession_topCancelLinkButton"
                                            runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="OnlineInterviewSession_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="OnlineInterviewSession_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewSession_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="OnlineInterviewSession_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="OnlineInterviewSession_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <asp:UpdatePanel runat="server" ID="OnlineInterviewSession_criteriaUpdatePanel">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                                                                <tr>
                                                                                    <td style="width: 14%">
                                                                                        <asp:Label ID="OnlineInterviewSession_interviewKeyLabel" runat="server" Text="Interview&nbsp;Key"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 18%">
                                                                                        <asp:TextBox ID="OnlineInterviewSession_interviewKeyTextBox" MaxLength="15" runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="OnlineInterviewSession_interviewNameLabel" runat="server" Text="Interview&nbsp;Name"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 18%">
                                                                                        <asp:TextBox ID="OnlineInterviewSession_interviewNameTextBox" MaxLength="50"  runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="OnlineInterviewSession_interviewCreatorHeadLabel" runat="server" Text="Interview Author"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                            <asp:TextBox ID="OnlineInterviewSession_interviewCreatorTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                                            <asp:HiddenField ID="OnlineInterviewSession_interviewCreatorIDHiddenField" runat="server" />
                                                                                            <asp:HiddenField ID="OnlineInterviewSession_interviewCreatorHiddenField" runat="server" />
                                                                                        </div>
                                                                                        <div style="float: left;">
                                                                                            &nbsp;<asp:ImageButton ID="OnlineInterviewSession_interviewCreatorImageButton" SkinID="sknbtnSearchicon"
                                                                                                runat="server" ImageAlign="Middle" />
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="OnlineInterviewSession_positionProfileHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Position Profile"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                            <asp:TextBox ID="OnlineInterviewSession_positionProfileTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                                            <asp:HiddenField ID="OnlineInterviewSession_positionProfileIDHiddenField" runat="server" />
                                                                                        </div>
                                                                                        <div style="float: left;">
                                                                                            &nbsp;<asp:ImageButton ID="OnlineInterviewSession_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                runat="server" ImageAlign="Middle" /></div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 4px">
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="OnlineInterviewSession_searchButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Search" OnClick="OnlineInterviewSession_searchButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 6px">
                        </td>
                    </tr>
                    <tr id="OnlineInterviewSession_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="OnlineInterviewSession_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                        &nbsp;<asp:Label ID="OnlineInterviewSession_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="OnlineInterviewSession_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="OnlineInterviewSession_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="OnlineInterviewSession_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="OnlineInterviewSession_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="OnlineInterviewSession_restoreHiddenField" runat="server" />
                                        <asp:HiddenField ID="OnlineInterviewSession_isMaximizedHiddenField" runat="server"
                                            Value="N" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <asp:UpdatePanel ID="OnlineInterviewSession_resultsGridViewUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="OnlineInterviewSession_interviewDetailGridViewDiv">
                                                    <asp:GridView ID="OnlineInterviewSession_resultsGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" OnSorting="OnlineInterviewSession_resultsGridView_Sorting"
                                                        OnRowCommand="OnlineInterviewSession_resultsGridView_RowCommand" OnRowCreated="OnlineInterviewSession_resultsGridView_RowCreated"
                                                        OnRowDataBound="OnlineInterviewSession_resultsGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="OnlineInterviewSession_editInterviewImageButton" runat="server"
                                                                        SkinID="sknEditSessionImageButton" ToolTip="Edit Interview" CommandName="EditInterview"
                                                                        CommandArgument='<%# Eval("Interviewkey") %>' Visible="true" />
                                                                    <asp:ImageButton ID="OnlineInterviewSession_copyInterviewImageButton" runat="server" SkinID="sknCopyImageButton"
                                                                        ToolTip="Copy Interview" CommandName="CopyInterview" CommandArgument='<%# Eval("Interviewkey") %>'
                                                                        Visible="true" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField SortExpression="INTERVIEWKEY" HeaderText="Interview Key">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OnlineInterviewSession_interviewKeyLabel" runat="server" Text='<%# Eval("Interviewkey") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField SortExpression="INTERVIEW_NAME" HeaderText="Interview&nbsp;Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OnlineInterviewSession_interviewNameNameLabel" runat="server" Text='<%# TrimContent(Eval("InterviewName").ToString(),20) %>'
                                                                        ToolTip='<%# Eval("InterviewName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Skill Detail">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OnlineInterviewSession_interviewSkill" runat="server" ToolTip='<%# Eval("Skills") %>'
                                                                        Text='<%# Eval("Skills") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Right" SortExpression="NUMBEROFQUESTIONS"
                                                                HeaderStyle-CssClass="td_padding_right_15" HeaderText="No of Questions" ItemStyle-CssClass="td_padding_right_15">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OnlineInterviewSession_noOfQuestionsLabel" runat="server" Text='<%# Eval("TotalQuestion") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField SortExpression="INTERVIEWAUTHOR" HeaderText="Interview Author Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OnlineInterviewSession_interviewAuthor" runat="server" ToolTip='<%# Eval("InterviewAuthorName") %>'
                                                                        Text='<%# Eval("InterviewAuthorName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField SortExpression="CREATEDDATE" HeaderText="Created Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OnlineInterviewSession_createdDateLabel" runat="server" Text='<%# Convert.ToDateTime(Eval("CreatedDate")).ToString("MM/dd/yyyy") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100%">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td align="right" style="width: 50%">
                                                            <uc3:PageNavigator ID="OnlineInterviewSession_pagingNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="OnlineInterviewSession_searchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="OnlineInterviewSession_copyInterviewUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="float: left;">
                            <asp:Panel ID="OnlineInterviewSession_interviewSaveAsPanel" runat="server" Style="display: none;
                                height: 250px;" CssClass="popupcontrol_interview_saveas">
                                <div id="OnlineInterviewSession_interviewSaveAsDiv" style="display: none">
                                    <asp:Button ID="OnlineInterviewSession_interviewSaveAs_hiddenButton" runat="server" />
                                </div>
                                <uc1:InterviewSaveAs ID="OnlineInterviewSession_interviewSaveAsControl" runat="server"
                                    Title="Create a Copy" Type="InterviewScoreConfirmType" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="OnlineInterviewSession_interviewSaveAs_ModalPopupExtender"
                                BehaviorID="OnlineInterviewSession_interviewSaveAs_ModalPopupExtender" runat="server"
                                PopupControlID="OnlineInterviewSession_interviewSaveAsPanel" TargetControlID="OnlineInterviewSession_interviewSaveAs_hiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="OnlineInterviewSession_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="OnlineInterviewSession_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewSession_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="OnlineInterviewSession_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            &nbsp;<asp:LinkButton ID="OnlineInterviewSession_bottomResetLinkButton" runat="server"
                                Text="Reset" SkinID="sknActionLinkButton" OnClick="OnlineInterviewSession_resetLinkButton_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="OnlineInterviewSession_bottomCancelLinkButton"
                                runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="OnlineInterviewSession_cancelLinkButton_Click"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
