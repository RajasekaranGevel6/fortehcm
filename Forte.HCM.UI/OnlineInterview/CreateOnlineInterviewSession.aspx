<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateOnlineInterviewSession.aspx.cs"
    MasterPageFile="~/MasterPages/InterviewMaster.Master" Inherits="Forte.HCM.UI.OnlineInterview.CreateOnlineInterviewSession" %>
<%@ Register Src="~/CommonControls/CandidateDetailControl.ascx" TagName="CandidateDetail"
    TagPrefix="uc5" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/OnlineInterviewSessionPreviewControl.ascx" TagName="OnlineInterviewSessionPreviewControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="CreateOnlineInterviewSession_bodyContent" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <script type="text/javascript">
        function ClearPositionProfile() {
            document.getElementById("<%= CreateOnlineInterviewSession_positionProfileTextBox.ClientID%>").value = '';
            document.getElementById("<%= CreateOnlineInterviewSession_positionProfileIDHiddenField.ClientID%>").value = '';

            return false;
        }
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" class="header_text_bold">
                            <asp:Literal ID="CreateOnlineInterviewSession_headerLiteral" runat="server" Text="Create Online Interview Session"></asp:Literal>
                        </td>
                        <td width="50%" align="right">
                            <asp:UpdatePanel ID="CreateOnlineInterviewSession_topButtonsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="4">
                                        <tr>
                                            <td>
                                                <asp:Button ID="CreateOnlineInterviewSession_topScheduleCandidateButton" runat="server"
                                                    Text="Schedule Candidate" SkinID="sknButtonId" Visible="false" 
                                                    OnClick="CreateOnlineInterviewSession_scheduleCandidateButton_Click" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="CreateOnlineInterviewSession_emailImageButton" runat="server"
                                                    ToolTip="Click here to email the online interview sessions" SkinID="sknMailImageButton" Visible="false" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreateOnlineInterviewSession_topResetLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateOnlineInterviewSession_resetButton_Click" />
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreateOnlineInterviewSession_topCancelLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateOnlineInterviewSession_topSuccessErrorMsgUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateOnlineInterviewSession_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateOnlineInterviewSession_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="non_tab_body_bg">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="panel_bg">
                            <asp:UpdatePanel ID="CreateOnlineInterviewSession_sessionDetailsUpdatePanel"
                                runat="server">
                                <ContentTemplate>
                                    <div id="CreateOnlineInterviewSession_sessionDetailsDIV" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateOnlineInterviewSession_sessionNameHeadLabel" runat="server"
                                                                                Text="Session Name" SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CreateOnlineInterviewSession_sessionNameTextBox" runat="server"
                                                                                MaxLength="100" AutoCompleteType="None" Columns="50" TabIndex="1"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="CreateOnlineInterviewSession_positionProfileLabel" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Position Profile"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="CreateOnlineInterviewSession_positionProfileTextBox" MaxLength="50"
                                                                                    ReadOnly="true" Columns="50" runat="server" TabIndex="2">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left; height: 16px; padding-right: 4px">
                                                                                <asp:ImageButton ID="CreateOnlineInterviewSession_positionProfileImageButton"
                                                                                    SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" 
                                                                                    ToolTip="Click here to select position profile to associate with online interview session" TabIndex="3" />
                                                                                <asp:ImageButton ID="CreateOnlineInterviewSession_positionProfileHelpImageButton"
                                                                                    SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                                    ToolTip="Click here to select position profile to associate with online interview session" TabIndex="4" />
                                                                            </div>
                                                                            <div style="padding-top: 2px; padding-left: 10px">
                                                                                <asp:LinkButton ID="CreateOnlineInterviewSession_clearPositionProfileLinkButton"
                                                                                    runat="server" Text="Clear" SkinID="sknActionLinkButton" 
                                                                                    OnClientClick="javascript:return ClearPositionProfile()"
                                                                                    ToolTip="Click here to clear the 'Position Profile' field" TabIndex="5" />
                                                                            </div>
                                                                            <asp:HiddenField ID="CreateOnlineInterviewSession_positionProfileIDHiddenField"
                                                                                runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateOnlineInterviewSession_skillHeadLabel" runat="server"
                                                                                Text="Skills/Areas" SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <asp:TextBox ID="CreateOnlineInterviewSession_skillTextBox" runat="server" 
                                                                                Width="350px" Columns="70"></asp:TextBox>
                                                                                <asp:ImageButton ID="CreateOnlineInterviewSession_NewSkillButton" runat="server"
                                                                                ImageAlign="AbsMiddle" SkinID="sknAddVectorGroupImageButton"
                                                                                Style="margin-left: 0px" ToolTip="Click here to add the entered skill"
                                                                                Width="16px" onclick="CreateOnlineInterviewSession_NewSkillButton_Click" />&nbsp;&nbsp;
                                                                                 <asp:ImageButton ID="CreateOnlineInterviewSession_SearchSkillButton" runat="server"
                                                                                ImageAlign="AbsMiddle"  SkinID="sknbtnSearchIcon"
                                                                                Style="margin-left: 0px" ToolTip="Click here to search & add skill"
                                                                                Width="16px" />&nbsp;&nbsp;
                                                                            <asp:ImageButton ID="EnrollAssessor_newAssessorSkillsImageButton" runat="server"
                                                                                ImageAlign="AbsMiddle" OnClientClick="javascript:return false;" SkinID="sknHelpImageButton"
                                                                                Style="margin-left: 0px" ToolTip="Enter multiple skills with comma separated and click the add button"
                                                                                Width="16px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:UpdatePanel ID="CreateOnlineInterviewSession_skillGridViewUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:GridView ID="CreateOnlineInterviewSession_skillGridView" runat="server"
                                                                                        AutoGenerateColumns="False"
                                                                                        EnableModelValidation="True" 
                                                                                        OnRowDeleting="CreateOnlineInterviewSession_skillGridView_RowDeleting">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="Category" HeaderText="Category" ItemStyle-Width="100px" />
                                                                                            <asp:BoundField DataField="Skill" HeaderText="Skill" ItemStyle-Width="150px" />
                                                                                            <asp:CommandField ButtonType="Image" DeleteImageUrl="~/Images/delete.png" ShowDeleteButton="True"
                                                                                                ItemStyle-Width="50px" />
                                                                                            <asp:TemplateField>
                                                                                                     <ItemTemplate>
                                                                                                        <asp:HiddenField ID="CreateOnlineInterviewSession_skillGridView_skillIDHiddenField" 
                                                                                                        runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SkillID")%>'  />
                                                                                                    </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                    <asp:HiddenField ID="CreateOnlineInterviewSession_CaegoryIDHiddenField" runat="server" />
                                                                                    <asp:HiddenField ID="CreateOnlineInterviewSession_SkillIDHiddenField" runat="server" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="CreateOnlineInterviewSession_sessionDescHeadLabel" runat="server"
                                                                    Text="Session Description" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="CreateOnlineInterviewSession_instructionsHeadLabel" runat="server"
                                                                    Text="Interview Instructions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="CreateOnlineInterviewSession_sessionDescTextBox" runat="server"
                                                                    TextMode="MultiLine" SkinID="sknMultiLineTextBox" MaxLength="8000" Width="466px"
                                                                    Height="80px" onkeyup="CommentsCount(8000,this)" onchange="CommentsCount(8000,this)"
                                                                    TabIndex="8"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="CreateOnlineInterviewSession_instructionsTextBox" runat="server"
                                                                    TextMode="MultiLine" MaxLength="8000" Width="466px" Height="80px" onkeyup="CommentsCount(8000,this)"
                                                                    onchange="CommentsCount(8000,this)" TabIndex="9"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_bg_testsession">
                                                    Settings
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="non_tab_body_bg">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="2" cellspacing="3" width="100%">
                                                                    <tr>
                                                                        <td style="width: 40%;">
                                                                            <asp:Label ID="CreateOnlineInterviewSession_sessionNoHeadLabel" runat="server"
                                                                                Text="Number Of Sessions" SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="CreateOnlineInterviewSession_sessionNoTextBox" MaxLength="2"
                                                                                            runat="server" Width="30px" TabIndex="10"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="vertical-align: bottom">
                                                                                                    <asp:ImageButton ID="CreateOnlineInterviewSession_upImageButton" runat="server"
                                                                                                        ImageAlign="AbsBottom" SkinID="sknNumericUpArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="vertical-align: top">
                                                                                                    <asp:ImageButton ID="CreateOnlineInterviewSession_downImageButton" runat="server"
                                                                                                        ImageAlign="Top" SkinID="sknNumericDownArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:NumericUpDownExtender ID="CreateOnlineInterviewSession_NumericUpDownExtender"
                                                                                Width="60" runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="CreateOnlineInterviewSession_sessionNoTextBox"
                                                                                TargetButtonUpID="CreateOnlineInterviewSession_upImageButton" TargetButtonDownID="CreateOnlineInterviewSession_downImageButton">
                                                                            </ajaxToolKit:NumericUpDownExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateOnlineInterviewSession_expiryDateHeadLabel" runat="server"
                                                                                Text="Expiry Date" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 40%">
                                                                                        <asp:TextBox ID="CreateOnlineInterviewSession_expiryDateTextBox" runat="server"
                                                                                            MaxLength="10" AutoCompleteType="None" Columns="15" TabIndex="11"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="CreateOnlineInterviewSession_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:MaskedEditExtender ID="CreateOnlineInterviewSession_MaskedEditExtender"
                                                                                runat="server" TargetControlID="CreateOnlineInterviewSession_expiryDateTextBox"
                                                                                Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="CreateOnlineInterviewSession_MaskedEditValidator"
                                                                                runat="server" ControlExtender="CreateOnlineInterviewSession_MaskedEditExtender"
                                                                                ControlToValidate="CreateOnlineInterviewSession_expiryDateTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="CreateOnlineInterviewSession_customCalendarExtender"
                                                                                runat="server" TargetControlID="CreateOnlineInterviewSession_expiryDateTextBox"
                                                                                CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="CreateOnlineInterviewSession_calendarImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="td_v_line">
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table width="100%" cellpadding="2" cellspacing="3" border="0" height="100px">
                                                                    <tr id="TrAssessor" runat="server">
                                                                        <td align="right" valign="top" style="height: 23px">
                                                                            <asp:UpdatePanel ID="CreateOnlineInterviewSession_assessorLinkUpdatePanel"
                                                                                runat="server">
                                                                            <ContentTemplate>
                                                                            <div style="display:none">
                                                                                <asp:Button ID="CreateOnlineInterviewSession_refreshGridButton" runat="server" Text="Refresh" 
                                                                                SkinID="sknButtonId" OnClick="CreateOnlineInterviewSession_refreshGridButton_Click" />
                                                                            </div>
                                                                            <span class="mandatory">&nbsp;*</span>
                                                                            <asp:LinkButton ID="CreateOnlineInterviewSession_recommendAssessorLinkButton" runat="server"
                                                                                SkinID="sknAddLinkButton" Text="Search and Add Assessors" 
                                                                                ToolTip="Search and Add Assessors" onclick="CreateOnlineInterviewSession_recommendAssessorLinkButton_Click" 
                                                                                ></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="CreateOnlineInterviewSession_assessorDetailsTr" style="width: 100%" runat="server">
                                                                        <td id="Td2" class="tab_body_bg" runat="server">
                                                                            <asp:UpdatePanel ID="CreateOnlineInterviewSession_assessorDetailsupdatePanel"
                                                                                runat="server">
                                                                                <ContentTemplate>
                                                                                    <div style="width: 100%; overflow: auto; height: 80px;" runat="server" id="CreateOnlineInterviewSession_assessorDetailsDiv"
                                                                                        visible="False">
                                                                                        <asp:GridView ID="CreateOnlineInterviewSession_assessorDetailsGridView" runat="server"
                                                                                            OnRowCommand="CreateOnlineInterviewSession_addAssessorGridView_RowCommand"
                                                                                            AutoGenerateColumns="True" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                            Width="100%">
                                                                                            <RowStyle CssClass="grid_alternate_row" />
                                                                                            <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                            <HeaderStyle CssClass="grid_header_row" />
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="Name">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateOnlineInterviewSession_assessorDetailsFirstNameLabel" runat="server"
                                                                                                            Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Email">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateOnlineInterviewSession_assessorDetailsEmailLabel" runat="server"
                                                                                                            Text='<%# Eval("UserEmail") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="td_padding_top_5">
                                                    <asp:Button ID="CreateOnlineInterviewSession_topSaveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="CreateOnlineInterviewSession_saveButton_Click"
                                                        TabIndex="13" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td >
                                        <asp:UpdatePanel ID="CreateOnlineInterviewSession_cancelTestPopupUpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="CreateOnlineInterviewSession_viewTestSessionSavePanel" runat="server"
                                                    Style="display: none" CssClass="popupcontrol_question_detail">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateOnlineInterviewSession_viewTestSessionSaveButton" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                               <uc2:OnlineInterviewSessionPreviewControl ID="CreateOnlineInterviewSession_viewTestSessionSave_UserControl"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table border="0" cellspacing="3" cellpadding="3" align="left">
                                                                    <tr>
                                                                        <td class="td_padding_top_5" style="padding-left: 10px">
                                                                            <asp:Button ID="CreateOnlineInterviewSession_previewTestSessionControl_createButton"
                                                                                runat="server" Text="Create" OnClick="CreateOnlineInterviewSession_previewTestSessionControl_createButton_Click"
                                                                                SkinID="sknButtonId" />
                                                                        </td>
                                                                        <td class="td_padding_top_5">
                                                                            <asp:LinkButton ID="CreateOnlineInterviewSession_previewTestSessionControl_cancelButton"
                                                                                runat="server" Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateOnlineInterviewSession_viewTestSessionSave_modalpPopupExtender"
                                                    runat="server" TargetControlID="CreateOnlineInterviewSession_viewTestSessionSaveButton"
                                                    PopupControlID="CreateOnlineInterviewSession_viewTestSessionSavePanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                               
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateOnlineInterviewSession_bottomSuccessErrorMsgUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateOnlineInterviewSession_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateOnlineInterviewSession_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="CreateOnlineInterviewSession_TestKeyHiddenField" runat="server" />
    <asp:HiddenField ID="CreateOnlineInterviewSession_candidateSessionIDsHiddenField"
        runat="server" />
    <asp:HiddenField ID="CreateOnlineInterviewSession_sessionIDHiddenField" runat="server" />
</asp:Content>
