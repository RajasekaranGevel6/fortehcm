﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewRequestStatus.aspx.cs"
    Inherits="Forte.HCM.UI.OnlineInterview.ViewRequestStatus" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<asp:Content ID="ViewRequestStatus_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <script type="text/javascript" language="javascript">

        // Hide all the expanded panel.
        function CollapseAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= ViewRequestStatus_timeSlotsGridViewDiv.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("ViewRequestStatus_timeSlotsGridView_detailsDiv") != "-1")
                    {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }

            return false;
        }


    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ViewRequestStatus_headerLiteral" runat="server" Text="View Time Slot Request Status"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="ViewRequestStatus_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="ViewRequestStatus_resetLinkButton_Click"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="ViewRequestStatus_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ViewRequestStatus_cancelLinkButton_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="ViewRequestStatus_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="ViewRequestStatus_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ViewRequestStatus_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ViewRequestStatus_searchButton" />
                        <asp:AsyncPostBackTrigger ControlID="ViewRequestStatus_availabilityRequestButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="ViewRequestStatus_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <asp:UpdatePanel runat="server" ID="ViewRequestStatus_criteriaUpdatePanel">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td style="width: 9%">
                                                                            <asp:Label ID="ViewRequestStatus_statusLabel" runat="server" Text="Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 24%">
                                                                            <div style="float: left; padding-right: 1px">
                                                                                <asp:DropDownList ID="ViewRequestStatus_statusDropDownList" runat="server" Width="176px">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ViewRequestStatus_requestStatusHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the status here"
                                                                                    Width="16px" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 9%">
                                                                            <asp:Label ID="ViewRequestStatus__initatedByLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Initiated By"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 24%">
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="ViewRequestStatus_initiatedByTextBox" MaxLength="50" runat="server" ReadOnly="true"
                                                                                    Width="170px"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ViewRequestStatus_initiatedByImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the initiator" />
                                                                                <asp:HiddenField ID="ViewRequestStatus_initiatedByHiddenField" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 9%">
                                                                            <asp:Label ID="ViewRequestStatus_initiatedDateLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Initiated Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 24%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ViewRequestStatus_initiatedDateTextBox" runat="server" Width="170px"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left">
                                                                                <asp:ImageButton ID="ViewRequestStatus_initiatedDateImageButton" SkinID="sknCalendarImageButton"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the initiated date" />
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="ViewRequestStatus_initiatedDateMaskedEditExtender"
                                                                                runat="server" TargetControlID="ViewRequestStatus_initiatedDateTextBox" Mask="99/99/9999"
                                                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="ViewRequestStatus_initiatedDateMaskedEditValidator"
                                                                                runat="server" ControlExtender="ViewRequestStatus_initiatedDateMaskedEditExtender"
                                                                                ControlToValidate="ViewRequestStatus_initiatedDateTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="ViewRequestStatus_initiatedDateCalendarExtender"
                                                                                runat="server" TargetControlID="ViewRequestStatus_initiatedDateTextBox" CssClass="MyCalendar"
                                                                                Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="ViewRequestStatus_initiatedDateImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height:8px" colspan="8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ViewRequestStatus_sessionLabel" runat="server" Text="Session" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 2px">
                                                                                <asp:TextBox ID="ViewRequestStatus_sessionTextBox" ReadOnly="true" runat="server"
                                                                                    Width="170px"></asp:TextBox>
                                                                            </div>
                                                                            &nbsp;
                                                                            <div style="float: left">
                                                                                <asp:ImageButton ID="ViewRequestStatus_sessionImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select online interview session" />
                                                                            </div>
                                                                            <asp:HiddenField ID="ViewRequestStatus_sessionKeyHiddenField" runat="server" />
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <asp:Label ID="ViewRequestStatus_assessorLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Assessor"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="ViewRequestStatus_assessorTextBox" MaxLength="50" runat="server" ReadOnly="true"
                                                                                    Width="170px"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ViewRequestStatus_assessorImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the assessor" />
                                                                                <asp:HiddenField ID="ViewRequestStatus_assessorIDHiddenField" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ViewRequestStatus_requestedDateLabel" runat="server" 
                                                                                SkinID="sknLabelFieldHeaderText" Text="Requested Date"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ViewRequestStatus_requestedDateTextBox" runat="server" Width="170px"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left">
                                                                                <asp:ImageButton ID="ViewRequestStatus_requestedDateImageButton" SkinID="sknCalendarImageButton"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the requested date" />
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="ViewRequestStatus_requestedDateMaskedEditExtender"
                                                                                runat="server" TargetControlID="ViewRequestStatus_requestedDateTextBox" Mask="99/99/9999"
                                                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="ViewRequestStatus_requestedDateMaskedEditValidator"
                                                                                runat="server" ControlExtender="ViewRequestStatus_requestedDateMaskedEditExtender"
                                                                                ControlToValidate="ViewRequestStatus_requestedDateTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="ViewRequestStatus_requestedDateCustomCalendarExtender"
                                                                                runat="server" TargetControlID="ViewRequestStatus_requestedDateTextBox" CssClass="MyCalendar"
                                                                                Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="ViewRequestStatus_requestedDateImageButton" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 8px" colspan="8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ViewRequestStatus_skillLabel" runat="server" 
                                                                                SkinID="sknLabelFieldHeaderText" Text="Skill"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="ViewRequestStatus_skillTextBox" runat="server" MaxLength="50" 
                                                                                Width="170px"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ViewRequestStatus_commentsLabel" runat="server" 
                                                                                SkinID="sknLabelFieldHeaderText" Text="Comments"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3" style="width: 90%">
                                                                            <asp:TextBox ID="ViewRequestStatus_commentsTextBox" runat="server" Width="486px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 4px">
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <div style="display: none">
                                                                    <asp:Button ID="ViewRequestStatus_availabilityRequestButton" runat="server" OnClick="ViewRequestStatus_availabilityRequestButton_Click" />
                                                                </div>
                                                                <asp:Button ID="ViewRequestStatus_searchButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Search" OnClick="ViewRequestStatus_searchButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 6px">
                        </td>
                    </tr>
                    <tr id="ViewRequestStatus_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="ViewRequestStatus_requestsResultsLiteral" runat="server" Text="Time Slots"></asp:Literal>
                                        &nbsp;<asp:Label ID="ViewRequestStatus_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="ViewRequestStatus_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="ViewRequestStatus_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="ViewRequestStatus_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="ViewRequestStatus_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="ViewRequestStatus_restoreHiddenField" runat="server" />
                                        <asp:HiddenField ID="ViewRequestStatus_isMaximizedHiddenField" runat="server" Value="N" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <asp:UpdatePanel ID="ViewRequestStatus_assessmentDetailsGridViewUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="ViewRequestStatus_timeSlotsGridViewDiv">
                                                    <asp:GridView ID="ViewRequestStatus_timeSlotsGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" OnSorting="ViewRequestStatus_timeSlotsGridView_Sorting"
                                                        OnRowCommand="ViewRequestStatus_timeSlotsGridView_RowCommand" OnRowCreated="ViewRequestStatus_timeSlotsGridView_RowCreated"
                                                        OnRowDataBound="ViewRequestStatus_timeSlotsGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="28px">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ViewRequestStatus_timeSlotsGridView_requestTimeSlotImageButton"
                                                                        runat="server" SkinID="sknRequestTimeSlotImageButton" ToolTip="Request Time Slot"
                                                                        CommandName="Request" CommandArgument="<%# Container.DataItemIndex %>" CssClass="showCursor" />
                                                                    <asp:ImageButton ID="ViewRequestStatus_timeSlotsGridView_emailAssessorImageButton" runat="server" ToolTip="Email Assessor"
                                                                        SkinID="sknMailImageButton"/>
                                                                    <asp:HiddenField ID="ViewRequestStatus_timeSlotsGridView_skillIDHiddenField" Value='<%# Eval("SkillID") %>'
                                                                        runat="server" />
                                                                    <asp:HiddenField ID="ViewRequestStatus_timeSlotsGridView_assessorIDHiddenField" Value='<%# Eval("AssessorID") %>'
                                                                        runat="server" />
                                                                    <asp:HiddenField ID="ViewRequestStatus_timeSlotsGridView_sessionKeyHiddenField" Value='<%# Eval("SessionKey") %>'
                                                                        runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Skill" SortExpression="SKILL">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="ViewRequestStatus_timeSlotsGridView_skillsLinkButton" runat="server" ToolTip="Click here to show/hide expanded view"
                                                                        Text='<%# Eval("Skill") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Requested Date" SortExpression="REQUESTED_DATE DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewRequestStatus_timeSlotsGridView_requestedDateLabel" runat="server"
                                                                        Text='<%# Eval("AvailabilityDateString") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Requested Time Slot" SortExpression="TIME_SLOT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewRequestStatus_timeSlotsGridView_timeSlotLabel" runat="server"
                                                                        Text='<%# Eval("TimeSlot") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Assessor" SortExpression="ASSESSOR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewRequestStatus_timeSlotsGridView_assessorLabel" runat="server"
                                                                        Text='<%# Eval("Assessor") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Initiated Date" SortExpression="INITIATED_DATE DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewRequestStatus_timeSlotsGridView_initiatedDateLabel" runat="server"
                                                                        Text='<%# Eval("InitiatedDateString") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" SortExpression="REQUEST_STATUS DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewRequestStatus_timeSlotsGridView_requestStausLabel" runat="server"
                                                                        Text='<%# Eval("RequestStatusName") %>'></asp:Label>
                                                                    <tr>
                                                                        <td class="grid_padding_right" colspan="7" style="width: 100%">
                                                                            <div id="ViewRequestStatus_timeSlotsGridView_detailsDiv" runat="server" style="display: none;"
                                                                                class="table_outline_bg_search_time_slot_request">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td style="width: 10%; height: 20px" align="left" valign="top">
                                                                                            <asp:Label ID="MyAvailability_timeSlotsGridView_sessionDescLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Session"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 89%" colspan="3" align="left" valign="top">
                                                                                            <div style="height: 56px; overflow: auto;">
                                                                                                <asp:Label ID="MyAvailability_timeSlotsGridView_sessionDescValueLabel" runat="server"
                                                                                                    SkinID="sknLabelFieldText" Text='<%# Eval("SessionDesc") %>'></asp:Label>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="height: 6px">
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%; height: 20px" align="left" valign="top">
                                                                                            <asp:Label ID="MyAvailability_timeSlotsGridView_commentsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Comments"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 89%" colspan="3" align="left" valign="top">
                                                                                            <div style="height: 44px; overflow: auto;">
                                                                                                <asp:Label ID="MyAvailability_timeSlotsGridView_commentsValueLabel" runat="server"
                                                                                                    SkinID="sknLabelFieldText" Text='<%# Eval("Comments") %>'></asp:Label>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <%-- popup DIV --%>
                                                                            <a href="#ViewRequestStatus_timeSlotsGridView_detailsViewFocusMeLink" id="ViewRequestStatus_timeSlotsGridView_detailsViewFocusDownLink"
                                                                                runat="server"></a><a href="#" id="ViewRequestStatus_timeSlotsGridView_detailsViewFocusMeLink"
                                                                                    runat="server"></a>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100%">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td align="right" style="width: 50%">
                                                            <uc3:PageNavigator ID="ViewRequestStatus_pagingNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ViewRequestStatus_searchButton" />
                                    <asp:AsyncPostBackTrigger ControlID="ViewRequestStatus_availabilityRequestButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="ViewRequestStatus_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="ViewRequestStatus_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ViewRequestStatus_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ViewRequestStatus_searchButton" />
                        <asp:AsyncPostBackTrigger ControlID="ViewRequestStatus_availabilityRequestButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            &nbsp;<asp:LinkButton ID="ViewRequestStatus_bottomResetLinkButton" runat="server"
                                Text="Reset" SkinID="sknActionLinkButton" OnClick="ViewRequestStatus_resetLinkButton_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="ViewRequestStatus_bottomCancelLinkButton"
                                runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="ViewRequestStatus_cancelLinkButton_Click"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
