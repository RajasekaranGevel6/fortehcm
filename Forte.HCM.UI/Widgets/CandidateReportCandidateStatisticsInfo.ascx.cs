﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateReportCandidateStatisticsInfo.aspx.cs
// File that represents the Candidate Report CandidateStatistics Info based on the 
// selected candidate and category.

#endregion

#region Directives                                                             
using System;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;
#endregion Directives

namespace Forte.HCM.UI.Widgets
{
    public partial class CandidateReportCandidateStatisticsInfo : UserControl, IWidgetControl
    {
        List<DropDownItem> dropDownItemList = null;

        #region Event Handler                                                  

        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl.Visible = false;
                LoadGrid(ViewState["instance"] as WidgetInstance);
            }
        }
       
        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CandidateReportCandidateStatisticsInfo_selectProperty(object sender, EventArgs e)
        {
            try
            {
                CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl.WidgetTypePropertyAllSelected(false);
                dropDownItemList = new List<DropDownItem>();
                dropDownItemList.Add(new DropDownItem(CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl.SelectedProperties.ToString(), "ComparisonCandidateList"));
                WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
                WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
                WidgetMultiSelectControlPrint.WidgetTypePropertyDataSource = dropDownItemList;
                WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(true);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on the select all clear all link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CandidateReportCandidateStatisticsInfo_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                SellectAll(true);
                // LoadGrid(ViewState["instance"] as WidgetInstance, int.Parse(ViewState["PAGE_NUMBER"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on the select all clear all link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CandidateReportCandidateStatisticsInfo_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                SellectAll(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CandidateStatisticsDetail> candidateStatisticsDetails
        {
            set
            {
                List<CandidateStatisticsDetail> candidateStatisticsPrintDetails = new List<CandidateStatisticsDetail>();
                List<DropDownItem> testInfoDropDownItemList = null;
                CandidateStatisticsDetail candidateStatisticsDetail = null;
                if ((bool)Session["PRINTER"])
                {
                    string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split(',');
                    for (int i = 0; i < value.Count; i++)
                    {
                        testInfoDropDownItemList = new List<DropDownItem>();
                        candidateStatisticsDetail = new CandidateStatisticsDetail();
                        foreach (string word in selectedProperty)
                        {
                            switch (word)
                            {
                                case "Answered Correctly":
                                    testInfoDropDownItemList.Add(new DropDownItem("Answered Correctly", value[i].AnsweredCorrectly.ToString()));
                                    break;
                                case "Absolute Score":
                                    testInfoDropDownItemList.Add(new DropDownItem("Absolute Score", value[i].AbsoluteScore.ToString()));
                                    break;
                                case "Relative Score":
                                    testInfoDropDownItemList.Add(new DropDownItem("Relative Score", value[i].RelativeScore.ToString()));
                                    break;
                                case "Percentile":
                                    testInfoDropDownItemList.Add(new DropDownItem("Percentile", value[i].Percentile.ToString()));
                                    break;
                                case "First Name":
                                    {
                                        if (!Utility.IsNullOrEmpty(value[i].FirstName))
                                            candidateStatisticsDetail.FirstName = value[i].FirstName;
                                    }
                                    break;
                                case "Address":
                                    {
                                        if (!Utility.IsNullOrEmpty(value[i].Address))
                                            candidateStatisticsDetail.Address = value[i].Address;
                                    }
                                    break;
                                case "Phone":
                                    {
                                        if (!Utility.IsNullOrEmpty(value[i].Phone))
                                            candidateStatisticsDetail.Phone = value[i].Phone;
                                    }
                                    break;
                                case "Synopsis":
                                    {
                                        if (!Utility.IsNullOrEmpty(value[i].Synopsis))
                                            candidateStatisticsDetail.Synopsis = value[i].Synopsis;
                                    }
                                    break;
                            }
                        }
                        if (testInfoDropDownItemList.Count > 0)
                        {
                            candidateStatisticsDetail.TestDetails = testInfoDropDownItemList;
                           
                        }
                        candidateStatisticsPrintDetails.Add(candidateStatisticsDetail);
                    }
                }
                else
                {

                    for (int i = 0; i < value.Count; i++)
                    {
                        candidateStatisticsDetail = new CandidateStatisticsDetail();
                        testInfoDropDownItemList = new List<DropDownItem>();
                        testInfoDropDownItemList.Add(new DropDownItem("Answered Correctly", value[i].AnsweredCorrectly.ToString()));
                        testInfoDropDownItemList.Add(new DropDownItem("Absolute Score", value[i].AbsoluteScore.ToString()));
                        testInfoDropDownItemList.Add(new DropDownItem("Relative Score", value[i].RelativeScore.ToString()));
                        testInfoDropDownItemList.Add(new DropDownItem("Percentile", value[i].Percentile.ToString()));
                        
                        if (!Utility.IsNullOrEmpty(value[i].FirstName))
                            candidateStatisticsDetail.FirstName = value[i].FirstName;
                        if (!Utility.IsNullOrEmpty(value[i].Address))
                            candidateStatisticsDetail.Address = value[i].Address;
                        if (!Utility.IsNullOrEmpty(value[i].Phone))
                            candidateStatisticsDetail.Phone = value[i].Phone;
                        if (!Utility.IsNullOrEmpty(value[i].Synopsis))
                            candidateStatisticsDetail.Synopsis = value[i].Synopsis;

                        if (testInfoDropDownItemList.Count > 0)
                        {
                            candidateStatisticsDetail.TestDetails = testInfoDropDownItemList;
                            candidateStatisticsPrintDetails.Add(candidateStatisticsDetail);
                        }
                    }
                }

                CandidateReportCandidateStatisticsInfo_candidateDetailsDataList.DataSource = candidateStatisticsPrintDetails;
                CandidateReportCandidateStatisticsInfo_candidateDetailsDataList.DataBind();
                //CandidateStatisticsInfo_candidateDetailsDataList.DataSource = testInfoDropDownItemList;
                //CandidateStatisticsInfo_candidateDetailsDataList.DataBind();


            }
        }
        #endregion Event Handler

        #region Private Methods                                                


        /// <summary>
        /// Represents the method to load the grid 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pagenumber
        /// </param>
        private void LoadGrid(WidgetInstance instance)
        {
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            List<CandidateStatisticsDetail> candidateStatisticsDetail = null;
            candidateStatisticsDetail = new ReportBLManager().
                GetCandidateReportCandidateStatistics(candidateReportDetail);
            if (candidateStatisticsDetail == null || candidateStatisticsDetail.Count == 0)
            {
                CandidateReportCandidateStatisticsInfo_messageLabel.Text = "No data found to display";
                return;
            }
            candidateStatisticsDetails = candidateStatisticsDetail;
            //CandidateReportCandidateStatisticsInfo_candidateDetailsDataList.DataSource = candidateStatisticsDetail;
            //CandidateReportCandidateStatisticsInfo_candidateDetailsDataList.DataBind();
           // dropDownItemList = new ReflectionManager().GetListItems(candidateStatisticsDetail[0]);
            dropDownItemList = new List<DropDownItem>();
            dropDownItemList.Add(new DropDownItem("First Name",""));
            dropDownItemList.Add(new DropDownItem("Address", ""));
            dropDownItemList.Add(new DropDownItem("Phone", ""));
            dropDownItemList.Add(new DropDownItem("Synopsis", ""));
            dropDownItemList.Add(new DropDownItem("Answered Correctly", ""));
            dropDownItemList.Add(new DropDownItem("Absolute Score", ""));
            dropDownItemList.Add(new DropDownItem("Relative Score", ""));
            dropDownItemList.Add(new DropDownItem("Percentile", ""));
            CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
        }

        #endregion Private Methods

        #region IWidgetControl Members                                         

        /// <summary>
        /// Represents the method to bind the data source
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the grid 
            LoadGrid(instance);
            SellectAll(true);
        }
        private void SellectAll(bool IsSellectAll)
        {
            CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl.WidgetTypePropertyAllSelected(IsSellectAll);
            dropDownItemList = new List<DropDownItem>();
            dropDownItemList.Add(new DropDownItem(CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl.SelectedProperties.ToString(), "ComparisonCandidateList"));
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(true);
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo
            commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }
        #endregion IWidgetControl Members
    }
}