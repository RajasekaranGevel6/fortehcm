﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateReportCandidateStatisticsInfo.ascx.cs"
    Inherits="Forte.HCM.UI.Widgets.CandidateReportCandidateStatisticsInfo" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="CandidateReportCandidateStatisticsInfo_updatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="CandidateReportCandidateStatisticsInfo_messageLabel" runat="server"
                                        SkinID="sknErrorMessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="middle" style="height: 30px;">
                                                <div style="width: 100%; overflow: auto">
                                                    <uc2:WidgetMultiSelectControl ID="CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl"
                                                        runat="server" OnselectedIndexChanged="CandidateReportCandidateStatisticsInfo_selectProperty"
                                                        OnClick="CandidateReportCandidateStatisticsInfo_widgetMultiSelectControl_Click" 
                                                        OncancelClick="CandidateReportCandidateStatisticsInfo_widgetMultiSelectControl_CancelClick" />
                                                    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                                    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DataList ID="CandidateReportCandidateStatisticsInfo_candidateDetailsDataList"
                                                    runat="server" RepeatColumns="1" RepeatDirection="Vertical" Width="100%">
                                                    <ItemTemplate>
                                                        <table width="100%" cellpadding="3" cellspacing="3" border="0">
                                                            <tr>
                                                                <td>
                                                                    <div style="float: left">
                                                                        <asp:Label ID="CandidateReportCandidateStatisticsInfo_nameLiteral" runat="server"
                                                                            Text='<%# Eval("FirstName") %>' SkinID="sknLabelFieldWidgetHeaderTexts"></asp:Label></div>
                                                                    <div style="float: right">
                                                                        <asp:Label ID="CandidateReportCandidateStatisticsInfo_locationLiteral" runat="server"
                                                                            Text='<%# Eval("Address") %>' SkinID="sknLabelFieldWidgetHeaderText"></asp:Label></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:Label ID="CandidateReportCandidateStatisticsInfo_contactLiteral" runat="server"
                                                                        Text='<%# Eval("Phone") %>' SkinID="sknLabelFieldWidgetHeaderText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="label_multi_field_text_widget">
                                                                        <asp:Literal ID="CandidateReportCandidateStatisticsInfo_synopsisLiteral" runat="server"
                                                                            Text='<%#Eval("Synopsis") %>'></asp:Literal>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <asp:DataList ID="CandidateStatisticsInfo_candidateDetailsDataList" runat="server"
                                                                            DataSource='<%# Eval("TestDetails") %>' RepeatLayout="Table" RepeatColumns="2"
                                                                            RepeatDirection="Horizontal">
                                                                            <HeaderTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="100%" cellpadding="0" cellspacing="2" border="0">
                                                                                            <tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <td>
                                                                                    <asp:Label ID="CandidateStatisticsInfo_scoreSDHeadLabel" runat="server" Text='<%# Eval("DisplayText") %>'
                                                                                        SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:Label ID="CandidateStatisticsInfo_scoreSDValueLabel" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                        Text='<%# Eval("ValueText") %>'></asp:Label>
                                                                                </td>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </tr> </table> </td> </tr>
                                                                            </FooterTemplate>
                                                                        </asp:DataList>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="cand_bg_dark" />
                                                    <AlternatingItemStyle CssClass="cand_bg_light" />
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
