﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Widgets {
    
    
    public partial class CandidateReportCandidateStatisticsInfo {
        
        /// <summary>
        /// CandidateReportCandidateStatisticsInfo_updatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel CandidateReportCandidateStatisticsInfo_updatePanel;
        
        /// <summary>
        /// CandidateReportCandidateStatisticsInfo_messageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label CandidateReportCandidateStatisticsInfo_messageLabel;
        
        /// <summary>
        /// CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.WidgetMultiSelectControl CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl;
        
        /// <summary>
        /// WidgetMultiSelectControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.WidgetMultiSelectControl WidgetMultiSelectControl;
        
        /// <summary>
        /// WidgetMultiSelectControlPrint control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.WidgetMultiSelectControl WidgetMultiSelectControlPrint;
        
        /// <summary>
        /// CandidateReportCandidateStatisticsInfo_candidateDetailsDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList CandidateReportCandidateStatisticsInfo_candidateDetailsDataList;
    }
}
