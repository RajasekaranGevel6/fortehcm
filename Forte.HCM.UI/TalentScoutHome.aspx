﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/TalentScoutMaster.Master"
   CodeBehind="TalentScoutHome.aspx.cs" Inherits="Forte.HCM.UI.TalentScoutHome" %>

<%@ Register Assembly="System.Web.Silverlight" Namespace="System.Web.UI.SilverlightControls"
    TagPrefix="aspq" %>

    <%@ MasterType VirtualPath="~/MasterPages/TalentScoutMaster.Master" %>
<asp:Content ID="TalentScoutHome_bodyContent" runat="server" ContentPlaceHolderID="TalentScoutMaster_body">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td class="msg_align">
                <asp:Label ID="TalentScoutHome_topErrorMessageLabel" runat="server" EnableViewState="false"
                    SkinID="sknErrorMessage"></asp:Label>
                <asp:Label ID="TalentScoutHome_topSuccessMessageLabel" runat="server" EnableViewState="false"
                    SkinID="sknSuccessMessage"></asp:Label>
            </td>
        </tr>
    </table>
    <div style="width: 100%; height: 700px">
        <aspq:Silverlight ID="TalentScoutHome_tsSilverlight" runat="server" Width="100%" Height="100%" Source="~/Forte.HCM.TalentScoutSL/Forte.HCM.TalentScoutSL.xap">
        </aspq:Silverlight>
    </div>
</asp:Content>
