﻿#region Headerr

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PageBase.aspx.cs
// File that represents the PageBase class that defines the base page for 
// the aspx pages used in the application. This base page contains common
// data and functionality used across the pages. 

#endregion Header

#region Directives

using System;
using System.Linq;
using System.Web.UI;
using System.Configuration;
using System.Drawing.Imaging;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Common
{
    /// <summary>
    /// Class that defines the base page for the aspx pages used in the 
    /// application. This base page contains common data and functionality 
    /// used across the pages.
    /// </summary>
    public abstract partial class PageBase : Page
    {
        #region Protected Fields

        /// <summary>
        /// A <see cref="int"/> that holds the default number of choices to be
        /// displayed for question entry.
        /// </summary>
        protected int numberOfAnswerChoices = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the user ID.
        /// </summary>
        protected int userID = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the user full name.
        /// </summary>
        protected string userFullName = string.Empty;

        /// <summary>
        /// A <see cref="bool"/> that holds the legal accepted status.
        /// </summary>
        protected bool isLegalAccepted = false;

        /// <summary>
        /// A <see cref="int"/> that holds the tenant ID.
        /// </summary>
        protected int tenantID = 0;

        /// <summary>
        /// A <see cref="string"/> that holds the logged in user email ID.
        /// </summary>
        protected string userEmailID = string.Empty;

        /// <summary>
        /// A <see cref="bool"/> that holds the flag values which identifies
        /// whether the logged in user is admin or not.
        /// </summary>
        protected bool isAdmin = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the flag values which identifies
        /// whether the logged in user is site admin or not.
        /// </summary>
        protected bool isSiteAdmin = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the flag values which identifies
        /// whether the logged in user is customer admin or not.
        /// </summary>
        protected bool isCustomerAdmin = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the flag values which identifies
        /// whether the user logged or not.
        /// </summary>
        protected bool isLogged = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the bool value which identifies 
        /// whether the page is accessible to only Site Admin
        /// </summary>
        protected bool isPageAccessibleToSiteAdmin = false;

        /// <summary>
        /// A <see cref="int"/> that holds the min questions per self admin test.
        /// </summary>
        protected int minQuestionPerSelfAdminTest = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the amx questions per self admin test.
        /// </summary>
        protected int maxQuestionPerSelfAdminTest = 0;

        protected const string secretSalt = "R9%#@*MASRlk31q4l1n$^&*()auOIP...";

        private string featureURL;

        private string url;
        private string receivedObfuscated;
        protected string userId;
        private List<string> parameters;
        #endregion Protected Fields

        #region Protected Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the CSS class name
        /// for the mouse over style.
        /// </summary>
        protected const string MOUSE_OVER_STYLE = "className='grid_normal_row'";

        /// <summary>
        /// A <see cref="string"/> constant that holds the CSS class name
        /// for the mouse out style.
        /// </summary>
        protected const string MOUSE_OUT_STYLE = "className='grid_alternate_row'";

        #endregion Protected Constants

        #region Protected Abstract Methods

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected abstract bool IsValidData();

        /// <summary>
        /// Method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected abstract void LoadValues();

        #endregion Protected Abstract Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Represents the method that is called when the page is being initialized.
        /// </summary>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        /// 
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                // Get the tenant ID from session.
                //if (Session[Constants.SessionConstants.TENANT_ID] != null)
                //    tenantID = Convert.ToInt32(Session[Constants.SessionConstants.TENANT_ID]);

                if (Session["USER_DETAIL"] != null)
                {
                    isLogged = true;
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                    this.userID = userDetail.UserID;
                    this.userFullName = userDetail.FirstName + " " + userDetail.LastName;
                    this.isAdmin = userDetail.IsAdmin;
                    this.tenantID = userDetail.TenantID;
                    this.userEmailID = userDetail.Email;
                    this.isSiteAdmin = (userDetail.UserType == UserType.SiteAdmin ? true : false);
                    this.isLegalAccepted = userDetail.LegalAccepted;

                    // Check if user is customer admin or not.
                    if (userDetail.Roles != null && userDetail.Roles.Contains
                        (UserRole.CorporateSubscriptionAdmin))
                    {
                        isCustomerAdmin = true;
                    }
                }

                // If candidate admin settings is not present in session, get it
                // and keep it in session.
                if (Session["CANDIDATE_ADMIN_SETTINGS"] == null)
                {
                    // Keep the admin settings in session.
                    Session["CANDIDATE_ADMIN_SETTINGS"] = new AdminBLManager().
                        GetCandidateAdminSettings();
                }

                // Get candidate admin settings from session.
                CandidateAdminSettings candidateAdminSettings = Session
                    ["CANDIDATE_ADMIN_SETTINGS"] as CandidateAdminSettings;

                // Assign candidate settings to fields.
                this.minQuestionPerSelfAdminTest = candidateAdminSettings.MinQuestionPerSelfAdminTest;
                this.maxQuestionPerSelfAdminTest = candidateAdminSettings.MaxQuestionPerSelfAdminTest;

                // Check if the license agreement is accepted. If not proceed
                // the specific page.
                if (isLogged == true && isLegalAccepted == false &&
                    !Page.Request.RawUrl.ToUpper().Contains("LICENSEAGREEMENT.ASPX"))
                {
                    Response.Redirect("~/Subscription/LicenseAgreement.aspx", false);
                    return;
                }

                ConfigurePageDefaults();
                if (IsPostBack)
                    return;
                CheckAndUpdateReadOnlyTextBox(this.Page);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// This method checks and updates read only text boxes in UI
        /// </summary>
        /// <param name="Parent">
        /// A <see cref="Control"/> that contains the control to look for 
        /// textbox.
        /// </param>
        private void CheckAndUpdateReadOnlyTextBox(Control Parent)
        {
            foreach (Control Child in Parent.Controls)
            {
                if (Child.HasControls())
                    CheckAndUpdateReadOnlyTextBox(Child);
                else
                {
                    if (!(Child is TextBox))
                        continue;
                    if (!((TextBox)Child).ReadOnly)
                        continue;
                    ((TextBox)Child).ReadOnly = false;
                    ((TextBox)Child).Attributes.Add("readonly", "readonly");
                }
            }
        }

        private void ConfigurePageDefaults()
        {
            if (Utility.IsNullOrEmpty(featureURL))
            {
                featureURL = Request.AppRelativeCurrentExecutionFilePath.Replace('~', ' ').Trim().ToLower();
            }
            if (!IsIgnorePage())
            {
                AssignPermissions();
            }
        }

        private bool IsIgnorePage()
        {
            if (featureURL.Contains(@"/home.aspx") ||
                featureURL.Contains(@"/otmhome.aspx") ||
                featureURL.Contains(@"/interviewhome.aspx") ||
                featureURL.Contains(@"/default.aspx") ||
               // featureURL.Contains(@"/talentscouthome.aspx") ||                             
                featureURL.Contains(@"/workflow.aspx") ||
                featureURL.Contains(@"/landing.aspx") ||
                featureURL.Contains(@"/assessmentworkflow.aspx") ||
                featureURL.Contains(@"/interviewworkflow.aspx") ||
                featureURL.Contains(@"/subscriptiontype.aspx") ||
                featureURL.Contains(@"/captchaviewer.aspx") ||
                featureURL.Contains(@"/captchaimagehandler.ashx") ||
                featureURL.Contains(@"/candidateimagehandler.ashx") ||
                featureURL.Contains(@"/subscriptionstatus.aspx") ||
                featureURL.Contains(@"/activatesubscription.aspx") ||
                featureURL.Contains(@"/signup.aspx") ||
                featureURL.Contains(@"/usersignup.aspx") ||
                featureURL.Contains(@"/featureslist.aspx") ||             
                featureURL.Contains(@"/licenseagreement.aspx") ||
                featureURL.Contains(@"/comingsoon.aspx") ||
                featureURL.Contains(@"/accessdenied.aspx") ||
                featureURL.Contains(@"/featuredenied.aspx") ||
               // featureURL.Contains(@"/unhandlederror.aspx") ||
                featureURL.Contains(@"/candidateunhandlederror.aspx") ||
                featureURL.Contains(@"/pageunderconstruction.aspx") ||
                featureURL.Contains(@"/feedback.aspx") ||
                featureURL.Contains(@"/userdashboard.aspx") ||
                featureURL.Contains(@"/help.aspx") ||
                featureURL.Contains(@"/quicktour.aspx") ||
                featureURL.Contains(@"/contactsales.aspx") ||
                featureURL.Contains(@"/otmoverview.aspx") ||
                featureURL.Contains(@"/positionprofileoverview.aspx") ||
                featureURL.Contains(@"/resumerepositoryoverview.aspx") ||
                featureURL.Contains(@"/talentscoutoverview.aspx") ||
                featureURL.Contains(@"/FHCMSignupService.asmx") ||
                featureURL.Contains(@"/reportcenter/candidatetestdetails.aspx") ||
                featureURL.Contains(@"/publishassessorrating.aspx") ||
                featureURL.Contains(@"/publishcandidateinterviewresponse.aspx") ||
                featureURL.Contains(@"/externalassessor.aspx") ||
                featureURL.Contains(@"/externalassessorrating.aspx") ||
                featureURL.Contains(@"/externalinterviewresponse.aspx"))
            {
                return true;
            }

            return false;
        }

        private void AssignPermissions()
        {
            UserDetail userDetails = new UserDetail();

            List<RoleRights> roleRightsList = new List<RoleRights>();

            if (isPageAccessibleToSiteAdmin)
            {
                Response.Redirect("~/Common/AccessDenied.aspx", false);
            }


            if (Session["USER_DETAIL"] != null)
            {
                userDetails = ((UserDetail)Session["USER_DETAIL"]);

                if (userDetails.UserType == UserType.SiteAdmin)
                    return;

                roleRightsList = new AuthenticationBLManager().GetAuthorization
                    (userDetails.UserID, featureURL.ToLower());

                if (roleRightsList.Count>0 && !roleRightsList.Exists(item => item.RoleRightView == true))
                {
                    if (featureURL.Contains(@"/popup/"))
                        Response.Redirect("~/Popup/PopupAccessDenied.aspx", false);
                    else
                        Response.Redirect("~/Common/AccessDenied.aspx", false);
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx", false);
            }
        }

        /// <summary>
        /// Method that retrieves the rating image skin based on the given 
        /// rating value.
        /// </summary>
        /// <param name="value">
        /// A <see cref="decimal"/> that holds the rating value.
        /// </param>
        /// <param name="type">
        /// A <see cref="string"/> that holds the image type. Applicable values
        /// are GENERAL and SUMMARY.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skin file name for the given
        /// rating value.
        /// </returns>
        private string GetRatingImageSkinID(decimal value, string type)
        {
            if (value > 0.0m && value < 1.0m)
                return "skn" + type + "Rating_0_50";
            else if (value >= 1.0m && value < 1.5m)
                return "skn" + type + "Rating_1_00";
            else if (value >= 1.5m && value < 2.0m)
                return "skn" + type + "Rating_1_50";
            else if (value >= 2.0m && value < 2.5m)
                return "skn" + type + "Rating_2_00";
            else if (value >= 2.5m && value < 3.0m)
                return "skn" + type + "Rating_2_50";
            else if (value >= 3.0m && value < 3.5m)
                return "skn" + type + "Rating_3_00";
            else if (value >= 3.5m && value < 4.0m)
                return "skn" + type + "Rating_3_50";
            else if (value >= 4.0m && value < 4.5m)
                return "skn" + type + "Rating_4_00";
            else if (value >= 4.5m && value < 5.0m)
                return "skn" + type + "Rating_4_50";
            else if (value >= 5.0m && value < 5.5m)
                return "skn" + type + "Rating_5_00";
            else if (value >= 5.5m && value < 6.0m)
                return "skn" + type + "Rating_5_50";
            else if (value >= 6.0m && value < 6.5m)
                return "skn" + type + "Rating_6_00";
            else if (value >= 6.5m && value < 7.0m)
                return "skn" + type + "Rating_6_50";
            else if (value >= 7.0m && value < 7.5m)
                return "skn" + type + "Rating_7_00";
            else if (value >= 7.5m && value < 8.0m)
                return "skn" + type + "Rating_7_50";
            else if (value >= 8.0m && value < 8.5m)
                return "skn" + type + "Rating_8_00";
            else if (value >= 8.5m && value < 9.0m)
                return "skn" + type + "Rating_8_50";
            else if (value >= 9.0m && value < 9.5m)
                return "skn" + type + "Rating_9_00";
            else if (value >= 9.5m && value < 10.0m)
                return "skn" + type + "Rating_9_50";
            else if (value >= 10.0m && value < 10.5m)
                return "skn" + type + "Rating_10_00";
            else
                return string.Empty;
        }

        /// <summary>
        /// Method that retrieves the rating image based on the given 
        /// rating value.
        /// </summary>
        /// <param name="value">
        /// A <see cref="decimal"/> that holds the rating value.
        /// </param>
        /// <param name="type">
        /// A <see cref="string"/> that holds the image type. Applicable values
        /// are GENERAL and SUMMARY.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skin file name for the given
        /// rating value.
        /// </returns>
        private string GetRatingImage(decimal value, string type)
        {
            if (value > 0.0m && value < 1.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/0.50.png";
            else if (value >= 1.0m && value < 1.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/1.00.png";
            else if (value >= 1.5m && value < 2.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/1.50.png";
            else if (value >= 2.0m && value < 2.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/2.00.png";
            else if (value >= 2.5m && value < 3.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/2.50.png";
            else if (value >= 3.0m && value < 3.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/3.00.png";
            else if (value >= 3.5m && value < 4.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/3.50.png";
            else if (value >= 4.0m && value < 4.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/4.00.png";
            else if (value >= 4.5m && value < 5.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/4.50.png";
            else if (value >= 5.0m && value < 5.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/5.00.png";
            else if (value >= 5.5m && value < 6.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/5.50.png";
            else if (value >= 6.0m && value < 6.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/6.00.png";
            else if (value >= 6.5m && value < 7.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/6.50.png";
            else if (value >= 7.0m && value < 7.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/7.00.png";
            else if (value >= 7.5m && value < 8.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/7.50.png";
            else if (value >= 8.0m && value < 8.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/8.00.png";
            else if (value >= 8.5m && value < 9.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/8.50.png";
            else if (value >= 9.0m && value < 9.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/9.00.png";
            else if (value >= 9.5m && value < 10.0m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/9.50.png";
            else if (value >= 10.0m && value < 10.5m)
                return "../App_Themes/DefaultTheme/" + type + "RatingImages/10.00.png";
            else
                return string.Empty;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Method that will return the date in MM/dd/yyyy format. If date not exists, 
        /// then it displays empty value in column.
        /// </summary>
        /// <param name="date">This parameter is passed from aspx page value 
        /// </param>
        /// <returns></returns>
        public string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001" || strDate == "01-01-0001")
                strDate = "";
            return strDate;
        }

        #endregion Public Methods

        #region Protected Methods

        /// <summary>
        /// Method that retreives the <see cref="ImageFormat"/> for the 
        /// candidate photo. This information is retrieved from the config 
        /// file.
        /// </summary>
        /// <returns>
        /// A <see cref="ImageFormat"/> that holds the image format.
        /// </returns>
        protected ImageFormat GetCandidatePhotoFormat()
        {
            // Get extension from config file.
            string extension = ConfigurationManager.AppSettings
                ["CANDIDATE_PHOTO_FILE_EXTENSION"].ToLower().Trim();

            switch (extension)
            {
                case "png":
                    return ImageFormat.Png;

                case "jpeg":
                case "jpg":
                    return ImageFormat.Jpeg;

                case "gif":
                    return ImageFormat.Gif;
            }

            throw new Exception(string.Format
                ("The extension '{0}' is not supported for candidate photo", extension));
        }

        /// <summary>
        /// Represents the method that sets the user interface elements 
        /// properties such as permission settings, styles, formatting, etc.
        /// </summary>
        /// <param name="controls">
        /// A <see cref="ControlCollection"/> that holds the list of controls.
        /// </param>
        protected void UpdateUI(ControlCollection controls)
        {
            foreach (Control parentControl in controls)
            {
                if (parentControl is Control)
                {
                    UpdateUI(parentControl.Controls);
                }
            }
        }

        /// <summary>
        /// Represents the method that clears all the search criterias that 
        /// was kept in the session.
        /// </summary>
        protected void ClearSearchCriteriaSession()
        {
            Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_REPORT] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REPORT] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE_FORM] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CUSTOMER] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT] = null;
            Session[Constants.SearchCriteriaSessionKey.POSITION_PROFILE_STATUS] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_INTERVIEW_SESSION_BY_INTERVIEW]=null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_ONLINE_INTERVIEW_SESSION_BY_ONLINE_INTERVIEW] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_RECOMMENDATION] = null;
            Session[Constants.SearchCriteriaSessionKey.TEST_RECOMMENDATION_SUMMARY] = null;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REVIEW_RESUMES] = null;
            Session[Constants.SearchCriteriaSessionKey.RESUME_UPLOADER_SELECTED_CANDIDATE] = null;
        }

        /// <summary>
        /// Represents the method that clears all the search criterias that 
        /// was kept in the session.
        /// </summary>
        protected void ClearSearchTestStaticticsCriteriaSession()
        {
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] = null;
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="topLabel">
        /// A <see cref="Label"/> that holds the top label control.
        /// </param>
        /// <param name="bottomLabel">
        /// A <see cref="Label"/> that holds the bottom label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        protected void ShowMessage(Label topLabel, Label bottomLabel,
            string message)
        {
            // Set the message to the label.
            if (topLabel.Text.Length == 0)
            {
                topLabel.Text = message;

                if (bottomLabel != null)
                    bottomLabel.Text = message;
            }
            else
            {
                topLabel.Text += "<br>" + message;

                if (bottomLabel != null)
                    bottomLabel.Text += "<br>" + message;
            }
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="label">
        /// A <see cref="Label"/> that holds the label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        protected void ShowMessage(Label topLabel, string message)
        {
            ShowMessage(topLabel, null, message);
        }

        /// <summary>
        /// Trim the Length content
        /// </summary>
        /// <param name="content"></param>
        /// <param name="lengthofContent"></param>
        /// <returns></returns>
        protected string TrimContent(string content, int lengthofContent)
        {
            if (Utility.IsNullOrEmpty(content))
                return "";
            content = content.Trim();
            if (content.Length < lengthofContent)
            {
                return content;

            }
            return content.Substring(0, lengthofContent - 3) + "...";
        }

        /// <summary>
        /// Reset the All controls
        /// </summary>
        /// <param name="parent"></param>
        protected void ResetFormControlValues(Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    ResetFormControlValues(c);
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "System.Web.UI.WebControls.TextBox":
                            ((TextBox)c).Text = "";
                            break;
                        case "System.Web.UI.WebControls.CheckBoxList":
                            ((CheckBoxList)c).ClearSelection();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Represents the method that acts as a handler for redirect to parent
        /// page action. This handler is integrated with cancel link buttons in
        /// pages.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender.
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Based on the 'parentpage' query string parameter, the parent page 
        /// is determined.
        /// </remarks>
        protected void ParentPageRedirect(object sender, EventArgs e)
        {
            // Retrieve the parent page flag from the query string.
            string parentPage = Request.QueryString["parentpage"];

            if (Utility.IsNullOrEmpty(parentPage))
            {
                LinkButton linkButton = (sender as LinkButton);

                if (linkButton != null && linkButton.Page != null && linkButton.Page.MasterPageFile != null)
                {
                    if (linkButton.Page.MasterPageFile.ToUpper().Contains("OTMMASTER"))
                        Response.Redirect("~/OTMHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("TALENTSCOUTMASTER"))
                        Response.Redirect("~/TalentScoutHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("POSITIONPROFILEMASTER"))
                    {
                        if (linkButton.Page.ToString().ToUpper().Contains("POSITIONPROFILEREVIEW"))
                        {
                            Response.Redirect(@"~\PositionProfile\SearchPositionProfile.aspx?m=1&s=1", false);
                        }
                        else
                            Response.Redirect("~/PositionProfileHome.aspx", false);
                    }
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("SITEADMINMASTER"))
                        Response.Redirect("~/SiteAdminHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("CUSTOMERADMINMASTER"))
                        Response.Redirect("~/CustomerAdminHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("RESUMEREPOSITORYMASTER"))
                        Response.Redirect("~/ResumeRepositoryHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("INTERVIEWMASTER"))
                        Response.Redirect("~/InterviewHome.aspx", false);
                    else
                        Response.Redirect("~/Default.aspx", false);
                }
                else
                    Response.Redirect("~/Default.aspx", false);

                return;
            }
            if (!Utility.IsNullOrEmpty(parentPage) && parentPage.ToUpper() == "MENU")
            {
                LinkButton linkButton = (sender as LinkButton);

                if (linkButton != null && linkButton.Page != null && linkButton.Page.MasterPageFile != null)
                {
                    if (linkButton.Page.MasterPageFile.ToUpper().Contains("OTMMASTER"))
                        Response.Redirect("~/OTMHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("TALENTSCOUTMASTER"))
                        Response.Redirect("~/TalentScoutHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("POSITIONPROFILEMASTER"))
                        Response.Redirect("~/PositionProfileHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("SITEADMINMASTER"))
                        Response.Redirect("~/SiteAdminHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("CUSTOMERADMINMASTER"))
                        Response.Redirect("~/CustomerAdminHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("RESUMEREPOSITORYMASTER"))
                        Response.Redirect("~/ResumeRepositoryHome.aspx", false);
                    else if (linkButton.Page.MasterPageFile.ToUpper().Contains("INTERVIEWMASTER"))
                        Response.Redirect("~/InterviewHome.aspx", false);
                    else
                        Response.Redirect("~/Default.aspx", false);
                }
                else
                    Response.Redirect("~/Default.aspx", false);

                return;
            }
            
            switch (parentPage.ToUpper().Trim())
            {
                // Search Question.
                case Constants.ParentPage.SEARCH_QUESTION:
                    Response.Redirect(@"..\Questions\SearchQuestion.aspx?m=0&s=2", false);
                    break;

                // Review Resume .
                case Constants.ParentPage.REVIEW_RESUME:
                    Response.Redirect(@"..\ResumeRepository\ReviewResume.aspx?m=0&s=4", false);
                    break;

                // Upload Resume .
                case Constants.ParentPage.UPLOAD_RESUME:
                    Response.Redirect(@"..\ResumeRepository\ResumeUploader.aspx?m=1&s=1", false);
                    break;

               // Search Interview Question.
                case Constants.ParentPage.SEARCH_INTERVIEW_QUESTION:
                    Response.Redirect(@"..\InterviewQuestions\SearchInterviewQuestion.aspx?m=0&s=2", false);
                    break;

                // Search Test.
                case Constants.ParentPage.SEARCH_TEST:
                    Response.Redirect(@"..\TestMaker\SearchTest.aspx?m=1&s=2", false);
                    break;

                // Search Test Recommendation.
                case Constants.ParentPage.SEARCH_TEST_RECOMMENDATION:
                    Response.Redirect(@"..\TestMaker\SearchTestRecommendation.aspx?m=3&s=1", false);
                    break;

                // Search Test Recommendation.
                case Constants.ParentPage.TEST_RECOMMENDATION_SUMMARY:
                    Response.Redirect(@"..\TestMaker\TestRecommendationSummary.aspx?m=3&s=1" + 
                        "&id=" + Request.QueryString["id"] + 
                        "&parentpage=S_TST_REC", false);
                    break;

                // Search Test.
                case Constants.ParentPage.SEARCH_INTERVIEW:
                    Response.Redirect(@"..\InterviewTestMaker\SearchInterviewTest.aspx?m=1&s=2", false);
                    break;

                // Search Test Session.
                case Constants.ParentPage.SEARCH_TEST_SESSION:
                    Response.Redirect(@"..\TestMaker\TestSession.aspx?m=1&s=3", false);
                    break;

                // Save Interview Automatic Question
                case Constants.ParentPage.INTERVIEW_TEST_CREATE_AUTO_QUESTIONS:
                    Response.Redirect(@"..\InterviewTestMaker\CreateAutomaticInterviewTest.aspx?m=1&s=0", false);
                    break;

                // Save Interview Manual Question
                case Constants.ParentPage.INTERVIEW_TEST_CREATE_MANU_QUESTIONS:
                    Response.Redirect(@"..\InterviewTestMaker\CreateManualInterviewTest.aspx?m=1&s=1", false);
                    break;

                // Search Interview Test Session.
                case Constants.ParentPage.SEARCH_INTERVIEW_TEST_SESSION:
                    Response.Redirect(@"..\InterviewTestMaker\InterviewTestSession.aspx?m=1&s=3", false);
                    break;

                // Schedule Candidate.
                case Constants.ParentPage.SCHEDULE_CANDIDATE:
                    Response.Redirect(@"..\Scheduler\ScheduleCandidate.aspx?m=2&s=1", false);
                    break;

                // Interview Schedule Candidate.
                case Constants.ParentPage.INTERVIEW_SCHEDULE_CANDIDATE:
                    Response.Redirect(@"..\InterviewScheduler\InterviewScheduleCandidate.aspx?m=2&s=1", false);
                    break;

                // View Test.
                case Constants.ParentPage.VIEW_TEST:
                    Response.Redirect(@"..\TestMaker\ViewTest.aspx?m=1&s=2" +
                    "&parentpage=" + Constants.ParentPage.SEARCH_TEST +
                    "&testkey=" + Request.QueryString["testkey"], false);
                    break;

                // View Test.
                case Constants.ParentPage.VIEW_INTERVIEW_TEST:
                    Response.Redirect(@"..\InterviewTestMaker\ViewInterviewTest.aspx?m=1&s=2" +
                    "&parentpage=" + Constants.ParentPage.SEARCH_INTERVIEW +
                    "&testkey=" + Request.QueryString["testkey"], false);
                    break;

                // Manual Test.
                case Constants.ParentPage.CREATE_MANUAL_TEST:
                    Response.Redirect(@"..\TestMaker\CreateManualTest.aspx?m=1&s=1" +
                    "&testkey=" + Request.QueryString["testkey"], false);
                    break;

                // Edit Test.
                case Constants.ParentPage.EDIT_TEST:
                    Response.Redirect(@"..\TestMaker\EditManualTest.aspx?m=1&s=1" +
                    "&parentpage=" + Constants.ParentPage.SEARCH_TEST +
                    "&testkey=" + Request.QueryString["testkey"], false);
                    break;

                // Edit Interview Test.
                case Constants.ParentPage.EDIT_INTERVIEW_TEST:
                    Response.Redirect(@"..\InterviewTestMaker\EditManualInterviewTest.aspx?m=1&s=1" +
                    "&parentpage=" + Constants.ParentPage.SEARCH_INTERVIEW +
                    "&testkey=" + Request.QueryString["testkey"], false);
                    break;
                // Edit Interview Test.
                case Constants.ParentPage.EDIT_ADAPTIVEINTERVIEW_TEST:
                    Response.Redirect(@"..\InterviewTestMaker\EditInterviewTestWithAdaptive.aspx?m=1&s=1" +
                    "&parentpage=" + Constants.ParentPage.SEARCH_INTERVIEW +
                    "&testkey=" + Request.QueryString["interviewtestkey"], false);
                    break;

                // Test Report.
                case Constants.ParentPage.TEST_REPORT:
                    Response.Redirect(@"..\ReportCenter\TestReport.aspx?m=3&s=0", false);
                    break;

                // Interview Test Report.
                case Constants.ParentPage.INTERVIEW_TEST_REPORT:
                    Response.Redirect(@"..\InterviewReportCenter\InterviewTestReport.aspx?m=3&s=0", false);
                    break;

                // My availability.
                case Constants.ParentPage.MY_AVAILABILITY:
                    Response.Redirect(@"..\Assessor\MyAvailability.aspx?m=5&s=1&parentpage=MENU", false);
                    break;

                // Test Statistics Info.
                case Constants.ParentPage.TEST_STATISTICS_INFO:
                    Response.Redirect(@"..\ReportCenter\TestStatisticsInfo.aspx?m=3&s=0" +
                    "&parentpage=" + Constants.ParentPage.TEST_REPORT +
                    "&testkey=" + Request.QueryString["testkey"] + "&tab=" + Request.QueryString["tab"], false);
                    break;

                // Interview Test Statistics Info.
                case Constants.ParentPage.INTERVIEW_TEST_STATISTICS_INFO:
                    Response.Redirect(@"..\InterviewReportCenter\InterviewTestStatisticsInfo.aspx?m=4&s=0" +
                    "&parentpage=" + Constants.ParentPage.INTERVIEW_TEST_REPORT +
                    "&testkey=" + Request.QueryString["testkey"] + "&tab=" + Request.QueryString["tab"], false);
                    break;

                // Interview Test Scheduler.
                case Constants.ParentPage.INTERVIEW_TEST_SCHEDULER:
                    Response.Redirect(@"..\InterviewScheduler\InterviewTestScheduler.aspx?m=2&s=0" +
                    "&parentpage=" + Constants.ParentPage.INTERVIEW_TEST_SCHEDULER, false);
                    break;

               // Enroll Assessor Page.
                case Constants.ParentPage.ENROLL_ASSESSOR:
                    Response.Redirect(@"..\Admin\EnrollAssessor.aspx?m=1&s=2" +
                    "&parentpage=MENU&userId=" + Request.QueryString["assessorId"], false);
                    break;

              // Test Scheduler.
                case Constants.ParentPage.TEST_SCHEDULER:
                    Response.Redirect(@"..\Scheduler\TestScheduler.aspx?m=2&s=0" +
                    "&parentpage=" + Constants.ParentPage.TEST_SCHEDULER, false);
                    break;
                 
                    //Edit Assessor page
                case Constants.ParentPage.EDIT_ASSESSOR:
                    Response.Redirect(@"..\Admin\CorporateUserManagement.aspx?m=0&s=0", false);
                    break;

                case Constants.ParentPage.CANDIDATE_REPORT:
                    Response.Redirect(@"\ReportCenter\CandidateReport.aspx?m=4&s=2" +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_REPORT, false);
                    break;

                case Constants.ParentPage.SEARCH_POSITION_PROFILE_FORM:
                    Response.Redirect(@"~\Admin\SearchPositionProfileForm.aspx?m=2&s=1", false);
                    break;

                case Constants.ParentPage.SEARCH_POSITION_PROFILE:
                    Response.Redirect(@"~\PositionProfile\SearchPositionProfile.aspx?m=1&s=1", false);
                    break;

                case Constants.ParentPage.TALENT_SCOUT:
                    Response.Redirect(@"~\TalentScoutHome.aspx", false);
                    break;

                case Constants.ParentPage.POSITION_PROFILE:
                    {
                        string positionProfileID = string.Empty;

                        if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING]))
                            positionProfileID = "&" + Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" +
                                Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING];
                        Response.Redirect(@"~\PositionProfile\PositionProfileBasicInfo.aspx?m=1&s=0" + positionProfileID, false);
                    }
                    break;

                case Constants.ParentPage.POSITION_PROFILE_REVIEW:
                    {
                        string positionProfileID = string.Empty;

                        if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                            positionProfileID = Request.QueryString["positionprofileid"];

                        Response.Redirect(@"~\PositionProfile\PositionProfileReview.aspx?m=1&s=0&positionprofileid=" + positionProfileID, false);
                    }
                    break;

                case Constants.ParentPage.POSITION_PROFILE_WORKFLOW:
                    {
                        string positionProfileID = string.Empty;

                        if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                            positionProfileID = Request.QueryString["positionprofileid"];

                        Response.Redirect(@"~\PositionProfile\PositionProfileWorkflowSelection.aspx?m=1&s=0&positionprofileid=" + positionProfileID + "&parentpage = PP_REVIEW", false);
                    }
                    break;
                    
                case Constants.ParentPage.VIEW_ACCOUNT:
                    Response.Redirect(@"~\Admin\ViewAccount.aspx?m=0&s=0", false);
                    break;

                case Constants.ParentPage.EDIT_ACCOUNT:
                    Response.Redirect(@"~\Admin\EditAccount.aspx?m=0&s=1", false);
                    break;

                case Constants.ParentPage.UPGRADE_ACCOUNT:
                    Response.Redirect(@"~\Admin\UpgradeAccount.aspx?m=0&s=2", false);
                    break;

                case Constants.ParentPage.EDIT_FORM:
                    Response.Redirect(@"~\Admin\PositionProfileFormEntry.aspx?m=2&s=0&mode=edit&form_id=" + Request.QueryString["form_id"], false);
                    break;

                case Constants.ParentPage.SEARCH_CAREER_BUILDER_RESUME:
                    Response.Redirect(@"\ResumeRepositoryHome.aspx", false);
                    break;

                case Constants.ParentPage.SEARCH_CUSTOMER:
                    Response.Redirect(@"~\Admin\SearchCustomer.aspx?m=2&s=1", false);
                    break;

                case Constants.ParentPage.SEARCH_CANDIDATE:
                    {
                        string sourcePage = string.Empty;
                        if (!Utility.IsNullOrEmpty(Request.QueryString["sourcepage"]))
                            sourcePage = Request.QueryString["sourcepage"];

                        string positionProfileID = string.Empty;
                        if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                            positionProfileID = Request.QueryString["positionprofileid"];

                        if (sourcePage == Constants.ParentPage.POSITION_PROFILE_REVIEW)
                        {
                            Response.Redirect(@"~\ResumeRepository\SearchCandidateRepository.aspx?m=0&s=1" + 
                                "&positionprofileid=" + positionProfileID + 
                                "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_REVIEW , false);
                        }
                        else
                            Response.Redirect(@"~\ResumeRepository\SearchCandidateRepository.aspx?m=0&s=1", false);
                    }
                    break;

                case Constants.ParentPage.USER_DASHBOARD:
                    Response.Redirect(@"~\UserDashboard.aspx", false);
                    break;

                case Constants.ParentPage.WORKFLOW_MAJOR:
                    Response.Redirect(@"~\Workflow.aspx", false);
                    break;

                case Constants.ParentPage.WORKFLOW_LANDING:
                    string landingURL = @"~\Landing.aspx";
                    if (Request.QueryString["index"] != null)
                        landingURL += "?index=" + Request.QueryString["index"];
                    Response.Redirect(landingURL, false);
                    break;

                case Constants.ParentPage.WORKFLOW_ASSESSMENT:
                    Response.Redirect(@"~\AssessmentWorkflow.aspx", false);
                    break;

                case Constants.ParentPage.WORKFLOW_INTERVIEW:
                    Response.Redirect(@"~\InterviewWorkflow.aspx", false);
                    break;

                case Constants.ParentPage.CORPORATE_ADMIN:
                    Response.Redirect(@"~\Admin\CorporateSubscriptionManagement.aspx?m=1&s=0", false);
                    break;

                case Constants.ParentPage.POSITION_PROFILE_STATUS:
                    string url = @"~\PositionProfile\PositionProfileStatus.aspx?m=1&s=1&kill=N";

                    // Assign position profile ID.
                    if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING]))
                        url = url + "&" + Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING];

                    url = url + "&parentpage=" + Constants.ParentPage.SEARCH_POSITION_PROFILE;

                    Response.Redirect(url, false);
                    break;

                case Constants.ParentPage.CLIENT_MANAGEMENT:
                    string clientManagementURL = @"~\ClientCenter\ClientManagement.aspx?m=0&s=0";
                    if (!Utility.IsNullOrEmpty(Request.QueryString["CLT_ID"]))
                        clientManagementURL += "&CLT_ID=" + Request.QueryString["CLT_ID"];
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["DEP_ID"]))
                        clientManagementURL += "&DEP_ID=" + Request.QueryString["DEP_ID"];
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["CONT_ID"]))
                        clientManagementURL += "&CONT_ID=" + Request.QueryString["CONT_ID"];
                    Response.Redirect(clientManagementURL, false);
                    break;

                case Constants.ParentPage.MY_ASSESSMENT:
                    Response.Redirect(@"~\Assessments\MyAssessments.aspx?m=5&s=0", false);
                    break;
                case Constants.ParentPage.EDIT_INTERVIEW_SESSION:
                    Response.Redirect(@"~\InterviewTestMaker\EditInterviewTestSession.aspx?m=1&s=3&testsessionid=" + Request.QueryString["interviewsessionid"], false);
                    break;

                //For online interview
                // Search Online Interview Session.
                case Constants.ParentPage.SEARCH_ONLINE_INTERVIEW_SESSION:
                    Response.Redirect(@"..\OnlineInterview\OnlineInterviewSession.aspx?m=3&s=1&parentpage=MENU", false);
                     break;

                default:
                     Response.Redirect("~/Default.aspx", false);
                    break;
            }
        }

        /// <summary>
        /// Method that retrieves the sort column index for the given grid view
        /// and currently sorted column key.
        /// </summary>
        /// <param name="gridView">
        /// A <see cref="GridView"/> that holds the grid view object.
        /// </param>
        /// <param name="columnKey">
        /// A <see cref="string"/> that holds the currently sorted column key.
        /// </param>
        /// <returns></returns>
        protected int GetSortColumnIndex(GridView gridView, string columnKey)
        {
            foreach (DataControlField field in gridView.Columns)
            {
                if (field.SortExpression.Split(' ')[0] == columnKey)
                {
                    return gridView.Columns.IndexOf(field);
                }
            }
            return -1;
        }
        
        /// <summary>
        /// Method that adds sort image (ascending or descending) to the sorted
        /// row header.
        /// </summary>
        /// <param name="columnIndex">
        /// A <see cref="int"/> that holds the column index.
        /// </param>
        /// <param name="headerRow">
        /// A <see cref="GridViewRow"/> that hollds the grid view row object.
        /// </param>
        /// <param name="sortOrder">
        /// A <see cref="string"/> that holds the sort order. 'A' represents
        /// ascending and 'D' descending.
        /// </param>
        protected void AddSortImage(int columnIndex, GridViewRow headerRow,
            SortType sortOrder)
        {
            // Create and assign the sort image based on the sort direction.
            Image sortImage = new Image();

            if (sortOrder == SortType.Ascending)
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            // Add the image to the appropriate header cell.
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// display the Yes and no while we get N and Y
        /// </summary>
        /// <param name="content"></param>
        /// <param name="lengthofContent"></param>
        /// <returns></returns>
        protected string GetExpansion(bool content)
        {
            if (content)
            {
                return "Yes";
            }
            else if (!content)
            {
                return "No";
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Method that retrieves the general rating image skin based on the 
        /// given rating value.
        /// </summary>
        /// <param name="value">
        /// A <see cref="decimal"/> that holds the rating value.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skin file name for the given
        /// rating value.
        /// </returns>
        protected string GetGeneralRatingImageSkinID(decimal value)
        {
            return GetRatingImageSkinID(value, "General");
        }

        /// <summary>
        /// Method that retrieves the summary rating image skin based on the 
        /// given rating value.
        /// </summary>
        /// <param name="value">
        /// A <see cref="decimal"/> that holds the rating value.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skin file name for the given
        /// rating value.
        /// </returns>
        protected string GetSummaryRatingImageSkinID(decimal value)
        {
            return GetRatingImageSkinID(value, "Summary");
        }

        /// <summary>
        /// Method that retrieves the general rating image based on the given 
        /// rating value.
        /// </summary>
        /// <param name="value">
        /// A <see cref="decimal"/> that holds the rating value.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skin file name for the given
        /// rating value.
        /// </returns>
        protected string GetGeneralRatingImage(decimal value)
        {
            return GetRatingImage(value, "General");
        }

        /// <summary>
        /// Method that retrieves the summary rating image based on the given 
        /// rating value.
        /// </summary>
        /// <param name="value">
        /// A <see cref="decimal"/> that holds the rating value.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skin file name for the given
        /// rating value.
        /// </returns>
        protected string GetSummaryRatingImage(decimal value)
        {
            return GetRatingImage(value, "Summary");
        }

        /// <summary>
        /// Method that checks if the given date is present in the date list.
        /// </summary>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the date to search.
        /// </param>
        /// <param name="dates">
        /// A list of <see cref="DateTime"/> that holds the date list.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True represents 
        /// date present in the dates list and false otherwise.
        /// </returns>
        protected bool IsDatePresent(DateTime date, List<DateTime> dates)
        {
            // Check if the date list is null
            if (dates == null || dates.Count == 0)
                return false;

            // Filter the date list with the given date and get the count.
            var count = (from item in dates
                         where item.Day == date.Day && item.Month == date.Month && item.Year == date.Year
                         select item).Count();

            return count == 0 ? false : true;
        }

        /// <summary>
        /// Method that checks if the given date are equal or not.
        /// </summary>
        /// <param name="firstDate">
        /// A <see cref="DateTime"/> that holds the first date.
        /// </param>
        /// <param name="secondDate">
        /// A <see cref="DateTime"/> that holds the second date.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. Returns true if dates 
        /// are equal and false otherwise.
        /// </returns>
        protected bool IsDatesEqual(DateTime firstDate, DateTime secondDate)
        {
            if (firstDate.Day == secondDate.Day &&
                firstDate.Month == secondDate.Month &&
                firstDate.Year == secondDate.Year)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method that checks if the given date is max date.
        /// </summary>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the date to check.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. Returns true if date is
        /// max and false otherwise.
        /// </returns>
        protected bool IsMaxDate(DateTime date)
        {
            if (date.Day == DateTime.MaxValue.Day &&
                date.Month == DateTime.MaxValue.Month &&
                date.Year == DateTime.MaxValue.Year)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method that checks if the given date is min date.
        /// </summary>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the date to check.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. Returns true if date is
        /// min and false otherwise.
        /// </returns>
        protected bool IsMinDate(DateTime date)
        {
            if (date.Day == DateTime.MinValue.Day &&
                date.Month == DateTime.MinValue.Month &&
                date.Year == DateTime.MinValue.Year)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method that checks if the user has the specified rights against the 
        /// position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="keyRights">
        /// A list of <see cref="string"/> that holds the key rights to check against the 
        /// position profile owner rights.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the rights availability status. If 
        /// any of the key rights is present in the rights, then it returns true. 
        /// Else returns false.
        /// </returns>
        protected bool HasPositionProfileOwnerRights(int positionProfileID, int userID, List<string> keyRights)
        {
            // Check for validity.
            if (positionProfileID == 0 || userID == 0 || keyRights == null || keyRights.Count == 0)
                return false;

            // Get position profile rights.
            List<string> rights = new PositionProfileBLManager().GetPositionProfileOwnerRights
                (positionProfileID, userID);

            // Check for validity.
            if (rights == null || rights.Count == 0)
                return false;

            // Loop through the key rights and check against the rights.
            foreach (string keyRight in keyRights)
            {
                if (rights.FindIndex(item => item.ToUpper().Trim() == keyRight.ToUpper().Trim()) != -1)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Method that checks if the user has the review rights against the 
        /// position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the review rights for the position profile.
        /// </returns>
        protected bool HasPositionProfileReviewRights(int positionProfileID, int tenantID)
        {
            // Get position profile review rights.
            List<string> reviewrights = new PositionProfileBLManager().GetPositionProfileReviewRights
                (positionProfileID, tenantID);

            // Check for validity.
            if (reviewrights == null || reviewrights.Count == 0)
                return false;
            else
                return true;
        }

        #endregion Proctected Methods

        #region Public Properties

        /// <summary>
        /// Property that retrieves the 'no photo available' image path for
        /// candidates.
        /// </summary>
        public string CandidateNoPhotoAvailablePath
        {
            get
            {
                return ConfigurationManager.AppSettings["DEFAULT_URL"] +
                ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] 
                    + "/" + "photo_not_available.png";
            }
        }
        /// <summary>
        /// Property that retrieves the grid page size.
        /// </summary>
        public int GridPageSize
        {
            get
            {
                if (Application[Constants.SessionConstants.GRID_PAGE_SIZE] == null)
                {
                    //  Keep page size in session.
                    Application[Constants.SessionConstants.GRID_PAGE_SIZE] = new
                        AdminBLManager().GetAdminSettings().GridPageSize;
                }

                return Convert.ToInt32
                    (Application[Constants.SessionConstants.GRID_PAGE_SIZE]);
            }
        }
        /// <summary>
        /// Property that retrieves the grid page size.
        /// </summary>
        public NomenclatureAllAttributes NomenclatureAllAttributes
        {
            get
            {
                if (Session[Constants.SessionConstants.ALL_NOMENCLATURE] == null)
                {
                    //  Keep page size in session.
                    Session[Constants.SessionConstants.ALL_NOMENCLATURE] = new
                        ControlUtility().LoadAllAttributeDetails(tenantID);
                }
                return (NomenclatureAllAttributes)Session[Constants.SessionConstants.ALL_NOMENCLATURE];
            }
        }

        public bool IsClientManagementDelete
        {
            get
            {
                if (isCustomerAdmin)
                    return true;

                if (Session[Constants.SessionConstants.IS_CLIENT_MANAGEMENT_DELETE] == null)
                {
                    if (Utility.IsNullOrEmpty(new ClientBLManager().GetClientManagementSettings(tenantID)))
                    {
                        Session[Constants.SessionConstants.IS_CLIENT_MANAGEMENT_DELETE] = false;
                        return false;
                    }
                    Session[Constants.SessionConstants.IS_CLIENT_MANAGEMENT_DELETE] = new ClientBLManager().GetClientManagementSettings(tenantID).AllowDeleteClientForAll;
                }
                if ((bool)Session[Constants.SessionConstants.IS_CLIENT_MANAGEMENT_DELETE])
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string URL
        {
            get { return this.url; }
            // write property
            set { this.url = value; }
        }

        /// <summary>
        /// Sets the tamper proof params.
        /// </summary>
        /// <value>The tamper proof params.</value>
        public List<string> Parameters
        {
            get { return this.parameters; }
            // write property
            set { this.parameters = value; }
        }


        /// <summary>
        /// Sets the received obfuscated.
        /// </summary>
        /// <value>The received obfuscated.</value>
        public string ReceivedObfuscated
        {
            get { return this.receivedObfuscated; }
            // write property
            set { this.receivedObfuscated = value; }
        }

        #endregion Public Properties
    }
}