﻿#region Directives

using System;
using System.IO;
using System.Web;
using System.Drawing;
using System.Web.Services;
using System.Configuration;
using System.Drawing.Imaging;
using System.Web.SessionState;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Common
{
    /// <summary>
    /// Class that represents the handler for displaying candidate images in 
    /// various pages.
    /// </summary>
    public class CandidateImageHandler : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
    {
        /// <summary>
        /// Process images for the request from various sources. 
        /// </summary>
        /// <param name="context">
        /// A <see cref="HttpContext"/> that holds the http context.
        /// </param>
        public void ProcessRequest(HttpContext context)
        {
            string source = null;
            int candidateID = 0;
            int userID = 0;
            byte[] image = null;
            Image candidatePhoto=null;
            string filePath = null;
            string fileType = null;
            
            if (!Utility.IsNullOrEmpty((context.Request.QueryString["filepath"])))
            {
                filePath = context.Request.QueryString["filepath"].ToString();
            }
            if (!Utility.IsNullOrEmpty((context.Request.QueryString["filetype"])))
            {
                fileType = context.Request.QueryString["filetype"].ToString();
            }

            if (filePath != null && filePath != fileType)
            {
                switch (fileType)
                {
                    case "FLV":
                        context.Response.Clear();
                        context.Response.Buffer = true;
                        context.Response.ClearContent();
                        context.Response.ContentType = "application/octetstream";
                        context.SkipAuthorization = true;
                        context.Response.AddHeader("content-disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(filePath)));
                        context.Response.TransmitFile(filePath);
                        context.Response.Flush();
                        context.Response.Close();
                        context.Response.End();
                        break;
                    case "xlsx":
                        byte[] resumeContent = (byte[])context.Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT];
                        context.Response.Clear();
                        context.Response.Buffer = true;
                        context.Response.ClearContent();
                        context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filePath + "." + fileType.ToLower());
                        context.Response.AddHeader("Content-Length", resumeContent.Length.ToString());
                        context.Response.ContentType = "application/octet-stream";
                        context.Response.BinaryWrite(resumeContent);
                        context.Response.Flush();
                        context.Response.End();
                        break;
                    default:
                        break;
                }
            }

            if (!Utility.IsNullOrEmpty((context.Request.QueryString["source"])))
            {
                source = context.Request.QueryString["source"].ToString().ToUpper();
            }

            if (!Utility.IsNullOrEmpty((context.Request.QueryString["candidateid"])))
            {
                candidateID = Convert.ToInt32(context.Request.QueryString["candidateid"].ToString());
            }
            if (!Utility.IsNullOrEmpty((context.Request.QueryString["userid"])))
            {
                userID = Convert.ToInt32(context.Request.QueryString["userid"].ToString());
            }
            
            // Retreive the image based on the source.
            switch (source)
            {
                case "CAND_DASH_PAGE":
                    if (context.Session["CANDIDATE_THUMBNAIL_PHOTO"] != null)
                    {
                        image = GetImageBytes(context.Session["CANDIDATE_THUMBNAIL_PHOTO"] as Image);
                    }
                    break;
                case "CAND_CREA_PAGE":
                    if (context.Session["CANDIDATE_THUMBNAIL_PHOTO"] != null)
                    {
                        var candImage = GetImageBytes(context.Session["CANDIDATE_THUMBNAIL_PHOTO"] as Image);
                        candidatePhoto = GetImage(candImage);
                    }
                    break;
                case "ASSESSOR_CREA_PAGE":
                    if (context.Session["ASSESSOR_THUMBNAIL_PHOTO"] != null)
                    {
                        image = GetImageBytes(context.Session["ASSESSOR_THUMBNAIL_PHOTO"] as Image);
                    }
                    break;
                case "CAND_CREA_POPUP":
                    if (context.Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] != null)
                    {
                        var candImagePopup = GetImageBytes(context.Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] as Image);
                        candidatePhoto = GetImage(candImagePopup);
                    }
                    break;

                case "PP_STAT_PAGE":
                case "VIEW_CAND":
                    if (candidateID != 0)
                    {
                        // Compose the candidate photo path.
                        string path = context.Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

                        // If file exists, then retrieve the bytes from the image.
                        if (File.Exists(path))
                        {
                            using (FileStream fileStream = new FileStream
                                (path, FileMode.Open, FileAccess.Read))
                            {
                                Image thumbnailPhoto = Image.FromStream(fileStream);
                                MemoryStream memoryStream = new MemoryStream();
                                thumbnailPhoto.Save(memoryStream, GetCandidatePhotoFormat());
                                image = memoryStream.ToArray();

                                fileStream.Close();
                            }
                        }
                    }
                   
                    break;

                case "CAS_PAGE":
                    if (candidateID != 0)
                    {
                        // Compose the candidate photo path.
                        string path = context.Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

                        // If file exists, then retrieve the bytes from the image.
                        if (File.Exists(path))
                        {
                            using (FileStream fileStream = new FileStream
                                (path, FileMode.Open, FileAccess.Read))
                            {
                                Image thumbnailPhoto = Image.FromStream(fileStream);
                                MemoryStream memoryStream = new MemoryStream();
                                thumbnailPhoto.Save(memoryStream, GetCandidatePhotoFormat());
                                image = memoryStream.ToArray();

                                fileStream.Close();
                            }
                        }
                    }
                    break;

                case "VIEW_ASS_PHOTO":
                    if (userID != 0)
                    {
                        // Compose the assessor photo path.
                        string path = context.Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["ASSESSOR_PHOTOS_FOLDER"] +
                            "\\" + userID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

                        // If file exists, then retrieve the bytes from the image.
                        if (File.Exists(path))
                        {
                            using (FileStream fileStream = new FileStream
                                (path, FileMode.Open, FileAccess.Read))
                            {
                                Image thumbnailPhoto = Image.FromStream(fileStream);
                                MemoryStream memoryStream = new MemoryStream();
                                thumbnailPhoto.Save(memoryStream, GetCandidatePhotoFormat());
                                image = memoryStream.ToArray();

                                fileStream.Close();
                            }
                        }
                    }

                    break;   
                default:
                    break;
            }
            
            // If image is not null, then write the image to the response.
            if (candidatePhoto != null)
            {
                context.Response.ContentType = "image/jpeg";
                candidatePhoto.Save(context.Response.OutputStream, GetCandidatePhotoFormat());
            }
            else if (image != null)
            {
                context.Response.BinaryWrite(image);
                context.Response.End();
            }
            else
            {
                // Show empty image.
                ShowEmptyImage(context);
            }
        }
        /// <summary>
        /// Method that retreives an image object from byte array
        /// </summary>
        /// <param name="image">
        /// A <see cref="Image"/> that holds the image
        /// </param>
        /// <returns>
        /// A array of <see cref="byte"/> that holds image bytes.
        /// </returns>
        private Image GetImage(byte[] storedImage)
        {
            var stream = new MemoryStream(storedImage);
            return Image.FromStream(stream);
        }
        /// <summary>
        /// Method that retreives the <see cref="ImageFormat"/> for the 
        /// candidate photo. This information is retrieved from the config 
        /// file.
        /// </summary>
        /// <returns>
        /// A <see cref="ImageFormat"/> that holds the image format.
        /// </returns>
        private ImageFormat GetCandidatePhotoFormat()
        {
            // Get extension from config file.
            string extension = ConfigurationManager.AppSettings
                ["CANDIDATE_PHOTO_FILE_EXTENSION"].ToLower().Trim();

            switch (extension)
            {
                case "png":
                    return ImageFormat.Png;

                case "jpeg":
                case "jpg":
                    return ImageFormat.Jpeg;

                case "gif":
                    return ImageFormat.Gif;
            }

            throw new Exception(string.Format
                ("The extension '{0}' is not supported for candidate photo", extension));
        }

        /// <summary>
        /// Method that shows an empty image.
        /// </summary>
        private void ShowEmptyImage(HttpContext context)
        {
            // Show the 'not available image'.
            string path = context.Server.MapPath("~/") +
                ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                "\\" + "photo_not_available.jpg";

            Image originalPhoto = Image.FromFile(path);
            int thumbnailWidth = Convert.ToInt32
                (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_WIDTH"]);
            int thumbnailHeight = Convert.ToInt32
                (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_HEIGHT"]);

            Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                (thumbnailWidth, thumbnailHeight, null, System.IntPtr.Zero);

            // Change the response headers to output a JPEG image.
            context.Response.Clear();
            context.Response.ContentType = "image/jpeg";

            // Write the image to the response stream in JPEG format.
            thumbnailPhoto.Save(context.Response.OutputStream, ImageFormat.Jpeg);
        }

        /// <summary>
        /// Method that retreives the byte array from an image object.
        /// </summary>
        /// <param name="image">
        /// A <see cref="Image"/> that holds the image
        /// </param>
        /// <returns>
        /// A array of <see cref="byte"/> that holds image bytes.
        /// </returns>
        private byte[] GetImageBytes(System.Drawing.Image image)
        {
            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Gif);
            return memoryStream.ToArray();
        }

        /// <summary>
        /// Property that retrieves the 'is reusable' status.
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
