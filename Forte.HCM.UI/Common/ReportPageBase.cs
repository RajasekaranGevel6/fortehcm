﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;

using System.Collections.Generic;

using Kalitte.Dashboard.Framework;

using Forte.HCM.UI.Common;

namespace Kalitte
{
    public class ReportPageBase : PageBase
    {
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
        }

        public virtual bool HasRightContent
        {
            get
            {
                return false;
            }
        }
        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {

            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
    }
}
