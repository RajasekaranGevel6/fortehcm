﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
    CodeBehind="ComingSoon.aspx.cs" Inherits="Forte.HCM.UI.Common.ComingSoon" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="ComingSoon_bodyContent" ContentPlaceHolderID="HomeMaster_body"
    runat="server">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td style="width: 35%" align="left" valign="top">
                <table style="width: 100%">
                    <tr>
                        <td class="coming_soon_title">
                            Coming Soon
                        </td>
                    </tr>
                    <tr>
                        <td class="coming_soon_message">
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="coming_soon_title">
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="coming_soon_message">
                            This feature is currently not available but is well on it's way, and will be coming soon
                        </td>
                    </tr>
                    <tr>
                        <td class="coming_soon_message">
                            For details contact your administrator
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="coming_soon_message_link_message">
                            Click here to go to <asp:LinkButton ID="ComingSoon_homeLinkButton" runat="server" Text="Home" CssClass="coming_soon_message_link_btn" PostBackUrl="~/Default.aspx" ToolTip="Click here to go to home page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                    <tr>
                        <td class="coming_soon_message_link_message">
                            Click here to go to <asp:LinkButton ID="ComingSoon_feedbackLinkButton" runat="server" Text="Feedback" CssClass="coming_soon_message_link_btn" PostBackUrl="~/General/Feedback.aspx" ToolTip="Click here to go to feedback page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 65%" class="coming_soon">
            </td>
        </tr>
    </table>
</asp:Content>
