﻿using System.IO;
using System.Web;
using System.Drawing;
using System.Web.Services;
using System.Web.SessionState;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.Utilities;

namespace Forte.HCM.UI.ReportCenter
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

    public class ImageHandler : IHttpHandler,IReadOnlySessionState
    {
        /// <summary>
        /// Process images for the request from the entities like certificate, 
        /// cyber proctoring images and candidate certificate images. 
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            int imageid = 0;
            int isThumb = 0;
            int logID = 0;
            string candidateSessionKey = string.Empty;
            string testKey = string.Empty;
            string questionKey = string.Empty;
            int attemptID = 0;
            string imageType=string.Empty;

            int.TryParse(context.Request.QueryString["ImageID"], out imageid);
            int.TryParse(context.Request.QueryString["isThumb"], out isThumb);
            int.TryParse(context.Request.QueryString["attemptid"], out attemptID);
            int.TryParse(context.Request.QueryString["logID"], out logID);

            byte[] image = null;
            string source = ""; 

            if (!Utility.IsNullOrEmpty((context.Request.QueryString["source"])))
            {
                source = context.Request.QueryString["source"].ToString();
            }
            if (!Utility.IsNullOrEmpty((context.Request.QueryString["imageType"])))
            {
                imageType = context.Request.QueryString["imageType"].ToString();
            }

            if (context.Request.QueryString["candidatesessionid"] != null)
            {
                candidateSessionKey = context.Request.QueryString["candidatesessionid"].Trim();
            }

            if (context.Request.QueryString["testkey"] != null)
            {
                testKey = context.Request.QueryString["testkey"].Trim();
            }
            if (context.Request.QueryString["questionKey"] != null)
            {
                questionKey = context.Request.QueryString["questionKey"].Trim();
            }
            switch (source)
            {
                    // Here, certificate template will be generated based on the certificate id.
                case "CERTIFICATE_TEMPLATE":
                    {
                        if (imageid == 0)
                            return;
                        CertificationDetail certificationDetail = new
                                AdminBLManager().GetCertificateDetails(imageid);
                        image = certificationDetail.ImageData;
                        break;
                    }

                case "OI":
                    {
                        if (imageid == 0)
                            return;

                        image = new TrackingBLManager().
                            GetOfflineInterviewTrackingImage(imageid, isThumb);

                        break;
                    }

                case "OILOG":
                    {
                        if (imageid == 0)
                            return;

                        image = new TrackingBLManager().
                            GetOfflineInterviewCommonLogImage(imageid, imageType, logID);

                        break;
                    }

                case "CP":
                    {
                        if (imageid == 0)
                            return;

                        image = new TrackingBLManager().GetCyberProctorDesktopImage(imageid, isThumb);

                        break;
                    }

                case "CPLOG":
                    {
                        if (imageid == 0)
                            return;

                        image = new TrackingBLManager().GetCyberProctorDesktopLogImage(imageid, imageType, logID);

                        break;
                    }
                    
                case "CERTIFICATE_IMAGE":
                    {
                        // Here, certificate will be generated for the candidate who is completed the test.
                        image = new AdminBLManager().GetCertificateImage(candidateSessionKey, attemptID);
                        break;
                    }

                case "CERTIFICATE_TEMPLATE_IMAGE":
                    {
                        // Here, certificate template will be generated for the certificate test.
                        CertificationDetail certificationDetail =
                            new AdminBLManager().GetTestCertificationDetails(testKey);
                        image = certificationDetail.ImageData;
                        break;
                    }

                case "QUESTION_IMAGE":
                    {
                        image = context.Session["POSTED_QUESTION_IMAGE"] as byte[];
                        image = ImageResizer.CreateThumbnail(image, new Size(700, 150));
                        break;
                    }

                case "INTERVIEW_QUESTION_IMAGE":
                    {
                        image = context.Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                        image = ImageResizer.CreateThumbnail(image, new Size(700, 150));
                        break;
                    }

                case "VIEWTEST_QUESTION_IMAGE":
                    {
                        image = new QuestionBLManager().GetQuestionImage(questionKey);
                        image = ImageResizer.CreateThumbnail(image, new Size(700, 150));
                        break;
                    }
                case "VIEW_INTERVIEW_QUESTION_IMAGE":
                    {
                        image = new InterviewQuestionBLManager().GetInterviewQuestionImage(questionKey);
                        image = ImageResizer.CreateThumbnail(image, new Size(700, 150));
                        break;
                    }

                default:
                    break;
            }

            if (image != null)
            {
                context.Response.BinaryWrite(image);
                context.Response.End();
            }
            else
            {
                
                string file = @"~/App_Themes/DefaultTheme/Images/missing_image.jpg";

                if (File.Exists(file))
                {
                    using (System.Drawing.Image im =
                           System.Drawing.Image.FromFile(
                           file))
                    {
                        MemoryStream ms = new MemoryStream();
                        im.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                        context.Response.BinaryWrite(ms.ToArray());
                        context.Response.End();
                    }
                }
            }
        }
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
