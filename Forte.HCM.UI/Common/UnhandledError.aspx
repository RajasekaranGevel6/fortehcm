﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
    CodeBehind="UnhandledError.aspx.cs" Inherits="Forte.HCM.UI.Common.UnhandledError" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="UnhandledError_bodyContent" ContentPlaceHolderID="HomeMaster_body"
    runat="server">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td style="width: 40%" align="left" valign="top">
                <table style="width: 100%">
                    <tr>
                        <td class="unhandled_error_title">
                            Error
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Unhandled error
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_title">
                            Message
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            An unhandled error has occured in the application
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Please contact your administrator
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_title">
                            Details
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            <asp:Label ID="UnhandledError_detailsLabel" runat="server" Text="None"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="UnhandledError_homeLinkButton" runat="server" Text="Home" CssClass="unhandled_error_message_link_btn"
                                PostBackUrl="~/Default.aspx" ToolTip="Click here to go to home page">
                            </asp:LinkButton>
                            page
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="UnhandledError_feedbackLinkButton" runat="server" Text="Feedback"
                                CssClass="unhandled_error_message_link_btn" PostBackUrl="~/General/Feedback.aspx"
                                ToolTip="Click here to go to feedback page">
                            </asp:LinkButton>
                            page
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 60%" class="unhandled_error">
            </td>
        </tr>
    </table>
</asp:Content>
