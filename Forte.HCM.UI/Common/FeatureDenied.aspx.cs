﻿using System;
using System.Web.UI;

namespace Forte.HCM.UI.Common
{
    public partial class FeatureDenied : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Feature Denied");
        }
    }
}