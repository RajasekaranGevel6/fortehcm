﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
    CodeBehind="AccessDenied.aspx.cs" Inherits="Forte.HCM.UI.Common.AccessDenied" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="AccessDenied_bodyContent" ContentPlaceHolderID="HomeMaster_body"
    runat="server">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td style="width: 40%" align="left" valign="top">
                <table style="width: 100%">
                    <tr>
                        <td class="access_denied_title">
                            Error
                        </td>
                    </tr>
                    <tr>
                        <td class="access_denied_message">
                            Access denied
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="access_denied_title">
                            Message
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="access_denied_message">
                            You do not have sufficient rights to access to the specific page
                        </td>
                    </tr>
                    <tr>
                        <td class="access_denied_message">
                            Please contact your administrator
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="access_denied_message_link_message">
                            Click here to go to <asp:LinkButton ID="AccessDenied_homeLinkButton" runat="server" Text="Home" CssClass="access_denied_message_link_btn" PostBackUrl="~/default.aspx" ToolTip="Click here to go to home page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                    <tr>
                        <td class="access_denied_message_link_message">
                            Click here to go to <asp:LinkButton ID="AccessDenied_feedbackLinkButton" runat="server" Text="Feedback" CssClass="access_denied_message_link_btn" PostBackUrl="~/General/Feedback.aspx" ToolTip="Click here to go to feedback page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 60%" class="access_denied">
            </td>
        </tr>
    </table>
</asp:Content>
