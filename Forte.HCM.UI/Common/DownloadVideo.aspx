﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadVideo.aspx.cs" Inherits="Forte.HCM.UI.Common.DownloadVideo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function Form_Close() {
            window.close();
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <div>
    <asp:Label ID="DownloadVideo_errorMessageLabel" runat="server" Text="" CssClass="error_message_text"></asp:Label><br />
        <asp:LinkButton ID="DownloadVideo_closeLinkButton" runat="server" OnClientClick="javascript:Form_Close();">Close</asp:LinkButton> 
    </div>
    </form>
</body>
</html>
