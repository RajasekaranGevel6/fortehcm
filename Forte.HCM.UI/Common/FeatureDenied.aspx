﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
    CodeBehind="FeatureDenied.aspx.cs" Inherits="Forte.HCM.UI.Common.FeatureDenied" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="FeatureDenied_bodyContent" ContentPlaceHolderID="HomeMaster_body"
    runat="server">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td style="width: 40%" align="left" valign="top">
                <table style="width: 100%">
                    <tr>
                        <td class="feature_denied_title">
                            Error
                        </td>
                    </tr>
                    <tr>
                        <td class="feature_denied_message">
                            Feature denied
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="feature_denied_title">
                            Message
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="feature_denied_message">
                            This feature is not applicable for your subscription plan
                        </td>
                    </tr>
                    <tr>
                        <td class="feature_denied_message">
                            Please contact your administrator
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="feature_denied_message_link_message">
                            Click here to go to <asp:LinkButton ID="FeatureDenied_homeLinkButton" runat="server" Text="Home" CssClass="feature_denied_message_link_btn" PostBackUrl="~/Default.aspx" ToolTip="Click here to go to home page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                    <tr>
                        <td class="feature_denied_message_link_message">
                            Click here to go to <asp:LinkButton ID="FeatureDenied_feedbackLinkButton" runat="server" Text="Feedback" CssClass="feature_denied_message_link_btn" PostBackUrl="~/General/Feedback.aspx" ToolTip="Click here to go to feedback page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 60%" class="feature_denied">
            </td>
        </tr>
    </table>
</asp:Content>
