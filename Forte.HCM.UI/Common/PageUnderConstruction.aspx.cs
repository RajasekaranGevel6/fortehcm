﻿using System;
using System.Web.UI;

using Forte.HCM.UI.Common;

namespace Forte.HCM.UI.Common
{
    public partial class PageUnderConstruction : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Page Under Construction");
        }
    }
}
