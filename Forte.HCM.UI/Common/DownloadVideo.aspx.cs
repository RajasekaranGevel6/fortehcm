﻿using System;
using System.Configuration;
 
using Forte.HCM.Support;
using System.IO;

namespace Forte.HCM.UI.Common
{
    public partial class DownloadVideo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            DownloadVideo_errorMessageLabel.Text = "";
            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
              Request.QueryString["type"].ToUpper() == "VIDEOFILE")
            {  
                DownloadVideoFile(Request.QueryString["foldername"],Request.QueryString["filename"]);
            }
        }

        #region private Methods
        /// <summary>
        /// Method to download the local system file.
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds thevideo file ID.
        /// </param>
        private void DownloadVideoFile(string folderName, string fileName )
        {
            string filePath = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_VIDEO_FILE"].ToString(); 
            
            fileName = fileName + ".flv";
            filePath += folderName + "//" + fileName; 

             if (File.Exists(Server.MapPath(filePath)))
            {
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.ContentType = "application/octet-stream";
                Response.TransmitFile(filePath);
                Response.Flush();
                Response.End();
            }
            else
            {
                DownloadVideo_errorMessageLabel.Text = "File not found"; 
            }
         
        }
        #endregion
    }
}