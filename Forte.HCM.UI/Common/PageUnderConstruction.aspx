﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
    CodeBehind="PageUnderConstruction.aspx.cs" Inherits="Forte.HCM.UI.Common.PageUnderConstruction"
    Title="Page Under Construction" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content runat="server" ID="PageUnderConstruction_body" ContentPlaceHolderID="HomeMaster_body">
<table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td style="width: 40%" align="left" valign="top">
                <table style="width: 100%">
                    <tr>
                        <td class="unhandled_error_title">
                            Error
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Page under construction
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_title">
                            Message
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            The requested page cannot be accessed at this time
                        </td>
                    </tr>
                     <tr>
                        <td class="unhandled_error_message">
                            The page might under be construction
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Please contact your administrator
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message_link_message">
                            Click here to go to <asp:LinkButton ID="PageUnderConstruction_homeLinkButton" runat="server" Text="Home" CssClass="unhandled_error_message_link_btn" PostBackUrl="~/Default.aspx" ToolTip="Click here to go to home page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message_link_message">
                            Click here to go to <asp:LinkButton ID="PageUnderConstruction_feedbackLinkButton" runat="server" Text="Feedback" CssClass="unhandled_error_message_link_btn" PostBackUrl="~/General/Feedback.aspx" ToolTip="Click here to go to feedback page">
                            </asp:LinkButton> page
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 60%" class="under_construction">
            </td>
        </tr>
    </table>   
</asp:Content>
