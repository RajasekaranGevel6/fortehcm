﻿using System;
using System.Web.UI;

using Forte.HCM.Trace;

namespace Forte.HCM.UI.Common
{
    public partial class UnhandledError : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Unhandled Error");

                if (Session["UNHANDLED_ERROR"] != null)
                {
                    UnhandledError_detailsLabel.Text = Session["UNHANDLED_ERROR"].ToString().Trim();
                }
                else
                {
                    UnhandledError_detailsLabel.Text = "None";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
    }
}