﻿using System;
using System.Web;
using System.Web.SessionState;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.Common
{
    /// <summary>
    /// Summary description for Datahandler
    /// </summary>
    public class Datahandler : IHttpHandler,IReadOnlySessionState, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string startQstring = context.Request.QueryString["start"];
            string nextQstring = context.Request.QueryString["next"];
            //null check
            if ((!string.IsNullOrEmpty(startQstring)) && (!string.IsNullOrEmpty(nextQstring)))
            {
                //convert string to int
                int start = Convert.ToInt32(startQstring);
                int next = Convert.ToInt32(nextQstring);

                //setting content type
                context.Response.ContentType = "text/plain"; 

                if (!Utility.IsNullOrEmpty(context.Session["INTERVIEW_SEARCH_CRITERIA"]))
                {
                    InterviewSearchCriteria searchInterview =
                        context.Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria;
                    searchInterview.CurrentPage = 1;
                    searchInterview.PageSize= next;
                    //writing response   
                    context.Response.Write(new OnlineInterviewAssessorBLManager().GetAjaxContent(searchInterview));
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}