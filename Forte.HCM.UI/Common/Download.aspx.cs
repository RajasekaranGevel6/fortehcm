﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Download.cs
// File that represents the download functionality.
// to the users.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

using iTextSharp.text; 
using iTextSharp.text.pdf;


#endregion Directives                                                          

namespace Forte.HCM.UI.Common
{
    /// <summary>
    /// Class that represents the download functionality.
    /// </summary>
    public partial class Download : Page
    {
        #region Static Variables                                               

        static BaseColor itemColor = new BaseColor(20, 142, 192);
        static BaseColor headerColor = new BaseColor(40, 48, 51);

        #endregion Static Variables                                            

        #region Private Constants                                              

        /// <summary>
        /// A <see cref="int"/> that holds the header detail table index.
        /// </summary>
        private const int HEADER_DETAIL_TABLE_INDEX = 0;

        #endregion Private Constants

        #region Private Variables                                              

        /// <summary>
        /// A <see cref="int"/> that hold the position profile ID.
        /// </summary>
        private int positionProfileID = 0;     


        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Method that is called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e"> 
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FileDownload_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                    return;

                 // Retrieve the document Id.
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    DownloadResume(Convert.ToInt32(Request.QueryString["id"]));
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["document"]))
                {
                    DownloadDocument(Request.QueryString["document"],Convert.ToInt32(Request.QueryString["documentid"]));
                }
                else if(!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "TEMPORARYRESUME" && !Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                {
                    DownloadTemporaryResume(Convert.ToInt32(Request.QueryString["candidateid"]));
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "PDF")
                {
                    DownloadPDF();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "DOWNLOADRESUME")
                {
                    DownloadCandidateResume(Request.QueryString["fname"], Request.QueryString["mtype"]);
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "DOWNLOADPROFILEPRINTCANDIDATERESUME")
                {
                    DownloadProfilePrintCandidateResume(Request.QueryString["candidateid"]);
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                   Request.QueryString["type"].ToUpper() == "DOWNLOADDOCUMENTS")
                {
                    DownloadAdditionalDocuments(Request.QueryString["documentId"].ToString());
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "CPLOGFILE")
                {
                    DownloadCyberProctorLogFile(Convert.ToInt32(Request.QueryString["logid"]), 
                        Request.QueryString["systemname"]);
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "OILOGFILE")
                {
                    DownloadOfflineInterviewLogFile(Convert.ToInt32(Request.QueryString["logid"]),
                        Request.QueryString["systemname"]);
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "CERTIFICATE")
                {
                    DownloadCertificate();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "CERTIFICATE_TEMPLATE")
                {
                    DownloadCertificateTemplate();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "REPORT")
                {
                    DownloadReport();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                   Request.QueryString["type"].ToUpper() == "PP")
                {
                    DownloadPositionProfile();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                   Request.QueryString["type"].ToUpper() == "IV")
                {
                    DownloadInterviewVideo();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "RU")
                {
                    DownloadResumeUploaderUtility();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "BTQT")
                {
                    // Batch test quesions template.
                    DownloadBatchTestQuestionsTemplate();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "OTBTQT")
                {
                    // Batch test quesions template.
                    DownloadOpenTextBatchTestQuestionsTemplate();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                Request.QueryString["type"].ToUpper() == "BSCT")
                {
                    // Bulk Schedule Candidate template.
                    DownloadBulkScheduleCandidateTemplate();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "BIQT")
                {
                    // Batch interview quesions template.
                    DownloadBatchInterviewQuestionsTemplate();
                }
                    //Downloading position profile review pdf
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                      Request.QueryString["type"].ToUpper() == "PP_REVIEW_PDF")
                {
                    // Get position profile ID.
                    int.TryParse(Request.QueryString["positionprofileid"], out positionProfileID);
                    DataSet rawData = new PositionProfileBLManager().GetPositionProfileSummary(positionProfileID, 0);
                    if (rawData != null && rawData.Tables.Count > 0)
                    {
                        CandidateReportDetail candidateReportDetail;
                        PDFDocumentWriter PdfWriter = new PDFDocumentWriter();
                        string logoPath = Server.MapPath("~/");
                        PDFHeader(out candidateReportDetail);
                        PDFHeader(out candidateReportDetail);
                        DataTable TableHeader = rawData.Tables[HEADER_DETAIL_TABLE_INDEX];
                        DataRow dataRow = TableHeader.Rows[0];
                        string fileName = dataRow["POSITION_PROFILE_NAME"].ToString().Trim();
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName.Replace(" ", "_") + ".pdf");
                        Response.ContentType = "application/pdf";
                        Response.BinaryWrite(PdfWriter.GetPostionprofileReview(rawData, logoPath, candidateReportDetail));
                        Response.End();                       
                    }
                }
                else if(!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                      Request.QueryString["type"].ToUpper() == "OIPDF")
                {
                    DownloadOnlineInterviewAssessment(Request.QueryString["interviewkey"].ToString(),
                        Request.QueryString["chatroomid"].ToString(), Convert.ToInt32(Request.QueryString["interviewid"]), Convert.ToInt32(Request.QueryString["rpttype"]));
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "ADDITIONALDOC" )
                {
                    string docPath = Request.QueryString["docpath"];
                    DownloadAdditionalDocumentUtility(docPath);
                }
                
                 
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                throw ex;
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                
 

        /// <summary>
        /// Method that retrieves the header for pdf.
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A <see cref="CandidateReportDetail"/> that holds the user detail.
        /// </param>
        private void PDFHeader(out CandidateReportDetail candidateReportDetail)
        {
            candidateReportDetail = new CandidateReportDetail();
            UserDetail userDetail = new UserDetail();
            userDetail = (UserDetail)Session["USER_DETAIL"];
            candidateReportDetail.LoginUser = userDetail.FirstName + " " + userDetail.LastName;
        }
        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        private void DownloadReport()
        {
            byte[] candidateResults =
                (byte[])Session[Constants.SessionConstants.REPORT_RESULTS_PDF];
            if (candidateResults == null)
            {
                Logger.ExceptionLog("No PDF is found to download");
                return;
            }
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Request.QueryString["filename"]);
            Response.AddHeader("Content-Length", candidateResults.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(candidateResults);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        private void DownloadPDF()
        {
            byte[] candidateResults =
                (byte[])Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF];

            if (candidateResults == null)
            {
                Logger.ExceptionLog("No PDF is found to download");
                return;
            }
            string fileName = string.Empty;
            if (Session["CAND_SESSION_KEY"] != null
                && Session["ATTEMPT_ID"] != null)
                fileName = Session["CAND_SESSION_KEY"].ToString() + "_"
                    + Session["ATTEMPT_ID"].ToString();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + "pdf");
            Response.AddHeader("Content-Length", candidateResults.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(candidateResults);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        private void DownloadCandidateResume(string fileName, string mimeType)
        {
            try
            {
                 Response.ClearHeaders();
                Response.Clear();
                byte[] resumeContent =
                    (byte[])Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT];
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);// + "." + mimeType);
                Response.AddHeader("Content-Length", resumeContent.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(resumeContent);
                Response.Flush();
                 
                Response.End();
            }
            catch  
            {
               // Response.End();
            }
        }

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateId">
        /// A <see cref="string"/> that holds the candidate id.
        /// </param>
        private void DownloadProfilePrintCandidateResume(string candidateId)
        {
            try
            {
                Response.ClearHeaders();
                Response.Clear();
                
                int candidateID = 0;
                if (!Utility.IsNullOrEmpty(candidateId))
                    candidateID = Convert.ToInt32(candidateId);

                string fileName = string.Empty;
                string mimeType = string.Empty;

                byte[] candidateResumeContent = new CandidateBLManager().GetResumeContentsByCandidiateID(candidateID,
                    out mimeType, out fileName);

                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.AddHeader("Content-Length", candidateResumeContent.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(candidateResumeContent);
                Response.Flush();

                Response.End();
            }
            catch
            {
                // Response.End();
            }
        }

        /// <summary>
        /// Method to download the additional documents attached to  mail
        /// </summary>
        /// <param name="documentId"></param>
        private void DownloadAdditionalDocuments(string documentId)
        {
            try
            {
                string fileName = string.Empty;
                string mimeType = string.Empty;
                int docId = Convert.ToInt32(documentId);
                byte[] documentContent = new CandidateBLManager().GetDocumentContentsByDocumentId(docId, out fileName);
               
                if (documentContent == null)
                {
                    Logger.ExceptionLog("No document contents found to in the database to download");
                    Response.End();
                }
                mimeType = Path.GetExtension(fileName);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + mimeType);
                Response.AddHeader("Content-Length", documentContent.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(documentContent);
                Response.Flush();
                Response.End();
            }
            finally
            {
                Response.End();
            }
           
        }

        private void DownloadDocument(string fileName, int documentId)
        {
            try
            {
                byte[] documentContent = new PositionProfileBLManager().GetAdditionalDocumentContent(documentId);

                if (documentContent == null)
                {
                    Logger.ExceptionLog("No document contents found to in the database to download");
                    Response.End();
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName.Replace(" ", "_"));
                Response.AddHeader("Content-Length", documentContent.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(documentContent);
                Response.Flush();
                Response.End();
            }
            finally
            {
                Response.End();
            }
        }


        /// <summary>
        /// Method that will download the certificate for the completed test.
        /// </summary>
        private void DownloadCertificate()
        {
            byte[] certificateImage = (byte[])Session[Constants.SessionConstants.CERTIFICATE_IMAGE];
            string certificateFileName = "Certificate";

            if (certificateImage == null)
            {
                Logger.ExceptionLog("No image is found to download");
                return;
            }

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + certificateFileName + ".jpg");
            Response.AddHeader("Content-Length", certificateImage.Length.ToString());
            Response.ContentType = "image/JPEG";
            Response.BinaryWrite(certificateImage);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that will download the certifcate template.
        /// </summary>
        private void DownloadCertificateTemplate()
        {
            byte[] certificateImage = (byte[])Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE];
            string certificateFileName = "CertificateTemplate";

            if (certificateImage == null)
            {
                Logger.ExceptionLog("No image is found to download");
                return;
            }

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + certificateFileName + ".jpg");
            Response.AddHeader("Content-Length", certificateImage.Length.ToString());
            Response.ContentType = "image/JPEG";
            Response.BinaryWrite(certificateImage);
            Response.Flush();
            Response.End();
        }


        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        private void DownloadResume(int candidateResumeID)
        {
            try
            {
                string mimeType;
                string fileName;
                byte[] resumeContents = new ResumeRepositoryBLManager().GetResumeContents
                    (candidateResumeID, out mimeType, out fileName);

                if (resumeContents == null)
                {
                    Logger.ExceptionLog("No resume contents found to in the database to download");
                    Response.End();
                }
                 

                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + mimeType);
                //Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.AddHeader("Content-Length", resumeContents.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(resumeContents);
                Response.Flush();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
            }
            finally
            {
                Response.End();
            }
        }

        private void DownloadTemporaryResume(int candidateID)
        { 
             try
            {
                string mimeType;
                string fileName;
                byte[] resumeContents = new ResumeRepositoryBLManager().GetTemporaryResumeContents
                    (candidateID, out mimeType, out fileName);

                if (resumeContents == null)
                {
                    Logger.ExceptionLog("No resume contents found to in the database to download");
                    Response.End();
                }
                 

                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + mimeType);
                //Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.AddHeader("Content-Length", resumeContents.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(resumeContents);
                Response.Flush();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
            }
            finally
            {
                Response.End();
            }
            
        }

        /// <summary>
        /// Method that downloads the log file taken from the candidate machine 
        /// during test session.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        private void DownloadCyberProctorLogFile(int logID, string systemName)
        {
            try
            {
                byte[] resumeContents = new TrackingBLManager().GetCyberProctorLogFile(logID);

                if (resumeContents == null)
                {
                    Logger.ExceptionLog("No log file contents found to display");
                    Response.End();
                }

                string fileName = systemName + "_CyberProctorExceptionLog";
                string mimeType = "txt";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + mimeType);
                Response.AddHeader("Content-Length", resumeContents.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(resumeContents);
                Response.Flush();
                Response.End();
            }
            finally
            {
                Response.End();
            }
        }

        /// <summary>
        /// Method that downloads the log file taken from the candidate machine 
        /// during interview session.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        private void DownloadOfflineInterviewLogFile(int logID, string systemName)
        {
            try
            {
                byte[] resumeContents = new TrackingBLManager().GetOfflineInterviewLogFile(logID);

                if (resumeContents == null)
                {
                    Logger.ExceptionLog("No log file contents found to display");
                    Response.End();
                }

                string fileName = systemName + "_OfflineInterviewExceptionLog";
                string mimeType = "txt";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + mimeType);
                Response.AddHeader("Content-Length", resumeContents.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(resumeContents);
                Response.Flush();
                Response.End();
            }
            finally
            {
                Response.End();
            }
        }

        private void DownloadPositionProfile()
        {
            try
            {
                if (Utility.IsNullOrEmpty(Session[Constants.SessionConstants.PP_PDF_HEADER]) || Utility.IsNullOrEmpty(Session[Constants.SessionConstants.PP_PDF_CONTENT]))
                    return;
                CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
                candidateReportDetail = (CandidateReportDetail)Session[Constants.SessionConstants.PP_PDF_HEADER];
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + candidateReportDetail.TestName.Replace(" ", "_") + ".pdf");
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(new WidgetReport().GetPDFPPByteArray("POSITION PROFILE", (List<Dictionary<object, object>>)Session[Constants.SessionConstants.PP_PDF_CONTENT], candidateReportDetail));
                Response.End();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        private void DownloadInterviewVideo()
        {
            try
            {
                // Check if candidate session ID is present.
                if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
                {
                    Logger.ExceptionLog("No candidate session ID found to download interview video");
                    Response.End();
                    return;
                }

                string candidateSessionID = Request.QueryString["candidatesessionid"].Trim();

                // Check if attempt ID is present.
                if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                {
                    Logger.ExceptionLog("No attempt ID found to download interview video");
                    Response.End();
                    return;
                }
                string attemptID = Request.QueryString["attemptid"].Trim();

                // Check if test question ID is present.
                if (Utility.IsNullOrEmpty(Request.QueryString["testquestionid"]))
                {
                    Logger.ExceptionLog("No test question ID found to download interview video");
                    Response.End();
                    return;
                }

                string testQuestionID = Request.QueryString["testquestionid"].Trim();

                string fileName = candidateSessionID + "_" + attemptID + "/" + testQuestionID +
                    "." + ConfigurationManager.AppSettings["VIDEO_FILE_EXTENSION"];

                string path = "~/" + ConfigurationManager.AppSettings["INTERVIEW_VIDEO_PATH"] +
                    "/" + fileName;

                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.ContentType = "application/octet-stream";
                Response.TransmitFile(Server.MapPath(path));
                Response.Flush();
                Response.End();
            }
            finally
            {
                Response.End();
            }
        }

        /// <summary>
        /// Method that downloads the resume uploader utility.
        /// </summary>
        private void DownloadResumeUploaderUtility()
        {
            string fileName = ConfigurationManager.AppSettings["RESUME_UPLOADER_ZIP"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the Bulk Schedule Candidate template.
        /// </summary>
        private void DownloadBulkScheduleCandidateTemplate()
        {
            string fileName = ConfigurationManager.AppSettings["BULK_SCHEDULE_CANDIDATE_TEMPLATE"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the batch test questions template.
        /// </summary>
        private void DownloadBatchTestQuestionsTemplate()
        {
            string fileName = ConfigurationManager.AppSettings["BATCH_TEST_QUESTIONS_TEMPLATE"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the batch test questions template.
        /// </summary>
        private void DownloadOpenTextBatchTestQuestionsTemplate()
        {
            string fileName = ConfigurationManager.AppSettings["OPEN_TEXT_BATCH_TEST_QUESTIONS_TEMPLATE"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the batch interview questions template.
        /// </summary>
        private void DownloadBatchInterviewQuestionsTemplate()
        {
            string fileName = ConfigurationManager.AppSettings["BATCH_INTERVIEW_QUESTIONS_TEMPLATE"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        private void DownloadOnlineInterviewAssessment(string interviewKey,string chatRoomid,int interviewid,int rptType)
        {
            DataTable assessmentTable = new DataTable();

            CandidateInterviewSessionDetail sessionDetail = new OnlineInterviewAssessorBLManager().GetOnlineInterviewDetail
                    (interviewKey, chatRoomid);

            PDFDocumentWriter pdfWriter = new PDFDocumentWriter();

            string fileName = sessionDetail.CandidateName + "_" + "InterviewAssessmentSummary.pdf";


            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Document doc = new Document(PDFDocumentWriter.paper,
                PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

            string logoPath = Server.MapPath("~/");

            iTextSharp.text.Image imgBackground = iTextSharp.text.Image.GetInstance(logoPath + "Images/AssessmentBackground.jpg");

            imgBackground.ScaleToFit(1000, 132);
            imgBackground.Alignment = iTextSharp.text.Image.UNDERLYING;
            imgBackground.SetAbsolutePosition(50, 360);

            PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(logoPath, "Interview Assessment Report"));
            pageHeader.SpacingAfter = 10f;

            //Construct Table Structures    
            PdfPTable tableHeaderDetail = new PdfPTable(1);
            PdfPCell cellBorder = new PdfPCell(new Phrase(""));
            cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            cellBorder.Border = 0;
            tableHeaderDetail.WidthPercentage = 90;
            cellBorder.PaddingTop = 20f;

            DataSet rawData = new DataSet();

            cellBorder.AddElement(pdfWriter.OnlineInterviewTableHeader(sessionDetail, interviewid,
                GetCandidateImageFile(sessionDetail.CandidateInformationID), rptType, out rawData));

            tableHeaderDetail.AddCell(cellBorder);
            tableHeaderDetail.SpacingAfter = 30f;

            //Construct empy table
            PdfPTable emptytable= new PdfPTable(1);
            PdfPCell emptyCell = new PdfPCell(new Phrase("\n\n", iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10)));

            emptyCell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            emptyCell.Border = 0; 
            emptytable.WidthPercentage = 90f;
            emptytable.AddCell(emptyCell);

            //Response Output
            PdfWriter.GetInstance(doc, Response.OutputStream);

            doc.Open();
            doc.Add(pageHeader);
            doc.Add(imgBackground);
            doc.Add(tableHeaderDetail); 

            switch(rptType)
            {
                case 1:
                    //Add title 
                    doc.Add(pdfWriter.TitleTable("Overall Score assigned by assessor(s)"));
                    //Construct asssessor score and comments table 
                    assessmentTable = pdfWriter.SummaryTable(rawData, rptType);
                    doc.Add(pdfWriter.OnlineInterviewAssessorScoreComments(assessmentTable));        
                    break;
                case 2:
                    //Add title 
                    doc.Add(pdfWriter.TitleTable("Overall Score assigned by assessor(s)"));
                    //Construct asssessor score and comments table
                    assessmentTable = pdfWriter.SummaryTable(rawData, 1); 
                    doc.Add(pdfWriter.OnlineInterviewAssessorScoreComments(assessmentTable));
                    
                    //Construct empty table
                    doc.Add(emptytable);

                    //Add title 
                    doc.Add(pdfWriter.TitleTable("Subject Scores"));
                    //Construct skill score table
                    assessmentTable = pdfWriter.SummaryTable(rawData, 2); 
                    doc.Add(pdfWriter.OnlineInterviewTableDetail(assessmentTable));
                    break;
                case 3:
                    //Add title 
                    doc.Add(pdfWriter.TitleTable("Overall Score assigned by assessor(s)"));
                    //Construct asssessor score and comments table
                    assessmentTable = pdfWriter.SummaryTable(rawData, 1);
                    doc.Add(pdfWriter.OnlineInterviewAssessorScoreComments(assessmentTable));

                    //Construct empty table
                    doc.Add(emptytable);

                    //Add title 
                    doc.Add(pdfWriter.TitleTable("Subject Scores"));
                    //Construct skill score table
                    assessmentTable = pdfWriter.SummaryTable(rawData, 2);
                    doc.Add(pdfWriter.OnlineInterviewTableDetail(assessmentTable));

                    //Construct empty table
                    doc.Add(emptytable);

                    //Add title 
                    doc.Add(pdfWriter.TitleTable("Question Response Scores"));
                    //Construct questions score level and specific comments table
                    doc.Add(pdfWriter.OnlineInterviewQuestionsRow(rawData));
                    break;
            }
            
            doc.Close();
        }


        /// <summary>
        /// Method that retrieves the image path for the given candidate ID.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the candidate image path.
        /// </returns>
        private string GetCandidateImageFile(int candidateID)
        {
            string path = null;

            bool isExists = false;
            if (candidateID > 0)
            {
                path = Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];
                if (File.Exists(path))
                    isExists = true;
                else
                    isExists = false;
            }

            if (isExists)
                return path;
            else
            {     // Show the 'not available image'.
                return Server.MapPath("~/") +
                      ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                      "\\" + "photo_not_available.jpg";
            }
        }


        /// <summary>
        /// Method that downloads the resume uploader utility.
        /// </summary>
        private void DownloadAdditionalDocumentUtility(string document)
        {
            int _index = document.LastIndexOf("/");

            string fileName =document.Substring(_index+1);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath(document));
            Response.Flush();
            Response.End();
        }

        #endregion Private Methods                                             
    }
}