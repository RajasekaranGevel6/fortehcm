﻿/*  ----------------------------------------------------------------------------
 *  Kalitte Professional Information Technologies
 *  ----------------------------------------------------------------------------
 *  Dynamic Dashboards
 *  ----------------------------------------------------------------------------
 *  File:       DashboardPage.cs
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Kalitte.Dashboard.Framework;

using Kalitte.Dashboard.Framework.Types;
using System.Threading;
using Kalitte.Dashboard.Framework.Providers;
using System.Configuration;
using System.Security.Principal;
using Forte.HCM.UI.Common;

namespace Kalitte
{
    public abstract class DashboardPage : ReportPageBase
    {
        public bool ShowUserDashboardsMenu
        {
            get
            {
                return Session["ShowEditor"] != null;
            }
        }

        

        protected virtual void BindDashboard()
        {
            Dashboard.DataBind();
        }


        protected override void OnLoad(EventArgs e)
        {

            if (Dashboard != null)
                try
                {
                    if (!Page.IsPostBack)
                        BindDashboard();
                }
                catch (ArgumentException) // Ivalid dashboardKey
                {
                    if (DashboardFramework.Provider is SessionDashboardProvider)  // user has deleted dashboard 
                    {
                        // do nothing
                    }
                    else throw;
                }

            base.OnLoad(e);
        }


        protected virtual void SetDashboardView(DashboardSurface dashboard)
        {
            if (dashboard != null)
            {
              //  dashboard.ViewMode = GetViewMode(dashboard);
              //  dashboard.AddtoColumnText = "&nbsp;&nbsp;<b>Add to</b>&nbsp;";
            }

        }

        protected void SetDashboardProperties(DashboardSurface dashboard)
        {
            SetDashboardView(dashboard);

            if (dashboard != null)
            {
                dashboard.DashboardToolbarPrepare += new DashboardToolbarPrepareHandler(dashboard_DashboardToolbarPrepare);
                dashboard.WidgetPropertiesSetting += new WidgetHandler(Dashboard_WidgetPropertiesSetting);
                dashboard.DashboardCommand += new DashboardCommandHandler(Dashboard_DashboardCommand);
                dashboard.DefaultDashboardViewUrl = "/pages/dynamic/dashboard.aspx?d={0}";
                dashboard.DashboardMenuPrepare += new DashboardMenuPrepareEventHandler(dashboard_DashboardMenuPrepare);
                dashboard.DashboardPropertiesSetting += new WidgetDashboardHandler(dashboard_DashboardPropertiesSetting);
                dashboard.ShowDashboardListPanel = true;

            }
        }

        void dashboard_DashboardPropertiesSetting(object sender, DashboardEventArgs e)
        {
            e.Instance.WidgetCreateAnimation = "fadeIn({ endOpacity: 1, easing: 'easeIn', duration: 2 })";
            e.Instance.WidgetCommandAnimation = "frame('ff0000', 1, { duration: 1 })";
            e.Instance.WidgetDropAnimation = "fadeIn({ endOpacity: 1, easing: 'easeIn', duration: 0.5 })";
            e.Instance.DashboardUpdateAnimation = "frame('C3DAF9', 1, { duration: 1 })";
            e.Instance.ToolCommands.RemoveAll(p => p.CommandName == "Refresh");
            ToolCommand cmd1 = new ToolCommand();
            cmd1.CommandName = "Refresh";
            cmd1.Hint = "Refresh Dashboard";
            cmd1.CommandIcon = Kalitte.Dashboard.Framework.ToolCommandIcon.Refresh;
            cmd1.MaskMessage = "Refreshing dashboard ...";
            e.Instance.ToolCommands.Add(cmd1);

        }


        void dashboard_DashboardMenuPrepare(object sender, DashboardMenuPrepareEventArgs e)
        {
            //var items = (sender as DashboardSurface).GetDefaultDashboardMenuItems();
            //var userItems = items.Where(p => p.Instance.Username == (sender as DashboardSurface).GetUsername()).ToList();
            //items = items.Where(p => p.Instance.Username != (sender as DashboardSurface).GetUsername()).ToList();
            //var userMenu = new List<DashboardMenuItemData>(userItems.Count + 5);
            //string lastGroup = "";
            //int userDisplayOrder = 0;
            //foreach (var userDashboard in userItems)
            //{
            //    if (!string.IsNullOrEmpty(userDashboard.Group) && lastGroup != userDashboard.Group)
            //    {
            //        var groupItem = new DashboardMenuItemData(userDashboard.Instance, userDashboard.Instance.ViewMode);
            //        groupItem.RenderMode = DashboardMenuItemRenderMode.TextMenuItem;
            //        groupItem.DisplayTitle = string.Format("<b class='menu-title'>{0}</b>", userDashboard.Instance.Group);
            //        groupItem.Group = "Your Dashboards";
            //        groupItem.DisplayOrder = userDisplayOrder++;
            //        groupItem.GroupDisplayOrder = int.MaxValue;
            //        userMenu.Add(groupItem);
            //    }
            //    lastGroup = userDashboard.Group;
            //    userDashboard.Group = "Your Dashboards";
            //    userDashboard.GroupDisplayOrder = int.MaxValue;
            //    userDashboard.DisplayOrder = userDisplayOrder++;
            //    userMenu.Add(userDashboard);
            //}

            //e.List.AddRange(items);
            //e.List.AddRange(userMenu);

        }

        void Dashboard_DashboardCommand(object sender, DashboardCommandArgs e)
        {
            //if (e.Command.CommandName == "EditDashboards")
            //{
           // Response.Redirect(string.Format("/Pages/Management/Dashboards.aspx?d={0}", e.Command.Arguments["key"]),false);
            //}
            //else if (e.Command.CommandName == "changeTheme")
            //{

            //    string newTheme = e.Command.Arguments.ContainsKey("theme") ? e.Command.Arguments["theme"].ToString() : "";
            //    if (string.IsNullOrEmpty(newTheme))
            //    {
            //        string[] themes = Enum.GetNames(typeof(DashboardTheme));
            //        newTheme = themes[new Random().Next(themes.Length)];
            //    }

            //   // WebSiteUtil.ChangeTheme(newTheme);
            //}
            //else if (e.Command.CommandName == "Refresh")
            //{
            //    Dashboard.Refresh();
            //}
        }

        void dashboard_DashboardToolbarPrepare(object sender, DashboardToolbarPrepareEventArgs e)
        {
            //if ((sender as DashboardSurface).ViewMode == WidgetViewMode.Edit)
            //{
            //    e.Toolbar.AddItem(new DashboardToolbarSeperator("ctlSeperator1"));
            //    DashboardToolbarButton btnEdit = new DashboardToolbarButton("ctlEditDashboard")
            //    {
            //        Text = "Edit Dashboard",
            //        Icon = WidgetIcon.PageEdit,
            //        CommandName = "EditDashboards",
            //        Hint = "<b>Edit Dashboard</b><br/>Click to edit this dashboard.",
            //        MaskMessage = "Loading dashboard editor ..."
            //    };
            //    btnEdit.Arguments.Add("key", e.Instance.InstanceKey.ToString());
            //    e.Toolbar.AddItem(btnEdit);
            //}
            //DashboardToolbarSplitButton themeBtn = new DashboardToolbarSplitButton("changeTheme", "Theme", WidgetIcon.Theme);
            //themeBtn.CommandName = "changeTheme";
            //themeBtn.MaskMessage = "Selecting a random theme ...";
            //DashboardMenu themeMenu = new DashboardMenu("themeMenu");
            //foreach (string theme in Enum.GetNames(typeof(DashboardTheme)))
            //{

            //    DashboardMenuItem item = new DashboardMenuItem(theme, string.Format("<em class='theme {0}'>{1}</em>", theme, theme), WidgetIcon.None);
            //    item.Icon = Page.Theme == theme ? WidgetIcon.BulletBlack : WidgetIcon.None;
            //    item.CommandName = "changeTheme";
            //    item.Arguments.Add("theme", theme);
            //    item.MaskMessage = string.Format("Loading <b>{0}</b> theme ...", theme);
            //    themeMenu.AddItem(item);

            //}
            //themeBtn.Menu = themeMenu;
            //e.Toolbar.AddItem(new DashboardToolbarSeperator("ctlSeperator2"));
            //e.Toolbar.AddItem(themeBtn);

        }

        protected virtual bool UseQueryStringForDashboardKey
        {
            get
            {
                return false;
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            //if (ConfigurationManager.AppSettings["Deployed"] == "true")
            //{
            //    IPrincipal principal = new GenericPrincipal(new GenericIdentity(Session.SessionID), new string[] { }); ;
            //    Thread.CurrentPrincipal = principal;
            //    HttpContext.Current.User = principal;

            //   // if (!DashboardInitializer.Inited)
            //  //      DashboardInitializer.Init();
            //}
            base.OnPreInit(e);
        }

        protected override void OnInit(EventArgs e)
        {
            if (Dashboard == null)
                base.OnInit(e);

            else if (UseQueryStringForDashboardKey)
            {
                string key = Request["d"];
                bool keyValid = false;
                if (!string.IsNullOrEmpty(key))
                {
                    try
                    {
                        DashboardInstance d = DashboardFramework.GetDashboard(key);
                        keyValid = d != null;
                    }
                    catch
                    {
                        keyValid = false;
                    }
                }
                else keyValid = false;
                if (keyValid)
                    Dashboard.DashboardKey = key;
                else
                {
                    Response.Redirect("~/", true);
                }
            }

            SetAutoEditMode();
            SetDashboardProperties(Dashboard);
            MaintainScrollPositionOnPostBack = false;
            base.OnInit(e);

        }

        protected virtual void SetAutoEditMode()
        {
            //if (DashboardFramework.Provider is SessionDashboardProvider)
            {
                if (Session["ShowEditor"] == null & Session["AutoEditorShowed"] == null)
                {
                    Session["ShowEditor"] = "true";
                    Session["AutoEditorShowed"] = true;
                }
            }
        }



        void Dashboard_WidgetPropertiesSetting(object sender, WidgetEventArgs e)
        {
            if (!e.Instance.PanelSettings.Header && ((DashboardSurface)sender).ViewMode == WidgetViewMode.Edit)
            {
                e.Instance.PanelSettings.Header = true;
                e.Instance.WidgetSettings["__set0"] = true;
                if (e.Instance.PanelSettings.HeaderDisplayMode != WidgetHeaderDisplayMode.MouseEnter)
                {
                    e.Instance.WidgetSettings["__set1"] = true;
                    e.Instance.PanelSettings.HeaderDisplayMode = WidgetHeaderDisplayMode.MouseEnter;
                }
            }
            else
            {
                if (e.Instance.WidgetSettings.ContainsKey("__set0"))
                    e.Instance.PanelSettings.Header = false;
                if (e.Instance.WidgetSettings.ContainsKey("__set1"))
                {
                    e.Instance.PanelSettings.HeaderDisplayMode = WidgetHeaderDisplayMode.Always;
                }
            }
        }

        protected abstract DashboardSurface Dashboard
        {
            get;
        }

    }
}
