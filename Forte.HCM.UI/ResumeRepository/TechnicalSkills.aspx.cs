﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TechnicalSkills.aspx.cs
// File that represents the page used to add or edit technical skills 
// details

#endregion Header

#region Directives

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class TechnicalSkills : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "273px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "350px";

        #endregion Declaration

        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Technical Skills");

                // Set default button.
                Page.Form.DefaultButton = TechnicalSkills_searchButton.UniqueID;

                //Set default Focus
                TechnicalSkills_groupTypeDropDownList.Focus();

                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    //Load the technical group in the technical group drop down list 
                    LoadValues();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "GROUP_NAME";

                    TechnicalSkills_resultDiv.Visible = false;
                    TechnicalSkills_bottomPagingNavigator.Visible = false;
                }

                TechnicalSkills_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestoreDiv('" +
                TechnicalSkills_resultDiv.ClientID + "','" +
                TechnicalSkills_searchDiv.ClientID + "','" +
                TechnicalSkills_searchResultsUpSpan.ClientID + "','" +
                TechnicalSkills_searchResultsDownSpan.ClientID + "','" +
                TechnicalSkills_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

                TechnicalSkills_bottomSuccessMessageLabel.Text = string.Empty;
                TechnicalSkills_topSuccessMessageLabel.Text = string.Empty;
                TechnicalSkills_bottomErrorMessageLabel.Text = string.Empty;
                TechnicalSkills_topErrorMessageLabel.Text = string.Empty;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                   TechnicalSkills_bottomErrorMessageLabel,
                 exception.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on reset link button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void TechnicalSkills_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                   TechnicalSkills_bottomErrorMessageLabel,
                 exception.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on search button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void TechnicalSkills_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Make the result div as visible
                TechnicalSkills_resultDiv.Visible = true;
                TechnicalSkills_bottomPagingNavigator.Visible = true;

                ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = "GROUP_NAME";

                TechnicalSkills_bottomPagingNavigator.Reset();

                //Load the technical skills details
                LoadTechnicalSkillDetails(1);
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                    TechnicalSkills_bottomErrorMessageLabel, exp.Message);
            }

        }
        /// <summary>
        /// Represents the event handler that is called on the save button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the sender of the object
        /// </param>
        protected void TechnicalSkills_editSkillsaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Check the mandatory field
                if (TechnicalSkills_editSkill_aliasTextBox.Text.Trim().Length == 0)
                {
                    TechnicalSkills_editSkill_errorLabel.Text = "Alias cannot be empty";
                    TechnicalSkills_editSkill_saveButton.Focus();
                    TechnicalSkills_editSkillModalPopUpExtender.Show();
                    return;
                }

                //Update the alias field for the given skills
                int skillID = int.Parse(TechnicalSkills_editSkillIDHiddenField.Value);
                string skillAliasName = TechnicalSkills_editSkill_aliasTextBox.Text.Trim();

                if (CheckEnteredSkillsValues(TechnicalSkills_editSkill_aliasTextBox))
                {
                    new ResumeRepositoryBLManager().UpdateTechnicalSkillsAliasName
                        (skillID, skillAliasName);

                    base.ShowMessage(TechnicalSkills_topSuccessMessageLabel,
                      TechnicalSkills_bottomSuccessMessageLabel, Resources.HCMResource.
                    CompetencyVectorManager_updateAliasSuccessMessage);

                    LoadTechnicalSkillDetails(1);

                }
                else
                {
                    TechnicalSkills_editSkill_errorLabel.Text =
                   Resources.HCMResource.CompetencyVectorManager_DuplicateNotAllowedMessage;
                    TechnicalSkills_editSkill_saveButton.Focus();
                    TechnicalSkills_editSkillModalPopUpExtender.Show();
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                  TechnicalSkills_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on the save button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the sender of the object
        /// </param>
        protected void TechnicalSkills_deletePopup_yesValidButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the skill id from the hidden field
                int skillID = int.Parse(TechnicalSkills_editSkillIDHiddenField.Value);

                //Check if the skill is involved in the 
                bool isSkillIdInvolved = new ResumeRepositoryBLManager().CheckSkillIDInvolved(skillID);

                //if it is not involved delete the skill
                if (isSkillIdInvolved)
                {
                    new ResumeRepositoryBLManager().DeleteTechnicalSkill(skillID);

                    base.ShowMessage(TechnicalSkills_topSuccessMessageLabel,
                  TechnicalSkills_bottomSuccessMessageLabel, Resources.HCMResource.
                  TechnicalSkills_DeleteSuccessMessage);
                    LoadTechnicalSkillDetails(1);
                }
                else
                {
                    TechnicalSkills_deletePopup_errorLabel.Text = Resources.HCMResource.
                        TechnicalSkills_SkillInvolved;
                    TechnicalSkills_deletePopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                    TechnicalSkills_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on grid view row command 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewCommandEventArgs"/>that holds the event args
        /// </param>
        protected void TechnicalSkills_searchGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField skillGroupNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                    ("TechnicalSkills_groupNameHiddenField") as HiddenField;

                HiddenField skillNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                   ("TechnicalSkills_skillsNameHiddenField") as HiddenField;

                HiddenField skillAliasNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                   ("TechnicalSkills_skillsAliasNameHiddenField") as HiddenField;

                HiddenField skillIDHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                    ("TechnicalSkills_skillIDHiddenField") as HiddenField;

                if (e.CommandName == "EditSkill")
                {
                    //Get the skills value and show the modal pop up extender
                    TechnicalSkills_editSkill_errorLabel.Text = string.Empty;
                    TechnicalSkills_editSkill_groupNameValueLabel.Text = skillGroupNameHiddenField.Value.Trim();
                    TechnicalSkills_editSkill_skillNameValueLabel.Text = skillNameHiddenField.Value.Trim();
                    TechnicalSkills_editSkill_aliasTextBox.Text = skillAliasNameHiddenField.Value.Trim();
                    TechnicalSkills_editSkillIDHiddenField.Value = skillIDHiddenField.Value;
                    TechnicalSkills_editSkill_saveButton.Focus();
                    TechnicalSkills_editSkillModalPopUpExtender.Show();
                }
                if (e.CommandName == "DeleteSkill")
                {
                    TechnicalSkills_deletePopup_messageConfirmLabel.Text = string.Format(Resources.HCMResource.
                        TechnicalSkills_DeleteSkillConfirmMessage, skillNameHiddenField.Value);
                    TechnicalSkills_deletePopup_errorLabel.Text = string.Empty;
                    TechnicalSkills_editSkillIDHiddenField.Value = skillIDHiddenField.Value;
                    TechnicalSkills_deletePopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                  TechnicalSkills_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on the grid view 
        /// row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void TechnicalSkills_searchGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                   TechnicalSkills_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row is created in the grid
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param
        protected void TechnicalSkills_searchGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //Add the sort image for the header row
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (TechnicalSkills_searchGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                   TechnicalSkills_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event that is called on page number click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="PageNumberEventArgs"/>that holds the events args
        /// </param>
        protected void TechnicalSkills_pageNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                //Load the grid with the current page number
                LoadTechnicalSkillDetails(e.PageNumber);
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                    TechnicalSkills_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on add skill link button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void TechnicalSkills_addSkillLinkButton_Click(object sender, EventArgs e)
        {
            //Assign the skills group details to the drop down list
            List<TechnicalSkillsGroup> competencyGroupList = new ResumeRepositoryBLManager().GetTechnicalGroup();

            TechnicalSkills_addNewSkillGroupNameDropDownList.DataSource = competencyGroupList;
            TechnicalSkills_addNewSkillGroupNameDropDownList.DataTextField = "GroupName";
            TechnicalSkills_addNewSkillGroupNameDropDownList.DataValueField = "GroupID";
            TechnicalSkills_addNewSkillGroupNameDropDownList.DataBind();

            //Clear the values 
            CTechnicalSkills_addNewSkillSkillNameTextBox.Text = string.Empty;
            TechnicalSkills_addNewSkillAliasTextBox.Text = string.Empty;
            CTechnicalSkills_addNewSkillErrorLabel.Text = string.Empty;

            TechnicalSkills_addNewSkillSaveButton.Focus();


            //Show the add new skill modal pop up
            TechnicalSkills_addNewSkillModalPopupExtender.Show();

        }

        /// <summary>
        /// Represents the event handler that is on the add new button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void TechnicalSkills_addNewSkillSaveButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                //Skills name and alias name should not be emtpy
                if ((CTechnicalSkills_addNewSkillSkillNameTextBox.Text.Trim().Length == 0) ||
                    (TechnicalSkills_addNewSkillAliasTextBox.Text.Trim().Length == 0))
                {
                    CTechnicalSkills_addNewSkillErrorLabel.Text =
                       "Please enter mandatory fields";
                    TechnicalSkills_addNewSkillSaveButton.Focus();
                    TechnicalSkills_addNewSkillModalPopupExtender.Show();
                }
                else
                {
                    if (CheckEnteredSkillsValues(TechnicalSkills_addNewSkillAliasTextBox))
                    {
                        //Save the skills to the Data base
                        int techGroupID = int.Parse
                            (TechnicalSkills_addNewSkillGroupNameDropDownList.SelectedValue);
                        string skillName = CTechnicalSkills_addNewSkillSkillNameTextBox.Text.Trim();
                        string aliasName = TechnicalSkills_addNewSkillAliasTextBox.Text.Trim();
                        if (new ResumeRepositoryBLManager().AddNewSkills(techGroupID, skillName,
                            aliasName, base.userID))
                        {
                            //insert the entry into the competency vector table

                            bool value = new CompetencyVectorBLManager().AddNewVector(Constants.
                                CompetencyVectorGroupConstants.TECHNICAL_SKILLS,
                                skillName, aliasName, base.userID);


                            base.ShowMessage(TechnicalSkills_topSuccessMessageLabel,
                                TechnicalSkills_bottomSuccessMessageLabel,
                                 Resources.HCMResource.TechnicalSkills_NewSkillAddedSuccessMessage);
                            LoadTechnicalSkillDetails(1);
                        }
                        else
                        {
                            CTechnicalSkills_addNewSkillErrorLabel.Text =
                                     Resources.HCMResource.TechnicalSkills_SkillAlreadyExistMessage;

                            TechnicalSkills_addNewSkillSaveButton.Focus();
                            TechnicalSkills_addNewSkillModalPopupExtender.Show();
                        }
                    }
                    else
                    {
                        CTechnicalSkills_addNewSkillErrorLabel.Text =
                       Resources.HCMResource.CompetencyVectorManager_DuplicateNotAllowedMessage;
                        TechnicalSkills_addNewSkillSaveButton.Focus();
                        TechnicalSkills_addNewSkillModalPopupExtender.Show();
                    }
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                    TechnicalSkills_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the handler that is called on sorting
        /// of the grid view
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void TechnicalSkills_searchGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                LoadTechnicalSkillDetails(1);

                TechnicalSkills_bottomPagingNavigator.Reset();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TechnicalSkills_topErrorMessageLabel,
                    TechnicalSkills_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handler

        #region Private Methods

        /// <summary>
        /// Represents the method that is used to check the entered skill values
        /// </summary>
        /// <param name="aliasTextBox">
        /// A<see cref="TextBox"/>that holds the alias text box name
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds whether the value entered in text box is 
        /// valid or not
        /// </returns>
        private bool CheckEnteredSkillsValues(TextBox aliasTextBox)
        {
            string aliasName = aliasTextBox.Text.Trim();
            //Get the individual string values
            string[] separatedAliasName = aliasName.Split(new char[] { ',' });
            //Get the distinct string count
            int numberOfStrings = separatedAliasName.Distinct().Count();
            //if the number of string is equal to length return true
            return numberOfStrings == separatedAliasName.Length ? true : false;
        }

        /// <summary>
        /// Represents the method to load the technical skill details
        /// </summary>
        /// <param name="pagenumber">
        /// A<see cref="int"/>that holds the page number 
        /// </param>
        private void LoadTechnicalSkillDetails(int pageNumber)
        {

            int total = 0;
            int techGroupId = int.Parse(TechnicalSkills_groupTypeDropDownList.SelectedValue);
            string techSkillName = TechnicalSkills_skillNameTextBox.Text.Trim();
            string techSkillAliasName = TechnicalSkills_skillsAliasTextBox.Text.Trim();

            //Get the  technical skills details from the database
            List<TechnicalSkillsDetails> technicalSkillsDetails = new ResumeRepositoryBLManager().
                GetTechSkillsDetails(techGroupId, techSkillName, techSkillAliasName, ViewState["SORT_FIELD"].ToString(),
                ViewState["SORT_ORDER"].ToString(), pageNumber, base.GridPageSize, out total);

            if (technicalSkillsDetails != null && technicalSkillsDetails.Count != 0)
            {
                TechnicalSkills_searchGridView.DataSource = technicalSkillsDetails;
                TechnicalSkills_searchGridView.DataBind();
            }
            else
            {
                TechnicalSkills_searchGridView.DataSource = technicalSkillsDetails;
                TechnicalSkills_searchGridView.DataBind();

                if (TechnicalSkills_topSuccessMessageLabel.Text.Length != 0)
                {
                    //show the default empty message
                    ShowMessage(TechnicalSkills_bottomErrorMessageLabel,
                        TechnicalSkills_topErrorMessageLabel, "</br>" + Resources.HCMResource.
                        Common_Empty_Grid);
                }
                else
                {
                    //show the default empty message
                    ShowMessage(TechnicalSkills_bottomErrorMessageLabel,
                        TechnicalSkills_topErrorMessageLabel, Resources.HCMResource.
                        Common_Empty_Grid);
                }
                //Make the result div as visible
                TechnicalSkills_resultDiv.Visible = false;
                TechnicalSkills_bottomPagingNavigator.Visible = false;
            }

            TechnicalSkills_bottomPagingNavigator.TotalRecords = total;
            TechnicalSkills_bottomPagingNavigator.PageSize = base.GridPageSize;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(TechnicalSkills_restoreHiddenField.Value) &&
                TechnicalSkills_restoreHiddenField.Value == "Y")
            {
                TechnicalSkills_searchDiv.Style["display"] = "none";
                TechnicalSkills_searchResultsUpSpan.Style["display"] = "block";
                TechnicalSkills_searchResultsDownSpan.Style["display"] = "none";
                TechnicalSkills_resultDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TechnicalSkills_searchDiv.Style["display"] = "block";
                TechnicalSkills_searchResultsUpSpan.Style["display"] = "none";
                TechnicalSkills_searchResultsDownSpan.Style["display"] = "block";
                TechnicalSkills_resultDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods

        #region Overloaded Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            List<TechnicalSkillsGroup> technicalGroupList = new ResumeRepositoryBLManager().GetTechnicalGroup();

            TechnicalSkills_groupTypeDropDownList.DataSource = technicalGroupList;
            TechnicalSkills_groupTypeDropDownList.DataTextField = "GroupName";
            TechnicalSkills_groupTypeDropDownList.DataValueField = "GroupID";
            TechnicalSkills_groupTypeDropDownList.DataBind();
            TechnicalSkills_groupTypeDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        #endregion Overloaded Methods
    }
}
