﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResumeMandatorySettings.aspx.cs"
    Inherits="Forte.HCM.UI.ResumeRepository.ResumeMandatorySettings" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="ResumeReminderSettings_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ResumeReminderSettings_headerLiteral" runat="server" Text="Resume Mandatory Settings"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                         <asp:UpdatePanel ID="ResumeMandatorySettings_topSaveUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="ResumeMandatorySettings_topSaveMandatorySettings" runat="server" SkinID="sknButtonId"
                                                    Text="Save" OnClick="ResumeMandatorySettings_saveMandatorySettings_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="ResumeReminderSettings_topResetUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="ResumeReminderSettings_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="ResumeReminderSettings_resetLinkButton_Click"></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ResumeReminderSettings_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ResumeReminderSettings_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ResumeReminderSettings_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="ResumeReminderSettings_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="Literal1" runat="server" Text="Contact Information"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="4" cellspacing="2" border="0">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="ResumeMandatorySettings_contactInfoUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DataList ID="ResumeMandatorySettings_contactInfoDataList" runat="server" RepeatColumns="1"
                                                                Width="100%" 
                                                                OnItemDataBound="ResumeMandatorySettings_contactInfoDataList_ItemDataBound" >
                                                                <HeaderTemplate>
                                                                    <table cellpadding="4" cellspacing="4" border="0">
                                                                        <tr>
                                                                            <td align="left" style="width: 250px;">
                                                                                <asp:Label ID="ResumeMandatorySettings_contactInfoFieldNameLabel" runat="server"
                                                                                    SkinID="sknLabelFieldHeaderText" Text="Field Name"></asp:Label>
                                                                            </td>
                                                                            <td align="left" style="width: 100px;">
                                                                                <asp:Label ID="ResumeMandatorySettings_contactInfoMandatoryLabel" runat="server"
                                                                                    SkinID="sknLabelFieldHeaderText" Text="Mandatory"></asp:Label>
                                                                            </td>
                                                                            <td id="Td1" runat="server">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr runat="server" id="ResumeMandatorySettings_contactInfoTR">
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:Label ID="ResumeMandatorySettings_contactInfoValidationLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Validation"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" style="width: 150px;">
                                                                                            <asp:Label ID="ResumeMandatorySettings_contactInfoValuesLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Length(at least)"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:Label ID="ResumeMandatorySettings_contactInfoReasonLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Comment"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:Label ID="ResumeMandatorySettings_contactInfoNoRecordLabel" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <table cellpadding="4" cellspacing="4" border="0">
                                                                        <tr>
                                                                            <td align="left" style="width: 250px;">
                                                                                <asp:Label ID="ResumeMandatorySettings_contactInfoElementNameLabel" Text='<%# Eval("Description").ToString() %>'
                                                                                    runat="server"></asp:Label>
                                                                                <asp:HiddenField ID="ResumeMandatorySettings_contactInfoElementIdHiddenField" Value='<%# Eval("ElementId").ToString() %>'
                                                                                    runat="server" />
                                                                                <asp:HiddenField ID="ResumeMandatorySettings_contactInfoExprTypeHiddenField" Value='<%# Eval("ValidateExprType") %>'
                                                                                    runat="server" />
                                                                            </td>
                                                                            <td align="left" style="width: 100px;">
                                                                                <asp:CheckBox ID="ResumeMandatorySettings_contactInfoElementCheckBox" Checked='<%# Eval("ElementIdAssigned") != null &&  Eval("ElementIdAssigned") != string.Empty  && Eval("ElementIdAssigned").ToString()==Eval("ElementId").ToString() %>'
                                                                                    runat="server" OnCheckedChanged="ResumeMandatorySettings_contactInfoElementCheckBox_CheckedChanged"
                                                                                    AutoPostBack="true" />
                                                                            </td>
                                                                            <td id="ResumeMandatorySettings_contactInfoValidationTd" runat="server">
                                                                                <table cellpadding="0" cellspacing="0" width="100%" id="ResumeMandatorySettings_contactInfoValidationTable"
                                                                                    runat="server">
                                                                                    <tr id="ResumeMandatorySettings_contactInfoValidationTr" runat="server">
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:DropDownList ID="ResumeMandatorySettings_contactInfoValidateExprTypeDropDown"
                                                                                                runat="server" AutoPostBack="true"
                                                                                                onselectedindexchanged="ResumeMandatorySettings_contactInfoValidateExprTypeDropDown_SelectedIndexChanged">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td align="left" style="width: 150px;" id="ResumeMandatorySettings_contactInfoValidateExprValueTD" runat="server">
                                                                                            <asp:TextBox ID="ResumeMandatorySettings_contactInfoValidateExprValueTextBox" runat="server"
                                                                                                MaxLength="10" Width="50px" Text='<%# Eval("ValidateExprValue") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:TextBox ID="ResumeMandatorySettings_contactInfoValidateExprReasonTextBox" runat="server"
                                                                                                MaxLength="100" Width="180px" Text='<%# Eval("ValidateReason") %>'></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- Excutive And Technical Information -->
                    <tr id="Tr2" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="Literal2" runat="server" Text="Executive & Technical Information"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="4" cellspacing="2" border="0">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="ResumeMandatorySettings_excutiveTechnicalUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DataList ID="ResumeMandatorySettings_excutiveTechnicalDataList" runat="server"
                                                                RepeatColumns="1" Width="100%"
                                                                OnItemDataBound="ResumeMandatorySettings_excutiveTechnicalDataList_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table cellpadding="4" cellspacing="4" border="0">
                                                                        <tr>
                                                                            <td align="left" style="width: 250px;">
                                                                                <asp:Label ID="ResumeMandatorySettings_excutiveTechnicalFieldNameLabel" runat="server"
                                                                                    SkinID="sknLabelFieldHeaderText" Text="Field Name"></asp:Label>
                                                                            </td>
                                                                            <td align="left" style="width: 100px;">
                                                                                <asp:Label ID="ResumeMandatorySettings_excutiveTechnicalMandatoryLabel" runat="server"
                                                                                    SkinID="sknLabelFieldHeaderText" Text="Mandatory"></asp:Label>
                                                                            </td>
                                                                            <td id="ResumeMandatorySettings_excutiveTechnicalTd" runat="server">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr runat="server" id="ResumeMandatorySettings_excutiveTechnicalTR">
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:Label ID="ResumeMandatorySettings_excutiveTechnicalValidationLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Validation"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" style="width: 150px;">
                                                                                            <asp:Label ID="ResumeMandatorySettings_excutiveTechnicalValuesLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Length(at least)"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:Label ID="ResumeMandatorySettings_excutiveTechnicalReasonLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Comment"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:Label ID="ResumeMandatorySettings_excutiveTechnicalNoRecordLabel" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <table cellpadding="4" cellspacing="4" border="0">
                                                                        <tr>
                                                                            <td align="left" style="width: 250px;">
                                                                                <asp:Label ID="ResumeMandatorySettings_excutiveTechnicalElementNameLabel" runat="server"
                                                                                    Text='<%# Eval("Description").ToString() %>'></asp:Label>
                                                                                <asp:HiddenField ID="ResumeMandatorySettings_excutiveTechnicalElementIdHiddenField"
                                                                                    Value='<%# Eval("ElementId").ToString() %>' runat="server" />
                                                                                <asp:HiddenField ID="ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField"
                                                                                    Value='<%# Eval("ValidateExprType") %>' runat="server" />
                                                                            </td>
                                                                            <td align="left" style="width: 100px;">
                                                                                <asp:CheckBox ID="ResumeMandatorySettings_excutiveTechnicalElementCheckBox" Checked='<%# Eval("ElementIdAssigned") != null &&  Eval("ElementIdAssigned") != string.Empty  && Eval("ElementIdAssigned").ToString()==Eval("ElementId").ToString() %>'
                                                                                    runat="server" OnCheckedChanged="ResumeMandatorySettings_excutiveTechnicalElementCheckBox_CheckedChanged"
                                                                                    AutoPostBack="true" />
                                                                            </td>
                                                                            <td id="ResumeMandatorySettings_excutiveTechnicalValidationTd" runat="server">
                                                                                <table cellpadding="0" cellspacing="0" width="100%" id="ResumeMandatorySettings_excutiveTechnicalValidationTable"
                                                                                    runat="server">
                                                                                    <tr runat="server" id="ResumeMandatorySettings_excutiveTechnicalValidationTr">
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:DropDownList ID="ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown"
                                                                                                runat="server" AutoPostBack="true"
                                                                                                onselectedindexchanged="ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown_SelectedIndexChanged">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td align="left" style="width: 150px;">
                                                                                            <asp:TextBox ID="ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox"
                                                                                                runat="server" MaxLength="10" Width="50px" Text='<%# Eval("ValidateExprValue") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td align="left" style="width: 200px;">
                                                                                            <asp:TextBox ID="ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox"
                                                                                                Text='<%# Eval("ValidateReason") %>' runat="server" Width="180px" MaxLength="100"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- End Excutive And Technical Information-->
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- Project Elements -->
                    <tr id="Tr3" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="Literal3" runat="server" Text="Project Information"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="4" cellspacing="2" border="0">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="ResumeMandatorySettings_projectsUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                             <asp:DataList ID="ResumeMandatorySettings_projectsDataList" runat="server" RepeatColumns="1"
                                                        Width="100%" OnItemDataBound="ResumeMandatorySettings_projectsDataList_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table cellpadding="4" cellspacing="4" border="0">
                                                                <tr>
                                                                    <td align="left" style="width: 250px;">
                                                                        <asp:Label ID="ResumeMandatorySettings_projectsFieldNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Field Name"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 100px;">
                                                                        <asp:Label ID="ResumeMandatorySettings_projectsMandatoryLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Mandatory"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:Label ID="ResumeMandatorySettings_projectsValidationLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Validation"></asp:Label>
                                                                                </td>
                                                                                <td align="left" style="width: 150px;">
                                                                                    <asp:Label ID="ResumeMandatorySettings_projectsValuesLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Length(at least)"></asp:Label>
                                                                                </td>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:Label ID="ResumeMandatorySettings_projectsValuesReasonLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Comment"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <table cellpadding="4" cellspacing="4" border="0">
                                                                <tr>
                                                                    <td align="left" style="width: 250px;">
                                                                        <asp:Label ID="ResumeMandatorySettings_projectsElementNameLabel" runat="server" Text='<%# Eval("Description").ToString() %>'></asp:Label>
                                                                        <asp:HiddenField ID="ResumeMandatorySettings_projectsElementIdHiddenField" Value='<%# Eval("ElementId").ToString() %>'
                                                                            runat="server" />
                                                                        <asp:HiddenField ID="ResumeMandatorySettings_projectsExprTypeHiddenField" Value='<%# Eval("ValidateExprType")%>'
                                                                            runat="server" />
                                                                    </td>
                                                                    <td align="left" style="width: 100px;">
                                                                        <asp:CheckBox ID="ResumeMandatorySettings_projectsElementCheckBox" Checked='<%# Eval("ElementIdAssigned") != null &&  Eval("ElementIdAssigned") != string.Empty  && Eval("ElementIdAssigned").ToString()==Eval("ElementId").ToString() %>'
                                                                            runat="server" OnCheckedChanged="ResumeMandatorySettings_projectsElementCheckBox_CheckedChanged"
                                                                            AutoPostBack="true" />
                                                                    </td>
                                                                    <td id="ResumeMandatorySettings_projectsValidationTd" runat="server"  >
                                                                        <table cellpadding="0" cellspacing="0" width="100%" id="ResumeMandatorySettings_projectsValidationTable"
                                                                            runat="server">
                                                                            <tr>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:DropDownList ID="ResumeMandatorySettings_projectsValidateExprTypeDropDown" 
                                                                                        runat="server" AutoPostBack="true"
                                                                                        onselectedindexchanged="ResumeMandatorySettings_projectsValidateExprTypeDropDown_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td align="left" style="width: 150px;">
                                                                                    <asp:TextBox ID="ResumeMandatorySettings_projectsValidateExprValueTextBox" Text='<%# Eval("ValidateExprValue") %>'
                                                                                        runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                                                                                </td>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:TextBox ID="ResumeMandatorySettings_projectsValidateExprReasonTextBox" Text='<%# Eval("ValidateReason") %>'
                                                                                        runat="server" MaxLength="100" Width="180px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                   
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- End Project Elements-->
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- End Education and Reference -->
                    <tr id="Tr4" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="Literal4" runat="server" Text="Education & Reference Information"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="4" cellspacing="2" border="0">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="ResumeMandatorySettings_educationReferenceUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DataList ID="ResumeMandatorySettings_educationReferenceDataList" runat="server"
                                                        RepeatColumns="1" Width="100%" OnItemDataBound="ResumeMandatorySettings_educationReferenceDataList_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table cellpadding="4" cellspacing="4" border="0">
                                                                <tr>
                                                                    <td align="left" style="width: 250px;">
                                                                        <asp:Label ID="ResumeMandatorySettings_educationReferenceFieldNameLabel" runat="server"
                                                                            SkinID="sknLabelFieldHeaderText" Text="Field Name"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 100px;">
                                                                        <asp:Label ID="ResumeMandatorySettings_educationReferenceMandatoryLabel" runat="server"
                                                                            SkinID="sknLabelFieldHeaderText" Text="Mandatory"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:Label ID="ResumeMandatorySettings_educationReferenceValidtionLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Validation"></asp:Label>
                                                                                </td>
                                                                                <td align="left" style="width: 150px;">
                                                                                    <asp:Label ID="ResumeMandatorySettings_educationReferenceValuesLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Length(at least)"></asp:Label>
                                                                                </td>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:Label ID="ResumeMandatorySettings_educationReferenceReasonLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Comment"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <table cellpadding="4" cellspacing="4" border="0">
                                                                <tr>
                                                                    <td align="left" style="width: 250px;">
                                                                        <asp:Label ID="ResumeMandatorySettings_educationReferenceElementNameLabel" runat="server"
                                                                            Text='<%# Eval("Description").ToString() %>'>
                                                                        </asp:Label>
                                                                        <asp:HiddenField ID="ResumeMandatorySettings_educationReferenceElementIdHiddenField"
                                                                            Value='<%# Eval("ElementId").ToString() %>' runat="server" />
                                                                        <asp:HiddenField ID="ResumeMandatorySettings_educationReferenceExprTypeHiddenField"
                                                                            Value='<%# Eval("ValidateExprType") %>' runat="server" />
                                                                    </td>
                                                                    <td align="left" style="width: 100px;">
                                                                        <asp:CheckBox ID="ResumeMandatorySettings_educationReferenceElementCheckBox" Checked='<%# Eval("ElementIdAssigned") != null &&  Eval("ElementIdAssigned") != string.Empty  && Eval("ElementIdAssigned").ToString()==Eval("ElementId").ToString() %>'
                                                                            runat="server" OnCheckedChanged="ResumeMandatorySettings_educationReferenceElementCheckBox_CheckedChanged"
                                                                            AutoPostBack="true" />
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" width="100%" id="ResumeMandatorySettings_educationReferenceValidationTable"
                                                                            runat="server">
                                                                            <tr>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:DropDownList ID="ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown"
                                                                                        runat="server" AutoPostBack="true"
                                                                                        onselectedindexchanged="ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td align="left" style="width: 150px;">
                                                                                    <asp:TextBox ID="ResumeMandatorySettings_educationReferenceValidateExprValueTextBox"
                                                                                        runat="server" MaxLength="10" Width="50px" Text='<%# Eval("ValidateExprValue") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td align="left" style="width: 200px;">
                                                                                    <asp:TextBox ID="ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox"
                                                                                        runat="server" MaxLength="100" Width="180px" Text='<%# Eval("ValidateReason") %>'></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>    
                                                    </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <!-- End Education and Reference-->
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ResumeReminderSettings_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ResumeReminderSettings_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="ResumeReminderSettings_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="ResumeMandatorySettings_bottomSaveUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="ResumeMandatorySettings_bottomSaveMandatorySettings" runat="server" SkinID="sknButtonId"
                                                    Text="Save" OnClick="ResumeMandatorySettings_saveMandatorySettings_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="ResumeReminderSettings_resetBottomUpdatePanel" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="ResumeReminderSettings_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" 
                                            onclick="ResumeReminderSettings_resetLinkButton_Click"></asp:LinkButton>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ResumeReminderSettings_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
