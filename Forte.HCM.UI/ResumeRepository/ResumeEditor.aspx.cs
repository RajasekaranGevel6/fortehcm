﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ResumeEditor.aspx.cs
// File that represents the user interface for displaying resume information
// like personal, contact, executive summary, projects, education and references 
// information for selected candidate.

#endregion Header

#region Directives                                                             

using System;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the resumeeditor page. This page allows us to view the personal, 
    /// contact, executive summary, project, education, reference information 
    /// and resume source information for selected candidate. This page also 
    /// provides editing, deleting candidate information for a selected candidate.
    /// It can allow us to upload a new resume for selected candidate.And also 
    /// download a uploaded resume for selected candidate.This class inherits 
    /// the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ResumeEditor : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Assign default button
                Page.Form.DefaultButton = ResumeEditor_topSaveButton.UniqueID;
                
                ResumeEditor_candidateIDTextBox.Focus();
                
                Master.SetPageCaption(Resources.HCMResource.ResumeEditor_Title);

                // Clear error/success messages.
                ClearAllLabelMessage();
               
                ResumeEditor_hideLeftPaneTD.Attributes.Add("onclick", "ShowHidePanel('" +
                    ResumeEditor_leftPaneDiv.ClientID + "','" + ResumeEditor_rightPaneDiv.ClientID + "')");
                ResumeEditor_hideRightPaneTD.Attributes.Add("onclick", "ShowHidePanel('" +
                    ResumeEditor_rightPaneDiv.ClientID + "','" + ResumeEditor_leftPaneDiv.ClientID + "')");

                // Assign handler to candidate selection popup.
                ResumeEditor_searchImageButton.Attributes.Add("onclick",
                   "return LoadCandidate('" + ResumeEditor_candidateIDHiddenField.ClientID + "','" +
                   ResumeEditor_candidateNameHiddenField.ClientID + "','" +
                   ResumeEditor_candidateIDTextBox.ClientID + "','" +
                   ResumeEditor_candidateFirstNameHiddenField.ClientID + "')");

                // Subscribe to message thrown events from user controls.ResumeEditor_mainResumeControl
                ResumeEditor_mainResumeControl.ControlMessageThrown += new MainResumeControl.ControlMessageThrownDelegate
                    (ResumeEditor_mainResumeControl_ControlMessageThrown);

                if (!IsPostBack)
                {
                    // Check if candidate ID is passed as query string. If passed, 
                    // load the candidate detail and by default select and show
                    // the last uploaded resume.
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                    {
                        LoadCandidateDetail();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeEditor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that loads the candidate detail for the passed candidate ID.
        /// </summary>
        private void LoadCandidateDetail()
        {
            int candidateID = 0;

            if (int.TryParse(Request.QueryString["candidateid"], out candidateID) == false)
                return;

            // Get candidate detail.
            CandidateDetail candidateDetail = new CandidateBLManager().GetCandidateInformation(candidateID);

            if (candidateDetail == null)
            {
                ShowMessage(ResumeEditor_topErrorMessageLabel, "No such candidate exists");
                return;
            }

            // Assign details to the field.
            ResumeEditor_candidateIDHiddenField.Value = candidateID.ToString();
            ResumeEditor_candidateIDTextBox.Text = candidateDetail.FirstName;

            // Load resume versions.
            LoadResumeVersions();

            // Load the last resume details by default.
            if (ResumeEdit_resumeVersionsGridView.Rows.Count > 0)
            {
                int index = ResumeEdit_resumeVersionsGridView.Rows.Count - 1;

                // If row index is passed, then give priority to that.
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["rowindex"]))
                {
                    int.TryParse(Request.QueryString["rowindex"], out index);
                }

                ((RadioButton)ResumeEdit_resumeVersionsGridView.Rows[index].FindControl
                    ("ResumeEdit_resumeVersionsGridView_selectRadioButton")).Checked = true;

                // Keep the candidate resume ID in the hidden field.
                ResumeEditor_candidateResumeIDHiddenField.Value =
                    ((HiddenField)ResumeEdit_resumeVersionsGridView.Rows[index].FindControl
                    ("ResumeEdit_resumeVersionsGridView_candidateResumeIDHiddenField")).Value;

                // Load the hrxml.
                ResumeEditor_isLoadClickHiddenField.Value = "Y";
                ProcessHRXml();
            }
        }

        void ResumeEditor_mainResumeControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            if (c.MessageType == MessageType.Error)
                ShowMessage(ResumeEditor_topErrorMessageLabel, c.Message);
            else
                ShowMessage(ResumeEditor_topSuccessMessageLabel, c.Message);
        }

        /// <summary>
        /// Handler method that will be called when the load button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to load the resume information of selected candidate.
        /// </remarks>
        protected void ResumeEditor_loadDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear resume contents.
                Session["RESUME_CONTENTS"] = null;

                ResumeEditor_downloadImageButton.Visible = true;
                // Clear error/success messages.
                ClearAllLabelMessage();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(ResumeEditor_candidateIDHiddenField.Value))
                {
                    ShowMessage(ResumeEditor_topErrorMessageLabel,
                       Resources.HCMResource.ResumeEditor_SelectCandidate);
                    return;
                }

                // Load resume versions.
                LoadResumeVersions();

                // Load the first resume details by default.
                if (ResumeEdit_resumeVersionsGridView.Rows.Count > 0)
                {
                    int index = 0;

                    ((RadioButton)ResumeEdit_resumeVersionsGridView.Rows[index].FindControl
                        ("ResumeEdit_resumeVersionsGridView_selectRadioButton")).Checked = true;

                    // Keep the candidate resume ID in the hidden field.
                    ResumeEditor_candidateResumeIDHiddenField.Value =
                        ((HiddenField)ResumeEdit_resumeVersionsGridView.Rows[index].FindControl
                        ("ResumeEdit_resumeVersionsGridView_candidateResumeIDHiddenField")).Value;

                    // Load the hrxml.
                    ResumeEditor_isLoadClickHiddenField.Value = "Y";
                    ProcessHRXml();
                }
            }
            catch (Exception exp)
            {
                ShowMessage(ResumeEditor_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that loads the candidate resume versions.
        /// </summary>
        private void LoadResumeVersions()
        {
            // Load the list of resume versions.
            List<ResumeVersion> resumeVersions = new ResumeRepositoryBLManager().
                GetResumeVersions(Convert.ToInt32(ResumeEditor_candidateIDHiddenField.Value));

            if (resumeVersions == null)
            {
                ShowMessage(ResumeEditor_topErrorMessageLabel, Resources.HCMResource.ResumeEditor_NoInformationFound);
                ResumeEditor_downloadImageButton.Visible = false;

                ResumeEdit_resumeVersionsGridView.DataSource = null;
                ResumeEdit_resumeVersionsGridView.DataBind();
                return;
            }

            // Show an alert message to the user. 
            ShowMessage(ResumeEditor_topSuccessMessageLabel,
                "Ensure that start & end date are present for all projects. This will add higher value to your resume");

            // Show the resume versions.
            ResumeEdit_resumeVersionsGridView.DataSource = resumeVersions;
            ResumeEdit_resumeVersionsGridView.DataBind();
        }

        /// <summary>
        /// Method that is called when the resume radio button is selected in the grid.
        /// </summary>
        /// <param name="sender">
       /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ResumeEdit_resumeVersionsGridView_selectRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                // Set the selected state to false for all radio button.
                foreach (GridViewRow gridRow in ResumeEdit_resumeVersionsGridView.Rows)
                {
                    ((RadioButton)gridRow.FindControl("ResumeEdit_resumeVersionsGridView_selectRadioButton")).Checked = false;
                }

                // Retrieve the selected radio button and row, and set it to selected state.
                RadioButton selectedRadioButton = (RadioButton)sender;
                GridViewRow row = (GridViewRow)selectedRadioButton.NamingContainer;
                    ((RadioButton)row.FindControl("ResumeEdit_resumeVersionsGridView_selectRadioButton")).Checked = true;

                // Keep the candidate resume ID in the hidden field.
                ResumeEditor_candidateResumeIDHiddenField.Value =
                    ((HiddenField)row.FindControl("ResumeEdit_resumeVersionsGridView_candidateResumeIDHiddenField")).Value;

                // Load the hrxml.
                ResumeEditor_isLoadClickHiddenField.Value = "Y";
                ProcessHRXml();
            }
            catch (Exception exp)
            {
                ShowMessage(ResumeEditor_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to save the resume information for selected candidate.
        /// </remarks>
        protected void ResumeEditor_topSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                
                SaveOrApproveResume(((Button)sender).CommandName.ToString());
                
                if (ResumeEditor_topErrorMessageLabel.Text.Trim().Length > 0)
                    return;
                
                //ResumeEditor_candidateTable.Visible = false;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeEditor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the approve button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to approve the resume information for selected candidate.
        /// </remarks>
        protected void ResumeEditor_topApproveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(ResumeEditor_candidateResumeIDHiddenField.Value))
                {
                    ShowMessage(ResumeEditor_topErrorMessageLabel,
                        Resources.HCMResource.ResumeEditor_NoInformationFound);
                    ResumeEditor_downloadImageButton.Visible = false;
                    return;
                }

                // Validate all the input fields before save and approve.
                if (!IsValidData())
                {
                    //ResumeEditor_candidateTable.Visible = false;
                    return;
                }

                //Method to save and approve resumes.
                SaveOrApproveResume(((Button)sender).CommandName.ToString());
                
                //  Approve resume deatils.
                new ResumeRepositoryBLManager().ApproveResume(Convert.ToInt32(ResumeEditor_candidateIDHiddenField.Value));
                ShowMessage(ResumeEditor_topSuccessMessageLabel,
                    Resources.HCMResource.ResumeApproval_Success);
                ResumeEditor_topErrorMessageLabel.Text = string.Empty;
                ResumeEditor_topSaveButton.Visible = false;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeEditor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download image is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to download the resume source for selected candidate.
        /// </remarks>
        protected void ResumeEditor_downloadImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (ResumeEditor_candidateResumeIDHiddenField.Value == null ||
                    ResumeEditor_candidateResumeIDHiddenField.Value.Trim().Length == 0)
                {
                    ShowMessage(ResumeEditor_topErrorMessageLabel, "Candidate resume not present");
                    return;
                }
                Response.Redirect("~/Common/Download.aspx?id=" + ResumeEditor_candidateResumeIDHiddenField.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeEditor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the  
        /// reset link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the resume parser page.
        /// </remarks>
        protected void ResumeEditor_topResetButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        #endregion Events Handlers

        #region Public Methods                                                 

        /// <summary>
        /// This method get the project details
        /// </summary>
        /// <returns>
        /// A <see cref="List"/> that contains the project details.
        /// </returns>
        public List<Project> GetProjectData()
        {
            ListView oProjList = (ListView)((ProjectsControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_projectsControl")).FindControl("ProjectsControl_listView");
            Project project = null;
            List<Project> Projects = null;
            DateTime TempDate;
            try
            {
                foreach (ListViewDataItem item in oProjList.Items)
                {
                    if (project == null)
                        project = new Project();
                    DateTime.TryParse(((TextBox)item.FindControl("ProjectsControl_startDtTextBox")).Text, out TempDate);
                    if (DateTime.MinValue != TempDate)
                        project.StartDate = TempDate;
                    DateTime.TryParse(((TextBox)item.FindControl("ProjectsControl_endDtTextBox")).Text, out TempDate);
                    if (DateTime.MinValue != TempDate)
                        project.EndDate = TempDate;
                    project.ClientName = ((TextBox)item.FindControl("ProjectsControl_clientNameTextBox")).Text.Trim();
                    project.ClientIndustry = ((TextBox)item.FindControl("ProjectsControl_clientIndustryTextBox")).Text.Trim();
                    project.ProjectName = ((TextBox)item.FindControl("ProjectsControl_projectNameTextBox")).Text.Trim();
                    project.PositionTitle = ((TextBox)item.FindControl("ProjectsControl_positionTextBox")).Text.Trim();
                    project.ProjectDescription = ((TextBox)item.FindControl("ProjectsControl_projectDescTextBox")).Text.Trim();
                    project.Role = ((TextBox)item.FindControl("ProjectsControl_roleTextBox")).Text.Trim();
                    project.Environment = ((TextBox)item.FindControl("ProjectsControl_environmentTextBox")).Text.Trim();
                    if (Projects == null)
                        Projects = new List<Project>();
                    Projects.Add(project);
                    project = null;
                }
                return Projects;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(oProjList)) oProjList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(project)) project = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Projects)) Projects = null;
            }
        }

        #endregion Public Methods

        #region Private Methods                                                

        /// <summary>
        /// This method helps to clear the success/error messages.
        /// </summary>                                    
        private void ClearAllLabelMessage()
        {
            // Clear messages.
            ResumeEditor_topErrorMessageLabel.Text = string.Empty;
            ResumeEditor_topSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// This method helps to construct the hrxml with file contents.
        /// </summary>
        /// <param name="IdValue">
        /// A <see cref="string"/> that holds the candidateid.
        /// </param>
        private XmlDocument WriteXml(string IdValue)
        {
            XmlDocument xmlDoc = new XmlDocument();
            // Not required if we insert in db otherwise its required
            //XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlText oText;
            // Create the root element
            XmlElement rootNode = xmlDoc.CreateElement("Resume");
            //  xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
            xmlDoc.AppendChild(rootNode);
            // Contstruct the ResumeID Node
            XmlElement resumeIdNode = xmlDoc.CreateElement("ResumeId");
            xmlDoc.DocumentElement.PrependChild(resumeIdNode);
            XmlElement idNode = xmlDoc.CreateElement("IdValue");
            xmlDoc.DocumentElement.PrependChild(idNode);
            oText = xmlDoc.CreateTextNode(IdValue);
            idNode.AppendChild(oText);
            resumeIdNode.AppendChild(idNode);
            rootNode.AppendChild(resumeIdNode);
            // Construct the DistributionGuidelinesnodes
            XmlElement distributionGuideNode = xmlDoc.CreateElement("DistributionGuidelines");
            xmlDoc.DocumentElement.PrependChild(distributionGuideNode);
            rootNode.AppendChild(distributionGuideNode);
            // Construct the StucturedXMLNode
            XmlElement structuredXMLNode = xmlDoc.CreateElement("StructuredXMLResume");
            xmlDoc.DocumentElement.PrependChild(structuredXMLNode);
            rootNode.AppendChild(structuredXMLNode);
            // Construct the NonXMLResume
            XmlElement nonXMLResumeNode = xmlDoc.CreateElement("NonXMLResume");
            xmlDoc.DocumentElement.PrependChild(nonXMLResumeNode);
            rootNode.AppendChild(nonXMLResumeNode);
            // Construct the contactInfoNode
            XmlElement contactInfoNode = xmlDoc.CreateElement("ContactInfo");
            xmlDoc.DocumentElement.PrependChild(contactInfoNode);
            structuredXMLNode.AppendChild(contactInfoNode);
            // Construct the PersonName
            XmlElement personNameNode = xmlDoc.CreateElement("PersonName");
            xmlDoc.DocumentElement.PrependChild(personNameNode);
            contactInfoNode.AppendChild(personNameNode);
            // Construct the FirstName
            XmlElement personFirstNameNode = xmlDoc.CreateElement("FirstName");
            xmlDoc.DocumentElement.PrependChild(personFirstNameNode);
            contactInfoNode.AppendChild(personFirstNameNode);
            // Construct the MiddleName
            XmlElement personMiddleNameNode = xmlDoc.CreateElement("MiddleName");
            xmlDoc.DocumentElement.PrependChild(personMiddleNameNode);
            contactInfoNode.AppendChild(personMiddleNameNode);
            // Construct the LastName
            XmlElement personLastNameNode = xmlDoc.CreateElement("LastName");
            xmlDoc.DocumentElement.PrependChild(personLastNameNode);
            contactInfoNode.AppendChild(personLastNameNode);
            // Construct the FormattedName
            XmlElement personFormattedNameNode = xmlDoc.CreateElement("FormattedName");
            xmlDoc.DocumentElement.PrependChild(personFormattedNameNode);
            personNameNode.AppendChild(personFormattedNameNode);
            // To Get NameControl User Control Data
            NameControl nc = (NameControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_nameControl");
            TextBox txtFirstName = (TextBox)nc.FindControl("NameControl_firstNameTextBox");
            TextBox txtMiddleName = (TextBox)nc.FindControl("NameControl_middleNameTextBox");
            TextBox txtLastName = (TextBox)nc.FindControl("NameControl_lastNameTextBox");
            oText = xmlDoc.CreateTextNode(txtFirstName.Text.Trim() + " "
                + txtMiddleName.Text.Trim() + " " + txtLastName.Text.Trim());
            personFormattedNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtFirstName.Text.Trim());
            personFirstNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtMiddleName.Text.Trim());
            personMiddleNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtLastName.Text.Trim());
            personLastNameNode.AppendChild(oText);
            // Construct the ContactMethod
            XmlElement contactMethodNode = xmlDoc.CreateElement("ContactMethod");
            xmlDoc.DocumentElement.PrependChild(contactMethodNode);
            contactInfoNode.AppendChild(contactMethodNode);
            // Construct the ContactTelephone
            XmlElement contactMethodTelephoneNode = xmlDoc.CreateElement("Telephone");
            xmlDoc.DocumentElement.PrependChild(contactMethodTelephoneNode);
            contactMethodNode.AppendChild(contactMethodTelephoneNode);
            XmlElement contactMethodTelephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodTelephoneFormattedNode);
            contactMethodTelephoneNode.AppendChild(contactMethodTelephoneFormattedNode);
            // Construct the HomePhone
            XmlElement contactMethodHomephoneNode = xmlDoc.CreateElement("HomePhone");
            xmlDoc.DocumentElement.PrependChild(contactMethodHomephoneNode);
            contactMethodNode.AppendChild(contactMethodHomephoneNode);
            XmlElement contactMethodHomephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodHomephoneFormattedNode);
            contactMethodHomephoneNode.AppendChild(contactMethodHomephoneFormattedNode);
            // Construct the ContactFax
            XmlElement contactMethodFaxNode = xmlDoc.CreateElement("Fax");
            xmlDoc.DocumentElement.PrependChild(contactMethodFaxNode);
            contactMethodNode.AppendChild(contactMethodFaxNode);
            XmlElement contactMethodFaxFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodFaxFormattedNode);
            contactMethodFaxNode.AppendChild(contactMethodFaxFormattedNode);
            // Construct the InternetEmailAddress
            XmlElement contactMethodEmailNode = xmlDoc.CreateElement("InternetEmailAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodEmailNode);
            contactMethodNode.AppendChild(contactMethodEmailNode);
            // Construct the InternetWebAddress
            XmlElement contactMethodInternetNode = xmlDoc.CreateElement("InternetWebAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodInternetNode);
            contactMethodNode.AppendChild(contactMethodInternetNode);
            // Construct the PostalAddress
            XmlElement contactMethodPostalAddressNode = xmlDoc.CreateElement("PostalAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodPostalAddressNode);
            contactMethodNode.AppendChild(contactMethodPostalAddressNode);
            // To Get ContactInformation User Control Data & Update the RootNode ContactInformation
            ContactInformationControl cil = (ContactInformationControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_contactInfoControl");
            TextBox txtAddress = (TextBox)cil.FindControl("ContactInformationControl_streetTextBox");
            TextBox txtCity = (TextBox)cil.FindControl("ContactInformationControl_cityTextBox");
            TextBox txtState = (TextBox)cil.FindControl("ContactInformationControl_stateTextBox");
            TextBox txtCountry = (TextBox)cil.FindControl("ContactInformationControl_countryNameTextBox");
            TextBox txtPostalCode = (TextBox)cil.FindControl("ContactInformationControl_postalCodeTextBox");
            TextBox txtFixedLine = (TextBox)cil.FindControl("ContactInformationControl_fixedLineTextBox");
            TextBox txtMobile = (TextBox)cil.FindControl("ContactInformationControl_mobileTextBox");
            TextBox txtEmail = (TextBox)cil.FindControl("ContactInformationControl_emailTextBox");
            TextBox txtWebSite = (TextBox)cil.FindControl("ContactInformationControl_websiteTextBox");
            XmlElement contactCountryNode = xmlDoc.CreateElement("CountryCode");
            xmlDoc.DocumentElement.PrependChild(contactCountryNode);
            XmlElement contactPostalCodeNode = xmlDoc.CreateElement("PostalCode");
            xmlDoc.DocumentElement.PrependChild(contactPostalCodeNode);
            XmlElement contactRegionNode = xmlDoc.CreateElement("Region");
            xmlDoc.DocumentElement.PrependChild(contactRegionNode);
            XmlElement contactCityNode = xmlDoc.CreateElement("Municipality");
            xmlDoc.DocumentElement.PrependChild(contactCityNode);
            XmlElement contactStateNode = xmlDoc.CreateElement("State");
            xmlDoc.DocumentElement.PrependChild(contactStateNode);
            XmlElement contactAddressNode = xmlDoc.CreateElement("Street");
            xmlDoc.DocumentElement.PrependChild(contactAddressNode);
            oText = xmlDoc.CreateTextNode(txtCountry.Text.Trim());
            contactCountryNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactCountryNode);
            oText = xmlDoc.CreateTextNode(txtPostalCode.Text.Trim());
            contactPostalCodeNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactPostalCodeNode);
            contactMethodPostalAddressNode.AppendChild(contactRegionNode);
            oText = xmlDoc.CreateTextNode(txtCity.Text.Trim());
            contactCityNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactCityNode);
            oText = xmlDoc.CreateTextNode(txtState.Text.Trim());
            contactStateNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactStateNode);
            oText = xmlDoc.CreateTextNode(txtAddress.Text.Trim());
            contactAddressNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactAddressNode);
            oText = xmlDoc.CreateTextNode(txtFixedLine.Text.Trim());
            contactMethodFaxFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtFixedLine.Text.Trim());
            contactMethodHomephoneFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtMobile.Text.Trim());
            contactMethodTelephoneFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtEmail.Text.Trim());
            contactMethodEmailNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtWebSite.Text.Trim());
            contactMethodInternetNode.AppendChild(oText);
            contactInfoNode.AppendChild(contactMethodNode);
            // Construct the ExecutiveSummary
            XmlElement execSummaryNode = xmlDoc.CreateElement("ExecutiveSummary");
            xmlDoc.DocumentElement.PrependChild(execSummaryNode);
            structuredXMLNode.AppendChild(execSummaryNode);
            // To get executivesummary data
            ExecutiveSummaryControl oExecCntl = (ExecutiveSummaryControl)
                ResumeEditor_mainResumeControl.FindControl("MainResumeControl_executiveSummaryControl");
            TextBox txtExecutiveSummary = (TextBox)oExecCntl.
                FindControl("ExecutiveSummaryControl_execSummaryTextBox");
            oText = xmlDoc.CreateTextNode(txtExecutiveSummary.Text.Trim());
            execSummaryNode.AppendChild(oText);
            structuredXMLNode.AppendChild(execSummaryNode);
            // Construct the Objective
            XmlElement objectiveNode = xmlDoc.CreateElement("Objective");
            xmlDoc.DocumentElement.PrependChild(objectiveNode);
            structuredXMLNode.AppendChild(objectiveNode);
            // Construct the EmploymentHistory
            XmlElement empHistoryNode = xmlDoc.CreateElement("EmploymentHistory");
            xmlDoc.DocumentElement.PrependChild(empHistoryNode);
            structuredXMLNode.AppendChild(empHistoryNode);
            // Construct the EducationHistory
            XmlElement educationHistoryNode = xmlDoc.CreateElement("EducationHistory");
            xmlDoc.DocumentElement.PrependChild(educationHistoryNode);
            structuredXMLNode.AppendChild(educationHistoryNode);
            // To get educational data
            EducationControl oEduCntl = (EducationControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_educationControl");
            ListView oEduList = (ListView)oEduCntl.FindControl("EducationControl_listView");
            foreach (ListViewDataItem item in oEduList.Items)
            {
                TextBox txtEduName = (TextBox)item.FindControl("EducationControl_nameTextBox");
                TextBox txtEduLocation = (TextBox)item.FindControl("EducationControl_locationTextBox");
                TextBox txtEduDegree = (TextBox)item.FindControl("EducationControl_degreeTextBox");
                TextBox txtEduSpec = (TextBox)item.FindControl("EducationControl_specializationTextBox");
                TextBox txtEduGPA = (TextBox)item.FindControl("EducationControl_gpaTextBox");
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                XmlElement eduSchoolOrInstitutionNode = xmlDoc.CreateElement("SchoolOrInstitution");
                XmlAttribute eduSchollOrInstitutionAttribute = xmlDoc.CreateAttribute("schoolType");
                eduSchollOrInstitutionAttribute.Value = "trade";
                eduSchoolOrInstitutionNode.Attributes.Append(eduSchollOrInstitutionAttribute);
                xmlDoc.DocumentElement.PrependChild(eduSchoolOrInstitutionNode);
                XmlElement eduSchoolNode = xmlDoc.CreateElement("School");
                xmlDoc.DocumentElement.PrependChild(eduSchoolNode);
                eduSchoolOrInstitutionNode.AppendChild(eduSchoolNode);
                XmlElement eduInternetDomainNameNode = xmlDoc.CreateElement("InternetDomainName");
                xmlDoc.DocumentElement.PrependChild(eduInternetDomainNameNode);
                eduSchoolNode.AppendChild(eduInternetDomainNameNode);
                XmlElement eduSchoolNameNode = xmlDoc.CreateElement("SchoolName");
                xmlDoc.DocumentElement.PrependChild(eduSchoolNameNode);
                eduSchoolNode.AppendChild(eduSchoolNameNode);
                XmlElement eduDegreeNode = xmlDoc.CreateElement("Degree");
                XmlAttribute eduDegreeTypeAttribute = xmlDoc.CreateAttribute("degreeType");
                eduDegreeTypeAttribute.Value = "Master";
                eduDegreeNode.Attributes.Append(eduDegreeTypeAttribute);
                xmlDoc.DocumentElement.PrependChild(eduDegreeNode);
                eduSchoolOrInstitutionNode.AppendChild(eduDegreeNode);
                XmlElement eduDegreeNameNode = xmlDoc.CreateElement("DegreeName");
                xmlDoc.DocumentElement.PrependChild(eduDegreeNameNode);
                eduDegreeNode.AppendChild(eduDegreeNameNode);
                XmlElement eduDegreeDateNode = xmlDoc.CreateElement("DegreeDate");
                xmlDoc.DocumentElement.PrependChild(eduDegreeDateNode);
                eduDegreeNode.AppendChild(eduDegreeDateNode);
                XmlElement eduDegreeYearMonthNode = xmlDoc.CreateElement("YearMonth");
                xmlDoc.DocumentElement.PrependChild(eduDegreeYearMonthNode);
                eduDegreeDateNode.AppendChild(eduDegreeYearMonthNode);
                XmlElement eduDegreeMajorNode = xmlDoc.CreateElement("DegreeMajor");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMajorNode);
                eduDegreeNode.AppendChild(eduDegreeMajorNode);
                XmlElement eduDegreeMajorNameNode = xmlDoc.CreateElement("Name");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMajorNameNode);
                eduDegreeMajorNode.AppendChild(eduDegreeMajorNameNode);
                XmlElement eduDegreeMinorNode = xmlDoc.CreateElement("DegreeMinor");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMinorNode);
                eduDegreeNode.AppendChild(eduDegreeMinorNode);
                XmlElement eduDegreeMinorNameNode = xmlDoc.CreateElement("Name");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMinorNameNode);
                eduDegreeMinorNode.AppendChild(eduDegreeMinorNameNode);
                XmlElement eduDegreeCommentsNode = xmlDoc.CreateElement("Comments");
                xmlDoc.DocumentElement.PrependChild(eduDegreeCommentsNode);
                eduDegreeNode.AppendChild(eduDegreeCommentsNode);
                oText = xmlDoc.CreateTextNode(txtEduName.Text.Trim() + "," + txtEduLocation.Text.Trim());
                eduSchoolNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduDegree.Text.Trim());
                eduDegreeNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduGradDate.Text.Trim());
                eduDegreeYearMonthNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduSpec.Text.Trim());
                eduDegreeMajorNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduGPA.Text.Trim());
                eduDegreeCommentsNode.AppendChild(oText);
                educationHistoryNode.AppendChild(eduSchoolOrInstitutionNode);
            }
            structuredXMLNode.AppendChild(educationHistoryNode);
            // Construct the LicenceAndCertification
            XmlElement licenceAndCertifyNode = xmlDoc.CreateElement("LicenceAndCertification");
            xmlDoc.DocumentElement.PrependChild(licenceAndCertifyNode);
            structuredXMLNode.AppendChild(licenceAndCertifyNode);
            // Construct the PatentHistory
            XmlElement patentHistoryNode = xmlDoc.CreateElement("PatentHistory");
            xmlDoc.DocumentElement.PrependChild(patentHistoryNode);
            structuredXMLNode.AppendChild(patentHistoryNode);
            // Construct the PublicationHistory
            XmlElement publicationHistoryNode = xmlDoc.CreateElement("PublicationHistory");
            xmlDoc.DocumentElement.PrependChild(publicationHistoryNode);
            structuredXMLNode.AppendChild(publicationHistoryNode);
            // Construct the SpeakingEventsHistory
            XmlElement speakingEventHistory = xmlDoc.CreateElement("SpeakingEventsHistory");
            xmlDoc.DocumentElement.PrependChild(speakingEventHistory);
            structuredXMLNode.AppendChild(speakingEventHistory);
            // Construct the Qualifications
            XmlElement qualificationNode = xmlDoc.CreateElement("Qualifications");
            xmlDoc.DocumentElement.PrependChild(qualificationNode);
            structuredXMLNode.AppendChild(qualificationNode);
            // Construct the Languages
            XmlElement languagesNode = xmlDoc.CreateElement("Languages");
            xmlDoc.DocumentElement.PrependChild(languagesNode);
            structuredXMLNode.AppendChild(languagesNode);
            // Construct the Achievements
            XmlElement achivementsNode = xmlDoc.CreateElement("Achievements");
            xmlDoc.DocumentElement.PrependChild(achivementsNode);
            structuredXMLNode.AppendChild(achivementsNode);
            // Construct the Associations
            XmlElement associationsNode = xmlDoc.CreateElement("Associations");
            xmlDoc.DocumentElement.PrependChild(associationsNode);
            structuredXMLNode.AppendChild(associationsNode);
            // Construct the References
            XmlElement referencesNode = xmlDoc.CreateElement("References");
            xmlDoc.DocumentElement.PrependChild(referencesNode);
            structuredXMLNode.AppendChild(referencesNode);
            // To get references data
            ReferencesControl oRefCntl = (ReferencesControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_referencesControl");
            ListView oRefList = (ListView)oRefCntl.FindControl("ReferencesControl_listView");
            foreach (ListViewDataItem item in oRefList.Items)
            {
                TextBox txtRefName = (TextBox)item.FindControl("ReferencesControl_nameTextBox");
                TextBox txtRefOrg = (TextBox)item.FindControl("ReferencesControl_organizationTextBox");
                TextBox txtRefRelation = (TextBox)item.FindControl("ReferencesControl_relationTextBox");
                TextBox txtRefContactInfo = (TextBox)item.FindControl("ReferencesControl_contInfoTextBox");
                XmlElement referenceNode = xmlDoc.CreateElement("Reference");
                XmlAttribute referenceTypeAttribute = xmlDoc.CreateAttribute("type");
                referenceTypeAttribute.Value = "Professional";
                referenceNode.Attributes.Append(referenceTypeAttribute);
                xmlDoc.DocumentElement.PrependChild(referenceNode);
                XmlElement refPersonNameNode = xmlDoc.CreateElement("PersonName");
                xmlDoc.DocumentElement.PrependChild(refPersonNameNode);
                referenceNode.AppendChild(refPersonNameNode);
                XmlElement refPersonFormattedNode = xmlDoc.CreateElement("FormattedName");
                xmlDoc.DocumentElement.PrependChild(refPersonFormattedNode);
                refPersonNameNode.AppendChild(refPersonFormattedNode);
                XmlElement refPositionTitleNode = xmlDoc.CreateElement("PositionTitle");
                xmlDoc.DocumentElement.PrependChild(refPositionTitleNode);
                referenceNode.AppendChild(refPositionTitleNode);
                XmlElement refContactMethodNode = xmlDoc.CreateElement("ContactMethod");
                xmlDoc.DocumentElement.PrependChild(refContactMethodNode);
                referenceNode.AppendChild(refContactMethodNode);
                XmlElement refContactMethodTelephoneNode = xmlDoc.CreateElement("Telephone");
                xmlDoc.DocumentElement.PrependChild(refContactMethodTelephoneNode);
                refContactMethodNode.AppendChild(refContactMethodTelephoneNode);
                XmlElement refContactMethodTelephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
                xmlDoc.DocumentElement.PrependChild(refContactMethodTelephoneFormattedNode);
                refContactMethodTelephoneNode.AppendChild(refContactMethodTelephoneFormattedNode);
                XmlElement refContactMethodInternetNode = xmlDoc.CreateElement("InternetEmailAddress");
                xmlDoc.DocumentElement.PrependChild(refContactMethodInternetNode);
                refContactMethodNode.AppendChild(refContactMethodInternetNode);
                XmlElement refCommentsNode = xmlDoc.CreateElement("Comments");
                xmlDoc.DocumentElement.PrependChild(refCommentsNode);
                referenceNode.AppendChild(refCommentsNode);
                oText = xmlDoc.CreateTextNode(txtRefName.Text.Trim());
                refPersonFormattedNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefOrg.Text.Trim());
                refCommentsNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefRelation.Text.Trim());
                refPositionTitleNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefContactInfo.Text.Trim());
                refContactMethodInternetNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefContactInfo.Text.Trim());
                refContactMethodTelephoneFormattedNode.AppendChild(oText);
                referencesNode.AppendChild(referenceNode);
            }
            structuredXMLNode.AppendChild(referencesNode);
            // Construct the SecurityCredentials
            XmlElement securityCredentialsNode = xmlDoc.CreateElement("SecurityCredentials");
            xmlDoc.DocumentElement.PrependChild(securityCredentialsNode);
            structuredXMLNode.AppendChild(securityCredentialsNode);
            // Construct the ResumeAdditionalItems
            XmlElement resumeAdditionItemsNode = xmlDoc.CreateElement("ResumeAdditionalItems");
            xmlDoc.DocumentElement.PrependChild(resumeAdditionItemsNode);
            structuredXMLNode.AppendChild(resumeAdditionItemsNode);
            XmlElement techSkillsNode = xmlDoc.CreateElement("TechnicalSkills");
            xmlDoc.DocumentElement.PrependChild(techSkillsNode);
            // To get technicalskills data
            TechnicalSkillsControl oTechCntl = (TechnicalSkillsControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_technicalSkillsControl");
            TextBox txtLangauages = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_languageTextBox");
            TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_osTextBox");
            TextBox txtDatabases = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_databaseTextBox");
            TextBox txtUITools = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_uiToolsTextBox");
            TextBox txtOtherSkills = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_otherSkillsTextBox");
            XmlElement languageNode = xmlDoc.CreateElement("Languages");
            xmlDoc.DocumentElement.PrependChild(languageNode);
            XmlElement operaSysNode = xmlDoc.CreateElement("OperatingSystem");
            xmlDoc.DocumentElement.PrependChild(operaSysNode);
            XmlElement databaseNode = xmlDoc.CreateElement("Databases");
            xmlDoc.DocumentElement.PrependChild(databaseNode);
            XmlElement uiToolsNode = xmlDoc.CreateElement("UITools");
            xmlDoc.DocumentElement.PrependChild(uiToolsNode);
            XmlElement otherSkillsNode = xmlDoc.CreateElement("OtherSkills");
            xmlDoc.DocumentElement.PrependChild(otherSkillsNode);
            oText = xmlDoc.CreateTextNode(txtLangauages.Text.Trim());
            languageNode.AppendChild(oText);
            techSkillsNode.AppendChild(languageNode);
            oText = xmlDoc.CreateTextNode(txtOperaSystem.Text.Trim());
            operaSysNode.AppendChild(oText);
            techSkillsNode.AppendChild(operaSysNode);
            oText = xmlDoc.CreateTextNode(txtDatabases.Text.Trim());
            databaseNode.AppendChild(oText);
            techSkillsNode.AppendChild(databaseNode);
            oText = xmlDoc.CreateTextNode(txtUITools.Text.Trim());
            uiToolsNode.AppendChild(oText);
            techSkillsNode.AppendChild(uiToolsNode);
            oText = xmlDoc.CreateTextNode(txtOtherSkills.Text.Trim());
            otherSkillsNode.AppendChild(oText);
            techSkillsNode.AppendChild(otherSkillsNode);

            structuredXMLNode.AppendChild(techSkillsNode);
            XmlElement projectNode = xmlDoc.CreateElement("ProjectHistory");
            xmlDoc.DocumentElement.PrependChild(projectNode);
            // To get projects data
            ProjectsControl oProjCntl = (ProjectsControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_projectsControl");
            ListView oProjList = (ListView)oProjCntl.FindControl("ProjectsControl_listView");
            foreach (ListViewDataItem item in oProjList.Items)
            {
                TextBox txtProjName = (TextBox)item.FindControl("ProjectsControl_projectNameTextBox");
                TextBox txtProjDesc = (TextBox)item.FindControl("ProjectsControl_projectDescTextBox");
                TextBox txtProjRole = (TextBox)item.FindControl("ProjectsControl_roleTextBox");
                TextBox txtProjPosition = (TextBox)item.FindControl("ProjectsControl_positionTextBox");
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                TextBox txtProjLocation = (TextBox)item.FindControl("ProjectsControl_locationTextBox");
                TextBox txtProjEnvironment = (TextBox)item.FindControl("ProjectsControl_environmentTextBox");
                TextBox txtProjClientName = (TextBox)item.FindControl("ProjectsControl_clientNameTextBox");
                TextBox txtProjClientIndustry = (TextBox)item.FindControl("ProjectsControl_clientIndustryTextBox");
                XmlElement projectNonode = xmlDoc.CreateElement("Project");
                xmlDoc.DocumentElement.PrependChild(projectNonode);
                XmlElement projectNameNode = xmlDoc.CreateElement("ProjectName");
                xmlDoc.DocumentElement.PrependChild(projectNameNode);
                XmlElement projDescNode = xmlDoc.CreateElement("Description");
                xmlDoc.DocumentElement.PrependChild(projDescNode);
                XmlElement projRoleNode = xmlDoc.CreateElement("Role");
                xmlDoc.DocumentElement.PrependChild(projRoleNode);
                XmlElement projPositionNode = xmlDoc.CreateElement("Position");
                xmlDoc.DocumentElement.PrependChild(projPositionNode);
                XmlElement projStartDateNode = xmlDoc.CreateElement("StartDate");
                xmlDoc.DocumentElement.PrependChild(projStartDateNode);
                XmlElement projEndDateNode = xmlDoc.CreateElement("EndDate");
                xmlDoc.DocumentElement.PrependChild(projEndDateNode);
                XmlElement projLocationNode = xmlDoc.CreateElement("Location");
                xmlDoc.DocumentElement.PrependChild(projLocationNode);
                XmlElement projEnvironmentNode = xmlDoc.CreateElement("Environment");
                xmlDoc.DocumentElement.PrependChild(projEnvironmentNode);
                XmlElement projClientNameNode = xmlDoc.CreateElement("ClientName");
                xmlDoc.DocumentElement.PrependChild(projClientNameNode);
                XmlElement projClientIndustryNode = xmlDoc.CreateElement("ClientIndustry");
                xmlDoc.DocumentElement.PrependChild(projClientIndustryNode);
                oText = xmlDoc.CreateTextNode(txtProjName.Text.Trim());
                projectNameNode.AppendChild(oText);
                projectNonode.AppendChild(projectNameNode);
                oText = xmlDoc.CreateTextNode(txtProjDesc.Text.Trim());
                projDescNode.AppendChild(oText);
                projectNonode.AppendChild(projDescNode);
                oText = xmlDoc.CreateTextNode(txtProjRole.Text.Trim());
                projRoleNode.AppendChild(oText);
                projectNonode.AppendChild(projRoleNode);
                oText = xmlDoc.CreateTextNode(txtProjPosition.Text.Trim());
                projPositionNode.AppendChild(oText);
                projectNonode.AppendChild(projPositionNode);
                oText = xmlDoc.CreateTextNode(txtProjStartDate.Text.Trim());
                projStartDateNode.AppendChild(oText);
                projectNonode.AppendChild(projStartDateNode);
                oText = xmlDoc.CreateTextNode(txtProjEndDate.Text.Trim());
                projEndDateNode.AppendChild(oText);
                projectNonode.AppendChild(projEndDateNode);
                oText = xmlDoc.CreateTextNode(txtProjLocation.Text.Trim());
                projLocationNode.AppendChild(oText);
                projectNonode.AppendChild(projLocationNode);
                oText = xmlDoc.CreateTextNode(txtProjEnvironment.Text.Trim());
                projEnvironmentNode.AppendChild(oText);
                projectNonode.AppendChild(projEnvironmentNode);
                oText = xmlDoc.CreateTextNode(txtProjClientName.Text.Trim());
                projClientNameNode.AppendChild(oText);
                projectNonode.AppendChild(projClientNameNode);
                oText = xmlDoc.CreateTextNode(txtProjClientIndustry.Text.Trim());
                projClientIndustryNode.AppendChild(oText);
                projectNonode.AppendChild(projClientIndustryNode);
                projectNode.AppendChild(projectNonode);
            }
            structuredXMLNode.AppendChild(projectNode);
            // Construct the ResumeAdditionalItems
            XmlElement revisionDateNode = xmlDoc.CreateElement("RevisionDate");
            xmlDoc.DocumentElement.PrependChild(revisionDateNode);
            oText = xmlDoc.CreateTextNode(DateTime.Now.ToString());
            revisionDateNode.AppendChild(oText);
            structuredXMLNode.AppendChild(revisionDateNode);
            try
            {
                // Construct NonXMLResume
                XmlElement textResumeNode = xmlDoc.CreateElement("TextResume");
                xmlDoc.DocumentElement.PrependChild(textResumeNode);

                if (Session["RESUME_CONTENTS"] != null)
                    oText = xmlDoc.CreateTextNode(Session["RESUME_CONTENTS"].ToString());
                else
                    oText = xmlDoc.CreateTextNode(string.Empty);

                textResumeNode.AppendChild(oText);
                nonXMLResumeNode.AppendChild(textResumeNode);
            }
            catch { }
            XmlElement supportingMatterialsNode = xmlDoc.CreateElement("SupportingMaterials");
            xmlDoc.DocumentElement.PrependChild(supportingMatterialsNode);
            nonXMLResumeNode.AppendChild(supportingMatterialsNode);
            XmlElement nonXMLLinkNode = xmlDoc.CreateElement("Link");
            xmlDoc.DocumentElement.PrependChild(nonXMLLinkNode);
            supportingMatterialsNode.AppendChild(nonXMLLinkNode);
            XmlElement nonXMLDescNode = xmlDoc.CreateElement("Description");
            xmlDoc.DocumentElement.PrependChild(nonXMLDescNode);
            supportingMatterialsNode.AppendChild(nonXMLDescNode);

            return xmlDoc;
        }

        /// <summary>
        /// This method helps to construct the hrxml without file contents.
        /// </summary>
        /// <param name="IdValue">
        /// A <see cref="string"/> that holds the candidateid.
        /// </param>
        private XmlDocument WriteHRXml(string IdValue)
        {
            XmlDocument xmlDoc = new XmlDocument();
            //XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlText oText;
            // Create the root element
            XmlElement rootNode = xmlDoc.CreateElement("Resume");
            // xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
            xmlDoc.AppendChild(rootNode);
            // Contstruct the ResumeID Node
            XmlElement resumeIdNode = xmlDoc.CreateElement("ResumeId");
            xmlDoc.DocumentElement.PrependChild(resumeIdNode);
            XmlElement idNode = xmlDoc.CreateElement("IdValue");
            xmlDoc.DocumentElement.PrependChild(idNode);
            oText = xmlDoc.CreateTextNode(IdValue);
            idNode.AppendChild(oText);
            resumeIdNode.AppendChild(idNode);
            rootNode.AppendChild(resumeIdNode);
            // Construct the DistributionGuidelinesnodes
            XmlElement distributionGuideNode = xmlDoc.CreateElement("DistributionGuidelines");
            xmlDoc.DocumentElement.PrependChild(distributionGuideNode);
            rootNode.AppendChild(distributionGuideNode);
            // Construct the StucturedXMLNode
            XmlElement structuredXMLNode = xmlDoc.CreateElement("StructuredXMLResume");
            xmlDoc.DocumentElement.PrependChild(structuredXMLNode);
            rootNode.AppendChild(structuredXMLNode);
            // Construct the NonXMLResume
            XmlElement nonXMLResumeNode = xmlDoc.CreateElement("NonXMLResume");
            xmlDoc.DocumentElement.PrependChild(nonXMLResumeNode);
            rootNode.AppendChild(nonXMLResumeNode);
            // Construct the contactInfoNode
            XmlElement contactInfoNode = xmlDoc.CreateElement("ContactInfo");
            xmlDoc.DocumentElement.PrependChild(contactInfoNode);
            structuredXMLNode.AppendChild(contactInfoNode);
            // Construct the PersonName
            XmlElement personNameNode = xmlDoc.CreateElement("PersonName");
            xmlDoc.DocumentElement.PrependChild(personNameNode);
            contactInfoNode.AppendChild(personNameNode);
            // Construct the FirstName
            XmlElement personFirstNameNode = xmlDoc.CreateElement("FirstName");
            xmlDoc.DocumentElement.PrependChild(personFirstNameNode);
            contactInfoNode.AppendChild(personFirstNameNode);
            // Construct the MiddleName
            XmlElement personMiddleNameNode = xmlDoc.CreateElement("MiddleName");
            xmlDoc.DocumentElement.PrependChild(personMiddleNameNode);
            contactInfoNode.AppendChild(personMiddleNameNode);
            // Construct the LastName
            XmlElement personLastNameNode = xmlDoc.CreateElement("LastName");
            xmlDoc.DocumentElement.PrependChild(personLastNameNode);
            contactInfoNode.AppendChild(personLastNameNode);
            // Construct the FormattedName
            XmlElement personFormattedNameNode = xmlDoc.CreateElement("FormattedName");
            xmlDoc.DocumentElement.PrependChild(personFormattedNameNode);
            personNameNode.AppendChild(personFormattedNameNode);
            // To get name data and build xml
            NameControl nc = (NameControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_nameControl");
            TextBox txtFirstName = (TextBox)nc.FindControl("NameControl_firstNameTextBox");
            TextBox txtMiddleName = (TextBox)nc.FindControl("NameControl_middleNameTextBox");
            TextBox txtLastName = (TextBox)nc.FindControl("NameControl_lastNameTextBox");
            oText = xmlDoc.CreateTextNode(txtFirstName.Text.Trim() + " " +
                txtMiddleName.Text.Trim() + " " + txtLastName.Text.Trim());
            personFormattedNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtFirstName.Text.Trim());
            personFirstNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtMiddleName.Text.Trim());
            personMiddleNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtLastName.Text.Trim());
            personLastNameNode.AppendChild(oText);
            // Construct the ContactMethod
            XmlElement contactMethodNode = xmlDoc.CreateElement("ContactMethod");
            xmlDoc.DocumentElement.PrependChild(contactMethodNode);
            contactInfoNode.AppendChild(contactMethodNode);
            // Construct the ContactTelephone
            XmlElement contactMethodTelephoneNode = xmlDoc.CreateElement("Telephone");
            xmlDoc.DocumentElement.PrependChild(contactMethodTelephoneNode);
            contactMethodNode.AppendChild(contactMethodTelephoneNode);
            XmlElement contactMethodTelephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodTelephoneFormattedNode);
            contactMethodTelephoneNode.AppendChild(contactMethodTelephoneFormattedNode);
            // Construct the HomePhone
            XmlElement contactMethodHomephoneNode = xmlDoc.CreateElement("HomePhone");
            xmlDoc.DocumentElement.PrependChild(contactMethodHomephoneNode);
            contactMethodNode.AppendChild(contactMethodHomephoneNode);
            XmlElement contactMethodHomephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodHomephoneFormattedNode);
            contactMethodHomephoneNode.AppendChild(contactMethodHomephoneFormattedNode);
            // Construct the ContactFax
            XmlElement contactMethodFaxNode = xmlDoc.CreateElement("Fax");
            xmlDoc.DocumentElement.PrependChild(contactMethodFaxNode);
            contactMethodNode.AppendChild(contactMethodFaxNode);
            XmlElement contactMethodFaxFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodFaxFormattedNode);
            contactMethodFaxNode.AppendChild(contactMethodFaxFormattedNode);
            // Construct the InternetEmailAddress
            XmlElement contactMethodEmailNode = xmlDoc.CreateElement("InternetEmailAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodEmailNode);
            contactMethodNode.AppendChild(contactMethodEmailNode);
            // Construct the InternetWebAddress
            XmlElement contactMethodInternetNode = xmlDoc.CreateElement("InternetWebAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodInternetNode);
            contactMethodNode.AppendChild(contactMethodInternetNode);
            // Construct the PostalAddress
            XmlElement contactMethodPostalAddressNode = xmlDoc.CreateElement("PostalAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodPostalAddressNode);
            contactMethodNode.AppendChild(contactMethodPostalAddressNode);
            // To get contactinformation data & Update the RootNode ContactInformation
            ContactInformationControl cil = (ContactInformationControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_contactInfoControl");
            TextBox txtAddress = (TextBox)cil.FindControl("ContactInformationControl_streetTextBox");
            TextBox txtCity = (TextBox)cil.FindControl("ContactInformationControl_cityTextBox");
            TextBox txtState = (TextBox)cil.FindControl("ContactInformationControl_stateTextBox");
            TextBox txtCountry = (TextBox)cil.FindControl("ContactInformationControl_countryNameTextBox");
            TextBox txtPostalCode = (TextBox)cil.FindControl("ContactInformationControl_postalCodeTextBox");
            TextBox txtFixedLine = (TextBox)cil.FindControl("ContactInformationControl_fixedLineTextBox");
            TextBox txtMobile = (TextBox)cil.FindControl("ContactInformationControl_mobileTextBox");
            TextBox txtEmail = (TextBox)cil.FindControl("ContactInformationControl_emailTextBox");
            TextBox txtWebSite = (TextBox)cil.FindControl("ContactInformationControl_websiteTextBox");
            XmlElement contactCountryNode = xmlDoc.CreateElement("CountryCode");
            xmlDoc.DocumentElement.PrependChild(contactCountryNode);
            XmlElement contactPostalCodeNode = xmlDoc.CreateElement("PostalCode");
            xmlDoc.DocumentElement.PrependChild(contactPostalCodeNode);
            XmlElement contactRegionNode = xmlDoc.CreateElement("Region");
            xmlDoc.DocumentElement.PrependChild(contactRegionNode);
            XmlElement contactCityNode = xmlDoc.CreateElement("Municipality");
            xmlDoc.DocumentElement.PrependChild(contactCityNode);
            XmlElement contactStateNode = xmlDoc.CreateElement("State");
            xmlDoc.DocumentElement.PrependChild(contactStateNode);
            XmlElement contactAddressNode = xmlDoc.CreateElement("Street");
            xmlDoc.DocumentElement.PrependChild(contactAddressNode);
            oText = xmlDoc.CreateTextNode(txtCountry.Text.Trim());
            contactCountryNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactCountryNode);
            oText = xmlDoc.CreateTextNode(txtPostalCode.Text.Trim());
            contactPostalCodeNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactPostalCodeNode);
            contactMethodPostalAddressNode.AppendChild(contactRegionNode);
            oText = xmlDoc.CreateTextNode(txtCity.Text.Trim());
            contactCityNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactCityNode);
            oText = xmlDoc.CreateTextNode(txtState.Text.Trim());
            contactStateNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactStateNode);
            oText = xmlDoc.CreateTextNode(txtAddress.Text.Trim());
            contactAddressNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactAddressNode);
            oText = xmlDoc.CreateTextNode(txtFixedLine.Text.Trim());
            contactMethodFaxFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtFixedLine.Text.Trim());
            contactMethodHomephoneFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtMobile.Text.Trim());
            contactMethodTelephoneFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtEmail.Text.Trim());
            contactMethodEmailNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtWebSite.Text.Trim());
            contactMethodInternetNode.AppendChild(oText);
            contactInfoNode.AppendChild(contactMethodNode);
            // Construct the ExecutiveSummary
            XmlElement execSummaryNode = xmlDoc.CreateElement("ExecutiveSummary");
            xmlDoc.DocumentElement.PrependChild(execSummaryNode);
            structuredXMLNode.AppendChild(execSummaryNode);
            // To get executivesummary data
            ExecutiveSummaryControl oExecCntl = (ExecutiveSummaryControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_executiveSummaryControl");
            TextBox txtExecutiveSummary = (TextBox)oExecCntl.
                FindControl("ExecutiveSummaryControl_execSummaryTextBox");
            oText = xmlDoc.CreateTextNode(txtExecutiveSummary.Text.Trim());
            execSummaryNode.AppendChild(oText);
            structuredXMLNode.AppendChild(execSummaryNode);
            // Construct the Objective
            XmlElement objectiveNode = xmlDoc.CreateElement("Objective");
            xmlDoc.DocumentElement.PrependChild(objectiveNode);
            structuredXMLNode.AppendChild(objectiveNode);
            // Construct the EmploymentHistory
            XmlElement empHistoryNode = xmlDoc.CreateElement("EmploymentHistory");
            xmlDoc.DocumentElement.PrependChild(empHistoryNode);
            structuredXMLNode.AppendChild(empHistoryNode);
            // Construct the EducationHistory
            XmlElement educationHistoryNode = xmlDoc.CreateElement("EducationHistory");
            xmlDoc.DocumentElement.PrependChild(educationHistoryNode);
            structuredXMLNode.AppendChild(educationHistoryNode);
            // To get educational data
            EducationControl oEduCntl = (EducationControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_educationControl");
            ListView oEduList = (ListView)oEduCntl.FindControl("EducationControl_listView");
            foreach (ListViewDataItem item in oEduList.Items)
            {
                TextBox txtEduName = (TextBox)item.FindControl("EducationControl_nameTextBox");
                TextBox txtEduLocation = (TextBox)item.FindControl("EducationControl_locationTextBox");
                TextBox txtEduDegree = (TextBox)item.FindControl("EducationControl_degreeTextBox");
                TextBox txtEduSpec = (TextBox)item.FindControl("EducationControl_specializationTextBox");
                TextBox txtEduGPA = (TextBox)item.FindControl("EducationControl_gpaTextBox");
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                XmlElement eduSchoolOrInstitutionNode = xmlDoc.CreateElement("SchoolOrInstitution");
                XmlAttribute eduSchollOrInstitutionAttribute = xmlDoc.CreateAttribute("schoolType");
                eduSchollOrInstitutionAttribute.Value = "trade";
                eduSchoolOrInstitutionNode.Attributes.Append(eduSchollOrInstitutionAttribute);
                xmlDoc.DocumentElement.PrependChild(eduSchoolOrInstitutionNode);
                XmlElement eduSchoolNode = xmlDoc.CreateElement("School");
                xmlDoc.DocumentElement.PrependChild(eduSchoolNode);
                eduSchoolOrInstitutionNode.AppendChild(eduSchoolNode);
                XmlElement eduInternetDomainNameNode = xmlDoc.CreateElement("InternetDomainName");
                xmlDoc.DocumentElement.PrependChild(eduInternetDomainNameNode);
                eduSchoolNode.AppendChild(eduInternetDomainNameNode);
                XmlElement eduSchoolNameNode = xmlDoc.CreateElement("SchoolName");
                xmlDoc.DocumentElement.PrependChild(eduSchoolNameNode);
                eduSchoolNode.AppendChild(eduSchoolNameNode);
                XmlElement eduDegreeNode = xmlDoc.CreateElement("Degree");
                XmlAttribute eduDegreeTypeAttribute = xmlDoc.CreateAttribute("degreeType");
                eduDegreeTypeAttribute.Value = "Master";
                eduDegreeNode.Attributes.Append(eduDegreeTypeAttribute);
                xmlDoc.DocumentElement.PrependChild(eduDegreeNode);
                eduSchoolOrInstitutionNode.AppendChild(eduDegreeNode);
                XmlElement eduDegreeNameNode = xmlDoc.CreateElement("DegreeName");
                xmlDoc.DocumentElement.PrependChild(eduDegreeNameNode);
                eduDegreeNode.AppendChild(eduDegreeNameNode);
                XmlElement eduDegreeDateNode = xmlDoc.CreateElement("DegreeDate");
                xmlDoc.DocumentElement.PrependChild(eduDegreeDateNode);
                eduDegreeNode.AppendChild(eduDegreeDateNode);
                XmlElement eduDegreeYearMonthNode = xmlDoc.CreateElement("YearMonth");
                xmlDoc.DocumentElement.PrependChild(eduDegreeYearMonthNode);
                eduDegreeDateNode.AppendChild(eduDegreeYearMonthNode);
                XmlElement eduDegreeMajorNode = xmlDoc.CreateElement("DegreeMajor");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMajorNode);
                eduDegreeNode.AppendChild(eduDegreeMajorNode);
                XmlElement eduDegreeMajorNameNode = xmlDoc.CreateElement("Name");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMajorNameNode);
                eduDegreeMajorNode.AppendChild(eduDegreeMajorNameNode);
                XmlElement eduDegreeMinorNode = xmlDoc.CreateElement("DegreeMinor");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMinorNode);
                eduDegreeNode.AppendChild(eduDegreeMinorNode);
                XmlElement eduDegreeMinorNameNode = xmlDoc.CreateElement("Name");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMinorNameNode);
                eduDegreeMinorNode.AppendChild(eduDegreeMinorNameNode);
                XmlElement eduDegreeCommentsNode = xmlDoc.CreateElement("Comments");
                xmlDoc.DocumentElement.PrependChild(eduDegreeCommentsNode);
                eduDegreeNode.AppendChild(eduDegreeCommentsNode);
                oText = xmlDoc.CreateTextNode(txtEduName.Text.Trim() + "," +
                    txtEduLocation.Text.Trim());
                eduSchoolNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduDegree.Text.Trim());
                eduDegreeNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduGradDate.Text.Trim());
                eduDegreeYearMonthNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduSpec.Text.Trim());
                eduDegreeMajorNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduGPA.Text.Trim());
                eduDegreeCommentsNode.AppendChild(oText);
                educationHistoryNode.AppendChild(eduSchoolOrInstitutionNode);
            }
            structuredXMLNode.AppendChild(educationHistoryNode);
            // Construct the LicenceAndCertification
            XmlElement licenceAndCertifyNode = xmlDoc.CreateElement("LicenceAndCertification");
            xmlDoc.DocumentElement.PrependChild(licenceAndCertifyNode);
            structuredXMLNode.AppendChild(licenceAndCertifyNode);
            // Construct the PatentHistory
            XmlElement patentHistoryNode = xmlDoc.CreateElement("PatentHistory");
            xmlDoc.DocumentElement.PrependChild(patentHistoryNode);
            structuredXMLNode.AppendChild(patentHistoryNode);
            // Construct the PublicationHistory
            XmlElement publicationHistoryNode = xmlDoc.CreateElement("PublicationHistory");
            xmlDoc.DocumentElement.PrependChild(publicationHistoryNode);
            structuredXMLNode.AppendChild(publicationHistoryNode);
            // Construct the SpeakingEventsHistory
            XmlElement speakingEventHistory = xmlDoc.CreateElement("SpeakingEventsHistory");
            xmlDoc.DocumentElement.PrependChild(speakingEventHistory);
            structuredXMLNode.AppendChild(speakingEventHistory);
            // Construct the Qualifications
            XmlElement qualificationNode = xmlDoc.CreateElement("Qualifications");
            xmlDoc.DocumentElement.PrependChild(qualificationNode);
            structuredXMLNode.AppendChild(qualificationNode);
            // Construct the Languages
            XmlElement languagesNode = xmlDoc.CreateElement("Languages");
            xmlDoc.DocumentElement.PrependChild(languagesNode);
            structuredXMLNode.AppendChild(languagesNode);
            // Construct the Achievements
            XmlElement achivementsNode = xmlDoc.CreateElement("Achievements");
            xmlDoc.DocumentElement.PrependChild(achivementsNode);
            structuredXMLNode.AppendChild(achivementsNode);
            // Construct the Associations
            XmlElement associationsNode = xmlDoc.CreateElement("Associations");
            xmlDoc.DocumentElement.PrependChild(associationsNode);
            structuredXMLNode.AppendChild(associationsNode);
            // Construct the References
            XmlElement referencesNode = xmlDoc.CreateElement("References");
            xmlDoc.DocumentElement.PrependChild(referencesNode);
            structuredXMLNode.AppendChild(referencesNode);
            // To get references data
            ReferencesControl oRefCntl = (ReferencesControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_referencesControl");
            ListView oRefList = (ListView)oRefCntl.FindControl("ReferencesControl_listView");
            foreach (ListViewDataItem item in oRefList.Items)
            {
                TextBox txtRefName = (TextBox)item.FindControl("ReferencesControl_nameTextBox");
                TextBox txtRefOrg = (TextBox)item.FindControl("ReferencesControl_organizationTextBox");
                TextBox txtRefRelation = (TextBox)item.FindControl("ReferencesControl_relationTextBox");
                TextBox txtRefContactInfo = (TextBox)item.FindControl("ReferencesControl_contInfoTextBox");
                XmlElement referenceNode = xmlDoc.CreateElement("Reference");
                XmlAttribute referenceTypeAttribute = xmlDoc.CreateAttribute("type");
                referenceTypeAttribute.Value = "Professional";
                referenceNode.Attributes.Append(referenceTypeAttribute);
                xmlDoc.DocumentElement.PrependChild(referenceNode);
                XmlElement refPersonNameNode = xmlDoc.CreateElement("PersonName");
                xmlDoc.DocumentElement.PrependChild(refPersonNameNode);
                referenceNode.AppendChild(refPersonNameNode);
                XmlElement refPersonFormattedNode = xmlDoc.CreateElement("FormattedName");
                xmlDoc.DocumentElement.PrependChild(refPersonFormattedNode);
                refPersonNameNode.AppendChild(refPersonFormattedNode);
                XmlElement refPositionTitleNode = xmlDoc.CreateElement("PositionTitle");
                xmlDoc.DocumentElement.PrependChild(refPositionTitleNode);
                referenceNode.AppendChild(refPositionTitleNode);
                XmlElement refContactMethodNode = xmlDoc.CreateElement("ContactMethod");
                xmlDoc.DocumentElement.PrependChild(refContactMethodNode);
                referenceNode.AppendChild(refContactMethodNode);
                XmlElement refContactMethodTelephoneNode = xmlDoc.CreateElement("Telephone");
                xmlDoc.DocumentElement.PrependChild(refContactMethodTelephoneNode);
                refContactMethodNode.AppendChild(refContactMethodTelephoneNode);
                XmlElement refContactMethodTelephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
                xmlDoc.DocumentElement.PrependChild(refContactMethodTelephoneFormattedNode);
                refContactMethodTelephoneNode.AppendChild(refContactMethodTelephoneFormattedNode);
                XmlElement refContactMethodInternetNode = xmlDoc.CreateElement("InternetEmailAddress");
                xmlDoc.DocumentElement.PrependChild(refContactMethodInternetNode);
                refContactMethodNode.AppendChild(refContactMethodInternetNode);
                XmlElement refCommentsNode = xmlDoc.CreateElement("Comments");
                xmlDoc.DocumentElement.PrependChild(refCommentsNode);
                referenceNode.AppendChild(refCommentsNode);
                oText = xmlDoc.CreateTextNode(txtRefName.Text.Trim());
                refPersonFormattedNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefOrg.Text.Trim());
                refCommentsNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefRelation.Text.Trim());
                refPositionTitleNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefContactInfo.Text.Trim());
                refContactMethodInternetNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefContactInfo.Text.Trim());
                refContactMethodTelephoneFormattedNode.AppendChild(oText);
                referencesNode.AppendChild(referenceNode);
            }
            structuredXMLNode.AppendChild(referencesNode);
            // Construct the SecurityCredentials
            XmlElement securityCredentialsNode = xmlDoc.CreateElement("SecurityCredentials");
            xmlDoc.DocumentElement.PrependChild(securityCredentialsNode);
            structuredXMLNode.AppendChild(securityCredentialsNode);
            // Construct the ResumeAdditionalItems
            XmlElement resumeAdditionItemsNode = xmlDoc.CreateElement("ResumeAdditionalItems");
            xmlDoc.DocumentElement.PrependChild(resumeAdditionItemsNode);
            structuredXMLNode.AppendChild(resumeAdditionItemsNode);
            XmlElement techSkillsNode = xmlDoc.CreateElement("TechnicalSkills");
            xmlDoc.DocumentElement.PrependChild(techSkillsNode);
            // To get technicalskills data
            TechnicalSkillsControl oTechCntl = (TechnicalSkillsControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_technicalSkillsControl");
            TextBox txtLangauages = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_languageTextBox");
            TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_osTextBox");
            TextBox txtDatabases = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_databaseTextBox");
            TextBox txtUITools = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_uiToolsTextBox");
            XmlElement languageNode = xmlDoc.CreateElement("Languages");
            xmlDoc.DocumentElement.PrependChild(languageNode);
            XmlElement operaSysNode = xmlDoc.CreateElement("OperatingSystem");
            xmlDoc.DocumentElement.PrependChild(operaSysNode);
            XmlElement databaseNode = xmlDoc.CreateElement("Databases");
            xmlDoc.DocumentElement.PrependChild(databaseNode);
            XmlElement uiToolsNode = xmlDoc.CreateElement("UITools");
            xmlDoc.DocumentElement.PrependChild(uiToolsNode);
            oText = xmlDoc.CreateTextNode(txtLangauages.Text.Trim());
            languageNode.AppendChild(oText);
            techSkillsNode.AppendChild(languageNode);
            oText = xmlDoc.CreateTextNode(txtOperaSystem.Text.Trim());
            operaSysNode.AppendChild(oText);
            techSkillsNode.AppendChild(operaSysNode);
            oText = xmlDoc.CreateTextNode(txtDatabases.Text.Trim());
            databaseNode.AppendChild(oText);
            techSkillsNode.AppendChild(databaseNode);
            oText = xmlDoc.CreateTextNode(txtUITools.Text.Trim());
            uiToolsNode.AppendChild(oText);
            techSkillsNode.AppendChild(uiToolsNode);
            structuredXMLNode.AppendChild(techSkillsNode);
            XmlElement projectNode = xmlDoc.CreateElement("ProjectHistory");
            xmlDoc.DocumentElement.PrependChild(projectNode);
            // To get projects data
            ProjectsControl oProjCntl = (ProjectsControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_projectsControl");
            ListView oProjList = (ListView)oProjCntl.FindControl("ProjectsControl_listView");
            foreach (ListViewDataItem item in oProjList.Items)
            {
                TextBox txtProjName = (TextBox)item.FindControl("ProjectsControl_projectNameTextBox");
                TextBox txtProjDesc = (TextBox)item.FindControl("ProjectsControl_projectDescTextBox");
                TextBox txtProjRole = (TextBox)item.FindControl("ProjectsControl_roleTextBox");
                TextBox txtProjPosition = (TextBox)item.FindControl("ProjectsControl_positionTextBox");
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                TextBox txtProjLocation = (TextBox)item.FindControl("ProjectsControl_locationTextBox");
                TextBox txtProjEnvironment = (TextBox)item.FindControl("ProjectsControl_environmentTextBox");
                TextBox txtProjClientName = (TextBox)item.FindControl("ProjectsControl_clientNameTextBox");
                TextBox txtProjClientIndustry = (TextBox)item.FindControl("ProjectsControl_clientIndustryTextBox");
                XmlElement projectNonode = xmlDoc.CreateElement("Project");
                xmlDoc.DocumentElement.PrependChild(projectNonode);
                XmlElement projectNameNode = xmlDoc.CreateElement("ProjectName");
                xmlDoc.DocumentElement.PrependChild(projectNameNode);
                XmlElement projDescNode = xmlDoc.CreateElement("Description");
                xmlDoc.DocumentElement.PrependChild(projDescNode);
                XmlElement projRoleNode = xmlDoc.CreateElement("Role");
                xmlDoc.DocumentElement.PrependChild(projRoleNode);
                XmlElement projPositionNode = xmlDoc.CreateElement("Position");
                xmlDoc.DocumentElement.PrependChild(projPositionNode);
                XmlElement projStartDateNode = xmlDoc.CreateElement("StartDate");
                xmlDoc.DocumentElement.PrependChild(projStartDateNode);
                XmlElement projEndDateNode = xmlDoc.CreateElement("EndDate");
                xmlDoc.DocumentElement.PrependChild(projEndDateNode);
                XmlElement projLocationNode = xmlDoc.CreateElement("Location");
                xmlDoc.DocumentElement.PrependChild(projLocationNode);
                XmlElement projEnvironmentNode = xmlDoc.CreateElement("Environment");
                xmlDoc.DocumentElement.PrependChild(projEnvironmentNode);
                XmlElement projClientNameNode = xmlDoc.CreateElement("ClientName");
                xmlDoc.DocumentElement.PrependChild(projClientNameNode);
                XmlElement projClientIndustryNode = xmlDoc.CreateElement("ClientIndustry");
                xmlDoc.DocumentElement.PrependChild(projClientIndustryNode);
                oText = xmlDoc.CreateTextNode(txtProjName.Text.Trim());
                projectNameNode.AppendChild(oText);
                projectNonode.AppendChild(projectNameNode);
                oText = xmlDoc.CreateTextNode(txtProjDesc.Text.Trim());
                projDescNode.AppendChild(oText);
                projectNonode.AppendChild(projDescNode);
                oText = xmlDoc.CreateTextNode(txtProjRole.Text.Trim());
                projRoleNode.AppendChild(oText);
                projectNonode.AppendChild(projRoleNode);
                oText = xmlDoc.CreateTextNode(txtProjPosition.Text.Trim());
                projPositionNode.AppendChild(oText);
                projectNonode.AppendChild(projPositionNode);
                oText = xmlDoc.CreateTextNode(txtProjStartDate.Text.Trim());
                projStartDateNode.AppendChild(oText);
                projectNonode.AppendChild(projStartDateNode);
                oText = xmlDoc.CreateTextNode(txtProjEndDate.Text.Trim());
                projEndDateNode.AppendChild(oText);
                projectNonode.AppendChild(projEndDateNode);
                oText = xmlDoc.CreateTextNode(txtProjLocation.Text.Trim());
                projLocationNode.AppendChild(oText);
                projectNonode.AppendChild(projLocationNode);
                oText = xmlDoc.CreateTextNode(txtProjEnvironment.Text.Trim());
                projEnvironmentNode.AppendChild(oText);
                projectNonode.AppendChild(projEnvironmentNode);
                oText = xmlDoc.CreateTextNode(txtProjClientName.Text.Trim());
                projClientNameNode.AppendChild(oText);
                projectNonode.AppendChild(projClientNameNode);
                oText = xmlDoc.CreateTextNode(txtProjClientIndustry.Text.Trim());
                projClientIndustryNode.AppendChild(oText);
                projectNonode.AppendChild(projClientIndustryNode);
                projectNode.AppendChild(projectNonode);
            }
            structuredXMLNode.AppendChild(projectNode);
            // Construct the ResumeAdditionalItems
            XmlElement revisionDateNode = xmlDoc.CreateElement("RevisionDate");
            xmlDoc.DocumentElement.PrependChild(revisionDateNode);
            oText = xmlDoc.CreateTextNode(DateTime.Now.ToString());
            revisionDateNode.AppendChild(oText);
            structuredXMLNode.AppendChild(revisionDateNode);
            try
            {
                // Construct NonXMLResume without textresume text node
                XmlElement textResumeNode = xmlDoc.CreateElement("TextResume");
                xmlDoc.DocumentElement.PrependChild(textResumeNode);
                nonXMLResumeNode.AppendChild(textResumeNode);
            }
            catch { }
            XmlElement supportingMatterialsNode = xmlDoc.CreateElement("SupportingMaterials");
            xmlDoc.DocumentElement.PrependChild(supportingMatterialsNode);
            nonXMLResumeNode.AppendChild(supportingMatterialsNode);
            XmlElement nonXMLLinkNode = xmlDoc.CreateElement("Link");
            xmlDoc.DocumentElement.PrependChild(nonXMLLinkNode);
            supportingMatterialsNode.AppendChild(nonXMLLinkNode);
            XmlElement nonXMLDescNode = xmlDoc.CreateElement("Description");
            xmlDoc.DocumentElement.PrependChild(nonXMLDescNode);
            supportingMatterialsNode.AppendChild(nonXMLDescNode);

            return xmlDoc;
        }

        /// <summary>
        /// This method helps to extract the hrxml data and display its contents.
        /// </summary>
        private void ProcessHRXml()
        {
            // Retrieve the resume detail for the selected candidate resume ID.
            ResumeDetail resumeDetail = new ResumeRepositoryBLManager().GetCandidateResumeDetail
                (Convert.ToInt32(ResumeEditor_candidateResumeIDHiddenField.Value));

            if (resumeDetail == null || resumeDetail.HRXML == null || resumeDetail.HRXML.Trim().Length == 0)
            {
                ShowMessage(ResumeEditor_topErrorMessageLabel, Resources.HCMResource.ResumeEditor_NoInformationFound);
                ResumeEditor_downloadImageButton.Visible = false;
                return;
            }
         
            ResumeEditor_rolesVectorControl.DataSource = ResumeEditor_candidateResumeIDHiddenField.Value;
            ResumeEditor_downloadImageButton.Visible = true;
            XmlDocument oXmlDoc = new XmlDocument();
            oXmlDoc.LoadXml(resumeDetail.HRXML);

            //Check the Xml nodes from the database
            CheckHRXMLNodes(oXmlDoc);
            if (oXmlDoc != null)
            {
                // Set Person Name 
                SetPersonalInfo(oXmlDoc);

                // Set Projects
                SetProjects(oXmlDoc);

                // Set EducationDetails
                SetEducation(oXmlDoc);

                // Set References
                SetReferences(oXmlDoc);

                // Set Preview Resume
                SetPreviewResume(oXmlDoc);
            }

            // Always show save button.
            ResumeEditor_topSaveButton.Visible = true;
        }

        /// <summary>
        /// This method helps to extract the hrxml personal data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPersonalInfo(XmlDocument oXmlDoc)
        {
            XmlNodeList oPersonNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ContactInfo/PersonName");
            if (oPersonNodeList.Count > 0)
            {
                NameControl nc = (NameControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_nameControl");
                TextBox txtFirstName = (TextBox)nc.FindControl("NameControl_firstNameTextBox");
                TextBox txtMiddleName = (TextBox)nc.FindControl("NameControl_middleNameTextBox");
                TextBox txtLastName = (TextBox)nc.FindControl("NameControl_lastNameTextBox");

                XmlNode personalDetail = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo");
                txtFirstName.Text = GetInnerText(personalDetail,"FirstName");
                txtMiddleName.Text = GetInnerText(personalDetail, "MiddleName");
                txtLastName.Text = GetInnerText(personalDetail, "LastName");               
            }

            XmlNodeList oContactInfoNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
            if (oContactInfoNodeList.Count > 0)
            {
                ContactInformationControl cil = (ContactInformationControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_contactInfoControl");
                TextBox txtAddress = (TextBox)cil.FindControl("ContactInformationControl_streetTextBox");
                TextBox txtCity = (TextBox)cil.FindControl("ContactInformationControl_cityTextBox");
                TextBox txtState = (TextBox)cil.FindControl("ContactInformationControl_stateTextBox");
                TextBox txtCountry = (TextBox)cil.FindControl("ContactInformationControl_countryNameTextBox");
                TextBox txtPostalCode = (TextBox)cil.FindControl("ContactInformationControl_postalCodeTextBox");
                TextBox txtFixedLine = (TextBox)cil.FindControl("ContactInformationControl_fixedLineTextBox");
                TextBox txtMobile = (TextBox)cil.FindControl("ContactInformationControl_mobileTextBox");
                TextBox txtEmail = (TextBox)cil.FindControl("ContactInformationControl_emailTextBox");
                TextBox txtWebSite = (TextBox)cil.FindControl("ContactInformationControl_websiteTextBox");
                // Set the TechnicalSkills Value         
                TechnicalSkillsControl oTechCntl = (TechnicalSkillsControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_technicalSkillsControl");
                TextBox txtLangauages = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_languageTextBox");
                TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_osTextBox");
                TextBox txtDatabases = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_databaseTextBox");
                TextBox txtUITools = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_uiToolsTextBox");
                TextBox txtOtherSkills = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_otherSkillsTextBox");

                XmlNode contactInfo = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
                XmlNode telephoneNode = contactInfo.SelectSingleNode("Telephone");
                txtMobile.Text = GetInnerText(telephoneNode, "FormattedNumber");

                XmlNode fixedLineNode = contactInfo.SelectSingleNode("Fax");
                txtFixedLine.Text = GetInnerText(fixedLineNode, "FormattedNumber");
               
                txtEmail.Text = GetInnerText(contactInfo,"InternetEmailAddress");
                txtWebSite.Text = GetInnerText(contactInfo,"InternetWebAddress");              

                XmlNode postalAddress = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress");
                txtState.Text = GetInnerText(postalAddress, "State");
                txtAddress.Text = GetInnerText(postalAddress, "Street");
                txtCity.Text = GetInnerText(postalAddress,"Municipality");
                txtCountry.Text = GetInnerText(postalAddress,"CountryCode");
                txtPostalCode.Text = GetInnerText(postalAddress,"PostalCode");           

                XmlNode techSkill = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/TechnicalSkills");
                txtLangauages.Text = GetInnerText(techSkill, "Languages");
                txtOperaSystem.Text = GetInnerText(techSkill, "OperatingSystem");
                txtDatabases.Text = GetInnerText(techSkill, "Databases");
                txtUITools.Text = GetInnerText(techSkill, "UITools");
                txtOtherSkills.Text = GetInnerText(techSkill, "OtherSkills");   
            }
            // Set the ExecutiveSummary Value
            XmlNodeList oExecSummaryNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ExecutiveSummary");
            if (oExecSummaryNodeList.Count > 0)
            {
                ExecutiveSummaryControl oExecCntl = (ExecutiveSummaryControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_executiveSummaryControl");
                TextBox txtExecutiveSummary = (TextBox)oExecCntl.FindControl("ExecutiveSummaryControl_execSummaryTextBox");
                txtExecutiveSummary.Text = oExecSummaryNodeList.Item(0).InnerText;
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml projects data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetProjects(XmlDocument oXmlDoc)
        {
            //Select the nodes that are in the project 
            XmlNodeList oProjectNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ProjectHistory/Project");
            if (oProjectNodeList.Count == 0)
            {
                return;
            }
            List<Project> oListProject = new List<Project>();
            int intProjectCnt = 0;

            foreach (XmlNode node in oProjectNodeList)
            {
                Project oProject = new Project();
                Location oLocation = new Location();
                intProjectCnt = intProjectCnt + 1;
                oProject.ProjectId = intProjectCnt;
                //Get the project name from the xml
                oProject.ProjectName = GetInnerText(node, "ProjectName");

                //Get the project description
                oProject.ProjectDescription = GetInnerText(node, "Description");

                //Get the role
                oProject.Role = GetInnerText(node, "Role");

                //Get the position title
                oProject.PositionTitle = GetInnerText(node, "Position");

                //Get the project start date and end date
                DateTime dtProjectStartDate, dtProjectEndDate;
                string startDate = GetInnerText(node, "StartDate");
                string endDate = GetInnerText(node, "EndDate");
                if (startDate.Length > 0)
                {
                    //Parse the string into datetime
                    if ((DateTime.TryParse(startDate, out dtProjectStartDate)))
                    {
                        oProject.StartDate = dtProjectStartDate;
                    }
                }

                if (endDate.Length > 0)
                {
                    if ((DateTime.TryParse(endDate, out dtProjectEndDate)))
                    {
                        oProject.EndDate = dtProjectEndDate;
                    }
                }

                //Get the location
                oLocation.City = GetInnerText(node, "Location");
                oProject.ProjectLocation = oLocation;

                //Get the environment
                oProject.Environment = GetInnerText(node, "Environment");

                //Get the client name
                oProject.ClientName = GetInnerText(node, "ClientName");

                //Get the client industry
                oProject.ClientIndustry = GetInnerText(node, "ClientIndustry");
                oListProject.Add(oProject);
            }

            ProjectsControl oProjCntl = (ProjectsControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_projectsControl");

            oProjCntl.DataSource = oListProject;
            oProjCntl.DataBind();
            SetProjectDate();
        }

        
        /// <summary>
        /// This method helps to set the date format empty in projects.
        /// </summary>
        private void SetProjectDate()
        {
            ProjectsControl oProjCntl = (ProjectsControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_projectsControl");
            ListView oProjectsControl_listView = (ListView)oProjCntl.FindControl("ProjectsControl_listView");
            foreach (ListViewDataItem item in oProjectsControl_listView.Items)
            {
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                if (txtProjStartDate.Text == "01/01/0001" || txtProjStartDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjStartDate.Text = string.Empty;
                }
                if (txtProjEndDate.Text == "01/01/0001" || txtProjEndDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjEndDate.Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// This method helps to set the date format empty in education.
        /// </summary>
        private void SetEducationDate()
        {
            EducationControl oEduCntl = (EducationControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_educationControl");
            ListView EducationControl_listView = (ListView)oEduCntl.FindControl("EducationControl_listView");
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                if (txtEduGradDate.Text == "01/01/0001" || txtEduGradDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtEduGradDate.Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml educational data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetEducation(XmlDocument oXmlDoc)
        {
            XmlNodeList oEducationNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/EducationHistory");
            if (oEducationNodeList.Count == 0)
            {
                return;
            }
            List<Education> oListEducation = new List<Education>();

            //Get the education list of the candidate
            //This will 
            XmlNodeList educationSchoolList = oXmlDoc.SelectNodes("//SchoolOrInstitution");

            int intEduCnt = 0;

            string innerXML = string.Empty;

            XmlNode nodeSchool = null;
            XmlNode nodeDegree = null;
            
            foreach (XmlNode node in educationSchoolList)
            {
                if (node.ChildNodes.Count == 0)
                {
                    continue;
                }

                Education oEducation = new Education();
                Location oLocation = new Location();

                //Get the school name of the candidate 
                nodeSchool = node.SelectSingleNode("School");
                innerXML = GetInnerText(nodeSchool, "SchoolName");

                //Separate the school name using , 
                string[] str = innerXML.Split(',');
                intEduCnt = intEduCnt + 1;
                oEducation.EducationId = intEduCnt;
                //Assign the school name
                oEducation.SchoolName = str[0];

                //If the length >1 assign location
                if (str.Length > 1)
                {
                    oLocation.City = innerXML.Remove(0, str[0].Length);
                    oEducation.SchoolLocation = oLocation;
                }

                nodeDegree = node.SelectSingleNode("Degree");

                //Get the degree name
                oEducation.DegreeName = GetInnerText(nodeDegree, "DegreeName");

                DateTime DtEduGradDate;

                //Get the degree date 
                XmlNode degreeDate = nodeDegree.SelectSingleNode("DegreeDate");

                string date = GetInnerText(degreeDate, "YearMonth");

                if (date.Length != 0)
                {
                    if ((DateTime.TryParse(date, out DtEduGradDate)))
                    {
                        oEducation.GraduationDate = DtEduGradDate;
                    }
                }

                XmlNode degreeNameNode = nodeDegree.SelectSingleNode("DegreeMajor");

                //Get the degree major 
                oEducation.Specialization = GetInnerText(degreeNameNode, "Name");

                //Get the degree comments
                oEducation.GPA = GetInnerText(nodeDegree, "Comments");
                oListEducation.Add(oEducation);
            }

            EducationControl oEduCntl = (EducationControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_educationControl");
            oEduCntl.DataSource = oListEducation;
            oEduCntl.DataBind();
            SetEducationDate();
        }

        /// <summary>
        /// Represents the method to get the inner text of the node
        /// </summary>
        /// <param name="parentNode">
        /// A<see cref="XmlNode"/>that holds the Xml Node
        /// </param>
        /// <param name="xpath">
        /// A<see cref="string"/>that holds the Xpath
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the string
        /// </returns>
        private string GetInnerText(XmlNode parentNode, string xpath)
        {
            XmlNode node1 = parentNode.SelectSingleNode(".");
            XmlNode xmlNode = node1.SelectSingleNode(xpath);

            //xmlNode = parentNode.FirstChild.SelectSingleNode(xpath);

            string innerXML = string.Empty;

            if (xmlNode != null)
            {
                innerXML = xmlNode.InnerText.Trim();
            }
            return innerXML;
        }

        /// <summary>
        /// This method helps to extract the hrxml references data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetReferences(XmlDocument oXmlDoc)
        {
            //Get the list of references
            XmlNodeList oReferenceNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/References/Reference");
            if (oReferenceNodeList.Count == 0)
            {
                return;
            }
            List<Reference> oListReference = new List<Reference>();
            int intRefCnt = 0;
            foreach (XmlNode node in oReferenceNodeList)
            {
                if (node.InnerXml != "")
                {
                    intRefCnt = intRefCnt + 1;
                    Reference oReference = new Reference();
                    ContactInformation oContactInfo = new ContactInformation();
                    PhoneNumber oPhone = new PhoneNumber();
                    oReference.ReferenceId = intRefCnt;

                    //Get the person name from the xml
                    XmlNode referenceNode = node.SelectSingleNode("PersonName");
                    oReference.Name = GetInnerText(referenceNode, "FormattedName");

                    //Get the position title
                    oReference.Relation = GetInnerText(node, "PositionTitle");

                    //Get the comments
                    oReference.Organization = GetInnerText(node, "Comments");

                    //Get the contact node from the xml
                    XmlNode contactNode = node.SelectSingleNode("ContactMethod");
                    XmlNode phoneNode = contactNode.SelectSingleNode("Telephone");

                    //Get the telephone number from the contact node
                    oPhone.Mobile = GetInnerText(phoneNode, "FormattedNumber");

                    oContactInfo.Phone = oPhone;
                    oReference.ContactInformation = oContactInfo;
                    oListReference.Add(oReference);
                }
            }
            ReferencesControl oRefCntl = (ReferencesControl)ResumeEditor_mainResumeControl.
                FindControl("MainResumeControl_referencesControl");
            oRefCntl.DataSource = oListReference;
            oRefCntl.DataBind();
        }

        /// <summary>
        /// This method helps to extract the hrxml resume contents and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPreviewResume(XmlDocument oXmlDoc)
        {
            // Set the ResumePreview
            XmlNode oPreviewResumeNodeList = oXmlDoc.SelectSingleNode("/Resume/NonXMLResume/TextResume");
            if (oPreviewResumeNodeList == null)
            {
                return;
            }
            if (oPreviewResumeNodeList.InnerText.Trim() != "")
            {
                ResumeEditor_perviewHtmlDiv.InnerHtml =
                    oPreviewResumeNodeList.InnerText.Replace("\r\n", "<br>");

                // Keep the resume contents in session.
                Session["RESUME_CONTENTS"] = oPreviewResumeNodeList.InnerText;
            }
        }

        /// <summary>
        /// This method helps to escape any illegal xml characters in the file if its present.
        /// </summary>
        /// <param name="ResumeSource">
        /// A <see cref="string"/> that holds the text resume information.
        /// </param>
        private static string EscapeXml(string ResumeSource)
        {
            string xml = ResumeSource;
            if (!string.IsNullOrEmpty(xml))
            {
                // replace literal values with entities
                xml = xml.Replace("&", "&amp;");
                xml = xml.Replace("&lt;", "&lt;");
                xml = xml.Replace("&gt;", "&gt;");
                xml = xml.Replace("\"", "&quot;");
                xml = xml.Replace("'", "&apos;");
            }
            return xml;
        }

        /// <summary>
        /// This method helps to save/approve resume details .
        /// </summary>
        /// <param name="clickedButtonCommandName">
        /// A <see cref="string"/> that holds the command name of button clicked.
        /// </param>
        private void SaveOrApproveResume(string clickedButtonCommandName)
        {
            // Clear error/success messages.
            ClearAllLabelMessage();
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ResumeEditor_candidateIDHiddenField.Value))
            {
                // Show error messages to the user
                ShowMessage(ResumeEditor_topErrorMessageLabel,
                    Resources.HCMResource.ResumeEditor_SelectCandidate);

                //ResumeEditor_candidateTable.Visible = true;
                return;
            }
            // To check whether load button is clicked or not
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ResumeEditor_isLoadClickHiddenField.Value))
            {
                ShowMessage(ResumeEditor_topErrorMessageLabel,
                    Resources.HCMResource.ResumeEditor_ClickLoadCandidate);
                ResumeEditor_candidateIDTextBox.Text = ResumeEditor_candidateFirstNameHiddenField.Value;
                return;
            }
            
            int candidateResumeID = Convert.ToInt32(ResumeEditor_candidateResumeIDHiddenField.Value);

            if (candidateResumeID != 0)
                ResumeEditor_rolesVectorControl.Save(candidateResumeID);

            // Construct the HR-Xml and update the Xml
            ResumeEditor_candidateResumeIDHiddenField.Value = candidateResumeID.ToString();
            XmlDocument oXmlDoc = WriteXml(candidateResumeID.ToString());
            String str = oXmlDoc.InnerXml.ToString();

            try
            {
                // Insert and update HR-Xml source with file contents
                new ResumeRepositoryBLManager().InsertCandidateHRXml(str, candidateResumeID, base.userID);

                if (clickedButtonCommandName == "cmdSave")
                {
                    ShowMessage(ResumeEditor_topSuccessMessageLabel,
                        Resources.HCMResource.ResumeEditor_ResumeSaveSuccess);
                    ResumeEditor_topErrorMessageLabel.Text = string.Empty;
                }
            }
            catch
            {
                // Construct the HR-Xml and update the Xml
                XmlDocument oXmlHRDoc = WriteHRXml(candidateResumeID.ToString());
                String strXml = oXmlHRDoc.InnerXml.ToString();
                // Insert and update Hr-Xml source without file contents
                new ResumeRepositoryBLManager().InsertCandidateHRXml(strXml, candidateResumeID, base.userID);
                ShowMessage(ResumeEditor_topErrorMessageLabel,
                    Resources.HCMResource.ResumeEditor_ResumeIllegarChars);
            }

        }

        /// <summary>
        /// This method helps to check the HRXML nodes
        /// </summary>
        /// <param name="oXmlDoc">
        /// A<see cref="XmlDocument"/>that holds the xml document
        /// </param>
        private void CheckHRXMLNodes(XmlDocument oXmlDoc)
        {
            //Get the list of lineage item
            List<LineageItem> lineageItem = new ResumeRepositoryBLManager().GetLineageItem();

            string nodeComment = string.Empty;

            foreach (LineageItem item in lineageItem)
            {
                //for each lineage item check the node in oXmldoc
                XmlNode document = oXmlDoc.SelectSingleNode("//" + item.Lineage);

                if (document == null)
                {
                    //if document is null add this 
                    nodeComment += nodeComment.Contains(item.ComponentName.Trim()) ? "" : item.ComponentName + ", ";
                }
            }
            if (nodeComment.Trim().Length > 0)
            {
                if (nodeComment.Trim().Length < 2)
                    return;

                nodeComment = nodeComment.Remove(nodeComment.Length - 2, 2);

                ShowMessage(ResumeEditor_topErrorMessageLabel,
                  "Mismatch in these nodes : " + nodeComment);
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        ///<summary>
        /// Method that validates the data entered by the user.
        ///</summary>
        ///<returns>
        // A <see cref="bool"/> that holds the validity status. True indicates
        // valid and false invalid.
        ///</returns>
        ///<remarks>
        /// This method needs to be overrided by the sub classes.
        ///</remarks>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            int i = 1;
            
            //To check project start and end dates
            ProjectsControl oProjCntl = (ProjectsControl)ResumeEditor_mainResumeControl.FindControl("MainResumeControl_projectsControl");
            ListView oProjList = (ListView)oProjCntl.FindControl("ProjectsControl_listView");
            i = 1;

            //if contains no data with empty grid then show error message
            if (oProjList.Items.Count == 0)
            {
                ShowMessage(ResumeEditor_topErrorMessageLabel, Resources.HCMResource.ResumeEditor_NoInformationFound);
                isValidData = false;
            }
            foreach (ListViewDataItem item in oProjList.Items)
            {
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                MaskedEditValidator oProjectsControl_startDtMaskedEditValidator = (MaskedEditValidator)item.
                    FindControl("ProjectsControl_startDtMaskedEditValidator");
                MaskedEditValidator oProjectsControl_endDtMaskedEditValidator = (MaskedEditValidator)item.
                    FindControl("ProjectsControl_endDtMaskedEditValidator");
                //Validate the masked edit validator to validate the Project StartDate text box
                oProjectsControl_startDtMaskedEditValidator.Validate();
                //Validate the masked edit validator to validate the to Project EndDate text box
                oProjectsControl_endDtMaskedEditValidator.Validate();
                if (txtProjStartDate.Text != string.Empty)
                {
                    if (!oProjectsControl_startDtMaskedEditValidator.IsValid)
                    {
                        isValidData = false;
                        ShowMessage(ResumeEditor_topErrorMessageLabel,
                            //Resources.HCMResource.ResumeEditor_EnterCorrectProjectStartDate);
                            "Project Row " + i + ": Please enter correct project start date");
                    }
                    else
                    {
                        DateTime fromDate = Convert.ToDateTime
                           (txtProjStartDate.Text.Trim());
                        if (DateTime.Parse(txtProjStartDate.Text) > DateTime.Today)
                        {
                            isValidData = false;
                            ShowMessage(ResumeEditor_topErrorMessageLabel,
                                //Resources.HCMResource.ResumeEditor_ProjectStartDateCannotBeGreaterThanCurrentDate);
                                "Project Row " + i + ": Project start date cannot be greater than current date");
                        }
                        if (fromDate < Convert.ToDateTime("1/1/1753"))
                        {
                            isValidData = false;
                            ShowMessage(ResumeEditor_topErrorMessageLabel,
                                //Resources.HCMResource.ResumeEditor_EnterCorrectProjectStartDateValidation);
                                "Project Row " + i + ": Please enter correct project start date");
                        }
                    }
                }
                else
                {
                    //isValidData = false;
                    //ShowMessage(ResumeEditor_topErrorMessageLabel,
                    //    "Project Row " + i + ": Project start date should not be empty");
                }
                if (txtProjEndDate.Text != string.Empty)
                {
                    if (!oProjectsControl_endDtMaskedEditValidator.IsValid)
                    {
                        isValidData = false;
                        ShowMessage(ResumeEditor_topErrorMessageLabel,
                            //Resources.HCMResource.ResumeEditor_EnterCorrectProjectEndDate);
                        "Project Row " + i + ": Please enter correct project end date. End date should be below than 12/31/9999");
                    }
                    else
                    {
                        DateTime toDate = Convert.ToDateTime
                            (txtProjEndDate.Text.Trim());
                        if (DateTime.Parse(txtProjEndDate.Text) > DateTime.Today)
                        {
                            isValidData = false;
                            ShowMessage(ResumeEditor_topErrorMessageLabel,
                                //Resources.HCMResource.ResumeEditor_ProjectEndDateCannotBeGreaterThanCurrentDate);
                                "Project Row " + i + ": Project end date cannot be greater than current date");
                        }
                        if (toDate > Convert.ToDateTime("12/31/9999"))
                        {
                            isValidData = false;
                            ShowMessage(ResumeEditor_topErrorMessageLabel,
                                //Resources.HCMResource.ResumeEditor_EnterCorrectProjectEndDateValidation);
                                "Project Row " + i + ": Please enter correct project end date. End date should be below than 12/31/9999");
                        }
                    }
                }
                else
                {
                    //isValidData = false;
                    //ShowMessage(ResumeEditor_topErrorMessageLabel,
                    //    "Project Row " + i + ": Project end date should not be empty");
                }
                if (txtProjStartDate.Text != string.Empty &&
                   txtProjEndDate.Text != string.Empty)
                {
                    if (oProjectsControl_startDtMaskedEditValidator.IsValid
                        && oProjectsControl_endDtMaskedEditValidator.IsValid)
                    {
                        DateTime fromDate = Convert.ToDateTime
                        (txtProjStartDate.Text.Trim());
                        DateTime toDate = Convert.ToDateTime
                            (txtProjEndDate.Text.Trim());
                        //
                        if (toDate < fromDate)
                        {
                            isValidData = false;
                            ShowMessage(ResumeEditor_topErrorMessageLabel,
                                //Resources.HCMResource.ResumeEditor_ProjectStartDateLessThanEndDate);
                                "Project Row " + i + ": The to-date must be greater than or equal to from-date");
                        }
                    }
                }
                i++;
            }
            return isValidData;
        }

        ///<summary>
        // Method that loads values into user input controls such 
        // as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        ///<remarks>
        // This method needs to be overrided by the sub classes.
        ///</remarks>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected override Methods
    }
}
