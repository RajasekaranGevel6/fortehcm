﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TechnicalSkills.aspx.cs"
    MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" Inherits="Forte.HCM.UI.ResumeRepository.TechnicalSkills" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ContentPlaceHolderID="ResumeRepositoryMaster_body" ID="TechnicalSkills_bodyContent"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <asp:Literal ID="TechnicalSkills_headerLiteral" runat="server" Text="Technical Skills"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="TechnicalSkills_resetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="TechnicalSkills_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="TechnicalSkills_cancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="TechnicalSkills_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="TechnicalSkills_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TechnicalSkills_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="TechnicalSkills_searchDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="TechnicalSkills_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="TechnicalSkills_simpleSearchDiv" runat="server" style="display: block;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="TechnicalSkills_groupTypeLabel" Text="Select Group"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="TechnicalSkills_groupTypeDropDownList" runat="server" Width="130px">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="center">
                                                                                <asp:Label runat="server" ID="TechnicalSkills_groupLabel" Text="Skill Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="TechnicalSkills_skillNameTextBox" MaxLength="50"
                                                                                    AutoCompleteType="Disabled"></asp:TextBox>
                                                                            </td>
                                                                            <td align="center">
                                                                                <asp:Label runat="server" ID="TechnicalSkills_skillsLabel" Text="Skill Alias" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="TechnicalSkills_skillsAliasTextBox" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="TechnicalSkills_searchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                            ToolTip="Search" OnClick="TechnicalSkills_searchButton_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="TechnicalSkills_searchResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="TechnicalSkills_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="TechnicalSkills_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="TechnicalSkills_searchResultsUpSpan" style="display: none;" runat="server">
                                            <asp:Image ID="TechnicalSkills_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="TechnicalSkills_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="TechnicalSkills_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="TechnicalSkills_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel runat="server" ID="TechnicalSkills_searchGridViewUpdatePanel">
                                            <ContentTemplate>
                                                <div style="height: 200px; overflow: auto;" runat="server" id="TechnicalSkills_resultDiv">
                                                    <asp:GridView ID="TechnicalSkills_searchGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowCommand="TechnicalSkills_searchGridView_RowCommand"
                                                        OnRowCreated="TechnicalSkills_searchGridView_RowCreated" OnRowDataBound="TechnicalSkills_searchGridView_RowDataBound"
                                                        OnSorting="TechnicalSkills_searchGridView_Sorting">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="TechnicalSkills_skillIDHiddenField" runat="server" Value='<%# Eval("TechSkillID") %>' />
                                                                    <asp:HiddenField ID="TechnicalSkills_groupNameHiddenField" runat="server" Value='<%# Eval("TechGroup") %>' />
                                                                    <asp:HiddenField ID="TechnicalSkills_skillsNameHiddenField" runat="server" Value='<%# Eval("TechSkillName") %>' />
                                                                    <asp:HiddenField ID="TechnicalSkills_skillsAliasNameHiddenField" runat="server" Value='<%# Eval("TechSkillAliasName") %>' />
                                                                    <asp:ImageButton ID="TechnicalSkills_editSkillImageButton" runat="server" SkinID="sknEditDictionaryEntryImageButton"
                                                                        ToolTip="Edit Skill Name" CommandName="EditSkill" CommandArgument='<%#Eval("TechSkillID") %>' />
                                                                    <asp:ImageButton ID="TechnicalSkills_deleteSkillImageButton" runat="server" SkinID="sknDeleteDictionaryEntryImageButton"
                                                                        ToolTip="Delete Skill" CommandName="DeleteSkill" CommandArgument='<%#Eval("TechSkillID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Group Name" DataField="TechGroup" SortExpression="GROUP_NAME"
                                                                ItemStyle-Width="20%" HeaderStyle-Width="20%" />
                                                            <asp:BoundField HeaderText="Skill Name" DataField="TechSkillName" SortExpression="TECH_NAME"
                                                                ItemStyle-Width="30%" HeaderStyle-Width="30%" />
                                                            <asp:BoundField HeaderText="Alias" DataField="TechSkillAliasName" SortExpression="TECH_NAME_ALIAS"
                                                                ItemStyle-Wrap="true" ItemStyle-Width="40%" HeaderStyle-Width="40%" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="TechnicalSkills_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="TechnicalSkills_bottomPagingNavigatorUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" style="padding-left: 6px; height: 15px; width: 50%">
                                                <asp:LinkButton ID="TechnicalSkills_addSkillLinkButton" SkinID="sknAddLinkButton"
                                                    ToolTip="Add New Skill" runat="server" Text="Add Skill" OnClick="TechnicalSkills_addSkillLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="right" style="width: 50%">
                                                <uc1:PageNavigator ID="TechnicalSkills_bottomPagingNavigator" runat="server" OnPageNumberClick="TechnicalSkills_pageNavigator_PageNumberClick" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="TechnicalSkills_addNewSkillUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="TechnicalSkills_addNewSkillPanel" runat="server" CssClass="popupcontrol_credit_request"
                            Style="display: none" DefaultButton="TechnicalSkills_addNewSkillSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="TechnicalSkills_addNewSkillHiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="TechnicalSkills_addNewSkillLiteral" runat="server" Text="Add New Skill"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="TechnicalSkills_addNewSkillCloseImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                    <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="2">
                                                                <asp:Label ID="CTechnicalSkills_addNewSkillErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 22%">
                                                                <asp:Label ID="TechnicalSkills_addNewSkillGroupNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Group Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="TechnicalSkills_addNewSkillGroupNameDropDownList" runat="server"
                                                                    Width="91%" TabIndex="2">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="TechnicalSkills_addNewSkillSkillNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Skill Name"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="CTechnicalSkills_addNewSkillSkillNameTextBox" runat="server" Width="89%"
                                                                    MaxLength="50" TabIndex="3">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="TechnicalSkills_addNewSkillAliasLabel" runat="server" Text="Alias"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:TextBox ID="TechnicalSkills_addNewSkillAliasTextBox" TextMode="MultiLine" Height="100"
                                                                        runat="server" Width="272px" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                        onchange="CommentsCount(500,this)" TabIndex="4"> </asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="TechnicalSkills_addNewSkillAliasHelpImageButton" SkinID="sknHelpImageButton"
                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="You can enter multiple alias name separated by comma " />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="TechnicalSkills_addNewSkillSaveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="TechnicalSkills_addNewSkillSaveButton_Click" TabIndex="5" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="TechnicalSkills_addNewSkillCancelLinkButton" runat="server" Text="Cancel"
                                                        TabIndex="6" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="TechnicalSkills_addNewSkillModalPopupExtender"
                            runat="server" TargetControlID="TechnicalSkills_addNewSkillHiddenButton" PopupControlID="TechnicalSkills_addNewSkillPanel"
                            BackgroundCssClass="modalBackground" CancelControlID="TechnicalSkills_addNewSkillCancelLinkButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="TechnicalSkills_editSkillUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="TechnicalSkills_editSkillPanel" runat="server" CssClass="popupcontrol_credit_request"
                            Style="display: block" DefaultButton="TechnicalSkills_editSkill_saveButton">
                            <div style="display: none;">
                                <asp:Button ID="TechnicalSkills_editSkillhiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="TechnicalSkills_editSkillLiteral" runat="server" Text="Edit Skill Details"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="TechnicalSkills_editSkilltopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                    <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="TechnicalSkills_editSkill_errorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 28%">
                                                                <asp:Label ID="TechnicalSkills_editSkill_groupNameHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Group Name"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:Label ID="TechnicalSkills_editSkill_groupNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:Label ID="TechnicalSkills_editSkill_skillNameHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Skill Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="TechnicalSkills_editSkill_skillNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="TechnicalSkills_editSkill_aliasHeadLabel" runat="server" Text="Alias"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td colspan="3">
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:TextBox ID="TechnicalSkills_editSkill_aliasTextBox" TextMode="MultiLine" Height="100"
                                                                        runat="server" Width="260px" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                        onchange="CommentsCount(500,this)" TabIndex="2"> </asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="TechnicalSkills_editSkill_aliasImageButton" SkinID="sknHelpImageButton"
                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="You can enter multiple alias name separated by comma " />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="TechnicalSkills_editSkill_saveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="TechnicalSkills_editSkillsaveButton_Click" TabIndex="3" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="TechnicalSkills_editSkill_CancelLinkButton" runat="server" Text="Cancel"
                                                        SkinID="sknPopupLinkButton" TabIndex="4"></asp:LinkButton>
                                                    <asp:HiddenField ID="TechnicalSkills_editSkillIDHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="TechnicalSkills_editSkillModalPopUpExtender"
                            runat="server" TargetControlID="TechnicalSkills_editSkillhiddenButton" PopupControlID="TechnicalSkills_editSkillPanel"
                            BackgroundCssClass="modalBackground" CancelControlID="TechnicalSkills_editSkill_CancelLinkButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="TechnicalSkills_deletePopup_UpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="TechnicalSkills_deletePopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Label ID="TechnicalSkills_deletePopup_titleLabel" runat="server" Text="Delete Skill"></asp:Label>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="TechnicalSkills_deletePopup_closeImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="msg_align">
                                                    <asp:Label ID="TechnicalSkills_deletePopup_errorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="TechnicalSkills_deletePopup_messageConfirmLabel" runat="server" SkinID="sknLabelFieldText"
                                                                    Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr valign="bottom">
                                                            <td align="right" style="text-align: center">
                                                                <asp:Button ID="TechnicalSkills_deletePopup_yesValidButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Yes" OnClick="TechnicalSkills_deletePopup_yesValidButton_Click" />
                                                                <asp:Button ID="TechnicalSkills_deletePopup_noValidButton" runat="server" SkinID="sknButtonId"
                                                                    Text="No" OnClientClick="javascript:return false;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <div style="display: none">
                            <asp:Button ID="TechnicalSkills_deletePopup_hiddenButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="TechnicalSkills_deletePopupExtender" runat="server"
                            PopupControlID="TechnicalSkills_deletePopupPanel" TargetControlID="TechnicalSkills_deletePopup_hiddenButton"
                            BackgroundCssClass="modalBackground" CancelControlID="TechnicalSkills_deletePopup_noValidButton"
                            DynamicServicePath="" Enabled="True">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="TechnicalSkills_bottomUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="TechnicalSkills_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TechnicalSkills_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="TechnicalSkills_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="TechnicalSkills_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="TechnicalSkills_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
