﻿#region Directives                                                   

using System;
using System.Web.UI.WebControls; 
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.EventSupport;

#endregion

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class DuplicateResumeRules : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";
        
        #endregion Private Constants


        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Duplicate Resume Rules");
                CheckAndSetExpandorRestore();
                DuplicateResumeRules_topSuccessMessageLabel.Text =string.Empty;
                DuplicateResumeRules_topErrorMessageLabel.Text = string.Empty;

                DuplicateResumeRules_bottomSuccessMessageLabel.Text = string.Empty;
                DuplicateResumeRules_bottomErrorMessageLabel.Text = string.Empty; 

                DuplicateResumeRules_searchTR.Attributes.Add("onclick", "ExpandOrRestore('" +
             DuplicateResumeRules_rulesDetailDIV.ClientID + "','" +
             DuplicateResumeRules_searchDIV.ClientID + "','" +
             DuplicateResumeRules_searchResultsUpSpan.ClientID + "','" +
             DuplicateResumeRules_searchResultsDownSpan.ClientID + "','" +
             DuplicateResumeRules_restoreHiddenField.ClientID + "','" +
             RESTORED_HEIGHT + "','" +
             EXPANDED_HEIGHT + "')");

                if (!IsPostBack)
                {
                    ClearControls();

                    DuplicateResumeRules_deleteRuleConfirmMsgControl.Message = "Are you sure to delete the rule?";
                    DuplicateResumeRules_deleteRuleConfirmMsgControl.Type = MessageBoxType.YesNo;
                    DuplicateResumeRules_deleteRuleConfirmMsgControl.Title = "Delete Rule";

                    LoadRules(ResumeRulesParameters(1));
                    CheckAndSetExpandorRestore();

                    DuplicateResumeRules_addRuleLinkButton.Attributes.Add("onclick", "return ShowAddRule('MYAVB','" + DuplicateResumeRules_searchButton.ClientID + "','A','0');");
                }

                DuplicateResumeRules_rulesGridView_pageNavigator.PageNumberClick += new
                  PageNavigator.PageNumberClickEventHandler
                  (DuplicateResumeRules_rulesGridView_pageNavigator_PageNumberClick);

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);

                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                DuplicateResumeRules_bottomErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// This function clear all controls when 
        /// on page load and other activities
        /// </summary>
        private void ClearControls()
        {
            DuplicateResumeRules_reasonTextBox.Text = "";
            DuplicateResumeRules_ruleNameTextBox.Text = "";
            DuplicateResumeRules_statusDropDownList.SelectedIndex = 0;
            
            DuplicateResumeRules_topSuccessMessageLabel.Text = "";
            DuplicateResumeRules_topErrorMessageLabel.Text = "";

            DuplicateResumeRules_bottomSuccessMessageLabel.Text = "";
            DuplicateResumeRules_bottomErrorMessageLabel.Text = "";

            DuplicateResumeRules_rulesGridView.DataSource = null;
            DuplicateResumeRules_rulesGridView.DataBind();
        }

        private ResumeRuleDetail ResumeRulesParameters(int pageNumber)
        {
            ResumeRuleDetail resumeRule = new ResumeRuleDetail();

            resumeRule.TenantId = base.tenantID;

            if (!Utility.IsNullOrEmpty(DuplicateResumeRules_ruleNameTextBox.Text))
                resumeRule.RuleName = DuplicateResumeRules_ruleNameTextBox.Text.Trim();

            if (!Utility.IsNullOrEmpty(DuplicateResumeRules_reasonTextBox.Text))
                resumeRule.RuleReason = DuplicateResumeRules_reasonTextBox.Text.Trim();

            if (DuplicateResumeRules_statusDropDownList.SelectedIndex != 0)
            {
                if (DuplicateResumeRules_statusDropDownList.SelectedIndex == 1)
                    resumeRule.RuleStatus = "Y";
                else
                    resumeRule.RuleStatus = "N";
            }

            resumeRule.PageSize = base.GridPageSize;
            resumeRule.PageNumber = pageNumber;

            return resumeRule; 
        }

        private void LoadRules(ResumeRuleDetail resumeRules)
        {
            try
            {
                int totalRecords = 0;
                List<ResumeRuleDetail> resumeRuleDetailList = new ResumeRepositoryBLManager().
                    GetResumeRulesList(resumeRules, out totalRecords);

                DuplicateResumeRules_rulesGridView.DataSource = null;
                DuplicateResumeRules_rulesGridView.DataBind();

                if (resumeRuleDetailList == null)
                {
                    DuplicateResumeRules_bottomSuccessMessageLabel.Text = string.Empty;
                    DuplicateResumeRules_topSuccessMessageLabel.Text = string.Empty;

                    base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
               DuplicateResumeRules_bottomErrorMessageLabel, "No data found to display");
                    return;
                }

                DuplicateResumeRules_rulesGridView.DataSource = resumeRuleDetailList;
                DuplicateResumeRules_rulesGridView.DataBind();

                DuplicateResumeRules_rulesGridView_pageNavigator.Reset();

                DuplicateResumeRules_rulesGridView_pageNavigator.PageSize = resumeRules.PageSize;
                DuplicateResumeRules_rulesGridView_pageNavigator.TotalRecords = totalRecords;
                                
                if (!Utility.IsNullOrEmpty(resumeRules.PageNumber)) 
                   DuplicateResumeRules_rulesGridView_pageNavigator.MoveToPage(resumeRules.PageNumber);  
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);

                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                DuplicateResumeRules_bottomErrorMessageLabel, ex.Message);
            }
        }

        protected void DuplicateResumeRules_searchButton_Click(object sender, EventArgs e)
        {
            try {

                LoadRules(ResumeRulesParameters(1));
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);

                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                DuplicateResumeRules_bottomErrorMessageLabel, ex.Message);
            }
        }

        protected void DuplicateResumeRules_topResetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void DuplicateResumeRules_bottomResetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void DuplicateResumeRules_rulesGridView_pageNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                ViewState["pageNo"] = e.PageNumber.ToString();

                LoadRules(ResumeRulesParameters(e.PageNumber));
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);

                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                DuplicateResumeRules_bottomErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(DuplicateResumeRules_restoreHiddenField.Value) &&
                         DuplicateResumeRules_restoreHiddenField.Value == "Y")
            {
                DuplicateResumeRules_searchDIV.Style["display"] = "none";
                DuplicateResumeRules_searchResultsUpSpan.Style["display"] = "block";
                DuplicateResumeRules_searchResultsDownSpan.Style["display"] = "none";
                DuplicateResumeRules_rulesDetailDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                DuplicateResumeRules_searchDIV.Style["display"] = "block";
                DuplicateResumeRules_searchResultsUpSpan.Style["display"] = "none";
                DuplicateResumeRules_searchResultsDownSpan.Style["display"] = "block";
                DuplicateResumeRules_rulesDetailDIV.Style["height"] = RESTORED_HEIGHT;
            }

            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE]))
                if (!Utility.IsNullOrEmpty(DuplicateResumeRules_restoreHiddenField.Value))
                    ((CandidateInformation)Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE]).IsMaximized =
                        DuplicateResumeRules_restoreHiddenField.Value == "Y" ? true : false;
        }

        protected void DuplicateResumeRules_rulesGridView_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {

            try 
            { 

                 GridViewRow candidateGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);

                if(candidateGridViewRow==null)return;

                 HiddenField DuplicateResumeRules_rulesGridView_ruleIDHiddenField =
                    (HiddenField)candidateGridViewRow.FindControl("DuplicateResumeRules_rulesGridView_ruleIDHiddenField");

                if (DuplicateResumeRules_rulesGridView_ruleIDHiddenField == null) return;

                ViewState["CURRENT_RULE_ID"] = DuplicateResumeRules_rulesGridView_ruleIDHiddenField.Value.ToString();

                if (e.CommandName == "DeleteRule")
                {    
                    DuplicateResumeRules_deleteRuleModalPopupExtender.Show();
                }
                else if (e.CommandName == "EditRule")
                {

                }
                else if (e.CommandName == "ActiveRule")
                {
                    string status ="Y";

                    if (e.CommandArgument.ToString() == "Y")
                        status = "N";

                    new ResumeRepositoryBLManager().UpdateResumeRuleStatus(Convert.ToInt32(ViewState["CURRENT_RULE_ID"]), status);

                    if (!Utility.IsNullOrEmpty(ViewState["pageNo"]))
                        LoadRules(ResumeRulesParameters(Convert.ToInt32(ViewState["pageNo"])));
                    else
                        LoadRules(ResumeRulesParameters(1));


                    if (e.CommandArgument.ToString() == "N")
                        base.ShowMessage(DuplicateResumeRules_topSuccessMessageLabel,
                       DuplicateResumeRules_bottomSuccessMessageLabel, "Rule activated successfully");
                    else
                        base.ShowMessage(DuplicateResumeRules_topSuccessMessageLabel,
                       DuplicateResumeRules_bottomSuccessMessageLabel, "Rule deactivated successfully");   

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                DuplicateResumeRules_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void DuplicateResumeRules_rulesGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton DuplicateResumeRules_rulesGridView_activeRuleImageButton = e.Row.FindControl
                   ("DuplicateResumeRules_rulesGridView_activeRuleImageButton") as ImageButton;

                if (DuplicateResumeRules_rulesGridView_activeRuleImageButton == null) return;

                object objResumeRuleDetail = e.Row.DataItem;

                if (objResumeRuleDetail == null) return;

                ResumeRuleDetail resumeRule = (ResumeRuleDetail)objResumeRuleDetail;
                
                if (resumeRule.RuleStatus == "Yes")
                {

                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_inactive.gif";
                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.CommandName = "ActiveRule";
                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.CommandArgument = "Y";
                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.ToolTip = "Click here to deactivate the rule";
                }
                else 
                {
                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.ToolTip = "Click here to activate the rule";
                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.CommandName = "ActiveRule";
                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.CommandArgument = "N";
                    DuplicateResumeRules_rulesGridView_activeRuleImageButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_active.gif";
                } 

                ImageButton DuplicateResumeRules_rulesGridView_editRuleImageButton = e.Row.FindControl
                   ("DuplicateResumeRules_rulesGridView_editRuleImageButton") as ImageButton;

                DuplicateResumeRules_rulesGridView_editRuleImageButton.Attributes.Add("onclick", "return ShowAddRule('MYAVB','" + DuplicateResumeRules_searchButton.ClientID + "','E','" + resumeRule.RuleId.ToString() + "');"); 
                
            }
            catch(Exception ex)
            {
                Logger.ExceptionLog(ex);

                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                DuplicateResumeRules_bottomErrorMessageLabel, ex.Message);
            }
        }

        protected void DuplicateResumeRules_rulesGridView_deleteRuleConfirmMsgControl_CancelClick
      (object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);

                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                DuplicateResumeRules_bottomErrorMessageLabel, ex.Message);
            }
        }

        protected void DuplicateResumeRules_rulesGridView_deleteRuleConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CURRENT_RULE_ID"] != null)
                {
                    new ResumeRepositoryBLManager().DeleteResumeRule(Convert.ToInt32(ViewState["CURRENT_RULE_ID"]));

                     if (!Utility.IsNullOrEmpty(ViewState["pageNo"]))
                         LoadRules(ResumeRulesParameters(Convert.ToInt32(ViewState["pageNo"])));
                     else
                         LoadRules(ResumeRulesParameters(1));

                    base.ShowMessage(DuplicateResumeRules_topSuccessMessageLabel,
                        DuplicateResumeRules_bottomSuccessMessageLabel,"Rule deleted successfully"); 
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeRules_topErrorMessageLabel,
                    DuplicateResumeRules_bottomErrorMessageLabel, ex.Message);
            }
        }
    }
}