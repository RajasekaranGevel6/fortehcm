﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DuplicateResumeRules.aspx.cs"
    MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" Inherits="Forte.HCM.UI.ResumeRepository.DuplicateResumeRules" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="DeleteRuleConfirmMsgControl"
    TagPrefix="uc2" %> 

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="DuplicateResumeRules_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
      <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td width="100%" class="header_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td class="header_text_bold">
                            <asp:Label ID="DuplicateResumeRules_titleLabel" runat="server" Text="Duplicate Resume Rules"></asp:Label>
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeRules_topResetLinkButton" runat="server"
                             SkinID="sknActionLinkButton" 
                                onclick="DuplicateResumeRules_topResetLinkButton_Click">Reset</asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeRules_topCancelLinkButton" runat="server"
                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect">Cancel</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="DuplicateResumeRules_topMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="DuplicateResumeRules_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="DuplicateResumeRules_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                    <Triggers> 
                        <asp:AsyncPostBackTrigger ControlID="DuplicateResumeRules_searchButton" />
                        <asp:AsyncPostBackTrigger ControlID="DuplicateResumeRules_rulesGridView" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
            
        <tr>
            <td class="tab_body_bg">
                 <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <div id="DuplicateResumeRules_searchDIV" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="panel_bg">
                                         <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="panel_inner_body_bg">
                                                        <table width="100%" cellpadding="2" cellspacing="3">
                                                            <tr>
                                                                <td style="width: 5%">
                                                                    <asp:Label ID="DuplicateResumeRules_ruleNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Rule Name"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%">
                                                                    <asp:TextBox ID="DuplicateResumeRules_ruleNameTextBox" Width="250px" MaxLength="100" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 5%">
                                                                    <asp:Label ID="DuplicateResumeRules_reasonLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Reason"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%">
                                                                    <asp:TextBox ID="DuplicateResumeRules_reasonTextBox" Width="250px" MaxLength="100" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 5%;" >
                                                                    <asp:Label ID="DuplicateResumeRules_statusLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Status"></asp:Label>
                                                                </td> 
                                                                <td style="width: 20%">
                                                                    <asp:DropDownList ID="DuplicateResumeRules_statusDropDownList" runat="server" 
                                                                        Height="16px" Width="184px" SkinID="sknSubjectDropDown"  >
                                                                        <asp:ListItem>--Select--</asp:ListItem>
                                                                        <asp:ListItem>Active</asp:ListItem>
                                                                        <asp:ListItem>Inactive</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="15%" align="left"> 
                                                                    <asp:ImageButton ID="DuplicateResumeRules_selectResumeRulesHelpImageButton" SkinID="sknHelpImageButton"
                                                                        runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the rule status"
                                                                        ImageAlign="Middle" />
                                                                </td>
                                                            </tr> 
                                                        </table>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td class="td_height_5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Button ID="DuplicateResumeRules_searchButton" runat="server" Text="Search" 
                                                            SkinID="sknButtonId" ToolTip="Click here to search the rules" onclick="DuplicateResumeRules_searchButton_Click"/>  
                                                    </td>
                                                </tr>
                                            </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5"></td>
                                </tr>

                            </table>
                          </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                         <asp:UpdatePanel ID="DuplicateResumeRules_rulesGridView_UpdatePanel" runat="server" UpdateMode="Conditional">
                          <ContentTemplate> 
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr  runat="server" id="DuplicateResumeRules_searchTR">
                                     <td class="header_bg">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="DuplicateResumeRules_searchResultsLiteral" runat="server" Text="Rules"></asp:Literal> 
                                                    </td>
                                                    <td style="width: 2%" align="right">
                                                        <span id="DuplicateResumeRules_searchResultsUpSpan" runat="server" style="display: none;">
                                                            <asp:Image ID="DuplicateResumeRules_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                        </span>
                                                            <span id="DuplicateResumeRules_searchResultsDownSpan" runat="server" style="display: block;">
                                                                <asp:Image ID="DuplicateResumeRules_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                          </span>
                                                        <asp:HiddenField ID="DuplicateResumeRules_restoreHiddenField" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                       <tr>
                                            <td class="grid_body_bg">
                                                <div id="DuplicateResumeRules_rulesDetailDIV" runat="server" style="height: 225px;
                                                    overflow: auto;"> 
                                                           <asp:GridView ID="DuplicateResumeRules_rulesGridView" runat="server"
                                                                AllowSorting="false" AutoGenerateColumns="false"  Width="100%" 
                                                                onrowcommand="DuplicateResumeRules_rulesGridView_RowCommand" 
                                                                onrowdatabound="DuplicateResumeRules_rulesGridView_RowDataBound"  >
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="DuplicateResumeRules_rulesGridView_editRuleImageButton"
                                                                                runat="server" CommandName="EditRule"  
                                                                                SkinID="sknEditCandidatesImageButton" ToolTip="Click here to edit the rule" /> 
                                                                    
                                                                            <asp:ImageButton ID="DuplicateResumeRules_rulesGridView_activeRuleImageButton"
                                                                                runat="server" CommandName="ActiveRule" 
                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/icon_inactive.gif" ToolTip="Active/Inactive" Visible="true" />

                                                                            <asp:ImageButton ID="DuplicateResumeRules_rulesGridView_deleteRuleImageButton"
                                                                                runat="server" CommandName="DeleteRule" 
                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/delete_position_profile.gif" ToolTip="Click here to delete the rule" Visible="true" />

                                                                            <asp:HiddenField ID="DuplicateResumeRules_rulesGridView_ruleIDHiddenField" runat="server" Value='<%# Eval("RuleId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Rule Name"   HeaderStyle-Width="18%"
                                                                        ItemStyle-Width="18%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="DuplicateResumeRules_rulesGridView_ruleNameLabel" runat="server" Text='<%# Eval("RuleName") %>'> 
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle  Wrap="true"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Reason" ItemStyle-Width="11%"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="11%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="DuplicateResumeRules_rulesGridView_reasonLabel" runat="server" text='<%# Eval("RuleReason") %>' > 
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle  Wrap="true"/>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="Active" ItemStyle-Width="7%"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="7%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="DuplicateResumeRules_rulesGridView_activeLabel" runat="server" text='<%# Eval("RuleStatus") %>' > 
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle  Wrap="true"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Conditions" ItemStyle-Width="14%"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="DuplicateResumeRules_rulesGridView_conditionsLabel" runat="server" text= '<%# Eval("RuleComments") %>'> 
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle  Wrap="true"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Action"  ItemStyle-Width="15%"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="DuplicateResumeRules_rulesGridView_actiionLabel" runat="server"  text='<%# Eval("RuleAction") %>'   > 
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle  Wrap="true"/>
                                                                    </asp:TemplateField> 
                                                                </Columns>
                                                            </asp:GridView> 
                                                </div>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td width="10%">
                                                        <asp:LinkButton ID="DuplicateResumeRules_addRuleLinkButton" ToolTip="Add New Rule" SkinID="sknAddLinkButton" runat="server">Add Rule</asp:LinkButton>
                                                    </td>
                                                    <td width="90%">
                                                            <uc1:PageNavigator ID="DuplicateResumeRules_rulesGridView_pageNavigator" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                    </tr>
                            </table>
                            </ContentTemplate>
                          <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="DuplicateResumeRules_searchButton" />
                        </Triggers>
                        </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="DuplicateResumeRules_deleteRuleHiddenField" runat="server" />
                <asp:Panel ID="DuplicateResumeRules_deleteRulePopupPanel" runat="server" Style="display: none"
                    CssClass="client_confirm_message_box">
                    <uc2:DeleteRuleConfirmMsgControl ID="DuplicateResumeRules_deleteRuleConfirmMsgControl"
                        runat="server" OnCancelClick="DuplicateResumeRules_rulesGridView_deleteRuleConfirmMsgControl_CancelClick"  
                        OnOkClick ="DuplicateResumeRules_rulesGridView_deleteRuleConfirmMsgControl_OkClick"/>
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="DuplicateResumeRules_deleteRuleModalPopupExtender"
                    runat="server" PopupControlID="DuplicateResumeRules_deleteRulePopupPanel" TargetControlID="DuplicateResumeRules_deleteRuleHiddenField"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        
         <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="DuplicateResumeRules_bottomMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="DuplicateResumeRules_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="DuplicateResumeRules_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                    <Triggers> 
                        <asp:AsyncPostBackTrigger ControlID="DuplicateResumeRules_searchButton" />
                        <asp:AsyncPostBackTrigger ControlID="DuplicateResumeRules_rulesGridView" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td width="100%" class="header_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td class="header_text_bold" width="91%">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeRules_bottomResetLinkButton" runat="server"
                             SkinID="sknActionLinkButton" 
                                onclick="DuplicateResumeRules_bottomResetLinkButton_Click">Reset</asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeRules_bottomCancelLinkButton" runat="server"
                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect">Cancel</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table>
</asp:Content>
