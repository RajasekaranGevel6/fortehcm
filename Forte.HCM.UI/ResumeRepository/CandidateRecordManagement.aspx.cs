﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Forte.HCM.DataObjects;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.BL;
using System.Xml;
using Forte.HCM.Support;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ResumeParser.ResumeValidator;

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class CandidateRecordManagement : PageBase
    {
        #region Event Handlers
        private int candidateID;
        // Get validator instance.
        IResumeValidator validator = Validator.GetInstance();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Incomplete Record Management");

                if(!Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                    candidateID = Convert.ToInt32(Request.QueryString["candidateid"].ToString());

                if (!IsPostBack)
                {
                    CandidateRecordManagement_deleteResumeConfirmMsgControl.Message = "Are you sure to delete the resume?";
                    CandidateRecordManagement_deleteResumeConfirmMsgControl.Type = MessageBoxType.YesNo;
                    CandidateRecordManagement_deleteResumeConfirmMsgControl.Title = "Confirmation";

                    ShowCandidateResumeDetail();
                    // Clear resume contents.
                    Session["RESUME_CONTENTS"] = null;
                    ProcessHRXml();
                }
                ShowResumesMenu();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateRecordManagement_topErrorMessageLabel, CandidateRecordManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to download the parsed resume
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateRecordManagement_downloadImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (candidateID == 0)
                {
                    ShowMessage(CandidateRecordManagement_topErrorMessageLabel, "Candidate resume not present");
                    return;
                }
                Response.Redirect("~/Common/Download.aspx?type=TEMPORARYRESUME&candidateid=" + candidateID, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateRecordManagement_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Method to save the XR-XML file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateRecordManagement_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                XmlDocument oXmlDoc = WriteXml(candidateID.ToString());
                if (oXmlDoc != null)
                {
                    int msgID = 0;
                    // Create output parameter object.
                    List<MissingFieldDetail> missingFields = null;
                    string completeReason = string.Empty;

                    // Retrieve the complete status.
                    bool complete = validator.CheckComplete(userID, candidateID, oXmlDoc,false, out missingFields, out completeReason);
                    CandidateDetail parsedCandidateDetail = null;
                    //Get parsed candidate details
                    parsedCandidateDetail = GetCandidateDetail(oXmlDoc);

                    if (complete == true)
                    {
                        // Update xml.
                        validator.UpdateHRXML(userID, candidateID, oXmlDoc, complete, completeReason);

                        //Update parsed candidate primary details[FirstName,MiddleName,LastName,Mobile...]
                        if (parsedCandidateDetail != null)
                            new ResumeRepositoryBLManager().UpdateCandidateInformation(parsedCandidateDetail, candidateID, userID, "CT_RS_UPLR");
                        
                        if (!validator.CheckDuplicate(candidateID, true))
                        {
                            validator.CreateCandidate(candidateID);
                            msgID = 5; // Profile completed and candidate created.
                        }
                        else
                        {
                            msgID = 4; // Profile completed but it contains duplication.
                        }
                        Response.Redirect("~/ResumeRepository/ReviewResume.aspx?m=0&s=4&parentpage=MENU&messageid=" + msgID, false);
                    }
                    else
                    {
                        // Update xml.
                        validator.UpdateHRXML(userID, candidateID, oXmlDoc, complete, completeReason);

                        //Update parsed candidate primary details[FirstName,MiddleName,LastName,Mobile...]
                        if (parsedCandidateDetail != null)
                            new ResumeRepositoryBLManager().UpdateCandidateInformation(parsedCandidateDetail, candidateID, userID, "CT_RS_UPLR");
                        
                        SetHRXml(oXmlDoc);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateRecordManagement_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Method to show the confirmation popup extender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateRecordManagement_deleteResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                CandidateRecordManagement_deleteResumeModalPopupExtender.Show();
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CandidateRecordManagement_topErrorMessageLabel,
                    CandidateRecordManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        /// <summary>
        /// Method to cancel the delete event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateRecordManagement_deleteResumeConfirmMsgControl_cancelClick(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CandidateRecordManagement_topErrorMessageLabel,
                    CandidateRecordManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        /// <summary>
        /// Method to delete the candidate resume details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateRecordManagement_deleteResumeConfirmMsgControl_okClick(object sender, EventArgs e)
        {
            try
            {
                new ResumeRepositoryBLManager().DeleteTemporaryResume(candidateID);
                Response.Redirect("~/ResumeRepository/ReviewResume.aspx?m=0&s=4&messageid=3&parentpage=MENU", false);
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CandidateRecordManagement_topErrorMessageLabel,
                    CandidateRecordManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        /// <summary>
        /// Method to reset the settings 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateRecordManagement_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                ProcessHRXml();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateRecordManagement_topErrorMessageLabel,
                    CandidateRecordManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion End Event Handlers

        #region Protected Overridden Methods
        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }
        
        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }
        #endregion Protected Overridden Methods

        #region Private Methods

        /// Method to clear the success/error message controls
        /// </summary>
        private void ClearControl()
        {
            CandidateRecordManagement_topSuccessMessageLabel.Text = string.Empty;
            CandidateRecordManagement_bottomSuccessMessageLabel.Text = string.Empty;
            CandidateRecordManagement_topErrorMessageLabel.Text = string.Empty;
            CandidateRecordManagement_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that constructs the candidate detail object from the contact
        /// information section of the parsed HRXML.
        /// </summary>
        /// <param name="xmlDocument">
        /// A <see cref="xmlDocument"/> that holds the HRXML.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </returns>
        public CandidateDetail GetCandidateDetail(XmlDocument xmlDocument)
        {
            XmlNode contactInfoNode = xmlDocument.SelectSingleNode
                ("/Resume/StructuredXMLResume/ContactInfo");

            if (contactInfoNode == null)
                return null;

            CandidateDetail candidateDetail = new CandidateDetail();

            // Get first name.
            candidateDetail.FirstName = GetSubNodeValue
                (contactInfoNode, "FirstName", 50);

            // Get middle name.
            candidateDetail.MiddleName = GetSubNodeValue
                (contactInfoNode, "MiddleName", 50);

            // Get last name.
            candidateDetail.LastName = GetSubNodeValue
                (contactInfoNode, "LastName", 50);

            // Get email ID.
            candidateDetail.EMailID = GetSubNodeValue
                (contactInfoNode, "ContactMethod/InternetEmailAddress", 100);

            // Get street.
            candidateDetail.Address = GetSubNodeValue
                (contactInfoNode, "ContactMethod/PostalAddress/Street", -1);

            // Get postal code.
            string postalCode = GetSubNodeValue
                (contactInfoNode, "ContactMethod/PostalAddress/PostalCode", -1);

            // Merge street & postal code.
            candidateDetail.Address = candidateDetail.Address + ", " + postalCode;
            candidateDetail.Address = candidateDetail.Address.Trim();
            candidateDetail.Address = candidateDetail.Address.Trim(new char[] { ',' });

            if (candidateDetail.Address.Length > 100)
                candidateDetail.Address = candidateDetail.Address.Substring(0, 100);

            // Get cell phone.
            candidateDetail.Mobile = GetSubNodeValue
                (contactInfoNode, "ContactMethod/Telephone", 20);

            // Get home phone.
            candidateDetail.Phone = GetSubNodeValue
                (contactInfoNode, "ContactMethod/HomePhone", 20);

            // Get fax.
            candidateDetail.Fax = GetSubNodeValue
                (contactInfoNode, "ContactMethod/Fax", 20);

            return candidateDetail;
        }

        /// <summary>
        /// Method to show the candidate resume detail
        /// </summary>
        private void ShowCandidateResumeDetail()
        {
            CandidateResumeDetails candidateResumeDetails = null;
            try
            {
                candidateResumeDetails = new ResumeRepositoryBLManager().GetCandidateExternalDataStore(candidateID);
                if (candidateResumeDetails != null)
                {
                    CandidateRecordManagement_candidateNameLabelValue.Text = candidateResumeDetails.FirstName;
                    CandidateRecordManagement_uploadedOnLabelValue.Text = candidateResumeDetails.ProcessedDate.ToString();
                    CandidateRecordManagement_uploadedByLabelValue.Text = candidateResumeDetails.Recruiter;
                    if(candidateResumeDetails.Complete!=null && candidateResumeDetails.Complete.ToString().ToUpper()=="N")
                        CandidateRecordManagement_statusLabelValue.Text = "Incomplete";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateRecordManagement_topErrorMessageLabel, 
                    CandidateRecordManagement_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml data and display its contents.
        /// </summary>
        private void ProcessHRXml()
        {
            List<string> oList = new ResumeRepositoryBLManager().GetTemporaryResumeHRXML(candidateID);
            if (oList == null)
            {
                ShowMessage(CandidateRecordManagement_topErrorMessageLabel,CandidateRecordManagement_bottomErrorMessageLabel,
                    Resources.HCMResource.ResumeEditor_NoInformationFound);
                return;
            }
            
            XmlDocument oXmlDoc = new XmlDocument();
            oXmlDoc.LoadXml(oList[1].ToString());
            //Check the Xml nodes from the database
            if (oXmlDoc != null)
            {
                SetHRXml(oXmlDoc);
            }
        }

        /// <summary>
        /// Method to set the HRXml elements
        /// </summary>
        /// <param name="oXmlDoc"></param>
        private void SetHRXml(XmlDocument oXmlDoc)
        {
            // Set Person Name 
            SetPersonalInfo(oXmlDoc);

            // Set Projects
            SetProjects(oXmlDoc);

            // Set EducationDetails
            SetEducation(oXmlDoc);

            // Set References
            SetReferences(oXmlDoc);

            // Set Preview Resume
            SetPreviewResume(oXmlDoc);

            //Highlight Missing Fields
            HighlightMissingFields(oXmlDoc);

            //Show the incomplete reason

        }

        /// <summary>
        /// Highlight missing field details
        /// </summary>
        /// <param name="oXmlDoc"></param>
        private void HighlightMissingFields(XmlDocument oXmlDoc)
        {
            List<MissingFieldDetail> missingFieldDeail = null;
            string missingReason = string.Empty;

            bool valSatus = validator.CheckComplete(userID, candidateID, oXmlDoc, false, out missingFieldDeail, out missingReason);
            if (missingFieldDeail != null)
            {
                #region Highlight contact and technical information
                XmlNodeList oPersonNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/PersonName");
                if (oPersonNodeList.Count > 0)
                {
                    ResumeContactInformationControl nc = (ResumeContactInformationControl)MainResumeControl_contactInfoControl;
                    TextBox txtFirstName = (TextBox)nc.FindControl("ResumeContactInformationControl_firstNameTextBox");
                    TextBox txtMiddleName = (TextBox)nc.FindControl("ResumeContactInformationControl_middleNameTextBox");
                    TextBox txtLastName = (TextBox)nc.FindControl("ResumeContactInformationControl_lastNameTextBox");

                    if (missingFieldDeail.Any(res => res.ElementID == 8))
                        txtFirstName.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtFirstName.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 9))
                        txtMiddleName.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtMiddleName.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 10))
                        txtLastName.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtLastName.CssClass = "can_resume_text_box_name";
                }
                XmlNodeList oContactInfoNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
                if (oContactInfoNodeList.Count > 0)
                {
                    ResumeContactInformationControl cil = (ResumeContactInformationControl)MainResumeControl_contactInfoControl;
                    TextBox txtAddress = (TextBox)cil.FindControl("ResumeContactInformationControl_streetTextBox");
                    TextBox txtCity = (TextBox)cil.FindControl("ResumeContactInformationControl_cityTextBox");
                    TextBox txtState = (TextBox)cil.FindControl("ResumeContactInformationControl_stateTextBox");
                    TextBox txtCountry = (TextBox)cil.FindControl("ResumeContactInformationControl_countryNameTextBox");
                    TextBox txtPostalCode = (TextBox)cil.FindControl("ResumeContactInformationControl_postalCodeTextBox");
                    TextBox txtFixedLine = (TextBox)cil.FindControl("ResumeContactInformationControl_fixedLineTextBox");
                    TextBox txtMobile = (TextBox)cil.FindControl("ResumeContactInformationControl_mobileTextBox");
                    TextBox txtEmail = (TextBox)cil.FindControl("ResumeContactInformationControl_emailTextBox");
                    TextBox txtWebSite = (TextBox)cil.FindControl("ResumeContactInformationControl_websiteTextBox");
                    // Set the TechnicalSkills Value         
                    ResumeTechnicalSkillsControl oTechCntl = (ResumeTechnicalSkillsControl)MainResumeControl_technicalSkillsControl;
                    TextBox txtLangauages = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_languageTextBox");
                    TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_osTextBox");
                    TextBox txtDatabases = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_databaseTextBox");
                    TextBox txtUITools = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_uiToolsTextBox");
                    TextBox txtOtherSkills = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_otherSkillsTextBox");

                    if (missingFieldDeail.Any(res => res.ElementID == 21))
                        txtAddress.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtAddress.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 19))
                        txtCity.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtCity.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 20))
                        txtState.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtState.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 16))
                        txtCountry.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtCountry.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 17))
                        txtPostalCode.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtPostalCode.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 12))
                        txtFixedLine.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtFixedLine.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 11))
                        txtMobile.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtMobile.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 14))
                        txtEmail.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtEmail.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 15))
                        txtWebSite.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtWebSite.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 22))
                        txtLangauages.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtLangauages.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 23))
                        txtOperaSystem.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtOperaSystem.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 24))
                        txtDatabases.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtDatabases.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 25))
                        txtUITools.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtUITools.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 49))
                        txtOtherSkills.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtOtherSkills.CssClass = "can_resume_text_box_name";

                }
                #endregion

                #region Highlight executive summary information
                XmlNodeList oExecSummaryNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ExecutiveSummary");
                if (oExecSummaryNodeList.Count > 0)
                {
                    ResumeExecutiveSummaryControl oExecCntl = (ResumeExecutiveSummaryControl)MainResumeControl_executiveSummaryControl;
                    TextBox txtExecutiveSummary = (TextBox)oExecCntl.FindControl("ResumeExecutiveSummaryControl_execSummaryTextBox");
                    txtExecutiveSummary.Text = oExecSummaryNodeList.Item(0).InnerText;
                    
                    if (missingFieldDeail.Any(res => res.ElementID == 2))
                        txtExecutiveSummary.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtExecutiveSummary.CssClass = "can_resume_text_box_name";
                }
                #endregion Highlight executive summary information

                #region Highlight project information
                int intProjectMissCnt = 0;
                ResumeProjectsControl oProjCntl = (ResumeProjectsControl)MainResumeControl_projectsControl;
                ListView oProjectsControl_listView = (ListView)oProjCntl.FindControl("ResumeProjectsControl_listView");
                foreach (ListViewDataItem item in oProjectsControl_listView.Items)
                {
                    intProjectMissCnt = intProjectMissCnt + 1;
                    TextBox txtProjName = (TextBox)item.FindControl("ResumeProjectsControl_projectNameTextBox");
                    TextBox txtProjDesc = (TextBox)item.FindControl("ResumeProjectsControl_projectDescTextBox");
                    TextBox txtProjEnv = (TextBox)item.FindControl("ResumeProjectsControl_environmentTextBox");
                    TextBox txtProjRole = (TextBox)item.FindControl("ResumeProjectsControl_roleTextBox");
                    TextBox txtProjPosition = (TextBox)item.FindControl("ResumeProjectsControl_positionTextBox");
                    TextBox txtProjStartDate = (TextBox)item.FindControl("ResumeProjectsControl_startDtTextBox");
                    TextBox txtProjEndDate = (TextBox)item.FindControl("ResumeProjectsControl_endDtTextBox");
                    TextBox txtProjClientName = (TextBox)item.FindControl("ResumeProjectsControl_clientNameTextBox");
                    TextBox txtProjLocation = (TextBox)item.FindControl("ResumeProjectsControl_locationTextBox");
                    TextBox txtProjClientIndustry = (TextBox)item.FindControl("ResumeProjectsControl_clientIndustryTextBox");

                    if (missingFieldDeail.Any(res => res.ElementID == 26 && res.RowID == intProjectMissCnt))
                        txtProjName.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjName.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 27 && res.RowID == intProjectMissCnt))
                        txtProjDesc.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjDesc.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 33 && res.RowID == intProjectMissCnt))
                        txtProjEnv.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjEnv.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 28 && res.RowID == intProjectMissCnt))
                        txtProjRole.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjRole.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 29 && res.RowID == intProjectMissCnt))
                        txtProjPosition.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjPosition.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 30 && res.RowID == intProjectMissCnt))
                        txtProjStartDate.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjStartDate.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 31 && res.RowID == intProjectMissCnt))
                        txtProjEndDate.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjEndDate.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 34 && res.RowID == intProjectMissCnt))
                        txtProjClientName.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjClientName.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 32 && res.RowID == intProjectMissCnt))
                        txtProjLocation.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjLocation.CssClass = "can_resume_text_box_name";
                    if (missingFieldDeail.Any(res => res.ElementID == 35 && res.RowID == intProjectMissCnt))
                        txtProjClientIndustry.CssClass = "can_resume_text_box_name_mandatory";
                    else
                        txtProjClientIndustry.CssClass = "can_resume_text_box_name";
                }
                #endregion Highlight project information

                #region Highlight education information
                int intEducationMissCnt = 0;

                ResumeEducationControl oEduCntl = (ResumeEducationControl)MainResumeControl_educationControl;
                ListView ResumeEducationControl_listView = (ListView)oEduCntl.FindControl("ResumeEducationControl_listView");

                foreach (ListViewDataItem item in ResumeEducationControl_listView.Items)
                {
                    intEducationMissCnt = intEducationMissCnt + 1;
                    TextBox txtEduName = (TextBox)item.FindControl("ResumeEducationControl_nameTextBox");
                    TextBox txtEduLocation = (TextBox)item.FindControl("ResumeEducationControl_locationTextBox");
                    TextBox txtEduDegree = (TextBox)item.FindControl("ResumeEducationControl_degreeTextBox");
                    TextBox txtEduSpecial = (TextBox)item.FindControl("ResumeEducationControl_specializationTextBox");
                    TextBox txtEduGpa = (TextBox)item.FindControl("ResumeEducationControl_gpaTextBox");
                    TextBox txtEduGradDate = (TextBox)item.FindControl("ResumeEducationControl_graduationDtTextBox");

                    if (missingFieldDeail.Any(res => res.ElementID == 39 && res.RowID == intEducationMissCnt))
                        txtEduName.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtEduName.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 38 && res.RowID == intEducationMissCnt))
                        txtEduLocation.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtEduLocation.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 42 && res.RowID == intEducationMissCnt))
                        txtEduSpecial.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtEduSpecial.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 40 && res.RowID == intEducationMissCnt))
                        txtEduDegree.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtEduDegree.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 41 && res.RowID == intEducationMissCnt))
                        txtEduGradDate.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtEduGradDate.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 37 && res.RowID == intEducationMissCnt))
                        txtEduGpa.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtEduGpa.CssClass = "can_resume_panel_text_box";
                    
                }
                #endregion Highlight education information

                #region Highlight reference information
                int intReferenceMissCnt = 0;

                ResumeReferencesControl oRefCntl = (ResumeReferencesControl)MainResumeControl_referencesControl;
                ListView ResumeReferencesControl_listView = (ListView)oRefCntl.FindControl("ResumeReferencesControl_listView");

                foreach (ListViewDataItem item in ResumeReferencesControl_listView.Items)
                {
                    intReferenceMissCnt = intReferenceMissCnt + 1;
                    TextBox txtRefName = (TextBox)item.FindControl("ResumeReferencesControl_nameTextBox");
                    TextBox txtRefOrg = (TextBox)item.FindControl("ResumeReferencesControl_organizationTextBox");
                    TextBox txtRefRel = (TextBox)item.FindControl("ResumeReferencesControl_relationTextBox");
                    TextBox txtRefCont = (TextBox)item.FindControl("ResumeReferencesControl_contInfoTextBox");

                    if (missingFieldDeail.Any(res => res.ElementID == 44 && res.RowID == intReferenceMissCnt))
                        txtRefName.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtRefName.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 48 && res.RowID == intReferenceMissCnt))
                        txtRefOrg.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtRefOrg.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 45 && res.RowID == intReferenceMissCnt))
                        txtRefRel.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtRefRel.CssClass = "can_resume_panel_text_box";
                    if (missingFieldDeail.Any(res => res.ElementID == 46 && res.RowID == intReferenceMissCnt))
                        txtRefCont.CssClass = "can_resume_panel_text_box_mandatory";
                    else
                        txtRefCont.CssClass = "can_resume_panel_text_box";
                    
                }
                #endregion Highlight reference information

                #region Show the incomplete reason
                CandidateRecordManagement_reasonLabelValue.Text = missingReason.ToString().Replace(".", "<br />");
                #endregion Show the incomplete reason
            }
        }

        /// <summary>
        /// This method helps to set the date format empty in projects.
        /// </summary>
        private void SetProjectDate()
        {
            ResumeProjectsControl oProjCntl = (ResumeProjectsControl)MainResumeControl_projectsControl;
            ListView oProjectsControl_listView = (ListView)oProjCntl.FindControl("ResumeProjectsControl_listView");
            foreach (ListViewDataItem item in oProjectsControl_listView.Items)
            {
                TextBox txtProjStartDate = (TextBox)item.FindControl("ResumeProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ResumeProjectsControl_endDtTextBox");
                if (txtProjStartDate.Text == "01/01/0001" || txtProjStartDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjStartDate.Text = string.Empty;
                }
                if (txtProjEndDate.Text == "01/01/0001" || txtProjEndDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjEndDate.Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// This method helps to set the date format empty in education.
        /// </summary>
        private void SetEducationDate()
        {
            ResumeEducationControl oEduCntl = (ResumeEducationControl)MainResumeControl_educationControl;
            ListView EducationControl_listView = (ListView)oEduCntl.FindControl("ResumeEducationControl_listView");
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                TextBox txtEduGradDate = (TextBox)item.FindControl("ResumeEducationControl_graduationDtTextBox");
                if (txtEduGradDate.Text == "01/01/0001" || txtEduGradDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtEduGradDate.Text = string.Empty;
                }
            }
        }
        /// <summary>
        /// This method helps to extract the hrxml personal data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPersonalInfo(XmlDocument oXmlDoc)
        {
            XmlNodeList oPersonNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ContactInfo/PersonName");
            if (oPersonNodeList.Count > 0)
            {
                ResumeContactInformationControl nc = (ResumeContactInformationControl)MainResumeControl_contactInfoControl;
                TextBox txtFirstName = (TextBox)nc.FindControl("ResumeContactInformationControl_firstNameTextBox");
                TextBox txtMiddleName = (TextBox)nc.FindControl("ResumeContactInformationControl_middleNameTextBox");
                TextBox txtLastName = (TextBox)nc.FindControl("ResumeContactInformationControl_lastNameTextBox");

                XmlNode personalDetail = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo");
                txtFirstName.Text = GetInnerText(personalDetail, "FirstName");
                txtMiddleName.Text = GetInnerText(personalDetail, "MiddleName");
                txtLastName.Text = GetInnerText(personalDetail, "LastName");
            }

            XmlNodeList oContactInfoNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
            if (oContactInfoNodeList.Count > 0)
            {
                ResumeContactInformationControl cil = (ResumeContactInformationControl)MainResumeControl_contactInfoControl;
                TextBox txtAddress = (TextBox)cil.FindControl("ResumeContactInformationControl_streetTextBox");
                TextBox txtCity = (TextBox)cil.FindControl("ResumeContactInformationControl_cityTextBox");
                TextBox txtState = (TextBox)cil.FindControl("ResumeContactInformationControl_stateTextBox");
                TextBox txtCountry = (TextBox)cil.FindControl("ResumeContactInformationControl_countryNameTextBox");
                TextBox txtPostalCode = (TextBox)cil.FindControl("ResumeContactInformationControl_postalCodeTextBox");
                TextBox txtFixedLine = (TextBox)cil.FindControl("ResumeContactInformationControl_fixedLineTextBox");
                TextBox txtMobile = (TextBox)cil.FindControl("ResumeContactInformationControl_mobileTextBox");
                TextBox txtEmail = (TextBox)cil.FindControl("ResumeContactInformationControl_emailTextBox");
                TextBox txtWebSite = (TextBox)cil.FindControl("ResumeContactInformationControl_websiteTextBox");
                // Set the TechnicalSkills Value         
                ResumeTechnicalSkillsControl oTechCntl = (ResumeTechnicalSkillsControl)MainResumeControl_technicalSkillsControl;
                TextBox txtLangauages = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_languageTextBox");
                TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_osTextBox");
                TextBox txtDatabases = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_databaseTextBox");
                TextBox txtUITools = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_uiToolsTextBox");
                TextBox txtOtherSkills = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_otherSkillsTextBox");

                XmlNode contactInfo = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
                XmlNode telephoneNode = contactInfo.SelectSingleNode("Telephone");
                txtMobile.Text = GetInnerText(telephoneNode, "FormattedNumber");

                XmlNode fixedLineNode = contactInfo.SelectSingleNode("Fax");
                txtFixedLine.Text = GetInnerText(fixedLineNode, "FormattedNumber");

                txtEmail.Text = GetInnerText(contactInfo, "InternetEmailAddress");
                txtWebSite.Text = GetInnerText(contactInfo, "InternetWebAddress");

                XmlNode postalAddress = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress");
                txtState.Text = GetInnerText(postalAddress, "State");
                txtAddress.Text = GetInnerText(postalAddress, "Street");
                txtCity.Text = GetInnerText(postalAddress, "Municipality");
                txtCountry.Text = GetInnerText(postalAddress, "CountryCode");
                txtPostalCode.Text = GetInnerText(postalAddress, "PostalCode");

                XmlNode techSkill = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/TechnicalSkills");
                txtLangauages.Text = GetInnerText(techSkill, "Languages");
                txtOperaSystem.Text = GetInnerText(techSkill, "OperatingSystem");
                txtDatabases.Text = GetInnerText(techSkill, "Databases");
                txtUITools.Text = GetInnerText(techSkill, "UITools");
                txtOtherSkills.Text = GetInnerText(techSkill, "OtherSkills");
                
            }
            // Set the ExecutiveSummary Value
            XmlNodeList oExecSummaryNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ExecutiveSummary");
            if (oExecSummaryNodeList.Count > 0)
            {
                ResumeExecutiveSummaryControl oExecCntl = (ResumeExecutiveSummaryControl)MainResumeControl_executiveSummaryControl;
                TextBox txtExecutiveSummary = (TextBox)oExecCntl.FindControl("ResumeExecutiveSummaryControl_execSummaryTextBox");
                txtExecutiveSummary.Text = oExecSummaryNodeList.Item(0).InnerText;
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml projects data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetProjects(XmlDocument oXmlDoc)
        {
            //Select the nodes that are in the project 
            XmlNodeList oProjectNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ProjectHistory/Project");
            if (oProjectNodeList.Count == 0)
            {
                return;
            }
            List<Project> oListProject = new List<Project>();
            int intProjectCnt = 0;

            foreach (XmlNode node in oProjectNodeList)
            {
                Project oProject = new Project();
                Location oLocation = new Location();
                intProjectCnt = intProjectCnt + 1;
                oProject.ProjectId = intProjectCnt;
                //Get the project name from the xml
                oProject.ProjectName = GetInnerText(node, "ProjectName");

                //Get the project description
                oProject.ProjectDescription = GetInnerText(node, "Description");

                //Get the role
                oProject.Role = GetInnerText(node, "Role");

                //Get the position title
                oProject.PositionTitle = GetInnerText(node, "Position");

                //Get the project start date and end date
                DateTime dtProjectStartDate, dtProjectEndDate;
                string startDate = GetInnerText(node, "StartDate");
                string endDate = GetInnerText(node, "EndDate");
                if (startDate.Length > 0)
                {
                    //Parse the string into datetime
                    if ((DateTime.TryParse(startDate, out dtProjectStartDate)))
                    {
                        oProject.StartDate = dtProjectStartDate;
                    }
                }

                if (endDate.Length > 0)
                {
                    if ((DateTime.TryParse(endDate, out dtProjectEndDate)))
                    {
                        oProject.EndDate = dtProjectEndDate;
                    }
                }

                //Get the location
                oLocation.City = GetInnerText(node, "Location");
                oProject.ProjectLocation = oLocation;

                //Get the environment
                oProject.Environment = GetInnerText(node, "Environment");

                //Get the client name
                oProject.ClientName = GetInnerText(node, "ClientName");

                //Get the client industry
                oProject.ClientIndustry = GetInnerText(node, "ClientIndustry");
                oListProject.Add(oProject);
            }

            ResumeProjectsControl oProjCntl = (ResumeProjectsControl)MainResumeControl_projectsControl;

            oProjCntl.DataSource = oListProject;
            oProjCntl.DataBind();
            SetProjectDate();
        }

       
        /// <summary>
        /// This method helps to extract the hrxml educational data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetEducation(XmlDocument oXmlDoc)
        {
            XmlNodeList oEducationNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/EducationHistory");

            if (oEducationNodeList.Count == 0)
            {
                return;
            }
            List<Education> oListEducation = new List<Education>();

            //Get the education list of the candidate
            //This will 
            XmlNodeList educationSchoolList = oXmlDoc.SelectNodes("//SchoolOrInstitution");

            int intEduCnt = 0;

            string innerXML = string.Empty;

            XmlNode nodeSchool = null;
            XmlNode nodeDegree = null;

            foreach (XmlNode node in educationSchoolList)
            {
                if (node.ChildNodes.Count == 0)
                {
                    continue;
                }

                Education oEducation = new Education();
                Location oLocation = new Location();

                //Get the school name of the candidate 
                nodeSchool = node.SelectSingleNode("School");
                innerXML = GetInnerText(nodeSchool, "SchoolName");

                //Separate the school name using , 
                string[] str = innerXML.Split(',');
                intEduCnt = intEduCnt + 1;
                oEducation.EducationId = intEduCnt;
                //Assign the school name
                oEducation.SchoolName = str[0];

                //If the length >1 assign location
                if (str.Length > 1)
                {
                    oLocation.City = innerXML.Remove(0, str[0].Length);
                    oEducation.SchoolLocation = oLocation;
                }

                nodeDegree = node.SelectSingleNode("Degree");

                //Get the degree name
                oEducation.DegreeName = GetInnerText(nodeDegree, "DegreeName");

                DateTime DtEduGradDate;

                //Get the degree date 
                XmlNode degreeDate = nodeDegree.SelectSingleNode("DegreeDate");

                string date = GetInnerText(degreeDate, "YearMonth");

                if (date.Length != 0)
                {
                    if ((DateTime.TryParse(date, out DtEduGradDate)))
                    {
                        oEducation.GraduationDate = DtEduGradDate;
                    }
                }

                XmlNode degreeNameNode = nodeDegree.SelectSingleNode("DegreeMajor");

                //Get the degree major 
                oEducation.Specialization = GetInnerText(degreeNameNode, "Name");

                //Get the degree comments
                oEducation.GPA = GetInnerText(nodeDegree, "Comments");
                oListEducation.Add(oEducation);
            }

            ResumeEducationControl oEduCntl = (ResumeEducationControl)MainResumeControl_educationControl;
            oEduCntl.DataSource = oListEducation;
            oEduCntl.DataBind();
            SetEducationDate();
        }

        /// <summary>
        /// This method helps to extract the hrxml references data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetReferences(XmlDocument oXmlDoc)
        {
            //Get the list of references
            XmlNodeList oReferenceNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/References/Reference");
            if (oReferenceNodeList.Count == 0)
            {
                return;
            }
            List<Reference> oListReference = new List<Reference>();
            int intRefCnt = 0;
            foreach (XmlNode node in oReferenceNodeList)
            {
                if (node.InnerXml != "")
                {
                    intRefCnt = intRefCnt + 1;
                    Reference oReference = new Reference();
                    ContactInformation oContactInfo = new ContactInformation();
                    PhoneNumber oPhone = new PhoneNumber();
                    oReference.ReferenceId = intRefCnt;

                    //Get the person name from the xml
                    XmlNode referenceNode = node.SelectSingleNode("PersonName");
                    oReference.Name = GetInnerText(referenceNode, "FormattedName");

                    //Get the position title
                    oReference.Relation = GetInnerText(node, "PositionTitle");

                    //Get the comments
                    oReference.Organization = GetInnerText(node, "Comments");

                    //Get the contact node from the xml
                    XmlNode contactNode = node.SelectSingleNode("ContactMethod");
                    XmlNode phoneNode = contactNode.SelectSingleNode("Telephone");

                    //Get the telephone number from the contact node
                    oPhone.Mobile = GetInnerText(phoneNode, "FormattedNumber");

                    oContactInfo.Phone = oPhone;
                    oReference.ContactInformation = oContactInfo;
                    oListReference.Add(oReference);
                }
            }
            ResumeReferencesControl oRefCntl = (ResumeReferencesControl)MainResumeControl_referencesControl;
            oRefCntl.DataSource = oListReference;
            oRefCntl.DataBind();
        }


        /// <summary>
        /// Represents the method to get the inner text of the node
        /// </summary>
        /// <param name="parentNode">
        /// A<see cref="XmlNode"/>that holds the Xml Node
        /// </param>
        /// <param name="xpath">
        /// A<see cref="string"/>that holds the Xpath
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the string
        /// </returns>
        private string GetInnerText(XmlNode parentNode, string xpath)
        {
            XmlNode node1 = parentNode.SelectSingleNode(".");
            XmlNode xmlNode = node1.SelectSingleNode(xpath);

            //xmlNode = parentNode.FirstChild.SelectSingleNode(xpath);

            string innerXML = string.Empty;

            if (xmlNode != null)
            {
                innerXML = xmlNode.InnerText.Trim();
            }
            return innerXML;
        }
        
        /// <summary>
        /// This method helps to construct the hrxml with file contents.
        /// </summary>
        /// <param name="IdValue">
        /// A <see cref="string"/> that holds the candidateid.
        /// </param>
        private XmlDocument WriteXml(string IdValue)
        {
            XmlDocument xmlDoc = new XmlDocument();
            // Not required if we insert in db otherwise its required
            //XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlText oText;
            // Create the root element
            XmlElement rootNode = xmlDoc.CreateElement("Resume");
            //  xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
            xmlDoc.AppendChild(rootNode);
            // Contstruct the ResumeID Node
            XmlElement resumeIdNode = xmlDoc.CreateElement("ResumeId");
            xmlDoc.DocumentElement.PrependChild(resumeIdNode);
            XmlElement idNode = xmlDoc.CreateElement("IdValue");
            xmlDoc.DocumentElement.PrependChild(idNode);
            oText = xmlDoc.CreateTextNode(IdValue);
            idNode.AppendChild(oText);
            resumeIdNode.AppendChild(idNode);
            rootNode.AppendChild(resumeIdNode);
            // Construct the DistributionGuidelinesnodes
            XmlElement distributionGuideNode = xmlDoc.CreateElement("DistributionGuidelines");
            xmlDoc.DocumentElement.PrependChild(distributionGuideNode);
            rootNode.AppendChild(distributionGuideNode);
            // Construct the StucturedXMLNode
            XmlElement structuredXMLNode = xmlDoc.CreateElement("StructuredXMLResume");
            xmlDoc.DocumentElement.PrependChild(structuredXMLNode);
            rootNode.AppendChild(structuredXMLNode);
            // Construct the NonXMLResume
            XmlElement nonXMLResumeNode = xmlDoc.CreateElement("NonXMLResume");
            xmlDoc.DocumentElement.PrependChild(nonXMLResumeNode);
            rootNode.AppendChild(nonXMLResumeNode);
            // Construct the contactInfoNode
            XmlElement contactInfoNode = xmlDoc.CreateElement("ContactInfo");
            xmlDoc.DocumentElement.PrependChild(contactInfoNode);
            structuredXMLNode.AppendChild(contactInfoNode);
            // Construct the PersonName
            XmlElement personNameNode = xmlDoc.CreateElement("PersonName");
            xmlDoc.DocumentElement.PrependChild(personNameNode);
            contactInfoNode.AppendChild(personNameNode);
            // Construct the FirstName
            XmlElement personFirstNameNode = xmlDoc.CreateElement("FirstName");
            xmlDoc.DocumentElement.PrependChild(personFirstNameNode);
            contactInfoNode.AppendChild(personFirstNameNode);
            // Construct the MiddleName
            XmlElement personMiddleNameNode = xmlDoc.CreateElement("MiddleName");
            xmlDoc.DocumentElement.PrependChild(personMiddleNameNode);
            contactInfoNode.AppendChild(personMiddleNameNode);
            // Construct the LastName
            XmlElement personLastNameNode = xmlDoc.CreateElement("LastName");
            xmlDoc.DocumentElement.PrependChild(personLastNameNode);
            contactInfoNode.AppendChild(personLastNameNode);
            // Construct the FormattedName
            XmlElement personFormattedNameNode = xmlDoc.CreateElement("FormattedName");
            xmlDoc.DocumentElement.PrependChild(personFormattedNameNode);
            personNameNode.AppendChild(personFormattedNameNode);
            // To Get NameControl User Control Data
            ResumeContactInformationControl nc = (ResumeContactInformationControl)MainResumeControl_contactInfoControl;
            TextBox txtFirstName = (TextBox)nc.FindControl("ResumeContactInformationControl_firstNameTextBox");
            TextBox txtMiddleName = (TextBox)nc.FindControl("ResumeContactInformationControl_middleNameTextBox");
            TextBox txtLastName = (TextBox)nc.FindControl("ResumeContactInformationControl_lastNameTextBox");
            oText = xmlDoc.CreateTextNode(txtFirstName.Text.Trim() + " "
                + txtMiddleName.Text.Trim() + " " + txtLastName.Text.Trim());
            personFormattedNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtFirstName.Text.Trim());
            personFirstNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtMiddleName.Text.Trim());
            personMiddleNameNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtLastName.Text.Trim());
            personLastNameNode.AppendChild(oText);
            // Construct the ContactMethod
            XmlElement contactMethodNode = xmlDoc.CreateElement("ContactMethod");
            xmlDoc.DocumentElement.PrependChild(contactMethodNode);
            contactInfoNode.AppendChild(contactMethodNode);
            // Construct the ContactTelephone
            XmlElement contactMethodTelephoneNode = xmlDoc.CreateElement("Telephone");
            xmlDoc.DocumentElement.PrependChild(contactMethodTelephoneNode);
            contactMethodNode.AppendChild(contactMethodTelephoneNode);
            XmlElement contactMethodTelephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodTelephoneFormattedNode);
            contactMethodTelephoneNode.AppendChild(contactMethodTelephoneFormattedNode);
            // Construct the HomePhone
            XmlElement contactMethodHomephoneNode = xmlDoc.CreateElement("HomePhone");
            xmlDoc.DocumentElement.PrependChild(contactMethodHomephoneNode);
            contactMethodNode.AppendChild(contactMethodHomephoneNode);
            XmlElement contactMethodHomephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodHomephoneFormattedNode);
            contactMethodHomephoneNode.AppendChild(contactMethodHomephoneFormattedNode);
            // Construct the ContactFax
            XmlElement contactMethodFaxNode = xmlDoc.CreateElement("Fax");
            xmlDoc.DocumentElement.PrependChild(contactMethodFaxNode);
            contactMethodNode.AppendChild(contactMethodFaxNode);
            XmlElement contactMethodFaxFormattedNode = xmlDoc.CreateElement("FormattedNumber");
            xmlDoc.DocumentElement.PrependChild(contactMethodFaxFormattedNode);
            contactMethodFaxNode.AppendChild(contactMethodFaxFormattedNode);
            // Construct the InternetEmailAddress
            XmlElement contactMethodEmailNode = xmlDoc.CreateElement("InternetEmailAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodEmailNode);
            contactMethodNode.AppendChild(contactMethodEmailNode);
            // Construct the InternetWebAddress
            XmlElement contactMethodInternetNode = xmlDoc.CreateElement("InternetWebAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodInternetNode);
            contactMethodNode.AppendChild(contactMethodInternetNode);
            // Construct the PostalAddress
            XmlElement contactMethodPostalAddressNode = xmlDoc.CreateElement("PostalAddress");
            xmlDoc.DocumentElement.PrependChild(contactMethodPostalAddressNode);
            contactMethodNode.AppendChild(contactMethodPostalAddressNode);
            // To Get ContactInformation User Control Data & Update the RootNode ContactInformation
            ResumeContactInformationControl cil = (ResumeContactInformationControl)MainResumeControl_contactInfoControl;
            TextBox txtAddress = (TextBox)cil.FindControl("ResumeContactInformationControl_streetTextBox");
            TextBox txtCity = (TextBox)cil.FindControl("ResumeContactInformationControl_cityTextBox");
            TextBox txtState = (TextBox)cil.FindControl("ResumeContactInformationControl_stateTextBox");
            TextBox txtCountry = (TextBox)cil.FindControl("ResumeContactInformationControl_countryNameTextBox");
            TextBox txtPostalCode = (TextBox)cil.FindControl("ResumeContactInformationControl_postalCodeTextBox");
            TextBox txtFixedLine = (TextBox)cil.FindControl("ResumeContactInformationControl_fixedLineTextBox");
            TextBox txtMobile = (TextBox)cil.FindControl("ResumeContactInformationControl_mobileTextBox");
            TextBox txtEmail = (TextBox)cil.FindControl("ResumeContactInformationControl_emailTextBox");
            TextBox txtWebSite = (TextBox)cil.FindControl("ResumeContactInformationControl_websiteTextBox");
            XmlElement contactCountryNode = xmlDoc.CreateElement("CountryCode");
            xmlDoc.DocumentElement.PrependChild(contactCountryNode);
            XmlElement contactPostalCodeNode = xmlDoc.CreateElement("PostalCode");
            xmlDoc.DocumentElement.PrependChild(contactPostalCodeNode);
            XmlElement contactRegionNode = xmlDoc.CreateElement("Region");
            xmlDoc.DocumentElement.PrependChild(contactRegionNode);
            XmlElement contactCityNode = xmlDoc.CreateElement("Municipality");
            xmlDoc.DocumentElement.PrependChild(contactCityNode);
            XmlElement contactStateNode = xmlDoc.CreateElement("State");
            xmlDoc.DocumentElement.PrependChild(contactStateNode);
            XmlElement contactAddressNode = xmlDoc.CreateElement("Street");
            xmlDoc.DocumentElement.PrependChild(contactAddressNode);
            oText = xmlDoc.CreateTextNode(txtCountry.Text.Trim());
            contactCountryNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactCountryNode);
            oText = xmlDoc.CreateTextNode(txtPostalCode.Text.Trim());
            contactPostalCodeNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactPostalCodeNode);
            contactMethodPostalAddressNode.AppendChild(contactRegionNode);
            oText = xmlDoc.CreateTextNode(txtCity.Text.Trim());
            contactCityNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactCityNode);
            oText = xmlDoc.CreateTextNode(txtState.Text.Trim());
            contactStateNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactStateNode);
            oText = xmlDoc.CreateTextNode(txtAddress.Text.Trim());
            contactAddressNode.AppendChild(oText);
            contactMethodPostalAddressNode.AppendChild(contactAddressNode);
            oText = xmlDoc.CreateTextNode(txtFixedLine.Text.Trim());
            contactMethodFaxFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtFixedLine.Text.Trim());
            contactMethodHomephoneFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtMobile.Text.Trim());
            contactMethodTelephoneFormattedNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtEmail.Text.Trim());
            contactMethodEmailNode.AppendChild(oText);
            oText = xmlDoc.CreateTextNode(txtWebSite.Text.Trim());
            contactMethodInternetNode.AppendChild(oText);
            contactInfoNode.AppendChild(contactMethodNode);
            // Construct the ExecutiveSummary
            XmlElement execSummaryNode = xmlDoc.CreateElement("ExecutiveSummary");
            xmlDoc.DocumentElement.PrependChild(execSummaryNode);
            structuredXMLNode.AppendChild(execSummaryNode);
            // To get executivesummary data
            ResumeExecutiveSummaryControl oExecCntl = (ResumeExecutiveSummaryControl)MainResumeControl_executiveSummaryControl;
            TextBox txtExecutiveSummary = (TextBox)oExecCntl.
                FindControl("ResumeExecutiveSummaryControl_execSummaryTextBox");
            oText = xmlDoc.CreateTextNode(txtExecutiveSummary.Text.Trim());
            execSummaryNode.AppendChild(oText);
            structuredXMLNode.AppendChild(execSummaryNode);
            // Construct the Objective
            XmlElement objectiveNode = xmlDoc.CreateElement("Objective");
            xmlDoc.DocumentElement.PrependChild(objectiveNode);
            structuredXMLNode.AppendChild(objectiveNode);
            // Construct the EmploymentHistory
            XmlElement empHistoryNode = xmlDoc.CreateElement("EmploymentHistory");
            xmlDoc.DocumentElement.PrependChild(empHistoryNode);
            structuredXMLNode.AppendChild(empHistoryNode);
            // Construct the EducationHistory
            XmlElement educationHistoryNode = xmlDoc.CreateElement("EducationHistory");
            xmlDoc.DocumentElement.PrependChild(educationHistoryNode);
            structuredXMLNode.AppendChild(educationHistoryNode);
            // To get educational data
            ResumeEducationControl oEduCntl = (ResumeEducationControl)MainResumeControl_educationControl;
            ListView oEduList = (ListView)oEduCntl.FindControl("ResumeEducationControl_listView");
            foreach (ListViewDataItem item in oEduList.Items)
            {
                TextBox txtEduName = (TextBox)item.FindControl("ResumeEducationControl_nameTextBox");
                TextBox txtEduLocation = (TextBox)item.FindControl("ResumeEducationControl_locationTextBox");
                TextBox txtEduDegree = (TextBox)item.FindControl("ResumeEducationControl_degreeTextBox");
                TextBox txtEduSpec = (TextBox)item.FindControl("ResumeEducationControl_specializationTextBox");
                TextBox txtEduGPA = (TextBox)item.FindControl("ResumeEducationControl_gpaTextBox");
                TextBox txtEduGradDate = (TextBox)item.FindControl("ResumeEducationControl_graduationDtTextBox");
                XmlElement eduSchoolOrInstitutionNode = xmlDoc.CreateElement("SchoolOrInstitution");
                XmlAttribute eduSchollOrInstitutionAttribute = xmlDoc.CreateAttribute("schoolType");
                eduSchollOrInstitutionAttribute.Value = "trade";
                eduSchoolOrInstitutionNode.Attributes.Append(eduSchollOrInstitutionAttribute);
                xmlDoc.DocumentElement.PrependChild(eduSchoolOrInstitutionNode);
                XmlElement eduSchoolNode = xmlDoc.CreateElement("School");
                xmlDoc.DocumentElement.PrependChild(eduSchoolNode);
                eduSchoolOrInstitutionNode.AppendChild(eduSchoolNode);
                XmlElement eduInternetDomainNameNode = xmlDoc.CreateElement("InternetDomainName");
                xmlDoc.DocumentElement.PrependChild(eduInternetDomainNameNode);
                eduSchoolNode.AppendChild(eduInternetDomainNameNode);
                XmlElement eduSchoolNameNode = xmlDoc.CreateElement("SchoolName");
                xmlDoc.DocumentElement.PrependChild(eduSchoolNameNode);
                eduSchoolNode.AppendChild(eduSchoolNameNode);
                XmlElement eduDegreeNode = xmlDoc.CreateElement("Degree");
                XmlAttribute eduDegreeTypeAttribute = xmlDoc.CreateAttribute("degreeType");
                eduDegreeTypeAttribute.Value = "Master";
                eduDegreeNode.Attributes.Append(eduDegreeTypeAttribute);
                xmlDoc.DocumentElement.PrependChild(eduDegreeNode);
                eduSchoolOrInstitutionNode.AppendChild(eduDegreeNode);
                XmlElement eduDegreeNameNode = xmlDoc.CreateElement("DegreeName");
                xmlDoc.DocumentElement.PrependChild(eduDegreeNameNode);
                eduDegreeNode.AppendChild(eduDegreeNameNode);
                XmlElement eduDegreeDateNode = xmlDoc.CreateElement("DegreeDate");
                xmlDoc.DocumentElement.PrependChild(eduDegreeDateNode);
                eduDegreeNode.AppendChild(eduDegreeDateNode);
                XmlElement eduDegreeYearMonthNode = xmlDoc.CreateElement("YearMonth");
                xmlDoc.DocumentElement.PrependChild(eduDegreeYearMonthNode);
                eduDegreeDateNode.AppendChild(eduDegreeYearMonthNode);
                XmlElement eduDegreeMajorNode = xmlDoc.CreateElement("DegreeMajor");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMajorNode);
                eduDegreeNode.AppendChild(eduDegreeMajorNode);
                XmlElement eduDegreeMajorNameNode = xmlDoc.CreateElement("Name");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMajorNameNode);
                eduDegreeMajorNode.AppendChild(eduDegreeMajorNameNode);
                XmlElement eduDegreeMinorNode = xmlDoc.CreateElement("DegreeMinor");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMinorNode);
                eduDegreeNode.AppendChild(eduDegreeMinorNode);
                XmlElement eduDegreeMinorNameNode = xmlDoc.CreateElement("Name");
                xmlDoc.DocumentElement.PrependChild(eduDegreeMinorNameNode);
                eduDegreeMinorNode.AppendChild(eduDegreeMinorNameNode);
                XmlElement eduDegreeCommentsNode = xmlDoc.CreateElement("Comments");
                xmlDoc.DocumentElement.PrependChild(eduDegreeCommentsNode);
                eduDegreeNode.AppendChild(eduDegreeCommentsNode);
                oText = xmlDoc.CreateTextNode(txtEduName.Text.Trim() + "," + txtEduLocation.Text.Trim());
                eduSchoolNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduDegree.Text.Trim());
                eduDegreeNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduGradDate.Text.Trim());
                eduDegreeYearMonthNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduSpec.Text.Trim());
                eduDegreeMajorNameNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtEduGPA.Text.Trim());
                eduDegreeCommentsNode.AppendChild(oText);
                educationHistoryNode.AppendChild(eduSchoolOrInstitutionNode);
            }
            structuredXMLNode.AppendChild(educationHistoryNode);
            // Construct the LicenceAndCertification
            XmlElement licenceAndCertifyNode = xmlDoc.CreateElement("LicenceAndCertification");
            xmlDoc.DocumentElement.PrependChild(licenceAndCertifyNode);
            structuredXMLNode.AppendChild(licenceAndCertifyNode);
            // Construct the PatentHistory
            XmlElement patentHistoryNode = xmlDoc.CreateElement("PatentHistory");
            xmlDoc.DocumentElement.PrependChild(patentHistoryNode);
            structuredXMLNode.AppendChild(patentHistoryNode);
            // Construct the PublicationHistory
            XmlElement publicationHistoryNode = xmlDoc.CreateElement("PublicationHistory");
            xmlDoc.DocumentElement.PrependChild(publicationHistoryNode);
            structuredXMLNode.AppendChild(publicationHistoryNode);
            // Construct the SpeakingEventsHistory
            XmlElement speakingEventHistory = xmlDoc.CreateElement("SpeakingEventsHistory");
            xmlDoc.DocumentElement.PrependChild(speakingEventHistory);
            structuredXMLNode.AppendChild(speakingEventHistory);
            // Construct the Qualifications
            XmlElement qualificationNode = xmlDoc.CreateElement("Qualifications");
            xmlDoc.DocumentElement.PrependChild(qualificationNode);
            structuredXMLNode.AppendChild(qualificationNode);
            // Construct the Languages
            XmlElement languagesNode = xmlDoc.CreateElement("Languages");
            xmlDoc.DocumentElement.PrependChild(languagesNode);
            structuredXMLNode.AppendChild(languagesNode);
            // Construct the Achievements
            XmlElement achivementsNode = xmlDoc.CreateElement("Achievements");
            xmlDoc.DocumentElement.PrependChild(achivementsNode);
            structuredXMLNode.AppendChild(achivementsNode);
            // Construct the Associations
            XmlElement associationsNode = xmlDoc.CreateElement("Associations");
            xmlDoc.DocumentElement.PrependChild(associationsNode);
            structuredXMLNode.AppendChild(associationsNode);
            // Construct the References
            XmlElement referencesNode = xmlDoc.CreateElement("References");
            xmlDoc.DocumentElement.PrependChild(referencesNode);
            structuredXMLNode.AppendChild(referencesNode);
            // To get references data
            ResumeReferencesControl oRefCntl = (ResumeReferencesControl)MainResumeControl_referencesControl;
            ListView oRefList = (ListView)oRefCntl.FindControl("ResumeReferencesControl_listView");
            foreach (ListViewDataItem item in oRefList.Items)
            {
                TextBox txtRefName = (TextBox)item.FindControl("ResumeReferencesControl_nameTextBox");
                TextBox txtRefOrg = (TextBox)item.FindControl("ResumeReferencesControl_organizationTextBox");
                TextBox txtRefRelation = (TextBox)item.FindControl("ResumeReferencesControl_relationTextBox");
                TextBox txtRefContactInfo = (TextBox)item.FindControl("ResumeReferencesControl_contInfoTextBox");
                XmlElement referenceNode = xmlDoc.CreateElement("Reference");
                XmlAttribute referenceTypeAttribute = xmlDoc.CreateAttribute("type");
                referenceTypeAttribute.Value = "Professional";
                referenceNode.Attributes.Append(referenceTypeAttribute);
                xmlDoc.DocumentElement.PrependChild(referenceNode);
                XmlElement refPersonNameNode = xmlDoc.CreateElement("PersonName");
                xmlDoc.DocumentElement.PrependChild(refPersonNameNode);
                referenceNode.AppendChild(refPersonNameNode);
                XmlElement refPersonFormattedNode = xmlDoc.CreateElement("FormattedName");
                xmlDoc.DocumentElement.PrependChild(refPersonFormattedNode);
                refPersonNameNode.AppendChild(refPersonFormattedNode);
                XmlElement refPositionTitleNode = xmlDoc.CreateElement("PositionTitle");
                xmlDoc.DocumentElement.PrependChild(refPositionTitleNode);
                referenceNode.AppendChild(refPositionTitleNode);
                XmlElement refContactMethodNode = xmlDoc.CreateElement("ContactMethod");
                xmlDoc.DocumentElement.PrependChild(refContactMethodNode);
                referenceNode.AppendChild(refContactMethodNode);
                XmlElement refContactMethodTelephoneNode = xmlDoc.CreateElement("Telephone");
                xmlDoc.DocumentElement.PrependChild(refContactMethodTelephoneNode);
                refContactMethodNode.AppendChild(refContactMethodTelephoneNode);
                XmlElement refContactMethodTelephoneFormattedNode = xmlDoc.CreateElement("FormattedNumber");
                xmlDoc.DocumentElement.PrependChild(refContactMethodTelephoneFormattedNode);
                refContactMethodTelephoneNode.AppendChild(refContactMethodTelephoneFormattedNode);
                XmlElement refContactMethodInternetNode = xmlDoc.CreateElement("InternetEmailAddress");
                xmlDoc.DocumentElement.PrependChild(refContactMethodInternetNode);
                refContactMethodNode.AppendChild(refContactMethodInternetNode);
                XmlElement refCommentsNode = xmlDoc.CreateElement("Comments");
                xmlDoc.DocumentElement.PrependChild(refCommentsNode);
                referenceNode.AppendChild(refCommentsNode);
                oText = xmlDoc.CreateTextNode(txtRefName.Text.Trim());
                refPersonFormattedNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefOrg.Text.Trim());
                refCommentsNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefRelation.Text.Trim());
                refPositionTitleNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefContactInfo.Text.Trim());
                refContactMethodInternetNode.AppendChild(oText);
                oText = xmlDoc.CreateTextNode(txtRefContactInfo.Text.Trim());
                refContactMethodTelephoneFormattedNode.AppendChild(oText);
                referencesNode.AppendChild(referenceNode);
            }
            structuredXMLNode.AppendChild(referencesNode);
            // Construct the SecurityCredentials
            XmlElement securityCredentialsNode = xmlDoc.CreateElement("SecurityCredentials");
            xmlDoc.DocumentElement.PrependChild(securityCredentialsNode);
            structuredXMLNode.AppendChild(securityCredentialsNode);
            // Construct the ResumeAdditionalItems
            XmlElement resumeAdditionItemsNode = xmlDoc.CreateElement("ResumeAdditionalItems");
            xmlDoc.DocumentElement.PrependChild(resumeAdditionItemsNode);
            structuredXMLNode.AppendChild(resumeAdditionItemsNode);
            XmlElement techSkillsNode = xmlDoc.CreateElement("TechnicalSkills");
            xmlDoc.DocumentElement.PrependChild(techSkillsNode);
            // To get technicalskills data
            ResumeTechnicalSkillsControl oTechCntl = (ResumeTechnicalSkillsControl)MainResumeControl_technicalSkillsControl;
            TextBox txtLangauages = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_languageTextBox");
            TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_osTextBox");
            TextBox txtDatabases = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_databaseTextBox");
            TextBox txtUITools = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_uiToolsTextBox");
            TextBox txtOtherSkills = (TextBox)oTechCntl.FindControl("ResumeTechnicalSkillsControl_otherSkillsTextBox");
            XmlElement languageNode = xmlDoc.CreateElement("Languages");
            xmlDoc.DocumentElement.PrependChild(languageNode);
            XmlElement operaSysNode = xmlDoc.CreateElement("OperatingSystem");
            xmlDoc.DocumentElement.PrependChild(operaSysNode);
            XmlElement databaseNode = xmlDoc.CreateElement("Databases");
            xmlDoc.DocumentElement.PrependChild(databaseNode);
            XmlElement uiToolsNode = xmlDoc.CreateElement("UITools");
            xmlDoc.DocumentElement.PrependChild(uiToolsNode);
            XmlElement otherSkillsNode = xmlDoc.CreateElement("OtherSkills");
            xmlDoc.DocumentElement.PrependChild(otherSkillsNode);
            oText = xmlDoc.CreateTextNode(txtLangauages.Text.Trim());
            languageNode.AppendChild(oText);
            techSkillsNode.AppendChild(languageNode);
            oText = xmlDoc.CreateTextNode(txtOperaSystem.Text.Trim());
            operaSysNode.AppendChild(oText);
            techSkillsNode.AppendChild(operaSysNode);
            oText = xmlDoc.CreateTextNode(txtDatabases.Text.Trim());
            databaseNode.AppendChild(oText);
            techSkillsNode.AppendChild(databaseNode);
            oText = xmlDoc.CreateTextNode(txtUITools.Text.Trim());
            uiToolsNode.AppendChild(oText);
            techSkillsNode.AppendChild(uiToolsNode);
            oText = xmlDoc.CreateTextNode(txtOtherSkills.Text.Trim());
            otherSkillsNode.AppendChild(oText);
            techSkillsNode.AppendChild(otherSkillsNode);

            structuredXMLNode.AppendChild(techSkillsNode);
            XmlElement projectNode = xmlDoc.CreateElement("ProjectHistory");
            xmlDoc.DocumentElement.PrependChild(projectNode);
            // To get projects data
            ResumeProjectsControl oProjCntl = (ResumeProjectsControl)MainResumeControl_projectsControl;
            ListView oProjList = (ListView)oProjCntl.FindControl("ResumeProjectsControl_listView");
            foreach (ListViewDataItem item in oProjList.Items)
            {
                TextBox txtProjName = (TextBox)item.FindControl("ResumeProjectsControl_projectNameTextBox");
                TextBox txtProjDesc = (TextBox)item.FindControl("ResumeProjectsControl_projectDescTextBox");
                TextBox txtProjRole = (TextBox)item.FindControl("ResumeProjectsControl_roleTextBox");
                TextBox txtProjPosition = (TextBox)item.FindControl("ResumeProjectsControl_positionTextBox");
                TextBox txtProjStartDate = (TextBox)item.FindControl("ResumeProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ResumeProjectsControl_endDtTextBox");
                TextBox txtProjLocation = (TextBox)item.FindControl("ResumeProjectsControl_locationTextBox");
                TextBox txtProjEnvironment = (TextBox)item.FindControl("ResumeProjectsControl_environmentTextBox");
                TextBox txtProjClientName = (TextBox)item.FindControl("ResumeProjectsControl_clientNameTextBox");
                TextBox txtProjClientIndustry = (TextBox)item.FindControl("ResumeProjectsControl_clientIndustryTextBox");
                XmlElement projectNonode = xmlDoc.CreateElement("Project");
                xmlDoc.DocumentElement.PrependChild(projectNonode);
                XmlElement projectNameNode = xmlDoc.CreateElement("ProjectName");
                xmlDoc.DocumentElement.PrependChild(projectNameNode);
                XmlElement projDescNode = xmlDoc.CreateElement("Description");
                xmlDoc.DocumentElement.PrependChild(projDescNode);
                XmlElement projRoleNode = xmlDoc.CreateElement("Role");
                xmlDoc.DocumentElement.PrependChild(projRoleNode);
                XmlElement projPositionNode = xmlDoc.CreateElement("Position");
                xmlDoc.DocumentElement.PrependChild(projPositionNode);
                XmlElement projStartDateNode = xmlDoc.CreateElement("StartDate");
                xmlDoc.DocumentElement.PrependChild(projStartDateNode);
                XmlElement projEndDateNode = xmlDoc.CreateElement("EndDate");
                xmlDoc.DocumentElement.PrependChild(projEndDateNode);
                XmlElement projLocationNode = xmlDoc.CreateElement("Location");
                xmlDoc.DocumentElement.PrependChild(projLocationNode);
                XmlElement projEnvironmentNode = xmlDoc.CreateElement("Environment");
                xmlDoc.DocumentElement.PrependChild(projEnvironmentNode);
                XmlElement projClientNameNode = xmlDoc.CreateElement("ClientName");
                xmlDoc.DocumentElement.PrependChild(projClientNameNode);
                XmlElement projClientIndustryNode = xmlDoc.CreateElement("ClientIndustry");
                xmlDoc.DocumentElement.PrependChild(projClientIndustryNode);
                oText = xmlDoc.CreateTextNode(txtProjName.Text.Trim());
                projectNameNode.AppendChild(oText);
                projectNonode.AppendChild(projectNameNode);
                oText = xmlDoc.CreateTextNode(txtProjDesc.Text.Trim());
                projDescNode.AppendChild(oText);
                projectNonode.AppendChild(projDescNode);
                oText = xmlDoc.CreateTextNode(txtProjRole.Text.Trim());
                projRoleNode.AppendChild(oText);
                projectNonode.AppendChild(projRoleNode);
                oText = xmlDoc.CreateTextNode(txtProjPosition.Text.Trim());
                projPositionNode.AppendChild(oText);
                projectNonode.AppendChild(projPositionNode);
                oText = xmlDoc.CreateTextNode(txtProjStartDate.Text.Trim());
                projStartDateNode.AppendChild(oText);
                projectNonode.AppendChild(projStartDateNode);
                oText = xmlDoc.CreateTextNode(txtProjEndDate.Text.Trim());
                projEndDateNode.AppendChild(oText);
                projectNonode.AppendChild(projEndDateNode);
                oText = xmlDoc.CreateTextNode(txtProjLocation.Text.Trim());
                projLocationNode.AppendChild(oText);
                projectNonode.AppendChild(projLocationNode);
                oText = xmlDoc.CreateTextNode(txtProjEnvironment.Text.Trim());
                projEnvironmentNode.AppendChild(oText);
                projectNonode.AppendChild(projEnvironmentNode);
                oText = xmlDoc.CreateTextNode(txtProjClientName.Text.Trim());
                projClientNameNode.AppendChild(oText);
                projectNonode.AppendChild(projClientNameNode);
                oText = xmlDoc.CreateTextNode(txtProjClientIndustry.Text.Trim());
                projClientIndustryNode.AppendChild(oText);
                projectNonode.AppendChild(projClientIndustryNode);
                projectNode.AppendChild(projectNonode);
            }
            structuredXMLNode.AppendChild(projectNode);
            // Construct the ResumeAdditionalItems
            XmlElement revisionDateNode = xmlDoc.CreateElement("RevisionDate");
            xmlDoc.DocumentElement.PrependChild(revisionDateNode);
            oText = xmlDoc.CreateTextNode(DateTime.Now.ToString());
            revisionDateNode.AppendChild(oText);
            structuredXMLNode.AppendChild(revisionDateNode);
            try
            {
                // Construct NonXMLResume
                XmlElement textResumeNode = xmlDoc.CreateElement("TextResume");
                xmlDoc.DocumentElement.PrependChild(textResumeNode);
                if (Session["RESUME_CONTENTS"] != null)
                    oText = xmlDoc.CreateTextNode(Session["RESUME_CONTENTS"].ToString());
                else
                    oText = xmlDoc.CreateTextNode(string.Empty);
                textResumeNode.AppendChild(oText);
                nonXMLResumeNode.AppendChild(textResumeNode);
            }
            catch { }
            XmlElement supportingMatterialsNode = xmlDoc.CreateElement("SupportingMaterials");
            xmlDoc.DocumentElement.PrependChild(supportingMatterialsNode);
            nonXMLResumeNode.AppendChild(supportingMatterialsNode);
            XmlElement nonXMLLinkNode = xmlDoc.CreateElement("Link");
            xmlDoc.DocumentElement.PrependChild(nonXMLLinkNode);
            supportingMatterialsNode.AppendChild(nonXMLLinkNode);
            XmlElement nonXMLDescNode = xmlDoc.CreateElement("Description");
            xmlDoc.DocumentElement.PrependChild(nonXMLDescNode);
            supportingMatterialsNode.AppendChild(nonXMLDescNode);

            return xmlDoc;
        }

        /// <summary>
        /// This method helps to escape any illegal xml characters in the file if its present.
        /// </summary>
        /// <param name="ResumeSource">
        /// A <see cref="string"/> that holds the text resume information.
        /// </param>
        private static string EscapeXml(string ResumeSource)
        {
            string xml = ResumeSource;
            if (!string.IsNullOrEmpty(xml))
            {
                // replace literal values with entities
                xml = xml.Replace("&", "&amp;");
                xml = xml.Replace("&lt;", "&lt;");
                xml = xml.Replace("&gt;", "&gt;");
                xml = xml.Replace("\"", "&quot;");
                xml = xml.Replace("'", "&apos;");
            }
            return xml;
        }

        /// <summary>
        /// This method helps to extract the hrxml resume contents and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPreviewResume(XmlDocument oXmlDoc)
        {
            // Set the ResumePreview
            XmlNode oPreviewResumeNodeList = oXmlDoc.SelectSingleNode("/Resume/NonXMLResume/TextResume");
            if (oPreviewResumeNodeList == null)
            {
                return;
            }
            if (oPreviewResumeNodeList.InnerText.Trim() != "")
            {
                CandidateRecordManagement_perviewHtmlDiv.InnerHtml =
                    oPreviewResumeNodeList.InnerText.Replace("\r\n", "<br>");

                // Keep the resume contents in session.
                Session["RESUME_CONTENTS"] = oPreviewResumeNodeList.InnerText;
            }
        }

        /// <summary>
        /// Method that shows the menu type and respective section based on the 
        /// selection.
        /// </summary>
        private void ShowResumesMenu()
        {
            // Get menu type.
            string menuType = CandidateRecordManagement_selectedMenuHiddenField.Value;

            // Hide all div.
            CandidateRecordManagement_contactInfoDiv.Attributes["style"] = "display:none";
            CandidateRecordManagement_executiveInfoDiv.Attributes["style"] = "display:none";
            CandidateRecordManagement_technicalInfoDiv.Attributes["style"] = "display:none";
            CandidateRecordManagement_projectsInfoDiv.Attributes["style"] = "display:none";
            CandidateRecordManagement_educationInfoDiv.Attributes["style"] = "display:none";
            CandidateRecordManagement_referencesInfoDiv.Attributes["style"] = "display:none";
            
            // Reset color.
            CandidateRecordManagement_contactInfoLinkButton.CssClass = "associate_resume_link_button";
            CandidateRecordManagement_executiveInfoLinkButton.CssClass = "associate_resume_link_button";
            CandidateRecordManagement_technicalInfoLinkButton.CssClass = "associate_resume_link_button";
            CandidateRecordManagement_projectsInfoLinkButton.CssClass = "associate_resume_link_button";
            CandidateRecordManagement_educationInfoLinkButton.CssClass = "associate_resume_link_button";
            CandidateRecordManagement_referencesInfoLinkButton.CssClass = "associate_resume_link_button";
            

            if (menuType == "CI")
            {
                CandidateRecordManagement_contactInfoDiv.Attributes["style"] = "display:block";
                CandidateRecordManagement_contactInfoLinkButton.CssClass = "associate_resume_link_button_selected";
            }
            else if (menuType == "SI")
            {
                CandidateRecordManagement_executiveInfoDiv.Attributes["style"] = "display:block";
                CandidateRecordManagement_executiveInfoLinkButton.CssClass = "associate_resume_link_button_selected";
            }
            else if (menuType == "TI")
            {
                CandidateRecordManagement_technicalInfoDiv.Attributes["style"] = "display:block";
                CandidateRecordManagement_technicalInfoLinkButton.CssClass = "associate_resume_link_button_selected";
            }
            else if (menuType == "PI")
            {
                CandidateRecordManagement_projectsInfoDiv.Attributes["style"] = "display:block";
                CandidateRecordManagement_projectsInfoLinkButton.CssClass = "associate_resume_link_button_selected";
            }
            else if (menuType == "EI")
            {
                CandidateRecordManagement_educationInfoDiv.Attributes["style"] = "display:block";
                CandidateRecordManagement_educationInfoLinkButton.CssClass = "associate_resume_link_button_selected";
            }
            else if (menuType == "RI")
            {
                CandidateRecordManagement_referencesInfoDiv.Attributes["style"] = "display:block";
                CandidateRecordManagement_referencesInfoLinkButton.CssClass = "associate_resume_link_button_selected";
            }
        }

        /// <summary>
        /// Method that retrieves the sub node based on the key. It will also 
        /// truncate the value to the given maximum length.
        /// </summary>
        /// <param name="mainNode">
        /// A <see cref="XmlNode"/> that holds the main node.
        /// </param>
        /// <param name="key">
        /// A <see cref="string"/> that holds the key.
        /// </param>
        /// <param name="maxLength">
        /// A <see cref="int"/> that holds the maximum length of the return value.
        /// This is based on the database table column width. If this is -1 it
        /// wont validate.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the value.
        /// </returns>
        private string GetSubNodeValue(XmlNode mainNode, string key, int maxLength)
        {
            // Check if the main node is null.
            if (mainNode == null)
                return string.Empty;

            // Get sub node.
            XmlNode subNode = mainNode.SelectSingleNode(key);

            // Check if sub node is null.
            if (subNode == null)
                return string.Empty;

            string value = subNode.InnerText == null ? string.Empty :
                subNode.InnerText.Trim();

            // Truncate sub node value.
            if (!Utility.IsNullOrEmpty(value) & maxLength != -1)
            {
                if (value.Length > maxLength)
                    value = value.Substring(0, maxLength);
            }

            return value;
        }
        #endregion Private Methods
    }
}