﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NodeManager.cs
// File that represents the user interface layout and functionalities for the
// Node Manager page. This page helps in managing dictionary nodes such as
// position title, degree names, degree discipline, etc.

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the Node Manager page. This page helps in managing dictionary nodes such 
    /// as position title, degree names, degree discipline, etc. This class 
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>    
    public partial class NodeManager : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Method that will be called when the page is loads.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus.
                NodeManager_searchTextBox.Focus();
                Page.Form.DefaultButton = NodeManager_searchButton.UniqueID;
                Master.SetPageCaption("Node Manager");

                ClearAllLabelMessages();

                if (!IsPostBack)
                {
                    LoadValues();
                }

                NodeManager_pageNavigator.PageNumberClick += new PageNavigator.PageNumberClickEventHandler
                    (NodeManager_pageNavigator_PageNumberClick);

                NodeManager_pageNavigator.Visible = false;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NodeManager_topErrorMessageLabel,
                    NodeManager_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called add button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will add the entry into database.
        /// </remarks>
        protected void NodeManager_addEntryButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                ClearAllLabelMessages();

                // Check if the node type is selected.
                if (Utility.IsNullOrEmpty(NodeManager_addEntryTypeDropDownList.SelectedValue))
                {
                    ShowMessage(NodeManager_addEntryErrorLabel,
                        "No node was selected");
                    NodeManager_addEntryTypeDropDownList.Focus();
                    NodeManager_addEntryModalPopupExtender.Show();
                    return;
                }

                // Check if the node name is entered.
                if (NodeManager_addEntryTextBox.Text.Trim().Length == 0 ||
                    NodeManager_addEntryAliasTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(NodeManager_addEntryErrorLabel,
                        "Please enter mandatory field");
                    NodeManager_addEntryTextBox.Focus();
                    NodeManager_addEntryModalPopupExtender.Show();
                    return;
                }

                // Keep the names list in view state.
                ViewState["NODE_LIST"] = GetFullNodeList();

                //Check if the position name already exist.
                if (ViewState["NODE_LIST"] != null)
                {
                    if ((ViewState["NODE_LIST"] as List<NodeDetail>).Find(item => item.Name.ToUpper().
                        Trim() == NodeManager_addEntryTextBox.Text.Trim().ToUpper()) != null)
                    {
                        ShowMessage(NodeManager_addEntryErrorLabel,
                            "Name already exist");

                        NodeManager_addEntryModalPopupExtender.Show();

                        NodeManager_pageNavigator.Visible = true;
                        return;
                    }
                }

                // Node detail.
                NodeDetail nodeDetail = new NodeDetail();
                nodeDetail.Name = NodeManager_addEntryTextBox.Text.Trim();
                nodeDetail.AliasName = NodeManager_addEntryAliasTextBox.Text.Trim();
                nodeDetail.CreatedBy = base.userID;

                new ResumeRepositoryBLManager().InserNode(nodeDetail, NodeManager_addEntryTypeDropDownList.SelectedValue);

                // Clear text boxes.
                NodeManager_addEntryTextBox.Text = string.Empty;

                // Refresh the grid data.
                ShowName(1);

                // Show success message.
                ShowMessage(NodeManager_topSuccessMessageLabel, NodeManager_bottomSuccessMessageLabel,
                    "Entry added successfully");

                // Set the focus to add position text box.
                NodeManager_addEntryTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(NodeManager_topErrorMessageLabel,
                    NodeManager_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                NodeManager_topMessageUpdatePanel.Update();
                NodeManager_bottomMessageUpdatePanel.Update();
                NodeManager_searchResultsUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when edit/delete is clicked in grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void NodeManager_nodeDetailsGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                // Clear message.
                ClearAllLabelMessages();

                // Update the message update panel.
                //NodeManager_messageUpdatePanel.Update();

                // Keep the selected values in view state.
                ViewState["SELECTED_ID"] = (((ImageButton)e.CommandSource).Parent.
                    FindControl("NodeManager_nodeDetailsGridView_idHiddenField") as HiddenField).Value;
                ViewState["SELECTED_NODE"] = (((ImageButton)e.CommandSource).Parent.
                     FindControl("NodeManager_nodeDetailsGridView_nameLabel") as Label).Text;

                NodeManager_nodeDetails_aliasNameHiddenField.Value = (((ImageButton)e.CommandSource).Parent.FindControl
                    ("NodeManager_nodeDetailsGridView_aliasNameLabel") as Label).Text;

                if (e.CommandName == "EditEntry")
                {
                    // Clear any previous messages.
                    NodeManager_editEntryPanel_errorMessageLabel.Text = string.Empty;


                    // Assign values and show the edit entry panel.
                    NodeManager_editEntryPanel_nodeTextBox.Text =
                        (ViewState["SELECTED_NODE"] == null ? string.Empty : ViewState["SELECTED_NODE"].ToString());

                    NodeManager_editEntryPanel_aliasNameTextBox.Text =
                        NodeManager_nodeDetails_aliasNameHiddenField.Value == null ? string.Empty :
                        NodeManager_nodeDetails_aliasNameHiddenField.Value.Trim();

                    NodeManager_editEntryPanel_updateButton.Focus();

                    NodeManager_editEntryPanelModalPopupExtender.Show();
                    NodeManager_editEntryUpdatePanel.Update();
                }
                else if (e.CommandName == "DeleteEntry")
                {
                    // Set message, title and type for the confirmation control for edit entry.
                    NodeManager_deleteEntryPanel_confirmMessageControl.Message =
                        string.Format("Are you sure to delete the node entry '{0}'?", ViewState["SELECTED_NODE"]);
                    NodeManager_deleteEntryPanel_confirmMessageControl.Title =
                        "Delete Name Entry";
                    NodeManager_deleteEntryPanel_confirmMessageControl.Type =
                        MessageBoxType.YesNo;

                    NodeManager_deleteEntryPanel_confirmModalPopupExtender.Show();
                    NodeManager_deleteEntryUpdatePanel.Update();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(NodeManager_topErrorMessageLabel, NodeManager_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                NodeManager_topMessageUpdatePanel.Update();
                NodeManager_bottomMessageUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Represents the event handler that is called on the grid view 
        /// row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void NodeManager_nodeDetailsGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                base.ShowMessage(NodeManager_topErrorMessageLabel, NodeManager_bottomErrorMessageLabel,
                    exception.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when the modal pop up update button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        ///  <remarks>
        /// This will update the entry into database.
        /// </remarks>
        protected void NodeManager_editEntryPanel_updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear any previous messages in the edit entry panel.
                NodeManager_editEntryPanel_errorMessageLabel.Text = string.Empty;

                // Check if the node name is entered.
                if (NodeManager_editEntryPanel_nodeTextBox.Text.Trim().Length == 0 ||
                    NodeManager_editEntryPanel_aliasNameTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(NodeManager_editEntryPanel_errorMessageLabel,
                        "Please enter mandatory fields");

                    // Reshow the edit entry panel.
                    NodeManager_editEntryPanelModalPopupExtender.Show();
                    NodeManager_editEntryUpdatePanel.Update();
                    return;
                }

                // Check if the position name already exist.
                if (ViewState["NODE_LIST"] != null)
                {
                    if ((ViewState["NODE_LIST"] as List<NodeDetail>).Find(item => item.Name.ToUpper().
                        Trim() == NodeManager_editEntryPanel_nodeTextBox.Text.Trim().ToUpper() &&
                        item.ID != Convert.ToInt32(ViewState["SELECTED_ID"]) &&
                        item.AliasName.ToUpper() == NodeManager_editEntryPanel_aliasNameTextBox.Text.Trim().ToUpper()) != null)
                    {
                        ShowMessage(NodeManager_editEntryPanel_errorMessageLabel,
                            "Name and alias name already exist");

                        // Reshow the edit entry panel.
                        NodeManager_editEntryPanelModalPopupExtender.Show();
                        NodeManager_editEntryUpdatePanel.Update();
                        return;
                    }
                }

                // Construct node detail.
                NodeDetail nodeDetail = new NodeDetail();
                nodeDetail.ID = Convert.ToInt32(ViewState["SELECTED_ID"]);
                nodeDetail.Name = NodeManager_editEntryPanel_nodeTextBox.Text.Trim();
                nodeDetail.ModifiedBy = base.userID;
                nodeDetail.AliasName = NodeManager_editEntryPanel_aliasNameTextBox.Text.Trim();

                new ResumeRepositoryBLManager().UpdateNode(nodeDetail, NodeManager_nodeTypeDropDownList.SelectedValue);

                ShowName(1);

                // Show success message.
                ShowMessage(NodeManager_topSuccessMessageLabel,
                    NodeManager_bottomSuccessMessageLabel, "Name entry updated successfully");

                // Update the message & grid view update panel.
                NodeManager_topMessageUpdatePanel.Update();
                NodeManager_bottomMessageUpdatePanel.Update();
                NodeManager_searchResultsUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NodeManager_topErrorMessageLabel,
                    NodeManager_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the selected index is changed in drop down list
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void NodeManager_nodeTypeDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                ClearAllLabelMessages();

                // Clear node grid.
                NodeManager_nodeDetailsGridView.DataSource = null;
                NodeManager_nodeDetailsGridView.DataBind();

                // Clear node list.
                ViewState["NODE_LIST"] = null;

                if (Utility.IsNullOrEmpty(NodeManager_nodeTypeDropDownList.SelectedValue))
                    return;

                //NodeManager_nodeDetailsGridView.DataSource = new
                //  NodeBLManager().GetNodes(Enum.GetName(typeof(NodeType), NodeManager_nodeTypeDropDownList.SelectedValue));

                //NodeManager_nodeDetailsGridView.DataSource = new
                //  NodeBLManager().GetNodes(NodeManager_nodeTypeDropDownList.SelectedValue);
                //NodeManager_nodeDetailsGridView.DataBind();

                NodeManager_pageNavigator.Reset();

                ShowName(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(NodeManager_topErrorMessageLabel, NodeManager_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                NodeManager_topMessageUpdatePanel.Update();
                NodeManager_bottomMessageUpdatePanel.Update();
                NodeManager_searchResultsUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete entry confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the entry from the database.
        /// </remarks>
        protected void NodeManager_deleteEntryPanel_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                ClearAllLabelMessages();

                new ResumeRepositoryBLManager().DeleteNode
                        (Convert.ToInt32(ViewState["SELECTED_ID"]), NodeManager_nodeTypeDropDownList.SelectedValue);

                // Show success message.
                ShowMessage(NodeManager_topSuccessMessageLabel, NodeManager_bottomSuccessMessageLabel,
                    "Node entry deleted successfully");

                // Reload names.
                ShowName(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(NodeManager_topErrorMessageLabel,
                    NodeManager_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                // Update the message & grid view update panel.
                NodeManager_topMessageUpdatePanel.Update();
                NodeManager_bottomMessageUpdatePanel.Update();
                NodeManager_searchResultsUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void NodeManager_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Represents the method to add the new node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NodeManager_addNodeLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                NodeManager_addEntryTypeDropDownList.SelectedValue =
                    NodeManager_nodeTypeDropDownList.SelectedValue;

                NodeManager_addEntryErrorLabel.Text = string.Empty;

                NodeManager_addEntryAliasTextBox.Text = string.Empty;

                NodeManager_addEntryTextBox.Text = string.Empty;
               
                //NodeManager_addEntryTextBox.Text = Form.DefaultFocus;

                //Form.DefaultFocus = NodeManager_addEntryButton.ClientID;

                NodeManager_addEntryTypeDropDownList.Focus();

                NodeManager_addEntryModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NodeManager_topErrorMessageLabel,
                    NodeManager_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Represents the method that is called when the search button is clicked 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void NodeManager_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(NodeManager_nodeTypeDropDownList.SelectedValue))
                {
                    base.ShowMessage(NodeManager_bottomErrorMessageLabel, NodeManager_topErrorMessageLabel,
                        "Please select a node type");
                    return;
                }

                //Reset the page navigator
                NodeManager_pageNavigator.Reset();

                ShowName(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NodeManager_topErrorMessageLabel,
                    NodeManager_bottomErrorMessageLabel, exception.Message);
            }

        }

        /// <summary>
        /// Represents the method that is called on the page navigator click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="PageNumberEventArgs"/>that holds the event args
        /// </param>
        void NodeManager_pageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowName(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NodeManager_topErrorMessageLabel,
                    NodeManager_bottomErrorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that loads names for specific nodes.
        /// </summary>       
        private void ShowName(int pageNumber)
        {
            // Clear node grid.
            NodeManager_nodeDetailsGridView.DataSource = null;
            NodeManager_nodeDetailsGridView.DataBind();

            // Clear node list.
            ViewState["NODE_LIST"] = null;

            if (Utility.IsNullOrEmpty(NodeManager_nodeTypeDropDownList.SelectedValue))
                return;

            List<NodeDetail> nodes = null;

            int totalRecords = 0;

            nodes = new ResumeRepositoryBLManager().GetNodes
                (NodeManager_nodeTypeDropDownList.SelectedValue, pageNumber, base.GridPageSize,
                out totalRecords, NodeManager_searchTextBox.Text.Trim());

            NodeManager_nodeDetailsGridView.DataSource = nodes;
            NodeManager_nodeDetailsGridView.DataBind();

            NodeManager_pageNavigator.Visible = true;

            NodeManager_pageNavigator.TotalRecords = totalRecords;
            NodeManager_pageNavigator.PageSize = base.GridPageSize;



            //// Keep the names list in view state.
            //ViewState["NODE_LIST"] = GetFullNodeList();

            if (nodes == null || nodes.Count == 0)
            {
                if (NodeManager_topSuccessMessageLabel.Text.Trim().Length == 0)
                {
                    ShowMessage(NodeManager_bottomErrorMessageLabel, NodeManager_topErrorMessageLabel,
                        Resources.HCMResource.Common_Empty_Grid);
                }
                else
                {
                    ShowMessage(NodeManager_bottomErrorMessageLabel, NodeManager_topErrorMessageLabel, "<br/>" +
                        Resources.HCMResource.Common_Empty_Grid);
                }
            }
        }

        /// <summary>
        /// Get the full list of the nodes to store in the view state
        /// </summary>
        private List<NodeDetail> GetFullNodeList()
        {
            return new ResumeRepositoryBLManager().GetNodesFullList(NodeManager_addEntryTypeDropDownList.SelectedValue);
        }



        /// <summary>
        /// Method that clears all label messages.
        /// </summary>
        private void ClearAllLabelMessages()
        {
            NodeManager_topErrorMessageLabel.Text = string.Empty;
            NodeManager_bottomErrorMessageLabel.Text = string.Empty;
            NodeManager_topSuccessMessageLabel.Text = string.Empty;
            NodeManager_bottomSuccessMessageLabel.Text = string.Empty;
            NodeManager_addEntryErrorLabel.Text = string.Empty;
        }


        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            //NodeManager_nodeTypeDropDownList.Items.Insert(0, new ListItem("--Select--", ""));
            //string[] PositionTitles = Enum.GetNames(typeof(NodeType));
            //for (int i = 0; i < PositionTitles.Length; i++)
            //    dicNodeManager.Add(PositionTitles[i], i.ToString());

            Dictionary<string, string> dicNodeManager = new
              Dictionary<string, string>();

            dicNodeManager.Add("--Select--", "");
            dicNodeManager.Add("Position Title", "1");
            dicNodeManager.Add("Degree Names", "2");
            dicNodeManager.Add("Negative Title", "3");
            dicNodeManager.Add("Degree Discipline", "4");
            NodeManager_nodeTypeDropDownList.DataSource = dicNodeManager;
            NodeManager_nodeTypeDropDownList.DataTextField = "Key";
            NodeManager_nodeTypeDropDownList.DataValueField = "Value";
            NodeManager_nodeTypeDropDownList.DataBind();

            NodeManager_addEntryTypeDropDownList.DataSource = dicNodeManager;
            NodeManager_addEntryTypeDropDownList.DataTextField = "Key";
            NodeManager_addEntryTypeDropDownList.DataValueField = "Value";
            NodeManager_addEntryTypeDropDownList.DataBind();

            //// Keep the names list in view state.
            //ViewState["NODE_LIST"] = GetFullNodeList();


            //NodeManager_nodeTypeDropDownList.SelectedIndex = 1;
            ShowName(1);
        }



        #endregion Protected Overridden Methods
    }
}
