﻿#region Directives
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;

using Forte.HCM.UI.Common;
using Forte.HCM.BL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using Forte.HCM.Trace;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class ResumeMandatorySettings : PageBase
    {
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Resume Mandatory Settings");
                if (!IsPostBack)
                {
                    LoadResumeElements();
                    BindAttributes();
                    AssignMandatorySettings();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Method to save the mandatory settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_saveMandatorySettings_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                if (!IsValidData())
                    return;
                List<ResumeElementsDetail> resumeElementDetails = new List<ResumeElementsDetail>();
                //Contact information
                foreach (DataListItem dataListItem in ResumeMandatorySettings_contactInfoDataList.Items)
                {
                    CheckBox ResumeMandatorySettings_contactInfoElementCheckBox = 
                        (CheckBox)(dataListItem.FindControl("ResumeMandatorySettings_contactInfoElementCheckBox"));
                    if (ResumeMandatorySettings_contactInfoElementCheckBox.Checked)
                    {
                        ResumeElementsDetail resumeElementDetail = new ResumeElementsDetail();

                        HiddenField ResumeMandatorySettings_contactInfoElementIdHiddenField = 
                            (HiddenField)(dataListItem.FindControl("ResumeMandatorySettings_contactInfoElementIdHiddenField"));
                        DropDownList ResumeMandatorySettings_contactInfoValidateExprTypeDropDown = 
                            (DropDownList)(dataListItem.FindControl("ResumeMandatorySettings_contactInfoValidateExprTypeDropDown"));
                        TextBox ResumeMandatorySettings_contactInfoValidateExprValueTextBox = 
                            (TextBox)(dataListItem.FindControl("ResumeMandatorySettings_contactInfoValidateExprValueTextBox"));
                        TextBox ResumeMandatorySettings_contactInfoValidateExprReasonTextBox = 
                            (TextBox)(dataListItem.FindControl("ResumeMandatorySettings_contactInfoValidateExprReasonTextBox"));

                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_contactInfoElementIdHiddenField.Value))
                            resumeElementDetail.ElementId = Convert.ToInt32(ResumeMandatorySettings_contactInfoElementIdHiddenField.Value);
                        resumeElementDetail.ValidateExprType = ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedValue.ToString();
                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Text))
                            resumeElementDetail.ValidateExprValue = Convert.ToInt32(ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Text);
                        resumeElementDetail.ValidateReason = ResumeMandatorySettings_contactInfoValidateExprReasonTextBox.Text.ToString().Trim();
                        resumeElementDetails.Add(resumeElementDetail);
                    }
                }

                //Project Information
                foreach (DataListItem dlProjectList in ResumeMandatorySettings_projectsDataList.Items)
                {
                    CheckBox ResumeMandatorySettings_projectsElementCheckBox = 
                        (CheckBox)(dlProjectList.FindControl("ResumeMandatorySettings_projectsElementCheckBox"));
                    if (ResumeMandatorySettings_projectsElementCheckBox.Checked)
                    {
                        ResumeElementsDetail resumeProjectElementDetail = new ResumeElementsDetail();

                        HiddenField ResumeMandatorySettings_projectsElementIdHiddenField = 
                            (HiddenField)(dlProjectList.FindControl("ResumeMandatorySettings_projectsElementIdHiddenField"));
                        DropDownList ResumeMandatorySettings_projectsValidateExprTypeDropDown =
                            (DropDownList)(dlProjectList.FindControl("ResumeMandatorySettings_projectsValidateExprTypeDropDown"));
                        TextBox ResumeMandatorySettings_projectsValidateExprValueTextBox =
                            (TextBox)(dlProjectList.FindControl("ResumeMandatorySettings_projectsValidateExprValueTextBox"));
                        TextBox ResumeMandatorySettings_projectsValidateExprReasonTextBox =
                            (TextBox)(dlProjectList.FindControl("ResumeMandatorySettings_projectsValidateExprReasonTextBox"));

                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_projectsElementIdHiddenField.Value))
                            resumeProjectElementDetail.ElementId = Convert.ToInt32(ResumeMandatorySettings_projectsElementIdHiddenField.Value);
                        resumeProjectElementDetail.ValidateExprType = ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedValue.ToString();
                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_projectsValidateExprValueTextBox.Text))
                            resumeProjectElementDetail.ValidateExprValue = Convert.ToInt32(ResumeMandatorySettings_projectsValidateExprValueTextBox.Text);
                        resumeProjectElementDetail.ValidateReason = ResumeMandatorySettings_projectsValidateExprReasonTextBox.Text.ToString().Trim();
                        resumeElementDetails.Add(resumeProjectElementDetail);
                    }
                }

                //Education and Reference
                foreach (DataListItem dlEducationReference in ResumeMandatorySettings_educationReferenceDataList.Items)
                {
                    CheckBox ResumeMandatorySettings_educationReferenceElementCheckBox = 
                        (CheckBox)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceElementCheckBox"));
                    if (ResumeMandatorySettings_educationReferenceElementCheckBox.Checked)
                    {
                        ResumeElementsDetail resumeEductionReferenceElementDetail = new ResumeElementsDetail();

                        HiddenField ResumeMandatorySettings_educationReferenceElementIdHiddenField = 
                            (HiddenField)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceElementIdHiddenField"));
                        DropDownList ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown = 
                            (DropDownList)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown"));
                        TextBox ResumeMandatorySettings_educationReferenceValidateExprValueTextBox = 
                            (TextBox)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceValidateExprValueTextBox"));
                        TextBox ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox = 
                            (TextBox)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox"));

                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_educationReferenceElementIdHiddenField.Value))
                            resumeEductionReferenceElementDetail.ElementId = Convert.ToInt32(ResumeMandatorySettings_educationReferenceElementIdHiddenField.Value);
                        resumeEductionReferenceElementDetail.ValidateExprType = ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedValue.ToString();
                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Text))
                            resumeEductionReferenceElementDetail.ValidateExprValue = Convert.ToInt32(ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Text.ToString());
                        resumeEductionReferenceElementDetail.ValidateReason = ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox.Text.ToString().Trim();
                        resumeElementDetails.Add(resumeEductionReferenceElementDetail);
                    }
                }

                //Executive and Technical Summary
                foreach (DataListItem dlExcutiveTechnicalReference in ResumeMandatorySettings_excutiveTechnicalDataList.Items)
                {
                    CheckBox ResumeMandatorySettings_excutiveTechnicalElementCheckBox = 
                        (CheckBox)(dlExcutiveTechnicalReference.FindControl("ResumeMandatorySettings_excutiveTechnicalElementCheckBox"));
                    if (ResumeMandatorySettings_excutiveTechnicalElementCheckBox.Checked)
                    {
                        ResumeElementsDetail resumeExecutiveTechnicalElementDetail = new ResumeElementsDetail();

                        HiddenField ResumeMandatorySettings_excutiveTechnicalElementIdHiddenField = 
                            (HiddenField)(dlExcutiveTechnicalReference.FindControl("ResumeMandatorySettings_excutiveTechnicalElementIdHiddenField"));
                        DropDownList ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown =
                            (DropDownList)(dlExcutiveTechnicalReference.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown"));
                        TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox = 
                            (TextBox)(dlExcutiveTechnicalReference.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox"));
                        TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox = 
                            (TextBox)(dlExcutiveTechnicalReference.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox"));

                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_excutiveTechnicalElementIdHiddenField.Value))
                            resumeExecutiveTechnicalElementDetail.ElementId = Convert.ToInt32(ResumeMandatorySettings_excutiveTechnicalElementIdHiddenField.Value);
                        resumeExecutiveTechnicalElementDetail.ValidateExprType = ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedValue.ToString();
                        if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Text))
                            resumeExecutiveTechnicalElementDetail.ValidateExprValue = Convert.ToInt32(ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Text);
                        resumeExecutiveTechnicalElementDetail.ValidateReason = ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox.Text.ToString().Trim();
                        resumeElementDetails.Add(resumeExecutiveTechnicalElementDetail);
                    }
                }

                //Insert resume element
                if (resumeElementDetails != null && resumeElementDetails.Count>0)
                {
                    new ResumeRepositoryBLManager().InsertResumeValidationRule(resumeElementDetails, tenantID, userID);
                    //AssignMandatorySettings();
                    base.ShowMessage(ResumeReminderSettings_topSuccessMessageLabel,
                        ResumeReminderSettings_bottomSuccessMessageLabel, "Mandatory settings saved successfully");
                }
                else
                {
                    base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                        ResumeReminderSettings_bottomErrorMessageLabel, "Please select mandatory");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to assign/highlight the existing contactinfo values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_contactInfoDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlTable ResumeMandatorySettings_contactInfoValidationTable = 
                        ((HtmlTable)e.Item.FindControl("ResumeMandatorySettings_contactInfoValidationTable"));
                    CheckBox ResumeMandatorySettings_contactInfoElementCheckBox =
                        (CheckBox)e.Item.FindControl("ResumeMandatorySettings_contactInfoElementCheckBox");
                    TextBox ResumeMandatorySettings_contactInfoValidateExprValueTextBox =
                        (TextBox)e.Item.FindControl("ResumeMandatorySettings_contactInfoValidateExprValueTextBox");
                    DropDownList ResumeMandatorySettings_contactInfoValidateExprTypeDropDown =
                        (DropDownList)e.Item.FindControl("ResumeMandatorySettings_contactInfoValidateExprTypeDropDown");
                    HiddenField ResumeMandatorySettings_contactInfoExprTypeHiddenField =
                        (HiddenField)e.Item.FindControl("ResumeMandatorySettings_contactInfoExprTypeHiddenField");

                    if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_contactInfoExprTypeHiddenField.Value))
                        ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedValue =
                            ResumeMandatorySettings_contactInfoExprTypeHiddenField.Value.ToString().Trim();

                    if (ResumeMandatorySettings_contactInfoElementCheckBox.Checked)
                        ResumeMandatorySettings_contactInfoValidationTable.Style.Add("display", "");
                    else
                        ResumeMandatorySettings_contactInfoValidationTable.Style.Add("display", "none");

                    if (ResumeMandatorySettings_contactInfoExprTypeHiddenField.Value.ToString().Trim() == "RVE_DTE" ||
                        ResumeMandatorySettings_contactInfoExprTypeHiddenField.Value.ToString().Trim() == "RVE_EML" ||
                        ResumeMandatorySettings_contactInfoExprTypeHiddenField.Value.ToString().Trim() == "RVE_WST")
                        ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Style.Add("display", "none");
                    else
                        ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Style.Add("display", "");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to assign/highlight the existing excutive technical values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_excutiveTechnicalDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlTable ResumeMandatorySettings_excutiveTechnicalValidationTable =
                        ((HtmlTable)e.Item.FindControl("ResumeMandatorySettings_excutiveTechnicalValidationTable"));
                    CheckBox ResumeMandatorySettings_excutiveTechnicalElementCheckBox = 
                        (CheckBox)e.Item.FindControl("ResumeMandatorySettings_excutiveTechnicalElementCheckBox");

                    TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox =
                       (TextBox)e.Item.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox");
                    DropDownList ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown =
                        (DropDownList)e.Item.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown");
                    HiddenField ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField =
                        (HiddenField)e.Item.FindControl("ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField");

                    if (ResumeMandatorySettings_excutiveTechnicalElementCheckBox.Checked)
                        ResumeMandatorySettings_excutiveTechnicalValidationTable.Style.Add("display", "");
                    else
                        ResumeMandatorySettings_excutiveTechnicalValidationTable.Style.Add("display", "none");

                    if (ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField.Value.ToString().Trim() == "RVE_DTE" ||
                       ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField.Value.ToString().Trim() == "RVE_EML" ||
                       ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField.Value.ToString().Trim() == "RVE_WST")
                        ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Style.Add("display", "none");
                    else
                        ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Style.Add("display", "");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to assign/highlight the existing project values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_projectsDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlTable ResumeMandatorySettings_projectsValidationTable = 
                        ((HtmlTable)e.Item.FindControl("ResumeMandatorySettings_projectsValidationTable"));
                    CheckBox ResumeMandatorySettings_projectsElementCheckBox = 
                        (CheckBox)e.Item.FindControl("ResumeMandatorySettings_projectsElementCheckBox");

                    TextBox ResumeMandatorySettings_projectsValidateExprValueTextBox =
                       (TextBox)e.Item.FindControl("ResumeMandatorySettings_projectsValidateExprValueTextBox");
                    DropDownList ResumeMandatorySettings_projectsValidateExprTypeDropDown =
                        (DropDownList)e.Item.FindControl("ResumeMandatorySettings_projectsValidateExprTypeDropDown");
                    HiddenField ResumeMandatorySettings_projectsExprTypeHiddenField =
                        (HiddenField)e.Item.FindControl("ResumeMandatorySettings_projectsExprTypeHiddenField");

                    if (ResumeMandatorySettings_projectsElementCheckBox.Checked)
                        ResumeMandatorySettings_projectsValidationTable.Style.Add("display", "");
                    else
                        ResumeMandatorySettings_projectsValidationTable.Style.Add("display", "none");

                    if (ResumeMandatorySettings_projectsExprTypeHiddenField.Value.ToString().Trim() == "RVE_DTE" ||
                       ResumeMandatorySettings_projectsExprTypeHiddenField.Value.ToString().Trim() == "RVE_EML" ||
                       ResumeMandatorySettings_projectsExprTypeHiddenField.Value.ToString().Trim() == "RVE_WST")
                        ResumeMandatorySettings_projectsValidateExprValueTextBox.Style.Add("display", "none");
                    else
                        ResumeMandatorySettings_projectsValidateExprValueTextBox.Style.Add("display", "");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to assign/highlight the existing education reference values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_educationReferenceDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlTable ResumeMandatorySettings_educationReferenceValidationTable = 
                        ((HtmlTable)e.Item.FindControl("ResumeMandatorySettings_educationReferenceValidationTable"));
                    CheckBox ResumeMandatorySettings_educationReferenceElementCheckBox = 
                        (CheckBox)e.Item.FindControl("ResumeMandatorySettings_educationReferenceElementCheckBox");

                    TextBox ResumeMandatorySettings_educationReferenceValidateExprValueTextBox =
                       (TextBox)e.Item.FindControl("ResumeMandatorySettings_educationReferenceValidateExprValueTextBox");
                    DropDownList ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown =
                        (DropDownList)e.Item.FindControl("ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown");
                    HiddenField ResumeMandatorySettings_educationReferenceExprTypeHiddenField =
                        (HiddenField)e.Item.FindControl("ResumeMandatorySettings_educationReferenceExprTypeHiddenField");

                    if (ResumeMandatorySettings_educationReferenceElementCheckBox.Checked)
                        ResumeMandatorySettings_educationReferenceValidationTable.Style.Add("display", "");
                    else
                        ResumeMandatorySettings_educationReferenceValidationTable.Style.Add("display", "none");

                    if (ResumeMandatorySettings_educationReferenceExprTypeHiddenField.Value.ToString().Trim() == "RVE_DTE" ||
                      ResumeMandatorySettings_educationReferenceExprTypeHiddenField.Value.ToString().Trim() == "RVE_EML" ||
                      ResumeMandatorySettings_educationReferenceExprTypeHiddenField.Value.ToString().Trim() == "RVE_WST")
                        ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Style.Add("display", "none");
                    else
                        ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Style.Add("display", "");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Hanlder to show/hide table validation rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_educationReferenceElementCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Get the values here
                CheckBox ResumeMandatorySettings_educationReferenceElementCheckBox = ((CheckBox)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_educationReferenceElementCheckBox.NamingContainer);

                HtmlTable ResumeMandatorySettings_educationReferenceValidationTable =
                      (HtmlTable)ResumeMandatorySettings_educationReferenceDataList.Items[item.ItemIndex].
                      FindControl("ResumeMandatorySettings_educationReferenceValidationTable");

                if (ResumeMandatorySettings_educationReferenceElementCheckBox.Checked)
                    ResumeMandatorySettings_educationReferenceValidationTable.Style.Add("display", "");
                else
                    ResumeMandatorySettings_educationReferenceValidationTable.Style.Add("display", "none");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Hanlder to show/hide table validation rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_projectsElementCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Get the values here
                CheckBox ResumeMandatorySettings_projectsElementCheckBox = ((CheckBox)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_projectsElementCheckBox.NamingContainer);

                HtmlTable ResumeMandatorySettings_projectsValidationTable =
                      (HtmlTable)ResumeMandatorySettings_projectsDataList.Items[item.ItemIndex].
                      FindControl("ResumeMandatorySettings_projectsValidationTable");

                if (ResumeMandatorySettings_projectsElementCheckBox.Checked)
                    ResumeMandatorySettings_projectsValidationTable.Style.Add("display", "");
                else
                    ResumeMandatorySettings_projectsValidationTable.Style.Add("display", "none");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Hanlder to show/hide table validation rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_excutiveTechnicalElementCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Get the values here
                CheckBox ResumeMandatorySettings_excutiveTechnicalElementCheckBox = ((CheckBox)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_excutiveTechnicalElementCheckBox.NamingContainer);

                HtmlTable ResumeMandatorySettings_excutiveTechnicalValidationTable =
                      (HtmlTable)ResumeMandatorySettings_excutiveTechnicalDataList.Items[item.ItemIndex].
                      FindControl("ResumeMandatorySettings_excutiveTechnicalValidationTable");

                if (ResumeMandatorySettings_excutiveTechnicalElementCheckBox.Checked)
                    ResumeMandatorySettings_excutiveTechnicalValidationTable.Style.Add("display", "");
                else
                    ResumeMandatorySettings_excutiveTechnicalValidationTable.Style.Add("display", "none");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Hanlder to show/hide table validation rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_contactInfoElementCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Get the values here
                CheckBox ResumeMandatorySettings_contactInfoElementCheckBox = ((CheckBox)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_contactInfoElementCheckBox.NamingContainer);

                HtmlTable ResumeMandatorySettings_contactInfoValidationTable =
                      (HtmlTable)ResumeMandatorySettings_contactInfoDataList.Items[item.ItemIndex].
                      FindControl("ResumeMandatorySettings_contactInfoValidationTable");

                if (ResumeMandatorySettings_contactInfoElementCheckBox.Checked)
                    ResumeMandatorySettings_contactInfoValidationTable.Style.Add("display", "");
                else
                    ResumeMandatorySettings_contactInfoValidationTable.Style.Add("display", "none");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Hanlder to show or hide the values/reason
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_contactInfoValidateExprTypeDropDown_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            try
            {
                //Get the selected values
                DropDownList ResumeMandatorySettings_contactInfoValidateExprTypeDropDown = ((DropDownList)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.NamingContainer);

                TextBox ResumeMandatorySettings_contactInfoValidateExprValueTextBox =
                    (TextBox)ResumeMandatorySettings_contactInfoDataList.Items[item.ItemIndex].
                    FindControl("ResumeMandatorySettings_contactInfoValidateExprValueTextBox");
                TextBox ResumeMandatorySettings_contactInfoValidateExprReasonTextBox =
                    (TextBox)ResumeMandatorySettings_contactInfoDataList.Items[item.ItemIndex].
                    FindControl("ResumeMandatorySettings_contactInfoValidateExprReasonTextBox");

                if (ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "--Select--")
                {
                    ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_contactInfoValidateExprReasonTextBox.Style.Add("display", "none");
                }
                else if (ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_DTE" ||
                    ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_EML" ||
                    ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_WST")
                {
                    ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_contactInfoValidateExprReasonTextBox.Style.Add("display", "");
                }
                else
                {
                    ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_contactInfoValidateExprReasonTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Text = "1";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Hanlder to show or hide the values/reason
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            try
            {
                //Get the selected values
                DropDownList ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown = ((DropDownList)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.NamingContainer);

                TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox =
                    (TextBox)ResumeMandatorySettings_excutiveTechnicalDataList.Items[item.ItemIndex].
                    FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox");
                TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox =
                    (TextBox)ResumeMandatorySettings_excutiveTechnicalDataList.Items[item.ItemIndex].
                    FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox");

                if (ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "--Select--")
                {
                    ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox.Style.Add("display", "none");
                }
                else if (ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_DTE" ||
                    ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_EML" ||
                    ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_WST")
                {
                    ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox.Style.Add("display", "");
                }
                else
                {
                    ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Text="1";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Hanlder to show or hide the values/reason
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_projectsValidateExprTypeDropDown_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            try
            {
                //Get the selected values
                DropDownList ResumeMandatorySettings_projectsValidateExprTypeDropDown = ((DropDownList)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_projectsValidateExprTypeDropDown.NamingContainer);

                TextBox ResumeMandatorySettings_projectsValidateExprValueTextBox =
                    (TextBox)ResumeMandatorySettings_projectsDataList.Items[item.ItemIndex].
                    FindControl("ResumeMandatorySettings_projectsValidateExprValueTextBox");

                TextBox ResumeMandatorySettings_projectsValidateExprReasonTextBox =
                   (TextBox)ResumeMandatorySettings_projectsDataList.Items[item.ItemIndex].
                   FindControl("ResumeMandatorySettings_projectsValidateExprReasonTextBox");

                if (ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "--Select--")
                {
                    ResumeMandatorySettings_projectsValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_projectsValidateExprReasonTextBox.Style.Add("display", "none");
                }
                else if (ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_DTE" ||
                    ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_EML" ||
                    ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_WST")
                {
                    ResumeMandatorySettings_projectsValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_projectsValidateExprReasonTextBox.Style.Add("display", "");
                }
                else
                {
                    ResumeMandatorySettings_projectsValidateExprValueTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_projectsValidateExprReasonTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_projectsValidateExprValueTextBox.Text = "1";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Hanlder to show or hide the values/reason
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            try
            {
                //Get the selected values
                DropDownList ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown = ((DropDownList)sender);
                DataListItem item = ((DataListItem)ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.NamingContainer);

                TextBox ResumeMandatorySettings_educationReferenceValidateExprValueTextBox =
                    (TextBox)ResumeMandatorySettings_educationReferenceDataList.Items[item.ItemIndex].
                    FindControl("ResumeMandatorySettings_educationReferenceValidateExprValueTextBox");

                TextBox ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox =
                     (TextBox)ResumeMandatorySettings_educationReferenceDataList.Items[item.ItemIndex].
                     FindControl("ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox");

                if (ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "--Select--")
                {
                    ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox.Style.Add("display", "none");
                }
                else if (ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_DTE" ||
                    ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_EML" ||
                    ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedItem.Value.ToString().Trim() == "RVE_WST")
                {
                    ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Style.Add("display", "none");
                    ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox.Style.Add("display", "");
                }
                else
                {
                    ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox.Style.Add("display", "");
                    ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Text = "1";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method to reset the settings 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeReminderSettings_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                LoadResumeElements();
                BindAttributes();
                AssignMandatorySettings();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method to assign the existing validation and its values
        /// </summary>
        private void AssignMandatorySettings()
        {
            //Contact Information
            foreach (DataListItem dlContactInfo in ResumeMandatorySettings_contactInfoDataList.Items)
            {
                    
                TextBox ResumeMandatorySettings_contactInfoValidateExprValueTextBox = 
                    (TextBox)(dlContactInfo.FindControl("ResumeMandatorySettings_contactInfoValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_contactInfoValidateExprReasonTextBox = 
                    (TextBox)(dlContactInfo.FindControl("ResumeMandatorySettings_contactInfoValidateExprReasonTextBox"));
                CheckBox ResumeMandatorySettings_contactInfoElementCheckBox = 
                    (CheckBox)(dlContactInfo.FindControl("ResumeMandatorySettings_contactInfoElementCheckBox"));
                DropDownList ResumeMandatorySettings_contactInfoValidateExprTypeDropDown = 
                    (DropDownList)(dlContactInfo.FindControl("ResumeMandatorySettings_contactInfoValidateExprTypeDropDown"));
                HiddenField ResumeMandatorySettings_contactInfoExprTypeHiddenField =
                    (HiddenField)(dlContactInfo.FindControl("ResumeMandatorySettings_contactInfoExprTypeHiddenField"));
                if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_contactInfoExprTypeHiddenField.Value))
                    ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedValue = 
                        ResumeMandatorySettings_contactInfoExprTypeHiddenField.Value;
                if (!ResumeMandatorySettings_contactInfoElementCheckBox.Checked)
                {
                    ResumeMandatorySettings_contactInfoValidateExprValueTextBox.Text = string.Empty;
                    ResumeMandatorySettings_contactInfoValidateExprReasonTextBox.Text = string.Empty;
                }
                
            }

            //Project Information
            foreach (DataListItem dlProjectInfo in ResumeMandatorySettings_projectsDataList.Items)
            {
                TextBox ResumeMandatorySettings_projectsValidateExprValueTextBox = 
                    (TextBox)(dlProjectInfo.FindControl("ResumeMandatorySettings_projectsValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_projectsValidateExprReasonTextBox = 
                    (TextBox)(dlProjectInfo.FindControl("ResumeMandatorySettings_projectsValidateExprReasonTextBox"));
                CheckBox ResumeMandatorySettings_projectsElementCheckBox = 
                    (CheckBox)(dlProjectInfo.FindControl("ResumeMandatorySettings_projectsElementCheckBox"));
                DropDownList ResumeMandatorySettings_projectsValidateExprTypeDropDown = 
                    (DropDownList)(dlProjectInfo.FindControl("ResumeMandatorySettings_projectsValidateExprTypeDropDown"));
                HiddenField ResumeMandatorySettings_projectsExprTypeHiddenField = 
                    (HiddenField)(dlProjectInfo.FindControl("ResumeMandatorySettings_projectsExprTypeHiddenField"));
                if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_projectsExprTypeHiddenField.Value))
                    ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedValue = 
                        ResumeMandatorySettings_projectsExprTypeHiddenField.Value;
                if (!ResumeMandatorySettings_projectsElementCheckBox.Checked)
                {
                    ResumeMandatorySettings_projectsValidateExprValueTextBox.Text = string.Empty;
                    ResumeMandatorySettings_projectsValidateExprReasonTextBox.Text = string.Empty;
                }
            }

            //Education and Reference
            foreach (DataListItem dlEducationReference in ResumeMandatorySettings_educationReferenceDataList.Items)
            {
                TextBox ResumeMandatorySettings_educationReferenceValidateExprValueTextBox = 
                    (TextBox)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox = 
                    (TextBox)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox"));
                CheckBox ResumeMandatorySettings_educationReferenceElementCheckBox = 
                    (CheckBox)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceElementCheckBox"));
                DropDownList ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown = 
                    (DropDownList)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown"));
                HiddenField ResumeMandatorySettings_educationReferenceExprTypeHiddenField = 
                    (HiddenField)(dlEducationReference.FindControl("ResumeMandatorySettings_educationReferenceExprTypeHiddenField"));
                if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_educationReferenceExprTypeHiddenField.Value))
                    ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedValue =
                        ResumeMandatorySettings_educationReferenceExprTypeHiddenField.Value;
                if (!ResumeMandatorySettings_educationReferenceElementCheckBox.Checked)
                {
                    ResumeMandatorySettings_educationReferenceValidateExprValueTextBox.Text = string.Empty;
                    ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox.Text = string.Empty;
                }
            }

            //Excutive and Technical Summary
            foreach (DataListItem dlExcutiveTechnical in ResumeMandatorySettings_excutiveTechnicalDataList.Items)
            {
                TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox =
                    (TextBox)(dlExcutiveTechnical.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox = 
                    (TextBox)(dlExcutiveTechnical.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox"));
                CheckBox ResumeMandatorySettings_excutiveTechnicalElementCheckBox = 
                    (CheckBox)(dlExcutiveTechnical.FindControl("ResumeMandatorySettings_excutiveTechnicalElementCheckBox"));
                DropDownList ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown =
                    (DropDownList)(dlExcutiveTechnical.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown"));
                HiddenField ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField =
                    (HiddenField)(dlExcutiveTechnical.FindControl("ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField"));
                if (!Utility.IsNullOrEmpty(ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField.Value))
                    ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedValue = 
                        ResumeMandatorySettings_excutiveTechnicalExprTypeHiddenField.Value;
                if (!ResumeMandatorySettings_excutiveTechnicalElementCheckBox.Checked)
                {
                    ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox.Text = string.Empty;
                    ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox.Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// To load the resume note and its element details
        /// </summary>
        private void LoadResumeElements()
        {
            List<ResumeElementsDetail> resumeElement = new List<ResumeElementsDetail>();
            resumeElement = new ResumeRepositoryBLManager().GetResumeElements(tenantID);
            if (resumeElement != null)
            {

                var ContactInfo = Enumerable.Where(resumeElement, 
                    p => p.Node.Trim() == Constants.ResumeMandatoryConstants.RESUME_MANDATORY_CONTACT_INFO);

                if (ContactInfo != null)
                {
                    ResumeMandatorySettings_contactInfoDataList.DataSource = ContactInfo;
                    ResumeMandatorySettings_contactInfoDataList.DataBind();
                }

                var ExcutiveTechnicalInfo = Enumerable.Where(resumeElement,
                    p => p.Node.Trim() == Constants.ResumeMandatoryConstants.RESUME_MANDATORY_EXECUTIVE_SUMMARY || 
                    p.Node.Trim() == Constants.ResumeMandatoryConstants.RESUME_MANDATORY_TECHNICAL_SKILL);
                if (ExcutiveTechnicalInfo != null)
                {
                    ResumeMandatorySettings_excutiveTechnicalDataList.DataSource = ExcutiveTechnicalInfo;
                    ResumeMandatorySettings_excutiveTechnicalDataList.DataBind();
                }

                var ProjectsInfo = Enumerable.Where(resumeElement, 
                    p => p.Node.Trim() == Constants.ResumeMandatoryConstants.RESUME_MANDATORY_PROJECT_HISTORY);
                if (ProjectsInfo != null)
                {
                    ResumeMandatorySettings_projectsDataList.DataSource = ProjectsInfo;
                    ResumeMandatorySettings_projectsDataList.DataBind();
                }
                var EducationReferenceInfo = Enumerable.Where(resumeElement,
                    p => p.Node.Trim() == Constants.ResumeMandatoryConstants.RESUME_MANDATORY_REFERENCES || 
                    p.Node.Trim() == Constants.ResumeMandatoryConstants.RESUME_MANDATORY_EDUCATION);
                if (EducationReferenceInfo != null)
                {
                    ResumeMandatorySettings_educationReferenceDataList.DataSource = EducationReferenceInfo;
                    ResumeMandatorySettings_educationReferenceDataList.DataBind();
                }
            }
        }

        /// <summary>
        /// Bind attributes for validation
        /// </summary>
        private void BindAttributes()
        {
            foreach (DataListItem dli in ResumeMandatorySettings_contactInfoDataList.Items)
            {
                DropDownList ResumeMandatorySettings_contactInfoValidateExprTypeDropDown = (DropDownList)(dli.FindControl("ResumeMandatorySettings_contactInfoValidateExprTypeDropDown"));

                ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.RESUME_VALIDATE_EXPRESSION, "A");
                ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.DataTextField = "AttributeName";
                ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.DataValueField = "AttributeID";
                ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.DataBind();

                ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.Items.Insert(0, "--Select--");
                ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedIndex = 0;
            }
            foreach (DataListItem dli in ResumeMandatorySettings_excutiveTechnicalDataList.Items)
            {
                DropDownList ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown = (DropDownList)(dli.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown"));

                ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.RESUME_VALIDATE_EXPRESSION, "A");
                ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.DataTextField = "AttributeName";
                ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.DataValueField = "AttributeID";
                ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.DataBind();

                ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.Items.Insert(0, "--Select--");
                ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedIndex = 0;
            }
            foreach (DataListItem dli in ResumeMandatorySettings_projectsDataList.Items)
            {
                DropDownList ResumeMandatorySettings_projectsValidateExprTypeDropDown = (DropDownList)(dli.FindControl("ResumeMandatorySettings_projectsValidateExprTypeDropDown"));

                ResumeMandatorySettings_projectsValidateExprTypeDropDown.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.RESUME_VALIDATE_EXPRESSION, "A");
                ResumeMandatorySettings_projectsValidateExprTypeDropDown.DataTextField = "AttributeName";
                ResumeMandatorySettings_projectsValidateExprTypeDropDown.DataValueField = "AttributeID";
                ResumeMandatorySettings_projectsValidateExprTypeDropDown.DataBind();

                ResumeMandatorySettings_projectsValidateExprTypeDropDown.Items.Insert(0, "--Select--");
                ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedIndex = 0;
            }

            foreach (DataListItem dli in ResumeMandatorySettings_educationReferenceDataList.Items)
            {
                DropDownList ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown = (DropDownList)(dli.FindControl("ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown"));

                ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.RESUME_VALIDATE_EXPRESSION, "A");
                ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.DataTextField = "AttributeName";
                ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.DataValueField = "AttributeID";
                ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.DataBind();

                ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.Items.Insert(0, "--Select--");
                ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Method to clear the success/error message controls
        /// </summary>
        private void ClearControl()
        {
            ResumeReminderSettings_topSuccessMessageLabel.Text = string.Empty;
            ResumeReminderSettings_bottomSuccessMessageLabel.Text = string.Empty;
            ResumeReminderSettings_topErrorMessageLabel.Text = string.Empty;
            ResumeReminderSettings_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method to validate mandatory fields
        /// </summary>
        /// <returns></returns>
        private bool validateMandatoryFields()
        {

            return true;
        }
        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool IsValidData = true;
            //Validate contact information
            foreach (DataListItem dli in ResumeMandatorySettings_contactInfoDataList.Items)
            {
                CheckBox ResumeMandatorySettings_contactInfoElementCheckBox = 
                    (CheckBox)(dli.FindControl("ResumeMandatorySettings_contactInfoElementCheckBox"));
                DropDownList ResumeMandatorySettings_contactInfoValidateExprTypeDropDown = 
                    (DropDownList)(dli.FindControl("ResumeMandatorySettings_contactInfoValidateExprTypeDropDown"));
                TextBox ResumeMandatorySettings_contactInfoValidateExprValueTextBox = 
                    (TextBox)(dli.FindControl("ResumeMandatorySettings_contactInfoValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_contactInfoValidateExprReasonTextBox = 
                    (TextBox)(dli.FindControl("ResumeMandatorySettings_contactInfoValidateExprReasonTextBox"));
                if (ResumeMandatorySettings_contactInfoElementCheckBox.Checked)
                {
                    if (ResumeMandatorySettings_contactInfoValidateExprTypeDropDown.SelectedIndex == 0 || 
                        Utility.IsNullOrEmpty(ResumeMandatorySettings_contactInfoValidateExprReasonTextBox.Text))
                    {
                        base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                            ResumeReminderSettings_bottomErrorMessageLabel, "Please complete contact information");
                        IsValidData = false;
                        
                    }
                }
            }
            //Validate excutive and technical information
            foreach (DataListItem dlExcutive in ResumeMandatorySettings_excutiveTechnicalDataList.Items)
            {
                CheckBox ResumeMandatorySettings_excutiveTechnicalElementCheckBox =
                    (CheckBox)(dlExcutive.FindControl("ResumeMandatorySettings_excutiveTechnicalElementCheckBox"));
                DropDownList ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown = 
                    (DropDownList)(dlExcutive.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown"));
                TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox = 
                    (TextBox)(dlExcutive.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox = 
                    (TextBox)(dlExcutive.FindControl("ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox"));
                if (ResumeMandatorySettings_excutiveTechnicalElementCheckBox.Checked)
                {
                    if (ResumeMandatorySettings_excutiveTechnicalValidateExprTypeDropDown.SelectedIndex == 0 ||
                        Utility.IsNullOrEmpty(ResumeMandatorySettings_excutiveTechnicalValidateExprReasonTextBox.Text))
                    {
                        base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, ResumeReminderSettings_bottomErrorMessageLabel,
                            "Please complete executive and technical information");
                        IsValidData = false;

                    }
                }
            }
            //Validate project details
            foreach (DataListItem dlProject in ResumeMandatorySettings_projectsDataList.Items)
            {
                CheckBox ResumeMandatorySettings_projectsElementCheckBox =
                    (CheckBox)(dlProject.FindControl("ResumeMandatorySettings_projectsElementCheckBox"));
                DropDownList ResumeMandatorySettings_projectsValidateExprTypeDropDown = 
                    (DropDownList)(dlProject.FindControl("ResumeMandatorySettings_projectsValidateExprTypeDropDown"));
                TextBox ResumeMandatorySettings_projectsValidateExprValueTextBox =
                    (TextBox)(dlProject.FindControl("ResumeMandatorySettings_projectsValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_projectsValidateExprReasonTextBox = 
                    (TextBox)(dlProject.FindControl("ResumeMandatorySettings_projectsValidateExprReasonTextBox"));
                if (ResumeMandatorySettings_projectsElementCheckBox.Checked)
                {
                    if (ResumeMandatorySettings_projectsValidateExprTypeDropDown.SelectedIndex == 0 ||
                        Utility.IsNullOrEmpty(ResumeMandatorySettings_projectsValidateExprReasonTextBox.Text))
                    {
                        base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                            ResumeReminderSettings_bottomErrorMessageLabel, "Please complete project information");
                        IsValidData = false;

                    }
                }
            }
            //Validate education and reference details
            foreach (DataListItem dlEducation in ResumeMandatorySettings_educationReferenceDataList.Items)
            {
                CheckBox ResumeMandatorySettings_educationReferenceElementCheckBox = 
                    (CheckBox)(dlEducation.FindControl("ResumeMandatorySettings_educationReferenceElementCheckBox"));
                DropDownList ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown =
                    (DropDownList)(dlEducation.FindControl("ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown"));
                TextBox ResumeMandatorySettings_educationReferenceValidateExprValueTextBox = 
                    (TextBox)(dlEducation.FindControl("ResumeMandatorySettings_educationReferenceValidateExprValueTextBox"));
                TextBox ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox =
                    (TextBox)(dlEducation.FindControl("ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox"));
                if (ResumeMandatorySettings_educationReferenceElementCheckBox.Checked)
                {
                    if (ResumeMandatorySettings_educationReferenceValidateExprTypeDropDown.SelectedIndex == 0 ||
                        Utility.IsNullOrEmpty(ResumeMandatorySettings_educationReferenceValidateExprReasonTextBox.Text))
                    {
                        base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel, 
                            ResumeReminderSettings_bottomErrorMessageLabel, "Please complete education & reference information ");
                        IsValidData = false;

                    }
                }
            }
            return IsValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

       
        #endregion Protected Overridden Methods



        
}
}