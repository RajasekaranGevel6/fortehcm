<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    CodeBehind="DictionaryManager.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.DictionaryManager" %>

<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="DictionaryManager_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <script language="javascript" type="text/javascript">

        // Function that will show the dictionary entry window.
        function ShowDictionaryEntryWindow() {
            var height = 540;
            var width = 600;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/DictionaryEntry.aspx";
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <asp:Literal ID="DictionaryManager_headerLiteral" runat="server" Text="Dictionary Manager"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="right" style="width: 60px">
                                        <asp:Button ID="DictionaryManager_editDictionaryButton" runat="server" SkinID="sknButtonId"
                                            Text="Edit Dictionary" ToolTip="Edit Dictionary" />
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="DictionaryManager_resetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="DictionaryManager_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="DictionaryManager_cancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="DictionaryManager_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="DictionaryManager_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="DictionaryManager_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="DictionaryManager_searchByTestDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="DictionaryManager_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="DictionaryManager_simpleSearchDiv" runat="server" style="display: block;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td style="width: 10%">
                                                                                <asp:Label runat="server" ID="DictionaryManager_dictionaryTypeLabel" Text="Dictionary Type"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:DropDownList ID="DictionaryManager_dictionaryTypeDropDownList" runat="server"
                                                                                    Width="130px">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td style="width: 5%">
                                                                                <asp:Label runat="server" ID="DictionaryManager_headLabel" Text="Head" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="DictionaryManager_headTextBox" Width="71%" MaxLength="255"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 5%">
                                                                                <asp:Label runat="server" ID="DictionaryManager_aliasLabel" Text="Alias" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="DictionaryManager_aliasTextBox" Width="71%" MaxLength="255"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="DictionaryManager_searchButton" runat="server" Text="Search" OnClick="DictionaryManager_searchButton_Click"
                                                            SkinID="sknButtonId" ToolTip="Search" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="DictionaryManager_searchTestResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="DictionaryManager_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="DictionaryManager_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="DictionaryManager_searchResultsUpSpan" style="display: none;" runat="server">
                                            <asp:Image ID="DictionaryManager_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="DictionaryManager_searchResultsDownSpan" style="display: block;"
                                            runat="server">
                                            <asp:Image ID="DictionaryManager_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="DictionaryManager_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel runat="server" ID="DictionaryManager_searchGridViewUpdatePanel">
                                            <ContentTemplate>
                                                <div style="height: 200px; overflow: auto;" runat="server" id="DictionaryManager_testDiv">
                                                    <asp:GridView ID="DictionaryManager_searchGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="DictionaryManager_searchGridView_RowDataBound"
                                                        OnRowCreated="DictionaryManager_searchGridView_RowCreated" OnSorting="DictionaryManager_searchGridView_Sorting"
                                                        OnRowCommand="DictionaryManager_searchGridView_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DictionaryManager_deleteImageButton" runat="server" SkinID="sknDeleteDictionaryEntryImageButton"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="DeleteEntry" ToolTip="Delete Dictionary Entry" />
                                                                    <asp:HiddenField ID="DictionaryManager_elementTypeIDHiddenField" runat="server" Value='<%# Eval("ElementTypeID") %>' />
                                                                    <asp:HiddenField ID="DictionaryManager_aliasIDHiddenField" runat="server" Value='<%# Eval("AliasID") %>' />
                                                                    <asp:HiddenField ID="DictionaryManager_aliasNameHiddenField" runat="server" Value='<%# Eval("AliasName") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Dictionary Type" DataField="ElementTypeName" SortExpression="DICTIONARY_TYPE"
                                                                ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                            <asp:BoundField HeaderText="Head" DataField="ElementName" SortExpression="HEAD" ItemStyle-Width="30%"
                                                                HeaderStyle-Width="30%" />
                                                            <asp:BoundField HeaderText="Alias" DataField="AliasName" SortExpression="ALIAS" ItemStyle-Width="65%"
                                                                HeaderStyle-Width="65%" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="DictionaryManager_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="DictionaryManager_bottomPagingNavigatorUpdatePanel">
                                <ContentTemplate>
                                    <uc2:PageNavigator ID="DictionaryManager_bottomPagingNavigator" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="DictionaryManager_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="DictionaryManager_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="DictionaryManager_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="DictionaryManager_deleteEntryUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="DictionaryManager_deleteEntryPanel" runat="server" Style="display: none;
                            height: 202px" CssClass="popupcontrol_confirm">
                            <div style="display: none">
                                <asp:Button ID="DictionaryManager_deleteEntryPanel_hiddenButton" runat="server" />
                            </div>
                            <uc1:ConfirmMsgControl ID="DictionaryManager_deleteEntryPanel_confirmMessageControl"
                                runat="server" OnOkClick="DictionaryManager_deleteEntryPanel_okButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="DictionaryManager_deleteEntryPanel_confirmModalPopupExtender"
                            runat="server" PopupControlID="DictionaryManager_deleteEntryPanel" TargetControlID="DictionaryManager_deleteEntryPanel_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
