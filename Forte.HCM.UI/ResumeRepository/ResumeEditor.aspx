<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" MaintainScrollPositionOnPostback="true"
    AutoEventWireup="true" CodeBehind="ResumeEditor.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.ResumeEditor" %>

<%@ Register Src="~/CommonControls/RolesVectorControl.ascx" TagName="RolesVectorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/MainResumeControl.ascx" TagName="MainResumeControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="ResumeEditor_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <script language="javascript" type="text/javascript">

        // Function that will call when candidate name load button is clicked
        // on the resumeeditor page. 
        function ShowHidePanel(ctrlHide, ctrlExpand)
        {
            if (document.getElementById(ctrlHide) != null)
            {
                if (document.getElementById(ctrlExpand).style.width == "99%")
                {
                    document.getElementById(ctrlHide).style.display = "";
                    document.getElementById(ctrlHide).style.width = "49%";
                    document.getElementById(ctrlExpand).style.width = "50%";
                }
                else if (document.getElementById(ctrlHide).style.width == "99%")
                {
                    document.getElementById(ctrlHide).style.display = "";
                    document.getElementById(ctrlExpand).style.display = "";
                    document.getElementById(ctrlHide).style.width = "49%";
                    document.getElementById(ctrlExpand).style.width = "50%";
                }
                else
                {
                    document.getElementById(ctrlHide).style.display = "none";
                    document.getElementById(ctrlExpand).style.width = "99%";
                    document.getElementById(ctrlExpand).style.display = "";
                }
            }
        }

        // Function that will call when candidate name load button is clicked
        // on the resumeeditor page. 
        function DisplayBottomPane(showDiv, hideTable)
        {
            if (document.getElementById('<%= ResumeEditor_candidateIDHiddenField.ClientID %>').value != "")
            {
                document.getElementById(showDiv).style.display = "";
                document.getElementById(hideTable).style.display = "none";
            }
        }

        // Function that will call when reset button is clicked
        // on the resumeeditor page. 
        function HideBottomPane(hideDiv)
        {
            document.getElementById(hideDiv).style.display = "none";
        }

        // Function that will call when candidate name search image button is clicked
        // on the resumeeditor page.
        function LoadCandidate(idctrl, namectrl, ctrlname, firstnamectrl)
        {
            var height = 550;
            var width = 880;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
            var queryStringValue = "../Popup/SearchCandidateRepository.aspx?idctrl=" + idctrl + "&namectrl=" + namectrl + "&ctrlName=" + ctrlname + "&firstnamectrl=" + firstnamectrl + "&subctrlname=RESUME_EDITOR";
            var retValue = window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

        // Function that will call when any action is performed
        // on the resumeeditor page and maintain scroll position. 
        var xDetailsPos, yDetailsPos, xRolesVectorPos, yRolesVectorPos, xResumePos, yResumePos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args)
        {
            xDetailsPos = $get('<%=ResumeEditor_scrollDetailsDiv.ClientID %>').scrollLeft;
            yDetailsPos = $get('<%=ResumeEditor_scrollDetailsDiv.ClientID %>').scrollTop;

            xRolesVectorPos = $get('<%=ResumeEditor_scrollRolesVectorDiv.ClientID %>').scrollLeft;
            yRolesVectorPos = $get('<%=ResumeEditor_scrollRolesVectorDiv.ClientID %>').scrollTop;

            xResumePos = $get('<%=ResumeEditor_rightPaneInnerDiv.ClientID %>').scrollLeft;
            yResumePos = $get('<%=ResumeEditor_rightPaneInnerDiv.ClientID %>').scrollTop;
        }
        function EndRequestHandler(sender, args)
        {
            $get('<%=ResumeEditor_scrollDetailsDiv.ClientID %>').scrollLeft = xDetailsPos;
            $get('<%=ResumeEditor_scrollDetailsDiv.ClientID %>').scrollTop = yDetailsPos;

            $get('<%=ResumeEditor_scrollRolesVectorDiv.ClientID %>').scrollLeft = xRolesVectorPos;
            $get('<%=ResumeEditor_scrollRolesVectorDiv.ClientID %>').scrollTop = yRolesVectorPos;

            $get('<%=ResumeEditor_rightPaneInnerDiv.ClientID %>').scrollLeft = xResumePos;
            $get('<%=ResumeEditor_rightPaneInnerDiv.ClientID %>').scrollTop = yResumePos;
        }            
    </script>
    <div>
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="width: 50%">
                                <asp:Literal ID="ResumeEditor_headerLiteral" runat="server" Text="Edit Resume"></asp:Literal>
                            </td>
                            <td style="width: 50%">
                                <asp:UpdatePanel ID="ResumeEditor_saveUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="2" cellspacing="0" align="right">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="ResumeEditor_topSaveButton" runat="server" CommandName="cmdSave"
                                                        SkinID="sknButtonId" Text="Save" OnClick="ResumeEditor_topSaveButton_Click" Visible="false"/>
                                                </td>
                                                <td>
                                                    <asp:Button ID="ResumeEditor_topApproveButton" runat="server" SkinID="sknButtonId"
                                                        Text="Approve" OnClick="ResumeEditor_topApproveButton_Click" Visible="false"/>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ResumeEditor_topResetButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                                        OnClick="ResumeEditor_topResetButton_Click"></asp:LinkButton>
                                                </td>
                                                <td align="center">
                                                    |
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ResumeEditor_topCancelButton" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                                        OnClick="ParentPageRedirect"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="ResumeEditor_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="ResumeEditor_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ResumeEditor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ResumeEditor_topSaveButton" />
                            <asp:AsyncPostBackTrigger ControlID="ResumeEditor_topApproveButton" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="ResumeEditor_parseBtnUpdatePanel" runat="server">
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0" width="100%" border="0" id="ResumeEditor_candidateTable"
                                runat="server">
                                <tr>
                                    <td class="header_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    <asp:Literal ID="ResumeEditor_selectLiteral" runat="server" Text="Select Candidate & Resume Version">
                                                    </asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table cellpadding="0" cellspacing="5" width="100%">
                                            <tr>
                                                <td class="td_padding_top_bottom" style="width: 40%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" runat="server">
                                                        <tr>
                                                            <td class="td_padding_top_bottom">
                                                                <asp:Label runat="server" ID="ResumeEditor_candidateLabel" Text="Candidate" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span class='mandatory'>*</span>
                                                            </td>
                                                            <td valign="middle" style="padding-left: 10px">
                                                                <asp:TextBox runat="server" ID="ResumeEditor_candidateIDTextBox" Width="220px" Text=""
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td valign="middle" align="right" style="padding-left: 4px">
                                                                <asp:ImageButton ID="ResumeEditor_searchImageButton" SkinID="sknbtnSearchicon" runat="server"
                                                                    ImageAlign="Middle" ToolTip="Click here to select the candidate" />
                                                                     <asp:HiddenField runat="server" ID="ResumeEditor_candidateIDHiddenField"></asp:HiddenField>
                                                                <asp:HiddenField ID="ResumeEditor_candidateFirstNameHiddenField" runat="server" />
                                                                <asp:HiddenField runat="server" ID="ResumeEditor_candidateNameHiddenField"></asp:HiddenField>
                                                                <asp:HiddenField runat="server" ID="ResumeEditor_isLoadClickHiddenField"></asp:HiddenField>
                                                                 <div style="display: none">
                                                                    <asp:Button runat="server" ID="ResumeEditor_loadDetailsButton" Text="Show Resumes"
                                                                        SkinID="sknButtonId" OnClick="ResumeEditor_loadDetailsButton_Click" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="td_padding_top_bottom" style="width: 60%" valign="top">
                                                    <div style="height: 116px; width: 100%; overflow: auto" runat="server" id="ResumeEdit_resumeVersionsGridViewDiv">
                                                        <asp:GridView ID="ResumeEdit_resumeVersionsGridView" runat="server" AllowSorting="False"
                                                            Width="100%" SkinID="sknWrapHeaderGrid" AutoGenerateColumns="False" BorderStyle="Solid"
                                                            BorderColor="#d1d8da" BorderWidth="2px">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Select" HeaderStyle-Width="30px" HeaderStyle-Wrap="true"
                                                                    ItemStyle-Width="30px" ItemStyle-Wrap="false">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="ResumeEdit_resumeVersionsGridView_selectRadioButton" runat="server"
                                                                            ToolTip="Click here to load resume details and preview" AutoPostBack="true" OnCheckedChanged="ResumeEdit_resumeVersionsGridView_selectRadioButton_CheckedChanged" />
                                                                        <asp:HiddenField ID="ResumeEdit_resumeVersionsGridView_candidateResumeIDHiddenField"
                                                                            Value='<%# Eval("CandidateResumeID") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Profile Name" HeaderStyle-Width="140px" HeaderStyle-Wrap="false"
                                                                    ItemStyle-Width="140px" ItemStyle-Wrap="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="ResumeEdit_resumeVersionsGridView_profileNameLabel" runat="server"
                                                                            Text='<%# Eval("ProfileName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Uploaded Date" HeaderStyle-Width="110px" HeaderStyle-Wrap="false"
                                                                    ItemStyle-Width="110px" ItemStyle-Wrap="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="ResumeEdit_resumeVersionsGridView_uploadedDateLabel" runat="server"
                                                                            Text='<%# GetDateFormat(Convert.ToDateTime(Eval("UploadedDate"))) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width: 100%; display: block;" runat="server" id="ResumeEditor_bottomPaneDIV">
                        <asp:UpdatePanel ID="ResumeEditor_tabUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div runat="server" class="LeftPaneDiv" id="ResumeEditor_leftPaneDiv">
                                    <ajaxToolKit:TabContainer ID="ResumeEditor_mainTabContainer" runat="server" ActiveTabIndex="0">
                                        <ajaxToolKit:TabPanel ID="ResumeEditor_detailsTabPanel" HeaderText="Details" runat="server">
                                            <HeaderTemplate>
                                                Details
                                            </HeaderTemplate>
                                            <ContentTemplate>
                                                <div style="height: 420px; overflow: auto;" id="ResumeEditor_scrollDetailsDiv" runat="server">
                                                    <uc1:MainResumeControl ID="ResumeEditor_mainResumeControl" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolKit:TabPanel>
                                        <ajaxToolKit:TabPanel ID="ResumeEditor_vectorTabPanel" HeaderText="Competency Vector"
                                            runat="server">
                                            <HeaderTemplate>
                                                Competency Vector
                                            </HeaderTemplate>
                                            <ContentTemplate>
                                                <div style="height: 420px; overflow: auto; width: 800px;" id="ResumeEditor_scrollRolesVectorDiv"
                                                    runat="server">
                                                    <uc3:RolesVectorControl ID="ResumeEditor_rolesVectorControl" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolKit:TabPanel>
                                    </ajaxToolKit:TabContainer>
                                </div>
                                <div style="width: 2%; height: 460px; display: inline; float: left;">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="height: 25px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="bottom" style="height: 418px;">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="bottom" runat="server" id="ResumeEditor_hideLeftPaneTD">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" runat="server" id="ResumeEditor_hideRightPaneTD">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div runat="server" class="RightPaneOuterDiv" align="left" id="ResumeEditor_rightPaneDiv">
                                    <ajaxToolKit:TabContainer ID="ResumeEditor_previewTabContainer" runat="server" ActiveTabIndex="0">
                                        <ajaxToolKit:TabPanel ID="ResumeEditor_htmlTab" HeaderText="Html" runat="server">
                                            <HeaderTemplate>
                                                Preview
                                            </HeaderTemplate>
                                            <ContentTemplate>
                                                <asp:UpdatePanel ID="ResumeEditor_UploadDiv" runat="server" ChildrenAsTriggers="true"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div runat="server" id="ResumeEditor_uploadButtonDiv" visible="true" style="height: 30px;
                                                            width: 100%;">
                                                            <table border="0" cellpadding="0" cellspacing="0" runat="server" id="ResumeEditor_UploadTable"
                                                                style="width: 100%;">
                                                                <tr>
                                                                    <td valign="middle">
                                                                        <asp:ImageButton ID="ResumeEditor_downloadImageButton" runat="server" Text="Download"
                                                                            SkinID="sknDownloadImageButton" OnClick="ResumeEditor_downloadImageButtonClick"
                                                                            ToolTip="Download Resume" AlternateText="Download" Visible="false"/>
                                                                        <asp:HiddenField ID="ResumeEditor_candidateResumeIDHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div runat="server" id="ResumeEditor_rightPaneInnerDiv" class="RightPaneInnerDiv">
                                                            <div runat="server" id="ResumeEditor_perviewHtmlDiv" style="height: 370px;">
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ResumeEditor_downloadImageButton" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ContentTemplate>
                                        </ajaxToolKit:TabPanel>
                                    </ajaxToolKit:TabContainer>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
