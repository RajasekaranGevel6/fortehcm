﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    AutoEventWireup="true" CodeBehind="CareerBuilderResumeSearch.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.CareerBuilderResumeSearch" %>

<%@ Register Src="~/CommonControls/ExternalResumePreview.ascx" TagName="ExternalResumePreview"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="MultiSelectControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="CareerBuilderResumeSearch_content" ContentPlaceHolderID="ResumeRepositoryMaster_body"
    runat="server">
    <script language="javascript" type="text/javascript">
        function addElement() {
            var locationDiv = document.getElementById('locationDiv');
            var initialValue = document.getElementById('theValue');
            var locationIds = document.getElementById("<%= locationIds.ClientID %>");
            var locationCount = document.getElementById('locationCount');
            locationCount.value = (locationCount.value - 1) + 2;
            var num = (document.getElementById('theValue').value - 1) + 2;
            initialValue.value = num;
            var newdiv = document.createElement('div');
            var divIdName = 'location' + num;
            newdiv.setAttribute('id', divIdName);
            locationIds.value = locationIds.value + "," + num;
            newdiv.innerHTML = "<br/><input name=" + divIdName + " id=" + divIdName + " type=text class=text_box> <a href=javascript:; onclick=removeElement('" + divIdName + "'," + num + ")><img src=../App_Themes/DefaultTheme/Images/tab_cont_close_btn.gif alt=close border=0 /></a>";
            //"<br/><div style=float: left; padding-right: 5px;><input name=" + divIdName + " id=" + divIdName + " type=text class=text_box></div><div style=float: left;><a href=javascript:; onclick=AdvanceremoveElement('" + divIdName + "'," + num + ")><img src=../App_Themes/DefaultTheme/Images/tab_cont_close_btn.gif alt=close border=0 /></a></div>";
            //"<br/><input name=" + divIdName + " id=" + divIdName + " type=text class=text_box> <a href=javascript:; onclick=removeElement('" + divIdName + "'," + num + ")><img src=../App_Themes/DefaultTheme/Images/tab_cont_close_btn.gif alt=close border=0 /></a>";
            locationDiv.appendChild(newdiv);
            hideAddLinkButton()
        }

        function removeElement(divNum, num) {
            var locationIds = document.getElementById("<%= locationIds.ClientID %>");
            locationIds.value = locationIds.value.replace(num, '');
            var d = document.getElementById('locationDiv');
            var olddiv = document.getElementById(divNum);
            d.removeChild(olddiv);
            var locationCount = document.getElementById('locationCount');
            locationCount.value = locationCount.value - 1;
            hideAddLinkButton()
        }

        function hideAddLinkButton() {
            //var numi = document.getElementById("<%= locationIds.ClientID %>");
            var numi = document.getElementById("locationCount");
            var linkButton = document.getElementById('addLocation');
            linkButton.style.visibility = 'visible';
            if (numi.value * 1 > 2) {
                linkButton.style.visibility = 'hidden';
            }
        }


        function addAdvanceElement() {
            var locationDiv = document.getElementById('AdvancelocationDiv');
            var initialValue = document.getElementById('AdvancetheValue');
            var locationIds = document.getElementById("<%= AdvancelocationIds.ClientID %>");
            var locationCount = document.getElementById('AdvancelocationCount');
            locationCount.value = (locationCount.value - 1) + 2;
            var num = (document.getElementById('AdvancetheValue').value - 1) + 2;
            initialValue.value = num;
            var newdiv = document.createElement('div');
            var divIdName = 'Advancelocation' + num;
            newdiv.setAttribute('id', divIdName);
            locationIds.value = locationIds.value + "," + num;
            newdiv.innerHTML = "<br/><input name=" + divIdName + " id=" + divIdName + " type=text class=text_box> <a href=javascript:; onclick=AdvanceremoveElement('" + divIdName + "'," + num + ")><img src=../App_Themes/DefaultTheme/Images/tab_cont_close_btn.gif alt=close border=0 /></a>";
            locationDiv.appendChild(newdiv);
            AdvancehideAddLinkButton()

        }

        function AdvanceremoveElement(divNum, num) {
            var locationIds = document.getElementById("<%= AdvancelocationIds.ClientID %>");
            locationIds.value = locationIds.value.replace(num, '');
            var d = document.getElementById('AdvancelocationDiv');
            var olddiv = document.getElementById(divNum);
            d.removeChild(olddiv);
            var locationCount = document.getElementById('AdvancelocationCount');
            locationCount.value = locationCount.value - 1;
            AdvancehideAddLinkButton()
        }

        function AdvancehideAddLinkButton() {
            //var numi = document.getElementById("<%= locationIds.ClientID %>");
            var numi = document.getElementById("AdvancelocationCount");
            var linkButton = document.getElementById('AdvanceaddLocation');
            linkButton.style.visibility = 'visible';
            if (numi.value * 1 > 2) {
                linkButton.style.visibility = 'hidden';
            }
        }


    </script>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CareerBuilderResumeSearch_headerLiteral" runat="server" Text="Search Career Builder"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table cellpadding="0" cellspacing="2" align="right">
                                <tr>
                                    <td width="63%" align="right">
                                        <asp:Button ID="CareerBuilderResumeSearch_topSearchButton" runat="server" SkinID="sknButtonId"
                                            Text="Search" Visible="false" />
                                        <asp:LinkButton ID="CareerBuilderResumeSearch_topModifySearchLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Modify Search" Visible="false" />
                                    </td>
                                    <td width="4%" align="center">
                                    </td>
                                    <td align="right" style="width: 15%">
                                        <asp:LinkButton ID="CareerBuilderResumeSearch_topResetLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Reset" OnClick="CareerBuilderResumeSearch_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="CareerBuilderResumeSearch_topCancelLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CareerBuilderResumeSearch_topMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <input type="hidden" value="1" id="locationCount" />
                        <input type="hidden" value="1" id="locationIds" name="locationIds" runat="server" />
                        <input type="hidden" value="1" id="AdvancelocationCount" />
                        <input type="hidden" value="1" id="AdvancelocationIds" name="AdvancelocationIds"
                            runat="server" />
                        <asp:HiddenField ID="CareerBuilderResumeSearch_totalRecordsHiddenField" runat="server"
                            Value="0" />
                        <asp:HiddenField ID="CareerBuilderResumeSearch_searchTypeHiddenField" runat="server"
                            Value="simple" />
                        <asp:HiddenField ID="CareerBuilderResumeSearch_sessionTokenHiddenField" runat="server"
                            Value="0" />
                        <asp:Label ID="CareerBuilderResumeSearch_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CareerBuilderResumeSearch_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <div id="CareerBuilderResumeSearch_searchByResumeDiv" runat="server" style="display: block;">
                    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>--%>
                    <ajaxToolKit:TabContainer ID="CareerBuilderResumeSearch_mainTabContainer" runat="server"
                        ActiveTabIndex="0" Visible="true">
                        <ajaxToolKit:TabPanel ID="CareerBuilderResumeSearch_simpleTabPanel" HeaderText="Simple"
                            runat="server" TabIndex="0">
                            <HeaderTemplate>
                                Simple
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div id="TestSession_byTest_searchByTestDiv" runat="server" style="display: block;">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="panel_bg">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td width="100%">
                                                                        <div id="TestSession_byTest_simpleSearchDiv" runat="server" style="display: block;
                                                                            width: 100%; padding-top: 2px">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                <tr>
                                                                                    <td style="width: 50%">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="SearchTest_categoryHeadLabel" runat="server" Text="Search by Keyword"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div>
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="CareerBuilderResumeSearch_simpleKeywordTextBox" runat="server" Width="250px"
                                                                                                                MaxLength="250"></asp:TextBox>
                                                                                                        </div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="CareerBuilderResumeSearch_keywordHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                                runat="server" ToolTip="Boolean operators (AND, OR, etc.) are allowed" ImageAlign="Middle"
                                                                                                                OnClientClick="javascript:return false;" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="SearchTest_subjectHeadLabel" runat="server" Text="Last Modified "
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="CareerBuilderResumeSearch_simpleLastModifiedDropDownList" runat="server"
                                                                                                        Width="150px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div style="float: left; padding-right: 5px;">
                                                                                                        <asp:Label ID="Label9" runat="server" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </div>
                                                                                                    <div style="float: left;">
                                                                                                        <asp:ImageButton ID="ImageButton1" SkinID="sknHelpImageButton" runat="server" ToolTip="City, State -OR- Zip Code"
                                                                                                            ImageAlign="Middle" OnClientClick="javascript:return false;" />
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <input type="hidden" value="1" id="theValue" />
                                                                                                    <div id="locationDiv">
                                                                                                        <input type="text" name="location1" class="text_box" />
                                                                                                    </div>
                                                                                                    <p>
                                                                                                        <a href="javascript:;" onclick="addElement();" id="addLocation" class="link_button">
                                                                                                            Add Location</a></p>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="width: 50%">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" class="td_padding_top_5">
                                                                        <asp:UpdatePanel ID="tt" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:Button ID="CareerBuilderResumeSearch_simpleSearchButton" runat="server" Text="Search"
                                                                                    SkinID="sknButtonId" OnCommand="CareerBuilderResumeSearch_simpleSearchButton_Command"
                                                                                    CommandName="simple" />&nbsp;
                                                                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Clear" SkinID="sknActionLinkButton"
                                                                                    Visible="false"></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="td_height_2">
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <asp:HiddenField ID="TestSession_byTest_isMaximizedHiddenField" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="CreateManualTest_advanceTabPanel" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                Advanced
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div id="TestSession_byTestSession_searchByTestSessionDiv" runat="server" style="display: block;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="panel_bg">
                                                            <table width="100%" cellpadding="" cellspacing="0">
                                                                <tr>
                                                                    <td class="panel_inner_body_bg">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label3" runat="server" Text="Search Terms" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 50%">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceKeywordLabel" runat="server" Text="Keyword(s)"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceKeywordUsingLabel" runat="server" Text="Using"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="CreateManualTest_advanceKeywordTextBox" runat="server" Width="250"
                                                                                                    MaxLength="250">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CreateManualTest_advanceKeywordUsingDropdownList" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceJobTitlLabel" runat="server" Text="Job Title(s)"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceJobTitleUsingLabel" runat="server" Text="Using"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="CreateManualTest_advanceJobTitleTextBox" runat="server" Width="250"
                                                                                                    MaxLength="250">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CreateManualTest_advanceJobTitleUsingDropdownList" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceCompanyNameLabel" runat="server" Text="Company Name(s)"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceCompanyNameUsingLabel" runat="server" Text="Using"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="CreateManualTest_advanceCompanyNameUsingTextBox" runat="server"
                                                                                                    Width="250" MaxLength="250">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CreateManualTest_advanceCompanyNameUsingDropdownList" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceResumeTitleLabel" runat="server" Text="Resume Title(s)"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_advanceResumeTitleUsingLabel" runat="server" Text="Usings"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="CreateManualTest_advanceResumeTitleTextBox" runat="server" Width="250"
                                                                                                    MaxLength="250">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CreateManualTest_advanceResumeTitleUsingDropdownList" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label1" runat="server" Text="Last Modified" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label2" runat="server" Text="Resume Last Modified" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceLastModifiedDropDownList"
                                                                                                    runat="server" Width="150">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label4" runat="server" Text="Job Categories" SkinID="sknLabelFieldHeaderText">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label5" runat="server" Text="Category" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <%--<uc1:MultiSelectControl ID="CandidateReportCandidateStatisticsInfo_WidgetMultiSelectControl"
                                                        runat="server" OnselectedIndexChanged="CandidateReportCandidateStatisticsInfo_selectProperty"
                                                        OnClick="CandidateReportCandidateStatisticsInfo_widgetMultiSelectControl_Click" 
                                                        OncancelClick="CandidateReportCandidateStatisticsInfo_widgetMultiSelectControl_CancelClick" />--%>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceJobCategoryDropDownList" runat="server"
                                                                                                    Width="150">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <%--<tr>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Location" SkinID="sknLabelFieldHeaderText">
                                        </asp:Label>
                                        <asp:Label ID="Label10" runat="server" Text="(Add up to 3)" SkinID="sknLabelFieldText">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                           <tr>
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" Text="City, State -OR-   Zip Code" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="CareerBuilderResumeSearch_locationTextBox" runat="server"></asp:TextBox>
                                                    <span id="locationSpan">&nbsp;</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" >
                                                    <a href="javascript:" onclick="add('text')">Add Location</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label16" runat="server" Text="Distance" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td>
                                                    Include Resumes within
                                                    <asp:DropDownList ID="CareerBuilderResumeSearch_advanceDistanceValueDropDownList"
                                                        runat="server" Width="50">
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="30" Value="30" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                                        <asp:ListItem Text="150" Value="150"></asp:ListItem>
                                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="CareerBuilderResumeSearch_advanceDistanceValueInDropDownList"
                                                        runat="server" Width="100">
                                                        <asp:ListItem Text="Kilometers" Value="KM"></asp:ListItem>
                                                        <asp:ListItem Text="Miles" Value="MI"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                                                            <tr>
                                                                                <td class="td_height_20">
                                                                                    <div style="float: left; padding-right: 5px;">
                                                                                        <asp:Label ID="Label30" runat="server" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </div>
                                                                                    <div style="float: left;">
                                                                                        <asp:ImageButton ID="ImageButton2" SkinID="sknHelpImageButton" runat="server" ToolTip="City, State -OR- Zip Code"
                                                                                            ImageAlign="Middle" OnClientClick="javascript:return false;" />
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="hidden" value="1" id="AdvancetheValue" />
                                                                                    <div id="AdvancelocationDiv">
                                                                                        <input type="text" name="Advancelocation1" class="text_box" />
                                                                                    </div>
                                                                                    <p>
                                                                                        <a href="javascript:;" onclick="addAdvanceElement();" id="AdvanceaddLocation" class="link_button">
                                                                                            Add Location</a></p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label11" runat="server" Text="Employment Type" SkinID="sknLabelFieldHeaderText">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:CheckBoxList ID="CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList"
                                                                                                    runat="server" RepeatDirection="Horizontal">
                                                                                                </asp:CheckBoxList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label8" runat="server" Text="Education" SkinID="sknLabelFieldHeaderText">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label6" runat="server" Text="School" SkinID="sknLabelFieldText">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="Label17" runat="server" Text="Min Degree" SkinID="sknLabelFieldText">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="Label18" runat="server" Text="Max Degree" SkinID="sknLabelFieldText">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="CareerBuilderResumeSearch_advanceSchoolTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceMinDegreeDropDownList" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceMaxDegreeDropDownList" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label12" runat="server" Text="Work Experience" SkinID="sknLabelFieldHeaderText">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label19" runat="server" Text="Min Years" SkinID="sknLabelFieldText">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="Label20" runat="server" Text="Max Years" SkinID="sknLabelFieldText">
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="Label21" runat="server" Text="Min Level of Travel" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceMinYearsExperienceDropDownList"
                                                                                                    runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceMaxYearsExperienceDropDownList"
                                                                                                    runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceMinLevelTravelDropDownList"
                                                                                                    runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <asp:Label ID="Label22" runat="server" Text="Management Experience" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <asp:CheckBox ID="CareerBuilderResumeSearch_advanceManagementExperienceCheckbox"
                                                                                                    runat="server" />
                                                                                                Candidate should have management experience, supervising at least
                                                                                                <asp:TextBox ID="CareerBuilderResumeSearch_advanceMinimumEmployeesManagedTextBox"
                                                                                                    runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                                                                                direct reports
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <asp:Label ID="Label23" runat="server" Text="Management Level" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <asp:CheckBoxList ID="CareerBuilderResumeSearch_advanceManagementLevelCheckBoxlist"
                                                                                                    RepeatDirection="Horizontal" runat="server">
                                                                                                </asp:CheckBoxList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label13" runat="server" Text="Salary" SkinID="sknLabelFieldHeaderText">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label24" runat="server" Text="Min Salary" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="Label25" runat="server" Text="Max Salary" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="minSalareyTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="maxSalarayTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:RadioButtonList ID="CareerBuilderResumeSearch_advanceYearlyRadoibutonList" runat="server"
                                                                                                    RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Text="Hourley" Value="HOUR"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Yearly" Value="SALR"></asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <asp:CheckBox ID="CareerBuilderResumeSearch_advanceCheckSalaryCheckBox" runat="server" />
                                                                                                Exclude resumes that don't include salary information
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label14" runat="server" Text="Work Status" SkinID="sknLabelFieldHeaderText">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label26" runat="server" Text="Authorization" SkinID="sknLabelFieldText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:CheckBoxList ID="CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList" RepeatDirection="Horizontal"
                                                                                                    RepeatColumns="2" runat="server">
                                                                                                </asp:CheckBoxList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label15" runat="server" Text="Additional Criteria" SkinID="sknLabelFieldHeaderText">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label27" runat="server" Text="Languages Spoken" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="CareerBuilderResumeSearch_advanceLanguageDropDownList" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label28" runat="server" Text="Miltary Experience" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:CheckBoxList ID="CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList"
                                                                                                    RepeatDirection="Horizontal" RepeatColumns="4" runat="server">
                                                                                                </asp:CheckBoxList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label29" runat="server" Text="Security" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:CheckBox ID="CareerBuilderResumeSearch_advancesecurityCheckBox" runat="server" />
                                                                                                Candidate should have government security clearance.
                                                                                            </td>
                                                                                        </tr>
                                                                                        <%--  <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="Label30" runat="server" Text="Resumes Per Page" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="CareerBuilderResumeSearch_advanceResumePageSize" runat="server">
                                                                                                <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                                                                                <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                                                                <asp:ListItem Text="75" Value="75"></asp:ListItem>
                                                                                                <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_padding_top_5" align="right">
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:Button ID="TestSession_byTestSession_topSearchButton" runat="server" Text="Search"
                                                                                    SkinID="sknButtonId" OnCommand="CareerBuilderResumeSearch_simpleSearchButton_Command"
                                                                                    CommandName="advance" />&nbsp;
                                                                                <asp:LinkButton ID="TestSession_byTestSession_clearLinkButton" runat="server" Text="Clear"
                                                                                    SkinID="sknActionLinkButton" Visible="false"></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                    <%-- </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="CareerBuilderResumeSearch_bottomModifySearchLinkButton" />
                        <asp:AsyncPostBackTrigger ControlID="CareerBuilderResumeSearch_topModifySearchLinkButton" />
                    </Triggers>
                </asp:UpdatePanel>--%>
                </div>
            </td>
        </tr>
        <tr>
            <td class="td_height_10">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <asp:UpdatePanel ID="CareerBuilderResumeSearch_simpleGridViewUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <asp:Panel ID="CareerBuilderResumeSearch_searchResultPanel" runat="server" Visible="true">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr id="CareerBuilderResumeSearch_searchResultsTR" runat="server">
                                                <td id="Td1" class="header_bg" align="center" runat="server">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                                <asp:Literal ID="CareerBuilderResumeSearch_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                                &nbsp;<asp:Label ID="CareerBuilderResumeSearch_searchResultsCountLabel" runat="server" SkinID="sknLabelText"
                                                                    ></asp:Label>
                                                            </td>
                                                            <td style="width: 50%" align="right">
                                                                <span id="CareerBuilderResumeSearch_searchResultsUpSpan" runat="server" style="display: none;">
                                                                    <asp:Image ID="CareerBuilderResumeSearch_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                </span><span id="CareerBuilderResumeSearch_searchResultsDownSpan" runat="server"
                                                                    style="display: block;">
                                                                    <asp:Image ID="CareerBuilderResumeSearch_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                </span>
                                                                <asp:HiddenField ID="CareerBuilderResumeSearch_restoreHiddenField" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left">
                                                                <div style="height: 225px; overflow: auto;" runat="server" id="CareerBuilderResumeSearch_resultGridViewDiv">
                                                                    <asp:GridView ID="CareerBuilderResumeSearch_simpleGridView" SkinID="sknNewGridView"
                                                                        runat="server" OnRowCommand="CareerBuilderResumeSearch_GridView_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-Width="25px">
                                                                                <ItemTemplate>
                                                                                    <table width="100%" border="0" cellspacing="2" cellpadding="5">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("ResumeTitle")%>' SkinID="sknLabelFieldTextBold"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <asp:ImageButton ID="previewResume" runat="server" SkinID="sknPreviewResumeImageButton"
                                                                                                    ToolTip="Preview Resume" CommandName="previewresume" CommandArgument='<%# Eval("ResumeID") %>' />
                                                                                                <asp:ImageButton ID="alreadyDownloadResume" runat="server" SkinID="sknAlreadySaveResumeImageButton"
                                                                                                    ToolTip="Resume already in repository" Visible="false" Enabled="false" />
                                                                                                <asp:ImageButton ID="downloadResume" runat="server" SkinID="sknSaveResumeImageButton"
                                                                                                    ToolTip="Click here to save resume" CommandName="simpledownloadresume" CommandArgument='<%# Eval("ResumeID") %>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" style="width: 50%">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label16" runat="server" Text='<%# Eval("CandidateName")%>' SkinID="sknLabelFieldText"></asp:Label><br />
                                                                                                            <asp:Label ID="Label41" runat="server" Text='<%# Eval("CandidateAddress")%>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="td_height_8">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label7" runat="server" Text="Recent Position" SkinID="sknLabelFieldHeaderText"></asp:Label><br />
                                                                                                            <asp:Label ID="Label31" runat="server" Text='<%# Eval("MostRecentTitle")%>' SkinID="sknLabelFieldText"></asp:Label><br />
                                                                                                            <asp:Label ID="Label32" runat="server" Text='<%# Eval("MostRecentCompanyName")%>'
                                                                                                                SkinID="sknLabelFieldText"></asp:Label><br />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td valign="top" style="width: 50%">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td style="width: 50%">
                                                                                                            <asp:Label ID="Label37" runat="server" Text="Years Experience" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 50%">
                                                                                                            <asp:Label ID="Label33" runat="server" Text='<%# int.Parse(Eval("ExperienceMonths").ToString())/12 %>'
                                                                                                                SkinID="sknLabelFieldText"></asp:Label><br />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label38" runat="server" Text="Modified" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label34" runat="server" Text='<%# Eval("LastModified")%>' SkinID="sknLabelFieldText"></asp:Label><br />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label39" runat="server" Text="Degree Level" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label35" runat="server" Text='<%# Eval("HighestDegree")%>' SkinID="sknLabelFieldText"></asp:Label><br />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label40" runat="server" Text="Recent Pay" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label36" runat="server" Text=' <%# "$ "+ Eval("RecentPay")%>' SkinID="sknLabelFieldText"></asp:Label><br />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc2:PageNavigator ID="CareerBuilderResumeSearch_simplePagingNavigator" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="CareerBuilderResumeSearch_bottomModifySearchLinkButton" />
                        <asp:AsyncPostBackTrigger ControlID="CareerBuilderResumeSearch_topModifySearchLinkButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CareerBuilderResumeSearch_byQuestion_bottomMessagesUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CareerBuilderResumeSearch_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CareerBuilderResumeSearch_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr align="right">
            <td align="right" class="header_bg">
                <table cellpadding="0" cellspacing="2" align="right">
                    <tr>
                        <td width="63%" align="right">
                            <asp:Button ID="CareerBuilderResumeSearch_bottomSearchButton" runat="server" SkinID="sknButtonId"
                                Text="Search" Visible="false" />
                            <asp:LinkButton ID="CareerBuilderResumeSearch_bottomModifySearchLinkButton" runat="server"
                                SkinID="sknActionLinkButton" Text="Modify Search" Visible="false" />
                        </td>
                        <td width="4%" align="center">
                        </td>
                        <td align="right" style="width: 15%">
                            <asp:LinkButton ID="CareerBuilderResumeSearch_bottomResetLinkButton" runat="server"
                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CareerBuilderResumeSearch_resetLinkButton_Click" />
                        </td>
                        <td width="4%" align="center">
                            |
                        </td>
                        <td width="18%" align="left">
                            <asp:LinkButton ID="CareerBuilderResumeSearch_bottomCancelLinkButton" runat="server"
                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="CareerBuilderResumeSearch_externalResumePreviewUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="CareerBuilderResumeSearch_externalResumePreviewControlPanel" runat="server"
                            Style="display: none" Class="popupcontrol_careerbuilder_resume">
                            <uc2:ExternalResumePreview ID="CareerBuilderResumeSearch_externalResumePreviewControl"
                                runat="server" OnCancelClick="CareerBuilderResumeSearch_Preview_cancelClick" />
                            <asp:Button ID="CareerBuilderResumeSearch_externalResumePreviewDummyButton" runat="server"
                                Style="display: none" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CareerBuilderResumeSearch_externalResumePreviewModalPopupExtender"
                            runat="server" TargetControlID="CareerBuilderResumeSearch_externalResumePreviewDummyButton"
                            PopupControlID="CareerBuilderResumeSearch_externalResumePreviewControlPanel"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
