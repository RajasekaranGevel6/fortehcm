<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    CodeBehind="CompetencyVectorManager.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.CompetencyVectorManager" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<asp:Content ID="CompetencyVectorManager_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <script language="javascript" type="text/javascript">

        // Function that will show the certification entry window.
        function ShowCertificationTitleWindow(vectorID) {
            var height = 560;
            var width = 720;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/CertificationTitleEntry.aspx?vectorid=" + vectorID;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <asp:Literal ID="CompetencyVectorManager_headerLiteral" runat="server" Text="Competency Vector Manager"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="CompetencyVectorManager_resetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CompetencyVectorManager_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="CompetencyVectorManager_cancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="CompetencyVectorManager_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="CompetencyVectorManager_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CompetencyVectorManager_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="CompetencyVectorManager_searchDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="CompetencyVectorManager_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="CompetencyVectorManager_simpleSearchDiv" runat="server" style="display: block;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="CompetencyVectorManager_groupTypeLabel" Text="Vector Group"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="CompetencyVectorManager_groupTypeDropDownList" runat="server"
                                                                                    Width="130px">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="center">
                                                                                <asp:Label runat="server" ID="CompetencyVectorManager_vectorLabel" Text="Vector Name"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CompetencyVectorManager_vectorTextBox" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                            <td align="center">
                                                                                <asp:Label runat="server" ID="CompetencyVectorManager_vectorAliasLabel" Text="Vector Alias"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="CompetencyVectorManager_vectorAliasTextBox" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="CompetencyVectorManager_searchButton" runat="server" Text="Search"
                                                            SkinID="sknButtonId" ToolTip="Search" OnClick="CompetencyVectorManager_searchButton_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_8">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="CompetencyVectorManager_searchResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="CompetencyVectorManager_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="CompetencyVectorManager_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="CompetencyVectorManager_searchResultsUpSpan" style="display: none;" runat="server">
                                            <asp:Image ID="CompetencyVectorManager_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="CompetencyVectorManager_searchResultsDownSpan" style="display: block;"
                                            runat="server">
                                            <asp:Image ID="CompetencyVectorManager_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="CompetencyVectorManager_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel runat="server" ID="CompetencyVectorManager_searchGridViewUpdatePanel">
                                            <ContentTemplate>
                                                <div style="height: 200px; overflow: auto;" runat="server" id="CompetencyVectorManager_resultDiv">
                                                    <asp:GridView ID="CompetencyVectorManager_searchGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="CompetencyVectorManager_searchGridView_RowDataBound"
                                                        OnSorting="CompetencyVectorManager_searchGridView_Sorting" OnRowCreated="CompetencyVectorManager_searchGridView_RowCreated"
                                                        OnRowCommand="CompetencyVectorManager_searchGridView_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="CompetencyVectorManager_vectorIDHiddenField" runat="server"
                                                                        Value='<%# Eval("VectorID") %>' />
                                                                    <asp:HiddenField ID="CompetencyVectorManager_vectorGroupNameHiddenField" runat="server"
                                                                        Value='<%# Eval("VectorGroup") %>' />
                                                                    <asp:HiddenField ID="CompetencyVectorManager_vectorNameHiddenField" runat="server"
                                                                        Value='<%# Eval("VectorName") %>' />
                                                                    <asp:HiddenField ID="CompetencyVectorManager_vectorAliasNameHiddenField" runat="server"
                                                                        Value='<%# Eval("AliasName") %>' />
                                                                    <asp:ImageButton ID="CompetencyVectorManager_editVectorImageButton" runat="server"
                                                                        SkinID="sknEditDictionaryEntryImageButton" ToolTip="Edit Alias Name" CommandName="EditVector"
                                                                        CommandArgument='<%#Eval("VectorID") %>' />
                                                                    <asp:ImageButton ID="CompetencyVectorManager_deleteVectorImageButton" runat="server"
                                                                        SkinID="sknDeleteDictionaryEntryImageButton" ToolTip="Delete Vector" CommandName="DeleteVector"
                                                                        CommandArgument='<%#Eval("VectorID") %>' />
                                                                    <asp:ImageButton ID="CompetencyVectorManager_certificationTitleImageButton" runat="server"
                                                                        SkinID="sknCertificationTitleImageButton" ToolTip="View/Edit Certification Titles" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Vector Group" DataField="VectorGroup" SortExpression="GROUP_NAME"
                                                                ItemStyle-Width="20%" HeaderStyle-Width="20%" />
                                                            <asp:BoundField HeaderText="Vector Name" DataField="VectorName" SortExpression="VECTOR_NAME"
                                                                ItemStyle-Width="30%" HeaderStyle-Width="30%" />
                                                            <asp:BoundField HeaderText="Alias" DataField="AliasName" SortExpression="VECTOR_NAME_ALIAS"
                                                                ItemStyle-Wrap="true" ItemStyle-Width="40%" HeaderStyle-Width="40%" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CompetencyVectorManager_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="CompetencyVectorManager_bottomPagingNavigatorUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" style="padding-left: 6px; height: 20px; width: 50%">
                                                <asp:LinkButton ID="CompetencyVectorManager_addVectorLinkButton" SkinID="sknAddLinkButton"
                                                    ToolTip="Add New Vector" runat="server" Text="Add Vector" OnClick="CompetencyVectorManager_addVectorLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="right" style="width: 50%">
                                                <uc2:PageNavigator ID="CompetencyVectorManager_bottomPagingNavigator" runat="server"
                                                    OnPageNumberClick="CompetencyVectorManager_pageNavigator_PageNumberClick" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CompetencyVectorManager_editVectorUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="CompetencyVectorManager_editVectorPanel" runat="server" CssClass="popupcontrol_credit_request"
                                        Style="display: block" DefaultButton="CompetencyVectorManager_saveButton">
                                        <div style="display: none;">
                                            <asp:Button ID="CompetencyVectorManager_editVectorhiddenButton" runat="server" Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="CompetencyVectorManager_editVectorLiteral" runat="server" Text="Edit Vector Details"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="CompetencyVectorManager_topCancelImageButton" runat="server"
                                                                                SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                                <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="4">
                                                                            <asp:Label ID="CompetencyVectorManager_editVector_errorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 35%">
                                                                            <asp:Label ID="CompetencyVectorManager_groupNameHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Group Name"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="CompetencyVectorManager_groupNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="CompetencyVectorManager_vectorNameHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Vector Name"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="CompetencyVectorManager_vectorNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CompetencyVectorManager_aliasHeadLabel" runat="server" Text="Alias"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="CompetencyVectorManager_aliasTextBox" TextMode="MultiLine" Height="100"
                                                                                    runat="server" Width="260px" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                                    onchange="CommentsCount(500,this)" TabIndex="2"> </asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="CompetencyVectorManager_aliasImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="You can enter multiple alias name separated by comma " />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Button ID="CompetencyVectorManager_saveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                                    OnClick="CompetencyVectorManager_saveButton_Click" TabIndex="3" />
                                                                &nbsp;
                                                                <asp:LinkButton ID="CompetencyVectorManager_editVectorCancelLinkButton" runat="server"
                                                                    Text="Cancel" SkinID="sknPopupLinkButton" TabIndex="4"></asp:LinkButton>
                                                                <asp:HiddenField ID="CompetencyVectorManager_editVectorIdHiddenField" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="CompetencyVectorManager_editVectorModalPopUpExtender"
                                        runat="server" TargetControlID="CompetencyVectorManager_editVectorhiddenButton"
                                        PopupControlID="CompetencyVectorManager_editVectorPanel" BackgroundCssClass="modalBackground"
                                        CancelControlID="CompetencyVectorManager_editVectorCancelLinkButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CompetencyVectorManager_deletePopupUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="CompetencyVectorManager_deletePopupPanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_confirm_remove">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Label ID="CompetencyVectorManager_deletePopup_titleLabel" runat="server" Text="Delete Vector"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="CompetencyVectorManager_deletePopup_closeImageButton" runat="server"
                                                                                SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="msg_align">
                                                                <asp:Label ID="CompetencyVectorManager_deletePopup_errorMessageLabel" runat="server"
                                                                    SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Label ID="CompetencyVectorManager_deletePopup_messageValidLabel" runat="server"
                                                                                SkinID="sknLabelFieldText" Text="Are you sure want to delete the vector?"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_20">
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="bottom">
                                                                        <td align="right" style="text-align: center">
                                                                            <asp:Button ID="CompetencyVectorManager_deletePopup_yesValidButton" runat="server"
                                                                                SkinID="sknButtonId" Text="Yes" OnClick="CompetencyVectorManager_deletePopup_yesValidButton_Click" />
                                                                            <asp:Button ID="CompetencyVectorManager_deletePopup_noValidButton" runat="server"
                                                                                SkinID="sknButtonId" Text="No" OnClientClick="javascript:return false;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_20">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div style="display: none">
                                        <asp:Button ID="CompetencyVectorManager_deletePopup_hiddenButton" runat="server" />
                                    </div>
                                    <ajaxToolKit:ModalPopupExtender ID="CompetencyVectorManager_deletePopupExtender"
                                        runat="server" PopupControlID="CompetencyVectorManager_deletePopupPanel" TargetControlID="CompetencyVectorManager_deletePopup_hiddenButton"
                                        BackgroundCssClass="modalBackground" CancelControlID="CompetencyVectorManager_deletePopup_noValidButton"
                                        DynamicServicePath="" Enabled="True">
                                    </ajaxToolKit:ModalPopupExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CompetencyVectorManager_addNewVectorUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="CompetencyVectorManager_addNewVectorPanel" runat="server" CssClass="popupcontrol_credit_request"
                                        Style="display: none" DefaultButton="CompetencyVectorManager_addNewVectorSaveButton">
                                        <div style="display: none;">
                                            <asp:Button ID="CompetencyVectorManager_addNewVectorHiddenButton" runat="server"
                                                Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="CompetencyVectorManager_addNewVectorLiteral" runat="server" Text="Add New Vector"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="CompetencyVectorManager_addNewVectorCloseImageButton" runat="server"
                                                                                SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                                <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="2">
                                                                            <asp:Label ID="CompetencyVectorManager_addNewVectorErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 22%">
                                                                            <asp:Label ID="CompetencyVectorManager_addNewVectorGroupNameLabel" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Group Name"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="CompetencyVectorManager_addNewVectorGroupNameDropDownList"
                                                                                runat="server" Width="91%" TabIndex="2">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CompetencyVectorManager_addNewVectorNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Vector Name"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CompetencyVectorManager_addNewVectorNameTextBox" runat="server"
                                                                                Width="89%" MaxLength="50" TabIndex="3">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CompetencyVectorManager_addNewVectorAliasLabel" runat="server" Text="Alias"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="CompetencyVectorManager_addNewVectorAliasTextBox" TextMode="MultiLine"
                                                                                    Height="100" runat="server" Width="272px" SkinID="sknMultiLineTextBox" MaxLength="500"
                                                                                    onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" TabIndex="4"> </asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="CompetencyVectorManager_addNewVectorAliasHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="You can enter multiple alias name separated by comma " />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Button ID="CompetencyVectorManager_addNewVectorSaveButton" runat="server" Text="Save"
                                                                    SkinID="sknButtonId" OnClick="CompetencyVectorManager_addNewVectorSaveButton_Click"
                                                                    TabIndex="5" />
                                                                &nbsp;
                                                                <asp:LinkButton ID="CompetencyVectorManager_addNewVectorCancelLinkButton" runat="server"
                                                                    Text="Cancel" SkinID="sknPopupLinkButton" TabIndex="6"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="CompetencyVectorManager_addNewVectorModalPopupExtender"
                                        runat="server" TargetControlID="CompetencyVectorManager_addNewVectorHiddenButton"
                                        PopupControlID="CompetencyVectorManager_addNewVectorPanel" BackgroundCssClass="modalBackground"
                                        CancelControlID="CompetencyVectorManager_addNewVectorCancelLinkButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="CompetencyVectorManager_bottomUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="CompetencyVectorManager_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CompetencyVectorManager_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="CompetencyVectorManager_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="CompetencyVectorManager_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="CompetencyVectorManager_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
