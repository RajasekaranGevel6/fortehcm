﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocationManager.aspx.cs"
    MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" Inherits="Forte.HCM.UI.ResumeRepository.LocationManager" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ContentPlaceHolderID="ResumeRepositoryMaster_body" ID="LocationManager_bodyContent"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <asp:Literal ID="LocationManager_headerLiteral" runat="server" Text="Location Manager"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="LocationManager_resetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="LocationManager_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="LocationManager_cancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="LocationManager_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="LocationManager_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="LocationManager_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="LocationManager_searchDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="LocationManager_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="LocationManager_simpleSearchDiv" runat="server" style="display: block;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="LocationManager_groupTypeLabel" Text="Location Type"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="LocationManager_groupTypeDropDownList" runat="server" Width="130px">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="center">
                                                                                <asp:Label runat="server" ID="LocationManager_groupLabel" Text="Location Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="LocationManager_skillNameTextBox" MaxLength="50"
                                                                                    AutoCompleteType="Disabled"></asp:TextBox>
                                                                            </td>
                                                                            <td align="center">
                                                                                <asp:Label runat="server" ID="LocationManager_skillsLabel" Text="Location Alias"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="LocationManager_skillsAliasTextBox" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="LocationManager_searchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                            ToolTip="Search" OnClick="LocationManager_searchButton_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="LocationManager_searchResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="LocationManager_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="LocationManager_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="LocationManager_searchResultsUpSpan" style="display: none;" runat="server">
                                            <asp:Image ID="LocationManager_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="LocationManager_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="LocationManager_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="LocationManager_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel runat="server" ID="LocationManager_searchGridViewUpdatePanel">
                                            <ContentTemplate>
                                                <div style="height: 200px; overflow: auto;" runat="server" id="LocationManager_resultDiv">
                                                    <asp:GridView ID="LocationManager_searchGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowCommand="LocationManager_searchGridView_RowCommand"
                                                        OnRowCreated="LocationManager_searchGridView_RowCreated" OnRowDataBound="LocationManager_searchGridView_RowDataBound"
                                                        OnSorting="LocationManager_searchGridView_Sorting">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="LocationManager_locationIDHiddenField" runat="server" Value='<%# Eval("LocationID") %>' />
                                                                    <asp:HiddenField ID="LocationManager_locationTypeNameHiddenField" runat="server"
                                                                        Value='<%# Eval("LocationType") %>' />
                                                                    <asp:HiddenField ID="LocationManager_locationNameHiddenField" runat="server" Value='<%# Eval("LocationName") %>' />
                                                                    <asp:HiddenField ID="LocationManager_locationAliasNameHiddenField" runat="server"
                                                                        Value='<%# Eval("LocationAliasName") %>' />
                                                                    <asp:ImageButton ID="LocationManager_editLocationImageButton" runat="server" SkinID="sknEditDictionaryEntryImageButton"
                                                                        ToolTip="Edit Location Name" CommandName="EditLocation" CommandArgument='<%#Eval("LocationID") %>' />
                                                                    <asp:ImageButton ID="LocationManager_deleteLocationImageButton" runat="server" SkinID="sknDeleteDictionaryEntryImageButton"
                                                                        ToolTip="Delete Location" CommandName="DeleteLocation" CommandArgument='<%#Eval("LocationID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Type Name" DataField="LocationType" SortExpression="LOCATION_GROUP_NAME"
                                                                ItemStyle-Width="20%" HeaderStyle-Width="20%" />
                                                            <asp:BoundField HeaderText="Location Name" DataField="LocationName" SortExpression="LOCATION_NAME"
                                                                ItemStyle-Width="30%" HeaderStyle-Width="30%" />
                                                            <asp:BoundField HeaderText="Alias" DataField="LocationAliasName" SortExpression="LOC_NAME_ALIAS"
                                                                ItemStyle-Wrap="true" ItemStyle-Width="40%" HeaderStyle-Width="40%" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="LocationManager_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="LocationManager_bottomPagingNavigatorUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" style="padding-left: 6px; height: 15px; width: 50%">
                                                <asp:LinkButton ID="LocationManager_addLocationLinkButton" SkinID="sknAddLinkButton"
                                                    ToolTip="Add New Location" runat="server" Text="Add Location" OnClick="LocationManager_addLocationLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="right" style="width: 50%">
                                                <uc1:PageNavigator ID="LocationManager_bottomPagingNavigator" runat="server" OnPageNumberClick="LocationManager_pageNavigator_PageNumberClick" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="LocationManager_addNewLocationUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="LocationManager_addNewLocationPanel" runat="server" CssClass="popupcontrol_credit_request"
                            Style="display: none" DefaultButton="LocationManager_addNewLocationSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="LocationManager_addNewLocationHiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="LocationManager_addNewLocationLiteral" runat="server" Text="Add New Location"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="LocationManager_addNewLocationCloseImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                    <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="2">
                                                                <asp:Label ID="LocationManager_addNewLocationErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="LocationManager_addNewLocationGroupNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Location Type"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="LocationManager_addNewLocationGroupNameDropDownList" runat="server"
                                                                    Width="91%">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="LocationManager_addNewLocationLocationNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Location Name"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="LocationManager_addNewLocationLocationNameTextBox" runat="server"
                                                                    Width="89%" MaxLength="50">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="LocationManager_addNewLocationAliasLabel" runat="server" Text="Alias"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:TextBox ID="LocationManager_addNewLocationAliasTextBox" TextMode="MultiLine"
                                                                        Height="100" runat="server" Width="262px" SkinID="sknMultiLineTextBox" MaxLength="500"
                                                                        onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" TabIndex="2"> </asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="LocationManager_addNewLocationAliasHelpImageButton" SkinID="sknHelpImageButton"
                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="You can enter multiple alias name separated by comma " />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="LocationManager_addNewLocationSaveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="LocationManager_addNewLocationSaveButton_Click" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="LocationManager_addNewLocationCancelLinkButton" runat="server"
                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="LocationManager_addNewLocationModalPopupExtender"
                            runat="server" TargetControlID="LocationManager_addNewLocationHiddenButton" PopupControlID="LocationManager_addNewLocationPanel"
                            BackgroundCssClass="modalBackground" CancelControlID="LocationManager_addNewLocationCancelLinkButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="LocationManager_editLocationUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="LocationManager_editLocationPanel" runat="server" CssClass="popupcontrol_credit_request"
                            Style="display: block" DefaultButton="LocationManager_editLocation_saveButton">
                            <div style="display: none;">
                                <asp:Button ID="LocationManager_editLocationhiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="LocationManager_editLocationLiteral" runat="server" Text="Edit Location Details"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="LocationManager_editLocationtopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                    <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="LocationManager_editLocation_errorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="LocationManager_editLocation_typeNameHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:Label ID="LocationManager_editLocation_typeNameValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                    Width="110px"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:Label ID="LocationManager_editLocation_locationNameHeadLabel" runat="server"
                                                                    SkinID="sknLabelFieldHeaderText" Text="Location Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LocationManager_editLocation_locationNameValueLabel" runat="server"
                                                                    SkinID="sknLabelFieldText" Width="110px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="LocationManager_editLocation_aliasHeadLabel" runat="server" Text="Alias"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td colspan="3">
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:TextBox ID="LocationManager_editLocation_aliasTextBox" TextMode="MultiLine"
                                                                        Height="100" runat="server" Width="300px" SkinID="sknMultiLineTextBox" MaxLength="500"
                                                                        onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" TabIndex="2"> </asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="LocationManager_editLocation_aliasImageButton" SkinID="sknHelpImageButton"
                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="You can enter multiple alias name separated by comma " />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="LocationManager_editLocation_saveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="LocationManager_editLocationsaveButton_Click" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="LocationManager_editLocation_CancelLinkButton" runat="server"
                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                    <asp:HiddenField ID="LocationManager_editLocationIDHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="LocationManager_editLocationModalPopUpExtender"
                            runat="server" TargetControlID="LocationManager_editLocationhiddenButton" PopupControlID="LocationManager_editLocationPanel"
                            BackgroundCssClass="modalBackground" CancelControlID="LocationManager_editLocation_CancelLinkButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="LocationManager_deletePopup_UpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="LocationManager_deletePopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Label ID="LocationManager_deletePopup_titleLabel" runat="server" Text="Delete Location"></asp:Label>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="LocationManager_deletePopup_closeImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="msg_align">
                                                    <asp:Label ID="LocationManager_deletePopup_errorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="LocationManager_deletePopup_messageConfirmLabel" runat="server" SkinID="sknLabelFieldText"
                                                                    Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr valign="bottom">
                                                            <td align="right" style="text-align: center">
                                                                <asp:Button ID="LocationManager_deletePopup_yesValidButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Yes" OnClick="LocationManager_deletePopup_yesValidButton_Click" />
                                                                <asp:Button ID="LocationManager_deletePopup_noValidButton" runat="server" SkinID="sknButtonId"
                                                                    Text="No" OnClientClick="javascript:return false;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <div style="display: none">
                            <asp:Button ID="LocationManager_deletePopup_hiddenButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="LocationManager_deletePopupExtender" runat="server"
                            PopupControlID="LocationManager_deletePopupPanel" TargetControlID="LocationManager_deletePopup_hiddenButton"
                            BackgroundCssClass="modalBackground" CancelControlID="LocationManager_deletePopup_noValidButton"
                            DynamicServicePath="" Enabled="True">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="LocationManager_bottomUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="LocationManager_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="LocationManager_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="LocationManager_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="LocationManager_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="LocationManager_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
