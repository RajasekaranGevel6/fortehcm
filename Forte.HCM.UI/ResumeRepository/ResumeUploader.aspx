<%@ Page Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    AutoEventWireup="true" CodeBehind="ResumeUploader.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.ResumeUploader" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<asp:Content ID="ResumeUploader_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <script language="javascript" type="text/javascript">

        function ShowResumeParserMessagePopup()
        {
            // Reset parsing completed status.
            document.getElementById('<%= ResumeUploader_parsingCompletedHiddenField.ClientID %>').value = 'N';

            // Reset title, message & button visibility.
            document.getElementById('<%= ResumeUploader_resumeParserMessageTitleLabel.ClientID %>').innerHTML = "Parsing On Process";
            document.getElementById('<%= ResumeUploader_resumeParserMessageLabel.ClientID %>').innerHTML = "The resume is under parsing and will take few seconds to complete. Please wait...";

            if (document.getElementById('<%= ResumeUploader_resumeParserRotatingImageDiv.ClientID %>') != null)
                document.getElementById('<%= ResumeUploader_resumeParserRotatingImageDiv.ClientID %>').style.display = "block";

            if (document.getElementById('<%= ResumeUploader_resumeParserCloseButton.ClientID %>') != null)
                document.getElementById('<%= ResumeUploader_resumeParserCloseButton.ClientID %>').style.display = "none";

            if (document.getElementById('<%= ResumeUploader_resumeParserReviewEditResumeButton.ClientID %>') != null)
                document.getElementById('<%= ResumeUploader_resumeParserReviewEditResumeButton.ClientID %>').style.display = "none";

            if (document.getElementById('<%= ResumeUploader_resumeParserUploadAnotherResumeButton.ClientID %>') != null)
                document.getElementById('<%= ResumeUploader_resumeParserUploadAnotherResumeButton.ClientID %>').style.display = "none";

            $find("<%= ResumeUploader_resumeParserMessageModalPopupExtender.ClientID %>").show();

            ShowResumeParserRotatingImage();
            return true;
        }

        function ShowResumeParserRotatingImage()
        {
            if ($get("<%=ResumeUploader_parsingCompletedHiddenField.ClientID%>").value == 'Y')
                return;

            document.getElementById('<%= ResumeUploader_resumeParserRotatingImage.ClientID %>').src = "../App_Themes/DefaultTheme/Images/resume_parsing_process.gif";
            setTimeout("ShowResumeParserRotatingImage()", 500);
        }

        // Trim left and right side white space
        function TrimAll(str)
        {
            str = str.replace(/^\s+/, '');
            return str.replace(/\s+$/, '');
        }

        // Function that will call when candidate name search image button is clicked
        // on the resumeuploader page.
        function LoadCandidate(idctrl, namectrl, ctrlname, firstnamectrl)
        {
            var height = 550;
            var width = 880;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/SearchCandidateRepository.aspx?idctrl=" + idctrl + "&namectrl=" + namectrl + "&ctrlName=" + ctrlname + "&firstnamectrl=" + firstnamectrl + "&subctrlname=RESUME_UPLOADER";
            var retValue = window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

        function ResumeUploader_resumeUploadError(sender, args)
        {
            return false;
        }

        //To validate the resume control
        function ResumeUploader_resumeClientUploadComplete(sender, args)
        {
            var fileName = args.get_fileName();
            var fileExtn = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
            if (fileExtn != "doc" && fileExtn != "DOC" && fileExtn != "pdf" && fileExtn != "PDF" && fileExtn != "docx" && fileExtn != "DOCX")
            {
                var errMsg = "Please select valid file format.(.doc,.docx,.pdf and rtf)";
                $get("<%=ResumeUploader_topErrorMessageLabel.ClientID%>").innerHTML = errMsg;
                args.set_cancel(true);
                var who1 = document.getElementsByName('<%=ResumeUploader_resumeAsyncFileUpload.UniqueID %>')[0];
                who1.value = "";
                var who3 = who1.cloneNode(false);
                who3.onchange = who1.onchange;
                who1.parentNode.replaceChild(who3, who1);

                return false;
                alert(errMsg);
            }
            else
            {
                document.getElementById("ResumeUploader_uploadButtonDiv").style.display = "block";
            }
        }      
    </script>
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="width: 50%">
                                <asp:HiddenField ID="ResumeUploader_candidateIDHiddenField" runat="server" />
                                <asp:HiddenField ID="ResumeUploader_parsingCompletedHiddenField" runat="server" />
                                <asp:HiddenField ID="ResumeUploader_isResumePresentHiddenField" runat="server" />
                                <asp:HiddenField ID="ResumeUploader_resumeUploadedHiddenField" runat="server" />
                                <asp:Literal ID="ResumeUploader_headerLiteral" runat="server" Text="Upload Resume"></asp:Literal>
                            </td>
                            <td style="width: 50%">
                                <asp:UpdatePanel ID="ResumeUploader_topUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="2" cellspacing="0" align="right">
                                            <tr>
                                                <td>
                                                    <div id="ResumeUploader_uploadButtonDiv" style="display: none">
                                                        <asp:Button ID="ResumeUploader_topUploadButton" runat="server" SkinID="sknButtonId"
                                                            Text="Upload" Visible="true" OnClick="ResumeUploader_topUploadButton_Click" OnClientClick="ShowResumeParserMessagePopup()" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ResumeUploader_topResetButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                                        OnClick="ResumeUploader_topResetButton_Click"></asp:LinkButton>
                                                </td>
                                                <td align="center">
                                                    |
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ResumeUploader_topCancelButton" runat="server" Text="Cancel"
                                                        SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="ResumeUploader_loadDetailsButton" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="ResumeUploader_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="ResumeUploader_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ResumeUploader_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td style="height: 4px">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="ResumeUploader_candidateUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    <asp:Literal ID="ResumeUploader_selectLiteral" runat="server" Text="Select Candidate & Resume"> </asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="td_padding_top_bottom">
                                                    <table border="0" cellpadding="0" cellspacing="0" runat="server" id="ResumeUploader_candidateTable">
                                                        <tr>
                                                            <td style="width: 8%" class="td_padding_top_bottom">
                                                                <asp:Label runat="server" ID="ResumeUploader_candidateLabel" Text="Candidate" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span
                                                                    class='mandatory'>*</span>
                                                                <div style="display: none">
                                                                    <asp:Button runat="server" ID="ResumeUploader_loadDetailsButton" Text="Show Resumes"
                                                                        SkinID="sknButtonId" OnClick="ResumeUploader_loadDetailsButton_Click" />
                                                                </div>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:TextBox runat="server" ID="ResumeUploader_candidateIDTextBox" Width="99%" Text=""
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 5%">
                                                                &nbsp;
                                                                <asp:ImageButton ID="ResumeUploader_searchImageButton" SkinID="sknbtnSearchicon"
                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the candidate" />
                                                            </td>
                                                            <td style="width: 2%">
                                                                <asp:HiddenField ID="ResumeUploader_candidateNameHiddenField" runat="server" />
                                                                <asp:HiddenField ID="ResumeUploader_candidateFirstNameHiddenField" runat="server" />
                                                                <asp:HiddenField ID="ResumeUploader_candidateLastNameHiddenField" runat="server" />
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="ResumeUploader_fileNameLabel" runat="server" Text="File Name" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span
                                                                    class='mandatory'>*</span>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <ajaxToolKit:AsyncFileUpload ID="ResumeUploader_resumeAsyncFileUpload" runat="server"
                                                                    Enabled="true" Width="300" Height="24px" ThrobberID="Throbber" FailedValidation="False"
                                                                    PersistFile="false" SkinID="sknCanTextBox" OnUploadedComplete="ResumeUploader_resumeUploadedComplete"
                                                                    ClientIDMode="AutoID" OnClientUploadError="ResumeUploader_resumeUploadError"
                                                                    OnClientUploadComplete="ResumeUploader_resumeClientUploadComplete" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ResumeUploader_helpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the doc/docx/pdf/rtf/txt file that contains the resume"
                                                                    ImageAlign="Middle" />
                                                            </td>
                                                            <td align="left">
                                                                <div style="display: none">
                                                                    <asp:LinkButton ID="ResumeUploader_previewLinkButton" runat="server" OnClick="ResumeUploader_previewLinkButton_Click"
                                                                        SkinID="sknActionLinkButton" Text="Preview" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                            </td>
                                                            <td colspan="4">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 4px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    <asp:Literal ID="ResumeUploader_resumeVersions" runat="server" Text="Resume Versions"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="td_padding_top_bottom">
                                                    <div style="height: 116px; width: 100%; overflow: auto" runat="server" id="ResumeUploader_resumeVersionsGridViewDiv">
                                                        <asp:GridView ID="ResumeUploader_resumeVersionsGridView" runat="server" AllowSorting="False"
                                                            Width="100%" SkinID="sknWrapHeaderGrid" AutoGenerateColumns="False" OnRowDataBound="ResumeUploader_resumeVersionsGridView_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="30px" HeaderStyle-Wrap="true"
                                                                    ItemStyle-Width="30px" ItemStyle-Wrap="false">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="ResumeUploader_editLinkButton" runat="server" Text="Edit" ToolTip="Click here to edit the resume contents"></asp:LinkButton>
                                                                        <asp:HiddenField ID="ResumeUploader_resumeVersionsGridView_candidateResumeIDHiddenField"
                                                                            Value='<%# Eval("CandidateResumeID") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Profile Name" HeaderStyle-Width="140px" HeaderStyle-Wrap="false"
                                                                    ItemStyle-Width="140px" ItemStyle-Wrap="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="ResumeUploader_resumeVersionsGridView_profileNameLabel" runat="server"
                                                                            Text='<%# Eval("ProfileName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Uploaded Date" HeaderStyle-Width="110px" HeaderStyle-Wrap="false"
                                                                    ItemStyle-Width="110px" ItemStyle-Wrap="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="ResumeUploader_resumeVersionsGridView_uploadedDateLabel" runat="server"
                                                                            Text='<%# GetDateFormat(Convert.ToDateTime(Eval("UploadedDate"))) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 4px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="width: 100%; height: 210px; display: none;" runat="server" id="ResumeUploader_bottomPaneDIV">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td class="header_bg">
                                                        <asp:Literal ID="ResumeUploader_previewHeaderLiteral" runat="server" Text="Preview"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="grid_body_bg" style="width: 20%">
                                                        <div runat="server" id="ResumeUploader_previewHtmlDiv" style="height: 180px; overflow: auto">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ResumeUploader_previewLinkButton" />
                            <asp:PostBackTrigger ControlID="ResumeUploader_topUploadButton" />
                            <asp:PostBackTrigger ControlID="ResumeUploader_loadDetailsButton" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td class="header_bg">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td class="header_text_bold">
                                            <asp:Literal ID="ResumeUploader_downloadLiteral" runat="server" Text="Download Resume Uploader Utility "> </asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="grid_body_bg">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td class="td_padding_top_bottom">
                                            <table border="0" cellpadding="0" cellspacing="0" runat="server">
                                                <tr>
                                                    <td style="width: 8%" class="label_multi_field_text">
                                                        <asp:Label runat="server" ID="ResumeUploader_downloadResumeUploaderUtilityInstructions"
                                                            Text="You can download the Resume Uploader utility to upload bulk resumes. This is limited based on your subscription plan"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label_multi_field_text" align="left" runat="server" visible="true">
                                                        Click
                                                        <asp:LinkButton ID="ResumeUploader_downloadResumeUploaderUtilityLinkButton" runat="server"
                                                            Text="here" OnClick="ResumeUploader_downloadResumeUploaderUtilityLinkButton_Click"></asp:LinkButton>&nbsp;to
                                                        download the resume uploader utility. The download zip file consist of the resume
                                                        uploader utility and instructions.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" align="center">
                    <asp:UpdatePanel ID="ResumeUploader_resumeParserMessageUpdatePanel" runat="server"
                        UpdateMode="Always">
                        <ContentTemplate>
                            <div style="display: none">
                                <asp:Button ID="ResumeUploader_resumeParserMessageHiddenButton" runat="server" />
                            </div>
                            <asp:Panel ID="ResumeUploader_resumeParserMessagePanel" runat="server" Style="display: none"
                                CssClass="popupcontrol_test_recommendation_change_test_status">
                                <div id="ResumeUploader_resumeParserMessageDiv" style="display: block; height: 190px;
                                    width: 580px;" class="popupcontrol_confirm">
                                    <div style="width: 548px; padding-left: 14px; float: left; padding-top: 14px">
                                        <div style="width: 524px; float: left; height: 134px; padding-left: 14px; padding-top: 14px;
                                            padding-right: 14px; padding-bottom: 14px;" class="resume_parser_status_inner_bg">
                                            <div style="text-align: center">
                                                <asp:Label ID="ResumeUploader_resumeParserMessageTitleLabel" runat="server" Text="Parsing On Process"
                                                    CssClass="resume_parser_status_message_title_label"></asp:Label>
                                            </div>
                                            <div style="height: 10px">
                                            </div>
                                            <div style="text-align: center">
                                                <asp:Label ID="ResumeUploader_resumeParserMessageLabel" runat="server" Text="The resume is under parsing and will take few seconds to complete. Please wait..."
                                                    CssClass="resume_parser_status_message_label"></asp:Label>
                                            </div>
                                            <div style="height: 10px">
                                            </div>
                                            <div style="text-align: center; display: block" runat="server" id="ResumeUploader_resumeParserRotatingImageDiv">
                                                <asp:Image ID="ResumeUploader_resumeParserRotatingImage" ImageUrl="~/App_Themes/DefaultTheme/Images/resume_parsing_process.gif"
                                                    runat="server" ToolTip="Please wait ..." />
                                            </div>
                                            <div style="text-align: center">
                                                <asp:Button ID="ResumeUploader_resumeParserCloseButton" runat="server" Text="Close"
                                                    SkinID="sknButtonBlue" Visible="false" Height="26px" Width="120px" ToolTip="Click here to close" />
                                                <asp:Button ID="ResumeUploader_resumeParserReviewEditResumeButton" runat="server"
                                                    Text="Review & Edit Resume" SkinID="sknButtonOrange" Visible="false" Height="26px"
                                                    Width="150px" OnClick="ResumeUploader_resumeParserReviewEditResumeButton_Click"
                                                    ToolTip="Click here to review & edit resume" />&nbsp;
                                                <asp:Button ID="ResumeUploader_resumeParserUploadAnotherResumeButton" runat="server"
                                                    Text="Upload Another Resume" SkinID="sknButtonBlue" Visible="false" Height="26px"
                                                    Width="150px" OnClick="ResumeUploader_resumeParserUploadAnotherResumeButton_Click"
                                                    ToolTip="Click here to upload another resume" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="ResumeUploader_resumeParserMessageModalPopupExtender"
                                runat="server" PopupControlID="ResumeUploader_resumeParserMessagePanel" TargetControlID="ResumeUploader_resumeParserMessageHiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
