﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    CodeBehind="CreateCandidate.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.CreateCandidate" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="CreateCandidate_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <script type="text/javascript" language="javascript">

        // Show or hide other text based on work authorization status.
        function ShowOtherTextBox(dropdown, div, textBoxID) {
            if (document.getElementById(dropdown).value == 'WAS_OTHER') {
                document.getElementById(div).style["display"] = "block";
                document.getElementById(textBoxID).focus();
                document.getElementById(textBoxID).value = '';
            }
            else {
                document.getElementById(div).style["display"] = "none";
                document.getElementById(textBoxID).value = '';
            }

            return false;
        }

        //To validate the resume control
        function CreateCandidate_resumeClientUploadComplete(sender, args) {
            var fileName = args.get_fileName();
            var fileExtn = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
            if (fileExtn != "doc" && fileExtn != "DOC" && fileExtn != "pdf" && fileExtn != "PDF" && fileExtn != "docx" && fileExtn != "DOCX") {
                var errMsg = "Please select valid file format.(.doc,.docx,.pdf and rtf)";
                $get("<%=CreateCandidate_topErrorMessageLabel.ClientID%>").innerHTML = errMsg;
                args.set_cancel(true);
                var who1 = document.getElementsByName('<%=CreateCandidate_resumeAsyncFileUpload.UniqueID %>')[0];
                who1.value = "";
                var who3 = who1.cloneNode(false);
                who3.onchange = who1.onchange;
                who1.parentNode.replaceChild(who3, who1);

                return false;
                alert(errMsg);
            }
        }

        //To validate the photo control
        function CreateCandidate_photoClientUploadComplete(sender, args) {
            var handlerPage = '<%= Page.ResolveClientUrl("~/Common/CandidateImageHandler.ashx")%>';
            var queryString = '?source=CAND_CREA_PAGE&randomno=' + getRandomNumber();
            var src = handlerPage + queryString;
            var clientId = '<%=CreateCandidate_candidateImage.ClientID %>';
            document.getElementById(clientId).setAttribute("src", src);
            var clearButton = document.getElementById("<%=CreateCandidate_selectPhotoClearLinkButton.ClientID%>");
        }

        function getRandomNumber() {
            var randomnumber = Math.random(10000);
            return randomnumber;
        }    
        function CreateCandidate_resumeUploadError(sender, args) {
            return false;
        }

        function CreateCandidate_photoUploadError(sender, args) {
            return false;
        }

        $(document).ready(function () {
            // Handler method for 'Change Status' link button click.
            $("#<%= CreateCandidate_changeUserNameLinkButton.ClientID %>").click(function () {
                $("#CreateCandidate_userNameLabelDiv").hide();
                $("#CreateCandidate_useTextBoxDiv").show();

                return false;
            });

            // Handler method for 'Cancel' link button click (save status cancellation).
            $("#<%= CreateCandidate_userNameCancelLinkButton.ClientID %>").click(function () {
                $("#CreateCandidate_useTextBoxDiv").hide();
                $("#CreateCandidate_userNameLabelDiv").show();

                return false;
            });

        });
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CreateCandidate_headerLiteral" runat="server" Text="New Candidate"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="CreateCandidate_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="CreateCandidate_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CreateCandidate_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CreateCandidate_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CreateCandidate_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateCandidate_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateCandidate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="CreateCandidate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CreateCandidate_candidateDetailsLiteral" runat="server" Text="Candidate Details"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <asp:UpdatePanel ID="CreateCandidate_bodyUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellpadding="2" cellspacing="3">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <div id="CreateCandidate_userContainerDiv" runat="server" style="width: auto; float: left; padding-right: 4px;display: block;">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="width: 115px">
                                                <asp:Label ID="CreateCandidate_userNameLabel" runat="server" Text="User Name" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span
                                                    class="mandatory">*</span>
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td>
                                                <div id="CreateCandidate_userNameLabelDiv" style="width: auto; float: left; padding-right: 4px;
                                                    display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="CreateCandidate_userNameValueLabel" runat="server" Text="" SkinID="sknLabelText"></asp:Label>
                                                            </td>
                                                            <td style="padding-left: 6px">
                                                                <asp:LinkButton ID="CreateCandidate_changeUserNameLinkButton" runat="server" Text="Edit User Name"
                                                                    ToolTip="Click here to change user name" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="CreateCandidate_useTextBoxDiv" style="width: auto; float: left; padding-right: 4px;
                                                    display: none;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="CreateCandidate_userNameTextBox" runat="server" MaxLength="50" Width="200px"
                                                                    TabIndex="1"></asp:TextBox>
                                                                <asp:HiddenField ID="CreateCandidate_userIDHiddenField" runat="server" />
                                                            </td>
                                                            <td style="padding-left: 6px">
                                                                <asp:Button ID="CreateCandidate_userNameSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                                    OnClick="CreateCandidate_userNameSaveButton_Click" ToolTip="Click here to save user name" />
                                                            </td>
                                                            <td style="padding-left: 6px">
                                                                <asp:LinkButton ID="CreateCandidate_userNameCancelLinkButton" runat="server" Text="Cancel"
                                                                    ToolTip="Click here to cancel saving user name" CssClass="position_profile_summary_profile_cancel_link"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 7%">
                                    <asp:Label ID="CreateCandidate_firstNameHeaderLabel" runat="server" Text="First Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td style="width: 20%">
                                    <asp:TextBox ID="CreateCandidate_firstNameTextBox" runat="server" MaxLength="50"
                                        Width="200px" TabIndex="1"></asp:TextBox>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td style="width: 7%">
                                    <asp:Label ID="CreateCandidate_addressLabel" runat="server" Text="Address" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td rowspan="3" valign="top" style="width: 12%">
                                    <asp:TextBox ID="CreateCandidate_addressTextBox" runat="server" TextMode="MultiLine"
                                        Columns="35" Height="70" MaxLength="100" SkinID="sknMultiLineTextBox" onkeyup="CommentsCount(100,this)"
                                        onchange="CommentsCount(100,this)" TabIndex="8"></asp:TextBox>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td style="width: 20%" rowspan="7" class="candidate_photo" align="center" valign="middle">
                                    <asp:UpdatePanel ID="CreateCandidate_previewPhotoUpdatePanel" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="CreateCandidate_candidateImage" runat="server" 
                                                ImageUrl="~/Common/CandidateImageHandler.ashx?source=CAND_CREA_PAGE" AlternateText="Photo not available" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="CreateCandidate_photoAsyncFileUpload" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 7%">
                                    <div style="float: left">
                                        <asp:Label ID="CreateCandidate_middleNameLabel" runat="server" Text="Middle Name"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div style="float: right">
                                    </div>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td>
                                    <asp:TextBox ID="CreateCandidate_middleNameTextBox" runat="server" MaxLength="50"
                                        Width="200px" TabIndex="2"></asp:TextBox>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td>
                                </td>
                                <td style="width: 1%">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 7%">
                                    <asp:Label ID="CreateCandidate_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span
                                        class="mandatory">*</span>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td>
                                    <asp:TextBox ID="CreateCandidate_lastNameTextBox" runat="server" MaxLength="50" Width="200px"
                                        TabIndex="3"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 1%">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 7%">
                                    <asp:Label ID="CreateCandidate_emailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span
                                        class="mandatory">*</span>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td>
                                    <asp:TextBox ID="CreateCandidate_emailTextBox" runat="server" MaxLength="100" Width="200px"
                                        TabIndex="4"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="CreateCandidate_homephoneLabel" runat="server" Text="Home Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="CreateCandidate_homephoneTextBox" runat="server" MaxLength="20"
                                        Width="200px" TabIndex="9"></asp:TextBox>
                                </td>
                                <td style="width: 1%">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 7%">
                                    <div style="float: left; width: 45%">
                                        <asp:Label ID="CreateCandidate_genderLabel" runat="server" Text="Gender" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div style="float: right; width: 55%">
                                    </div>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td>
                                    <asp:DropDownList ID="CreateCandidate_genderDropDownList" runat="server" Width="120px"
                                        TabIndex="5">
                                        <asp:ListItem Text="--Select--" Value="0">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Male" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Female" Value="2">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td>
                                    <asp:Label ID="CreateCandidate_cellPhoneLabel" runat="server" Text="Cell Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="CreateCandidate_cellPhoneTextBox" runat="server" MaxLength="20"
                                        Width="200px" TabIndex="10"></asp:TextBox>
                                </td>
                                <td style="width: 1%">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 7%">
                                    <div style="float: left; width: 45%">
                                        <asp:Label ID="CreateCandidate_dateOfBirthLabel" runat="server" Text="Date Of Birth"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div style="float: right; width: 55%">
                                    </div>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td style="width: 7%">
                                    <div style="float: left; padding-right: 5px; width: 48%">
                                        <asp:TextBox ID="CreateCandidate_dateOfBirthTextBox" runat="server" MaxLength="100"
                                            Width="94px" ReadOnly="true" TabIndex="6"></asp:TextBox>
                                    </div>
                                    <div style="float: left; width: 5%">
                                        <asp:ImageButton ID="CreateCandidate_dateOfBirthLinkButton" runat="server" SkinID="sknCalendarImageButton"
                                            ImageAlign="Middle" />
                                    </div>
                                    <ajaxToolKit:CalendarExtender ID="CreateCandidate_dateOfBirthCalenderExtender" runat="server"
                                        TargetControlID="CreateCandidate_dateOfBirthTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy"
                                        PopupPosition="BottomLeft" PopupButtonID="CreateCandidate_dateOfBirthLinkButton" />
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td>
                                    <asp:Label ID="CreateCandidate_typeLabel" runat="server" Text="Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="CreateCandidate_candidateRadioButton" runat="server" GroupName="type"
                                                    Checked="true" />
                                                <asp:Label ID="CreateCandidate_candidateLabel" runat="server" Text="Candidate" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="CreateCandidate_employeeRadioButton" runat="server" GroupName="type" />
                                                <asp:Label ID="CreateCandidate_employeeLabel" runat="server" Text="Employee" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 1%">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="float: left; width: 45%">
                                        <asp:Label ID="CreateCandidate_workAuthorizationStatusLabel" runat="server" Text="Work Authorization"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div style="float: right; width: 55%">
                                    </div>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <div style="float: left; width: 60%">
                                        <asp:DropDownList ID="CreateCandidate_workAuthorizationStatusDropDownList" runat="server"
                                            Width="140px" TabIndex="7">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="CreateCandidate_workAuthorizationStatus_otherDIV" runat="server" style="float: left;
                                        display: none;">
                                        <span class="mandatory">*</span>&nbsp;<asp:TextBox ID="CreateCandidate_workAuthorizationStatus_otherTextBox"
                                            runat="server" MaxLength="50" Width="70px"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="CreateCandidate_limitedAccessLabel" runat="server" Text="Limited Access"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="CreateCandidate_limitedAccessCheckBox" Checked="true" />
                                </td>
                                <td style="width: 1%">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="CreateCandidate_LocationLabel" runat="server" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox ID="CreateCandidate_LocationTextBox" ReadOnly="true" runat="server"
                                        Width="200px"></asp:TextBox>
                                    <asp:ImageButton ID="CreateCandidate_locationImageButton" SkinID="sknbtnSearchicon"
                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select city, state, country" />
                                    <asp:HiddenField ID="CreateCandidate_cityIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="CreateCandidate_stateIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="CreateCandidate_countryIDHiddenField" runat="server" />
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="CreateCandidate_IsactiveLabel" runat="server" Text="Is Active" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CreateCandidate_IsactiveCheckbox" runat="server" Checked="true" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td><asp:Label ID="CreateCandidate_linkedInProfileLabel" runat="server" Text="LinkedID Profile" SkinID="sknLabelFieldHeaderText"></asp:Label></td>
                                <td></td>
                                <td colspan="5"><asp:TextBox ID="CreateCandidate_linkedInProfileTextBox" ReadOnly="false" runat="server"
                                        Width="575px"></asp:TextBox></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr style="display:none">
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CreateCandidate_uploadResumeAndPhotoLiteral" runat="server" Text="Upload Resume & Photo"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="display:none;">
            <td class="grid_body_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="td_padding_top_bottom">
                            <table border="0" cellpadding="0" cellspacing="2" runat="server" width="100%" id="CreateCandidate_candidateTable">
                                <tr>
                                    <td style="width: 8%">
                                        <asp:Label ID="CreateCandidate_selectResumeLabel" runat="server" Text="Select Resume"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 2%">
                                    </td>
                                    <td style="width: 25%" align="right">
                                        <ajaxToolKit:AsyncFileUpload ID="CreateCandidate_resumeAsyncFileUpload" runat="server"
                                            Width="300" Height="24px" ThrobberID="Throbber" FailedValidation="False" PersistFile="false"
                                            SkinID="sknCanTextBox" OnUploadedComplete="CreateCandidate_resumeUploadedComplete"
                                            ClientIDMode="AutoID" OnClientUploadError="CreateCandidate_resumeUploadError"
                                            OnClientUploadComplete="CreateCandidate_resumeClientUploadComplete" />
                                    </td>
                                    <td align="left" style="width: 1%">
                                        <asp:ImageButton ID="CreateCandidate_selectResumeHelpImageButton" SkinID="sknHelpImageButton"
                                            runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the doc/docx/pdf/rtf file that contains the resume"
                                            ImageAlign="Middle" />
                                    </td>
                                    <td style="width: 25%">
                                    </td>
                                    <td style="width: 8%">
                                        <asp:Label ID="CreateCandidate_selectPhotoLabel" runat="server" Text="Select Photo"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 2%">
                                    </td>
                                    <td style="width: 25%" align="right">
                                        <asp:UpdatePanel ID="CreateCandidate_photoUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <ajaxToolKit:AsyncFileUpload ID="CreateCandidate_photoAsyncFileUpload" runat="server"
                                                    Width="300" Height="24px" ThrobberID="Throbber" FailedValidation="False" PersistFile="false"
                                                    SkinID="sknCanTextBox" OnUploadedComplete="CreateCandidate_photoUploadedComplete"
                                                    ClientIDMode="AutoID" OnClientUploadError="CreateCandidate_photoUploadError"
                                                    OnClientUploadComplete="CreateCandidate_photoClientUploadComplete" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td align="left" style="width: 1%">
                                        <asp:ImageButton ID="CreateCandidate_selectPhotoHelpImageButton" SkinID="sknHelpImageButton"
                                            runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the gif/png/jpg file that contains the photo"
                                            ImageAlign="Middle" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="CreateCandidate_selectPhotoClearLinkButton" runat="server"
                                            Visible="false" SkinID="sknActionLinkButton" Text="Clear" ToolTip="Click here to clear the photo"
                                            OnClick="CreateCandidate_selectPhotoClearLinkButton_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <%--<asp:RegularExpressionValidator ID="CreateCandidate_selectResumeRegularExpressionValidataor"
                                            runat="server" ControlToValidate="CreateCandidate_resumeAsyncFileUpload" ErrorMessage="Only doc/docx/pdf/rtf files are allowed"
                                            ValidationExpression="^([a-zA-Z].*|[0-9].*)\.(((p|P)(d|D)(f|F))|((r|R)(t|T)(f|F))|((d|D)(o|O)(c|C))|((d|D)(o|O)(c|C)(x|X)))$"> 
                                         </asp:RegularExpressionValidator>--%>
                                    </td>
                                    <td style="width: 2%">
                                    </td>
                                    <td style="width: 2%" colspan="3">
                                        <%--<asp:RegularExpressionValidator ID="CreateCandidate_selectPhotoRegularExpressionValidataor"
                                            runat="server" ControlToValidate="CreateCandidate_photoAsyncFileUpload" ErrorMessage="Only gif/png/jpg/jpeg files are allowed"
                                            ValidationExpression="^([a-zA-Z].*|[0-9].*)\.(((g|F)(i|I)(f|F))|((p|P)(n|N)(g|G))|((j|J)(p|P)(g|G))|((j|J)(p|P)(e|E)(g|G)))$"> </asp:RegularExpressionValidator>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <!-- Show popup extender -->
        <tr>
            <td>
               <asp:UpdatePanel ID="CreateCandidate_candidatePopupUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="CreateCandidate_schedulePanel" runat="server" CssClass="popupcontrol_duplicate_candidate"
                            Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="CreateCandidate_hiddenButton" runat="server" Text="Hidden" />
                            </div>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" class="popup_header_text" style="width: 50%" valign="middle">
                                                    <asp:Literal ID="CreateCandidate_questionResultLiteral" runat="server" Text="Review & Confirm Candidate Creation"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table align="right" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="CreateCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="0" class="popupcontrol_question_inner_bg"
                                                        width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div id="CreateCandidate_enteredDiv" runat="server">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="panel_bg">
                                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                <tr id="Tr1" runat="server">
                                                                                                    <td class="header_bg">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                                                    <asp:Literal ID="CreateCandidate_enteredLiteral" runat="server" Text="You entered"></asp:Literal>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="grid_body_bg">
                                                                                                        <table width="100%" cellpadding="2" cellspacing="3">
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <asp:Label ID="CreateCandidate_candidateFirstNameLabel" runat="server" 
                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="First Name"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:Label ID="CreateCandidate_candidateFirstNameLabelValue" SkinID="sknLabelFieldText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 10%">
                                                                                                                    <asp:Label ID="CreateCandidate_candidateLastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Last Name"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:Label ID="CreateCandidate_candidateLastNameLabelValue" SkinID="sknLabelFieldText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 10%">
                                                                                                                    <asp:Label ID="CreateCandidate_candidateEmailLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Email"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 35%">
                                                                                                                    <asp:Label ID="CreateCandidate_candidateEmailLabelValue" SkinID="sknLabelFieldText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="CreateCandidate_candidateGridViewUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="CreateCandidate_candidateGridViewDIV" runat="server">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">

                                                                                            <tr>
                                                                                                <td class="panel_bg">
                                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                        <tr id="CreateCandidate_candidateGridViewHeaderTR" runat="server">
                                                                                                            <td class="header_bg">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                                                                                            <asp:Literal ID="CreateCandidate_resultsLiteral" runat="server" Text="Following list of duplicate candidate(s) are identified"></asp:Literal>&nbsp;<asp:Label
                                                                                                                                ID="CreateCandidate_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="You can anyway create the candidate or cancel this"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="grid_body_bg">
                                                                                                                <div id="CreateCandidate_candidateDetailDIV" runat="server" style="height: 220px;
                                                                                                                    overflow: auto;">
                                                                                                                    <asp:GridView ID="CreateCandidate_candidateDetailGridView" runat="server" AllowSorting="true"
                                                                                                                        AutoGenerateColumns="false" OnRowDataBound="CreateCandidate_candidateDetailGridView_RowDataBound">
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateField HeaderText="First Name" HeaderStyle-Width="14%" ItemStyle-Width="14%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="CreateCandidate_firstNameGridViewLabel" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Last Name" ItemStyle-Width="14%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="14%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="CreateCandidate_lastNameGridViewLabel" runat="server" Text='<%# Eval("LastName") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Email" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="CreateCandidate_emailGridViewLabel" runat="server" Text='<%# Eval("EMailID") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Reason" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="CreateCandidate_reasonGridViewLabel" runat="server" Text='<%# Eval("Reason")==null ? Eval("Reason") : Eval("Reason").ToString().Replace(".", "<br />") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Uploaded By" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="CreateCandidate_uploadedByGridViewLabel" runat="server" Text='<%# Eval("UploadedByName") %>'></asp:Label>
                                                                                                                                    <asp:HiddenField ID="CreateCandidate_candidateIDHiddenField" runat="server" Value='<%# Eval("CandidateID") %>' />
                                                                                                                                    <asp:HiddenField ID="CreateCandidate_candidateResumeIDHiddenField" runat="server"
                                                                                                                                        Value='<%# Eval("CandidateResumeID") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Uploaded Date" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="CreateCandidate_uploadedDateGridViewLabel" runat="server" Text='<%# Eval("UploadedDate") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                        </Columns>
                                                                                                                    </asp:GridView>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                          
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-left: 5px">
                                                    <asp:Button ID="CreateCandidate_newCandidateButton" runat="server" OnClick="CreateCandidate_newCandidateButton_Click"
                                                        SkinID="sknButtonId" Text="Create Anyway" />
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <asp:LinkButton ID="CreateCandidate_bottomCloseLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                                        Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CreateCandidate_scheduleModalPopupExtender" runat="server"
                            BackgroundCssClass="modalBackground" PopupControlID="CreateCandidate_schedulePanel"
                            TargetControlID="CreateCandidate_hiddenButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="CreateCandidate_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>

        <!-- End show popup extender-->
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateCandidate_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateCandidate_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="CreateCandidate_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                       <asp:UpdatePanel ID="CreateCandidate_bottomSaveUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="CreateCandidate_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                    OnClick="CreateCandidate_saveButton_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CreateCandidate_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CreateCandidate_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CreateCandidate_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
