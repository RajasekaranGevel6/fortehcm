﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    AutoEventWireup="true" CodeBehind="ReviewResume.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.ReviewResume" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ResumeDeleteConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="Uploaded_resumeStatusContent" ContentPlaceHolderID="ResumeRepositoryMaster_body"
    runat="server">
    <script type="text/javascript">
        function DownloadTemporaryResume(candidateID) {
            var url = '../Common/Download.aspx?candidateid=' + candidateID + '&type=TEMPORARYRESUME';

            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');
            return false;
        }

        function OpenSkillsMatrix(candidateid, type) {
            var height = 700;
            var width = 850;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/SkillsMatrix.aspx?&CandID=" + candidateid + "&Type=" + type;
            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }
        function LoadUserForTenant(ctrlId, ctrlHidID, labelCtrl) {
            var height = 530;
            var width = 750;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/SearchUser.aspx?ctrlid=" + ctrlId + "&ctrlhidId=" + ctrlHidID + "&ctrlFirstNameid=" + labelCtrl + "&home=REVIEW_RESUME";
            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }
    </script>
    <asp:UpdatePanel ID="UploadedResumeStatus_mainUpdatePanel" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="UploadedResumeStatus_topMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="UploadedResumeStatus_topErrorLabel" runat="server" Text="" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="UploadedResumeStatus_topSuccessLabel" runat="server" Text="" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="UploadedResumeStatus_resumeStatusSearchButton" />
                                <asp:AsyncPostBackTrigger ControlID="UploadedResumeStatus_pendingResumeDataList" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="left" width="91%">
                                    <asp:Label ID="CandidateDashboard_titleLabel" runat="server" CssClass="header_text_bold"
                                        Text="Review Resume"></asp:Label>
                                </td>
                                <td width="3%">
                                    <asp:LinkButton ID="CandidateDashboard_resetTopLinkButton" runat="server" SkinID="sknActionLinkButton"
                                        OnClick="CandidateDashboard_resetTopLinkButton_Click">Reset</asp:LinkButton>
                                </td>
                                <td align="center" class="link_button" width="3%">
                                    |
                                </td>
                                <td width="3%">
                                    <asp:LinkButton ID="CandidateDashboard_cancelTopLinkButton" runat="server" SkinID="sknActionLinkButton"
                                        OnClick="ParentPageRedirect">Cancel</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td class="panel_inner_body_bg">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td class="header_text_bold">
                                    <%--<asp:Label ID="UploadedResumeStatus_searchTypeLabel" runat="server" Text="Search by"></asp:Label>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="panel_bg">
                                    <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                        <tr>
                                            <td class="panel_inner_body_bg">
                                                <table cellpadding="2" cellspacing="3" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="10%">
                                                                        <asp:Label ID="UploadedResumeStatus_profileNameSearchLabel" runat="server" Text="Profile Name "></asp:Label>
                                                                    </td>
                                                                    <td width="20%">
                                                                        <asp:TextBox ID="UploadedResumeStatus_profileNameSearchTextBox" runat="server" CssClass="text_box"
                                                                            Width="175px"></asp:TextBox>
                                                                    </td>
                                                                    <td width="10%">
                                                                        <asp:Label ID="UploadedResumeStatus_recruitedBySearchLabel" runat="server" Text="Uploaded By "></asp:Label>
                                                                    </td>
                                                                    <td width="20%">
                                                                        <asp:TextBox ID="UploadedResumeStatus_recruitedBySearchTextBox" runat="server" CssClass="text_box"
                                                                            Width="175px" ReadOnly="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="ReviewResume_uploadedByDummyIdHidenField" runat="server" />
                                                                        <asp:HiddenField ID="ReviewResume_recruitedIDHiddenField" runat="server" />
                                                                    </td>
                                                                    <td width="10%">
                                                                        <asp:ImageButton ID="UploadedResumeStatus_recruiterSearchImageButton" runat="server"
                                                                            ImageUrl="~/App_Themes/DefaultTheme/Images/can_search_icon.gif" ToolTip="Click here to select the uploader" />
                                                                    </td>
                                                                    <td width="5%">
                                                                        <asp:Label ID="UploadedResumeStatus_statusSearchLabel" runat="server" Text="Status "></asp:Label>
                                                                    </td>
                                                                    <td width="20%">
                                                                        <asp:DropDownList ID="UploadedResumeStatus_statusDropDownList" runat="server" SkinID="sknSubjectDropDown"
                                                                            Width="175px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="12%">
                                                                        <asp:Label ID="UploadedResumeStatus_updateDateFromLabel" runat="server" Text="Uploaded Date From "></asp:Label>
                                                                    </td>
                                                                    <td width="12%">
                                                                        <asp:TextBox ID="UploadedResumeStatus_updateDateFromTextBox" runat="server" CssClass="text_box"
                                                                            Width="100px"></asp:TextBox>
                                                                    </td>
                                                                    <td width="2%">
                                                                        <asp:ImageButton ID="UploadedResumeStatus_updateDateFromImageButton" runat="server"
                                                                            ImageAlign="Middle" SkinID="sknCalendarImageButton" Width="16px" />
                                                                        <ajaxToolKit:CalendarExtender ID="UploadedResumeStatus_updateDateFromCalenderExtender"
                                                                            runat="server" TargetControlID="UploadedResumeStatus_updateDateFromTextBox" CssClass="MyCalendar"
                                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="UploadedResumeStatus_updateDateFromImageButton" />
                                                                    </td>
                                                                    <td align="left" width="14%" style="padding-left: 39px">
                                                                        <asp:Label ID="UploadedResumeStatus_updateDateToLabel" runat="server" Text="To "></asp:Label>
                                                                    </td>
                                                                    <td width="12%">
                                                                        <asp:TextBox ID="UploadedResumeStatus_updateDateToTextBox" runat="server" CssClass="text_box"
                                                                            Width="100px"></asp:TextBox>
                                                                    </td>
                                                                    <td width="4%">
                                                                        <asp:ImageButton ID="UploadedResumeStatus_updateDateToImageButton" runat="server"
                                                                            ImageAlign="Middle" SkinID="sknCalendarImageButton" Width="16px" />
                                                                        <ajaxToolKit:CalendarExtender ID="UploadedResumeStatus_updateDateToCalenderExtender"
                                                                            runat="server" TargetControlID="UploadedResumeStatus_updateDateToTextBox" CssClass="MyCalendar"
                                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="UploadedResumeStatus_updateDateToImageButton" />
                                                                    </td>
                                                                    <td width="15%">
                                                                        <asp:Button ID="UploadedResumeStatus_resumeStatusSearchButton" runat="server" SkinID="sknButtonId"
                                                                            Text="Search" OnClick="UploadedResumeStatus_resumeStatusSearchButton_Click" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DataList ID="UploadedResumeStatus_pendingResumeDataList" runat="server" GridLines="Both"
                                        RepeatColumns="1" ShowFooter="False" ShowHeader="False" Width="100%" CellPadding="10"
                                        CellSpacing="10" OnItemDataBound="UploadedResumeStatus_pendingResumeDataList_ItemDataBound"
                                        OnItemCommand="UploadedResumeStatus_pendingResumeDataList_ItemCommand">
                                        <ItemTemplate>
                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td width="60%" style="padding-left: 8px">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td class="Question_Rating_title">
                                                                    <asp:Label ID="UploadedResumeStatus_resumeNameLabel" runat="server"></asp:Label>
                                                                    <asp:HiddenField ID="UploadedResumeStatus_candidateIDHiddenField" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="UploadedResumeStatus_lastUpdateDateLabel" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="UploadedResumeStatus_recruitedByLabel" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td width="35%" class="label_assessor_field_text">
                                                                                <asp:Label ID="ReviewResume_statusLabel" runat="server" ForeColor="#FF5400"></asp:Label>
                                                                            </td>
                                                                            <td width="1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="2%">
                                                                                <asp:ImageButton ID="ReviewResume_downloadResumeImageButton" runat="server" ToolTip="Click here to download the resume"
                                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/icon_word.gif" CommandName="DownloadResume" />
                                                                            </td>
                                                                            <td width="1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="2%">
                                                                                <asp:ImageButton ID="ReviewResume_candidateSkillImageButton" runat="server" ToolTip="Click here to view candidate skills"
                                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/can_skill.gif" CommandName="CandidateSkill" />
                                                                            </td>
                                                                            <td width="1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="2%">
                                                                                <asp:HyperLink ID="ReviewResume_candidateDashboardHyperLink" ToolTip="Click here to go candidate dashboard"
                                                                                    runat="server" Target="_self" ImageUrl="~/App_Themes/DefaultTheme/Images/can_dashboard.gif"
                                                                                    CommandName="Dashboard" />
                                                                            </td>
                                                                            <td width="1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="2%">
                                                                                <asp:ImageButton ID="ReviewResume_resumeActiveImageButton" ToolTip="Click here to active"
                                                                                    runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_inactive.gif"
                                                                                    CommandName="Status" />
                                                                            </td>
                                                                            <td width="1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="2%">
                                                                                <asp:HyperLink ID="ReviewResume_emailImageButton" ToolTip="Click here to send email to recruiter"
                                                                                    runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_mail.gif" />
                                                                            </td>
                                                                            <td width="1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="2%">
                                                                                <asp:ImageButton ID="ReviewResume_deleteImageButton" ToolTip="Click here to delete the resume"
                                                                                    runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/delete_position_profile.gif"
                                                                                    CommandName="Delete" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="40%" valign="top">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    <asp:Label ID="UploadedResumeStatus_reasonTitleLabel" runat="server" Text="Reason"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="label_assessor_field_text" style="padding-top: 5px">
                                                                    <div style="width: 300px; overflow: auto; word-wrap: break-word; white-space: normal"
                                                                        runat="server" id="UploadedResumeStatus_reasonDiv">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle BorderStyle="Solid" BorderWidth="1px" BorderColor="#D8D8D8" BackColor="#ffffff" />
                                    </asp:DataList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="ReviewResume_deleteResumeHiddenField" runat="server" />
                                    <asp:Panel ID="ReviewResume_deleteResumePopupPanel" runat="server" Style="display: none"
                                        CssClass="client_confirm_message_box">
                                        <uc2:ResumeDeleteConfirmMsgControl ID="ReviewResume_deleteResumeConfirmMsgControl"
                                            runat="server" OnCancelClick="DuplicateResumeManagement_newCandidateConfirmMsgControl_cancelClick"
                                            OnOkClick="DuplicateResumeManagement_newCandidateConfirmMsgControl_okClick" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ReviewResume_deleteResumeModalPopupExtender"
                                        runat="server" PopupControlID="ReviewResume_deleteResumePopupPanel" TargetControlID="ReviewResume_deleteResumeHiddenField"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:PageNavigator ID="ReviewResume_pageNavigator" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="UploadedResumeStatus_bottomUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="UploadedResumeStatus_bottomErrorLabel" runat="server" Text="" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="UploadedResumeStatus_bottomSuccessLabel" runat="server" Text="" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="UploadedResumeStatus_resumeStatusSearchButton" />
                                <asp:AsyncPostBackTrigger ControlID="UploadedResumeStatus_pendingResumeDataList" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="left" width="91%">
                                    &nbsp;
                                </td>
                                <td width="3%">
                                    <asp:LinkButton ID="CandidateDashboard_resetBottomLinkButton" runat="server" SkinID="sknActionLinkButton"
                                        OnClick="CandidateDashboard_resetTopLinkButton_Click">Reset</asp:LinkButton>
                                </td>
                                <td align="center" class="link_button" width="3%">
                                    |
                                </td>
                                <td width="3%">
                                    <asp:LinkButton ID="CandidateDashboard_cancelBottomLinkButton" runat="server" SkinID="sknActionLinkButton"
                                        OnClick="ParentPageRedirect">Cancel</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
