﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// DictionaryManager.cs
// File that represents the represents the user interface layout and 
// functionalities for the Dictionary Manager page. This page helps 
// in managing the resume parser dictionary
//

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.ResumeRepository
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Dictionary Manager page. This page helps in managing the 
    /// resume parser dictionary. This class inherits the 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class DictionaryManager : PageBase
    {
        #region Declaration                                                    

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "275px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "350px";

        #endregion Declaration                                                 

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Dictionary Manager");

                //Assingn default focus 
                DictionaryManager_dictionaryTypeDropDownList.Focus();

                //Assingn default button 
                Page.Form.DefaultButton = DictionaryManager_searchButton.UniqueID;

                // Set default button.
                Page.Form.DefaultButton = DictionaryManager_searchButton.UniqueID;
                CheckAndSetExpandorRestore();

                DictionaryManager_editDictionaryButton.Attributes.Add("OnClick",
                    "Javascript:return ShowDictionaryEntryWindow();");

                if (!IsPostBack)
                {
                    LoadValues();

                    AssignHandlers();

                    CheckAndSetExpandorRestore();

                    DictionaryManager_testDiv.Visible = false;
                }

                DictionaryManager_topSuccessMessageLabel.Text = string.Empty;
                DictionaryManager_bottomSuccessMessageLabel.Text = string.Empty;
                DictionaryManager_topErrorMessageLabel.Text = string.Empty;
                DictionaryManager_bottomErrorMessageLabel.Text = string.Empty;

                DictionaryManager_bottomPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                    (DictionaryManager_pagingNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void DictionaryManager_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                DictionaryManager_bottomPagingNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "DICTIONARY_TYPE";
                ViewState["PAGE_NUMBER"] = 1;
                LoadTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void DictionaryManager_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGE_NUMBER"] = e.PageNumber;
                
                //Bind the Search Result based on the Pageno, Sort Expression and Sort Direction
                LoadTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void DictionaryManager_searchGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (DictionaryManager_searchGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void DictionaryManager_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void DictionaryManager_searchGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                    DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void DictionaryManager_searchGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                DictionaryManager_bottomPagingNavigator.Reset();
                LoadTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                    DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void ClearMessages()
        {
            // Clear message.
            DictionaryManager_topErrorMessageLabel.Text = string.Empty;
            DictionaryManager_topSuccessMessageLabel.Text = string.Empty;
            DictionaryManager_bottomErrorMessageLabel.Text = string.Empty;
            DictionaryManager_bottomSuccessMessageLabel.Text = string.Empty;
        }

        protected void DictionaryManager_searchGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                // Clear messages.
                ClearMessages();

                // Keep the selected values in view state.
                ViewState["SELECTED_ELEMENT_TYPE_ID"] = (((ImageButton)e.CommandSource).Parent.
                    FindControl("DictionaryManager_elementTypeIDHiddenField") as HiddenField).Value;
                ViewState["SELECTED_ID"] = (((ImageButton)e.CommandSource).Parent.
                    FindControl("DictionaryManager_aliasIDHiddenField") as HiddenField).Value;
                ViewState["SELECTED_ALIAS"] = (((ImageButton)e.CommandSource).Parent.
                     FindControl("DictionaryManager_aliasNameHiddenField") as HiddenField).Value;

                if (e.CommandName == "DeleteEntry")
                {
                    // Set message, title and type for the confirmation control for edit entry.
                    DictionaryManager_deleteEntryPanel_confirmMessageControl.Message =
                        string.Format("Are you sure to delete the dictionary entry '{0}'?", ViewState["SELECTED_ALIAS"]);
                    DictionaryManager_deleteEntryPanel_confirmMessageControl.Title =
                        "Delete Dictionary Entry";
                    DictionaryManager_deleteEntryPanel_confirmMessageControl.Type =
                        MessageBoxType.YesNo;

                    DictionaryManager_deleteEntryPanel_confirmModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                    DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete entry confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the entry from the database.
        /// </remarks>
        protected void DictionaryManager_deleteEntryPanel_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                // Check if the dictionary type is node or attribute.
                bool isAttribute = false;
                if (ViewState["SELECTED_ELEMENT_TYPE_ID"].ToString().Trim() == "2")
                    isAttribute = true; 

                if (isAttribute)
                {
                    new ResumeRepositoryBLManager().DeleteAttributeDictionary
                        (Convert.ToInt32(ViewState["SELECTED_ID"]));
                }
                else
                {
                    new ResumeRepositoryBLManager().DeleteTitleDictionary
                        (Convert.ToInt32(ViewState["SELECTED_ID"]));
                }

                // Show success message.
                ShowMessage(DictionaryManager_topSuccessMessageLabel, 
                    DictionaryManager_bottomSuccessMessageLabel, 
                    "Dictionary entry deleted successfully");

                // Reload dictionary details with current page number.
                LoadTests(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                    DictionaryManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadTests(int pageNumber)
        {
            int totalRecords = 0;
            List<DictionaryDetail> dics = new ResumeRepositoryBLManager().
                GetDictionaryDetails(
                Convert.ToInt32(DictionaryManager_dictionaryTypeDropDownList.SelectedValue),
                DictionaryManager_headTextBox.Text.Trim(),
                DictionaryManager_aliasTextBox.Text.Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize,
                out totalRecords);

            DictionaryManager_searchGridView.DataSource = dics;
            DictionaryManager_searchGridView.DataBind();

            if (dics == null || dics.Count == 0)
            {
                base.ShowMessage(DictionaryManager_topErrorMessageLabel,
                DictionaryManager_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                DictionaryManager_searchGridView.DataSource = null;
                DictionaryManager_searchGridView.DataBind();
                DictionaryManager_bottomPagingNavigator.TotalRecords = 0;
                DictionaryManager_testDiv.Visible = false;
            }
            else
            {
                DictionaryManager_searchGridView.DataSource = dics;
                DictionaryManager_searchGridView.DataBind();
                DictionaryManager_bottomPagingNavigator.PageSize = base.GridPageSize;
                DictionaryManager_bottomPagingNavigator.TotalRecords = totalRecords;
                DictionaryManager_testDiv.Visible = true;
            }
        }

        /// <summary>
        /// Method that adds the click handler for expand or restore buttons.
        /// </summary>
        private void AssignHandlers()
        {
            DictionaryManager_searchTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestoreDiv('" +
                DictionaryManager_testDiv.ClientID + "','" +
                DictionaryManager_searchByTestDiv.ClientID + "','" +
                DictionaryManager_searchResultsUpSpan.ClientID + "','" +
                DictionaryManager_searchResultsDownSpan.ClientID + "','" +
                DictionaryManager_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(DictionaryManager_restoreHiddenField.Value) &&
                DictionaryManager_restoreHiddenField.Value == "Y")
            {
                DictionaryManager_searchByTestDiv.Style["display"] = "none";
                DictionaryManager_searchResultsUpSpan.Style["display"] = "block";
                DictionaryManager_searchResultsDownSpan.Style["display"] = "none";
                DictionaryManager_testDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                DictionaryManager_searchByTestDiv.Style["display"] = "block";
                DictionaryManager_searchResultsUpSpan.Style["display"] = "none";
                DictionaryManager_searchResultsDownSpan.Style["display"] = "block";
                DictionaryManager_testDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            List<ElementTypeDetail> elementTypes = new ResumeRepositoryBLManager().
                GetElementTypes();

            if (elementTypes == null || elementTypes.Count == 0)
                return;

            // Load drop down items for dictionary type (search criteria).
            DictionaryManager_dictionaryTypeDropDownList.DataSource = elementTypes;
            DictionaryManager_dictionaryTypeDropDownList.DataTextField = "ElementTypeName";
            DictionaryManager_dictionaryTypeDropDownList.DataValueField = "ElementTypeID";
            DictionaryManager_dictionaryTypeDropDownList.DataBind();

            // Insert the 'Both' item at 0 index.
            DictionaryManager_dictionaryTypeDropDownList.Items.Insert(0, new ListItem("Both", "0"));
        }

        #endregion Protected Overridden Methods                                
    }
}