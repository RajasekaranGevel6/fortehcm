﻿#region Directives

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class LocationManager : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "273px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "350px";

        #endregion Declaration

        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Location Manager");

                // Set default button.
                Page.Form.DefaultButton = LocationManager_searchButton.UniqueID;

                //Set default Focus
                LocationManager_skillNameTextBox.Focus();

                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    //Load the technical group in the technical group drop down list 
                    LoadValues();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "GROUP_NAME";

                    LocationManager_resultDiv.Visible = false;
                    LocationManager_bottomPagingNavigator.Visible = false;
                }

                LocationManager_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestoreDiv('" +
                LocationManager_resultDiv.ClientID + "','" +
                LocationManager_searchDiv.ClientID + "','" +
                LocationManager_searchResultsUpSpan.ClientID + "','" +
                LocationManager_searchResultsDownSpan.ClientID + "','" +
                LocationManager_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

                LocationManager_bottomSuccessMessageLabel.Text = string.Empty;
                LocationManager_topSuccessMessageLabel.Text = string.Empty;
                LocationManager_bottomErrorMessageLabel.Text = string.Empty;
                LocationManager_topErrorMessageLabel.Text = string.Empty;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                   LocationManager_bottomErrorMessageLabel,
                 exception.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on reset link button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void LocationManager_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                   LocationManager_bottomErrorMessageLabel,
                 exception.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on search button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void LocationManager_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Make the result div as visible
                LocationManager_resultDiv.Visible = true;
                LocationManager_bottomPagingNavigator.Visible = true;

                //Add the default sort order in view state
                ViewState["SORT_ORDER"] = SortType.Ascending;

                //Add the default sort field
                ViewState["SORT_FIELD"] = "GROUP_NAME";

                //Reset the paging control 
                LocationManager_bottomPagingNavigator.Reset();

                //Load the location details
                LoadLocationDetails(1);
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                    LocationManager_bottomErrorMessageLabel, exp.Message);
            }

        }
        /// <summary>
        /// Represents the event handler that is called on the save button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the sender of the object
        /// </param>
        protected void LocationManager_editLocationsaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Check the mandatory field
                if (LocationManager_editLocation_aliasTextBox.Text.Trim().Length == 0)
                {
                    LocationManager_editLocation_errorLabel.Text = "Alias cannot be empty";
                    LocationManager_editLocation_saveButton.Focus();
                    LocationManager_editLocationModalPopUpExtender.Show();
                    return;
                }

                //Update the alias field for the given skills
                int skillID = int.Parse(LocationManager_editLocationIDHiddenField.Value);
                string skillAliasName = LocationManager_editLocation_aliasTextBox.Text.Trim();

                if (CheckEnteredSkillsValues(LocationManager_editLocation_aliasTextBox))
                {
                    new ResumeRepositoryBLManager().UpdateLocationAliasName
                        (skillID, skillAliasName);

                    base.ShowMessage(LocationManager_topSuccessMessageLabel,
                      LocationManager_bottomSuccessMessageLabel, Resources.HCMResource.
                    CompetencyVectorManager_updateAliasSuccessMessage);

                    LoadLocationDetails(1);

                }
                else
                {
                    LocationManager_editLocation_errorLabel.Text =
                   Resources.HCMResource.CompetencyVectorManager_DuplicateNotAllowedMessage;
                    LocationManager_editLocation_saveButton.Focus();
                    LocationManager_editLocationModalPopUpExtender.Show();
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                  LocationManager_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on the save button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the sender of the object
        /// </param>
        protected void LocationManager_deletePopup_yesValidButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the location id from the hidden field
                int skillID = int.Parse(LocationManager_editLocationIDHiddenField.Value);

                //if it is not involved delete the location
                //if (isSkillIdInvolved)
                //{
                new ResumeRepositoryBLManager().DeleteLocation(skillID);

                base.ShowMessage(LocationManager_topSuccessMessageLabel,
              LocationManager_bottomSuccessMessageLabel, Resources.HCMResource.
              LocationManager_DeleteSuccessMessage);
                LoadLocationDetails(1);
                //}
                //else
                //{
                //    LocationManager_deletePopup_errorLabel.Text = Resources.HCMResource.
                //        LocationManager_SkillInvolved; 
                //    LocationManager_deletePopupExtender.Show();
                //}
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                    LocationManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on grid view row command 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewCommandEventArgs"/>that holds the event args
        /// </param>
        protected void LocationManager_searchGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField locationGroupNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                    ("LocationManager_locationTypeNameHiddenField") as HiddenField;

                HiddenField locationNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                   ("LocationManager_locationNameHiddenField") as HiddenField;

                HiddenField locationAliasNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                   ("LocationManager_locationAliasNameHiddenField") as HiddenField;

                HiddenField locationIDHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                    ("LocationManager_locationIDHiddenField") as HiddenField;

                if (e.CommandName == "EditLocation")
                {
                    //Get the skills value and show the modal pop up extender
                    LocationManager_editLocation_errorLabel.Text = string.Empty;
                    LocationManager_editLocation_typeNameValueLabel.Text = locationGroupNameHiddenField.Value.Trim();
                    LocationManager_editLocation_locationNameValueLabel.Text = locationNameHiddenField.Value.Trim();
                    LocationManager_editLocation_aliasTextBox.Text = locationAliasNameHiddenField.Value.Trim();
                    LocationManager_editLocationIDHiddenField.Value = locationIDHiddenField.Value;
                    LocationManager_editLocation_saveButton.Focus();
                    LocationManager_editLocationModalPopUpExtender.Show();
                }
                if (e.CommandName == "DeleteLocation")
                {
                    LocationManager_deletePopup_messageConfirmLabel.Text = string.Format(Resources.HCMResource.
                        LocationManager_DeleteLocationConfirmMessage, locationNameHiddenField.Value);
                    LocationManager_deletePopup_errorLabel.Text = string.Empty;
                    LocationManager_editLocationIDHiddenField.Value = locationIDHiddenField.Value;
                    LocationManager_deletePopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                  LocationManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on the grid view 
        /// row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void LocationManager_searchGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                   LocationManager_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row is created in the grid
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param
        protected void LocationManager_searchGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //Add the sort image for the header row
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (LocationManager_searchGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                   LocationManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event that is called on page number click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="PageNumberEventArgs"/>that holds the events args
        /// </param>
        protected void LocationManager_pageNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                //Load the grid with the current page number
                LoadLocationDetails(e.PageNumber);
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                    LocationManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on add location link button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void LocationManager_addLocationLinkButton_Click(object sender, EventArgs e)
        {
            //Assign the skills group details to the drop down list
            List<LocationDetails> locationDetailsList = new ResumeRepositoryBLManager().GetLocationType();

            LocationManager_addNewLocationGroupNameDropDownList.DataSource = locationDetailsList;
            LocationManager_addNewLocationGroupNameDropDownList.DataTextField = "LocationType";
            LocationManager_addNewLocationGroupNameDropDownList.DataValueField = "LocationTypeID";
            LocationManager_addNewLocationGroupNameDropDownList.DataBind();

            //Clear the values 
            LocationManager_addNewLocationLocationNameTextBox.Text = string.Empty;
            LocationManager_addNewLocationAliasTextBox.Text = string.Empty;
            LocationManager_addNewLocationErrorLabel.Text = string.Empty;

            LocationManager_addNewLocationSaveButton.Focus();

            //Show the add new location modal pop up
            LocationManager_addNewLocationModalPopupExtender.Show();

        }

        /// <summary>
        /// Represents the event handler that is on the add new button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void LocationManager_addNewLocationSaveButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                //Skills name and alias name should not be emtpy
                if ((LocationManager_addNewLocationLocationNameTextBox.Text.Trim().Length == 0) ||
                    (LocationManager_addNewLocationAliasTextBox.Text.Trim().Length == 0))
                {
                    LocationManager_addNewLocationErrorLabel.Text =
                       "Please enter mandatory fields";
                    LocationManager_addNewLocationSaveButton.Focus();
                    LocationManager_addNewLocationModalPopupExtender.Show();
                }
                else
                {
                    if (CheckEnteredSkillsValues(LocationManager_addNewLocationAliasTextBox))
                    {
                        //Save the skills to the Data base
                        int locationGroupID = int.Parse
                            (LocationManager_addNewLocationGroupNameDropDownList.SelectedValue);
                        string locationName = LocationManager_addNewLocationLocationNameTextBox.Text.Trim();
                        string locaAliasName = LocationManager_addNewLocationAliasTextBox.Text.Trim();
                        if (new ResumeRepositoryBLManager().AddNewLocation(locationGroupID, locationName,
                            locaAliasName, base.userID))
                        {
                            base.ShowMessage(LocationManager_topSuccessMessageLabel,
                                LocationManager_bottomSuccessMessageLabel,
                                 string.Format(Resources.HCMResource.LocationManager_NewLocationAddedSuccessMessage,
                                 locationName));
                            LoadLocationDetails(1);
                        }
                        else
                        {
                            LocationManager_addNewLocationErrorLabel.Text =
                                     Resources.HCMResource.LocationManager_LocationAlreadyExistMessage;
                            LocationManager_addNewLocationSaveButton.Focus();
                            LocationManager_addNewLocationModalPopupExtender.Show();
                        }
                    }
                    else
                    {
                        LocationManager_addNewLocationErrorLabel.Text =
                       Resources.HCMResource.CompetencyVectorManager_DuplicateNotAllowedMessage;
                        LocationManager_addNewLocationSaveButton.Focus();
                        LocationManager_addNewLocationModalPopupExtender.Show();
                    }
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                    LocationManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the handler that is called on sorting
        /// of the grid view
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void LocationManager_searchGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                LoadLocationDetails(1);

                LocationManager_bottomPagingNavigator.Reset();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(LocationManager_topErrorMessageLabel,
                    LocationManager_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handler

        #region Private Methods

        /// <summary>
        /// Represents the method that is used to check the entered location values
        /// </summary>
        /// <param name="aliasTextBox">
        /// A<see cref="TextBox"/>that holds the alias text box name
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds whether the value entered in text box is 
        /// valid or not
        /// </returns>
        private bool CheckEnteredSkillsValues(TextBox aliasTextBox)
        {
            string aliasName = aliasTextBox.Text.Trim();
            //Get the individual string values
            string[] separatedAliasName = aliasName.Split(new char[] { ',' });
            //Get the distinct string count
            int numberOfStrings = separatedAliasName.Distinct().Count();
            //if the number of string is equal to length return true
            return numberOfStrings == separatedAliasName.Length ? true : false;
        }

        /// <summary>
        /// Represents the method to load the technical location details
        /// </summary>
        /// <param name="pagenumber">
        /// A<see cref="int"/>that holds the page number 
        /// </param>
        private void LoadLocationDetails(int pageNumber)
        {

            int total = 0;
            int locationGroupId = int.Parse(LocationManager_groupTypeDropDownList.SelectedValue);
            string locationName = LocationManager_skillNameTextBox.Text.Trim();
            string locationAliasName = LocationManager_skillsAliasTextBox.Text.Trim();

            //Get the  technical skills details from the database
            List<LocationDetails> technicalSkillsDetails =
            new ResumeRepositoryBLManager().
                GetLocationDetails(locationGroupId, locationName, locationAliasName, ViewState["SORT_FIELD"].ToString(),
                ViewState["SORT_ORDER"].ToString(), pageNumber, base.GridPageSize, out total);

            if (technicalSkillsDetails != null && technicalSkillsDetails.Count != 0)
            {
                LocationManager_searchGridView.DataSource = technicalSkillsDetails;
                LocationManager_searchGridView.DataBind();
            }
            else
            {
                LocationManager_searchGridView.DataSource = technicalSkillsDetails;
                LocationManager_searchGridView.DataBind();

                if (LocationManager_topSuccessMessageLabel.Text.Length != 0)
                {
                    //show the default empty message
                    ShowMessage(LocationManager_bottomErrorMessageLabel,
                        LocationManager_topErrorMessageLabel, "</br>" + Resources.HCMResource.
                        Common_Empty_Grid);
                }
                else
                {
                    //show the default empty message
                    ShowMessage(LocationManager_bottomErrorMessageLabel,
                        LocationManager_topErrorMessageLabel, Resources.HCMResource.
                        Common_Empty_Grid);
                }
                //Make the result div as visible
                LocationManager_resultDiv.Visible = false;
                LocationManager_bottomPagingNavigator.Visible = false;
            }

            LocationManager_bottomPagingNavigator.TotalRecords = total;
            LocationManager_bottomPagingNavigator.PageSize = base.GridPageSize;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(LocationManager_restoreHiddenField.Value) &&
                LocationManager_restoreHiddenField.Value == "Y")
            {
                LocationManager_searchDiv.Style["display"] = "none";
                LocationManager_searchResultsUpSpan.Style["display"] = "block";
                LocationManager_searchResultsDownSpan.Style["display"] = "none";
                LocationManager_resultDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                LocationManager_searchDiv.Style["display"] = "block";
                LocationManager_searchResultsUpSpan.Style["display"] = "none";
                LocationManager_searchResultsDownSpan.Style["display"] = "block";
                LocationManager_resultDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods

        #region Overloaded Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            List<LocationDetails> technicalGroupList = new ResumeRepositoryBLManager().GetLocationType();

            LocationManager_groupTypeDropDownList.DataSource = technicalGroupList;
            LocationManager_groupTypeDropDownList.DataTextField = "LocationType";
            LocationManager_groupTypeDropDownList.DataValueField = "LocationTypeID";
            LocationManager_groupTypeDropDownList.DataBind();
            LocationManager_groupTypeDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        #endregion Overloaded Methods
    }
}
