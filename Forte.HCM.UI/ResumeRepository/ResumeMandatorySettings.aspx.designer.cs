﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.ResumeRepository {
    
    
    public partial class ResumeMandatorySettings {
        
        /// <summary>
        /// ResumeReminderSettings_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ResumeReminderSettings_headerLiteral;
        
        /// <summary>
        /// ResumeMandatorySettings_topSaveUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeMandatorySettings_topSaveUpdatePanel;
        
        /// <summary>
        /// ResumeMandatorySettings_topSaveMandatorySettings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ResumeMandatorySettings_topSaveMandatorySettings;
        
        /// <summary>
        /// ResumeReminderSettings_topResetUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeReminderSettings_topResetUpdatePanel;
        
        /// <summary>
        /// ResumeReminderSettings_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ResumeReminderSettings_topResetLinkButton;
        
        /// <summary>
        /// ResumeReminderSettings_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ResumeReminderSettings_topCancelLinkButton;
        
        /// <summary>
        /// ResumeReminderSettings_topMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeReminderSettings_topMessageUpdatePanel;
        
        /// <summary>
        /// ResumeReminderSettings_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeReminderSettings_topSuccessMessageLabel;
        
        /// <summary>
        /// ResumeReminderSettings_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeReminderSettings_topErrorMessageLabel;
        
        /// <summary>
        /// Tr1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow Tr1;
        
        /// <summary>
        /// Literal1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal1;
        
        /// <summary>
        /// ResumeMandatorySettings_contactInfoUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeMandatorySettings_contactInfoUpdatePanel;
        
        /// <summary>
        /// ResumeMandatorySettings_contactInfoDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList ResumeMandatorySettings_contactInfoDataList;
        
        /// <summary>
        /// Tr2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow Tr2;
        
        /// <summary>
        /// Literal2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal2;
        
        /// <summary>
        /// ResumeMandatorySettings_excutiveTechnicalUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeMandatorySettings_excutiveTechnicalUpdatePanel;
        
        /// <summary>
        /// ResumeMandatorySettings_excutiveTechnicalDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList ResumeMandatorySettings_excutiveTechnicalDataList;
        
        /// <summary>
        /// Tr3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow Tr3;
        
        /// <summary>
        /// Literal3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal3;
        
        /// <summary>
        /// ResumeMandatorySettings_projectsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeMandatorySettings_projectsUpdatePanel;
        
        /// <summary>
        /// ResumeMandatorySettings_projectsDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList ResumeMandatorySettings_projectsDataList;
        
        /// <summary>
        /// Tr4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow Tr4;
        
        /// <summary>
        /// Literal4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal4;
        
        /// <summary>
        /// ResumeMandatorySettings_educationReferenceUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeMandatorySettings_educationReferenceUpdatePanel;
        
        /// <summary>
        /// ResumeMandatorySettings_educationReferenceDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList ResumeMandatorySettings_educationReferenceDataList;
        
        /// <summary>
        /// ResumeReminderSettings_bottomMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeReminderSettings_bottomMessageUpdatePanel;
        
        /// <summary>
        /// ResumeReminderSettings_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeReminderSettings_bottomSuccessMessageLabel;
        
        /// <summary>
        /// ResumeReminderSettings_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeReminderSettings_bottomErrorMessageLabel;
        
        /// <summary>
        /// ResumeMandatorySettings_bottomSaveUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeMandatorySettings_bottomSaveUpdatePanel;
        
        /// <summary>
        /// ResumeMandatorySettings_bottomSaveMandatorySettings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ResumeMandatorySettings_bottomSaveMandatorySettings;
        
        /// <summary>
        /// ResumeReminderSettings_resetBottomUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ResumeReminderSettings_resetBottomUpdatePanel;
        
        /// <summary>
        /// ResumeReminderSettings_bottomResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ResumeReminderSettings_bottomResetLinkButton;
        
        /// <summary>
        /// ResumeReminderSettings_bottomCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ResumeReminderSettings_bottomCancelLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.ResumeRepositoryMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.ResumeRepositoryMaster)(base.Master));
            }
        }
    }
}
