﻿using System;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ResumeParser.ResumeValidator;

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class DuplicateResumeManagement : PageBase
    {
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Duplicate Resume Management");
                ClearMessageControls();
                DuplicateResumeManagement_topErrorMessageLabel.Text = "";
                DuplicateResumeManagement_bottomErrorMessageLabel.Text = "";
                DuplicateResumeManagement_topSuccessMessageLabel.Text = "";
                DuplicateResumeManagement_bottomSuccessMessageLabel.Text = "";

                if (!IsPostBack)
                {
                    DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl.Message = "Are you sure to delete the candidate?";
                    DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl.Type = MessageBoxType.YesNo;
                    DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl.Title = "Delete Candidate";

                    DuplicateResumeManagement_candidateInfoTR.Style["display"] = "none";
                    /* DuplicateResumeManagement_newCandidateConfirmMsgControl.Message = "Are you sure to add the candidate as new?";
                     DuplicateResumeManagement_newCandidateConfirmMsgControl.Type = MessageBoxType.YesNo;
                     DuplicateResumeManagement_newCandidateConfirmMsgControl.Title = "Confirmation";*/


                    DuplicateResumeManagement_newResumeConfirmMsgControl.Message = "Are you sure to add this resume to this candidate?";
                    DuplicateResumeManagement_newResumeConfirmMsgControl.Type = MessageBoxType.YesNo;
                    DuplicateResumeManagement_newResumeConfirmMsgControl.Title = "Add New Resume";


                    GetCandidate_TemporaryProfile(Convert.ToInt32(Request.QueryString["candidateid"]));

                    ProcessHRXml(Convert.ToInt32(Request.QueryString["candidateid"]),
                        "TEMP", DuplicateResumeManagement_uploadedResumeDiv);

                    LoadDefaultDuplicateCandidate(Convert.ToInt32(Request.QueryString["candidateid"]));
                }
                DuplicateResumeManagement_searchCandidatepageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (DuplicateResumeManagement_searchCandidatepageNavigator_PageNumberClick);
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                    DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        private void DuplicateResumeManagement_searchCandidatepageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                if (ViewState["AUTOLOAD"] == null) return;

                ViewState["pageNo"] = e.PageNumber;

                if (ViewState["AUTOLOAD"].ToString() == "NO")
                    SearchRecordsBind(e.PageNumber);
                /*else
                  LoadDefaultDuplicateCandidate(Convert.ToInt32(Request.QueryString["candidateid"]));  */
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                    DuplicateResumeManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        protected void DuplicateResumeManagement_clearLinkButton_Click(object sender, EventArgs e)
        {
            ClearSearchControls();
        }
        protected void DuplicateResumeManagement_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                {
                    base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                        DuplicateResumeManagement_bottomErrorMessageLabel, "Enter search values");
                    return;
                }

                DuplicateResumeManagement_resumeListGridView.DataSource = null;
                DuplicateResumeManagement_resumeListGridView.DataBind();
                DuplicateResumeManagement_useIDValueLabel.Text = "";
                DuplicateResumeManagement_candidateNameValueLabel.Text = "";
                DuplicateResumeManagement_candidateEmailValueLabel.Text = "";
                DuplicateResumeManagement_candidatePhoneNoValueLabel.Text = "";
                ViewState["AUTOLOAD"] = "NO";
                SearchRecordsBind(1);

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                    DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_searchDuplicateResumeGridView_selectCandidateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow oldrow in DuplicateResumeManagement_searchDuplicateResumeGridView.Rows)
                {
                    ((RadioButton)oldrow.FindControl("DuplicateResumeManagement_searchDuplicateResumeGridView_selectCandidateRadioButton")).Checked = false;
                }

                //string _text = "<html><body>-</body></html>";
                //DuplicateResumeManagement_existingResumeDiv.InnerText = _text;
                //DuplicateResumeManagement_existingResumeDiv.InnerHtml = _text;

                DuplicateResumeManagement_existingResumeDiv.Style["display"] = "none";

                //Set the new selected row
                RadioButton rb = (RadioButton)sender;
                GridViewRow row = (GridViewRow)rb.NamingContainer;
                ((RadioButton)row.FindControl("DuplicateResumeManagement_searchDuplicateResumeGridView_selectCandidateRadioButton")).Checked = true;

                HiddenField DuplicateResumeManagement_searchDuplicateResumeGridView_candidateIDHiddenField =
                    (HiddenField)row.FindControl("DuplicateResumeManagement_searchDuplicateResumeGridView_candidateIDHiddenField");

                if (DuplicateResumeManagement_searchDuplicateResumeGridView_candidateIDHiddenField != null)
                {
                    int candidateID = 0;
                    if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_searchDuplicateResumeGridView_candidateIDHiddenField.Value))
                        candidateID = Convert.ToInt32(DuplicateResumeManagement_searchDuplicateResumeGridView_candidateIDHiddenField.Value);

                    ViewState["CURRENT_CANDIDATEID"] = candidateID;

                    CandidateDetail candidateDetail = new CandidateBLManager().
                        GetCandidateBasicDetail(candidateID);
                    DuplicateResumeManagement_useIDValueLabel.Text = candidateDetail.UserName;

                    DuplicateResumeManagement_candidateNameValueLabel.Text = candidateDetail.FirstName;
                    DuplicateResumeManagement_candidateEmailValueLabel.Text = candidateDetail.EMailID;
                    DuplicateResumeManagement_candidatePhoneNoValueLabel.Text = candidateDetail.Mobile;

                    LoadResumeList(candidateID);

                    UploadedResumeStatus_candidateDashboardHyperLink.NavigateUrl
                        = "~/ReportCenter/CandidateDashboard.aspx?candidateid=" + candidateID;

                    SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogImageButton.Attributes.
                        Add("onclick", "javascript:return OpenCandidateActivity('" + candidateID + "');");

                }
                DuplicateResumeManagement_existingResumeDiv.Style["display"] = "none";
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel, DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_resumeListGridView_selectCandidateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow oldrow in DuplicateResumeManagement_resumeListGridView.Rows)
                {
                    ((RadioButton)oldrow.FindControl("DuplicateResumeManagement_resumeListGridView_selectCandidateRadioButton")).Checked = false;
                    ((ImageButton)oldrow.FindControl("DuplicateResumeManagement_resumeListGridView_downloadImageButton")).Enabled = false;
                }


                //Set the new selected row
                RadioButton rb = (RadioButton)sender;
                GridViewRow row = (GridViewRow)rb.NamingContainer;
                ((RadioButton)row.FindControl("DuplicateResumeManagement_resumeListGridView_selectCandidateRadioButton")).Checked = true;
                ((ImageButton)row.FindControl("DuplicateResumeManagement_resumeListGridView_downloadImageButton")).Enabled = true;

                HiddenField DuplicateResumeManagement_resumeListGridView_candidateIDHiddenField =
                    (HiddenField)row.FindControl("DuplicateResumeManagement_resumeListGridView_candidateIDHiddenField");

                DuplicateResumeManagement_existingResumeDiv.Style["display"] = "block";

                if (DuplicateResumeManagement_resumeListGridView_candidateIDHiddenField != null)
                {
                    ProcessHRXml(Convert.ToInt32(DuplicateResumeManagement_resumeListGridView_candidateIDHiddenField.Value),
                        "NORMAL", DuplicateResumeManagement_existingResumeDiv);
                }

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel, DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_resumeListGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteResume")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    HiddenField resumeID = (HiddenField)row.FindControl("DuplicateResumeManagement_resumeListGridView_resumeIDHiddenField");
                    HiddenField candidateID = (HiddenField)row.FindControl("DuplicateResumeManagement_resumeListGridView_candidateIDHiddenField");

                    if (resumeID != null)
                    {
                        new CandidateBLManager().DeleteCandidateResume(Convert.ToInt32(resumeID.Value));
                        base.ShowMessage(DuplicateResumeManagement_topSuccessMessageLabel,
                            DuplicateResumeManagement_bottomSuccessMessageLabel, "Deleted Successfully");
                        //DuplicateResumeManagement_resumeListGridView.DeleteRow(row.RowIndex);                         
                        LoadResumeList(Convert.ToInt32(candidateID.Value));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                        DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_resumeListGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                object objResumeDetail = e.Row.DataItem;

                if (objResumeDetail != null)
                {
                    ImageButton DuplicateResumeManagement_resumeListGridView_downloadImageButton =
                        (ImageButton)e.Row.FindControl("DuplicateResumeManagement_resumeListGridView_downloadImageButton");
                    DuplicateResumeManagement_resumeListGridView_downloadImageButton.Attributes.Add("onclick",
                        "javascript:return DownloadResumeID('" + ((Forte.HCM.DataObjects.CandidateResumeDetails)(objResumeDetail)).CandidateResumeId + "');");
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel, DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_bottomResetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        protected void DuplicateResumeManagement_resetTopLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        protected void DuplicateResumeManagement_newCandidateConfirmMsgControl_okClick
       (object sender, EventArgs e)
        {
            try
            {
                int newCandidateId = new CandidateBLManager().CreateCandidate(Convert.ToInt32(Request.QueryString["candidateid"]));

                base.ShowMessage(DuplicateResumeManagement_topSuccessMessageLabel,
                     DuplicateResumeManagement_bottomSuccessMessageLabel, "New candidate created successfully");
                Response.Redirect("../ResumeRepository/ReviewResume.aspx?m=0&s=4&messageid=1", false);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                         DuplicateResumeManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        protected void DuplicateResumeManagement_newCandidateButton_Click(object sender, EventArgs e)
        {
            try
            {
                int candidateid = 0;
                if (Request.QueryString["candidateid"] != null)
                {
                    candidateid = Convert.ToInt32(Request.QueryString["candidateid"]);
                }
                if (candidateid > 0)
                {

                    DuplicateResumeManagement_candidateInfoTR.Style["width"] = "100%";
                    if (DuplicateResumeManagement_candidateInfoTR.Style["display"] == "block")
                    {
                        DuplicateResumeManagement_candidateInfoTR.Style["display"] = "none";
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "stopScroll();", true);
                    }
                    else
                    {
                        DuplicateResumeManagement_candidateInfoTR.Style["display"] = "block";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "scroll", "pageScroll();", true);
                    }

                    Page.SetFocus(DuplicateResumeManagement_SaveButton.ClientID);
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                        DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_newResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                int temporyCandidateID = 0;

                if (Request.QueryString["candidateid"] != null)
                {
                    temporyCandidateID = Convert.ToInt32(Request.QueryString["candidateid"]);
                }
                if (temporyCandidateID > 0 && ViewState["CURRENT_CANDIDATEID"] != null)
                {
                    int caiID = int.Parse(ViewState["CURRENT_CANDIDATEID"].ToString());
                    if (caiID > 0)
                        DuplicateResumeManagement_newResumeModalPopupExtender.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                        DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_newResumeConfirmMsgControl_cancelClick
        (object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                         DuplicateResumeManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        protected void DuplicateResumeManagement_newResumeConfirmMsgControl_okClick
       (object sender, EventArgs e)
        {
            try
            {
                new CandidateBLManager().CreateCandidateNewResume(Convert.ToInt32(Request.QueryString["candidateid"]),
                    Convert.ToInt32(ViewState["CURRENT_CANDIDATEID"]));

                base.ShowMessage(DuplicateResumeManagement_topSuccessMessageLabel,
                        DuplicateResumeManagement_bottomSuccessMessageLabel, "New resume assigned successfully");

                Response.Redirect("../ResumeRepository/ReviewResume.aspx?m=0&s=4&messageid=2", false);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                         DuplicateResumeManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        protected void DuplicateResumeManagement_autoLoadLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearSearchControls();

                LoadDefaultDuplicateCandidate(Convert.ToInt32(Request.QueryString["candidateid"]));
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                        DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl_cancelClick
         (object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                         DuplicateResumeManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        protected void DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl_okClick
       (object sender, EventArgs e)
        {
            try
            {
                new ResumeRepositoryBLManager().DeleteTemporaryResume(Convert.ToInt32(Request.QueryString["candidateid"]));

                Response.Redirect("../ResumeRepository/ReviewResume.aspx?m=0&s=4&messageid=3", false);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                         DuplicateResumeManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        protected void DuplicateResumeManagement_deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                //DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl.Message = "Are you sure to delete the candidate?";
                //DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl.Type = MessageBoxType.OkCancel;
                //DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl.Title = "Confirmation";   
                DuplicateResumeManagement_deleteTemporaryCandidateModalPopupExtender.Show();
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                        DuplicateResumeManagement_bottomErrorMessageLabel, ex.Message);
            }
        }
        protected void DuplicateResumeManagement_SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValueCheck())
                {
                    base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                        DuplicateResumeManagement_bottomErrorMessageLabel,
                        "Please enter mandatory fields");

                    return;
                }

                CandidateInformation candidateInfo = new CandidateInformation();

                candidateInfo.caiFirstName =
                    DuplicateResumeManagement_temporaryFirstNameTextBox.Text.Trim();

                candidateInfo.caiMiddleName =
                    DuplicateResumeManagement_middleNameTextBox.Text.Trim();

                candidateInfo.caiLastName =
                    DuplicateResumeManagement_temporaryLastNameTextBox.Text.Trim();

                candidateInfo.caiEmail =
                    DuplicateResumeManagement_temporaryEmailTextBox.Text.Trim();

                candidateInfo.caiHomePhone =
                    DuplicateResumeManagement_homephoneTextBox.Text.Trim();

                candidateInfo.caiCell =
                    DuplicateResumeManagement_cellPhoneTextBox.Text.Trim();

                candidateInfo.caiAddress =
                    DuplicateResumeManagement_addressTextBox.Text.Trim();

                candidateInfo.caiCityName =
                    DuplicateResumeManagement_cityNameTextBox.Text.Trim();

                candidateInfo.caiStateName =
                    DuplicateResumeManagement_stateNameTextBox.Text.Trim();

                candidateInfo.caiCountryName =
                    DuplicateResumeManagement_countryTextBox.Text.Trim();

                candidateInfo.caiZip =
                    DuplicateResumeManagement_postalCodeTextBox.Text.Trim();

                candidateInfo.caiID = Convert.ToInt32(Request.QueryString["candidateid"]);

                if (new ResumeRepositoryBLManager().UpdateTemporaryCandidateContactInfo(candidateInfo))
                {
                    int newCandidateId = new CandidateBLManager().
                        CreateCandidate(Convert.ToInt32(Request.QueryString["candidateid"]));

                    base.ShowMessage(DuplicateResumeManagement_topSuccessMessageLabel,
                         DuplicateResumeManagement_bottomSuccessMessageLabel,
                         "New candidate created successfully");

                    Response.Redirect("../ResumeRepository/ReviewResume.aspx?m=0&s=4&messageid=1", false);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                         DuplicateResumeManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// This function load the default matching candidates
        /// </summary>
        /// <param name="temporaryCandidateID">
        /// <see cref="System.Int32"/>
        /// </param> 
        private void LoadDefaultDuplicateCandidate(int temporaryCandidateID)
        {

            IResumeValidator validator = Validator.GetInstance();


            List<DuplicateCandidateDetail> duplicateCandidates = null;
            bool isDuplicate = validator.CheckDuplicate((int)temporaryCandidateID,
                false, out duplicateCandidates);

            //int totalRecords = 0;

            if (duplicateCandidates == null) return;

            ViewState["AUTOLOAD"] = "YES";

            DuplicateResumeManagement_searchDuplicateResumeGridView.
                DataSource = duplicateCandidates;

            DuplicateResumeManagement_searchDuplicateResumeGridView.DataBind();

            /*DuplicateResumeManagement_searchCandidatepageNavigator.PageSize = base.GridPageSize;
            DuplicateResumeManagement_searchCandidatepageNavigator.TotalRecords 
                = duplicateCandidates.Count; */

        }

        private void GetCandidate_TemporaryProfile(int candidateID)
        {
            CandidateResumeDetails candidateResumeDetail = null;

            candidateResumeDetail = new ResumeRepositoryBLManager().
                GetCandidateExternalDataStore(candidateID);

            if (candidateResumeDetail != null)
            {
                DuplicateResumeManagement_displayCandidateNameLabelValue.Text
                    = candidateResumeDetail.FirstName;

                DuplicateResumeManagement_lastUploadedDateLabel.Text
                    = candidateResumeDetail.ProcessedDate.ToShortDateString();

                DuplicateResumeManagement_lastUploadedByLabel.Text
                    = candidateResumeDetail.Recruiter;

                DuplicateResumeManagement_uploadedResumeStausLabel.Text
                    = candidateResumeDetail.Duplicate;

                DuplicateResumeManagement_uploadedResumeReasonLabel.Text
                    = candidateResumeDetail.Reason;
            }

        }

        private void ClearSearchControls()
        {
            DuplicateResumeManagement_firstNameTextBox.Text = "";
            DuplicateResumeManagement_lastNameTextBox.Text = "";
            DuplicateResumeManagement_emailTextBox.Text = "";
            DuplicateResumeManagement_phoneTextBox.Text = "";
            DuplicateResumeManagement_searchDuplicateResumeGridView.DataSource = null;
            DuplicateResumeManagement_searchDuplicateResumeGridView.DataBind();

            DuplicateResumeManagement_searchCandidatepageNavigator.Reset();
            //DuplicateResumeManagement_searchCandidatepageNavigator.PageSize = 1;
            //DuplicateResumeManagement_searchCandidatepageNavigator.TotalRecords = 0;
            //DuplicateResumeManagement_searchCandidatepageNavigator.DataBind();

            DuplicateResumeManagement_resumeListGridView.DataSource = null;
            DuplicateResumeManagement_resumeListGridView.DataBind();
            DuplicateResumeManagement_existingResumeDiv.InnerText = "";
            DuplicateResumeManagement_useIDValueLabel.Text = "";
            DuplicateResumeManagement_candidateNameValueLabel.Text = "";
            DuplicateResumeManagement_candidateEmailValueLabel.Text = "";
            DuplicateResumeManagement_candidatePhoneNoValueLabel.Text = "";
            ViewState["CURRENT_CANDIDATEID"] = "";
            SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogImageButton.Attributes.Clear();
            UploadedResumeStatus_candidateDashboardHyperLink.NavigateUrl = string.Empty;
        }
        private void SearchRecordsBind(int pageNo)
        {

            string firstName = null;
            string lastName = null;
            string emailID = null;
            string phoneNo = null;

            if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_firstNameTextBox.Text))
                firstName = DuplicateResumeManagement_firstNameTextBox.Text.Trim();

            if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_lastNameTextBox.Text))
                lastName = DuplicateResumeManagement_lastNameTextBox.Text.Trim();

            if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_emailTextBox.Text))
                emailID = DuplicateResumeManagement_emailTextBox.Text.Trim();

            if (Utility.IsNullOrEmpty(DuplicateResumeManagement_phoneTextBox.Text))
                phoneNo = DuplicateResumeManagement_phoneTextBox.Text.Trim();

            DuplicateResumeManagement_searchDuplicateResumeGridView.DataSource = null;
            DuplicateResumeManagement_searchDuplicateResumeGridView.DataBind();

            int totalRecords = 0;

            List<CandidateResumeDetails> candidateList = null;

            candidateList = new ResumeRepositoryBLManager().GetDuplicateResumeCandidate(base.tenantID, firstName,
                    lastName, emailID, phoneNo, pageNo, base.GridPageSize, out totalRecords);

            if (candidateList == null || candidateList.Count == 0)
            {
                base.ShowMessage(DuplicateResumeManagement_topErrorMessageLabel,
                          DuplicateResumeManagement_bottomErrorMessageLabel, "No data found to display");

                return;
            }

            DuplicateResumeManagement_searchDuplicateResumeGridView.DataSource = candidateList;
            DuplicateResumeManagement_searchDuplicateResumeGridView.DataBind();

            DuplicateResumeManagement_searchCandidatepageNavigator.PageSize = base.GridPageSize;
            DuplicateResumeManagement_searchCandidatepageNavigator.TotalRecords = totalRecords;
        }
        private void LoadResumeList(int candidateID)
        {
            DuplicateResumeManagement_resumeListGridView.DataSource = null;
            DuplicateResumeManagement_resumeListGridView.DataBind();

            List<CandidateResumeDetails> resumeList = new ResumeRepositoryBLManager().GetCandidateResumeList(candidateID);
            DuplicateResumeManagement_resumeListGridView.DataSource = resumeList;
            DuplicateResumeManagement_resumeListGridView.DataBind();
        }
        private bool ValueCheck()
        {
            if (Utility.IsNullOrEmpty(DuplicateResumeManagement_temporaryFirstNameTextBox))
                return false;

            if (Utility.IsNullOrEmpty(DuplicateResumeManagement_temporaryLastNameTextBox.Text))
                return false;

            if (Utility.IsNullOrEmpty(DuplicateResumeManagement_temporaryEmailTextBox.Text))
                return false;

            return true;
        }

        private void ClearMessageControls()
        { 
            DuplicateResumeManagement_topErrorMessageLabel.Text =string.Empty;
            DuplicateResumeManagement_bottomErrorMessageLabel.Text = string.Empty;
        }
        #endregion

        #region Overridden Methods
        protected override bool IsValidData()
        {
            if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_firstNameTextBox.Text))
                return true;
            if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_lastNameTextBox.Text))
                return true;
            if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_emailTextBox.Text))
                return true;
            if (!Utility.IsNullOrEmpty(DuplicateResumeManagement_phoneTextBox.Text))
                return true;

            return false;
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region HRXML Methods
        private void ProcessHRXml(int candidateID, string type, HtmlGenericControl resumeDiv)
        {


            List<string> oList = null;
            if (type.ToString().ToUpper() == "TEMP")
                oList = new ResumeRepositoryBLManager().GetTemporaryResumeHRXML(candidateID);
            else
                oList = new ResumeRepositoryBLManager().GetCandidateHRXml(candidateID);

            if (oList == null)
            {
                return;
            }
            int candidateResumeID = Convert.ToInt32(oList[0].ToString());
            XmlDocument oXmlDoc = new XmlDocument();
            oXmlDoc.LoadXml(oList[1].ToString());
            //Check the Xml nodes from the database
            CheckHRXMLNodes(oXmlDoc);
            if (oXmlDoc != null)
            {
                // Set Person Name 
                SetPersonalInfo(oXmlDoc, type);

                // Set Projects
                SetProjects(oXmlDoc);

                // Set EducationDetails
                SetEducation(oXmlDoc);

                // Set References
                SetReferences(oXmlDoc);

                // Set Preview Resume
                SetPreviewResume(oXmlDoc, resumeDiv);
            }
            //if (oList[3].ToString() == "Y")
            //    ResumeEditor_topSaveButton.Visible = false;
            //ResumeEditor_saveUpdatePanel.Update();
        }

        /// <summary>
        /// This method helps to extract the hrxml resume contents and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPreviewResume(XmlDocument oXmlDoc, HtmlGenericControl htmlDiv)
        {
            // Set the ResumePreview
            XmlNode oPreviewResumeNodeList = oXmlDoc.SelectSingleNode("/Resume/NonXMLResume/TextResume");
            if (oPreviewResumeNodeList == null)
            {
                return;
            }
            if (oPreviewResumeNodeList.InnerText.Trim() != "")
            {
                htmlDiv.InnerHtml =
                    oPreviewResumeNodeList.InnerText.Replace("\r\n", "<br>");

                // Keep the resume contents in session.
                Session["RESUME_CONTENTS"] = oPreviewResumeNodeList.InnerText;
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml references data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetReferences(XmlDocument oXmlDoc)
        {
            //Get the list of references
            XmlNodeList oReferenceNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/References/Reference");
            if (oReferenceNodeList.Count == 0)
            {
                return;
            }
            List<Reference> oListReference = new List<Reference>();
            int intRefCnt = 0;
            foreach (XmlNode node in oReferenceNodeList)
            {
                if (node.InnerXml != "")
                {
                    intRefCnt = intRefCnt + 1;
                    Reference oReference = new Reference();
                    ContactInformation oContactInfo = new ContactInformation();
                    PhoneNumber oPhone = new PhoneNumber();
                    oReference.ReferenceId = intRefCnt;

                    //Get the person name from the xml
                    XmlNode referenceNode = node.SelectSingleNode("PersonName");
                    oReference.Name = GetInnerText(referenceNode, "FormattedName");

                    //Get the position title
                    oReference.Relation = GetInnerText(node, "PositionTitle");

                    //Get the comments
                    oReference.Organization = GetInnerText(node, "Comments");

                    //Get the contact node from the xml
                    XmlNode contactNode = node.SelectSingleNode("ContactMethod");
                    XmlNode phoneNode = contactNode.SelectSingleNode("Telephone");

                    //Get the telephone number from the contact node
                    oPhone.Mobile = GetInnerText(phoneNode, "FormattedNumber");

                    oContactInfo.Phone = oPhone;
                    oReference.ContactInformation = oContactInfo;
                    oListReference.Add(oReference);
                }
            }
            ReferencesControl oRefCntl = (ReferencesControl)DuplicateResumeManagement_mainResumeControl.
                FindControl("MainResumeControl_referencesControl");
            oRefCntl.DataSource = oListReference;
            oRefCntl.DataBind();
        }

        /// <summary>
        /// This method helps to extract the hrxml educational data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetEducation(XmlDocument oXmlDoc)
        {
            XmlNodeList oEducationNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/EducationHistory");
            if (oEducationNodeList.Count == 0)
            {
                return;
            }
            List<Education> oListEducation = new List<Education>();

            //Get the education list of the candidate
            //This will 
            XmlNodeList educationSchoolList = oXmlDoc.SelectNodes("//SchoolOrInstitution");

            int intEduCnt = 0;

            string innerXML = string.Empty;

            XmlNode nodeSchool = null;
            XmlNode nodeDegree = null;

            foreach (XmlNode node in educationSchoolList)
            {
                if (node.ChildNodes.Count == 0)
                {
                    continue;
                }

                Education oEducation = new Education();
                Location oLocation = new Location();

                //Get the school name of the candidate 
                nodeSchool = node.SelectSingleNode("School");
                innerXML = GetInnerText(nodeSchool, "SchoolName");

                //Separate the school name using , 
                string[] str = innerXML.Split(',');
                intEduCnt = intEduCnt + 1;
                oEducation.EducationId = intEduCnt;
                //Assign the school name
                oEducation.SchoolName = str[0];

                //If the length >1 assign location
                if (str.Length > 1)
                {
                    oLocation.City = innerXML.Remove(0, str[0].Length);
                    oEducation.SchoolLocation = oLocation;
                }

                nodeDegree = node.SelectSingleNode("Degree");

                //Get the degree name
                oEducation.DegreeName = GetInnerText(nodeDegree, "DegreeName");

                DateTime DtEduGradDate;

                //Get the degree date 
                XmlNode degreeDate = nodeDegree.SelectSingleNode("DegreeDate");

                string date = GetInnerText(degreeDate, "YearMonth");

                if (date.Length != 0)
                {
                    if ((DateTime.TryParse(date, out DtEduGradDate)))
                    {
                        oEducation.GraduationDate = DtEduGradDate;
                    }
                }

                XmlNode degreeNameNode = nodeDegree.SelectSingleNode("DegreeMajor");

                //Get the degree major 
                oEducation.Specialization = GetInnerText(degreeNameNode, "Name");

                //Get the degree comments
                oEducation.GPA = GetInnerText(nodeDegree, "Comments");
                oListEducation.Add(oEducation);
            }

            EducationControl oEduCntl = (EducationControl)DuplicateResumeManagement_mainResumeControl.FindControl("MainResumeControl_educationControl");
            oEduCntl.DataSource = oListEducation;
            oEduCntl.DataBind();
            SetEducationDate();
        }

        /// <summary>
        /// This method helps to set the date format empty in education.
        /// </summary>
        private void SetEducationDate()
        {
            EducationControl oEduCntl = (EducationControl)DuplicateResumeManagement_mainResumeControl.FindControl("MainResumeControl_educationControl");
            ListView EducationControl_listView = (ListView)oEduCntl.FindControl("EducationControl_listView");
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                if (txtEduGradDate.Text == "01/01/0001" || txtEduGradDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtEduGradDate.Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// This method helps to check the HRXML nodes
        /// </summary>
        /// <param name="oXmlDoc">
        /// A<see cref="XmlDocument"/>that holds the xml document
        /// </param>
        private void CheckHRXMLNodes(XmlDocument oXmlDoc)
        {
            //Get the list of lineage item
            List<LineageItem> lineageItem = new ResumeRepositoryBLManager().GetLineageItem();

            string nodeComment = string.Empty;

            foreach (LineageItem item in lineageItem)
            {
                if (item.Lineage == null) return;
                //for each lineage item check the node in oXmldoc
                XmlNode document = oXmlDoc.SelectSingleNode("//" + item.Lineage);

                if (document == null)
                {
                    //if document is null add this 
                    nodeComment += nodeComment.Contains(item.ComponentName.Trim()) ? "" : item.ComponentName + ", ";
                }
            }
            if (nodeComment.Trim().Length > 0)
            {
                if (nodeComment.Trim().Length < 2)
                    return;

                nodeComment = nodeComment.Remove(nodeComment.Length - 2, 2);

                //ShowMessage(ResumeEditor_topErrorMessageLabel,
                //  "Mismatch in these nodes : " + nodeComment);
            }
        }
        /// <summary>
        /// This method helps to extract the hrxml personal data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPersonalInfo(XmlDocument oXmlDoc, string resumeType)
        {
            XmlNodeList oPersonNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ContactInfo/PersonName");
            if (oPersonNodeList.Count > 0)
            {
                NameControl nc = (NameControl)DuplicateResumeManagement_mainResumeControl.FindControl("MainResumeControl_nameControl");
                TextBox txtFirstName = (TextBox)nc.FindControl("NameControl_firstNameTextBox");
                TextBox txtMiddleName = (TextBox)nc.FindControl("NameControl_middleNameTextBox");
                TextBox txtLastName = (TextBox)nc.FindControl("NameControl_lastNameTextBox");

                XmlNode personalDetail = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo");
                txtFirstName.Text = GetInnerText(personalDetail, "FirstName");
                txtMiddleName.Text = GetInnerText(personalDetail, "MiddleName");
                txtLastName.Text = GetInnerText(personalDetail, "LastName");

                if (resumeType == "TEMP")
                {
                    /*Assign Candidate Values*/
                    DuplicateResumeManagement_temporaryFirstNameTextBox.Text = txtFirstName.Text;
                    DuplicateResumeManagement_middleNameTextBox.Text = txtMiddleName.Text;
                    DuplicateResumeManagement_temporaryLastNameTextBox.Text = txtLastName.Text;
                }
            }

            XmlNodeList oContactInfoNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
            if (oContactInfoNodeList.Count > 0)
            {
                ContactInformationControl cil = (ContactInformationControl)DuplicateResumeManagement_mainResumeControl.FindControl("MainResumeControl_contactInfoControl");
                TextBox txtAddress = (TextBox)cil.FindControl("ContactInformationControl_streetTextBox");
                TextBox txtCity = (TextBox)cil.FindControl("ContactInformationControl_cityTextBox");
                TextBox txtState = (TextBox)cil.FindControl("ContactInformationControl_stateTextBox");
                TextBox txtCountry = (TextBox)cil.FindControl("ContactInformationControl_countryNameTextBox");
                TextBox txtPostalCode = (TextBox)cil.FindControl("ContactInformationControl_postalCodeTextBox");
                TextBox txtFixedLine = (TextBox)cil.FindControl("ContactInformationControl_fixedLineTextBox");
                TextBox txtMobile = (TextBox)cil.FindControl("ContactInformationControl_mobileTextBox");
                TextBox txtEmail = (TextBox)cil.FindControl("ContactInformationControl_emailTextBox");
                TextBox txtWebSite = (TextBox)cil.FindControl("ContactInformationControl_websiteTextBox");


                // Set the TechnicalSkills Value         
                TechnicalSkillsControl oTechCntl = (TechnicalSkillsControl)DuplicateResumeManagement_mainResumeControl.FindControl("MainResumeControl_technicalSkillsControl");
                TextBox txtLangauages = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_languageTextBox");
                TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_osTextBox");
                TextBox txtDatabases = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_databaseTextBox");
                TextBox txtUITools = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_uiToolsTextBox");
                TextBox txtOtherSkills = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_otherSkillsTextBox");

                XmlNode contactInfo = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
                XmlNode telephoneNode = contactInfo.SelectSingleNode("Telephone");
                txtMobile.Text = GetInnerText(telephoneNode, "FormattedNumber");

                XmlNode fixedLineNode = contactInfo.SelectSingleNode("Fax");
                txtFixedLine.Text = GetInnerText(fixedLineNode, "FormattedNumber");

                txtEmail.Text = GetInnerText(contactInfo, "InternetEmailAddress");
                txtWebSite.Text = GetInnerText(contactInfo, "InternetWebAddress");

                XmlNode postalAddress = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress");
                txtState.Text = GetInnerText(postalAddress, "State");
                txtAddress.Text = GetInnerText(postalAddress, "Street");
                txtCity.Text = GetInnerText(postalAddress, "Municipality");
                txtCountry.Text = GetInnerText(postalAddress, "CountryCode");
                txtPostalCode.Text = GetInnerText(postalAddress, "PostalCode");

                XmlNode techSkill = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/TechnicalSkills");
                txtLangauages.Text = GetInnerText(techSkill, "Languages");
                txtOperaSystem.Text = GetInnerText(techSkill, "OperatingSystem");
                txtDatabases.Text = GetInnerText(techSkill, "Databases");
                txtUITools.Text = GetInnerText(techSkill, "UITools");
                txtOtherSkills.Text = GetInnerText(techSkill, "OtherSkills");

                if (resumeType == "TEMP")
                {
                    DuplicateResumeManagement_temporaryEmailTextBox.Text = txtEmail.Text;
                    DuplicateResumeManagement_cellPhoneTextBox.Text = txtMobile.Text;

                    DuplicateResumeManagement_addressTextBox.Text = txtAddress.Text;
                    DuplicateResumeManagement_cityNameTextBox.Text = txtCity.Text;
                    DuplicateResumeManagement_stateNameTextBox.Text = txtState.Text;
                    DuplicateResumeManagement_countryTextBox.Text = txtCountry.Text;
                    DuplicateResumeManagement_postalCodeTextBox.Text = txtPostalCode.Text;
                    XmlNode homePhoneNode = contactInfo.SelectSingleNode("HomePhone");
                    DuplicateResumeManagement_homephoneTextBox.Text = GetInnerText(homePhoneNode, "FormattedNumber"); ;
                }
            }
            // Set the ExecutiveSummary Value
            XmlNodeList oExecSummaryNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ExecutiveSummary");
            if (oExecSummaryNodeList.Count > 0)
            {
                ExecutiveSummaryControl oExecCntl = (ExecutiveSummaryControl)DuplicateResumeManagement_mainResumeControl.FindControl("MainResumeControl_executiveSummaryControl");
                TextBox txtExecutiveSummary = (TextBox)oExecCntl.FindControl("ExecutiveSummaryControl_execSummaryTextBox");
                txtExecutiveSummary.Text = oExecSummaryNodeList.Item(0).InnerText;
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml projects data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetProjects(XmlDocument oXmlDoc)
        {
            //Select the nodes that are in the project 
            XmlNodeList oProjectNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ProjectHistory/Project");
            if (oProjectNodeList.Count == 0)
            {
                return;
            }
            List<Project> oListProject = new List<Project>();
            int intProjectCnt = 0;

            foreach (XmlNode node in oProjectNodeList)
            {
                Project oProject = new Project();
                Location oLocation = new Location();
                intProjectCnt = intProjectCnt + 1;
                oProject.ProjectId = intProjectCnt;
                //Get the project name from the xml
                oProject.ProjectName = GetInnerText(node, "ProjectName");

                //Get the project description
                oProject.ProjectDescription = GetInnerText(node, "Description");

                //Get the role
                oProject.Role = GetInnerText(node, "Role");

                //Get the position title
                oProject.PositionTitle = GetInnerText(node, "Position");

                //Get the project start date and end date
                DateTime dtProjectStartDate, dtProjectEndDate;
                string startDate = GetInnerText(node, "StartDate");
                string endDate = GetInnerText(node, "EndDate");
                if (startDate.Length > 0)
                {
                    //Parse the string into datetime
                    if ((DateTime.TryParse(startDate, out dtProjectStartDate)))
                    {
                        oProject.StartDate = dtProjectStartDate;
                    }
                }

                if (endDate.Length > 0)
                {
                    if ((DateTime.TryParse(endDate, out dtProjectEndDate)))
                    {
                        oProject.EndDate = dtProjectEndDate;
                    }
                }

                //Get the location
                oLocation.City = GetInnerText(node, "Location");
                oProject.ProjectLocation = oLocation;

                //Get the environment
                oProject.Environment = GetInnerText(node, "Environment");

                //Get the client name
                oProject.ClientName = GetInnerText(node, "ClientName");

                //Get the client industry
                oProject.ClientIndustry = GetInnerText(node, "ClientIndustry");
                oListProject.Add(oProject);
            }

            ProjectsControl oProjCntl = (ProjectsControl)DuplicateResumeManagement_mainResumeControl.
                FindControl("MainResumeControl_projectsControl");

            oProjCntl.DataSource = oListProject;
            oProjCntl.DataBind();
            SetProjectDate();
        }

        /// <summary>
        /// This method helps to set the date format empty in projects.
        /// </summary>
        private void SetProjectDate()
        {
            ProjectsControl oProjCntl = (ProjectsControl)DuplicateResumeManagement_mainResumeControl.FindControl("MainResumeControl_projectsControl");
            ListView oProjectsControl_listView = (ListView)oProjCntl.FindControl("ProjectsControl_listView");
            foreach (ListViewDataItem item in oProjectsControl_listView.Items)
            {
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                if (txtProjStartDate.Text == "01/01/0001" || txtProjStartDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjStartDate.Text = string.Empty;
                }
                if (txtProjEndDate.Text == "01/01/0001" || txtProjEndDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjEndDate.Text = string.Empty;
                }
            }
        }


        /// <summary>
        /// Represents the method to get the inner text of the node
        /// </summary>
        /// <param name="parentNode">
        /// A<see cref="XmlNode"/>that holds the Xml Node
        /// </param>
        /// <param name="xpath">
        /// A<see cref="string"/>that holds the Xpath
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the string
        /// </returns>
        private string GetInnerText(XmlNode parentNode, string xpath)
        {
            XmlNode node1 = parentNode.SelectSingleNode(".");
            XmlNode xmlNode = node1.SelectSingleNode(xpath);

            //xmlNode = parentNode.FirstChild.SelectSingleNode(xpath);

            string innerXML = string.Empty;

            if (xmlNode != null)
            {
                innerXML = xmlNode.InnerText.Trim();
            }
            return innerXML;
        }

        #endregion
 
    }
}