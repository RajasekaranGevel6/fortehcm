﻿#region Directives                                                   
using System;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Resources;
using System.Data;
using Forte.HCM.Common.DL;


#endregion

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class SearchCandidateRepository : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion Enum
        #endregion Private Constants

        #region Event Handlers                                                 

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckAndSetExpandorRestore();
                SearchCandidateRepository_firstnameTextBox.Focus();

                Page.Form.DefaultButton = SearchCandidateRepository_searchButton.UniqueID;
                // Master.SetPageCaption("Search Candidate");
                SearchCandidateRepository_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (SearchCandidateRepository_pageNavigator_PageNumberClick);

                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    Master.SetPageCaption("Associate Candidate(s)");
                    SearchCandidateRepository_headerLiteral.Text = "Associate Candidate(s)";
                }
                else
                {
                    Master.SetPageCaption("Search Candidate");
                    SearchCandidateRepository_headerLiteral.Text = "Search Candidate";
                }

                SearchCandidateRepository_bottomErrorMessageLabel.Text = string.Empty;
                SearchCandidateRepository_topErrorMessageLabel.Text = string.Empty;
                SearchCandidateRepository_topSuccessMessageLabel.Text = string.Empty;
                SearchCandidateRepository_bottomSuccessMessageLabel.Text = string.Empty;
                SearchCandidateRepository_createUserNameSuccessMessageLabel.Text = string.Empty;
                SearchCandidateRepository_createUserNameErrorMessageLabel.Text = string.Empty;


                UserRightsMatrix_assignRolesGridViewheaderTR.Attributes.Add("onclick",
                 "ExpandOrRestore('" +
                 SearchCandidateRepository_candidateDetailDIV.ClientID + "','" +
                 SearchCandidateRepository_searchDIV.ClientID + "','" +
                 SearchCandidateRepository_searchResultsUpSpan.ClientID + "','" +
                 SearchCandidateRepository_searchResultsDownSpan.ClientID + "','" +
                 SearchCandidateRepository_restoreHiddenField.ClientID + "','" +
                 RESTORED_HEIGHT + "','" +
                 EXPANDED_HEIGHT + "')");

                SearchCandidateRepository_positionProfileid_ImageButton.Attributes.Add("onclick", "javascript:ShowPositionProfileList('" + SearchCandidateRepository_positionProfile_NameTextBox.ClientID + "','" + SearchCandidateRepository_positionProfile_idHiddenField.ClientID + "');");

                if (!IsPostBack)
                {
                    // Apply default value.
                    ViewState["SORT_FIELD"] = "FirstName";
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                    SearchCandidateRepository_resumeShowCheckBox.Attributes.Add("onclick", "javascript:ShowCreatedByDiv('" + SearchCandidateRepository_resumeShowCheckBox.ClientID + "','" + SearchCandidateRepository_corporateUserDIV.ClientID + "');");
                    SearchCandidateRepository_corporateUserDIV.Style["display"] = "none";
                    SearchCandidateRepository_resumeShowCheckBox.Checked = true;
                    SearchCandidateRepository_candidateDetailDIV.Visible = false;
                    SearchCandidateRepository_pageNavigator.Visible = false;

                    SearchCandidateRepository_keywordShowMyCandidatesCheckBox.Attributes.Add("onclick", "javascript:ShowkeyWordCorpUserDiv('" + SearchCandidateRepository_keywordShowMyCandidatesCheckBox.ClientID + "','" + SearchCandidateRepository_keyWordShowMyCandiatesCorporateUserDIV.ClientID + "');");
                    SearchCandidateRepository_keyWordShowMyCandiatesCorporateUserDIV.Style["display"] = "none";
                    SearchCandidateRepository_keywordShowMyCandidatesCheckBox.Checked = true;


                    CandidateSearchCriteria candidateSearchType =
                    new ResumeRepositoryBLManager().GetCandidateTypeDetail(base.userID);
                    if (candidateSearchType != null)
                    {
                        SearchCandidateRepository_corporateUserDropDownList.DataSource = null;
                        SearchCandidateRepository_corporateUserDropDownList.DataBind();
                        SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.DataSource = null;
                        SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.DataBind();

                        SearchCandidateRepository_resumeUploadType_DropDownList.DataSource = null;
                        SearchCandidateRepository_resumeUploadType_DropDownList.DataBind();

                        if (candidateSearchType.candidateDetail != null)
                        {
                            SearchCandidateRepository_corporateUserDropDownList.DataSource = candidateSearchType.candidateDetail;
                            SearchCandidateRepository_corporateUserDropDownList.DataTextField = "FirstName";
                            SearchCandidateRepository_corporateUserDropDownList.DataValueField = "CandidateID";
                            SearchCandidateRepository_corporateUserDropDownList.DataBind();

                            SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.DataSource = candidateSearchType.candidateDetail;
                            SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.DataTextField = "FirstName";
                            SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.DataValueField = "CandidateID";
                            SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.DataBind();
                        }
                        if (candidateSearchType.attributesList != null)
                        {
                            SearchCandidateRepository_resumeUploadType_DropDownList.DataSource = candidateSearchType.attributesList;
                            SearchCandidateRepository_resumeUploadType_DropDownList.DataValueField = "AttributeID";
                            SearchCandidateRepository_resumeUploadType_DropDownList.DataTextField = "AttributeName";
                            SearchCandidateRepository_resumeUploadType_DropDownList.DataBind();
                        }
                    }

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU ||
                        Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.POSITION_PROFILE_REVIEW))
                    {
                        base.ClearSearchCriteriaSession();
                    }

                    if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    {
                        int positionProfileId = Convert.ToInt32(Request.QueryString["positionprofileid"]);
                        SearchCandidateRepository_positionProfile_idHiddenField.Value = positionProfileId.ToString();
                        PositionProfileDetail positionDetail = new PositionProfileBLManager().GetPositionProfile(positionProfileId);
                        SearchCandidateRepository_positionProfile_NameTextBox.Text = positionDetail.PositionName;

                        SearchCandidateRepository_advancedSearch_div.Style["display"] = "block";
                        SearchCandidateRepository_advancedSearchOption_Input.Checked = true;

                    }
                    else
                    {
                        SearchCandidateRepository_advancedSearch_div.Style["display"] = "none";
                        SearchCandidateRepository_advancedSearchOption_Input.Checked = false;

                    }
                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE] != null)
                    {
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE]
                            as CandidateInformation);
                    }
                    else
                    {
                        // Apply the default search.
                        LoadValues(1);
                    }

                    CheckAndSetExpandorRestore();
                }
                else
                {
                    if (!Utility.IsNullOrEmpty(SearchCandidateRepository_positionProfile_idHiddenField.Value))
                    {
                        SearchCandidateRepository_advancedSearch_div.Style["display"] = "block";
                        SearchCandidateRepository_advancedSearchOption_Input.Checked = true;
                    }
                    else
                    {
                        if (SearchCandidateRepository_advancedSearchOption_Input.Checked)
                        {
                            if (Utility.IsNullOrEmpty(SearchCandidateRepository_positionProfile_idHiddenField.Value))
                            {
                                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                                    SearchCandidateRepository_bottomErrorMessageLabel, "Select position profile");
                                SearchCandidateRepository_advancedSearch_div.Style["display"] = "block";
                                return;
                            }
                        }
                        else
                        {
                            SearchCandidateRepository_advancedSearch_div.Style["display"] = "none";
                            SearchCandidateRepository_advancedSearchOption_Input.Checked = false;
                        }
                    }
                }

                // Maintain div status.
                if (Utility.IsNullOrEmpty(SearchCandidateRepository_searchTypeHiddenField.Value) ||
                    SearchCandidateRepository_searchTypeHiddenField.Value.Trim().ToUpper() == "S")
                {
                    SearchCandidateRepository_simpleSearchDiv.Style["display"] = "block";
                    SearchCandidateRepository_keywordSearchDiv.Style["display"] = "none";
                    SearchCandidateRepository_simpleLinkButton.Text = "Keyword";
                    SearchCandidateRepository_simpleLinkButton.ToolTip = "Click here to do a keyword search";
                }
                else
                {
                    SearchCandidateRepository_simpleSearchDiv.Style["display"] = "none";
                    SearchCandidateRepository_keywordSearchDiv.Style["display"] = "block";
                    SearchCandidateRepository_simpleLinkButton.Text = "Simple";
                    SearchCandidateRepository_simpleLinkButton.ToolTip = "Click here to do a simple search";
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        void SearchCandidateRepository_pageNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                // Apply the search for the selected page number.
                LoadValues(e.PageNumber);

                if (!Utility.IsNullOrEmpty(SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField.Value))
                {
                    SearchCandidateRepository_associateCandidateDiv.Style["display"] = "block";
                }
                else
                    SearchCandidateRepository_associateCandidateDiv.Style["display"] = "none";
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void SearchCandidateRepository_createUserNamePanel_checkUserEmailIdAvailableButton_Click(
            object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text.Trim()))
                {
                    if (!IsValidEmailAddress(SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text.Trim()))
                    {
                        base.ShowMessage(SearchCandidateRepository_createUserNameErrorMessageLabel,
                                HCMResource.NewCandidate_EnterValidEmailID);
                        SearchCandidateRepository_createUserModalPopupExtender.Show();
                        return;
                    }

                    if (CheckForEmailAddressAvailability(SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text.Trim()))
                        SearchCandidateRepository_createUserNamePanel_validEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailAvailable;
                    else
                        SearchCandidateRepository_createUserNamePanel_inValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                    SearchCandidateRepository_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add("display", "block");

                    SearchCandidateRepository_createUserModalPopupExtender.Show();
                }
                else
                {
                    base.ShowMessage(SearchCandidateRepository_createUserNameErrorMessageLabel,
                                HCMResource.SearchCandidateRepository_PleaseEnterTheUsername);
                    SearchCandidateRepository_createUserModalPopupExtender.Show();
                    return;
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void SearchCandidateRepository_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if keyword is entered or not when keyword search type is selected.
                if (!Utility.IsNullOrEmpty(SearchCandidateRepository_searchTypeHiddenField.Value) &&
                    SearchCandidateRepository_searchTypeHiddenField.Value.Trim().ToUpper() == "K")
                {
                    if (SearchCandidateRepository_keywordTextBox.Text.Trim().Length == 0)
                    {
                        base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                            SearchCandidateRepository_bottomErrorMessageLabel, "Keyword cannot be empty");

                        return;
                    }
                }

                SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField.Value = "";
                string searchOption = SearchCandidateRepository_advancedSearchOption_HiddenField.Value;

                // Apply the default search for page number 1.
                LoadValues(1);

                if (!SearchCandidateRepository_resumeShowCheckBox.Checked)
                {
                    SearchCandidateRepository_corporateUserDIV.Style["display"] = "block";                     
                }
                else
                    SearchCandidateRepository_corporateUserDIV.Style["display"] = "none";

                if (searchOption == "Simple")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "reg", "javascript:return ShowAdvanceSearchDivRuntime()");
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void SearchCandidateRepository_candidateDetailGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                SearchCandidateRepository_pageNavigator.Reset();
                LoadValues(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void SearchCandidateRepository_candidateDetailGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                // Get parent page.
                string parentPage = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]))
                    parentPage = Request.QueryString["parentpage"];

                // Get position profile ID.
                string positionProfileID = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Request.QueryString["positionprofileid"];

                if (e.CommandName == "AddCandidateID")
                {
                    SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text = string.Empty;

                    SearchCandidateRepository_createUserNamePanel_passwordTextBox.Text = string.Empty;

                    SearchCandidateRepository_createUserNamePanel_retypePasswordTextBox.Text = string.Empty;

                    GridViewRow candidateGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);

                    SearchCandidateRepository_createUserNamePanel_firstNameHiddenField.Value =
                        ((Label)candidateGridViewRow.FindControl("SearchCandidateRepository_candidateDetailGridView_firstNameLabel")).Text;

                    SearchCandidateRepository_createUserNamePanel_lastNameHiddenField.Value =
                       ((Label)candidateGridViewRow.FindControl("SearchCandidateRepository_candidateDetailGridView_lastNameLabel")).Text;

                    SearchCandidateRepository_createUserNamePanel_candidateIDHiddenField.Value =
                       ((HiddenField)candidateGridViewRow.FindControl("SearchCandidateRepository_candidateDetailGridView_candidateIDHiddenField")).Value;

                    SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text = 
                        ((Label)candidateGridViewRow.FindControl("SearchCandidateRepository_candidateDetailGridView_emailNameLabel")).Text;

                    SearchCandidateRepository_createUserModalPopupExtender.Show();
                }
                else if (e.CommandName == "DeleteResume")
                {
                    GridViewRow candidateGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);
                    SearchCandidateRepository_createUserNamePanel_candidateResumeIDHiddenField.Value =
                       ((HiddenField)candidateGridViewRow.FindControl("SearchCandidateRepository_candidateDetailGridView_candidateResumeIDHiddenField")).Value;

                    // Show the delete confirmation message.
                    // Set message, title and type for the confirmation control for edit entry.
                    SearchCandidateRepository_deleteResume_confirmMessageControl.Message =
                        "Are you sure to delete the resume ?";
                    SearchCandidateRepository_deleteResume_confirmMessageControl.Title =
                        "Delete Resume";
                    SearchCandidateRepository_deleteResume_confirmMessageControl.Type =
                        MessageBoxType.YesNo;

                    SearchCandidateRepository_deleteResume_confirmModalPopupExtender.Show();
                    SearchCandidateRepository_deleteResumeUpdatePanel.Update();
                }
                else if (e.CommandName == "DeleteCandidate")
                {
                    int candidateID = Convert.ToInt32(e.CommandArgument.ToString());  

                    SearchCandidateRepository_deleteCandidateID_hiddenField.Value = candidateID.ToString();

                    // Show the delete confirmation message.
                    // Set message, title and type for the confirmation control for edit entry.
                    SearchCandidateRepository_deleteCandidate_confirmMessageControl.Message =
                        "Are you sure you want to delete the candidate ?";
                    SearchCandidateRepository_deleteCandidate_confirmMessageControl.Title =
                        "Delete Candidate";
                    SearchCandidateRepository_deleteCandidate_confirmMessageControl.Type =
                        MessageBoxType.YesNo;
                    SearchCandidateRepository_deleteCandidate_confirmModalPopupExtender.Show();
                    SearchCandidateRepository_deleteCandidateUpdatePanel.Update(); 
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void SearchCandidateRepository_candidateDetailGridView_RowCreated(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    int sortColumnIndex = GetSortColumnIndex
                        (SearchCandidateRepository_candidateDetailGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }

                if (e.Row.RowType != DataControlRowType.DataRow) return;
 
                if (!Utility.IsNullOrEmpty( SearchCandidateRepository_positionProfile_idHiddenField.Value))
                 {
                    HtmlInputCheckBox inputCheckBox = (HtmlInputCheckBox)e.Row.
                    FindControl("SearchCandidateRepository_candidateDetailGridView_asscociateCandidateCheckBox"); 

                    object datarow = e.Row.DataItem;

                    if (datarow == null) return;

                    CandidateInformation candidateInfo = (CandidateInformation)datarow;

                    if (!Utility.IsNullOrEmpty(candidateInfo.CandidateStatus) 
                        &&  candidateInfo.CandidateStatus=="Y") 
                        inputCheckBox.Visible = true; 
                    else
                        inputCheckBox.Visible = false;


                    if (!Utility.IsNullOrEmpty(SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField.Value))
                    {
                        string[] selectedCandidateList = SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField.Value.Split(',');

                        foreach (string candidateId in selectedCandidateList)
                        {
                            inputCheckBox.Visible = true;

                            if (candidateInfo.caiID.ToString() == candidateId.Trim())
                            {
                                inputCheckBox.Checked = true;
                            }
                        }
                    } 
                 }
                 
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }

        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchCandidateRepository_candidateDetailGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Get parent page.
                string parentPage = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]))
                    parentPage = Request.QueryString["parentpage"];

                // Get position profile ID.
                string positionProfileID = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Request.QueryString["positionprofileid"];

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                // Get candidate ID.
                int candidateID = Convert.ToInt32((e.Row.FindControl
                    ("SearchCandidateRepository_candidateDetailGridView_candidateIDHiddenField") as HiddenField).Value);

                #region Edit Candidate

                HyperLink SearchCandidateRepository_candidateDetailGridView_editCandidateHyperLink
                    = (HyperLink)e.Row.FindControl("SearchCandidateRepository_candidateDetailGridView_editCandidateHyperLink");

                if (SearchCandidateRepository_candidateDetailGridView_editCandidateHyperLink != null)
                {
                    string url = "~/ResumeRepository/CreateCandidate.aspx?m=0&s=1&parentpage=" +
                        Constants.ParentPage.SEARCH_CANDIDATE + "&candidateID=" + candidateID;

                    // Assign source page.
                    if (!Utility.IsNullOrEmpty(parentPage) && parentPage != Constants.ParentPage.MENU)
                        url += "&sourcepage=" + parentPage;

                    // Assign position profile ID.
                    if (!Utility.IsNullOrEmpty(positionProfileID))
                        url += "&positionprofileid=" + positionProfileID;

                    // Assign navigate url.
                    SearchCandidateRepository_candidateDetailGridView_editCandidateHyperLink.NavigateUrl = url;
                }

                #endregion Edit Candidate

                #region Edit Resume

                HyperLink SearchCandidateRepository_candidateDetailGridView_editResumeHyperLink
                    = (HyperLink)e.Row.FindControl("SearchCandidateRepository_candidateDetailGridView_editResumeHyperLink");

                if (SearchCandidateRepository_candidateDetailGridView_editResumeHyperLink != null)
                {
                    string url = "~/ResumeRepository/ResumeEditor.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_CANDIDATE + "&candidateid=" + candidateID + "&rowindex=0";

                    // Assign source page.
                    if (!Utility.IsNullOrEmpty(parentPage) && parentPage != Constants.ParentPage.MENU)
                        url += "&sourcepage=" + parentPage;

                    // Assign position profile ID.
                    if (!Utility.IsNullOrEmpty(positionProfileID))
                        url += "&positionprofileid=" + positionProfileID;

                    // Assign navigate url.
                    SearchCandidateRepository_candidateDetailGridView_editResumeHyperLink.NavigateUrl = url;
                }

                #endregion Edit Resume

                #region View Candidate Activity Log

                HyperLink SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogHyperLink
                    = (HyperLink)e.Row.FindControl("SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogHyperLink");

                if (SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogHyperLink != null)
                {
                    string url = "~/ResumeRepository/ViewCandidateActivityLog.aspx?m=0&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_CANDIDATE + "&candidateid=" + candidateID;

                    // Assign source page.
                    if (!Utility.IsNullOrEmpty(parentPage) && parentPage != Constants.ParentPage.MENU)
                        url += "&sourcepage=" + parentPage;

                    // Assign position profile ID.
                    if (!Utility.IsNullOrEmpty(positionProfileID))
                        url += "&positionprofileid=" + positionProfileID;

                    // Assign navigate url.
                    SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogHyperLink.NavigateUrl = url;
                }

                #endregion View Candidate Activity Log

                #region Add Notes

                // Assign events to 'add notes' icon.
                ImageButton SearchPositionProfileCandidate_candidateDetailGridView_addNotesImageButton = e.Row.FindControl
                    ("SearchCandidateRepository_candidateDetailGridView_addNotesImageButton") as ImageButton;

                SearchPositionProfileCandidate_candidateDetailGridView_addNotesImageButton.Attributes.Add("OnClick",
                   "javascript:return OpenAddNotesPopup('" + candidateID + "')");

                #endregion Add Notes

                #region View Candidate Activity Dashboard

                HyperLink SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityDashboardHyperLink
                    = (HyperLink)e.Row.FindControl("SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityDashboardHyperLink");

                if (SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityDashboardHyperLink != null)
                {
                    string url = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&candidateid=" + candidateID +
                        "&parentpage=" + Constants.ParentPage.SEARCH_CANDIDATE;

                    // Assign source page.
                    if (!Utility.IsNullOrEmpty(parentPage) && parentPage != Constants.ParentPage.MENU)
                        url += "&sourcepage=" + parentPage;

                    // Assign position profile ID.
                    if (!Utility.IsNullOrEmpty(positionProfileID))
                        url += "&positionprofileid=" + positionProfileID;

                    // Assign navigate url.
                    SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityDashboardHyperLink.NavigateUrl = url;
                }

                #endregion View Candidate Activity Dashboard

                #region Delete Candidate

                ImageButton SearchCandidateRepository_candidateDetailGridView_deleteCandidateIDImageButton =
                    e.Row.FindControl("SearchCandidateRepository_candidateDetailGridView_deleteCandidateIDImageButton") as ImageButton;

                if(!Utility.IsNullOrEmpty(Session["USER_DETAIL"])) 
                {                     
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                    if (userDetail.SubscriptionRole == "SR_COR_AMN")
                        SearchCandidateRepository_candidateDetailGridView_deleteCandidateIDImageButton.Visible = true;
                }

                #endregion Delete Candidate
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel, 
                    SearchCandidateRepository_bottomSuccessMessageLabel, exp.Message);
            }
        }

        protected void SearchCandidateRepository_createUserNamePanel_saveButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (CheckCandidateUserDetails())
                {
                    if (!string.IsNullOrEmpty
                       (SearchCandidateRepository_createUserNamePanel_passwordTextBox.Text.Trim()) &&
                       (!string.IsNullOrEmpty
                       (SearchCandidateRepository_createUserNamePanel_retypePasswordTextBox.Text.Trim())))
                    {
                        if (SearchCandidateRepository_createUserNamePanel_passwordTextBox.Text.Trim() !=
                            SearchCandidateRepository_createUserNamePanel_retypePasswordTextBox.Text.Trim())
                        {
                            base.ShowMessage(SearchCandidateRepository_createUserNameErrorMessageLabel,
                               HCMResource.SearchCandidateRepository_EnterCorrectPassword);
                            SearchCandidateRepository_createUserModalPopupExtender.Show();

                            return;
                        }

                        if (!CheckForEmailAddressAvailability
                            (SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text.Trim()))
                        {
                            base.ShowMessage(SearchCandidateRepository_createUserNameErrorMessageLabel,
                            HCMResource.UserRegistration_UserEmailNotAvailable);
                            SearchCandidateRepository_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add
                                ("display", "block");
                            SearchCandidateRepository_createUserModalPopupExtender.Show();
                            return;
                        }

                        if (!IsValidEmailAddress(SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text.Trim()))
                        {
                            base.ShowMessage(SearchCandidateRepository_createUserNameErrorMessageLabel,
                            HCMResource.NewCandidate_EnterValidEmailID);
                            SearchCandidateRepository_createUserModalPopupExtender.Show();
                            return;
                        }

                        SaveUserName();

                        ViewState["SORT_FIELD"] = "FirstName";
                        ViewState["SORT_ORDER"] = SortType.Ascending;
                        LoadValues(1);

                        base.ShowMessage(SearchCandidateRepository_topSuccessMessageLabel,
                            SearchCandidateRepository_bottomSuccessMessageLabel,
                            HCMResource.SearchCandidateRepository_UserNameCreatedSuccessfully);
                    }
                }
                else
                {
                    base.ShowMessage(SearchCandidateRepository_createUserNameErrorMessageLabel,
                                HCMResource.SearchCandidateRepository_EnterMandatoryField);
                    SearchCandidateRepository_createUserModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }

        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete resume confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the resume for the selected candidate
        /// </remarks>
        protected void SearchCandidateRepository_deleteResume_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Check if candidate resume ID available.
                if (Utility.IsNullOrEmpty(SearchCandidateRepository_createUserNamePanel_candidateResumeIDHiddenField.Value))
                {
                    base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                        SearchCandidateRepository_bottomErrorMessageLabel, "No resume found to delete");
                    return;
                }

                // Delete the resume.
                new CandidateBLManager().DeleteCandidateResume(Convert.ToInt32
                    (SearchCandidateRepository_createUserNamePanel_candidateResumeIDHiddenField.Value));

                // Reset the value.
                SearchCandidateRepository_createUserNamePanel_candidateResumeIDHiddenField.Value = null;

                // Reload the data.
                LoadValues(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
                if (!Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                    SearchCandidateRepository_pageNavigator.MoveToPage(Convert.ToInt32(ViewState["PAGE_NUMBER"])); 
                // Show a success message.
                base.ShowMessage(SearchCandidateRepository_topSuccessMessageLabel,
                    SearchCandidateRepository_bottomSuccessMessageLabel, "Candidate resume deleted successfully");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete resume confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the resume for the selected candidate
        /// </remarks>
        protected void SearchCandidateRepository_deleteCandidate_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Check if candidate resume ID available.
                if (Utility.IsNullOrEmpty(SearchCandidateRepository_deleteCandidateID_hiddenField.Value))
                {
                    base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                        SearchCandidateRepository_bottomErrorMessageLabel, "No candidate found to delete");
                    return;
                }

                // Delete the candidate.
              int transactionCount= new CandidateBLManager().DeleteCandidate(Convert.ToInt32
                    (SearchCandidateRepository_deleteCandidateID_hiddenField.Value));

                // Reset the value.
                SearchCandidateRepository_deleteCandidateID_hiddenField.Value = null;

                // Reload the data.
                LoadValues(Convert.ToInt32(ViewState["PAGE_NUMBER"]));

                if (!Utility.IsNullOrEmpty (ViewState["PAGE_NUMBER"]))
                    SearchCandidateRepository_pageNavigator.MoveToPage(Convert.ToInt32(ViewState["PAGE_NUMBER"])); 

                // Show a success message.
                if (transactionCount==0)
                    base.ShowMessage(SearchCandidateRepository_topSuccessMessageLabel,
                        SearchCandidateRepository_bottomSuccessMessageLabel, "Candidate deleted successfully");
                else
                    base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                        SearchCandidateRepository_bottomErrorMessageLabel, "Candidate is having test or interview activity and hence cannot be deleted");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void SearchCandidateRepository_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                base.ClearSearchCriteriaSession();
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void SearchCandidateRepository_associateCandidateButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(SearchCandidateRepository_positionProfile_idHiddenField.Value))
                {
                    base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, "Select position profile");
                    return;
                }

                PositionProfileCandidate positionProfileCandidate = null;

                if (!Utility.IsNullOrEmpty(SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField.Value))
                {
                    string candidateList = SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField.Value;

                    if (candidateList.LastIndexOf(',') != -1)
                        candidateList = candidateList.Remove(candidateList.LastIndexOf(','));


                    string[] selectedCandidateList = candidateList.Split(',');

                    IDbTransaction transaction = new TransactionManager().Transaction;
                    foreach (string candidateId in selectedCandidateList)
                    {
                        positionProfileCandidate = new PositionProfileCandidate();

                        positionProfileCandidate.CandidateStatus = "PCS_ASS";
                        positionProfileCandidate.RecruiterID = base.userID;
                        positionProfileCandidate.PositionProfileID = Convert.ToInt32(SearchCandidateRepository_positionProfile_idHiddenField.Value);
                        positionProfileCandidate.CandidateID = Convert.ToInt32(candidateId);
                        positionProfileCandidate.CandidateSessionID = null;
                        positionProfileCandidate.Status = null;
                        positionProfileCandidate.PickedBy = base.userID;
                        positionProfileCandidate.PickedDate = DateTime.Now;
                        positionProfileCandidate.SchelduleDate = null;
                        positionProfileCandidate.ResumeScore = null;
                        positionProfileCandidate.AttemptID = null;
                        positionProfileCandidate.SourceFrom = "N";
                        positionProfileCandidate.CreatedBy = base.userID;

                        new PositionProfileBLManager().InsertPositionProfileCandidates(positionProfileCandidate, transaction);
                    }
                    transaction.Commit();

                    // Apply the search for last searched page.
                    LoadValues(Convert.ToInt32(ViewState["PAGE_NUMBER"]));

                    base.ShowMessage(SearchCandidateRepository_topSuccessMessageLabel,
                    SearchCandidateRepository_bottomSuccessMessageLabel, "Candidates added under the position profile " + SearchCandidateRepository_positionProfile_NameTextBox.Text);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                    SearchCandidateRepository_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Loads the values.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void LoadValues(int pageNumber)
        {
            ViewState["PAGE_NUMBER"] = pageNumber;

            bool userCandidate = false;
            if (SearchCandidateRepository_resumeShowCheckBox.Checked)
                userCandidate = true;

            int recruiterID = 0;
            if (SearchCandidateRepository_corporateUserDIV.Visible)
            {
                if (SearchCandidateRepository_corporateUserDropDownList.SelectedItem.Value != null)
                {
                    recruiterID = int.Parse(SearchCandidateRepository_corporateUserDropDownList.SelectedItem.Value.ToString());
                }
            }

            if (userCandidate == true)
                recruiterID = base.userID;

            string resumeType = null;

            if (SearchCandidateRepository_resumeUploadType_DropDownList.SelectedIndex != -1)
            {
                if (SearchCandidateRepository_resumeUploadType_DropDownList.SelectedValue!=null )
                {
                    resumeType = SearchCandidateRepository_resumeUploadType_DropDownList.SelectedValue.ToString();
                    if (resumeType == "NONE") resumeType = null;
                }
            }

            int totalRecords = 0;
            string candidateStatus = null;

            List<CandidateInformation> candidateInformation = new List<CandidateInformation>();
             
            CandidateInformation candidateSearchInformation = new CandidateInformation();

            candidateSearchInformation.caiFirstName = SearchCandidateRepository_firstnameTextBox.Text.Trim();
            candidateSearchInformation.caiMiddleName = SearchCandidateRepository_middleNameTextBox.Text.Trim();
            candidateSearchInformation.caiLastName = SearchCandidateRepository_lastNameTextBox.Text.Trim();
            candidateSearchInformation.caiEmail = SearchCandidateRepository_emailTextBox.Text.Trim();
            candidateSearchInformation.SortField = ViewState["SORT_FIELD"].ToString();
            candidateSearchInformation.SortOrder = (SortType)ViewState["SORT_ORDER"];
            candidateSearchInformation.PageNumber = pageNumber;
            candidateSearchInformation.IsMaximized =
               SearchCandidateRepository_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

            candidateSearchInformation.caiType = SearchCandidateRepository_resumeUploadType_DropDownList.SelectedValue.ToString();
  
            if (!SearchCandidateRepository_resumeShowCheckBox.Checked)
            {
                candidateSearchInformation.RecruiterID = Convert.ToInt32(SearchCandidateRepository_corporateUserDropDownList.SelectedValue);
            }
 
            if (SearchCandidateRepository_candidateStatus_DropDownList.SelectedIndex == -1)
            {
                candidateStatus = null;
                candidateSearchInformation.caiIsActive = Convert.ToChar("A");
            }
            else if (SearchCandidateRepository_candidateStatus_DropDownList.SelectedIndex == 0)
            {
                candidateStatus = null;
                candidateSearchInformation.caiIsActive = Convert.ToChar("A");
            }
            else
            {
                candidateStatus = SearchCandidateRepository_candidateStatus_DropDownList.SelectedValue;
                candidateSearchInformation.caiIsActive = Convert.ToChar(candidateStatus);
            }

            DateTime fromDate=Convert.ToDateTime( "01/01/0001" );
            DateTime toDate = Convert.ToDateTime("01/01/0001"); ;

            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_resumeUpload_DateFromTextBox.Text))
            {
                candidateSearchInformation.FromDate = 
                    Convert.ToDateTime(SearchCandidateRepository_resumeUpload_DateFromTextBox.Text);
                
                fromDate=candidateSearchInformation.FromDate;
            }
            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_resumeUpload_DateToTextBox.Text))
            {
                candidateSearchInformation.ToDate =
                    Convert.ToDateTime(SearchCandidateRepository_resumeUpload_DateToTextBox.Text);

                toDate = candidateSearchInformation.ToDate;
            }
            if (Request.QueryString["positionprofileid"] != null)
            {
                candidateSearchInformation.PositionProfileID = Convert.ToInt32(Request.QueryString["positionprofileid"]);
                candidateSearchInformation.PositionName = SearchCandidateRepository_positionProfile_NameTextBox.Text; 
            }
            else if (!Utility.IsNullOrEmpty(SearchCandidateRepository_positionProfile_idHiddenField.Value))
            {
                candidateSearchInformation.PositionProfileID =
                    Convert.ToInt32(SearchCandidateRepository_positionProfile_idHiddenField.Value);
                candidateSearchInformation.PositionName = SearchCandidateRepository_positionProfile_NameTextBox.Text;
            }
            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_resumeUpload_DateFromTextBox.Text))
                candidateSearchInformation.FromDate = Convert.ToDateTime(SearchCandidateRepository_resumeUpload_DateFromTextBox.Text);

            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_resumeUpload_DateToTextBox.Text))
                candidateSearchInformation.ToDate = Convert.ToDateTime(SearchCandidateRepository_resumeUpload_DateToTextBox.Text);

            // Assign search type & keyword.
            candidateSearchInformation.SearchType = SearchCandidateRepository_searchTypeHiddenField.Value;
            candidateSearchInformation.Keyword = SearchCandidateRepository_keywordTextBox.Text;

            int kewyWordUserID = 0;
            if (SearchCandidateRepository_keywordShowMyCandidatesCheckBox.Checked == true ||
                SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.SelectedItem.Value == "0")
                kewyWordUserID = base.userID;
            else
                kewyWordUserID = int.Parse(SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.SelectedItem.Value.ToString());

            DateTime keywordFromDate = Convert.ToDateTime("01/01/0001");
            DateTime keywordToDate = Convert.ToDateTime("01/01/0001");

            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_keyWordResumeUpload_DateFromTextBox.Text))
            {
                keywordFromDate =
                    Convert.ToDateTime(SearchCandidateRepository_keyWordResumeUpload_DateFromTextBox.Text);
            }
            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_keyWordResumeUpload_DateToTextBox.Text))
            {
                keywordToDate =
                    Convert.ToDateTime(SearchCandidateRepository_keyWordResumeUpload_DateToTextBox.Text);
            }

            candidateSearchInformation.keywordFromDate = keywordFromDate;
            candidateSearchInformation.keywordToDate = keywordToDate;
            candidateSearchInformation.keywordUserID = kewyWordUserID;

             // Keep the search criteria in session.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE] = candidateSearchInformation;

            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_searchTypeHiddenField.Value) &&
                    SearchCandidateRepository_searchTypeHiddenField.Value.Trim().ToUpper() == "K")
            {
                // Search based on keyword.
                candidateInformation = new ResumeRepositoryBLManager().GetSearchCandidateInformation(
                    base.tenantID, kewyWordUserID, SearchCandidateRepository_keywordTextBox.Text.Trim(),
                    keywordFromDate,keywordToDate,
                    ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]), "Y", 
                    pageNumber, base.GridPageSize, candidateSearchInformation.PositionProfileID, out totalRecords);
            }
            else
            {
                // Search based on simple criteria.
                candidateInformation = new ResumeRepositoryBLManager().GetSearchCandidateInformation(base.tenantID,
                    SearchCandidateRepository_firstnameTextBox.Text.Trim(),
                    SearchCandidateRepository_middleNameTextBox.Text.Trim(),
                    SearchCandidateRepository_lastNameTextBox.Text.Trim(),
                    SearchCandidateRepository_emailTextBox.Text.Trim(),
                    ViewState["SORT_FIELD"].ToString(),
                    ((SortType)ViewState["SORT_ORDER"]),
                    pageNumber, base.GridPageSize,
                    Convert.ToString(recruiterID),
                    userCandidate,
                    resumeType,
                    candidateStatus,
                    fromDate, toDate, candidateSearchInformation.PositionProfileID,
                    out totalRecords);
            }
            

            if (candidateInformation == null || candidateInformation.Count == 0)
            {
                base.ShowMessage(SearchCandidateRepository_topErrorMessageLabel,
                   SearchCandidateRepository_bottomErrorMessageLabel, HCMResource.Common_Empty_Grid);

                SearchCandidateRepository_pageNavigator.TotalRecords = totalRecords;
                SearchCandidateRepository_candidateDetailGridView.DataSource = null;
                SearchCandidateRepository_candidateDetailGridView.DataBind();
            }

            SearchCandidateRepository_candidateDetailDIV.Visible = true;
            SearchCandidateRepository_pageNavigator.Visible = true;
            SearchCandidateRepository_pageNavigator.Reset();
            SearchCandidateRepository_pageNavigator.TotalRecords = totalRecords;
            SearchCandidateRepository_pageNavigator.PageSize = base.GridPageSize;

            if (candidateInformation != null)
            {
                SearchCandidateRepository_candidateDetailGridView.DataSource = candidateInformation;
                SearchCandidateRepository_candidateDetailGridView.DataBind();
            }

            if (!Utility.IsNullOrEmpty(candidateSearchInformation.PageNumber) &&
                candidateSearchInformation.PageNumber > 0)
                SearchCandidateRepository_pageNavigator.MoveToPage(candidateSearchInformation.PageNumber);

        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(SearchCandidateRepository_restoreHiddenField.Value) &&
                         SearchCandidateRepository_restoreHiddenField.Value == "Y")
            {
                SearchCandidateRepository_searchDIV.Style["display"] = "none";
                SearchCandidateRepository_searchResultsUpSpan.Style["display"] = "block";
                SearchCandidateRepository_searchResultsDownSpan.Style["display"] = "none";
                SearchCandidateRepository_candidateDetailDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchCandidateRepository_searchDIV.Style["display"] = "block";
                SearchCandidateRepository_searchResultsUpSpan.Style["display"] = "none";
                SearchCandidateRepository_searchResultsDownSpan.Style["display"] = "block";
                SearchCandidateRepository_candidateDetailDIV.Style["height"] = RESTORED_HEIGHT;
            }

            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE]))
                if (!Utility.IsNullOrEmpty(SearchCandidateRepository_restoreHiddenField.Value))
                    ((CandidateInformation)Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE]).IsMaximized =
                        SearchCandidateRepository_restoreHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        protected bool GetVisibility(string username)
        {
            return username.Length == 0 ? true : false;
        }

        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        protected bool GetAssociateCandidateVisibility(object val)
        {
            return Convert.ToInt32(val)== 0 ? true : false;
        }


        /// <summary>
        /// Saves the name of the user.
        /// </summary>
        private void SaveUserName()
        {
            CandidateInformation candidateInformation = new CandidateInformation();

            candidateInformation.caiFirstName = SearchCandidateRepository_createUserNamePanel_firstNameHiddenField.Value;
            candidateInformation.caiLastName = SearchCandidateRepository_createUserNamePanel_lastNameHiddenField.Value;
            candidateInformation.caiID = int.Parse(
           SearchCandidateRepository_createUserNamePanel_candidateIDHiddenField.Value);
            candidateInformation.UserName = SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text.Trim();
            candidateInformation.Password = new EncryptAndDecrypt().EncryptString(SearchCandidateRepository_createUserNamePanel_passwordTextBox.Text.Trim());
            string confirmationCode = GetConfirmationCode();
            new ResumeRepositoryBLManager().InsertCandidateUsers(base.tenantID, candidateInformation,
                base.userID, confirmationCode);
        }

        /// <summary>
        /// Checks the candidate user details.
        /// </summary>
        /// <returns></returns>
        private bool CheckCandidateUserDetails()
        {
            bool value = true;

            if (string.IsNullOrEmpty(SearchCandidateRepository_createUserNamePanel_userNameTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(SearchCandidateRepository_createUserNamePanel_passwordTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(SearchCandidateRepository_createUserNamePanel_retypePasswordTextBox.Text.Trim()))
            {
                value = false;
            }
            return value;
        }

        /// <summary>
        /// Checks for email address availability.
        /// </summary>
        /// <param name="User_Email">The user_ email.</param>
        /// <returns></returns>
        private bool CheckForEmailAddressAvailability(string User_Email)
        {
            return new UserRegistrationBLManager().CheckUserEmailExists(User_Email, base.tenantID);
        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// A Method that validates whether the user inputed username
        /// is valid email or not using regular expression
        /// </summary>
        /// <param name="strUserEmailId">
        /// A <see cref="System.String"/> that holds the email to validate
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the validity status
        /// </returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Fills the search criteria.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        private void FillSearchCriteria(CandidateInformation candidateInformation)
        {
            // Assign search type & keyword.
            SearchCandidateRepository_searchTypeHiddenField.Value = candidateInformation.SearchType;
            SearchCandidateRepository_keywordTextBox.Text = candidateInformation.Keyword;
            
            SearchCandidateRepository_firstnameTextBox.Text = candidateInformation.caiFirstName;
            SearchCandidateRepository_middleNameTextBox.Text = candidateInformation.caiMiddleName;
            SearchCandidateRepository_lastNameTextBox.Text = candidateInformation.caiLastName;
            SearchCandidateRepository_emailTextBox.Text = candidateInformation.caiEmail;
            ViewState["SORT_FIELD"] = candidateInformation.SortField;
            ViewState["SORT_ORDER"] = candidateInformation.SortOrder;
            if (Utility.IsNullOrEmpty(candidateInformation.caiIsActive)
                || candidateInformation.caiIsActive.ToString() =="A")
                SearchCandidateRepository_candidateStatus_DropDownList.SelectedIndex = 0;
            else if (candidateInformation.caiIsActive.ToString() == "Y" )
                SearchCandidateRepository_candidateStatus_DropDownList.SelectedIndex = 1;
            else if (candidateInformation.caiIsActive.ToString() == "N")
                SearchCandidateRepository_candidateStatus_DropDownList.SelectedIndex = 2;

            SearchCandidateRepository_restoreHiddenField.Value =
                candidateInformation.IsMaximized == true ? "Y" : "N";

            if (!Utility.IsNullOrEmpty(candidateInformation.RecruiterID))
            {
                if (candidateInformation.RecruiterID > 0)
                {
                    SearchCandidateRepository_resumeShowCheckBox.Checked = false;
                    SearchCandidateRepository_corporateUserDIV.Style["display"] = "block";
                    SearchCandidateRepository_corporateUserDropDownList.SelectedValue =
                       Convert.ToString(candidateInformation.RecruiterID);
                }
            }

            SearchCandidateRepository_positionProfile_idHiddenField.Value = candidateInformation.PositionProfileID.ToString();
            if (!Utility.IsNullOrEmpty(candidateInformation.PositionName))
                SearchCandidateRepository_positionProfile_NameTextBox.Text = candidateInformation.PositionName;

            if (candidateInformation.ToDate.ToShortDateString() != "1/1/0001")
                SearchCandidateRepository_resumeUpload_DateToTextBox.Text = candidateInformation.ToDate.ToShortDateString();

            if (candidateInformation.FromDate.ToShortDateString() != "1/1/0001")
                SearchCandidateRepository_resumeUpload_DateFromTextBox.Text = candidateInformation.FromDate.ToShortDateString();


            if (candidateInformation.keywordFromDate.ToShortDateString() != "1/1/0001")
                SearchCandidateRepository_keyWordResumeUpload_DateFromTextBox.Text = candidateInformation.keywordFromDate.ToShortDateString();

            if (candidateInformation.keywordToDate.ToShortDateString() != "1/1/0001")
                SearchCandidateRepository_keyWordResumeUpload_DateToTextBox.Text = candidateInformation.keywordToDate.ToShortDateString();

            if ((!Utility.IsNullOrEmpty(candidateInformation.keywordUserID)) && 
                candidateInformation.keywordUserID==base.userID)
            {
                SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.SelectedValue=
                    Convert.ToString(candidateInformation.keywordUserID);
                SearchCandidateRepository_keywordShowMyCandidatesCheckBox.Checked=true;
                SearchCandidateRepository_keyWordShowMyCandiatesCorporateUserDIV.Style["display"] = "none";
            }
            else
            {
                SearchCandidateRepository_keywordShowMyCandidatesCheckBox.Checked=false;
                SearchCandidateRepository_keyWordShowMyCandiatesCorporateUserDIV.Style["display"] = "block";
                SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList.SelectedValue=
                    Convert.ToString(candidateInformation.keywordUserID);
            }

            SearchCandidateRepository_resumeUploadType_DropDownList.SelectedValue
                = candidateInformation.caiType;

            LoadValues(candidateInformation.PageNumber);

            SearchCandidateRepository_pageNavigator.MoveToPage
                (candidateInformation.PageNumber);
        }
        #endregion

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the allow edit status.
        /// </summary>
        /// <param name="recruiterID">
        /// A <see cref="recruiterID"/> that holds the recruiter ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of allow edit 
        /// status. True represents allow edit and false do not allow
        /// </returns>
        /// <remarks>
        /// Corporate admin can edit all records. Other users can edit their
        /// own candidate only.
        /// </remarks>
        protected bool IsAllowEdit(string recruiterID)
        {
            // Check if user is corporate admin.
            if (((UserDetail)Session["USER_DETAIL"]).SubscriptionRole == 
                Enum.GetName(typeof(SubscriptionRolesEnum), 3))
                return true;
            else
                return (recruiterID.Trim() == base.userID.ToString().Trim() ? true : false);
        }

        /// <summary>
        /// Method that retrieves the allow delete resume status.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="recruiterID"/> that holds the recruiter ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of allow delete 
        /// status. True represents allow delete and false do not allow
        /// </returns>
        /// <remarks>
        /// Corporate admin can delete all candidate resume. Other users can 
        /// delete their own candidate resume only.
        /// </remarks>
        protected bool IsAllowDeleteResume(int candidateResumeID, int recruiterID)
        {
            // Check if candidate resume ID is valid.
            if (candidateResumeID == 0)
                return false;

            // Check if user is corporate admin.
            if (((UserDetail)Session["USER_DETAIL"]).SubscriptionRole ==
                Enum.GetName(typeof(SubscriptionRolesEnum), 3))
                return true;
            else
                return (recruiterID  == base.userID ? true : false);
        }

        #endregion Protected Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}