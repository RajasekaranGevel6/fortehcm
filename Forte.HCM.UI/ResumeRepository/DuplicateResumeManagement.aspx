﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    AutoEventWireup="true" CodeBehind="DuplicateResumeManagement.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.DuplicateResumeManagement" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<%@ Register Src="~/CommonControls/MainResumeControl.ascx" TagName="MainResumeControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="AddCandidateConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="DeleteCandidateConfirmMsgControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="AddResumeConfirmMsgControl"
    TagPrefix="uc5" %>
<asp:Content ID="Duplicate_resumeManagementContent" ContentPlaceHolderID="ResumeRepositoryMaster_body"
    runat="server">
    <script type="text/javascript">

        // Function that downloads the candidate resume.
        function DownloadResumeID(resumeID) {
            var url = '../Common/Download.aspx?id=' + resumeID;
            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');
            return false;
        }

        function OpenCandidateActivity(candiateid) {
            var height = 560;
            var width = 930;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
                + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/ViewCandidateActivityLogPopup.aspx?candidateid=" + candiateid;

            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }

        function CheckboxRadionButton(spanChk) {

            var IsChecked = spanChk.checked;
            if (IsChecked) {
                spanChk.parentElement.parentElement.style.backgroundColor = '#228b22';  // grdEmployees.SelectedItemStyle.BackColor
                spanChk.parentElement.parentElement.style.color = 'white'; // grdEmployees.SelectedItemStyle.ForeColor
            }
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('DuplicateResumeManagement_searchDuplicateResumeGridView');
            for (i = 0; i < Parent.rows.length; i++) {
                var tr = Parent.rows[i];
                var td = tr.childNodes[0];
                var item = td.firstChild;
                if (item.id != CurrentRdbID && item.type == "radio") {
                    if (item.checked) {
                        item.checked = false;
                        item.parentElement.parentElement.style.backgroundColor = 'white'; //grdEmployees.ItemStyle.BackColor
                        item.parentElement.parentElement.style.color = 'black'; //grdEmployees.ItemStyle.ForeColor
                    }
                }
            }
        }
        function pageScroll() {
            window.scrollBy(0, 50); // horizontal and vertical scroll increments
            scrolldelay = setTimeout('pageScroll()', 100); // scrolls every 100 milliseconds

            setInterval(stopScroll, 600);
             
        }
        function stopScroll() {
            clearTimeout(scrolldelay);
        }
      

    </script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td class="header_text_bold">
                            <asp:Label ID="DuplicateResumeManagement_titleLabel" runat="server" Text="Duplicate Resume Management"></asp:Label>
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeManagement_resetTopLinkButton" runat="server"
                                SkinID="sknActionLinkButton" OnClick="DuplicateResumeManagement_resetTopLinkButton_Click">Reset</asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeManagement_cancelTopLinkButton" runat="server"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect">Cancel</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="DuplicateResumeManagement_topMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="DuplicateResumeManagement_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="DuplicateResumeManagement_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID ="DuplicateResumeManagement_searchButton" />
                        <asp:AsyncPostBackTrigger ControlID ="DuplicateResumeManagement_deleteButton" />
                        <asp:AsyncPostBackTrigger ControlID ="DuplicateResumeManagement_SaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="DuplicateResumeManagement_displayProfileDetails" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="panel_inner_body_bg">
                                                        <table width="100%" cellpadding="2" cellspacing="3">
                                                            <tr>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="DuplicateResumeManagement_profileNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Candidate Name"></asp:Label>
                                                                </td>
                                                                <td style="width: 70px">
                                                                    <asp:Label ID="DuplicateResumeManagement_uploadedOnLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Uploaded On"></asp:Label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="DuplicateResumeManagement_recruitedByLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Uploaded By"></asp:Label>
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:Label ID="DuplicateResumeManagement_statusLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Status"></asp:Label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="DuplicateResumeManagement_reasonLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Reason"></asp:Label>
                                                                </td>
                                                                <td rowspan="2" width="50px" align="left" valign="middle">
                                                                    <asp:Button ID="DuplicateResumeManagement_deleteButton" Height="25px" runat="server"
                                                                        Text="Delete" SkinID="sknButtonId" OnClick="DuplicateResumeManagement_deleteButton_Click" />
                                                                    <asp:Panel ID="DuplicateResumeManagement_deleteTemporaryCandidatePanel" runat="server"
                                                                        Style="display: none" CssClass="popupcontrol_confirm_remove">
                                                                        <uc4:DeleteCandidateConfirmMsgControl ID="DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl"
                                                                            runat="server" OnCancelClick="DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl_cancelClick"
                                                                            OnOkClick="DuplicateResumeManagement_deleteTemporaryCandidateConfirmMsgControl_okClick" />
                                                                    </asp:Panel>
                                                                    <ajaxToolKit:ModalPopupExtender ID="DuplicateResumeManagement_deleteTemporaryCandidateModalPopupExtender"
                                                                        runat="server" PopupControlID="DuplicateResumeManagement_deleteTemporaryCandidatePanel"
                                                                        TargetControlID="DuplicateResumeManagement_deleteButton" BackgroundCssClass="modalBackground">
                                                                    </ajaxToolKit:ModalPopupExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="DuplicateResumeManagement_displayCandidateNameLabelValue" runat="server"
                                                                        SkinID="sknLabelFieldText" Text="Sarah"></asp:Label>
                                                                </td>
                                                                <td style="width: 70px">
                                                                    <asp:Label ID="DuplicateResumeManagement_lastUploadedDateLabel" runat="server" SkinID="sknLabelFieldText"
                                                                        Text="03/30/2012 "></asp:Label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="DuplicateResumeManagement_lastUploadedByLabel" runat="server" SkinID="sknLabelFieldText"
                                                                        Text="Prabu"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="DuplicateResumeManagement_uploadedResumeStausLabel" runat="server"
                                                                        SkinID="sknLabelFieldText" Text="Incomplete"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="DuplicateResumeManagement_uploadedResumeReasonLabel" runat="server"
                                                                        SkinID="sknLabelFieldText" Text="Last Name<br>Phone Number"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="49%" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="panel_bg">
                                            <tr>
                                                <td colspan="4" class="panel_inner_body_bg">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 2px">
                                                        <tr>
                                                            <td style="padding-left: 10px">
                                                                <asp:Label ID="DuplicateResumeManagement_firstNameLabel" SkinID="sknLabelFieldHeaderText"
                                                                    runat="server" Text="First Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_firstNameTextBox" runat="server" SkinID="sknHomePageTextBox" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="DuplicateResumeManagement_lastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Last Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_lastNameTextBox" SkinID="sknHomePageTextBox"
                                                                    runat="server" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-left: 10px">
                                                                <asp:Label ID="DuplicateResumeManagement_emailLabel" SkinID="sknLabelFieldHeaderText"
                                                                    runat="server" Text="Email"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_emailTextBox" runat="server" SkinID="sknHomePageTextBox" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="DuplicateResumeManagement_phoneLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Phone"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_phoneTextBox" SkinID="sknHomePageTextBox" MaxLength="20"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="65%">
                                                    &nbsp;
                                                </td>
                                                <td width="15%">
                                                    <asp:Button ID="DuplicateResumeManagement_searchButton" runat="server" Text="Search"
                                                        SkinID="sknButtonId" OnClick="DuplicateResumeManagement_searchButton_Click" />
                                                </td>
                                                <td width="10%">
                                                    <asp:LinkButton ID="DuplicateResumeManagement_clearLinkButton" SkinID="sknActionLinkButton"
                                                        runat="server" OnClick="DuplicateResumeManagement_clearLinkButton_Click">Clear</asp:LinkButton>
                                                </td>
                                                <td width="10%">
                                                    <asp:LinkButton ID="DuplicateResumeManagement_autoLoadLinkButton" SkinID="sknActionLinkButton"
                                                        runat="server" OnClick="DuplicateResumeManagement_autoLoadLinkButton_Click">Auto Load</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="49%" rowspan="3" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="DuplicateResumeManagement_candidateDetailTitleLabel" runat="server"
                                                                    SkinID="sknLabelFieldHeaderText" Text="Candidate Details "></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="panel_inner_body_bg">
                                                    <asp:UpdatePanel ID="DuplicateResumeManagement_resumeListUpdatePanel" runat="server"  UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="20%">
                                                                        <asp:Label ID="DuplicateResumeManagement_useIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="UserID"></asp:Label>
                                                                    </td>
                                                                    <td width="2%">
                                                                    </td>
                                                                    <td width="50%">
                                                                        <asp:Label ID="DuplicateResumeManagement_useIDValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                            Text=""></asp:Label>
                                                                    </td>
                                                                    <td width="28%" align="right">
                                                                        <asp:ImageButton ID="SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogImageButton"
                                                                            runat="server" CommandName="ViewCandidateActivityLog" SkinID="sknViewCandidateActivityLogImageButton"
                                                                            ToolTip="View Candidate Activity Log" />&nbsp;&nbsp;
                                                                        <asp:HyperLink ID="UploadedResumeStatus_candidateDashboardHyperLink" ToolTip="View candidate activity dashboard"
                                                                            runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/can_dashboard.gif" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="20%">
                                                                        <asp:Label ID="DuplicateResumeManagement_candidateNameLabel" runat="server" Text="Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td width="2%">
                                                                    </td>
                                                                    <td width="50%">
                                                                        <asp:Label ID="DuplicateResumeManagement_candidateNameValueLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                    </td>
                                                                    <td width="28%">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="20%">
                                                                        <asp:Label ID="DuplicateResumeManagement_candidateEmailLabel" runat="server" Text="Email"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td width="2%">
                                                                    </td>
                                                                    <td width="50%">
                                                                        <asp:Label ID="DuplicateResumeManagement_candidateEmailValueLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                    </td>
                                                                    <td width="28%">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="20%">
                                                                        <asp:Label ID="DuplicateResumeManagement_candidatePhoneNoLabel" runat="server" Text="Phone"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td width="2%">
                                                                    </td>
                                                                    <td width="50%">
                                                                        <asp:Label ID="DuplicateResumeManagement_candidatePhoneNoValueLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                    </td>
                                                                    <td width="28%">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="95%" align="center">
                                                                            <tr>
                                                                                <td class="header_bg">
                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="DuplicateResumeManagement_resumeListLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                    Text="List of Resume(s)"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" class="panel_inner_body_bg">
                                                                                    <div style="height: 175px; overflow: auto">
                                                                                        <asp:GridView ID="DuplicateResumeManagement_resumeListGridView" runat="server" AutoGenerateColumns="False"
                                                                                            Width="100%" OnRowCommand="DuplicateResumeManagement_resumeListGridView_RowCommand"
                                                                                            OnRowDataBound="DuplicateResumeManagement_resumeListGridView_RowDataBound">
                                                                                            <Columns>
                                                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="DuplicateResumeManagement_resumeListGridView_candidateIDHiddenField"
                                                                                                            runat="server" Value='<%# Eval("CandidateId") %>' />
                                                                                                        <asp:HiddenField ID="DuplicateResumeManagement_resumeListGridView_resumeIDHiddenField"
                                                                                                            runat="server" Value='<%# Eval("CandidateResumeId") %>' />
                                                                                                        <asp:RadioButton ID="DuplicateResumeManagement_resumeListGridView_selectCandidateRadioButton"
                                                                                                            runat="server" ToolTip="Select candidate to map" AutoPostBack="true" OnCheckedChanged="DuplicateResumeManagement_resumeListGridView_selectCandidateRadioButton_CheckedChanged" />
                                                                                                        <asp:ImageButton ID="DuplicateResumeManagement_resumeListGridView_downloadImageButton"
                                                                                                            runat="server" Text="Download" SkinID="sknDownloadImageButton" ToolTip="Download Resume"
                                                                                                            AlternateText="Download" CommandName="DownloadResume" />
                                                                                                        <asp:ImageButton ID="DuplicateResumeManagement_resumeListGridView_deleteCandidate"
                                                                                                            runat="server" SkinID="sknDeleteImageButton" ToolTip="Delete Candidate" CommandName="DeleteResume" />
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Resume Title" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                                                                    <ItemTemplate>
                                                                                                        <div id="DuplicateResumeManagement_resumeListGridView_resumeTitleDiv" runat="server"
                                                                                                            style="width: 120px; word-wrap: break-word; white-space: normal" innertext='<%# Eval("FileName") %>'>
                                                                                                        </div>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle Width="20%"></HeaderStyle>
                                                                                                    <ItemStyle Wrap="true" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Uploaded By" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                                                                    HeaderStyle-Width="20%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="DuplicateResumeManagement_resumeListGridView_uploadedByLabel" runat="server"
                                                                                                            Text='<%# Eval("Recruiter") %>'>
                                                                                                        </asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                                                                    <ItemStyle Wrap="true" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Uploaded Date" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                                                                    HeaderStyle-Width="20%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="DuplicateResumeManagement_resumeListGridView_uploadedDateLabel" runat="server"
                                                                                                            Text='<%# GetDateFormat(Convert.ToDateTime(Eval("ProcessedDate"))) %>'>
                                                                                                        </asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                                                                    <ItemStyle Wrap="true" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="DuplicateResumeManagement_searchDuplicateResumeGridView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="49%" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td width="22%">
                                                                <asp:Label ID="DuplicateResumeManagement_searchResultsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Search Results "></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="DuplicateResumeManagement_searchResultsSortLabel" runat="server" SkinID="sknContactLabelText"
                                                                     ></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="panel_inner_body_bg" valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div style="height: 175px; overflow: auto">
                                                                    <asp:UpdatePanel ID="DuplicateResumeManagement_searchDuplicateResumeUpdatePanel"
                                                                        UpdateMode="Conditional" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="DuplicateResumeManagement_searchDuplicateResumeGridView" runat="server"
                                                                                AutoGenerateColumns="False" Width="100%">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="DuplicateResumeManagement_searchDuplicateResumeGridView_candidateIDHiddenField"
                                                                                                runat="server" Value='<%# Eval("CandidateId") %>' />
                                                                                            <asp:RadioButton ID="DuplicateResumeManagement_searchDuplicateResumeGridView_selectCandidateRadioButton"
                                                                                                runat="server" ToolTip="Select candidate to map" GroupName="DuplicateCandidateResume"
                                                                                                ValidationGroup="DuplicateCandidateResume" AutoPostBack="true" OnCheckedChanged="DuplicateResumeManagement_searchDuplicateResumeGridView_selectCandidateRadioButton_CheckedChanged" />
                                                                                            <asp:ImageButton ID="DuplicateResumeManagement_searchDuplicateResumeGridView_deleteCandidate"
                                                                                                runat="server" SkinID="sknDeleteImageButton" ToolTip="Delete Candidate" Visible="false" />
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="First Name" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="DuplicateResumeManagement_searchDuplicateResumeGridView_firstNameLabel"
                                                                                                runat="server" Text='<%# Eval("FirstName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle Width="20%"></HeaderStyle>
                                                                                        <ItemStyle Wrap="true" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Last Name" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="DuplicateResumeManagement_searchDuplicateResumeGridView_lastNameLabel"
                                                                                                runat="server" Text='<%# Eval("LastName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                                                        <ItemStyle Wrap="true" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Uploaded By" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="DuplicateResumeManagement_searchDuplicateResumeGridView_uploadedByLabel"
                                                                                                runat="server" Text='<%# Eval("UploadedByName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                                                        <ItemStyle Wrap="true" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Uploaded Date" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="DuplicateResumeManagement_searchDuplicateResumeGridView_uploadedDateLabel"
                                                                                                runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("UploadedDate"))) %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                                                        <ItemStyle Wrap="true" />
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="DuplicateResumeManagement_searchButton" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc2:PageNavigator ID="DuplicateResumeManagement_searchCandidatepageNavigator" runat="server" />
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="49%" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td width="22%">
                                                                <asp:Label ID="DuplicateResumeManagement_uploadedResumeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Uploaded Resume"></asp:Label>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="panel_inner_body_bg" valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="96%" align="center">
                                                        <tr>
                                                            <td>
                                                                <div style="display: none">
                                                                    <uc1:MainResumeControl ID="DuplicateResumeManagement_mainResumeControl" runat="server" />
                                                                </div>
                                                                <div runat="server" id="DuplicateResumeManagement_uploadedResumeDiv" style="overflow: auto;
                                                                    height: 220px" class="resume_ListViewNormalStyle">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="DuplicateResumeManagement_newCandidateButton" SkinID="sknButtonId"
                                                                    runat="server" Text="Create New Candidate" OnClick="DuplicateResumeManagement_newCandidateButton_Click" />
                                                               
                                                                <%--<asp:Panel ID="DuplicateResumeManagement_newCandidatePopupPanel" runat="server" Style="display: none"
                                                                    CssClass="client_confirm_message_box">
                                                                    <uc3:AddCandidateConfirmMsgControl ID="DuplicateResumeManagement_newCandidateConfirmMsgControl"
                                                                        runat="server" OnCancelClick="DuplicateResumeManagement_newCandidateConfirmMsgControl_cancelClick"
                                                                        OnOkClick="DuplicateResumeManagement_newCandidateConfirmMsgControl_okClick" />
                                                                </asp:Panel>
                                                                <ajaxToolKit:ModalPopupExtender ID="DuplicateResumeManagement_newCandidateModalPopupExtender"
                                                                    runat="server" PopupControlID="DuplicateResumeManagement_newCandidatePopupPanel"
                                                                    TargetControlID="DuplicateResumeManagement_newCandidateButton" BackgroundCssClass="modalBackground">
                                                                </ajaxToolKit:ModalPopupExtender>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="49%" rowspan="3" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td width="22%">
                                                                <asp:Label ID="DuplicateResumeManagement_existingResumeTitleLabel" runat="server"
                                                                    SkinID="sknLabelFieldHeaderText" Text="Existing Resume"></asp:Label>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="panel_inner_body_bg" valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="96%">
                                                        <tr>
                                                            <td height="220px">
                                                                <asp:UpdatePanel ID="DuplicateResumeManagement_existingResumeUpdatePanel" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate> 
                                                                        <div runat="server" id="DuplicateResumeManagement_existingResumeDiv" style="overflow: auto;
                                                                            border: 1px; height: 220px" class="resume_ListViewNormalStyle">
                                                                        </div> 
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="DuplicateResumeManagement_resumeListGridView" />
                                                                        <asp:AsyncPostBackTrigger ControlID ="DuplicateResumeManagement_searchDuplicateResumeGridView" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="DuplicateResumeManagement_newResumeButton" SkinID="sknButtonId" runat="server"
                                                                    Text="Add New Resume" OnClick="DuplicateResumeManagement_newResumeButton_Click" /> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                 <asp:Panel ID="DuplicateResumeManagement_newResumePopupPanel" runat="server" Style="display: none"
                                                                    CssClass="popupcontrol_confirm_remove">
                                                                    <uc5:AddResumeConfirmMsgControl ID="DuplicateResumeManagement_newResumeConfirmMsgControl" 
                                                                        runat="server" OnCancelClick="DuplicateResumeManagement_newResumeConfirmMsgControl_cancelClick"
                                                                        OnOkClick="DuplicateResumeManagement_newResumeConfirmMsgControl_okClick" />
                                                                </asp:Panel>
                                                                <ajaxToolKit:ModalPopupExtender ID="DuplicateResumeManagement_newResumeModalPopupExtender"
                                                                    runat="server" PopupControlID="DuplicateResumeManagement_newResumePopupPanel"
                                                                    TargetControlID="DuplicateResumeManagement_newResumeButton" BackgroundCssClass="modalBackground">
                                                                </ajaxToolKit:ModalPopupExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr runat="server" id="DuplicateResumeManagement_candidateInfoTR" style="display:none">
                        <td>
                           <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="header_bg">
                                          <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td width="22%">
                                                    <asp:Label ID="DuplicateResumeManagement_candidateInfoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                        Text="Candidate Information"></asp:Label>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_inner_body_bg" valign="top" >
                                         <asp:UpdatePanel ID="DuplicateResumeManagement_bodyUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <table width="965px" cellpadding="2" cellspacing="3" style="padding-left:120px;padding-right:120px">
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" valign="top">
                                                                <asp:Label ID="DuplicateResumeManagement_firstNameHeaderLabel" runat="server" Text="First Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                            </td> 
                                                            <td style="width: 10%" valign="top">
                                                                <asp:TextBox ID="DuplicateResumeManagement_temporaryFirstNameTextBox" runat="server" MaxLength="50"
                                                                    Width="200px" TabIndex="1"></asp:TextBox>
                                                            </td> 
                                                            <td width="20%">&nbsp;</td>
                                                            <td style="width: 10%" valign="top">
                                                                <asp:Label ID="DuplicateResumeManagement_addressLabel" runat="server" Text="Street" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td  valign="top" style="width:15%"  align="left">
                                                                <asp:TextBox ID="DuplicateResumeManagement_addressTextBox" runat="server" TextMode="SingleLine"
                                                                      MaxLength="100"  Width="200px"  onkeyup="CommentsCount(100,this)"
                                                                    onchange="CommentsCount(100,this)" TabIndex="8"></asp:TextBox>
                                                            </td> 
                                                        </tr>
                                                        <tr>
                                                            <td  >
                                                                <div style="float: left">
                                                                    <asp:Label ID="DuplicateResumeManagement_middleNameLabel" runat="server" Text="Middle Name"
                                                                        SkinID="sknLabelFieldHeaderText" MaxLength="50"></asp:Label>
                                                                </div> 
                                                            </td>
                                                           
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_middleNameTextBox" runat="server" MaxLength="50"
                                                                    Width="200px" TabIndex="2"></asp:TextBox>
                                                            </td> 
                                                            <td width="55%">&nbsp;</td>
                                                            <td>
                                                              <asp:Label ID="DuplicateResumeManagement_cityNameLabel" runat="server" Text="City"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                              <asp:TextBox ID="DuplicateResumeManagement_cityNameTextBox" runat="server" MaxLength="50"
                                                                    Width="200px" TabIndex="2"></asp:TextBox>
                                                            </td>
                                                           
                                                        </tr>
                                                        <tr>
                                                            <td  >
                                                                <asp:Label ID="CDuplicateResumeManagement_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;<span
                                                                    class="mandatory" >*</span>
                                                            </td>
                                                            
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_temporaryLastNameTextBox" runat="server" MaxLength="50" Width="200px"
                                                                    TabIndex="3"></asp:TextBox>
                                                            </td> 
                                                            <td width="55%">&nbsp;</td>
                                                            <td>
                                                              <asp:Label ID="Label1" runat="server" Text="State"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                              <asp:TextBox ID="DuplicateResumeManagement_stateNameTextBox" runat="server" MaxLength="50"
                                                                    Width="200px" TabIndex="2"></asp:TextBox>
                                                            </td>                                                   
                                                        </tr>
                                                        <tr>
                                                            <td  >
                                                                <asp:Label ID="DuplicateResumeManagement_temporaryEmailLabel" runat="server" Text="Email" 
                                                                SkinID="sknLabelFieldHeaderText"  MaxLength="50"></asp:Label>&nbsp;<span
                                                                    class="mandatory">*</span>
                                                            </td>
                                                           
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_temporaryEmailTextBox" runat="server" MaxLength="100" Width="200px"
                                                                    TabIndex="4"></asp:TextBox>
                                                            </td>
                                                            <td width="55%">&nbsp;</td>
                                                            <td>
                                                                 <asp:Label ID="DuplicateResumeManagement_countryLabel" runat="server" 
                                                                    SkinID="sknLabelFieldHeaderText" Text="Country"></asp:Label> </td>
                                                            <td> 
                                                             <asp:TextBox ID="DuplicateResumeManagement_countryTextBox" runat="server" 
                                                                    MaxLength="20" TabIndex="9" Width="200px"></asp:TextBox>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>  
                                                                <asp:Label ID="DuplicateResumeManagement_homephoneLabel" runat="server" 
                                                                    SkinID="sknLabelFieldHeaderText" Text="Home Phone"></asp:Label> 
                                                            </td>
                                                            <td>  
                                                                <asp:TextBox ID="DuplicateResumeManagement_homephoneTextBox" runat="server" 
                                                                    MaxLength="20" TabIndex="9" Width="200px"></asp:TextBox> 
                                                            </td>
                                                           <td width="55%">&nbsp;</td>
                                                            <td>
                                                                <asp:Label ID="DuplicateResumeManagement_postalCodeLabel" runat="server" SkinID="sknLabelFieldHeaderText" 
                                                                    Text="Postal Code"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="DuplicateResumeManagement_postalCodeTextBox" runat="server" MaxLength="20" TabIndex="10" 
                                                                    Width="200px"></asp:TextBox>
                                                            </td>
                                                        </tr> 

                                                        <tr>
                                                            <td  >  
                                                                <asp:Label ID="DuplicateResumeManagement_cellPhoneLabel" runat="server" 
                                                                    SkinID="sknLabelFieldHeaderText" Text="Cell Phone"></asp:Label>
                                                            </td>
                                                            <td>  
                                                                <asp:TextBox ID="DuplicateResumeManagement_cellPhoneTextBox" runat="server" 
                                                                    MaxLength="20" TabIndex="10" Width="200px"></asp:TextBox> 
                                                            </td>
                                                           <td width="55%">&nbsp;</td>
                                                            <td>
                                                                &nbsp;</td>
                                                            <td>
                                                                &nbsp;</td>
                                                        </tr> 

                                                        <tr>
                                                            <td  colspan="5" height="22px">
                                                                  <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td align="right">
                                                                             <asp:Button ID="DuplicateResumeManagement_SaveButton" runat="server" 
                                                                                 Text="Save" SkinID="sknButtonId" 
                                                                                 onclick="DuplicateResumeManagement_SaveButton_Click"/>
                                                                        </td>
                                                                        <td width="1%"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                    </td>
                                </tr>
                           </table> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="DuplicateResumeManagement_bottomMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="DuplicateResumeManagement_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="DuplicateResumeManagement_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID ="DuplicateResumeManagement_searchButton" />
                        <asp:AsyncPostBackTrigger ControlID ="DuplicateResumeManagement_deleteButton" />
                        <asp:AsyncPostBackTrigger ControlID ="DuplicateResumeManagement_SaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
         <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td class="header_text_bold">
                            &nbsp;
                            
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeManagement_bottomResetLinkButton" runat="server"
                                SkinID="sknActionLinkButton" OnClick="DuplicateResumeManagement_bottomResetLinkButton_Click">Reset</asp:LinkButton>
                        </td>
                        <td align="center" class="link_button" width="3%"> 
                            |
                        </td>
                        <td width="3%">
                            <asp:LinkButton ID="DuplicateResumeManagement_bottomCancelLinkButton" runat="server"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect">Cancel</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
