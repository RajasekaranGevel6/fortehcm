﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    AutoEventWireup="true" CodeBehind="NodeManager.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.NodeManager" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="NodeManager_bodyContent" ContentPlaceHolderID="ResumeRepositoryMaster_body"
    runat="server">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <asp:Literal ID="NodeManager_headerLiteral" runat="server" Text="Node Manager"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <table border="0" cellpadding="0" cellspacing="0" align="right" width="100%">
                                <tr>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td align="right" style="width: 60px">
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="NodeManager_resetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                            OnClick="NodeManager_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" style="width: 2px">
                                        |
                                    </td>
                                    <td align="center" style="width: 45px">
                                        <asp:LinkButton ID="NodeManager_cancelLinkButton" runat="server" Text="Cancel" OnClick="ParentPageRedirect"
                                            SkinID="sknActionLinkButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="NodeManager_topMessageUpdatePanel" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="NodeManager_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="NodeManager_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="NodeManager_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" style="padding-bottom: 1px; padding-top: 5px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top" colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="NodeManager_selectHeaderUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr id="Tr1" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <asp:Literal ID="NodeManager_selectNodeHeader" runat="server" Text="Select Node Header"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table cellpadding="3" cellspacing="3" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 9%">
                                                                        <asp:Label runat="server" ID="NodeManager_addNodeTypeLabel" Text="Node Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList runat="server" ID="NodeManager_nodeTypeDropDownList" Width="90%"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="NodeManager_nodeTypeDropDownList_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 6%">
                                                                        <asp:Label ID="NodeManager_searchKeyWordLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:TextBox ID="NodeManager_searchTextBox" runat="server" Width="98%" MaxLength="200"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="NodeManager_searchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                                            OnClick="NodeManager_searchButton_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="NodeManager_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr id="NodeManager_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <asp:Literal ID="NodeManager_nodeResultsLiteral" runat="server" Text="Node Details"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 226px; overflow: auto;" runat="server" id="NodeManager_nodeDetailsDiv">
                                                                            <asp:GridView ID="NodeManager_nodeDetailsGridView" runat="server" AllowSorting="True"
                                                                                AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowCommand="NodeManager_nodeDetailsGridView_RowCommand"
                                                                                OnRowDataBound="NodeManager_nodeDetailsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="8%">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="NodeManager_nodeDetailsGridView_idHiddenField" runat="server"
                                                                                                Value='<%# Eval("ID") %>' />
                                                                                            <asp:ImageButton ID="NodeManager_nodeDetailsGridView_editImageButton" runat="server"
                                                                                                SkinID="sknEditDictionaryEntryImageButton" ToolTip="Edit Entry" CommandName="EditEntry" />&nbsp;
                                                                                            <asp:ImageButton ID="NodeManager_nodeDetailsGridView_deleteImageButton" runat="server"
                                                                                                SkinID="sknDeleteDictionaryEntryImageButton" ToolTip="Delete Entry" CommandName="DeleteEntry" />&nbsp;
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField ItemStyle-Width="40%" HeaderText="Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="NodeManager_nodeDetailsGridView_nameLabel" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField ItemStyle-Width="52%" HeaderText="Alias Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="NodeManager_nodeDetailsGridView_aliasNameLabel" runat="server" Text='<%# Eval("AliasName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:HiddenField ID="NodeManager_nodeDetails_aliasNameHiddenField" runat="server" />
                                                                        <uc2:PageNavigator ID="NodeManager_pageNavigator" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="NodeManager_nodeTypeDropDownList" />
                                                <asp:AsyncPostBackTrigger ControlID="NodeManager_addEntryButton" />
                                                <asp:AsyncPostBackTrigger ControlID="NodeManager_pageNavigator" />
                                                <asp:AsyncPostBackTrigger ControlID="NodeManager_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="padding-left: 6px; height: 15px; width: 50%">
                            <asp:UpdatePanel ID="NodeManager_addNodeLinkButtonUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="NodeManager_addNodeLinkButton" SkinID="sknAddLinkButton" ToolTip="Add New Location"
                                        runat="server" Text="Add Node" OnClick="NodeManager_addNodeLinkButton_Click"></asp:LinkButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="NodeManager_editEntryUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel ID="NodeManager_editEntryPanel" runat="server" CssClass="popupcontrol_edit_node_entry"
                                        Style="display: none" DefaultButton="NodeManager_editEntryPanel_updateButton">
                                        <div style="display: none;">
                                            <asp:Button ID="NodeManager_editEntryPanel_hiddenButton" runat="server" Text="Hidden" />
                                        </div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="NodeManager_editEntryPanel_headerLiteral" runat="server" Text="Edit Entry"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="NodeManager_editEntryPanel_closeImageButton" runat="server"
                                                                                SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_edit_dictionary_entry_inner_bg" style="width: 100%" align="left"
                                                                valign="top" colspan="2">
                                                                <table border="0" cellpadding="2" cellspacing="5" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table border="0" cellpadding="2" cellspacing="3" width="100%" class="tab_body_bg"
                                                                                align="left">
                                                                                <tr>
                                                                                    <td class="msg_align" colspan="3">
                                                                                        <asp:Label ID="NodeManager_editEntryPanel_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 16%">
                                                                                        <asp:Label ID="NodeManager_editEntryPanel_NodeLabel" runat="server" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                                                                            class="mandatory">*</span>
                                                                                    </td>
                                                                                    <td style="width: 83%">
                                                                                        <asp:TextBox runat="server" ID="NodeManager_editEntryPanel_nodeTextBox" Width="98%"
                                                                                            MaxLength="200" TabIndex="3"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 1%">
                                                                                        <asp:ImageButton ID="NodeManager_editEntryPanel_NodeHelpImageButton" SkinID="sknHelpImageButton"
                                                                                            runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                                            ToolTip="Enter or modify the name , alias name here" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 16%">
                                                                                        <asp:Label ID="NodeManager_editEntryPanel_aliasNameLabel" runat="server" Text="Alias Name"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                                                    </td>
                                                                                    <td style="width: 83%">
                                                                                        <asp:TextBox runat="server" ID="NodeManager_editEntryPanel_aliasNameTextBox" Width="98%"
                                                                                            TextMode="MultiLine" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                                            onchange="CommentsCount(500,this)" TabIndex="4"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 8%">
                                                                                    </td>
                                                                                    <td style="width: 91%">
                                                                                    </td>
                                                                                    <td style="width: 1%">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr align="left" valign="top">
                                                            <td colspan="2">
                                                                <asp:Button ID="NodeManager_editEntryPanel_updateButton" runat="server" Text="Update"
                                                                    SkinID="sknButtonId" OnClick="NodeManager_editEntryPanel_updateButton_Click"
                                                                    TabIndex="5" />
                                                                <asp:LinkButton ID="NodeManager_editEntryPanel_cancelButton" runat="server" Text="Cancel"
                                                                    SkinID="sknPopupLinkButton" TabIndex="6"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5" colspan="2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="NodeManager_editEntryPanelModalPopupExtender"
                                        runat="server" TargetControlID="NodeManager_editEntryPanel_hiddenButton" PopupControlID="NodeManager_editEntryPanel"
                                        BackgroundCssClass="modalBackground" CancelControlID="NodeManager_editEntryPanel_cancelButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="NodeManager_addNodeUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="NodeManager_addNodePanel" runat="server" CssClass="popupcontrol_add_node"
                                        Style="display: none" DefaultButton="NodeManager_addEntryButton">
                                        <div style="display: none;">
                                            <asp:Button ID="NodeManager_addNodeHiddenButton" runat="server" Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="NodeManager_addEntryLiteral" runat="server" Text="Add Entry"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="NodeManager_addEntryCloseImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                                <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="3">
                                                                            <asp:Label ID="NodeManager_addEntryErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="NodeManager_addEntryTypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Select Type"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="NodeManager_addEntryTypeDropDownList" runat="server" Width="87%"
                                                                                TabIndex="3">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="NodeManager_addEntryNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Node Name"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="NodeManager_addEntryTextBox" runat="server" MaxLength="50" Width="270px"
                                                                                TabIndex="4">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 2%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="NodeManager_addEntryAliasLabel" runat="server" Text="Alias Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="NodeManager_addEntryAliasTextBox" TextMode="MultiLine" Height="100"
                                                                                    runat="server" Width="270px" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                                    onchange="CommentsCount(500,this)" TabIndex="5"> </asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="NodeManager_addEntryHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="You can enter multiple alias name separated by comma " />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Button ID="NodeManager_addEntryButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                                    OnClick="NodeManager_addEntryButton_Click" TabIndex="5" />
                                                                &nbsp;
                                                                <asp:LinkButton ID="LocationManager_addNodeCancelLinkButton" runat="server" Text="Cancel"
                                                                    SkinID="sknPopupLinkButton" TabIndex="6"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="NodeManager_addEntryModalPopupExtender" runat="server"
                                        TargetControlID="NodeManager_addNodeHiddenButton" PopupControlID="NodeManager_addNodePanel"
                                        BackgroundCssClass="modalBackground" CancelControlID="LocationManager_addNodeCancelLinkButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="NodeManager_bottomMessageUpdatePanel" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="NodeManager_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="NodeManager_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="NodeManager_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="NodeManager_deleteEntryUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="NodeManager_deleteEntryPanel" runat="server" Style="display: none;
                            height: 202px" CssClass="popupcontrol_confirm">
                            <div style="display: none">
                                <asp:Button ID="NodeManager_deleteEntryPanel_hiddenButton" runat="server" />
                            </div>
                            <uc1:ConfirmMsgControl ID="NodeManager_deleteEntryPanel_confirmMessageControl" runat="server"
                                OnOkClick="NodeManager_deleteEntryPanel_okButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="NodeManager_deleteEntryPanel_confirmModalPopupExtender"
                            runat="server" PopupControlID="NodeManager_deleteEntryPanel" TargetControlID="NodeManager_deleteEntryPanel_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
