﻿#region Directives                                                   

using System;
using System.Data;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using OfficeOpenXml;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class ViewCandidateActivityLog : PageBase
    {
        #region Private Constants                                    

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                       

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Candidate Activity Log");

                // Subscribe to message thrown events.
                ViewCandidateActivityLog_candidateActivityViewerControl.ControlMessageThrown +=
                    new CandidateActivityViewerControl.ControlMessageThrownDelegate
                    (ViewCandidateActivityLog_candidateActivityViewerControl_ControlMessageThrown);

                // Set page size.
                ViewCandidateActivityLog_candidateActivityViewerControl.GridPageSize = base.GridPageSize;

                // Allow change candidate.
                ViewCandidateActivityLog_candidateActivityViewerControl.AllowChangeCandidate = true;

                if (!IsPostBack)
                {
                    // Check if candidate ID is passed.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                    {
                        int candidateID = 0;
                        if (int.TryParse(Request.QueryString["candidateid"].ToString().Trim(), out candidateID) == true)
                        {
                            ViewCandidateActivityLog_candidateActivityViewerControl.CandidateID = candidateID;
                        }
                    }
                }

                // Check if post back is raised due after adding notes.
                if (ViewCandidateActivityLog_refreshHiddenField.Value != null &&
                    ViewCandidateActivityLog_refreshHiddenField.Value.Trim().ToUpper() == "Y")
                {
                    // Reset the flag value.
                    ViewCandidateActivityLog_refreshHiddenField.Value = "N";

                    // Refresh activities.
                    RefreshActivities();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                   ViewCandidateActivityLog_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the refresh link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will refresh the page.
        /// </remarks>
        protected void ViewCandidateActivityLog_refreshLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Refresh activities.
                RefreshActivities();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                   ViewCandidateActivityLog_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the reset link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to its origininal state.
        /// </remarks>
        protected void ViewCandidateActivityLog_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                   ViewCandidateActivityLog_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the download link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the activities to an excel file.
        /// </remarks>
        protected void ViewCandidateActivityLog_downloadLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewCandidateActivityLog_topErrorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_topSuccessMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_bottomErrorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_bottomSuccessMessageLabel.Text = string.Empty;

                // Check if candidate ID is present.
                if (ViewCandidateActivityLog_candidateActivityViewerControl.CandidateID == 0)
                {
                    ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                        ViewCandidateActivityLog_bottomErrorMessageLabel,
                        "No candidate was selected");

                    return;
                }

                // Get the table that contains the rows for download.
                DataTable table = ViewCandidateActivityLog_candidateActivityViewerControl.
                    GetDownloadTable();

                // Check if table is not null and contain rows.
                if (table == null || table.Rows.Count == 0)
                {
                    ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                        ViewCandidateActivityLog_bottomErrorMessageLabel,
                        "No activities found to download");

                    return;
                }

                // Remove the row where 'total' columns is not null (which 
                // contains the total records used for paging).
                DataRow[] foundRows =
                    table.Select("[TOTAL] is not null", "", DataViewRowState.OriginalRows);

                // Check if rows are found with 'total column is not null'
                if (foundRows != null && foundRows.Length > 0)
                {
                    // Remove the 0th row.
                    table.Rows.Remove(foundRows[0]);
                }

                // Remove the unncessary columns from the table.
                table.Columns.Remove("ACTIVITY_BY");
                table.Columns.Remove("ACTIVITY_TYPE");
                table.Columns.Remove("TOTAL");

                // Change the column names.
                table.Columns["ACTIVITY_DATE"].ColumnName = "Activity Date";
                table.Columns["ACTIVITY_BY_NAME"].ColumnName = "Activity By";
                table.Columns["ACTIVITY_TYPE_NAME"].ColumnName = "Activity";
                table.Columns["COMMENTS"].ColumnName = "Comments";

                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add
                        (Constants.ExcelExportSheetName.CANDIDATE_ACTIVITY_LOG);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                    foreach (DataColumn column in table.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }
                                    col.Style.Numberformat.Format = "0\",\"0000";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    // Construct file name.
                    string fileName = string.Format("{0}_{1}.xlsx",
                        Constants.ExcelExportFileName.CANDIDATE_ACTIVITY_LOG,
                        Utility.GetValidExportFileNameString(ViewCandidateActivityLog_candidateActivityViewerControl.CandidateName, 50));

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                   ViewCandidateActivityLog_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the add notes link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will show the add notes popup.
        /// </remarks>
        protected void ViewCandidateActivityLog_addNotesLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewCandidateActivityLog_topErrorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_topSuccessMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_bottomErrorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_bottomSuccessMessageLabel.Text = string.Empty;

                // Retrieve and check if candidate ID is present.
                int candidateID = ViewCandidateActivityLog_candidateActivityViewerControl.CandidateID;

                if (candidateID == 0)
                {
                    ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                        ViewCandidateActivityLog_bottomErrorMessageLabel,
                        "No candidate was selected");

                    return;
                }

                // Add the handler to the add notes link.
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenAddNotesPopupWithRefresh",
                    "javascript: OpenAddNotesPopupWithRefresh('" + candidateID + "')", true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel,
                   ViewCandidateActivityLog_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the control message thrown event is 
        /// fired from the candidate activity viewer control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will show the message on the corresponding labels.
        /// </remarks>
        private void ViewCandidateActivityLog_candidateActivityViewerControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            // Check if message is empty. If empty clear all messages.
            if (Utility.IsNullOrEmpty(c.Message))
            {
                ViewCandidateActivityLog_topErrorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_topSuccessMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_bottomErrorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLog_bottomSuccessMessageLabel.Text = string.Empty;
                return;
            }

            // Show the message.
            if (c.MessageType == MessageType.Error)
            {
                base.ShowMessage(ViewCandidateActivityLog_topErrorMessageLabel, 
                    ViewCandidateActivityLog_bottomErrorMessageLabel, c.Message);
            }
        }
        
        #endregion Event Handlers

        #region Protected Override Methods                           

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Override Methods

        #region Private Methods

        /// <summary>
        /// Method that refresh the activities.
        /// </summary>
        private void RefreshActivities()
        {
            ViewCandidateActivityLog_candidateActivityViewerControl.RefreshActivities();
        }

        #endregion Private Methods
    }
}