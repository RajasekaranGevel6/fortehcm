﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CompetencyVectorManager.cs
// File that represents the Competency Vector Manager used for adding , editing
// or deleting a vector
//

#endregion Header

#region Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class CompetencyVectorManager : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "298px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "350px";

        #endregion Declaration

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Competency Vector");

                // Set default button.
                Page.Form.DefaultButton = CompetencyVectorManager_searchButton.UniqueID;

                CompetencyVectorManager_groupTypeDropDownList.Focus();

                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    //Load the vector group in the vector group drop down list 
                    LoadVectorGroup();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "GROUP_NAME";

                    CompetencyVectorManager_resultDiv.Visible = false;
                }

                CompetencyVectorManager_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestoreDiv('" +
                CompetencyVectorManager_resultDiv.ClientID + "','" +
                CompetencyVectorManager_searchDiv.ClientID + "','" +
                CompetencyVectorManager_searchResultsUpSpan.ClientID + "','" +
                CompetencyVectorManager_searchResultsDownSpan.ClientID + "','" +
                CompetencyVectorManager_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

                CompetencyVectorManager_bottomSuccessMessageLabel.Text = string.Empty;
                CompetencyVectorManager_topSuccessMessageLabel.Text = string.Empty;
                CompetencyVectorManager_bottomErrorMessageLabel.Text = string.Empty;
                CompetencyVectorManager_topErrorMessageLabel.Text = string.Empty;
                CompetencyVectorManager_deletePopup_errorMessageLabel.Text = string.Empty;
                CompetencyVectorManager_addNewVectorErrorLabel.Text = string.Empty;
                CompetencyVectorManager_editVector_errorLabel.Text = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel,
                 exception.Message);
            }
        }

        /// <summary>
        /// Represents the handler that is called on the row data bound 
        /// of the grid view
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void CompetencyVectorManager_searchGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    // Add handler to load certification title window.
                    HiddenField vectorIDHiddenField = ((HiddenField)e.Row.FindControl
                        ("CompetencyVectorManager_vectorIDHiddenField"));
                    ImageButton certificationTitleImagebutton = ((ImageButton)e.Row.FindControl
                        ("CompetencyVectorManager_certificationTitleImageButton"));
                    certificationTitleImagebutton.Attributes.Add
                        ("onClick", "javascript:return ShowCertificationTitleWindow(" +
                        vectorIDHiddenField.Value + ");");

                    //HiddenField vectorGroupNameHiddenField = ((HiddenField)e.Row.FindControl
                    //   ("CompetencyVectorManager_vectorGroupNameHiddenField"));
                    //ImageButton certificationTitleImageButton = ((ImageButton)e.Row.FindControl
                    //   ("CompetencyVectorManager_certificationTitleImageButton"));

                    //if (vectorGroupNameHiddenField.Value.Trim() == "Technical Skills") //Temp Solution
                    //    certificationTitleImageButton.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the handler that is called on sorting
        /// of the grid view
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void CompetencyVectorManager_searchGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                LoadCompetencyVectorDetails(1);

                CompetencyVectorManager_bottomPagingNavigator.Reset();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that is called when the row is created in the grid
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>       
        protected void CompetencyVectorManager_searchGridView_RowCreated(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                //Add the sort image for the header row
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (CompetencyVectorManager_searchGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event that is called on page number click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="PageNumberEventArgs"/>that holds the events args
        /// </param>
        protected void CompetencyVectorManager_pageNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                //Load the grid with the current page number
                LoadCompetencyVectorDetails(e.PageNumber);
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the method that is called when the search button is clicked 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object 
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event args
        /// </param>
        protected void CompetencyVectorManager_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Make the result div as visible
                CompetencyVectorManager_resultDiv.Visible = true;

                ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = "GROUP_NAME";

                //Reset the paging navigator
                CompetencyVectorManager_bottomPagingNavigator.Reset();

                //Load the competency vector details
                LoadCompetencyVectorDetails(1);
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on the save button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the sender of the object
        /// </param>
        protected void CompetencyVectorManager_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Check the mandatory field
                if (CompetencyVectorManager_aliasTextBox.Text.Trim().Length == 0)
                {
                    CompetencyVectorManager_editVector_errorLabel.Text = "Alias cannot be empty";
                    CompetencyVectorManager_editVectorModalPopUpExtender.Show();
                    return;
                }

                //Update the alias field for the given vector
                int vectorID = int.Parse(CompetencyVectorManager_editVectorIdHiddenField.Value);
                string vectorAliasName = CompetencyVectorManager_aliasTextBox.Text.Trim();

                new CompetencyVectorBLManager().UpdateCompetencyVectorManagerAliasName
                    (vectorID, vectorAliasName);

                base.ShowMessage(CompetencyVectorManager_topSuccessMessageLabel,
                  CompetencyVectorManager_bottomSuccessMessageLabel, Resources.HCMResource.
                  CompetencyVectorManager_updateAliasSuccessMessage);

                LoadCompetencyVectorDetails(1);

            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called on the row command
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>       
        protected void CompetencyVectorManager_searchGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField vectorGroupNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                    ("CompetencyVectorManager_vectorGroupNameHiddenField") as HiddenField;

                HiddenField vectorNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                   ("CompetencyVectorManager_vectorNameHiddenField") as HiddenField;

                HiddenField vectorAliasNameHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                   ("CompetencyVectorManager_vectorAliasNameHiddenField") as HiddenField;

                HiddenField vectorIDHiddenField = ((ImageButton)e.CommandSource).Parent.FindControl
                    ("CompetencyVectorManager_vectorIDHiddenField") as HiddenField;

                if (e.CommandName == "EditVector")
                {
                    CompetencyVectorManager_editVector_errorLabel.Text = string.Empty;
                    CompetencyVectorManager_groupNameValueLabel.Text = vectorGroupNameHiddenField.Value.Trim();
                    CompetencyVectorManager_vectorNameValueLabel.Text = vectorNameHiddenField.Value.Trim();
                    CompetencyVectorManager_aliasTextBox.Text = vectorAliasNameHiddenField.Value.Trim();
                    CompetencyVectorManager_editVectorIdHiddenField.Value = vectorIDHiddenField.Value;
                    CompetencyVectorManager_saveButton.Focus();
                    CompetencyVectorManager_editVectorModalPopUpExtender.Show();
                }
                if (e.CommandName == "DeleteVector")
                {

                    CompetencyVectorManager_deletePopup_errorMessageLabel.Text = string.Empty;
                    CompetencyVectorManager_editVectorIdHiddenField.Value = vectorIDHiddenField.Value;

                    CompetencyVectorManager_deletePopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is on the yes button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void CompetencyVectorManager_deletePopup_yesValidButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                //Get the vector id from the hidden field
                int vectorID = int.Parse(CompetencyVectorManager_editVectorIdHiddenField.Value);

                //Check if the vector is involved in the 
                bool isVectorIdInvolved = new CompetencyVectorBLManager().CheckVectorIDInvolved(vectorID);

                //if it is not involved delete the vector
                if (isVectorIdInvolved)
                {
                    new CompetencyVectorBLManager().DeleteCompetencyVector(vectorID);

                    base.ShowMessage(CompetencyVectorManager_topSuccessMessageLabel,
                  CompetencyVectorManager_bottomSuccessMessageLabel, Resources.HCMResource.
                  CompetencyVectorManager_DeleteSuccessMessage);
                    LoadCompetencyVectorDetails(1);
                }
                else
                {
                    CompetencyVectorManager_deletePopup_errorMessageLabel.Text = Resources.HCMResource.
                        CompetencyVectorManager_VectorInvolved;

                    CompetencyVectorManager_deletePopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on the yes button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void CompetencyVectorManager_addVectorLinkButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                //Assign the vector group details to the drop down list
                List<CompetencyVectorGroup> competencyGroupList = new CompetencyVectorBLManager().GetCompetencyVectorGroup();

                CompetencyVectorManager_addNewVectorGroupNameDropDownList.DataSource = competencyGroupList;
                CompetencyVectorManager_addNewVectorGroupNameDropDownList.DataTextField = "VectorGroupName";
                CompetencyVectorManager_addNewVectorGroupNameDropDownList.DataValueField = "VectorGroupID";
                CompetencyVectorManager_addNewVectorGroupNameDropDownList.DataBind();

                //Clear the values 
                CompetencyVectorManager_addNewVectorAliasTextBox.Text = string.Empty;
                CompetencyVectorManager_addNewVectorNameTextBox.Text = string.Empty;

                CompetencyVectorManager_addNewVectorSaveButton.Focus();

                //Show the add new vecto modal pop up
                CompetencyVectorManager_addNewVectorModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is on the add new button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void CompetencyVectorManager_addNewVectorSaveButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                //Vector name and alias name should not be emtpy
                if ((CompetencyVectorManager_addNewVectorNameTextBox.Text.Trim().Length == 0) ||
                    (CompetencyVectorManager_addNewVectorAliasTextBox.Text.Trim().Length == 0))
                {
                    CompetencyVectorManager_addNewVectorErrorLabel.Text =
                       "Please enter mandatory fields";
                    CompetencyVectorManager_addNewVectorModalPopupExtender.Show();
                }
                else
                {
                    if (CheckEnteredVectorValues())
                    {
                        //Save the vector to the Data base
                        int vectorGroupID = int.Parse
                            (CompetencyVectorManager_addNewVectorGroupNameDropDownList.SelectedValue);
                        string vectorName = CompetencyVectorManager_addNewVectorNameTextBox.Text.Trim();
                        string aliasName = CompetencyVectorManager_addNewVectorAliasTextBox.Text.Trim().TrimEnd(',');
                        if (new CompetencyVectorBLManager().AddNewVector(vectorGroupID, vectorName, aliasName, base.userID))
                        {
                            base.ShowMessage(CompetencyVectorManager_topSuccessMessageLabel,
                                CompetencyVectorManager_bottomSuccessMessageLabel,
                                 Resources.HCMResource.CompetencyVectorManager_NewVectorAddedSuccessMessage);
                            LoadCompetencyVectorDetails(1);
                        }
                        else
                        {
                            CompetencyVectorManager_addNewVectorErrorLabel.Text =
                                     Resources.HCMResource.CompetencyVectorManager_VectorAlreadyExistMessage;
                            CompetencyVectorManager_addNewVectorModalPopupExtender.Show();
                        }
                    }
                    else
                    {
                        CompetencyVectorManager_addNewVectorErrorLabel.Text =
                       Resources.HCMResource.CompetencyVectorManager_DuplicateNotAllowedMessage;
                        CompetencyVectorManager_addNewVectorModalPopupExtender.Show();
                    }
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the method to check the vector alias values
        /// </summary>
        private bool CheckEnteredVectorValues()
        {
            string aliasName = CompetencyVectorManager_addNewVectorAliasTextBox.Text.Trim();
            //Get the individual string values
            string[] separatedAliasName = aliasName.Split(new char[] { ',' });
            //Get the distinct string count
            int numberOfStrings = separatedAliasName.Distinct().Count();
            //if the number of string is equal to length return true
            return numberOfStrings == separatedAliasName.Length ? true : false;
        }

        /// <summary>
        /// Represents the event handler that is on the reset button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void CompetencyVectorManager_resetLinkButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                //Redirect to the raw url
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CompetencyVectorManager_topErrorMessageLabel,
                    CompetencyVectorManager_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// Represents the method to load the vector
        /// group details in the drop down list
        /// </summary>
        private void LoadVectorGroup()
        {
            List<CompetencyVectorGroup> competencyGroupList = new CompetencyVectorBLManager().GetCompetencyVectorGroup();

            CompetencyVectorManager_groupTypeDropDownList.DataSource = competencyGroupList;
            CompetencyVectorManager_groupTypeDropDownList.DataTextField = "VectorGroupName";
            CompetencyVectorManager_groupTypeDropDownList.DataValueField = "VectorGroupID";
            CompetencyVectorManager_groupTypeDropDownList.DataBind();
            CompetencyVectorManager_groupTypeDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));

        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(CompetencyVectorManager_restoreHiddenField.Value) &&
                CompetencyVectorManager_restoreHiddenField.Value == "Y")
            {
                CompetencyVectorManager_searchDiv.Style["display"] = "none";
                CompetencyVectorManager_searchResultsUpSpan.Style["display"] = "block";
                CompetencyVectorManager_searchResultsDownSpan.Style["display"] = "none";
                CompetencyVectorManager_resultDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CompetencyVectorManager_searchDiv.Style["display"] = "block";
                CompetencyVectorManager_searchResultsUpSpan.Style["display"] = "none";
                CompetencyVectorManager_searchResultsDownSpan.Style["display"] = "block";
                CompetencyVectorManager_resultDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Represents the method to load the competency vector details
        /// </summary>
        /// <param name="p"></param>
        private void LoadCompetencyVectorDetails(int pageNumber)
        {
            int total = 0;
            string vectorGroupId = CompetencyVectorManager_groupTypeDropDownList.SelectedValue;
            string vectorName = CompetencyVectorManager_vectorTextBox.Text.Trim();
            string vectorAliasName = CompetencyVectorManager_vectorAliasTextBox.Text.Trim();

            //Get the competency vector details from the database
            List<CompetencyVectorDetails> competencyVectorDetails = new CompetencyVectorBLManager().
                GetCompetencyVectorDetails(vectorGroupId, vectorName, vectorAliasName, ViewState["SORT_FIELD"].ToString(),
                ViewState["SORT_ORDER"].ToString(), pageNumber, base.GridPageSize, out total);

            if (competencyVectorDetails != null && competencyVectorDetails.Count != 0)
            {
                CompetencyVectorManager_searchGridView.DataSource = competencyVectorDetails;
                CompetencyVectorManager_searchGridView.DataBind();
            }
            else
            {
                CompetencyVectorManager_searchGridView.DataSource = competencyVectorDetails;
                CompetencyVectorManager_searchGridView.DataBind();

                //show the default empty message
                ShowMessage(CompetencyVectorManager_bottomErrorMessageLabel,
                    CompetencyVectorManager_topErrorMessageLabel, Resources.HCMResource.
                    Common_Empty_Grid);
                //Make the result div as visible
                CompetencyVectorManager_resultDiv.Visible = false;

            }

            CompetencyVectorManager_bottomPagingNavigator.TotalRecords = total;
            CompetencyVectorManager_bottomPagingNavigator.PageSize = base.GridPageSize;

        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }
        #endregion Protected Overridden Methods
    }
}
