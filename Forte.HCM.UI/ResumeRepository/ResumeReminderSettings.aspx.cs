﻿#region Directives

using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class ResumeReminderSettings : PageBase
    {
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Resume Reminder Settings");
                if (!IsPostBack)
                {
                    LoadResumeEmailSettings();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to save the email reminder settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeReminderSettings_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                //if (!IsValidData())
                //    return;

                //Get incomplete resume email settings
                List<ResumeEmailReminderDetail> resumeEmailReminderDetails = new List<ResumeEmailReminderDetail>();
                if (ResumeReminderSettings_immediateReminderToResumeUploaderCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_IMM;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_onceInADayReminderToResumeUploaderCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DLY;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_onceInAWeekReminderToResumeUploaderCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_WLY;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }

                if (ResumeReminderSettings_immediateReminderToAdminCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_IMM; 
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_onceInADayReminderToAdminCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DLY; 
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_onceInAWeekReminderToAdminCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_WLY;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }

                
                //Get duplicate resume email settings
                if (ResumeReminderSettings_duplicateImmediateReminderToResumeUploaderCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_IMM;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_duplicateOnceInADayReminderToResumeUploaderCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_DLY;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_duplicateOnceInAWeekReminderToResumeUploaderCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_WLY;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }

                if (ResumeReminderSettings_duplicateImmediateReminderToAdminCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_IMM;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_duplicateOnceInADayReminderToAdminCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_DLY;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                if (ResumeReminderSettings_duplicateOnceInAWeekReminderToAdminCheckBox.Checked)
                {
                    ResumeEmailReminderDetail resumeEmailReminderDetail = new ResumeEmailReminderDetail();
                    resumeEmailReminderDetail.UserType = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN;
                    resumeEmailReminderDetail.EmailOption = Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_WLY;
                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }

                //Insert/Reset resume email reminder settings
                new ResumeRepositoryBLManager().InsertResumeEmailReminderSettings(resumeEmailReminderDetails, tenantID, userID);
                base.ShowMessage(ResumeReminderSettings_topSuccessMessageLabel,
                    ResumeReminderSettings_bottomSuccessMessageLabel,
                    "Resume reminder settings saved successfully");
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to reset the email reminder settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResumeReminderSettings_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                LoadResumeEmailSettings();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                    ResumeReminderSettings_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// To load the resume note and its element details
        /// </summary>
        private void LoadResumeEmailSettings()
        {
            List<ResumeEmailReminderDetail> resumeEmailReminderDetail = new List<ResumeEmailReminderDetail>();
            resumeEmailReminderDetail = new ResumeRepositoryBLManager().GetResumeEmailReminderSettings(tenantID);
            if (resumeEmailReminderDetail != null)
            {
                #region Load incomplete admin settings
                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() == 
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN && 
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_IMM))
                    ResumeReminderSettings_immediateReminderToAdminCheckBox.Checked = true;
                else
                    ResumeReminderSettings_immediateReminderToAdminCheckBox.Checked = false;

                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() == 
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN &&
                   res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DLY))
                    ResumeReminderSettings_onceInADayReminderToAdminCheckBox.Checked = true;
                else
                    ResumeReminderSettings_onceInADayReminderToAdminCheckBox.Checked = false;


                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() == 
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_WLY))
                    ResumeReminderSettings_onceInAWeekReminderToAdminCheckBox.Checked = true;
                else
                    ResumeReminderSettings_onceInAWeekReminderToAdminCheckBox.Checked = false;

                #endregion Load incomplete admin settings

                #region Load duplicate admin settings
                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() ==
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_IMM))
                    ResumeReminderSettings_duplicateImmediateReminderToAdminCheckBox.Checked = true;
                else
                    ResumeReminderSettings_duplicateImmediateReminderToAdminCheckBox.Checked = false;

                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() ==
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN &&
                   res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_DLY))
                    ResumeReminderSettings_duplicateOnceInADayReminderToAdminCheckBox.Checked = true;
                else
                    ResumeReminderSettings_duplicateOnceInADayReminderToAdminCheckBox.Checked = false;


                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() ==
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_ADMIN &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_WLY))
                    ResumeReminderSettings_duplicateOnceInAWeekReminderToAdminCheckBox.Checked = true;
                else
                    ResumeReminderSettings_duplicateOnceInAWeekReminderToAdminCheckBox.Checked = false;

                #endregion Load duplicate admin settings

                #region Load incomplete uploader settings
                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() == 
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_IMM))
                    ResumeReminderSettings_immediateReminderToResumeUploaderCheckBox.Checked = true;
                else
                    ResumeReminderSettings_immediateReminderToResumeUploaderCheckBox.Checked = false;

                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() == 
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DLY))
                    ResumeReminderSettings_onceInADayReminderToResumeUploaderCheckBox.Checked = true;
                else
                    ResumeReminderSettings_onceInADayReminderToResumeUploaderCheckBox.Checked = false;

                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() == 
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_WLY))
                    ResumeReminderSettings_onceInAWeekReminderToResumeUploaderCheckBox.Checked = true;
                else
                    ResumeReminderSettings_onceInAWeekReminderToResumeUploaderCheckBox.Checked = false;

                #endregion incomplete uploader settings

                #region Load duplicate uploader settings
                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() ==
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_IMM))
                    ResumeReminderSettings_duplicateImmediateReminderToResumeUploaderCheckBox.Checked = true;
                else
                    ResumeReminderSettings_duplicateImmediateReminderToResumeUploaderCheckBox.Checked = false;

                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() ==
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_DLY))
                    ResumeReminderSettings_duplicateOnceInADayReminderToResumeUploaderCheckBox.Checked = true;
                else
                    ResumeReminderSettings_duplicateOnceInADayReminderToResumeUploaderCheckBox.Checked = false;

                if (resumeEmailReminderDetail.Any(res => res.AttributeID.Trim() ==
                    Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_UPLOADER &&
                    res.AttributeType.Trim() == Constants.ResumeEmailReminderConstants.RESUME_EMAIL_REMINDER_DUP_WLY))
                    ResumeReminderSettings_duplicateOnceInAWeekReminderToResumeUploaderCheckBox.Checked = true;
                else
                    ResumeReminderSettings_duplicateOnceInAWeekReminderToResumeUploaderCheckBox.Checked = false;

                #endregion duplicate uploader settings
            }
        }

        /// <summary>
        /// Method to clear the success/error message controls
        /// </summary>
        private void ClearControl()
        {
            ResumeReminderSettings_topSuccessMessageLabel.Text = string.Empty;
            ResumeReminderSettings_bottomSuccessMessageLabel.Text = string.Empty;
            ResumeReminderSettings_topErrorMessageLabel.Text = string.Empty;
            ResumeReminderSettings_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            if (!ResumeReminderSettings_immediateReminderToResumeUploaderCheckBox.Checked &&
                !ResumeReminderSettings_onceInADayReminderToResumeUploaderCheckBox.Checked &&
                !ResumeReminderSettings_onceInAWeekReminderToResumeUploaderCheckBox.Checked &&
                !ResumeReminderSettings_immediateReminderToAdminCheckBox.Checked &&
                !ResumeReminderSettings_onceInADayReminderToAdminCheckBox.Checked &&
                !ResumeReminderSettings_onceInAWeekReminderToAdminCheckBox.Checked)
            {
                base.ShowMessage(ResumeReminderSettings_topErrorMessageLabel,
                   ResumeReminderSettings_bottomErrorMessageLabel, 
                   "Please select at least one email settings");
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}