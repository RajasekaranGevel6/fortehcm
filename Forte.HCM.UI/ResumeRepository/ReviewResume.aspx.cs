﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class ReviewResume : PageBase
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                Master.SetPageCaption("Review Resume");
                ClearControls();
                UploadedResumeStatus_topSuccessLabel.Text = "";
                UploadedResumeStatus_topErrorLabel.Text = "";
                UploadedResumeStatus_bottomErrorLabel.Text = "";
                UploadedResumeStatus_bottomSuccessLabel.Text = "";
                Page.Form.DefaultButton = UploadedResumeStatus_resumeStatusSearchButton.UniqueID;

                if (!Utility.IsNullOrEmpty(Request.QueryString["messageid"]))
                {
                    string messageType = Request.QueryString["messageid"];

                    switch (messageType)
                    {
                        case "1":
                            UploadedResumeStatus_topSuccessLabel.Text = "New candidate added successfully";
                            UploadedResumeStatus_bottomSuccessLabel.Text = "New candidate added successfully";
                            break;
                        case "2":
                            UploadedResumeStatus_topSuccessLabel.Text = "New resume added successfully";
                            UploadedResumeStatus_bottomSuccessLabel.Text = "New resume added successfully";
                            break;
                        case "3":
                            UploadedResumeStatus_topSuccessLabel.Text = "Temporary candidate deleted successfully";
                            UploadedResumeStatus_bottomSuccessLabel.Text = "Temporary candidate deleted successfully";
                            break;
                        case "4":
                            UploadedResumeStatus_topSuccessLabel.Text = "Profile completed";
                            UploadedResumeStatus_bottomSuccessLabel.Text = "Profile completed";
                            break;
                        case "5":
                            UploadedResumeStatus_topSuccessLabel.Text = "Profile completed and candidate created";
                            UploadedResumeStatus_bottomSuccessLabel.Text = "Profile completed and candidate created";
                            break;
                        default:
                            break;
                    }
                } 

                if (!IsPostBack)
                {
                    UploadedResumeStatus_statusDropDownList.Items.Clear();
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                    UploadedResumeStatus_statusDropDownList.Items.Insert(0, new ListItem("--Select--", "-1"));
                    if (userDetail != null)
                    {
                        UploadedResumeStatus_recruiterSearchImageButton.Attributes.Add("onclick",
                         "javascript:return LoadUserForTenant('"
                        + ReviewResume_recruitedIDHiddenField.ClientID + "','"
                        + ReviewResume_uploadedByDummyIdHidenField.ClientID + "','"
                        + UploadedResumeStatus_recruitedBySearchTextBox.ClientID + "')");


                        if (userDetail.SubscriptionRole.Trim() == "SR_COR_AMN")
                        {
                            UploadedResumeStatus_statusDropDownList.Items.Insert(1, new ListItem("Inactive", "1"));
                            UploadedResumeStatus_statusDropDownList.Items.Insert(2, new ListItem("Duplicate", "2"));
                            UploadedResumeStatus_statusDropDownList.Items.Insert(3, new ListItem("Incomplete", "3"));

                            ReviewResume_recruitedIDHiddenField.Value = userDetail.UserID.ToString();
                        }
                        else
                        { 
                            ReviewResume_recruitedIDHiddenField.Value = userDetail.UserID.ToString();

                            ReviewResume_uploadedByDummyIdHidenField.Value = userDetail.UserID.ToString();

                            UploadedResumeStatus_recruitedBySearchTextBox.Text = userDetail.FirstName;// +" " + userDetail.LastName;
                            UploadedResumeStatus_statusDropDownList.Items.Insert(1, new ListItem("Inactive", "1"));
                            UploadedResumeStatus_statusDropDownList.Items.Insert(2, new ListItem("Incomplete", "2"));
                        }

                        if (Request.QueryString["showtype"] != null)
                        {
                            string showtype = Request.QueryString["showtype"].Trim().ToLower();
                            switch (showtype)
                            {
                                case "ic":
                                    if (UploadedResumeStatus_statusDropDownList.Items.Count > 2)
                                        UploadedResumeStatus_statusDropDownList.SelectedIndex = 3;
                                    else
                                        UploadedResumeStatus_statusDropDownList.SelectedIndex = -1;
                                    break;
                                case "dp":
                                    if (UploadedResumeStatus_statusDropDownList.Items.Count > 1)
                                        UploadedResumeStatus_statusDropDownList.SelectedIndex = 2;
                                    else
                                        UploadedResumeStatus_statusDropDownList.SelectedIndex = -1;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    ReviewResume_deleteResumeConfirmMsgControl.Message = "Are you sure to delete the resume?";
                    ReviewResume_deleteResumeConfirmMsgControl.Type = MessageBoxType.YesNo;
                    ReviewResume_deleteResumeConfirmMsgControl.Title = "Delete Resume";

                    UploadedResumeStatus_pendingResumeDataList.DataSource = null;
                    UploadedResumeStatus_pendingResumeDataList.DataBind();

                    ViewState["pageNo"] = 1;

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                          (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                    }

                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REVIEW_RESUMES] != null)
                    {
                        ReviewResumeDetail resumeDetail = Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REVIEW_RESUMES]
                            as ReviewResumeDetail;

                        UploadedResumeStatus_profileNameSearchTextBox.Text = resumeDetail.ProfileName;
                        ReviewResume_uploadedByDummyIdHidenField.Value = resumeDetail.RecruiterID.ToString();

                        if (resumeDetail.Status != null)
                            UploadedResumeStatus_statusDropDownList.SelectedItem.Text = resumeDetail.Status;

                        if (resumeDetail.FromDate.ToShortDateString() != "1/1/0001")
                            UploadedResumeStatus_updateDateFromTextBox.Text = resumeDetail.FromDate.ToShortDateString();

                        if (resumeDetail.ToDate.ToShortDateString() != "1/1/0001")
                            UploadedResumeStatus_updateDateToTextBox.Text = resumeDetail.ToDate.ToShortDateString();

                        GetReviewResumes(resumeDetail);
                    }
                    else
                        GetReviewResumes(GetReviewResume_Parameters(1));

                }
                ReviewResume_pageNavigator.PageNumberClick += new
                     PageNavigator.PageNumberClickEventHandler
                     (ReviewResume_pageNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                   UploadedResumeStatus_bottomErrorLabel, exp.Message);
            }
        }

        private void ReviewResume_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["pageNo"] = e.PageNumber;

                GetReviewResumes(GetReviewResume_Parameters(e.PageNumber));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                UploadedResumeStatus_bottomErrorLabel, exp.Message);
            }
        }

        protected void UploadedResumeStatus_resumeStatusSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                GetReviewResumes(GetReviewResume_Parameters(1));
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                    UploadedResumeStatus_bottomErrorLabel, ex.Message);
            }
        }

        protected void UploadedResumeStatus_pendingResumeDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem listItem = e.Item;

                CandidateResumeDetails resumeDetail = null;

                resumeDetail = (CandidateResumeDetails)listItem.DataItem;

                Label UploadedResumeStatus_resumeNameLabel = (Label)e.Item.FindControl("UploadedResumeStatus_resumeNameLabel");

                if (UploadedResumeStatus_resumeNameLabel != null)
                    UploadedResumeStatus_resumeNameLabel.Text = resumeDetail.FileName;

                Label UploadedResumeStatus_lastUpdateDateLabel = (Label)e.Item.FindControl("UploadedResumeStatus_lastUpdateDateLabel");

                if (UploadedResumeStatus_lastUpdateDateLabel != null)
                    UploadedResumeStatus_lastUpdateDateLabel.Text = "Uploaded on " + resumeDetail.ProcessedDate.ToShortDateString();

                HiddenField UploadedResumeStatus_candidateIDHiddenField = (HiddenField)e.Item.FindControl("UploadedResumeStatus_candidateIDHiddenField");

                ImageButton ReviewResume_downloadResumeImageButton = (ImageButton)e.Item.FindControl("ReviewResume_downloadResumeImageButton");
                if (ReviewResume_downloadResumeImageButton != null)
                    ReviewResume_downloadResumeImageButton.Attributes.Add("onclick", "javascript:return DownloadTemporaryResume('" + resumeDetail.CandidateId.ToString() + "');");

                ImageButton ReviewResume_candidateSkillImageButton = (ImageButton)e.Item.FindControl("ReviewResume_candidateSkillImageButton");
                if (ReviewResume_candidateSkillImageButton != null)
                    ReviewResume_candidateSkillImageButton.Attributes.Add("onclick",
                      "javascript: return OpenSkillsMatrix('"
                      + resumeDetail.CandidateId.ToString() + "','" + Constants.CandidateResumeRepositoryConstants.CANDIDATE_TEMPORARY_RESUME_SKILL + "');");

                if (UploadedResumeStatus_candidateIDHiddenField != null)
                    UploadedResumeStatus_candidateIDHiddenField.Value = resumeDetail.CandidateId.ToString();

                Label UploadedResumeStatus_recruitedByLabel = (Label)e.Item.FindControl("UploadedResumeStatus_recruitedByLabel");
                if (UploadedResumeStatus_recruitedByLabel != null)
                    UploadedResumeStatus_recruitedByLabel.Text = "Uploaded by " + resumeDetail.Recruiter;

                if (resumeDetail.Complete != null)
                {
                    Label ReviewResume_statusLabel = (Label)e.Item.FindControl("ReviewResume_statusLabel");

                    if (resumeDetail.Complete == "Y" && resumeDetail.Duplicate == "Y")
                    {
                        if (ReviewResume_statusLabel != null)
                            ReviewResume_statusLabel.Text = "Duplicate";
                    }
                    else
                    {
                        if (ReviewResume_statusLabel != null)
                            ReviewResume_statusLabel.Text = "Incomplete";
                    }
                }

                Literal UploadedResumeStatus_reasonLiteral = (Literal)e.Item.FindControl("UploadedResumeStatus_reasonLiteral");
                HtmlGenericControl UploadedResumeStatus_reasonDiv = (HtmlGenericControl)e.Item.FindControl("UploadedResumeStatus_reasonDiv");
                if (UploadedResumeStatus_reasonDiv != null)
                {
                    if (resumeDetail.Reason != null)
                    {
                        UploadedResumeStatus_reasonDiv.InnerText = resumeDetail.Reason.Replace("\r", "<br>")
                               .Replace("\a", "<br>").Replace("\n", "<br>");
                    }
                }

                ImageButton ReviewResume_resumeActiveImageButton = (ImageButton)e.Item.FindControl("ReviewResume_resumeActiveImageButton");
                if (ReviewResume_resumeActiveImageButton != null)
                {
                    if (resumeDetail.Active == "Y")
                    {
                        ReviewResume_resumeActiveImageButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_active.gif";
                        ReviewResume_resumeActiveImageButton.CommandName = "Active";
                        ReviewResume_resumeActiveImageButton.CommandArgument = "Y";
                        ReviewResume_resumeActiveImageButton.ToolTip = "Click here to deactivate the resume";
                    }
                    else
                    {
                        ReviewResume_resumeActiveImageButton.ToolTip = "Click here to activate the resume";
                        ReviewResume_resumeActiveImageButton.CommandName = "Active";
                        ReviewResume_resumeActiveImageButton.CommandArgument = "N";
                        ReviewResume_resumeActiveImageButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_inactive.gif";
                    }
                }

                ImageButton ReviewResume_deleteImageButton = (ImageButton)e.Item.FindControl("ReviewResume_deleteImageButton");
                if (ReviewResume_deleteImageButton != null)
                    ReviewResume_deleteImageButton.CommandName = "Delete";

                Label UploadedResumeStatus_reasonTitleLabel = (Label)e.Item.FindControl("UploadedResumeStatus_reasonTitleLabel");
                if (UploadedResumeStatus_reasonTitleLabel != null)
                {
                    if (!Utility.IsNullOrEmpty(resumeDetail.Reason))
                        UploadedResumeStatus_reasonTitleLabel.Visible = true;
                    else
                        UploadedResumeStatus_reasonTitleLabel.Visible = false;
                }

                HyperLink ReviewResume_candidateDashboardHyperLink = (HyperLink)e.Item.FindControl("ReviewResume_candidateDashboardHyperLink");
                if (resumeDetail.Complete == "N")
                {
                    ReviewResume_candidateDashboardHyperLink.ToolTip = "Click here to review and complete the resume";
                    ReviewResume_candidateDashboardHyperLink.NavigateUrl = "~/ResumeRepository/CandidateRecordManagement.aspx?candidateid=" + resumeDetail.CandidateId.ToString() + "&parentpage=" + Constants.ParentPage.REVIEW_RESUME + "&m=0&s=4";
                }
                else
                {
                    ReviewResume_candidateDashboardHyperLink.ToolTip = "Click here to manage the duplicate resume";
                    ReviewResume_candidateDashboardHyperLink.NavigateUrl = "~/ResumeRepository/DuplicateResumeManagement.aspx?candidateid=" + resumeDetail.CandidateId.ToString() + "&parentpage=" + Constants.ParentPage.REVIEW_RESUME + "&m=0&s=4";
                }


                HyperLink ReviewResume_emailImageButton = (HyperLink)e.Item.FindControl("ReviewResume_emailImageButton");
                if (!Utility.IsNullOrEmpty(resumeDetail.EmailID))
                {
                    ReviewResume_emailImageButton.NavigateUrl = "mailto:" + resumeDetail.EmailID;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                UploadedResumeStatus_bottomErrorLabel, exp.Message);
            }
        }

        protected void UploadedResumeStatus_pendingResumeDataList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                HiddenField UploadedResumeStatus_candidateIDHiddenField =
                    (HiddenField)e.Item.FindControl("UploadedResumeStatus_candidateIDHiddenField");
                int candidateID = 0;

                if (UploadedResumeStatus_candidateIDHiddenField != null)
                    candidateID = Convert.ToInt32(UploadedResumeStatus_candidateIDHiddenField.Value);

                if (e.CommandName == "Active")
                {
                    new ResumeRepositoryBLManager().UpdateTemporaryResumeStatus(candidateID, e.CommandArgument.ToString());
                    if (e.CommandArgument.ToString() == "N")
                        base.ShowMessage(UploadedResumeStatus_topSuccessLabel, "Activated successfully");
                    else
                        base.ShowMessage(UploadedResumeStatus_topSuccessLabel, "Inactivated successfully");
                }
                else if (e.CommandName == "Delete")
                {
                    ViewState["CURRENT_CANDIDAT_ID"] = candidateID.ToString();
                    ReviewResume_deleteResumeModalPopupExtender.Show();
                    /*new ResumeRepositoryBLManager().DeleteTemporaryResume(candidateID);
                    base.ShowMessage(UploadedResumeStatus_topSuccessLabel, "Deleted successfully"); */
                }
                ReviewResume_pageNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                (ReviewResume_pageNavigator_PageNumberClick);

                if (ViewState["pageNo"] != null)
                    GetReviewResumes(GetReviewResume_Parameters(Convert.ToInt32(ViewState["pageNo"])));

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                    UploadedResumeStatus_bottomErrorLabel, ex.Message);
            }
        }

        protected void DuplicateResumeManagement_newCandidateConfirmMsgControl_cancelClick
       (object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                      UploadedResumeStatus_bottomErrorLabel, ex.Message);
            }
        }

        protected void DuplicateResumeManagement_newCandidateConfirmMsgControl_okClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CURRENT_CANDIDAT_ID"] != null)
                {
                    new ResumeRepositoryBLManager().DeleteTemporaryResume(Convert.ToInt32(ViewState["CURRENT_CANDIDAT_ID"]));

                    if (!Utility.IsNullOrEmpty(ViewState["pageNo"]))
                        GetReviewResumes(GetReviewResume_Parameters(Convert.ToInt32(ViewState["pageNo"])));

                    base.ShowMessage(UploadedResumeStatus_topSuccessLabel, "Deleted successfully");
                }

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                     UploadedResumeStatus_bottomErrorLabel, ex.Message);
            }
        }

        protected void CandidateDashboard_resetTopLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        #endregion

        #region Private Methods
        private ReviewResumeDetail GetReviewResume_Parameters(int pageNo)
        {
            ReviewResumeDetail reviewResumeDetail = new ReviewResumeDetail();
            reviewResumeDetail.TenantID = base.tenantID;

            if (!Utility.IsNullOrEmpty(UploadedResumeStatus_profileNameSearchTextBox.Text))
            {
                reviewResumeDetail.ProfileName =
                    UploadedResumeStatus_profileNameSearchTextBox.Text.Trim();
            }

            if (!Utility.IsNullOrEmpty(ReviewResume_uploadedByDummyIdHidenField.Value))
            {
                reviewResumeDetail.RecruiterID =
                   Convert.ToInt32(ReviewResume_uploadedByDummyIdHidenField.Value);
            }

            if (UploadedResumeStatus_statusDropDownList.SelectedIndex > 0)
            {
                reviewResumeDetail.Status = UploadedResumeStatus_statusDropDownList.SelectedItem.Text;
            }
            if (!Utility.IsNullOrEmpty(UploadedResumeStatus_updateDateFromTextBox.Text))
            {
                reviewResumeDetail.FromDate = Convert.ToDateTime(UploadedResumeStatus_updateDateFromTextBox.Text);
            }

            if (!Utility.IsNullOrEmpty(UploadedResumeStatus_updateDateToTextBox.Text))
            {
                reviewResumeDetail.ToDate = Convert.ToDateTime(UploadedResumeStatus_updateDateToTextBox.Text).AddDays(1);
            }

            reviewResumeDetail.PageNumber = pageNo;
            reviewResumeDetail.PageSize = this.GridPageSize;

            Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REVIEW_RESUMES] = reviewResumeDetail;

            return reviewResumeDetail;
        }
        private void GetReviewResumes(ReviewResumeDetail reviewResumeDetail)
        {
            int totalRecords = 0;
            UploadedResumeStatus_pendingResumeDataList.DataSource = null;
            UploadedResumeStatus_pendingResumeDataList.DataBind();

            List<CandidateResumeDetails> candidateList = null;
            candidateList = new ResumeRepositoryBLManager().
                GetTemporaryResumes(reviewResumeDetail, out totalRecords);

            if (candidateList == null || candidateList.Count == 0)
            {
                UploadedResumeStatus_topSuccessLabel.Text = "";
                UploadedResumeStatus_bottomSuccessLabel.Text = "";

                base.ShowMessage(UploadedResumeStatus_topErrorLabel,
                    UploadedResumeStatus_bottomErrorLabel, "No data found to display");
                ReviewResume_pageNavigator.Visible = false;
                return;
            }
            else
            {
                ReviewResume_pageNavigator.Visible = true;
                UploadedResumeStatus_pendingResumeDataList.DataSource = candidateList;
                UploadedResumeStatus_pendingResumeDataList.DataBind();
                ReviewResume_pageNavigator.PageSize = this.GridPageSize;
            }
            if (!Utility.IsNullOrEmpty(reviewResumeDetail.PageNumber))
                ReviewResume_pageNavigator.MoveToPage(reviewResumeDetail.PageNumber);

            ReviewResume_pageNavigator.TotalRecords = totalRecords;
        }

        private void ClearControls()
        { 
            UploadedResumeStatus_topErrorLabel.Text =string.Empty;
            UploadedResumeStatus_bottomErrorLabel.Text = string.Empty;
        }
        #endregion

        #region Overridden Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}