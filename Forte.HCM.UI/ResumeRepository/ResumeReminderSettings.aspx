﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResumeReminderSettings.aspx.cs"
    Inherits="Forte.HCM.UI.ResumeRepository.ResumeReminderSettings" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="ResumeReminderSettings_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ResumeReminderSettings_headerLiteral" runat="server" Text="Resume Reminder Settings"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <asp:UpdatePanel ID="ResumeReminderSettings_topSaveUpdatePanel" runat="server">
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ResumeReminderSettings_topSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="ResumeReminderSettings_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ResumeReminderSettings_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="ResumeReminderSettings_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ResumeReminderSettings_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ResumeReminderSettings_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ResumeReminderSettings_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="ResumeReminderSettings_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr id="ResumeReminderSettings_emailOptionTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="ResumeReminderSettings_emailOptionLiteral" runat="server" 
                                        Text="Incomplete Resume Alerts"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel ID="ResumeReminderSettings_settingsUpdatePanel" runat="server">
                            <ContentTemplate>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="4" cellspacing="2" border="0">
                                                <tr>
                                                    <td style="width:450px">&nbsp;</td>
                                                    <td style="width:200px">
                                                        <asp:Label ID="ResumeReminderSettings_resumeUploaderLabel" Text="Resume Uploader"
                                                            SkinID="sknLabelFieldHeaderText" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_AdminLabel" Text="Admin" runat="server" 
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_immediateReminderLabelValue" 
                                                            Text="For each individual resume, as soon as an incomplete resume is detected"
                                                            runat="server" SkinID="sknLabelText">
                                                        </asp:Label>
                                                    </td>
                                                    <td style="padding-left:40px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_immediateReminderToResumeUploaderCheckBox"
                                                            runat="server" />
                                                    </td>
                                                    <td style="padding-left:12px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_immediateReminderToAdminCheckBox" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_onceInADayReminderLabel" Text="Once in a day, with a list of all incomplete resumes"
                                                            runat="server" SkinID="sknLabelText"></asp:Label>
                                                    </td>
                                                    <td style="padding-left:40px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_onceInADayReminderToResumeUploaderCheckBox"
                                                            runat="server" />
                                                    </td>
                                                    <td style="padding-left:12px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_onceInADayReminderToAdminCheckBox" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_onceInAWeekReminderLabel" Text="Once in a week, with a list of all incomplete resumes"
                                                            runat="server" SkinID="sknLabelText"></asp:Label>
                                                    </td>
                                                    <td style="padding-left:40px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_onceInAWeekReminderToResumeUploaderCheckBox"
                                                            runat="server" />
                                                    </td>
                                                    <td style="padding-left:12px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_onceInAWeekReminderToAdminCheckBox" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ResumeReminderSettings_topSaveButton" />
                                <asp:AsyncPostBackTrigger ControlID="ResumeReminderSettings_topResetLinkButton" />
                                <asp:AsyncPostBackTrigger ControlID="ResumeReminderSettings_bottomSaveButton" />
                                <asp:AsyncPostBackTrigger ControlID="ResumeReminderSettings_bottomResetLinkButton" />
                            </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="ResumeReminderSettings_duplicateLiteral" runat="server" 
                                        Text="Duplicate Resume Alerts"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                   <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel ID="ResumeReminderSettings_duplicateSettingsUpdatePanel" runat="server">
                            <ContentTemplate>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="4" cellspacing="2" border="0">
                                                <tr>
                                                    <td style="width:450px">&nbsp;</td>
                                                    <td style="width:200px">
                                                        <asp:Label ID="ResumeReminderSettings_duplicateResumeUploaderLabel" Text="Resume Uploader"
                                                            SkinID="sknLabelFieldHeaderText" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_duplicateAdminLabel" Text="Admin" runat="server" 
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_duplicateImmediateReminderLabelValue" 
                                                            Text="For each individual resume, as soon as duplicate resume is detected"
                                                            runat="server" SkinID="sknLabelText">
                                                        </asp:Label>
                                                    </td>
                                                    <td style="padding-left:40px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_duplicateImmediateReminderToResumeUploaderCheckBox"
                                                            runat="server" />
                                                    </td>
                                                    <td style="padding-left:12px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_duplicateImmediateReminderToAdminCheckBox" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_duplicateOnceInADayReminderLabel" 
                                                            Text="Once in a day, with a list of all duplicate resumes"
                                                            runat="server" SkinID="sknLabelText"></asp:Label>
                                                    </td>
                                                    <td style="padding-left:40px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_duplicateOnceInADayReminderToResumeUploaderCheckBox"
                                                            runat="server" />
                                                    </td>
                                                    <td style="padding-left:12px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_duplicateOnceInADayReminderToAdminCheckBox" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ResumeReminderSettings_duplicateOnceInAWeekReminderLabel" 
                                                            Text="Once in a week, with a list of all duplicate resumes"
                                                            runat="server" SkinID="sknLabelText"></asp:Label>
                                                    </td>
                                                    <td style="padding-left:40px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_duplicateOnceInAWeekReminderToResumeUploaderCheckBox"
                                                            runat="server" />
                                                    </td>
                                                    <td style="padding-left:12px">
                                                        <asp:CheckBox ID="ResumeReminderSettings_duplicateOnceInAWeekReminderToAdminCheckBox" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ResumeReminderSettings_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ResumeReminderSettings_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="ResumeReminderSettings_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <asp:UpdatePanel ID="ResumeReminderSettings_bottomSaveUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                <asp:Button ID="ResumeReminderSettings_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                                    Text="Save" OnClick="ResumeReminderSettings_saveButton_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="ResumeReminderSettings_bottomResetLinkButton" runat="server"
                                                    Text="Reset" OnClick="ResumeReminderSettings_resetLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="ResumeReminderSettings_bottomCancelLinkButton" runat="server"
                                                    Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
