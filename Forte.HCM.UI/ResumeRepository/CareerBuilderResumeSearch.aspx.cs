﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CareerBuilderResumeSearch.cs
// File that represents the user Serach the CareerBuilder API  by various Key filed.
// This will helps Search a Resume From the CareerBuilder API and downloaded to resume repository.
//

#endregion Header                                                              

#region Directives                                                             
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using System.Configuration;
using Forte.HCM.DataObjects;
#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class CareerBuilderResumeSearch : PageBase
    {
        #region Declaration                                                    
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        string EXTERNAL_RESUME_LIST_VIEWSTATE = "EXTERNAL_RESUME_LIST_VIEWSTATE";
        string CANDIDATE_DETAILS_LIST_VIEWSTATE = "CANDIDATE_DETAILS_LIST_VIEWSTATE";
        string pageSize = "3";
        #endregion Declaration

        #region Event Handler                                                  
                                                
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
                if (!IsPostBack)
                {
                    BindDropdownValues();
                }
                CareerBuilderResumeSearch_simplePagingNavigator.PageNumberClick += new CommonControls.PageNavigator.PageNumberClickEventHandler(CareerBuilderResumeSearch_simplePagingNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                   CareerBuilderResumeSearch_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handles the Click event of the CareerBuilderResumeSearch_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CareerBuilderResumeSearch_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handles the PageNumberClick event of the CareerBuilderResumeSearch_simplePagingNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Forte.HCM.EventSupport.PageNumberEventArgs"/>
        /// instance containing the event data.</param>
        void CareerBuilderResumeSearch_simplePagingNavigator_PageNumberClick(object sender,
            EventSupport.PageNumberEventArgs e)
        {
            try
            {
                SearchResult(e.PageNumber.ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                   CareerBuilderResumeSearch_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Command event of the CareerBuilderResumeSearch_simpleSearchButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/>
        /// instance containing the event data.</param>
        protected void CareerBuilderResumeSearch_simpleSearchButton_Command(object sender,
            CommandEventArgs e)
        {
            try
            {
                CareerBuilderResumeSearch_searchTypeHiddenField.Value = e.CommandName;
                CareerBuilderResumeSearch_simplePagingNavigator.Reset();
                SearchResult("1");

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                   CareerBuilderResumeSearch_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the CareerBuilderResumeSearch_GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void CareerBuilderResumeSearch_GridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if ((e.CommandName == "previewresume"))
                {
                    CareerBuilderResumeSearch_externalResumePreviewControl.ResumePreview = getExternalResume(e.CommandArgument.ToString());
                    CareerBuilderResumeSearch_externalResumePreviewModalPopupExtender.Show();
                }
                else
                {
                    ImageButton downloadResume = (ImageButton)e.CommandSource; // get event caused button
                    GridViewRow gvrow = (GridViewRow)downloadResume.NamingContainer; // get grid row contains event caused button
                    int index = gvrow.RowIndex;
                    GetResumeXML(index);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                   CareerBuilderResumeSearch_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handles the cancelClick event of the CareerBuilderResumeSearch_Preview control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CareerBuilderResumeSearch_Preview_cancelClick(object sender, EventArgs e)
        {
            try
            {
                CareerBuilderResumeSearch_resultGridViewDiv.Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                CareerBuilderResumeSearch_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the CareerBuilderResumeSearch_locationGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void CareerBuilderResumeSearch_locationGridView_RowDataBound(object sender,
           GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton delteImgaeButton = (ImageButton)e.Row.FindControl
                        ("CareerBuilderResumeSearch_locationGridView_deleteImageButton");
                    delteImgaeButton.CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                   CareerBuilderResumeSearch_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion end Event Handler

        #region Private methods                                                
                                             
        /// <summary>
        /// Searches the candidate details.
        /// </summary>
        /// <param name="careerBuilderSearchResultsList">The career builder search results list.</param>
        /// <param name="candidateInformationList">The candidate information list.</param>
        /// <returns></returns>
        private List<ExternalResume> SearchCandidateDetails(List<CareerBuilderSearchResults>
            careerBuilderSearchResultsList, out List<CandidateInformation> candidateInformationList)
        {
            List<ExternalResume> externalResumeList = new List<ExternalResume>();
            candidateInformationList = new List<CandidateInformation>();
            foreach (CareerBuilderSearchResults resumeResultItem in careerBuilderSearchResultsList)
            {
                GetResumeDetails(candidateInformationList, externalResumeList, resumeResultItem.ResumeID, resumeResultItem);
            }
            return externalResumeList;
        }

        /// <summary>
        /// Gets the resume details.
        /// </summary>
        /// <param name="candidateInformationList">The candidate information list.</param>
        /// <param name="externalResumeList">The external resume list.</param>
        /// <param name="resumeId">The resume id.</param>
        /// <param name="resumeResultItem">The resume result item.</param>
        private void GetResumeDetails(List<CandidateInformation> candidateInformationList, 
            List<ExternalResume> externalResumeList, string resumeId, CareerBuilderSearchResults resumeResultItem)
        {
            ExternalResume externalResume = new ExternalResume();
            externalResume = new ExternalResumeServiceBLManager().GetExternalResume(resumeId);
            XmlDocument xmlDoc = new XmlDocument();
            if (Support.Utility.IsNullOrEmpty(externalResume))
            {
                externalResume = new ExternalResume();
                Resumes resume = new Resumes();
                resume.Url = GetHWServiceURL();
                string packet = "<Packet><SessionToken>" + CareerBuilderResumeSearch_sessionTokenHiddenField.Value + "</SessionToken><ResumeID>" + resumeId + "</ResumeID><GetWordDocIfAvailable>True</GetWordDocIfAvailable></Packet>";
                string resumeXMLString = resume.V2_GetResume(packet);
                xmlDoc.LoadXml(resumeXMLString);
            }
            else
                xmlDoc.LoadXml(externalResume.ResumeDetails);

            XmlNode xmlNode = xmlDoc.SelectSingleNode("/Packet");
            CandidateInformation candidateInformation = new CandidateInformation();
            byte[] resumeContent = null;
            string resumeType = "txt";
            string resumeText = "";

            string candidateFirstName = "";
            externalResume.ResumeID = resumeId;

            externalResume.JobBoardID = "JB_CR_BLDR";

            externalResume.CandidateAddress = resumeResultItem.HomeLocation;
            externalResume.CandidateName = resumeResultItem.ContactName;
            externalResume.RecentPay = resumeResultItem.RecentPay;
            externalResume.LastModified = resumeResultItem.LastUpdate;
            externalResume.CandidateEmail = resumeResultItem.ContactEmail;


            for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
            {
                switch (xmlNode.ChildNodes[i].Name)
                {
                    case "ContactName":
                        candidateFirstName = xmlNode.ChildNodes[i].InnerText;
                        candidateInformation.caiFirstName = candidateFirstName;
                        break;
                    case "ContactEmail":
                        externalResume.CandidateEmail = xmlNode.ChildNodes[i].InnerText;
                        candidateInformation.caiEmail = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "ContactPhone":
                        externalResume.CandidatePhone = xmlNode.ChildNodes[i].InnerText;
                        candidateInformation.caiCell = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "HomeLocation":
                        candidateInformation.caiZip = xmlNode.ChildNodes[i].ChildNodes[3].InnerText;
                        externalResume.ZipCode = xmlNode.ChildNodes[i].ChildNodes[3].InnerText;
                        candidateInformation.caiLocation = xmlNode.ChildNodes[i].ChildNodes[0].InnerText;
                        break;
                    case "DesiredPay":
                        if (xmlNode.ChildNodes[i].ChildNodes[0].InnerText.Length > 0)
                            externalResume.DesiredPay = xmlNode.ChildNodes[i].ChildNodes[0].InnerText + " per " +
                                xmlNode.ChildNodes[i].ChildNodes[1].InnerText;
                        break;
                    case "TravelPreference":
                        externalResume.DesiredTravel = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "Relocations":
                        string relocations = "";
                        for (int j = 0; j < xmlNode.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[0].InnerText.Length > 0)
                                relocations = relocations + " " + xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[0].InnerText + ",";
                            if (xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[1].InnerText.Length > 0)
                                relocations = relocations + " " + xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[1].InnerText + ",";
                            if (xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[2].InnerText.Length > 0)
                                relocations = relocations + " " + xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[2].InnerText + ",";
                            if (xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[3].InnerText.Length > 0)
                                relocations = relocations + " " + xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[3].InnerText + ",";
                            if (xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[4].InnerText.Length > 0)
                                relocations = relocations + " " + xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[4].InnerText + ",";
                        }
                        externalResume.DesiredReLocations = relocations.TrimEnd(',');
                        break;
                    case "MaxCommuteMiles":
                        externalResume.DesiredCommute = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "DesiredJobTypes":
                        string jobTypes = "";
                        for (int j = 0; j < xmlNode.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            jobTypes = jobTypes + xmlNode.ChildNodes[i].ChildNodes[j].InnerText + ",";
                        }
                        externalResume.DesiredJobTypes = jobTypes.TrimEnd(',');
                        break;
                    case "MostRecentPay":
                        decimal cost2Company = 0;
                        decimal.TryParse(xmlNode.ChildNodes[i].ChildNodes[0].InnerText, out cost2Company);

                        if (xmlNode.ChildNodes[i].ChildNodes[1].InnerText == "month")
                        {
                            cost2Company = cost2Company * 12;
                        }
                        else if (xmlNode.ChildNodes[i].ChildNodes[1].InnerText == "hour")
                        {
                            cost2Company = cost2Company * 2400;
                        }
                        candidateInformation.caiC2CCost = cost2Company;
                       // externalResume.RecentPay = cost2Company.ToString();
                        break;
                    case "OriginalWordDoc":
                        if (xmlNode.ChildNodes[i].ChildNodes[0].InnerText.Length > 0)
                        {
                            string[] resumeTypeArray = xmlNode.ChildNodes[i].ChildNodes[0].InnerText.Split('.');
                            resumeType = resumeTypeArray[resumeTypeArray.Length - 1];
                            resumeContent = Convert.FromBase64String(xmlNode.ChildNodes[i].ChildNodes[1].InnerText);
                            xmlDoc.DocumentElement.RemoveChild(xmlNode.ChildNodes[i]);
                        }
                        externalResume.ResumeType = resumeType;
                        externalResume.ResumeContent = resumeContent;
                        break;
                    case "ResumeTitle":
                        externalResume.ResumeTitle = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "ResumeText":
                        resumeText = xmlNode.ChildNodes[i].InnerText;
                        externalResume.ResumeText = resumeText;
                        xmlDoc.DocumentElement.RemoveChild(xmlNode.ChildNodes[i]);
                        break;
                    case "MostRecentTitle":
                        externalResume.MostRecentTitle = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "WorkHistory":
                        List<Experience> experienceList = null;
                        for (int j = 0; j < xmlNode.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            if (j == 0)
                            {
                                externalResume.MostRecentCompanyName = xmlNode.ChildNodes[i].ChildNodes[0].ChildNodes[0].InnerText;
                                externalResume.MostRecentTitle = xmlNode.ChildNodes[i].ChildNodes[0].ChildNodes[1].InnerText;
                            }
                            if (Support.Utility.IsNullOrEmpty(experienceList))
                                experienceList = new List<Experience>();
                            Experience experience = new Experience();
                            //for (int k = 0; k < xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes.Count; k++)
                            {
                                experience.Company = xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[0].InnerText;
                                experience.Title = xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[1].InnerText;
                                experience.Period = xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[2].InnerText;
                            }
                            experienceList.Add(experience);
                        }
                        externalResume.ExperienceHistory = experienceList;
                        break;
                    case "EducationHistory":
                        if (Support.Utility.IsNullOrEmpty(xmlNode.ChildNodes[i].ChildNodes[0]))
                            continue;
                        List<EducationHistory> educationalList = null;
                        for (int j = 0; j < xmlNode.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            if (Support.Utility.IsNullOrEmpty(educationalList))
                                educationalList = new List<EducationHistory>();
                            if (j == 0)
                                externalResume.HighestDegree = xmlNode.ChildNodes[i].ChildNodes[0].ChildNodes[2].InnerText;
                            EducationHistory education = new EducationHistory();
                            education.School = xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[0].InnerText;
                            education.Major = xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[1].InnerText;
                            education.Degree = xmlNode.ChildNodes[i].ChildNodes[j].ChildNodes[2].InnerText;
                            educationalList.Add(education);
                        }
                        externalResume.EducationHistory = educationalList;
                        break;
                    case "ExperienceMonths":
                        externalResume.ExperienceMonths = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "MilitaryExperience":
                        externalResume.MilitaryExperience = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "SecurityClearance":
                        externalResume.SecurityClearance = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "Management":
                        string managedOthers = "No";
                        if (xmlNode.ChildNodes[i].ChildNodes.Count > 0)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[0].InnerText.ToLower() == "yes")
                                managedOthers = xmlNode.ChildNodes[i].ChildNodes[0].InnerText + " (" + xmlNode.ChildNodes[i].ChildNodes[1].InnerText + ")";
                        }
                        externalResume.ManagedOthers = managedOthers;
                        break;
                    case "FelonyConvictions":
                        externalResume.FelonyConviction = xmlNode.ChildNodes[i].InnerText;
                        break;
                    case "Languages":
                        string languages = "";
                        for (int j = 0; j < xmlNode.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            languages = languages + xmlNode.ChildNodes[i].ChildNodes[j].InnerText + ",";
                        }
                        externalResume.Languages = languages.TrimEnd(',');
                        break;
                }
            }
            externalResume.ResumeDetails = xmlDoc.InnerXml;
            candidateInformationList.Add(candidateInformation);
            externalResumeList.Add(externalResume);
        }

        /// <summary>
        /// Simples the search results.
        /// </summary>
        private void SimpleSearchResults()
        {
            List<ExternalResume> externalResumeList = ViewState[EXTERNAL_RESUME_LIST_VIEWSTATE] as List<ExternalResume>;
            CareerBuilderResumeSearch_simplePagingNavigator.PageSize = Int32.Parse(pageSize);
            CareerBuilderResumeSearch_simplePagingNavigator.TotalRecords = int.Parse(CareerBuilderResumeSearch_totalRecordsHiddenField.Value);
            CareerBuilderResumeSearch_simpleGridView.DataSource = externalResumeList;
            CareerBuilderResumeSearch_simpleGridView.DataBind();
        }

        /// <summary>
        /// Gets the external resume.
        /// </summary>
        /// <param name="resumeID">The resume ID.</param>
        /// <returns></returns>
        private ExternalResume getExternalResume(string resumeID)
        {
            List<ExternalResume> externalResumeList = ViewState[EXTERNAL_RESUME_LIST_VIEWSTATE] as List<ExternalResume>;
            return (externalResumeList.Find(item => item.ResumeID == resumeID));
        }

        /// <summary>
        /// Downloads the icon visibility.
        /// </summary>
        private void DownloadIconVisibility()
        {
            int tenantId = base.tenantID;
            foreach (GridViewRow gvr in CareerBuilderResumeSearch_simpleGridView.Rows)
            {
                ImageButton alreadyDownloadResume = (ImageButton)gvr.FindControl("alreadyDownloadResume");
                ImageButton downloadResume = (ImageButton)gvr.FindControl("downloadResume");
                if (new ExternalResumeServiceBLManager().GetExistingCandidateID(downloadResume.CommandArgument, tenantId) > 0)
                {
                    downloadResume.Visible = false;
                    alreadyDownloadResume.Visible = true;
                }
                else
                {
                    downloadResume.Visible = true;
                    alreadyDownloadResume.Visible = false;
                }
            }
        }

        /// <summary>
        /// Careers the builder resume search.
        /// </summary>
        /// <param name="pageNo">The page no.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        private List<CareerBuilderSearchResults> careerBuilderResumeSearch(string pageNo, out string errorMessage)
        {
            List<CareerBuilderSearchResults> careerBuilderSearchResultsList = null;
            Resumes resume = new Resumes();
            resume.Url = GetHWServiceURL();
            if (CareerBuilderResumeSearch_sessionTokenHiddenField.Value == "0")
            {
                SessionInfo sessionInfo = new SessionInfo();
                sessionInfo = resume.CB_BeginSession("Karthic@Geval6.com", "PXX3SY93");
                if (sessionInfo.Errors.Length > 0)
                {
                    errorMessage = sessionInfo.Errors[0].Text;
                    return careerBuilderSearchResultsList;
                }
                else
                    CareerBuilderResumeSearch_sessionTokenHiddenField.Value = sessionInfo.SessionToken;
                //CareerBuilderResumeSearch_sessionTokenHiddenField.Value = resume.CB_BeginSession("Karthic@Geval6.com", "PXX3SY93").SessionToken;
            }
            string location;
            string firstAlternationLocation;
            string secondAlternationLocation;
            GetLocationList(out location, out  firstAlternationLocation, out secondAlternationLocation);
            string packet = "";
            if (CareerBuilderResumeSearch_searchTypeHiddenField.Value == "simple")
            {
                packet = "<Packet><SessionToken>"
                    + CareerBuilderResumeSearch_sessionTokenHiddenField.Value
                    + "</SessionToken><Keywords>"
                    + CareerBuilderResumeSearch_simpleKeywordTextBox.Text + "</Keywords>"
                    + location + firstAlternationLocation + secondAlternationLocation
                    + "<PageNumber>" + pageNo + "</PageNumber><RowsPerPage>" + pageSize + "</RowsPerPage>"
                    + "</Packet>";
            }
            else
            {
                string employmentType = "";
                for (int i = 0; i < CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList.Items.Count; i++)
                {
                    if (CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList.Items[i].Selected)
                        employmentType = employmentType + CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList.Items[i].Value + "|";
                }
                string miltaryExperience = "";
                for (int i = 0; i < CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList.Items.Count; i++)
                {
                    if (CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList.Items[i].Selected)
                        miltaryExperience = miltaryExperience + CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList.Items[i].Value + "|";
                }
                string workStatus = "";
                for (int i = 0; i < CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList.Items.Count; i++)
                {
                    if (CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList.Items[i].Selected)
                        workStatus = workStatus + CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList.Items[i].Value + "|";
                }

                string managementExp = CareerBuilderResumeSearch_advanceManagementExperienceCheckbox.Checked == true ? "Yes" : "No";
                string securityClearance = CareerBuilderResumeSearch_advancesecurityCheckBox.Checked == true ? "Yes" : "No";
                string excludeSalaryCheckBox = CareerBuilderResumeSearch_advanceCheckSalaryCheckBox.Checked == true ? "Yes" : "No";

                packet = "<Packet><SessionToken>" + CareerBuilderResumeSearch_sessionTokenHiddenField.Value
                     + "</SessionToken><Keywords>" + CreateManualTest_advanceKeywordTextBox.Text.Trim()
                     + "</Keywords><SearchPattern>" + CreateManualTest_advanceKeywordUsingDropdownList.SelectedValue
                     + "</SearchPattern><JobCategories>" + CareerBuilderResumeSearch_advanceJobCategoryDropDownList.SelectedValue
                     + "</JobCategories><JobTitle>" + CreateManualTest_advanceJobTitleTextBox.Text
                     + "</JobTitle><Company>" + CreateManualTest_advanceCompanyNameUsingTextBox.Text.Trim()
                     + "</Company><ResumeTitle>" + CreateManualTest_advanceResumeTitleTextBox.Text.Trim()
                     + "</ResumeTitle><SearchRadiusInMiles></SearchRadiusInMiles><RelocationFilter></RelocationFilter><FreshnessInDays>" + CareerBuilderResumeSearch_advanceLastModifiedDropDownList.SelectedValue
                     + "</FreshnessInDays><EmploymentType>" + employmentType.TrimEnd('|')
                         + "</EmploymentType><MinimumTravelRequirement>" + CareerBuilderResumeSearch_advanceMinLevelTravelDropDownList.SelectedValue
                         + "</MinimumTravelRequirement><MinimumDegree>" + CareerBuilderResumeSearch_advanceMinDegreeDropDownList.SelectedValue
                         + "</MinimumDegree><CompensationType>" + CareerBuilderResumeSearch_advanceYearlyRadoibutonList.SelectedValue
                         + "</CompensationType><MinimumSalary>" + minSalareyTextBox.Text.Trim()
                         + "</MinimumSalary><MaximumSalary>" + minSalareyTextBox.Text.Trim()
                             + "</MaximumSalary><ExcludeResumesWithNoSalary>" + excludeSalaryCheckBox
                             + "</ExcludeResumesWithNoSalary><LanguagesSpoken>" + CareerBuilderResumeSearch_advanceLanguageDropDownList.SelectedValue
                             + "</LanguagesSpoken><CurrentlyEmployed></CurrentlyEmployed><ManagementExperience>" + managementExp
                                 + "</ManagementExperience><MinimumEmployeesManaged>" + CareerBuilderResumeSearch_advanceMinimumEmployeesManagedTextBox.Text
                                 + "</MinimumEmployeesManaged><MaximumCommute>" + CareerBuilderResumeSearch_advanceMinLevelTravelDropDownList.SelectedValue
                                 +"</MaximumCommute><SecurityClearance>" + securityClearance
                                 + "</SecurityClearance><WorkStatus>" + workStatus.TrimEnd('|')
                                 + "</WorkStatus><ExcludeIVRResumes></ExcludeIVRResumes><OrderBy></OrderBy><PageNumber>" + pageNo
                                     + "</PageNumber><RowsPerPage>" + pageSize
                                     + "</RowsPerPage><CBMinimumExperience>" + CareerBuilderResumeSearch_advanceMinYearsExperienceDropDownList.SelectedValue
                                     + "</CBMinimumExperience><CBMaximumExperience>" + CareerBuilderResumeSearch_advanceMaxYearsExperienceDropDownList.SelectedValue
                                     + "</CBMaximumExperience><MilitaryExperience>" + miltaryExperience.TrimEnd('|')
                                     + "</MilitaryExperience><School>" + CareerBuilderResumeSearch_advanceSchoolTextBox.Text
                                     +"</School>"
                                     + location + firstAlternationLocation + secondAlternationLocation+"</Packet>";
            }
            string searchResuletXMLString = resume.V2_AdvancedResumeSearch(packet);
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(searchResuletXMLString);
            XmlNode xmlNodeErrorCodeNode = xmlDoc.SelectSingleNode("/Packet/Errors");
            XmlNode xmlNodeHitsNode = xmlDoc.SelectSingleNode("/Packet/Hits");
            CareerBuilderResumeSearch_totalRecordsHiddenField.Value = xmlNodeHitsNode.InnerText;
           // CareerBuilderResumeSearch_searchResultsCountLabel.Text = "(" + xmlNodeHitsNode.InnerText + ")";
            errorMessage = xmlNodeErrorCodeNode.InnerText;
          
            if (xmlNodeHitsNode.InnerText == "0")
                return careerBuilderSearchResultsList;

            XmlNode xmlNode = xmlDoc.SelectSingleNode("/Packet/Results");
            if (Support.Utility.IsNullOrEmpty(xmlNode))
                return careerBuilderSearchResultsList;
            for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
            {
                if (Support.Utility.IsNullOrEmpty(careerBuilderSearchResultsList))
                    careerBuilderSearchResultsList = new List<CareerBuilderSearchResults>();
                CareerBuilderSearchResults careerBuilderSearchResults = new CareerBuilderSearchResults();
                switch (xmlNode.ChildNodes[i].Name)
                {
                    case "ResumeResultItem_V3":
                        careerBuilderSearchResults.ContactEmail = xmlNode.ChildNodes[i].ChildNodes[0].InnerText;
                        careerBuilderSearchResults.ContactName = xmlNode.ChildNodes[i].ChildNodes[1].InnerText;
                        careerBuilderSearchResults.HomeLocation = xmlNode.ChildNodes[i].ChildNodes[2].InnerText;
                        careerBuilderSearchResults.LastUpdate = xmlNode.ChildNodes[i].ChildNodes[3].InnerText;
                        careerBuilderSearchResults.RecentPay = xmlNode.ChildNodes[i].ChildNodes[8].InnerText;
                        careerBuilderSearchResults.ResumeID = xmlNode.ChildNodes[i].ChildNodes[9].InnerText;
                        break;
                }
                careerBuilderSearchResultsList.Add(careerBuilderSearchResults);
            }
            return careerBuilderSearchResultsList;
        }

        /// <summary>
        /// Gets the location list.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="firstAlternationLocation">The first alternation location.</param>
        /// <param name="secondAlternationLocation">The second alternation location.</param>
        private void GetLocationList(out string location, out string firstAlternationLocation, 
            out string secondAlternationLocation)
        {
            string[] strValue = locationIds.Value.TrimEnd(',').Split(',');
            string advance = "";
            if (CareerBuilderResumeSearch_searchTypeHiddenField.Value == "advance")
            {
                strValue = AdvancelocationIds.Value.TrimEnd(',').Split(',');
                advance = "Advance";
            }
            //  Page.Request.Form["locationIds"].ToString().TrimEnd(',').Split(',');
            location = "";
            StringBuilder locationStrBuilder = new StringBuilder();
            StringBuilder firstAlternationLocationStingBuilder = new StringBuilder();
            firstAlternationLocationStingBuilder.Append("<FirstAlternateLocation>");
            StringBuilder secondAlternationLocationStringBuilder = new StringBuilder();
            secondAlternationLocationStringBuilder.Append("<SecondAlternateLocation>");
            string city = "";
            string zipcode = "";
            string state = "";
            int j = 0;
            foreach (string locationId in strValue)
            {
                if (locationId.Length < 1)
                    continue;
                j = j + 1;
                string[] stringArray = Page.Request.Form[advance + "location" + locationId].ToString().TrimEnd(',').Split(',');
                for (int i = 0; i < stringArray.Length; i++)
                {
                    if (stringArray[i].Length < 1)
                        continue;
                    city = "";
                    zipcode = "";
                    state = "";

                    if (i == 0)
                        city = stringArray[i];
                    else if (i == 1)
                    {
                        int zipcode1;
                        int.TryParse(stringArray[i], out zipcode1);

                        if (zipcode1 < 1)
                            state = stringArray[i];
                        else
                            zipcode = zipcode1.ToString();
                    }

                    if (city.Length > 0)
                    {
                        if (j == 1)
                        {
                            locationStrBuilder.Append("<City>" + city + "</City>");
                        }
                        else if (j == 2)
                        {
                            firstAlternationLocationStingBuilder.Append("<City>" + city + "</City>");
                        }
                        else if (j == 3)
                        {
                            secondAlternationLocationStringBuilder.Append("<City>" + city + "</City>");
                        }
                    }
                    else if (zipcode.Length > 0)
                    {
                        if (j == 1)
                        {
                            locationStrBuilder.Append("<ZipCode>" + zipcode + "</ZipCode>");
                        }
                        else if (j == 2)
                        {
                            firstAlternationLocationStingBuilder.Append("<ZipCode>" + zipcode + "</ZipCode>");
                        }
                        else if (j == 3)
                        {
                            secondAlternationLocationStringBuilder.Append("<ZipCode>" + zipcode + "</ZipCode>");
                        }
                    }
                    else if (state.Length > 0)
                    {
                        if (j == 1)
                        {
                            locationStrBuilder.Append("<State>" + state + "</State>");
                        }
                        else if (j == 2)
                        {
                            firstAlternationLocationStingBuilder.Append("<State>" + state + "</State>");
                        }
                        else if (j == 3)
                        {
                            secondAlternationLocationStringBuilder.Append("<State>" + state + "</State>");
                        }
                    }
                }
            }
            firstAlternationLocationStingBuilder.Append("</FirstAlternateLocation>");
            secondAlternationLocationStringBuilder.Append("</SecondAlternateLocation>");

            location = locationStrBuilder.ToString();
            firstAlternationLocation = firstAlternationLocationStingBuilder.ToString();
            secondAlternationLocation = secondAlternationLocationStringBuilder.ToString();
        }

        /// <summary>
        /// Gets the uploaded resume path.
        /// </summary>
        /// <returns></returns>
        private string GetUploadedResumePath()
        {
            return ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"];
        }

        /// <summary>
        /// Gets the HW service URL.
        /// </summary>
        /// <returns></returns>
        private string GetHWServiceURL()
        {
            return ConfigurationManager.AppSettings["CAREER_BUILDER_API"];
        }

        /// <summary>
        /// Binds the dropdown values.
        /// </summary>
        private void BindDropdownValues()
        {
            if (IsPostBack)
                return;
            //     BindCategory(true);

            CareerBuilder careerBuilder = new CareerBuilder();
            CreateManualTest_advanceKeywordUsingDropdownList.DataSource = careerBuilder.GetUsingDropdown();
            CreateManualTest_advanceKeywordUsingDropdownList.DataValueField = "Value";
            CreateManualTest_advanceKeywordUsingDropdownList.DataTextField = "Key";
            CreateManualTest_advanceKeywordUsingDropdownList.DataBind();

            CreateManualTest_advanceJobTitleUsingDropdownList.DataSource = careerBuilder.GetUsingDropdown();
            CreateManualTest_advanceJobTitleUsingDropdownList.DataValueField = "Value";
            CreateManualTest_advanceJobTitleUsingDropdownList.DataTextField = "Key";
            CreateManualTest_advanceJobTitleUsingDropdownList.DataBind();

            CreateManualTest_advanceCompanyNameUsingDropdownList.DataSource = careerBuilder.GetUsingDropdown();
            CreateManualTest_advanceCompanyNameUsingDropdownList.DataValueField = "Value";
            CreateManualTest_advanceCompanyNameUsingDropdownList.DataTextField = "Key";
            CreateManualTest_advanceCompanyNameUsingDropdownList.DataBind();

            CreateManualTest_advanceResumeTitleUsingDropdownList.DataSource = careerBuilder.GetUsingDropdown();
            CreateManualTest_advanceResumeTitleUsingDropdownList.DataValueField = "Value";
            CreateManualTest_advanceResumeTitleUsingDropdownList.DataTextField = "Key";
            CreateManualTest_advanceResumeTitleUsingDropdownList.DataBind();


            CareerBuilderResumeSearch_simpleLastModifiedDropDownList.DataSource = careerBuilder.GetLastModified();
            CareerBuilderResumeSearch_simpleLastModifiedDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_simpleLastModifiedDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_simpleLastModifiedDropDownList.DataBind();


            CareerBuilderResumeSearch_advanceMinDegreeDropDownList.DataSource = careerBuilder.GetDegree();
            CareerBuilderResumeSearch_advanceMinDegreeDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceMinDegreeDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceMinDegreeDropDownList.DataBind();

            CareerBuilderResumeSearch_advanceMaxDegreeDropDownList.DataSource = careerBuilder.GetDegree();
            CareerBuilderResumeSearch_advanceMaxDegreeDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceMaxDegreeDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceMaxDegreeDropDownList.DataBind();

            CareerBuilderResumeSearch_advanceMinYearsExperienceDropDownList.DataSource = careerBuilder.GetYear(1980);
            CareerBuilderResumeSearch_advanceMinYearsExperienceDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceMinYearsExperienceDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceMinYearsExperienceDropDownList.DataBind();

            CareerBuilderResumeSearch_advanceMaxYearsExperienceDropDownList.DataSource = careerBuilder.GetYear(1980);
            CareerBuilderResumeSearch_advanceMaxYearsExperienceDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceMaxYearsExperienceDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceMaxYearsExperienceDropDownList.DataBind();

            CareerBuilderResumeSearch_advanceJobCategoryDropDownList.DataSource = careerBuilder.GetJobTypeCode();
            CareerBuilderResumeSearch_advanceJobCategoryDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceJobCategoryDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceJobCategoryDropDownList.DataBind();

            CareerBuilderResumeSearch_advanceLastModifiedDropDownList.DataSource = careerBuilder.GetLastModified();
            CareerBuilderResumeSearch_advanceLastModifiedDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceLastModifiedDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceLastModifiedDropDownList.DataBind();


            CareerBuilderResumeSearch_advanceMinLevelTravelDropDownList.DataSource = careerBuilder.GetWorkExperienceLevelOfTravel();
            CareerBuilderResumeSearch_advanceMinLevelTravelDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceMinLevelTravelDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceMinLevelTravelDropDownList.DataBind();

            CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList.DataSource = careerBuilder.GetEmploymentType();
            CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceEmplymentTypeCheckBoxList.DataBind();


            CareerBuilderResumeSearch_advanceManagementLevelCheckBoxlist.DataSource = careerBuilder.GetManagementLevel();
            CareerBuilderResumeSearch_advanceManagementLevelCheckBoxlist.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceManagementLevelCheckBoxlist.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceManagementLevelCheckBoxlist.DataBind();


            CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList.DataSource = careerBuilder.GetWorkStatus();
            CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceWorkStatusCheckBoxList.DataBind();


            CareerBuilderResumeSearch_advanceLanguageDropDownList.DataSource = careerBuilder.GetLanguage();
            CareerBuilderResumeSearch_advanceLanguageDropDownList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceLanguageDropDownList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceLanguageDropDownList.DataBind();

            CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList.DataSource = careerBuilder.GetMiltaryExperience();
            CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList.DataValueField = "Value";
            CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList.DataTextField = "Key";
            CareerBuilderResumeSearch_advanceMiltaryExperienceCheckBoxList.DataBind();

        }

        /// <summary>
        /// Searches the result.
        /// </summary>
        /// <param name="pageNo">The page no.</param>
        private void SearchResult(string pageNo)
        {
            string searchType = CareerBuilderResumeSearch_searchTypeHiddenField.Value;
            CareerBuilderResumeSearch_topErrorMessageLabel.Text = "";
            CareerBuilderResumeSearch_bottomErrorMessageLabel.Text = "";
            List<CandidateInformation> candidateInformationList;
            string errorMessage = "";
            List<CareerBuilderSearchResults> careerBuilderSearchResultsList = 
                careerBuilderResumeSearch(pageNo, out errorMessage);
            //ResumeSearchResults resumeSearchResults = SimpleSearch(pageNo);

            if (!Support.Utility.IsNullOrEmpty(careerBuilderSearchResultsList))
            {
                List<ExternalResume> externalResumeList = SearchCandidateDetails(careerBuilderSearchResultsList, 
                    out candidateInformationList);
                ViewState[EXTERNAL_RESUME_LIST_VIEWSTATE] = externalResumeList;
                ViewState[CANDIDATE_DETAILS_LIST_VIEWSTATE] = candidateInformationList;
                CareerBuilderResumeSearch_simplePagingNavigator.Visible = true;
                SimpleSearchResults();
                DownloadIconVisibility();
                CareerBuilderResumeSearch_resultGridViewDiv.Visible = true;
            }
            else
            {
                if (errorMessage.Length == 0)
                    errorMessage = "No Data found for this search criteria";
                ViewState[EXTERNAL_RESUME_LIST_VIEWSTATE] = null;
                ViewState[CANDIDATE_DETAILS_LIST_VIEWSTATE] = null;
                CareerBuilderResumeSearch_simplePagingNavigator.PageSize = 0;
                CareerBuilderResumeSearch_simplePagingNavigator.TotalRecords = 0;
                CareerBuilderResumeSearch_simpleGridView.DataSource = null;
                CareerBuilderResumeSearch_simpleGridView.DataBind();
                ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                    CareerBuilderResumeSearch_bottomErrorMessageLabel, errorMessage);
                CareerBuilderResumeSearch_simplePagingNavigator.Visible = false;
                CareerBuilderResumeSearch_resultGridViewDiv.Visible = false;
            }
        }

        /// <summary>
        /// Gets the resume XML.
        /// </summary>
        /// <param name="index">The index.</param>
        void GetResumeXML(int index)
        {
            try
            {
                List<ExternalResume> externalResumeList = ViewState[EXTERNAL_RESUME_LIST_VIEWSTATE] as List<ExternalResume>;
                List<CandidateInformation> candidateInformationList = ViewState[CANDIDATE_DETAILS_LIST_VIEWSTATE] as List<CandidateInformation>;
                ExternalResume externalResume = new ExternalResume();
                externalResume = externalResumeList[index];
                CandidateInformation candidateInformation = new CandidateInformation();
                candidateInformation = candidateInformationList[index];
                int candidateID = 0;

                candidateID = new ExternalResumeServiceBLManager().GetExistingCandidateID(externalResume.ResumeID, null);
                if (candidateID > 0)
                {
                    new ExternalResumeServiceBLManager().InsertTenanatCandidate(base.tenantID, candidateID, base.userID);
                }
                else
                {
                    candidateInformation.caiType = "CT_CR_BLDR";
                    candidateInformation.caiLastName = string.Empty;
                    candidateID = new ExternalResumeServiceBLManager().InsertCareerBuiderResume(externalResume, candidateInformation, base.userID, base.tenantID);
                }
                if (candidateID == 0)
                    base.ShowMessage(CareerBuilderResumeSearch_topErrorMessageLabel,
                    CareerBuilderResumeSearch_bottomErrorMessageLabel, "Resume not stored in resume repository");
                else
                {
                   
                    string resumeType = externalResume.ResumeType;
                   // string filePath = GetUploadedResumePath();
                    string fileName = candidateID + "_" + candidateInformation.caiFirstName + "." + resumeType;
                    string filePath = GetUploadedResumePath() + fileName;

                    if (!new FileInfo(filePath).Exists)
                    {
                        if (resumeType == "txt")
                        {
                            StreamWriter SW;
                            SW = File.CreateText(filePath);
                            SW.WriteLine(externalResume.ResumeText);
                            SW.Close();
                        }
                        else
                        {
                            FileStream fileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.ReadWrite);
                            fileStream.Write(externalResume.ResumeContent, 0, externalResume.ResumeContent.Length);
                            fileStream.Close();
                        }
                    }
                    new ResumeRepositoryBLManager().InsertDownloadloadResumeLog(candidateID, candidateInformation.caiFirstName, fileName, base.userID);
                    ShowMessage(CareerBuilderResumeSearch_topSuccessMessageLabel,
                           CareerBuilderResumeSearch_bottomSuccessMessageLabel,
                           "Resume stored in resume repository");
                    CareerBuilderResumeSearch_resultGridViewDiv.Visible = true;

                }
                SimpleSearchResults();
                DownloadIconVisibility();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Clears the message.
        /// </summary>
        private void ClearMessage()
        {
            CareerBuilderResumeSearch_topSuccessMessageLabel.Text = string.Empty;
            CareerBuilderResumeSearch_topErrorMessageLabel.Text = string.Empty;
            CareerBuilderResumeSearch_bottomErrorMessageLabel.Text = string.Empty;
            CareerBuilderResumeSearch_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Javascripts the method.
        /// </summary>
        private void JavascriptMethod()
        {
            CareerBuilderResumeSearch_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                CareerBuilderResumeSearch_resultGridViewDiv.ClientID + "','" +
                CareerBuilderResumeSearch_searchByResumeDiv.ClientID + "','" +
                CareerBuilderResumeSearch_searchResultsUpSpan.ClientID + "','" +
                CareerBuilderResumeSearch_searchResultsDownSpan.ClientID + "','" +
                CareerBuilderResumeSearch_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");


        }

        /// <summary>
        /// Checks the and set expandor restore.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CareerBuilderResumeSearch_restoreHiddenField.Value) &&
                CareerBuilderResumeSearch_restoreHiddenField.Value == "Y")
            {
                CareerBuilderResumeSearch_searchByResumeDiv.Style["display"] = "none";
                CareerBuilderResumeSearch_searchResultsUpSpan.Style["display"] = "block";
                CareerBuilderResumeSearch_searchResultsDownSpan.Style["display"] = "none";
                CareerBuilderResumeSearch_resultGridViewDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CareerBuilderResumeSearch_searchByResumeDiv.Style["display"] = "block";
                CareerBuilderResumeSearch_searchResultsUpSpan.Style["display"] = "none";
                CareerBuilderResumeSearch_searchResultsDownSpan.Style["display"] = "block";
                CareerBuilderResumeSearch_resultGridViewDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CheckAndSetExpandorRestore();
            ClearMessage();
            CareerBuilderResumeSearch_resultGridViewDiv.Visible = false;

            JavascriptMethod();
            Master.SetPageCaption("Search Career Builder");
        }
        #endregion Protected Overridden Methods
    }
}