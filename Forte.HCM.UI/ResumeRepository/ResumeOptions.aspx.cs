﻿#region Directives                                                             
using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.ResumeRepository
{
    /// <summary>
    /// Represents the class Options that is used to save the 
    /// parser settings
    /// </summary>
    public partial class ResumeOptions : PageBase
    {
        #region Event Handler                                                  
        /// <summary>
        /// Represents the event handler that is called on page load 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that hodls the event args 
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page caption
                Master.SetPageCaption("Options");

                //Set the default focus and default button
                Page.Form.DefaultButton = ResumeOptions_topSaveButton.UniqueID;

                ResumeOptions_versionCodeTextBox.Focus();

                if (!IsPostBack)
                {
                    //Load the values for the first time
                    LoadValues();
                }

                //Reset the message labels 
                ResumeOptions_topErrorMessageLabel.Text = string.Empty;
                ResumeOptions_bottomErrorMessageLabel.Text = string.Empty;

                ResumeOptions_topSuccessMessageLabel.Text = string.Empty;
                ResumeOptions_bottomSuccessMessageLabel.Text = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(ResumeOptions_topErrorMessageLabel,
                    ResumeOptions_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ResumeOptions_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ParserSettings parserSetting = new ParserSettings();
                parserSetting.VersionCode = ResumeOptions_versionCodeTextBox.Text.Trim();
                parserSetting.ModifiedBy = base.userID;

                if (ResumeOptions_versionCodeTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(ResumeOptions_topErrorMessageLabel, ResumeOptions_bottomErrorMessageLabel,
                        "Version code cannot be empty");
                }
                else
                {
                    new ResumeRepositoryBLManager().UpdateParserSettings(parserSetting);

                    base.ShowMessage(ResumeOptions_topSuccessMessageLabel, ResumeOptions_bottomSuccessMessageLabel,
                        "Version code updated successfully");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(ResumeOptions_topErrorMessageLabel,
                    ResumeOptions_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirects to the same page by loading the previously
        /// saved values.
        /// </remarks>
        protected void ResumeOptions_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(ResumeOptions_topErrorMessageLabel,
                    ResumeOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event handler that is called on restore default link 
        /// button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void ResumeOptions_restoreTechnicalSettingLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(ResumeOptions_topErrorMessageLabel,
                    ResumeOptions_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handler

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            ParserSettings parserSettings = new ResumeRepositoryBLManager().GetParserOptions();
            ResumeOptions_versionCodeTextBox.Text = parserSettings.VersionCode;
        }

        #endregion Protected Overridden Methods
    }
}
