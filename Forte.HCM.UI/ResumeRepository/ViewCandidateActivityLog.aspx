﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCandidateActivityLog.aspx.cs"
    MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" Inherits="Forte.HCM.UI.ResumeRepository.ViewCandidateActivityLog" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<%@ Register Src="../CommonControls/CandidateActivityViewerControl.ascx" TagName="CandidateActivityViewerControl"
    TagPrefix="uc1" %>
<asp:Content ID="ViewCandidateActivityLog_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
 <script type="text/javascript" language="javascript">

     // Method that opens the add notes popup.
     // candidateID - Refers to the candidate ID (CandidateInformation table).
     function OpenAddNotesPopupWithRefresh(candidateID)
     {
         var height = 260;
         var width = 620;
         var top = (screen.availHeight - parseInt(height)) / 2;
         var left = (screen.availWidth - parseInt(width)) / 2;
         var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
                + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

         var queryStringValue = "../Popup/AddNotes.aspx?candidateid=" + candidateID;

         window.open(queryStringValue, window.self, sModalFeature);

         document.getElementById('<%=ViewCandidateActivityLog_refreshHiddenField.ClientID %>').value = 'Y';
         
         __doPostBack('<%=ViewCandidateActivityLog_topRefreshLinkButton.ClientID %>', "OnClick");
     }

 </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg" style="width: 100%">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ViewCandidateActivityLog_headerLiteral" runat="server" Text="Candidate Activity Log"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_topAddNotesLinkButton" runat="server" ToolTip="Click here to add notes"
                                            Text="Add Notes" SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_addNotesLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_topDownloadLinkButton" runat="server" ToolTip="Click here to download the activity log"
                                            Text="Download" SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_downloadLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_topRefreshLinkButton" runat="server" ToolTip="Click here to refresh the activity log"
                                            Text="Refresh" SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_refreshLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_topResetLinkButton" runat="server" Text="Reset" ToolTip="Click here to reset the page"
                                            SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_topCancelLinkButton" runat="server" ToolTip="Click here to navigate back to the parent page"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ViewCandidateActivityLog_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ViewCandidateActivityLog_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="ViewCandidateActivityLog_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <uc1:CandidateActivityViewerControl ID="ViewCandidateActivityLog_candidateActivityViewerControl"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ViewCandidateActivityLog_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ViewCandidateActivityLog_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="ViewCandidateActivityLog_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_bottomAddNotesLinkButton" runat="server" ToolTip="Click here to add notes"
                                            Text="Add Notes" SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_addNotesLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_bottomDownloadLinkButton" runat="server" ToolTip="Click here to download the activity log"
                                            Text="Download" SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_downloadLinkButton_Click"></asp:LinkButton>
                                            <asp:HiddenField ID="ViewCandidateActivityLog_refreshHiddenField" runat="server" />
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_bottmRefreshLinkButton" runat="server" ToolTip="Click here to refresh the activity log"
                                            Text="Refresh" SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_refreshLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_bottomResetLinkButton" runat="server" ToolTip="Click here to reset the page"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="ViewCandidateActivityLog_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ViewCandidateActivityLog_bottomCancelLinkButton" runat="server" ToolTip="Click here to navigate back to the parent page"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
