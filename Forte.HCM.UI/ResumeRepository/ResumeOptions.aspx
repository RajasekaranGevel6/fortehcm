<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" AutoEventWireup="true"
    CodeBehind="ResumeOptions.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepository.ResumeOptions" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="ResumeOptions_placeHolderContent" ContentPlaceHolderID="ResumeRepositoryMaster_body"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="ResumeOptions_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellpadding="0" align="center">
                            <tr>
                                <td class="header_bg">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" class="header_text_bold">
                                                <asp:Literal ID="ResumeOptions_headerLiteral" runat="server" Text="Options"></asp:Literal>
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="ResumeOptions_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                                OnClick="ResumeOptions_saveButton_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="ResumeOptions_topResetLinkButton" runat="server" Text="Reset" OnClick="ResumeOptions_resetLinkButton_Click"
                                                                SkinID="sknActionLinkButton"></asp:LinkButton>
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="ResumeOptions_topCancelLinkButton" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                                                OnClick="ParentPageRedirect"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ResumeOptions_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ResumeOptions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Literal ID="ResumeOptions_technicalSettingLabel" runat="server" Text="Technical Skills"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tab_body_bg">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width: 10%;">
                                                                        <asp:Label ID="ResumeOptions_versionCodeLabel" runat="server" Text="Version Code" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 34%;">
                                                                        <asp:TextBox ID="ResumeOptions_versionCodeTextBox" runat="server"  Width="300px" MaxLength="200" ></asp:TextBox>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:LinkButton ID="ResumeOptions_restoreTechnicalSettingLinkButton" runat="server" Text="Restore Defaults"
                                                                            ToolTip="Click here to restore default technical setting values" SkinID="sknActionLinkButton"
                                                                            OnClick="ResumeOptions_restoreTechnicalSettingLinkButton_Click"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ResumeOptions_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ResumeOptions_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                <asp:Button ID="ResumeOptions_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                    OnClick="ResumeOptions_saveButton_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="ResumeOptions_bottomResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                                    OnClick="ResumeOptions_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="ResumeOptions_bottomCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
