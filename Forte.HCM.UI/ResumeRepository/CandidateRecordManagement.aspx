﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CandidateRecordManagement.aspx.cs"
    Inherits="Forte.HCM.UI.ResumeRepository.CandidateRecordManagement" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" %>

<%@ Register Src="~/CommonControls/ResumeContactInformationControl.ascx" TagName="ContactInformationControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ResumeExecutiveSummaryControl.ascx" TagName="ExecutiveSummaryControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ResumeTechnicalSkillsControl.ascx" TagName="TechnicalSkillsControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/ResumeProjectsControl.ascx" TagName="ProjectsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ResumeEducationControl.ascx" TagName="EducationControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ResumeReferencesControl.ascx" TagName="ReferencesControl"
    TagPrefix="uc6" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc7" %>
<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="CandidateRecordManagement_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <script type="text/javascript" language="javascript">
        function OnResumeMenuClick(menuType) {
            // Hide all div.
            document.getElementById('<%=CandidateRecordManagement_selectedMenuHiddenField.ClientID %>').value = menuType;
            document.getElementById('<%=CandidateRecordManagement_contactInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=CandidateRecordManagement_executiveInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=CandidateRecordManagement_technicalInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=CandidateRecordManagement_projectsInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=CandidateRecordManagement_educationInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=CandidateRecordManagement_referencesInfoDiv.ClientID %>').style.display = 'none';


            // Reset color.
            document.getElementById('<%=CandidateRecordManagement_contactInfoLinkButton.ClientID %>').className = "associate_resume_link_button";
            document.getElementById('<%=CandidateRecordManagement_executiveInfoLinkButton.ClientID %>').className = "associate_resume_link_button";
            document.getElementById('<%=CandidateRecordManagement_technicalInfoLinkButton.ClientID %>').className = "associate_resume_link_button";
            document.getElementById('<%=CandidateRecordManagement_projectsInfoLinkButton.ClientID %>').className = "associate_resume_link_button";
            document.getElementById('<%=CandidateRecordManagement_educationInfoLinkButton.ClientID %>').className = "associate_resume_link_button";
            document.getElementById('<%=CandidateRecordManagement_referencesInfoLinkButton.ClientID %>').className = "associate_resume_link_button";


            if (menuType == 'CI') {
                document.getElementById('<%=CandidateRecordManagement_contactInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=CandidateRecordManagement_contactInfoLinkButton.ClientID %>').className = "associate_resume_link_button_selected";
            }
            else if (menuType == 'SI') {
                document.getElementById('<%=CandidateRecordManagement_executiveInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=CandidateRecordManagement_executiveInfoLinkButton.ClientID %>').className = "associate_resume_link_button_selected";
            }
            else if (menuType == 'TI') {
                document.getElementById('<%=CandidateRecordManagement_technicalInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=CandidateRecordManagement_technicalInfoLinkButton.ClientID %>').className = "associate_resume_link_button_selected";
            }
            else if (menuType == 'PI') {
                document.getElementById('<%=CandidateRecordManagement_projectsInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=CandidateRecordManagement_projectsInfoLinkButton.ClientID %>').className = "associate_resume_link_button_selected";
            }
            else if (menuType == 'EI') {
                document.getElementById('<%=CandidateRecordManagement_educationInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=CandidateRecordManagement_educationInfoLinkButton.ClientID %>').className = "associate_resume_link_button_selected";
            }
            else if (menuType == 'RI') {
                document.getElementById('<%=CandidateRecordManagement_referencesInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=CandidateRecordManagement_referencesInfoLinkButton.ClientID %>').className = "associate_resume_link_button_selected";
            }

        }
        // Handler for onchange and onkeyup to check for max characters.
        function CommentsCount(characterLength, clientId) {
            var maxlength = new Number(characterLength);
            if (clientId.value.length > maxlength) {
                clientId.value = clientId.value.substring(0, maxlength);
            }
        }
        // Function that shows the create candidate popup
        function ShowCandidatePopup(nameCtrl, emailCtrl, idCtrl) {
            var height = 540;
            var width = 980;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/AddCandidate.aspx?namectrl=" + nameCtrl;
            queryStringValue += "&emailctrl=" + emailCtrl;
            queryStringValue += "&idctrl=" + idCtrl;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }
     
    </script>
    <style type="text/css">
        .missing_fields
        {
            background-color: red;
            border: 1px solid #C8CBD2;
            padding: 5px;
            color: #FFFFFF;
            font-size: 10px;
            font-weight: bold;
        }
    </style>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%;" class="header_text_bold">
                            <asp:Literal ID="CandidateRecordManagement_headerLiteral" runat="server" Text="Incomplete Record Management"></asp:Literal>
                        </td>
                        <td style="width: 50%;">
                            <asp:UpdatePanel ID="CandidateRecordManagement_topResetUpdatePanel" runat="server">
                            <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="CandidateRecordManagement_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="CandidateRecordManagement_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CandidateRecordManagement_topCancelLinkButton" runat="server"
                                            Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CandidateRecordManagement_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CandidateRecordManagement_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="CandidateRecordManagement_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CandidateRecordManagement_resumeInfoUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate> 
                            <div id="CandidateRecordManagement_displayProfileDetails" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="panel_inner_body_bg">
                                                        <table width="100%" cellpadding="2" cellspacing="3">
                                                            <tr>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="CandidateRecordManagement_candidateNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Candidate Name"></asp:Label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="CandidateRecordManagement_uploadedOnLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Uploaded On"></asp:Label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="CandidateRecordManagement_uploadedByLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Uploaded By"></asp:Label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <asp:Label ID="CandidateRecordManagement_statusLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Status"></asp:Label>
                                                                </td>
                                                                <td style="width: 200px">
                                                                    <asp:Label ID="CandidateRecordManagement_reasonLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                        Text="Reason"></asp:Label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100px" valign="top">
                                                                    <asp:Label ID="CandidateRecordManagement_candidateNameLabelValue" runat="server"
                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                                <td style="width: 100px" valign="top">
                                                                    <asp:Label ID="CandidateRecordManagement_uploadedOnLabelValue" runat="server" SkinID="sknLabelFieldText">
                                                                    </asp:Label>
                                                                </td>
                                                                <td style="width: 100px" valign="top">
                                                                    <asp:Label ID="CandidateRecordManagement_uploadedByLabelValue" runat="server" SkinID="sknLabelFieldText">
                                                                    </asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateRecordManagement_statusLabelValue" runat="server" SkinID="sknLabelFieldText">
                                                                    </asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <div style="overflow:auto;height:150px">
                                                                    <asp:Label ID="CandidateRecordManagement_reasonLabelValue" runat="server" SkinID="sknLabelFieldText">
                                                                    </asp:Label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <asp:UpdatePanel ID="CandidateRecordManagement_deleteResumeUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:Button ID="CandidateRecordManagement_deleteResumeButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Delete" OnClick="CandidateRecordManagement_deleteResumeButton_Click" />
                                                                            <asp:Panel ID="CandidateRecordManagement_deleteResumePanel" runat="server" Style="display: none"
                                                                                CssClass="client_confirm_message_box">
                                                                                <uc7:ConfirmMsgControl ID="CandidateRecordManagement_deleteResumeConfirmMsgControl"
                                                                                    runat="server" OnCancelClick="CandidateRecordManagement_deleteResumeConfirmMsgControl_cancelClick"
                                                                                    OnOkClick="CandidateRecordManagement_deleteResumeConfirmMsgControl_okClick" />
                                                                            </asp:Panel>
                                                                            <ajaxToolKit:ModalPopupExtender ID="CandidateRecordManagement_deleteResumeModalPopupExtender"
                                                                                runat="server" PopupControlID="CandidateRecordManagement_deleteResumePanel" 
                                                                                TargetControlID="CandidateRecordManagement_deleteResumeButton"
                                                                                BackgroundCssClass="modalBackground">
                                                                            </ajaxToolKit:ModalPopupExtender>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="CandidateRecordManagement_saveButton" />
                            </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div1" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="panel_inner_body_bg">
                                                        <table width="100%" cellpadding="2" cellspacing="3">
                                                            <tr>
                                                                <td>
                                                                    <div runat="server" id="CandidateRecordManagement_perviewHtmlDiv" style="overflow: auto;
                                                                        width: 950px; height: 100px" class="resume_ListViewNormalStyle">
                                                                    </div>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:ImageButton ID="CandidateRecordManagement_downloadImageButton" runat="server"
                                                                        Text="Download" SkinID="sknDownloadImageButton" ToolTip="Download Resume" AlternateText="Download"
                                                                        OnClick="CandidateRecordManagement_downloadImageButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr id="CandidateRecordManagement_assignRolesGridViewheaderTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="CandidateRecordManagement_searchResultsLiteral" runat="server" Text="Resume Detail"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <asp:UpdatePanel ID="CandidateRecordManagement_assignRolesGridViewUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="CandidateRecordManagement_assignRolesGridViewDIV" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 90px; text-align: center" class="missing_fields">
                                                                <asp:Label ID="CandidateRecordManagement_missingFieldsLabel" runat="server" Text="Missing Fields"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <!-- Resume menu links -->
                                                                <div id="CandidateRecordManagement_Panel_Links">
                                                                    <div id="CandidateRecordManagement_Links">
                                                                        <asp:UpdatePanel ID="CandidateRecordManagement_menuUpdatePanel" runat="server">
                                                                            <ContentTemplate>
                                                                                <div id="CandidateRecordManagement_contactInfoLinkButtionDiv" class="resume_float_left">
                                                                                    <asp:HiddenField ID="CandidateRecordManagement_selectedMenuHiddenField" runat="server"
                                                                                        Value="CI" />
                                                                                    <asp:LinkButton ID="CandidateRecordManagement_contactInfoLinkButton" runat="server"
                                                                                        Text="Contact Information" ToolTip="Contact Information" CssClass="associate_resume_link_button_selected"
                                                                                        OnClientClick="javascript: return OnResumeMenuClick('CI')"></asp:LinkButton>
                                                                                </div>
                                                                                <div id="CandidateRecordManagement_executiveSummaryLinkButtionDiv" class="resume_float_left">
                                                                                    <asp:LinkButton ID="CandidateRecordManagement_executiveInfoLinkButton" runat="server"
                                                                                        Text="Executive Summary" ToolTip="Executive Summary" CssClass="associate_resume_link_button"
                                                                                        OnClientClick="javascript: return OnResumeMenuClick('SI')"> </asp:LinkButton>
                                                                                </div>
                                                                                <div id="CandidateRecordManagement_technicalSkillsLinkButtionDiv" class="resume_float_left">
                                                                                    <asp:LinkButton ID="CandidateRecordManagement_technicalInfoLinkButton" runat="server"
                                                                                        ToolTip="Technical Skills" Text="Technical Skills" CssClass="associate_resume_link_button"
                                                                                        OnClientClick="javascript: return OnResumeMenuClick('TI')"> </asp:LinkButton>
                                                                                </div>
                                                                                <div id="CandidateRecordManagement_projectsInfoLinkButtonDiv" class="resume_float_left">
                                                                                    <asp:LinkButton ID="CandidateRecordManagement_projectsInfoLinkButton" runat="server"
                                                                                        ToolTip="Projects" Text="Projects" CssClass="associate_resume_link_button" 
                                                                                        OnClientClick="javascript: return OnResumeMenuClick('PI')"> </asp:LinkButton>
                                                                                </div>
                                                                                <div id="CandidateRecordManagement_educationInfoLinkButtonDiv" class="resume_float_left">
                                                                                    <asp:LinkButton ID="CandidateRecordManagement_educationInfoLinkButton" runat="server"
                                                                                        ToolTip="Education" Text="Education" CssClass="associate_resume_link_button"
                                                                                        OnClientClick="javascript: return OnResumeMenuClick('EI')"> </asp:LinkButton>
                                                                                </div>
                                                                                <div id="CandidateRecordManagement_referencesInfoLinkButtonDiv" class="resume_float_left">
                                                                                    <asp:LinkButton ID="CandidateRecordManagement_referencesInfoLinkButton" runat="server"
                                                                                        ToolTip="References" Text="References" CssClass="associate_resume_link_button"
                                                                                        OnClientClick="javascript: return OnResumeMenuClick('RI')"> </asp:LinkButton>
                                                                                </div>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                    <div id="CandidateRecordManagement_Link_Source">
                                                                        <asp:UpdatePanel ID="CandidateRecordManagement_menuControlsUpdatePanel" runat="server">
                                                                            <ContentTemplate>
                                                                                <div>
                                                                                    <div class="empty_height">
                                                                                        &nbsp;</div>
                                                                                    <div id="CandidateRecordManagement_contactInfoDiv" runat="server" style="display: block;">
                                                                                        <uc1:ContactInformationControl ID="MainResumeControl_contactInfoControl" runat="server" />
                                                                                    </div>
                                                                                    <div id="CandidateRecordManagement_executiveInfoDiv" runat="server" style="display: block">
                                                                                        <uc2:ExecutiveSummaryControl ID="MainResumeControl_executiveSummaryControl" runat="server" />
                                                                                    </div>
                                                                                    <div id="CandidateRecordManagement_technicalInfoDiv" runat="server" style="display: block">
                                                                                        <uc3:TechnicalSkillsControl ID="MainResumeControl_technicalSkillsControl" runat="server" />
                                                                                    </div>
                                                                                    <div id="CandidateRecordManagement_projectsInfoDiv" runat="server" style="overflow: auto;
                                                                                        height: 300px;">
                                                                                        <uc4:ProjectsControl ID="MainResumeControl_projectsControl" runat="server" />
                                                                                    </div>
                                                                                    <div id="CandidateRecordManagement_educationInfoDiv" runat="server" style="display: block">
                                                                                        <uc5:EducationControl ID="MainResumeControl_educationControl" runat="server" />
                                                                                    </div>
                                                                                    <div class="empty_height">
                                                                                        &nbsp;</div>
                                                                                    <div id="CandidateRecordManagement_referencesInfoDiv" runat="server" style="display: block">
                                                                                        <uc6:ReferencesControl ID="MainResumeControl_referencesControl" runat="server" />
                                                                                    </div>
                                                                                    <div class="empty_height">
                                                                                        &nbsp;</div>
                                                                                </div>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                </div>
                                                                <!-- Resume menu ends here -->
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="CandidateRecordManagement_saveUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="CandidateRecordManagement_saveButton" runat="server" SkinID="sknButtonId"
                                        Text="Save" OnClick="CandidateRecordManagement_saveButton_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CandidateRecordManagement_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CandidateRecordManagement_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="CandidateRecordManagement_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <asp:UpdatePanel ID="CandidateRecordManagement_bottomResetUpdatePanel" runat="server">
                            <ContentTemplate>
                                  <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="CandidateRecordManagement_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="CandidateRecordManagement_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CandidateRecordManagement_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
