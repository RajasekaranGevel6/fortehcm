﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCandidateRepository.aspx.cs"
    MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master" Inherits="Forte.HCM.UI.ResumeRepository.SearchCandidateRepository" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmDeleteMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="SearchCandidateRepository_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <script type="text/javascript" language="javascript">

        // JQuery methods.
        $(document).ready(function ()
        {
            // Handler method for 'Simple' link button click.
            $("#<%= SearchCandidateRepository_simpleLinkButton.ClientID %>").click(function ()
            {
                if ($("#<%= SearchCandidateRepository_simpleLinkButton.ClientID %>").text() == 'Simple')
                {
                    // Set link button text & tooltip
                    $("#<%= SearchCandidateRepository_simpleLinkButton.ClientID %>").text('Keyword');
                    $("#<%= SearchCandidateRepository_simpleLinkButton.ClientID %>").attr('title', 'Click here to do a keyword search');

                    // Set search type hidden field.
                    $("#<%= SearchCandidateRepository_searchTypeHiddenField.ClientID %>").val('S');

                    // Hide the keyword search div.
                    $("#<%= SearchCandidateRepository_keywordSearchDiv.ClientID %>").hide();

                    // Show the simple search div.
                    $("#<%= SearchCandidateRepository_simpleSearchDiv.ClientID %>").slideToggle("normal");
                }
                else
                {
                    // Set link button text & tooltip
                    $("#<%= SearchCandidateRepository_simpleLinkButton.ClientID %>").text('Simple');
                    $("#<%= SearchCandidateRepository_simpleLinkButton.ClientID %>").attr('title', 'Click here to do a simple search');

                    // Set search type hidden field.
                    $("#<%= SearchCandidateRepository_searchTypeHiddenField.ClientID %>").val('K');

                    // Hide the simple search div.
                    $("#<%= SearchCandidateRepository_simpleSearchDiv.ClientID %>").hide();

                    // Show the keyword search div.
                    $("#<%= SearchCandidateRepository_keywordSearchDiv.ClientID %>").slideToggle("normal");
                }

                return false;
            });
        });

        function ShowPositionProfileList(ctrlId, ctrlhidId)
        {
            var height = 550;
            var width = 900;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/SearchClientRequest.aspx?ctrlId=" + ctrlId + "&ctrlhidId=" + ctrlhidId + '&parent=test';
            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }

        function ShowkeyWordCorpUserDiv(checkBox, div) {
            var checkBox = document.getElementById(checkBox);
            var divID = document.getElementById(div);
            if (checkBox.checked == true) {
                divID.style.display = 'none';
                //document.getElementById('<%= SearchCandidateRepository_searchButton.ClientID %>').click();
            }
            else {
                divID.style.display = 'block';
            }
        }

        function ShowCreatedByDiv(checkBox, div)
        {
            var checkBox = document.getElementById(checkBox);
            var divID = document.getElementById(div);
            if (checkBox.checked == true)
            {
                divID.style.display = 'none';
                document.getElementById('<%= SearchCandidateRepository_searchButton.ClientID %>').click();
            }
            else
            {
                divID.style.display = 'block';
            }
        }

        function SelectCandidates(ctrl)
        {

            var candidateHiddenField = document.getElementById('<%=SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField.ClientID %>');
            var div = document.getElementById('<%=SearchCandidateRepository_associateCandidateDiv.ClientID %>');

            if (ctrl.checked == true)
            {
                var candidateids = ctrl.value.trim();

                candidateids = candidateids + ",";

                if (candidateHiddenField != null)
                    candidateHiddenField.value += candidateids;

                div.style.display = "block";
            }
            else
            {
                var candidateids1 = ctrl.value.trim();

                var val = candidateHiddenField.value;

                val = val.replace(candidateids1 + ",", "");

                candidateHiddenField.value = val;

                if (val == "")
                    div.style.display = "none";
                else
                    div.style.display = "block";
            }
        }


        function ShowAdvanceSearchDiv(divid)
        {
            var customDiv = document.getElementById('<%= SearchCandidateRepository_advancedSearch_div.ClientID %>');
            //            var custombtn = document.getElementById('<%= SearchCandidateRepository_advancedSearch_div.ClientID %>');

            var customHidden = document.getElementById('<%= SearchCandidateRepository_advancedSearchOption_HiddenField.ClientID %>');

            if (customDiv == null) return;

            if (customDiv.style.display == "block")
            {
                customDiv.style.display = "none";

                document.getElementById('<%= SearchCandidateRepository_positionProfile_idHiddenField.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_positionProfile_NameTextBox.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_resumeUpload_DateFromTextBox.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_resumeUpload_DateToTextBox.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_searchButton.ClientID %>').click();

                //                if (custombtn != null)
                //                    custombtn.text = "Advanced";
                if (customHidden != null)
                    customHidden.value = "A";
            }
            else
            {
                customDiv.style.display = "block";

                //                if (custombtn != null)
                //                    custombtn.text = "Simple";

                if (customHidden != null)
                    customHidden.value = "S";
            }
            return false;
        }

        function ShowAdvanceOption()
        {
            var customDiv = document.getElementById('<%= SearchCandidateRepository_advancedSearch_div.ClientID %>');

            var customHidden = document.getElementById('<%= SearchCandidateRepository_advancedSearchOption_HiddenField.ClientID %>');

            if (customDiv == null) return;

            if (customDiv.style.display == "block")
            {
                customDiv.style.display = "none";

                document.getElementById('<%= SearchCandidateRepository_positionProfile_idHiddenField.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_positionProfile_NameTextBox.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_resumeUpload_DateFromTextBox.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_resumeUpload_DateToTextBox.ClientID %>').value = "";
                document.getElementById('<%= SearchCandidateRepository_searchButton.ClientID %>').click();

                if (customHidden != null)
                    customHidden.value = "A";
            }
            else
            {
                customDiv.style.display = "block";

                if (customHidden != null)
                    customHidden.value = "S";
            }
        }

        function ShowAdvanceSearchDivRuntime()
        {
            var customDiv = document.getElementById("SearchCandidateRepository_advancedSearch_div");
            customDiv.style.display = "block";

            return false;
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="SearchCandidateRepository_headerLiteral" runat="server" Text="Search Candidate"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SearchCandidateRepository_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchCandidateRepository_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SearchCandidateRepository_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SearchCandidateRepository_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SearchCandidateRepository_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="SearchCandidateRepository_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="SearchCandidateRepository_searchDIV" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:LinkButton ID="SearchCandidateRepository_simpleLinkButton" runat="server" Text="Keyword"
                                                SkinID="sknActionLinkButton" ToolTip="Click here to do a keyword search"/>
                                            <asp:HiddenField ID="SearchCandidateRepository_searchTypeHiddenField" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="td_height_5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_inner_body_bg">
                                                        <div id="SearchCandidateRepository_simpleSearchDiv" runat="server" style="display: block; height: 134px">
                                                        <table width="100%" cellpadding="2" cellspacing="3">
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="SearchCandidateRepository_firstnameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="First Name"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:TextBox ID="SearchCandidateRepository_firstnameTextBox" runat="server" Width="70%"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                        <asp:Label ID="SearchCandidateRepository_middleNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Middle Name"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:TextBox ID="SearchCandidateRepository_middleNameTextBox" runat="server" Width="70%"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="SearchCandidateRepository_lastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Last Name"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:TextBox ID="SearchCandidateRepository_lastNameTextBox" runat="server" Width="70%"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                        <asp:Label ID="SearchCandidateRepository_emailLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Email"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:TextBox ID="SearchCandidateRepository_emailTextBox" runat="server" Width="70%"
                                                                            MaxLength="100"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="SearchCandidateRepository_resumeShowLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Show Only My Records"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%" align="left">
                                                                        <asp:CheckBox ID="SearchCandidateRepository_resumeShowCheckBox" runat="server" BorderStyle="None" />
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td colspan="2" style="width: 45%;" align="left">
                                                                        <div id="SearchCandidateRepository_corporateUserDIV" runat="server" style="width: 100%">
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 30%">
                                                                                        <asp:Label ID="SearchCandidateRepository_corporateUserLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Users"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 40%">
                                                                                        <asp:DropDownList ID="SearchCandidateRepository_corporateUserDropDownList" runat="server"
                                                                                            SkinID="sknSubjectDropDown">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="SearchCandidateRepository_resumeUploadTypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Resume Type"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:DropDownList ID="SearchCandidateRepository_resumeUploadType_DropDownList" runat="server"
                                                                            SkinID="sknSubjectDropDown" Width="190px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                        <asp:Label ID="Label1" runat="server" SkinID="sknLabelFieldHeaderText" Text="Status"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList ID="SearchCandidateRepository_candidateStatus_DropDownList" runat="server"
                                                                            SkinID="sknSubjectDropDown" Width="225px">
                                                                            <asp:ListItem Text="All" Value="A"></asp:ListItem>
                                                                            <asp:ListItem Text="Active" Value="Y" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="Inactive" Value="N"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td width="20%">
                                                                                    <asp:Label ID="SearchCandidateRepository_resumeUpload_DateFromLabel" runat="server"
                                                                                        Text="Recent Resume Uploaded From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td width="20%">
                                                                                    <div style="float: left;">
                                                                                        <asp:TextBox ID="SearchCandidateRepository_resumeUpload_DateFromTextBox" runat="server"></asp:TextBox>
                                                                                    </div>
                                                                                    <div style="float: left;">
                                                                                        <asp:ImageButton ID="SearchCandidateRepository_resumeUpload_DateFromImageButton"
                                                                                            SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                                    </div>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="SearchCandidateRepository_resumeUpload_DateFromMaskedEditExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_resumeUpload_DateFromTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="SearchCandidateRepository_resumeUpload_DateFromMaskedEditValidator"
                                                                                        runat="server" ControlExtender="SearchCandidateRepository_resumeUpload_DateFromMaskedEditExtender"
                                                                                        ControlToValidate="SearchCandidateRepository_resumeUpload_DateFromTextBox" EmptyValueMessage="Date is required"
                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="SearchCandidateRepository_resumeUpload_DateFromCustomCalendarExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_resumeUpload_DateFromTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCandidateRepository_resumeUpload_DateFromImageButton" />
                                                                                </td>
                                                                                <td width="15%">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td width="11%" style="padding-left: 5px">
                                                                                    <asp:Label ID="SearchCandidateRepository_resumeUpload_DateToLabel" runat="server"
                                                                                        Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <div style="float: left; padding-right: 2px;">
                                                                                        <asp:TextBox ID="SearchCandidateRepository_resumeUpload_DateToTextBox" runat="server"></asp:TextBox>
                                                                                    </div>
                                                                                    <div style="float: left;">
                                                                                        <asp:ImageButton ID="SearchCandidateRepository_resumeUpload_DateToImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </div>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="SearchCandidateRepository_resumeUpload_DateToMaskedEditExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_resumeUpload_DateToTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="SearchCandidateRepository_resumeUpload_DateToMaskedEditValidator"
                                                                                        runat="server" ControlExtender="SearchCandidateRepository_resumeUpload_DateToMaskedEditExtender"
                                                                                        ControlToValidate="SearchCandidateRepository_resumeUpload_DateToTextBox" EmptyValueMessage="Date is required"
                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="SearchCandidateRepository_resumeUpload_DateToCustomCalendarExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_resumeUpload_DateToTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCandidateRepository_resumeUpload_DateToImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="SearchCandidateRepository_keywordSearchDiv" runat="server" style="display: none; height: 100px">
                                                            <table width="100%" cellpadding="2" cellspacing="3">
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td style="width:100px">
                                                                                    <asp:Label ID="SearchCandidateRepository_keywordLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Keyword"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 400px">
                                                                                    <div style="float: left; width: 85%">
                                                                                        <asp:TextBox ID="SearchCandidateRepository_keywordTextBox" runat="server" Width="100%"
                                                                                            MaxLength="150"></asp:TextBox>
                                                                                    </div>
                                                                                    <div style="float: left; padding-left: 8px">
                                                                                        <asp:ImageButton ID="SearchCandidateRepository_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                            runat="server" OnClientClick="javascript:return false;" ToolTip="Enter the keywords separated by comma, that can search in the candidate name & resume contents"
                                                                                            ImageAlign="Middle" />
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="SearchCandidateRepository_keywordShowMyCandidatesLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Show Only My Candidates"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 20%" align="left">
                                                                                    <asp:CheckBox ID="SearchCandidateRepository_keywordShowMyCandidatesCheckBox" Checked="true" runat="server"/>
                                                                                </td>
                                                                                <td style="width: 10%">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td align="left">
                                                                                    <div id="SearchCandidateRepository_keyWordShowMyCandiatesCorporateUserDIV" runat="server">
                                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td align="left" style="width: 19%">
                                                                                                    <asp:Label ID="SearchCandidateRepository_keywordShowMyCandiatesCorporateUserLabel"
                                                                                                        runat="server" SkinID="sknLabelFieldHeaderText" Text="Users"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 40%">
                                                                                                    <asp:DropDownList ID="SearchCandidateRepository_keywordShowMyCandiatesCorporateUserDropDownList"
                                                                                                        runat="server" SkinID="sknSubjectDropDown">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td width="20%">
                                                                                    <asp:Label ID="SearchCandidateRepository_keyWordResumeUpload_DateFromLabel" runat="server"
                                                                                        Text="Recent Resume Uploaded From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td width="20%">
                                                                                    <div style="float: left;padding-right: 2px;">
                                                                                        <asp:TextBox ID="SearchCandidateRepository_keyWordResumeUpload_DateFromTextBox" runat="server"></asp:TextBox>
                                                                                    </div>
                                                                                    <div style="float: left;">
                                                                                        <asp:ImageButton ID="SearchCandidateRepository_keyWordResumeUpload_DateFromImageButton"
                                                                                            SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                                    </div>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="SearchCandidateRepository_keyWordResumeUpload_DateFromMaskedEditExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_keyWordResumeUpload_DateFromTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="SearchCandidateRepository_keyWordResumeUpload_DateFromMaskedEditValidator"
                                                                                        runat="server" ControlExtender="SearchCandidateRepository_keyWordResumeUpload_DateFromMaskedEditExtender"
                                                                                        ControlToValidate="SearchCandidateRepository_keyWordResumeUpload_DateFromTextBox"
                                                                                        EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                                        TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                        ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="SearchCandidateRepository_keyWordResumeUpload_DateFromCustomCalendarExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_keyWordResumeUpload_DateFromTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCandidateRepository_keyWordResumeUpload_DateFromImageButton" />
                                                                                </td>
                                                                                <td width="10%">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td align="left" width="6%">
                                                                                    <asp:Label ID="SearchCandidateRepository_keyWordResumeUpload_DateToLabel" runat="server"
                                                                                        Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <div style="float: left; padding-right: 2px;">
                                                                                        <asp:TextBox ID="SearchCandidateRepository_keyWordResumeUpload_DateToTextBox" runat="server"></asp:TextBox>
                                                                                    </div>
                                                                                    <div style="float: left;">
                                                                                        <asp:ImageButton ID="SearchCandidateRepository_keyWordResumeUpload_DateToImageButton"
                                                                                            SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                                    </div>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="SearchCandidateRepository_keyWordResumeUpload_DateToMaskedEditExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_keyWordResumeUpload_DateToTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="SearchCandidateRepository_keyWordResumeUpload_DateToMaskedEditValidator"
                                                                                        runat="server" ControlExtender="SearchCandidateRepository_keyWordResumeUpload_DateToMaskedEditExtender"
                                                                                        ControlToValidate="SearchCandidateRepository_keyWordResumeUpload_DateToTextBox"
                                                                                        EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                                        TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                        ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="SearchCandidateRepository_keyWordResumeUpload_DateToCustomCalendarExtender"
                                                                                        runat="server" TargetControlID="SearchCandidateRepository_keyWordResumeUpload_DateToTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCandidateRepository_keyWordResumeUpload_DateToImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:HiddenField ID="SearchCandidateRepository_advancedSearchOption_HiddenField"
                                                            runat="server" />
                                                        <%--<asp:LinkButton ID="SearchCandidateRepository_advancedSearchOption_LinkButton"  
                                                        runat="server" SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowAdvanceSearchDiv();">Advanced</asp:LinkButton> --%>
                                                        <input type="checkbox" runat="server" id="SearchCandidateRepository_advancedSearchOption_Input"
                                                            onclick="javascript:return ShowAdvanceOption();" />
                                                        <asp:Label ID="SearchCandidateRepository_advancedSearchOption_CheckBox" runat="server"
                                                            Text="Associate candidate to position profile" TextAlign="Right" SkinID="sknLabelFieldHeaderText"
                                                            ToolTip="Click here view the position profile" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div id="SearchCandidateRepository_advancedSearch_div" runat="server" style="display: block">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="15%">
                                                                        <asp:Label ID="SearchCandidateRepository_positionProfile_NameTitleLabel" runat="server"
                                                                            Text="Position Profile Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td width="28%">
                                                                        <asp:HiddenField ID="SearchCandidateRepository_positionProfile_idHiddenField" runat="server" />
                                                                        <asp:TextBox ID="SearchCandidateRepository_positionProfile_NameTextBox" SkinID="sknHomePageTextBox"
                                                                            runat="server" Width="270px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                    <td width="1%">
                                                                        <asp:ImageButton ID="SearchCandidateRepository_positionProfileid_ImageButton" runat="server"
                                                                            ToolTip="Click here to select the position profile" SkinID="sknViewPositionProfile" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Button ID="SearchCandidateRepository_searchButton" runat="server" SkinID="sknButtonId"
                                                            Text="Search" OnClick="SearchCandidateRepository_searchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UserRightsMatrix_assignRolesGridViewUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="UserRightsMatrix_assignRolesGridViewDIV" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr id="UserRightsMatrix_assignRolesGridViewheaderTR" runat="server">
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                                <asp:Literal ID="SearchCandidateRepository_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                    ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                            </td>
                                                            <td style="width: 2%" align="right">
                                                                <span id="SearchCandidateRepository_searchResultsUpSpan" runat="server" style="display: none;">
                                                                    <asp:Image ID="SearchCandidateRepository_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                        id="SearchCandidateRepository_searchResultsDownSpan" runat="server" style="display: block;"><asp:Image
                                                                            ID="SearchCandidateRepository_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                                <asp:HiddenField ID="SearchCandidateRepository_restoreHiddenField" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <div id="SearchCandidateRepository_candidateDetailDIV" runat="server" style="height: 225px;
                                                        overflow: auto;">
                                                        <asp:HiddenField ID="SearchCandidateRepository_candidateDetailGridView_candidateIdsHiddenField"
                                                            runat="server" />
                                                        <asp:GridView ID="SearchCandidateRepository_candidateDetailGridView" runat="server"
                                                            AllowSorting="true" AutoGenerateColumns="false" OnRowCreated="SearchCandidateRepository_candidateDetailGridView_RowCreated"
                                                            OnSorting="SearchCandidateRepository_candidateDetailGridView_Sorting" OnRowDataBound="SearchCandidateRepository_candidateDetailGridView_RowDataBound"
                                                            OnRowCommand="SearchCandidateRepository_candidateDetailGridView_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="SearchCandidateRepository_candidateDetailGridView_editCandidateHyperLink"
                                                                            runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_edit.gif"
                                                                            ToolTip="Edit Candidate" Visible='<%# IsAllowEdit(Eval("RecruiterID").ToString())%>'>
                                                                        </asp:HyperLink>
                                                                        <asp:HyperLink ID="SearchCandidateRepository_candidateDetailGridView_editResumeHyperLink"
                                                                            runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_edit_resume.gif"
                                                                            ToolTip="Edit Resume" Visible='<%# IsAllowEdit(Eval("RecruiterID").ToString())%>'>
                                                                        </asp:HyperLink>
                                                                        <asp:ImageButton ID="SearchCandidateRepository_candidateDetailGridView_addNotesImageButton"
                                                                            runat="server" SkinID="sknAddNotesImageButton" ToolTip="Add Notes" />
                                                                        <asp:HyperLink ID="SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityLogHyperLink"
                                                                            runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/view_candidate_activity_log.gif"
                                                                            ToolTip="View Candidate Activity Log">
                                                                        </asp:HyperLink>
                                                                        <asp:HyperLink ID="SearchCandidateRepository_candidateDetailGridView_viewCandidateActivityDashboardHyperLink"
                                                                            runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/mail_icon_to.png"
                                                                            ToolTip="View Candidate Activity Dashboard">
                                                                        </asp:HyperLink>
                                                                        <asp:ImageButton ID="SearchCandidateRepository_candidateDetailGridView_addCandidateIDImageButton"
                                                                            runat="server" CommandName="AddCandidateID" CommandArgument='<%# Eval("caiID") %>'
                                                                            SkinID="sknCreateCandidateUserNameImageButton" Visible='<% # Convert.ToBoolean(GetVisibility(Convert.ToString(Eval("UserName"))))  %>'
                                                                            ToolTip="Create User Name" />
                                                                        <asp:ImageButton ID="SearchCandidateRepository_candidateDetailGridView_deleteResumeImageButton"
                                                                            runat="server" CommandName="DeleteResume" CommandArgument='<%# Eval("caiID") %>'
                                                                            SkinID="sknDeleteResumeImageButton" ToolTip="Delete Resume" Visible="false" />
                                                                        <asp:ImageButton ID="SearchCandidateRepository_candidateDetailGridView_deleteCandidateIDImageButton"
                                                                            runat="server" CommandName="DeleteCandidate" CommandArgument='<%# Eval("caiID") %>'
                                                                            SkinID="sknDeleteImageButton" ToolTip="Delete Candidate" Visible="false" />
                                                                        <input type="checkbox" id="SearchCandidateRepository_candidateDetailGridView_asscociateCandidateCheckBox"
                                                                            runat="server" name="SearchCandidateRepository_candidateDetailGridView_asscociateCandidateInput"
                                                                            value='<%# Eval("caiID") %>' onclick="javascript:return SelectCandidates(this);"
                                                                            visible="false" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name" SortExpression="FirstName" HeaderStyle-Width="19%"
                                                                    ItemStyle-Width="14%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="SearchCandidateRepository_candidateDetailGridView_firstNameLabel"
                                                                            runat="server" Text='<%# Eval("caiFirstName") %>' Width="100px"> 
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Middle Name" SortExpression="MiddleName" ItemStyle-Width="11%"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="11%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="SearchCandidateRepository_candidateDetailGridView_middleNameLabel"
                                                                            runat="server" Text='<%# Eval("caiMiddleName") %>'> 
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Last Name" SortExpression="LastName" ItemStyle-Width="14%"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="SearchCandidateRepository_candidateDetailGridView_lastNameLabel" runat="server"
                                                                            Text='<%# Eval("caiLastName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" SortExpression="Email" ItemStyle-Width="15%"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="SearchCandidateRepository_candidateDetailGridView_emailNameLabel"
                                                                            runat="server" Text='<%# Eval("caiEmail") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Login User Name" SortExpression="USERNAME" ItemStyle-Width="15%"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="SearchCandidateRepository_candidateDetailGridView_userNameLabel" runat="server"
                                                                            Text='<%# Eval("UserName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="SearchCandidateRepository_candidateDetailGridView_tenantIDHiddenField"
                                                                            runat="server" Value='<%# Eval("tenantID") %>' />
                                                                        <asp:HiddenField ID="SearchCandidateRepository_candidateDetailGridView_candidateIDHiddenField"
                                                                            runat="server" Value='<%# Eval("caiID") %>' />
                                                                        <asp:HiddenField ID="SearchCandidateRepository_candidateDetailGridView_candidateResumeIDHiddenField"
                                                                            runat="server" Value='<%# Eval("CandidateResumeID") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Recruited By" SortExpression="RECRUITER" ItemStyle-Width="15%"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="SearchCandidateRepository_candidateDetailGridView_recruiterNameLabel"
                                                                            runat="server" Text='<%# Eval("RecruiterName") %>'> 
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                    <div align="center" style="padding-top: 20px">
                                                        <div id="SearchCandidateRepository_associateCandidateDiv" runat="server" style="display: none">
                                                            <asp:Button ID="SearchCandidateRepository_associateCandidateButton" runat="server"
                                                                SkinID="sknButtonId" Text="Associate Candidate(s)" OnClick="SearchCandidateRepository_associateCandidateButton_Click" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc1:PageNavigator ID="SearchCandidateRepository_pageNavigator" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID ="SearchCandidateRepository_searchButton" />
                                    <asp:AsyncPostBackTrigger ControlID ="SearchCandidateRepository_positionProfileid_ImageButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SearchCandidateRepository_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SearchCandidateRepository_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="SearchCandidateRepository_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SearchCandidateRepository_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchCandidateRepository_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SearchCandidateRepository_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="SearchCandidateRepository_modelPopupUpdatePanel">
                    <ContentTemplate>
                        <asp:Panel ID="SearchCandidateRepository_createUserNamePanel" runat="server" Style="display: block"
                            CssClass="popupcontrol_search_candidate_username_detail">
                            <div style="display: none">
                                <asp:Button ID="SearchCandidateRepository_hiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Label ID="SearchCandidateRepository_createUserHeaderLiteral" runat="server"
                                                        Text="Create User Name"></asp:Label>
                                                </td>
                                                <td style="width: 25%" align="right">
                                                    <asp:ImageButton ID="SearchCandidateRepository_createUserPanelcancelImageButton"
                                                        runat="server" SkinID="sknCloseImageButton" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="2" cellspacing="3">
                                                        <tr>
                                                            <td colspan="2" class="msg_align">
                                                                <asp:Label ID="SearchCandidateRepository_createUserNameSuccessMessageLabel" runat="server"
                                                                    SkinID="sknSuccessMessage"></asp:Label>
                                                                <asp:Label ID="SearchCandidateRepository_createUserNameErrorMessageLabel" runat="server"
                                                                    SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 33%">
                                                                <asp:Label ID="SearchCandidateRepository_createUserNamePanel_userNameLabel" Text="User Name"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 67%">
                                                                <asp:TextBox ID="SearchCandidateRepository_createUserNamePanel_userNameTextBox" runat="server"
                                                                    Width="100%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:LinkButton ID="SearchCandidateRepository_createUserNamePanel_checkUserEmailIdAvailableButton"
                                                                        CssClass="link_button_hcm" runat="server" Text="Check" OnClick="SearchCandidateRepository_createUserNamePanel_checkUserEmailIdAvailableButton_Click"></asp:LinkButton>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <div id="SearchCandidateRepository_createUserNamePanel_userEmailAvailableStatusDiv"
                                                                        runat="server" style="display: none;">
                                                                        <asp:Label ID="SearchCandidateRepository_createUserNamePanel_inValidEmailAvailableStatus"
                                                                            runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>
                                                                        <asp:Label ID="SearchCandidateRepository_createUserNamePanel_validEmailAvailableStatus"
                                                                            runat="server" EnableViewState="false" ForeColor="Green"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchCandidateRepository_createUserNamePanel_passwordLabel" Text="Password"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidateRepository_createUserNamePanel_passwordTextBox" runat="server"
                                                                    Width="100%" MaxLength="50" TextMode="Password"></asp:TextBox>
                                                                <asp:HiddenField ID="SearchCandidateRepository_createUserNamePanel_firstNameHiddenField"
                                                                    runat="server" />
                                                                <asp:HiddenField ID="SearchCandidateRepository_createUserNamePanel_lastNameHiddenField"
                                                                    runat="server" />
                                                                <asp:HiddenField ID="SearchCandidateRepository_createUserNamePanel_candidateIDHiddenField"
                                                                    runat="server" />
                                                                <asp:HiddenField ID="SearchCandidateRepository_createUserNamePanel_candidateResumeIDHiddenField"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchCandidateRepository_createUserNamePanel_retypePasswordLabel"
                                                                    Text="Retype Password" runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidateRepository_createUserNamePanel_retypePasswordTextBox"
                                                                    runat="server" Width="100%" TextMode="Password"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="SearchCandidateRepository_createUserNamePanel_saveButton" runat="server"
                                                        Text="Save" SkinID="sknButtonId" OnClick="SearchCandidateRepository_createUserNamePanel_saveButton_Click" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="SearchCandidateRepository_createUserNamePanel_closeButton" runat="server"
                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchCandidateRepository_createUserModalPopupExtender"
                            runat="server" PopupControlID="SearchCandidateRepository_createUserNamePanel"
                            TargetControlID="SearchCandidateRepository_hiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchCandidateRepository_deleteResumeUpdatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="SearchCandidateRepository_deleteResumePanel" runat="server" Style="display: none;
                            height: 202px" CssClass="search_candidate_respository_resume_delete_confirm">
                            <div style="display: none">
                                <asp:Button ID="SearchCandidateRepository_deleteResume_hiddenButton" runat="server" />
                            </div>
                            <uc2:ConfirmMsgControl ID="SearchCandidateRepository_deleteResume_confirmMessageControl"
                                runat="server" OnOkClick="SearchCandidateRepository_deleteResume_okButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchCandidateRepository_deleteResume_confirmModalPopupExtender"
                            runat="server" PopupControlID="SearchCandidateRepository_deleteResumePanel" TargetControlID="SearchCandidateRepository_deleteResume_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchCandidateRepository_deleteCandidateUpdatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="SearchCandidateRepository_deleteCandidateID_hiddenField" runat="server" />
                        <asp:Panel ID="SearchCandidateRepository_deleteCandidatePanel" runat="server" Style="display: none;
                            height: 202px; width: 250px" CssClass="search_candidate_respository_resume_delete_confirm">
                            <div style="display: none">
                                <asp:Button ID="SearchCandidateRepository_deleteCandidate_hiddenButton" runat="server" />
                            </div>
                            <uc3:ConfirmDeleteMsgControl ID="SearchCandidateRepository_deleteCandidate_confirmMessageControl"
                                runat="server" OnOkClick="SearchCandidateRepository_deleteCandidate_okButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchCandidateRepository_deleteCandidate_confirmModalPopupExtender"
                            runat="server" PopupControlID="SearchCandidateRepository_deleteCandidatePanel"
                            TargetControlID="SearchCandidateRepository_deleteCandidate_hiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
