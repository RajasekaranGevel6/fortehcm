﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.ResumeRepository {
    
    
    public partial class DuplicateResumeRules {
        
        /// <summary>
        /// DuplicateResumeRules_titleLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_titleLabel;
        
        /// <summary>
        /// DuplicateResumeRules_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton DuplicateResumeRules_topResetLinkButton;
        
        /// <summary>
        /// DuplicateResumeRules_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton DuplicateResumeRules_topCancelLinkButton;
        
        /// <summary>
        /// DuplicateResumeRules_topMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel DuplicateResumeRules_topMessageUpdatePanel;
        
        /// <summary>
        /// DuplicateResumeRules_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_topSuccessMessageLabel;
        
        /// <summary>
        /// DuplicateResumeRules_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_topErrorMessageLabel;
        
        /// <summary>
        /// DuplicateResumeRules_searchDIV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl DuplicateResumeRules_searchDIV;
        
        /// <summary>
        /// DuplicateResumeRules_ruleNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_ruleNameLabel;
        
        /// <summary>
        /// DuplicateResumeRules_ruleNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DuplicateResumeRules_ruleNameTextBox;
        
        /// <summary>
        /// DuplicateResumeRules_reasonLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_reasonLabel;
        
        /// <summary>
        /// DuplicateResumeRules_reasonTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DuplicateResumeRules_reasonTextBox;
        
        /// <summary>
        /// DuplicateResumeRules_statusLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_statusLabel;
        
        /// <summary>
        /// DuplicateResumeRules_statusDropDownList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DuplicateResumeRules_statusDropDownList;
        
        /// <summary>
        /// DuplicateResumeRules_selectResumeRulesHelpImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton DuplicateResumeRules_selectResumeRulesHelpImageButton;
        
        /// <summary>
        /// DuplicateResumeRules_searchButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button DuplicateResumeRules_searchButton;
        
        /// <summary>
        /// DuplicateResumeRules_rulesGridView_UpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel DuplicateResumeRules_rulesGridView_UpdatePanel;
        
        /// <summary>
        /// DuplicateResumeRules_searchTR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow DuplicateResumeRules_searchTR;
        
        /// <summary>
        /// DuplicateResumeRules_searchResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DuplicateResumeRules_searchResultsLiteral;
        
        /// <summary>
        /// DuplicateResumeRules_searchResultsUpSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl DuplicateResumeRules_searchResultsUpSpan;
        
        /// <summary>
        /// DuplicateResumeRules_searchResultsUpImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image DuplicateResumeRules_searchResultsUpImage;
        
        /// <summary>
        /// DuplicateResumeRules_searchResultsDownSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl DuplicateResumeRules_searchResultsDownSpan;
        
        /// <summary>
        /// DuplicateResumeRules_searchResultsDownImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image DuplicateResumeRules_searchResultsDownImage;
        
        /// <summary>
        /// DuplicateResumeRules_restoreHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField DuplicateResumeRules_restoreHiddenField;
        
        /// <summary>
        /// DuplicateResumeRules_rulesDetailDIV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl DuplicateResumeRules_rulesDetailDIV;
        
        /// <summary>
        /// DuplicateResumeRules_rulesGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView DuplicateResumeRules_rulesGridView;
        
        /// <summary>
        /// DuplicateResumeRules_addRuleLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton DuplicateResumeRules_addRuleLinkButton;
        
        /// <summary>
        /// DuplicateResumeRules_rulesGridView_pageNavigator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PageNavigator DuplicateResumeRules_rulesGridView_pageNavigator;
        
        /// <summary>
        /// DuplicateResumeRules_deleteRuleHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField DuplicateResumeRules_deleteRuleHiddenField;
        
        /// <summary>
        /// DuplicateResumeRules_deleteRulePopupPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel DuplicateResumeRules_deleteRulePopupPanel;
        
        /// <summary>
        /// DuplicateResumeRules_deleteRuleConfirmMsgControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ConfirmMsgControl DuplicateResumeRules_deleteRuleConfirmMsgControl;
        
        /// <summary>
        /// DuplicateResumeRules_deleteRuleModalPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender DuplicateResumeRules_deleteRuleModalPopupExtender;
        
        /// <summary>
        /// DuplicateResumeRules_bottomMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel DuplicateResumeRules_bottomMessageUpdatePanel;
        
        /// <summary>
        /// DuplicateResumeRules_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_bottomSuccessMessageLabel;
        
        /// <summary>
        /// DuplicateResumeRules_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label DuplicateResumeRules_bottomErrorMessageLabel;
        
        /// <summary>
        /// DuplicateResumeRules_bottomResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton DuplicateResumeRules_bottomResetLinkButton;
        
        /// <summary>
        /// DuplicateResumeRules_bottomCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton DuplicateResumeRules_bottomCancelLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.ResumeRepositoryMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.ResumeRepositoryMaster)(base.Master));
            }
        }
    }
}
