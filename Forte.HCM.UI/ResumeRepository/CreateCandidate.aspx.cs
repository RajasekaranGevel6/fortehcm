﻿#region Directives                                                   

using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ResumeParser.ResumeValidator;
#endregion

namespace Forte.HCM.UI.ResumeRepository
{
    public partial class CreateCandidate : PageBase
    {
        #region Event Handlers                                       
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("New Candidate");

                CreateCandidate_firstNameTextBox.Focus();

                Page.Form.DefaultButton=CreateCandidate_topSaveButton.UniqueID;

                CreateCandidate_workAuthorizationStatusDropDownList.Attributes.Add("onchange",
                   "javascript:ShowOtherTextBox('" + CreateCandidate_workAuthorizationStatusDropDownList.ClientID + "','" +
                   CreateCandidate_workAuthorizationStatus_otherDIV.ClientID +
                   "','" + CreateCandidate_workAuthorizationStatus_otherTextBox.ClientID + "');");

                CreateCandidate_topErrorMessageLabel.Text = string.Empty;
                CreateCandidate_bottomErrorMessageLabel.Text = string.Empty;
                CreateCandidate_topSuccessMessageLabel.Text = string.Empty;
                CreateCandidate_bottomSuccessMessageLabel.Text = string.Empty;


                if (Session["CREATE_SUCCESS_MESSAGE"] != null)
                {
                    // Show a success message.
                    base.ShowMessage(CreateCandidate_topSuccessMessageLabel,
                        CreateCandidate_bottomSuccessMessageLabel,
                        Session["CREATE_SUCCESS_MESSAGE"].ToString());

                    //Clear the message 
                    Session["CREATE_SUCCESS_MESSAGE"] = null;
                }

                if (!IsPostBack)
                {
                    // Clear candidate photo from the session.
                    Session["CANDIDATE_THUMBNAIL_PHOTO"] = null;
                    Session["PHOTO_CHANGED"] = false;
                    ViewState["CANDIDATE_INFORMATION"] = null;

                    // Assign data source to work authorization.
                    CreateCandidate_workAuthorizationStatusDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType("WA_STATUS ", "V");
                    CreateCandidate_workAuthorizationStatusDropDownList.DataTextField = "AttributeName";
                    CreateCandidate_workAuthorizationStatusDropDownList.DataValueField = "AttributeID";
                    CreateCandidate_workAuthorizationStatusDropDownList.DataBind();
                    CreateCandidate_workAuthorizationStatusDropDownList.Items.Insert(0, "--Select--");

                    if (Request.QueryString["candidateID"] != null)
                    {
                        Master.SetPageCaption("Edit Candidate");
                        LoadCandidateDetail(int.Parse(Request.QueryString["candidateID"]));
                        CreateCandidate_headerLiteral.Text = "Edit Candidate";

                        //Get candidate user name if its available
                        CandidateUserName();
                    }
                    else
                    {
                        CreateCandidate_userContainerDiv.Style["display"] = "none";
                    }
                }

                if (ViewState["IS_EDIT_MODE"] != null)
                {
                    Master.SetPageCaption("Edit Candidate");
                    CreateCandidate_headerLiteral.Text = "Edit Candidate";
                }

                // Show the other div based on the authorization status type selected.
                if (CreateCandidate_workAuthorizationStatusDropDownList.SelectedValue != null &&
                    CreateCandidate_workAuthorizationStatusDropDownList.SelectedValue == "WAS_OTHER")
                    CreateCandidate_workAuthorizationStatus_otherDIV.Style.Add("display", "block");
                else
                    CreateCandidate_workAuthorizationStatus_otherDIV.Style.Add("display", "none");

               
                // Set the visibility of the 'clear' photo link.
                CreateCandidate_selectPhotoClearLinkButton.Visible =
                    !(Session["CANDIDATE_THUMBNAIL_PHOTO"] == null);

                CreateCandidate_locationImageButton.Attributes.Add("onclick",
                  "return SearchLocation('" + CreateCandidate_cityIDHiddenField.ClientID + "','" +
                   CreateCandidate_stateIDHiddenField.ClientID + "','" +
                   CreateCandidate_countryIDHiddenField.ClientID + "','" +
                   CreateCandidate_LocationTextBox.ClientID + "')");
            }
            catch (Exception exception)
            {
                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel,
                    exception.Message);

                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Click event of the CreateCandidate_newCandidateButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateCandidate_newCandidateButton_Click(object sender, EventArgs e)
        {
            try
            {
                InsertCandidateDetail();
            }
            catch (Exception exception)
            {
                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel,
                    exception.Message);

                Logger.ExceptionLog(exception);
            }
        }
        
        /// <summary>
        /// Handles the Click event of the CreateCandidate_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateCandidate_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                {
                    base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel, HCMResource.NewCandidate_EnterRequiredFields);

                    return;
                }

                if (!IsValidEmailAddress(CreateCandidate_emailTextBox.Text.Trim()))
                {
                    base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel, HCMResource.NewCandidate_EnterValidEmailID);

                    return;
                }

                CandidateInformation candidateInformation = new CandidateInformation();

                if (ViewState["CANDIDATE_INFORMATION"] != null)
                {
                    candidateInformation = ViewState["CANDIDATE_INFORMATION"] as CandidateInformation;
                }

                candidateInformation.caiFirstName = CreateCandidate_firstNameTextBox.Text.Trim();

                candidateInformation.caiMiddleName = CreateCandidate_middleNameTextBox.Text.Trim();

                candidateInformation.caiLastName = CreateCandidate_lastNameTextBox.Text.Trim();

                candidateInformation.caiEmail = CreateCandidate_emailTextBox.Text.Trim();

                candidateInformation.caiGender = int.Parse(CreateCandidate_genderDropDownList.SelectedValue);

                candidateInformation.LinkedInProfile = CreateCandidate_linkedInProfileTextBox.Text.Trim();

                if (CreateCandidate_dateOfBirthTextBox.Text.Trim().Length != 0)
                {
                    candidateInformation.caiDOB = Convert.ToDateTime(CreateCandidate_dateOfBirthTextBox.Text.Trim());
                }
                else
                {
                    candidateInformation.caiDOB = DateTime.MinValue;
                }

                if (CreateCandidate_workAuthorizationStatusDropDownList.SelectedIndex != 0)
                {
                    candidateInformation.caiWorkAuthorization = CreateCandidate_workAuthorizationStatusDropDownList.SelectedValue;
                    candidateInformation.caiWorkAuthorizationOther = CreateCandidate_workAuthorizationStatus_otherTextBox.Text.Trim();
                }
                candidateInformation.caiAddress = CreateCandidate_addressTextBox.Text;

                candidateInformation.caiHomePhone = CreateCandidate_homephoneTextBox.Text;

                candidateInformation.caiCell = CreateCandidate_cellPhoneTextBox.Text;
                candidateInformation.caiType = "CT_MANUAL";
                candidateInformation.caiEmployee = CreateCandidate_candidateRadioButton.Checked ? 'N' : 'Y';
                candidateInformation.caiLimitedAccess = CreateCandidate_limitedAccessCheckBox.Checked;
                candidateInformation.caiIsActive = CreateCandidate_IsactiveCheckbox.Checked ? 'Y' : 'N';

                if (!Utility.IsNullOrEmpty(CreateCandidate_cityIDHiddenField.Value))
                    candidateInformation.caiCity = Convert.ToInt32(CreateCandidate_cityIDHiddenField.Value);

                if (!Utility.IsNullOrEmpty(CreateCandidate_stateIDHiddenField.Value))
                    candidateInformation.staID = Convert.ToInt32(CreateCandidate_stateIDHiddenField.Value);

                if (!Utility.IsNullOrEmpty(CreateCandidate_countryIDHiddenField.Value))
                    candidateInformation.couID = Convert.ToInt32(CreateCandidate_countryIDHiddenField.Value);

                if (ViewState["IS_EDIT_MODE"] == null)
                {
                    // Get validator instance.
                    IResumeValidator validator = Validator.GetInstance();
                    List<DuplicateCandidateDetail> duplicateCandidates = null;

                    //Check for the duplication record
                    bool isDuplicate = validator.CheckDuplicate(tenantID, CreateCandidate_firstNameTextBox.Text.Trim(),
                        CreateCandidate_lastNameTextBox.Text.ToString().Trim(), CreateCandidate_emailTextBox.Text.Trim(),
                        out duplicateCandidates);
                    //Show popup extender
                    ViewState["CANDIDATE_INFORMATION"] = null;
                    ViewState["CANDIDATE_INFORMATION"] = candidateInformation;

                    if (isDuplicate)
                    {
                        if (duplicateCandidates != null && duplicateCandidates.Count > 0)
                        {
                            CreateCandidate_candidateFirstNameLabelValue.Text = candidateInformation.caiFirstName;
                            CreateCandidate_candidateLastNameLabelValue.Text = candidateInformation.caiLastName;
                            CreateCandidate_candidateEmailLabelValue.Text = candidateInformation.caiEmail;
                            CreateCandidate_candidateDetailGridView.DataSource = duplicateCandidates;
                            CreateCandidate_candidateDetailGridView.DataBind();
                            CreateCandidate_scheduleModalPopupExtender.Show();
                            return;
                        }
                    }
                    InsertCandidateDetail();
                }
                else if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
                {
                    new ResumeRepositoryBLManager().UpdateCandidate(candidateInformation, base.userID);

                    // Save candidate resume
                    SaveCandidateResume(candidateInformation.caiID, candidateInformation.caiFirstName);

                    // Save candidate Photo
                    SaveCandidatePhoto(candidateInformation.caiID);

                    // Show a success message.
                    base.ShowMessage(CreateCandidate_topSuccessMessageLabel,
                        CreateCandidate_bottomSuccessMessageLabel,
                        HCMResource.NewCandidate_CandidateUpdatedSuccessfully);
                }
            }
            catch (Exception exception)
            {
                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel,
                    exception.Message);

                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Click event of the CreateCandidate_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateCandidate_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel,
                    exception.Message);

                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Method that is called when the clear link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will clear the selected photo.
        /// </remarks>
        protected void CreateCandidate_selectPhotoClearLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear candidate photo from the session.
                Session["CANDIDATE_THUMBNAIL_PHOTO"] = null;
                Session["PHOTO_CHANGED"] = true;

                // Set the visibility of the 'clear' photo link.
                CreateCandidate_selectPhotoClearLinkButton.Visible = false;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the candidate grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateCandidate_candidateDetailGridView_RowDataBound
            (object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != System.Web.UI.WebControls.DataControlRowType.DataRow)
                    return;

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler will be called when the browse button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload the file to server.
        /// </remarks>
        protected void CreateCandidate_resumeUploadedComplete(object sender, 
            AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (CreateCandidate_resumeAsyncFileUpload.HasFile)
                {
                    // Validation for file extension
                    if (((!Path.GetExtension(e.FileName).ToLower().Contains(".doc")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".docx")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".pdf")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".rtf"))))
                    {
                        ShowMessage(CreateCandidate_topErrorMessageLabel, CreateCandidate_bottomErrorMessageLabel,
                            "Please select valid file format.(.doc,.docx,.pdf and rtf)");
                        return;
                    }
                    
                    string filePath = ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"].ToString();
                    string saveAsPath=string.Empty;
                    Session["UPLOADED_RESUME_NAME"] = string.Empty;

                    saveAsPath = filePath + "\\" + e.FileName;
                    CreateCandidate_resumeAsyncFileUpload.SaveAs(saveAsPath);
                    Session["UPLOADED_RESUME_NAME"] = e.FileName;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Handler will be called when the browse button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload the file to server.
        /// </remarks>
        protected void CreateCandidate_photoUploadedComplete(object sender,
            AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (CreateCandidate_photoAsyncFileUpload.HasFile)
                {
                    // Validation for file extension
                    if (((!Path.GetExtension(e.FileName).ToLower().Contains(".jpg")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".gif")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".png")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".jpeg"))))
                    {
                        ShowMessage(CreateCandidate_topErrorMessageLabel, CreateCandidate_bottomErrorMessageLabel,
                            "Only gif/png/jpg/jpeg files are allowed)");
                        return;
                    }

                    // Check if photo size exceeds the maximum limit.
                    int maxFileSize = Convert.ToInt32
                        (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_MAX_SIZE_IN_BYTES"]);

                    if (CreateCandidate_photoAsyncFileUpload.FileBytes.Length > maxFileSize)
                    {
                        base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                           CreateCandidate_bottomErrorMessageLabel,
                           string.Format("File size exceeds the maximum limit of {0} bytes", maxFileSize));

                        return;
                    }

                    // Retrieve the original photo from the file upload control.
                    System.Drawing.Image originalPhoto = System.Drawing.Image.FromStream
                        (new MemoryStream(CreateCandidate_photoAsyncFileUpload.FileBytes));

                    int thumbnailWidth = Convert.ToInt32
                        (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_WIDTH"]);
                    int thumbnailHeight = Convert.ToInt32
                        (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_HEIGHT"]);

                    // Check the width and height of the original image exceeds the
                    // size of the standard size. If exceeds convert the image to
                    // thumbnail of the standard size and store.
                    if (originalPhoto.Width > thumbnailWidth && originalPhoto.Height > thumbnailHeight)
                    {
                        // Resize both width and height.
                        System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                            (thumbnailWidth, thumbnailHeight, null, System.IntPtr.Zero);

                        // Keep the thumbnail photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                    }
                    else if (originalPhoto.Width > thumbnailWidth)
                    {
                        // Resize only the width.
                        System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                            (thumbnailWidth, originalPhoto.Height, null, System.IntPtr.Zero);

                        // Keep the thumbnail photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                    }
                    else if (originalPhoto.Height > thumbnailHeight)
                    {
                        // Resize only the height.
                        System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                            (originalPhoto.Width, thumbnailHeight, null, System.IntPtr.Zero);

                        // Keep the thumbnail photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                    }
                    else
                    {
                        // Keep the original photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO"] = originalPhoto;
                    }

                    // Set the phto changed status.
                    Session["PHOTO_CHANGED"] = true;

                    // Set the visibility of the 'clear' photo link.
                    CreateCandidate_selectPhotoClearLinkButton.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel, ex.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when the user name save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CreateCandidate_userNameSaveButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(CreateCandidate_userNameTextBox.Text))
                    return;

                int usrID = 0;
                usrID = Convert.ToInt32(CreateCandidate_userIDHiddenField.Value);

                // Update user name
                new CandidateBLManager().UpdateUserName(usrID, CreateCandidate_userNameTextBox.Text, 
                   base.userID);

                // Redirect to the same page by
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                ShowMessage(CreateCandidate_topErrorMessageLabel,
                    CreateCandidate_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }


        #endregion Event Handlers

        #region Private Methods                                      
        /// <summary>
        /// Method to load the selected candidate details
        /// </summary>
        /// <param name="candidateID">candidateID</param>
        private void LoadCandidateDetail(int candidateID)
        {
            CandidateInformation candidateInformation =
                new ResumeRepositoryBLManager().GetCandidateInformation(candidateID);

            CreateCandidate_firstNameTextBox.Text = candidateInformation.caiFirstName;

            CreateCandidate_middleNameTextBox.Text = candidateInformation.caiMiddleName;

            CreateCandidate_lastNameTextBox.Text = candidateInformation.caiLastName;

            CreateCandidate_emailTextBox.Text = candidateInformation.caiEmail;

            CreateCandidate_genderDropDownList.SelectedIndex =
            CreateCandidate_genderDropDownList.Items.IndexOf(
                CreateCandidate_genderDropDownList.Items.FindByValue(candidateInformation.caiGender.ToString()));

            if (candidateInformation.caiDOB != null)
            {
                CreateCandidate_dateOfBirthTextBox.Text = candidateInformation.caiDOB.Value.ToShortDateString();
            }

            CreateCandidate_addressTextBox.Text = candidateInformation.caiAddress;

            CreateCandidate_homephoneTextBox.Text = candidateInformation.caiHomePhone;

            CreateCandidate_cellPhoneTextBox.Text = candidateInformation.caiCell;

            CreateCandidate_cityIDHiddenField.Value = candidateInformation.caiCity.ToString();
            CreateCandidate_stateIDHiddenField.Value = candidateInformation.staID.ToString();
            CreateCandidate_countryIDHiddenField.Value = candidateInformation.couID.ToString();

            // Work authorization
            if (candidateInformation.caiWorkAuthorization != null)
            {
                CreateCandidate_workAuthorizationStatusDropDownList.SelectedIndex =
                    CreateCandidate_workAuthorizationStatusDropDownList.Items.IndexOf(
                    CreateCandidate_workAuthorizationStatusDropDownList.Items.FindByValue
                    (candidateInformation.caiWorkAuthorization.ToString()));
            }

            CreateCandidate_workAuthorizationStatus_otherTextBox.Text = candidateInformation.caiWorkAuthorizationOther;

            if (ViewState["CANDIDATE_INFORMATION"] == null)
            {
                ViewState["CANDIDATE_INFORMATION"] = candidateInformation;
            }

            CreateCandidate_limitedAccessCheckBox.Checked = candidateInformation.caiLimitedAccess;

            if (candidateInformation.caiIsActive == 'N')
                CreateCandidate_IsactiveCheckbox.Checked = false;
            else
                CreateCandidate_IsactiveCheckbox.Checked = true;

            CreateCandidate_LocationTextBox.Text = candidateInformation.caiLocation;
            CreateCandidate_linkedInProfileTextBox.Text = candidateInformation.LinkedInProfile;

            ViewState["IS_EDIT_MODE"] = true;

            // Compose the candidate photo path.
            string filePath = Server.MapPath("~/") +
                ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                "\\" + candidateID + "." +
                ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

            // If file exists, then retrieve the bytes from the image.
            if (File.Exists(filePath))
            {
                using (FileStream fileStream = new FileStream
                    (filePath, FileMode.Open, FileAccess.Read))
                {
                    Image thumbnailPhoto = Image.FromStream(fileStream);
                    Session["CANDIDATE_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                    fileStream.Close();
                }

                // Set the visibility of the 'clear' photo link.
                CreateCandidate_selectPhotoClearLinkButton.Visible = true;
            }
        }

        /// <summary>
        /// Method to validate the email address
        /// </summary>
        /// <param name="strUserEmailId"></param>
        /// <returns></returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Method to insert the candidate details
        /// </summary>
        private void InsertCandidateDetail()
        {
            CandidateInformation candidateInformation = null;
            if (ViewState["CANDIDATE_INFORMATION"] != null)
            {
                candidateInformation = ViewState["CANDIDATE_INFORMATION"] as CandidateInformation;
                int candidateID = new ResumeRepositoryBLManager().InsertNewCandidate(candidateInformation,
                    base.userID, base.tenantID);
                candidateInformation.caiID = candidateID;

                // Save candidate resume
                SaveCandidateResume(candidateInformation.caiID, candidateInformation.caiFirstName);

                // Save candidate Photo
                SaveCandidatePhoto(candidateInformation.caiID);

                Session["CREATE_SUCCESS_MESSAGE"] = HCMResource.NewCandidate_CandidateInsertedSuccessfully;

                // Reset the page inputs
                Response.Redirect(Request.RawUrl, false);
            }
        }

        /// <summary>
        /// Method to save the candidate resume details
        /// </summary>
        /// <param name="candidateInfoID">
        /// A <see cref="int"/> that holds the candidate info ID.
        /// </param>
        /// <param name="candidateName">
        /// A <see cref="string"/> that holds the candidate name.
        /// </param>
        private void SaveCandidateResume(int candidateInfoID, string candidateName)
        {
            string serverPath = ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"].ToString();
            string newFilePath = string.Empty;
            string oldFilePath = string.Empty;
            string getExtension = string.Empty;

            if (!Utility.IsNullOrEmpty(Session["UPLOADED_RESUME_NAME"]))
            {
                // Insert parser candidate information and retreive the ID.
                int parserCandidateID = new ResumeRepositoryBLManager().InsertParserCandidateInformation
                    (candidateInfoID, "RUT_CRU", Session["UPLOADED_RESUME_NAME"].ToString(), base.userID);

                //To get the file extension
                getExtension = Path.GetExtension(Session["UPLOADED_RESUME_NAME"].ToString()).ToLower();

                //Old file path
                oldFilePath = serverPath + "\\" + Session["UPLOADED_RESUME_NAME"].ToString();

                //To save word file as 
                newFilePath = serverPath + "\\" + parserCandidateID + "_" + candidateName + getExtension;

                //Check if file exists already then delete it
                if (File.Exists(newFilePath) == true)
                    File.Delete(newFilePath);
                //File copy now
                File.Copy(oldFilePath, newFilePath);

                //Delete the temp file
                File.Delete(oldFilePath);
                Session["UPLOADED_RESUME_NAME"] = string.Empty;
            }
        }

        /// <summary>
        /// Method to save the candidate resume 
        /// </summary>
        /// <param name="candidateID"></param>
        private void SaveCandidatePhoto(int candidateID)
        {
            // Save candidate photo.
            if (!Utility.IsNullOrEmpty(Session["PHOTO_CHANGED"]) &&
                Convert.ToBoolean(Session["PHOTO_CHANGED"]) == true)
            {
                Image thumbnail = null;
                if (Session["CANDIDATE_THUMBNAIL_PHOTO"] != null)
                {
                    thumbnail = Session["CANDIDATE_THUMBNAIL_PHOTO"] as Image;
                }

                // Save the thumbnail image into server path.
                string filePath = Server.MapPath("~/") +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                    "\\" + candidateID + "." +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

                // Check if the file exist. If exist, then remove it.
                if (File.Exists(filePath))
                    File.Delete(filePath);

                thumbnail.Save(filePath, base.GetCandidatePhotoFormat());

                // Reset the status.
                Session["PHOTO_CHANGED"] = false;
            }
        }

        /// <summary>
        /// Method to get candidate user name
        /// </summary>
        private void CandidateUserName()
        {
            int candidateID=0;
            if (!Utility.IsNullOrEmpty(Request.QueryString["candidateID"]))
                candidateID = Convert.ToInt32(Request.QueryString["candidateID"]);

            UserDetail userDetail = null;

            userDetail = new CandidateBLManager().GetUserNameByCandidateID(candidateID);
            if (Utility.IsNullOrEmpty(userDetail))
            {
                CreateCandidate_userContainerDiv.Style["display"] = "none";
                return;                
            }
            
            CreateCandidate_userNameTextBox.Text = userDetail.UserName.Trim();
            CreateCandidate_userNameValueLabel.Text = userDetail.UserName.Trim();
            CreateCandidate_userIDHiddenField.Value = userDetail.UserID.ToString();
        }

        #endregion Private Methods

        #region Protected Override Methods                           
        protected override bool IsValidData()
        {
            bool value = true;

            if (CreateCandidate_firstNameTextBox.Text.Trim().Length == 0)
            {
                value = false;
            }
            if (CreateCandidate_lastNameTextBox.Text.Trim().Length == 0)
            {
                value = false;
            }
            if (CreateCandidate_emailTextBox.Text.Trim().Length == 0)
            {
                value = false;
            }
            if (CreateCandidate_emailTextBox.Text.Trim().Length == 0)
            {
                value = false;
            }
            if (CreateCandidate_workAuthorizationStatusDropDownList.SelectedValue != null &&
                    CreateCandidate_workAuthorizationStatusDropDownList.SelectedValue == "WAS_OTHER")
            {
                if (CreateCandidate_workAuthorizationStatus_otherTextBox.Text.Trim().Length == 0)
                    value = false;
            }

            return value;
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Override Methods
    }
}