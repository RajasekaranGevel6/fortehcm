﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ResumeUploader.cs
// File that represents the user interface layout and functionalities
// for the resume uploader page. This page allows us to upload file 
// in the server. This page also provides to preview the uploaded file.
// This class inherits the Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;


using Forte.HCM.ResumeParser.Parser;
#endregion

namespace Forte.HCM.UI.ResumeRepository
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the resume uploader page. This page allows us to upload file 
    /// in the server. This page also provides to preview the uploaded file.
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ResumeUploader : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(Resources.HCMResource.ResumeUploader_Title);

                //Assign default button
                Page.Form.DefaultButton = ResumeUploader_topUploadButton.UniqueID;

                //Assign focus 
                ResumeUploader_candidateIDTextBox.Focus();

                // Clear error/success messages.
                ClearAllLabelMessage();

                // If file present in the file upload control, then store it in the session.
                if (ResumeUploader_resumeAsyncFileUpload.FileName != null &&
                    ResumeUploader_resumeAsyncFileUpload.FileName.Trim().Length > 0)
                {
                    Session["UPLOADED_FILE_NAME"] = ResumeUploader_resumeAsyncFileUpload.FileName;
                }

                if (!IsPostBack)
                {
                    //ResumeUploader_topUploadButton.Visible = true;
                    Session["PATH_NAME"] = string.Empty;
                    Session["UPLOADED_FILE_NAME"] = string.Empty;
                    Session["SELECTED_FILE_NAME"] = string.Empty;

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                     (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                    }


                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, load the details for the
                    // selected candidate.
                    if (Session[Constants.SearchCriteriaSessionKey.RESUME_UPLOADER_SELECTED_CANDIDATE] != null)
                        LoadCandidateDetail();
                }

                // Assign handler to candidate selection popup.
                ResumeUploader_searchImageButton.Attributes.Add("onclick",
                    "return LoadCandidate('" + ResumeUploader_candidateIDHiddenField.ClientID + "','" +
                  ResumeUploader_candidateNameHiddenField.ClientID + "','" +
                  ResumeUploader_candidateIDTextBox.ClientID + "','" +
                  ResumeUploader_candidateFirstNameHiddenField.ClientID + "')");

                if (IsPostBack)
                {
                    //ResumeUploader_bottomPaneDIV.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeUploader_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that loads the candidate detail for the passed candidate ID.
        /// </summary>
        private void LoadCandidateDetail()
        {
            int candidateID = 0;

            if (int.TryParse(Session[Constants.SearchCriteriaSessionKey.RESUME_UPLOADER_SELECTED_CANDIDATE].ToString(), out candidateID) == false)
                return;

            // Get candidate detail.
            CandidateDetail candidateDetail = new CandidateBLManager().GetCandidateInformation(candidateID);

            if (candidateDetail == null)
            {
                ShowMessage(ResumeUploader_topErrorMessageLabel, "No such candidate exists");
                return;
            }

            // Assign details to the field.
            ResumeUploader_candidateIDHiddenField.Value = candidateID.ToString();
            ResumeUploader_candidateIDTextBox.Text = candidateDetail.FirstName;

            // Load resume versions.
            LoadResumeVersions();
        }

        /// <summary>
        /// Handler method that will be called when the upload button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload file in the server.
        /// </remarks>
        protected void ResumeUploader_topUploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(ResumeUploader_candidateIDHiddenField.Value))
                {
                    // Show error messages to the user
                    ShowMessage(ResumeUploader_topErrorMessageLabel,
                        Resources.HCMResource.ResumeUploader_SelectCandidate);
                    if (IsPostBack)
                    {
                        ResumeUploader_candidateIDTextBox.Text =
                            ResumeUploader_candidateFirstNameHiddenField.Value;
                    }
                    return;
                }
                else if (((ResumeUploader_resumeAsyncFileUpload.Attributes["filename"] == null) || (ResumeUploader_resumeAsyncFileUpload.Attributes["filename"].ToString() == string.Empty))
                    && (!ResumeUploader_resumeAsyncFileUpload.HasFile) && (Session["PATH_NAME"].ToString() == string.Empty))
                {
                    ResumeUploader_candidateIDTextBox.Text =
                        ResumeUploader_candidateFirstNameHiddenField.Value;
                    ShowMessage(ResumeUploader_topErrorMessageLabel,
                        Resources.HCMResource.ResumeUploader_SelectFileToUpload);
                    return;
                }

                // Parse resume.
                ParseResume();

                //if (!Utility.IsNullOrEmpty(ResumeUploader_isResumePresentHiddenField.Value) && ResumeUploader_isResumePresentHiddenField.Value == "Y")
                //{
                   // UploadResume(;
                //}
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeUploader_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the review & edit resume button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will load the HRXML details in the edit window.
        /// </remarks>
        protected void ResumeUploader_resumeParserReviewEditResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ResumeRepository/ResumeEditor.aspx?m=1&s=2&candidateid=" + 
                    ResumeUploader_candidateIDHiddenField.Value + "&parentpage=UPLOAD_RESUME", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeUploader_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the upload another resume button  
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reload the page again to upload another resume.
        /// </remarks>
        protected void ResumeUploader_resumeParserUploadAnotherResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ResumeRepository/ResumeUploader.aspx?m=1&s=1&parentpage=MENU", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ResumeUploader_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler will be called when the browse button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload the file to server.
        /// </remarks>
        protected void ResumeUploader_resumeUploadedComplete(object sender,
            AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (ResumeUploader_resumeAsyncFileUpload.HasFile)
                {
                    //ResumeUploader_topUploadButton.Visible = true;
                    ResumeUploader_resumeUploadedHiddenField.Value = "Y";
                   // ResumeUploader_topUploadButton.Attributes.Add("Onclick", "javascript:return ShowResumeParserMessagePopup(" + 
                    //    ResumeUploader_candidateIDHiddenField.Value + ",'" + ResumeUploader_resumeUploadedHiddenField.Value + "')");
                }
                /*
                if (ResumeUploader_resumeAsyncFileUpload.HasFile)
                {
                    
                    // Validation for file extension
                    if (((!Path.GetExtension(e.FileName).ToLower().Contains(".doc")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".docx")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".pdf")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".rtf"))))
                    {
                        ShowMessage(ResumeUploader_topErrorMessageLabel,
                            "Please select valid file format.(.doc,.docx,.pdf and rtf)");
                        return;
                    }

                    string filePath = ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"].ToString();
                    string saveAsPath = string.Empty;
                    Session["UPLOADED_RESUME_NAME"] = string.Empty;

                    saveAsPath = filePath + "\\" + e.FileName;
                    CreateCandidate_resumeAsyncFileUpload.SaveAs(saveAsPath);
                    Session["UPLOADED_RESUME_NAME"] = e.FileName;
                    
                    //ResumeUploader_bottomPaneDIV.Style.Value = "none";
                    //ResumeUploader_bottomPaneDIV.Visible = false;
                    if (Utility.IsNullOrEmpty(ResumeUploader_candidateIDHiddenField.Value))
                    {
                        // Show error messages to the user
                        ShowMessage(ResumeUploader_topErrorMessageLabel,
                            Resources.HCMResource.ResumeUploader_SelectCandidate);
                        return;
                    }
                    // Clear the Session path name from session state
                    Session["PATH_NAME"] = string.Empty;
                    if (ResumeUploader_resumeAsyncFileUpload.HasFile)
                    {
                        //Save file in the server and get the file name
                        string fileName = SaveFileAs();
                        //If file name is empty return
                        if (fileName == string.Empty)
                            return;
                        Session["PATH_NAME"] = fileName;
                        //Get the Preview
                        PreviewResume(fileName);

                        //ResumeUploader_bottomPaneDIV.Style.Value = "block";
                        //ResumeUploader_bottomPaneDIV.Visible = true;
                    }
                    else
                    {
                        ResumeUploader_candidateIDTextBox.Text = ResumeUploader_candidateFirstNameHiddenField.Value;
                        ShowMessage(ResumeUploader_topErrorMessageLabel,
                            Resources.HCMResource.ResumeUploader_SelectFileToUpload);
                        return;
                    }
                }*/
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(ResumeUploader_topErrorMessageLabel,
                    ex.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the load button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to load the resume information of selected candidate.
        /// </remarks>
        protected void ResumeUploader_loadDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Load resume versions.
                LoadResumeVersions();

                // Enable resume upload control.
                ResumeUploader_resumeAsyncFileUpload.Enabled = true;
            }
            catch (Exception exp)
            {
                ShowMessage(ResumeUploader_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ResumeUploader_resumeVersionsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Assign events to 'view' link.
                LinkButton ResumeUploader_editLinkButton = e.Row.FindControl
                    ("ResumeUploader_editLinkButton") as LinkButton;

                ResumeUploader_editLinkButton.PostBackUrl = "~/ResumeRepository/ResumeEditor.aspx?m=1&s=2&candidateid=" +
                    ResumeUploader_candidateIDHiddenField.Value + "&rowindex=" + e.Row.RowIndex +  "&parentpage=" + Constants.ParentPage.UPLOAD_RESUME;
            }
            catch (Exception exp)
            {
                ShowMessage(ResumeUploader_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the  
        /// reset button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the resume uploader page.
        /// </remarks>
        protected void ResumeUploader_topResetButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method that will be called when the preview linkbutton is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to preview the file.
        /// </remarks>
        protected void ResumeUploader_previewLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAllLabelMessage();
                ResumeUploader_bottomPaneDIV.Style.Value = "none";
                ResumeUploader_bottomPaneDIV.Visible = false;
                if (Utility.IsNullOrEmpty(ResumeUploader_candidateIDHiddenField.Value))
                {
                    // Show error messages to the user
                    ShowMessage(ResumeUploader_topErrorMessageLabel,
                        Resources.HCMResource.ResumeUploader_SelectCandidate);
                    return;
                }
                // Clear the Session path name from session state
                Session["PATH_NAME"] = string.Empty;
                if (ResumeUploader_resumeAsyncFileUpload.HasFile)
                {
                    //Save file in the server and get the file name
                    string fileName = SaveFileAs();
                    //If file name is empty return
                    if (fileName == string.Empty)
                        return;
                    Session["PATH_NAME"] = fileName;
                    //Get the Preview
                    PreviewResume(fileName);

                    ResumeUploader_bottomPaneDIV.Style.Value = "block";
                    ResumeUploader_bottomPaneDIV.Visible = true;
                }
                else
                {
                    ResumeUploader_candidateIDTextBox.Text = ResumeUploader_candidateFirstNameHiddenField.Value;
                    ShowMessage(ResumeUploader_topErrorMessageLabel,
                        Resources.HCMResource.ResumeUploader_SelectFileToUpload);
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user
                ShowMessage(ResumeUploader_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the offline interview setup
        /// download link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the cyber proctoring troubleshooting document.
        /// </remarks>
        protected void ResumeUploader_downloadResumeUploaderUtilityLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Common/Download.aspx?type=RU", false);
                Response.End();
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(ResumeUploader_topErrorMessageLabel,
                   exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that uploads the resume.
        /// </summary>
        private void UploadResume()
        {
            ClearAllLabelMessage();

            bool uploaded = false;
            string resumeName = string.Empty;

            // To check the file is present or not
            if (ResumeUploader_resumeAsyncFileUpload.HasFile)
            {
                //Save file in the server and get the file name
                string fileName = SaveUploadedFileAs();

                //If file name is empty return
                if (fileName == string.Empty)
                    return;
                ResumeUploader_candidateIDTextBox.Text =
                    ResumeUploader_candidateFirstNameHiddenField.Value;
                ResumeUploader_topUploadButton.Visible = false;
                ResumeUploader_previewLinkButton.Visible = false;
                ResumeUploader_resumeAsyncFileUpload.Enabled = false;
                ResumeUploader_searchImageButton.Visible = false;
                ShowMessage(ResumeUploader_topSuccessMessageLabel,
                     Resources.HCMResource.ResumeUploader_Success);

                uploaded = true;

                // Reset the parsing completed status.
               ResumeUploader_parsingCompletedHiddenField.Value = "N";

            }
            else if (Session["PATH_NAME"].ToString() != "")
            {
                string strFilePath = ConfigurationManager.AppSettings
                    ["UPLOADED_RESUMES_PATH"].ToString();

                //To create a directory in the server
                //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);
                int candidateInfoID = Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value);

                if (!Utility.IsNullOrEmpty(Session["SELECTED_FILE_NAME"]))
                    resumeName = Session["SELECTED_FILE_NAME"].ToString();
                else
                    resumeName = Path.GetFileName(Session["PATH_NAME"].ToString());

                // Insert parser candidate information and retreive the ID.
                int parserCandidateID = new ResumeRepositoryBLManager().InsertParserCandidateInformation
                    (candidateInfoID, "RUT_CRU", resumeName, base.userID);

                if (parserCandidateID != 0)
                {
                    string saveAsPath = string.Empty;

                    if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".doc")
                    {
                        //To save word file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".doc";
                    }

                    if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".pdf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".pdf";
                    }
                    if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".docx")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".docx";
                    }
                    if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".rtf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".rtf";
                    }

                    File.Copy(Session["PATH_NAME"].ToString(), saveAsPath, true);
                    ResumeUploader_previewLinkButton.Visible = false;
                    ResumeUploader_topUploadButton.Visible = false;
                    ResumeUploader_searchImageButton.Visible = false;
                    ResumeUploader_resumeAsyncFileUpload.Enabled = false;
                    ShowMessage(ResumeUploader_topSuccessMessageLabel,
                        Resources.HCMResource.ResumeUploader_Success);

                    uploaded = true;
                }
            }
            else if (ResumeUploader_resumeAsyncFileUpload.Attributes["filename"] != "")
            {
                string strFilePath = ConfigurationManager.AppSettings
                    ["UPLOADED_RESUMES_PATH"].ToString();

                //To create a directory in the server
                //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);
                int candidateInfoID = Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value);

                if (!Utility.IsNullOrEmpty(Session["SELECTED_FILE_NAME"]))
                    resumeName = Session["SELECTED_FILE_NAME"].ToString();
                else
                    resumeName = Path.GetFileName(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"]);

                // Insert parser candidate information and retreive the ID.
                int parserCandidateID = new ResumeRepositoryBLManager().InsertParserCandidateInformation
                    (candidateInfoID, "RUT_CRU", resumeName, base.userID);

                if (parserCandidateID != 0)
                {
                    string saveAsPath = string.Empty;

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"]).ToLower() == ".doc")
                    {
                        //To save word file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateLastNameHiddenField.Value + ".doc";
                    }

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"]).ToLower() == ".pdf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".pdf";
                    }

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"]).ToLower() == ".docx")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".docx";
                    }

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"]).ToLower() == ".rtf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" + 
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".rtf";
                    }

                    File.Copy(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"], saveAsPath, true);
                    ResumeUploader_previewLinkButton.Visible = false;
                    ResumeUploader_topUploadButton.Visible = false;
                    ResumeUploader_searchImageButton.Visible = false;
                    ResumeUploader_resumeAsyncFileUpload.Enabled = false;
                    ShowMessage(ResumeUploader_topSuccessMessageLabel,
                        Resources.HCMResource.ResumeUploader_Success);

                    uploaded = true;
                }
            }
            else
            {
                ResumeUploader_candidateIDTextBox.Text =
                    ResumeUploader_candidateFirstNameHiddenField.Value;
                ShowMessage(ResumeUploader_topErrorMessageLabel,
                    Resources.HCMResource.ResumeUploader_SelectFileToUpload);
                return;
            }

            if (uploaded)
            {
                // Insert the log entry.
                int activityLogID = new ResumeRepositoryBLManager().InsertUploadResumeLog(Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value),
                    ResumeUploader_candidateFirstNameHiddenField.Value, resumeName, base.userID, ResumeUploader_candidateFirstNameHiddenField.Value);
            }
            if (IsPostBack)
            {
                ResumeUploader_candidateIDTextBox.Text = ResumeUploader_candidateFirstNameHiddenField.Value;
            }
        }

        /// <summary>
        /// Method that parse the uploaded resume and navigates to the edit resume page.
        /// </summary>
        private void ParseResume()
        {
            try
            {
                string fileName;

                // Retrieve user detail from session.
                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                // Insert parser candidate information and retreive the ID.
                int parserCandidateID = new ResumeRepositoryBLManager().InsertParserCandidateInformation
                    (Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value), "RUT_CRU", ResumeUploader_resumeAsyncFileUpload.FileName, base.userID);

                // Upload the resume.
                bool uploadStatus = UploadResume(out fileName, parserCandidateID);

                if (uploadStatus == false)
                {
                    ResumeUploader_resumeParserRotatingImageDiv.Visible = false;
                    ResumeUploader_resumeParserMessageTitleLabel.Text = "Error Occurred";
                    ResumeUploader_resumeParserMessageLabel.Text = "There was an error in uploading the resume. Please try later";
                    ResumeUploader_resumeParserCloseButton.Visible = true;
                    ResumeUploader_resumeParserMessageModalPopupExtender.Show();
                    ResumeUploader_parsingCompletedHiddenField.Value = "Y";
                    return;
                }

                ConverterSettings converterSettings = new ConverterSettings();
                converterSettings.ConvertedResumesPath = ConfigurationManager.AppSettings["RECRUITER_CONVERSION_SUCCEEDED_FOLDER"];
                converterSettings.ConversionTemporaryPath = ConfigurationManager.AppSettings["RECRUITER_CONVERSION_TEMP_FOLDER"];
                converterSettings.ConversionFailedPath = ConfigurationManager.AppSettings["RECRUITER_CONVERSION_FAILED_FOLDER"];
                converterSettings.ReadyToParsePath = ConfigurationManager.AppSettings["RECRUITER_READY_TO_PARSE_FOLDER"];
                converterSettings.UserID = base.userID;

                HCM.ResumeParser.Parser.ParserSettings parserSettings = new HCM.ResumeParser.Parser.ParserSettings();
                parserSettings.ProcessPath = ConfigurationManager.AppSettings["RECRUITER_PARSING_ON_PROCESS_FOLDER"];
                parserSettings.SucceededPath = ConfigurationManager.AppSettings["RECRUITER_PARSING_SUCCEEDED_FOLDER"];
                parserSettings.FailedPath = ConfigurationManager.AppSettings["RECRUITER_PARSING_FAILED_FOLDER"];
                parserSettings.ProjectFailedPath = ConfigurationManager.AppSettings["RECRUITER_PROJECT_FAILED_FOLDER"];
                parserSettings.HRXMLPath = ConfigurationManager.AppSettings["RECRUITER_HRXML_FOLDER"];
                parserSettings.UserID = base.userID;

                // Get Parser instance.
                IParser parser = ParserFactory.GetInstance(converterSettings, parserSettings);

                // Convert resume
                bool convertResult = parser.ConvertDocument(fileName, parserCandidateID);

                if (convertResult == false)
                {
                    ResumeUploader_resumeParserRotatingImageDiv.Visible = false;
                    ResumeUploader_resumeParserMessageTitleLabel.Text = "Error Occurred";
                    ResumeUploader_resumeParserMessageLabel.Text = "There was an error in uploading the resume. Please try later";
                    ResumeUploader_resumeParserCloseButton.Visible = true;
                    ResumeUploader_resumeParserMessageModalPopupExtender.Show();
                    ResumeUploader_parsingCompletedHiddenField.Value = "Y";
                    return;
                }

                // Parse resume.
                string parseFile = ConfigurationManager.AppSettings["RECRUITER_READY_TO_PARSE_FOLDER"] + "\\"
                    + parserCandidateID + "_" + ResumeUploader_candidateIDTextBox.Text + ".txt";

                bool parserResult = parser.Parse(parseFile, parserCandidateID);

                if (parserResult == false)
                {
                    ResumeUploader_resumeParserRotatingImageDiv.Visible = false;
                    ResumeUploader_resumeParserMessageTitleLabel.Text = "Error Occurred";
                    ResumeUploader_resumeParserMessageLabel.Text = "There was an error in parsing the resume. Please try later";
                    ResumeUploader_resumeParserCloseButton.Visible = true;
                    ResumeUploader_parsingCompletedHiddenField.Value = "Y";
                    ResumeUploader_resumeParserMessageModalPopupExtender.Show();
                    return;
                }

                ResumeUploader_resumeParserRotatingImageDiv.Visible = false;
                ResumeUploader_resumeParserMessageTitleLabel.Text = "Parsing Succeeded";
                ResumeUploader_resumeParserMessageLabel.Text = "The resume has been parsed successfully. You can now review and edit it";
                ResumeUploader_resumeParserCloseButton.Visible = false;
                ResumeUploader_resumeParserReviewEditResumeButton.Visible = true;
                ResumeUploader_resumeParserUploadAnotherResumeButton.Visible = true;
                ResumeUploader_parsingCompletedHiddenField.Value = "Y";
                ResumeUploader_resumeParserMessageModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ResumeUploader_parsingCompletedHiddenField.Value = "Y";
                ResumeUploader_resumeParserRotatingImageDiv.Visible = false;
                ResumeUploader_resumeParserMessageTitleLabel.Text = "Error Occurred";
                ResumeUploader_resumeParserMessageLabel.Text = "There was an error in parsing the resume. Please try later";
                ResumeUploader_resumeParserCloseButton.Visible = true;
                ResumeUploader_resumeParserMessageModalPopupExtender.Show();
            }
        }

        /// <summary>
        /// Method that uploads the resume.
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the file name as an output parameter.
        /// </param>
        /// <param name="parserCandidateID">
        /// A <see cref="int"/> that holds the parser candidate ID. This ID will
        /// be prefixed with the file name.
        /// </param>
        private bool UploadResume(out string fileName, int parserCandidateID)
        {
            // Assign default values to output parameters.
            fileName = string.Empty;

            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

            bool uploaded = false;

            if (!ResumeUploader_resumeAsyncFileUpload.HasFile)
                return false;

            // To check the file is present or not
            if (ResumeUploader_resumeAsyncFileUpload.HasFile)
            {
                // Save file in the server and get the file name
                fileName = SaveUploadedFileAs(ConfigurationManager.AppSettings
                    ["RECRUITER_UPLOADED_RESUMES_FOLDER"], parserCandidateID);

                //If file name is empty return
                if (fileName == string.Empty)
                    return false;

                uploaded = true;
            }

            if (uploaded)
            {
                // Insert the log entry.
                int activityLogID = new ResumeRepositoryBLManager().InsertUploadResumeLog(Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value),
                        ResumeUploader_candidateIDTextBox.Text, ResumeUploader_resumeAsyncFileUpload.FileName,
                        base.userID, ResumeUploader_resumeAsyncFileUpload.FileName);
            }

            return uploaded;
        }

        /// <summary>
        /// Method that saves the uploaded file into the specific path.
        /// </summary>
        /// <param name="path">
        /// A <see cref="string"/> that holds the path.
        /// </param>
        /// <param name="parserCandidateID">
        /// A <see cref="int"/> that holds the parser candidate ID. This ID will
        /// be prefixed with the file name.
        /// </param>
        private string SaveUploadedFileAs(string path, int parserCandidateID)
        {
            try
            {
                string fileName = ResumeUploader_resumeAsyncFileUpload.FileName;

                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                //Check if the uploaded file is word or pdf file
                if ((fileName == string.Empty) ||
                     ((ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType
                     != "application/msword") && (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType != "application/ms-word") && (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType
                     != "application/pdf")) ||
                      ((Path.GetExtension(fileName).ToLower() != ".doc") && (Path.GetExtension(fileName).ToLower() != ".pdf") && (Path.GetExtension(fileName).ToLower() != ".rtf") &&
                      (Path.GetExtension(fileName).ToLower() != ".docx")))
                {
                    ShowMessage(ResumeUploader_topErrorMessageLabel,
                        Resources.HCMResource.ResumeUploader_PleaseProvideAWordFile);
                }
                else if (fileName != string.Empty)
                {
                    //To create a directory in the server
                    //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);

                    string saveAsPath = string.Empty;

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".doc")
                    {
                        //To save word file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + ResumeUploader_candidateIDTextBox.Text + ".doc";
                    }

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".pdf")
                    {
                        //To save pdf file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + ResumeUploader_candidateIDTextBox.Text + ".pdf";
                    }
                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".rtf")
                    {
                        //To save pdf file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + ResumeUploader_candidateIDTextBox.Text + ".rtf";
                    }
                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".docx")
                    {
                        //To save pdf file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + ResumeUploader_candidateIDTextBox.Text + ".docx";
                    }

                    if (File.Exists(saveAsPath) == true)
                    {
                        File.Delete(saveAsPath);
                    }

                    ResumeUploader_resumeAsyncFileUpload.SaveAs(saveAsPath);
                    return saveAsPath;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(ResumeUploader_topErrorMessageLabel, ex.Message);
            }

            return string.Empty;
        }

        /// <summary>
        /// This method helps to clear the success/error messages.
        /// </summary>
        private void ClearAllLabelMessage()
        {
            // Clear messages.
            ResumeUploader_topErrorMessageLabel.Text = string.Empty;
            ResumeUploader_topSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// This method helps to save the file in the server.
        /// </summary>
        private string SaveFileAs()
        {
            if (!Utility.IsNullOrEmpty(ResumeUploader_resumeAsyncFileUpload.FileName))
                Session["SELECTED_FILE_NAME"] = ResumeUploader_resumeAsyncFileUpload.FileName;

            string fileName = ResumeUploader_resumeAsyncFileUpload.FileName;
            //Check if the file is word or a pdf file 
            if ((fileName == string.Empty) ||
                 (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType
                 != "application/msword") &&
                  (Path.GetExtension(fileName).ToLower() != ".doc") && (Path.GetExtension(fileName).ToLower() != ".pdf") && (Path.GetExtension(fileName).ToLower() != ".rtf") &&
                  (Path.GetExtension(fileName).ToLower() != ".docx") &&
                  (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType
                 != "application/pdf") && (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType != "application / vnd.openxmlformats - officedocument.wordprocessingml.document"))
            {
                ShowMessage(ResumeUploader_topErrorMessageLabel,
                    Resources.HCMResource.ResumeUploader_PleaseProvideAWordFile);
            }
            else if (fileName != string.Empty)
            {
                //To create a directory in the server
                Directory.CreateDirectory(Server.MapPath("~/") + "\\Resumes");
                int userID = Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value);

                string saveAsPath = string.Empty;

                if ((Path.GetExtension(fileName).ToLower() == ".doc") || (Path.GetExtension(fileName).ToLower() == ".docx"))
                {
                    //To save word file as 
                    saveAsPath = Server.MapPath("~/") + "Resumes\\"
                         + userID + "_" + ResumeUploader_candidateFirstNameHiddenField.Value + "_" + 
                         ResumeUploader_candidateLastNameHiddenField.Value + "_" + 
                         DateTime.Now.Ticks.ToString() + Path.GetExtension(fileName);
                    ResumeUploader_resumeAsyncFileUpload.PostedFile.SaveAs(saveAsPath);
                }
                else if (fileName.ToLower().EndsWith(".pdf"))
                {
                    //To save pdf file as 
                    saveAsPath = Server.MapPath("~/") + "Resumes\\"
                         + userID + "_" + ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                         ResumeUploader_candidateLastNameHiddenField.Value + "_" + 
                         DateTime.Now.Ticks.ToString() + ".pdf";
                    ResumeUploader_resumeAsyncFileUpload.PostedFile.SaveAs(saveAsPath);
                }
                else if (fileName.ToLower().EndsWith(".rtf"))
                {
                    //To save pdf file as 
                    saveAsPath = Server.MapPath("~/") + "Resumes\\"
                         + userID + "_" + ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                         ResumeUploader_candidateLastNameHiddenField.Value + "_" +
                         DateTime.Now.Ticks.ToString() + ".rtf";
                    ResumeUploader_resumeAsyncFileUpload.PostedFile.SaveAs(saveAsPath);
                }
                return saveAsPath;
            }
            else if (ResumeUploader_resumeAsyncFileUpload.Attributes["filename"].ToString() != "")
            {
                string strFilePath = ResumeUploader_resumeAsyncFileUpload.Attributes["filename"].ToString();

                //To create a directory in the server
                //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);
                int userID = Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value);

                string saveAsPath = string.Empty;

                if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"].ToString()).ToLower() == ".doc")
                {
                    //To save word file as 
                    saveAsPath = strFilePath + "\\" + userID + "_"
                        + ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                        ResumeUploader_candidateLastNameHiddenField.Value + ".doc";
                }

                if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"].ToString()).ToLower() == ".pdf")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                        ResumeUploader_candidateLastNameHiddenField.Value + ".pdf";
                }

                if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.Attributes["filename"].ToString()).ToLower() == ".rtf")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                        ResumeUploader_candidateLastNameHiddenField.Value + ".rtf";
                }

                ResumeUploader_resumeAsyncFileUpload.PostedFile.SaveAs(saveAsPath);

                return saveAsPath;

            }
            return string.Empty;
        }

        /// <summary>
        /// This method helps to save the file into specified folder on server.
        /// </summary>
        private string SaveUploadedFileAs()
        {
            string fileName = ResumeUploader_resumeAsyncFileUpload.FileName;
            Session["SELECTED_FILE_NAME"] = fileName;

            string strFilePath = ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"].ToString();
            //Check if the uploaded file is word or pdf file
            if ((fileName == string.Empty) ||
                 ((ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType
                 != "application/msword") && (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType != "application/ms-word") && (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && (ResumeUploader_resumeAsyncFileUpload.PostedFile.ContentType
                 != "application/pdf")) ||
                  ((Path.GetExtension(fileName).ToLower() != ".doc") && (Path.GetExtension(fileName).ToLower() != ".pdf") && (Path.GetExtension(fileName).ToLower() != ".rtf") &&
                  (Path.GetExtension(fileName).ToLower() != ".docx")))
            {
                ShowMessage(ResumeUploader_topErrorMessageLabel,
                    Resources.HCMResource.ResumeUploader_PleaseProvideAWordFile);
            }
            else if (fileName != string.Empty)
            {
                //To create a directory in the server
                //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);
                int candidateInfoID = Convert.ToInt16(ResumeUploader_candidateIDHiddenField.Value);

                string resumeName = string.Empty;

                if (!Utility.IsNullOrEmpty(Session["SELECTED_FILE_NAME"]))
                    resumeName = Session["SELECTED_FILE_NAME"].ToString();
                else
                    resumeName = Path.GetFileName(ResumeUploader_resumeAsyncFileUpload.FileName);

                // Insert parser candidate information and retreive the ID.
                int parserCandidateID = new ResumeRepositoryBLManager().InsertParserCandidateInformation
                    (candidateInfoID, "RUT_CRU", resumeName, base.userID);

                if (parserCandidateID != 0)
                {
                    string saveAsPath = string.Empty;

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".doc")
                    {
                        //To save word file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" +
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".doc";
                    }

                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".pdf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" +
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".pdf";
                    }
                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".rtf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" +
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".rtf";
                    }
                    if (Path.GetExtension(ResumeUploader_resumeAsyncFileUpload.FileName).ToLower() == ".docx")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\" + parserCandidateID + "_" +
                            ResumeUploader_candidateFirstNameHiddenField.Value + "_" +
                            ResumeUploader_candidateLastNameHiddenField.Value + ".docx";
                    }

                    if (File.Exists(saveAsPath) == true)
                    {
                        File.Delete(saveAsPath);
                    }
                    ResumeUploader_resumeAsyncFileUpload.SaveAs(saveAsPath);
                    return saveAsPath;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// This method helps to preview the file.
        /// </summary>
        private void PreviewResume(string filePath)
        {
            OfficeFileReader objFileReader = new OfficeFileReader();

            WordDocumentReader docReader = new WordDocumentReader();
            //string ResumeContent = "";
            try
            {
                if (filePath != "")
                {
                    //Check if the document is word document
                    if ((Path.GetExtension(filePath).ToLower() == ".doc") || (Path.GetExtension(filePath).ToLower() == ".docx"))
                    {
                        ResumeUploader_previewHtmlDiv.InnerHtml = docReader.GetPreviewText(filePath).Replace("\r", "<br>")
                           .Replace("\a", "<br>").Replace("\n", "<br>");
                    }
                    //if the document is a pdf then read using document reader
                    else if (Path.GetExtension(filePath).ToLower() == ".pdf")
                    {
                        PDFDocumentReader pdfReader = new PDFDocumentReader();

                        //Get the string and display the string
                        ResumeUploader_previewHtmlDiv.InnerHtml = pdfReader.GetPreviewText(filePath).
                            Replace("\r", "<br>")
                           .Replace("\a", "<br>").Replace("\n", "<br>");
                    }
                    //if the document is a rtf then read using RTFDocumentReader
                    else if (Path.GetExtension(filePath).ToLower() == ".rtf")
                    {
                        RTFDocumentReader rtfReader = new RTFDocumentReader();

                        //Get the string and display the string
                        ResumeUploader_previewHtmlDiv.InnerHtml = rtfReader.GetPreviewText(filePath).
                            Replace("\r", "<br>")
                           .Replace("\a", "<br>").Replace("\n", "<br>");
                    }
                }
            }
            catch
            { }
        }

        /// <summary>
        /// Method that loads the candidate resume versions.
        /// </summary>
        private void LoadResumeVersions()
        {
            // Keep selected candidate ID in hidden. This is useful to preload,
            // last selected candidate, when navigating back from the child
            // pages.
            Session[Constants.SearchCriteriaSessionKey.RESUME_UPLOADER_SELECTED_CANDIDATE] = 
                ResumeUploader_candidateIDHiddenField.Value;

            // Load the list of resume versions.
            List<ResumeVersion> resumeVersions = new ResumeRepositoryBLManager().
                GetResumeVersions(Convert.ToInt32(ResumeUploader_candidateIDHiddenField.Value));

            // Keep the resume present status in hidden field.
            ResumeUploader_isResumePresentHiddenField.Value = (resumeVersions == null || resumeVersions.Count == 0 ? "N" : "Y");

            if (resumeVersions == null)
            {
                ShowMessage(ResumeUploader_topErrorMessageLabel, Resources.HCMResource.ResumeEditor_NoInformationFound);

                ResumeUploader_resumeVersionsGridView.DataSource = null;
                ResumeUploader_resumeVersionsGridView.DataBind();
            }
            else
            {
                // Show the resume versions.
                ResumeUploader_resumeVersionsGridView.DataSource = resumeVersions;
                ResumeUploader_resumeVersionsGridView.DataBind();

                // Show the warning message.
                ShowMessage(ResumeUploader_topSuccessMessageLabel, string.Format("{0} version of resume(s) already present for the candidate", resumeVersions.Count));
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}
