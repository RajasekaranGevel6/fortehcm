﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/ModuleHomeMaster.Master"
    AutoEventWireup="true" CodeBehind="SampleDashboard.aspx.cs" Inherits="Forte.HCM.UI.MasterPages.SampleDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ModuleHomeMaster_body" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td style="width: 85%">
                <table cellpadding="0" cellspacing="0" border="0" width="99%" style="height: 100%;
                    table-layout: fixed">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="db_sample_right_bg_blue" style="width: 25px" valign="top">
                                    </td>
                                    <td class="dash_center_bg" valign="top" style="width: 95%">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="db_left_bg" style="width: 12px" align="right">
                                                            </td>
                                                            <td class="dash_icon_bg_center" align="center">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldTextBold" Text="Talent Scout" ID="UserDashboard_createPPLabel"
                                                                                ToolTip="Create Position Profile"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="db_right_bg" style="width: 12px" align="left">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px" width="100%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td width="100%">
                                                                <div class="db_content_div" style="width: 100%">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="width: 25%">
                                                                            </td>
                                                                            <td style="border: solid; border-color: black; width: 10%">
                                                                                <asp:ImageButton ID="positionProfileImageButton" runat="server" SkinID="sknbtnPPLeftIcon"
                                                                                    PostBackUrl="~/PositionProfile/SearchPositionProfile.aspx" />
                                                                                &nbsp;<br />
                                                                                <asp:Label ID="positionProfile" runat="server" Text="Position Profile Search"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 10%">
                                                                            </td>
                                                                            <td style="border: solid; border-color: Orange; width: 10%">
                                                                                <asp:ImageButton ID="ImageButton1" runat="server" SkinID="sknbtnTSLeftIcon" />
                                                                                <br />
                                                                                <asp:Label ID="talentScoutSearch" runat="server" Text="Weighted Search"></asp:Label>
                                                                                <br />
                                                                                <asp:LinkButton ID="search1" runat="server" Text="Search 1" PostBackUrl="~/TalentScoutHome.aspx"></asp:LinkButton>
                                                                                <br />
                                                                                <asp:LinkButton ID="search2" runat="server" Text="Search 2" PostBackUrl="~/TalentScoutHome.aspx"></asp:LinkButton>
                                                                                <br />
                                                                            </td>
                                                                            <td style="width: 10%">
                                                                            </td>
                                                                            <td style="border: solid; border-color: black; width: 10%">
                                                                                <asp:ImageButton ID="ImageButton2" runat="server" SkinID="sknbtnTSLeftIcon" PostBackUrl="~/TalentScoutHome.aspx" />
                                                                                <br />
                                                                                <asp:Label ID="ttsLabel" runat="server" Text="Talent Scout Result"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="UserDashboard_positionProfileMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                                        runat="server" ToolTip="More Position Profile " CommandName="SearchPositionProfile">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="db_right_bg_blue" style="width: 25px" valign="top">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="4" style="width: 15%">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="vertical-align: top">
                    <tr>
                        <td valign="top" align="center">
                            <asp:LinkButton ID="UpgradeLinkButton" runat="server" Text="Upgrade" SkinID="sknDashboardLinkButton"
                                ToolTip="Click here to upgrade your subscription " OnClick="UpgradeLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td class="td_height_20">
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td id="report_right_bg_db" runat="server" class="db_sampleright_bg_green" style="width: 12px"
                            align="right">
                        </td>
                        <td class="dash_center_bg" valign="top" align="center">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="db_left_bg" style="width: 12px" align="right">
                                                </td>
                                                <td class="dash_icon_bg_center" align="center" valign="middle">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" SkinID="sknLabelFieldTextBold" Text="Report" ID="Label1"
                                                                    ToolTip="Create Position Profile"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="db_right_bg" style="width: 12px" align="left">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="UserDashboard_reportLeftIconImageButton" runat="server" SkinID="sknbtnReportLeftIcon"
                                CommandName="TestReport" ToolTip="Test Report" PostBackUrl="~/ReportCenter/TestReport.aspx"
                                Enabled="false" />
                            <br />
                            <asp:Label ID="testReportLabel" runat="server" Text="Report"></asp:Label>
                        </td>
                        <td id="report_left_bg_db" runat="server" class="db_right_bg_green" style="width: 12px"
                            align="right">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" style="width: 99%">
                    <tr>
                        <td id="assessment_right_bgTD" runat="server" class="db_sampleright_bg_green">
                        </td>
                        <td style="width: 95%" class="dash_center_bg" valign="top">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="db_left_bg" style="width: 12px" align="right">
                                                </td>
                                                <td class="dash_icon_bg_center" align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" SkinID="sknLabelFieldTextBold" Text="Assessment Module"
                                                                    ID="UserDashboard_searchTestLabel" ToolTip="Search Test"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="db_right_bg" style="width: 12px" align="right">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 20px">
                                        <div class="db_content_div" style="width: 100%">
                                            <table width="100% " cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%">
                                                    </td>
                                                    <td style="border: solid; border-color: black; width: 10%">
                                                        <asp:ImageButton ID="QuestionImageButton" runat="server" SkinID="sknbtnTestLeftIcon"
                                                            ToolTip="Question and Test Creation" Enabled="false" PostBackUrl="~/TestMaker/CreateManualTest.aspx" />
                                                        <br />
                                                        <asp:Label ID="QuestionLabel" runat="server" Text="Question and test creation"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                    </td>
                                                    <td style="border: solid; border-color: black; width: 10%">
                                                        <asp:ImageButton ID="ScheduleCandidateButton" runat="server" SkinID="sknbtnSCLeftIcon"
                                                            Enabled="false" PostBackUrl="~/Scheduler/ScheduleCandidate.aspx" />
                                                        <br />
                                                        <asp:Label ID="ScheduleLabel" runat="server" Text="Schedule Candidate"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                    </td>
                                                    <td style="border: solid; border-color: black; width: 10%">
                                                        <asp:ImageButton ID="OnlineTestImageButton" runat="server" SkinID="sknSampleDashboardImageButton"
                                                            Enabled="false" PostBackUrl="~/TestMaker/TestResult.aspx" />
                                                        <br />
                                                        <asp:Label ID="TestConductorLabel" runat="server" Text="Online Test"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                    </td>
                                                    <td style="border: solid; border-color: black; width: 10%">
                                                        <asp:ImageButton ID="OnlineInterviewImageButton" runat="server" SkinID="sknbtnReportLeftIcon"
                                                            Enabled="false" />
                                                        <br />
                                                        <asp:Label ID="InterviewLabel" runat="server" Text="Online Interview"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:LinkButton ID="UserDashboard_testMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                            CommandName="SearchTest" runat="server" ToolTip="More Tests"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td id="assessment_left_bgTD" runat="server" class="db_right_bg_green">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
