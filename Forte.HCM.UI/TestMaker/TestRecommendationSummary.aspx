<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestRecommendationSummary.aspx.cs"
    Inherits="Forte.HCM.UI.TestMaker.TestRecommendationSummary" MasterPageFile="~/MasterPages/SiteAdminMaster.Master" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc8" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="TestRecommendationSummary_bodyContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="TestRecommendationSummary_headerLiteral" runat="server" Text="Test Summary"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td align="right">
                                        <asp:LinkButton ID="TestRecommendationSummary_topResetLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Reset" OnClick="TestRecommendationSummary_resetLinkButton_Click" />
                                        &nbsp;|&nbsp;<asp:LinkButton ID="TestRecommendationSummary_topCancelLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="TestRecommendationSummary_topMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="TestRecommendationSummary_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestRecommendationSummary_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 100%">
                            <div id="TestRecommendationSummary_viewTestDetailsDiv" runat="server" style="display: none;
                                width: 100%">
                                <asp:UpdatePanel runat="server" ID="TestRecommendationSummary_viewTestDetailsUpdatePanel">
                                    <ContentTemplate>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 80%" align="left" class="header_text_bold">
                                                                <asp:Literal ID="TestRecommendationSummary_viewTestDetailLiteral" runat="server"
                                                                    Text="Test Details"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%" class="grid_body_bg">
                                                    <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 13%">
                                                                            <asp:Label ID="TestRecommendationSummary_viewNumberOfQuestionLabel" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Number Of Questions"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%">
                                                                            <asp:Label ID="TestRecommendationSummary_viewNumberOfQuestionValueLabel" runat="server"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="TestRecommendationSummary_viewSkillsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Test Skill(s)"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 46%">
                                                                            <asp:Label ID="TestRecommendationSummary_viewskillsValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 11%">
                                                                            <asp:Label ID="TestRecommendationSummary_viewTimeLimitLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Time Limit (hh:mm)"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 6%">
                                                                            <asp:Label ID="TestRecommendationSummary_viewTimeLimitValueLabel" runat="server"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 8%" valign="top">
                                                                            <asp:Label ID="TestRecommendationSummary_viewTestNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Test Name"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 37%" valign="top">
                                                                            <asp:Label ID="TestRecommendationSummary_viewTestNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 11%" valign="top">
                                                                            <asp:Label ID="TestRecommendationSummary_viewTestDescriptionLabel" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Test Description"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 43%" valign="top">
                                                                            <asp:Label ID="TestRecommendationSummary_viewTestDescriptionValueLabel" runat="server"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_bg" runat="server">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 80%" align="left" class="header_text_bold">
                                                                <asp:Literal ID="TestRecommendationSummary_viewTestSegmentsLiteral" runat="server"
                                                                    Text="Test Segments"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%" class="grid_body_bg">
                                                    <asp:GridView ID="TestRecommendationSummary_viewSegmentsGridView" runat="server"
                                                        AutoGenerateColumns="False" ShowHeader="False" SkinID="sknNewGridView" AutoGenerateDeleteButton="false"
                                                        OnRowDataBound="TestRecommendationSummary_viewSegmentsGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table width="100%" border="0" cellspacing="2" cellpadding="5">
                                                                        <tr>
                                                                            <td style="width: 100%">
                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td style="width: 5%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_segmentLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                runat="server" Text="Segment">
                                                                                            </asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td style="width: 93%" align="left">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_segmentNoLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                runat="server" Text='<%# Container.DataItemIndex + 1 %>'>
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 100%">
                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_categoryLabel" runat="server"
                                                                                                Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_categoryValueLabel"
                                                                                                runat="server" Text='<%# Eval("Category") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_subjectLabel" runat="server"
                                                                                                Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_subjectValueLabel"
                                                                                                runat="server" Text='<%# Eval("Subject") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_weightageLabel" runat="server"
                                                                                                Text="Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_weightageValueLabel"
                                                                                                runat="server" Width="8%" MaxLength="3" Text='<%# Eval("Weightage") %>' SkinID="sknLabelFieldText">
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_testAreaLabel" runat="server"
                                                                                                Text="Test Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_testAreaValueLabel"
                                                                                                runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            <asp:HiddenField ID="TestRecommendationSummary_viewSegmentsGridView_testAreaHiddenField"
                                                                                                runat="server" Value='<%# Eval("TestArea") %>' />
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_complexityLabel" runat="server"
                                                                                                Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_complexityValueLabel"
                                                                                                runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            <asp:HiddenField ID="TestRecommendationSummary_viewSegmentsGridView_complexityHiddenField"
                                                                                                runat="server" Value='<%# Eval("Complexity") %>' />
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_keywordLabel" runat="server"
                                                                                                Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_keywordValueLabel"
                                                                                                runat="server" Text='<%# Eval("Keyword") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <div id="TestRecommendationSummary_viewSegmentsGridView_expectedQuestionDiv" runat="server"
                                                                                    visible="false" style="width: 100%;">
                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 15%;">
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_expectedQuestionsHeadLabel"
                                                                                                    runat="server" Text="Expected Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 10%;">
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_expecetdQuestions"
                                                                                                    runat="server" Text='<%# Eval("ExpectedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%;">
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_returnQuestionsHeadLabel"
                                                                                                    runat="server" Text="Picked Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 10%;">
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_returnQuestionsLabel"
                                                                                                    runat="server" Text='<%# Eval("PickedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 8%;">
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_remarksHeadLabel" runat="server"
                                                                                                    Text="Remarks" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_remarksLabel" runat="server"
                                                                                                    Text='<%# Eval("Remarks") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_noOfQuestionsInDBHeadLabel"
                                                                                                    runat="server" Text="Total Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="TestRecommendationSummary_viewSegmentsGridView_noOfQuestionsLabel"
                                                                                                    runat="server" Text='<%# Eval("TotalRecordsinDB") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg" width="100%" runat="server">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%;" align="left" class="header_text_bold">
                                        <asp:Literal ID="TestRecommendationSummary_candidateDetailsLiteral" runat="server"
                                            Text="Candidate Details"></asp:Literal>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="TestRecommendationSummary_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="TestRecommendationSummary_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" ToolTip="Click here to expand the view"/>
                                        </span><span id="TestRecommendationSummary_searchResultsDownSpan" runat="server"
                                            style="display: block;">
                                            <asp:Image ID="TestRecommendationSummary_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" ToolTip="Click here to shrink the view"/>
                                        </span>
                                        <asp:HiddenField ID="TestRecommendationSummary_isMaximizedHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="Td2" class="grid_body_bg" runat="server">
                            <asp:UpdatePanel ID="ViewTestRecommendation_candidateDetailsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div style="width: 100%; height: 300px;" runat="server" id="TestRecommendationSummary_candidateDetailsDiv"
                                        visible="true">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                                            <tr>
                                                <td>
                                                    <div id="TestRecommendationSummary_searchCriteriasDiv" runat="server" style="display: block;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="panel_bg">
                                                                    <asp:UpdatePanel runat="server" ID="TestRecommendationSummary_criteriaUpdatePanel">
                                                                        <ContentTemplate>
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                                                            <tr>
                                                                                                <td style="width: 6%">
                                                                                                    <asp:Label ID="TestRecommendationSummary_statusLabel" runat="server" Text="Status"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 11%">
                                                                                                    <div style="float: left; padding-right: 2px;">
                                                                                                        <asp:DropDownList ID="TestRecommendationSummary_statusDropDownList" runat="server"
                                                                                                            Width="90px">
                                                                                                            <asp:ListItem Text="All" Value="" Selected="True"></asp:ListItem>
                                                                                                            <asp:ListItem Text="Saved" Value="SAT_SAVD"></asp:ListItem>
                                                                                                            <asp:ListItem Text="Scheduled" Value="SAT_SCHD"></asp:ListItem>
                                                                                                            <asp:ListItem Text="Completed" Value="SAT_COMP"></asp:ListItem>
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                    <div style="float: left; padding-top: 2px">
                                                                                                        <asp:ImageButton ID="TestRecommendationSummary_statusHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the status here"
                                                                                                            Width="16px" />
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td style="width: 1%">
                                                                                                </td>
                                                                                                <td style="width: 8%">
                                                                                                    <asp:Label ID="TestRecommendationSummary_candidateNameLabel" runat="server" Text="Candidate Name"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 18%">
                                                                                                    <asp:TextBox ID="TestRecommendationSummary_candidateNameTextBox" MaxLength="50" runat="server"
                                                                                                        Width="160px"></asp:TextBox>
                                                                                                </td>
                                                                                                <td style="width: 1%">
                                                                                                </td>
                                                                                                <td style="width: 11%">
                                                                                                    <asp:Label ID="TestRecommendationSummary_candidateEmailLabel" runat="server" Text="Candidate Email"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 18%">
                                                                                                    <asp:TextBox ID="TestRecommendationSummary_candidateEmailTextBox" MaxLength="500" runat="server"
                                                                                                        Width="160px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 4px">
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <asp:Button ID="TestRecommendationSummary_searchButton" runat="server" SkinID="sknButtonId"
                                                                                            Text="Search" OnClick="TestRecommendationSummary_searchButton_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 6px">
                                                </td>
                                            </tr>
                                            <tr id="TestRecommendationSummary_searchResultsTR" runat="server">
                                                <td class="header_bg" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 100%" align="left" class="header_text_bold">
                                                                <asp:Literal ID="TestRecommendationSummary_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                                &nbsp;<asp:Label ID="TestRecommendationSummary_testRecommendationsSortHelpLabel"
                                                                    runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="grid_body_bg">
                                                    <asp:UpdatePanel ID="TestRecommendationSummary_candidatesGridViewUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="TestRecommendationSummary_candidatesGridViewDiv">
                                                                            <asp:GridView ID="TestRecommendationSummary_candidatesGridView" runat="server" AllowSorting="True" Width="100%" SkinID="sknWrapHeaderGrid"
                                                                                AutoGenerateColumns="False" OnSorting="TestRecommendationSummary_candidatesGridView_Sorting"
                                                                                OnRowCommand="TestRecommendationSummary_candidatesGridView_RowCommand" OnRowCreated="TestRecommendationSummary_candidatesGridView_RowCreated"
                                                                                OnRowDataBound="TestRecommendationSummary_candidatesGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="30px" HeaderStyle-Wrap="true" ItemStyle-Width="30px" ItemStyle-Wrap="false">
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="TestRecommendationSummary_testRecommendationsGridViewDiv_viewImageButton"
                                                                                                runat="server" SkinID="sknViewImageButton" ToolTip="Test Statistics Info" CommandName="TestStatistics"
                                                                                                CommandArgument="<%# Container.DataItemIndex %>" Visible='<%# Eval("Status").ToString().ToUpper().Trim() == "SAT_COMP" %>' />
                                                                                            <asp:HiddenField ID="TestRecommendationSummary_candidatesGridView_userIDHiddenField"
                                                                                                Value='<%# Eval("UserId") %>' runat="server" />
                                                                                            <asp:HiddenField ID="TestRecommendationSummary_candidatesGridView_testKeyHiddenField"
                                                                                                Value='<%# Eval("TestKey") %>' runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Name" SortExpression="FIRST_NAME" HeaderStyle-Width="140px" HeaderStyle-Wrap="false" ItemStyle-Width="140px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_candidateNameLabel" runat="server"
                                                                                                Text='<%# Eval("CandidateName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Email" SortExpression="EMAIL" HeaderStyle-Width="110px" HeaderStyle-Wrap="false" ItemStyle-Width="110px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_candidateEmailLabel"
                                                                                                runat="server" Text='<%# Eval("CandidateEmail") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Status" SortExpression="STATUS" HeaderStyle-Width="60px" HeaderStyle-Wrap="false" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_statusLabel" runat="server"
                                                                                                Text='<%# Eval("StatusName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Test Date" SortExpression="DATE" HeaderStyle-Width="60px"  HeaderStyle-Wrap="false" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_testDateLabel" runat="server"
                                                                                                Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CompletedDate"))) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Answered Correctly(%)" SortExpression="ANSWERCORECTLY" HeaderStyle-Width="60px" HeaderStyle-Wrap="true" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_answeredCorrectlyLabel" runat="server"
                                                                                                Text='<%# Eval("AnsweredCorrectly") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Total Time Taken" SortExpression="TOTALTIMETAKEN" HeaderStyle-Width="60px" HeaderStyle-Wrap="true" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_timeTakenLabel" runat="server"
                                                                                                Text='<%# Eval("TimeTaken") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Avg Time Taken (Per Question)" SortExpression="AVGTOTALTIMETAKEN" HeaderStyle-Width="60px" HeaderStyle-Wrap="true" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_averageTimeTakenLabel" runat="server"
                                                                                                Text='<%# Eval("AverageTimeTaken") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                       <asp:TemplateField HeaderText="Absolute Score" SortExpression="ABSOLUTE_SCORE" HeaderStyle-Width="60px" HeaderStyle-Wrap="true" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_absoluteScoreLabel" runat="server"
                                                                                                Text='<%# Eval("AbsoluteScore") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Relative Score" SortExpression="RELATIVE_SCORE" HeaderStyle-Width="60px" HeaderStyle-Wrap="true" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_relativeScoreLabel" runat="server"
                                                                                                Text='<%# Eval("RelativeScore") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                 <asp:TemplateField HeaderText="Percentile" SortExpression="PERCENTILE" HeaderStyle-Width="60px" HeaderStyle-Wrap="true" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="TestRecommendationSummary_candidatesGridView_percentileLabel" runat="server"
                                                                                                Text='<%# Eval("Percentile") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <EmptyDataTemplate>
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                                        <tr>
                                                                                            <td style="height: 190px" valign="middle" align="center">
                                                                                                <asp:Label ID="TestRecommendationSummary_candidatesGridView_messageLabel" runat="server" Text="No candidate found to display" SkinID="sknErrorMessage"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="width: 100%">
                                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                            <tr>
                                                                                <td align="right" style="width: 100%">
                                                                                    <uc8:PageNavigator ID="TestRecommendationSummary_pagingNavigator" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="TestRecommendationSummary_searchButton" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="TestRecommendationSummary_byQuestion_bottomMessagesUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Label ID="TestRecommendationSummary_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestRecommendationSummary_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td align="right" style="width: 15%">
                                        <asp:LinkButton ID="TestRecommendationSummary_bottomResetLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Reset" OnClick="TestRecommendationSummary_resetLinkButton_Click" />
                                        &nbsp;|&nbsp;<asp:LinkButton ID="TestRecommendationSummary_bottomCancelLinkButton"
                                            runat="server" SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
