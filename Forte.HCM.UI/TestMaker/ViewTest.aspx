﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="ViewTest.aspx.cs" Inherits="Forte.HCM.UI.TestMaker.ViewTest" %>

<%@ Register Src="../CommonControls/ViewTestDetailsControl.ascx" TagName="ViewTestDetailsControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="ViewTest_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <asp:HiddenField ID="ViewTest_testStatusHiddenField" runat="server" />
    <asp:HiddenField ID="ViewTest_testKeyHiddenField" runat="server" />
    <asp:HiddenField ID="ViewTest_testAuthorHiddenField" runat="server" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ViewTest_headerLiteral" runat="server" Text="View Test"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ViewTest_topEditTestButton" runat="server" SkinID="sknButtonId" Text="Edit Test"
                                            OnClick="ViewTest_Edit_Button_Click" CommandName="EDIT_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewTest_topDeleteTestButton" runat="server" SkinID="sknButtonId"
                                            Text="Delete Test" OnClick="ViewTest_Delete_Button_Click" CommandName="DELETE_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewTest_topInactivetestButton" SkinID="sknButtonId" runat="server"
                                            Text="Deactivate Test" OnClick="ViewTest_Active_Button_Click" CommandName="DEACTIVATE_TEST"
                                            Visible="false" />
                                        <asp:Button ID="ViewTest_topActiveTestButton" SkinID="sknButtonId" runat="server"
                                            Text="Activate Test" OnClick="ViewTest_Active_Button_Click" CommandName="ACTIVATE_TEST"
                                            Visible="false" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewTest_topCreateTestSessionButton" runat="server" Text="Create Session"
                                            SkinID="sknButtonId" OnClick="ViewTest_Button_Click" CommandName="CREATE_SESSION" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="ViewTest_topCancelLinkButton" SkinID="sknActionLinkButton"
                                            runat="server" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ViewTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ViewTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolKit:TabContainer ID="ViewTest_mainTabContainer" runat="server" ActiveTabIndex="0">
                    <ajaxToolKit:TabPanel ID="ViewTest_testdetailsTabPanel" runat="server">
                        <HeaderTemplate>
                            Test Details
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="width: 100%">
                                        <uc1:ViewTestDetailsControl ID="ViewTest_testDetailsUserControl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="ViewTest_QuestionsTabPanel" HeaderText="questions" runat="server">
                        <HeaderTemplate>
                            Questions
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="ViewTest_testDraftGridviewUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="width: 100%" align="right">
                                                <asp:Label ID="ViewTest_groupByLabel" runat="server" Text="Group&nbsp;By" SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;
                                                <asp:DropDownList ID="ViewTest_groupByDDL" runat="server" OnSelectedIndexChanged="ViewTest_groupByDDL_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Text="Question" Value="QUESTION"></asp:ListItem>
                                                    <asp:ListItem Text="Category" Value="CATEGORY"></asp:ListItem>
                                                    <asp:ListItem Text="Subject" Value="SUBJECT"></asp:ListItem>
                                                    <asp:ListItem Text="Test Area" Value="TESTAREA"></asp:ListItem>
                                                    <asp:ListItem Text="Complexity" Value="COMPLEXITY"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" valign="top">
                                                <asp:GridView ID="ViewTest_testDrftGridView" runat="server" AutoGenerateColumns="False"
                                                    SkinID="sknNewGridView" OnRowDataBound="ViewTest_testDrftGridView_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                    <tr>
                                                                        <td valign="middle" style="width: 5%">
                                                                            <asp:Image ID="ViewTest_questionImage" runat="server" SkinID="sknQuestionImage" ToolTip="Question" />
                                                                            <asp:Label ID="ViewTest_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                        </td>
                                                                        <td colspan="6" style="width: 92%">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="label_multi_field_text">
                                                                                            <asp:Literal ID="ViewTest_questionLiteral" runat="server" Text='<%# Eval("Question") %>'></asp:Literal>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Image runat="server" ID="ViewTest_questionImageDisplay" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="ViewTest_answerQuestionLabel" runat="server" Text="Answer" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td colspan="5">
                                                                            <div class="label_multi_field_text">
                                                                                <asp:Label ID="ViewTest_answerQuestionTextBox" SkinID="sknLabelFieldText" runat="server"
                                                                                    Text='<%# Eval("Choice_Desc") %>'></asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="ViewTest_categoryQuestionLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="ViewTest_categoryQuestionTextBox" SkinID="sknLabelFieldText" runat="server"
                                                                                Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                        </td>
                                                                        <td style="width: 6%">
                                                                            <asp:Label ID="ViewTest_subjectQuestionLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="ViewTest_subjectQuestionTextBox" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="ViewTest_testAreaQuestionHeadLabel" runat="server" Text="Test Area"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="ViewTest_testAreaQuestionLabel" SkinID="sknLabelFieldText" runat="server"
                                                                                Text='<%# Eval("TestAreaName") %>' />
                                                                        </td>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="ViewTest_complexityQuestionHeadLabel" runat="server" Text="Complexity"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ViewTest_complexityQuestionLabel" SkinID="sknLabelFieldText" runat="server"
                                                                                Text='<%# Eval("Complexity") %>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                            DataField="QuestionID" SortExpression="QuestionID">
                                                                            <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField ReadOnly="True" HeaderText="Category" InsertVisible="False" DataField="Categories"
                                                                            SortExpression="Categories">
                                                                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField ReadOnly="True" HeaderText="Subject" InsertVisible="False" DataField="Subject"
                                                                            SortExpression="Subject">
                                                                            <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField ReadOnly="True" HeaderText="Question" InsertVisible="False" DataField="Question"
                                                                            SortExpression="Question">
                                                                            <ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField ReadOnly="True" HeaderText="Answer" InsertVisible="False" DataField="CorrectAnswer"
                                                                            SortExpression="CorrectAnswer">
                                                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField ReadOnly="True" HeaderText="Complexity" InsertVisible="False" DataField="Complexity"
                                                                            SortExpression="Complexity">
                                                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField ReadOnly="True" HeaderText="Author" InsertVisible="False" DataField="TestAuthor"
                                                                            SortExpression="TestAuthor">
                                                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                                                        </asp:BoundField>--%>
                                                    </Columns>
                                                    <HeaderStyle CssClass="grid_header" />
                                                    <AlternatingRowStyle CssClass="grid_001" />
                                                    <RowStyle CssClass="grid" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="tr_height_25" align="center">
                <asp:Label ID="ViewTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ViewTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 50%">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" class="header_bg">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ViewTest_bottomEditTestButton" runat="server" SkinID="sknButtonId"
                                            Text="Edit Test" OnClick="ViewTest_Edit_Button_Click" CommandName="EDIT_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewTest_bottomDeleteTestButton" SkinID="sknButtonId" runat="server"
                                            Text="Delete Test" OnClick="ViewTest_Delete_Button_Click" CommandName="DELETE_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewTest_bottomInactiveTestButton" SkinID="sknButtonId" runat="server"
                                            OnClick="ViewTest_Active_Button_Click" CommandName="DEACTIVATE_TEST" Text="Deactivate Test"
                                            Visible="false" />
                                        <asp:Button ID="ViewTest_bottomActiveTestButton" SkinID="sknButtonId" runat="server"
                                            Text="Activate Test" OnClick="ViewTest_Active_Button_Click" CommandName="ACTIVATE_TEST"
                                            Visible="false" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewTest_bottomCreateTestSessionButton" SkinID="sknButtonId"
                                            runat="server" Text="Create Session" OnClick="ViewTest_Button_Click" CommandName="CREATE_SESSION" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="ViewTest_bottomCancelLinkButton" SkinID="sknActionLinkButton"
                                            runat="server" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%--                            <asp:Panel ID="ViewTest_deletepopupPanel" runat="server" Style="display: none" CssClass="popupcontrol_confirm_remove">
                                <uc2:ConfirmMsgControl ID="ViewTest_deleteModalPopupExtendeControl" runat="server"
                                    Title="Delete Confirm" Message="Are you sure want to delete the test?" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="ViewTest_deletepopupExtender" runat="server"
                                PopupControlID="ViewTest_deletepopupPanel" TargetControlID="ViewTest_bottomDeleteTestButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                            --%>
                            <asp:Panel ID="ViewTest_inactivepopupPanel" runat="server" Style="display: none"
                                CssClass="popupcontrol_confirm_remove">
                                <div style="display: none" id="ViewTest_hiddenButtonDIV">
                                    <asp:Button ID="ViewTest_hiddenButton" runat="server" />
                                </div>
                                <uc2:ConfirmMsgControl ID="ViewTest_inactivePopupExtenderControl" runat="server"
                                    OnOkClick="ViewTest_OkClick" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="ViewTest_activeInActivepopupExtender" runat="server"
                                PopupControlID="ViewTest_inactivepopupPanel" TargetControlID="ViewTest_hiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
