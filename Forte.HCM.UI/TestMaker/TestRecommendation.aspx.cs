#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestRecommendation.cs
// File that represents the user interface layout and functionalities for
// the TestRecommendation page. This page helps in creating and editing  
// recommended test and segment details. This class inherits the
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives                                                             

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the TestRecommendation page. This page helps in creating and editing  
    /// recommended test and segment details. This class inherits the
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class TestRecommendation : PageBase
    {
        #region Declarations                                                   

        /// <summary>
        /// A <see cref="string"/> that holds the status that indicates whether
        /// to display page as create or edit mode.
        /// </summary>
        /// <remarks>
        /// The value 'E' indicates edit and empty indicates create mode.
        /// </remarks>
        private string mode = string.Empty;

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the questions for the test in test draft gridview.
        /// </summary>
        private const string TestRecommendation_QUESTIONVIEWSTATE = "TestRecommendation_QUESTIONSGRID";

        
        #endregion Declarations

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loading.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button.
                Page.Form.DefaultButton = TestRecommendation_bottomSaveButton.UniqueID;

                //Page title
                if (!Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    mode = Request.QueryString["mode"].ToUpper();

                if (mode == "E")
                {
                    Master.SetPageCaption("Edit Test Recommendation");
                    TestRecommendation_headerLiteral.Text = "Edit Test Recommendation";
                }
                else if (mode == "C")
                {
                    Master.SetPageCaption("Copy Test Recommendation");
                    TestRecommendation_headerLiteral.Text = "Copy Test Recommendation";
                }
                else
                {
                    Master.SetPageCaption("Create Test Recommendation");
                    TestRecommendation_headerLiteral.Text = "Create Test Recommendation";
                }

                ClearAllLabelMessages();
                CheckAndSetExpandorRestore();
                if (!IsPostBack)
                {
                    LoadValues();

                    if (mode == "E" || mode == "C")
                    {
                        // Load test details.
                        LoadTestDetails();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler event that will reset the page when user cliks on the reset link button
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendation_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the generate button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendation_byQuestion_generateTestButton_Click(object sender, EventArgs e)
        {
            try
            {
                TestRecommendation_byQuestion_testDrftDiv.Visible = false;
                TestRecommendation_byQuestion_automatedTestUserControl.Visible = false;
                ViewState[TestRecommendation_QUESTIONVIEWSTATE] = null;

                TestRecommendation_byQuestion_testSkillTextBox.Text =
                    Request[TestRecommendation_byQuestion_testSkillTextBox.UniqueID].Trim();

                LoadAutomatedQuestions(true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the test draft grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void TestRecommendation_byQuestion_testDraftGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the test segment grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendation_byQuestion_testSegmentGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                ImageButton TestRecommendation_categoryImageButton = ((ImageButton)e.Row.FindControl
                        ("TestRecommendation_byQuestion_categoryImageButton"));
                TextBox TestRecommendation_categoryTextBox = ((TextBox)e.Row.FindControl
                        ("TestRecommendation_byQuestion_categoryTextBox"));
                TextBox TestRecommendation_subjectTextBox = ((TextBox)e.Row.FindControl
                        ("TestRecommendation_byQuestion_subjectTextBox"));
                HiddenField TestRecommendation_categoryHiddenField = (HiddenField)e.Row.FindControl
                        ("TestRecommendation_byQuestion_categoryHiddenField");
                HiddenField TestRecommendation_categoryNameHiddenField = (HiddenField)e.Row.FindControl
                        ("TestRecommendation_byQuestion_categoryNameHiddenField");
                HiddenField TestRecommendation_subjectNameHiddenField = (HiddenField)e.Row.FindControl
                        ("TestRecommendation_byQuestion_subjectNameHiddenField");
                Label TestRecommendation_byQuestion_expecetdQuestions =
                        (Label)e.Row.FindControl("TestRecommendation_byQuestion_expecetdQuestions");
                //
                string[] SelectedTestAreaID = ((HiddenField)e.Row.
                    FindControl("TestRecommendation_byQuestion_testAreaHiddenField")).Value.Split(',');
                string[] SelectedComplexityID = ((HiddenField)e.Row.
                    FindControl("TestRecommendation_byQuestion_complexityHiddenField")).Value.Split(',');
                //
                //Select Subject and Category
                TestRecommendation_categoryImageButton.Attributes.Add("onclick",
                        "return LoadCategorySubjectLookUpforReadOnly('" + TestRecommendation_categoryTextBox.ClientID +
                            "','" + TestRecommendation_subjectTextBox.ClientID + "','" +
                            TestRecommendation_categoryHiddenField.ClientID + "','" +
                            TestRecommendation_categoryNameHiddenField.ClientID + "','" +
                            TestRecommendation_subjectNameHiddenField.ClientID + "');");
                CheckBoxList TestRecommendation_testAreaCheckBoxList = ((CheckBoxList)e.Row.FindControl
                        ("TestRecommendation_byQuestion_testAreaCheckBoxList"));
                CheckBoxList TestRecommendation_complexityCheckBoxList = ((CheckBoxList)e.Row.FindControl
                        ("TestRecommendation_byQuestion_complexityCheckBoxList"));
                // Binds test aread check box list
                TestRecommendation_testAreaCheckBoxList.DataSource = GetTestAreaList();
                TestRecommendation_testAreaCheckBoxList.DataTextField = "AttributeName";
                TestRecommendation_testAreaCheckBoxList.DataValueField = "AttributeID";
                TestRecommendation_testAreaCheckBoxList.DataBind();
                for (int i = 0; i < SelectedTestAreaID.Length; i++)
                    if (SelectedTestAreaID[i] != "")
                        TestRecommendation_testAreaCheckBoxList.Items.FindByValue(SelectedTestAreaID[i]).Selected = true;
                // Bind Complexity Check box list
                TestRecommendation_complexityCheckBoxList.DataSource = GetComplexityList();
                TestRecommendation_complexityCheckBoxList.DataTextField = "AttributeName";
                TestRecommendation_complexityCheckBoxList.DataValueField = "AttributeID";
                TestRecommendation_complexityCheckBoxList.DataBind();
                for (int i = 0; i < SelectedComplexityID.Length; i++)
                    if (SelectedComplexityID[i] != "")
                        TestRecommendation_complexityCheckBoxList.Items.FindByValue(SelectedComplexityID[i]).Selected = true;
                if (TestRecommendation_byQuestion_expecetdQuestions.Text != "")
                    ((HtmlGenericControl)e.Row.FindControl("TestRecommendation_byQuestion_expectedQuestionsDiv")).Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendation_byQuestion_testDraftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "Select")
                    return;
                TestRecommendation_byQuestion_bottomQuestionModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the test draft grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void TestRecommendation_byQuestion_testDraftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                ImageButton alertImageButton = (ImageButton)e.Row.FindControl("CreateAutomaticTes_byQuestion_testDrftRemoveImage");
                LinkButton TestRecommendation_questionIdLinkButton =
                                    (LinkButton)e.Row.FindControl("QuestionDetailPreviewControl_byQuestion_questionLinkButton"); alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" +
                    TestRecommendation_questionIdLinkButton.Text + "' );");
                HtmlContainerControl SearchQuestion_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("TestRecommendation_byQuestion_detailsDiv");
                HtmlAnchor SearchQuestion_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("TestRecommendation_byQuestion_focusDownLink");
                TestRecommendation_questionIdLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                SearchQuestion_detailsDiv.ClientID + "','" + SearchQuestion_focusDownLink.ClientID + "')");
                PlaceHolder TestRecommendation_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl(
                    "TestRecommendation_byQuestion_answerChoicesPlaceHolder");
                TestRecommendation_answerChoicesPlaceHolder.Controls.Add
                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions
                    (TestRecommendation_questionIdLinkButton.Text.ToString()), false));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the delete segment image button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendation_byQuestion_testSegmentGridView_deleteImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DeleteTestSegmentRow(((GridViewRow)((ImageButton)sender).Parent.Parent).RowIndex);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add segment button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendation_byQuestion_addSegmantLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                AddTestSegmentRow(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendation_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if mandatory information is given.
                if (!IsValidData())
                    return;

                ViewState[TestRecommendation_QUESTIONVIEWSTATE] = null;

                TestRecommendation_byQuestion_testSkillTextBox.Text =
                    Request[TestRecommendation_byQuestion_testSkillTextBox.UniqueID].Trim();

                // Load questions (but do not show on the grid).
                if (LoadAutomatedQuestions(false) == false)
                    return;

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[TestRecommendation_QUESTIONVIEWSTATE]))
                    return;

                if (Convert.ToInt32(TestRecommendation_byQuestion_totalNumberTextBox.Text) > 
                    ((List<QuestionDetail>)ViewState[TestRecommendation_QUESTIONVIEWSTATE]).Count)
                {
                    ShowConfirmPopUp("There is lack of questions present in the repository for the given test segments. The system will not be able to generate the expected number of questions during the test session. Do you want to save anyway ?");
                }
                else
                {
                    SaveTest();
                    SetVisibleStatusForButton(true);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void TestRecommendation_okPopUpClick(object sender, EventArgs e)
        {
            try
            {
                SaveTest();
                SetVisibleStatusForButton(true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void TestRecommendation_cancelPopUpClick(object sender, EventArgs e)
        {
            DataTable searchCriteria = null;
            try
            {
                searchCriteria = BuildTableFromGridView();
                TestRecommendation_byQuestion_testSegmentGridView.DataSource = searchCriteria;
                TestRecommendation_byQuestion_testSegmentGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(searchCriteria)) 
                    searchCriteria = null;
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the test details.
        /// </summary>
        private void LoadTestDetails()
        {
            // Check if test recommendation ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["id"]))
            {
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, "Test recommendation ID is missing");

                return;
            }

            // Check if test recommendation ID is vaid.
            int testRecommendationID = 0;
            int.TryParse(Request.QueryString["id"], out testRecommendationID);

            if (testRecommendationID == 0)
            {
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, "Test recommendation ID is invalid");

                return;
            }

            // Get test recommendation detail.
            TestRecommendationDetail testDetail = new TestRecommendationBLManager().
                GetTestSegmentDetail(testRecommendationID);

            if (testDetail == null)
            {
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, "Test recommendation not found");

                return;
            }

            // Load test detail.
            TestRecommendation_byQuestion_totalNumberTextBox.Text = testDetail.NoOfQuestions.ToString();
            TestRecommendation_byQuestion_testSkillTextBox.Text = testDetail.Skill;
            TestRecommendation_byQuestion_timeSearchTextbox.Text = Utility.ConvertSecondsToHHMM(testDetail.TimeLimit);
            TestRecommendation_byQuestion_testNameTextBox.Text = testDetail.TestName;
            TestRecommendation_testDescriptionTextBox.Text = testDetail.TestDescription;

            // Construct segment data table.
            DataTable segmentTable = new DataTable();
            AddTestSegmentColumns(ref segmentTable);

            // Fill segments.
            FillSegmentsTable(testDetail.Segments, segmentTable);

            // Assign segments.
            TestRecommendation_byQuestion_testSegmentGridView.DataSource = segmentTable;
            TestRecommendation_byQuestion_testSegmentGridView.DataBind();
        }

        /// <summary>
        /// Method that shows the confirmation popup, whenenever a user confirmation
        /// required during svae.
        /// </summary>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        private void ShowConfirmPopUp(string message)
        {
            TestRecommendation_confirmPopupExtenderControl.Message = message;
            TestRecommendation_confirmPopupExtenderControl.Title = "Warning";
            TestRecommendation_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
            TestRecommendation_confirmPopupExtenderControl.DataBind();
            TestRecommendation_saveTestModalPopupExtender.Show();
        }

        
        /// <summary>
        /// Method that saves the question to the DB and generates a test key
        /// which will be shown to the user in the control.
        /// </summary>
        private void SaveTest()
        {
            TestRecommendationDetail testRecommendationDetail = null;
            DataTable searchCriteria = null;

            try
            {
                searchCriteria = BuildTableFromGridView();
                
                testRecommendationDetail = new TestRecommendationDetail();

                int testRecommendationID = 0;
                if (mode == "E")
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString["id"]))
                        int.TryParse(Request.QueryString["id"], out testRecommendationID);
                }

                // Assign test recommendation ID. For create mode this will be zero 
                // and for edit mode this will be non-zero.
                testRecommendationDetail.TestRecommendId = testRecommendationID;

                testRecommendationDetail.UserId = userID;
                testRecommendationDetail.NoOfQuestions = Convert.ToInt32(TestRecommendation_byQuestion_totalNumberTextBox.Text.Trim());
                testRecommendationDetail.TestName = TestRecommendation_byQuestion_testNameTextBox.Text.Trim();
                testRecommendationDetail.Skill = TestRecommendation_byQuestion_testSkillTextBox.Text.Trim();
                testRecommendationDetail.TestDescription = TestRecommendation_testDescriptionTextBox.Text.Trim();
                testRecommendationDetail.TimeLimit = Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(
                    TestRecommendation_byQuestion_timeSearchTextbox.Text);

                // Save test segments.
                new TestRecommendationBLManager().SaveTestSegments(testRecommendationDetail, searchCriteria,
                    Convert.ToInt32(userID));

                if (mode == "E")
                {
                    // Show a success message.
                    base.ShowMessage(TestRecommendation_topSuccessMessageLabel,
                        TestRecommendation_bottomSuccessMessageLabel, "Test recommendation updated successfully");
                }
                else if (mode == "C")
                {
                    // Show a success message.
                    base.ShowMessage(TestRecommendation_topSuccessMessageLabel,
                        TestRecommendation_bottomSuccessMessageLabel, "Test recommendation copied successfully");
                }
                else
                {
                    // Show a success message.
                    base.ShowMessage(TestRecommendation_topSuccessMessageLabel,
                        TestRecommendation_bottomSuccessMessageLabel, "Test recommendation created successfully");
                }
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(searchCriteria)) searchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testRecommendationDetail)) testRecommendationDetail = null;
            }
        }

        /// <summary>
        /// Clear all top and bottom message labels
        /// </summary>
        private void ClearAllLabelMessages()
        {
            TestRecommendation_topErrorMessageLabel.Text = string.Empty;
            TestRecommendation_topSuccessMessageLabel.Text = string.Empty;
            TestRecommendation_bottomErrorMessageLabel.Text = string.Empty;
            TestRecommendation_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Binds emtpty test segment table grid
        /// </summary>
        private void BindEmptyTestSegmentGrid()
        {
            DataTable dtTestSegment = new DataTable();
            AddTestSegmentColumns(ref dtTestSegment);
            DataRow drNewRow = dtTestSegment.NewRow();
            dtTestSegment.Rows.Add(drNewRow);
            TestRecommendation_byQuestion_testSegmentGridView.DataSource = dtTestSegment;
            TestRecommendation_byQuestion_testSegmentGridView.DataBind();
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Cat_sub_id", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Complexity", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("TestArea", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Category", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Subject", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Keyword", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("ExpectedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("PickedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("TotalRecordsinDB", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("QuestionsDifference", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Remarks", typeof(string));
        }

        /// <summary>
        /// Method that fills the segment table rows by taking the source data 
        /// from the segments list.
        /// </summary>
        /// <param name="segments">
        /// A list of <see cref="TestRecommendationSegment"/> that holds the source data.
        /// </param>
        /// <param name="segmentTable">
        /// A <see cref="DataTable"/> that holds the segment table.
        /// </param>
        private void FillSegmentsTable(List<TestRecommendationSegment> segments, DataTable segmentTable)
        {
            if (segments == null || segments.Count == 0)
                return;

            DataRow drNewRow = null;

            int SNo = 1;
            foreach (TestRecommendationSegment segment in segments)
            {
                drNewRow = segmentTable.NewRow();
                drNewRow["SNO"] = SNo++.ToString();
                drNewRow["Cat_sub_id"] = segment.CateogorySubjectID;
                drNewRow["Category"] = segment.Category;
                drNewRow["Subject"] = segment.Subject;
                drNewRow["Weightage"] = segment.Weightage;
                drNewRow["Keyword"] = segment.Keyword;
                drNewRow["TestArea"] = segment.TestArea;
                drNewRow["Complexity"] = segment.Complexity;
                segmentTable.Rows.Add(drNewRow);
            }
        }

        /// <summary>
        /// Builds data table from test segment grid view
        /// </summary>
        /// <returns>Data table that contains user given input for test segment grid</returns>
        private DataTable BuildTableFromGridView()
        {
            DataTable dtTestSegment = null;
            try
            {
                dtTestSegment = new DataTable();
                AddTestSegmentColumns(ref dtTestSegment);
                DataRow drNewRow = null;
                int Weightage = 0;
                int SNo = 1;
                foreach (GridViewRow gridViewRow in TestRecommendation_byQuestion_testSegmentGridView.Rows)
                {
                    drNewRow = dtTestSegment.NewRow();
                    drNewRow["SNO"] = SNo++.ToString();
                    drNewRow["Cat_sub_id"] = ((HiddenField)gridViewRow.
                        FindControl("TestRecommendation_byQuestion_categoryHiddenField")).Value;
                    drNewRow["Category"] = ((HiddenField)gridViewRow.
                        FindControl("TestRecommendation_byQuestion_categoryNameHiddenField")).Value.Trim();
                    drNewRow["Subject"] = ((HiddenField)gridViewRow.
                        FindControl("TestRecommendation_byQuestion_subjectNameHiddenField")).Value.Trim();
                    int.TryParse(((TextBox)gridViewRow.
                        FindControl("TestRecommendation_byQuestion_weightageTextBox")).Text, out Weightage);
                    if (Weightage > 0)
                        drNewRow["Weightage"] = Weightage;
                    drNewRow["Keyword"] = ((TextBox)gridViewRow.
                        FindControl("TestRecommendation_byQuestion_keywordTextBox")).Text;
                    drNewRow["TestArea"] = GetSelectedValuesfromCheckBoxList(
                        (CheckBoxList)gridViewRow.FindControl("TestRecommendation_byQuestion_testAreaCheckBoxList"));
                    drNewRow["Complexity"] = GetSelectedValuesfromCheckBoxList(
                        (CheckBoxList)gridViewRow.FindControl("TestRecommendation_byQuestion_complexityCheckBoxList"));
                    if (((HtmlContainerControl)gridViewRow.FindControl("TestRecommendation_byQuestion_expectedQuestionsDiv")).Visible)
                    {
                        drNewRow["ExpectedQuestions"] = ((Label)gridViewRow.FindControl("TestRecommendation_byQuestion_expecetdQuestions")).Text;
                        drNewRow["PickedQuestions"] = ((Label)gridViewRow.FindControl("TestRecommendation_byQuestions_returnQuestionsLabel")).Text;
                        drNewRow["Remarks"] = ((Label)gridViewRow.FindControl("TestRecommendation_byQuestin_remarkLabel")).Text;
                        drNewRow["TotalRecordsinDB"] = ((Label)gridViewRow.FindControl("TestRecommendation_byQuestion_noOfQuestionsLabel")).Text;
                    }
                    dtTestSegment.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                return dtTestSegment;
            }
            finally
            {
                if (dtTestSegment != null) dtTestSegment = null;
            }
        }

        /// <summary>
        /// Gets the elected ID's for a checkboxlist
        /// </summary>
        /// <param name="checkBoxList">checkboxlist to get the selected values</param>
        /// <returns>Selected ID's in the checkboxlist</returns>
        private string GetSelectedValuesfromCheckBoxList(CheckBoxList checkBoxList)
        {
            StringBuilder stringBuilder = null;
            try
            {
                if (stringBuilder == null)
                    stringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        stringBuilder.Append(checkBoxList.Items[i].Value);
                        stringBuilder.Append(",");
                    }
                }
                return stringBuilder.ToString().TrimEnd(',');
            }
            finally
            {
                if (stringBuilder != null) stringBuilder = null;
            }
        }

        /// <summary>
        /// Add an Emty row to the test segment grid (this methos will 
        /// also check for maximum segments limit and no of question given less than 
        /// segments or not)
        /// </summary>
        private void AddTestSegmentRow(bool AddPositionProfileRows)
        {
            DataTable dtTestSegment = BuildTableFromGridView();
            if (TestRecommendation_byQuestion_totalNumberTextBox.Text.Trim() != "")
                if (Convert.ToInt32(TestRecommendation_byQuestion_totalNumberTextBox.Text.Trim()) <= dtTestSegment.Rows.Count)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                     TestRecommendation_bottomErrorMessageLabel,
                     Resources.HCMResource.CreateAutomaticTest_SegmentsGreaterthanSegments);
                    return;
                }
            if (dtTestSegment.Rows.Count == 10)
            {
                base.ShowMessage(TestRecommendation_topErrorMessageLabel, TestRecommendation_bottomErrorMessageLabel,
                    "Maximum limit reached");
                return;
            }
            DataRow drNewRow = null;
            if (AddPositionProfileRows)
            {
                string[] PositionProfiles = TestRecommendation_byQuestion_positionProfileTagsHiddenField.Value.Split(',');
                for (int i = 0; i < PositionProfiles.Length; i++)
                {
                    if (dtTestSegment.Select("Keyword LIKE '%" + GetSkillName(PositionProfiles[i]) + "%'").Length > 0)
                        continue;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drNewRow))
                        drNewRow = null;
                    drNewRow = dtTestSegment.NewRow();
                    drNewRow["Keyword"] = GetSkillName(PositionProfiles[i]);
                    drNewRow["Weightage"] = GetSkillWeightage(PositionProfiles[i]);
                    dtTestSegment.Rows.Add(drNewRow);
                    if (dtTestSegment.Rows.Count == 10)
                    {
                        base.ShowMessage(TestRecommendation_topErrorMessageLabel, TestRecommendation_bottomErrorMessageLabel,
                            "Maximum limit reached");
                        break;
                    }
                }
            }
            else
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drNewRow))
                    drNewRow = null;
                drNewRow = dtTestSegment.NewRow();
                dtTestSegment.Rows.Add(drNewRow);
            }
            TestRecommendation_byQuestion_testSegmentGridView.DataSource = dtTestSegment;
            TestRecommendation_byQuestion_testSegmentGridView.DataBind();
        }

        private string GetSkillName(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return strKeyWord;
            return strKeyWord.Substring(0, strKeyWord.IndexOf('['));
        }

        private string GetSkillWeightage(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return string.Empty;
            return strKeyWord.Substring(strKeyWord.IndexOf('[') + 1, strKeyWord.Length - strKeyWord.IndexOf('[') - 2);
        }

        /// <summary>
        /// Method that checks if the every segment contains valid search criteria.
        /// </summary>
        /// <param name="segments">
        /// A <see cref="DataTable"/> that holds the segments.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True if valid and false
        /// otherwise.
        /// </returns>
        private bool CheckForEmptyValuesInSegmentGrid(DataTable segments)
        {
            bool isValid = true;
            int NoofQuestions = 0;
            StringBuilder sbErrorSegements = null;
            StringBuilder sbDeleteSegments = null;
            try
            {
                int.TryParse(TestRecommendation_byQuestion_totalNumberTextBox.Text.Trim(), out NoofQuestions);
                if (NoofQuestions == 0)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                        TestRecommendation_bottomErrorMessageLabel, "Enter valid number of questions");
                    isValid = false;
                    return isValid;
                }
                if (NoofQuestions < segments.Rows.Count)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                          TestRecommendation_bottomErrorMessageLabel,
                          Resources.HCMResource.CreateAutomaticTest_SegmentsGreaterthanSegments);
                    isValid = false;
                    return isValid;
                }
                int InvalidColumnCount = 0;
                int TotalColumns = 7;
                sbErrorSegements = new StringBuilder();
                sbDeleteSegments = new StringBuilder();
                for (int i = 0; i < segments.Rows.Count; i++)
                {
                    for (int ColumnCount = 1; ColumnCount <= TotalColumns; ColumnCount++)
                        if (segments.Rows[i][ColumnCount].ToString() == "")
                            InvalidColumnCount++;
                    if (InvalidColumnCount == TotalColumns)
                    {
                        sbErrorSegements.Append(i + 1);
                        sbErrorSegements.Append(",");
                        isValid = false;
                        if (i != 0)
                        {
                            sbDeleteSegments.Append(i + 1);
                            sbDeleteSegments.Append(",");
                        }
                    }
                    InvalidColumnCount = 0;
                }
                if (!isValid)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                        TestRecommendation_bottomErrorMessageLabel,
                        string.Format(Resources.HCMResource.CreateAutomaticTest_segmentEmpty,
                        (sbDeleteSegments.ToString().Length == 0) ?
                        sbErrorSegements.ToString().TrimEnd(',') :
                        sbErrorSegements.ToString().TrimEnd(',') + " or delete segement " + sbDeleteSegments.ToString().TrimEnd(',')));
                }
                return isValid;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbErrorSegements)) sbErrorSegements = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbDeleteSegments)) sbDeleteSegments = null;
            }
        }

        /// <summary>
        /// Deletes the user selected search segment from the grid
        /// </summary>
        /// <param name="RowIndex">Index of the segment to delete</param>
        private void DeleteTestSegmentRow(int RowIndex)
        {
            DataTable dtTestSegment = BuildTableFromGridView();
            if (dtTestSegment.Rows.Count == 1)
            {
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                    TestRecommendation_bottomErrorMessageLabel, Resources.HCMResource.CreateAutomaticTest_AllSegementsDelee);
                return;
            }
            dtTestSegment.Rows.RemoveAt(RowIndex);
            TestRecommendation_byQuestion_testSegmentGridView.DataSource = dtTestSegment;
            TestRecommendation_byQuestion_testSegmentGridView.DataBind();
        }

        /// <summary>
        /// This method do all validations for generate button clicks and 
        /// if all validations passed it will call approriate method for to 
        /// load test according to the user given serach criteria
        /// </summary>
        /// <param name="showQuestions">
        /// A <see cref="bool"/> status that indicates whether to show the 
        /// questions on the grid or not.
        /// </param>
        private bool LoadAutomatedQuestions(bool showQuestions)
        {
            DataTable searchCriteria = null;
            try
            {
                // Build search criteria.
                searchCriteria = BuildTableFromGridView();

                TestRecommendation_byQuestion_testSegmentGridView.DataSource = searchCriteria;
                TestRecommendation_byQuestion_testSegmentGridView.DataBind();

                // Check if every segment contains valid search criteria.
                if (!CheckForEmptyValuesInSegmentGrid(searchCriteria))
                    return false;

                // Load questions.
                LoadQuestions(Convert.ToInt32
                    (TestRecommendation_byQuestion_totalNumberTextBox.Text.Trim()), searchCriteria, showQuestions);

                return true;
            }
            finally
            {
                if (searchCriteria != null) searchCriteria = null;
            }
        }

        /// <summary>
        /// This method loads the user provided search criteria 
        /// in to collection of 'TestSearchCriteria' data object
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid data table</param>
        /// <returns>List of 'TestSearchCriteria' data object that contains
        /// user given search criteria</returns>
        private List<TestSearchCriteria> GetQuestionSearchCriteria(ref DataTable dtSearchCriteria)
        {
            List<TestSearchCriteria> testSearchCriterias = null;
            TestSearchCriteria testSearchCriteria = null;
            Subject subject = null;
            ////DataView SearchQuestionsOrderedDataView = null;
            ////DataTable TempDataTable = null;
            try
            {
                //SearchQuestionsOrderedDataView = dtSearchCriteria.DefaultView;
                //SearchQuestionsOrderedDataView.Sort = "ExpectedQuestions ASC";
                //TempDataTable = SearchQuestionsOrderedDataView.ToTable();
                for (int i = 0; i < dtSearchCriteria.Rows.Count; i++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria))
                        testSearchCriteria = new TestSearchCriteria();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Cat_sub_id"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(subject))
                            subject = new Subject();
                        subject.SubjectID = Convert.ToInt32(dtSearchCriteria.Rows[i]["Cat_sub_id"]);
                        testSearchCriteria.Subjects = new List<Subject>();
                        testSearchCriteria.Subjects.Add(subject);
                    }
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["TestArea"]))
                        testSearchCriteria.TestAreasID = dtSearchCriteria.Rows[i]["TestArea"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Complexity"]))
                        testSearchCriteria.Complexity = dtSearchCriteria.Rows[i]["Complexity"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Keyword"]))
                        testSearchCriteria.Keyword = dtSearchCriteria.Rows[i]["Keyword"].ToString();
                    testSearchCriteria.NoOfQuestions = Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"]);
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias))
                        testSearchCriterias = new List<TestSearchCriteria>();
                    testSearchCriterias.Add(testSearchCriteria);
                    testSearchCriteria = null;
                    subject = null;
                }
                return testSearchCriterias;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias)) testSearchCriterias = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(subject)) subject = null;
                //if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchQuestionsOrderedDataView)) SearchQuestionsOrderedDataView = null;
                //if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TempDataTable)) TempDataTable = null;
            }
        }

        /// <summary>
        /// This methods distributes the total questions to the segments
        /// according to the segment weightage 
        /// either given by user or system generated.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        private void LoadExpectedQuestions(ref DataTable dtSearchCriteria, int NoofQuestions, out bool zeroExpectedQuestion)
        {
            zeroExpectedQuestion = false;
            int SearchSegements = dtSearchCriteria.Rows.Count;
            int QuestionsAdded = 0;
            int ExpectedQuestions = 0;
            int LoopI = 0;
            for (LoopI = 0; LoopI < SearchSegements; LoopI++)
            {
                QuestionsAdded = Convert.ToInt32((Convert.ToDecimal(dtSearchCriteria.Rows[LoopI]["Weightage"])
                    / 100) * NoofQuestions);
                dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] = QuestionsAdded;
                ExpectedQuestions += QuestionsAdded;
            }
            if (ExpectedQuestions > NoofQuestions)
            {
                int QuestionsToAdd = ExpectedQuestions - NoofQuestions;
                for (LoopI = SearchSegements - 1; LoopI >= 0; LoopI--)
                {
                    if (Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd <= 0)
                        continue;
                    dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd;
                    break;
                }
            }
            else if (ExpectedQuestions < NoofQuestions)
                dtSearchCriteria.Rows[LoopI - 1]["ExpectedQuestions"] = QuestionsAdded +
                    (NoofQuestions - ExpectedQuestions);
            if (dtSearchCriteria.Select("ExpectedQuestions <= 0").Length > 0)
            {
                base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                         TestRecommendation_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticTest_ExpectedQuestionZero);
                zeroExpectedQuestion = true;
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                         TestRecommendation_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                    dtSearchCriteria.Rows[LoopI]["PickedQuestions"] = 0;
                    dtSearchCriteria.Rows[LoopI]["TotalRecordsinDB"] = 0;
                    dtSearchCriteria.Rows[LoopI]["QuestionsDifference"] = 0;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// Method that retrieves the question based on the given search criteria.
        /// </summary>
        /// <param name="noOfQuestions">
        /// A <see cref="int"/> that holds the number of questions.
        /// </param>
        /// <param name="searchCriteria">
        /// A <see cref="DataTable"/> that holds the search criteria.
        /// </param>
        /// <param name="showQuestions">
        /// A <see cref="bool"/> status that indicates whether to show the 
        /// questions on the grid or not.
        /// </param>
        private void LoadQuestions(int noOfQuestions, DataTable searchCriteria, bool showQuestions)
        {
            int TotalWeightage = 0;
            bool zeroExpectedQuestion = false;
            LoadWeightages(ref searchCriteria, noOfQuestions, out TotalWeightage);

            if (showQuestions)
            {
                TestRecommendation_byQuestion_testSegmentGridView.DataSource = searchCriteria;
                TestRecommendation_byQuestion_testSegmentGridView.DataBind();
            }

            if (TotalWeightage > 100)
                return;

            // Load expected questions.
            LoadExpectedQuestions(ref searchCriteria, noOfQuestions, out zeroExpectedQuestion);
            if (zeroExpectedQuestion)
                return;

            List<QuestionDetail> questionDetails = new QuestionBLManager().
                GetAutomatedQuestions(QuestionType.MultipleChoice, GetQuestionSearchCriteria(ref searchCriteria),
                noOfQuestions, ref searchCriteria, 0, base.userID);

            if (questionDetails == null || questionDetails.Count == 0)
            {
                TestRecommendation_byQuestion_testDrftDiv.Visible = false;
                TestRecommendation_byQuestion_automatedTestUserControl.Visible = false;
                TestRecommendation_byQuestion_generateTestButton.Visible = false;
                TestRecommendation_byQuestion_generateTestButton.Text = "Generate Sample Test";
                TestRecommendation_byQuestion_generateTestHitCountHiddenField.Value = "0";
                SetEnableStatusForSearchControls(true);
                SetVisibleStatusForButton(false);

                if (showQuestions)
                {
                    TestRecommendation_byQuestion_testSegmentGridView.DataSource = searchCriteria;
                    TestRecommendation_byQuestion_testSegmentGridView.DataBind();
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                        TestRecommendation_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                }
            }
            else
            {
                ViewState[TestRecommendation_QUESTIONVIEWSTATE] = questionDetails;
                TestStatistics testStatistics = AutomatedSummaryList(questionDetails);
                LoadTestStatisticsObject(ref questionDetails, ref testStatistics);

                if (showQuestions)
                {
                    TestRecommendation_byQuestion_automatedTestUserControl.DataSource = testStatistics;
                    TestRecommendation_byQuestion_automatedTestUserControl.Visible = true;
                    TestRecommendation_byQuestion_testDrftDiv.Visible = true;
                }

                /*
                if (noOfQuestions == questionDetails.Count)
                {
                    TestRecommendation_byQuestion_generateTestButton.Text = "Generate Sample Test";
                    TestRecommendation_byQuestion_generateTestHitCountHiddenField.Value =
                        (Convert.ToInt32(TestRecommendation_byQuestion_generateTestHitCountHiddenField.Value) + 1).ToString();
                    
                    if (Convert.ToInt32(TestRecommendation_byQuestion_generateTestHitCountHiddenField.Value) > 0)
                        SetEnableStatusForSearchControls(true);
                }
                else
                {
                    TestRecommendation_byQuestion_generateTestButton.Text = "Generate Sample Test";
                    TestRecommendation_byQuestion_generateTestHitCountHiddenField.Value = "0";
                        SetEnableStatusForSearchControls(true);
                }
                */
                if (showQuestions)
                {
                    TestRecommendation_byQuestion_testDraftGridView.DataSource = questionDetails;
                    TestRecommendation_byQuestion_testDraftGridView.DataBind();
                    TestRecommendation_byQuestion_testSegmentGridView.DataSource = searchCriteria;
                    TestRecommendation_byQuestion_testSegmentGridView.DataBind();
                }
            }

            if (showQuestions)
            {
                if (ViewState[TestRecommendation_QUESTIONVIEWSTATE] != null)
                {
                    if (Convert.ToInt32(noOfQuestions) > ((List<QuestionDetail>)ViewState[TestRecommendation_QUESTIONVIEWSTATE]).Count)
                    {
                        base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                            TestRecommendation_bottomErrorMessageLabel,
                            "There is lack of questions present in the repository for the given test segments. The system is unable to pick the expected number of questions.");
                        return;
                    }
                }
            }
        }

        private void SetEnableStatusForSearchControls(bool VisibilityStatus)
        {
            TestRecommendation_byQuestion_totalNumberTextBox.Enabled = VisibilityStatus;
            TestRecommendation_byQuestion_testNameTextBox.Enabled = VisibilityStatus;
            TestRecommendation_byQuestion_testSkillTextBox.Enabled = VisibilityStatus;
            TestRecommendation_byQuestion_timeSearchTextbox.Enabled = VisibilityStatus;
            TestRecommendation_byQuestion_addSegmantLinkButton.Visible = VisibilityStatus;
        }

        /// <summary>
        /// This method will load all the test statistics user control data.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object.</param>
        private void LoadTestStatisticsObject(ref List<QuestionDetail> questionDetails, ref TestStatistics testStatistics)
        {
            TestDetail testDetail = null;
            try
            {
                testStatistics.NoOfQuestions = questionDetails.Count;
                testStatistics.TestCost = 0;
                for (int i = 0; i < questionDetails.Count; i++)
                    testStatistics.TestCost += questionDetails[i].CreditsEarned;
                if (testStatistics.TestCost == 0)
                    testStatistics.TestCost = 0.00M;
                testStatistics.AverageTimeTakenByCandidates =
                    Convert.ToInt32(questionDetails.Average(p => p.AverageTimeTaken)) * questionDetails.Count;
                testStatistics.AutomatedTestAverageComplexity = ((AttributeDetail)
                    (new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetails))).
                    AttributeName;
                LoadChartDetails(ref questionDetails, ref testStatistics);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        /// <summary>
        /// This method loads the chart details to the test statistics object.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object by adding chart controls data.</param>
        private void LoadChartDetails(ref List<QuestionDetail> questionDetails,
            ref TestStatistics testStatistics)
        {
            ChartData chartData = null;
            List<ChartData> testAreaChartDatum = null;
            List<ChartData> complexityChartDatum = null;
            try
            {
                var complexityGroups =
                                          from q in questionDetails
                                          group q by q.ComplexityName into g
                                          select new { ComplexityName = g.Key, ComplexityCount = g.Count() };
                complexityChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var complexity in complexityGroups)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = complexity.ComplexityName;
                    chartData.ChartYValue = complexity.ComplexityCount;
                    complexityChartDatum.Add(chartData);
                }
                chartData = null;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.ComplexityStatisticsChartData))
                    testStatistics.ComplexityStatisticsChartData = new SingleChartData();
                testStatistics.ComplexityStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.ComplexityStatisticsChartData.ChartData = complexityChartDatum;
                testStatistics.ComplexityStatisticsChartData.ChartLength = 140;
                testStatistics.ComplexityStatisticsChartData.ChartWidth = 300;
                testStatistics.ComplexityStatisticsChartData.ChartTitle = "Complexity Statistics";
                var testAreaCounts =
                        from t in questionDetails
                        group t by t.TestAreaName into g
                        select new { TestAreaName = g.Key, TestAreaCOunt = g.Count() };
                testAreaChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var testArea in testAreaCounts)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = testArea.TestAreaName;
                    chartData.ChartYValue = testArea.TestAreaCOunt;
                    testAreaChartDatum.Add(chartData);
                }
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.TestAreaStatisticsChartData))
                    testStatistics.TestAreaStatisticsChartData = new SingleChartData();
                testStatistics.TestAreaStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.TestAreaStatisticsChartData.ChartData = testAreaChartDatum;
                testStatistics.TestAreaStatisticsChartData.ChartLength = 140;
                testStatistics.TestAreaStatisticsChartData.ChartWidth = 300;
                testStatistics.TestAreaStatisticsChartData.ChartTitle = "Test Area Statistics";
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(chartData)) chartData = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testAreaChartDatum)) testAreaChartDatum = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(complexityChartDatum)) complexityChartDatum = null;
            }
        }

        /// <summary>
        /// This Method gives the summary of the result questions.
        /// Summary will be like 
        /// Category Name, subject, test area, complexity, total questions
        /// </summary>
        /// <param name="ResultQuestionDetails">Question List that o/p to user</param>
        /// <returns>List of strings with each string contains
        /// category & subject & Test Area & Complexity & 
        /// Toal Number of Questions shown to the user</returns>
        private TestStatistics AutomatedSummaryList(List<QuestionDetail> ResultQuestionDetails)
        {
            StringBuilder CategoryNames = new StringBuilder();
            AutomatedTestSummaryGrid automatedTestSummaryGrid = null;
            TestStatistics testStatistics = null;
            List<AutomatedTestSummaryGrid> automatedTestSummaryGridOrderdByQuestion = null;
            int TotalCategory = 0;
            try
            {
                for (int i = 0; i < ResultQuestionDetails.Count; i++)
                {
                    if (CategoryNames.ToString().IndexOf("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() + " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() + " COMP: " + ResultQuestionDetails[i].Complexity.Trim()) >= 0)
                        continue;
                    TotalCategory = ResultQuestionDetails.FindAll(
                        p => "CAT: " + p.CategoryName.Trim() + " SUB: " + p.SubjectName.Trim() +
                    " TA: " + p.TestAreaName.Trim() +
                    " COMP: " + p.Complexity.Trim() == "CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim()).Count;
                    CategoryNames.Append("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim());
                    CategoryNames.Append(",");
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                        automatedTestSummaryGrid = new AutomatedTestSummaryGrid();
                    automatedTestSummaryGrid.CategoryName = ResultQuestionDetails[i].CategoryName;
                    automatedTestSummaryGrid.SubjectName = ResultQuestionDetails[i].SubjectName;
                    automatedTestSummaryGrid.TestAreaName = ResultQuestionDetails[i].TestAreaName;
                    automatedTestSummaryGrid.Complexity = ResultQuestionDetails[i].ComplexityName;
                    automatedTestSummaryGrid.NoofQuestionsInCategory = TotalCategory;
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics))
                        testStatistics = new TestStatistics();
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                        testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                    testStatistics.AutomatedTestSummaryGrid.Add(automatedTestSummaryGrid);
                    automatedTestSummaryGrid = null;
                    TotalCategory = 0;
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                {
                    automatedTestSummaryGridOrderdByQuestion = testStatistics.AutomatedTestSummaryGrid.OrderByDescending(p => p.NoofQuestionsInCategory).ToList();
                    testStatistics.AutomatedTestSummaryGrid = null;
                    testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                    testStatistics.AutomatedTestSummaryGrid = automatedTestSummaryGridOrderdByQuestion;
                }
                testStatistics.NoOfQuestions = ResultQuestionDetails.Count;
                return testStatistics;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CategoryNames)) CategoryNames = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics)) testStatistics = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                    automatedTestSummaryGrid = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGridOrderdByQuestion))
                    automatedTestSummaryGridOrderdByQuestion = null;
            }
        }

        /// <summary>
        /// Gets the test area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetTestAreaList()
        {
            return new AttributeBLManager().GetTestAreas(base.tenantID);//GetAttributesByType(Constants.AttributeTypes.TEST_AREA,.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Gets the complexity area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetComplexityList()
        {
            return new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY,
                Constants.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestRecommendation_byQuestion_isMaximizedHiddenField.Value) &&
                         TestRecommendation_byQuestion_isMaximizedHiddenField.Value == "Y")
            {
                TestRecommendation_byQuestion_serachDiv.Style["display"] = "none";
                TestRecommendation_byQuestion_searchResultsUpSpan.Style["display"] = "block";
                TestRecommendation_byQuestion_searchResultsDownSpan.Style["display"] = "none";
                TestRecommendation_byQuestion_testDrftDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TestRecommendation_byQuestion_serachDiv.Style["display"] = "block";
                TestRecommendation_byQuestion_searchResultsUpSpan.Style["display"] = "none";
                TestRecommendation_byQuestion_searchResultsDownSpan.Style["display"] = "block";
                TestRecommendation_byQuestion_testDrftDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Load all javascript tags
        /// </summary>
        private void SubscribeClientSideHandlers()
        {
            TestRecommendation_byQuestion_searchResultsUpSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                TestRecommendation_byQuestion_testDrftDiv.ClientID + "','" +
                TestRecommendation_byQuestion_serachDiv.ClientID + "','" +
                TestRecommendation_byQuestion_searchResultsUpSpan.ClientID + "','" +
                TestRecommendation_byQuestion_searchResultsDownSpan.ClientID + "','" +
                TestRecommendation_byQuestion_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
            TestRecommendation_byQuestion_searchResultsDownSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                TestRecommendation_byQuestion_testDrftDiv.ClientID + "','" +
                TestRecommendation_byQuestion_serachDiv.ClientID + "','" +
                TestRecommendation_byQuestion_searchResultsUpSpan.ClientID + "','" +
                TestRecommendation_byQuestion_searchResultsDownSpan.ClientID + "','" +
                TestRecommendation_byQuestion_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
        }

        /// <summary>
        /// Sets the Visibility Status for save and create session buttons.
        /// </summary>
        /// <param name="VisibilityStatus">Visibility status for those button <br />
        /// Note(:- Save button Visibile status show with 'Not' gate)</param>
        private void SetVisibleStatusForButton(bool VisibilityStatus)
        {
            TestRecommendation_bottomSaveButton.Visible = !VisibilityStatus;
            TestRecommendation_topSaveButton.Visible = !VisibilityStatus;
            TestRecommendation_byQuestion_generateTestButton.Visible = !VisibilityStatus;
        }

        /// <summary>
        /// This method enables or disables test segment grid controls
        /// according to the search status.
        /// </summary>
        /// <param name="RowIndex">Row index of the grid to disable controls</param>
        private void EnableOrDisableTestSegmentGridControls(GridViewRow CurrenRowBounding)
        {
            if (TestRecommendation_byQuestion_generateTestHitCountHiddenField.Value == "")
                return;
            if (Convert.ToInt32(TestRecommendation_byQuestion_generateTestHitCountHiddenField.Value) == 0)
                return;
            ((ImageButton)
                CurrenRowBounding.FindControl("TestRecommendation_byQuestion_testSegmentGridView_deleteImageButton")).Visible = false;
            ((ImageButton)CurrenRowBounding
                .FindControl("TestRecommendation_byQuestion_categoryImageButton")).Visible = false;
            ((TextBox)CurrenRowBounding.
                FindControl("TestRecommendation_byQuestion_weightageTextBox")).Enabled = false;
            ((TextBox)CurrenRowBounding.
                FindControl("TestRecommendation_byQuestion_keywordTextBox")).Enabled = false;
            ((CheckBoxList)CurrenRowBounding.
                FindControl("TestRecommendation_byQuestion_complexityCheckBoxList")).Enabled = false;
            ((CheckBoxList)CurrenRowBounding.
                FindControl("TestRecommendation_byQuestion_testAreaCheckBoxList")).Enabled = false;
        }


        private DataTable FilledSegmants(ref DataTable dtTestSegment)
        {
            int InvalidColumnCount = 0;
            int TotalColumns = 7;
            for (int i = 0; i < dtTestSegment.Rows.Count; i++)
            {
                for (int ColumnCount = 1; ColumnCount <= TotalColumns; ColumnCount++)
                    if (dtTestSegment.Rows[i][ColumnCount].ToString() == "")
                        InvalidColumnCount++;
                if (InvalidColumnCount == TotalColumns)
                {
                    dtTestSegment.Rows.Remove(dtTestSegment.Rows[i]);
                }
                InvalidColumnCount = 0;
            }
            return dtTestSegment;
        }


        #endregion Private Methods

        #region Protected Overriden Methods                                    

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            ///bool FirstTabPriority = false;
            int noOfQuestions = 0;
            List<QuestionDetail> questionDetailTestDraftResultList = null;
            try
            {
                // Check if no of question is valid or not.
                int.TryParse(TestRecommendation_byQuestion_totalNumberTextBox.Text.Trim(), out noOfQuestions);
                if (noOfQuestions == 0)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                        TestRecommendation_bottomErrorMessageLabel, "Number of questions cannot be empty");
                    isValidData = false;
                }

                // Check if no of question is within the min and max limit.
                if (noOfQuestions != 0 && (noOfQuestions < base.minQuestionPerSelfAdminTest || noOfQuestions > base.maxQuestionPerSelfAdminTest))
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                        TestRecommendation_bottomErrorMessageLabel, string.Format("Number of questions must be within the range {0} - {1}", 
                        base.minQuestionPerSelfAdminTest , base.maxQuestionPerSelfAdminTest));
                    isValidData = false;
                }

                if (TestRecommendation_byQuestion_testNameTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                       TestRecommendation_bottomErrorMessageLabel,
                       "Test name cannot be empty");
                    isValidData = false;
                }

                if (TestRecommendation_byQuestion_testSkillTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                       TestRecommendation_bottomErrorMessageLabel, "Test skill cannot be empty");
                    isValidData = false;
                }

                if (TestRecommendation_testDescriptionTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                       TestRecommendation_bottomErrorMessageLabel, "Test description cannot be empty");
                    isValidData = false;
                }

                DateTime timeLimit;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(TestRecommendation_byQuestion_timeSearchTextbox.Text.Trim()))
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                       TestRecommendation_bottomErrorMessageLabel, "Time limit cannot be empty");
                    isValidData = false;
                }
                else if (DateTime.TryParse(TestRecommendation_byQuestion_timeSearchTextbox.Text, out timeLimit) == false)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                       TestRecommendation_bottomErrorMessageLabel, "Time limit must be in a valid hh:mm format");
                    isValidData = false;
                }
                else if (Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(TestRecommendation_byQuestion_timeSearchTextbox.Text) < Constants.General.DEFAULT_TEST_MINIMUM_TIME)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                       TestRecommendation_bottomErrorMessageLabel, "Time limit must be greater than or equal to 5 minutes and less than 24 hrs");
                    isValidData = false;
                }

                /*
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                questionDetailTestDraftResultList = ViewState[TestRecommendation_QUESTIONVIEWSTATE] as List<QuestionDetail>;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                        TestRecommendation_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateAutomaticTest_QuestionsEmpty);
                    isValidData = false;
                }
                else if (questionDetailTestDraftResultList.Count == 0)
                {
                    base.ShowMessage(TestRecommendation_topErrorMessageLabel,
                        TestRecommendation_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateManualTest_QuestionCount);
                    isValidData = false;
                }
                */
                return isValidData;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList)) questionDetailTestDraftResultList = null;
            }
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            SetVisibleStatusForButton(false);
            SubscribeClientSideHandlers();
            TestRecommendation_byQuestion_automatedTestUserControl.Visible = false;
            BindEmptyTestSegmentGrid();
        }

      #endregion Protected Overriden Methods

    }
}