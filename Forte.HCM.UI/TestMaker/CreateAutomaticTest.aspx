<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateAutomaticTest.aspx.cs"
    Inherits="Forte.HCM.UI.TestMaker.CreateAutomaticTest" MasterPageFile="~/MasterPages/OTMMaster.Master" %>

<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/AutomatedTestSummaryControl.ascx" TagName="AutomatedTestControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/TestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc7" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
      <%@ Register Src="../CommonControls/QuestionDetailPreviewControlOpenText.ascx" TagName="QuestionDetailPreviewControlOpenText"
    TagPrefix="uc5" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ContentPlaceHolderID="OTMMaster_body" ID="CreateAutomaticTest_bodyContent"
    runat="server">

    <script src="../JS/ChartScript.js" type="text/javascript"></script>
     <script type="text/javascript" language="javascript">

         var control;
         function setFocus(sender, e) {
             try {
                 var activeTab = $find('<%=CreateAutomaticTest_mainTabContainer.ClientID%>').get_activeTabIndex();
                 if (activeTab == "0") {
                     try {
                         control = $get('<%=CreateAutomaticTest_byQuestion_totalNumberTextBox.ClientID%>');
                         control.focus();
                     }
                     catch (er1) {

                     }
                 }
                 else {
                     control = $get('ctl00_OTMMaster_body_CreateAutomaticTest_mainTabContainer_CreateManualTest_testdetailsTabPanel_CreateAutomaticTest_byQuestion_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                     control.focus();
                 }
             }
             catch (er) {
             }
         }

    </script>
    <script type="text/javascript" language="javascript">

        // Expand or Collapse question details in gridview
        function ExpORCollapseRows(targetControl) {
            var ctrl = document.getElementById('<%=CreateAutomaticTest_byQuestion_stateHiddenField.ClientID %>');
            targetValue = ctrl.value;
            if (targetValue == "0") {
                targetControl.innerHTML = "Collapse All";
                ExpandAllRows(targetControl);
                ctrl.value = 1;
            }
            else {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }

            return false;
        }

        // Expands all question details in test draft gridview
        function ExpandAllRows(targetControl) {
            var questionType = document.getElementById("<%= CreateAutomaticTest_byQuestion_questionTypeDropDownList.ClientID %>");
            var selectedValue = questionType.value;            

            var gridCtrl = document.getElementById("<%= CreateAutomaticTest_byQuestion_testDraftGridView.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (selectedValue == "1") {
                        if (rowItems[indexRow].id.indexOf("CreateAutomaticTest_byQuestion_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "block";
                        }
                    }
                    else {
                        if (rowItems[indexRow].id.indexOf("CreateAutomaticTest_byQuestionOpenText_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "block";
                        }
                    }
                }
            }
            return false;
        }

        // Collpase all question details in test draft gridview
        function CollapseAllRows(targetControl) {
            var questionType = document.getElementById("<%= CreateAutomaticTest_byQuestion_questionTypeDropDownList.ClientID %>");
            var selectedValue = questionType.value;        

            var gridCtrl = document.getElementById("<%= CreateAutomaticTest_byQuestion_testDraftGridView.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (selectedValue == "1") {
                        if (rowItems[indexRow].id.indexOf("CreateAutomaticTest_byQuestion_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "none";
                        }
                    }
                    else {
                        if (rowItems[indexRow].id.indexOf("CreateAutomaticTest_byQuestionOpenText_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "none";
                        }
                    }
                }
            }
            return false;
        }

        function ShowZoomedChart(sessionName) 
        {
            var height = 620;
            var width = 950;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/ZoomedChart.aspx?sessionName=" + sessionName;
            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }
    </script>

    <asp:Panel ID="CreateAutomaticText_mainPanel" runat="server" DefaultButton="CreateAutomaticTest_bottomSaveButton">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="CreateAutomaticTest_headerLiteral" runat="server" Text="Create Automated Test"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="63%" align="right">
                                        <asp:Button ID="CreateAutomaticTest_topCreateSessionButton" runat="server" SkinID="sknButtonId"
                                            Text="Create Session" OnClick="CreateAutomaticTest_CreateSessionButton_Click" />
                                        <asp:Button ID="CreateAutomaticTest_topSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="CreateAutomaticTest_saveButton_Click" />
                                    </td>
                                    <td align="right" style="width: 15%">
                                        <asp:LinkButton ID="CreateAutomaticTest_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="CreateAutomaticTest_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="CreateAutomaticTest_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateAutomaticTest_topMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateAutomaticTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateAutomaticTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolKit:TabContainer ID="CreateAutomaticTest_mainTabContainer" runat="server"
                    ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                    <ajaxToolKit:TabPanel ID="CreateAutomaticTest_QuestionsTabPanel" HeaderText="questions"
                        runat="server">
                        <HeaderTemplate>
                            Questions</HeaderTemplate>
                        <ContentTemplate>
                        <asp:Panel ID="CreaetAutomaticTest_questionPanel" runat="server" DefaultButton="CreateAutomaticTest_byQuestion_generateTestButton">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 100%" class="panel_bg">
                                        <asp:HiddenField ID="CreaetAutomaticTest_byQuestion_testKeyHiddenField" runat="server" />
                                        <div id="CreateAutomaticTest_byQuestion_serachDiv" runat="server" style="display: block;
                                            width: 100%">
                                            <asp:UpdatePanel ID="CreateAutomaticTest_byQuestion_searchCriteriaUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="panel_inner_body_bg" style="width: 100%">
                                                                <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_totalNumberLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Number Of Questions"></asp:Label><span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 10%">
                                                                            <asp:TextBox ID="CreateAutomaticTest_byQuestion_totalNumberTextBox" runat="server"
                                                                                MaxLength="3" Width="25%"></asp:TextBox>
                                                                            <ajaxToolKit:FilteredTextBoxExtender ID="CreateAutomaticTest_byQuestion_numberOfQuestionsFileteredExtender"
                                                                                runat="server" TargetControlID="CreateAutomaticTest_byQuestion_totalNumberTextBox"
                                                                                FilterType="Numbers">
                                                                            </ajaxToolKit:FilteredTextBoxExtender>
                                                                             <asp:HiddenField ID="CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField"
                                                                                runat="server" Value="0" />
                                                                        </td>
                                                                         <td style="width: 10%">
                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_questionTypeLabel" runat="server" Text="Question Type" 
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                         <asp:DropDownList ID="CreateAutomaticTest_byQuestion_questionTypeDropDownList" SkinID="sknQuestionTypeDropDown"  runat="server" AutoPostBack="false" EnableViewState="true">
                                                                            <asp:ListItem Text="Multiple Choice" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="Open Text" Value="2"></asp:ListItem>
                                                                          </asp:DropDownList>
                                                                        </td>
                                                                      <%--  <td style="width: 4%; display: none" align="center">
                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_totalCostLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Cost" Visible = "false"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%; display: none">
                                                                            <asp:TextBox ID="CreateAutomaticTest_byQuestion_totalCostTextBox" runat="server" Visible ="false"></asp:TextBox>
                                                                            <ajaxToolKit:FilteredTextBoxExtender ID="CreateAutomaticTest_byQuestion_totalCostFilteredTextBoxExtender"
                                                                                runat="server" TargetControlID="CreateAutomaticTest_byQuestion_totalCostTextBox"
                                                                                FilterType="Custom" ValidChars="1234567890.1234567890">
                                                                            </ajaxToolKit:FilteredTextBoxExtender>
                                                                           
                                                                        </td>--%>
                                                                        <td style="width: 14%" align="center">
                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_positionProfileLabel" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Position Profile"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 35%">
                                                                            <div style="float: left; padding-right: 5px; width:70%">
                                                                                <asp:TextBox ID="CreateAutomaticTest_byQuestion_positionProfileTextBox" MaxLength="50"
                                                                                    ReadOnly="true" runat="server" Columns="28"></asp:TextBox>
                                                                                <asp:HiddenField ID="CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField"
                                                                                    runat="server" />
                                                                                <asp:HiddenField ID="CreateAutomaticTest_byQuestion_positionProfileIDHiddenField"
                                                                                    runat="server" />
                                                                            </div>
                                                                            <div style="float: left; width: 20%;">
                                                                                <table cellpadding="2" cellspacing="0" border="0" style="width:100%">
                                                                                    <tr>
                                                                                        <td style="width: 40%">
                                                                                            <asp:ImageButton ID="CreateAutomaticTest_byQuestion_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" />
                                                                                        </td>
                                                                                        <td style="width: 60%">
                                                                                            <asp:LinkButton ID="SearchCategorySubjectControl_addLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                                                Text="Add" ToolTip="Click here to add position profile segments" OnClick="CreateAutomaticTest_byQuestion_addPositionProfileLinkButton_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 4%">
                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_timeSearchLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Time"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%">
                                                                            <asp:TextBox ID="CreateAutomaticTest_byQuestion_timeSearchTextbox" runat="server"
                                                                                Columns="10"></asp:TextBox>
                                                                            <ajaxToolKit:MaskedEditExtender ID="CreateAutomaticTest_byQuestion_timeSearchMaskedEditExtender"
                                                                                runat="server" TargetControlID="CreateAutomaticTest_byQuestion_timeSearchTextbox"
                                                                                Mask="99:99:99" MaskType="Time" ErrorTooltipEnabled="True" CultureAMPMPlaceholder=""
                                                                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                                                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                                                Enabled="True">
                                                                            </ajaxToolKit:MaskedEditExtender>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="Td3" class="header_bg" runat="server">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 80%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="CreateAutomaticTest_byQuestion_testSegmentsLiteral" runat="server"
                                                                                Text="Test Segments"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%" class="grid_body_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="width: 100%; overflow: auto; display: block; background-color: Red">
                                                                                <asp:GridView ID="CreateAutomaticTest_byQuestion_testSegmantGridView" runat="server"
                                                                                    AutoGenerateColumns="False" ShowHeader="False" SkinID="sknNewGridView" OnRowDataBound="CreateAutomaticTest_byQuestion_testSegmantGridView_RowDataBound"
                                                                                    AutoGenerateDeleteButton="false">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <table width="100%" border="0" cellspacing="2" cellpadding="5">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:ImageButton ID="CreateAutomaticTest_byQuestion_testSegmantGridView_deleteImageButton"
                                                                                                                runat="server" CommandName="Delete Segment" SkinID="sknDeleteImageButton" ToolTip="Delete segment"
                                                                                                                OnClick="CreateAutomaticTest_byQuestion_testSegmantGridView_deleteImageButton_Click" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div style="float: left; width: 92%;" class="grouping_border_bg">
                                                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 15%">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_categoryLabel" runat="server" Text="Category"
                                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 48%; padding-left: 13px">
                                                                                                                            <asp:TextBox ID="CreateAutomaticTest_byQuestion_categoryTextBox" ReadOnly="true"
                                                                                                                                runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 20%">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_subjectLabel" runat="server" Text="Subject"
                                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                            <asp:HiddenField ID="CreateAutomaticTest_byQuestion_categoryHiddenField" runat="server"
                                                                                                                                Value='<%# Eval("Cat_sub_id") %>' />
                                                                                                                            <asp:HiddenField ID="CreateAutomaticTest_byQuestion_categoryNameHiddenField" runat="server"
                                                                                                                                Value='<%# Eval("Category") %>' />
                                                                                                                            <asp:HiddenField ID="CreateAutomaticTest_byQuestion_subjectNameHiddenField" runat="server"
                                                                                                                                Value='<%# Eval("Subject") %>' />
                                                                                                                        </td>
                                                                                                                        <td style="width: 30%">
                                                                                                                            <asp:TextBox ID="CreateAutomaticTest_byQuestion_subjectTextBox" ReadOnly="true" runat="server"
                                                                                                                                Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                            <div style="float: left; padding-left: 10px; padding-top: 5px;">
                                                                                                                <asp:ImageButton ID="CreateAutomaticTest_byQuestion_categoryImageButton" SkinID="sknbtnSearchicon"
                                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td colspan="2">
                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_weightageHeadLabel" runat="server"
                                                                                                                Text="Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            <asp:TextBox ID="CreateAutomaticTest_byQuestion_weightageTextBox" runat="server"
                                                                                                                Width="8%" MaxLength="3" Text='<%# Eval("Weightage") %>'>
                                                                                                            </asp:TextBox>
                                                                                                            <ajaxToolKit:FilteredTextBoxExtender ID="CreateAutomaticTest_byQuestion_weightageFileteredExtender"
                                                                                                                runat="server" TargetControlID="CreateAutomaticTest_byQuestion_weightageTextBox"
                                                                                                                FilterType="Numbers">
                                                                                                            </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="1">
                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_testAreaLabel" runat="server" Text="Test Area"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td colspan="3" align="left">
                                                                                                            <div class="checkbox_list_bg" style="padding-top:5px;width: 100%; height: 60px; overflow: auto">
                                                                                                                <asp:CheckBoxList ID="CreateAutomaticTest_byQuestion_testAreaCheckBoxList" runat="server"
                                                                                                                    RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="100%" TextAlign="Right">
                                                                                                                </asp:CheckBoxList>
                                                                                                                <asp:HiddenField ID="CreateAutomaticTest_byQuestion_testAreaHiddenField" runat="server"
                                                                                                                    Value='<%# Eval("TestArea") %>' />
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 25px;">
                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_rowSegmentLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                                runat="server" Text="Segment">
                                                                                                            </asp:Label>&nbsp;<asp:Label ID="CreateAutomaticTest_byQuestion_rowIdLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                                runat="server" Text='<%# Container.DataItemIndex + 1 %>'>
                                                                                                            </asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left" style="width:450px;">
                                                                                                            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td style="width: 10%;">
                                                                                                                        <asp:Label ID="CreateAutomaticTest_byQuestion_complexityLabel" runat="server" Text="Complexity"
                                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td align="left" style="width: 90%;">
                                                                                                                        <div class="checkbox_list_bg" style="height: 33px; overflow: auto;">
                                                                                                                            <asp:CheckBoxList ID="CreateAutomaticTest_byQuestion_complexityCheckBoxList" runat="server"
                                                                                                                                RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="75%">
                                                                                                                            </asp:CheckBoxList>
                                                                                                                            <asp:HiddenField ID="CreateAutomaticText_byQuestion_complexityHiddenField" runat="server"
                                                                                                                                Value='<%# Eval("Complexity") %>' />
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <td colspan="2" style="width:150px;">
                                                                                                            <table border="0" cellpadding="2" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="CreateAutomaticTest_byQuestion_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <div style="float: left">
                                                                                                                            <asp:TextBox ID="CreateAutomaticTest_byQuestion_keywordTextBox" runat="server" Columns="35"
                                                                                                                                MaxLength="100" Text='<%# Eval("Keyword") %>'></asp:TextBox>
                                                                                                                        </div>
                                                                                                                        <div style="float: left; padding-left: 5px">
                                                                                                                            <asp:ImageButton ID="CreateAutomaticTest_byQuestion_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="td_height_2">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="4">
                                                                                                            <div id="CreateAutomaticTest_byQuestion_expectedQuestionsDiv" runat="server" visible="false"
                                                                                                                style="width: 100%;">
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 15%;">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_expectedQuestionsHeadLabel" runat="server"
                                                                                                                                Text="Expected Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%;">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_expecetdQuestions" runat="server" Text='<%# Eval("ExpectedQuestions") %>'
                                                                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 15%;">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestions_returnQuestionsHeadLabel" runat="server"
                                                                                                                                Text="Picked Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%;">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestions_returnQuestionsLabel" runat="server"
                                                                                                                                Text='<%# Eval("PickedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 8%;">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_remarkHeadLabel" runat="server" Text="Remarks"
                                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestin_remarkLabel" runat="server" Text='<%# Eval("Remarks") %>'
                                                                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_noOfQuestionsInDBHeadLabel" runat="server"
                                                                                                                                Text="Total Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_noOfQuestionsLabel" runat="server"
                                                                                                                                Text='<%# Eval("TotalRecordsinDB") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="padding-left: 1%">
                                                                <asp:LinkButton ID="CreateAutomaticTest_byQuestion_addSegmantLinkButton" SkinID="sknAddLinkButton"
                                                                    runat="server" Text="Add" ToolTip="Add new segment" OnClick="CreateAutomaticTest_byQuestion_addSegmantLinkButton_Click"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="CreateAutomaticTest_byQuestion_generateTestButton" runat="server"
                                                                    Text="Generate Test" SkinID="sknButtonId" CausesValidation="False" OnClick="CreateAutomaticTest_byQuestion_generateTestButton_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr id="CreateAutomaticTest_byQuestion_searchTestResultsTR" runat="server">
                                    <td class="header_bg" width="100%" runat="server">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%;" align="left" class="header_text_bold">
                                                </td>
                                                <td style="width: 48%" align="left">
                                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="CreateManualTest_byQuestion_testDraftExpandLinkButton" runat="server"
                                                                    Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseRows(this);"></asp:LinkButton>
                                                                <asp:HiddenField ID="CreateAutomaticTest_byQuestion_stateHiddenField" runat="server"
                                                                    Value="0" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 2%" align="right">
                                                    <span id="CreateAutomaticTest_byQuestion_searchResultsUpSpan" runat="server" style="display: none;">
                                                        <asp:Image ID="CreateAutomaticTest_byQuestion_searchResultsUpImage" runat="server"
                                                            SkinID="sknMinimizeImage" />
                                                    </span><span id="CreateAutomaticTest_byQuestion_searchResultsDownSpan" runat="server"
                                                        style="display: block;">
                                                        <asp:Image ID="CreateAutomaticTest_byQuestion_searchResultsDownImage" runat="server"
                                                            SkinID="sknMaximizeImage" />
                                                    </span>
                                                    <asp:HiddenField ID="CreateAutomaticTest_byQuestion_isMaximizedHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="CreateManualTest_draftTr" style="width: 100%" runat="server">
                                    <td id="Td2" class="grid_body_bg" runat="server">
                                        <asp:UpdatePanel ID="CreateAutomaticTest_byQuestion_updatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="width: 100%; overflow: auto; height: 300px;" runat="server" id="CreateAutomaticTest_byQuestion_testDrftDiv"
                                                    visible="False">
                                                    <asp:GridView ID="CreateAutomaticTest_byQuestion_testDraftGridView" OnRowDataBound="CreateAutomaticTest_byQuestion_testDraftGridView_RowDataBound"
                                                        runat="server" AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white"
                                                        BorderWidth="1px" OnRowCommand="CreateAutomaticTest_byQuestion_testDraftGridView_RowCommand"
                                                        OnSorting="CreateAutomaticTest_byQuestion_testDraftGridView_Sorting" Width="100%">
                                                        <RowStyle CssClass="grid_alternate_row" />
                                                        <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                        <HeaderStyle CssClass="grid_header_row" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="CreateAutomaticTes_byQuestion_testDrftRemoveImage" runat="server"
                                                                        SkinID="sknAlertImageButton" CommandName="Select" ToolTip="Flag question" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question ID">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="QuestionDetailPreviewControl_byQuestion_questionLinkButton" runat="server"
                                                                        Text='<%# Eval("QuestionKey") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField ReadOnly="True" HeaderText="Category" InsertVisible="False" DataField="CategoryName">
                                                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField ReadOnly="True" HeaderText="Subject" InsertVisible="False" DataField="SubjectName">
                                                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Question">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchQuestion_byQuestion_questionGridLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),20) %>'
                                                                        ToolTip='<%# Eval("Question") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Wrap="False" />
                                                            </asp:TemplateField>
                                                           <%-- <asp:TemplateField HeaderText="Answer">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchQuestion_byQuestion_answerGridLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                        ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Wrap="False" />
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Complexity">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CreateAutomaticTest_byQuestion_complexityLabel" runat="server" Text='<%# Eval("ComplexityName") %>'></asp:Label>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="9" style="width: 100%">
                                                                            <div id="CreateAutomaticTest_byQuestion_detailsDiv" runat="server" style="display: none;">
                                                                                
                                                                                <table border="0" cellpadding="2" cellspacing="0" width="850px" class="table_outline_bg">
                                                                                    <tr>
                                                                                        <td class="popup_question_icon" colspan="4">
                                                                                            <div style="word-wrap: break-word; white-space: normal;">
                                                                                                <asp:Label ID="CreateAutomaticTest_byQuestion_questionLabel" runat="server" SkinID="sknLabelFieldMultiText"
                                                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_padding_left_20" colspan="4">
                                                                                            <asp:PlaceHolder ID="CreateAutomaticTest_byQuestion_answerChoicesPlaceHolder" runat="server">
                                                                                            </asp:PlaceHolder>
                                                                                            <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_5" colspan="4">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 15%">
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_testAreaHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Test Area"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_testAreaTextBox" runat="server" ReadOnly="true"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("TestAreaName") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 16%">
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_complexityHeadLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Complexity"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestion_complexityTextBox" runat="server" ReadOnly="true"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("ComplexityName") %>'></asp:Label>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" class="td_height_8">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                
                                                                            </div>

                                                                            <div id="CreateAutomaticTest_byQuestionOpenText_detailsDiv" runat="server" style="display: none;">
                                                                                
                                                                                <table border="0" cellpadding="2" cellspacing="0" width="850px" class="table_outline_bg">
                                                                                    <tr>
                                                                                        <td class="popup_question_icon" colspan="4">
                                                                                            <div style="word-wrap: break-word; white-space: normal;">
                                                                                                <asp:Label ID="CreateAutomaticTest_byQuestionOpenText_questionLabel" runat="server" SkinID="sknLabelFieldMultiText"
                                                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_padding_left_20" colspan="4">
                                                                                            <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                <div class="label_multi_field_text" style="height: 40px; overflow: auto; word-wrap: break-word; white-space: normal;">
                                                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestionOpenText_AnswerLabel" runat="server" SkinID="sknLabelFieldHeaderText" Text="Answer:"></asp:Label>&nbsp;&nbsp;&nbsp; 
                                                                                                                                <asp:Literal ID="CreateAutomaticTest_byQuestionOpenText_Answer" runat="server" SkinID="sknMultiLineText"
                                                                                                                                    Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                                                            </div>
                                                                                                <asp:HiddenField ID="CreateAutomaticTest_byQuestionOpenText_CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                            </div>
                                                                                            
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_5" colspan="4">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 15%">
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestionOpenText_testAreaHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Test Area"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestionOpenText_testAreaTextBox" runat="server" ReadOnly="true"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("TestAreaName") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 16%">
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestionOpenText_complexityHeadLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Complexity"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="CreateAutomaticTest_byQuestionOpenText_complexityTextBox" runat="server" ReadOnly="true"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("ComplexityName") %>'></asp:Label>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" class="td_height_8">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                
                                                                            </div>
                                                                            <%-- popup DIV --%>
                                                                            <a href="#SearchQuestion_focusmeLink" id="CreateAutomaticTest_byQuestion_focusDownLink"
                                                                                runat="server"></a><a href="#" id="CreateAutomaticTest_byQuestion_focusmeLink" runat="server">
                                                                                </a>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr style="display:none">
                                    <td width="100%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="CreateAutomaticTest_byQuestion_automatedControlUpdatePanel"
                                                        runat="server">
                                                        <ContentTemplate>
                                                            <uc2:AutomatedTestControl ID="CreateAutomaticTest_byQuestion_automatedTestUserControl"
                                                                runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="CreateAutomaticTest_byQuestion_questionPanel" runat="server" Style="display: none"
                                            CssClass="popupcontrol_question_detail">
                                            <div style="display: none">
                                                <asp:Button ID="CreateAutomaticTest_byQuestion_hiddenButton" runat="server" Text="Hidden" /></div>
                                            <uc4:QuestionDetailPreviewControl ID="CreateAutomaticTest_byQuestion_QuestionDetailPreviewControl"
                                                runat="server" />
                                        </asp:Panel>
                                        <ajaxToolKit:ModalPopupExtender ID="CreateAutomaticTest_byQuestion_bottomQuestionModalPopupExtender"
                                            runat="server" PopupControlID="CreateAutomaticTest_byQuestion_questionPanel"
                                            TargetControlID="CreateAutomaticTest_byQuestion_hiddenButton" BackgroundCssClass="modalBackground"
                                            DynamicServicePath="" Enabled="True">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                            </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="CreateManualTest_testdetailsTabPanel" runat="server">
                        <HeaderTemplate>
                            Test Details</HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="CreateAutomaticTest_byQuestion_testDetailsControlUpdatePanel"
                                runat="server">
                                <ContentTemplate>
                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td style="width: 100%">
                                                <uc1:TestDetailsControl ID="CreateAutomaticTest_byQuestion_testDetailsUserControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateAutomaticTest_byQuestion_bottomMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateAutomaticTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateAutomaticTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="63%" align="right">
                                        <asp:Button ID="CreateAutomaticTest_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="CreateAutomaticTest_saveButton_Click" />
                                        <asp:Button ID="CreateAutomaticTest_bottomCreateSessionButton" runat="server" SkinID="sknButtonId"
                                            Text="Create Session" OnClick="CreateAutomaticTest_CreateSessionButton_Click" />
                                    </td>
                                    <td align="right" style="width: 15%">
                                        <asp:LinkButton ID="CreateAutomaticTest_bottomResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="CreateAutomaticTest_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="CreateAutomaticTest_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <div id="SearchQuestion_hiddenDIV" runat="server" style="display: none">
        <asp:Button ID="CreateAutomaticText_hiddenPopupModalButton" runat="server" />
    </div>
    <ajaxToolKit:ModalPopupExtender ID="CreateAutomaticText_saveTestModalPopupExtender"
        runat="server" PopupControlID="CreateAutomaticText_saveTestConfrimPopUpPanel"
        TargetControlID="CreateAutomaticText_hiddenPopupModalButton" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="CreateAutomaticText_saveTestConfrimPopUpPanel" runat="server" Style="display: none"
        Height="210px" CssClass="popupcontrol_confirm">
        <uc3:ConfirmMsgControl ID="CreateAutomaticText_confirmPopupExtenderControl" runat="server"
            OnOkClick="CreateAutomaticText_okPopUpClick" OnCancelClick="CreateAutomaticText_cancelPopUpClick" />
    </asp:Panel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=CreateAutomaticTest_byQuestion_totalNumberTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('ctl00_OTMMaster_body_CreateAutomaticTest_mainTabContainer_CreateManualTest_testdetailsTabPanel_CreateAutomaticTest_byQuestion_testDetailsUserControl_TestDetailsControl_testNameTextBox').focus();
                }
                catch (Err1) {
                }
            }
        }
    </script>
</asp:Content>
