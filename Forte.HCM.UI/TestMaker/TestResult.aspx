﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="TestResult.aspx.cs" Inherits="Forte.HCM.UI.TestMaker.TestResult" %>

<%@ Register Src="~/CommonControls/TestResultsControl.ascx" TagName="TestResultsControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="TestResult_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">

    <script src="../JS/ChartScript.js" type="text/javascript"></script>

    <script type="text/javascript">
        function ShowEmailConfirm() {
            //displays email confirmation pop up extender
            $find("<%= TestResult_emailConfirmation_ModalPopupExtender.ID %>").show();
            return false;
        }        
    </script>

    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Label ID="TestResult_headerLiteral" runat="server" Text="Test Results"></asp:Label>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="2" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="TestResult_topDownloadButton" runat="server" Text="Download" OnClick="TestResult_downloadButton_Click"
                                            SkinID="sknButtonId" />
                                    </td>
                                    <td>
                                        <asp:Button ID="TestResult_topEmailButton" OnClientClick="javascript:return ShowEmailConfirm();"
                                            runat="server" Text="Email" SkinID="sknButtonId" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="TestResult_topCancelButton" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                            OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="TestResult_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TestResult_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:TestResultsControl ID="TestResult_testResultsControl" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="TestResult_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TestResult_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="header_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%" align="right">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="TestResult_bottomDownloadButton" runat="server" Text="Download" OnClick="TestResult_downloadButton_Click"
                                            SkinID="sknButtonId" />
                                    </td>
                                    <td>
                                        <asp:Button ID="TestResult_bottomEmailButton" OnClientClick="javascript:return ShowEmailConfirm();"
                                            runat="server" Text="Email" SkinID="sknButtonId" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="TestResult_bottomCancelButton" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                            OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="TestResult_emailConfirmationPanel" runat="server" Style="display: none;
                    height: 205px;" CssClass="popupcontrol_confirm">
                    <div id="TestResult_emailConfirmationDiv" style="display: none">
                        <asp:Button ID="TestResult_emailConfirmation_hiddenButton" runat="server" />
                    </div>
                    <uc3:ConfirmMsgControl ID="TestResult_emailConfirmation_ConfirmMsgControl" runat="server"
                        Title="Email Confirmation" Type="EmailConfirmType" Message="How do you want to email your results?" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="TestResult_emailConfirmation_ModalPopupExtender"
                    BehaviorID="TestResult_emailConfirmation_ModalPopupExtender" runat="server" PopupControlID="TestResult_emailConfirmationPanel"
                    TargetControlID="TestResult_emailConfirmation_hiddenButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
