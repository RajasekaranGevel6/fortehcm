﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchTest.cs
// File that represents the user Search the SearchTest by various Key filed.
// This will helps Search a Test From the Test repository.
//

#endregion Header

#region Directives

using System;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Search Test page. This page helps in searching tests based on
    /// search criteria are, category,subject,keyword, testarea and so forth.
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchTest : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Declaration

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button.
                Page.Form.DefaultButton = SearchTest_topSearchButton.UniqueID;
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    LoadValues();

                    // Check if the page is launched from menu. If so, clear the existing
                    // session variables.
                    if ((!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                        Session["CATEGORY_SUBJECTS"] = null;
                    }

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST]
                            as TestSearchCriteria);
                    CheckAndSetExpandorRestore();
                }

                SearchTest_topSuccessMessageLabel.Text = string.Empty;
                SearchTest_bottomSuccessMessageLabel.Text = string.Empty;
                SearchTest_topErrorMessageLabel.Text = string.Empty;
                SearchTest_bottomErrorMessageLabel.Text = string.Empty;

                SearchTest_bottomPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                         (SearchTest_pagingNavigator_PageNumberClick);
                SearchTest_categorySubjectControl.ControlMessageThrown += new
                    CategorySubjectControl.ControlMessageThrownDelegate(
                         SearchTest_categorySubjectControl_ControlMessageThrown);

                foreach (MultiHandleSliderTarget target in
                    SearchTest_noOfQuestionsMultiHandleSliderExtender.MultiHandleSliderTargets)
                    target.ControlID = SearchTest_noOfQuestionsMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;

                //foreach (MultiHandleSliderTarget target in
                //    SearchTest_testCostMultiHandleSliderExtender.MultiHandleSliderTargets)
                //    target.ControlID = SearchTest_testCostMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;

                if (!IsPostBack)
                {
                    // Load position profile details. This is applicable when navigating
                    // from the 'search position profile' page.
                    LoadPositionProfileDetail();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/> that holds the event data.
        /// </param>
        private void SearchTest_categorySubjectControl_ControlMessageThrown(object sender,
            ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(SearchTest_topErrorMessageLabel,
                        SearchTest_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(SearchTest_topSuccessMessageLabel,
                        SearchTest_bottomSuccessMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void SearchTest_OkClick(object sender, EventArgs e)
        {
            try
            {
                TestBLManager testBLManager = new TestBLManager();

                string searchTest_Action = SearchTest_Action.Value.ToString();
                string testKey = SearchTest_TestKey.Value.Trim().ToString();
                if (searchTest_Action == "active")
                {
                    testBLManager.ActivateTest(testKey, base.userID);
                    base.ShowMessage(SearchTest_topSuccessMessageLabel,
                    SearchTest_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchTest_Activate, testKey));
                }
                else if (searchTest_Action == "inactive")
                {
                    testBLManager.DeactivateTest(testKey, base.userID);
                    base.ShowMessage(SearchTest_topSuccessMessageLabel,
                     SearchTest_bottomSuccessMessageLabel,
                     string.Format(Resources.HCMResource.SearchTest_InActivate, testKey));
                }
                else if (searchTest_Action == "deletes")
                {
                    testBLManager.DeleteTest(testKey, base.userID);
                    base.ShowMessage(SearchTest_topSuccessMessageLabel,
                    SearchTest_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchTest_Delete, testKey));
                }
                LoadTests(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link and search criteria fields.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTest_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SearchTest_simpleLinkButton.Text.ToLower() == "simple")
                {
                    SearchTest_simpleLinkButton.Text = "Advanced";
                    SearchTest_simpleSearchDiv.Visible = true;
                    SearchTest_advanceSearchDiv.Visible = false;
                }
                else
                {
                    SearchTest_simpleLinkButton.Text = "Simple";
                    SearchTest_simpleSearchDiv.Visible = false;
                    SearchTest_advanceSearchDiv.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchTest_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchTest_bottomPagingNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "TESTKEY";
                LoadTests(1);
                SearchTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
                //SearchTest_testCostMultiHandleSliderExtender.ClientState = "0";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchTest_pagingNavigator_PageNumberClick(object sender,
                     PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;

                //Bind the Search Result based on the Pageno, Sort Expression and Sort Direction
                LoadTests(e.PageNumber);
                SearchTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
                //SearchTest_testCostMultiHandleSliderExtender.ClientState = "0";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchTest_testGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;

                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                SearchTest_bottomPagingNavigator.Reset();
                LoadTests(1);
                SearchTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
                //SearchTest_testCostMultiHandleSliderExtender.ClientState = "0";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchTest_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton SearchTest_viewImage = (ImageButton)e.Row.FindControl("SearchTest_viewImage");

                    if (SearchTest_viewImage != null && !string.IsNullOrEmpty(SearchTest_viewImage.CommandArgument))
                    {
                        SearchTest_viewImage.Visible = true;
                    }

                    ImageButton SearchTest_additionalDetailsImageButton = (ImageButton)e.Row.FindControl(
                        "SearchTest_additionalDetailsImageButton");

                    ImageButton SearchTest_activeImageButton = (ImageButton)e.Row.FindControl(
                        "SearchTest_activeImageButton");
                    ImageButton SearchTest_inactiveImageButton = (ImageButton)e.Row.FindControl(
                        "SearchTest_inactiveImageButton");
                    ImageButton SearchTest_createNewImageButton = (ImageButton)e.Row.FindControl(
                        "SearchTest_createNewImageButton");
                    ImageButton SearchTest_deleteImageButton = (ImageButton)e.Row.FindControl(
                        "SearchTest_deleteImageButton");
                    ImageButton SearchTest_editImageButton = (ImageButton)e.Row.FindControl(
                        "SearchTest_editImageButton");
                    HiddenField SearchTest_statusHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchTest_statusHiddenField");
                    HiddenField SearchTest_testKeyHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchTest_testKeyHiddenField");
                    HiddenField SearchTest_activeInactivestatusHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchTest_activeInactivestatusHiddenField");
                    HiddenField SearchTest_testAutherIdHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchTest_testAutherIdHiddenField");
                    HiddenField SearchTest_dataAccessRightsHiddenField = (HiddenField)e.Row.FindControl(
                   "SearchTest_dataAccessRightsHiddenField");

                    ImageButton SearchTest_copyImageButton = (ImageButton)e.Row.FindControl(
                       "SearchTest_copyImageButton");

                    ImageButton SearchTest_associateToPPImageButton = (ImageButton)e.Row.FindControl(
                       "SearchTest_associateToPPImageButton");
                    HiddenField SearchTest_positionProfileIDHiddenField = (HiddenField)e.Row.FindControl(
                   "SearchTest_positionProfileIDHiddenField");
                    HiddenField SearchTest_tenantIDHiddenField = (HiddenField)e.Row.FindControl(
                   "SearchTest_tenantIDHiddenField");

                    SearchTest_additionalDetailsImageButton.Attributes.Add("onclick", "OpenAdditionalTestDetail('" +
                        SearchTest_testKeyHiddenField.Value + "')");
                    bool loginAuther = SearchTest_testAutherIdHiddenField.Value ==
                        base.userID.ToString() ? true : false;

                    if (SearchTest_statusHiddenField.Value == "True")
                    {
                        if (loginAuther)
                        {
                            if (SearchTest_activeInactivestatusHiddenField.Value == "True")
                            {
                                SearchTest_activeImageButton.Visible = true;
                                SearchTest_editImageButton.Visible = true;
                            }
                        }
                        else
                        {
                            SearchTest_deleteImageButton.Visible = false;
                            SearchTest_editImageButton.Visible = false;
                        }
                    }
                    else
                    {
                        SearchTest_activeImageButton.Visible = false;
                        SearchTest_editImageButton.Visible = false;
                        SearchTest_createNewImageButton.Visible = false;
                        if (loginAuther)
                        {
                            if (SearchTest_activeInactivestatusHiddenField.Value == "True")
                            {
                                SearchTest_inactiveImageButton.Visible = true;
                            }
                        }
                        else
                        {
                            SearchTest_deleteImageButton.Visible = false;
                        }
                    }

                    if ((SearchTest_dataAccessRightsHiddenField.Value == "V"))
                    {
                        SearchTest_editImageButton.Visible = false;
                        SearchTest_inactiveImageButton.Visible = false;
                        SearchTest_deleteImageButton.Visible = false;
                        SearchTest_activeImageButton.Visible = false;
                        SearchTest_copyImageButton.Visible = true;
                        SearchTest_createNewImageButton.Visible = false;
                        ImageButton SearchTest_viewImageButton = (ImageButton)e.Row.FindControl(
                        "SearchTest_viewImageButton");
                        SearchTest_viewImageButton.CommandName = "copyviewtest";
                    }

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchTest_tenantIDHiddenField.Value)
                        && SearchTest_tenantIDHiddenField.Value == base.tenantID.ToString())
                    {
                        SearchTest_createNewImageButton.Visible = true;
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(SearchTest_positionProfileIDHiddenField.Value))
                        {
                            SearchTest_associateToPPImageButton.Visible = true;
                            SearchTest_copyImageButton.Visible = false;
                        }
                        else
                            SearchTest_copyImageButton.Visible = true;

                    }
                    int positionProfileID = 0;

                    if (!int.TryParse(Request.QueryString
               [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
                    {
                        SearchTest_associateToPPImageButton.Visible = false;
                        SearchTest_copyImageButton.Visible = true;

                    }

                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchTest_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex(
                        SearchTest_testGridView,
                        (string)ViewState["SORT_FIELD"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void SearchTest_resetLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void SearchTest_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "active")
                {
                    SearchTest_inactivePopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchTest_Activate_Confirm_Message, e.CommandArgument.ToString());
                    SearchTest_inactivePopupExtenderControl.Title = "Activate Test";
                    SearchTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchTest_inactivepopupExtender.Show();
                }
                else if (e.CommandName == "inactive")
                {
                    SearchTest_inactivePopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchTest_DeActivate_Confirm_Message, e.CommandArgument.ToString());
                    SearchTest_inactivePopupExtenderControl.Title = "Deactivate Test";
                    SearchTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchTest_inactivepopupExtender.Show();
                }
                else if (e.CommandName == "deletes")
                {
                    SearchTest_inactivePopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchTest_Delete_Confirm_Message, e.CommandArgument.ToString());
                    SearchTest_inactivePopupExtenderControl.Title = "Delete Test";
                    SearchTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchTest_inactivepopupExtender.Show();
                }
                else if (e.CommandName == "view")
                {
                    Response.Redirect("ViewTest.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_TEST + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "edittest")
                {
                    Response.Redirect(Constants.TestMakerConstants.EDIT_TEST + "?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_TEST + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "create")
                {
                    Response.Redirect("CreateTestSession.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_TEST + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "copytest")
                {
                    Response.Redirect(Constants.TestMakerConstants.EDIT_TEST + "?m=1&s=2&type=copy&parentpage=" +
                                           Constants.ParentPage.SEARCH_TEST + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "copyviewtest")
                {
                    Response.Redirect("ViewTest.aspx?m=1&s=2&type=copy&parentpage=" +
                                           Constants.ParentPage.SEARCH_TEST + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "associatePP")
                {
                    int positionProfileID = 0;

                    if (!int.TryParse(Request.QueryString
               [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
                    {
                        return;
                    }
                    new TestBLManager().UpdateTest(e.CommandArgument.ToString(), positionProfileID, userID);
                    LoadTests(int.Parse(ViewState["PAGENUMBER"].ToString()));
                    Response.Redirect("CreateTestSession.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_TEST + "&testkey=" + e.CommandArgument, false);
               //     base.ShowMessage(SearchTest_topSuccessMessageLabel,
               //SearchTest_bottomSuccessMessageLabel, "Test associated with position profile");
                }

                SearchTest_Action.Value = e.CommandName.ToString();
                SearchTest_TestKey.Value = e.CommandArgument.ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// During that time, it will clear all the search criteria session,
        /// category subject session values.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SearchTest_cancelLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect("~/OTMHome.aspx", false);
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadTests(int pageNumber)
        {
            int totalRecords = 0;
            StringBuilder testAreaID = GetTestAreaID();
            TestSearchCriteria testSearchCriteria = new TestSearchCriteria();
            testSearchCriteria.IsCertification = null;
            List<Subject> categorySubjects = null;

            if (SearchTest_simpleLinkButton.Text == "Advanced")
            {
                testSearchCriteria.Category = SearchTest_categoryTextBox.Text.Trim() == "" ?
                                    null : SearchTest_categoryTextBox.Text.Trim();
                testSearchCriteria.Subject = SearchTest_subjectTextBox.Text.Trim() == "" ?
                                    null : SearchTest_subjectTextBox.Text.Trim();
                testSearchCriteria.Keyword = SearchTest_keywordSimpleTextBox.Text.Trim() == "" ?
                                    null : SearchTest_keywordSimpleTextBox.Text.Trim();
                testSearchCriteria.QuestionType = (QuestionType)Convert.ToInt32(SearchTest_testTypeDropDownList.SelectedValue);

                testSearchCriteria.SearchType = SearchType.Simple;
                testSearchCriteria.CurrentPage = pageNumber;
                testSearchCriteria.IsMaximized =
                    SearchTest_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                testSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                testSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
                testSearchCriteria.TestAuthorID = base.userID;
                testSearchCriteria.ShowCopiedTests = true;
            }
            else
            {
                testSearchCriteria.SearchType = SearchType.Advanced;
                testSearchCriteria.QuestionType = (QuestionType)Convert.ToInt32(SearchTest_advTestTypeDropDownList.SelectedValue);

                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchTest_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in SearchTest_categorySubjectControl.SubjectDataSource)
                    {
                        if (categorySubjects == null)
                            categorySubjects = new List<Subject>();

                        Subject subjectCat = new Subject();

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                        // Get all the subjects and categories and then assign to the list.
                        subjectCat.CategoryID = subject.CategoryID;
                        subjectCat.CategoryName = subject.CategoryName;
                        subjectCat.SubjectID = subject.SubjectID;
                        subjectCat.SubjectName = subject.SubjectName;
                        subjectCat.IsSelected = subject.IsSelected;
                        categorySubjects.Add(subjectCat);
                    }

                    //Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in SearchTest_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }

                // Keep the categories and subjects in Session
                Session["CATEGORY_SUBJECTS"] = categorySubjects;

                if (SearchTest_certificateTestDropDownList.SelectedValue != "S")
                    testSearchCriteria.IsCertification = (SearchTest_certificateTestDropDownList.SelectedValue
                        == "Y") ? true : false;

                testSearchCriteria.CategoriesID = SelectedSubjectIDs == "" ?
                    null : SelectedSubjectIDs.TrimEnd(',');
                testSearchCriteria.TestAreasID = testAreaID.ToString() == "" ?
                    null : testAreaID.ToString().TrimEnd(',');
                testSearchCriteria.TestKey = SearchTest_testIdTextBox.Text.Trim() == "" ?
                    null : SearchTest_testIdTextBox.Text.Trim();
                testSearchCriteria.Name = SearchTest_testNameTextBox.Text.Trim() == "" ?
                    null : SearchTest_testNameTextBox.Text.Trim();

                if (SearchTest_positionProfileHiddenField.Value != null &&
                    SearchTest_positionProfileHiddenField.Value.Trim().Length > 0)
                {
                    testSearchCriteria.PositionProfileID = Convert.ToInt32(SearchTest_positionProfileHiddenField.Value);
                }
                testSearchCriteria.PositionProfileName = SearchTest_positionProfileTextBox.Text;

                testSearchCriteria.Keyword = SearchTest_keywordAdvanceTextBox.Text.Trim() == "" ?
                    null : SearchTest_keywordAdvanceTextBox.Text.Trim();

                testSearchCriteria.TestAuthorName = SearchTest_authorTextBox.Text.Trim() == "" ?
                    null : SearchTest_authorIdHiddenField.Value;

                //testSearchCriteria.TestCostStart = (SearchTest_testCostMinValueTextBox.Text.Trim() == "") ?
                //    0 : Convert.ToInt32(SearchTest_testCostMinValueTextBox.Text.Trim());

                //testSearchCriteria.TestCostEnd = (SearchTest_testCostMaxValueTextBox.Text.Trim() == "") ?
                //    0 : Convert.ToInt32(SearchTest_testCostMaxValueTextBox.Text.Trim());

                testSearchCriteria.TotalQuestionStart = (SearchTest_noOfQuestionsMinValueTextBox.Text.Trim() == "") ?
                    0 : Convert.ToInt32(SearchTest_noOfQuestionsMinValueTextBox.Text.Trim());

                testSearchCriteria.TotalQuestionEnd = (SearchTest_noOfQuestionsMaxValueTextBox.Text.Trim() == "") ?
                    0 : int.Parse(SearchTest_noOfQuestionsMaxValueTextBox.Text.Trim());

                testSearchCriteria.CurrentPage = pageNumber;
                testSearchCriteria.IsMaximized =
                    SearchTest_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                testSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                testSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

                testSearchCriteria.TestAuthorID = base.userID;
                testSearchCriteria.ShowCopiedTests = SearchTest_showCopiedTestsCheckBox.Checked;
            }

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST] = testSearchCriteria;

            List<TestDetail> testDetail = new List<TestDetail>();
            testDetail = new BL.TestBLManager().GetTests(testSearchCriteria, base.GridPageSize, pageNumber,
                testSearchCriteria.SortExpression, testSearchCriteria.SortDirection, out totalRecords);

            if (testDetail == null || testDetail.Count == 0)
            {
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                SearchTest_testGridView.DataSource = null;
                SearchTest_testGridView.DataBind();
                SearchTest_bottomPagingNavigator.TotalRecords = 0;
                SearchTest_testDiv.Visible = false;
            }
            else
            {
                SearchTest_testGridView.DataSource = testDetail;
                SearchTest_testGridView.DataBind();
                SearchTest_bottomPagingNavigator.PageSize = base.GridPageSize;
                SearchTest_bottomPagingNavigator.TotalRecords = totalRecords;
                SearchTest_testDiv.Visible = true;
            }
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A <see cref="TestSearchCriteria"/> that contains the test search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(TestSearchCriteria testSearchCriteria)
        {
            string[] testAreaIDs = null;

            // Check if the simple link button is clicked.
            if (testSearchCriteria.SearchType == SearchType.Simple)
            {
                SearchTest_simpleLinkButton.Text = "Advanced";
                SearchTest_simpleSearchDiv.Visible = true;
                SearchTest_advanceSearchDiv.Visible = false;

                if (testSearchCriteria.Category != null)
                    SearchTest_categoryTextBox.Text = testSearchCriteria.Category;

                if (testSearchCriteria.Subject != null)
                    SearchTest_subjectTextBox.Text = testSearchCriteria.Subject;

                if (testSearchCriteria.Keyword != null)
                    SearchTest_keywordSimpleTextBox.Text = testSearchCriteria.Keyword;

                SearchTest_restoreHiddenField.Value = testSearchCriteria.IsMaximized == true ? "Y" : "N";
                ViewState["SORT_FIELD"] = testSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = testSearchCriteria.SortDirection;

                // Apply search
                LoadTests(testSearchCriteria.CurrentPage);

                // Highlight the last page number which is stored in session
                // when the page is launched from somewhere else.
                SearchTest_bottomPagingNavigator.MoveToPage(testSearchCriteria.CurrentPage);
            }
            else
            {
                SearchTest_simpleLinkButton.Text = "Simple";
                SearchTest_simpleSearchDiv.Visible = false;
                SearchTest_advanceSearchDiv.Visible = true;

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.TestAreasID))
                {
                    testAreaIDs = testSearchCriteria.TestAreasID.Split(new char[] { ',' });

                    // Make selection test area.
                    for (int idx = 0; idx < testAreaIDs.Length; idx++)
                        SearchTest_testAreaCheckBoxList.Items.FindByValue(testAreaIDs[idx]).Selected = true;
                }

                if (testSearchCriteria.TestKey != null)
                    SearchTest_testIdTextBox.Text = testSearchCriteria.TestKey.Trim();

                if (testSearchCriteria.Name != null)
                    SearchTest_testNameTextBox.Text = testSearchCriteria.Name.Trim();

                if (testSearchCriteria.TestAuthorName != null)
                    SearchTest_authorTextBox.Text = testSearchCriteria.TestAuthorName.Trim();

                if (testSearchCriteria.IsCertification != null)
                    SearchTest_certificateTestDropDownList.SelectedValue =
                        (testSearchCriteria.IsCertification == true) ? "Y" : "N";

                SearchTest_positionProfileTextBox.Text = testSearchCriteria.PositionProfileName.Trim();
                SearchTest_positionProfileHiddenField.Value = testSearchCriteria.PositionProfileID.ToString();

                if (testSearchCriteria.Keyword != null)
                    SearchTest_keywordAdvanceTextBox.Text = testSearchCriteria.Keyword.Trim();

                //if (testSearchCriteria.TestCostStart != 0)
                //    SearchTest_testCostMinValueTextBox.Text = testSearchCriteria.TestCostStart.ToString();

                //if (testSearchCriteria.TestCostEnd != 0)
                //    SearchTest_testCostMaxValueTextBox.Text = testSearchCriteria.TestCostEnd.ToString();

                if (testSearchCriteria.TotalQuestionStart != 0)
                    SearchTest_noOfQuestionsMinValueTextBox.Text = testSearchCriteria.TotalQuestionStart.ToString();

                if (testSearchCriteria.TotalQuestionEnd != 0)
                    SearchTest_noOfQuestionsMaxValueTextBox.Text = testSearchCriteria.TotalQuestionEnd.ToString();

                if (Session["CATEGORY_SUBJECTS"] != null)
                {
                    // Fetch unique categories from the list.
                    var distinctCategories = (Session["CATEGORY_SUBJECTS"] as List<Subject>).GroupBy
                        (x => x.CategoryID).Select(x => x.First());

                    // This datasource is used to make the selection of the subjects
                    // which are already selected by the user to search.
                    SearchTest_categorySubjectControl.SubjectsToBeSelected =
                        Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Bind distinct categories in the category datalist
                    SearchTest_categorySubjectControl.CategoryDataSource =
                        distinctCategories.ToList<Subject>();

                    // Bind subjects for the categories which are bind previously.
                    SearchTest_categorySubjectControl.SubjectDataSource =
                        distinctCategories.ToList<Subject>();
                }
                SearchTest_showCopiedTestsCheckBox.Checked = testSearchCriteria.ShowCopiedTests;
                SearchTest_restoreHiddenField.Value = testSearchCriteria.IsMaximized == true ? "Y" : "N";
                ViewState["SORT_FIELD"] = testSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = testSearchCriteria.SortDirection;

                // Apply search
                LoadTests(testSearchCriteria.CurrentPage);

                // Highlight the last page number which is stored in session
                // when the page is launched from somewhere else.
                SearchTest_bottomPagingNavigator.MoveToPage(testSearchCriteria.CurrentPage);
            }
        }

        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {
            try
            {
                SearchTest_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
                SearchTest_testAreaCheckBoxList.DataTextField = "AttributeName";
                SearchTest_testAreaCheckBoxList.DataValueField = "AttributeID";
                SearchTest_testAreaCheckBoxList.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTest_topErrorMessageLabel,
                SearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < SearchTest_testAreaCheckBoxList.Items.Count; i++)
            {
                if (SearchTest_testAreaCheckBoxList.Items[i].Selected)
                {
                    testAreaID.Append(SearchTest_testAreaCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return testAreaID;
        }

        /// <summary>
        /// Method that adds the click handler for expand or restore buttons.
        /// </summary>
        private void AssignHandlers()
        {
            SearchTest_searchTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                SearchTest_testDiv.ClientID + "','" +
                SearchTest_searchByTestDiv.ClientID + "','" +
                SearchTest_searchResultsUpSpan.ClientID + "','" +
                SearchTest_searchResultsDownSpan.ClientID + "','" +
                SearchTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            // Add click hander for position profile image icon.
            SearchTest_positionProfileImageButton.Attributes.Add("onclick",
                "return LoadPositionProfileName('" + SearchTest_positionProfileTextBox.ClientID + "','" +
                SearchTest_positionProfileHiddenField.ClientID + "')");

            // Add click hander for author image icon.
            SearchTest_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + SearchTest_dummyAuthorID.ClientID + "','"
                + SearchTest_authorIdHiddenField.ClientID + "','"
                + SearchTest_authorTextBox.ClientID + "','TA')");
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchTest_restoreHiddenField.Value) &&
                SearchTest_restoreHiddenField.Value == "Y")
            {
                SearchTest_searchByTestDiv.Style["display"] = "none";
                SearchTest_searchResultsUpSpan.Style["display"] = "block";
                SearchTest_searchResultsDownSpan.Style["display"] = "none";
                SearchTest_testDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchTest_searchByTestDiv.Style["display"] = "block";
                SearchTest_searchResultsUpSpan.Style["display"] = "none";
                SearchTest_searchResultsDownSpan.Style["display"] = "block";
                SearchTest_testDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchTest_restoreHiddenField.Value))
                    ((TestSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST]).IsMaximized =
                        SearchTest_restoreHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// A Method that gets the position profile keywords by removing there
        /// weightages
        /// </summary>
        /// <param name="KeyWordWithWeightages">
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// among with their weightages
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// by removing their weightages
        /// </returns>
        private string GetPositionProfileKeys(string KeyWordWithWeightages)
        {
            StringBuilder sbKeywords = null;
            string[] strSplitKeywords = null;
            try
            {
                strSplitKeywords = KeyWordWithWeightages.Split(',');
                sbKeywords = new StringBuilder();
                for (int i = 0; i < strSplitKeywords.Length; i++)
                    if (strSplitKeywords[i].IndexOf('[') != -1)
                        sbKeywords.Append(strSplitKeywords[i].Substring(0, strSplitKeywords[i].IndexOf('[')) + ",");
                    else
                        sbKeywords.Append(strSplitKeywords[i] + ",");
                return sbKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (strSplitKeywords != null) strSplitKeywords = null;
                if (sbKeywords != null) sbKeywords = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        private void LoadPositionProfileDetail()
        {
            // Check if position profile ID is present.
            if (Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING] == null ||
                Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING].Trim().Length == 0)
            {
                return;
            }

            int positionProfileID = 0;

            // Check if the query string contains a valid position profile ID.
            if (!int.TryParse(Request.QueryString
                [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
            {
                return;
            }

            PositionProfileDetail positionProfileDetail = null;
            positionProfileDetail = new PositionProfileBLManager().GetPositionProfileKeyWord(positionProfileID);
            if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                return;

            // Assign keywords alone.
            SearchTest_keywordAdvanceTextBox.Text = GetPositionProfileKeys(positionProfileDetail.Keys);

            // Expand the advanced search panel.
            SearchTest_simpleLinkButton.Text = "Simple";
            SearchTest_simpleSearchDiv.Visible = false;
            SearchTest_advanceSearchDiv.Visible = true;

            // Un-check the 'Show Copied Tests' checkbox.
            SearchTest_showCopiedTestsCheckBox.Checked = false;

            // Apply default search.
            SearchTest_bottomPagingNavigator.Reset();
            ViewState["SORT_ORDER"] = SortType.Descending;
            ViewState["SORT_FIELD"] = "TESTKEY";
            LoadTests(1);
            SearchTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
            //SearchTest_testCostMultiHandleSliderExtender.ClientState = "0";
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Page title
            Master.SetPageCaption(Resources.HCMResource.SearchTest_Title);

            // Bind test Area.
            BindTestArea();

            // Set default focus field.
            Page.Form.DefaultFocus = SearchTest_categoryTextBox.UniqueID;
            SearchTest_categoryTextBox.Focus();
            SearchTest_testDiv.Visible = false;

            AssignHandlers();

            // Default settings
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Descending;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "TESTKEY";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";
            LoadTests(1);
        }

        #endregion Protected Overridden Methods

    }
}