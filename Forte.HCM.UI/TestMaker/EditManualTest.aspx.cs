﻿#region Header                                                                                        

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditManualTest.cs
// File that represents the user Serach the Question by various Key filed. 
// and Modified the Exists test Details and questions if Test included the Some of Session the new test will be created.
// This will helps edit & create a Test and have to implemeted the Certification details..

#endregion

#region Directives                                                                                    
using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
#endregion
namespace Forte.HCM.UI.TestMaker
{
    public partial class EditManualTest : PageBase
    {
        #region Declaration                                                                           
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        string parentPage = "";

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";
        string testKeyQuestyString = "";
        string questionID = "";
        List<QuestionDetail> questionDetailSearchResultList;
        /// <summary>
        /// A <see cref="int"/> constant that holds the adaptive page size.
        /// </summary>
        private const int ADAPTIVE_PAGE_SIZE = 5;
        private const string ADAPTIVE_QUESTION_DETAILS_VIEWSTATE = "ADAPTIVEQUESTIONDETAILS";
      
        #endregion

        #region Event Handlers                                                                        

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                parentPage = Request.QueryString.Get("parentpage");
                if (EditManualTest_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = EditManualTest_topSearchButton.UniqueID;
                else if (EditManualTest_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = EditManualTest_bottomSaveButton.UniqueID;
                EditManualTest_recommendAdaptiveQuestions.Style.Add("display", "none");
                StateMaintainence();
                MessageLabel_Reset();
                Master.SetPageCaption(Resources.HCMResource.EditTest_Title);
                if (!IsPostBack)
                {
                    LoadValues();
                    EditManualTest_simpleSearchDiv.Visible = true;
                    EditManualTest_advanceSearchDiv.Visible = false;
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                        ViewState["SORTDIRECTIONKEY"] = "A";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                        ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                        ViewState["PAGENUMBER"] = "1";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                        ViewState["SEARCHRESULTGRID"] = null;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                        testKeyQuestyString = Request.QueryString["testkey"].ToUpper();

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        EditManualTest_topCreateSessionButton.Visible = false;
                        EditManualTest_bottomCreateSessionButton.Visible = false;
                    }

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["showmessage"]))
                        {
                            if (Request.QueryString["mode"].ToLower() == "new")
                            {
                                base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                                        EditManualTest_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualTest_AddedSuccessfully, testKeyQuestyString));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "edit")
                            {
                                base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                                       EditManualTest_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualTest_UpdatedSuccessfully, testKeyQuestyString));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "copy")
                            {
                                base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                                      EditManualTest_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualTest_CopySuccessfully, testKeyQuestyString));
                                EditManualTest_topCreateSessionButton.Visible = true;
                                EditManualTest_bottomCreateSessionButton.Visible = true;
                            }
                        }
                    }
                    EditManualTest_bottomCreateSessionButton.PostBackUrl = "CreateTestSession.aspx?m=1&s=1&parentpage=" + Constants.ParentPage.EDIT_TEST + "&testkey=" + testKeyQuestyString;
                    EditManualTest_topCreateSessionButton.PostBackUrl = "CreateTestSession.aspx?m=1&s=1&parentpage=" + Constants.ParentPage.EDIT_TEST + "&testkey=" + testKeyQuestyString;

                    EditManualTest_questionKeyHiddenField.Value = testKeyQuestyString;

                    BindTestQuestions(testKeyQuestyString);
                    BindTestDetails(testKeyQuestyString);

                    EditManualTest_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('" 
                        + EditManualTest_dummyAuthorID.ClientID + "','" 
                        + EditManualTest_authorIdHiddenField.ClientID + "','"
                        + EditManualTest_authorTextBox.ClientID + "','QA')");

                    EditManualTest_positionProfileImageButton.Attributes.Add("onclick",
                            "return ShowClientRequestForManual('" + EditManualTest_positionProfileTextBox.ClientID + "','" +
                            EditManualTest_positionProfileKeywordTextBox.ClientID + "','" +
                             EditManualTest_positionProfileHiddenField.ClientID + "')");

                    EditManualTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
                    EditManualTest_testDrftGridView.DataBind();
                    BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                                   false), Convert.ToInt32(EditManualTest_adaptivePagingHiddenField.Value));
                    EditManualTest_adaptivebottomPagingNavigator.Visible = true;
                }
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");
                ExpandRestore();
                EditManualTest_bottomPagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                      (EditManualTest_pagingNavigator_PageNumberClick);

                // Create events for paging control
                EditManualTest_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (EditManualTest_adaptivepagingNavigator_PageNumberClick);

                EditManualTest_categorySubjectControl.ControlMessageThrown += new CategorySubjectControl.ControlMessageThrownDelegate(EditManualTest_categorySubjectControl_ControlMessageThrown);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void StateMaintainence()
        {
            switch (EditManualTest_resultsHiddenField.Value)
            {
                case "0":
                    EditManualTest_questionDiv.Style.Add("width", "936px");
                    EditManualTest_adaptiveQuestionDiv.Style.Add("width", "0px");
                    break;
                case "1":
                    EditManualTest_questionDiv.Style.Add("width", "0px");
                    EditManualTest_adaptiveQuestionDiv.Style.Add("width", "936px");
                    break;
                case "2":
                    EditManualTest_questionDiv.Style.Add("width", "450px");
                    EditManualTest_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
                default:
                    EditManualTest_questionDiv.Style.Add("width", "450px");
                    EditManualTest_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void EditManualTest_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                EditManualTest_topErrorMessageLabel.Text = c.Message.ToString();
                EditManualTest_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                EditManualTest_topSuccessMessageLabel.Text = c.Message.ToString();
                EditManualTest_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
            BindSearchAndTestDraftGridView();
        }

        /// <summary>
        /// Handler method event is raised whenever recommend adaptive button
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualTest_recommendAdaptiveQuestions_Click(object sender, EventArgs e)
        {
            try
            {
                BindSearchAndTestDraftGridView();
                EditManualTest_adaptiveRecommendClickedHiddenField.Value = "1";
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    true ), Convert.ToInt32(EditManualTest_adaptivePagingHiddenField.Value));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void EditManualTest_okClick(object sender, EventArgs e)
        {
            
            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel, 
                    EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
            
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditManualTest_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "view")
                    return;
                string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                EditManualTest_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                EditManualTest_questionDetailSummaryControl.ShowAddButton = true;
                EditManualTest_questionDetailSummaryControl.LoadQuestionDetails(questionCollection[0], int.Parse(questionCollection[1]));
                EditManualTest_questionModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditManualTest_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                HiddenField EditManualTest_hidddenValue = (HiddenField)e.Row.FindControl("EditManualTest_hidddenValue");
                string questionID = EditManualTest_hidddenValue.Value.Split(':')[0].ToString();
                ImageButton alertImageButton = (ImageButton)e.Row.FindControl("EditManualTest_searchQuestionAlertImageButton");
                alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");
                Image img = (Image)e.Row.FindControl("EditManualTest_searchQuestionSelectImage");
                img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualTest_searchQuestionGridView.ClientID + "');");
                // img.Attributes.Add("OnClick", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualTest_searchQuestionGridView.ClientID + "');");
                img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + EditManualTest_hidddenValue.Value + "','" + EditManualTest_selectMultipleImage.ClientID + "');");
                e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                Label EditManualTest_questionLabel =
                    (Label)e.Row.FindControl("EditManualTest_questionLabel");
                HoverMenuExtender EditManualTest_hoverMenuExtender = (HoverMenuExtender)
                    e.Row.FindControl("EditManualTest_hoverMenuExtender");
                EditManualTest_hoverMenuExtender.TargetControlID = EditManualTest_questionLabel.ID;
                // Label EditManualTest_questionDetailPreviewControlQuestionLabel =
                //   (Label)e.Row.FindControl("EditManualTest_questionDetailPreviewControlQuestionLabel");
                //RadioButtonList EditManualTest_questionDetailPreviewControlAnswerRadioButtonList =
                //    (RadioButtonList)e.Row.FindControl("EditManualTest_questionDetailPreviewControlAnswerRadioButtonList");
                PlaceHolder EditManualTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("EditManualTest_answerChoicesPlaceHolder");
                EditManualTest_answerChoicesPlaceHolder.Controls.Add
                                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID),true));
                HiddenField EditManualTest_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                       "EditManualTest_correctAnswerHiddenField");
                List<AnswerChoice> answerChoice = new List<AnswerChoice>();
                answerChoice = new BL.QuestionBLManager().GetQuestionOptions(questionID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditManualTest_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    EditManualTest_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    string[] QuestionDetails = e.CommandArgument.ToString().Split(':');
                    EditManualTest_questionDetailSummaryControl.Visible = true;
                    EditManualTest_questionDetailSummaryControl.ShowAddButton = true;
                    EditManualTest_questionDetailSummaryControl.LoadQuestionDetails(QuestionDetails[0], Convert.ToInt32(QuestionDetails[1]));

                    EditManualTest_questionModalPopupExtender.Show();
                }
                if (e.CommandName == "AdaptiveQuestion")
                {
                    EditManualTest_adaptiveQuestionPreviewModalPopupExtender.Show();

                    ViewState["RowIndex"] = (e.CommandArgument.ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditManualTest_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                ImageButton EditManualTest_adaptiveSelectImage = (ImageButton)e.Row.FindControl("EditManualTest_adaptiveSelectImage");
                string questionID = ((HiddenField)e.Row.FindControl("EditManualTest_adaptiveQuestionhiddenValue")).Value;
                EditManualTest_adaptiveSelectImage.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + questionID + "','" + EditManualTest_selectMultipleImage.ClientID + "');");
                ImageButton alertImage = (ImageButton)e.Row.FindControl("EditManualTest_adaptiveAlertImageButton");
                alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion('" + questionID.Split(':')[0] + "');");
                Image img = (Image)e.Row.FindControl("EditManualTest_adaptiveSelectImage");
                img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualTest_adaptiveGridView.ClientID + "');");
                img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" +
                    ((HiddenField)e.Row.FindControl("EditManualTest_adaptiveQuestionhiddenValue")).Value + "','" + EditManualTest_selectMultipleImage.ClientID + "');");
                e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                Label EditManualTest_questionLabel =
                     (Label)e.Row.FindControl("EditManualTest_adaptiveQuestionLabel");
                HoverMenuExtender EditManualTest_hoverMenuExtender = (HoverMenuExtender)
                    e.Row.FindControl("EditManualTest_adaptivePreviewhoverMenuExtender");
                EditManualTest_hoverMenuExtender.TargetControlID = EditManualTest_questionLabel.ID;
                PlaceHolder EditManualTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("EditManualTest_adaptiveAnswerChoicesPlaceHolder");
                EditManualTest_answerChoicesPlaceHolder.Controls.Add
                                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID.Split(':')[0]),true));
                HiddenField EditManualTest_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                       "EditManualTest_adaptiveCorrectAnswerHiddenField");
                List<AnswerChoice> answerChoice = new List<AnswerChoice>();
                answerChoice = new BL.QuestionBLManager().GetQuestionOptions(questionID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                        EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }
      
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditManualTest_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image img = (Image)e.Row.FindControl("EditManualTest_testDrftRemoveImage");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                    Label EditManualTest_questionLabel =
                         (Label)e.Row.FindControl("EditManualTest_draftQuestionLabel");

                    HoverMenuExtender EditManualTest_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("EditManualTest_draftHoverMenuExtender1");
                    EditManualTest_hoverMenuExtender.TargetControlID = EditManualTest_questionLabel.ID;
                   
                    //RadioButtonList EditManualTest_questionDetailPreviewControlAnswerRadioButtonList =
                   //     (RadioButtonList)e.Row.FindControl("QuestionDetailPreviewControl_draftAnswerRadioButtonList");
                    HiddenField EditManualTest_testDraftQuestionKeyHiddenField = (HiddenField)e.Row.FindControl(
                              "EditManualTest_testDraftQuestionKeyHiddenField");
                    HiddenField EditManualTest_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                             "EditManualTest_correctAnswerHiddenField");
                    string questionID = EditManualTest_testDraftQuestionKeyHiddenField.Value.Split(':')[0].ToString().Trim();
                    PlaceHolder EditManualTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("EditManualTest_answerChoicesPlaceHolder");
                    EditManualTest_answerChoicesPlaceHolder.Controls.Add
                                        (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID),true));

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditManualTest_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    EditManualTest_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    EditManualTest_ConfirmPopupPanel.Style.Add("height", "220px");
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    EditManualTest_ConfirmPopupExtenderControl.Message =string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message,e.CommandArgument.ToString().Split(':')[0]);
                    EditManualTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditManualTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditManualTest_ConfirmPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }
      
        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void EditManualTest_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                EditManualTest_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void EditManualTest_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                AddSearchQuestionToTestDraft();
                if (hiddenValue.Value != "")
                {
                    MoveToTestDraft("Add");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(EditManualTest_topErrorMessageLabel,
                                       EditManualTest_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_Search_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void EditManualTest_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in EditManualTest_testDrftGridView.Rows)
                    {
                        CheckBox EditManualTest_testDrftCheckbox = (CheckBox)row.FindControl("EditManualTest_testDrftCheckbox");
                        HiddenField EditManualTest_testDraftQuestionKeyHiddenField = (HiddenField)row.FindControl("EditManualTest_testDraftQuestionKeyHiddenField");
                        if (EditManualTest_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + EditManualTest_testDraftQuestionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }
                    }
                  //  hiddenValue.Value = questionID.TrimEnd(',');
                   
                }
                if (questionSelected)
                {
                    EditManualTest_ConfirmPopupPanel.Style.Add("height", "220px");
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    EditManualTest_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    EditManualTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditManualTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditManualTest_ConfirmPopupExtender.Show();  
                    //MoveToTestDraft("Delete");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(EditManualTest_topErrorMessageLabel,
                                       EditManualTest_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_TestDraft_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualTest_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (EditManualTest_simpleLinkButton.Text.ToLower() == "simple")
                {
                    EditManualTest_simpleLinkButton.Text = "Advanced";
                    EditManualTest_simpleSearchDiv.Visible = true;
                    EditManualTest_advanceSearchDiv.Visible = false;
                }
                else
                {
                    EditManualTest_simpleLinkButton.Text = "Simple";
                    EditManualTest_simpleSearchDiv.Visible = false;
                    EditManualTest_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualTest_pagingNavigator_PageNumberClick(object sender, 
            PageNumberEventArgs e)
        {
            //BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
            //    ((EditManualTest_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)),
            //    Convert.ToInt32(EditManualTest_adaptivePagingHiddenField.Value));
            MoveToTestDraft("");
            ViewState["PAGENUMBER"] = e.PageNumber;
            LoadQuestion(e.PageNumber, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
        }

        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditManualTest_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    EditTest();
                }
                else
                {
                    MoveToTestDraft("");
                    if( EditManualTest_questionDiv.Visible == true)
                        LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()), ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void EditManualTest_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl +"&showmessage=n" , false);
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualTest_adaptivepagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                BindSearchAndTestDraftGridView();
                EditManualTest_adaptivePagingHiddenField.Value = e.PageNumber.ToString();
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((EditManualTest_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)), 
                    e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void EditManualTest_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                EditManualTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter add Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateManualTest_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = EditManualTest_previewQuestionAddHiddenField.Value;
                EditManualTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter add Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void EditManualTest_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = EditManualTest_previewQuestionAddHiddenField.Value;
                EditManualTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the radio button list selected index changing event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void EditManualTest_adaptiveYesNoRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindSearchAndTestDraftGridView();
                if (EditManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                {
                    EditManualTest_adaptiveRecommendClickedHiddenField.Value = "";
                    EditManualTest_adaptivePagingHiddenField.Value = "1";
                    EditManualTest_adaptiveGridView.Visible = false;
                    ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] = null;
                    EditManualTest_recommendAdaptiveQuestions.Style.Add("display", "none");
                    BindEmptyAdaptiveGridView();
                    return;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((EditManualTest_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)),
                    Convert.ToInt32(EditManualTest_adaptivePagingHiddenField.Value));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void EditManualTest_questionDetail_TopAddButton_Click(object sender, EventArgs e)
        {
            //EditManualTest_adaptiveQuestionPreviewModalPopupExtender.Hide();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);
            //ViewState["RowIndex"] = 0;
        }
        
        #endregion Event Handlers

        #region Private  Methods                                                                      
        
        /// <summary>
        /// Modify the Test.
        /// </summary>
        private void EditTest()
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            List<decimal> creditEarned = new List<decimal>();
            List<decimal> timeTaken = new List<decimal>();
            string testKey = "";
            for (int i = 0; i < questionDetailSearchResultList.Count; i++)
            {
                creditEarned.Add(questionDetailSearchResultList[i].CreditsEarned);
                timeTaken.Add(questionDetailSearchResultList[i].AverageTimeTaken);
            }
           string complexity= new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailSearchResultList).AttributeID.ToString();
            int questionCount = Convert.ToInt32(questionDetailSearchResultList.Count.ToString());
            TestDetail testDetail = new TestDetail();
            testDetail = EditManualTest_testDetailsUserControl.TestDetailDataSource;
            int systemRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionCount;
            testDetail.Questions = questionDetailSearchResultList;
            testDetail.IsActive = true;
            testDetail.SystemRecommendedTime = systemRecommendedTime;
            testDetail.CreatedBy = userID;
            testDetail.IsDeleted = false;
            testDetail.ModifiedBy = userID;            
            testDetail.NoOfQuestions = questionCount;
            testDetail.SystemRecommendedTime = systemRecommendedTime;
            testDetail.TestAuthorID = userID;
            testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
            testDetail.TestCreationDate = DateTime.Now;
            testDetail.TestStatus = TestStatus.Active;
            testDetail.TestType = TestType.Genenal;
            testDetail.TestMode = "MANUAL";
            if (Request.QueryString.Get("type") == "copy")
            {
                testDetail.TestKey = testKey;

                TestBLManager testBLManager = new TestBLManager();
                testKey = testBLManager.SaveTest(testDetail, userID, complexity);

                // Check if the new saved test is equal to the parent test (based on questions).
                // If yes update the copy and parent test ID status.
                string parentTestKey = Request.QueryString["testkey"];
                testBLManager.UpdateCopyTestStatus(parentTestKey, testKey);

                EditManualTest_sessionInclucedKeyHiddenField.Value = "";
                Response.Redirect("EditManualTest.aspx?m=1&s=2&mode=copy&parentpage=" + parentPage + "&testkey=" + testKey, false);
            }
            else
            {
                if (EditManualTest_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
                {
                    // Check if feature usage limit exceeds for creating test.
                    if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                    {
                        base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                            EditManualTest_bottomSuccessMessageLabel,
                            "You have reached the maximum limit based your subscription plan. Cannot create more tests");
                        return;
                    }

                    testDetail.TestKey = testKey;
                    testKey = new TestBLManager().SaveTest(testDetail, userID, complexity);
                    EditManualTest_sessionInclucedKeyHiddenField.Value = "";
                    Response.Redirect(Constants.TestMakerConstants.EDIT_TEST_WA + "?m=1&s=2&mode=new&parentpage="
                        + parentPage + "&testkey=" + testKey, false);
                }
                else
                {
                    testDetail.TestKey = EditManualTest_questionKeyHiddenField.Value;
                    // Check if the question is modified by some body.
                    if (new CommonBLManager().IsRecordModified("TEST", testDetail.TestKey,
                        (DateTime)ViewState["MODIFIED_DATE"]))
                    {
                        ShowMessage(EditManualTest_topErrorMessageLabel,
                            EditManualTest_bottomErrorMessageLabel,
                                Resources.HCMResource.Edit_Test_Modifed_Another_User);
                        return;
                    }
                    new TestBLManager().UpdateTest(testDetail, userID,
                        complexity, EditManualTest_deletedQuestionHiddenField.Value.TrimEnd(','),
                        EditManualTest_insertedQuestionHiddenField.Value.TrimEnd(','));
                    EditManualTest_sessionInclucedKeyHiddenField.Value = "";
                    Response.Redirect(Constants.TestMakerConstants.EDIT_TEST_WA + "?m=1&s=" + Request.QueryString["s"] + "&mode=edit&parentpage=" + parentPage + "&testkey=" + testDetail.TestKey, false);
                }
            }
            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questionDetailSearchResultList;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailSearchResultList);
            EditManualTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                EditManualTest_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.CreateManualTest_AddedSuccessfully, testKey));
        }

        /// <summary>
        /// Expand or Restore the Result Grid 
        /// </summary>
        private void ExpandRestore()
        {
            if (EditManualTest_restoreHiddenField.Value == "Y")
            {
                EditManualTest_searchCriteriasDiv.Style["display"] = "none";
                EditManualTest_searchResultsUpSpan.Style["display"] = "block";
                EditManualTest_searchResultsDownSpan.Style["display"] = "none";
                EditManualTest_questionDiv.Style["height"] = EXPANDED_HEIGHT;
                EditManualTest_adaptiveQuestionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                EditManualTest_searchCriteriasDiv.Style["display"] = "block";
                EditManualTest_searchResultsUpSpan.Style["display"] = "none";
                EditManualTest_searchResultsDownSpan.Style["display"] = "block";
                EditManualTest_questionDiv.Style["height"] = RESTORED_HEIGHT;
                EditManualTest_adaptiveQuestionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (IsPostBack)
                return;
            EditManualTest_searchResultsUpImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                EditManualTest_questionDiv.ClientID + "','" +
                EditManualTest_searchCriteriasDiv.ClientID + "','" +
                EditManualTest_searchResultsUpSpan.ClientID + "','" +
                EditManualTest_searchResultsDownSpan.ClientID + "','" +
                EditManualTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                EditManualTest_adaptiveQuestionDiv.ClientID + "');");
            EditManualTest_searchResultsDownImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                EditManualTest_questionDiv.ClientID + "','" +
                EditManualTest_searchCriteriasDiv.ClientID + "','" +
                EditManualTest_searchResultsUpSpan.ClientID + "','" +
                EditManualTest_searchResultsDownSpan.ClientID + "','" +
                EditManualTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                EditManualTest_adaptiveQuestionDiv.ClientID + "');");
        }

        /// <summary>
        /// Reset the message labels.
        /// </summary>
        private void MessageLabel_Reset()
        {
            EditManualTest_topErrorMessageLabel.Text = string.Empty;
            EditManualTest_bottomErrorMessageLabel.Text = string.Empty;
            EditManualTest_topSuccessMessageLabel.Text = string.Empty;
            EditManualTest_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            StringBuilder sbQuestionIds = null;
            try
            {
                sbQuestionIds = new StringBuilder();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in EditManualTest_searchQuestionGridView.Rows)
                    {
                        CheckBox EditManualTest_searchQuestionCheckbox = (CheckBox)row.FindControl("EditManualTest_searchQuestionCheckbox");
                        HiddenField EditManualTest_searchQuestionKeyHiddenField = (HiddenField)row.FindControl("EditManualTest_searchQuestionKeyHiddenField");
                        if (EditManualTest_searchQuestionCheckbox.Checked)
                            sbQuestionIds.Append(EditManualTest_searchQuestionKeyHiddenField.Value + ",");
                    }
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                    if (EditManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return;
                    if (!EditManualTest_adaptiveGridView.Visible)
                        return;
                    for (int i = 0; i < EditManualTest_adaptiveGridView.Rows.Count; i++)
                    {
                        if (((CheckBox)(EditManualTest_adaptiveGridView.Rows[i].FindControl("EditManualTest_adaptiveCheckbox"))).Checked)
                            sbQuestionIds.Append(((HiddenField)(EditManualTest_adaptiveGridView.Rows[i].
                                FindControl("EditManualTest_adaptiveQuestionKeyHiddenField"))).Value + ",");
                    }
                    hiddenValue.Value = "";
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                }
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionIds)) sbQuestionIds = null;
            }
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (EditManualTest_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    EditManualTest_categoryTextBox.Text.Trim() == "" ?
                                    null : EditManualTest_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    EditManualTest_subjectTextBox.Text.Trim() == "" ?
                                    null : EditManualTest_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditManualTest_keywordsTextBox.Text.Trim() == "" ?
                                    null : EditManualTest_keywordsTextBox.Text.Trim();
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualTest_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in EditManualTest_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in EditManualTest_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    EditManualTest_questionIDTextBox.Text.Trim() == "" ?
                                    null : EditManualTest_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditManualTest_keywordTextBox.Text.Trim() == "" ?
                                    null : EditManualTest_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.AuthorName =
                                    EditManualTest_authorTextBox.Text.Trim() == "" ?
                                    null : EditManualTest_authorTextBox.Text.Trim();
                questionDetailSearchCriteria.ActiveFlag = null;
                questionDetailSearchCriteria.CreditsEarned =
                                    EditManualTest_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(EditManualTest_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.ClientRequest = EditManualTest_positionProfileKeywordTextBox.Text.Trim() == "" ?
                   null : EditManualTest_positionProfileKeywordTextBox.Text.Trim();
            }


            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail = new BL.TestBLManager().GetSearchQuestions(QuestionType.MultipleChoice, questionDetailSearchCriteria, base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                EditManualTest_searchQuestionGridView.DataSource = null;
                EditManualTest_searchQuestionGridView.DataBind();
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                 EditManualTest_bottomErrorMessageLabel,Resources.HCMResource.Common_Empty_Grid);
                EditManualTest_bottomPagingNavigator.TotalRecords = 0;
                EditManualTest_questionDiv.Visible = false;
            }
            else
            {
                EditManualTest_searchQuestionGridView.DataSource = questionDetail;
                EditManualTest_searchQuestionGridView.DataBind();
                EditManualTest_bottomPagingNavigator.PageSize = base.GridPageSize;
                EditManualTest_bottomPagingNavigator.TotalRecords = totalRecords;
                EditManualTest_questionDiv.Visible = true;
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
            // return questionDetail;
        }
       
        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            EditManualTest_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            EditManualTest_complexityCheckBoxList.DataTextField = "AttributeName";
            EditManualTest_complexityCheckBoxList.DataValueField = "AttributeID";
            EditManualTest_complexityCheckBoxList.DataBind();
        }
      
        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            EditManualTest_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
               //new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            EditManualTest_testAreaCheckBoxList.DataTextField = "AttributeName";
            EditManualTest_testAreaCheckBoxList.DataValueField = "AttributeID";
            EditManualTest_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < EditManualTest_testAreaCheckBoxList.Items.Count; i++)
            {

                if (EditManualTest_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(EditManualTest_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }
     
        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < EditManualTest_complexityCheckBoxList.Items.Count; i++)
            {
                if (EditManualTest_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(EditManualTest_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }
     
        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in EditManualTest_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditManualTest_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditManualTest_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in EditManualTest_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditManualTest_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditManualTest_adaptiveCheckbox")).Checked = false;
                }
            }

        }
     
        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            List<QuestionDetail> questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }
            int StartIndex = 0;
            int TotalCount = 0;
            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            if (ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] != null)
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailSearchResultList))
                    questionDetailSearchResultList = new List<QuestionDetail>();
                StartIndex = questionDetailSearchResultList.Count;
                questionDetailSearchResultList.AddRange((List<QuestionDetail>)ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE]);
                TotalCount = questionDetailSearchResultList.Count - StartIndex;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');

                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                        string[] idList = ids.Split(':');
                        string id = idList[0];
                        string relationId = idList[1];
                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualTest_insertedQuestionHiddenField.Value))
                                    {
                                        EditManualTest_insertedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    else
                                    {
                                        EditManualTest_insertedQuestionHiddenField.Value = EditManualTest_insertedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                                        EditManualTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                                    EditManualTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualTest_deletedQuestionHiddenField.Value))
                                {
                                    EditManualTest_deletedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                }
                                else
                                {
                                    EditManualTest_deletedQuestionHiddenField.Value = EditManualTest_deletedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                }
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            base.ShowMessage(EditManualTest_topSuccessMessageLabel,
                                        EditManualTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                        }

                    }
                }
            }
            if (EditManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
            {
                switch (action)
                {
                    case "Add":
                        if (addedQuestionID == "")
                        {
                            EditManualTest_adaptiveRecommendClickedHiddenField.Value = EditManualTest_adaptiveRecommendClickedHiddenField.Value;
                            EditManualTest_adaptivePagingHiddenField.Value = EditManualTest_adaptivePagingHiddenField.Value;
                        }
                        else
                        {
                            EditManualTest_adaptiveRecommendClickedHiddenField.Value = "";
                            EditManualTest_adaptivePagingHiddenField.Value = "1";
                        }
                        break;
                    case "Delete":
                        EditManualTest_adaptiveRecommendClickedHiddenField.Value = "";
                        EditManualTest_adaptivePagingHiddenField.Value = "1";
                        break;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((EditManualTest_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)),
                    Convert.ToInt32(EditManualTest_adaptivePagingHiddenField.Value));
            }
            if (alertFlag)
            {
                string alertMessage = string.Format(Resources.HCMResource.CreateManualTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                {
                    alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(','));
                }
                EditManualTest_ConfirmPopupPanel.Style.Add("height", "270px");
                EditManualTest_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                EditManualTest_ConfirmPopupExtenderControl.Title = "Warning";
                EditManualTest_ConfirmPopupExtenderControl.Message = alertMessage;
                EditManualTest_ConfirmPopupExtender.Show();

            }
            hiddenValue.Value = null;
            if (TotalCount != 0)
                questionDetailSearchResultList.RemoveRange(StartIndex, TotalCount);
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            EditManualTest_testDrftGridView.DataSource = questionDetailTestDraftResultList;
            EditManualTest_testDrftGridView.DataBind();
            //if (questionDetailSearchResultList != null)
            //{
            //    EditManualTest_searchQuestionGridView.DataSource = questionDetailSearchResultList;
            //    EditManualTest_questionDiv.Visible = true;
            //}
            //else
            //{
            //    EditManualTest_searchQuestionGridView.DataSource = null;
            //    EditManualTest_questionDiv.Visible = false;
            //}
            BindSearchAndTestDraftGridView();
            //EditManualTest_searchQuestionGridView.DataBind();

            if (action != "")
            {
                questionDetailSearchResultList = new List<QuestionDetail>();
                questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                List<decimal> timeTaken = new List<decimal>();

                for (int i = 0; i < questionDetailSearchResultList.Count; i++)
                {
                    timeTaken.Add(questionDetailSearchResultList[i].AverageTimeTaken);
                }
                if (questionDetailTestDraftResultList.Count > 0)
                {
                    EditManualTest_testDetailsUserControl.SysRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionDetailTestDraftResultList.Count;
                }
                else
                {
                    EditManualTest_testDetailsUserControl.SysRecommendedTime = 0;
                }
                TestDraftSummary testDraftSummary = new TestDraftSummary();
                testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList);
                EditManualTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;

            }

            UncheckCheckBox();
        }

        /// <summary>
        /// Helps to bind test question details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindTestQuestions(string testKey)
        {
            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail =
                new TestBLManager().GetTestQuestionDetail(testKey, "");
             TestDraftSummary testDraftSummary = new TestDraftSummary();
                testDraftSummary.QuestionDetail = questionDetail;
                testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetail);
                EditManualTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            //EditManualTest_testSummaryControl.LoadQuestionDetailsSummary(questionDetail, GetComplexity());
            ViewState["TESTDRAFTGRID"] = questionDetail;
        }

        /// <summary>
        /// Helps to bind test details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindTestDetails(string testKey)
        {
            TestDetail testDetail = new TestDetail();
            testDetail =
                new TestBLManager().GetTestAndCertificateDetail(testKey);
            EditManualTest_testDetailsUserControl.TestDetailDataSource = testDetail;
            EditManualTest_sessionInclucedKeyHiddenField.Value = testDetail.SessionIncluded.ToString();
            ViewState["MODIFIED_DATE"] = testDetail.ModifiedDate;
            if (Request.QueryString.Get("type") == "copy")
            {
                EditManualTest_topSaveButton.Text = "Copy Test";
                EditManualTest_bottomSaveButton.Text = "Copy Test";

                // Clear position profile details.
                EditManualTest_testDetailsUserControl.PositionProfileID = 0;
                EditManualTest_testDetailsUserControl.PositionProfileName = string.Empty;

                return;
            }
            if (EditManualTest_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                  EditManualTest_bottomErrorMessageLabel, string.Format(Resources.HCMResource.EditManualTest_TestAlreadyExists,testKey));
            }
            
        }

        #region Adaptive Recommendation Questions Method                       

        /// <summary>
        /// This method loads the search and test area grid view.
        /// </summary>
        protected void BindSearchAndTestDraftGridView()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
            {
                EditManualTest_searchQuestionGridView.DataSource = (List<QuestionDetail>)ViewState["SEARCHRESULTGRID"];
                EditManualTest_searchQuestionGridView.DataBind();
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["TESTDRAFTGRID"]))
            {
                EditManualTest_testDrftGridView.DataSource = (List<QuestionDetail>)ViewState["TESTDRAFTGRID"];
                EditManualTest_testDrftGridView.DataBind();
            }
        }

        /// <summary>
        /// This method gets the question id's that are in the draft panel
        /// </summary>
        /// <param name="questionDetails">List of question details that are in draft panel</param>
        /// <param name="ForceLoad">Whether the questions should load though 
        /// questions exceeded the adaptive limit.</param>
        /// <returns>string contains the draft question ids</returns>
        private string GetSelectedQuestionsIds(List<QuestionDetail> questionDetails, bool ForceLoad)
        {
            StringBuilder sbQuestionDetails = null;
            try
            {
                EditManualTest_recommendAdaptiveQuestions.Style.Add("display", "none");
                BindEmptyAdaptiveGridView();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetails))
                    return string.Empty;
                if (questionDetails.Count == 0)
                    return string.Empty;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"]))
                    return "";
                if ((ForceLoad == false) && (Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"])
                    < questionDetails.Count))
                {
                    if (EditManualTest_adaptiveYesNoRadioButtonList.SelectedIndex != 1)
                        EditManualTest_recommendAdaptiveQuestions.Style.Add("display", "block");
                    return "";
                }
                else
                    if (EditManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return "";
                sbQuestionDetails = new StringBuilder();
                for (int i = 0; i < questionDetails.Count; i++)
                {
                    sbQuestionDetails.Append(questionDetails[i].QuestionKey);
                    sbQuestionDetails.Append(",");
                }
                return sbQuestionDetails.ToString().TrimEnd(',');
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionDetails)) sbQuestionDetails = null;
            }
        }

        /// <summary>
        /// This will Make adaptive grid view empty
        /// </summary>
        private void BindEmptyAdaptiveGridView()
        {
            ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] = null;
            EditManualTest_adaptiveGridView.DataSource = null;
            EditManualTest_adaptiveGridView.DataBind();
            EditManualTest_adaptivebottomPagingNavigator.TotalRecords = 0;
            EditManualTest_adaptivebottomPagingNavigator.Reset();
        }

        /// <summary>
        /// This method communicates with the BL Manager and gets the questions
        /// </summary>
        /// <param name="SelectedQuestionIds">Questions that are in test draft panel</param>
        /// <param name="PageNumber">Page number to load in to the 
        /// adaptive grid.</param>
        private void BindAdaptiveQuestions(string SelectedQuestionIds, int PageNumber)
        {
            if (SelectedQuestionIds == "")
            {
                EditManualTest_adaptiveGridView.DataSource = null;
                EditManualTest_adaptiveGridView.DataBind();
                EditManualTest_adaptiveGridView.Visible = false;
                return;
            }
            int TotalNoOfRecords = 0;
            EditManualTest_adaptiveGridView.DataSource =
                new TestBLManager().GetAdaptiveQuestions(base.userID, SelectedQuestionIds,
                ADAPTIVE_PAGE_SIZE, PageNumber,base.userID, out TotalNoOfRecords);
            EditManualTest_adaptiveGridView.DataBind();
            if (TotalNoOfRecords == 0)
            {
                if (EditManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
                    EditManualTest_adaptiveGridView.Visible = true;
                else
                    EditManualTest_adaptiveGridView.Visible = false;
                EditManualTest_adaptivebottomPagingNavigator.TotalRecords = 0;
                EditManualTest_adaptivebottomPagingNavigator.Reset();
                EditManualTest_adaptivePagingHiddenField.Value = "1";
            }
            else
            {
                EditManualTest_adaptiveGridView.Visible = true;
                ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] =
                    (List<QuestionDetail>)EditManualTest_adaptiveGridView.DataSource;
                EditManualTest_adaptivebottomPagingNavigator.Visible = true;
                EditManualTest_adaptivePagingHiddenField.Value = PageNumber.ToString();
                if (PageNumber == 1)
                    EditManualTest_adaptivebottomPagingNavigator.Reset();
                else
                    EditManualTest_adaptivebottomPagingNavigator.MoveToPage(PageNumber);
                EditManualTest_adaptivebottomPagingNavigator.TotalRecords = TotalNoOfRecords;
                EditManualTest_adaptivebottomPagingNavigator.PageSize = ADAPTIVE_PAGE_SIZE;
            }
        }

        #endregion Adaptive Recommendation Questions Method

        #endregion

        #region Protected Overridden Methods                                                          

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;
            EditManualTest_topErrorMessageLabel.Text = string.Empty;
            EditManualTest_bottomErrorMessageLabel.Text = string.Empty;
            TestDetail testDetail = new TestDetail();
            testDetail = EditManualTest_testDetailsUserControl.TestDetailDataSource;
            int recommendedTime = testDetail.RecommendedCompletionTime;

            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailSearchResultList))
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }
            else if (questionDetailSearchResultList.Count == 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }

            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Name_Empty);
                isValidData = false;
            }
            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                   EditManualTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Description_Empty);
                isValidData = false;
            }

            if (recommendedTime == -1)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
               EditManualTest_bottomErrorMessageLabel,
               Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_Empty);
                isValidData = false;
            }
            else if (recommendedTime == -2)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
               EditManualTest_bottomErrorMessageLabel,
               string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended"));
                isValidData = false;
            }
            else if (recommendedTime == 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                EditManualTest_bottomErrorMessageLabel,
                Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                isValidData = false;
            }
            if (isQuestionTabActive)
                EditManualTest_mainTabContainer.ActiveTab = EditManualTest_questionsTabPanel;
            else
                EditManualTest_mainTabContainer.ActiveTab = EditManualTest_testdetailsTabPanel;
            if (!(bool)testDetail.IsCertification)
                return isValidData;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateDetailsEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateID <= 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateFormatEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.DaysElapseBetweenRetakes < 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyDaysRetakeTest);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.PermissibleRetakes <= 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyRetakes);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MinimumTotalScoreRequired <= 0)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_MinimumQualifacationEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateValidity == "0")
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                     Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateValidity);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -2)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                        EditManualTest_bottomErrorMessageLabel,
                        string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended certification completion"));
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -1)
            {
                base.ShowMessage(EditManualTest_topErrorMessageLabel,
                    EditManualTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateAutomaticTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                isValidData = false;
            }
            if ((testDetail.RecommendedCompletionTime > 0) && (testDetail.CertificationDetail.MaximumTimePermissible > 0))
            {
                if (testDetail.RecommendedCompletionTime < testDetail.CertificationDetail.MaximumTimePermissible)
                {
                    base.ShowMessage(EditManualTest_topErrorMessageLabel,
                        EditManualTest_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificationTest_CertificationTimeExceeds);
                    isValidData = false;
                }
            }
            if (isQuestionTabActive)
                EditManualTest_mainTabContainer.ActiveTab = EditManualTest_questionsTabPanel;
            else
                EditManualTest_mainTabContainer.ActiveTab = EditManualTest_testdetailsTabPanel;
            EditManualTest_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;
        }
        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
        }

        #endregion Protected Overridden Methods

    }
}
