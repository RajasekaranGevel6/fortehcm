﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.TestMaker {
    
    
    public partial class SearchTestRecommendation {
        
        /// <summary>
        /// SearchTestRecommendation_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchTestRecommendation_headerLiteral;
        
        /// <summary>
        /// SearchTestRecommendation_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchTestRecommendation_topResetLinkButton;
        
        /// <summary>
        /// SearchTestRecommendation_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchTestRecommendation_topCancelLinkButton;
        
        /// <summary>
        /// SearchTestRecommendation_topMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchTestRecommendation_topMessageUpdatePanel;
        
        /// <summary>
        /// SearchTestRecommendation_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_topSuccessMessageLabel;
        
        /// <summary>
        /// SearchTestRecommendation_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_topErrorMessageLabel;
        
        /// <summary>
        /// SearchTestRecommendation_searchCriteriasDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchTestRecommendation_searchCriteriasDiv;
        
        /// <summary>
        /// SearchTestRecommendation_criteriaUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchTestRecommendation_criteriaUpdatePanel;
        
        /// <summary>
        /// SearchTestRecommendation_statusLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_statusLabel;
        
        /// <summary>
        /// SearchTestRecommendation_statusDropDownList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList SearchTestRecommendation_statusDropDownList;
        
        /// <summary>
        /// SearchTestRecommendation_statusHelpImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton SearchTestRecommendation_statusHelpImageButton;
        
        /// <summary>
        /// SearchTestRecommendation_testNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_testNameLabel;
        
        /// <summary>
        /// SearchTestRecommendation_testNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchTestRecommendation_testNameTextBox;
        
        /// <summary>
        /// SearchTestRecommendation_testDescriptionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_testDescriptionLabel;
        
        /// <summary>
        /// SearchTestRecommendation_testDescriptionTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchTestRecommendation_testDescriptionTextBox;
        
        /// <summary>
        /// SearchTestRecommendation_skillLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_skillLabel;
        
        /// <summary>
        /// SearchTestRecommendation_skillTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchTestRecommendation_skillTextBox;
        
        /// <summary>
        /// SearchTestRecommendation_searchButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SearchTestRecommendation_searchButton;
        
        /// <summary>
        /// SearchTestRecommendation_searchResultsTR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow SearchTestRecommendation_searchResultsTR;
        
        /// <summary>
        /// SearchTestRecommendation_testRecommendationsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchTestRecommendation_testRecommendationsLiteral;
        
        /// <summary>
        /// SearchTestRecommendation_testRecommendationsSortHelpLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_testRecommendationsSortHelpLabel;
        
        /// <summary>
        /// SearchTestRecommendation_searchResultsUpSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchTestRecommendation_searchResultsUpSpan;
        
        /// <summary>
        /// SearchTestRecommendation_searchResultsUpImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image SearchTestRecommendation_searchResultsUpImage;
        
        /// <summary>
        /// SearchTestRecommendation_searchResultsDownSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchTestRecommendation_searchResultsDownSpan;
        
        /// <summary>
        /// SearchTestRecommendation_searchResultsDownImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image SearchTestRecommendation_searchResultsDownImage;
        
        /// <summary>
        /// SearchTestRecommendation_restoreHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchTestRecommendation_restoreHiddenField;
        
        /// <summary>
        /// SearchTestRecommendation_isMaximizedHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchTestRecommendation_isMaximizedHiddenField;
        
        /// <summary>
        /// SearchTestRecommendation_testRecommendationsGridViewUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchTestRecommendation_testRecommendationsGridViewUpdatePanel;
        
        /// <summary>
        /// SearchTestRecommendation_testRecommendationsGridViewDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchTestRecommendation_testRecommendationsGridViewDiv;
        
        /// <summary>
        /// SearchTestRecommendation_testRecommendationsGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView SearchTestRecommendation_testRecommendationsGridView;
        
        /// <summary>
        /// SearchTestRecommendation_pagingNavigator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PageNavigator SearchTestRecommendation_pagingNavigator;
        
        /// <summary>
        /// SearchTestRecommendation_bottomMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchTestRecommendation_bottomMessageUpdatePanel;
        
        /// <summary>
        /// SearchTestRecommendation_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_bottomSuccessMessageLabel;
        
        /// <summary>
        /// SearchTestRecommendation_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchTestRecommendation_bottomErrorMessageLabel;
        
        /// <summary>
        /// SearchTestRecommendation_bottomResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchTestRecommendation_bottomResetLinkButton;
        
        /// <summary>
        /// SearchTestRecommendation_bottomCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchTestRecommendation_bottomCancelLinkButton;
        
        /// <summary>
        /// SearchTestRecommendation_changeTestStatusUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchTestRecommendation_changeTestStatusUpdatePanel;
        
        /// <summary>
        /// SearchTestRecommendation_changeTestStatusHiddenButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SearchTestRecommendation_changeTestStatusHiddenButton;
        
        /// <summary>
        /// SearchTestRecommendation_changeTestStatusPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel SearchTestRecommendation_changeTestStatusPanel;
        
        /// <summary>
        /// SearchTestRecommendation_changeTestStatusConfirmMsgControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ConfirmMsgControl SearchTestRecommendation_changeTestStatusConfirmMsgControl;
        
        /// <summary>
        /// SearchTestRecommendation_changeTestStatusTestRecommendationIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchTestRecommendation_changeTestStatusTestRecommendationIDHiddenField;
        
        /// <summary>
        /// SearchTestRecommendation_changeTestStatusTestStatusHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchTestRecommendation_changeTestStatusTestStatusHiddenField;
        
        /// <summary>
        /// SearchTestRecommendation_changeTestStatusModalPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender SearchTestRecommendation_changeTestStatusModalPopupExtender;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.SiteAdminMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.SiteAdminMaster)(base.Master));
            }
        }
    }
}
