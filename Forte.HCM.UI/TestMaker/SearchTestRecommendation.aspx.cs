﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchTestRecommendation.aspx.cs
// File that represents the user interface layout and functionalities for
// the SearchTestRecommendation page. This page helps in searching for 
// self admin test recommendations.

#endregion Header

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the SearchTestRecommendation page. This page helps in searching for 
    /// self admin test recommendations. This class inherits the
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchTestRecommendation: PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that is called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Search Test Recommendation");

                // Set default button & focus.
                Page.Form.DefaultButton = SearchTestRecommendation_searchButton.UniqueID;
                SearchTestRecommendation_statusDropDownList.Focus();

                if (!IsPostBack)
                {
                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                    }

                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "TEST_NAME";

                    // Assign handler for expand/shrink results.
                    SearchTestRecommendation_searchResultsTR.Attributes.Add("onclick",
                        "ExpandOrRestore('" +
                        SearchTestRecommendation_testRecommendationsGridViewDiv.ClientID + "','" +
                        SearchTestRecommendation_searchCriteriasDiv.ClientID + "','" +
                        SearchTestRecommendation_searchResultsUpSpan.ClientID + "','" +
                        SearchTestRecommendation_searchResultsDownSpan.ClientID + "','" +
                        SearchTestRecommendation_isMaximizedHiddenField.ClientID + "','" +
                        RESTORED_HEIGHT + "','" +
                        EXPANDED_HEIGHT + "')");

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_RECOMMENDATION] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_RECOMMENDATION]
                            as TestRecommendationDetailSearchCriteria);
                }

                // Set the expand or restore state.
                CheckAndSetExpandOrRestore();

                // Subscribes to paging event.
                SearchTestRecommendation_pagingNavigator.PageNumberClick += new 
                    PageNavigator.PageNumberClickEventHandler(SearchTestRecommendation_pagingNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the page number is clicked
        /// in the paging control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that holds the event data.
        /// </param>
        private void SearchTestRecommendation_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the search button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTestRecommendation_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearLabelMessage();

                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "TEST_NAME";

                // Load tests
                LoadTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the reset link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTestRecommendation_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                base.ClearSearchCriteriaSession();
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row created event is fired 
        /// in the test recommendations grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void SearchTestRecommendation_testRecommendationsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchTestRecommendation_testRecommendationsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row command event is fired
        /// in the test recommendations grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void SearchTestRecommendation_testRecommendationsGridView_RowCommand
           (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                // Check if the command name is valid.
                if (e.CommandName != "ViewTest" && e.CommandName != "EditTest" && e.CommandName != "ActivateTest"
                    && e.CommandName != "DeactivateTest" && e.CommandName != "CopyTest" && e.CommandName != "TestSummary")
                {
                    return;
                }

                // Clear messages.
                ClearLabelMessage();

                int index = Convert.ToInt32(e.CommandArgument);
                int testRecommendationID = Convert.ToInt32((SearchTestRecommendation_testRecommendationsGridView.Rows
                    [index].FindControl("SearchTestRecommendation_testRecommendationsGridViewDiv_testRecommendationIDHiddenField") as HiddenField).Value);

                if (e.CommandName == "ViewTest")
                {
                    // Redirects to the view test recommendation page.
                    string url = "~/TestMaker/ViewTestRecommendation.aspx" +
                        "?m=3&s=1" +
                        "&id=" + testRecommendationID +
                        "&parentpage=" + Constants.ParentPage.SEARCH_TEST_RECOMMENDATION;

                    // Redirect.
                    Response.Redirect(url, false);
                }
                else if (e.CommandName == "EditTest")
                {
                    // Redirects to the edit test recommendation page.
                    string url = "~/TestMaker/TestRecommendation.aspx" +
                        "?m=3&s=1&mode=E" +
                        "&id=" + testRecommendationID +
                        "&parentpage=" + Constants.ParentPage.SEARCH_TEST_RECOMMENDATION;

                    // Redirect.
                    Response.Redirect(url, false);
                }
                else if (e.CommandName == "ActivateTest")
                {
                    SearchTestRecommendation_changeTestStatusConfirmMsgControl.Message = "Are you sure to activate this test ?";
                    SearchTestRecommendation_changeTestStatusConfirmMsgControl.Type = MessageBoxType.YesNo;
                    SearchTestRecommendation_changeTestStatusConfirmMsgControl.Title = "Activate Test";
                    SearchTestRecommendation_changeTestStatusTestRecommendationIDHiddenField.Value = testRecommendationID.ToString();
                    SearchTestRecommendation_changeTestStatusTestStatusHiddenField.Value = "Y";

                    // Show the popup.
                    SearchTestRecommendation_changeTestStatusModalPopupExtender.Show();
                }
                else if (e.CommandName == "DeactivateTest")
                {
                    SearchTestRecommendation_changeTestStatusConfirmMsgControl.Message = "Are you sure to deactivate this test ?";
                    SearchTestRecommendation_changeTestStatusConfirmMsgControl.Type = MessageBoxType.YesNo;
                    SearchTestRecommendation_changeTestStatusConfirmMsgControl.Title = "Deactivate Test";
                    SearchTestRecommendation_changeTestStatusTestRecommendationIDHiddenField.Value = testRecommendationID.ToString();
                    SearchTestRecommendation_changeTestStatusTestStatusHiddenField.Value = "N";

                    // Show the popup.
                    SearchTestRecommendation_changeTestStatusModalPopupExtender.Show();
                }
                else if (e.CommandName == "CopyTest")
                {
                    // Redirects to the edit test recommendation page.
                    string url = "~/TestMaker/TestRecommendation.aspx" +
                        "?m=3&s=1&mode=C" +
                        "&id=" + testRecommendationID +
                        "&parentpage=" + Constants.ParentPage.SEARCH_TEST_RECOMMENDATION;

                    // Redirect.
                    Response.Redirect(url, false);
                }
                else if (e.CommandName == "TestSummary")
                {
                    // Redirects to the edit test recommendation page.
                    string url = "~/TestMaker/TestRecommendationSummary.aspx" +
                        "?m=3&s=1" +
                        "&id=" + testRecommendationID +
                        "&parentpage=" + Constants.ParentPage.SEARCH_TEST_RECOMMENDATION;

                    // Redirect.
                    Response.Redirect(url, false);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the sorting event is fired in 
        /// the test recommendations grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchTestRecommendation_testRecommendationsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                // Check whether the sort field in view state is same as 
                // the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                // Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchTestRecommendation_pagingNavigator.Reset();

                // Set question details based on the values.
                LoadTests(1);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row data bound event is
        /// fired in the test recommendationsgrid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchTestRecommendation_testRecommendationsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in the
        /// activate/deactive test popup.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Based on the test recommendation ID and status kept in the hidden
        /// field the test will be either activated or deactivated.
        /// </remarks>
        protected void SearchTestRecommendation_changeTestStatusConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                // Update the test status.
                new TestRecommendationBLManager().UpdateSelfAdminTestRecommendationStatus
                    (Convert.ToInt32(SearchTestRecommendation_changeTestStatusTestRecommendationIDHiddenField.Value),
                    SearchTestRecommendation_changeTestStatusTestStatusHiddenField.Value.Trim().ToUpper(), base.userID);

                // Reload the data for the current page.
                LoadTests(Convert.ToInt32(ViewState["PAGE_NUMBER"]));

                // Show a success message.
                if (SearchTestRecommendation_changeTestStatusTestStatusHiddenField.Value.Trim().ToUpper() == "N")
                {
                    base.ShowMessage(SearchTestRecommendation_topSuccessMessageLabel, 
                        SearchTestRecommendation_bottomSuccessMessageLabel, "Test deactivated successfully");
                }
                else
                {
                    base.ShowMessage(SearchTestRecommendation_topSuccessMessageLabel,
                        SearchTestRecommendation_bottomSuccessMessageLabel, "Test activated successfully");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            return true;
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that fills the search criteria and apply the search.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="TestRecommendationDetailSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <remarks>
        /// This will be done only if the return request is made to this page (for eg from
        /// coming back from edit test recommendation page).
        /// </remarks>
        private void FillSearchCriteria(TestRecommendationDetailSearchCriteria criteria)
        {
            // Apply search criteria.
            SearchTestRecommendation_testNameTextBox.Text = criteria.TestName;
            SearchTestRecommendation_testDescriptionTextBox.Text = criteria.TestDescription;
            SearchTestRecommendation_skillTextBox.Text = criteria.Skill;

            if (!Utility.IsNullOrEmpty(criteria.Status))
                SearchTestRecommendation_statusDropDownList.SelectedValue = criteria.Status;
            else
                SearchTestRecommendation_statusDropDownList.SelectedValue = string.Empty;

            SearchTestRecommendation_isMaximizedHiddenField.Value = criteria.IsMaximized ? "Y" : "N";

            ViewState["SORT_ORDER"] = criteria.SortType;
            ViewState["SORT_FIELD"] = criteria.SortField;

            // Apply search.
            LoadTests(criteria.PageNumber);

            // Highlight the last page number which is stored in session
            // when the page is launched from somewhere else.
            SearchTestRecommendation_pagingNavigator.MoveToPage(criteria.PageNumber);
        }

        /// <summary>
        /// Method that loads the time slots for the given page number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadTests(int pageNumber)
        {
            int totalRecords = 0;

            // Keep the page number in view state.
            ViewState["PAGE_NUMBER"] = pageNumber;

            // Construct search criteria.
            TestRecommendationDetailSearchCriteria criteria = new TestRecommendationDetailSearchCriteria();

            criteria.TestName = SearchTestRecommendation_testNameTextBox.Text.Trim() == string.Empty ?
                null : SearchTestRecommendation_testNameTextBox.Text.Trim();

            criteria.TestDescription = SearchTestRecommendation_testDescriptionTextBox.Text.Trim() == string.Empty ?
                null : SearchTestRecommendation_testDescriptionTextBox.Text.Trim();

            criteria.Skill = SearchTestRecommendation_skillTextBox.Text.Trim() == string.Empty ?
                null : SearchTestRecommendation_skillTextBox.Text.Trim();

            if (!Utility.IsNullOrEmpty(SearchTestRecommendation_statusDropDownList.SelectedValue))
                criteria.Status = SearchTestRecommendation_statusDropDownList.SelectedValue.Trim();
            else
                criteria.Status = string.Empty;

            criteria.PageNumber = pageNumber;
            criteria.IsMaximized = Utility.IsNullOrEmpty(SearchTestRecommendation_isMaximizedHiddenField.Value) ? false :
                (SearchTestRecommendation_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false);

            criteria.SortType = (SortType)ViewState["SORT_ORDER"];
            criteria.SortField = ViewState["SORT_FIELD"].ToString();

            // Keep the search criteria in session.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_RECOMMENDATION] = criteria;

            List<TestRecommendationDetail> timeSlots = new TestRecommendationBLManager().GetSelfAdminTestRecommendations
                (criteria, pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                (SortType)ViewState["SORT_ORDER"], out totalRecords);

            if (timeSlots == null || timeSlots.Count == 0)
            {
                base.ShowMessage(SearchTestRecommendation_topErrorMessageLabel,
                    SearchTestRecommendation_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }

            SearchTestRecommendation_testRecommendationsGridView.DataSource = timeSlots;
            SearchTestRecommendation_testRecommendationsGridView.DataBind();
            SearchTestRecommendation_pagingNavigator.TotalRecords = totalRecords;
            SearchTestRecommendation_pagingNavigator.PageSize = base.GridPageSize;
        }

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            SearchTestRecommendation_topSuccessMessageLabel.Text = string.Empty;
            SearchTestRecommendation_bottomSuccessMessageLabel.Text = string.Empty;
            SearchTestRecommendation_topErrorMessageLabel.Text = string.Empty;
            SearchTestRecommendation_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Methodt that sets the expand or restore state.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(SearchTestRecommendation_isMaximizedHiddenField.Value) &&
                SearchTestRecommendation_isMaximizedHiddenField.Value == "Y")
            {
                SearchTestRecommendation_searchCriteriasDiv.Style["display"] = "none";
                SearchTestRecommendation_searchResultsUpSpan.Style["display"] = "block";
                SearchTestRecommendation_searchResultsDownSpan.Style["display"] = "none";
                SearchTestRecommendation_testRecommendationsGridViewDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchTestRecommendation_searchCriteriasDiv.Style["display"] = "block";
                SearchTestRecommendation_searchResultsUpSpan.Style["display"] = "none";
                SearchTestRecommendation_searchResultsDownSpan.Style["display"] = "block";
                SearchTestRecommendation_testRecommendationsGridViewDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods
    }
}