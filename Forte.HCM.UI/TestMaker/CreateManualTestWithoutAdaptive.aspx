<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    Inherits="Forte.HCM.UI.TestMaker.CreateManualTestWithoutAdaptive" CodeBehind="CreateManualTestWithoutAdaptive.aspx.cs" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/TestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ManualTestSummaryControl.ascx" TagName="ManualTestSummaryControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc6" %>
<%@ Register Src="../CommonControls/QuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControlOpenText.ascx" TagName="QuestionDetailPreviewControlOpenText"
    TagPrefix="uc8" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ContentPlaceHolderID="OTMMaster_body" ID="CreateManualTestWithoutAdaptive_content"
    runat="server">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var control;
        function setFocus(sender, e) {
            try {
                var activeTab = $find('<%=CreateManualTestWithoutAdaptive_mainTabContainer.ClientID%>').get_activeTabIndex();
                if (activeTab == "0") {
                    try {
                        control = $get('<%=CreateManualTestWithoutAdaptive_categoryTextBox.ClientID%>');
                        control.focus();
                    }
                    catch (er1) {
                        try {
                            control = $get('<%=CreateManualTestWithoutAdaptive_keywordTextBox.ClientID%>');
                            control.focus();
                        }
                        catch (er2) {
                        }
                    }
                }
                else {
                    control = $get('ctl00_OTMMaster_body_CreateManualTestWithoutAdaptive_mainTabContainer_CreateManualTestWithoutAdaptive_testdetailsTabPanel_CreateManualTestWithoutAdaptive_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                    control.focus();
                }
            }
            catch (er) {
            }
        }

    </script>
    <script type="text/javascript" language="javascript">

        // Display  Or Hide the adaptive TestDraft Or Manual test Draft Questions.
        function ShowOrHideDiv(caseValue) {
            switch (caseValue.toString()) {
                case '0':
                    {
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.display = "block";
                        document.getElementById("CreateManualTestWithoutAdaptive_adaptiveTd").style.display = "none";
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.width = "100%";

                        document.getElementById("<%= CreateManualTestWithoutAdaptive_questionDiv.ClientID %>").style.width = "936px";
                        document.getElementById("<%= CreateManualTestWithoutAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "0";
                        break;
                    }
                case '1':
                    {
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.display = "none";
                        document.getElementById("CreateManualTestWithoutAdaptive_adaptiveTd").style.display = "block";
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.width = "0";
                        document.getElementById("CreateManualTestWithoutAdaptive_adaptiveTd").style.width = "100%";

                        document.getElementById("<%= CreateManualTestWithoutAdaptive_questionDiv.ClientID %>").style.width = "0";
                        document.getElementById("<%= CreateManualTestWithoutAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "936px";
                        break;
                    }
                case '2':
                    {
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.display = "block";
                        document.getElementById("CreateManualTestWithoutAdaptive_adaptiveTd").style.display = "block";
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.width = "49%";
                        document.getElementById("CreateManualTestWithoutAdaptive_adaptiveTd").style.width = "50%";

                        document.getElementById("<%= CreateManualTestWithoutAdaptive_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateManualTestWithoutAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
                default:
                    {
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.display = "block";
                        document.getElementById("CreateManualTestWithoutAdaptive_adaptiveTd").style.display = "block";
                        document.getElementById("CreateManualTestWithoutAdaptive_manualTd").style.width = "49%";
                        document.getElementById("CreateManualTestWithoutAdaptive_adaptiveTd").style.width = "50%";

                        document.getElementById("<%= CreateManualTestWithoutAdaptive_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateManualTestWithoutAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
            }
            return false;
        }

        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID) {
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source) {

            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID)) {
                CheckNoOfChecked(QuestionID);
            }
            else {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id) {

            var table = sourceTable;
            var testVar = document.getElementById('<%=CreateManualTestWithoutAdaptive_searchQuestionGridView.ClientID %>');
            var testVarTwo = document.getElementById('<%=CreateManualTestWithoutAdaptive_adaptiveGridView.ClientID %>');
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                if (id == i) {
                    var row = table.rows[i];
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox) {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;
                    }
                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable)) {
                    CheckNoOfChecked(id);
                }
            }
        }

        //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }
        // Question remove from the Test Draft.
        function RemoveRows(rowIndex) {
            sourceTable = document.getElementById('<%=CreateManualTestWithoutAdaptive_testDrftGridView.ClientID %>');

            if (confirm('Are you sure to delete these question(s)?')) {
                if (checkChecked(rowIndex)) {
                    DelRows();

                }
                else {
                    CheckCorrectQuestion(rowIndex);
                    DelRows();
                }
            }
            else
                return false;
        }

        function DelRows() {
            try {
                var table = document.getElementById('<%=CreateManualTestWithoutAdaptive_testDrftGridView.ClientID %>');
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox;
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }
                    if (null != chkbox && true == chkbox.checked) {
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        }

        function CheckNoOfChecked(QuestionID) {

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=CreateManualTestWithoutAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move ' + numberOfQuestions + ' question(s)';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser()) {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                    }
                    else {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[4].childNodes[0].value + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[0].value + ',';
                        }
                    }
                }

            }
            //var ShowDiv =
        }



        function checkChecked(id) {

            var isChecked = false;

            var table = sourceTable;
            // document.getElementById('<%=CreateManualTestWithoutAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    isChecked = true;
                }

                if (isChecked) {
                    return true;
                }
                else {
                    return false;
                }
            }

        }

        function mouseMove(ev) {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject) {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser()) {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else {

            }
            return false;
        }
        function mouseCoords(ev) {
            if (ev != null) {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev) {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=CreateManualTestWithoutAdaptive_testDrftGridView.ClientID %>');
            var buttonID = document.getElementById('<%=CreateManualTestWithoutAdaptive_selectMultipleImage.ClientID %>');
            var targPos = getPosition(curTarget);
            var scrolltargPos;
            if (detectBrowser()) {
                scrolltargPos = targPos.y;
            }
            else {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos)) {

                // __doPostBack('ctl00_HCMMaster_body_CreateManualTestWithoutAdaptive_mainTabContainer_CreateManualTestWithoutAdaptive_questionsTabPanel_CreateManualTestWithoutAdaptive_selectMultipleImage', "OnClick");
                __doPostBack('<%= CreateManualTestWithoutAdaptive_selectMultipleImage.ClientID %>', "OnClick");

                dragObject.style.display = 'none';
            }
            else {
                if (dragObject != null) {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale() {
            var table = sourceTable;
            //document.getElementById('<%=CreateManualTestWithoutAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e) {
            var left = 0;
            var top = 0;

            if (e != null) {
                while (e.offsetParent) {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }
                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }

        function checkCount(event) {
            var count = 0;
            if (event.checked) {
                count = 1;
            }
            var row = event.parentNode.parentNode;
            //Get the reference of GridView
            var GridView = row.parentNode;

            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //The First element is the Header Checkbox
                var headerCheckBox = inputList[0];

                //Based on all or none checkboxes
                //are checked check/uncheck Header Checkbox                            
                if (inputList[i].type == "checkbox") {// && inputList[i] != headerCheckBox) {                  
                    if (inputList[i].checked) {
                        count++;
                    }
                }
            }
            var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');
            noOfQuestionsTextBox.style.display = 'block';
            noOfQuestionsTextBox.value = 'selected ' + count + ' question(s)';
        }
    </script>
    <asp:Panel ID="CreateManualTest_mainPanel" runat="server" DefaultButton="CreateManualTestWithoutAdaptive_bottomSaveButton">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="68%" class="header_text_bold">
                                <asp:Literal ID="CreateManualTestWithoutAdaptive_headerLiteral" runat="server" Text="Create Manual Test"></asp:Literal>
                            </td>
                            <td width="32%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="20%" align="right">
                                            <asp:Button ID="CreateManualTestWithoutAdaptive_topSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateManualTestWithoutAdaptive_saveButton_Click" />
                                            <asp:Button ID="CreateManualTestWithoutAdaptive_topCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="48%" align="center">
                                            <asp:LinkButton ID="CreateManualTestWithoutAdaptive_adptiveTestLinkButton" SkinID="sknActionLinkButton"
                                                Visible="false" runat="server" Text="Create Test With Adaptive" OnClick="CreateManualTestWithoutAdaptive_adptiveTestLinkButton_Click">
                                            </asp:LinkButton>
                                        </td>
                                        <td width="1%" align="center">
                                            |
                                        </td>
                                        <td width="12%" align="right">
                                            <asp:LinkButton ID="CreateManualTestWithoutAdaptive_topResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateManualTestWithoutAdaptive_resetLinkButton_Click" />
                                        </td>
                                        <td width="1%" align="center">
                                            |
                                        </td>
                                        <td width="10%" align="left">
                                            <asp:LinkButton ID="CreateManualTestWithoutAdaptive_topCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="CreateManualTestWithoutAdaptive_topSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateManualTestWithoutAdaptive_topErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label><asp:HiddenField ID="CreateManualTestWithoutAdaptive_stateHiddenField"
                                    runat="server" Value="0" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolKit:TabContainer ID="CreateManualTestWithoutAdaptive_mainTabContainer" runat="server"
                        ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                        <ajaxToolKit:TabPanel ID="CreateManualTestWithoutAdaptive_questionsTabPanel" HeaderText="questions"
                            runat="server">
                            <HeaderTemplate>
                                Questions
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="CreateManualTest_testDetailsPanel" runat="server" DefaultButton="CreateManualTestWithoutAdaptive_topSearchButton">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed">
                                        <tr>
                                            <td>
                                                <div id="CreateManualTestWithoutAdaptive_searchCriteriasDiv" runat="server" style="display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" class="td_padding_bottom_5">
                                                                <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_simpleLinkButtonUpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="CreateManualTestWithoutAdaptive_simpleLinkButton" runat="server"
                                                                            Text="Advanced" SkinID="sknActionLinkButton" OnClick="CreateManualTestWithoutAdaptive_simpleLinkButton_Click" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_searchDiv_UpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="CreateManualTestWithoutAdaptive_questionTypeDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                            <tr>
                                                                                                <td style="width: 10%">
                                                                                                    <div style="float: left; padding-top: 4px">
                                                                                                        <asp:Label ID="CreateAutomaticTest_questionTypeLabel" runat="server" Text="Question Type"
                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                                                                    </div>
                                                                                                    <div style="float: left; padding-left: 5px; padding-top: 2px">
                                                                                                        <asp:DropDownList ID="CreateManualTestWithoutAdaptive_questionTypeDropDownList" SkinID="sknQuestionTypeDropDown"
                                                                                                            runat="server" AutoPostBack="false" EnableViewState="true">
                                                                                                            <asp:ListItem Text="Multiple Choice" Value="1"></asp:ListItem>
                                                                                                            <asp:ListItem Text="Open Text" Value="2"></asp:ListItem>
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div style="padding-top: 4px">
                                                                                    </div>
                                                                                    <div id="CreateManualTestWithoutAdaptive_simpleSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_categoryHeadLabel" runat="server"
                                                                                                        Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateManualTestWithoutAdaptive_categoryTextBox" runat="server"
                                                                                                        MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_subjectHeadLabel" runat="server" Text="Subject"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateManualTestWithoutAdaptive_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_keywordsHeadLabel" runat="server"
                                                                                                        Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <div style="float: left; padding-right: 5px;">
                                                                                                        <asp:TextBox ID="CreateManualTestWithoutAdaptive_keywordsTextBox" runat="server"
                                                                                                            MaxLength="100"></asp:TextBox></div>
                                                                                                    <div style="float: left;">
                                                                                                        <asp:ImageButton ID="CreateManualTestWithoutAdaptive_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div id="CreateManualTestWithoutAdaptive_advanceSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <uc1:CategorySubjectControl ID="CreateManualTestWithoutAdaptive_categorySubjectControl"
                                                                                                        runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_8">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="panel_inner_body_bg">
                                                                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_testAreaHeadLabel" runat="server"
                                                                                                                    Text="Test Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="5" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                                                <asp:CheckBoxList ID="CreateManualTestWithoutAdaptive_testAreaCheckBoxList" runat="server"
                                                                                                                    RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                </asp:CheckBoxList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_complexityLabel" runat="server" Text="Complexity"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3" align="left" valign="middle">
                                                                                                                <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                                    class="checkbox_list_bg">
                                                                                                                    <asp:CheckBoxList ID="CreateManualTestWithoutAdaptive_complexityCheckBoxList" runat="server"
                                                                                                                        RepeatColumns="4" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                    </asp:CheckBoxList>
                                                                                                                </div>
                                                                                                                <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                                    <asp:ImageButton ID="CreateManualTestWithoutAdaptive_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualTestWithoutAdaptive_keywordTextBox" runat="server" Columns="50"
                                                                                                                        MaxLength="100"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualTestWithoutAdaptive_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionLabel" runat="server" Text="Question ID"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="CreateManualTestWithoutAdaptive_questionIDTextBox" runat="server"
                                                                                                                    MaxLength="15"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_creditHeadLabel" runat="server" Text="Credit"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="CreateManualTestWithoutAdaptive_creditTextBox" runat="server"></asp:TextBox>
                                                                                                                <ajaxToolKit:FilteredTextBoxExtender ID="CreateManualTestWithoutAdaptive_weightageFileteredExtender"
                                                                                                                    runat="server" TargetControlID="CreateManualTestWithoutAdaptive_creditTextBox"
                                                                                                                    FilterType="Numbers" Enabled="True">
                                                                                                                </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_authorHeadLabel" runat="server" Text="Author"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualTestWithoutAdaptive_authorTextBox" runat="server"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="CreateManualTestWithoutAdaptive_dummyAuthorID" runat="server" />
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualTestWithoutAdaptive_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_positionProfileLabel" runat="server"
                                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Position Profile"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3">
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualTestWithoutAdaptive_positionProfileTextBox" runat="server"
                                                                                                                        MaxLength="200" Columns="55"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualTestWithoutAdaptive_positionProfileImageButton"
                                                                                                                        SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" /></div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_positionProfileKeywordLabel" runat="server"
                                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Position Profile Keyword"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px; width: 100%">
                                                                                                                    <asp:TextBox ID="CreateManualTestWithoutAdaptive_positionProfileKeywordTextBox" runat="server"
                                                                                                                        MaxLength="500"></asp:TextBox>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="td_padding_top_5">
                                                                            <asp:Button ID="CreateManualTestWithoutAdaptive_topSearchButton" runat="server" Text="Search"
                                                                                SkinID="sknButtonId" OnClick="CreateManualTestWithoutAdaptive_searchQuestionButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr id="CreateManualTestWithoutAdaptive_searchResultsTR" runat="server">
                                            <td class="header_bg" runat="server">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 80%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="CreateManualTestWithoutAdaptive_searchResultsLiteral" runat="server"
                                                                Text="Questions"></asp:Literal>
                                                            &#160;<asp:Label ID="CreateManualTestWithoutAdaptive_sortHelpLabel" runat="server"
                                                                SkinID="sknLabelText"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualTestWithoutAdaptive_manualImageButton" runat="server"
                                                                SkinID="sknManualImageButton" ToolTip="Click here to view 'Search Results' only"
                                                                OnClientClick="javascript:return ShowOrHideDiv('0');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualTestWithoutAdaptive_adaptiveImageButton" runat="server"
                                                                SkinID="sknAdaptiveImageButton" ToolTip="Click here to view 'Adaptive Recommended Questions' only"
                                                                OnClientClick="javascript:return ShowOrHideDiv('1');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualTestWithoutAdaptive_bothImageButton" runat="server"
                                                                SkinID="sknCombinedAMImageButton" ToolTip="Click here to view both 'Search Results' and 'Adaptive Recommended Questions'"
                                                                OnClientClick="javascript:return ShowOrHideDiv('2');" />
                                                        </td>
                                                        <td style="width: 5%" align="right">
                                                            <span id="CreateManualTestWithoutAdaptive_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="CreateManualTestWithoutAdaptive_searchResultsUpImage" runat="server"
                                                                    SkinID="sknMinimizeImage" />
                                                            </span><span id="CreateManualTestWithoutAdaptive_searchResultsDownSpan" runat="server"
                                                                style="display: block;">
                                                                <asp:Image ID="CreateManualTestWithoutAdaptive_searchResultsDownImage" runat="server"
                                                                    SkinID="sknMaximizeImage" />
                                                            </span>
                                                            <asp:HiddenField ID="CreateManualTestWithoutAdaptive_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td valign="top">
                                                <div id="CreateManualTestWithoutAdaptive_manualTd" runat="server" style="width: 100%;
                                                    float: left; display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateManualTestWithoutAdaptive_manualQuestLiteral" runat="server"
                                                                                Text="Search Results"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg" valign="top">
                                                                <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_searchQuestionGridView_UpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 180px; width: 939px; overflow: auto;" runat="server" id="CreateManualTestWithoutAdaptive_questionDiv">
                                                                                        <asp:GridView ID="CreateManualTestWithoutAdaptive_searchQuestionGridView" runat="server"
                                                                                            OnRowDataBound="CreateManualTestWithoutAdaptive_searchQuestionGridView_RowDataBound"
                                                                                            OnRowCommand="CreateManualTestWithoutAdaptive_searchQuestionGridView_RowCommand"
                                                                                            OnSorting="CreateManualTestWithoutAdaptive_searchQuestionGridView_Sorting">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="CreateManualTestWithoutAdaptive_searchQuestionCheckbox" runat="server"
                                                                                                            onchange="javascript: checkCount(this);" />
                                                                                                        <asp:HiddenField ID="CreateManualTestWithoutAdaptive_questionKeyHiddenField" runat="server"
                                                                                                            Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTestWithoutAdaptive_previewImageButton" runat="server"
                                                                                                            SkinID="sknPreviewImageButton" CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                            ToolTip="Preview Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTestWithoutAdaptive_searchQuestionAlertImageButton"
                                                                                                            runat="server" SkinID="sknAlertImageButton" ToolTip="Flag Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTestWithoutAdaptive_searchQuestionSelectImage" runat="server"
                                                                                                            SkinID="sknMoveDown_ArrowImageButton" CommandName="Select" ToolTip="Select Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="CreateManualTestWithoutAdaptive_hidddenValue" runat="server"
                                                                                                            Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                        <asp:Panel ID="CreateManualTestWithoutAdaptive_hoverPanel" CssClass="table_outline_bg"
                                                                                                            runat="server" Width="750px">
                                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                                                <tr>
                                                                                                                    <th class="popup_question_icon">
                                                                                                                        <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                            <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailPreviewControlQuestionLabel"
                                                                                                                                Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                                                                                runat="server" SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                        </div>
                                                                                                                    </th>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th class="td_padding_left_20">
                                                                                                                        <asp:PlaceHolder ID="CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder" runat="server">
                                                                                                                        </asp:PlaceHolder>
                                                                                                                        <asp:Label ID="CreateManualTestWithoutAdaptive_AnswerLabel1" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                            Text="Answer:"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                                                                        <asp:Literal ID="CreateManualTestWithoutAdaptive_questionAnswer" runat="server" SkinID="sknMultiLineText"
                                                                                                                            Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                                    </th>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </asp:Panel>
                                                                                                        <ajaxToolKit:HoverMenuExtender ID="CreateManualTestWithoutAdaptive_hoverMenuExtender"
                                                                                                            runat="server" PopupControlID="CreateManualTestWithoutAdaptive_hoverPanel" PopupPosition="Bottom"
                                                                                                            HoverCssClass="popupHover" TargetControlID="CreateManualTestWithoutAdaptive_questionLabel"
                                                                                                            PopDelay="50" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Question">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTestWithoutAdaptive_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),60) %>'
                                                                                                            Width="320px"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Category">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTestWithoutAdaptive_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTestWithoutAdaptive_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Answer">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTestWithoutAdaptive_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                            Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTestWithoutAdaptive_complexityLabel1" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                            Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" runat="server" id="CreateManualTestWithoutAdaptive_searchResultCountDiv"
                                                                                    visible="false">
                                                                                    No.of Records:
                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_searchResultCountLabel" runat="server"
                                                                                         ></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <uc2:PageNavigator ID="CreateManualTestWithoutAdaptive_bottomPagingNavigator" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <div id="moveDisplayDIV" runat="server">
                                                                                <asp:TextBox ID="sampleDIVTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </div>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="CreateManualTestWithoutAdaptive_topSearchButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="CreateManualTestWithoutAdaptive_adaptiveTd" style="width: 0%; float: right;
                                                    display: none;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateManualTestWithoutAdaptive_adaptiveQuestLiteral" runat="server"
                                                                                Text="Adaptive Recommended Questions"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                            <td class="grid_body_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 180px; width: 0px; overflow: auto;" runat="server" id="CreateManualTestWithoutAdaptive_adaptiveQuestionDiv">
                                                                                <asp:GridView ID="CreateManualTestWithoutAdaptive_adaptiveGridView" runat="server"
                                                                                    OnRowDataBound="CreateManualTestWithoutAdaptive_adaptiveGridView_RowDataBound"
                                                                                    OnRowCommand="CreateManualTestWithoutAdaptive_adaptiveGridView_RowCommand" OnSorting="CreateManualTestWithoutAdaptive_adaptiveGridView_Sorting"
                                                                                    EnableModelValidation="True">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="CreateManualTestWithoutAdaptive_adaptiveCheckbox" runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualTestWithoutAdaptive_adptivepreviewImageButton" runat="server"
                                                                                                    SkinID="sknPreviewImageButton" CommandName="view" ToolTip="Preview Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualTestWithoutAdaptive_adaptiveAlertImageButton" runat="server"
                                                                                                    SkinID="sknAlertImageButton" ToolTip="Flag Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualTestWithoutAdaptive_adaptiveSelectImage" runat="server"
                                                                                                    SkinID="sknMoveDown_ArrowImageButton" ToolTip="Select Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="CreateManualTestWithoutAdaptive_hidddenValue" runat="server"
                                                                                                    Value='<%#Eval("QuestionID") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualTestWithoutAdaptive_adaptiveQuestionDetailsImage"
                                                                                                    runat="server" SkinID="sknDraftImageButton" ToolTip="Adaptive Question Details"
                                                                                                    CommandName="AdaptiveQuestion" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                        <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Question">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionLabel" runat="server" Width="340px"
                                                                                                    Text='<%# TrimContent(Eval("Question").ToString(),65) %>'></asp:Label>
                                                                                                <asp:Panel ID="CreateManualTestWithoutAdaptive_hoverPanel" runat="server" CssClass="table_outline_bg">
                                                                                                    <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                                        <tr>
                                                                                                            <td class="popup_question_icon">
                                                                                                                <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailPreviewControlQuestionLabel"
                                                                                                                        Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                                                                        runat="server" SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="td_padding_left_20">
                                                                                                                <asp:RadioButtonList ID="CreateManualTestWithoutAdaptive_questionPreviewControlAnswerRadioButtonList"
                                                                                                                    runat="server" RepeatColumns="1" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                                    TextAlign="Right" Width="100%">
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:Panel>
                                                                                                <ajaxToolKit:HoverMenuExtender ID="CreateManualTestWithoutAdaptive_hoverMenuExtender1"
                                                                                                    runat="server" PopupControlID="CreateManualTestWithoutAdaptive_hoverPanel" PopupPosition="Left"
                                                                                                    HoverCssClass="popupHover" TargetControlID="CreateManualTestWithoutAdaptive_hoverPanel"
                                                                                                    PopDelay="50" />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Category" InsertVisible="False" DataField="Categories">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="140px"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Subject" InsertVisible="False" DataField="subject">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="140px" Wrap="False"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Answer" InsertVisible="False" DataField="CorrectAnswer">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="120px" Wrap="False"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Complexity" InsertVisible="False" DataField="Complexity">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <asp:HiddenField ID="CreateManualTestWithoutAdaptive_adaptiveGridViewHiddenField"
                                                                                runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <uc2:PageNavigator ID="CreateManualTestWithoutAdaptive_adaptivebottomPagingNavigator"
                                                                                runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td align="center" class="grid_header_bg">
                                                <table width="10%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="CreateManualTestWithoutAdaptive_selectMultipleImage" runat="server"
                                                                SkinID="sknMultiDown_ArrowImageButton" CommandName="Select" ToolTip="Move questions to test draft"
                                                                OnClick="CreateManualTestWithoutAdaptive_selectMultipleImage_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="CreateManualTestWithoutAdaptive_removeMultipleImage" runat="server"
                                                                SkinID="sknMultiUp_ArrowImageButton" CommandName="Select" ToolTip="Remove questions from test draft"
                                                                OnClick="CreateManualTestWithoutAdaptive_removeMultipleImage_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_testDrftGridView_UpdatePanel"
                                                                runat="server">
                                                                <ContentTemplate>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                                            <td class="grid_header_bg">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="header_text_bold">
                                                                                            <asp:Literal ID="CreateManualTestWithoutAdaptive_testDraftLiteral" runat="server"
                                                                                                Text="Test Draft"></asp:Literal>&nbsp;
                                                                                            <asp:Label ID="CreateManualTestWithoutAdaptive_testDrafttHelpLabel" runat="server"
                                                                                                SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"
                                                                                                Visible="false"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr onmouseup="javascript:srsmouseup(event);" onmousemove="javascript:mouseMove(event);"
                                                                            style="height: 35px">
                                                                            <td class="grid_body_bg" valign="top">
                                                                                <div style="height: 200px; width: 939px; overflow: auto; table-layout: fixed; width: 100%">
                                                                                    <asp:GridView ID="CreateManualTestWithoutAdaptive_testDrftGridView" OnRowDataBound="CreateManualTestWithoutAdaptive_testDrftGridView_RowDataBound"
                                                                                        runat="server" OnRowCommand="CreateManualTestWithoutAdaptive_testDrftGridView_RowCommand"
                                                                                        OnSorting="CreateManualTestWithoutAdaptive_testDrftGridView_Sorting" EmptyDataText="&nbsp;">
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="CreateManualTestWithoutAdaptive_testDrftCheckbox" runat="server" />
                                                                                                    <asp:HiddenField ID="CreateManualTestWithoutAdaptive_questionKeyHiddenField" runat="server"
                                                                                                        Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="CreateManualTestWithoutAdaptive_testDrftRemoveImage" runat="server"
                                                                                                        SkinID="sknDeleteImageButton" CommandName="Select" ToolTip="Remove Question"
                                                                                                        CommandArgument='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                                                DataField="QuestionID" Visible="False">
                                                                                                <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                            <asp:TemplateField HeaderText="Question">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_draftQuestionLabel" runat="server"
                                                                                                        Text='<%# TrimContent(Eval("Question").ToString(),70) %>' Width="350px"></asp:Label>
                                                                                                    <asp:Panel ID="CreateManualTestWithoutAdaptive_hoverPanel" runat="server" Width="750px"
                                                                                                        CssClass="table_outline_bg">
                                                                                                        <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                                                                                            <tr>
                                                                                                                <th class="popup_question_icon">
                                                                                                                    <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                        <asp:Label ID="CreateManualTestWithoutAdaptive_draftPreviewQuestionLabel" runat="server"
                                                                                                                            SkinID="sknLabelFieldMultiText" Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'></asp:Label>
                                                                                                                    </div>
                                                                                                                </th>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th class="td_padding_left_20">
                                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_AnswerLabel1" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Answer:"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                                                                    <asp:PlaceHolder ID="CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder" runat="server">
                                                                                                                    </asp:PlaceHolder>
                                                                                                                    <asp:Literal ID="CreateManualTestWithoutAdaptive_questionAnswer" runat="server" SkinID="sknMultiLineText"
                                                                                                                        Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                                                    <asp:HiddenField ID="CreateManualTes1tWithoutAdaptive_correctAnswerHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("Answer") %>' />
                                                                                                                </th>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:Panel>
                                                                                                    <ajaxToolKit:HoverMenuExtender ID="CreateManualTestWithoutAdaptive_draftHoverMenuExtender1"
                                                                                                        runat="server" PopupControlID="CreateManualTestWithoutAdaptive_hoverPanel" PopupPosition="Bottom"
                                                                                                        HoverCssClass="popupHover" TargetControlID="CreateManualTestWithoutAdaptive_draftQuestionLabel"
                                                                                                        PopDelay="50" />
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle Wrap="true" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Category">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                        Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle Wrap="False" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Subject">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                        Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle Wrap="False" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Answer">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                        Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle Wrap="False" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Complexity">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_complexityLabel2" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                        Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle Wrap="False" />
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" runat="server" id="CreateManualTestWithoutAdaptive_testDrftGridView_RecordCountLabelDiv"
                                                                                visible="false">
                                                                                No.of.Records:
                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_testDrftGridView_RecordCountLabel"
                                                                                    runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <uc2:PageNavigator ID="CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator"
                                                                                    runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="CreateManualTestWithoutAdaptive_selectMultipleImage" />
                                                                    <asp:AsyncPostBackTrigger ControlID="CreateManualTestWithoutAdaptive_removeMultipleImage" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_testSummaryUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <uc5:ManualTestSummaryControl ID="CreateManualTestWithoutAdaptive_testSummaryControl"
                                                            runat="server" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="CreateManualTestWithoutAdaptive_testdetailsTabPanel" runat="server">
                            <HeaderTemplate>
                                Test Details
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="width: 100%">
                                            <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_testDetailsUserControl_UpdatePanel"
                                                runat="server">
                                                <ContentTemplate>
                                                    <uc4:TestDetailsControl ID="CreateManualTestWithoutAdaptive_testDetailsUserControl"
                                                        runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_bottomMessageupdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="CreateManualTestWithoutAdaptive_bottomSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateManualTestWithoutAdaptive_bottomErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="22%">
                                        </td>
                                        <td width="40%" align="right">
                                            <asp:Button ID="CreateManualTestWithoutAdaptive_bottomSaveButton" runat="server"
                                                SkinID="sknButtonId" Text="Save" OnClick="CreateManualTestWithoutAdaptive_saveButton_Click" />
                                            <asp:Button ID="CreateManualTestWithoutAdaptive_bottomCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="16%" align="right">
                                            <asp:LinkButton ID="CreateManualTestWithoutAdaptive_bottompResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateManualTestWithoutAdaptive_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateManualTestWithoutAdaptive_bottomCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="CreateManualTestWithoutAdaptive_questionDetailPanel" runat="server"
                                                CssClass="popupcontrol_question_draft" Style="display: none">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="td_height_20">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                        <asp:Literal ID="CreateManualTestWithoutAdaptive_questionDetailResultLiteral" runat="server"
                                                                            Text="Question Detail"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CreateManualTestWithoutAdaptive_questionDetailPreviewControlTopCancelImageButton"
                                                                                        runat="server" SkinID="sknCloseImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataGrid ID="CreateManualTestWithoutAdaptive_questionDetailCategoryDataGrid"
                                                                                        runat="server" AutoGenerateColumns="false">
                                                                                        <Columns>
                                                                                            <asp:BoundColumn HeaderText="Category" DataField="Category"></asp:BoundColumn>
                                                                                            <asp:BoundColumn HeaderText="Subject" DataField="Subject"></asp:BoundColumn>
                                                                                        </Columns>
                                                                                    </asp:DataGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_panel_inner_bg">
                                                                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailTestAreaHeadLabel" runat="server"
                                                                                                    Text="Test Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailTestAreaLabel" runat="server"
                                                                                                    Text="Concept" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailNoOfTestLabel" runat="server"
                                                                                                    Text="Number Of Administered Test " SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailTestAdministeredLabel"
                                                                                                    runat="server" Text="5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailAvgTimeLabel" runat="server"
                                                                                                    Text="Average Time Taken(in minutes)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailAvgTimeValueLabel" runat="server"
                                                                                                    Text="15.5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailCorrectToAttendedRatioHeadLabel"
                                                                                                    runat="server" Text=" Ratio of Correct Answer To Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailCorrectToAttendedRatioLabel"
                                                                                                    runat="server" Text="36:50" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailComplexityHeadLabel"
                                                                                                    runat="server" Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailPreviewControlComplexityLabel"
                                                                                                    runat="server" Text="Normal" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_question_icon">
                                                                                    <asp:Label ID="CreateManualTestWithoutAdaptive_questionDetailQuestionLabel" runat="server"
                                                                                        SkinID="sknLabelText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_padding_left_20">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <asp:RadioButtonList ID="CreateManualTestWithoutAdaptive_questionDetailAnswerRadioButtonList"
                                                                                            runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                            TextAlign="Right" Width="100%">
                                                                                        </asp:RadioButtonList>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="padding-left: 30px">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="CreateManualTestWithoutAdaptive_questionDetailTopAddButton" runat="server"
                                                                                        Text="Add" SkinID="sknButtonId" OnClick="CreateManualTestWithoutAdaptive_questionDetailTopAddButton_Click" />
                                                                                </td>
                                                                                <td style="padding-left: 10px">
                                                                                    <asp:LinkButton ID="CreateManualTestWithoutAdaptive_questionDetailTopCancelButton"
                                                                                        runat="server" SkinID="sknCancelLinkButton" Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div style="display: none">
                                                <asp:Button ID="CreateManualTestWithoutAdaptive_adaptiveQuestionPreviewPanelTargeButton"
                                                    runat="server" />
                                            </div>
                                            <ajaxToolKit:ModalPopupExtender ID="CreateManualTestWithoutAdaptive_adaptiveQuestionPreviewModalPopupExtender"
                                                runat="server" PopupControlID="CreateManualTestWithoutAdaptive_questionDetailPanel"
                                                TargetControlID="CreateManualTestWithoutAdaptive_adaptiveQuestionPreviewPanelTargeButton"
                                                BackgroundCssClass="modalBackground" CancelControlID="CreateManualTestWithoutAdaptive_questionDetailTopCancelButton">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_questionDetailPreviewUpdatePanel"
        runat="server">
        <ContentTemplate>
            <asp:Panel ID="CreateManualTestWithoutAdaptive_questionPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <div style="display: none">
                    <asp:Button ID="CreateManualTestWithoutAdaptive_questionModalTargeButton" runat="server" />
                </div>
                <asp:HiddenField ID="CreateManualTestWithoutAdaptive_previewQuestionAddHiddenField"
                    runat="server" />
                <%--<uc3:QuestionDetailPreviewControl ID="CreateManualTestWithoutAdaptive_questionDetailPreviewControl"
                    runat="server" OnCancelClick="CreateManualTestWithoutAdaptive_cancelClick" />--%>
                <div id="CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlDiv" runat="server"
                    style="display: none">
                    <uc7:QuestionDetailSummaryControl ID="CreateManualTestWithoutAdaptive_questionDetailSummaryControl"
                        runat="server" Title="Question Details Summary" OnCancelClick="CreateManualTestWithoutAdaptive_cancelClick"
                        OnAddClick="CreateManualTestWithoutAdaptive_questionDetailPreviewControl_AddClick" />
                </div>
                <div id="CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenTextDiv"
                    runat="server" style="display: none">
                    <uc8:QuestionDetailPreviewControlOpenText ID="CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenText"
                        Title="Question Details Summary" runat="server" OnCancelClick="CreateManualTestWithoutAdaptive_cancelClick" />
                </div>
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateManualTestWithoutAdaptive_questionModalPopupExtender"
                runat="server" PopupControlID="CreateManualTestWithoutAdaptive_questionPanel"
                TargetControlID="CreateManualTestWithoutAdaptive_questionModalTargeButton" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="CreateManualTestWithoutAdaptive_ConfirmPopupUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="CreateManualTestWithoutAdaptive_authorIdHiddenField" runat="server" />
            <asp:HiddenField ID="CreateManualTestWithoutAdaptive_positionProfileHiddenField"
                runat="server" />
            <asp:HiddenField ID="hiddenValue" runat="server" />
            <asp:HiddenField ID="hiddenDeleteValue" runat="server" />
            <div id="CreateManualTestWithoutAdaptive_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="CreateManualTestWithoutAdaptive_hiddenPopupModalButton" runat="server" />
            </div>
            <asp:Panel ID="CreateManualTestWithoutAdaptive_ConfirmPopupPanel" runat="server"
                Style="display: none" CssClass="popupcontrol_confirm_remove">
                <uc6:ConfirmMsgControl ID="CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl"
                    runat="server" OnOkClick="CreateManualTestWithoutAdaptive_okClick" OnCancelClick="CreateManualTestWithoutAdaptive_cancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateManualTestWithoutAdaptive_ConfirmPopupExtender"
                runat="server" PopupControlID="CreateManualTestWithoutAdaptive_ConfirmPopupPanel"
                TargetControlID="CreateManualTestWithoutAdaptive_hiddenPopupModalButton" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=CreateManualTestWithoutAdaptive_categoryTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('<%=CreateManualTestWithoutAdaptive_keywordTextBox.ClientID %>').focus();
                }
                catch (Err1) {
                    document.getElementById('ctl00_OTMMaster_body_CreateManualTestWithoutAdaptive_mainTabContainer_CreateManualTestWithoutAdaptive_testdetailsTabPanel_CreateManualTestWithoutAdaptive_testDetailsUserControl_TestDetailsControl_testNameTextBox').focus();

                }
            }
        }
    </script>
</asp:Content>
