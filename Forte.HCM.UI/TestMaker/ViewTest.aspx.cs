﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewTest.aspx.cs
// File that represents the user interface for displaying test detail.
// This will also used to show the question detail against the test key.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the ViewTest page. This page allows us to view the test details
    /// and question information which are involved in the particular test. 
    /// And,questions can be sorted based on the key words which are present
    /// in the question tab. This page also provides the links for editing, 
    /// deleting, activating/deactivating a test. It can allow us to create 
    /// a new test session for the test which is in active status. This class
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ViewTest : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Handler which will be called when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that keeps the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Load default values whenever the page is post backed.
                // For example, page title, default button settings etc.
                LoadValues();

                if (!IsPostBack)
                {
                    // Check whether a testkey is not null
                    if (Request.QueryString["testkey"] != null)
                        LoadTestCertificateDetails(Request.QueryString["testkey"].ToString().Trim());

                    // Make first item to be selected in Group By dropdown
                    ViewTest_groupByDDL.SelectedIndex = 0;

                    // Call BindTestQuestions method. This method will load the question
                    // detail according to the sortkey and test key.
                    BindTestQuestions(Request.QueryString["testkey"].ToString().Trim(),
                        ViewTest_groupByDDL.SelectedValue);

                    // The querystring will hold the deleted key means the test is deleted.
                    // In this case, the 'successful deleted' message will be shown in the page.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["deletedstatus"]) &&
                        Request.QueryString["deletedstatus"].ToUpper() == "DEL")
                    {
                        base.ShowMessage(ViewTest_topSuccessMessageLabel, ViewTest_bottomSuccessMessageLabel,
                           string.Format(Resources.HCMResource.ViewTest_TestIsSuccessfullyDeleted,
                           Request.QueryString["testkey"].ToString().Trim()));
                    }
                }

                if (Request.QueryString.Get("type") == "copy")
                {
                    ViewTest_topInactivetestButton.Visible = false;
                    ViewTest_topDeleteTestButton.Visible = false;
                    ViewTest_topActiveTestButton.Visible = false;
                    ViewTest_topCreateTestSessionButton.Visible = false;

                    ViewTest_bottomDeleteTestButton.Visible = false;
                    ViewTest_bottomInactiveTestButton.Visible = false;
                    ViewTest_bottomCreateTestSessionButton.Visible = false;
                    ViewTest_bottomEditTestButton.Visible = true;
                    ViewTest_topEditTestButton.Visible = true;
                    ViewTest_bottomEditTestButton.Text = "Copy Test";
                    ViewTest_topEditTestButton.Text = "Copy Test";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This event handler helps to find a question number label 
        /// to bind the row number as question serial no(Eg: 1,2...)
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that contains the sender of the event
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> object which holds the event data.
        /// </param>
        protected void ViewTest_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // Find the row type is DataRow
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Find a label from the current row.
                Label ViewTest_rowNoLabel = (Label)e.Row.FindControl("ViewTest_rowNoLabel");

                // Assign the row no to the label.
                ViewTest_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();

                // Display the image if any
                QuestionDetail questionDetail = ((QuestionDetail)e.Row.DataItem);
                Image ViewTest_questionImageDisplay = (Image)e.Row.FindControl("ViewTest_questionImageDisplay");
                if (((QuestionDetail)e.Row.DataItem).HasImage)
                {
                    ViewTest_questionImageDisplay.Visible = true;
                    ViewTest_questionImageDisplay.ImageUrl
                        = @"~/Common/ImageHandler.ashx?questionKey="
                            + questionDetail.QuestionKey
                            + "&isThumb=0&source=VIEWTEST_QUESTION_IMAGE";
                }
                else
                {
                    ViewTest_questionImageDisplay.Visible = false;
                }
            }

        }

        /// <summary>
        /// Event handler that calls the activate/deactivate popup
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that can contain the event data.
        /// </param>
        protected void ViewTest_Active_Button_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear success label message
                ViewTest_topSuccessMessageLabel.Text = string.Empty;
                ViewTest_bottomSuccessMessageLabel.Text = string.Empty;

                // Get the currently clicked button object and its command name.
                // Because both (Activate/Deactivate) buttons are using the same
                // event handler.
                Button btn = (sender) as Button;
                string btnCommand = btn.CommandName;

                // Check the command name is deactivate test.
                // If it is, modify the value of hidden field as activate, 
                // popup control title, message and content of the popup
                if (btnCommand == "DEACTIVATE_TEST")
                {
                    ViewTest_testStatusHiddenField.Value = "ACTIVATE";
                    ViewTest_inactivePopupExtenderControl.Title = "Deactivate Test";
                    ViewTest_inactivePopupExtenderControl.Message =
                        string.Format(Resources.HCMResource.SearchTest_DeActivate_Confirm_Message,
                        Request.QueryString["testkey"].ToString().Trim());

                    ViewTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    ViewTest_activeInActivepopupExtender.Show();
                }
                else
                {
                    // This part of the code for activate button
                    ViewTest_testStatusHiddenField.Value = "DEACTIVATE";
                    ViewTest_inactivePopupExtenderControl.Title = "Activate Test";
                    ViewTest_inactivePopupExtenderControl.Message =
                        string.Format(Resources.HCMResource.SearchTest_Activate_Confirm_Message,
                        Request.QueryString["testkey"].ToString().Trim());

                    ViewTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    ViewTest_activeInActivepopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This popup extender event handler is used to activate/deactivate
        /// and delete a test.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event handler.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ViewTest_OkClick(object sender, EventArgs e)
        {
            try
            {
                TestBLManager testBLManager = new TestBLManager();

                // Retrieve the test action(i.e. ACTIVATE,DEACTIVATE,DELETE), test key, 
                // and assign them to the respective hidden fields
                string ViewTest_Action = ViewTest_testStatusHiddenField.Value.ToString().Trim();
                string testKey = ViewTest_testKeyHiddenField.Value.ToString().Trim();

                // Check if the test status is ACTIVATE
                if (ViewTest_Action == "ACTIVATE")
                {
                    // Call a method to deactivate a test
                    testBLManager.DeactivateTest(testKey, base.userID);

                    // Show success message
                    base.ShowMessage(ViewTest_topSuccessMessageLabel,
                    ViewTest_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.ViewTest_TestIsDeactivated, testKey));

                    // Hide the deactivate buttons and show the activate buttons
                    ViewTest_bottomInactiveTestButton.Visible = false;
                    ViewTest_topInactivetestButton.Visible = false;

                    ViewTest_topActiveTestButton.Visible = true;
                    ViewTest_bottomActiveTestButton.Visible = true;

                    // Set visibility FALSE for edit, create test session buttons
                    ViewTest_bottomEditTestButton.Visible = false;
                    ViewTest_topEditTestButton.Visible = false;
                    ViewTest_topCreateTestSessionButton.Visible = false;
                    ViewTest_bottomCreateTestSessionButton.Visible = false;
                }
                // Check if the test status is DEACTIVATE
                else if (ViewTest_Action == "DEACTIVATE")
                {
                    // Call a method to activate a test
                    testBLManager.ActivateTest(testKey, base.userID);

                    // Show success message
                    base.ShowMessage(ViewTest_topSuccessMessageLabel,
                     ViewTest_bottomSuccessMessageLabel,
                     string.Format(Resources.HCMResource.ViewTest_TestIsActivated, testKey));

                    // Hide the activate buttons and show the deactivate buttons
                    ViewTest_bottomInactiveTestButton.Visible = true;
                    ViewTest_topInactivetestButton.Visible = true;

                    ViewTest_topActiveTestButton.Visible = false;
                    ViewTest_bottomActiveTestButton.Visible = false;

                    // Set visibility for edit, create test session buttons
                    ViewTest_bottomEditTestButton.Visible = true;
                    ViewTest_topEditTestButton.Visible = true;
                    ViewTest_topCreateTestSessionButton.Visible = true;
                    ViewTest_bottomCreateTestSessionButton.Visible = true;
                }
                // Check if the action is DELETE_TEST.
                // Note: A test can only be deleted if it is not included in session.
                // If this is included, the DELETE TEST button will not be shown in UI.
                else if (ViewTest_Action == "DELETE_TEST")
                {
                    // Call a method to delete a test 
                    new TestBLManager().DeleteTest(testKey, base.userID);

                    // Redirect to the current page after the test is deleted.
                    Response.Redirect("ViewTest.aspx?m=1&s=2&parentpage=S_TST&testkey="
                        + testKey
                        + "&deletedstatus=DEL", false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This event handler redirects to the edit test page
        /// when the edit test button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> holds the event data.
        /// </param>
        protected void ViewTest_Edit_Button_Click(object sender, EventArgs e)
        {
            try
            {
                string type = "";
                if (Request.QueryString.Get("type") == "copy")
                    type = "&type=copy";
                Response.Redirect(Constants.TestMakerConstants.EDIT_TEST + "?m=1&s=2&parentpage="
                    + Constants.ParentPage.VIEW_TEST + "&testkey="
                    + Request.QueryString["testkey"].ToString().Trim() + type, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Redirects to the create test session page when the create test
        /// session button is clicked
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> objects holds the event data.
        /// </param>
        protected void ViewTest_Button_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CreateTestSession.aspx?mode=create&m=1&s=2&testkey="
                    + Request.QueryString["testkey"].ToString().Trim()
                    + "&parentpage=" + Constants.ParentPage.VIEW_TEST, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler will call when the delete button is clicked.
        /// It performs to delete a test if it is not included 
        /// in the test session.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> holds the event data.
        /// </param>
        protected void ViewTest_Delete_Button_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear label messages
                ViewTest_topSuccessMessageLabel.Text = string.Empty;
                ViewTest_bottomSuccessMessageLabel.Text = string.Empty;

                // Assign test status key as DELETE_TEST to the hidden field.
                ViewTest_testStatusHiddenField.Value = "DELETE_TEST";

                // Set popup extender title and message
                ViewTest_inactivePopupExtenderControl.Title = "Delete Test";
                ViewTest_inactivePopupExtenderControl.Message =
                    string.Format(Resources.HCMResource.SearchTest_Delete_Confirm_Message,
                    Request.QueryString["testkey"].ToString().Trim());

                // Set button type and show the popup
                ViewTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                ViewTest_activeInActivepopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This event handler helps to rebind the grid with sorted data
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> holds the event data.
        /// </param>
        protected void ViewTest_groupByDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Call a method by passing testkey and selected sortexpression field
                BindTestQuestions(Request.QueryString["testkey"].ToString().Trim(),
                    ViewTest_groupByDDL.SelectedValue);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Methos that helps to load test details
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="testKey"/> contains test key
        /// </param>
        private void LoadTestCertificateDetails(string testKey)
        {
            try
            {
                // Clear error label message
                ViewTest_topErrorMessageLabel.Text = string.Empty;
                ViewTest_bottomErrorMessageLabel.Text = string.Empty;

                // Instantiate test detail object by assigning GetTestAndCertificateDetail method
                TestDetail testDetail = new TestBLManager().GetTestAndCertificateDetail(testKey);

                // Check if a test key is empty or null
                if (Utility.IsNullOrEmpty(testDetail.TestKey))
                {
                    base.ShowMessage(ViewTest_topErrorMessageLabel, ViewTest_bottomErrorMessageLabel,
                        "Invalid test key");
                }
                else
                {
                    // Assign data source to view test detail user control
                    ViewTest_testDetailsUserControl.TestDetailDataSource = testDetail;

                    // Assign test status to the hidden field
                    ViewTest_testStatusHiddenField.Value = testDetail.IsActive.ToString();

                    // Assign testkey to the hidden field
                    ViewTest_testKeyHiddenField.Value = testDetail.TestKey;

                    // Get test deleted status. By assigning !IsDeleted is, by default in DB,
                    // deleted status is 'N'. So in DL, IsDeleted property get assigned TRUE
                    // if the status is 'N'. Otherwise, IsDeleted will be FALSE
                    bool IsDeletedTest = !testDetail.IsDeleted;

                    // Get test author id 
                    ViewTest_testAuthorHiddenField.Value = testDetail.TestAuthorID.ToString();

                    // Check if the test author is equal to logged in user
                    bool loggedUser =
                        (ViewTest_testAuthorHiddenField.Value == base.userID.ToString()) ? true : false;

                    // Hide delete test button
                    if (testDetail.SessionIncluded)
                    {
                        ViewTest_bottomDeleteTestButton.Visible = false;
                        ViewTest_topDeleteTestButton.Visible = false;
                    }

                    // Check if the test is active
                    if (testDetail.IsActive)
                    {
                        // Check if test author and logged in user is same
                        if (loggedUser)
                        {
                            // If so, check if the test falls into anyone of the status (COMP,QUIT,CANCEL)
                            if (testDetail.SessionIncludedActive)
                            {
                                // Show deactivate test buttons
                                ViewTest_bottomInactiveTestButton.Visible = true;
                                ViewTest_topInactivetestButton.Visible = true;

                                // Show edit buttons
                                ViewTest_topEditTestButton.Visible = true;
                                ViewTest_bottomEditTestButton.Visible = true;
                            }
                        }
                        else
                        {
                            // Hide edit buttons
                            ViewTest_topEditTestButton.Visible = false;
                            ViewTest_bottomEditTestButton.Visible = false;

                            // Hide delete buttons
                            ViewTest_bottomDeleteTestButton.Visible = false;
                            ViewTest_topDeleteTestButton.Visible = false;
                        }
                    }
                    else
                    {
                        // Hide deactivatetest buttons
                        ViewTest_bottomInactiveTestButton.Visible = false;
                        ViewTest_topInactivetestButton.Visible = false;

                        // Hide edittest buttons
                        ViewTest_topEditTestButton.Visible = false;
                        ViewTest_bottomEditTestButton.Visible = false;

                        // Hide CreateTestSession buttons
                        ViewTest_topCreateTestSessionButton.Visible = false;
                        ViewTest_bottomCreateTestSessionButton.Visible = false;

                        // Check if the logged in user is equal to testauthor
                        if (loggedUser)
                        {
                            // If it is, check the status (3 status) of the test that described above
                            if (testDetail.SessionIncludedActive)
                            {
                                ViewTest_topActiveTestButton.Visible = true;
                                ViewTest_bottomActiveTestButton.Visible = true;
                            }
                        }
                        else
                        {
                            // Hide deletetest buttons
                            ViewTest_bottomDeleteTestButton.Visible = false;
                            ViewTest_topDeleteTestButton.Visible = false;
                        }
                    }

                    // If a test is deleted, set visible FALSE to all the buttons
                    if (IsDeletedTest)
                    {
                        ViewTest_bottomDeleteTestButton.Visible = false;
                        ViewTest_topDeleteTestButton.Visible = false;
                        ViewTest_bottomEditTestButton.Visible = false;
                        ViewTest_topEditTestButton.Visible = false;
                        ViewTest_topCreateTestSessionButton.Visible = false;
                        ViewTest_bottomCreateTestSessionButton.Visible = false;
                        ViewTest_topInactivetestButton.Visible = false;
                        ViewTest_bottomInactiveTestButton.Visible = false;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewTest_topErrorMessageLabel,
                    ViewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Helps to bind test question details against the testkey
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="testKey"/> contains test key.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="sortExpression"/> takes key value to be sorted.
        /// </param>
        private void BindTestQuestions(string testKey, string sortExpression)
        {
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            // Call a method to build question detail object
            // by passing testkey and sortexpression
            questionDetails =
                new TestBLManager().GetTestQuestionDetail(testKey, sortExpression);

            // Bind gridview
            ViewTest_testDrftGridView.DataSource = questionDetails;
            ViewTest_testDrftGridView.DataBind();

        }

        #endregion Private Methods

        #region Protected Overidden Methods

        /// <summary>
        /// Method which loads the default values and settings.
        /// </summary>
        protected override void LoadValues()
        {
            // Set default button
            Page.Form.DefaultButton = ViewTest_topCancelLinkButton.UniqueID;

            // Set page title
            Master.SetPageCaption(Resources.HCMResource.ViewTest_Title);

            // Clear label messages
            ViewTest_topSuccessMessageLabel.Text = string.Empty;
            ViewTest_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Helps to validate the input data if it is provided.
        /// </summary>
        /// <returns></returns>
        protected override bool IsValidData()
        { return false; }

        #endregion Protected Overidden Methods
    }
}