<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="EditManualTest.aspx.cs" Inherits="Forte.HCM.UI.TestMaker.EditManualTest" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/TestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ManualTestSummaryControl.ascx" TagName="ManualTestSummaryControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc6" %>
<%@ Register Src="../CommonControls/QuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ContentPlaceHolderID="OTMMaster_body" ID="EditManualTest_content" runat="server">

    <script src="../JS/ChartScript.js" type="text/javascript"></script>
     <script type="text/javascript" language="javascript">
         var control;
         function setFocus(sender, e) {
             try {
                 var activeTab = $find('<%=EditManualTest_mainTabContainer.ClientID%>').get_activeTabIndex();
                 if (activeTab == "0") {
                     try {
                         control = $get('<%=EditManualTest_categoryTextBox.ClientID%>');
                         control.focus();
                     }
                     catch (er1) {
                         try {
                             control = $get('<%=EditManualTest_keywordTextBox.ClientID%>');
                             control.focus();
                         }
                         catch (er2) {
                         }
                     }
                 }
                 else {
                     control = $get('ctl00_OTMMaster_body_EditManualTest_mainTabContainer_EditManualTest_testdetailsTabPanel_EditManualTest_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                     control.focus();
                 }
             }
             catch (er) {
             }
         }

    </script>
    <script type="text/javascript" language="javascript">

        // Display  Or Hide the adaptive TestDraft Or Manual test Draft Questions.
        function ShowOrHideDiv(caseValue) {
            switch (caseValue.toString()) {
                case '0':
                    {
                        document.getElementById("<%= EditManualTest_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("EditManualTest_manualTd").style.display = "block";
                        document.getElementById("EditManualTest_adaptiveDiv").style.display = "none";
                        document.getElementById("EditManualTest_manualTd").style.width = "100%";
                        document.getElementById("EditManualTest_adaptiveTD").style.display = "none";
                        document.getElementById("<%= EditManualTest_questionDiv.ClientID %>").style.width = "936px";
                        document.getElementById("<%= EditManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "0";
                        break;
                    }
                case '1':
                    {
                        document.getElementById("<%= EditManualTest_resultsHiddenField.ClientID %>").value = "1";
                        document.getElementById("EditManualTest_manualTd").style.display = "none";
                        document.getElementById("EditManualTest_adaptiveDiv").style.display = "block";
                        document.getElementById("EditManualTest_manualTd").style.width = "0";
                        document.getElementById("EditManualTest_adaptiveTD").style.display = "block";
                        document.getElementById("EditManualTest_adaptiveDiv").style.width = "100%";

                        document.getElementById("<%= EditManualTest_questionDiv.ClientID %>").style.width = "0";
                        document.getElementById("<%= EditManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "936px";
                        break;
                    }
                case '2':
                    {
                        document.getElementById("<%= EditManualTest_resultsHiddenField.ClientID %>").value = "2";
                        document.getElementById("EditManualTest_manualTd").style.display = "block";
                        document.getElementById("EditManualTest_adaptiveDiv").style.display = "block";
                        document.getElementById("EditManualTest_manualTd").style.width = "49%";
                        document.getElementById("EditManualTest_adaptiveTD").style.display = "block";
                        document.getElementById("EditManualTest_adaptiveDiv").style.width = "50%";

                        document.getElementById("<%= EditManualTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= EditManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
                default:
                    {
                        document.getElementById("<%= EditManualTest_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("EditManualTest_manualTd").style.display = "block";
                        document.getElementById("EditManualTest_adaptiveDiv").style.display = "block";
                        document.getElementById("EditManualTest_adaptiveTD").style.display = "block";
                        document.getElementById("EditManualTest_manualTd").style.width = "49%";
                        document.getElementById("EditManualTest_adaptiveDiv").style.width = "50%";

                        document.getElementById("<%= EditManualTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= EditManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
            }
            return false;
        }


        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID) {
            //alert(buttonID);
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source) {
            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID)) {
                CheckNoOfChecked(QuestionID);
            }
            else {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id) {
            var table = sourceTable;
            var testVar = document.getElementById('<%=EditManualTest_searchQuestionGridView.ClientID %>');
            var testVarTwo = document.getElementById('<%=EditManualTest_adaptiveGridView.ClientID %>');
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                if (id == i) {
                    var row = table.rows[i];
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox) {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;

                    }

                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable)) {
                    CheckNoOfChecked(id);
                }
            }

        }

        //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }

        // Question remove from the Test Draft.
        function RemoveRows(rowIndex) {
            sourceTable = document.getElementById('<%=EditManualTest_testDrftGridView.ClientID %>');

            if (confirm('Are you sure to delete these question(s)?')) {
                if (checkChecked(rowIndex)) {
                    DelRows();

                }
                else {
                    CheckCorrectQuestion(rowIndex);
                    DelRows();
                }
            }
            else
                return false;
        }

        function DelRows() {
            try {
                var table = document.getElementById('<%=EditManualTest_testDrftGridView.ClientID %>');
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox;
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }
                    if (null != chkbox && true == chkbox.checked) {
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        }

        function CheckNoOfChecked(QuestionID) {

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=EditManualTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move' + numberOfQuestions + ' questions';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser()) {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                    }
                    else {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[4].childNodes[0].value + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[0].value + ',';
                        }
                    }
                }
            }
            //var ShowDiv =
        }

        function checkChecked(id) {

            var isChecked = false;

            var table = sourceTable;
            // document.getElementById('<%=EditManualTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    isChecked = true;
                }

                if (isChecked) {
                    return true;
                }
                else {
                    return false;
                }
            }

        }

        function mouseMove(ev) {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject) {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser()) {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else {

            }
            return false;
        }
        function mouseCoords(ev) {
            if (ev != null) {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev) {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=EditManualTest_testDrftGridView.ClientID %>');
            var targPos = getPosition(curTarget);

            var scrolltargPos;
            if (detectBrowser()) {
                scrolltargPos = targPos.y;
            }
            else {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos)) {

                __doPostBack('ctl00_HCMMaster_body_EditManualTest_mainTabContainer_EditManualTest_questionsTabPanel_EditManualTest_selectMultipleImage', "OnClick");
                dragObject.style.display = 'none';
            }
            else {
                if (dragObject != null) {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale() {
            var table = sourceTable;
            //document.getElementById('<%=EditManualTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e) {
            var left = 0;
            var top = 0;

            if (e != null) {
                while (e.offsetParent) {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }


                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }
    
    </script>
    <asp:Panel ID="EditManualTest_mainPanel" runat="server" DefaultButton="EditManualTest_bottomSaveButton">

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="EditManualTest_headerLiteral" runat="server" Text="Edit Test"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="22%">
                                        <asp:Button ID="EditManualTest_topSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="EditManualTest_saveButton_Click" />
                                    </td>
                                    <td width="40%">
                                        <asp:Button ID="EditManualTest_topCreateSessionButton" runat="server" SkinID="sknButtonId"
                                            Text="Create Session" />
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="EditManualTest_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="EditManualTest_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="EditManualTest_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EditManualTest_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EditManualTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditManualTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolKit:TabContainer ID="EditManualTest_mainTabContainer" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                    <ajaxToolKit:TabPanel ID="EditManualTest_questionsTabPanel" HeaderText="questions"
                        runat="server">
                        <HeaderTemplate>
                            Questions</HeaderTemplate>
                        <ContentTemplate>
                        <asp:Panel ID="EditManualTest_testQuestionDetailsPanel" runat="server" DefaultButton="EditManualTest_topSearchButton">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <div id="EditManualTest_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="right" class="td_padding_bottom_5">
                                                        <asp:UpdatePanel ID="EditManualTest_simpleLinkButtonUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:HiddenField ID="EditManualTest_adaptiveRecommendClickedHiddenField" runat="server" />
                                                                <asp:LinkButton ID="EditManualTest_simpleLinkButton" runat="server" Text="Advanced"
                                                                    SkinID="sknActionLinkButton" OnClick="EditManualTest_simpleLinkButton_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_bg">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td width="100%">
                                                                    <asp:UpdatePanel ID="EditManualTest_searchDiv_UpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <div id="EditManualTest_simpleSearchDiv" runat="server">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_categoryHeadLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="EditManualTest_categoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="EditManualTest_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_keywordsHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="EditManualTest_keywordsTextBox" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="EditManualTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <div id="EditManualTest_advanceSearchDiv" runat="server">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <uc1:CategorySubjectControl ID="EditManualTest_categorySubjectControl" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_8">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="panel_inner_body_bg">
                                                                                            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td style="width: 10%">
                                                                                                        <asp:Label ID="EditManualTest_testAreaHeadLabel" runat="server" Text="Test Area"
                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="5" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                                        <asp:CheckBoxList ID="EditManualTest_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                            RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                        </asp:CheckBoxList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 10%">
                                                                                                        <asp:Label ID="EditManualTest_complexityLabel" runat="server" Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="3" align="left" valign="middle">
                                                                                                        <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                            class="checkbox_list_bg">
                                                                                                            <asp:CheckBoxList ID="EditManualTest_complexityCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                            </asp:CheckBoxList>
                                                                                                        </div>
                                                                                                        <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                            <asp:ImageButton ID="EditManualTest_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditManualTest_keywordHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="EditManualTest_keywordTextBox" runat="server" Columns="50" MaxLength="100"></asp:TextBox></div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="EditManualTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditManualTest_questionLabel" runat="server" Text="Question ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="EditManualTest_questionIDTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditManualTest_creditHeadLabel" runat="server" Text="Credit" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="EditManualTest_creditTextBox" runat="server"></asp:TextBox>
                                                                                                        <ajaxToolKit:FilteredTextBoxExtender ID="EditManualTest_weightageFileteredExtender"
                                                                                                            runat="server" TargetControlID="EditManualTest_creditTextBox" FilterType="Numbers"
                                                                                                            Enabled="True">
                                                                                                        </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditManualTest_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="EditManualTest_authorTextBox" runat="server"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="EditManualTest_dummyAuthorID" runat="server" />
                                                                                                        </div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="EditManualTest_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" /></div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditManualTest_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                            Text="Position Profile"></asp:Label>
                                                                                                    </td>
                                                                                                   <td colspan="3">
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="EditManualTest_positionProfileTextBox" runat="server" MaxLength="200" Columns="55"></asp:TextBox>
                                                                                                        </div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="EditManualTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" /></div>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditManualTest_positionProfileKeywordLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                            Text="Position Profile Keyword"></asp:Label>
                                                                                                    </td>
                                                                                                    <td >
                                                                                                        <div style="float: left; padding-right: 5px; width: 100%">
                                                                                                           <asp:TextBox ID="EditManualTest_positionProfileKeywordTextBox" runat="server" MaxLength="500"></asp:TextBox>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="td_padding_top_5">
                                                                    <asp:Button ID="EditManualTest_topSearchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                                        OnClick="EditManualTest_searchQuestionButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr id="EditManualTest_searchResultsTR" runat="server">
                                    <td class="header_bg" runat="server">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 80%" align="left" class="header_text_bold">
                                                    <asp:Literal ID="EditManualTest_searchResultsLiteral" runat="server" Text="Questions"></asp:Literal>&nbsp;<asp:Label
                                                        ID="EditManualTest_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:ImageButton ID="EditManualTest_manualImageButton" runat="server" SkinID="sknManualImageButton"
                                                        ToolTip="Click here to view 'Search Results' only" OnClientClick="javascript:return ShowOrHideDiv('0');" />
                                                    <asp:HiddenField ID="EditManualTest_resultsHiddenField" runat="server" />
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:ImageButton ID="EditManualTest_adaptiveImageButton" runat="server" SkinID="sknAdaptiveImageButton"
                                                        ToolTip="Click here to view 'Adaptive Recommended Questions' only" OnClientClick="javascript:return ShowOrHideDiv('1');" />
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:ImageButton ID="EditManualTest_bothImageButton" runat="server" SkinID="sknCombinedAMImageButton"
                                                        ToolTip="Click here to view both 'Search Results' and 'Adaptive Recommended Questions'"
                                                        OnClientClick="javascript:return ShowOrHideDiv('2');" />
                                                </td>
                                                <td style="width: 5%" align="right">
                                                    <span id="EditManualTest_searchResultsUpSpan" runat="server" style="display: none;">
                                                        <asp:Image ID="EditManualTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                    </span><span id="EditManualTest_searchResultsDownSpan" runat="server" style="display: block;">
                                                        <asp:Image ID="EditManualTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                    </span>
                                                    <asp:HiddenField ID="EditManualTest_restoreHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" id="EditManualTest_adaptiveTD">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="EditManualTest_adaptiveQuestionsHeadLabel" runat="server" Text="Adaptive Recommendations"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:UpdatePanel ID="EditManualTest_adaptiveYesNoUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButtonList ID="EditManualTest_adaptiveYesNoRadioButtonList" runat="server"
                                                                            RepeatColumns="2" OnSelectedIndexChanged="EditManualTest_adaptiveYesNoRadioButtonList_SelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                            <asp:ListItem Text="On" Value="0"></asp:ListItem>
                                                                            <asp:ListItem Text="Off" Value="1" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="EditManualTest_recommendAdaptiveQuestions" runat="server" Text="Recommend Questions"
                                                                            SkinID="sknButtonId" OnClick="EditManualTest_recommendAdaptiveQuestions_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td valign="top">
                                        <div id="EditManualTest_manualTd" style="width: 49%; float: left; display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    <asp:Literal ID="EditManualTest_manualQuestLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" class="grid_body_bg">
                                                        <asp:UpdatePanel ID="EditManualTest_searchQuestionGridView_UpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 225px; overflow: auto;" runat="server" id="EditManualTest_questionDiv">
                                                                                <asp:GridView ID="EditManualTest_searchQuestionGridView" runat="server" AutoGenerateColumns="False"
                                                                                    AllowSorting="false" GridLines="None" Width="100%" OnRowDataBound="EditManualTest_searchQuestionGridView_RowDataBound"
                                                                                    OnRowCommand="EditManualTest_searchQuestionGridView_RowCommand">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="EditManualTest_searchQuestionCheckbox" runat="server" />
                                                                                                <asp:HiddenField ID="EditManualTest_searchQuestionKeyHiddenField" runat="server"
                                                                                                    Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualTest_previewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                                    CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                    ToolTip="Question Details Summary " />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualTest_searchQuestionAlertImageButton" runat="server"
                                                                                                    SkinID="sknAlertImageButton" ToolTip="Flag Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualTest_searchQuestionSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                                    CommandName="Select" ToolTip="Select Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="EditManualTest_hidddenValue" runat="server" Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                <asp:Panel ID="EditManualTest_hoverPanel" runat="server" Width="750px" CssClass="popupMenu table_outline_bg">
                                                                                                    <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                                        <tr>
                                                                                                            <th class="popup_question_icon">
                                                                                                                <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                <asp:Label ID="EditManualTest_questionDetailPreviewControlQuestionLabel" 
                                                                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                                                                    runat="server" SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                </div>
                                                                                                            </th>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <th class="td_padding_left_20">
                                                                                                            <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                                <asp:PlaceHolder ID="EditManualTest_answerChoicesPlaceHolder" runat="server"></asp:PlaceHolder>
                                                                                                                </div>
                                                                                                                <asp:HiddenField ID="EditManualTest_correctAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                            </th>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:Panel>
                                                                                                <ajaxToolKit:HoverMenuExtender ID="EditManualTest_hoverMenuExtender" runat="server"
                                                                                                    PopupControlID="EditManualTest_hoverPanel" PopupPosition="Bottom" HoverCssClass="popupHover"
                                                                                                    TargetControlID="EditManualTest_questionLabel" PopDelay="50" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                        <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Question">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                    Width="360px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Category">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Subject">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Answer">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                    Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Complexity">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                    Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <uc2:PageNavigator ID="EditManualTest_bottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="EditManualTest_topSearchButton" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="moveDisplayDIV" style="display: none; background-color: Lime" runat="server">
                                            <asp:TextBox ID="sampleDIVTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                        </div>
                                        <div id="EditManualTest_adaptiveDiv" style="width: 50%; float: right; display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    <asp:Literal ID="EditManualTest_adaptiveQuestLiteral" runat="server" Text="Adaptive Recommended Questions"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                    <td class="grid_body_bg">
                                                        <asp:UpdatePanel ID="EditManualTest_adaptiveUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 225px; width: 0px; overflow: auto;" runat="server" id="EditManualTest_adaptiveQuestionDiv">
                                                                                <asp:GridView ID="EditManualTest_adaptiveGridView" runat="server" AutoGenerateColumns="False"
                                                                                    AllowSorting="false" GridLines="None" Width="100%" OnRowDataBound="EditManualTest_adaptiveGridView_RowDataBound"
                                                                                    OnRowCommand="EditManualTest_adaptiveGridView_RowCommand">
                                                                                    <EmptyDataTemplate>
                                                                                        <center>
                                                                                            <asp:Label ID="EditManualTest_adaptiveEmptyLabel" runat="server" Text="No question found for recommendation"
                                                                                                SkinID="sknErrorMessage"></asp:Label>
                                                                                        </center>
                                                                                    </EmptyDataTemplate>
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="EditManualTest_adaptiveCheckbox" runat="server" />
                                                                                                <asp:HiddenField ID="EditManualTest_adaptiveQuestionKeyHiddenField" runat="server"
                                                                                                    Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualTest_adaptivepreviewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                                    CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                    ToolTip="Question Details Summary " />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualTest_adaptiveAlertImageButton" runat="server" SkinID="sknAlertImageButton"
                                                                                                    ToolTip="Flag Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualTest_adaptiveSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                                    CommandName="Select" ToolTip="Select Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="EditManualTest_adaptiveQuestionhiddenValue" runat="server" Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                <asp:Panel ID="EditManualTest_adaptvieQuestionPreviewhoverPanel" runat="server" Width="750px" CssClass="popupMenu table_outline_bg">
                                                                                                    <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                                        <tr>
                                                                                                            <th class="popup_question_icon">
                                                                                                                <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                <asp:Label ID="EditManualTest_adaptiveQuestionDetailPreviewControlQuestionLabel"
                                                                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                                                                     runat="server" SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                </div>
                                                                                                            </th>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <th class="td_padding_left_20">
                                                                                                            <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                                <asp:PlaceHolder ID="EditManualTest_adaptiveAnswerChoicesPlaceHolder" runat="server">
                                                                                                                </asp:PlaceHolder>
                                                                                                                </div>
                                                                                                                <asp:HiddenField ID="EditManualTest_adaptiveCorrectAnswerHiddenField" runat="server"
                                                                                                                    Value='<%# Eval("Answer") %>' />
                                                                                                            </th>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:Panel>
                                                                                                <ajaxToolKit:HoverMenuExtender ID="EditManualTest_adaptivePreviewhoverMenuExtender"
                                                                                                    runat="server" PopupControlID="EditManualTest_adaptvieQuestionPreviewhoverPanel"
                                                                                                    PopupPosition="Bottom" HoverCssClass="popupHover" TargetControlID="EditManualTest_adaptiveQuestionLabel"
                                                                                                    PopDelay="50" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                        <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Question">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_adaptiveQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                    Width="360px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Category">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_adaptiveCategoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Subject">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_adaptiveSubjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Answer">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_adaptiveAnswerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                    Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Complexity">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                    Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <asp:HiddenField ID="EditManualTest_adaptiveGridViewHiddenField" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:HiddenField ID="EditManualTest_adaptivePagingHiddenField" runat="server" Value="1" />
                                                                            <uc2:PageNavigator ID="EditManualTest_adaptivebottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td align="center" class="grid_header_bg">
                                        <table width="10%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="EditManualTest_selectMultipleImage" runat="server" SkinID="sknMultiDown_ArrowImageButton"
                                                        CommandName="Select" ToolTip="Move questions to test draft" OnClick="EditManualTest_selectMultipleImage_Click" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="EditManualTest_removeMultipleImage" runat="server" SkinID="sknMultiUp_ArrowImageButton"
                                                        CommandName="Select" ToolTip="Remove questions from test draft" OnClick="EditManualTest_removeMultipleImage_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="EditManualTest_testDrftGridView_UpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="grid_header_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_text_bold">
                                                                        <asp:Literal ID="EditManualTest_testDraftLiteral" runat="server" Text="Test Draft"></asp:Literal>&nbsp;
                                                                        <asp:Label ID="EditManualTest_testDrafttHelpLabel" runat="server" SkinID="sknLabelText"
                                                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>" Visible="false"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr onmouseup="javascript:srsmouseup(event);" onmousemove="javascript:mouseMove(event);"
                                                        style="height: 35px">
                                                        <td class="grid_body_bg" valign="top">
                                                            <asp:GridView ID="EditManualTest_testDrftGridView" OnRowDataBound="EditManualTest_testDrftGridView_RowDataBound"
                                                                runat="server" GridLines="None" Width="100%" OnRowCommand="EditManualTest_testDrftGridView_RowCommand"
                                                                EmptyDataText="&nbsp;">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="EditManualTest_testDrftCheckbox" runat="server" />
                                                                            <asp:HiddenField ID="EditManualTest_testDraftQuestionKeyHiddenField" runat="server"
                                                                                Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="EditManualTest_testDrftRemoveImage" runat="server" SkinID="sknDeleteImageButton"
                                                                                CommandName="Select" ToolTip="Remove Question" CommandArgument='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                        DataField="QuestionID" Visible="False">
                                                                        <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="Question">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditManualTest_draftQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),70) %>'
                                                                                Width="370px"></asp:Label>
                                                                            <asp:Panel ID="EditManualTest_hoverPanel" runat="server" CssClass="popupMenu table_outline_bg" Width="750px">
                                                                                <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                    <tr>
                                                                                        <th class="popup_question_icon">
                                                                                            <div style="word-wrap: break-word; white-space: normal;">
                                                                                                <asp:Label ID="EditManualTest_questionDetailPreviewControl" runat="server" SkinID="sknLabelFieldMultiText"
                                                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                        </th>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="td_padding_left_20">
                                                                                        <div style="height: 60px; overflow: auto; width: 100%">
                                                                                            <asp:PlaceHolder ID="EditManualTest_answerChoicesPlaceHolder" runat="server"></asp:PlaceHolder>
                                                                                            </div>
                                                                                            <asp:HiddenField ID="EditManualTest_correctAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                        </th>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                            <ajaxToolKit:HoverMenuExtender ID="EditManualTest_draftHoverMenuExtender1" runat="server"
                                                                                PopupControlID="EditManualTest_hoverPanel" PopupPosition="Bottom" HoverCssClass="popupHover"
                                                                                TargetControlID="EditManualTest_draftQuestionLabel" PopDelay="50" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="true" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Category">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditManualTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Subject">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditManualTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Complexity">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditManualTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:HiddenField ID="EditManualTest_previewQuestionAddHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="EditManualTest_selectMultipleImage" />
                                                <asp:AsyncPostBackTrigger ControlID="EditManualTest_removeMultipleImage" />
                                                <asp:AsyncPostBackTrigger ControlID="EditManualTest_searchQuestionGridView" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <uc5:ManualTestSummaryControl ID="EditManualTest_testSummaryControl" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="EditManualTest_testdetailsTabPanel" runat="server">
                        <HeaderTemplate>
                            Test Details</HeaderTemplate>
                        <ContentTemplate>
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td style="width: 100%">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <uc4:TestDetailsControl ID="EditManualTest_testDetailsUserControl" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EditManualTest_bottomMessageupdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EditManualTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditManualTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="22%">
                                        <asp:Button ID="EditManualTest_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="EditManualTest_saveButton_Click" />
                                    </td>
                                    <td width="40%">
                                        <asp:Button ID="EditManualTest_bottomCreateSessionButton" runat="server" SkinID="sknButtonId"
                                            Text="Create Session" />
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="EditManualTest_bottompResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="EditManualTest_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="EditManualTest_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="EditManualTest_questionDetailPanel" runat="server" CssClass="popupcontrol_question_draft"
                                            Style="display: none">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="td_height_20">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                    <asp:Literal ID="EditManualTest_questionDetail_questionResultLiteral" runat="server"
                                                                        Text="Question Detail"></asp:Literal>
                                                                </td>
                                                                <td style="width: 50%" valign="top">
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:ImageButton ID="EditManualTest_questionDetailPreviewControl_topCancelImageButton"
                                                                                    runat="server" SkinID="sknCloseImageButton" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_20">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DataGrid ID="EditManualTest_questionDetailCategoryDataGrid" runat="server" AutoGenerateColumns="false">
                                                                                    <Columns>
                                                                                        <asp:BoundColumn HeaderText="Category" DataField="Category"></asp:BoundColumn>
                                                                                        <asp:BoundColumn HeaderText="Subject" DataField="Subject"></asp:BoundColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="popup_panel_inner_bg">
                                                                                <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailTestAreaHeadLabel" runat="server" Text="Test Area"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailTestAreaLabel" runat="server" Text="Concept"
                                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailNoOfTestLabel" runat="server" Text="Number Of Administered Test "
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailTestAdministeredLabel" runat="server"
                                                                                                Text="5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailAvgTimeLabel" runat="server" Text="Average Time Taken(in minutes)"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailAvgTimeValueLabel" runat="server" Text="15.5"
                                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailCorrectToAttendedRatioHeadLabel" runat="server"
                                                                                                Text=" Ratio of Correct Answer To Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailCorrectToAttendedRatioLabel" runat="server"
                                                                                                Text="36:50" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailComplexityHeadLabel" runat="server" Text="Complexity"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditManualTest_questionDetailPreviewControlComplexityLabel" runat="server"
                                                                                                Text="Normal" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="popup_question_icon">
                                                                                <asp:Label ID="EditManualTest_questionDetailQuestionLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_padding_left_20">
                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                    <asp:RadioButtonList ID="EditManualTest_questionDetailAnswerRadioButtonList" runat="server"
                                                                                        RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5" TextAlign="Right"
                                                                                        Width="100%">
                                                                                    </asp:RadioButtonList>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="padding-left: 30px">
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="EditManualTest_questionDetail_TopAddButton" runat="server" Text="Add"
                                                                                    SkinID="sknButtonId" OnClick="EditManualTest_questionDetail_TopAddButton_Click" />
                                                                            </td>
                                                                            <td style="padding-left: 10px">
                                                                                <asp:LinkButton ID="EditManualTest_questionDetail_TopCancelButton" runat="server"
                                                                                    SkinID="sknCancelLinkButton" Text="Cancel" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div style="display: none">
                                            <asp:Button ID="EditManualTest_adaptiveQuestionPreviewPanelTargeButton" runat="server" />
                                        </div>
                                        <ajaxToolKit:ModalPopupExtender ID="EditManualTest_adaptiveQuestionPreviewModalPopupExtender"
                                            runat="server" PopupControlID="EditManualTest_questionDetailPanel" TargetControlID="EditManualTest_adaptiveQuestionPreviewPanelTargeButton"
                                            BackgroundCssClass="modalBackground" CancelControlID="EditManualTest_questionDetail_TopCancelButton">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:UpdatePanel ID="EditManualTest_questionDetailPreviewUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="EditManualTest_questionPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <%--<uc3:QuestionDetailPreviewControl ID="EditManualTest_questionDetailPreviewControl"
                    runat="server" OnCancelClick="EditManualTest_cancelClick" />--%>
                <uc7:QuestionDetailSummaryControl ID="EditManualTest_questionDetailSummaryControl"
                    runat="server" Title="Question Details Summary" OnCancelClick="EditManualTest_cancelClick"
                    OnAddClick="EditManualTest_questionDetailPreviewControl_AddClick" />
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="EditManualTest_questionModalTargeButton" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="EditManualTest_questionModalPopupExtender" runat="server"
                PopupControlID="EditManualTest_questionPanel" TargetControlID="EditManualTest_questionModalTargeButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="EditManualTest_ConfirmPopupUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="EditManualTest_sessionInclucedKeyHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualTest_questionKeyHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualTest_deletedQuestionHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualTest_insertedQuestionHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualTest_authorIdHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualTest_positionProfileHiddenField" runat="server" />
            <asp:HiddenField ID="hiddenValue" runat="server" />
            <asp:HiddenField ID="hiddenDeleteValue" runat="server" />
            <div id="EditManualTest_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="EditManualTest_hiddenPopupModalButton" runat="server" />
            </div>
            <asp:Panel ID="EditManualTest_ConfirmPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_remove">
                <uc6:ConfirmMsgControl ID="EditManualTest_ConfirmPopupExtenderControl" runat="server"
                    OnOkClick="EditManualTest_okClick" OnCancelClick="EditManualTest_cancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="EditManualTest_ConfirmPopupExtender" runat="server"
                PopupControlID="EditManualTest_ConfirmPopupPanel" TargetControlID="EditManualTest_hiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=EditManualTest_categoryTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('<%=EditManualTest_keywordTextBox.ClientID %>').focus();
                }
                catch (Err1) {
                    document.getElementById('ctl00$OTMMaster_body$CreateManualTest_mainTabContainer$CreateManualTest_testdetailsTabPanel$CreateManualTest_testDetailsUserControl$TestDetailsControl_testNameTextBox').focus();
                }
            }
        }
    </script>
</asp:Content>
