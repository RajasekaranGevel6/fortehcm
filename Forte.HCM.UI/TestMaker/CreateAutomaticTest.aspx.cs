﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateAutomaticTest.cs
// File that represents the user interface for Generating a test with
// system generated questions according to the user input search 
// criteria

#endregion Header

#region Directives                                                             

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

#endregion

namespace Forte.HCM.UI.TestMaker
{
    public partial class CreateAutomaticTest : PageBase
    {

        #region Declarations                                                   

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the questions for the test in test draft gridview.
        /// </summary>
        private const string CREATEAUTOMATICTEST_QUESTIONVIEWSTATE = "CREATEAUTOMATICTEST_QUESTIONSGRID";

        #endregion Declarations

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loading.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (CreateAutomaticTest_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = CreateAutomaticTest_byQuestion_generateTestButton.UniqueID;
                else if (CreateAutomaticTest_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = CreateAutomaticTest_bottomSaveButton.UniqueID;

                //Page title
                Master.SetPageCaption(Resources.HCMResource.CreateAutomaticTest_Title);
                ClearAllLabelMessages();
                CheckAndSetExpandorRestore();
                if (!IsPostBack)
                    LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler event that will reset the page when user cliks on the reset link button
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the generate button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_byQuestion_generateTestButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateAutomaticTest_byQuestion_testDrftDiv.Visible = false;
                CreateAutomaticTest_byQuestion_automatedTestUserControl.Visible = false;
                ViewState[CREATEAUTOMATICTEST_QUESTIONVIEWSTATE] = null;

                CreateAutomaticTest_byQuestion_positionProfileTextBox.Text =
                    Request[CreateAutomaticTest_byQuestion_positionProfileTextBox.UniqueID].Trim();

                LoadAutomatedQuestions();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the test draft grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CreateAutomaticTest_byQuestion_testDraftGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the test segment grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_byQuestion_testSegmantGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                ImageButton CreateAutomaticTest_categoryImageButton = ((ImageButton)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_categoryImageButton"));
                TextBox CreateAutomaticTest_categoryTextBox = ((TextBox)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_categoryTextBox"));
                TextBox CreateAutomaticTest_subjectTextBox = ((TextBox)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_subjectTextBox"));
                HiddenField CreateAutomaticTest_categoryHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_categoryHiddenField");
                HiddenField CreateAutomaticTest_categoryNameHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_categoryNameHiddenField");
                HiddenField CreateAutomaticTest_subjectNameHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_subjectNameHiddenField");
                Label CreateAutomaticTest_byQuestion_expecetdQuestions =
                        (Label)e.Row.FindControl("CreateAutomaticTest_byQuestion_expecetdQuestions");
                //
                string[] SelectedTestAreaID = ((HiddenField)e.Row.
                    FindControl("CreateAutomaticTest_byQuestion_testAreaHiddenField")).Value.Split(',');
                string[] SelectedComplexityID = ((HiddenField)e.Row.
                    FindControl("CreateAutomaticText_byQuestion_complexityHiddenField")).Value.Split(',');
                //
                //Select Subject and Category
                CreateAutomaticTest_categoryImageButton.Attributes.Add("onclick",
                        "return LoadCategorySubjectLookUpforReadOnly('" + CreateAutomaticTest_categoryTextBox.ClientID +
                            "','" + CreateAutomaticTest_subjectTextBox.ClientID + "','" +
                            CreateAutomaticTest_categoryHiddenField.ClientID + "','" +
                            CreateAutomaticTest_categoryNameHiddenField.ClientID + "','" +
                            CreateAutomaticTest_subjectNameHiddenField.ClientID + "');");
                //CreateAutomaticTest_byQuestion_authorImageButton.Attributes.Add("onclick",
                //    "return ShowClientRequest('" + CreateAutomaticTest_byQuestion_positionProfileTextBox.ClientID + "','" +
                //    CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.ClientID + "','" +
                //    CreateAutomaticTest_byQuestion_positionProfileIdHiddenField.ClientID + "');");

                CheckBoxList CreateAutomaticTest_testAreaCheckBoxList = ((CheckBoxList)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_testAreaCheckBoxList"));
                CheckBoxList CreateAutomaticTest_complexityCheckBoxList = ((CheckBoxList)e.Row.FindControl
                        ("CreateAutomaticTest_byQuestion_complexityCheckBoxList"));
                // Binds test aread check box list
                CreateAutomaticTest_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
                CreateAutomaticTest_testAreaCheckBoxList.DataTextField = "AttributeName";
                CreateAutomaticTest_testAreaCheckBoxList.DataValueField = "AttributeID";
                CreateAutomaticTest_testAreaCheckBoxList.DataBind();
                for (int i = 0; i < SelectedTestAreaID.Length; i++)
                    if (SelectedTestAreaID[i] != "")
                        CreateAutomaticTest_testAreaCheckBoxList.Items.FindByValue(SelectedTestAreaID[i]).Selected = true;
                // Bind Complexity Check box list
                CreateAutomaticTest_complexityCheckBoxList.DataSource = GetComplexityList();
                CreateAutomaticTest_complexityCheckBoxList.DataTextField = "AttributeName";
                CreateAutomaticTest_complexityCheckBoxList.DataValueField = "AttributeID";
                CreateAutomaticTest_complexityCheckBoxList.DataBind();
                for (int i = 0; i < SelectedComplexityID.Length; i++)
                    if (SelectedComplexityID[i] != "")
                        CreateAutomaticTest_complexityCheckBoxList.Items.FindByValue(SelectedComplexityID[i]).Selected = true;
                if (CreateAutomaticTest_byQuestion_expecetdQuestions.Text != "")
                    ((HtmlGenericControl)e.Row.FindControl("CreateAutomaticTest_byQuestion_expectedQuestionsDiv")).Visible = true;
                EnableOrDisableTestSegmentGridControls(e.Row);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_byQuestion_testDraftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "Select")
                    return;
                CreateAutomaticTest_byQuestion_bottomQuestionModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the test draft grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void CreateAutomaticTest_byQuestion_testDraftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                ImageButton alertImageButton = (ImageButton)e.Row.FindControl("CreateAutomaticTes_byQuestion_testDrftRemoveImage");
                LinkButton CreateAutomaticTest_questionIdLinkButton =
                                    (LinkButton)e.Row.FindControl("QuestionDetailPreviewControl_byQuestion_questionLinkButton"); alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" +
                    CreateAutomaticTest_questionIdLinkButton.Text + "' );");
                HtmlContainerControl CreateAutomaticTest_byQuestion_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("CreateAutomaticTest_byQuestion_detailsDiv");

                HtmlContainerControl CreateAutomaticTest_byQuestionOpenText_detailsDiv =
                   (HtmlContainerControl)e.Row.FindControl("CreateAutomaticTest_byQuestionOpenText_detailsDiv");
                HtmlAnchor SearchQuestion_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("CreateAutomaticTest_byQuestion_focusDownLink");
                if (CreateAutomaticTest_byQuestion_questionTypeDropDownList.SelectedValue.ToString() == "1")
                {
                    CreateAutomaticTest_questionIdLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                CreateAutomaticTest_byQuestion_detailsDiv.ClientID + "','" + SearchQuestion_focusDownLink.ClientID + "')");
                    PlaceHolder CreateAutomaticTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl(
                        "CreateAutomaticTest_byQuestion_answerChoicesPlaceHolder");
                    CreateAutomaticTest_answerChoicesPlaceHolder.Controls.Add
                        (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions
                        (CreateAutomaticTest_questionIdLinkButton.Text.ToString()), false));
                }
                else
                {
                    CreateAutomaticTest_questionIdLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                CreateAutomaticTest_byQuestionOpenText_detailsDiv.ClientID + "','" + SearchQuestion_focusDownLink.ClientID + "')");
                    Literal CreateAutomaticTest_byQuestionOpenText_Answer = (Literal)e.Row.FindControl(
                        "CreateAutomaticTest_byQuestionOpenText_Answer");
                    QuestionDetail questionDetail = new BL.QuestionBLManager().GetQuestion
                   (CreateAutomaticTest_questionIdLinkButton.Text.ToString());
                    CreateAutomaticTest_byQuestionOpenText_Answer.Text = questionDetail.AnswerReference;
                }
                
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the delete segment image button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_byQuestion_testSegmantGridView_deleteImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DeleteTestSegmentRow(((GridViewRow)((ImageButton)sender).Parent.Parent).RowIndex);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add segment button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_byQuestion_addSegmantLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                AddTestSegmentRow(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add position profile
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref=
        protected void CreateAutomaticTest_byQuestion_addPositionProfileLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.Value))
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, "Please select the position profile");
                    return;
                }
                DataTable dtTestSearchCriteria = BuildTableFromGridView();

                CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = FilledSegmants(ref dtTestSearchCriteria);
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();

                AddTestSegmentRow(true);
                CreateAutomaticTest_byQuestion_positionProfileTextBox.Text = "";
                CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.Value = "";
                CreateAutomaticTest_byQuestion_positionProfileIDHiddenField.Value = "";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //CreateAutomaticTest_byQuestion_positionProfileTextBox.Text = CreateAutomaticTest_byQuestion_positionProfileIDHiddenField.Value;

                // Check if feature usage limit exceeds for creating test.
                if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel, 
                        "You have reached the maximum limit based your subscription plan. Cannot create more tests");
                    return;
                }

                if (!IsValidData())
                    return;
                if (Convert.ToInt32(CreateAutomaticTest_byQuestion_totalNumberTextBox.Text) > ((List<QuestionDetail>)ViewState[CREATEAUTOMATICTEST_QUESTIONVIEWSTATE]).Count)
                    ShowConfirmPopUp();
                else
                {
                    SaveQuestions();
                    SetVisibleStatusForButton(true);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateAutomaticText_okPopUpClick(object sender, EventArgs e)
        {
            try
            {
                SaveQuestions();
                SetVisibleStatusForButton(true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void CreateAutomaticText_cancelPopUpClick(object sender, EventArgs e)
        {
            DataTable dtTestSearchCriteria = null;
            try
            {
                dtTestSearchCriteria = BuildTableFromGridView();
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtTestSearchCriteria;
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtTestSearchCriteria)) dtTestSearchCriteria = null;
            }
        }

        /// <summary>
        /// Handler method that will be called when the create session button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticTest_CreateSessionButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CreateTestSession.aspx?m=1&s=3&parentpage=S_TSN&testkey=" +
                    CreaetAutomaticTest_byQuestion_testKeyHiddenField.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// This method Invokes Confirm pop up when user tries to save
        /// the less number of questions in the test draft panel with the
        /// number of questions provided.
        /// </summary>
        private void ShowConfirmPopUp()
        {
            CreateAutomaticText_confirmPopupExtenderControl.Message =
                Resources.HCMResource.CreateAutomaticTest_SaveTestLessQuestionsWarningMessage;
            CreateAutomaticText_confirmPopupExtenderControl.Title = "Warning";
            CreateAutomaticText_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
            CreateAutomaticText_confirmPopupExtenderControl.DataBind();
            CreateAutomaticText_saveTestModalPopupExtender.Show();
        }

        /// <summary>
        /// Binds the empty data source to the test detail user control.
        /// </summary>
        private void BindEmptyTestDetailDataSource1()
        {
            TestDetail testDetail = null;
            try
            {
                CreaetAutomaticTest_byQuestion_testKeyHiddenField.Value = "";
                testDetail = new TestDetail();
                testDetail.TestKey = "";
                testDetail.Name = "";
                testDetail.Description = "";
                testDetail.SystemRecommendedTime = 0;
                testDetail.RecommendedCompletionTime = 0;
                CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }


        /// <summary>
        /// Method that saves the question to the DB and generates a test key
        /// which will be shown to the user in the control.
        /// </summary>
        private void SaveQuestions()
        {
            List<QuestionDetail> questionDetailTestDraftResultList = null;
            TestDetail testDetail = null;
            try
            {
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                questionDetailTestDraftResultList = ViewState[CREATEAUTOMATICTEST_QUESTIONVIEWSTATE] as List<QuestionDetail>;
                List<decimal> creditEarned = new List<decimal>();
                List<decimal> timeTaken = new List<decimal>();
                for (int i = 0; i < questionDetailTestDraftResultList.Count; i++)
                {
                    creditEarned.Add(questionDetailTestDraftResultList[i].CreditsEarned);
                    timeTaken.Add(questionDetailTestDraftResultList[i].AverageTimeTaken);
                }
                //int questionCount = Convert.ToInt32(questionDetailTestDraftResultList.Count);
                testDetail = new TestDetail();
                testDetail = CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource;
                testDetail.Questions = questionDetailTestDraftResultList;
                testDetail.IsActive = true;
                //testDetail.IsCertification = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
                    //  testDetail.CertificationDetail = new CertificationDetail();
                    testDetail.CertificationId = 0;
                else
                    testDetail.CertificationId = testDetail.CertificationDetail.CertificateID;
                testDetail.CreatedBy = userID;
                testDetail.IsDeleted = false;
                testDetail.ModifiedBy = userID;
                testDetail.NoOfQuestions = questionDetailTestDraftResultList.Count;
                testDetail.SystemRecommendedTime = Convert.ToInt32(timeTaken.Average()) *
                    questionDetailTestDraftResultList.Count;
                testDetail.TestAuthorID = userID;
                testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
                testDetail.TestCreationDate = DateTime.Now;
                testDetail.TestStatus = TestStatus.Active;
                testDetail.TestType = TestType.Genenal;
                testDetail.TestMode = "AUTO";
                testDetail.TestKey = "";
                testDetail.QuestionType = (QuestionType)Convert.ToInt32(CreateAutomaticTest_byQuestion_questionTypeDropDownList.SelectedValue);

                // Save the test.
                testDetail.TestKey = new TestBLManager().SaveTest(testDetail, userID,
                    new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                    questionDetailTestDraftResultList).AttributeID.ToString());


                CreaetAutomaticTest_byQuestion_testKeyHiddenField.Value = testDetail.TestKey;
                CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;

                // Show a success message.
                base.ShowMessage(CreateAutomaticTest_topSuccessMessageLabel,
                    CreateAutomaticTest_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.CreateAutomaticTest_TestSuccessfullMessage, testDetail.TestKey));
                CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;

                // Send a mail to the position profile creator.
                try
                {
                    if (testDetail.PositionProfileID != 0)
                    {
                        new EmailHandler().SendMail(EntityType.TestCreatedForPositionProfile, testDetail.TestKey);
                    }
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        /// <summary>
        /// Clear all top and bottom message labels
        /// </summary>
        private void ClearAllLabelMessages()
        {
            CreateAutomaticTest_topErrorMessageLabel.Text = string.Empty;
            CreateAutomaticTest_topSuccessMessageLabel.Text = string.Empty;
            CreateAutomaticTest_bottomErrorMessageLabel.Text = string.Empty;
            CreateAutomaticTest_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Binds emtpty test segment table grid
        /// </summary>
        private void BindEmptyTestSegmentGrid()
        {
            DataTable dtTestSegment = new DataTable();
            AddTestSegmentColumns(ref dtTestSegment);
            DataRow drNewRow = dtTestSegment.NewRow();
            dtTestSegment.Rows.Add(drNewRow);
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtTestSegment;
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Cat_sub_id", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Complexity", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("TestArea", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Category", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Subject", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Keyword", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("ExpectedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("PickedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("TotalRecordsinDB", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("QuestionsDifference", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Remarks", typeof(string));
        }

        /// <summary>
        /// Builds data table from test segment grid view
        /// </summary>
        /// <returns>Data table that contains user given input for test segment grid</returns>
        private DataTable BuildTableFromGridView()
        {
            DataTable dtTestSegment = null;
            try
            {
                dtTestSegment = new DataTable();
                AddTestSegmentColumns(ref dtTestSegment);
                DataRow drNewRow = null;
                int Weightage = 0;
                int SNo = 1;
                foreach (GridViewRow gridViewRow in CreateAutomaticTest_byQuestion_testSegmantGridView.Rows)
                {
                    drNewRow = dtTestSegment.NewRow();
                    drNewRow["SNO"] = SNo++.ToString();
                    drNewRow["Cat_sub_id"] = ((HiddenField)gridViewRow.
                        FindControl("CreateAutomaticTest_byQuestion_categoryHiddenField")).Value;
                    drNewRow["Category"] = ((HiddenField)gridViewRow.
                        FindControl("CreateAutomaticTest_byQuestion_categoryNameHiddenField")).Value.Trim();
                    drNewRow["Subject"] = ((HiddenField)gridViewRow.
                        FindControl("CreateAutomaticTest_byQuestion_subjectNameHiddenField")).Value.Trim();
                    int.TryParse(((TextBox)gridViewRow.
                        FindControl("CreateAutomaticTest_byQuestion_weightageTextBox")).Text, out Weightage);
                    if (Weightage > 0)
                        drNewRow["Weightage"] = Weightage;
                    drNewRow["Keyword"] = ((TextBox)gridViewRow.
                        FindControl("CreateAutomaticTest_byQuestion_keywordTextBox")).Text;
                    drNewRow["TestArea"] = GetSelectedValuesfromCheckBoxList(
                        (CheckBoxList)gridViewRow.FindControl("CreateAutomaticTest_byQuestion_testAreaCheckBoxList"));
                    drNewRow["Complexity"] = GetSelectedValuesfromCheckBoxList(
                        (CheckBoxList)gridViewRow.FindControl("CreateAutomaticTest_byQuestion_complexityCheckBoxList"));
                    if (((HtmlContainerControl)gridViewRow.FindControl("CreateAutomaticTest_byQuestion_expectedQuestionsDiv")).Visible)
                    {
                        drNewRow["ExpectedQuestions"] = ((Label)gridViewRow.FindControl("CreateAutomaticTest_byQuestion_expecetdQuestions")).Text;
                        drNewRow["PickedQuestions"] = ((Label)gridViewRow.FindControl("CreateAutomaticTest_byQuestions_returnQuestionsLabel")).Text;
                        drNewRow["Remarks"] = ((Label)gridViewRow.FindControl("CreateAutomaticTest_byQuestin_remarkLabel")).Text;
                        drNewRow["TotalRecordsinDB"] = ((Label)gridViewRow.FindControl("CreateAutomaticTest_byQuestion_noOfQuestionsLabel")).Text;
                    }
                    dtTestSegment.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                return dtTestSegment;
            }
            finally
            {
                if (dtTestSegment != null) dtTestSegment = null;
            }
        }

        /// <summary>
        /// Gets the elected ID's for a checkboxlist
        /// </summary>
        /// <param name="checkBoxList">checkboxlist to get the selected values</param>
        /// <returns>Selected ID's in the checkboxlist</returns>
        private string GetSelectedValuesfromCheckBoxList(CheckBoxList checkBoxList)
        {
            StringBuilder stringBuilder = null;
            try
            {
                if (stringBuilder == null)
                    stringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        stringBuilder.Append(checkBoxList.Items[i].Value);
                        stringBuilder.Append(",");
                    }
                }
                return stringBuilder.ToString().TrimEnd(',');
            }
            finally
            {
                if (stringBuilder != null) stringBuilder = null;
            }
        }

        /// <summary>
        /// Add an Emty row to the test segment grid (this methos will 
        /// also check for maximum segments limit and no of question given less than 
        /// segments or not)
        /// </summary>
        private void AddTestSegmentRow(bool AddPositionProfileRows)
        {
            DataTable dtTestSegment = BuildTableFromGridView();
            if (CreateAutomaticTest_byQuestion_totalNumberTextBox.Text.Trim() != "")
                if (Convert.ToInt32(CreateAutomaticTest_byQuestion_totalNumberTextBox.Text.Trim()) <= dtTestSegment.Rows.Count)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                     CreateAutomaticTest_bottomErrorMessageLabel,
                     Resources.HCMResource.CreateAutomaticTest_SegmentsGreaterthanSegments);
                    return;
                }
            if (dtTestSegment.Rows.Count == 10)
            {
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel, CreateAutomaticTest_bottomErrorMessageLabel,
                    "Maximum limit reached");
                return;
            }
            DataRow drNewRow = null;
            if (AddPositionProfileRows)
            {
                if (string.IsNullOrEmpty(CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.Value)) return;

                string[] PositionProfiles = CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.Value.Split(',');
                for (int i = 0; i < PositionProfiles.Length; i++)
                {
                    if (dtTestSegment.Select("Keyword LIKE '%" + GetSkillName(PositionProfiles[i]) + "%'").Length > 0)
                        continue;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drNewRow))
                        drNewRow = null;
                    drNewRow = dtTestSegment.NewRow();
                    drNewRow["Keyword"] = GetSkillName(PositionProfiles[i]);
                    drNewRow["Weightage"] = GetSkillWeightage(PositionProfiles[i]);
                    dtTestSegment.Rows.Add(drNewRow);
                    if (dtTestSegment.Rows.Count == 10)
                    {
                        base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel, CreateAutomaticTest_bottomErrorMessageLabel,
                            "Maximum limit reached");
                        break;
                    }
                }
            }
            else
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drNewRow))
                    drNewRow = null;
                drNewRow = dtTestSegment.NewRow();
                dtTestSegment.Rows.Add(drNewRow);
            }
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtTestSegment;
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
        }

        private string GetSkillName(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return strKeyWord;
            return strKeyWord.Substring(0, strKeyWord.IndexOf('['));
        }

        private string GetSkillWeightage(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return string.Empty;
            return strKeyWord.Substring(strKeyWord.IndexOf('[') + 1, strKeyWord.Length - strKeyWord.IndexOf('[') - 2);
        }

        /// <summary>
        /// Checks for an empty value in segment wise (atleast a value
        /// should be given for a criteria)
        /// </summary>
        /// <param name="dtTestSegment">Datatable of the test segment grid</param>
        /// <returns>true if a single segment had atleast one search criteria 
        /// else false</returns>
        private bool CheckForEmptyValuesInSegmentGrid(ref DataTable dtTestSegment)
        {
            bool isValid = true;
            int NoofQuestions = 0;
            StringBuilder sbErrorSegements = null;
            StringBuilder sbDeleteSegments = null;
            try
            {
                int.TryParse(CreateAutomaticTest_byQuestion_totalNumberTextBox.Text.Trim(), out NoofQuestions);
                if (NoofQuestions == 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel, "Enter valid number of questions");
                    isValid = false;
                    return isValid;
                }
                if (NoofQuestions < dtTestSegment.Rows.Count)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                          CreateAutomaticTest_bottomErrorMessageLabel,
                          Resources.HCMResource.CreateAutomaticTest_SegmentsGreaterthanSegments);
                    isValid = false;
                    return isValid;
                }
                int InvalidColumnCount = 0;
                int TotalColumns = 7;
                sbErrorSegements = new StringBuilder();
                sbDeleteSegments = new StringBuilder();
                for (int i = 0; i < dtTestSegment.Rows.Count; i++)
                {
                    for (int ColumnCount = 1; ColumnCount <= TotalColumns; ColumnCount++)
                        if (dtTestSegment.Rows[i][ColumnCount].ToString() == "")
                            InvalidColumnCount++;
                    if (InvalidColumnCount == TotalColumns)
                    {
                        sbErrorSegements.Append(i + 1);
                        sbErrorSegements.Append(",");
                        isValid = false;
                        if (i != 0)
                        {
                            sbDeleteSegments.Append(i + 1);
                            sbDeleteSegments.Append(",");
                        }
                    }
                    InvalidColumnCount = 0;
                }
                if (!isValid)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                                    CreateAutomaticTest_bottomErrorMessageLabel,
                                    string.Format(Resources.HCMResource.CreateAutomaticTest_segmentEmpty,
                                    (sbDeleteSegments.ToString().Length == 0) ?
                                    sbErrorSegements.ToString().TrimEnd(',') :
                                    sbErrorSegements.ToString().TrimEnd(',') + " or delete segement " + sbDeleteSegments.ToString().TrimEnd(',')));
                }
                return isValid;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbErrorSegements)) sbErrorSegements = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbDeleteSegments)) sbDeleteSegments = null;
            }
        }

        /// <summary>
        /// Deletes the user selected search segment from the grid
        /// </summary>
        /// <param name="RowIndex">Index of the segment to delete</param>
        private void DeleteTestSegmentRow(int RowIndex)
        {
            DataTable dtTestSegment = BuildTableFromGridView();
            if (dtTestSegment.Rows.Count == 1)
            {
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel, Resources.HCMResource.CreateAutomaticTest_AllSegementsDelee);
                return;
            }
            dtTestSegment.Rows.RemoveAt(RowIndex);
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtTestSegment;
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
        }

        /// <summary>
        /// This method do all validations for generate button clicks and 
        /// if all validations passed it will call approriate method for to 
        /// load test according to the user given serach criteria
        /// </summary>
        private void LoadAutomatedQuestions()
        {
            DataTable dtTestSearchCriteria = null;
            try
            {
                dtTestSearchCriteria = BuildTableFromGridView();
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtTestSearchCriteria;
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
                if (!CheckForEmptyValuesInSegmentGrid(ref dtTestSearchCriteria))
                    return;
                LoadQuestions(Convert.ToInt32(CreateAutomaticTest_byQuestion_totalNumberTextBox.Text.Trim()),
                    ref dtTestSearchCriteria);
            }
            finally
            {
                if (dtTestSearchCriteria != null) dtTestSearchCriteria = null;
            }
        }

        /// <summary>
        /// This method loads the user provided search criteria 
        /// in to collection of 'TestSearchCriteria' data object
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid data table</param>
        /// <returns>List of 'TestSearchCriteria' data object that contains
        /// user given search criteria</returns>
        private List<TestSearchCriteria> GetQuestionSearchCriteria(ref DataTable dtSearchCriteria)
        {
            List<TestSearchCriteria> testSearchCriterias = null;
            TestSearchCriteria testSearchCriteria = null;
            Subject subject = null;
            ////DataView SearchQuestionsOrderedDataView = null;
            ////DataTable TempDataTable = null;
            try
            {
                //SearchQuestionsOrderedDataView = dtSearchCriteria.DefaultView;
                //SearchQuestionsOrderedDataView.Sort = "ExpectedQuestions ASC";
                //TempDataTable = SearchQuestionsOrderedDataView.ToTable();
                for (int i = 0; i < dtSearchCriteria.Rows.Count; i++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria))
                        testSearchCriteria = new TestSearchCriteria();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Cat_sub_id"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(subject))
                            subject = new Subject();
                        subject.SubjectID = Convert.ToInt32(dtSearchCriteria.Rows[i]["Cat_sub_id"]);
                        testSearchCriteria.Subjects = new List<Subject>();
                        testSearchCriteria.Subjects.Add(subject);
                    }
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["TestArea"]))
                        testSearchCriteria.TestAreasID = dtSearchCriteria.Rows[i]["TestArea"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Complexity"]))
                        testSearchCriteria.Complexity = dtSearchCriteria.Rows[i]["Complexity"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Keyword"]))
                        testSearchCriteria.Keyword = dtSearchCriteria.Rows[i]["Keyword"].ToString();
                    testSearchCriteria.NoOfQuestions = Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"]);
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias))
                        testSearchCriterias = new List<TestSearchCriteria>();
                    testSearchCriterias.Add(testSearchCriteria);
                    testSearchCriteria = null;
                    subject = null;
                }
                return testSearchCriterias;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias)) testSearchCriterias = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(subject)) subject = null;
                //if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchQuestionsOrderedDataView)) SearchQuestionsOrderedDataView = null;
                //if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TempDataTable)) TempDataTable = null;
            }
        }

        /// <summary>
        /// This methods distributes the total questions to the segments
        /// according to the segment weightage 
        /// either given by user or system generated.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        private void LoadExpectedQuestions(ref DataTable dtSearchCriteria, int NoofQuestions, out bool ZeroExpectedQuestion)
        {
            ZeroExpectedQuestion = false;
            int SearchSegements = dtSearchCriteria.Rows.Count;
            int QuestionsAdded = 0;
            int ExpectedQuestions = 0;
            int LoopI = 0;
            for (LoopI = 0; LoopI < SearchSegements; LoopI++)
            {
                QuestionsAdded = Convert.ToInt32((Convert.ToDecimal(dtSearchCriteria.Rows[LoopI]["Weightage"])
                    / 100) * NoofQuestions);
                dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] = QuestionsAdded;
                ExpectedQuestions += QuestionsAdded;
            }
            if (ExpectedQuestions > NoofQuestions)
            {
                int QuestionsToAdd = ExpectedQuestions - NoofQuestions;
                for (LoopI = SearchSegements - 1; LoopI >= 0; LoopI--)
                {
                    if (Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd <= 0)
                        continue;
                    dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd;
                    break;
                }
            }
            else if (ExpectedQuestions < NoofQuestions)
                dtSearchCriteria.Rows[LoopI - 1]["ExpectedQuestions"] = QuestionsAdded +
                    (NoofQuestions - ExpectedQuestions);
            if (dtSearchCriteria.Select("ExpectedQuestions <= 0").Length > 0)
            {
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                         CreateAutomaticTest_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticTest_ExpectedQuestionZero);
                ZeroExpectedQuestion = true;
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                         CreateAutomaticTest_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                    dtSearchCriteria.Rows[LoopI]["PickedQuestions"] = 0;
                    dtSearchCriteria.Rows[LoopI]["TotalRecordsinDB"] = 0;
                    dtSearchCriteria.Rows[LoopI]["QuestionsDifference"] = 0;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// This method finally communicates with question detail BL manager
        /// to get the questions for the test with user provided serach
        /// criteria.
        /// </summary>
        /// <param name="NoofQuestions">Total number of questions to pick for 
        /// the test</param>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        private void LoadQuestions(int NoofQuestions, ref DataTable dtSearchCriteria)
        {
            int TotalWeightage = 0;
            bool ZeroExpectedQuestion = false;
            LoadWeightages(ref dtSearchCriteria, NoofQuestions, out TotalWeightage);
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtSearchCriteria;
            CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
            if (TotalWeightage > 100)
                return;
            LoadExpectedQuestions(ref dtSearchCriteria, NoofQuestions, out ZeroExpectedQuestion);
            if (ZeroExpectedQuestion)
                return;
            QuestionType questionType = (QuestionType)Convert.ToInt32(CreateAutomaticTest_byQuestion_questionTypeDropDownList.SelectedValue);

            List<QuestionDetail> questionDetails = new QuestionBLManager().GetAutomatedQuestions(questionType, 
                GetQuestionSearchCriteria(ref dtSearchCriteria), NoofQuestions, ref dtSearchCriteria,
                Convert.ToInt32(CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value),base.userID);
            if ((questionDetails == null) || (questionDetails.Count == 0))
            {
                CreateAutomaticTest_byQuestion_testDrftDiv.Visible = false;
                CreateAutomaticTest_byQuestion_automatedTestUserControl.Visible = false;
                CreateAutomaticTest_byQuestion_generateTestButton.Visible = false;
                CreateAutomaticTest_byQuestion_generateTestButton.Text = "Generate Test";
                CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value = "0";
                CreateAutomaticTest_byQuestion_testDetailsUserControl.SysRecommendedTime = 0;
                SetEnableStatusForSearchControls(true);
                SetVisibleStatusForButton(false);
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtSearchCriteria;
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
                base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                ViewState[CREATEAUTOMATICTEST_QUESTIONVIEWSTATE] = questionDetails;
                TestStatistics testStatistics = AutomatedSummaryList(questionDetails);
                LoadTestStatisticsObject(ref questionDetails, ref testStatistics);
                CreateAutomaticTest_byQuestion_automatedTestUserControl.DataSource = testStatistics;
                CreateAutomaticTest_byQuestion_automatedTestUserControl.Visible = true;
                CreateAutomaticTest_byQuestion_testDrftDiv.Visible = true;
                if (NoofQuestions == questionDetails.Count)
                {
                    CreateAutomaticTest_byQuestion_generateTestButton.Text = "Regenerate Test";
                    CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value =
                        (Convert.ToInt32(CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value) + 1).ToString();
                    if (Convert.ToInt32(CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value) > 0)
                        SetEnableStatusForSearchControls(false);
                }
                else
                {
                    CreateAutomaticTest_byQuestion_generateTestButton.Text = "Generate Test";
                    CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value = "0";
                    SetEnableStatusForSearchControls(true);
                }
                CreateAutomaticTest_byQuestion_testDraftGridView.DataSource = questionDetails;
                CreateAutomaticTest_byQuestion_testDraftGridView.DataBind();
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataSource = dtSearchCriteria;
                CreateAutomaticTest_byQuestion_testSegmantGridView.DataBind();
            }
        }

        private void SetEnableStatusForSearchControls(bool VisibilityStatus)
        {
            CreateAutomaticTest_byQuestion_totalNumberTextBox.Enabled = VisibilityStatus;
            //CreateAutomaticTest_byQuestion_totalCostTextBox.Enabled = VisibilityStatus;
            CreateAutomaticTest_byQuestion_positionProfileTextBox.Enabled = VisibilityStatus;
            CreateAutomaticTest_byQuestion_authorImageButton.Visible = VisibilityStatus;
            CreateAutomaticTest_byQuestion_timeSearchTextbox.Enabled = VisibilityStatus;
            CreateAutomaticTest_byQuestion_addSegmantLinkButton.Visible = VisibilityStatus;
            SearchCategorySubjectControl_addLinkButton.Visible = VisibilityStatus;
        }

        /// <summary>
        /// This method will load all the test statistics user control data.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object.</param>
        private void LoadTestStatisticsObject(ref List<QuestionDetail> questionDetails, ref TestStatistics testStatistics)
        {
            TestDetail testDetail = null;
            try
            {
                testStatistics.NoOfQuestions = questionDetails.Count;
                testStatistics.TestCost = 0;
                for (int i = 0; i < questionDetails.Count; i++)
                    testStatistics.TestCost += questionDetails[i].CreditsEarned;
                if (testStatistics.TestCost == 0)
                    testStatistics.TestCost = 0.00M;
                testStatistics.AverageTimeTakenByCandidates =
                    Convert.ToInt32(questionDetails.Average(p => p.AverageTimeTaken)) * questionDetails.Count;
                testStatistics.AutomatedTestAverageComplexity = ((AttributeDetail)
                    (new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetails))).
                    AttributeName;
                LoadChartDetails(ref questionDetails, ref testStatistics);
                testDetail = CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource;
                testDetail.SystemRecommendedTime = 0;
                testDetail.SystemRecommendedTime = Convert.ToInt32(testStatistics.AverageTimeTakenByCandidates);
                CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        /// <summary>
        /// This method loads the chart details to the test statistics object.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object by adding chart controls data.</param>
        private void LoadChartDetails(ref List<QuestionDetail> questionDetails,
            ref TestStatistics testStatistics)
        {
            ChartData chartData = null;
            List<ChartData> testAreaChartDatum = null;
            List<ChartData> complexityChartDatum = null;
            try
            {
                var complexityGroups =
                                          from q in questionDetails
                                          group q by q.ComplexityName into g
                                          select new { ComplexityName = g.Key, ComplexityCount = g.Count() };
                complexityChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var complexity in complexityGroups)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = complexity.ComplexityName;
                    chartData.ChartYValue = complexity.ComplexityCount;
                    complexityChartDatum.Add(chartData);
                }
                chartData = null;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.ComplexityStatisticsChartData))
                    testStatistics.ComplexityStatisticsChartData = new SingleChartData();
                testStatistics.ComplexityStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.ComplexityStatisticsChartData.ChartData = complexityChartDatum;
                testStatistics.ComplexityStatisticsChartData.ChartLength = 140;
                testStatistics.ComplexityStatisticsChartData.ChartWidth = 300;
                testStatistics.ComplexityStatisticsChartData.ChartTitle = "Complexity Statistics";
                var testAreaCounts =
                        from t in questionDetails
                        group t by t.TestAreaName into g
                        select new { TestAreaName = g.Key, TestAreaCOunt = g.Count() };
                testAreaChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var testArea in testAreaCounts)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = testArea.TestAreaName;
                    chartData.ChartYValue = testArea.TestAreaCOunt;
                    testAreaChartDatum.Add(chartData);
                }
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.TestAreaStatisticsChartData))
                    testStatistics.TestAreaStatisticsChartData = new SingleChartData();
                testStatistics.TestAreaStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.TestAreaStatisticsChartData.ChartData = testAreaChartDatum;
                testStatistics.TestAreaStatisticsChartData.ChartLength = 140;
                testStatistics.TestAreaStatisticsChartData.ChartWidth = 300;
                testStatistics.TestAreaStatisticsChartData.ChartTitle = "Test Area Statistics";
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(chartData)) chartData = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testAreaChartDatum)) testAreaChartDatum = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(complexityChartDatum)) complexityChartDatum = null;
            }
        }

        /// <summary>
        /// This Method gives the summary of the result questions.
        /// Summary will be like 
        /// Category Name, subject, test area, complexity, total questions
        /// </summary>
        /// <param name="ResultQuestionDetails">Question List that o/p to user</param>
        /// <returns>List of strings with each string contains
        /// category & subject & Test Area & Complexity & 
        /// Toal Number of Questions shown to the user</returns>
        private TestStatistics AutomatedSummaryList(List<QuestionDetail> ResultQuestionDetails)
        {
            StringBuilder CategoryNames = new StringBuilder();
            AutomatedTestSummaryGrid automatedTestSummaryGrid = null;
            TestStatistics testStatistics = null;
            List<AutomatedTestSummaryGrid> automatedTestSummaryGridOrderdByQuestion = null;
            int TotalCategory = 0;
            try
            {
                for (int i = 0; i < ResultQuestionDetails.Count; i++)
                {
                    if (CategoryNames.ToString().IndexOf("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() + " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() + " COMP: " + ResultQuestionDetails[i].Complexity.Trim()) >= 0)
                        continue;
                    TotalCategory = ResultQuestionDetails.FindAll(
                        p => "CAT: " + p.CategoryName.Trim() + " SUB: " + p.SubjectName.Trim() +
                    " TA: " + p.TestAreaName.Trim() +
                    " COMP: " + p.Complexity.Trim() == "CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim()).Count;
                    CategoryNames.Append("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim());
                    CategoryNames.Append(",");
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                        automatedTestSummaryGrid = new AutomatedTestSummaryGrid();
                    automatedTestSummaryGrid.CategoryName = ResultQuestionDetails[i].CategoryName;
                    automatedTestSummaryGrid.SubjectName = ResultQuestionDetails[i].SubjectName;
                    automatedTestSummaryGrid.TestAreaName = ResultQuestionDetails[i].TestAreaName;
                    automatedTestSummaryGrid.Complexity = ResultQuestionDetails[i].ComplexityName;
                    automatedTestSummaryGrid.NoofQuestionsInCategory = TotalCategory;
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics))
                        testStatistics = new TestStatistics();
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                        testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                    testStatistics.AutomatedTestSummaryGrid.Add(automatedTestSummaryGrid);
                    automatedTestSummaryGrid = null;
                    TotalCategory = 0;
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                {
                    automatedTestSummaryGridOrderdByQuestion = testStatistics.AutomatedTestSummaryGrid.OrderByDescending(p => p.NoofQuestionsInCategory).ToList();
                    testStatistics.AutomatedTestSummaryGrid = null;
                    testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                    testStatistics.AutomatedTestSummaryGrid = automatedTestSummaryGridOrderdByQuestion;
                }
                testStatistics.NoOfQuestions = ResultQuestionDetails.Count;
                return testStatistics;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CategoryNames)) CategoryNames = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics)) testStatistics = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                    automatedTestSummaryGrid = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGridOrderdByQuestion))
                    automatedTestSummaryGridOrderdByQuestion = null;
            }
        }

        /// <summary>
        /// Gets the test area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetTestAreaList()
        {
            return new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA,
                  Constants.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Gets the complexity area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetComplexityList()
        {
            return new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY,
                Constants.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CreateAutomaticTest_byQuestion_isMaximizedHiddenField.Value) &&
                         CreateAutomaticTest_byQuestion_isMaximizedHiddenField.Value == "Y")
            {
                CreateAutomaticTest_byQuestion_serachDiv.Style["display"] = "none";
                CreateAutomaticTest_byQuestion_searchResultsUpSpan.Style["display"] = "block";
                CreateAutomaticTest_byQuestion_searchResultsDownSpan.Style["display"] = "none";
                CreateAutomaticTest_byQuestion_testDrftDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CreateAutomaticTest_byQuestion_serachDiv.Style["display"] = "block";
                CreateAutomaticTest_byQuestion_searchResultsUpSpan.Style["display"] = "none";
                CreateAutomaticTest_byQuestion_searchResultsDownSpan.Style["display"] = "block";
                CreateAutomaticTest_byQuestion_testDrftDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Load all javascript tags
        /// </summary>
        private void SubscribeClientSideHandlers()
        {
            CreateAutomaticTest_byQuestion_authorImageButton.Attributes.Add("onclick",
                    "return ShowClientRequestToGetTagWeightages('" + CreateAutomaticTest_byQuestion_positionProfileTextBox.ClientID + "','" +
                    CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.ClientID + "','" +
                    CreateAutomaticTest_byQuestion_positionProfileIDHiddenField.ClientID + "');");
            CreateAutomaticTest_byQuestion_searchResultsUpSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                CreateAutomaticTest_byQuestion_testDrftDiv.ClientID + "','" +
                CreateAutomaticTest_byQuestion_serachDiv.ClientID + "','" +
                CreateAutomaticTest_byQuestion_searchResultsUpSpan.ClientID + "','" +
                CreateAutomaticTest_byQuestion_searchResultsDownSpan.ClientID + "','" +
                CreateAutomaticTest_byQuestion_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
            CreateAutomaticTest_byQuestion_searchResultsDownSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                CreateAutomaticTest_byQuestion_testDrftDiv.ClientID + "','" +
                CreateAutomaticTest_byQuestion_serachDiv.ClientID + "','" +
                CreateAutomaticTest_byQuestion_searchResultsUpSpan.ClientID + "','" +
                CreateAutomaticTest_byQuestion_searchResultsDownSpan.ClientID + "','" +
                CreateAutomaticTest_byQuestion_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
        }

        /// <summary>
        /// Sets the Visibility Status for save and create session buttons.
        /// </summary>
        /// <param name="VisibilityStatus">Visibility status for those button <br />
        /// Note(:- Save button Visibile status show with 'Not' gate)</param>
        private void SetVisibleStatusForButton(bool VisibilityStatus)
        {
            CreateAutomaticTest_topCreateSessionButton.Visible = VisibilityStatus;
            CreateAutomaticTest_bottomCreateSessionButton.Visible = VisibilityStatus;
            CreateAutomaticTest_bottomSaveButton.Visible = !VisibilityStatus;
            CreateAutomaticTest_topSaveButton.Visible = !VisibilityStatus;
            CreateAutomaticTest_byQuestion_generateTestButton.Visible = !VisibilityStatus;
        }

        /// <summary>
        /// This method enables or disables test segment grid controls
        /// according to the search status.
        /// </summary>
        /// <param name="RowIndex">Row index of the grid to disable controls</param>
        private void EnableOrDisableTestSegmentGridControls(GridViewRow CurrenRowBounding)
        {
            if (CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value == "")
                return;
            if (Convert.ToInt32(CreateAutomaticTest_byQuestion_generateTestHitCountHiddenField.Value) == 0)
                return;
            ((ImageButton)
                CurrenRowBounding.FindControl("CreateAutomaticTest_byQuestion_testSegmantGridView_deleteImageButton")).Visible = false;
            ((ImageButton)CurrenRowBounding
                .FindControl("CreateAutomaticTest_byQuestion_categoryImageButton")).Visible = false;
            ((TextBox)CurrenRowBounding.
                FindControl("CreateAutomaticTest_byQuestion_weightageTextBox")).Enabled = false;
            ((TextBox)CurrenRowBounding.
                FindControl("CreateAutomaticTest_byQuestion_keywordTextBox")).Enabled = false;
            ((CheckBoxList)CurrenRowBounding.
                FindControl("CreateAutomaticTest_byQuestion_complexityCheckBoxList")).Enabled = false;
            ((CheckBoxList)CurrenRowBounding.
                FindControl("CreateAutomaticTest_byQuestion_testAreaCheckBoxList")).Enabled = false;
        }


        private DataTable FilledSegmants(ref DataTable dtTestSegment)
        {
            int InvalidColumnCount = 0;
            int TotalColumns = 7;
            for (int i = 0; i < dtTestSegment.Rows.Count; i++)
            {
                for (int ColumnCount = 1; ColumnCount <= TotalColumns; ColumnCount++)
                    if (dtTestSegment.Rows[i][ColumnCount].ToString() == "")
                        InvalidColumnCount++;
                if (InvalidColumnCount == TotalColumns)
                {
                    dtTestSegment.Rows.Remove(dtTestSegment.Rows[i]);
                }
                InvalidColumnCount = 0;
            }
            return dtTestSegment;
        }


        #endregion Private Methods

        #region Protected Overriden Methods                                    

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool FirstTabPriority = false;
            int NoofQuestions = 0;
            List<QuestionDetail> questionDetailTestDraftResultList = null;
            TestDetail testDetail = null;
            try
            {
                int.TryParse(CreateAutomaticTest_byQuestion_totalNumberTextBox.Text.Trim(), out NoofQuestions);
                if (NoofQuestions == 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel, "Enter valid number of questions");
                    isValidData = false;
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 0;
                    FirstTabPriority = true;
                    //                    return isValidData;
                }
                testDetail = new TestDetail();
                testDetail = CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource;
                int recommendedTime = testDetail.RecommendedCompletionTime;
                if (testDetail.Name.Trim().Length == 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                       CreateAutomaticTest_bottomErrorMessageLabel,
                       Resources.HCMResource.CreateAutomaticTest_TestNameEmpty);
                    isValidData = false;
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 1;
                }
                if (testDetail.Description.Trim().Length == 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                       CreateAutomaticTest_bottomErrorMessageLabel,
                       Resources.HCMResource.CreateManualTest_Test_Description_Empty);
                    isValidData = false;
                }
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                questionDetailTestDraftResultList = ViewState[CREATEAUTOMATICTEST_QUESTIONVIEWSTATE] as List<QuestionDetail>;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateAutomaticTest_QuestionsEmpty);
                    isValidData = false;
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 0;
                    FirstTabPriority = true;
                }
                else if (questionDetailTestDraftResultList.Count == 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateManualTest_QuestionCount);
                    isValidData = false;
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 0;
                    FirstTabPriority = true;
                }
                if (recommendedTime == -1)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                   CreateAutomaticTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateAutomaticTest_TestDetailsControl_recommendedTimeLabelTextBox_Empty);
                    isValidData = false;
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 1;
                }
                else if (recommendedTime == -2)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                   CreateAutomaticTest_bottomErrorMessageLabel,
                    string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended"));
                    isValidData = false;
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 1;
                }
                else if (recommendedTime == 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                    CreateAutomaticTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateAutomaticTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                    isValidData = false;
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 1;
                }
                if (FirstTabPriority)
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 0;
                if (!(bool)testDetail.IsCertification)
                    return isValidData;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateDetailsEmpty);
                    isValidData = false;
                }
                if (testDetail.CertificationDetail.CertificateID <= 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateFormatEmpty);
                    isValidData = false;
                }
                if (testDetail.CertificationDetail.DaysElapseBetweenRetakes < 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyDaysRetakeTest);
                    isValidData = false;
                }
                if (testDetail.CertificationDetail.PermissibleRetakes <= 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyRetakes);
                    isValidData = false;
                }
                if (testDetail.CertificationDetail.MinimumTotalScoreRequired <= 0)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificateTest_MinimumQualifacationEmpty);
                    isValidData = false;
                }
                if (testDetail.CertificationDetail.CertificateValidity == "0")
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                         Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateValidity);
                    isValidData = false;
                }
                if (testDetail.CertificationDetail.MaximumTimePermissible == -2)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                            CreateAutomaticTest_bottomErrorMessageLabel,
                            string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended certification completion"));
                    isValidData = false;
                }
                if (testDetail.CertificationDetail.MaximumTimePermissible == -1)
                {
                    base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                        CreateAutomaticTest_bottomErrorMessageLabel,
                       Resources.HCMResource.CreateAutomaticTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                    isValidData = false;
                }
                if ((testDetail.RecommendedCompletionTime > 0) && (testDetail.CertificationDetail.MaximumTimePermissible > 0))
                {
                    if (testDetail.RecommendedCompletionTime < testDetail.CertificationDetail.MaximumTimePermissible)
                    {
                        base.ShowMessage(CreateAutomaticTest_topErrorMessageLabel,
                            CreateAutomaticTest_bottomErrorMessageLabel,
                            Resources.HCMResource.TestDetailsControl_CertificationTest_CertificationTimeExceeds);
                        isValidData = false;
                    }
                }
                CreateAutomaticTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;
                if (FirstTabPriority)
                    CreateAutomaticTest_mainTabContainer.ActiveTabIndex = 0;
                return isValidData;
            }
            finally
            {

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList)) questionDetailTestDraftResultList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            SetVisibleStatusForButton(false);
            SubscribeClientSideHandlers();
            
            CreateAutomaticTest_byQuestion_automatedTestUserControl.Visible = false;
            LoadPositionProfileDetail();
            if (Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING] != null &&
                Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING != string.Empty)
                return;

            BindEmptyTestSegmentGrid();
        }

        /// <summary>
        /// Represents the method to load the position profile details from the query string
        /// </summary>
        private void LoadPositionProfileDetail()
        {
            // Check if position profile ID is present.
            if (Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING] == null ||
                Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING].Trim().Length == 0)
            {
                return;
            }

            int positionProfileID = 0;

            // Check if the query string contains a valid position profile ID.
            if (!int.TryParse(Request.QueryString
                [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
            {
                return;
            }

            PositionProfileDetail positionProfileDetail = new PositionProfileBLManager().GetPositionProfileKeyWord(positionProfileID);
            if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                return;

            CreateAutomaticTest_byQuestion_positionProfileTextBox.Text = positionProfileDetail.PositionProfileName;
            CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.Value = positionProfileDetail.Keys;
            try
            {
                AddTestSegmentRow(true);
            }
            finally
            {
                CreateAutomaticTest_byQuestion_positionProfileTagsHiddenField.Value = "";
            }
            //CreateAutomaticTest_byQuestion_positionProfileIdHiddenField.Value = ppID.ToString();
            CreateAutomaticTest_byQuestion_positionProfileIDHiddenField.Value = positionProfileDetail.PositionProfileID.ToString();

            // Assign the position profile name and ID to the test details tab.
            CreateAutomaticTest_byQuestion_testDetailsUserControl.PositionProfileID = positionProfileID;
            CreateAutomaticTest_byQuestion_testDetailsUserControl.PositionProfileName = positionProfileDetail.PositionProfileName;
        }

        #endregion Protected Overriden Methods

    }
}