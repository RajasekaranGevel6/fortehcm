﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateTestSession.cs
// File that represents the user interface for the create a new test
// session. This will interact with the TestBLManager to insert test 
// session details to the database.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for CreateTestSession page. This is used
    /// to create a test session by providing number of candidates,
    /// session description, test instructions etc. 
    /// Also this page provides some set of links in the test session
    /// grid for viewing candidate detail, test session and cancel the
    /// cancel the candidate test session. This class is inherited from
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CreateTestSession : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Variables                                           

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will fire when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = CreateTestSession_saveCancellationButton.UniqueID;
                Master.SetPageCaption(Resources.HCMResource.CreateTestSession_Title);

                // Call pagination event handler
                CreateTestSession_bottomSessionPagingNavigator.PageNumberClick
                    += new PageNavigator.PageNumberClickEventHandler
                        (CreateTestSession_bottomSessionPagingNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = CreateTestSession_sessionDescTextBox.UniqueID;
                    CreateTestSession_sessionDescTextBox.Focus();

                    if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                        ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                        ViewState["SORT_EXPRESSION"] = "TestSessionID";

                    if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                        ViewState["PAGE_NUMBER"] = "1";

                    if (Request.QueryString["testkey"] != null)
                    {
                        CreateTestSession_TestKeyHiddenField.Value =
                            Request.QueryString["testkey"].ToString();
                    }
                    LoadTestDetail(CreateTestSession_TestKeyHiddenField.Value);

                    // Enable/disable cyber proctoring based on feature
                    // applicability.
                    bool isCyberProctorApplicable = new
                        AdminBLManager().IsFeatureApplicable(base.tenantID, 
                        Constants.FeatureConstants.CYBER_PROCTOR);

                    CreateTestSession_cyberProctorateCheckBox.Enabled = isCyberProctorApplicable;
                    CreateTestSession_cyberProctorWhyDisabledLinkButton.Visible = !isCyberProctorApplicable;
                }

                // Add javascript function for expanding and collapsing gridview.
                CreateTestSession_searchTestSessionResultsTR.Attributes.Add("onclick", "ExpandorCompress('"
                  + CreateTestSession_testSessionDiv.ClientID + "','"
                  + CreateTestSession_sessionDetailsDIV.ClientID
                  + "','CreateTestSession_searchSessionResultsUpSpan',"
                  + "'CreateTestSession_searchSessionResultsDownSpan')");

                // Add handler for email button.
                CreateTestSession_emailImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowEmailCandidateSessions('EMAIL_TEST_SESSION')");

                // Validation for expand all and collapse all image button
                if (!Utility.IsNullOrEmpty(CreateTestSession_restoreHiddenField.Value) &&
                   CreateTestSession_restoreHiddenField.Value == "Y")
                {
                    CreateTestSession_sessionDetailsDIV.Style["display"] = "none";
                    CreateTestSession_searchSessionResultsUpSpan.Style["display"] = "block";
                    CreateTestSession_searchSessionResultsDownSpan.Style["display"] = "none";
                    CreateTestSession_testSessionDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    CreateTestSession_sessionDetailsDIV.Style["display"] = "block";
                    CreateTestSession_searchSessionResultsUpSpan.Style["display"] = "none";
                    CreateTestSession_searchSessionResultsDownSpan.Style["display"] = "block";
                    CreateTestSession_testSessionDiv.Style["height"] = RESTORED_HEIGHT;
                }
                CreateTestSession_searchTestSessionResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    CreateTestSession_testSessionDiv.ClientID + "','" +
                    CreateTestSession_sessionDetailsDIV.ClientID + "','" +
                    CreateTestSession_searchSessionResultsUpSpan.ClientID + "','" +
                    CreateTestSession_searchSessionResultsDownSpan.ClientID + "','" +
                    CreateTestSession_restoreHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when a page number is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that contains the event data.
        /// </param>
        void CreateTestSession_bottomSessionPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            LoadTestSessions(e.PageNumber);
            ViewState["PAGE_NUMBER"] = e.PageNumber;
        }

        /// <summary>
        /// Handler that will help to validate input fields before they saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateTestSession_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                // Validate the data
                if (!IsValidData())
                    return;

                TestSessionDetail testSessionDetail = PreviewTestSession();

                // Set the TestSessionPreview Mode. If the mode is VIEW, checkboxes
                // won't be displayed for Randomize question, cyberproctoring, and
                // display results to candidate. Instead, respective label control
                // will be shown as YES.
                CreateTestSession_viewTestSessionSave_UserControl.Mode = "view";

                CreateTestSession_viewTestSessionSave_UserControl.DataSource = testSessionDetail;
                CreateTestSession_viewTestSessionSave_modalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when clicking on create button which shows
        /// the given information before its getting inserted in the database.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateTestSession_previewTestSessionControl_createButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear success/error label message
                ClearLabelMessage();

                TestSessionDetail testSessionDetail = ConstructTestSessionDetail();

                CreditRequestDetail creditRequestDetail = new CreditRequestDetail();

                creditRequestDetail.Amount = decimal.Parse(CreateTestSession_creditValueLabel.Text);

                string testSessionID = null;
                string candidateSessionIDs = null;
                bool isMailSent = false;
                
                new TestBLManager().SaveTestSession(testSessionDetail, base.userID,
                    out testSessionID, out candidateSessionIDs, out isMailSent, creditRequestDetail);

                // Keep the TestSessionID in Viewstate. 
                // This will be used by ScheduleCandidate button event handler
                ViewState["TEST_SESSION_ID"] = testSessionID;

                base.ShowMessage(CreateTestSession_topSuccessMessageLabel,
                    CreateTestSession_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.
                    CreateTestSession_TestSessionAndCandidateSessionsCreatedSuccessfully,
                    testSessionID, candidateSessionIDs));

                // If the mail sent failed, then show the error message.
                if (isMailSent == false)
                {
                    base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                        CreateTestSession_bottomErrorMessageLabel,
                         "<br>Mail cannot be sent to the session author. Contact your administrator");
                }

                CreateTestSession_sessionKeyValueLabel.Text =
                    (testSessionID != null ? testSessionID : string.Empty);

                // Set visible false once the session is created.
                CreateTestSession_topSaveButton.Visible = false;

                // Enable the schedule candidate button
                CreateTestSession_topScheduleCandidateButton.Visible = true;

                // Show the email session button.
                CreateTestSession_emailImageButton.Visible = true;

                // Keep the created test session details in session, so that it can
                // be used in the email popup.
                Session["EMAIL_TEST_SESSION_SUBJECT"] = string.Format
                    ("Test session '{0}' created", testSessionID);
                Session["EMAIL_TEST_SESSION_MESSAGE"] = string.Format
                    ("Dear Viewer,\n\nSub: Test session '{0}' created.\n\nThe following candidate sessions are associated:\n{1}", 
                    testSessionID, candidateSessionIDs);

                // Refresh test session details gridview i.e. Once a testsession is
                // created, that will be loaded in the gridview
                LoadTestSessions(1);
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when clicking anyone of the icons such as,
        /// CandidateDetail/View Test Session/Cancel Test Session by passing 
        /// its command name and command argument.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void CreateTestSession_testSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "CandidateDetail")
                {
                    CandidateTestDetail candidateDetail = new CandidateTestDetail();
                    candidateDetail.CandidateSessionID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("CreateTestSession_byTestSession_candidateSessionIdLabel")).Text;
                    candidateDetail.AttemptID = int.Parse(((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("CreateTestSession_attemptIdHiddenField")).Value);
                    candidateDetail.TestID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("CreateTestSession_byTestSession_testKeyLabel")).Text;
                     candidateDetail.CandidateName = ((Label)((ImageButton)e.CommandSource).
                        FindControl("CreateTestSession_candidateFullnameLabel")).Text;
                     candidateDetail.ShowTestScore = new TestSessionBLManager().GetShowTestScore(candidateDetail.CandidateSessionID);
                    CreateTestSession_candidateDetailControl.Datasource = candidateDetail;
                    CreateTestSession_candidateDetailModalPopupExtender.CancelControlID =
                        ((ImageButton)CreateTestSession_candidateDetailControl.
                        FindControl("CandidateDetailControl_topCancelImageButton")).ClientID;
                    CreateTestSession_candidateDetailModalPopupExtender.Show();
                }
                else if (e.CommandName == "ViewTestSession")
                {
                    // Instead of dummy, if we pass empty string, it returns all 
                    // candidate sessions available in the session.
                    TestSessionDetail testSession =
                        new TestSchedulerBLManager().GetTestSessionDetail
                        (e.CommandArgument.ToString(), "dummy",base.userID);

                    // Assign test session object to the preview test session user
                    // control's datasource. This will show the test session detail 
                    // before create.
                    CreateTestSession_previewTestSessionControl_userControl1.DataSource = testSession;
                    CreateTestSession_previewTestSessionControl_modalpPopupExtender1.Show();
                }
                else if (e.CommandName == "CancelTestSession")
                {
                    CreateTestSession_cancelTestReasonTextBox.Text = string.Empty;

                    HiddenField attemptIdHiddenField = (e.CommandSource as ImageButton).Parent.
                        FindControl("CreateTestSession_attemptIdHiddenField") as HiddenField;

                    ViewState["CANDIDATE_SESSION_ID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;

                    CreateTestSession_cancelErrorMessageLabel.Text = string.Empty;
                    CreateTestSession_cancelSessionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call whenever a row gets the information. 
        /// It implements the mouse over and out style and showing icons
        /// based on the validation.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> contains the event data.
        /// </param>
        protected void CreateTestSession_testSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton CreateTestSession_candidateDetailImageButton = (ImageButton)
                    e.Row.FindControl("CreateTestSession_candidateDetailImageButton");

                ImageButton CreateTestSession_cancelCreateTestSessionImageButton = (ImageButton)
                    e.Row.FindControl("CreateTestSession_cancelCreateTestSessionImageButton");

                HiddenField CreateTestSession_statusHiddenField = (HiddenField)
                    e.Row.FindControl("CreateTestSession_statusHiddenField");

                // Set visibility based on the attempt status
                if (CreateTestSession_statusHiddenField.Value.Trim()
                    == Constants.CandidateAttemptStatus.SCHEDULED)
                {
                    CreateTestSession_candidateDetailImageButton.Visible = true;
                    CreateTestSession_cancelCreateTestSessionImageButton.Visible = true;
                }

                if (CreateTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.NOT_SCHEDULED
                    || CreateTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CANCELLED)
                {
                    CreateTestSession_candidateDetailImageButton.Visible = false;
                    CreateTestSession_cancelCreateTestSessionImageButton.Visible = false;
                }
                else if (CreateTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.IN_PROGRESS
                    || CreateTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CYBER_PROC_INIT
                    || CreateTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.COMPLETED
                    || CreateTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.ELAPSED
                    || CreateTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.QUIT)
                {
                    CreateTestSession_candidateDetailImageButton.Visible = true;
                    CreateTestSession_cancelCreateTestSessionImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Handler that will trigger on clicking the save button. 
        /// This cancels the candidate test session by updating the reason.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateTestSession_saveCancellationButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                if (ViewState["ATTEMPT_ID"] == null || ViewState["CANDIDATE_SESSION_ID"] == null)
                    return;

                // Check if the reason text box is empty. If it is, show the error message.
                // Otherwise, update the session status as CANCEL.
                if (CreateTestSession_cancelTestReasonTextBox.Text.Trim().Length == 0)
                {
                    CreateTestSession_cancelErrorMessageLabel.Text = "Reason cannot be empty";
                    CreateTestSession_cancelSessionModalPopupExtender.Show();
                }
                else
                {
                    int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                    string candidateSessionId = ViewState["CANDIDATE_SESSION_ID"].ToString();

                    bool isMailSent = true;

                    new TestConductionBLManager().UpdateSessionStatus(candidateSessionId,
                       attemptId, Constants.CandidateAttemptStatus.CANCELLED,
                       Constants.CandidateSessionStatus.CANCELLED, base.userID, out isMailSent);

                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();

                    candidateTestSession.AttemptID = attemptId;
                    candidateTestSession.CandidateTestSessionID = candidateSessionId;
                    candidateTestSession.CancelReason = CreateTestSession_cancelTestReasonTextBox.Text.Trim();
                    candidateTestSession.ModifiedBy = base.userID;

                    new TestBLManager().CancelTestSession(candidateTestSession);

                    base.ShowMessage(CreateTestSession_topSuccessMessageLabel,
                        CreateTestSession_bottomSuccessMessageLabel,
                        string.Format(Resources.HCMResource.CreateTestSession_SessionCancelled,
                        candidateSessionId));

                    LoadTestSessions(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Handler that will perform reset/clear the input fields.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateTestSession_resetButton_Click(object sender, EventArgs e)
        {
            CreateTestSession_sessionDescTextBox.Text = string.Empty;
            CreateTestSession_instructionsTextBox.Text = string.Empty;
            CreateTestSession_sessionNoTextBox.Text = "0";
            CreateTestSession_timeLimitTextBox.Text = string.Empty;
            CreateTestSession_expiryDateTextBox.Text = string.Empty;
            CreateTestSession_randomSelectionCheckBox.Checked = false;
            CreateTestSession_cyberProctorateCheckBox.Checked = false;
            CreateTestSession_displayResultsCheckBox.Checked = false;
            ClearLabelMessage();
        }

        /// <summary>
        /// Handler that performs to redirect to schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void CreateTestSession_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Scheduler/ScheduleCandidate.aspx?m=1&s=2&parentpage="
                + Constants.ParentPage.SEARCH_TEST + "&testsessionid="
                + ViewState["TEST_SESSION_ID"].ToString().Trim(), false);
        }

        #endregion Event Handlers                                              

        #region Public Methods                                                 

        /// <summary>
        /// Method that will show pop up extender when download button clicked on user control.
        /// </summary>
        public void ShowModalPopup()
        {
            CreateTestSession_candidateDetailModalPopupExtender.Show();
        }

        #endregion Public Methods                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that will preview the test session details once the user clicked 
        /// on the save button.
        /// </summary>
        /// <returns>Returns the <see cref="TestSessionDetail"/> object</returns>
        private TestSessionDetail PreviewTestSession()
        {
            // Initialize the test session detail object
            TestSessionDetail testSessionDetail = new TestSessionDetail();

            // Get the information from the controls and assign to the object members
            testSessionDetail.TestID = Request.QueryString["testkey"];
            testSessionDetail.TestName = CreateTestSession_testNameValueLabel.Text;
            testSessionDetail.NumberOfCandidateSessions
                = Convert.ToInt32(CreateTestSession_sessionNoTextBox.Text);

            testSessionDetail.TotalCredit = Convert.ToDecimal(CreateTestSession_creditValueLabel.Text);
            testSessionDetail.PositionProfileName = CreateTestSession_positionProfileValueLabel.Text;

            testSessionDetail.TimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(CreateTestSession_timeLimitTextBox.Text);

            testSessionDetail.RecommendedTimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(CreateTestSession_recommendedTimeValueLabel.Text);

            // Set the time to maximum in expiry date.
            testSessionDetail.ExpiryDate = (Convert.ToDateTime(CreateTestSession_expiryDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

            testSessionDetail.IsRandomizeQuestionsOrdering = CreateTestSession_randomSelectionCheckBox.Checked;
            testSessionDetail.IsDisplayResultsToCandidate = CreateTestSession_displayResultsCheckBox.Checked;
            testSessionDetail.IsCyberProctoringEnabled = CreateTestSession_cyberProctorateCheckBox.Checked;
            testSessionDetail.TestSessionDesc = CreateTestSession_sessionDescTextBox.Text;
            testSessionDetail.Instructions = CreateTestSession_instructionsTextBox.Text;

            testSessionDetail.CreatedBy = base.userID;
            testSessionDetail.ModifiedBy = base.userID;

            return testSessionDetail;
        }

        /// <summary>
        /// This method gets the seconds values and updated the time format as hh:mm:yyyy
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string GetTimeFormat(int seconds)
        {
            return Utility.ConvertSecondsToHoursMinutesSeconds(seconds);
        }

        /// <summary>
        /// This method passes the testId and pagennumber to retrieve the candidate session ids.
        /// </summary>
        /// <param name="testID"></param>
        /// <param name="pageNumber"></param>
        private void LoadTestSessions(int pageNumber)
        {
            int totalRecords = 0;
            List<CandidateTestSessionDetail> candidateTestSessions =
                new TestBLManager().GetCandidateTestSessions(CreateTestSession_TestKeyHiddenField.Value,
                pageNumber, base.GridPageSize, ViewState["SORT_EXPRESSION"].ToString(),
                (SortType)ViewState["SORT_DIRECTION_KEY"],base.userID, out totalRecords);

            // Check if the candidateTestSession object is null, 
            // Then hide the gridview. Otherwise, show the loaded gridview 
            if (candidateTestSessions == null)
                CreateTestSession_testSessionGridviewTR.Visible = false;
            else
            {
                CreateTestSession_testSessionGridviewTR.Visible = true;
                CreateTestSession_testSessionGridView.DataSource = candidateTestSessions;
                CreateTestSession_testSessionGridView.DataBind();
            }

            // Update page size and total records
            CreateTestSession_bottomSessionPagingNavigator.PageSize = base.GridPageSize;
            CreateTestSession_bottomSessionPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// On loading the page, below method displays all the informations about the test.
        /// </summary>
        /// <param name="testID">testID is the key passed in query string</param>
        private void LoadTestDetail(string testID)
        {
            // Get test detail information by calling GetTestDetail method
            TestDetail testDetail = new TestBLManager().GetTestDetail(testID);

            if (testDetail == null)
            {
                base.ShowMessage(CreateTestSession_bottomErrorMessageLabel,
                    CreateTestSession_topErrorMessageLabel,
                    Resources.HCMResource.CreateTestSession_InValidTestKey);
                return;
            }
            // Assign test id to the text field
            CreateTestSession_testKeyValueLabel.Text = testID;
            CreateTestSession_testNameValueLabel.Text = testDetail.Name;
            CreateTestSession_creditValueLabel.Text = testDetail.TestCost.ToString();
            CreateTestSession_positionProfileIDHiddenField.Value = testDetail.PositionProfileID.ToString();
            CreateTestSession_positionProfileValueLabel.Text = testDetail.PositionProfileName;

            // Recommended time returns the seconds. 
            // Pass the seconds value to below method to get the time in hh:mm:ss
            TimeSpan timeSpan = TimeSpan.FromSeconds(testDetail.RecommendedCompletionTime);

            // Convert recommendedtime as hours/minutes/seconds format
            string hoursMinutesSeconds = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    timeSpan.Hours,
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);
            CreateTestSession_recommendedTimeValueLabel.Text = hoursMinutesSeconds;

            // Call load test session method by passing test key and page number
            LoadTestSessions(1);
        }

        /// <summary>
        /// This method returns the TestSessionDetails instance with all the values 
        /// given as input by user.
        /// </summary>
        /// <returns></returns>
        private TestSessionDetail ConstructTestSessionDetail()
        {
            // Initialize test session detail object
            TestSessionDetail testSessionDetail = new TestSessionDetail();

            // Check whether a test key is empty or not before assigning to 
            // the testid
            if (Request.QueryString["testkey"] != null)
                testSessionDetail.TestID = Request.QueryString["testkey"];

            // Set test name
            testSessionDetail.TestName = CreateTestSession_testNameValueLabel.Text;

            // Set number of candidate session (session count)
            testSessionDetail.NumberOfCandidateSessions =
                Convert.ToInt32(CreateTestSession_sessionNoTextBox.Text);

            // Set total credits limit
            testSessionDetail.TotalCredit =
                Convert.ToDecimal(CreateTestSession_creditValueLabel.Text);

            // Set position profile ID.
            if (!Utility.IsNullOrEmpty(CreateTestSession_positionProfileIDHiddenField.Value))
                testSessionDetail.PositionProfileID =
                    Convert.ToInt32(CreateTestSession_positionProfileIDHiddenField.Value);
            else
                testSessionDetail.PositionProfileID = 0;

            // Set time limit
            testSessionDetail.TimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(CreateTestSession_timeLimitTextBox.Text);

            // Set recommended time limit
            testSessionDetail.RecommendedTimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(CreateTestSession_recommendedTimeValueLabel.Text);

            // Set expiry date
            testSessionDetail.ExpiryDate = Convert.ToDateTime(CreateTestSession_expiryDateTextBox.Text);

            // Set random question order status
            testSessionDetail.IsRandomizeQuestionsOrdering
                = (CreateTestSession_randomSelectionCheckBox.Checked) ? true : false;

            // Set display result status 
            testSessionDetail.IsDisplayResultsToCandidate
                = (CreateTestSession_displayResultsCheckBox.Checked) ? true : false;

            // Set cyber proctoring status
            testSessionDetail.IsCyberProctoringEnabled
                = (CreateTestSession_cyberProctorateCheckBox.Checked) ? true : false;

            // Set created by
            testSessionDetail.CreatedBy = base.userID;

            // Set modified by
            testSessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            testSessionDetail.Instructions =
                CreateTestSession_instructionsTextBox.Text.ToString().Trim();

            // Set session descriptions
            testSessionDetail.TestSessionDesc =
                CreateTestSession_sessionDescTextBox.Text.ToString().Trim();

            return testSessionDetail;
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            CreateTestSession_topSuccessMessageLabel.Text = string.Empty;
            CreateTestSession_bottomSuccessMessageLabel.Text = string.Empty;
            CreateTestSession_topErrorMessageLabel.Text = string.Empty;
            CreateTestSession_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods                                             

        #region Protected Methods                                              

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;

            // Validate number of session field
            if (Convert.ToInt32(CreateTestSession_sessionNoTextBox.Text) != 0 &&
                (Convert.ToInt32(CreateTestSession_sessionNoTextBox.Text) > 30))
            {
                isValidData = false;
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateTestSession_SessionCountCannotBeZero);
            }

            DateTime timeLimit;
            // Validate time limit field 
            // case i) Time limit text is empty
            // case ii) Time limit has invalid time i.e exceeds 24hrs (25:25:25)
            // case iii) Time limit contains 00:00:00
            if (CreateTestSession_timeLimitTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateTestSession_TimeLimitCannotBeEmpty);
            }
            else if (DateTime.TryParse(CreateTestSession_timeLimitTextBox.Text, out timeLimit) == false)
            {
                isValidData = false;
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateTestSession_EnteredTimeLimitIsNotValid);
            }
            else if (Utility.ConvertHoursMinutesSecondsToSeconds(CreateTestSession_timeLimitTextBox.Text)
                    < Constants.General.DEFAULT_TEST_MINIMUM_TIME)
            {
                isValidData = false;
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateTestSession_TimeLimit_Invalid);
            }

            CreateTestSession_MaskedEditValidator.Validate();

            if (CreateTestSession_expiryDateTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateTestSession_ExpiryDateCannotBeEmpty);
            }
            else
            {
                // This will check if the date is valid date. i.e. 99/99/9999
                if (!CreateTestSession_MaskedEditValidator.IsValid)
                {
                    isValidData = false;
                    base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel,
                    CreateTestSession_MaskedEditValidator.ErrorMessage);
                }
                else
                {
                    // Check if the expiry date is less than current date
                    if (DateTime.Parse(CreateTestSession_expiryDateTextBox.Text) < DateTime.Today)
                    {
                        isValidData = false;
                        base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                        CreateTestSession_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateTestSession_ExpiryDateCannotBeLessThanCurrentDate);
                    }
                }
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Methods                                           

        #region Sort Related Coding                                            

        /// <summary>
        /// This event handler is used to show an image icon on the header column
        /// which is clicked
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> contains the event data.
        /// </param>
        protected void CreateTestSession_testSessionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    // Get last clicked column index
                    int sortColumnIndex = GetSortColumnIndex
                        (CreateTestSession_testSessionGridView,
                        (string)ViewState["SORT_EXPRESSION"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_DIRECTION_KEY"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Here, grid view data will be re-arranged either ascending or descending order
        /// At the same time, pagination and grid will be reloaded
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the send of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void CreateTestSession_testSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                // Assign sort expression column to the viewstate
                if (ViewState["SORT_EXPRESSION"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_DIRECTION_KEY"] =
                        ((SortType)ViewState["SORT_DIRECTION_KEY"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;
                else
                {
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;
                }

                ViewState["SORT_EXPRESSION"] = e.SortExpression;

                // Reload the pagination and gridview controls
                CreateTestSession_bottomSessionPagingNavigator.Reset();
                LoadTestSessions(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateTestSession_topErrorMessageLabel,
                    CreateTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Sort Related Coding                                         
    }
}