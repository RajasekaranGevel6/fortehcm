﻿#region Header                                                                                        

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditManualTestWithoutAdaptive.cs
// File that represents the user Serach the Question by various Key filed. 
// and Modified the Exists test Details and questions if Test included the Some of Session the new test will be created.
// This will helps edit & create a Test and have to implemeted the Certification details..

#endregion

#region Directives                                                                                    
using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
#endregion
namespace Forte.HCM.UI.TestMaker
{
    public partial class EditManualTestWithoutAdaptive : PageBase
    {
        #region Declaration                                                                           
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        string parentPage = "";
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";
        string testKeyQuestyString = "";
        string questionID = "";
        List<QuestionDetail> questionDetailSearchResultList;
      
        #endregion

        #region Event Handlers                                                                        

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                parentPage = Request.QueryString.Get("parentpage");

                if (EditManualTestWithoutAdaptive_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = EditManualTestWithoutAdaptive_topSearchButton.UniqueID;
                else if (EditManualTestWithoutAdaptive_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = EditManualTestWithoutAdaptive_bottomSaveButton.UniqueID;
                
                MessageLabel_Reset();
                Master.SetPageCaption(Resources.HCMResource.EditTest_Title);
                EditManualTestWithoutAdaptive_ConfirmPopupPanel.Style.Add("height", "220px");
                if (!IsPostBack)
                {
                    LoadValues();
                    EditManualTestWithoutAdaptive_manualImageButton.Visible = false;
                    EditManualTestWithoutAdaptive_adaptiveImageButton.Visible = false;
                    EditManualTestWithoutAdaptive_bothImageButton.Visible = false;
                    EditManualTestWithoutAdaptive_simpleSearchDiv.Visible = true;
                    EditManualTestWithoutAdaptive_advanceSearchDiv.Visible = false;
                    EditManualTestWithoutAdaptive_questionDiv.Visible = false;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                        ViewState["SORTDIRECTIONKEY"] = "A";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                        ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                        ViewState["PAGENUMBER"] = "1";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                        ViewState["SEARCHRESULTGRID"] = null;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                        testKeyQuestyString = Request.QueryString["testkey"].ToUpper();

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        EditManualTestWithoutAdaptive_topCreateSessionButton.Visible = false;
                        EditManualTestWithoutAdaptive_bottomCreateSessionButton.Visible = false;
                    }
                    
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["showmessage"]))
                        {
                            if (Request.QueryString["mode"].ToLower() == "new")
                            {
                                base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                                        EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualTest_AddedSuccessfully, testKeyQuestyString));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "edit")
                            {
                                base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                                       EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualTest_UpdatedSuccessfully, testKeyQuestyString));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "copy")
                            {
                                base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                                       EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualTest_CopySuccessfully, testKeyQuestyString));
                                EditManualTestWithoutAdaptive_topCreateSessionButton.Visible = true;
                                EditManualTestWithoutAdaptive_bottomCreateSessionButton.Visible = true;
                            }
                        }
                    }

                    EditManualTestWithoutAdaptive_bottomCreateSessionButton.PostBackUrl = "CreateTestSession.aspx?m=1&s=1&parentpage=" + Constants.ParentPage.EDIT_TEST + "&testkey=" + testKeyQuestyString;
                    EditManualTestWithoutAdaptive_topCreateSessionButton.PostBackUrl = "CreateTestSession.aspx?m=1&s=1&parentpage=" + Constants.ParentPage.EDIT_TEST + "&testkey=" + testKeyQuestyString;

                    EditManualTestWithoutAdaptive_questionKeyHiddenField.Value = testKeyQuestyString;

                    BindTestQuestions(testKeyQuestyString);
                    BindTestDetails(testKeyQuestyString);

                    EditManualTestWithoutAdaptive_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('" 
                        + EditManualTestWithoutAdaptive_dummyAuthorID.ClientID + "','" 
                        + EditManualTestWithoutAdaptive_authorIdHiddenField.ClientID + "','"
                        + EditManualTestWithoutAdaptive_authorTextBox.ClientID + "','QA')");

                    EditManualTestWithoutAdaptive_positionProfileImageButton.Attributes.Add("onclick",
                            "return ShowClientRequestForManual('" + EditManualTestWithoutAdaptive_positionProfileTextBox.ClientID + "','" +
                            EditManualTestWithoutAdaptive_positionProfileKeywordTextBox.ClientID + "','" +
                             EditManualTestWithoutAdaptive_positionProfileHiddenField.ClientID + "')");

                    EditManualTestWithoutAdaptive_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
                    EditManualTestWithoutAdaptive_testDrftGridView.DataBind();
                }
                else
                {
                    //  EditManualTestWithoutAdaptive_questionDiv.Visible = true;
                }
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");
                ExpandRestore();
                EditManualTestWithoutAdaptive_bottomPagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                      (EditManualTestWithoutAdaptive_pagingNavigator_PageNumberClick);

                // Create events for paging control
                EditManualTestWithoutAdaptive_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (EditManualTestWithoutAdaptive_adaptivepagingNavigator_PageNumberClick);

                EditManualTestWithoutAdaptive_categorySubjectControl.ControlMessageThrown += new CategorySubjectControl.ControlMessageThrownDelegate(EditManualTestWithoutAdaptive_categorySubjectControl_ControlMessageThrown);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void EditManualTestWithoutAdaptive_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                EditManualTestWithoutAdaptive_topErrorMessageLabel.Text = c.Message.ToString();
                EditManualTestWithoutAdaptive_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                EditManualTestWithoutAdaptive_topSuccessMessageLabel.Text = c.Message.ToString();
                EditManualTestWithoutAdaptive_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_okClick(object sender, EventArgs e)
        {
            
            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel, 
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
            
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                    EditManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    EditManualTestWithoutAdaptive_questionDetailSummaryControl.ShowAddButton = true;
                    EditManualTestWithoutAdaptive_questionDetailSummaryControl.LoadQuestionDetails
                        (questionCollection[0], int.Parse(questionCollection[1]));
                    EditManualTestWithoutAdaptive_questionDetailSummaryControl.Visible = true;
                    EditManualTestWithoutAdaptive_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField EditManualTestWithoutAdaptive_hidddenValue = (HiddenField)e.Row.FindControl("EditManualTestWithoutAdaptive_hidddenValue");
                    string questionID = EditManualTestWithoutAdaptive_hidddenValue.Value.Split(':')[0].ToString();
                    ImageButton alertImageButton = (ImageButton)e.Row.FindControl("EditManualTestWithoutAdaptive_searchQuestionAlertImageButton");
                    alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");

                    Image img = (Image)e.Row.FindControl("EditManualTestWithoutAdaptive_searchQuestionSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualTestWithoutAdaptive_searchQuestionGridView.ClientID + "');");
                   // img.Attributes.Add("OnClick", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualTestWithoutAdaptive_searchQuestionGridView.ClientID + "');");
                    img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + EditManualTestWithoutAdaptive_hidddenValue.Value + "','" + EditManualTestWithoutAdaptive_selectMultipleImage.ClientID + "');");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                    
                    Label EditManualTestWithoutAdaptive_questionLabel =
                        (Label)e.Row.FindControl("EditManualTestWithoutAdaptive_questionLabel");

                    HoverMenuExtender EditManualTestWithoutAdaptive_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("EditManualTestWithoutAdaptive_hoverMenuExtender");
                    EditManualTestWithoutAdaptive_hoverMenuExtender.TargetControlID = EditManualTestWithoutAdaptive_questionLabel.ID;

                   // Label EditManualTestWithoutAdaptive_questionDetailPreviewControlQuestionLabel =
                     //   (Label)e.Row.FindControl("EditManualTestWithoutAdaptive_questionDetailPreviewControlQuestionLabel");
                    //RadioButtonList EditManualTestWithoutAdaptive_questionDetailPreviewControlAnswerRadioButtonList =
                    //    (RadioButtonList)e.Row.FindControl("EditManualTestWithoutAdaptive_questionDetailPreviewControlAnswerRadioButtonList");
                    Literal EditManualTestWithoutAdaptive_questionAnswer = (Literal)e.Row.FindControl("EditManualTestWithoutAdaptive_questionAnswer");
                    Label EditManualTestWithoutAdaptive_AnswerLabel1 = (Label)e.Row.FindControl("EditManualTestWithoutAdaptive_AnswerLabel1");

                    PlaceHolder EditManualTestWithoutAdaptive_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("EditManualTestWithoutAdaptive_answerChoicesPlaceHolder");
                    EditManualTestWithoutAdaptive_answerChoicesPlaceHolder.Controls.Add
                                        (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID),true));
                    if (EditManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue.ToString() == "1")
                    {
                        EditManualTestWithoutAdaptive_AnswerLabel1.Visible = false;
                        EditManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = true;
                        EditManualTestWithoutAdaptive_questionAnswer.Visible = false;
                        EditManualTestWithoutAdaptive_answerChoicesPlaceHolder.Controls.Add
                                            (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID), true));
                    }
                    else
                    {
                        QuestionDetail questionDetail = new BL.QuestionBLManager().GetQuestion(questionID);
                        EditManualTestWithoutAdaptive_AnswerLabel1.Visible = true;
                        EditManualTestWithoutAdaptive_questionAnswer.Visible = true;
                        EditManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = false;
                        EditManualTestWithoutAdaptive_questionAnswer.Text = questionDetail.AnswerReference;

                    }


                    HiddenField EditManualTestWithoutAdaptive_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                           "EditManualTestWithoutAdaptive_correctAnswerHiddenField");

                    List<AnswerChoice> answerChoice = new List<AnswerChoice>();
                    answerChoice = new BL.QuestionBLManager().GetQuestionOptions(questionID);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }
      
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image img = (Image)e.Row.FindControl("EditManualTestWithoutAdaptive_testDrftRemoveImage");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                    Label EditManualTestWithoutAdaptive_questionLabel =
                         (Label)e.Row.FindControl("EditManualTestWithoutAdaptive_draftQuestionLabel");

                    HoverMenuExtender EditManualTestWithoutAdaptive_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("EditManualTestWithoutAdaptive_draftHoverMenuExtender1");
                    EditManualTestWithoutAdaptive_hoverMenuExtender.TargetControlID = EditManualTestWithoutAdaptive_questionLabel.ID;
                   
                    //RadioButtonList EditManualTestWithoutAdaptive_questionDetailPreviewControlAnswerRadioButtonList =
                   //     (RadioButtonList)e.Row.FindControl("QuestionDetailPreviewControl_draftAnswerRadioButtonList");
                    HiddenField EditManualTestWithoutAdaptive_testDraftQuestionKeyHiddenField = (HiddenField)e.Row.FindControl(
                              "EditManualTestWithoutAdaptive_testDraftQuestionKeyHiddenField");
                    HiddenField EditManualTestWithoutAdaptive_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                             "EditManualTestWithoutAdaptive_correctAnswerHiddenField");
                    string questionID = EditManualTestWithoutAdaptive_testDraftQuestionKeyHiddenField.Value.Split(':')[0].ToString().Trim();
                    PlaceHolder EditManualTestWithoutAdaptive_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("EditManualTestWithoutAdaptive_answerChoicesPlaceHolder");

                    Literal EditManualTestWithoutAdaptive_questionAnswer = (Literal)e.Row.FindControl("EditManualTestWithoutAdaptive_draftQuestionAnswer");
                    Label EditManualTestWithoutAdaptive_AnswerLabel1 = (Label)e.Row.FindControl("EditManualTestWithoutAdaptive_draftAnswerLabel1");
 
                    if (EditManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue.ToString() == "1")
                    {
                        EditManualTestWithoutAdaptive_AnswerLabel1.Visible = false;
                        EditManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = true;
                        EditManualTestWithoutAdaptive_questionAnswer.Visible = false;
                        EditManualTestWithoutAdaptive_answerChoicesPlaceHolder.Controls.Add
                                            (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID), true));
                    }
                    else
                    {
                        QuestionDetail questionDetail = new BL.QuestionBLManager().GetQuestion(questionID);
                        EditManualTestWithoutAdaptive_AnswerLabel1.Visible = true;
                        EditManualTestWithoutAdaptive_questionAnswer.Visible = true;
                        EditManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = false;
                        EditManualTestWithoutAdaptive_questionAnswer.Text = questionDetail.AnswerReference;

                    }
                    

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    EditManualTestWithoutAdaptive_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Message =string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message,e.CommandArgument.ToString().Split(':')[0]);
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditManualTestWithoutAdaptive_ConfirmPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }
      
        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
               
                EditManualTestWithoutAdaptive_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                AddSearchQuestionToTestDraft();
               
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in EditManualTestWithoutAdaptive_testDrftGridView.Rows)
                    {
                        CheckBox EditManualTestWithoutAdaptive_testDrftCheckbox = (CheckBox)row.FindControl("EditManualTestWithoutAdaptive_testDrftCheckbox");
                        HiddenField EditManualTestWithoutAdaptive_testDraftQuestionKeyHiddenField = (HiddenField)row.FindControl("EditManualTestWithoutAdaptive_testDraftQuestionKeyHiddenField");
                        if (EditManualTestWithoutAdaptive_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + EditManualTestWithoutAdaptive_testDraftQuestionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }
                    }
                  //  hiddenValue.Value = questionID.TrimEnd(',');
                   
                }
                if (questionSelected)
                {
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditManualTestWithoutAdaptive_ConfirmPopupExtender.Show();  
                    //MoveToTestDraft("Delete");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                                       EditManualTestWithoutAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_TestDraft_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualTestWithoutAdaptive_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (EditManualTestWithoutAdaptive_simpleLinkButton.Text.ToLower() == "simple")
                {
                    EditManualTestWithoutAdaptive_simpleLinkButton.Text = "Advanced";
                    EditManualTestWithoutAdaptive_simpleSearchDiv.Visible = true;
                    EditManualTestWithoutAdaptive_advanceSearchDiv.Visible = false;
                }
                else
                {
                    EditManualTestWithoutAdaptive_simpleLinkButton.Text = "Simple";
                    EditManualTestWithoutAdaptive_simpleSearchDiv.Visible = false;
                    EditManualTestWithoutAdaptive_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualTestWithoutAdaptive_pagingNavigator_PageNumberClick(object sender, 
            PageNumberEventArgs e)
        {
            MoveToTestDraft("");
            ViewState["PAGENUMBER"] = e.PageNumber;
            LoadQuestion(e.PageNumber, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
        }

        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditManualTestWithoutAdaptive_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    EditTest();
                }
                else
                {
                    MoveToTestDraft("");
                    if( EditManualTestWithoutAdaptive_questionDiv.Visible == true)
                        LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()), ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_resetLinkButton_Click(object sender, EventArgs e)
        {

            Response.Redirect(Request.RawUrl +"&showmessage=n" , false);
        }

        /// <summary>
        /// Handler method that will be called when the With Adaptive button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_adptiveTestLinkButton_Click(object sender, EventArgs e)
        {
            string type = "";
            if (Request.QueryString.Get("type") == "copy")
            {
                type = "&type=copy";
            }
            Response.Redirect(Constants.TestMakerConstants.EDIT_TEST_WA
                + "?m=1&s=1&testkey=" + Request.QueryString["testkey"] + "&parentpage=" + parentPage + type, false);
        }
        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Add  Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = EditManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value;
                EditManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                EditManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion

        #region Private  Methods                                                                      
        
        /// <summary>
        /// modified the Test.
        /// </summary>
        private void EditTest()
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            List<decimal> creditEarned = new List<decimal>();
            List<decimal> timeTaken = new List<decimal>();
            string testKey = "";
            for (int i = 0; i < questionDetailSearchResultList.Count; i++)
            {
                creditEarned.Add(questionDetailSearchResultList[i].CreditsEarned);
                timeTaken.Add(questionDetailSearchResultList[i].AverageTimeTaken);
            }
           string complexity= new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailSearchResultList).AttributeID.ToString();
            int questionCount = Convert.ToInt32(questionDetailSearchResultList.Count.ToString());
            
            TestDetail testDetail = new TestDetail();
            testDetail = EditManualTestWithoutAdaptive_testDetailsUserControl.TestDetailDataSource;
            int systemRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionCount;
           // CertificationDetail certificationDetail = new CertificationDetail();
            testDetail.Questions = questionDetailSearchResultList;
            testDetail.IsActive = true;
          //  testDetail.IsCertification= false;
            testDetail.SystemRecommendedTime = systemRecommendedTime;
            //testDetail.CertificationDetail = certificationDetail;
            testDetail.CertificationId = 0;
            testDetail.CreatedBy = userID;
            testDetail.IsDeleted = false;
            
            testDetail.ModifiedBy = userID;
            
            testDetail.NoOfQuestions = questionCount;
            
            testDetail.SystemRecommendedTime = systemRecommendedTime;
            testDetail.TestAuthorID = userID;
            testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
            testDetail.TestCreationDate = DateTime.Now;
            testDetail.TestStatus = TestStatus.Active;
            testDetail.TestType = TestType.Genenal;
            testDetail.TestMode = "MANUAL";

            if (Request.QueryString.Get("type") == "copy")
            {
                testDetail.TestKey = testKey;

                TestBLManager testBLManager = new TestBLManager();
                testKey = testBLManager.SaveTest(testDetail, userID, complexity);

                // Check if the new saved test is equal to the parent test (based on questions).
                // If yes update the copy and parent test ID status.
                string parentTestKey = Request.QueryString["testkey"];
                testBLManager.UpdateCopyTestStatus(parentTestKey, testKey);

                EditManualTestWithoutAdaptive_sessionInclucedKeyHiddenField.Value = "";
                Response.Redirect("EditManualTestWithoutAdaptive.aspx?m=1&s=2&mode=copy&parentpage=" + parentPage + "&testkey=" + testKey, false);
            }
            else
            {
                if (EditManualTestWithoutAdaptive_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
                {
                    // Check if feature usage limit exceeds for creating test.
                    if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                    {
                        base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                            EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, 
                            "You have reached the maximum limit based your subscription plan. Cannot create more tests");
                        return;
                    }
                    testDetail.TestKey = testKey;
                    testKey = new TestBLManager().SaveTest(testDetail, userID, complexity);
                    EditManualTestWithoutAdaptive_sessionInclucedKeyHiddenField.Value = "";
                    Response.Redirect("EditManualTestWithoutAdaptive.aspx?m=1&s=2&mode=new&parentpage=" + parentPage + "&testkey=" + testKey, false);
                }
                else
                {
                    testDetail.TestKey = EditManualTestWithoutAdaptive_questionKeyHiddenField.Value;
                    // Check if the question is modified by some body.
                    if (new CommonBLManager().IsRecordModified("TEST", testDetail.TestKey,
                        (DateTime)ViewState["MODIFIED_DATE"]))
                    {
                        ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                            EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                                Resources.HCMResource.Edit_Test_Modifed_Another_User);

                        return;
                    }

                    new TestBLManager().UpdateTest(testDetail, userID,
                        complexity, EditManualTestWithoutAdaptive_deletedQuestionHiddenField.Value.TrimEnd(','),
                        EditManualTestWithoutAdaptive_insertedQuestionHiddenField.Value.TrimEnd(','));
                    EditManualTestWithoutAdaptive_sessionInclucedKeyHiddenField.Value = "";
                    Response.Redirect("EditManualTestWithoutAdaptive.aspx?m=1&s=" + Request.QueryString["s"] + "&mode=edit&parentpage=" + parentPage + "&testkey=" + testDetail.TestKey, false);
                }
            }

            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questionDetailSearchResultList;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailSearchResultList);
            EditManualTestWithoutAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;

            base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.CreateManualTest_AddedSuccessfully, testKey));
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestore()
        {
            if (EditManualTestWithoutAdaptive_restoreHiddenField.Value == "Y")
            {
                EditManualTestWithoutAdaptive_searchCriteriasDiv.Style["display"] = "none";
                EditManualTestWithoutAdaptive_searchResultsUpSpan.Style["display"] = "block";
                EditManualTestWithoutAdaptive_searchResultsDownSpan.Style["display"] = "none";
                EditManualTestWithoutAdaptive_questionDiv.Style["height"] =EXPANDED_HEIGHT;
            }
            else
            {
                EditManualTestWithoutAdaptive_searchCriteriasDiv.Style["display"] = "block";
                EditManualTestWithoutAdaptive_searchResultsUpSpan.Style["display"] = "none";
                EditManualTestWithoutAdaptive_searchResultsDownSpan.Style["display"] = "block";
                EditManualTestWithoutAdaptive_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            EditManualTestWithoutAdaptive_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                EditManualTestWithoutAdaptive_questionDiv.ClientID + "','" +
                EditManualTestWithoutAdaptive_searchCriteriasDiv.ClientID + "','" +
                EditManualTestWithoutAdaptive_searchResultsUpSpan.ClientID + "','" +
                EditManualTestWithoutAdaptive_searchResultsDownSpan.ClientID + "','" +
                EditManualTestWithoutAdaptive_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Reset the Messeags.
        /// </summary>
        private void MessageLabel_Reset()
        {
            EditManualTestWithoutAdaptive_topErrorMessageLabel.Text = string.Empty;
            EditManualTestWithoutAdaptive_bottomErrorMessageLabel.Text = string.Empty;
            EditManualTestWithoutAdaptive_topSuccessMessageLabel.Text = string.Empty;
            EditManualTestWithoutAdaptive_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            bool questionSelected = false;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {
                foreach (GridViewRow row in EditManualTestWithoutAdaptive_searchQuestionGridView.Rows)
                {
                    CheckBox EditManualTestWithoutAdaptive_searchQuestionCheckbox = (CheckBox)row.FindControl("EditManualTestWithoutAdaptive_searchQuestionCheckbox");
                    HiddenField EditManualTestWithoutAdaptive_searchQuestionKeyHiddenField = (HiddenField)row.FindControl("EditManualTestWithoutAdaptive_searchQuestionKeyHiddenField");
                    if (EditManualTestWithoutAdaptive_searchQuestionCheckbox.Checked)
                    {
                        questionID = questionID + EditManualTestWithoutAdaptive_searchQuestionKeyHiddenField.Value + ",";
                        questionSelected = true;
                    }
                }
                hiddenValue.Value = questionID.TrimEnd(',');

            }
            if (questionSelected)
            {
                MoveToTestDraft("Add");
            }
            else
            {
                MoveToTestDraft("");
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_Search_Result);
            }
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            QuestionType questionType = (QuestionType)Convert.ToInt32(EditManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue);

            if (EditManualTestWithoutAdaptive_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    EditManualTestWithoutAdaptive_categoryTextBox.Text.Trim() == "" ?
                                    null : EditManualTestWithoutAdaptive_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    EditManualTestWithoutAdaptive_subjectTextBox.Text.Trim() == "" ?
                                    null : EditManualTestWithoutAdaptive_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditManualTestWithoutAdaptive_keywordsTextBox.Text.Trim() == "" ?
                                    null : EditManualTestWithoutAdaptive_keywordsTextBox.Text.Trim();
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualTestWithoutAdaptive_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in EditManualTestWithoutAdaptive_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in EditManualTestWithoutAdaptive_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    EditManualTestWithoutAdaptive_questionIDTextBox.Text.Trim() == "" ?
                                    null : EditManualTestWithoutAdaptive_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditManualTestWithoutAdaptive_keywordTextBox.Text.Trim() == "" ?
                                    null : EditManualTestWithoutAdaptive_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.AuthorName =
                                    EditManualTestWithoutAdaptive_authorTextBox.Text.Trim() == "" ?
                                    null : EditManualTestWithoutAdaptive_authorTextBox.Text.Trim();
                questionDetailSearchCriteria.ActiveFlag = null;
                questionDetailSearchCriteria.CreditsEarned =
                                    EditManualTestWithoutAdaptive_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(EditManualTestWithoutAdaptive_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.ClientRequest = EditManualTestWithoutAdaptive_positionProfileKeywordTextBox.Text.Trim() == "" ?
                  null : EditManualTestWithoutAdaptive_positionProfileKeywordTextBox.Text.Trim();
            }


            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail = new BL.TestBLManager().GetSearchQuestions(questionType, questionDetailSearchCriteria, base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                EditManualTestWithoutAdaptive_searchQuestionGridView.DataSource = null;
                EditManualTestWithoutAdaptive_searchQuestionGridView.DataBind();
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                 EditManualTestWithoutAdaptive_bottomErrorMessageLabel,Resources.HCMResource.Common_Empty_Grid);
                EditManualTestWithoutAdaptive_bottomPagingNavigator.TotalRecords = 0;
                EditManualTestWithoutAdaptive_questionDiv.Visible = false;
            }
            else
            {
                EditManualTestWithoutAdaptive_searchQuestionGridView.DataSource = questionDetail;
                EditManualTestWithoutAdaptive_searchQuestionGridView.DataBind();
                EditManualTestWithoutAdaptive_bottomPagingNavigator.PageSize = base.GridPageSize;
                EditManualTestWithoutAdaptive_bottomPagingNavigator.TotalRecords = totalRecords;
                EditManualTestWithoutAdaptive_questionDiv.Visible = true;
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
            // return questionDetail;
        }
       
        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            EditManualTestWithoutAdaptive_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            EditManualTestWithoutAdaptive_complexityCheckBoxList.DataTextField = "AttributeName";
            EditManualTestWithoutAdaptive_complexityCheckBoxList.DataValueField = "AttributeID";
            EditManualTestWithoutAdaptive_complexityCheckBoxList.DataBind();
        }
      
        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            EditManualTestWithoutAdaptive_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
               //new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            EditManualTestWithoutAdaptive_testAreaCheckBoxList.DataTextField = "AttributeName";
            EditManualTestWithoutAdaptive_testAreaCheckBoxList.DataValueField = "AttributeID";
            EditManualTestWithoutAdaptive_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < EditManualTestWithoutAdaptive_testAreaCheckBoxList.Items.Count; i++)
            {

                if (EditManualTestWithoutAdaptive_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(EditManualTestWithoutAdaptive_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }
     
        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < EditManualTestWithoutAdaptive_complexityCheckBoxList.Items.Count; i++)
            {
                if (EditManualTestWithoutAdaptive_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(EditManualTestWithoutAdaptive_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }
     
        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in EditManualTestWithoutAdaptive_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditManualTestWithoutAdaptive_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditManualTestWithoutAdaptive_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in EditManualTestWithoutAdaptive_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditManualTestWithoutAdaptive_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditManualTestWithoutAdaptive_adaptiveCheckbox")).Checked = false;
                }
            }

        }
     
        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            List<QuestionDetail> questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }
            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');

                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                        string[] idList = ids.Split(':');
                        string id = idList[0];
                        string relationId = idList[1];
                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualTestWithoutAdaptive_insertedQuestionHiddenField.Value))
                                    {
                                        EditManualTestWithoutAdaptive_insertedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    else
                                    {
                                        EditManualTestWithoutAdaptive_insertedQuestionHiddenField.Value = EditManualTestWithoutAdaptive_insertedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                                        EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                                    EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualTestWithoutAdaptive_deletedQuestionHiddenField.Value))
                                {
                                    EditManualTestWithoutAdaptive_deletedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                }
                                else
                                {
                                    EditManualTestWithoutAdaptive_deletedQuestionHiddenField.Value = EditManualTestWithoutAdaptive_deletedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                }
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            base.ShowMessage(EditManualTestWithoutAdaptive_topSuccessMessageLabel,
                                        EditManualTestWithoutAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                        }

                    }
                }
            }
                if (alertFlag)
                {
                    string alertMessage = string.Format(Resources.HCMResource.CreateManualTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                    {
                        alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(','));
                    }
                    EditManualTestWithoutAdaptive_ConfirmPopupPanel.Style.Add("height", "270px");
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Title = "Warning";
                    EditManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Message = alertMessage;
                    EditManualTestWithoutAdaptive_ConfirmPopupExtender.Show();

                }
                hiddenValue.Value = null;

                questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                EditManualTestWithoutAdaptive_testDrftGridView.DataSource = questionDetailTestDraftResultList;
                EditManualTestWithoutAdaptive_testDrftGridView.DataBind();
                if (questionDetailSearchResultList != null)
                {
                    EditManualTestWithoutAdaptive_searchQuestionGridView.DataSource = questionDetailSearchResultList;
                    EditManualTestWithoutAdaptive_questionDiv.Visible = true;
                }
                else
                {
                    EditManualTestWithoutAdaptive_searchQuestionGridView.DataSource = null;
                    EditManualTestWithoutAdaptive_questionDiv.Visible = false;
                }

                EditManualTestWithoutAdaptive_searchQuestionGridView.DataBind();

                if (action != "")
                {
                    questionDetailSearchResultList = new List<QuestionDetail>();
                    questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    List<decimal> timeTaken = new List<decimal>();

                    for (int i = 0; i < questionDetailSearchResultList.Count; i++)
                    {
                        timeTaken.Add(questionDetailSearchResultList[i].AverageTimeTaken);
                    }

                    if (questionDetailTestDraftResultList.Count > 0)
                    {
                        EditManualTestWithoutAdaptive_testDetailsUserControl.SysRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionDetailTestDraftResultList.Count;
                    }
                    else
                    {
                        EditManualTestWithoutAdaptive_testDetailsUserControl.SysRecommendedTime = 0;
                    }

                    TestDraftSummary testDraftSummary = new TestDraftSummary();
                    testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                    testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList);
                    EditManualTestWithoutAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;

                }
            
            UncheckCheckBox();
        }

        /// <summary>
        /// Helps to bind test question details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindTestQuestions(string testKey)
        {
            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail =
                new TestBLManager().GetTestQuestionDetail(testKey, "");
             TestDraftSummary testDraftSummary = new TestDraftSummary();
             string testQuestionType = questionDetail[0].QuestionType == QuestionType.OpenText ? "2" : "1";
             EditManualTestWithoutAdaptive_questionTypeDropDownList.Items.FindByValue(testQuestionType).Selected = true;
             EditManualTestWithoutAdaptive_questionTypeDropDownList.Enabled = false;
                testDraftSummary.QuestionDetail = questionDetail;
                testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetail);
                EditManualTestWithoutAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            //EditManualTestWithoutAdaptive_testSummaryControl.LoadQuestionDetailsSummary(questionDetail, GetComplexity());
            ViewState["TESTDRAFTGRID"] = questionDetail;
        }

        /// <summary>
        /// Helps to bind test details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindTestDetails(string testKey)
        {
            TestDetail testDetail = new TestDetail();
            testDetail =
                new TestBLManager().GetTestAndCertificateDetail(testKey);
            EditManualTestWithoutAdaptive_testDetailsUserControl.TestDetailDataSource = testDetail;
            EditManualTestWithoutAdaptive_sessionInclucedKeyHiddenField.Value = testDetail.SessionIncluded.ToString();
            ViewState["MODIFIED_DATE"] = testDetail.ModifiedDate;

            if (Request.QueryString.Get("type") == "copy")
            {
                EditManualTestWithoutAdaptive_topSaveButton.Text = "Copy Test";
                EditManualTestWithoutAdaptive_bottomSaveButton.Text = "Copy Test";

                // Clear position profile details.
                EditManualTestWithoutAdaptive_testDetailsUserControl.PositionProfileID = 0;
                EditManualTestWithoutAdaptive_testDetailsUserControl.PositionProfileName = string.Empty;

                return;
            }
            if (EditManualTestWithoutAdaptive_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                  EditManualTestWithoutAdaptive_bottomErrorMessageLabel, string.Format(Resources.HCMResource.EditManualTest_TestAlreadyExists, testKey));
            }
            
        }

        #endregion

        #region Protected Overridden Methods                                                          

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;
            EditManualTestWithoutAdaptive_topErrorMessageLabel.Text = string.Empty;
            EditManualTestWithoutAdaptive_bottomErrorMessageLabel.Text = string.Empty;
            TestDetail testDetail = new TestDetail();
            testDetail = EditManualTestWithoutAdaptive_testDetailsUserControl.TestDetailDataSource;
            int recommendedTime = testDetail.RecommendedCompletionTime;

            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailSearchResultList))
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }
            else if (questionDetailSearchResultList.Count == 0)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }

            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Name_Empty);
                isValidData = false;
            }
            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Description_Empty);

                isValidData = false;
            }
           
            if (recommendedTime==-1)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
               EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
               Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_Empty);
                isValidData = false;
            }
            else if (recommendedTime == -2)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
               EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
               string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended"));
                isValidData = false;
            }
           else if (recommendedTime == 0)
           {   
                    base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                    isValidData = false;
           }

            if (isQuestionTabActive)
                EditManualTestWithoutAdaptive_mainTabContainer.ActiveTab = EditManualTestWithoutAdaptive_questionsTabPanel;
            else
                EditManualTestWithoutAdaptive_mainTabContainer.ActiveTab = EditManualTestWithoutAdaptive_testdetailsTabPanel;

            if (!(bool)testDetail.IsCertification)
                return isValidData;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateDetailsEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateID <= 0)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateFormatEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.DaysElapseBetweenRetakes < 0)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyDaysRetakeTest);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.PermissibleRetakes <= 0)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyRetakes);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MinimumTotalScoreRequired <= 0)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_MinimumQualifacationEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateValidity == "0")
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                     Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateValidity);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -2)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                        EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                        string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended certification completion"));
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -1)
            {
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                    EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateAutomaticTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                isValidData = false;
            }
            if ((testDetail.RecommendedCompletionTime > 0) && (testDetail.CertificationDetail.MaximumTimePermissible > 0))
            {
                if (testDetail.RecommendedCompletionTime < testDetail.CertificationDetail.MaximumTimePermissible)
                {
                    base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                        EditManualTestWithoutAdaptive_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificationTest_CertificationTimeExceeds);
                    isValidData = false;
                }
            }
            EditManualTestWithoutAdaptive_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;


        }
        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
        }

        #endregion Protected Overridden Methods

        #region Not Used Now (Adaptive Question & Sorting)                                            

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_searchQuestionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            //may be used in future.
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void EditManualTestWithoutAdaptive_testDrftGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            //may be used in future    
        }

        protected void EditManualTestWithoutAdaptive_adaptiveGridView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void EditManualTestWithoutAdaptive_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view")
            {
                EditManualTestWithoutAdaptive_questionModalPopupExtender.Show();
            }
            if (e.CommandName == "AdaptiveQuestion")
            {
                EditManualTestWithoutAdaptive_adaptiveQuestionPreviewModalPopupExtender.Show();

                ViewState["RowIndex"] = (e.CommandArgument.ToString());
            }
        }
        protected void EditManualTestWithoutAdaptive_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image alertImage = (Image)e.Row.FindControl("EditManualTestWithoutAdaptive_adaptiveAlertImageButton");
                    alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion();");

                    Image img = (Image)e.Row.FindControl("EditManualTestWithoutAdaptive_adaptiveSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualTestWithoutAdaptive_adaptiveGridView.ClientID + "');");
                    //(Image)e.Row.FindControl("EditManualTestWithoutAdaptive_adaptiveSelectImage");

                    //img.Attributes.Add("onClick", "javascript:return AddRow(" + e.Row.RowIndex.ToString() + "," + 1 + ");");
                    // if (dtSearchQuestions.Rows[e.Row.RowIndex][0].ToString().Equals(""))
                    {
                        img.Visible = false;
                        ImageButton img1 = (ImageButton)e.Row.FindControl("EditManualTestWithoutAdaptive_adaptiveAlertImageButton");
                        img1.Visible = false;
                        //Image img2 = (Image)e.Row.FindControl("EditManualTestWithoutAdaptive_adaptiveViewImage");
                        //img2.Visible = false;
                        CheckBox chk = (CheckBox)e.Row.FindControl("EditManualTestWithoutAdaptive_adaptiveCheckbox");
                        chk.Visible = false;
                    }

                    ImageButton imageButton = (ImageButton)e.Row.FindControl("EditManualTestWithoutAdaptive_adaptiveQuestionDetailsImage");

                    imageButton.CommandArgument = e.Row.RowIndex.ToString();
                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                    Label EditManualTestWithoutAdaptive_questionLabel =
                         (Label)e.Row.FindControl("EditManualTestWithoutAdaptive_questionLabel");

                    HoverMenuExtender EditManualTestWithoutAdaptive_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("EditManualTestWithoutAdaptive_hoverMenuExtender1");
                    EditManualTestWithoutAdaptive_hoverMenuExtender.TargetControlID = EditManualTestWithoutAdaptive_questionLabel.ID;

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualTestWithoutAdaptive_topErrorMessageLabel,
                   EditManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }
        protected void CreateManulTest_CloseButton_Click(object sender, EventArgs e)
        {
            MoveToTestDraft("");
            EditManualTestWithoutAdaptive_questionModalPopupExtender.Show();
        }

        protected void EditManualTestWithoutAdaptive_questionDetail_TopAddButton_Click(object sender, EventArgs e)
        {
            //EditManualTestWithoutAdaptive_adaptiveQuestionPreviewModalPopupExtender.Hide();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);

            //ViewState["RowIndex"] = 0;

        }

        protected void QuestionDetail_topCancelButton_Click(object sender, EventArgs e)
        {
            EditManualTestWithoutAdaptive_adaptiveQuestionPreviewModalPopupExtender.Hide();
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualTestWithoutAdaptive_adaptivepagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {

        }

        #endregion
    }
}
