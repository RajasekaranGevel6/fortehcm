﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="SearchTest.aspx.cs" Inherits="Forte.HCM.UI.TestMaker.SearchTest" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="SearchTest_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="SearchTest_headerLiteral" runat="server" Text="Search Test"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 42%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 20%">
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchTest_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                            OnClick="SearchTest_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchTest_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchTest_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="SearchTest_searchByTestDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:UpdatePanel runat="server" ID="SearchTest_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="SearchTest_simpleLinkButton" runat="server" Text="Advanced" SkinID="sknActionLinkButton"
                                                        OnClick="SearchTest_simpleLinkButton_Click" />
                                                    <asp:HiddenField ID="SearchTest_stateHiddenField" runat="server" Value="0" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="SearchTest_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="SearchTest_simpleSearchDiv" runat="server" style="display: block;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="SearchTest_categoryHeadLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchTest_categoryTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchTest_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchTest_subjectTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchTest_keywordsHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:TextBox ID="SearchTest_keywordSimpleTextBox" runat="server"></asp:TextBox>
                                                                                </div>
                                                                                <div style="float: left;">
                                                                                    <asp:ImageButton ID="SearchTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Test_KeywordHelp %>" />
                                                                                </div>
                                                                            </td>
                                                                             <td>
                                                                                <asp:Label ID="SearchTest_testTypeHeadLabel" runat="server" Text="Test Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                             <asp:DropDownList ID="SearchTest_testTypeDropDownList" SkinID="sknQuestionTypeDropDown"  runat="server" AutoPostBack="false" EnableViewState="true">
                                                                                <asp:ListItem Text="Multiple Choice" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Open Text" Value="2"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="SearchTest_advanceSearchDiv" runat="server" style="width: 100%" visible="false">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <uc3:CategorySubjectControl ID="SearchTest_categorySubjectControl" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="panel_inner_body_bg">
                                                                                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchTest_testAreaHeadLabel" runat="server" Text="Test Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td class="checkbox_list_bg" style="width: 100%;" colspan="5">
                                                                                            <asp:CheckBoxList ID="SearchTest_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%;">
                                                                                            <asp:Label ID="SearchTest_testIdLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%;">
                                                                                            <asp:TextBox ID="SearchTest_testIdTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 16%">
                                                                                            <asp:Label ID="SearchTest_testNameLabel" runat="server" Text="Test Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:TextBox ID="SearchTest_testNameTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 10%;">
                                                                                            <asp:Label ID="SearchTest_authorHeadLabel" runat="server" Text="Test Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 19%;">
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchTest_authorTextBox" runat="server"></asp:TextBox>
                                                                                                <asp:HiddenField ID="SearchTest_dummyAuthorID" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchTest_authorImageButton" SkinID="sknbtnSearchicon" runat="server"
                                                                                                    ImageAlign="Middle" ToolTip="Click here to select the test author" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                       <td>
                                                                                       <asp:Label ID="SearchTest_advTestTypeLabel" runat="server" Text="Test Type"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                       </td>
                                                                                       <td>
                                                                                       <asp:DropDownList ID="SearchTest_advTestTypeDropDownList" SkinID="sknQuestionTypeDropDown"  runat="server" AutoPostBack="false" EnableViewState="true">
                                                                                            <asp:ListItem Text="Multiple Choice" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Open Text" Value="2"></asp:ListItem>
                                                                                          </asp:DropDownList>

                                                                                       </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchTest_positionProfileLabel" runat="server" Text="Position Profile"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchTest_positionProfileTextBox" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the position profile" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchTest_keywordHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchTest_keywordAdvanceTextBox" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="SearchTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Test_KeywordHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                     <td style="display:none">
                                                                                            <asp:Label ID="SearchTest_certificateTestHeadLabel" runat="server" Text="Certificate Test"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="display:none">
                                                                                            <asp:DropDownList ID="SearchTest_certificateTestDropDownList" runat="server" Width="133px">
                                                                                                <%-- <asp:ListItem Text="--Select--" Value="S"></asp:ListItem>--%>
                                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                       <%-- <td>
                                                                                            <asp:Label ID="SearchTest_testCostHeadLabel" runat="server" Text="Test Cost" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table width="50%" border="0" cellpadding="1" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="SearchTest_testCostMinValueTextBox" runat="server" MaxLength="5"
                                                                                                            Width="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td style="width: 60%">
                                                                                                        <asp:TextBox ID="SearchTest_testCostTextBox" runat="server" MaxLength="250"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="SearchTest_testCostMaxValueTextBox" runat="server" MaxLength="5"
                                                                                                            Width="25"></asp:TextBox>
                                                                                                        <ajaxToolKit:MultiHandleSliderExtender ID="SearchTest_testCostMultiHandleSliderExtender"
                                                                                                            runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                                                                            RailCssClass="slider_rail" HandleCssClass="slider_handler" TargetControlID="SearchTest_testCostTextBox"
                                                                                                            ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                                                                            <MultiHandleSliderTargets>
                                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchTest_testCostMinValueTextBox"
                                                                                                                    HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchTest_testCostMaxValueTextBox"
                                                                                                                    HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                                            </MultiHandleSliderTargets>
                                                                                                        </ajaxToolKit:MultiHandleSliderExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>--%>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchTest_noOfQuestionsLabel" runat="server" Text="Number of Questions"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table width="40%" border="0" cellpadding="1" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="SearchTest_noOfQuestionsMinValueTextBox" runat="server" MaxLength="5"
                                                                                                            Width="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td style="width: 60%">
                                                                                                        <asp:TextBox ID="SearchTest_noOfQuestionsTextBox" runat="server" MaxLength="225"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="SearchTest_noOfQuestionsMaxValueTextBox" runat="server" MaxLength="5"
                                                                                                            Width="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <ajaxToolKit:MultiHandleSliderExtender ID="SearchTest_noOfQuestionsMultiHandleSliderExtender"
                                                                                                runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                                                                TargetControlID="SearchTest_noOfQuestionsTextBox" RailCssClass="slider_rail"
                                                                                                HandleCssClass="slider_handler" ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                                                                <MultiHandleSliderTargets>
                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchTest_noOfQuestionsMinValueTextBox"
                                                                                                        HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchTest_noOfQuestionsMaxValueTextBox"
                                                                                                        HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                                </MultiHandleSliderTargets>
                                                                                            </ajaxToolKit:MultiHandleSliderExtender>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchTest_showCopiedTestsLabel" runat="server" Text="Include Copied Tests" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:CheckBox ID="SearchTest_showCopiedTestsCheckBox" runat="server"></asp:CheckBox>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="SearchTest_showCopiedTestsImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Check this to search for copied tests too" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="SearchTest_topSearchButton" runat="server" Text="Search" OnClick="SearchTest_topSearchButton_Click"
                                                            SkinID="sknButtonId" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="SearchTest_searchTestResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="SearchTest_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="SearchTest_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="SearchTest_searchResultsUpSpan" style="display: none;" runat="server">
                                            <asp:Image ID="SearchTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="SearchTest_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="SearchTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="SearchTest_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel runat="server" ID="SearchTest_testGridViewUpdatePanel">
                                            <ContentTemplate>
                                                <div style="height: 200px; overflow: auto;" runat="server" id="SearchTest_testDiv">
                                                    <asp:GridView ID="SearchTest_testGridView" runat="server" OnSorting="SearchTest_testGridView_Sorting" 
                                                        OnRowDataBound="SearchTest_testGridView_RowDataBound" OnRowCommand="SearchTest_testGridView_RowCommand"
                                                        OnRowCreated="SearchTest_testGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="140px">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="SearchTest_additionalDetailsImageButton" runat="server" SkinID="sknAdditionalDetailImageButton"
                                                                        ToolTip="Test Statistics" />
                                                                    <asp:HiddenField ID="SearchTest_testKeyHiddenField" runat="server" Value='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchTest_viewImageButton" runat="server" SkinID="sknViewImageButton"
                                                                        ToolTip="View Test" CommandName="view" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchTest_editImageButton" runat="server" SkinID="sknEditImageButton"
                                                                        ToolTip="Edit Test" CommandName="edittest" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchTest_createNewImageButton" runat="server" SkinID="sknCreateNewSessionImageButton"
                                                                        ToolTip="Create/View Test Sessions" CommandName="create" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchTest_activeImageButton" runat="server" SkinID="sknInactiveImageButton"
                                                                        Visible="false" ToolTip="Deactivate Test" CommandName="inactive" CommandArgument='<%# Eval("TestKey") %>' /><asp:ImageButton
                                                                            ID="SearchTest_inactiveImageButton" runat="server" SkinID="sknActiveImageButton"
                                                                            ToolTip="Activate Test" Visible="false" CommandName="active" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchTest_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                        ToolTip="Delete Test" CommandName="deletes" Visible='<%# Eval("SessionIncluded") %>'
                                                                        CommandArgument='<%# Eval("TestKey") %>' />
                                                                        <asp:ImageButton ID="SearchTest_copyImageButton" runat="server" SkinID="sknCopyImageButton"
                                                                        ToolTip="Copy Test" CommandName="copytest" CommandArgument='<%# Eval("TestKey") %>' Visible="false" />
                                                                          <asp:ImageButton ID="SearchTest_associateToPPImageButton" runat="server" SkinID="sknPPassociateCandidateImageButton"
                                                                        ToolTip="Associate to position profile" CommandName="associatePP" CommandArgument='<%# Eval("TestKey") %>' Visible="false" />
                                                                    <asp:HiddenField ID="SearchTest_statusHiddenField" runat="server" Value='<%# Eval("IsActive") %>' />
                                                                    <asp:HiddenField ID="SearchTest_activeInactivestatusHiddenField" runat="server" Value='<%# Eval("SessionIncludedActive") %>' />
                                                                    <asp:HiddenField ID="SearchTest_testAutherIdHiddenField" runat="server" Value='<%# Eval("TestAuthorID") %>' />
                                                                    <asp:HiddenField ID="SearchTest_dataAccessRightsHiddenField" runat="server" Value='<%# Eval("DataAccessRights") %>' />
                                                                    <asp:HiddenField ID="SearchTest_positionProfileIDHiddenField" runat="server" Value='<%# Eval("PositionProfileID") %>' />
                                                                    <asp:HiddenField ID="SearchTest_tenantIDHiddenField" runat="server" Value='<%# Eval("TenantID") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle Wrap="false" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Test ID" DataField="TestKey" SortExpression="TESTKEY">
                                                                <ItemStyle Width="95px" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Test Name" SortExpression="TESTNAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),20) %>'
                                                                        ToolTip='<%# Eval("Name") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="150px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Author" SortExpression="USRNAME" ItemStyle-Width="135px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testAuthorNameLabel" runat="server" Text='<%# Eval("TestAuthorName") %>'
                                                                        ToolTip='<%# Eval("TestAuthorFullName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="No of Questions" DataField="NoOfQuestions" SortExpression="TOTALQUESTION"
                                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="110px" ItemStyle-HorizontalAlign="right"
                                                                HeaderStyle-CssClass="td_padding_right_20"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Created Date" SortExpression="CREATEDDATE DESC" ItemStyle-Width="120px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          <%--  <asp:BoundField HeaderText="Cost (in&nbsp;$)" DataField="TestCost" SortExpression="TESTCOST"
                                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="75px" ItemStyle-HorizontalAlign="right"
                                                                HeaderStyle-CssClass="td_padding_right_20"></asp:BoundField>--%>
                                                            <asp:TemplateField HeaderText="Certification Test" SortExpression="CERTIFICATION DESC" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testCertificationLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsCertification")))%>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="110px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchTest_topSearchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="SearchTest_bottomPagingNavigatorUpdatePanel">
                                <ContentTemplate>
                                    <uc1:PageNavigator ID="SearchTest_bottomPagingNavigator" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchTest_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 32%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 30%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchTest_bottomReset" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                            OnClick="SearchTest_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchTest_bottomCancel" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                            OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="SearchTest_modelPopupUpdatePanel">
                    <ContentTemplate>
                        <div id="SearchTest_hiddenDIV" style="display: none">
                            <asp:Button ID="SearchTest_hiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="SearchTest_inactivepopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="SearchTest_inactivePopupExtenderControl" runat="server"
                                OnOkClick="SearchTest_OkClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchTest_inactivepopupExtender" runat="server"
                            PopupControlID="SearchTest_inactivepopupPanel" TargetControlID="SearchTest_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:HiddenField ID="SearchTest_authorIdHiddenField" runat="server" />
                        <asp:HiddenField ID="SearchTest_positionProfileHiddenField" runat="server" />
                        <asp:HiddenField ID="SearchTest_Action" runat="server" />
                        <asp:HiddenField ID="SearchTest_TestKey" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
