﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestResult.cs
// File that represents the user interface for test result page.
// This will helps to view the test results.

#endregion Header

#region Directives

using System;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Web.UI.WebControls;

#endregion Directives

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Represents the class that holds the User Interface and 
    /// fucntionalities of the TestResult page.
    /// This page helps to view the  various statistics and count of the 
    /// questions in the test
    /// </summary>
    public partial class TestResult : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Test Result");
                //Method to load the values in test statistics tab
                if (IsPostBack)
                    return;
                LoadValues();
                //Method to load the values in candidate statistics tab
                LoadCandidateStatistics();
                if ((Utility.IsNullOrEmpty(Request.QueryString["candidatesession"])) ||
                        (Utility.IsNullOrEmpty(Request.QueryString["attemptid"])) ||
                        (Utility.IsNullOrEmpty(Request.QueryString["testkey"])) ||
                        (Utility.IsNullOrEmpty(Request.QueryString["parentpage"])))
                    return;
                ((Button)TestResult_emailConfirmation_ConfirmMsgControl.
                   FindControl("ConfirmMsgControl_emailAttachmentButton")).
                   Attributes.Add("onclick", "javascript:return OpenEmailAttachmentAndCloseConfirmPopUp('"
                    + Request.QueryString["candidatesession"].ToString().Trim() + "','"
                    + Request.QueryString["attemptid"].ToString().Trim() + "','"
                    + Request.QueryString["testkey"].ToString().Trim() + "','"
                    + Request.QueryString["parentpage"].ToString().Trim() + "','Y','" +
                    TestResult_emailConfirmation_ModalPopupExtender.ID + "');");

                ((Button)TestResult_emailConfirmation_ConfirmMsgControl.
                    FindControl("ConfirmMsgControl_emailContentButton")).
                      Attributes.Add("onclick", "javascript:return OpenEmailAttachmentAndCloseConfirmPopUp('"
                    + Request.QueryString["candidatesession"].ToString().Trim() + "','"
                    + Request.QueryString["attemptid"].ToString().Trim() + "','"
                    + Request.QueryString["testkey"].ToString().Trim() + "','"
                    + Request.QueryString["parentpage"].ToString().Trim() + "','N','" +
                    TestResult_emailConfirmation_ModalPopupExtender.ID + "');");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestResult_bottomErrorMessageLabel,
                TestResult_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the download button is clicked.
        /// In that time, it will construct the PDF of the results and
        /// download it.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestResult_downloadButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("../PrintPages/TestResultPrint.aspx?candidatesession="
                + Request.QueryString["candidatesession"]
                + "&attemptid=" + Request.QueryString["attemptid"]
                + "&testkey=" + Request.QueryString["testkey"]
                + "&downloadoremail=D", false);
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        private void LoadCandidateStatistics()
        {
            string sessionKey = string.Empty;
            string testKey = string.Empty;

            int attemptID = 0;
            if ((Request.QueryString["candidatesession"] != null) &&
                (Request.QueryString["candidatesession"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesession"].ToString();

            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());

            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            //Get the candidate statistics details for the given session key,
            //test key, attempt id 
            TestResult_testResultsControl.CandidateStatisticsDataSource =
                new ReportBLManager().GetCandidateTestResultStatisticsDetails
                (sessionKey, testKey, attemptID);

            if (TestResult_testResultsControl.CandidateStatisticsDataSource != null)
            {
                // Keep the candidate details
                Session["CANDIDATE_STATISTICS"] =
                    TestResult_testResultsControl.CandidateStatisticsDataSource;

                //Load the candidate details with the datasource
                TestResult_testResultsControl.LoadCandidateStatisticsDetails();
            }

            //Get the categories details for the test 
            MultipleSeriesChartData categoryChartDataSource = new ReportBLManager().
                GetCandidateStatisticsCategoriesChartDetails(sessionKey, testKey, attemptID);

            if (categoryChartDataSource.MultipleSeriesChartDataSource.Count == 0)
            {
                TestResult_testResultsControl.
                    DisplayErrorMessage = "No chart statistics";
                return;
            }
            //Assign the chart type
            categoryChartDataSource.ChartType = SeriesChartType.Column;

            //Assign the chart length
            categoryChartDataSource.ChartLength = 170;

            //Assign the chart width 
            categoryChartDataSource.ChartWidth = 450;

            //Assign property whether to display axis title 
            categoryChartDataSource.IsDisplayAxisTitle = true;

            //Assign property whether to display chart title 
            categoryChartDataSource.IsDisplayChartTitle = false;

            //Assign the x axis title for the chart
            categoryChartDataSource.XAxisTitle = "Categories";

            //Assign the y axis title for the chart
            categoryChartDataSource.YAxisTitle = "Test Scores";

            categoryChartDataSource.ChartImageName = Constants.ChartConstants.CHART_CATEGORY +
                "-" + testKey + "-" + sessionKey + "-" + attemptID;

            //Assign the chart title
            categoryChartDataSource.ChartTitle =
                Resources.HCMResource.TestResult_CategoryChartTitle;

            categoryChartDataSource.IsShowLabel = true;
            //Load the chart with the assigned properties
            //TestResult_testResultsControl.LoadCategoryChartControl(categoryChartDataSource);

            //Load the subject chart
            MultipleSeriesChartData subjectChartDataSource = new ReportBLManager().
                GetCandidateStatisticsSubjectsChartDetails(sessionKey, testKey, attemptID);

            if (subjectChartDataSource != null)
            {
                //Assign the chart type for the chart
                subjectChartDataSource.ChartType = SeriesChartType.Column;

                //Assign the chart length 
                subjectChartDataSource.ChartLength = 170;

                //Assign the chart width 
                subjectChartDataSource.ChartWidth = 450;

                //Assign property whether to display axis title
                subjectChartDataSource.IsDisplayAxisTitle = true;

                //Assign property whether to display chart title
                subjectChartDataSource.IsDisplayChartTitle = false;

                //Assign the x axis title 
                subjectChartDataSource.XAxisTitle = "Subjects";

                //Assign the y axis title
                subjectChartDataSource.YAxisTitle = "Test Scores";

                //Assign  the chart title
                subjectChartDataSource.ChartTitle =
                    Resources.HCMResource.TestResult_SubjectChartTitle;
                subjectChartDataSource.IsShowLabel = true;
                subjectChartDataSource.ChartImageName = Constants.ChartConstants.CHART_SUBJECT + "-"
                    + testKey + "-" + sessionKey + "-" + attemptID;

                //Load the chart with assigned values
                //  TestResult_testResultsControl.LoadSubjectChartControl(subjectChartDataSource);
            }

            //get the test area details for the test 
            MultipleSeriesChartData testAreaChartDataSource = new ReportBLManager().
               GetCandidateStatisticsTestAreaChartDetails(sessionKey, testKey, attemptID);

            if (testAreaChartDataSource != null)
            {
                //Assign the chart type for the chart
                testAreaChartDataSource.ChartType = SeriesChartType.Column;

                //Assign the chart length 
                testAreaChartDataSource.ChartLength = 170;

                //Assign the chart width 
                testAreaChartDataSource.ChartWidth = 450;

                //Assign property whether to display axis title
                testAreaChartDataSource.IsDisplayAxisTitle = true;

                //Assign property whether to display chart title
                testAreaChartDataSource.IsDisplayChartTitle = false;

                //Assign the x axis title
                testAreaChartDataSource.XAxisTitle = "Test Areas";

                //Assign the y axis title 
                testAreaChartDataSource.YAxisTitle = "Test Scores";

                //Assign the chart title
                testAreaChartDataSource.ChartTitle = Resources.HCMResource.TestResult_TestAreaChartTitle;

                testAreaChartDataSource.ChartImageName = Constants.ChartConstants.CHART_TESTAREA + "-"
                    + testKey + "-" + sessionKey + "-" + attemptID;

                testAreaChartDataSource.IsShowLabel = true;
                //Load the chart control with the assigned values
                // TestResult_testResultsControl.LoadTestAreaChartControl(testAreaChartDataSource);

                TestResult_testResultsControl.LoadChartData(categoryChartDataSource,
                    subjectChartDataSource, testAreaChartDataSource);

                //Call the method to load the histogram chart .
                //My score value should be passed since it need to shown in graph
                LoadHistogramChart(TestResult_testResultsControl.CandidateStatisticsDataSource.MyScore);
            }
        }

        /// <summary>
        /// Represents the method to load the histogram chart details
        /// </summary>
        private void LoadHistogramChart(decimal myScore)
        {
            string sessionKey = string.Empty;
            int attemptID = 0;
            if ((Request.QueryString["candidatesession"] != null) &&
                (Request.QueryString["candidatesession"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesession"].ToString();

            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
            {
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
            }

            //Initialise the histogram data class
            ReportHistogramData histogramData = new ReportHistogramData();

            //Assign the chart length 
            histogramData.ChartLength = 170;

            //Assign the chart width 
            histogramData.ChartWidth = 450;

            //Assign the property whether to display legend
            histogramData.IsDisplayLegend = false;

            //Assign the property whether to show label
            histogramData.IsShowLabel = true;

            //Assign the segment interval of the chart.
            //It can be assigned to 10 or 20
            histogramData.SegmentInterval = 10;

            //Assign the property whether to display axis title
            histogramData.IsDisplayAxisTitle = true;

            //Assign the chart title for the chart
            histogramData.ChartTitle = Resources.HCMResource.TestResult_HistogramChartTitle;

            //Assign the x axis title
            histogramData.XAxisTitle = "Scores";

            //Assign the y axis title
            histogramData.YAxisTitle = "No Of Candidates";

            //Assign the property whether to display candidate score
            histogramData.IsDisplayCandidateScore = false;

            //Assign the candidate score
            histogramData.CandidateScore = Convert.ToDouble(myScore.ToString());

            string testKey = string.Empty;

            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            histogramData.ChartData = new ReportBLManager().GetTestCandidateScores(testKey);

            if (histogramData.ChartData != null)
            {
                histogramData.ChartImageName = Constants.ChartConstants.CHART_HISTOGRAM
                        + "-" + testKey + "-" + sessionKey + "-" + attemptID;
            }
            TestResult_testResultsControl.LoadHistogramChartControl(histogramData);
        }

        #endregion Private Methods

        #region Protected Override Methods

        /// <summary>
        /// Method to check the data is valid
        /// </summary>
        /// <returns>
        /// A<see cref="bool"/>that return whether valid or not
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method used to load the values
        /// </summary>
        protected override void LoadValues()
        {
            //Get the test id
            string testID = Request.QueryString["testkey"];

            //Get the test name   
            string testName;
            testName = new ReportBLManager().
                     GetTestName(Request.QueryString["testkey"]);

            string candidateSessionId = null;

            string candidateName = null;

            if (Request.QueryString["candidatesession"] != null &&
                Request.QueryString["candidatesession"].ToString().Length != 0)
            {
                candidateSessionId = Request.QueryString["candidatesession"].ToString().Trim();

                //Get the CandidateName
                candidateName = new CandidateBLManager().GetCandidateName(candidateSessionId);

                //This method will load the test id , test name,
                //candidate name and credits
                TestResult_testResultsControl.LoadCandidateTestDetails
                    (testID, testName, candidateName, "0.00");
            }

            //Get the test details from database.Assign the values to the 
            //TestStatisticsDataSource. 
            //This datasource is for the first tab of the page
            TestResult_testResultsControl.TestStatisticsDataSource =
                new ReportBLManager().GetTestSummaryDetails(testID);

            //Load the test summary details .After getting values
            //assign the values to the control
            TestResult_testResultsControl.LoadTestSummaryDetails();
        }

        #endregion Protected Override Methods
    }
}
