<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="CreateManualTest.aspx.cs" Inherits="Forte.HCM.UI.TestMaker.CreateManualTest" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/QuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>
<%@ Register Src="../CommonControls/TestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ManualTestSummaryControl.ascx" TagName="ManualTestSummaryControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc6" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ContentPlaceHolderID="OTMMaster_body" ID="CreateManualTest_content"
    runat="server">
    <script type="text/javascript" language="javascript">

        var control;
        function setFocus(sender, e)
        {
            try
            {
                var activeTab = $find('<%=CreateManualTest_mainTabContainer.ClientID%>').get_activeTabIndex();
                if (activeTab == "0")
                {
                    try
                    {
                        control = $get('<%=CreateManualTest_categoryTextBox.ClientID%>');
                        control.focus();
                    }
                    catch (er1)
                    {
                        try
                        {
                            control = $get('<%=CreateManualTest_keywordTextBox.ClientID%>');
                            control.focus();
                        }
                        catch (er2)
                        {
                        }
                    }
                }
                else
                {
                    control = $get('ctl00_OTMMaster_body_CreateManualTest_mainTabContainer_CreateManualTest_testdetailsTabPanel_CreateManualTest_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                    control.focus();
                }
            }
            catch (er)
            {
            }
        }

    </script>
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        // Display  Or Hide the adaptive TestDraft Or Manual test Draft Questions.
        function ShowOrHideDiv(caseValue)
        {
            switch (caseValue.toString())
            {
                case '0':
                    {
                        document.getElementById("<%= CreateManualTest_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("CreateManualTest_manualDiv").style.display = "block";
                        document.getElementById("CreateManualTest_adaptivelDiv").style.display = "none";
                        document.getElementById("CreateManualTest_manualDiv").style.width = "100%";
                        document.getElementById("CreateManualTest_adaptiveTD").style.display = "none";
                        document.getElementById("<%= CreateManualTest_questionDiv.ClientID %>").style.width = "936px";
                        document.getElementById("<%= CreateManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "0";
                        break;
                    }
                case '1':
                    {
                        document.getElementById("<%= CreateManualTest_resultsHiddenField.ClientID %>").value = "1";
                        document.getElementById("CreateManualTest_manualDiv").style.display = "none";
                        document.getElementById("CreateManualTest_adaptivelDiv").style.display = "block";
                        document.getElementById("CreateManualTest_manualDiv").style.width = "0";
                        document.getElementById("CreateManualTest_adaptiveTD").style.display = "block";
                        document.getElementById("CreateManualTest_adaptivelDiv").style.width = "100%";

                        document.getElementById("<%= CreateManualTest_questionDiv.ClientID %>").style.width = "0";
                        document.getElementById("<%= CreateManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "936px";
                        break;
                    }
                case '2':
                    {
                        document.getElementById("<%= CreateManualTest_resultsHiddenField.ClientID %>").value = "2";
                        document.getElementById("CreateManualTest_manualDiv").style.display = "block";
                        document.getElementById("CreateManualTest_adaptivelDiv").style.display = "block";
                        document.getElementById("CreateManualTest_manualDiv").style.width = "49%";
                        document.getElementById("CreateManualTest_adaptiveTD").style.display = "block";
                        document.getElementById("CreateManualTest_adaptivelDiv").style.width = "50%";

                        document.getElementById("<%= CreateManualTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
                default:
                    {
                        document.getElementById("<%= CreateManualTest_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("CreateManualTest_manualDiv").style.display = "block";
                        document.getElementById("CreateManualTest_adaptivelDiv").style.display = "block";
                        document.getElementById("CreateManualTest_adaptiveTD").style.display = "block";
                        document.getElementById("CreateManualTest_manualDiv").style.width = "49%";
                        document.getElementById("CreateManualTest_adaptivelDiv").style.width = "50%";

                        document.getElementById("<%= CreateManualTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateManualTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
            }
            return false;
        }

        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID)
        {
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source)
        {
            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID))
            {
                CheckNoOfChecked(QuestionID);
            }
            else
            {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id)
        {
            var table = sourceTable;
            var testVar = document.getElementById('<%=CreateManualTest_searchQuestionGridView.ClientID %>');
            var testVarTwo = document.getElementById('<%=CreateManualTest_adaptiveGridView.ClientID %>');
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++)
            {
                if (id == i)
                {
                    var row = table.rows[i];
                    if (detectBrowser())
                    {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else
                    {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox)
                    {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;
                    }
                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable))
                {
                    CheckNoOfChecked(id);
                }
            }
        }

        //find the Browser details
        function detectBrowser()
        {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape")
            {
                return true;
            }
            else
            {
                return false; ;
            }
        }
        // Question remove from the Test Draft.
        function RemoveRows(rowIndex)
        {
            sourceTable = document.getElementById('<%=CreateManualTest_testDrftGridView.ClientID %>');

            if (confirm('Are you sure to delete these question(s)?'))
            {
                if (checkChecked(rowIndex))
                {
                    DelRows();

                }
                else
                {
                    CheckCorrectQuestion(rowIndex);
                    DelRows();
                }
            }
            else
                return false;
        }

        function DelRows()
        {
            try
            {
                var table = document.getElementById('<%=CreateManualTest_testDrftGridView.ClientID %>');
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++)
                {
                    var row = table.rows[i];
                    var chkbox;
                    if (detectBrowser())
                    {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else
                    {
                        chkbox = row.cells[0].childNodes[0];
                    }
                    if (null != chkbox && true == chkbox.checked)
                    {
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e)
            {
                alert(e);
            }
            return false;
        }

        function CheckNoOfChecked(QuestionID)
        {

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=CreateManualTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++)
            {
                var row = table.rows[i];
                if (detectBrowser())
                {
                    chkbox = row.cells[0].childNodes[1];
                }
                else
                {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked)
                {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move ' + numberOfQuestions + ' question(s)';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser())
                    {
                        if (hiddenvalue.value == "")
                        {
                            hiddenvalue.value = row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                        else
                        {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                    }
                    else
                    {
                        if (hiddenvalue.value == "")
                        {
                            hiddenvalue.value = row.childNodes[4].childNodes[0].value + ',';
                        }
                        else
                        {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[0].value + ',';
                        }
                    }
                }

            }
            //var ShowDiv =
        }

        function checkChecked(id)
        {

            var isChecked = false;

            var table = sourceTable;
            // document.getElementById('<%=CreateManualTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++)
            {
                var row = table.rows[i];
                if (detectBrowser())
                {
                    chkbox = row.cells[0].childNodes[1];
                }
                else
                {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked)
                {
                    isChecked = true;
                }

                if (isChecked)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        function mouseMove(ev)
        {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject)
            {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser())
                {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else
                {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else
            {

            }
            return false;
        }
        function mouseCoords(ev)
        {
            if (ev != null)
            {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev)
        {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=CreateManualTest_testDrftGridView.ClientID %>');
            var buttonID = document.getElementById('<%=CreateManualTest_selectMultipleImage.ClientID %>');
            var targPos = getPosition(curTarget);
            var scrolltargPos;
            if (detectBrowser())
            {
                scrolltargPos = targPos.y;
            }
            else
            {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos))
            {

                // __doPostBack('ctl00_HCMMaster_body_CreateManualTest_mainTabContainer_CreateManualTest_questionsTabPanel_CreateManualTest_selectMultipleImage', "OnClick");
                __doPostBack('<%=CreateManualTest_selectMultipleImage.ClientID %>', "OnClick");

                dragObject.style.display = 'none';
            }
            else
            {
                if (dragObject != null)
                {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale()
        {
            var table = sourceTable;
            //document.getElementById('<%=CreateManualTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++)
            {
                var row = table.rows[i];
                if (detectBrowser())
                {
                    chkbox = row.cells[0].childNodes[1];
                }
                else
                {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked)
                {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e)
        {
            var left = 0;
            var top = 0;

            if (e != null)
            {
                while (e.offsetParent)
                {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }
                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }
    </script>
    <asp:Panel ID="CreateManualTest_mainPanel" runat="server" DefaultButton="CreateManualTest_bottomSaveButton">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text_bold">
                                <asp:Literal ID="CreateManualTest_headerLiteral" runat="server" Text="Create Manual Test"></asp:Literal>
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="22%">
                                        </td>
                                        <td width="40%" align="right">
                                            <asp:Button ID="CreateManualTest_topSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateManualTest_saveButton_Click" />
                                            <asp:Button ID="CreateManualTest_topCreateSessionButton" runat="server" SkinID="sknButtonId"
                                                Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="16%" align="right">
                                            <asp:LinkButton ID="CreateManualTest_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Reset" OnClick="CreateManualTest_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateManualTest_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateManualTest_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="CreateManualTest_adaptiveRecommendationsClicked" runat="server" />
                            <asp:Label ID="CreateManualTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateManualTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="CreateManualTest_stateHiddenField" runat="server" Value="0" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolKit:TabContainer ID="CreateManualTest_mainTabContainer" runat="server" ActiveTabIndex="0"
                        OnClientActiveTabChanged="setFocus">
                        <ajaxToolKit:TabPanel ID="CreateManualTest_questionsTabPanel" HeaderText="questions"
                            runat="server">
                            <HeaderTemplate>
                                Questions</HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="CreateManualTest_testQuestionDetailsPanel" runat="server" DefaultButton="CreateManualTest_topSearchButton">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div id="CreateManualTest_searchCriteriasDiv" runat="server" style="display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" class="td_padding_bottom_5">
                                                                <asp:UpdatePanel ID="CreateManualTest_simpleLinkButtonUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="CreateManualTest_simpleLinkButton" runat="server" Text="Advanced"
                                                                            SkinID="sknActionLinkButton" OnClick="CreateManualTest_simpleLinkButton_Click" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <asp:UpdatePanel ID="CreateManualTest_searchDiv_UpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="CreateManualTest_simpleSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualTest_categoryHeadLabel" runat="server" Text="Category"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateManualTest_categoryTextBox" runat="server" MaxLength="50"
                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualTest_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateManualTest_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualTest_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <div style="float: left; padding-right: 5px;">
                                                                                                        <asp:TextBox ID="CreateManualTest_keywordsTextBox" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                                    <div style="float: left;">
                                                                                                        <asp:ImageButton ID="CreateManualTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div id="CreateManualTest_advanceSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <uc1:CategorySubjectControl ID="CreateManualTest_categorySubjectControl" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_8">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="panel_inner_body_bg">
                                                                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateManualTest_testAreaHeadLabel" runat="server" Text="Test Area"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="5" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                                                <asp:CheckBoxList ID="CreateManualTest_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                                    RepeatDirection="Vertical" CellSpacing="5" Width="100%">
                                                                                                                </asp:CheckBoxList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateManualTest_complexityLabel" runat="server" Text="Complexity"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3" align="left" valign="middle">
                                                                                                                <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                                    class="checkbox_list_bg">
                                                                                                                    <asp:CheckBoxList ID="CreateManualTest_complexityCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                                        RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                    </asp:CheckBoxList>
                                                                                                                </div>
                                                                                                                <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                                    <asp:ImageButton ID="CreateManualTest_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTest_keywordHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualTest_keywordTextBox" runat="server" Columns="50" MaxLength="100"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTest_questionLabel" runat="server" Text="Question ID"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="CreateManualTest_questionIDTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTest_creditHeadLabel" runat="server" Text="Credit" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="CreateManualTest_creditTextBox" runat="server"></asp:TextBox>
                                                                                                                <ajaxToolKit:FilteredTextBoxExtender ID="CreateManualTest_weightageFileteredExtender"
                                                                                                                    runat="server" TargetControlID="CreateManualTest_creditTextBox" FilterType="Numbers"
                                                                                                                    Enabled="True">
                                                                                                                </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualTest_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualTest_authorTextBox" runat="server"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="CreateManualTest_dummyAuthorID" runat="server" />
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualTest_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManulTest_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Position Profile"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3">
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManulTest_positionProfileTextBox" runat="server" MaxLength="200" Columns="55"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManulTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" /></div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManulTest_positionProfileKeywordLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Position Profile Keyword"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px; width: 100%">
                                                                                                                    <asp:TextBox ID="CreateManulTest_positionProfileKeywordTextBox" runat="server" MaxLength="500"></asp:TextBox>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <div style="display: none">
                                                                                                            </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        </td> </tr> </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="td_padding_top_5">
                                                                            <asp:Button ID="CreateManualTest_topSearchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                                                OnClick="CreateManualTest_searchQuestionButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr id="CreateManualTest_searchResultsTR" runat="server">
                                            <td id="Td1" class="header_bg" runat="server">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 80%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="CreateManualTest_searchResultsLiteral" runat="server" Text="Questions"></asp:Literal>&nbsp;<asp:Label
                                                                ID="CreateManualTest_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                            <asp:HiddenField ID="CreateManualTest_resultsHiddenField" runat="server" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualTest_manualImageButton" runat="server" SkinID="sknManualImageButton"
                                                                ToolTip="Click here to view 'Search Results' only" OnClientClick="javascript:return ShowOrHideDiv('0');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualTest_adaptiveImageButton" runat="server" SkinID="sknAdaptiveImageButton"
                                                                ToolTip="Click here to view 'Adaptive Recommended Questions' only" OnClientClick="javascript:return ShowOrHideDiv('1');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualTest_bothImageButton" runat="server" SkinID="sknCombinedAMImageButton"
                                                                ToolTip="Click here to view both 'Search Results' and 'Adaptive Recommended Questions'"
                                                                OnClientClick="javascript:return ShowOrHideDiv('2');" />
                                                        </td>
                                                        <td style="width: 5%" align="right">
                                                            <span id="CreateManualTest_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="CreateManualTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="CreateManualTest_searchResultsDownSpan" runat="server" style="display: block;">
                                                                <asp:Image ID="CreateManualTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                            <asp:HiddenField ID="CreateManualTest_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" id="CreateManualTest_adaptiveTD">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="CreateManualTest_adaptiveQuestionsHeadLabel" runat="server" Text="Adaptive Recommendations"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:UpdatePanel ID="CreateManualTest_adaptiveYesNoUpdatePanel" runat="server">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="CreateManualTest_adaptiveYesNoRadioButtonList" runat="server"
                                                                                    RepeatColumns="2" OnSelectedIndexChanged="CreateManualTest_adaptiveYesNoRadioButtonList_SelectedIndexChanged"
                                                                                    AutoPostBack="true">
                                                                                    <asp:ListItem Text="On" Value="0"></asp:ListItem>
                                                                                    <asp:ListItem Text="Off" Value="1" Selected="True"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="CreateManualTest_recommendAdaptiveQuestions" runat="server" Text="Recommend Questions"
                                                                                    SkinID="sknButtonId" OnClick="CreateManualTest_recommendAdaptiveQuestions_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td valign="top">
                                                <div id="CreateManualTest_manualDiv" style="width: 49%; float: left; display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateManualTest_manualQuestLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="grid_body_bg">
                                                                <asp:UpdatePanel ID="CreateManualTest_searchQuestionGridView_UpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 180px; width: 100%; overflow: auto;" runat="server" id="CreateManualTest_questionDiv">
                                                                                        <asp:GridView ID="CreateManualTest_searchQuestionGridView" runat="server" AutoGenerateColumns="False"
                                                                                            OnRowDataBound="CreateManualTest_searchQuestionGridView_RowDataBound" OnRowCommand="CreateManualTest_searchQuestionGridView_RowCommand">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="CreateManualTest_searchQuestionCheckbox" runat="server" />
                                                                                                        <asp:HiddenField ID="CreateManualTest_questionKeyHiddenField" runat="server" Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTest_previewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                                            CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                            ToolTip="Question Details Summary" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTest_searchQuestionAlertImageButton" runat="server"
                                                                                                            SkinID="sknAlertImageButton" ToolTip="Flag Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTest_searchQuestionSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                                            CommandName="Select" ToolTip="Select Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="CreateManualTest_hidddenValue" runat="server" Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                        <asp:Panel ID="CreateManualTest_hoverPanel" runat="server" width="750px" CssClass="table_outline_bg">
                                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                                                <tr>
                                                                                                                    <th class="popup_question_icon">
                                                                                                                        <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                        <asp:Label ID="CreateManualTest_questionDetailPreviewControlQuestionLabel"
                                                                                                                            Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                                                                            runat="server" SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                        </div>
                                                                                                                    </th>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th class="td_padding_left_20">
                                                                                                                        <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                                            <asp:PlaceHolder ID="CreateManualTest_answerChoicesPlaceHolder" runat="server"></asp:PlaceHolder>
                                                                                                                        </div>
                                                                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                                    </th>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </asp:Panel>
                                                                                                        <ajaxToolKit:HoverMenuExtender ID="CreateManualTest_hoverMenuExtender" runat="server"
                                                                                                            PopupControlID="CreateManualTest_hoverPanel" PopupPosition="Bottom" HoverCssClass="popupHover"
                                                                                                            TargetControlID="CreateManualTest_questionLabel" PopDelay="50" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Question">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                            Width="320px"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Category">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Answer">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                            Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                            Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <uc2:PageNavigator ID="CreateManualTest_bottomPagingNavigator" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="CreateManualTest_topSearchButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="moveDisplayDIV" style="display: none; background-color: Lime" runat="server">
                                                    <asp:TextBox ID="sampleDIVTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                </div>
                                                <div id="CreateManualTest_adaptivelDiv" style="width: 50%; float: right; display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateManualTest_adaptiveQuestLiteral" runat="server" Text="Adaptive Recommended Questions"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                            <td class="grid_body_bg">
                                                                <asp:UpdatePanel ID="CreateManualTest_adaptiveUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 180px; width: 0px; overflow: auto;" runat="server" id="CreateManualTest_adaptiveQuestionDiv">
                                                                                        <asp:GridView ID="CreateManualTest_adaptiveGridView" runat="server" AutoGenerateColumns="False"
                                                                                            OnRowDataBound="CreateManualTest_adaptiveGridView_RowDataBound" OnRowCommand="CreateManualTest_adaptiveGridView_RowCommand">
                                                                                            <EmptyDataTemplate>
                                                                                                <center>
                                                                                                    <asp:Label ID="CreateManualTest_adaptiveEmptyLabel" runat="server" Text="No question found for recommendation"
                                                                                                        SkinID="sknErrorMessage"></asp:Label>
                                                                                                </center>
                                                                                            </EmptyDataTemplate>
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="CreateManualTest_adaptiveCheckbox" runat="server" />
                                                                                                        <asp:HiddenField ID="CreateManualTest_adaptiveQuestionKeyHiddenField" runat="server"
                                                                                                            Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTest_adaptivepreviewImageButton" runat="server"
                                                                                                            SkinID="sknPreviewImageButton" CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                            ToolTip="Question Details Summary" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTest_adaptiveAlertImageButton" runat="server" SkinID="sknAlertImageButton"
                                                                                                            ToolTip="Flag Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualTest_adaptiveSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                                            CommandName="Select" ToolTip="Select Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="CreateManualTest_adaptiveQuestionhiddenValue" runat="server"
                                                                                                            Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                        <asp:Panel ID="CreateManualTest_adaptvieQuestionPreviewhoverPanel" runat="server"
                                                                                                            CssClass="table_outline_bg" Width="750px">
                                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="3" class="">
                                                                                                                <tr>
                                                                                                                    <th class="popup_question_icon">
                                                                                                                    <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                        <asp:Label ID="CreateManualTest_adaptiveQuestionDetailPreviewControlQuestionLabel"
                                                                                                                            Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>' runat="server" SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                    </div>
                                                                                                                    </th>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <th class="td_padding_left_20">
                                                                                                                        <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                                            <asp:PlaceHolder ID="CreateManualTest_adaptiveAnswerChoicesPlaceHolder" runat="server">
                                                                                                                            </asp:PlaceHolder>
                                                                                                                        </div>
                                                                                                                        <asp:HiddenField ID="CreateManualTest_adaptiveCorrectAnswerHiddenField" runat="server"
                                                                                                                            Value='<%# Eval("Answer") %>' />
                                                                                                                    </th>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </asp:Panel>
                                                                                                        <ajaxToolKit:HoverMenuExtender ID="CreateManualTest_adaptivePreviewhoverMenuExtender"
                                                                                                            runat="server" PopupControlID="CreateManualTest_adaptvieQuestionPreviewhoverPanel"
                                                                                                            PopupPosition="Bottom" HoverCssClass="popupHover" TargetControlID="CreateManualTest_adaptiveQuestionLabel"
                                                                                                            PopDelay="50" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Question">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_adaptiveQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                            Width="320px"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Category">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_adaptiveCategoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_adaptiveSubjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Answer">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_adaptiveAnswerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                            Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                            Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <%--<Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="CreateManualTest_adaptiveCheckbox" runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateManualTest_adptivepreviewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                            CommandName="view" ToolTip="Preview Question" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateManualTest_adaptiveAlertImageButton" runat="server" SkinID="sknAlertImageButton"
                                                                                            ToolTip="Flag Question" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateManualTest_adaptiveSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                            ToolTip="Select Question" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="CreateManualTest_hidddenValue" runat="server" Value='<%#Eval("QuestionID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateManualTest_adaptiveQuestionDetailsImage" runat="server"
                                                                                            SkinID="sknDraftImageButton" ToolTip="Adaptive Question Details" CommandName="AdaptiveQuestion" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Question">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="CreateManualTest_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),70) %>'></asp:Label>
                                                                                        <asp:Panel ID="CreateManualTest_hoverPanel" runat="server" CssClass="popupMenu">
                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="3" class="table_outline_bg">
                                                                                                <tr>
                                                                                                    <td class="popup_question_icon">
                                                                                                        <asp:Label ID="CreateManualTest_questionDetailPreviewControlQuestionLabel" Text='<%# Eval("Question") %>'
                                                                                                            runat="server" SkinID="sknLabelText"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_padding_left_20">
                                                                                                        <asp:RadioButtonList ID="CreateManualTest_questionPreviewControlAnswerRadioButtonList"
                                                                                                            runat="server" RepeatColumns="1" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                            TextAlign="Right" Width="100%">
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <ajaxToolKit:HoverMenuExtender ID="CreateManualTest_hoverMenuExtender1" runat="server"
                                                                                            PopupControlID="CreateManualTest_hoverPanel" PopupPosition="Left" HoverCssClass="popupHover"
                                                                                            TargetControlID="CreateManualTest_hoverPanel" PopDelay="50" />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Wrap="False" />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Category" InsertVisible="False" DataField="CategoryName">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="140px"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Subject" InsertVisible="False" DataField="SubjectName">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="140px" Wrap="False"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Answer" InsertVisible="False" DataField="AnswerID">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="120px" Wrap="False"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Complexity" InsertVisible="False" DataField="ComplexityName">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>--%>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                    <asp:HiddenField ID="CreateManualTest_adaptiveGridViewHiddenField" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:HiddenField ID="CreateManualTest_adaptivePagingHiddenField" runat="server" Value="1" />
                                                                                    <uc2:PageNavigator ID="CreateManualTest_adaptivebottomPagingNavigator" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td align="center" class="grid_header_bg">
                                                <table width="10%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="CreateManualTest_selectMultipleImage" runat="server" SkinID="sknMultiDown_ArrowImageButton"
                                                                CommandName="Select" ToolTip="Move questions to test draft" OnClick="CreateManualTest_selectMultipleImage_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="CreateManualTest_removeMultipleImage" runat="server" SkinID="sknMultiUp_ArrowImageButton"
                                                                CommandName="Select" ToolTip="Remove questions from test draft" OnClick="CreateManualTest_removeMultipleImage_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="CreateManualTest_testDrftGridView_UpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                                <td class="grid_header_bg">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="header_text_bold">
                                                                                <asp:Literal ID="CreateManualTest_testDraftLiteral" runat="server" Text="Test Draft"></asp:Literal>&nbsp;
                                                                                <asp:Label ID="CreateManualTest_testDrafttHelpLabel" runat="server" SkinID="sknLabelText"
                                                                                    Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>" Visible="false"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr onmouseup="javascript:srsmouseup(event);" onmousemove="javascript:mouseMove(event);"
                                                                style="height: 35px">
                                                                <td class="grid_body_bg" valign="top">
                                                                    <asp:GridView ID="CreateManualTest_testDrftGridView" OnRowDataBound="CreateManualTest_testDrftGridView_RowDataBound"
                                                                        runat="server" OnRowCommand="CreateManualTest_testDrftGridView_RowCommand" EmptyDataText="&nbsp;">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="CreateManualTest_testDrftCheckbox" runat="server" />
                                                                                    <asp:HiddenField ID="CreateManualTest_questionKeyHiddenField" runat="server" Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="CreateManualTest_testDrftRemoveImage" runat="server" SkinID="sknDeleteImageButton"
                                                                                        CommandName="Select" ToolTip="Remove Question" CommandArgument='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                                DataField="QuestionID" Visible="False">
                                                                                <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Question">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateManualTest_draftQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                        Width="320px"></asp:Label>
                                                                                    <asp:Panel ID="CreateManualTest_hoverPanel" runat="server" Width="750px" CssClass="table_outline_bg">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <th class="popup_question_icon">
                                                                                                    <div style="word-wrap: break-word; white-space: normal;">
                                                                                                    <asp:Label ID="CreateManualTest_draftPreviewQuestionLabel" runat="server" SkinID="sknLabelFieldMultiText"
                                                                                                        Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </th>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th class="td_padding_left_20">
                                                                                                    <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                        <asp:PlaceHolder ID="CreateManualTest_answerChoicesPlaceHolder" runat="server"></asp:PlaceHolder>
                                                                                                    </div>
                                                                                                    <asp:HiddenField ID="CreateManualTest_correctAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                </th>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>
                                                                                    <ajaxToolKit:HoverMenuExtender ID="CreateManualTest_draftHoverMenuExtender1" runat="server"
                                                                                        PopupControlID="CreateManualTest_hoverPanel" PopupPosition="Bottom" HoverCssClass="popupHover"
                                                                                        TargetControlID="CreateManualTest_draftQuestionLabel" PopDelay="50" />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="true" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Category">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateManualTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                        Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Subject">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateManualTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                        Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Answer">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateManualTest_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                        Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Complexity">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateManualTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                        Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="CreateManualTest_selectMultipleImage" />
                                                        <asp:AsyncPostBackTrigger ControlID="CreateManualTest_removeMultipleImage" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="CreateManualTest_testSummaryUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <uc5:ManualTestSummaryControl ID="CreateManualTest_testSummaryControl" runat="server" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="CreateManualTest_testdetailsTabPanel" runat="server">
                            <HeaderTemplate>
                                Test Details</HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="width: 100%">
                                            <asp:UpdatePanel ID="CreateManualTest_testDetailsUserControl_UpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <uc4:TestDetailsControl ID="CreateManualTest_testDetailsUserControl" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateManualTest_bottomMessageupdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="CreateManualTest_previewQuestionAddHiddenField" runat="server" />
                            <asp:Label ID="CreateManualTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateManualTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="22%">
                                        </td>
                                        <td width="40%" align="right">
                                            <asp:Button ID="CreateManualTest_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateManualTest_saveButton_Click" />
                                            <asp:Button ID="CreateManualTest_bottomCreateSessionButton" runat="server" SkinID="sknButtonId"
                                                Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="16%" align="right">
                                            <asp:LinkButton ID="CreateManualTest_bottompResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Reset" OnClick="CreateManualTest_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateManualTest_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="CreateManualTest_questionDetailPanel" runat="server" CssClass="popupcontrol_question_draft"
                                                Style="display: none">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="td_height_20">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                        <asp:Literal ID="CreateManualTest_questionDetailResultLiteral" runat="server" Text="Question Detail"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CreateManualTest_questionDetailPreviewControlTopCancelImageButton"
                                                                                        runat="server" SkinID="sknCloseImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataGrid ID="CreateManualTest_questionDetailCategoryDataGrid" runat="server"
                                                                                        AutoGenerateColumns="false">
                                                                                        <Columns>
                                                                                            <asp:BoundColumn HeaderText="Category" DataField="Category"></asp:BoundColumn>
                                                                                            <asp:BoundColumn HeaderText="Subject" DataField="Subject"></asp:BoundColumn>
                                                                                        </Columns>
                                                                                    </asp:DataGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_panel_inner_bg">
                                                                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailTestAreaHeadLabel" runat="server" Text="Test Area"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailTestAreaLabel" runat="server" Text="Concept"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailNoOfTestLabel" runat="server" Text="Number Of Administered Test "
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailTestAdministeredLabel" runat="server"
                                                                                                    Text="5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailAvgTimeLabel" runat="server" Text="Average Time Taken(in minutes)"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailAvgTimeValueLabel" runat="server" Text="15.5"
                                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailCorrectToAttendedRatioHeadLabel" runat="server"
                                                                                                    Text=" Ratio of Correct Answer To Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailCorrectToAttendedRatioLabel" runat="server"
                                                                                                    Text="36:50" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailComplexityHeadLabel" runat="server"
                                                                                                    Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualTest_questionDetailPreviewControlComplexityLabel" runat="server"
                                                                                                    Text="Normal" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_question_icon">
                                                                                    <asp:Label ID="CreateManualTest_questionDetailQuestionLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_padding_left_20">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <asp:RadioButtonList ID="CreateManualTest_questionDetailAnswerRadioButtonList" runat="server"
                                                                                            RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5" TextAlign="Right"
                                                                                            Width="100%">
                                                                                        </asp:RadioButtonList>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="padding-left: 30px">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="CreateManualTest_questionDetailTopAddButton" runat="server" Text="Add"
                                                                                        SkinID="sknButtonId" OnClick="CreateManualTest_questionDetailTopAddButton_Click" />
                                                                                </td>
                                                                                <td style="padding-left: 10px">
                                                                                    <asp:LinkButton ID="CreateManualTest_questionDetailTopCancelButton" runat="server"
                                                                                        SkinID="sknCancelLinkButton" Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div style="display: none">
                                                <asp:Button ID="CreateManualTest_adaptiveQuestionPreviewPanelTargeButton" runat="server" />
                                            </div>
                                            <ajaxToolKit:ModalPopupExtender ID="CreateManualTest_adaptiveQuestionPreviewModalPopupExtender"
                                                runat="server" PopupControlID="CreateManualTest_questionDetailPanel" TargetControlID="CreateManualTest_adaptiveQuestionPreviewPanelTargeButton"
                                                BackgroundCssClass="modalBackground" CancelControlID="CreateManualTest_questionDetailTopCancelButton">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="CreateManualTest_questionDetailPreviewUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="CreateManualTest_questionPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <div style="display: none">
                    <asp:Button ID="CreateManualTest_questionModalTargeButton" runat="server" />
                </div>
                <%-- <uc3:QuestionDetailPreviewControl ID="CreateManualTest_questionDetailPreviewControl"
                    runat="server" Title="Question Detail Preview" OnCancelClick="CreateManualTest_cancelClick" OnAddClick="CreateManualTest_questionDetailPreviewControl_AddClick" />--%>
                <uc7:QuestionDetailSummaryControl ID="CreateManualTest_questionDetailSummaryControl"
                    runat="server" Title="Question Details Summary" OnCancelClick="CreateManualTest_cancelClick"
                    OnAddClick="CreateManualTest_questionDetailPreviewControl_AddClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateManualTest_questionModalPopupExtender"
                runat="server" PopupControlID="CreateManualTest_questionPanel" TargetControlID="CreateManualTest_questionModalTargeButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="CreateManualTest_ConfirmPopupUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="CreateManualTest_authorIdHiddenField" runat="server" />
            <asp:HiddenField ID="CreateManualTest_positionProfileHiddenField" runat="server" />
            <asp:HiddenField ID="hiddenValue" runat="server" />
            <asp:HiddenField ID="hiddenDeleteValue" runat="server" />
            <div id="CreateManualTest_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="CreateManualTest_hiddenPopupModalButton" runat="server" />
            </div>
            <asp:Panel ID="CreateManualTest_ConfirmPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_remove">
                <uc6:ConfirmMsgControl ID="CreateManualTest_ConfirmPopupExtenderControl" runat="server"
                    OnOkClick="CreateManualTest_okClick" OnCancelClick="CreateManualTest_cancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateManualTest_ConfirmPopupExtender" runat="server"
                PopupControlID="CreateManualTest_ConfirmPopupPanel" TargetControlID="CreateManualTest_hiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function ()
        {
            window.setTimeout(focus, 1);
        }
    )
        function focus()
        {
            try
            {
                document.getElementById('<%=CreateManualTest_categoryTextBox.ClientID %>').focus();
            }
            catch (Err)
            {
                try
                {
                    document.getElementById('<%=CreateManualTest_keywordTextBox.ClientID %>').focus();
                }
                catch (Err1)
                {
                    document.getElementById('ctl00$OTMMaster_body$CreateManualTest_mainTabContainer$CreateManualTest_testdetailsTabPanel$CreateManualTest_testDetailsUserControl$TestDetailsControl_testNameTextBox').focus();

                }
            }
        }
    </script>
</asp:Content>
