#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestRecommendationSummary.cs
// File that represents the user interface layout and functionalities for
// the ViewTestRecommendation page. This page helps in viewing the 
// recommended test and segment details.

#endregion Header

#region Directives                                                             

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewTestRecommendation page. This page helps in viewing the 
    /// recommended test and segment details. This class inherits the
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class TestRecommendationSummary : PageBase
    {
        #region Declarations                                                   

        /// <summary>
        /// A <see cref="string"/> that holds the status that indicates whether
        /// to display page as create or edit mode.
        /// </summary>
        /// <remarks>
        /// The value 'E' indicates edit and empty indicates create mode.
        /// </remarks>
        private string mode = string.Empty;

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the questions for the test in test draft gridview.
        /// </summary>
        private const string TestRecommendationSummary_QUESTIONVIEWSTATE = "TestRecommendationSummary_QUESTIONSGRID";

        
        #endregion Declarations

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loading.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button.
                Page.Form.DefaultButton = TestRecommendationSummary_searchButton.UniqueID;
               
                // Set browser title.
                Master.SetPageCaption("Test Summary");

                ClearAllLabelMessages();
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    // Load values.
                    LoadValues();

                    // Load test details.
                    LoadTestDetails();

                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "FIRST_NAME";

                    // Check if page is redirected from any child page. If the page if redirected
                    // from any child page, fill the search criteria and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.TEST_RECOMMENDATION_SUMMARY] != null)
                    {
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.TEST_RECOMMENDATION_SUMMARY]
                            as TestRecommendationDetailSearchCriteria);

                        CheckAndSetExpandorRestore();
                    }
                    else
                    {
                        // Load candidates.
                        LoadCandidates(1);
                    }
                }

                TestRecommendationSummary_pagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler(TestRecommendationSummary_pagingNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler event that will reset the page when user cliks on the reset link button
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendationSummary_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear search criteria from session.
                Session[Constants.SearchCriteriaSessionKey.TEST_RECOMMENDATION_SUMMARY] = null;

                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the test segment grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendationSummary_viewSegmentsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                Label TestRecommendationSummary_viewSegmentsGridView_expecetdQuestions =
                    (Label)e.Row.FindControl("TestRecommendationSummary_viewSegmentsGridView_expecetdQuestions");

                string[] selectedTestAreaIDs = ((HiddenField)e.Row.
                    FindControl("TestRecommendationSummary_viewSegmentsGridView_testAreaHiddenField")).Value.Split(',');

                // Compose the comma separated test area names.
                string testAreaNames = string.Empty;

                // Loop through the list of test areas ans find the matching names.
                foreach (string selectedTestAreaID in selectedTestAreaIDs)
                {
                    foreach (AttributeDetail testArea in GetTestAreaList())
                    {
                        if (selectedTestAreaID == testArea.AttributeID)
                        {
                            if (testAreaNames == string.Empty)
                                testAreaNames = testArea.AttributeName;
                            else
                                testAreaNames = testAreaNames  + ", " + testArea.AttributeName;
                        }
                    }
                }
                
                // Assign the test area names.
                ((Label)e.Row.FindControl("TestRecommendationSummary_viewSegmentsGridView_testAreaValueLabel")).Text = testAreaNames;

                string[] selectedComplexityIDs = ((HiddenField)e.Row.
                    FindControl("TestRecommendationSummary_viewSegmentsGridView_complexityHiddenField")).Value.Split(',');

                // Compose the comma separated test area names.
                string complexityNames = string.Empty;

                // Loop through the list of test areas ans find the matching names.
                foreach (string selectedComplexityID in selectedComplexityIDs)
                {
                    foreach (AttributeDetail complexity in GetComplexityList())
                    {
                        if (selectedComplexityID == complexity.AttributeID)
                        {
                            if (complexityNames == string.Empty)
                                complexityNames = complexity.AttributeName;
                            else
                                complexityNames = complexityNames + ", " + complexity.AttributeName;
                        }
                    }
                }

                // Assign the complexity names.
                ((Label)e.Row.FindControl("TestRecommendationSummary_viewSegmentsGridView_complexityValueLabel")).Text = complexityNames;

                if (TestRecommendationSummary_viewSegmentsGridView_expecetdQuestions.Text != "")
                    ((HtmlGenericControl)e.Row.FindControl("TestRecommendationSummary_viewSegmentsGridView_expectedQuestionDiv")).Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the search button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestRecommendationSummary_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearLabelMessage();

                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "FIRST_NAME";

                // Load tests
                LoadCandidates(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row created event is fired 
        /// in the test recommendations grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void TestRecommendationSummary_candidatesGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (TestRecommendationSummary_candidatesGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row command event is fired
        /// in the test recommendations grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void TestRecommendationSummary_candidatesGridView_RowCommand
           (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                // Check if the command name is valid.
                if (e.CommandName != "TestStatistics")
                {
                    return;
                }

                int index = Convert.ToInt32(e.CommandArgument);
                string testKey = (TestRecommendationSummary_candidatesGridView.Rows
                    [index].FindControl("TestRecommendationSummary_candidatesGridView_testKeyHiddenField") as HiddenField).Value;

                if (e.CommandName == "TestStatistics")
                {
                    // Redirects to the test statistics page.
                    string url = "~/ReportCenter/TestStatisticsInfo.aspx" +
                        "?m=3&s=0" +
                        "&id=" + Request.QueryString["id"] + 
                        "&testkey=" + testKey + 
                        "&parentpage=" + Constants.ParentPage.TEST_RECOMMENDATION_SUMMARY;

                    // Redirect.
                    Response.Redirect(url, false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the sorting event is fired in 
        /// the test recommendations grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void TestRecommendationSummary_candidatesGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                // Check whether the sort field in view state is same as 
                // the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                // Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                TestRecommendationSummary_pagingNavigator.Reset();

                // Set question details based on the values.
                LoadCandidates(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row data bound event is
        /// fired in the test recommendationsgrid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void TestRecommendationSummary_candidatesGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the page number is clicked
        /// in the paging control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that holds the event data.
        /// </param>
        private void TestRecommendationSummary_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadCandidates(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the test details.
        /// </summary>
        private void LoadTestDetails()
        {
            // Check if test recommendation ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["id"]))
            {
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, "Test recommendation ID is missing");

                return;
            }

            // Check if test recommendation ID is vaid.
            int testRecommendationID = 0;
            int.TryParse(Request.QueryString["id"], out testRecommendationID);

            if (testRecommendationID == 0)
            {
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, "Test recommendation ID is invalid");

                return;
            }

            // Get test recommendation detail.
            TestRecommendationDetail testDetail = new TestRecommendationBLManager().
                GetTestSegmentDetail(testRecommendationID);

            if (testDetail == null)
            {
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                    TestRecommendationSummary_bottomErrorMessageLabel, "Test recommendation not found");

                return;
            }

            // Load test detail for view mode.
            TestRecommendationSummary_viewNumberOfQuestionValueLabel.Text = testDetail.NoOfQuestions.ToString();
            TestRecommendationSummary_viewskillsValueLabel.Text = testDetail.Skill;
            TestRecommendationSummary_viewTimeLimitValueLabel.Text = Utility.ConvertSecondsToHHMM(testDetail.TimeLimit);
            TestRecommendationSummary_viewTestNameValueLabel.Text = testDetail.TestName;
            TestRecommendationSummary_viewTestDescriptionValueLabel.Text = testDetail.TestDescription;

            // Construct segment data table.
            DataTable segmentTable = new DataTable();
            AddTestSegmentColumns(ref segmentTable);

            // Fill segments.
            FillSegmentsTable(testDetail.Segments, segmentTable);

            // Assign segments.
            TestRecommendationSummary_viewSegmentsGridView.DataSource = segmentTable;
            TestRecommendationSummary_viewSegmentsGridView.DataBind();
        }

        /// <summary>
        /// Method that fills the search criteria and apply the search.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="TestRecommendationDetailSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <remarks>
        /// This will be done only if the return request is made to this page (for eg from
        /// coming back from edit test recommendation page).
        /// </remarks>
        private void FillSearchCriteria(TestRecommendationDetailSearchCriteria criteria)
        {
            // Apply search criteria.
            TestRecommendationSummary_candidateNameTextBox.Text = criteria.CandidateName;
            TestRecommendationSummary_candidateEmailTextBox.Text = criteria.CandidateEmail;

            if (!Utility.IsNullOrEmpty(criteria.Status))
                TestRecommendationSummary_statusDropDownList.SelectedValue = criteria.Status;
            else
                TestRecommendationSummary_statusDropDownList.SelectedValue = string.Empty;

            TestRecommendationSummary_isMaximizedHiddenField.Value = criteria.IsMaximized ? "Y" : "N";

            ViewState["SORT_ORDER"] = criteria.SortType;
            ViewState["SORT_FIELD"] = criteria.SortField;

            // Apply search.
            LoadCandidates(criteria.PageNumber);

            // Highlight the last page number which is stored in session
            // when the page is launched from somewhere else.
            TestRecommendationSummary_pagingNavigator.MoveToPage(criteria.PageNumber);
        }

        /// <summary>
        /// Method that loads the candidates for the given page number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadCandidates(int pageNumber)
        {
            // Clear messages.
            ClearLabelMessage();

            int totalRecords = 0;

            // Keep the page number in view state.
            ViewState["PAGE_NUMBER"] = pageNumber;

            // Construct search criteria.
            TestRecommendationDetailSearchCriteria criteria = new TestRecommendationDetailSearchCriteria();

            criteria.TestRecommendId = Convert.ToInt32(Request.QueryString["id"]);

            if (!Utility.IsNullOrEmpty(TestRecommendationSummary_statusDropDownList.SelectedValue))
                criteria.Status = TestRecommendationSummary_statusDropDownList.SelectedValue.Trim();
            else
                criteria.Status = string.Empty;

            criteria.CandidateName = TestRecommendationSummary_candidateNameTextBox.Text.Trim() == string.Empty ?
                null : TestRecommendationSummary_candidateNameTextBox.Text.Trim();

            criteria.CandidateEmail = TestRecommendationSummary_candidateEmailTextBox.Text.Trim() == string.Empty ?
                null : TestRecommendationSummary_candidateEmailTextBox.Text.Trim();

            criteria.PageNumber = pageNumber;
            criteria.IsMaximized = Utility.IsNullOrEmpty(TestRecommendationSummary_isMaximizedHiddenField.Value) ? false :
                (TestRecommendationSummary_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false);

            criteria.SortType = (SortType)ViewState["SORT_ORDER"];
            criteria.SortField = ViewState["SORT_FIELD"].ToString();

            // Keep the search criteria in session.
            Session[Constants.SearchCriteriaSessionKey.TEST_RECOMMENDATION_SUMMARY] = criteria;

            List<TestRecommendationDetail> candidates = new TestRecommendationBLManager().GetSelfAdminTestCandidates
                (criteria, pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                (SortType)ViewState["SORT_ORDER"], out totalRecords);

            TestRecommendationSummary_candidatesGridView.DataSource = candidates;
            TestRecommendationSummary_candidatesGridView.DataBind();
            TestRecommendationSummary_pagingNavigator.TotalRecords = totalRecords;
            TestRecommendationSummary_pagingNavigator.PageSize = base.GridPageSize;
        }

        /// <summary>
        /// Clear all top and bottom message labels
        /// </summary>
        private void ClearAllLabelMessages()
        {
            TestRecommendationSummary_topErrorMessageLabel.Text = string.Empty;
            TestRecommendationSummary_topSuccessMessageLabel.Text = string.Empty;
            TestRecommendationSummary_bottomErrorMessageLabel.Text = string.Empty;
            TestRecommendationSummary_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Binds emtpty test segment table grid
        /// </summary>
        private void BindEmptyTestSegmentGrid()
        {
            DataTable dtTestSegment = new DataTable();
            AddTestSegmentColumns(ref dtTestSegment);
            DataRow drNewRow = dtTestSegment.NewRow();
            dtTestSegment.Rows.Add(drNewRow);

            TestRecommendationSummary_viewSegmentsGridView.DataSource = dtTestSegment;
            TestRecommendationSummary_viewSegmentsGridView.DataBind();
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Cat_sub_id", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Complexity", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("TestArea", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Category", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Subject", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Keyword", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("ExpectedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("PickedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("TotalRecordsinDB", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("QuestionsDifference", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Remarks", typeof(string));
        }

        /// <summary>
        /// Method that fills the segment table rows by taking the source data 
        /// from the segments list.
        /// </summary>
        /// <param name="segments">
        /// A list of <see cref="TestRecommendationSegment"/> that holds the source data.
        /// </param>
        /// <param name="segmentTable">
        /// A <see cref="DataTable"/> that holds the segment table.
        /// </param>
        private void FillSegmentsTable(List<TestRecommendationSegment> segments, DataTable segmentTable)
        {
            if (segments == null || segments.Count == 0)
                return;

            DataRow drNewRow = null;

            int SNo = 1;
            foreach (TestRecommendationSegment segment in segments)
            {
                drNewRow = segmentTable.NewRow();
                drNewRow["SNO"] = SNo++.ToString();
                drNewRow["Cat_sub_id"] = segment.CateogorySubjectID;
                drNewRow["Category"] = segment.Category;
                drNewRow["Subject"] = segment.Subject;
                drNewRow["Weightage"] = segment.Weightage;
                drNewRow["Keyword"] = segment.Keyword;
                drNewRow["TestArea"] = segment.TestArea;
                drNewRow["Complexity"] = segment.Complexity;
                segmentTable.Rows.Add(drNewRow);
            }
        }

        /// <summary>
        /// Gets the elected ID's for a checkboxlist
        /// </summary>
        /// <param name="checkBoxList">checkboxlist to get the selected values</param>
        /// <returns>Selected ID's in the checkboxlist</returns>
        private string GetSelectedValuesfromCheckBoxList(CheckBoxList checkBoxList)
        {
            StringBuilder stringBuilder = null;
            try
            {
                if (stringBuilder == null)
                    stringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        stringBuilder.Append(checkBoxList.Items[i].Value);
                        stringBuilder.Append(",");
                    }
                }
                return stringBuilder.ToString().TrimEnd(',');
            }
            finally
            {
                if (stringBuilder != null) stringBuilder = null;
            }
        }

        private string GetSkillName(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return strKeyWord;
            return strKeyWord.Substring(0, strKeyWord.IndexOf('['));
        }

        private string GetSkillWeightage(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return string.Empty;
            return strKeyWord.Substring(strKeyWord.IndexOf('[') + 1, strKeyWord.Length - strKeyWord.IndexOf('[') - 2);
        }

        /// <summary>
        /// This method loads the user provided search criteria 
        /// in to collection of 'TestSearchCriteria' data object
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid data table</param>
        /// <returns>List of 'TestSearchCriteria' data object that contains
        /// user given search criteria</returns>
        private List<TestSearchCriteria> GetQuestionSearchCriteria(ref DataTable dtSearchCriteria)
        {
            List<TestSearchCriteria> testSearchCriterias = null;
            TestSearchCriteria testSearchCriteria = null;
            Subject subject = null;
            ////DataView SearchQuestionsOrderedDataView = null;
            ////DataTable TempDataTable = null;
            try
            {
                //SearchQuestionsOrderedDataView = dtSearchCriteria.DefaultView;
                //SearchQuestionsOrderedDataView.Sort = "ExpectedQuestions ASC";
                //TempDataTable = SearchQuestionsOrderedDataView.ToTable();
                for (int i = 0; i < dtSearchCriteria.Rows.Count; i++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria))
                        testSearchCriteria = new TestSearchCriteria();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Cat_sub_id"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(subject))
                            subject = new Subject();
                        subject.SubjectID = Convert.ToInt32(dtSearchCriteria.Rows[i]["Cat_sub_id"]);
                        testSearchCriteria.Subjects = new List<Subject>();
                        testSearchCriteria.Subjects.Add(subject);
                    }
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["TestArea"]))
                        testSearchCriteria.TestAreasID = dtSearchCriteria.Rows[i]["TestArea"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Complexity"]))
                        testSearchCriteria.Complexity = dtSearchCriteria.Rows[i]["Complexity"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Keyword"]))
                        testSearchCriteria.Keyword = dtSearchCriteria.Rows[i]["Keyword"].ToString();
                    testSearchCriteria.NoOfQuestions = Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"]);
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias))
                        testSearchCriterias = new List<TestSearchCriteria>();
                    testSearchCriterias.Add(testSearchCriteria);
                    testSearchCriteria = null;
                    subject = null;
                }
                return testSearchCriterias;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias)) testSearchCriterias = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(subject)) subject = null;
                //if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchQuestionsOrderedDataView)) SearchQuestionsOrderedDataView = null;
                //if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TempDataTable)) TempDataTable = null;
            }
        }

        /// <summary>
        /// This methods distributes the total questions to the segments
        /// according to the segment weightage 
        /// either given by user or system generated.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        private void LoadExpectedQuestions(ref DataTable dtSearchCriteria, int NoofQuestions, out bool zeroExpectedQuestion)
        {
            zeroExpectedQuestion = false;
            int SearchSegements = dtSearchCriteria.Rows.Count;
            int QuestionsAdded = 0;
            int ExpectedQuestions = 0;
            int LoopI = 0;
            for (LoopI = 0; LoopI < SearchSegements; LoopI++)
            {
                QuestionsAdded = Convert.ToInt32((Convert.ToDecimal(dtSearchCriteria.Rows[LoopI]["Weightage"])
                    / 100) * NoofQuestions);
                dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] = QuestionsAdded;
                ExpectedQuestions += QuestionsAdded;
            }
            if (ExpectedQuestions > NoofQuestions)
            {
                int QuestionsToAdd = ExpectedQuestions - NoofQuestions;
                for (LoopI = SearchSegements - 1; LoopI >= 0; LoopI--)
                {
                    if (Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd <= 0)
                        continue;
                    dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd;
                    break;
                }
            }
            else if (ExpectedQuestions < NoofQuestions)
                dtSearchCriteria.Rows[LoopI - 1]["ExpectedQuestions"] = QuestionsAdded +
                    (NoofQuestions - ExpectedQuestions);
            if (dtSearchCriteria.Select("ExpectedQuestions <= 0").Length > 0)
            {
                base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                         TestRecommendationSummary_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticTest_ExpectedQuestionZero);
                zeroExpectedQuestion = true;
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(TestRecommendationSummary_topErrorMessageLabel,
                         TestRecommendationSummary_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                    dtSearchCriteria.Rows[LoopI]["PickedQuestions"] = 0;
                    dtSearchCriteria.Rows[LoopI]["TotalRecordsinDB"] = 0;
                    dtSearchCriteria.Rows[LoopI]["QuestionsDifference"] = 0;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestRecommendationSummary_isMaximizedHiddenField.Value) &&
                TestRecommendationSummary_isMaximizedHiddenField.Value == "Y")
            {
                TestRecommendationSummary_viewTestDetailsDiv.Style["display"] = "none";
                TestRecommendationSummary_searchResultsUpSpan.Style["display"] = "block";
                TestRecommendationSummary_searchResultsDownSpan.Style["display"] = "none";
                TestRecommendationSummary_candidateDetailsDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TestRecommendationSummary_viewTestDetailsDiv.Style["display"] = "block";
                TestRecommendationSummary_searchResultsUpSpan.Style["display"] = "none";
                TestRecommendationSummary_searchResultsDownSpan.Style["display"] = "block";
                TestRecommendationSummary_candidateDetailsDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Load all javascript tags
        /// </summary>
        private void SubscribeClientSideHandlers()
        {
            TestRecommendationSummary_searchResultsUpSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                TestRecommendationSummary_candidateDetailsDiv.ClientID + "','" +
                TestRecommendationSummary_viewTestDetailsDiv.ClientID + "','" +
                TestRecommendationSummary_searchResultsUpSpan.ClientID + "','" +
                TestRecommendationSummary_searchResultsDownSpan.ClientID + "','" +
                TestRecommendationSummary_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
            TestRecommendationSummary_searchResultsDownSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                TestRecommendationSummary_candidateDetailsDiv.ClientID + "','" +
                TestRecommendationSummary_viewTestDetailsDiv.ClientID + "','" +
                TestRecommendationSummary_searchResultsUpSpan.ClientID + "','" +
                TestRecommendationSummary_searchResultsDownSpan.ClientID + "','" +
                TestRecommendationSummary_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
        }

        /// <summary>
        /// Gets the test area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetTestAreaList()
        {
            return new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA,
                  Constants.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Gets the complexity area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetComplexityList()
        {
            return new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY,
                Constants.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            TestRecommendationSummary_topSuccessMessageLabel.Text = string.Empty;
            TestRecommendationSummary_bottomSuccessMessageLabel.Text = string.Empty;
            TestRecommendationSummary_topErrorMessageLabel.Text = string.Empty;
            TestRecommendationSummary_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                    

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            SubscribeClientSideHandlers();
            BindEmptyTestSegmentGrid();
        }

      #endregion Protected Overridden Methods

    }
}