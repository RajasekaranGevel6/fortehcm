﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateManualTestWithoutAdaptive.cs
// File that represents the user Serach the Question by various Key filed. 
// and Create the test Details and questions.
// This will helps Create a Test and have to implemeted the Certification details..

#endregion

#region Directives
using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;
using System.Web.UI.HtmlControls;

#endregion

namespace Forte.HCM.UI.TestMaker
{
    public partial class CreateManualTestWithoutAdaptive : PageBase
    {
        #region Declaration
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        string sQryString = "";
        string questionID = "";
        string parentPage = string.Empty;
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        List<QuestionDetail> questionDetailSearchResultList;
        List<QuestionDetail> questionDetailTestDraftResultList;


        #endregion

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (CreateManualTestWithoutAdaptive_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = CreateManualTestWithoutAdaptive_topSearchButton.UniqueID;
                else if (CreateManualTestWithoutAdaptive_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = CreateManualTestWithoutAdaptive_bottomSaveButton.UniqueID;

                 
                if (!IsPostBack)
                {
                    CreateManualTestWithoutAdaptive_searchResultCountDiv.Visible = false;
                    CreateManualTestWithoutAdaptive_testDrftGridView_RecordCountLabelDiv.Visible = false;
                    LoadValues();                    
                }
               
                sampleDIVTextBox.Style["display"] = "none";
                CreateManualTestWithoutAdaptive_ConfirmPopupPanel.Style.Add("height", "220px");
                Master.SetPageCaption(Resources.HCMResource.CreateManualTest_Title);
                MessageLabel_Reset();
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");

                ExpandRestore();
                CreateManualTestWithoutAdaptive_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateManualTestWithoutAdaptive_pagingNavigator_PageNumberClick);

                // Create events for paging control
                CreateManualTestWithoutAdaptive_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateManualTestWithoutAdaptive_adaptivepagingNavigator_PageNumberClick);

                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.PageNumberClick += new 
                    PageNavigator.PageNumberClickEventHandler(CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigatorClick);

                CreateManualTestWithoutAdaptive_categorySubjectControl.ControlMessageThrown += new CategorySubjectControl.ControlMessageThrownDelegate(CreateManualTestWithoutAdaptive_categorySubjectControl_ControlMessageThrown);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void CreateManualTestWithoutAdaptive_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                CreateManualTestWithoutAdaptive_topErrorMessageLabel.Text = c.Message.ToString();
                CreateManualTestWithoutAdaptive_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                CreateManualTestWithoutAdaptive_topSuccessMessageLabel.Text = c.Message.ToString();
                CreateManualTestWithoutAdaptive_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
            MoveToTestDraft("");
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_okClick(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    if (CreateManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue.ToString() == "1")
                    {
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlDiv.Style["display"] = "block";
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenTextDiv.Style["display"] = "none";
                        string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                        CreateManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                        CreateManualTestWithoutAdaptive_questionDetailSummaryControl.ShowAddButton = true;
                        CreateManualTestWithoutAdaptive_questionDetailSummaryControl.LoadQuestionDetails
                            (questionCollection[0], int.Parse(questionCollection[1]));
                        CreateManualTestWithoutAdaptive_questionDetailSummaryControl.Visible = true;                        
                    }
                    else
                    {
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlDiv.Style["display"] = "none";
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenTextDiv.Style["display"] = "block";
                        string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenText.LoadQuestionDetails
                            (questionCollection[0], 0);
                    }
                    CreateManualTestWithoutAdaptive_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }
        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Add  Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = CreateManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value;
                CreateManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                CreateManualTestWithoutAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField CreateManualTestWithoutAdaptive_hidddenValue = (HiddenField)e.Row.FindControl("CreateManualTestWithoutAdaptive_hidddenValue");
                    string questionID = CreateManualTestWithoutAdaptive_hidddenValue.Value.Split(':')[0].ToString();
                    ImageButton alertImageButton = (ImageButton)e.Row.FindControl("CreateManualTestWithoutAdaptive_searchQuestionAlertImageButton");
                    alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");

                    Image img = (Image)e.Row.FindControl("CreateManualTestWithoutAdaptive_searchQuestionSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateManualTestWithoutAdaptive_searchQuestionGridView.ClientID + "');");
                    img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + CreateManualTestWithoutAdaptive_hidddenValue.Value + "','" + CreateManualTestWithoutAdaptive_selectMultipleImage.ClientID + "');");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);


                    Label CreateManualTestWithoutAdaptive_questionLabel =
                        (Label)e.Row.FindControl("CreateManualTestWithoutAdaptive_questionLabel");

                    HoverMenuExtender CreateManualTestWithoutAdaptive_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("CreateManualTestWithoutAdaptive_hoverMenuExtender");
                    CreateManualTestWithoutAdaptive_hoverMenuExtender.TargetControlID = CreateManualTestWithoutAdaptive_questionLabel.ID;

                    // Label CreateManualTestWithoutAdaptive_questionDetailPreviewControlQuestionLabel =
                    //   (Label)e.Row.FindControl("CreateManualTestWithoutAdaptive_questionDetailPreviewControlQuestionLabel");
                    //RadioButtonList CreateManualTestWithoutAdaptive_questionPreviewControlAnswerRadioButtonList =
                    //    (RadioButtonList)e.Row.FindControl("CreateManualTestWithoutAdaptive_questionPreviewControlAnswerRadioButtonList");


                    //PlaceHolder CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder");
                    //CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder.Controls.Add
                    //                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID), true));


                    HiddenField CreateManualTestWithoutAdaptive_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                           "CreateManualTestWithoutAdaptive_correctAnswerHiddenField");

                   // HtmlGenericControl CreateManualTestWithoutAdaptive_answerChoicesDiv = (HtmlGenericControl)e.Row.FindControl("CreateManualTestWithoutAdaptive_answerChoicesDiv");
                    //HtmlGenericControl CreateManualTestWithoutAdaptive_answerDiv = (HtmlGenericControl)e.Row.FindControl("CreateManualTestWithoutAdaptive_answerDiv");
                    PlaceHolder CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder");
                    Literal CreateManualTestWithoutAdaptive_questionAnswer = (Literal)e.Row.FindControl("CreateManualTestWithoutAdaptive_questionAnswer");


                    if (CreateManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue.ToString() == "1")
                    {
                        //CreateManualTestWithoutAdaptive_answerChoicesDiv.Style["display"] = "block";
                       // CreateManualTestWithoutAdaptive_answerDiv.Style["display"] = "none";
                        CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = true;
                        CreateManualTestWithoutAdaptive_questionAnswer.Visible = false;
                        CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder.Controls.Add
                                            (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID), true));
                    }
                    else
                    {
                        QuestionDetail questionDetail = new BL.QuestionBLManager().GetQuestion
                            (questionID);
                        //CreateManualTestWithoutAdaptive_answerChoicesDiv.Style["display"] = "none";
                        //CreateManualTestWithoutAdaptive_answerDiv.Style["display"] = "block";
                        CreateManualTestWithoutAdaptive_questionAnswer.Visible = true;
                        CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = false;
                        CreateManualTestWithoutAdaptive_questionAnswer.Text = questionDetail.AnswerReference;

                    }

                    List<AnswerChoice> answerChoice = new List<AnswerChoice>();
                    answerChoice = new BL.QuestionBLManager().GetQuestionOptions(questionID);

                    // CreateManualTestWithoutAdaptive_questionDetailPreviewControlQuestionLabel.Text = CreateManualTestWithoutAdaptive_questionLabel.Text;


                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image img = (Image)e.Row.FindControl("CreateManualTestWithoutAdaptive_testDrftRemoveImage");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                    Label CreateManualTestWithoutAdaptive_questionLabel =
                         (Label)e.Row.FindControl("CreateManualTestWithoutAdaptive_draftQuestionLabel");

                    HoverMenuExtender CreateManualTestWithoutAdaptive_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("CreateManualTestWithoutAdaptive_draftHoverMenuExtender1");
                    CreateManualTestWithoutAdaptive_hoverMenuExtender.TargetControlID = CreateManualTestWithoutAdaptive_questionLabel.ID;

                    //RadioButtonList CreateManualTestWithoutAdaptive_questionPreviewControlAnswerRadioButtonList =
                    //     (RadioButtonList)e.Row.FindControl("QuestionDetailPreviewControl_draftAnswerRadioButtonList");
                    HiddenField CreateManualTestWithoutAdaptive_questionKeyHiddenField = (HiddenField)e.Row.FindControl(
                              "CreateManualTestWithoutAdaptive_questionKeyHiddenField");

                    HiddenField CreateManualTestWithoutAdaptive_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                             "CreateManualTestWithoutAdaptive_correctAnswerHiddenField");
                    string questionID = CreateManualTestWithoutAdaptive_questionKeyHiddenField.Value.ToString().Split(':')[0];
        
                    Literal CreateManualTestWithoutAdaptive_questionAnswer = (Literal)e.Row.FindControl("CreateManualTestWithoutAdaptive_questionAnswer");
                    Label CreateManualTestWithoutAdaptive_AnswerLabel1 = (Label)e.Row.FindControl("CreateManualTestWithoutAdaptive_AnswerLabel1");

                    PlaceHolder CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder");

                    if (CreateManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue.ToString() == "1")
                    {
                        CreateManualTestWithoutAdaptive_AnswerLabel1.Visible = false;
                        CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = true;
                        CreateManualTestWithoutAdaptive_questionAnswer.Visible = false;
                        CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder.Controls.Add
                                            (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID), true));
                    }
                    else
                    {
                         QuestionDetail questionDetail=  new BL.QuestionBLManager().GetQuestion(questionID);                        
                         CreateManualTestWithoutAdaptive_AnswerLabel1.Visible = true;
                        CreateManualTestWithoutAdaptive_questionAnswer.Visible = true;
                        CreateManualTestWithoutAdaptive_answerChoicesPlaceHolder.Visible = false;
                        CreateManualTestWithoutAdaptive_questionAnswer.Text = questionDetail.AnswerReference;

                    }

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a page in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewPageEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_testDrftGridView_Paging(object sender, GridViewPageEventArgs e)
        {
            //CreateManualTestWithoutAdaptive_testDrftGridView.PageIndex = e.NewPageIndex;
            //List<QuestionDetail> questionDetailTestDraftResultList = new List<QuestionDetail>();
            //questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            //CreateManualTestWithoutAdaptive_testDrftGridView.DataSource = questionDetailTestDraftResultList;
            //CreateManualTestWithoutAdaptive_testDrftGridView.DataBind();  
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    if (CreateManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue.ToString() == "1")
                    {
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlDiv.Style["display"] = "block";
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenTextDiv.Style["display"] = "none";
                    }
                    else
                    {
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlDiv.Style["display"] = "none";
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenTextDiv.Style["display"] = "block";
                        string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                        CreateManualTestWithoutAdaptive_QuestionDetailPreviewControlOpenText.LoadQuestionDetails
                            (questionCollection[0],0);
                    }
                    CreateManualTestWithoutAdaptive_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Message = string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message, e.CommandArgument.ToString().Split(':')[0]);
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                CreateManualTestWithoutAdaptive_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                sampleDIVTextBox.Text = "";
                //sampleDIVTextBox.Visible = false;
                sampleDIVTextBox.Style["display"] = "none";
                AddSearchQuestionToTestDraft();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in CreateManualTestWithoutAdaptive_testDrftGridView.Rows)
                    {
                        CheckBox CreateManualTestWithoutAdaptive_testDrftCheckbox = (CheckBox)row.FindControl("CreateManualTestWithoutAdaptive_testDrftCheckbox");
                        HiddenField CreateManualTestWithoutAdaptive_questionKeyHiddenField = (HiddenField)row.FindControl("CreateManualTestWithoutAdaptive_questionKeyHiddenField");
                        if (CreateManualTestWithoutAdaptive_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + CreateManualTestWithoutAdaptive_questionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }  
                    }
                    // hiddenValue.Value = questionID.TrimEnd(',');

                }
                if (questionSelected)
                {
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateManualTestWithoutAdaptive_ConfirmPopupExtender.Show();
                    // MoveToTestDraft("Delete");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                                       CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_TestDraft_Result);

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualTestWithoutAdaptive_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CreateManualTestWithoutAdaptive_simpleLinkButton.Text.ToLower() == "simple")
                {
                    CreateManualTestWithoutAdaptive_simpleLinkButton.Text = "Advanced";
                    CreateManualTestWithoutAdaptive_simpleSearchDiv.Visible = true;
                    CreateManualTestWithoutAdaptive_advanceSearchDiv.Visible = false;
                }
                else
                {
                    CreateManualTestWithoutAdaptive_simpleLinkButton.Text = "Simple";
                    CreateManualTestWithoutAdaptive_simpleSearchDiv.Visible = false;
                    CreateManualTestWithoutAdaptive_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualTestWithoutAdaptive_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                ViewState["PAGENUMBER"] = e.PageNumber;
                LoadQuestion(e.PageNumber, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigatorClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.Reset();

               List<QuestionDetail> questionDetailTestDraft = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
               var records = from record in questionDetailTestDraft select record;
                var pageNumber =e.PageNumber;
                var pageSize = base.GridPageSize;

                records = records.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                CreateManualTestWithoutAdaptive_testDrftGridView.DataSource = records;
                CreateManualTestWithoutAdaptive_testDrftGridView.DataBind();
             


                //LoadQuestion(e.PageNumber, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateManualTestWithoutAdaptive_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if feature usage limit exceeds for creating test.
                if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                {
                    base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                        CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, 
                        "You have reached the maximum limit based your subscription plan. Cannot create more tests");
                    return;
                }

                if (IsValidData())
                {
                    questionDetailTestDraftResultList = new List<QuestionDetail>();
                    questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    List<decimal> creditEarned = new List<decimal>();
                    List<decimal> timeTaken = new List<decimal>();
                    string testKey = "";
                    for (int i = 0; i < questionDetailTestDraftResultList.Count; i++)
                    {
                        creditEarned.Add(questionDetailTestDraftResultList[i].CreditsEarned);
                        timeTaken.Add(questionDetailTestDraftResultList[i].AverageTimeTaken);

                    }
                    int questionCount = questionDetailTestDraftResultList.Count;

                    TestDetail testDetail = new TestDetail();
                    testDetail = CreateManualTestWithoutAdaptive_testDetailsUserControl.TestDetailDataSource;

                    CertificationDetail certificationDetail = new CertificationDetail();
                    testDetail.Questions = questionDetailTestDraftResultList;
                    testDetail.IsActive = true;
                    // testDetail.IsCertification = false;
                    // testDetail.CertificationDetail = certificationDetail;
                    testDetail.CertificationId = 0;
                    testDetail.CreatedBy = userID;
                    testDetail.IsDeleted = false;
                    testDetail.ModifiedBy = userID;
                    testDetail.NoOfQuestions = questionCount;
                    testDetail.SystemRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionCount;
                    testDetail.TestAuthorID = userID;
                    testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
                    testDetail.TestCreationDate = DateTime.Now;
                    testDetail.TestStatus = TestStatus.Active;
                    testDetail.TestType = TestType.Genenal;
                    testDetail.TestMode = "MANUAL";
                    testDetail.TestKey = testKey;
                    testDetail.QuestionType = (QuestionType)Convert.ToInt32(CreateManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue);
                    testKey = new TestBLManager().SaveTest(testDetail, userID,
                        new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                        questionDetailTestDraftResultList).AttributeID.ToString());

                    // Send a mail to the position profile creator.
                    try
                    {
                        if (testDetail.PositionProfileID != 0)
                        {
                            new EmailHandler().SendMail(EntityType.TestCreatedForPositionProfile, testDetail.TestKey);
                        }
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }

                    if (Support.Utility.IsNullOrEmpty(Request.QueryString.Get("parentpage")))
                        parentPage = Constants.ParentPage.SEARCH_TEST;
                    else
                        parentPage = Request.QueryString.Get("parentpage");

                    Response.Redirect(Constants.TestMakerConstants.EDIT_TEST + "?m=1&s=1&mode=new&parentpage=" +
                       parentPage + "&testkey=" + testKey, false);
                }
                else
                {
                    MoveToTestDraft("");
                    if (CreateManualTestWithoutAdaptive_questionDiv.Visible == true)
                        LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()),
                            ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method that will be called when the With Adaptive button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_adptiveTestLinkButton_Click(object sender, EventArgs e)
        {
            if (!Support.Utility.IsNullOrEmpty(Request.QueryString.Get("parentpage")))
                parentPage = "&parentpage=" + Request.QueryString.Get("parentpage");
            Response.Redirect(Constants.TestMakerConstants.CREATE_TEST_WA + "?m=1&s=1" + parentPage, false);
        }

        #endregion

        #region Private  Methods

        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            bool questionSelected = false;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {
                foreach (GridViewRow row in CreateManualTestWithoutAdaptive_searchQuestionGridView.Rows)
                {
                    CheckBox CreateManualTestWithoutAdaptive_searchQuestionCheckbox = (CheckBox)row.FindControl("CreateManualTestWithoutAdaptive_searchQuestionCheckbox");
                    HiddenField CreateManualTestWithoutAdaptive_questionKeyHiddenField = (HiddenField)row.FindControl("CreateManualTestWithoutAdaptive_questionKeyHiddenField");
                    if (CreateManualTestWithoutAdaptive_searchQuestionCheckbox.Checked)
                    {
                        questionID = questionID + CreateManualTestWithoutAdaptive_questionKeyHiddenField.Value + ",";
                        questionSelected = true;
                    }
                }
                hiddenValue.Value = questionID.TrimEnd(',');

            }
            if (questionSelected)
            {
                MoveToTestDraft("Add");
            }
            else
            {
                MoveToTestDraft("");
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_Search_Result);
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>                                         
        private void ExpandRestore()
        {
            if (CreateManualTestWithoutAdaptive_restoreHiddenField.Value == "Y")
            {
                CreateManualTestWithoutAdaptive_searchCriteriasDiv.Style["display"] = "none";
                CreateManualTestWithoutAdaptive_searchResultsUpSpan.Style["display"] = "block";
                CreateManualTestWithoutAdaptive_searchResultsDownSpan.Style["display"] = "none";
                CreateManualTestWithoutAdaptive_questionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CreateManualTestWithoutAdaptive_searchCriteriasDiv.Style["display"] = "block";
                CreateManualTestWithoutAdaptive_searchResultsUpSpan.Style["display"] = "none";
                CreateManualTestWithoutAdaptive_searchResultsDownSpan.Style["display"] = "block";
                CreateManualTestWithoutAdaptive_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            CreateManualTestWithoutAdaptive_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                CreateManualTestWithoutAdaptive_questionDiv.ClientID + "','" +
                CreateManualTestWithoutAdaptive_searchCriteriasDiv.ClientID + "','" +
                CreateManualTestWithoutAdaptive_searchResultsUpSpan.ClientID + "','" +
                CreateManualTestWithoutAdaptive_searchResultsDownSpan.ClientID + "','" +
                CreateManualTestWithoutAdaptive_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Reset the Messeags.
        /// </summary>
        private void MessageLabel_Reset()
        {
            CreateManualTestWithoutAdaptive_topErrorMessageLabel.Text = string.Empty;
            CreateManualTestWithoutAdaptive_bottomErrorMessageLabel.Text = string.Empty;
            CreateManualTestWithoutAdaptive_topSuccessMessageLabel.Text = string.Empty;
            CreateManualTestWithoutAdaptive_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            QuestionType questionType = (QuestionType)Convert.ToInt32(CreateManualTestWithoutAdaptive_questionTypeDropDownList.SelectedValue);

            if (CreateManualTestWithoutAdaptive_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    CreateManualTestWithoutAdaptive_categoryTextBox.Text.Trim() == "" ?
                                    null : CreateManualTestWithoutAdaptive_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    CreateManualTestWithoutAdaptive_subjectTextBox.Text.Trim() == "" ?
                                    null : CreateManualTestWithoutAdaptive_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateManualTestWithoutAdaptive_keywordsTextBox.Text.Trim() == "" ?
                                    null : CreateManualTestWithoutAdaptive_keywordsTextBox.Text.Trim();
                questionDetailSearchCriteria.Author = base.userID;
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CreateManualTestWithoutAdaptive_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in CreateManualTestWithoutAdaptive_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in CreateManualTestWithoutAdaptive_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    CreateManualTestWithoutAdaptive_questionIDTextBox.Text.Trim() == "" ?
                                    null : CreateManualTestWithoutAdaptive_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateManualTestWithoutAdaptive_keywordTextBox.Text.Trim() == "" ?
                                    null : CreateManualTestWithoutAdaptive_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.AuthorName =
                                    CreateManualTestWithoutAdaptive_authorTextBox.Text.Trim() == "" ?
                                    null : CreateManualTestWithoutAdaptive_authorIdHiddenField.Value;

                questionDetailSearchCriteria.ActiveFlag = null;
                questionDetailSearchCriteria.CreditsEarned =
                                    CreateManualTestWithoutAdaptive_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(CreateManualTestWithoutAdaptive_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.ClientRequest = CreateManualTestWithoutAdaptive_positionProfileKeywordTextBox.Text.Trim() == "" ?
                   null : CreateManualTestWithoutAdaptive_positionProfileKeywordTextBox.Text.Trim();

                
                    questionDetailSearchCriteria.Author = base.userID;
                   

            }


            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail = new BL.TestBLManager().GetSearchQuestions(questionType, questionDetailSearchCriteria, base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);
            CreateManualTestWithoutAdaptive_searchResultCountDiv.Visible = true;
            CreateManualTestWithoutAdaptive_searchResultCountLabel.Text = totalRecords.ToString();            
            if (questionDetail == null || questionDetail.Count == 0)
            {
                CreateManualTestWithoutAdaptive_searchQuestionGridView.DataSource = null;
                CreateManualTestWithoutAdaptive_searchQuestionGridView.DataBind();
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                 CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                CreateManualTestWithoutAdaptive_bottomPagingNavigator.TotalRecords = 0;
                CreateManualTestWithoutAdaptive_questionDiv.Visible = false;
               // CreateManualTestWithoutAdaptive_searchResultCountLabel.Text = totalRecords.ToString();
            }
            else
            {
                CreateManualTestWithoutAdaptive_searchQuestionGridView.DataSource = questionDetail;
                CreateManualTestWithoutAdaptive_searchQuestionGridView.DataBind();
                CreateManualTestWithoutAdaptive_bottomPagingNavigator.PageSize = base.GridPageSize;
                CreateManualTestWithoutAdaptive_bottomPagingNavigator.TotalRecords = totalRecords;
                CreateManualTestWithoutAdaptive_questionDiv.Visible = true;
               // CreateManualTestWithoutAdaptive_searchResultCountLabel.Text = totalRecords.ToString();
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
            // return questionDetail;
        }
        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            CreateManualTestWithoutAdaptive_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            CreateManualTestWithoutAdaptive_complexityCheckBoxList.DataTextField = "AttributeName";
            CreateManualTestWithoutAdaptive_complexityCheckBoxList.DataValueField = "AttributeID";
            CreateManualTestWithoutAdaptive_complexityCheckBoxList.DataBind();
        }
        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            CreateManualTestWithoutAdaptive_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
               //new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            CreateManualTestWithoutAdaptive_testAreaCheckBoxList.DataTextField = "AttributeName";
            CreateManualTestWithoutAdaptive_testAreaCheckBoxList.DataValueField = "AttributeID";
            CreateManualTestWithoutAdaptive_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < CreateManualTestWithoutAdaptive_testAreaCheckBoxList.Items.Count; i++)
            {

                if (CreateManualTestWithoutAdaptive_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(CreateManualTestWithoutAdaptive_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }
        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < CreateManualTestWithoutAdaptive_complexityCheckBoxList.Items.Count; i++)
            {
                if (CreateManualTestWithoutAdaptive_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(CreateManualTestWithoutAdaptive_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }
        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in CreateManualTestWithoutAdaptive_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateManualTestWithoutAdaptive_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateManualTestWithoutAdaptive_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in CreateManualTestWithoutAdaptive_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateManualTestWithoutAdaptive_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateManualTestWithoutAdaptive_adaptiveCheckbox")).Checked = false;
                }
            }

        }
        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {


            questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }


            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
            {
                questionDetailSearchResultList = new List<QuestionDetail>();
                questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');


                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                        string[] idList = ids.Split(':');
                        string id = idList[0];
                        string relationId = idList[1];

                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    base.ShowMessage(CreateManualTestWithoutAdaptive_topSuccessMessageLabel,
                                        CreateManualTestWithoutAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(CreateManualTestWithoutAdaptive_topSuccessMessageLabel,
                                    CreateManualTestWithoutAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            base.ShowMessage(CreateManualTestWithoutAdaptive_topSuccessMessageLabel,
                                       CreateManualTestWithoutAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                        }

                    }
                }
            }
            if (alertFlag)
            {
                string alertMessage = string.Format(Resources.HCMResource.CreateManualTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                {
                    alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(','));
                }
                CreateManualTestWithoutAdaptive_ConfirmPopupPanel.Style.Add("height", "270px");
                CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Title = "Warning";
                CreateManualTestWithoutAdaptive_ConfirmPopupExtenderControl.Message = alertMessage;
                CreateManualTestWithoutAdaptive_ConfirmPopupExtender.Show();
            }
            hiddenValue.Value = null;

            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            if (questionDetailTestDraftResultList != null && questionDetailTestDraftResultList.Count > 0)
            {
                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.PageSize = base.GridPageSize;
                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.TotalRecords = questionDetailTestDraftResultList.Count;
                CreateManualTestWithoutAdaptive_testDrftGridView_RecordCountLabelDiv.Visible = true;
                CreateManualTestWithoutAdaptive_testDrftGridView_RecordCountLabel.Text = questionDetailTestDraftResultList.Count.ToString();
                var records = from record in questionDetailTestDraftResultList select record;
                var pageNumber = 1;
                var pageSize = base.GridPageSize;

                records = records.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                CreateManualTestWithoutAdaptive_testDrftGridView.DataSource = records;
                CreateManualTestWithoutAdaptive_testDrftGridView.DataBind();
            }
            else
            {

                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.TotalRecords = 0;
                CreateManualTestWithoutAdaptive_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                CreateManualTestWithoutAdaptive_testDrftGridView.DataBind();
            }
           

            if (questionDetailSearchResultList != null)
            {
                CreateManualTestWithoutAdaptive_searchQuestionGridView.DataSource = questionDetailSearchResultList;
                CreateManualTestWithoutAdaptive_questionDiv.Visible = true;
            }
            else
            {
                CreateManualTestWithoutAdaptive_searchQuestionGridView.DataSource = null;
                CreateManualTestWithoutAdaptive_questionDiv.Visible = false;
            }


            CreateManualTestWithoutAdaptive_searchQuestionGridView.DataBind();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                if (action != "")
                {
                    questionDetailTestDraftResultList = new List<QuestionDetail>();
                    questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    List<decimal> timeTaken = new List<decimal>();

                    for (int i = 0; i < questionDetailTestDraftResultList.Count; i++)
                    {
                        timeTaken.Add(questionDetailTestDraftResultList[i].AverageTimeTaken);
                    }

                    if (questionDetailTestDraftResultList.Count > 0)
                    {
                        CreateManualTestWithoutAdaptive_testDetailsUserControl.SysRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionDetailTestDraftResultList.Count;
                    }
                    else
                    {
                        CreateManualTestWithoutAdaptive_testDetailsUserControl.SysRecommendedTime = 0;
                    }
                }
                TestDraftSummary testDraftSummary = new TestDraftSummary();
                testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList);
                CreateManualTestWithoutAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;


            }
            UncheckCheckBox();
        }

        #endregion

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;


            TestDetail testDetail = new TestDetail();
            testDetail = CreateManualTestWithoutAdaptive_testDetailsUserControl.TestDetailDataSource;
            int recommendedTime = testDetail.RecommendedCompletionTime;
            CreateManualTestWithoutAdaptive_topErrorMessageLabel.Text = string.Empty;
            CreateManualTestWithoutAdaptive_bottomErrorMessageLabel.Text = string.Empty;

            questionDetailTestDraftResultList = new List<QuestionDetail>();
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;

            }
            else if (questionDetailTestDraftResultList.Count == 0)
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
            }


            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Name_Empty);

                isValidData = false;
            }

            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Description_Empty);

                isValidData = false;
            }

            if (recommendedTime == -1)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
               CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
               Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_Empty);
                isValidData = false;
            }
            else if (recommendedTime == -2)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
               CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended"));
                isValidData = false;
            }
            else if (recommendedTime == 0)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                isValidData = false;

            }
            if (isQuestionTabActive)
                CreateManualTestWithoutAdaptive_mainTabContainer.ActiveTab = CreateManualTestWithoutAdaptive_questionsTabPanel;
            else
                CreateManualTestWithoutAdaptive_mainTabContainer.ActiveTab = CreateManualTestWithoutAdaptive_testdetailsTabPanel;

            if (!(bool)testDetail.IsCertification)
                return isValidData;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateDetailsEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateID <= 0)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateFormatEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.DaysElapseBetweenRetakes < 0)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyDaysRetakeTest);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.PermissibleRetakes <= 0)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyRetakes);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MinimumTotalScoreRequired <= 0)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_MinimumQualifacationEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateValidity == "0")
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                     Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateValidity);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -2)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                        CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                        string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended certification completion"));
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -1)
            {
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                    CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateAutomaticTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                isValidData = false;
            }
            if ((testDetail.RecommendedCompletionTime > 0) && (testDetail.CertificationDetail.MaximumTimePermissible > 0))
            {
                if (testDetail.RecommendedCompletionTime < testDetail.CertificationDetail.MaximumTimePermissible)
                {
                    base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                        CreateManualTestWithoutAdaptive_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificationTest_CertificationTimeExceeds);
                    isValidData = false;
                }
            }
            CreateManualTestWithoutAdaptive_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
            CreateManualTestWithoutAdaptive_manualImageButton.Visible = false;
            CreateManualTestWithoutAdaptive_adaptiveImageButton.Visible = false;
            CreateManualTestWithoutAdaptive_bothImageButton.Visible = false;
            CreateManualTestWithoutAdaptive_simpleSearchDiv.Visible = true;
            CreateManualTestWithoutAdaptive_advanceSearchDiv.Visible = false;
            CreateManualTestWithoutAdaptive_questionDiv.Visible = false;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                ViewState["SORTDIRECTIONKEY"] = "A";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                ViewState["SEARCHRESULTGRID"] = null;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                sQryString = Request.QueryString["mode"].ToUpper();

            CreateManualTestWithoutAdaptive_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + CreateManualTestWithoutAdaptive_dummyAuthorID.ClientID + "','"
                + CreateManualTestWithoutAdaptive_authorIdHiddenField.ClientID + "','"
                + CreateManualTestWithoutAdaptive_authorTextBox.ClientID + "','QA')");

            CreateManualTestWithoutAdaptive_positionProfileImageButton.Attributes.Add("onclick",
                    "return ShowClientRequestForManual('" + CreateManualTestWithoutAdaptive_positionProfileTextBox.ClientID + "','" +
                    CreateManualTestWithoutAdaptive_positionProfileKeywordTextBox.ClientID + "','" +
                     CreateManualTestWithoutAdaptive_positionProfileHiddenField.ClientID + "')");
            
            List<QuestionDetail> questionDetail= ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            if (questionDetail != null&& questionDetail.Count>0)
            {
                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.PageSize = base.GridPageSize;
                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.TotalRecords = questionDetail.Count;

                var records = from emp in questionDetail

                              select emp;


                var pageNumber = 1;


                var pageSize = base.GridPageSize;


                records = records.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                CreateManualTestWithoutAdaptive_testDrftGridView.DataSource = records;
                CreateManualTestWithoutAdaptive_testDrftGridView.DataBind();
            }
            else
            {
                CreateManualTestWithoutAdaptive_testDrftGridView_PagingNavigator.TotalRecords =0;
                CreateManualTestWithoutAdaptive_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                CreateManualTestWithoutAdaptive_testDrftGridView.DataBind();
            }
        }

        #endregion Protected Overridden Methods

        #region Not Use Now (Adaptive Question)

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualTestWithoutAdaptive_adaptivepagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {

        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_searchQuestionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            // may be used in future.
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CreateManualTestWithoutAdaptive_testDrftGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            // may be used in future.
        }
        protected void CreateManualTestWithoutAdaptive_adaptiveGridView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void CreateManualTestWithoutAdaptive_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view")
            {
                CreateManualTestWithoutAdaptive_questionModalPopupExtender.Show();
            }
            if (e.CommandName == "AdaptiveQuestion")
            {
                CreateManualTestWithoutAdaptive_adaptiveQuestionPreviewModalPopupExtender.Show();

                ViewState["RowIndex"] = (e.CommandArgument.ToString());
            }
        }
        protected void CreateManualTestWithoutAdaptive_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image alertImage = (Image)e.Row.FindControl("CreateManualTestWithoutAdaptive_adaptiveAlertImageButton");
                    alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion();");

                    Image img = (Image)e.Row.FindControl("CreateManualTestWithoutAdaptive_adaptiveSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateManualTestWithoutAdaptive_adaptiveGridView.ClientID + "');");
                    //(Image)e.Row.FindControl("CreateManualTestWithoutAdaptive_adaptiveSelectImage");

                    //img.Attributes.Add("onClick", "javascript:return AddRow(" + e.Row.RowIndex.ToString() + "," + 1 + ");");
                    // if (dtSearchQuestions.Rows[e.Row.RowIndex][0].ToString().Equals(""))
                    {
                        img.Visible = false;
                        ImageButton img1 = (ImageButton)e.Row.FindControl("CreateManualTestWithoutAdaptive_adaptiveAlertImageButton");
                        img1.Visible = false;
                        //Image img2 = (Image)e.Row.FindControl("CreateManualTestWithoutAdaptive_adaptiveViewImage");
                        //img2.Visible = false;
                        CheckBox chk = (CheckBox)e.Row.FindControl("CreateManualTestWithoutAdaptive_adaptiveCheckbox");
                        chk.Visible = false;
                    }

                    ImageButton imageButton = (ImageButton)e.Row.FindControl("CreateManualTestWithoutAdaptive_adaptiveQuestionDetailsImage");

                    imageButton.CommandArgument = e.Row.RowIndex.ToString();
                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                    Label CreateManualTestWithoutAdaptive_questionLabel =
                         (Label)e.Row.FindControl("CreateManualTestWithoutAdaptive_questionLabel");

                    HoverMenuExtender CreateManualTestWithoutAdaptive_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("CreateManualTestWithoutAdaptive_hoverMenuExtender1");
                    CreateManualTestWithoutAdaptive_hoverMenuExtender.TargetControlID = CreateManualTestWithoutAdaptive_questionLabel.ID;

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTestWithoutAdaptive_topErrorMessageLabel,
                   CreateManualTestWithoutAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }
        protected void CreateManulTest_CloseButton_Click(object sender, EventArgs e)
        {
            MoveToTestDraft("");
            CreateManualTestWithoutAdaptive_questionModalPopupExtender.Show();
        }
        protected void CreateManualTestWithoutAdaptive_questionDetailTopAddButton_Click(object sender, EventArgs e)
        {
            //CreateManualTestWithoutAdaptive_adaptiveQuestionPreviewModalPopupExtender.Hide();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);

            //ViewState["RowIndex"] = 0;

        }
        protected void QuestionDetail_topCancelButton_Click(object sender, EventArgs e)
        {
            CreateManualTestWithoutAdaptive_adaptiveQuestionPreviewModalPopupExtender.Hide();
        }
        #endregion
    }
}
