﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchTestRecommendation.aspx.cs"
    Inherits="Forte.HCM.UI.TestMaker.SearchTestRecommendation" MasterPageFile="~/MasterPages/SiteAdminMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<asp:Content ID="SearchTestRecommendation_bodyContent" runat="server" ContentPlaceHolderID="SiteAdminMaster_body">
    <script type="text/javascript" language="javascript">

        // Hide all the expanded panel.
        function CollapseAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= SearchTestRecommendation_testRecommendationsGridView.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("SearchTestRecommendation_testRecommendationsGridView") != "-1")
                    {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }

            return false;
        }


    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="SearchTestRecommendation_headerLiteral" runat="server" Text="Search Test Recommendation"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="SearchTestRecommendation_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchTestRecommendation_resetLinkButton_Click"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="SearchTestRecommendation_topCancelLinkButton"
                                            runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchTestRecommendation_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchTestRecommendation_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchTestRecommendation_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="SearchTestRecommendation_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="SearchTestRecommendation_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <asp:UpdatePanel runat="server" ID="SearchTestRecommendation_criteriaUpdatePanel">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td style="width: 6%">
                                                                            <asp:Label ID="SearchTestRecommendation_statusLabel" runat="server" Text="Status"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 11%">
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:DropDownList ID="SearchTestRecommendation_statusDropDownList" runat="server"
                                                                                    Width="80px">
                                                                                    <asp:ListItem Text="All" Value="" Selected="True"></asp:ListItem>
                                                                                    <asp:ListItem Text="Active" Value="Y"></asp:ListItem>
                                                                                    <asp:ListItem Text="Inactive" Value="N"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div style="float: left; padding-top: 2px">
                                                                                <asp:ImageButton ID="SearchTestRecommendation_statusHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the status here"
                                                                                    Width="16px" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                        </td>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="SearchTestRecommendation_testNameLabel" runat="server" Text="Test Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <asp:TextBox ID="SearchTestRecommendation_testNameTextBox" MaxLength="50" runat="server"
                                                                                Width="160px"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                        </td>
                                                                        <td style="width: 11%">
                                                                            <asp:Label ID="SearchTestRecommendation_testDescriptionLabel" runat="server" Text="Test Description"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <asp:TextBox ID="SearchTestRecommendation_testDescriptionTextBox" MaxLength="500"
                                                                                runat="server" Width="160px"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                        </td>
                                                                        <td style="width: 6%">
                                                                            <asp:Label ID="SearchTestRecommendation_skillLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Skill"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <asp:TextBox ID="SearchTestRecommendation_skillTextBox" MaxLength="100" runat="server"
                                                                                Width="160px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 4px">
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="SearchTestRecommendation_searchButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Search" OnClick="SearchTestRecommendation_searchButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 6px">
                        </td>
                    </tr>
                    <tr id="SearchTestRecommendation_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="SearchTestRecommendation_testRecommendationsLiteral" runat="server"
                                            Text="Test Recommendations"></asp:Literal>
                                        &nbsp;<asp:Label ID="SearchTestRecommendation_testRecommendationsSortHelpLabel" runat="server"
                                            SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="SearchTestRecommendation_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="SearchTestRecommendation_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="SearchTestRecommendation_searchResultsDownSpan" style="display: block;"
                                            runat="server">
                                            <asp:Image ID="SearchTestRecommendation_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="SearchTestRecommendation_restoreHiddenField" runat="server" />
                                        <asp:HiddenField ID="SearchTestRecommendation_isMaximizedHiddenField" runat="server"
                                            Value="N" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <asp:UpdatePanel ID="SearchTestRecommendation_testRecommendationsGridViewUpdatePanel"
                                runat="server">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="SearchTestRecommendation_testRecommendationsGridViewDiv">
                                                    <asp:GridView ID="SearchTestRecommendation_testRecommendationsGridView" runat="server"
                                                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="SearchTestRecommendation_testRecommendationsGridView_Sorting"
                                                        OnRowCommand="SearchTestRecommendation_testRecommendationsGridView_RowCommand"
                                                        OnRowCreated="SearchTestRecommendation_testRecommendationsGridView_RowCreated"
                                                        OnRowDataBound="SearchTestRecommendation_testRecommendationsGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="9%" HeaderStyle-Wrap="true">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="SearchTestRecommendation_testRecommendationsGridViewDiv_viewImageButton" 
                                                                        runat="server" SkinID="sknViewImageButton" ToolTip="View Test"
                                                                        CommandName="ViewTest" CommandArgument="<%# Container.DataItemIndex %>" />
                                                                    <asp:ImageButton ID="SearchTestRecommendation_testRecommendationsGridViewDiv_editImageButton"
                                                                        runat="server" SkinID="sknEditImageButton" ToolTip="Edit Test Recommendation"
                                                                        CommandName="EditTest" CommandArgument="<%# Container.DataItemIndex %>" Visible='<%# Convert.ToBoolean(Eval("IsTestUsed")) == false %>'/>
                                                                    <asp:ImageButton ID="SearchTestRecommendation_testRecommendationsGridViewDiv_activateTestImageButton"
                                                                        runat="server" SkinID="sknActiveImageButton" ToolTip="Activate Test" Visible='<%# Convert.ToBoolean(Eval("IsActive")) == false %>'
                                                                        CommandName="ActivateTest" CommandArgument="<%# Container.DataItemIndex %>" />
                                                                    <asp:ImageButton ID="SearchTestRecommendation_testRecommendationsGridViewDiv_deactivateTestImageButton"
                                                                        runat="server" SkinID="sknInactiveImageButton" ToolTip="Deactivate Test" Visible='<%# Convert.ToBoolean(Eval("IsActive")) == true %>'
                                                                        CommandName="DeactivateTest" CommandArgument="<%# Container.DataItemIndex %>" />
                                                                    <asp:ImageButton ID="SearchTestRecommendation_testRecommendationsGridViewDiv_copyTestImageButton"
                                                                        runat="server" SkinID="sknCopyImageButton" ToolTip="Copy Test"
                                                                        CommandName="CopyTest" CommandArgument="<%# Container.DataItemIndex %>" />
                                                                    <asp:ImageButton ID="SearchTestRecommendation_testRecommendationsGridViewDiv_testSummaryImageButton"
                                                                        runat="server" SkinID="sknAdditionalDetailImageButton" ToolTip="Test Summary"
                                                                        CommandName="TestSummary" CommandArgument="<%# Container.DataItemIndex %>" />
                                                                    <asp:HiddenField ID="SearchTestRecommendation_testRecommendationsGridViewDiv_testRecommendationIDHiddenField"
                                                                        Value='<%# Eval("TestRecommendId") %>' runat="server" />
                                                                    <asp:HiddenField ID="SearchTestRecommendation_testRecommendationsGridViewDiv_isTestUsedHiddenField"
                                                                        Value='<%# Eval("IsTestUsed") %>' runat="server" />
                                                                    <asp:HiddenField ID="SearchTestRecommendation_testRecommendationsGridViewDiv_isActiveHiddenField"
                                                                        Value='<%# Eval("IsActive") %>' runat="server" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="9%" Wrap="true"/>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Name" SortExpression="TEST_NAME" HeaderStyle-Width="13%" HeaderStyle-Wrap="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTestRecommendation_testRecommendationsGridViewDiv_testNameLabel"
                                                                        runat="server" Text='<%# Eval("TestName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="13%" Wrap="true"/>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Description" SortExpression="TEST_DESCRIPTION" HeaderStyle-Width="28%" HeaderStyle-Wrap="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTestRecommendation_testRecommendationsGridViewDiv_testDescriptionLabel"
                                                                        runat="server" Text='<%# Eval("TestDescription") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="28%" Wrap="true"/>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Skill" SortExpression="SKILL" HeaderStyle-Width="10%" HeaderStyle-Wrap="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTestRecommendation_testRecommendationsGridViewDiv_skillLabel"
                                                                        runat="server" Text='<%# Eval("Skill") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10%" Wrap="true"/>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No Of<br>Questions" SortExpression="NO_OF_QUESTIONS" HeaderStyle-Width="6%" HeaderStyle-Wrap="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTestRecommendation_testRecommendationsGridViewDiv_noOfQuestionsLabel"
                                                                        runat="server" Text='<%# Eval("NoOfQuestions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="6%" Wrap="true"/>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Time Limit<br>(hh:mm)" SortExpression="TIME_LIMIT" HeaderStyle-Width="6%" HeaderStyle-Wrap="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTestRecommendation_testRecommendationsGridViewDiv_timeLimitLabel"
                                                                        runat="server" Text='<%# Forte.HCM.Support.Utility.ConvertSecondsToHHMM(Convert.ToInt32(Eval("TimeLimit"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="6%" Wrap="true"/>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Active" SortExpression="ACTIVE" HeaderStyle-Width="6%" HeaderStyle-Wrap="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTestRecommendation_testRecommendationsGridViewDiv_activeStatusLabel"
                                                                        runat="server" Text='<%# Eval("ActiveStatusName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="6%" Wrap="true"/>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100%">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td align="right" style="width: 100%">
                                                            <uc3:PageNavigator ID="SearchTestRecommendation_pagingNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchTestRecommendation_searchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchTestRecommendation_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchTestRecommendation_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchTestRecommendation_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="SearchTestRecommendation_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            &nbsp;<asp:LinkButton ID="SearchTestRecommendation_bottomResetLinkButton" runat="server"
                                Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchTestRecommendation_resetLinkButton_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="SearchTestRecommendation_bottomCancelLinkButton"
                                runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchTestRecommendation_changeTestStatusUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="SearchTestRecommendation_changeTestStatusHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="SearchTestRecommendation_changeTestStatusPanel" runat="server"
                            Style="display: none" CssClass="popupcontrol_test_recommendation_change_test_status">
                            <uc2:ConfirmMsgControl ID="SearchTestRecommendation_changeTestStatusConfirmMsgControl"
                                runat="server" OnOkClick="SearchTestRecommendation_changeTestStatusConfirmMsgControl_OkClick" />
                            <asp:HiddenField ID="SearchTestRecommendation_changeTestStatusTestRecommendationIDHiddenField" runat="server" />
                            <asp:HiddenField ID="SearchTestRecommendation_changeTestStatusTestStatusHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchTestRecommendation_changeTestStatusModalPopupExtender"
                            runat="server" PopupControlID="SearchTestRecommendation_changeTestStatusPanel"
                            TargetControlID="SearchTestRecommendation_changeTestStatusHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
