﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditTestSession.cs
// File that represents the user interface for editing an existing test
// session details. This will interact with the TestBLManager to update 
// the test session.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface and layout for 
    /// EditTestSession page. This provides to modify the existing
    /// information against the test session id. Also this page 
    /// provides some set of links in the test session grid to 
    /// view the candidate detail, test session and cancel the 
    /// the candidate test session. This class is inherited from 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class EditTestSession : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Variables                                           

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will fire when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = EditTestSession_saveButton.UniqueID;
                Master.SetPageCaption("Edit Test Session");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = EditTestSession_sessionDescTextBox.UniqueID;
                    EditTestSession_sessionDescTextBox.Focus();

                    if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                        ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                        ViewState["SORT_EXPRESSION"] = "TestSessionID";

                    if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                        ViewState["PAGE_NUMBER"] = "1";

                    if (!Utility.IsNullOrEmpty(Request.QueryString["testsessionid"]))
                    {
                        EditTestSession_testSessionIDHiddenField.Value =
                            Request.QueryString["testsessionid"].ToString();

                        // Method that will bind the test session details in the grid 
                        // against the test key.
                        LoadTestSessionDetail(EditTestSession_testSessionIDHiddenField.Value);

                        // Enable/disable cyber proctoring based on feature
                        // applicability.
                        bool isCyberProctorApplicable = new
                            AdminBLManager().IsFeatureApplicable(base.tenantID,
                            Constants.FeatureConstants.CYBER_PROCTOR);

                        EditTestSession_cyberProctorateCheckBox.Enabled = isCyberProctorApplicable;
                        EditTestSession_cyberProctorWhyDisabledLinkButton.Visible = !isCyberProctorApplicable;
                    }

                    if (!Utility.IsNullOrEmpty(Request.QueryString["edit"]) &&
                        Request.QueryString["edit"].ToUpper() == "Y")
                    {
                        base.ShowMessage(EditTestSession_topSuccessMessageLabel,
                            EditTestSession_bottomSuccessMessageLabel,
                            string.Format(Resources.HCMResource.EditTestSession_TestSessionsUpdatedSuccessfully,
                            Request.QueryString["testsessionid"].ToString().Trim(),
                            Request.QueryString["candidatesessionids"].ToString().Trim()));
                        EditTestSession_emailImageButton.Visible = true;
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["edit"]) &&
                        Request.QueryString["edit"].ToUpper() == "N")
                    {
                        base.ShowMessage(EditTestSession_topSuccessMessageLabel,
                            EditTestSession_bottomSuccessMessageLabel,
                            string.Format(Resources.HCMResource.
                            EditTestSession_CandidateSessionsCreatedSuccessfully,
                            Request.QueryString["testsessionid"].ToString().Trim(),
                            Request.QueryString["candidatesessionids"].ToString().Trim()));
                        EditTestSession_emailImageButton.Visible = true;
                    }
                }

                // Call pagination event handler
                EditTestSession_bottomSessionPagingNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                        (EditTestSession_bottomSessionPagingNavigator_PageNumberClick);

                // Add javascript function for expanding and collapsing gridview.
                EditTestSession_searchTestSessionResultsTR.Attributes.Add("onclick", "ExpandorCompress('"
                  + EditTestSession_testSessionDiv.ClientID + "','"
                  + EditTestSession_sessionDetailsDIV.ClientID
                  + "','EditTestSession_searchSessionResultsUpSpan',"
                  + "'EditTestSession_searchSessionResultsDownSpan')");

                // Add handler for email button.
                EditTestSession_emailImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowEmailCandidateSessions('EMAIL_TEST_SESSION')");


                // Validation for expand all and collapse all image button
                if (!Utility.IsNullOrEmpty(EditTestSession_restoreHiddenField.Value) &&
                   EditTestSession_restoreHiddenField.Value == "Y")
                {
                    EditTestSession_sessionDetailsDIV.Style["display"] = "none";
                    EditTestSession_searchSessionResultsUpSpan.Style["display"] = "block";
                    EditTestSession_searchSessionResultsDownSpan.Style["display"] = "none";
                    EditTestSession_testSessionDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    EditTestSession_sessionDetailsDIV.Style["display"] = "block";
                    EditTestSession_searchSessionResultsUpSpan.Style["display"] = "none";
                    EditTestSession_searchSessionResultsDownSpan.Style["display"] = "block";
                    EditTestSession_testSessionDiv.Style["height"] = RESTORED_HEIGHT;
                }

                EditTestSession_searchTestSessionResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    EditTestSession_testSessionDiv.ClientID + "','" +
                    EditTestSession_sessionDetailsDIV.ClientID + "','" +
                    EditTestSession_searchSessionResultsUpSpan.ClientID + "','" +
                    EditTestSession_searchSessionResultsDownSpan.ClientID + "','" +
                    EditTestSession_restoreHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when a page number is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that contains the event data.
        /// </param>
        void EditTestSession_bottomSessionPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            LoadCandidateSessions(e.PageNumber);
            ViewState["PAGE_NUMBER"] = e.PageNumber;
        }

        /// <summary>
        /// Handler that will validate the input fields before they saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditTestSession_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                // Validate the input fields
                if (!IsValidData())
                    return;

                TestSessionDetail testSessionDetail = PreviewTestSession();

                // Set the TestSessionPreview Mode. If the mode is VIEW, checkboxes
                // won't be displayed for Randomize question, cyberproctoring, and
                // display results to candidate. Instead, respective label control
                // will be shown as YES.
                EditTestSession_viewTestSessionSave_UserControl.Mode = "view";
                EditTestSession_viewTestSessionSave_UserControl.DataSource = testSessionDetail;
                EditTestSession_viewTestSessionSave_modalpPopupExtender.Show();

                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that shows the modified information before its getting updated
        /// in the database.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void EditTestSession_previewTestSessionControl_updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                TestSessionDetail testSessionDetail = ConstructTestSessionDetails();

                string testSessionID = EditTestSession_testSessionIDHiddenField.Value.Trim();
                string candidateSessionIDs = null;

                // Check if the test session is modified by someone else. 
                // This will help to prevent concurrent updation
                // The MODIFIED_DATE should be retrieved as formatted date.
                // i.e Convert(varchar(25),date,121)
                if (new CommonBLManager().IsRecordModified("TEST_SESS",
                    testSessionDetail.TestSessionID,
                    (DateTime)ViewState["MODIFIED_DATE"]))
                {
                    base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_ConcurrencyIsNotPossible);
                    return;
                }

                bool isMailSent = false;
                new TestBLManager().UpdateTestSession(testSessionDetail, base.userID,
                    testSessionID, out candidateSessionIDs,
                    (testSessionDetail.NumberOfCandidateSessions -
                    Convert.ToInt32(ViewState["NO_OF_SESSIONS"])), out isMailSent);

                // If the number of session is increased, 
                // then the page redirect with EDIT=N parameter.
                if (testSessionDetail.NumberOfCandidateSessions -
                    Convert.ToInt32(ViewState["NO_OF_SESSIONS"]) > 0)
                {
                    Response.Redirect("EditTestSession.aspx?m=1&s=3" +
                        "&testsessionid=" + testSessionID +
                        "&parentpage=S_TSN&edit=n" +
                        "&candidatesessionids=" + candidateSessionIDs, false);
                }
                else
                {
                    Response.Redirect("EditTestSession.aspx?m=1&s=3" +
                        "&testsessionid=" + testSessionID +
                        "&parentpage=S_TSN&edit=y" +
                        "&candidatesessionids=" + candidateSessionIDs, false);
                }


                // If the mail sent failed, then show the error message.
                if (isMailSent == false)
                {
                    base.ShowMessage(EditTestSession_topErrorMessageLabel,
                        EditTestSession_bottomErrorMessageLabel,
                         "Mail cannot be sent to the session author. Contact your administrator");
                }

                // Show the email session button.
                EditTestSession_emailImageButton.Visible = true;

                // Keep the created test session details in session, so that it can
                // be used in the email popup.
                Session["EMAIL_TEST_SESSION_SUBJECT"] = string.Format
                    ("Test session '{0}' updated", testSessionID);
                Session["EMAIL_TEST_SESSION_MESSAGE"] = string.Format
                    ("Dear Viewer,\n\nSub: Test session '{0}' updated.\n\nThe following candidate sessions are associated:\n{1}",
                    testSessionID, candidateSessionIDs);

                // Refresh test session details gridview i.e. Once a candidate 
                // session is created, it'll be updated in the gridview.
                LoadCandidateSessions(1); ;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that shows the CandidateDetail/View Test Session/Cancel Test Session
        /// image according to the passed the command name and its command argument.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void EditTestSession_testSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "CandidateDetail")
                {
                    CandidateTestDetail candidateDetail = new CandidateTestDetail();
                    candidateDetail.CandidateSessionID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("EditTestSession_byTestSession_candidateSessionIdLabel")).Text;
                    candidateDetail.AttemptID = int.Parse(((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("EditTestSession_attemptIdHiddenField")).Value);
                    candidateDetail.TestID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("EditTestSession_byTestSession_testKeyLabel")).Text;
                     candidateDetail.CandidateName = ((Label)((ImageButton)e.CommandSource).
                        FindControl("EditTestSession_candidateFullnameLabel")).Text;
                     candidateDetail.ShowTestScore = new TestSessionBLManager().GetShowTestScore(candidateDetail.CandidateSessionID);
                    EditTestSession_candidateDetailControl.Datasource = candidateDetail;
                    EditTestSession_candidateDetailModalPopupExtender.CancelControlID =
                        ((ImageButton)EditTestSession_candidateDetailControl.
                        FindControl("CandidateDetailControl_topCancelImageButton")).ClientID;
                    EditTestSession_candidateDetailModalPopupExtender.Show();
                }
                else if (e.CommandName == "ViewTestSession")
                {
                    // Instead of dummy, if we pass empty string, it returns all 
                    // candidate sessions available in the session.
                    TestSessionDetail testSession =
                        new TestSchedulerBLManager().GetTestSessionDetail
                        (e.CommandArgument.ToString(), "dummy",base.userID);

                    // Assign test session object to the preview test session user control's datasource.
                    // This will show the test session detail before create.
                    EditTestSession_previewTestSessionControl_userControl1.DataSource = testSession;
                    EditTestSession_previewTestSessionControl_modalpPopupExtender1.Show();
                }
                else if (e.CommandName == "CancelTestSession")
                {
                    EditTestSession_cancelTestReasonTextBox.Text = string.Empty;

                    HiddenField attemptIdHiddenField = (e.CommandSource as ImageButton).Parent.
                        FindControl("EditTestSession_attemptIdHiddenField") as HiddenField;

                    ViewState["CANDIDATE_SESSION_ID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;

                    EditTestSession_cancelErrorMessageLabel.Text = string.Empty;
                    EditTestSession_cancelSessionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call whenever a row gets the information. 
        /// It implements the mouse over and out style and showing icons
        /// based on the validation.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void EditTestSession_testSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton EditTestSession_candidateDetailImageButton = (ImageButton)
                    e.Row.FindControl("EditTestSession_candidateDetailImageButton");

                ImageButton EditTestSession_cancelTestSessionImageButton = (ImageButton)
                    e.Row.FindControl("EditTestSession_cancelTestSessionImageButton");

                HiddenField EditTestSession_statusHiddenField = (HiddenField)
                    e.Row.FindControl("EditTestSession_statusHiddenField");

                // Set visibility based on the attempt status
                if (EditTestSession_statusHiddenField.Value.Trim()
                    == Constants.CandidateAttemptStatus.SCHEDULED)
                {
                    EditTestSession_candidateDetailImageButton.Visible = true;
                    EditTestSession_cancelTestSessionImageButton.Visible = true;
                }

                if (EditTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.NOT_SCHEDULED
                    || EditTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CANCELLED)
                {
                    EditTestSession_candidateDetailImageButton.Visible = false;
                    EditTestSession_cancelTestSessionImageButton.Visible = false;
                }
                else if (EditTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.IN_PROGRESS
                    || EditTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CYBER_PROC_INIT
                    || EditTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.COMPLETED
                    || EditTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.ELAPSED
                    || EditTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.QUIT)
                {
                    EditTestSession_candidateDetailImageButton.Visible = true;
                    EditTestSession_cancelTestSessionImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Handler that is used to show an icon on the header column
        /// which is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void EditTestSession_testSessionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    // Get last clicked column index
                    int sortColumnIndex = GetSortColumnIndex
                        (EditTestSession_testSessionGridView,
                        (string)ViewState["SORT_EXPRESSION"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_DIRECTION_KEY"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will sort the grid view data.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the send of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void EditTestSession_testSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                // Assign sort expression column to the viewstate
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_EXPRESSION"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_DIRECTION_KEY"] = ((SortType)ViewState["SORT_DIRECTION_KEY"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;
                else
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;


                ViewState["SORT_EXPRESSION"] = e.SortExpression;

                // Reload the pagination and gridview controls
                EditTestSession_bottomSessionPagingNavigator.Reset();
                LoadCandidateSessions(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will fire when clicking on the save button. 
        /// This cancels the candidate test session by updating the reason.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditTestSession_saveCancellationButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                if (ViewState["ATTEMPT_ID"] == null || ViewState["CANDIDATE_SESSION_ID"] == null)
                    return;

                // Check if the reason text box is empty. If it is, show the error message.
                // Otherwise, update the session status as CANCEL.
                if (EditTestSession_cancelTestReasonTextBox.Text.Trim().Length == 0)
                {
                    EditTestSession_cancelErrorMessageLabel.Text = "Reason cannot be empty";
                    EditTestSession_cancelSessionModalPopupExtender.Show();
                }
                else
                {
                    int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                    string candidateSessionId = ViewState["CANDIDATE_SESSION_ID"].ToString();

                    bool isMailSent = true;

                    new TestConductionBLManager().UpdateSessionStatus(candidateSessionId,
                       attemptId, Constants.CandidateAttemptStatus.CANCELLED,
                       Constants.CandidateSessionStatus.CANCELLED, base.userID, out isMailSent);

                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();

                    candidateTestSession.AttemptID = attemptId;
                    candidateTestSession.CandidateTestSessionID = candidateSessionId;
                    candidateTestSession.CancelReason = EditTestSession_cancelTestReasonTextBox.Text.Trim();
                    candidateTestSession.ModifiedBy = base.userID;

                    new TestBLManager().CancelTestSession(candidateTestSession);

                    base.ShowMessage(EditTestSession_topSuccessMessageLabel,
                        EditTestSession_bottomSuccessMessageLabel,
                        string.Format(Resources.HCMResource.
                        EditTestSession_CandidateSessionIsCancelled, candidateSessionId));

                    LoadCandidateSessions(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the reset linkbutton is clicked.
        /// During that time, it will load values before modified.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditTestSession_resetButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler that will redirect to the schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditTestSession_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Scheduler/ScheduleCandidate.aspx?m=1&s=3&parentpage="
                + Constants.ParentPage.SEARCH_TEST_SESSION
                + "&testsessionid="
                + EditTestSession_testSessionIDHiddenField.Value.Trim(), false);
        }

        #endregion Event Handlers                                              

        #region Public Methods                                                 

        /// <summary>
        /// Method that will show pop up extender when download button clicked on user control.
        /// </summary>
        public void ShowModalPopup()
        {
            EditTestSession_candidateDetailModalPopupExtender.Show();
        }

        #endregion Public Methods                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that will populate the test session and candidate
        /// session details against the test session id.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the test session id.
        /// </param>
        private void LoadTestSessionDetail(string testSessionID)
        {
            // Instead of dummy, if we pass empty string, it returns all 
            // candidate sessions available in the session.
            TestSessionDetail testSession =
                new TestSchedulerBLManager().GetTestSessionDetail
               (Request.QueryString["testsessionid"], "",base.userID);

            if (testSession == null)
                return;

            EditTestSession_testKeyValueLabel.Text = testSession.TestID;
            EditTestSession_testNameValueLabel.Text = testSession.TestName.Trim();
            EditTestSession_sessionKeyValueLabel.Text = testSessionID;
            EditTestSession_sessionNoTextBox.Text = testSession.NumberOfCandidateSessions.ToString();
            EditTestSession_creditValueLabel.Text = testSession.TotalCredit.ToString();
            EditTestSession_positionProfileValueLabel.Text = testSession.PositionProfileName;
            EditTestSession_positionProfileIDHiddenField.Value = testSession.PositionProfileID.ToString();
            EditTestSession_timeLimitTextBox.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds(testSession.TimeLimit);
            EditTestSession_recommendedTimeValueLabel.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds(testSession.RecommendedTimeLimit);
            EditTestSession_expiryDateTextBox.Text = testSession.ExpiryDate.ToString();
            EditTestSession_randomSelectionCheckBox.Checked = testSession.IsRandomizeQuestionsOrdering;
            EditTestSession_cyberProctorateCheckBox.Checked = testSession.IsCyberProctoringEnabled;
            EditTestSession_displayResultsCheckBox.Checked = testSession.IsDisplayResultsToCandidate;
            EditTestSession_sessionDescTextBox.Text = testSession.TestSessionDesc.Trim();
            EditTestSession_instructionsTextBox.Text = testSession.Instructions.Trim();

            ViewState["NO_OF_SESSIONS"] = testSession.NumberOfCandidateSessions.ToString();
            EditTestSession_testKeyHiddenField.Value = testSession.TestID;
            ViewState["CREATED_BY"] = testSession.CreatedBy;
            ViewState["MODIFIED_DATE"] = testSession.ModifiedDate;

            // Call load test session method by passing test key and page number
            LoadCandidateSessions(1);
        }

        /// <summary>
        /// Method that is used to preview test session details once the user clicked 
        /// on the save button.
        /// </summary>
        /// <returns>Returns the <see cref="TestSessionDetail"/> object</returns>
        private TestSessionDetail PreviewTestSession()
        {
            // Initialize the test session detail object
            TestSessionDetail testSessionDetail = new TestSessionDetail();

            // Get the information from the controls and assign to the object members
            testSessionDetail.TestID = EditTestSession_testKeyValueLabel.Text.Trim();
            testSessionDetail.TestName = EditTestSession_testNameValueLabel.Text;
            testSessionDetail.TestSessionID = Request.QueryString["testsessionid"].Trim();

            testSessionDetail.NumberOfCandidateSessions
                = Convert.ToInt32(EditTestSession_sessionNoTextBox.Text);

            testSessionDetail.TotalCredit = Convert.ToDecimal(EditTestSession_creditValueLabel.Text);

            //Request[ScheduleCandidate_Popup_candidateNameTextBox.UniqueID].Trim();
            testSessionDetail.PositionProfileName = EditTestSession_positionProfileValueLabel.Text;

            testSessionDetail.TimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(EditTestSession_timeLimitTextBox.Text);

            testSessionDetail.RecommendedTimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(EditTestSession_recommendedTimeValueLabel.Text);

            // Set the time to maximum in expiry date.
            testSessionDetail.ExpiryDate = (Convert.ToDateTime(EditTestSession_expiryDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

            testSessionDetail.IsRandomizeQuestionsOrdering = EditTestSession_randomSelectionCheckBox.Checked;
            testSessionDetail.IsDisplayResultsToCandidate = EditTestSession_displayResultsCheckBox.Checked;
            testSessionDetail.IsCyberProctoringEnabled = EditTestSession_cyberProctorateCheckBox.Checked;
            testSessionDetail.TestSessionDesc = EditTestSession_sessionDescTextBox.Text;
            testSessionDetail.Instructions = EditTestSession_instructionsTextBox.Text;

            testSessionDetail.CreatedBy = base.userID;
            testSessionDetail.ModifiedBy = base.userID;

            return testSessionDetail;
        }

        /// <summary>
        /// Method that gets the seconds values and updated the time format as hh:mm:yyyy
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string GetTimeFormat(int seconds)
        {
            return Utility.ConvertSecondsToHoursMinutesSeconds(seconds);
        }

        /// <summary>
        /// Method that will load the candidate sessions.
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="sortBy">
        /// A <see cref="string"/> that contains the column to be sorted.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="string"/> that contains the sort direction either ASC/DESC.
        /// </param>
        private void LoadCandidateSessions(int pageNumber)
        {
            int totalRecords = 0;
            List<CandidateTestSessionDetail> candidateTestSessions =
                new TestBLManager().GetCandidateTestSessions
                (EditTestSession_testKeyHiddenField.Value, pageNumber,
                base.GridPageSize, ViewState["SORT_EXPRESSION"].ToString(),
               (SortType)ViewState["SORT_DIRECTION_KEY"],base.userID, out totalRecords);

            // Check if the candidateTestSession object is null, 
            // Then hide the gridview. Otherwise, show the loaded gridview 
            if (candidateTestSessions == null)
                EditTestSession_testSessionGridviewTR.Visible = false;
            else
            {
                EditTestSession_testSessionGridviewTR.Visible = true;
                EditTestSession_testSessionGridView.DataSource = candidateTestSessions;
                EditTestSession_testSessionGridView.DataBind();
            }

            // Update page size and total records
            EditTestSession_bottomSessionPagingNavigator.PageSize = base.GridPageSize;
            EditTestSession_bottomSessionPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that returns the TestSessionDetails object.
        /// </summary>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session details.
        /// </returns>
        private TestSessionDetail ConstructTestSessionDetails()
        {
            // Initialize test session detail object
            TestSessionDetail testSessionDetail = new TestSessionDetail();

            testSessionDetail.TestID = EditTestSession_testKeyValueLabel.Text.Trim();
            testSessionDetail.TestSessionID = EditTestSession_sessionKeyValueLabel.Text.Trim();
            testSessionDetail.TestName = EditTestSession_testNameValueLabel.Text.Trim();

            testSessionDetail.NumberOfCandidateSessions =
                Convert.ToInt32(EditTestSession_sessionNoTextBox.Text);
            testSessionDetail.TotalCredit =
                Convert.ToDecimal(EditTestSession_creditValueLabel.Text);

            if (!Utility.IsNullOrEmpty(EditTestSession_positionProfileIDHiddenField.Value))
                testSessionDetail.PositionProfileID = Convert.ToInt32(EditTestSession_positionProfileIDHiddenField.Value);
            
            testSessionDetail.TimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(EditTestSession_timeLimitTextBox.Text);
            testSessionDetail.RecommendedTimeLimit =
                Utility.ConvertHoursMinutesSecondsToSeconds(EditTestSession_recommendedTimeValueLabel.Text);
            testSessionDetail.ExpiryDate = Convert.ToDateTime(EditTestSession_expiryDateTextBox.Text);

            // Set random question order status
            testSessionDetail.IsRandomizeQuestionsOrdering
                = (EditTestSession_randomSelectionCheckBox.Checked) ? true : false;

            // Set display result status 
            testSessionDetail.IsDisplayResultsToCandidate
                = (EditTestSession_displayResultsCheckBox.Checked) ? true : false;

            // Set cyber proctoring status
            testSessionDetail.IsCyberProctoringEnabled
                = (EditTestSession_cyberProctorateCheckBox.Checked) ? true : false;

            testSessionDetail.CreatedBy = base.userID;
            testSessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            testSessionDetail.Instructions =
                EditTestSession_instructionsTextBox.Text.Trim();

            // Set session descriptions
            testSessionDetail.TestSessionDesc =
                EditTestSession_sessionDescTextBox.Text.Trim();

            return testSessionDetail;
        }

        /// <summary>
        /// Method that helps to clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            EditTestSession_topSuccessMessageLabel.Text = string.Empty;
            EditTestSession_bottomSuccessMessageLabel.Text = string.Empty;
            EditTestSession_topErrorMessageLabel.Text = string.Empty;
            EditTestSession_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods                                             

        #region Protected Methods                                              

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;

            // Validate number of session field
            if (Convert.ToInt32(EditTestSession_sessionNoTextBox.Text) <= 0 ||
                (Convert.ToInt32(EditTestSession_sessionNoTextBox.Text) > 30))
            {
                isValidData = false;
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_EnteredNumberOfSessionIsInvalid);
            }
            else
            {
                if (Convert.ToInt32(EditTestSession_sessionNoTextBox.Text) <
                    Convert.ToInt32(ViewState["NO_OF_SESSIONS"]))
                {
                    isValidData = false;
                    base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_NumberOfSessionCannotBeDecreased);
                }
            }

            DateTime timeLimit;
            // Validate time limit field 
            // case i) Time limit text is empty
            // case ii) Time limit has invalid time i.e exceeds 24hrs (25:25:25)
            // case iii) Time limit contains 00:00:00
            if (EditTestSession_timeLimitTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_TimeLimitCannotBeEmpty);
            }
            else if (DateTime.TryParse(EditTestSession_timeLimitTextBox.Text, out timeLimit) == false)
            {
                isValidData = false;
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_EnteredTimeLimitIsNotValid);
            }
            else if (Utility.ConvertHoursMinutesSecondsToSeconds(EditTestSession_timeLimitTextBox.Text)
                    < Constants.General.DEFAULT_TEST_MINIMUM_TIME)
            {
                isValidData = false;
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_TestDetailsControl_RecommendedTimeLabelTextBox_NotValid1);
            }

            EditTestSession_MaskedEditValidator.Validate();

            if (EditTestSession_expiryDateTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_ExpiryDateCannotBeEmpty);
            }
            else
            {
                // This will check if the date is valid date. i.e. 99/99/9999
                if (!EditTestSession_MaskedEditValidator.IsValid)
                {
                    isValidData = false;
                    base.ShowMessage(EditTestSession_topErrorMessageLabel,
                    EditTestSession_bottomErrorMessageLabel,
                    EditTestSession_MaskedEditValidator.ErrorMessage);
                }
                else
                {
                    // Check if the expiry date is less than current date
                    if (DateTime.Parse(EditTestSession_expiryDateTextBox.Text) < DateTime.Today)
                    {
                        isValidData = false;
                        base.ShowMessage(EditTestSession_topErrorMessageLabel,
                        EditTestSession_bottomErrorMessageLabel,
                        Resources.HCMResource.EditTestSession_ExpiryDateCannotBeLessThanCurrentDate);
                    }
                }
            }
            return isValidData;
        }

        #endregion Protected Methods                                           
    }
}