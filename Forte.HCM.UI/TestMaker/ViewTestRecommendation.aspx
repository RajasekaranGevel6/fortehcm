<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewTestRecommendation.aspx.cs"
    Inherits="Forte.HCM.UI.TestMaker.ViewTestRecommendation" MasterPageFile="~/MasterPages/SiteAdminMaster.Master" %>

<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/AutomatedTestSummaryControl.ascx" TagName="AutomatedTestControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/TestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc7" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="ViewTestRecommendation_bodyContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        var control;
        function setFocus(sender, e)
        {
            try
            {
                control = $get('<%=ViewTestRecommendation_byQuestion_totalNumberTextBox.ClientID%>');
                control.focus();
            }
            catch (er)
            {
            }
        }

    </script>
    <script type="text/javascript" language="javascript">

        // Expand or Collapse question details in gridview
        function ExpORCollapseRows(targetControl)
        {
            var ctrl = document.getElementById('<%=ViewTestRecommendation_byQuestion_stateHiddenField.ClientID %>');
            targetValue = ctrl.value;
            if (targetValue == "0")
            {
                targetControl.innerHTML = "Collapse All";
                ExpandAllRows(targetControl);
                ctrl.value = 1;
            }
            else
            {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }

            return false;
        }

        // Expands all question details in test draft gridview
        function ExpandAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= ViewTestRecommendation_byQuestion_testDraftGridView.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("ViewTestRecommendation_byQuestion_detailsDiv") != "-1")
                    {
                        rowItems[indexRow].style.display = "block";
                    }
                }
            }
            return false;
        }

        // Collpase all question details in test draft gridview
        function CollapseAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= ViewTestRecommendation_byQuestion_testDraftGridView.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("ViewTestRecommendation_byQuestion_detailsDiv") != "-1")
                    {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }
            return false;
        }

        function ShowZoomedChart(sessionName)
        {
            var height = 620;
            var width = 950;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/ZoomedChart.aspx?sessionName=" + sessionName;
            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }

    </script>
    <asp:Panel ID="ViewTestRecommendation_mainPanel" runat="server" DefaultButton="ViewTestRecommendation_topGenerateTestButton">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text_bold">
                                <asp:Literal ID="ViewTestRecommendation_headerLiteral" runat="server" Text="View Test Recommendation"></asp:Literal>
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="58%" align="right">
                                            <asp:Button ID="ViewTestRecommendation_topGenerateTestButton" runat="server" Text="Generate Sample Test"
                                                SkinID="sknButtonId" CausesValidation="False" OnClick="ViewTestRecommendation_generateTestButton_Click" />
                                        </td>
                                        <td align="right" style="width: 30%">
                                            <asp:LinkButton ID="ViewTestRecommendation_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Reset" OnClick="ViewTestRecommendation_resetLinkButton_Click" />
                                            &nbsp;|&nbsp;<asp:LinkButton ID="ViewTestRecommendation_topCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="ViewTestRecommendation_topMessagesUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="ViewTestRecommendation_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewTestRecommendation_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="ViewTestRecommendation_questionPanel" runat="server" DefaultButton="ViewTestRecommendation_topGenerateTestButton">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 100%">
                                    <div id="ViewTestRecommendation_searchCriteriaDiv" runat="server" style="display: none;
                                        width: 100%">
                                        <asp:UpdatePanel ID="ViewTestRecommendation_byQuestion_searchCriteriaUpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="panel_inner_body_bg" style="width: 100%">
                                                            <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 13%">
                                                                                    <asp:Label ID="ViewTestRecommendation_byQuestion_totalNumberLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Number Of Questions"></asp:Label>&nbsp;<span
                                                                                            class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 3%">
                                                                                    <asp:TextBox ID="ViewTestRecommendation_byQuestion_totalNumberTextBox" runat="server"
                                                                                        MaxLength="2" Width="100%"></asp:TextBox>
                                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="ViewTestRecommendation_byQuestion_numberOfQuestionsFileteredExtender"
                                                                                        runat="server" TargetControlID="ViewTestRecommendation_byQuestion_totalNumberTextBox"
                                                                                        FilterType="Numbers">
                                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                                </td>
                                                                                <td style="width: 16%" align="right">
                                                                                    <asp:Label ID="ViewTestRecommendation_byQuestion_testSkillLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Test Skill(s)"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 44%" align="left">
                                                                                    <div style="float: left; width: 95%; text-align: right">
                                                                                        <asp:TextBox ID="ViewTestRecommendation_byQuestion_testSkillTextBox" MaxLength="50"
                                                                                            runat="server" Width="92%">
                                                                                        </asp:TextBox>
                                                                                        <asp:HiddenField ID="ViewTestRecommendation_byQuestion_positionProfileTagsHiddenField"
                                                                                            runat="server" />
                                                                                        <asp:HiddenField ID="ViewTestRecommendation_byQuestion_positionProfileIDHiddenField"
                                                                                            runat="server" />
                                                                                    </div>
                                                                                    <div style="float: left; padding-left: 2px">
                                                                                        <asp:ImageButton ID="ViewTestRecommendation_byQuestion_skillsImageButton" SkinID="sknHelpImageButton"
                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter multiple skills with comma separated" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 4%">
                                                                                    <asp:Label ID="ViewTestRecommendation_byQuestion_timeSearchLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Time Limit (hh:mm)"></asp:Label>&nbsp;<span
                                                                                            class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 6%">
                                                                                    <asp:TextBox ID="ViewTestRecommendation_byQuestion_timeSearchTextbox" runat="server"
                                                                                        Columns="4"></asp:TextBox>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="ViewTestRecommendation_byQuestion_timeSearchMaskedEditExtender"
                                                                                        runat="server" TargetControlID="ViewTestRecommendation_byQuestion_timeSearchTextbox"
                                                                                        Mask="99:99" MaskType="Time" ErrorTooltipEnabled="True" CultureAMPMPlaceholder=""
                                                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                                                                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                                                        Enabled="True">
                                                                                    </ajaxToolKit:MaskedEditExtender>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 9%" valign="top">
                                                                                    <asp:Label ID="ViewTestRecommendation_byQuestion_testNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Test Name"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 36%" valign="top">
                                                                                    <asp:TextBox ID="ViewTestRecommendation_byQuestion_testNameTextBox" runat="server"
                                                                                        Width="96%" MaxLength="50"></asp:TextBox>
                                                                                    <asp:HiddenField ID="ViewTestRecommendation_byQuestion_generateTestHitCountHiddenField"
                                                                                        runat="server" Value="0" />
                                                                                </td>
                                                                                <td style="width: 12%" valign="top">
                                                                                    <asp:Label ID="ViewTestRecommendation_testDescriptionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Test Description"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 42%" valign="top">
                                                                                    <asp:TextBox ID="ViewTestRecommendation_testDescriptionTextBox" runat="server" Width="98%"
                                                                                        TextMode="MultiLine" Height="60" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                                        onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td3" class="header_bg" runat="server">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%" align="left" class="header_text_bold">
                                                                        <asp:Literal ID="ViewTestRecommendation_byQuestion_testSegmentsLiteral" runat="server"
                                                                            Text="Test Segments"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%" class="grid_body_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <div style="width: 100%; overflow: auto; display: block; background-color: Red">
                                                                            <asp:GridView ID="ViewTestRecommendation_byQuestion_testSegmentGridView" runat="server"
                                                                                AutoGenerateColumns="False" ShowHeader="False" SkinID="sknNewGridView" OnRowDataBound="ViewTestRecommendation_byQuestion_testSegmentGridView_RowDataBound"
                                                                                AutoGenerateDeleteButton="false">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="5">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:ImageButton ID="ViewTestRecommendation_byQuestion_testSegmentGridView_deleteImageButton"
                                                                                                            runat="server" CommandName="Delete Segment" SkinID="sknDeleteImageButton" ToolTip="Delete segment"
                                                                                                            OnClick="ViewTestRecommendation_byQuestion_testSegmentGridView_deleteImageButton_Click" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div style="float: left; width: 92%;" class="grouping_border_bg">
                                                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td style="width: 15%">
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_categoryLabel" runat="server" Text="Category"
                                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 48%; padding-left: 13px">
                                                                                                                        <asp:TextBox ID="ViewTestRecommendation_byQuestion_categoryTextBox" ReadOnly="true"
                                                                                                                            runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 20%">
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_subjectLabel" runat="server" Text="Subject"
                                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                        <asp:HiddenField ID="ViewTestRecommendation_byQuestion_categoryHiddenField" runat="server"
                                                                                                                            Value='<%# Eval("Cat_sub_id") %>' />
                                                                                                                        <asp:HiddenField ID="ViewTestRecommendation_byQuestion_categoryNameHiddenField" runat="server"
                                                                                                                            Value='<%# Eval("Category") %>' />
                                                                                                                        <asp:HiddenField ID="ViewTestRecommendation_byQuestion_subjectNameHiddenField" runat="server"
                                                                                                                            Value='<%# Eval("Subject") %>' />
                                                                                                                    </td>
                                                                                                                    <td style="width: 30%">
                                                                                                                        <asp:TextBox ID="ViewTestRecommendation_byQuestion_subjectTextBox" ReadOnly="true"
                                                                                                                            runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                        <div style="float: left; padding-left: 10px; padding-top: 5px;">
                                                                                                            <asp:ImageButton ID="ViewTestRecommendation_byQuestion_categoryImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td colspan="2">
                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_weightageHeadLabel" runat="server"
                                                                                                            Text="Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        <asp:TextBox ID="ViewTestRecommendation_byQuestion_weightageTextBox" runat="server"
                                                                                                            Width="8%" MaxLength="3" Text='<%# Eval("Weightage") %>'>
                                                                                                        </asp:TextBox>
                                                                                                        <ajaxToolKit:FilteredTextBoxExtender ID="ViewTestRecommendation_byQuestion_weightageFileteredExtender"
                                                                                                            runat="server" TargetControlID="ViewTestRecommendation_byQuestion_weightageTextBox"
                                                                                                            FilterType="Numbers">
                                                                                                        </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="1">
                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_testAreaLabel" runat="server" Text="Test Area"
                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="3" align="left">
                                                                                                        <div class="checkbox_list_bg" style="width: 100%; height: 15%; overflow: auto">
                                                                                                            <asp:CheckBoxList ID="ViewTestRecommendation_byQuestion_testAreaCheckBoxList" runat="server"
                                                                                                                RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="100%" TextAlign="Right">
                                                                                                            </asp:CheckBoxList>
                                                                                                            <asp:HiddenField ID="ViewTestRecommendation_byQuestion_testAreaHiddenField" runat="server"
                                                                                                                Value='<%# Eval("TestArea") %>' />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%;">
                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_rowSegmentLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                            runat="server" Text="Segment">
                                                                                                        </asp:Label>&nbsp;<asp:Label ID="ViewTestRecommendation_byQuestion_rowIdLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                            runat="server" Text='<%# Container.DataItemIndex + 1 %>'>
                                                                                                        </asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <table border="0" cellpadding="2" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td style="width: 10%;">
                                                                                                                    <asp:Label ID="ViewTestRecommendation_byQuestion_complexityLabel" runat="server"
                                                                                                                        Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                </td>
                                                                                                                <td align="left" style="width: 90%;">
                                                                                                                    <div class="checkbox_list_bg" style="width: 99.5%; height: 15%; overflow: auto;">
                                                                                                                        <asp:CheckBoxList ID="ViewTestRecommendation_byQuestion_complexityCheckBoxList" runat="server"
                                                                                                                            RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="75%">
                                                                                                                        </asp:CheckBoxList>
                                                                                                                        <asp:HiddenField ID="ViewTestRecommendation_byQuestion_complexityHiddenField" runat="server"
                                                                                                                            Value='<%# Eval("Complexity") %>' />
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td colspan="2">
                                                                                                        <table border="0" cellpadding="2" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="ViewTestRecommendation_byQuestion_keywordHeadLabel" runat="server"
                                                                                                                        Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <div style="float: left">
                                                                                                                        <asp:TextBox ID="ViewTestRecommendation_byQuestion_keywordTextBox" runat="server"
                                                                                                                            Columns="35" MaxLength="100" Text='<%# Eval("Keyword") %>'></asp:TextBox>
                                                                                                                    </div>
                                                                                                                    <div style="float: left; padding-left: 5px">
                                                                                                                        <asp:ImageButton ID="ViewTestRecommendation_byQuestion_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_height_2">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="4">
                                                                                                        <div id="ViewTestRecommendation_byQuestion_expectedQuestionsDiv" runat="server" visible="false"
                                                                                                            style="width: 100%;">
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td style="width: 15%;">
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_expectedQuestionsHeadLabel" runat="server"
                                                                                                                            Text="Expected Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 10%;">
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_expecetdQuestions" runat="server"
                                                                                                                            Text='<%# Eval("ExpectedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 15%;">
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestions_returnQuestionsHeadLabel" runat="server"
                                                                                                                            Text="Picked Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 10%;">
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestions_returnQuestionsLabel" runat="server"
                                                                                                                            Text='<%# Eval("PickedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 8%;">
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_remarkHeadLabel" runat="server"
                                                                                                                            Text="Remarks" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestin_remarkLabel" runat="server" Text='<%# Eval("Remarks") %>'
                                                                                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_noOfQuestionsInDBHeadLabel" runat="server"
                                                                                                                            Text="Total Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_noOfQuestionsLabel" runat="server"
                                                                                                                            Text='<%# Eval("TotalRecordsinDB") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="padding-left: 1%">
                                                            <asp:LinkButton ID="ViewTestRecommendation_byQuestion_addSegmantLinkButton" SkinID="sknAddLinkButton"
                                                                runat="server" Text="Add" ToolTip="Add new segment" OnClick="ViewTestRecommendation_byQuestion_addSegmantLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div id="ViewTestRecommendation_viewTestDetailsDiv" runat="server" style="display: none;
                                        width: 100%">
                                        <asp:UpdatePanel runat="server" ID="ViewTestRecommendation_viewTestDetailsUpdatePanel">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td id="Td1" class="header_bg" runat="server">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%" align="left" class="header_text_bold">
                                                                        <asp:Literal ID="ViewTestRecommendation_viewTestDetailLiteral" runat="server" Text="Test Details"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%" class="grid_body_bg">
                                                            <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 13%">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewNumberOfQuestionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Number Of Questions"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 5%">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewNumberOfQuestionValueLabel" runat="server"
                                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 8%">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewSkillsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Test Skill(s)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 46%">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewskillsValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 11%">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewTimeLimitLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Time Limit (hh:mm)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 6%">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewTimeLimitValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 8%" valign="top">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewTestNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Test Name"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 37%" valign="top">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewTestNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 11%" valign="top">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewTestDescriptionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Test Description"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 43%" valign="top">
                                                                                    <asp:Label ID="ViewTestRecommendation_viewTestDescriptionValueLabel" runat="server"
                                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="header_bg" runat="server">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%" align="left" class="header_text_bold">
                                                                        <asp:Literal ID="ViewTestRecommendation_viewTestSegmentsLiteral" runat="server" Text="Test Segments"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%" class="grid_body_bg">
                                                            <asp:GridView ID="ViewTestRecommendation_viewSegmentsGridView" runat="server" AutoGenerateColumns="False"
                                                                ShowHeader="False" SkinID="sknNewGridView" AutoGenerateDeleteButton="false" OnRowDataBound="ViewTestRecommendation_viewSegmentsGridView_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="5">
                                                                                <tr>
                                                                                    <td style="width: 100%">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="width: 5%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_segmentLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                        runat="server" Text="Segment">
                                                                                                    </asp:Label>&nbsp;
                                                                                                </td>
                                                                                                <td style="width: 93%" align="left">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_segmentNoLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                        runat="server" Text='<%# Container.DataItemIndex + 1 %>'>
                                                                                                    </asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100%">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="width: 8%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_categoryLabel" runat="server"
                                                                                                        Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 25%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_categoryValueLabel" runat="server"
                                                                                                        Text='<%# Eval("Category") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 8%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_subjectLabel" runat="server"
                                                                                                        Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 25%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_subjectValueLabel" runat="server"
                                                                                                        Text='<%# Eval("Subject") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 8%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_weightageLabel" runat="server"
                                                                                                        Text="Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 25%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_weightageValueLabel" runat="server"
                                                                                                        Width="8%" MaxLength="3" Text='<%# Eval("Weightage") %>' SkinID="sknLabelFieldText">
                                                                                                    </asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100%">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="width: 8%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_testAreaLabel" runat="server"
                                                                                                        Text="Test Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 25%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_testAreaValueLabel" runat="server"
                                                                                                        Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    <asp:HiddenField ID="ViewTestRecommendation_viewSegmentsGridView_testAreaHiddenField"
                                                                                                        runat="server" Value='<%# Eval("TestArea") %>' />
                                                                                                </td>
                                                                                                <td style="width: 8%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_complexityLabel" runat="server"
                                                                                                        Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 25%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_complexityValueLabel"
                                                                                                        runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    <asp:HiddenField ID="ViewTestRecommendation_viewSegmentsGridView_complexityHiddenField"
                                                                                                        runat="server" Value='<%# Eval("Complexity") %>' />
                                                                                                </td>
                                                                                                <td style="width: 8%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_keywordLabel" runat="server"
                                                                                                        Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 25%">
                                                                                                    <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_keywordValueLabel" runat="server"
                                                                                                        Text='<%# Eval("Keyword") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_2">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4">
                                                                                        <div id="ViewTestRecommendation_viewSegmentsGridView_expectedQuestionDiv" runat="server"
                                                                                            visible="false" style="width: 100%;">
                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td style="width: 15%;">
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_expectedQuestionsHeadLabel"
                                                                                                            runat="server" Text="Expected Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 10%;">
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_expecetdQuestions" runat="server"
                                                                                                            Text='<%# Eval("ExpectedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 15%;">
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_returnQuestionsHeadLabel"
                                                                                                            runat="server" Text="Picked Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 10%;">
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_returnQuestionsLabel"
                                                                                                            runat="server" Text='<%# Eval("PickedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 8%;">
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_remarksHeadLabel" runat="server"
                                                                                                            Text="Remarks" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_remarksLabel" runat="server"
                                                                                                            Text='<%# Eval("Remarks") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_noOfQuestionsInDBHeadLabel"
                                                                                                            runat="server" Text="Total Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="ViewTestRecommendation_viewSegmentsGridView_noOfQuestionsLabel" runat="server"
                                                                                                            Text='<%# Eval("TotalRecordsinDB") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr id="ViewTestRecommendation_generateRow" runat="server" visible="false">
                                <td class="header_bg" width="100%" runat="server">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%;" align="left" class="header_text_bold">
                                            </td>
                                            <td style="width: 48%" align="left">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="CreateManualTest_byQuestion_testDraftExpandLinkButton" runat="server"
                                                                Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseRows(this);"></asp:LinkButton>
                                                            <asp:HiddenField ID="ViewTestRecommendation_byQuestion_stateHiddenField" runat="server"
                                                                Value="0" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 2%" align="right">
                                                <span id="ViewTestRecommendation_byQuestion_searchResultsUpSpan" runat="server" style="display: none;">
                                                    <asp:Image ID="ViewTestRecommendation_byQuestion_searchResultsUpImage" runat="server"
                                                        SkinID="sknMinimizeImage" />
                                                </span><span id="ViewTestRecommendation_byQuestion_searchResultsDownSpan" runat="server"
                                                    style="display: block;">
                                                    <asp:Image ID="ViewTestRecommendation_byQuestion_searchResultsDownImage" runat="server"
                                                        SkinID="sknMaximizeImage" />
                                                </span>
                                                <asp:HiddenField ID="ViewTestRecommendation_byQuestion_isMaximizedHiddenField" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="ViewTestRecommendation_questionsRow" style="width: 100%" runat="server" visible="false">
                                <td id="Td2" class="grid_body_bg" runat="server">
                                    <asp:UpdatePanel ID="ViewTestRecommendation_byQuestion_updatePanel" runat="server">
                                        <ContentTemplate>
                                            <div style="width: 100%; overflow: auto; height: 300px;" runat="server" id="ViewTestRecommendation_byQuestion_testDrftDiv"
                                                visible="False">
                                                <asp:GridView ID="ViewTestRecommendation_byQuestion_testDraftGridView" OnRowDataBound="ViewTestRecommendation_byQuestion_testDraftGridView_RowDataBound"
                                                    runat="server" AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white"
                                                    BorderWidth="1px" OnRowCommand="ViewTestRecommendation_byQuestion_testDraftGridView_RowCommand"
                                                    OnSorting="ViewTestRecommendation_byQuestion_testDraftGridView_Sorting" Width="100%">
                                                    <RowStyle CssClass="grid_alternate_row" />
                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                    <HeaderStyle CssClass="grid_header_row" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="CreateAutomaticTes_byQuestion_testDrftRemoveImage" runat="server"
                                                                    SkinID="sknAlertImageButton" CommandName="Select" ToolTip="Flag question" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Question ID">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="QuestionDetailPreviewControl_byQuestion_questionLinkButton" runat="server"
                                                                    Text='<%# Eval("QuestionKey") %>'></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField ReadOnly="True" HeaderText="Category" InsertVisible="False" DataField="CategoryName">
                                                            <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField ReadOnly="True" HeaderText="Subject" InsertVisible="False" DataField="SubjectName">
                                                            <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Question">
                                                            <ItemTemplate>
                                                                <asp:Label ID="SearchQuestion_byQuestion_questionGridLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),20) %>'
                                                                    ToolTip='<%# Eval("Question") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Answer">
                                                            <ItemTemplate>
                                                                <asp:Label ID="SearchQuestion_byQuestion_answerGridLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                    ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Wrap="False" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Complexity">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ViewTestRecommendation_byQuestion_complexityLabel" runat="server"
                                                                    Text='<%# Eval("ComplexityName") %>'></asp:Label>
                                                                <tr>
                                                                    <td colspan="9" style="width: 100%">
                                                                        <div id="ViewTestRecommendation_byQuestion_detailsDiv" runat="server" style="display: none;"
                                                                            class="table_outline_bg">
                                                                            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="popup_question_icon" style="width: 35%" colspan="4">
                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_questionLabel" runat="server" SkinID="sknLabelText"
                                                                                            Text='<%# Eval("Question").ToString()%>' Width="50%"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_padding_left_20" colspan="7">
                                                                                        <asp:PlaceHolder ID="ViewTestRecommendation_byQuestion_answerChoicesPlaceHolder"
                                                                                            runat="server"></asp:PlaceHolder>
                                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_testAreaHeadLabel" runat="server"
                                                                                            SkinID="sknLabelFieldHeaderText" Text="Test Area"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_testAreaTextBox" runat="server"
                                                                                            ReadOnly="true" SkinID="sknLabelFieldText" Text='<%# Eval("TestAreaName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 16%">
                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_complexityHeadLabel" runat="server"
                                                                                            SkinID="sknLabelFieldHeaderText" Text="Complexity"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="ViewTestRecommendation_byQuestion_complexityTextBox" runat="server"
                                                                                            ReadOnly="true" SkinID="sknLabelFieldText" Text='<%# Eval("ComplexityName") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_8">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <%-- popup DIV --%>
                                                                        <a href="#SearchQuestion_focusmeLink" id="ViewTestRecommendation_byQuestion_focusDownLink"
                                                                            runat="server"></a><a href="#" id="ViewTestRecommendation_byQuestion_focusmeLink"
                                                                                runat="server"></a>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </ContentTemplate>
                                        <%--<Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ViewTestRecommendation_topGenerateTestButton" />
                                            <asp:AsyncPostBackTrigger ControlID="ViewTestRecommendation_bottomGenerateTestButton" />
                                        </Triggers>--%>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr id="ViewTestRecommendation_emptyRow" runat="server" visible="false">
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr id="ViewTestRecommendation_summaryRow" runat="server" visible="false">
                                <td width="100%">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="ViewTestRecommendation_byQuestion_automatedControlUpdatePanel"
                                                    runat="server">
                                                    <ContentTemplate>
                                                        <uc2:AutomatedTestControl ID="ViewTestRecommendation_byQuestion_automatedTestUserControl"
                                                            runat="server" />
                                                    </ContentTemplate>
                                                    <%--   <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ViewTestRecommendation_topGenerateTestButton" />
                                                        <asp:AsyncPostBackTrigger ControlID="ViewTestRecommendation_bottomGenerateTestButton" />
                                                    </Triggers>--%>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ViewTestRecommendation_byQuestion_questionPanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_question_detail">
                                        <div style="display: none">
                                            <asp:Button ID="ViewTestRecommendation_byQuestion_hiddenButton" runat="server" Text="Hidden" /></div>
                                        <uc4:QuestionDetailPreviewControl ID="ViewTestRecommendation_byQuestion_QuestionDetailPreviewControl"
                                            runat="server" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ViewTestRecommendation_byQuestion_bottomQuestionModalPopupExtender"
                                        runat="server" PopupControlID="ViewTestRecommendation_byQuestion_questionPanel"
                                        TargetControlID="ViewTestRecommendation_byQuestion_hiddenButton" BackgroundCssClass="modalBackground"
                                        DynamicServicePath="" Enabled="True">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="ViewTestRecommendation_byQuestion_bottomMessagesUpdatePanel"
                        runat="server">
                        <ContentTemplate>
                            <asp:Label ID="ViewTestRecommendation_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewTestRecommendation_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                        <%-- <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ViewTestRecommendation_topGenerateTestButton" />
                            <asp:AsyncPostBackTrigger ControlID="ViewTestRecommendation_bottomGenerateTestButton" />
                        </Triggers>--%>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%">
                                &nbsp;
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="58%" align="right">
                                            <asp:Button ID="ViewTestRecommendation_bottomGenerateTestButton" runat="server" Text="Generate Sample Test"
                                                SkinID="sknButtonId" CausesValidation="False" OnClick="ViewTestRecommendation_generateTestButton_Click" />
                                        </td>
                                         <td align="right" style="width: 30%">
                                            <asp:LinkButton ID="ViewTestRecommendation_bottomResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="ViewTestRecommendation_resetLinkButton_Click" />
                                                &nbsp;|&nbsp;<asp:LinkButton ID="ViewTestRecommendation_bottomCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function ()
        {
            window.setTimeout(focus, 1);
        }
    )
        function focus()
        {
            try
            {
                document.getElementById('<%=ViewTestRecommendation_byQuestion_totalNumberTextBox.ClientID %>').focus();
            }
            catch (Err)
            {
                try
                {
                    document.getElementById('ctl00_OTMMaster_body_ViewTestRecommendation_mainTabContainer_CreateManualTest_testdetailsTabPanel_ViewTestRecommendation_byQuestion_testDetailsUserControl_TestDetailsControl_testNameTextBox').focus();
                }
                catch (Err1)
                {
                }
            }
        }
    </script>
</asp:Content>
