﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestSession.cs
// File that represents the user interface for searching Testsession either by test or testsession.

#endregion Header

#region Directives

using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Collections;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.TestMaker
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the TestSession page. The page is for searching the test session 
    /// either by test or test session. User can Create,view,edit test session.
    /// </summary>
    public partial class TestSession : PageBase
    {
        #region Declarations

        /// <summary>
        /// Enum that is used for to save Session Indexes.
        /// </summary>
        private enum SessionArrayListString
        {
            BYTEST_TESTSEARCH = 0,
            BYTEST_STATICNUMBER = 1,
            BYTEST_GRID_PAGENUMBER = 2,
            BYTEST_GRID_SORTCOLUMNNAME = 3,
            BYTEST_GRID_SORTORDER = 4,
            BYTESTSESSION_TESTSEARCH = 5,
            BYTESTSESSION_STATICNUMBER = 6,
            BYTESTSESSION_GRID_PAGENUMBER = 7,
            BYTESTSESSION_GRID_SORTCOLUMNNAME = 8,
            BYTESTSESSION_GRID_SORTORDER = 9,
            ACTIVETABINDEX = 10,
            BYTEST_SIMPLELINKTEXT = 11,
            BYTEST_ISMAXIMIZED = 12,
            BYTESTSESSION_ISMAXIMIZED = 13
        }
        /// <summary>
        /// A <see cref="string"/>constants that hold the key for the viewstate
        /// which refer to the sort column name in the 
        /// 'search by test session' grid
        /// </summary>
        private const string BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE = "BYTESTSESSION_SORT_COLUMN_NAME";
        /// <summary>
        /// A <see cref="string"/>constants that hold the key for the viewstate
        /// which refer to the sort column order in the 
        /// 'search by test session' grid
        /// </summary>
        private const string BYTESTSESSION_SORT_ORDER_VIEWSTATE = "BYTESTSESSION_SORT_ORDER";
        /// <summary>
        /// A <see cref="string"/>constants that hold the key for the viewstate
        /// which refer to the sort column name in the 
        /// 'search by test' grid
        /// </summary>
        private const string BYTEST_SORT_COLUMN_NAME_VIEWSTATE = "BYTEST_SORT_COLUMN_NAME";
        /// <summary>
        /// A <see cref="string"/>constants that hold the key for the viewstate
        /// which refer to the sort column order in the 
        /// 'search by test' grid
        /// </summary>
        private const string BYTEST_SORT_ORDER_VIEWSTATE = "BYTEST_SORT_ORDER";
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Declarations

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loading.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                TestSession_byTestSession_testSessionPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                        (TestSession_byTestSession_testSessionPagingNavigator_PageNumberClick);
                TestSession_byTest_testPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                        (TestSession_byTest_testPagingNavigator_PageNumberClick);
                TestSession_byTest_categorySubjectControl.ControlMessageThrown += new
                    CategorySubjectControl.ControlMessageThrownDelegate
                    (TestSession_byTest_categorySubjectControl_ControlMessageThrown);
                foreach (MultiHandleSliderTarget target in TestSession_byTest_noofQuestionsMultiHandleSliderExtender.MultiHandleSliderTargets)
                    target.ControlID = TestSession_byTest_noofQuestionsMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;
                //foreach (MultiHandleSliderTarget target in TestSession_testCostMultiHandleSliderExtender.MultiHandleSliderTargets)
                //    target.ControlID = TestSession_testCostMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;
                ClearAllLabelMessage();
                CheckAndSetExpandorRestore();

                //Add attributes for the credits earned link button 
                //Pass query string as test session 
                TestSession_creditsEarnedLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowCredit('TestSession','" + base.userID + "');");

                //page title
                Master.SetPageCaption(Resources.HCMResource.TestSession_Title);
                if (!IsPostBack)
                {
                    if ((!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU 
                        || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING
                        || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_ASSESSMENT
                        ))
                        base.ClearSearchCriteriaSession();

                    //Change the session created by  as read only 
                    TestSession_byTestSession_testSessionCreatorTextBox.ReadOnly = true;
                    TestSession_byTestSession_testSessionCreatorImageButton.Visible = false;


                    TestSession_byTest_testGridViewDiv.Visible = false;
                    TestSession_byTestSession_testSessionGridViewDiv.Visible = false;

                    SetAuthorDetails();
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when user clicks simple link button
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_byTest_SimpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                SetVisibleForSimpleOrAdvanced();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler event that will reset the page when user cliks on the reset link button
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST] = null;
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void TestSession_byTest_clearLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (TestSession_mainTabContainer.ActiveTabIndex == 0)
                    ClearByTestControls();
                else
                    ClearByTestSessionControls();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                TestSession_byTestSession_testSessionGridViewUpdatePanel.Update();
                TestSession_byTest_GridVeiwUpdatePanel.Update();
                TestSession_byTest_testSessionPageNavigationUpdatePanel.Update();
                TestSession_byTestSession_testSessionPageNavigationUpdatePanel.Update();
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void TestSession_byTest_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(TestSession_topErrorMessageLabel,
                            TestSession_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(TestSession_topErrorMessageLabel,
                            TestSession_bottomErrorMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // To maintain session author name and schedule creator when the page is post backed.
                TestSession_byTestSession_testSessionCreatorTextBox.Text =
                    Request[TestSession_byTestSession_testSessionCreatorTextBox.UniqueID].Trim();


                if (TestSession_mainTabContainer.ActiveTabIndex == 0)
                {
                    ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE] = "TESTNAME";
                    ViewState[BYTEST_SORT_ORDER_VIEWSTATE] = SortType.Ascending;
                    TestSession_byTest_testPagingNavigator.Reset();
                    TestSession_byTest_categoryTextBox.Focus();
                }
                if (TestSession_mainTabContainer.ActiveTabIndex == 1)
                {
                    ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = SortType.Descending;
                    ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE] = "TESTSESSIONID";
                    TestSession_byTestSession_testSessionPagingNavigator.Reset();
                    TestSession_byTestSession_candidateSessionIdTextBox.Focus();
                }
                LoadDetails(1);
                TestSession_byTest_noofQuestionsMultiHandleSliderExtender.ClientState = "0";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                TestSession_bottomMessagesUpdatePanel.Update();
                TestSession_topMessagesUpdatePanel.Update();
                TestSession_byTestSession_testSessionGridViewUpdatePanel.Update();
                TestSession_byTest_GridVeiwUpdatePanel.Update();
                TestSession_byTest_testSessionPageNavigationUpdatePanel.Update();
                TestSession_byTestSession_testSessionPageNavigationUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_saveCancellationButton_Click(object sender, EventArgs e)
        {
            try
            {
                CancelTestSession();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the 
        /// testsession_pagingNavigator control.
        /// This binds the data into the grid for the pagenumber.
        /// </summary>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e">The 
        /// <see cref="Adm.IT.eAdm.eLogistics.Common.BL.PageNumberEventArgs"/> 
        /// instance containing the event data.</param>
        protected void TestSession_byTestSession_testSessionPagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadDetails(e.PageNumber);
                TestSession_byTestSession_testSessionGridViewUpdatePanel.Update();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void TestSession_byTest_testPagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadDetails(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                TestSession_byTest_GridVeiwUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the test session grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_byTestSession_testSessionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ImageButton bySession_CandidateDetailImageButton = null;
            ImageButton bySession_CancelReasonImageButton = null;
            ImageButton bySession_CancelImageButton = null;
            HiddenField bySession_CancelReasonHiddenField = null;
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                bySession_CandidateDetailImageButton = (ImageButton)e.Row.FindControl("TestSession_byTestSession_candidateDetailImageButton");
                if (Convert.ToBoolean(bySession_CandidateDetailImageButton.CommandArgument))
                    bySession_CandidateDetailImageButton.Visible = true;
                else
                    bySession_CandidateDetailImageButton.Visible = false;
                bySession_CancelReasonImageButton = (ImageButton)e.Row.FindControl("TestSession_byTestSession_cancelReasonImageButton");
                bySession_CancelReasonHiddenField = (HiddenField)e.Row.FindControl("TestSession_byTestSession_cancelReasonHiddenField");
                bySession_CancelImageButton = (ImageButton)e.Row.FindControl("TestSession_byTestSession_cancelTestSessionImageButton");
                if ((bySession_CancelReasonHiddenField != null) &&
                        (bySession_CancelReasonHiddenField.Value == ""))
                    bySession_CancelReasonImageButton.Visible = false;
                else
                {
                    bySession_CancelImageButton.Visible = false;
                    bySession_CancelReasonImageButton.Attributes.Add("onclick", "return ViewCancelReason('" +
                            bySession_CancelReasonHiddenField.Value + "');");
                }
                if (!Convert.ToBoolean(bySession_CancelImageButton.CommandArgument))
                    bySession_CancelImageButton.Visible = false;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (bySession_CandidateDetailImageButton != null) bySession_CandidateDetailImageButton = null;
                if (bySession_CancelReasonImageButton != null) bySession_CancelReasonImageButton = null;
                if (bySession_CancelImageButton != null) bySession_CancelImageButton = null;
                if (bySession_CancelReasonHiddenField != null) bySession_CancelReasonHiddenField = null;
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the test grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_byTest_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ImageButton byTest_ViewSessionImageButton = null;
            ImageButton byTest_CreateNewSessionImageButton = null;
            HiddenField byTest_TestStatusHiddenField = null;
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                byTest_TestStatusHiddenField = (HiddenField)e.Row.FindControl("TestSession_byTest_TestStatusHiddenField");
                byTest_CreateNewSessionImageButton = (ImageButton)
                    e.Row.FindControl("TestSession_byTest_createNewImageButton");
                if (byTest_TestStatusHiddenField.Value == "Active")
                    byTest_CreateNewSessionImageButton.Visible = true;
                else
                    byTest_CreateNewSessionImageButton.Visible = false;
                byTest_ViewSessionImageButton =
                ((ImageButton)e.Row.FindControl("TestSession_byTest_viewSessionsImageButton"));
                byTest_ViewSessionImageButton.Attributes.Add("onclick", "SetBySessionTestIDTextBox('" +
                            byTest_ViewSessionImageButton.CommandArgument + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (byTest_ViewSessionImageButton != null) byTest_ViewSessionImageButton = null;
                if (byTest_CreateNewSessionImageButton != null) byTest_CreateNewSessionImageButton = null;
                if (byTest_TestStatusHiddenField != null) byTest_TestStatusHiddenField = null;
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the test session grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void TestSession_byTestSession_testSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE].ToString() == e.SortExpression)
                {
                    ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = ((SortType)ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = SortType.Descending;
                else
                    ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = SortType.Ascending;


                e.SortDirection = ((SortDirection)ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE]) == SortDirection.Ascending ?
                        SortDirection.Ascending : SortDirection.Descending;
                
                ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE] = e.SortExpression;
                TestSession_byTestSession_testSessionPagingNavigator.Reset();
                LoadDetails(1);
                TestSession_byTestSession_testSessionPageNavigationUpdatePanel.Update();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the test session grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void TestSession_byTestSession_testSessionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex(TestSession_byTestSession_testSessionGridView,
                    ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE].ToString());
                if (sortColumnIndex != -1)
                    AddSortImage(sortColumnIndex, e.Row, (SortType)ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE]);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_byTestSession_testSessionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               
                if (e.CommandName == "viewTestSession")
                {
                    TestSession_previewTestSessionControl_userControl.DataSource =
                        new TestSchedulerBLManager().GetTestSessionDetail(e.CommandArgument.ToString(), "dummy",base.userID);
                    TestSession_previewTestSessionControl_modalpPopupExtender.Show();
                }
                else if (e.CommandName == "Cancel Test")
                {
                    ViewState["CANDIDATE_SESSION_ID"] =
                        ((Label)((ImageButton)e.CommandSource).
                        FindControl("TestSession_byTestSession_candidateSessionIdLabel")).Text;
                    ViewState["ATTEMPT_ID"] = ((HiddenField)((ImageButton)e.CommandSource).FindControl("TestSession_byTestSession_attemptIDHiddenField")).Value;
                    TestSession_cancelErrorMessageLabel.Text = "";
                    TestSession_cancelTestReasonTextBox.Text = "";
                    TestSession_cancelSessionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Candidate Detail")
                {
                    CandidateTestDetail candidateDetail = new CandidateTestDetail();
                    /*Added by MKN 06-Apr-2012*/
                    candidateDetail.CandidatID =Convert.ToInt32(((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("TestSession_byTestSession_candidateInfoIDHiddenField")).Value);
                    /**/
                    candidateDetail.CandidateSessionID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("TestSession_byTestSession_candidateSessionIdLabel")).Text;
                    candidateDetail.AttemptID = int.Parse(((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("TestSession_byTestSession_attemptIDHiddenField")).Value);
                    candidateDetail.TestID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("TestSession_byTestSession_testKeyLabel")).Text;
                    candidateDetail.CandidateName = ((Label)((ImageButton)e.CommandSource).
                        FindControl("TestSession_byTestSession_candidateFullNameLabel")).Text;
                    candidateDetail.ShowTestScore =Convert.ToBoolean(((HiddenField)((ImageButton)e.CommandSource).
               FindControl("TestSession_byTestSession_showTestScoreHiddenField")).Value);

                    TestSession_candidateDetailControl.Datasource = candidateDetail;
                    TestSession_candidateDetailModalPopupExtender.CancelControlID = ((ImageButton)TestSession_candidateDetailControl.
                       FindControl("CandidateDetailControl_topCancelImageButton")).ClientID;
                    TestSession_candidateDetailModalPopupExtender.Show();
                }
                else if (e.CommandName == "Edit Test Session")
                {
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST]))
                        ((ArrayList)Session[Forte.HCM.Support.Constants.
                            SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST])
                            [(int)SessionArrayListString.ACTIVETABINDEX] = 1;
                    Response.Redirect(string.Format("~/TestMaker/EditTestSession.aspx?m=1&s=3&testsessionid={0}&parentpage=S_TSN",
                        e.CommandArgument), false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the test grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void TestSession_byTest_testGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE].ToString() == e.SortExpression)
                {
                    ViewState[BYTEST_SORT_ORDER_VIEWSTATE] = ((SortType)ViewState[BYTEST_SORT_ORDER_VIEWSTATE]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState[BYTEST_SORT_ORDER_VIEWSTATE] = SortType.Descending;
                else
                    ViewState[BYTEST_SORT_ORDER_VIEWSTATE] = SortType.Ascending;


                e.SortDirection = ((SortDirection)ViewState[BYTEST_SORT_ORDER_VIEWSTATE]) == SortDirection.Ascending ?
                        SortDirection.Ascending : SortDirection.Descending;

                ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE] = e.SortExpression;
                TestSession_byTest_testPagingNavigator.Reset();
                LoadDetails(1);
                TestSession_byTest_testSessionPageNavigationUpdatePanel.Update();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_byTest_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewSessions")
                {
                    TestSession_mainTabContainer.ActiveTabIndex = 1;
                    ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = SortType.Ascending;
                    ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE] = "TESTSESSIONID";
                    TestSession_byTestSession_testIdTextBox.Text = e.CommandArgument.ToString();
                    LoadDetails(1);
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST]))
                        ((ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.
                            SEARCH_TEST_SESSION_BY_TEST])[(int)SessionArrayListString.ACTIVETABINDEX] = 0;
                    TestSession_byTestSession_testSessionGridViewUpdatePanel.Update();
                    TestSession_byTestSession_testSessionPageNavigationUpdatePanel.Update();
                }
                else if (e.CommandName == "Create Test Session")
                {
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST]))
                        ((ArrayList)Session[Forte.HCM.Support.Constants.
                            SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST])[(int)SessionArrayListString.ACTIVETABINDEX] = 0;
                    Response.Redirect("~/TestMaker/CreateTestSession.aspx?m=1&s=3&parentpage=S_TSN&testkey=" +
                        e.CommandArgument, false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the test grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void TestSession_byTest_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex(TestSession_byTest_testGridView,
                ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE].ToString());
                if (sortColumnIndex != -1)
                    AddSortImage(sortColumnIndex, e.Row, (SortType)ViewState[BYTEST_SORT_ORDER_VIEWSTATE]);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when user clicks cancel link button
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestSession_CancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST] = null;
                Response.Redirect("~/OTMHome.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Public Methods

        /// <summary>
        /// Method that will show pop up extender when download button is clicked on user control.
        /// </summary>
        public void ShowModalPopup()
        {
            TestSession_candidateDetailModalPopupExtender.Show();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Clear all error and success label messages
        /// </summary>
        private void ClearAllLabelMessage()
        {
            TestSession_bottomSuccessMessageLabel.Text = string.Empty;
            TestSession_topErrorMessageLabel.Text = string.Empty;
            TestSession_topSuccessMessageLabel.Text = string.Empty;
            TestSession_topErrorMessageLabel.Text = string.Empty;
            TestSession_bottomErrorMessageLabel.Text = string.Empty;
            TestSession_bottomMessagesUpdatePanel.Update();
            TestSession_topMessagesUpdatePanel.Update();
        }

        /// <summary>
        /// Sets the visibility status according for simple and advanced Controls.
        /// </summary>
        private void SetVisibleForSimpleOrAdvanced()
        {
            if (TestSession_byTest_SimpleLinkButton.Text == "Simple")
            {
                TestSession_byTest_SimpleLinkButton.Text = "Advanced";
                TestSession_byTest_simpleSearchDiv.Visible = true;
                TestSession_byTest_advanceSearchDiv.Visible = false;
                TestSession_byTest_categoryTextBox.Focus();
            }
            else if (TestSession_byTest_SimpleLinkButton.Text == "Advanced")
            {
                TestSession_byTest_SimpleLinkButton.Text = "Simple";
                TestSession_byTest_simpleSearchDiv.Visible = false;
                TestSession_byTest_advanceSearchDiv.Visible = true;
                TestSession_byTest_testIdTextBox.Focus();
            }
            TestSession_byTest_searchDivUpdatePanel.Update();
        }

        /// <summary>
        /// Load the data to the test session gridview
        /// </summary>
        /// <param name="PageNumber">Page number of the grid to be load in to the gridview</param>
        private void LoadSearchbyTestSessionDetails(int PageNumber)
        {
            ArrayList arrayList = null;
            TestSessionSearchCriteria testSessionSearchCriteria = null;
            try
            {
                TestSession_byTestSession_pageNumberHiddenField.Value = PageNumber.ToString();
                arrayList = (ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST];
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList))
                    arrayList = new ArrayList();
                AddNullValuesToArrayList(ref arrayList);
                testSessionSearchCriteria = GetSessionSearchCriteria();
                arrayList[(int)SessionArrayListString.BYTESTSESSION_TESTSEARCH] = testSessionSearchCriteria;
                arrayList[(int)SessionArrayListString.BYTESTSESSION_STATICNUMBER] = 2;
                arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_PAGENUMBER] = PageNumber;
                arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTCOLUMNNAME] = ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE].ToString();
                arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTORDER] = (SortType)ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE];
                arrayList[(int)SessionArrayListString.ACTIVETABINDEX] = 1;
                Session[Forte.HCM.Support.Constants.
                    SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST] = arrayList;
                LoadByTestSessionGrid(testSessionSearchCriteria, PageNumber,
                    ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE].ToString(),
                    (SortType)ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE]);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList)) arrayList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria))
                    testSessionSearchCriteria = null;
            }
        }

        /// <summary>
        /// This method all null values to the array list.
        /// The array list which we are saving in session.
        /// (Note:- this parameter you need to pass by using 'ref' keyword
        /// </summary>
        /// <param name="arrayList">array list to bind the values.</param>
        private void AddNullValuesToArrayList(ref ArrayList arrayList)
        {
            for (int i = arrayList.Count; i < Enum.GetNames(typeof(SessionArrayListString)).Length; i++)
                arrayList.Add(null);
        }

        /// <summary>
        /// This method fills the grid view in serach by test session tab.
        /// </summary>
        /// <param name="testSessionSearchCriteria">search crtieria
        /// provided but the user</param>
        /// <param name="PageNumber">Page number of the grid
        /// to be load in to the grid view</param>
        /// <param name="SortOrderColumnName">Column name to sort in
        /// the grid view</param>
        /// <param name="sortType">Sort direction type</param>
        private void LoadByTestSessionGrid(TestSessionSearchCriteria testSessionSearchCriteria,
            int PageNumber, string SortOrderColumnName, SortType sortType)
        {
            int totalNoOfRecords = 0;
            TestSession_byTestSession_testSessionGridView.DataSource = new TestSessionBLManager().
                GetTestSessionDetails(testSessionSearchCriteria, PageNumber,
                base.GridPageSize, SortOrderColumnName, sortType, out totalNoOfRecords);

            TestSession_byTestSession_testSessionGridView.DataBind();
            if (TestSession_byTestSession_testSessionGridView.Rows.Count == 0)
            {
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                TestSession_byTestSession_testSessionPagingNavigator.TotalRecords = 0;
                TestSession_byTestSession_testSessionGridViewDiv.Visible = false;
            }
            else
            {
                TestSession_byTestSession_testSessionPagingNavigator.PageSize = base.GridPageSize;
                TestSession_byTestSession_testSessionPagingNavigator.TotalRecords = totalNoOfRecords;
                TestSession_byTestSession_testSessionGridViewDiv.Visible = true;
            }
            TestSession_byTestSession_testSessionPageNavigationUpdatePanel.Update();
        }

        /// <summary>
        /// To get the test session serach criteria whether user provided any input for search
        /// </summary>
        /// <returns>test session search object contains the 
        /// user provided search criteria.</returns>
        private TestSessionSearchCriteria GetSessionSearchCriteria()
        {
            TestSessionSearchCriteria testSessionSearchCriteria = null;
            try
            {
                testSessionSearchCriteria = new TestSessionSearchCriteria();
                if (TestSession_byTestSession_candidateSessionIdTextBox.Text.Trim() != string.Empty)
                    testSessionSearchCriteria.CandidateSessionID = TestSession_byTestSession_candidateSessionIdTextBox.Text.Trim();
                else
                    testSessionSearchCriteria.CandidateSessionID = null;
                if (TestSession_byTestSession_testSessionIdTextBox.Text.Trim() != string.Empty)
                    testSessionSearchCriteria.SessionKey = TestSession_byTestSession_testSessionIdTextBox.Text.Trim();
                else
                    testSessionSearchCriteria.SessionKey = null;
                if (TestSession_byTestSession_testIdTextBox.Text.Trim() != string.Empty)
                    testSessionSearchCriteria.TestKey = TestSession_byTestSession_testIdTextBox.Text.Trim();
                else
                    testSessionSearchCriteria.TestKey = null;
                if (TestSession_byTestSession_testNameTextBox.Text.Trim() != string.Empty)
                    testSessionSearchCriteria.TestName = TestSession_byTestSession_testNameTextBox.Text.Trim();
                else
                    testSessionSearchCriteria.TestName = null;
                if (TestSession_byTestSession_schedulerNameTextBox.Text.Trim() != string.Empty)
                    testSessionSearchCriteria.SchedulerName = TestSession_byTestSession_schedulerNameTextBox.Text.Trim();
                else
                    testSessionSearchCriteria.SchedulerName = null;
                if (TestSession_byTestSession_candidateNameTextBox.Text.Trim() != string.Empty)
                    testSessionSearchCriteria.CandidateName = TestSession_byTestSession_candidateNameTextBox.Text.Trim();
                else
                    testSessionSearchCriteria.CandidateName = null;

                if (TestSession_byTestSession_testSessionCreatorTextBox.Text.Trim() != string.Empty)
                    testSessionSearchCriteria.TestSessionCreator = TestSession_byTestSession_testSessionCreatorTextBox.Text.Trim();
                else
                    testSessionSearchCriteria.TestSessionCreator = null;


                if (TestSession_byTestSession_schedulerIdHiddenField.Value.Trim().Length != 0)
                    testSessionSearchCriteria.SchedulerNameID = int.Parse
                        (TestSession_byTestSession_schedulerIdHiddenField.Value.Trim());
                else
                    testSessionSearchCriteria.SchedulerNameID = 0;

                if (TestSession_byTestSession_testSessionCreatorIdHiddenField.Value.Trim().Length != 0)
                    testSessionSearchCriteria.TestSessionCreatorID = int.Parse(TestSession_byTestSession_testSessionCreatorIdHiddenField.Value.Trim());
                else
                    testSessionSearchCriteria.TestSessionCreatorID = 0;

                if (TestSession_byTestSession_positionProfileIDHiddenField.Value == null ||
                    TestSession_byTestSession_positionProfileIDHiddenField.Value.Trim().Length == 0)
                {
                    testSessionSearchCriteria.PositionProfileID = 0;
                }
                else
                {
                    testSessionSearchCriteria.PositionProfileID = Convert.ToInt32(TestSession_byTestSession_positionProfileIDHiddenField.Value);
                }
                testSessionSearchCriteria.PositionProfileName = TestSession_byTestSession_positionProfileTextBox.Text;
                
                return testSessionSearchCriteria;
            }
            finally
            {
                if (testSessionSearchCriteria != null) testSessionSearchCriteria = null;
            }
        }

        /// <summary>
        /// this method will decide which grid has to bind either test grid or test session grid
        /// </summary>
        /// <param name="pageNumber">Page number of the grid to be load in to the gridview</param>
        private void LoadDetails(int pageNumber)
        {
            if (TestSession_mainTabContainer.ActiveTabIndex == 0)
                LoadSearchbyTestDetails(pageNumber);
            else
                LoadSearchbyTestSessionDetails(pageNumber);
        }

        /// <summary>
        /// To load the tests
        /// </summary>
        /// <param name="PageNumber">Page number of the grid to be load
        /// in to the gridview</param>
        private void LoadSearchbyTestDetails(int PageNumber)
        {
            TestSearchCriteria testSearchCriteria = null;
            ArrayList arrayList = null;
            try
            {
                arrayList = (ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST];
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList))
                    arrayList = new ArrayList();
                AddNullValuesToArrayList(ref arrayList);
                testSearchCriteria = GetTestSearchCriteria();
                arrayList[(int)SessionArrayListString.BYTEST_TESTSEARCH] = testSearchCriteria;
                arrayList[(int)SessionArrayListString.BYTEST_STATICNUMBER] = 1;
                arrayList[(int)SessionArrayListString.BYTEST_GRID_PAGENUMBER] = PageNumber;
                arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTCOLUMNNAME] = ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE].ToString();
                arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTORDER] = (SortType)ViewState[BYTEST_SORT_ORDER_VIEWSTATE];
                arrayList[(int)SessionArrayListString.ACTIVETABINDEX] = 0;
                if (TestSession_byTest_SimpleLinkButton.Text == "Advanced")
                    arrayList[(int)SessionArrayListString.BYTEST_SIMPLELINKTEXT] = "Advanced";
                else
                    arrayList[(int)SessionArrayListString.BYTEST_SIMPLELINKTEXT] = "Simple";
                Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST] = arrayList;
                LoadByTestGrid(testSearchCriteria, PageNumber,
                    ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE].ToString(),
                    (SortType)ViewState[BYTEST_SORT_ORDER_VIEWSTATE]);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList)) arrayList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
            }
        }

        /// <summary>
        /// This Method fills the grid view in search by test tab.
        /// </summary>
        /// <param name="testSearchCriteria">search criteria provided 
        /// by the user</param>
        /// <param name="PageNumber">Page number of the grid to be load
        /// in to the gridview</param>
        /// <param name="SortColumnName">Column name to sort in grid view</param>
        /// <param name="direction">sort direction type</param>
        private void LoadByTestGrid(TestSearchCriteria testSearchCriteria, int PageNumber,
            string SortColumnName, SortType direction)
        {
            int totalNoOfRecords = 0;
            TestSession_byTest_testGridView.DataSource = new TestBLManager().GetTestForTestSession(
                    testSearchCriteria, PageNumber, base.GridPageSize, SortColumnName,
                    direction, out totalNoOfRecords);
            TestSession_byTest_testGridView.DataBind();
            if (TestSession_byTest_testGridView.Rows.Count == 0)
            {
                base.ShowMessage(TestSession_topErrorMessageLabel,
                    TestSession_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                TestSession_byTest_testPagingNavigator.TotalRecords = 0;
                TestSession_byTest_testGridViewDiv.Visible = false;
            }
            else
            {
                TestSession_byTest_testPagingNavigator.PageSize = base.GridPageSize;
                TestSession_byTest_testPagingNavigator.TotalRecords = totalNoOfRecords;
                TestSession_byTest_testGridViewDiv.Visible = true;
            }
            TestSession_byTest_testSessionPageNavigationUpdatePanel.Update();
        }

        /// <summary>
        /// Gets the by test tab serach criteria given by the user
        /// </summary>
        /// <returns></returns>
        private TestSearchCriteria GetTestSearchCriteria()
        {
            TestSearchCriteria testSearchCriteria = null;
            List<Subject> categorySubjects = null;
            try
            {
                testSearchCriteria = new TestSearchCriteria();
                if (TestSession_byTest_SimpleLinkButton.Text == "Advanced")
                {
                    testSearchCriteria.Category = TestSession_byTest_categoryTextBox.Text.Trim() == "" ?
                                        null : TestSession_byTest_categoryTextBox.Text.Trim();
                    testSearchCriteria.Subject = TestSession_byTest_subjectTextBox.Text.Trim() == "" ?
                                        null : TestSession_byTest_subjectTextBox.Text.Trim();
                    testSearchCriteria.Keyword = TestSession_byTest_keywordsTextBox.Text.Trim() == "" ?
                                        null : TestSession_byTest_keywordsTextBox.Text.Trim();
                    testSearchCriteria.QuestionType = (QuestionType)Convert.ToInt32(TestSession_byTest_testTypeDropDownList.SelectedValue);

                    testSearchCriteria.TestAuthorID = base.userID;
                    testSearchCriteria.ShowCopiedTests = true;
                }
                else
                {
                    testSearchCriteria.QuestionType = (QuestionType)Convert.ToInt32(TestSession_byTest_advTestTypeDropDownList.SelectedValue);
                    StringBuilder SelectedSubjectIDs = null;
                    StringBuilder testAreaID = null;
                    /// To get Selected Test Area IDs.
                    for (int i = 0; i < TestSession_byTest_testAreaCheckBoxList.Items.Count; i++)
                        if (TestSession_byTest_testAreaCheckBoxList.Items[i].Selected)
                        {
                            if (Forte.HCM.Support.Utility.IsNullOrEmpty(testAreaID))
                                testAreaID = new StringBuilder();
                            testAreaID.Append(TestSession_byTest_testAreaCheckBoxList.Items[i].Value);
                            testAreaID.Append(",");
                        }
                    categorySubjects = new List<Subject>();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestSession_byTest_categorySubjectControl.SubjectDataSource))
                    {
                        List<Category> categoryList = new List<Category>();
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(SelectedSubjectIDs))
                            SelectedSubjectIDs = new StringBuilder();
                        foreach (Subject subject in TestSession_byTest_categorySubjectControl.SubjectDataSource)
                        {
                            Subject subjectCat = new Subject();
                            if (subject.IsSelected)
                            {
                                Category category = new Category();
                                SelectedSubjectIDs.Append(subject.SubjectID.ToString() + ",");
                                category.CategoryID = subject.CategoryID;
                                categoryList.Add(category);
                            }
                            // Get all the subjects and categories and then assign to the list.
                            subjectCat.CategoryID = subject.CategoryID;
                            subjectCat.CategoryName = subject.CategoryName;
                            subjectCat.SubjectID = subject.SubjectID;
                            subjectCat.SubjectName = subject.SubjectName;
                            subjectCat.IsSelected = subject.IsSelected;
                            categorySubjects.Add(subjectCat);
                        }
                        //Get all Subject ids in  Un selected Subject List.
                        Category categoryID = new Category();
                        foreach (Subject subject in TestSession_byTest_categorySubjectControl.SubjectDataSource)
                        {
                            categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                            if (categoryID == null)
                                SelectedSubjectIDs.Append(subject.SubjectID.ToString() + ",");
                        }
                    }
                    ////
                    testSearchCriteria.Subjects = categorySubjects;
                    if (TestSession_byTest_certificateTestDropDownList.SelectedValue != "S")
                        testSearchCriteria.IsCertification = (TestSession_byTest_certificateTestDropDownList.SelectedValue
                            == "Y") ? true : false;
                    testSearchCriteria.CategoriesID = Forte.HCM.Support.Utility.IsNullOrEmpty(SelectedSubjectIDs) ?
                                    null : SelectedSubjectIDs.ToString().TrimEnd(',');
                    testSearchCriteria.TestAreasID = Forte.HCM.Support.Utility.IsNullOrEmpty(testAreaID) ?
                                    null : testAreaID.ToString().TrimEnd(',');
                    testSearchCriteria.TestKey = TestSession_byTest_testIdTextBox.Text.Trim() == "" ?
                                    null : TestSession_byTest_testIdTextBox.Text.Trim();
                    testSearchCriteria.Name = TestSession_byTest_testNameTextBox.Text.Trim() == "" ?
                                    null : TestSession_byTest_testNameTextBox.Text.Trim();
                    testSearchCriteria.PositionProfileID = (TestSession_byTest_positionProfileIDHiddenField.Value == null ||TestSession_byTest_positionProfileIDHiddenField.Value.Trim().Length == 0) ?
                                    0 : Convert.ToInt32(TestSession_byTest_positionProfileIDHiddenField.Value);
                    testSearchCriteria.PositionProfileName = TestSession_byTest_positionProfileTextBox.Text;
                    testSearchCriteria.Keyword = TestSession_byTest_keywordAdvanceTextBox.Text.Trim() == "" ?
                                    null : TestSession_byTest_keywordAdvanceTextBox.Text.Trim();
                    testSearchCriteria.TestAuthorName = TestSession_byTest_authorNameTextBox.Text.Trim() == "" ?
                                  null : TestSession_byTest_autherNameHiddenField.Value;
                    //testSearchCriteria.TestCostStart = int.Parse(TestSession_byTest_testCostMinValueTextBox.Text.Trim());
                    //testSearchCriteria.TestCostEnd = int.Parse(TestSession_byTest_testCostMaxValueTextBox.Text.Trim());
                    testSearchCriteria.TotalQuestionStart = int.Parse(TestSession_byTest_noOfQuestionsMinValueTextBox.Text.Trim());
                    testSearchCriteria.TotalQuestionEnd = int.Parse(TestSession_byTest_noOfQuestionsMaxValueTextBox.Text.Trim());
                    testSearchCriteria.TestAuthorID = base.userID;
                    testSearchCriteria.ShowCopiedTests = TestSession_byTest_showCopiedTestsCheckBox.Checked;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testAreaID)) testAreaID = null;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SelectedSubjectIDs)) SelectedSubjectIDs = null;
                }

                return testSearchCriteria;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(categorySubjects)) categorySubjects = null;
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestSession_byTest_isMaximizedHiddenField.Value) &&
                         TestSession_byTest_isMaximizedHiddenField.Value == "Y")
            {
                TestSession_byTest_searchByTestDiv.Style["display"] = "none";
                TestSession_byTest_searchResultsUpSpan.Style["display"] = "block";
                TestSession_byTest_searchResultsDownSpan.Style["display"] = "none";
                TestSession_byTest_testGridViewDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TestSession_byTest_searchByTestDiv.Style["display"] = "block";
                TestSession_byTest_searchResultsUpSpan.Style["display"] = "none";
                TestSession_byTest_searchResultsDownSpan.Style["display"] = "block";
                TestSession_byTest_testGridViewDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestSession_byTestSession_isMaximizedHiddenField.Value) &&
                         TestSession_byTestSession_isMaximizedHiddenField.Value == "Y")
            {
                TestSession_byTestSession_searchByTestSessionDiv.Style["display"] = "none";
                TestSession_byTestSession_searchSessionResultsUpSpan.Style["display"] = "block";
                TestSession_byTestSession_searchSessionResultsDownSpan.Style["display"] = "none";
                TestSession_byTestSession_testSessionGridViewDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TestSession_byTestSession_searchByTestSessionDiv.Style["display"] = "block";
                TestSession_byTestSession_searchSessionResultsUpSpan.Style["display"] = "none";
                TestSession_byTestSession_searchSessionResultsDownSpan.Style["display"] = "block";
                TestSession_byTestSession_testSessionGridViewDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST]))
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestSession_byTest_isMaximizedHiddenField.Value))
                    ((ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST])
                        [(int)SessionArrayListString.BYTEST_ISMAXIMIZED] = TestSession_byTest_isMaximizedHiddenField.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestSession_byTestSession_isMaximizedHiddenField.Value))
                    ((ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST])
                        [(int)SessionArrayListString.BYTESTSESSION_ISMAXIMIZED] = TestSession_byTestSession_isMaximizedHiddenField.Value;
            }
        }

        /// <summary>
        /// Load all javascript tags
        /// </summary>
        private void SubscribeClientSideHandlers()
        {
            TestSession_byTest_searchTestResultsTR.Attributes.Add("onclick", "ExpandOrRestore('" +
                TestSession_byTest_testGridViewDiv.ClientID + "','" +
                TestSession_byTest_searchByTestDiv.ClientID + "','" +
                TestSession_byTest_searchResultsUpSpan.ClientID + "','" +
                TestSession_byTest_searchResultsDownSpan.ClientID + "','" +
                TestSession_byTest_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
            TestSession_byTestSession_searchTestSessionResultsTR.Attributes.Add("onclick", "ExpandOrRestore('" +
                TestSession_byTestSession_testSessionGridViewDiv.ClientID + "','" +
                TestSession_byTestSession_searchByTestSessionDiv.ClientID + "','" +
                TestSession_byTestSession_searchSessionResultsUpSpan.ClientID + "','" +
                TestSession_byTestSession_searchSessionResultsDownSpan.ClientID + "','" +
                TestSession_byTestSession_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
            TestSession_byTest_authorImageButton.Attributes.Add("onclick",
               "return LoadAdminName('" + TestSession_byTest_autherIdHiddenField.ClientID + "','" +
               TestSession_byTest_autherNameHiddenField.ClientID + "','" +
                TestSession_byTest_authorNameTextBox.ClientID + "','TS')");
            TestSession_byTest_positionProfileImageButton.Attributes.Add("onclick",
               "return LoadPositionProfileName('" + TestSession_byTest_positionProfileTextBox.ClientID + "','" +
               TestSession_byTest_positionProfileIDHiddenField.ClientID + "');");
            TestSession_byTestSession_schedulerNameImageButton.Attributes.Add("onclick",
               "return LoadAdminName('" + TestSession_byTestSession_schedulerNameHiddenField.ClientID + "','" +
               TestSession_byTestSession_schedulerIdHiddenField.ClientID + "','" +
                TestSession_byTestSession_schedulerNameTextBox.ClientID + "','TC')");
            TestSession_byTestSession_candidateNameImageButton.Attributes.Add("onclick",
               "return LoadCandidate('" + TestSession_byTestSession_candidateNameTextBox.ClientID + "','" +
               TestSession_byTestSession_candidateEmailHiddenField.ClientID + "','" +
               TestSession_byTestSession_candidateIdHiddenField.ClientID + "');");
            TestSession_byTestSession_positionProfileImageButton.Attributes.Add("onclick",
               "return LoadPositionProfileName('" + TestSession_byTestSession_positionProfileTextBox.ClientID + "','" +
               TestSession_byTestSession_positionProfileIDHiddenField.ClientID + "');");
            TestSession_byTestSession_testSessionCreatorImageButton.Attributes.Add("onclick",
              "return LoadAdminName('" + TestSession_byTestSession_testSessionCreatorHiddenField.ClientID + "','" +
              TestSession_byTestSession_testSessionCreatorIdHiddenField.ClientID + "','" +
               TestSession_byTestSession_testSessionCreatorTextBox.ClientID + "','TS')");
        }

        /// <summary>
        /// Binds the test area check box list
        /// </summary>
        private void BindTestArea()
        {
            TestSession_byTest_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
                  //new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA,Constants.SortTypeConstants.ASCENDING);
            TestSession_byTest_testAreaCheckBoxList.DataTextField = "AttributeName";
            TestSession_byTest_testAreaCheckBoxList.DataValueField = "AttributeID";
            TestSession_byTest_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Clears all the input controls in 'by-test' tab
        /// </summary>
        private void ClearByTestControls()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST]))
            {
                ArrayList arrayList = (ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST];
                arrayList[(int)SessionArrayListString.BYTEST_TESTSEARCH] = null;
                arrayList[(int)SessionArrayListString.BYTEST_STATICNUMBER] = null;
                arrayList[(int)SessionArrayListString.BYTEST_GRID_PAGENUMBER] = null;
                arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTCOLUMNNAME] = null;
                arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTORDER] = null;
                arrayList[(int)SessionArrayListString.BYTEST_SIMPLELINKTEXT] = "Simple";
                Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST] = arrayList;
            }
            TestSession_byTest_testAreaCheckBoxList.ClearSelection();
            TestSession_byTest_categorySubjectControl.CategoryDataSource = null;
            TestSession_byTest_categorySubjectControl.SubjectDataSource = null;
            TestSession_byTest_certificateTestDropDownList.SelectedIndex = 0;
            TestSession_byTest_categoryTextBox.Text = string.Empty;
            TestSession_byTest_subjectTextBox.Text = string.Empty;
            TestSession_byTest_keywordsTextBox.Text = string.Empty;
            TestSession_byTest_testIdTextBox.Text = string.Empty;
            TestSession_byTest_testNameTextBox.Text = string.Empty;
            TestSession_byTest_authorNameTextBox.Text = string.Empty;
            TestSession_byTest_positionProfileTextBox.Text = string.Empty;
            TestSession_byTest_positionProfileIDHiddenField.Value = string.Empty;
            TestSession_byTest_keywordAdvanceTextBox.Text = string.Empty;
            TestSession_byTest_noOfQuestionsTextBox.Text = string.Empty;
            //((TextBox)TestSession_testCostMultiHandleSliderExtender.FindControl("TestSession_byTest_testCostMaxValueTextBox")).Text = "0";
            //((TextBox)TestSession_testCostMultiHandleSliderExtender.FindControl("TestSession_byTest_testCostMinValueTextBox")).Text = "0";
            ((TextBox)TestSession_byTest_noofQuestionsMultiHandleSliderExtender.FindControl("TestSession_byTest_noOfQuestionsMinValueTextBox")).Text = "0";
            ((TextBox)TestSession_byTest_noofQuestionsMultiHandleSliderExtender.FindControl("TestSession_byTest_noOfQuestionsMaxValueTextBox")).Text = "0";
            //TestSession_testCostMultiHandleSliderExtender.ClientState = "0,0";
            TestSession_byTest_noofQuestionsMultiHandleSliderExtender.ClientState = "0,0";
            TestSession_byTest_testGridView.DataSource = null;
            TestSession_byTest_testGridView.DataBind();
            TestSession_byTest_testPagingNavigator.TotalRecords = 0;
            TestSession_byTest_testGridViewDiv.Visible = false;
            TestSession_byTest_SimpleLinkButton.Text = "Advanced";
            TestSession_byTest_simpleSearchDiv.Visible = true;
            TestSession_byTest_advanceSearchDiv.Visible = false;
          //  Page.Form.DefaultFocus = TestSession_byTest_testIdTextBox.UniqueID;
            TestSession_byTest_searchDivUpdatePanel.Update();
        }

        /// <summary>
        /// Clears all input controls in 'by-test-session' tab
        /// </summary>
        private void ClearByTestSessionControls()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST]))
            {
                ArrayList arrayList = (ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST];
                arrayList[(int)SessionArrayListString.BYTESTSESSION_TESTSEARCH] = null;
                arrayList[(int)SessionArrayListString.BYTESTSESSION_STATICNUMBER] = null;
                arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_PAGENUMBER] = null;
                arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTCOLUMNNAME] = null;
                arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTORDER] = null;
                Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST] = arrayList;
            }
            TestSession_byTestSession_candidateSessionIdTextBox.Text = string.Empty;
            TestSession_byTestSession_testSessionIdTextBox.Text = string.Empty;
            TestSession_byTestSession_testIdTextBox.Text = string.Empty;
            TestSession_byTestSession_testNameTextBox.Text = string.Empty;
            TestSession_byTestSession_schedulerNameTextBox.Text = string.Empty;
            TestSession_byTestSession_schedulerNameHiddenField.Value = string.Empty;
            TestSession_byTestSession_candidateNameTextBox.Text = string.Empty;
            TestSession_byTestSession_candidateIdHiddenField.Value = string.Empty;
            TestSession_byTestSession_candidateEmailHiddenField.Value = string.Empty;
            TestSession_byTestSession_positionProfileTextBox.Text = string.Empty;
            TestSession_byTestSession_positionProfileIDHiddenField.Value = string.Empty;
            TestSession_byTestSession_testSessionGridView.DataSource = null;
            TestSession_byTestSession_testSessionGridView.DataBind();
            TestSession_byTestSession_testSessionPagingNavigator.TotalRecords = 0;
            TestSession_byTestSession_testSessionGridViewDiv.Visible = false;
        }

        /// <summary>
        /// Cancels the test session
        /// </summary>
        private void CancelTestSession()
        {
            if (ViewState["ATTEMPT_ID"] == null || ViewState["CANDIDATE_SESSION_ID"] == null)
                return;

            // Check if the reason text box is empty. If it is, show the error message.
            // Otherwise, update the session status as CANCEL.
            if (TestSession_cancelTestReasonTextBox.Text.Trim().Length == 0)
            {
                TestSession_cancelErrorMessageLabel.Text = "Reason cannot be empty";
                TestSession_cancelSessionModalPopupExtender.Show();
            }
            else
            {
                int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                string candidateSessionId = ViewState["CANDIDATE_SESSION_ID"].ToString();
                bool isMailSent = true;

                new TestConductionBLManager().UpdateSessionStatus(candidateSessionId,
                   attemptId, Constants.CandidateAttemptStatus.CANCELLED,
                   Constants.CandidateSessionStatus.CANCELLED, base.userID, out isMailSent);

                CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();
                candidateTestSession.AttemptID = attemptId;
                candidateTestSession.CandidateTestSessionID = candidateSessionId;
                candidateTestSession.CancelReason = TestSession_cancelTestReasonTextBox.Text.Trim();
                candidateTestSession.ModifiedBy = base.userID;
                new TestBLManager().CancelTestSession(candidateTestSession);
                base.ShowMessage(TestSession_topSuccessMessageLabel,
                    TestSession_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.CreateTestSession_SessionCancelled, candidateSessionId));

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(TestSession_byTestSession_pageNumberHiddenField.Value))
                    LoadDetails(1);
                else
                    LoadDetails(Convert.ToInt32(TestSession_byTestSession_pageNumberHiddenField.Value));

                TestSession_byTestSession_testSessionGridViewUpdatePanel.Update();
                TestSession_topMessagesUpdatePanel.Update();
                TestSession_bottomMessagesUpdatePanel.Update();
            }
        }

        /// <summary>
        /// This method fills the search segment of by test tab when
        /// page redirects from other page.
        /// </summary>
        /// <param name="testSearchCriteria">previous serach criteria(by test tab)</param>
        /// <param name="SimpleLinkButtonText">Link button text</param>
        private void FillByTestSearchCriteria(TestSearchCriteria testSearchCriteria, string SimpleLinkButtonText)
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.Category))
                TestSession_byTest_categoryTextBox.Text = testSearchCriteria.Category;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.Subject))
                TestSession_byTest_subjectTextBox.Text = testSearchCriteria.Subject;
            if (SimpleLinkButtonText == "Simple")
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.Keyword))
                    TestSession_byTest_keywordsTextBox.Text = testSearchCriteria.Keyword;
            //string[] SelectedSubjectIDs = null;
            string[] testAreaID = null;
            /// To get Selected Test Area IDs.
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.TestAreasID))
            {
                testAreaID = testSearchCriteria.TestAreasID.Split(',');
                for (int i = 0; i < testAreaID.Length; i++)
                    TestSession_byTest_testAreaCheckBoxList.Items.FindByValue(testAreaID[i]).Selected = true;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.IsCertification))
                if (testSearchCriteria.IsCertification == true)
                    TestSession_byTest_certificateTestDropDownList.SelectedIndex = 1;
                else
                    TestSession_byTest_certificateTestDropDownList.SelectedIndex = 2;
            else
                TestSession_byTest_certificateTestDropDownList.SelectedIndex = 0;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.TestKey))
                TestSession_byTest_testIdTextBox.Text = testSearchCriteria.TestKey;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.Name))
                TestSession_byTest_testNameTextBox.Text = testSearchCriteria.Name;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.PositionProfileName))
                TestSession_byTest_positionProfileTextBox.Text = testSearchCriteria.PositionProfileName;
            
            TestSession_byTest_positionProfileIDHiddenField.Value = testSearchCriteria.PositionProfileID.ToString();
            TestSession_byTest_showCopiedTestsCheckBox.Checked = testSearchCriteria.ShowCopiedTests;

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.Keyword))
                TestSession_byTest_keywordAdvanceTextBox.Text = testSearchCriteria.Keyword;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.TestAuthorName))
                TestSession_byTest_authorNameTextBox.Text = testSearchCriteria.TestAuthorName;
            //((TextBox)TestSession_testCostMultiHandleSliderExtender.
            //    FindControl("TestSession_byTest_testCostMinValueTextBox")).Text = testSearchCriteria.TestCostStart.ToString();
            //((TextBox)TestSession_testCostMultiHandleSliderExtender.
            //    FindControl("TestSession_byTest_testCostMaxValueTextBox")).Text = testSearchCriteria.TestCostEnd.ToString();
            ((TextBox)TestSession_byTest_noofQuestionsMultiHandleSliderExtender.
                FindControl("TestSession_byTest_noOfQuestionsMinValueTextBox")).Text = testSearchCriteria.TotalQuestionStart.ToString();
            ((TextBox)TestSession_byTest_noofQuestionsMultiHandleSliderExtender.
                FindControl("TestSession_byTest_noOfQuestionsMaxValueTextBox")).Text = testSearchCriteria.TotalQuestionEnd.ToString();
            //TestSession_testCostMultiHandleSliderExtender.ClientState =
            //    testSearchCriteria.TestCostStart.ToString() + "," + testSearchCriteria.TestCostEnd.ToString();
            TestSession_byTest_noofQuestionsMultiHandleSliderExtender.ClientState =
                testSearchCriteria.TotalQuestionStart.ToString() + "," + testSearchCriteria.TotalQuestionEnd.ToString();
            if (testSearchCriteria.Subjects != null)
            {
                // Fetch unique categories from the list.
                var distinctCategories = testSearchCriteria.Subjects.GroupBy(x => x.CategoryID)
                    .Select(x => x.First());
                // This datasource is used to make the selection of the subjects
                // which are already selected by the user to search.
                TestSession_byTest_categorySubjectControl.SubjectsToBeSelected = testSearchCriteria.Subjects;
                // Bind distinct categories in the category datalist
                TestSession_byTest_categorySubjectControl.CategoryDataSource =
                    distinctCategories.ToList<Subject>();
                // Bind subjects for the categories which are bind previously.
                TestSession_byTest_categorySubjectControl.SubjectDataSource =
                    distinctCategories.ToList<Subject>();
            }
        }

        /// <summary>
        /// This Method fills the search criteria text boxes 
        /// when the page comes from other page.
        /// </summary>
        /// <param name="testSessionSearchCriteria">previous serach criteria(by test session tab)</param>
        private void FillTestSessionSearchCriteria(TestSessionSearchCriteria testSessionSearchCriteria)
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria.CandidateSessionID))
                TestSession_byTestSession_candidateSessionIdTextBox.Text = testSessionSearchCriteria.CandidateSessionID;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria.SessionKey))
                TestSession_byTestSession_testSessionIdTextBox.Text = testSessionSearchCriteria.SessionKey;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria.TestKey))
                TestSession_byTestSession_testIdTextBox.Text = testSessionSearchCriteria.TestKey;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria.TestName))
                TestSession_byTestSession_testNameTextBox.Text = testSessionSearchCriteria.TestName;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria.SchedulerName))
                TestSession_byTestSession_schedulerNameTextBox.Text = testSessionSearchCriteria.SchedulerName;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria.CandidateName))
                TestSession_byTestSession_candidateNameTextBox.Text = testSessionSearchCriteria.CandidateName;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionSearchCriteria.PositionProfileName))
                TestSession_byTestSession_positionProfileTextBox.Text = testSessionSearchCriteria.PositionProfileName.ToString();
            TestSession_byTestSession_positionProfileIDHiddenField.Value = testSessionSearchCriteria.PositionProfileID.ToString();
        }

        /// <summary>
        /// This method checks whether the page is loading directly
        /// or it is redirected from other page if so, it will load all the
        /// values when user leaving this page.
        /// </summary>
        private void CheckForSessionUsage()
        {
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST]))
                return;
            ArrayList arrayList = null;
            try
            {
                arrayList = (ArrayList)Session[Forte.HCM.Support.Constants.SearchCriteriaSessionKey.SEARCH_TEST_SESSION_BY_TEST];
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList[(int)SessionArrayListString.ACTIVETABINDEX]))
                    TestSession_mainTabContainer.ActiveTabIndex =
                        (int)arrayList[(int)SessionArrayListString.ACTIVETABINDEX];
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList[
                    (int)SessionArrayListString.BYTEST_GRID_SORTCOLUMNNAME]))
                {
                    // Initializing Sort column name By test tab view state
                    ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE] = arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTCOLUMNNAME];
                    // Initializing sort order by test tab view state
                    ViewState[BYTEST_SORT_ORDER_VIEWSTATE] = (SortType)arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTORDER];
                }
                else
                {
                    ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE] = "TESTKEY";
                    ViewState[BYTEST_SORT_ORDER_VIEWSTATE] = SortType.Ascending;
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList[
                    (int)SessionArrayListString.BYTESTSESSION_GRID_SORTCOLUMNNAME]))
                {
                    // Initializing Sort column name By test session tab view state
                    ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE] = arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTCOLUMNNAME];
                    // Initializing sort order by test session tab view state
                    ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = (SortType)arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTORDER];
                }
                else
                {
                    ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = SortType.Ascending;
                    ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE] = "TESTSESSIONID";
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList[(int)SessionArrayListString.BYTEST_TESTSEARCH]))
                {
                    TestSession_byTest_SimpleLinkButton.Text =
                        arrayList[(int)SessionArrayListString.BYTEST_SIMPLELINKTEXT].ToString() == "Simple" ? "Advanced" : "Simple";
                    // Fill the search criteria in to text boxes.
                    FillByTestSearchCriteria((TestSearchCriteria)arrayList[(int)SessionArrayListString.BYTEST_TESTSEARCH],
                        arrayList[(int)SessionArrayListString.BYTEST_SIMPLELINKTEXT].ToString());
                    // Loads the by test tab grid view
                    LoadByTestGrid((TestSearchCriteria)arrayList[(int)SessionArrayListString.BYTEST_TESTSEARCH],
                        (int)arrayList[(int)SessionArrayListString.BYTEST_GRID_PAGENUMBER],
                        arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTCOLUMNNAME].ToString(),
                        (SortType)arrayList[(int)SessionArrayListString.BYTEST_GRID_SORTORDER]);
                    // set Page number high lighted for by test tab grid.
                    TestSession_byTest_testPagingNavigator.MoveToPage((int)arrayList[(int)SessionArrayListString.BYTEST_GRID_PAGENUMBER]);
                    // Loads the Maximized state in to the Hidden Control
                    TestSession_byTest_isMaximizedHiddenField.Value =
                        (string)arrayList[(int)SessionArrayListString.BYTEST_ISMAXIMIZED];
                    if (TestSession_mainTabContainer.ActiveTabIndex == 0)
                        SetVisibleForSimpleOrAdvanced();
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList[(int)SessionArrayListString.BYTESTSESSION_TESTSEARCH]))
                {
                    // Fill the search criteria in to text boxes.
                    FillTestSessionSearchCriteria((TestSessionSearchCriteria)
                        arrayList[(int)SessionArrayListString.BYTESTSESSION_TESTSEARCH]);
                    // Loads the by test sesssion tab grid view
                    LoadByTestSessionGrid((TestSessionSearchCriteria)arrayList[(int)SessionArrayListString.BYTESTSESSION_TESTSEARCH],
                        (int)arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_PAGENUMBER],
                        arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTCOLUMNNAME].ToString(),
                        (SortType)arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_SORTORDER]);
                    // set Page number high lighted for by test session tab grid.
                    TestSession_byTestSession_testSessionPagingNavigator.MoveToPage
                        ((int)arrayList[(int)SessionArrayListString.BYTESTSESSION_GRID_PAGENUMBER]);
                    // Loads the Maximized state in to the Hidden Control
                    TestSession_byTestSession_isMaximizedHiddenField.Value =
                        (string)arrayList[(int)SessionArrayListString.BYTESTSESSION_ISMAXIMIZED];
                }
                CheckAndSetExpandorRestore();
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(arrayList)) arrayList = null;
                TestSession_bottomMessagesUpdatePanel.Update();
                TestSession_topMessagesUpdatePanel.Update();
                TestSession_byTestSession_testSessionGridViewUpdatePanel.Update();
                TestSession_byTest_GridVeiwUpdatePanel.Update();
                TestSession_byTest_testSessionPageNavigationUpdatePanel.Update();
                TestSession_byTestSession_testSessionPageNavigationUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Method used to set the author name and first name for the first time
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id 
        /// </param>
        private void SetAuthorDetails()
        {
            //Get the user details from DB and assign it
            UserDetail userDetail = new QuestionBLManager().GetAuthorIDAndName(base.userID);

            if (userDetail == null)
                return;

            TestSession_byTestSession_testSessionCreatorTextBox.Text = userDetail.FirstName;
            TestSession_byTestSession_testSessionCreatorIdHiddenField.Value = base.userID.ToString();

            //if (base.isAdmin)
            //{
            //    TestSession_byTestSession_testSessionCreatorImageButton.Visible = true;
            //    TestSession_byTestSession_testSessionCreatorTextBox.Text = string.Empty;
            //}
        }

        #endregion Private Methods

        #region Protected Overriden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CreditsSummary creditSummary = new CreditBLManager().GetCreditSummary(base.userID);
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(creditSummary))
                TestSession_creditsEarnedLinkButton.Text = creditSummary.AvailableCredits.ToString();

            BindTestArea();
            //  Page.Form.DefaultFocus = TestSession_byTest_categoryTextBox.UniqueID;
            SubscribeClientSideHandlers();
            CheckForSessionUsage();
            ViewState[BYTEST_SORT_COLUMN_NAME_VIEWSTATE] = "TESTKEY";
            ViewState[BYTEST_SORT_ORDER_VIEWSTATE] = SortType.Descending;
            ViewState[BYTESTSESSION_SORT_ORDER_VIEWSTATE] = SortType.Descending;
            ViewState[BYTESTSESSION_SORT_COLUMN_NAME_VIEWSTATE] = "TESTSESSIONID";

            LoadSearchbyTestDetails(1);
            LoadSearchbyTestSessionDetails(1);
        }

        #endregion Protected Overriden Methods
    }
}