﻿#region Header                                                                                        

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateManualTest.cs
// File that represents the user Serach the Question by various Key filed. 
// and Create the test Details and questions.
// This will helps Create a Test and have to implemeted the Certification details..

#endregion

#region Directives                                                                                    

using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

#endregion

namespace Forte.HCM.UI.TestMaker
{
    public partial class CreateManualTest : PageBase
    {
        #region Declaration                                                                           
        string sQryString = "";
        string questionID = "";
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="int"/> constant that holds the adaptive page size.
        /// </summary>
        private const int ADAPTIVE_PAGE_SIZE = 5;

        List<QuestionDetail> questionDetailSearchResultList;
        List<QuestionDetail> questionDetailTestDraftResultList;
        private const string ADAPTIVE_QUESTION_DETAILS_VIEWSTATE = "ADAPTIVEQUESTIONDETAILS";
     
        #endregion

        #region Event Handlers                                                                        

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (CreateManualTest_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = CreateManualTest_topSearchButton.UniqueID;
                else if (CreateManualTest_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = CreateManualTest_bottomSaveButton.UniqueID;

                StateMaintainence();
                CreateManualTest_ConfirmPopupPanel.Style.Add("height", "220px");
                //SetSearchGridView();
                if (!IsPostBack)
                {
                    CreateManualTest_recommendAdaptiveQuestions.Style.Add("display", "none");
                    LoadValues();
                }
                Master.SetPageCaption(Resources.HCMResource.CreateManualTest_Title);
                MessageLabel_Reset();
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");
                ExpandRestore();
                CreateManualTest_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateManualTest_pagingNavigator_PageNumberClick);

                // Create events for paging control
                CreateManualTest_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateManualTest_adaptivepagingNavigator_PageNumberClick);

                CreateManualTest_categorySubjectControl.ControlMessageThrown += 
                    new CategorySubjectControl.ControlMessageThrownDelegate(CreateManualTest_categorySubjectControl_ControlMessageThrown);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void CreateManualTest_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                CreateManualTest_topErrorMessageLabel.Text = c.Message.ToString();
                CreateManualTest_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                CreateManualTest_topSuccessMessageLabel.Text = c.Message.ToString();
                CreateManualTest_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
            BindTestDraftGrid();
            BindSearchGridView();
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateManualTest_okClick(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel, 
                    CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
            
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void CreateManualTest_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                CreateManualTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualTest_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                { 
                    string[] questionCollection=e.CommandArgument.ToString().Trim().Split(':');
                    CreateManualTest_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    CreateManualTest_questionDetailSummaryControl.ShowAddButton = true;
                    CreateManualTest_questionDetailSummaryControl.LoadQuestionDetails
                        (questionCollection[0], int.Parse(questionCollection[1]));
                    CreateManualTest_questionDetailSummaryControl.Visible = true;
                    CreateManualTest_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateManualTest_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField CreateManualTest_hidddenValue = (HiddenField)e.Row.FindControl("CreateManualTest_hidddenValue");
                    string questionID = CreateManualTest_hidddenValue.Value.Split(':')[0].ToString();
                    ImageButton alertImageButton = (ImageButton)e.Row.FindControl("CreateManualTest_searchQuestionAlertImageButton");
                    alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");

                    Image img = (Image)e.Row.FindControl("CreateManualTest_searchQuestionSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateManualTest_searchQuestionGridView.ClientID + "');");
                    img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + CreateManualTest_hidddenValue.Value + "','" + CreateManualTest_selectMultipleImage.ClientID + "');");

                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    
                    Label CreateManualTest_questionLabel =
                        (Label)e.Row.FindControl("CreateManualTest_questionLabel");

                    HoverMenuExtender CreateManualTest_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("CreateManualTest_hoverMenuExtender");
                    CreateManualTest_hoverMenuExtender.TargetControlID = CreateManualTest_questionLabel.ID;

                   // Label CreateManualTest_questionDetailPreviewControlQuestionLabel =
                     //   (Label)e.Row.FindControl("CreateManualTest_questionDetailPreviewControlQuestionLabel");
                    //RadioButtonList CreateManualTest_questionPreviewControlAnswerRadioButtonList =
                    //    (RadioButtonList)e.Row.FindControl("CreateManualTest_questionPreviewControlAnswerRadioButtonList");


                    PlaceHolder CreateManualTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("CreateManualTest_answerChoicesPlaceHolder");
                    CreateManualTest_answerChoicesPlaceHolder.Controls.Add
                                        (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID),true));


                    HiddenField CreateManualTest_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                           "CreateManualTest_correctAnswerHiddenField");

                    List<AnswerChoice> answerChoice = new List<AnswerChoice>();
                    answerChoice = new BL.QuestionBLManager().GetQuestionOptions(questionID);

                   // CreateManualTest_questionDetailPreviewControlQuestionLabel.Text = CreateManualTest_questionLabel.Text;

                 
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

       
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateManualTest_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image img = (Image)e.Row.FindControl("CreateManualTest_testDrftRemoveImage");

                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);

                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    Label CreateManualTest_questionLabel =
                         (Label)e.Row.FindControl("CreateManualTest_draftQuestionLabel");

                    HoverMenuExtender CreateManualTest_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("CreateManualTest_draftHoverMenuExtender1");
                    CreateManualTest_hoverMenuExtender.TargetControlID = CreateManualTest_questionLabel.ID;
                   
                    //RadioButtonList CreateManualTest_questionPreviewControlAnswerRadioButtonList =
                   //     (RadioButtonList)e.Row.FindControl("QuestionDetailPreviewControl_draftAnswerRadioButtonList");
                    HiddenField CreateManualTest_questionKeyHiddenField = (HiddenField)e.Row.FindControl(
                              "CreateManualTest_questionKeyHiddenField");
                    
                    HiddenField CreateManualTest_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                             "CreateManualTest_correctAnswerHiddenField");
                    string questionID = CreateManualTest_questionKeyHiddenField.Value.ToString().Split(':')[0];
                    PlaceHolder CreateManualTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("CreateManualTest_answerChoicesPlaceHolder");
                    CreateManualTest_answerChoicesPlaceHolder.Controls.Add
                                        (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID),true));

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualTest_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    CreateManualTest_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    CreateManualTest_ConfirmPopupExtenderControl.Message =string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message,e.CommandArgument.ToString().Split(':')[0]);
                    CreateManualTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateManualTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateManualTest_ConfirmPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void CreateManualTest_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                CreateManualTest_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }
               
        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void CreateManualTest_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                AddSearchQuestionToTestDraft();
                if (hiddenValue.Value != "")
                {
                    MoveToTestDraft("Add");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                                       CreateManualTest_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_Search_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void CreateManualTest_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in CreateManualTest_testDrftGridView.Rows)
                    {
                        CheckBox CreateManualTest_testDrftCheckbox = (CheckBox)row.FindControl("CreateManualTest_testDrftCheckbox");
                        HiddenField CreateManualTest_questionKeyHiddenField = (HiddenField)row.FindControl("CreateManualTest_questionKeyHiddenField");
                        if (CreateManualTest_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + CreateManualTest_questionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }
                    }
                   // hiddenValue.Value = questionID.TrimEnd(',');
                   
                }
                if (questionSelected)
                {
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    CreateManualTest_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    CreateManualTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateManualTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateManualTest_ConfirmPopupExtender.Show();
                   // MoveToTestDraft("Delete");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                                       CreateManualTest_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_TestDraft_Result);
                    
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualTest_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CreateManualTest_simpleLinkButton.Text.ToLower() == "simple")
                {
                    CreateManualTest_simpleLinkButton.Text = "Advanced";
                    CreateManualTest_simpleSearchDiv.Visible = true;
                    CreateManualTest_advanceSearchDiv.Visible = false;
                }
                else
                {
                    CreateManualTest_simpleLinkButton.Text = "Simple";
                    CreateManualTest_simpleSearchDiv.Visible = false;
                    CreateManualTest_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualTest_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                ViewState["PAGENUMBER"] = e.PageNumber;
                LoadQuestion(e.PageNumber, ViewState["SORTEXPRESSION"].ToString(), ViewState["SORTDIRECTIONKEY"].ToString());
                //BindAdaptiveGridStateMaintain();
            }
            catch(Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }
       
        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateManualTest_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if feature usage limit exceeds for creating test.
                if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                {
                    base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                        CreateManualTest_bottomErrorMessageLabel, 
                        "You have reached the maximum limit based your subscription plan. Cannot create more tests");
                    return;
                }

                if (IsValidData())
                {
                    questionDetailTestDraftResultList = new List<QuestionDetail>();
                    questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    List<decimal> creditEarned = new List<decimal>();
                    List<decimal> timeTaken = new List<decimal>();
                    string testKey = "";
                    for (int i = 0; i < questionDetailTestDraftResultList.Count; i++)
                    {
                        creditEarned.Add(questionDetailTestDraftResultList[i].CreditsEarned);
                        timeTaken.Add(questionDetailTestDraftResultList[i].AverageTimeTaken);
                    }
                    int questionCount = questionDetailTestDraftResultList.Count;
                    TestDetail testDetail = new TestDetail();
                    testDetail = CreateManualTest_testDetailsUserControl.TestDetailDataSource;
                    testDetail.Questions = questionDetailTestDraftResultList;
                    testDetail.IsActive = true;
                    testDetail.CreatedBy = userID;
                    testDetail.IsDeleted = false;
                    testDetail.ModifiedBy = userID;
                    testDetail.NoOfQuestions = questionCount;
                    testDetail.SystemRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionCount;
                    testDetail.TestAuthorID = userID;
                    testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
                    testDetail.TestCreationDate = DateTime.Now;
                    testDetail.TestStatus = TestStatus.Active;
                    testDetail.TestType = TestType.Genenal;
                    testDetail.TestMode = "MANUAL";
                    testDetail.TestKey = testKey;
                    testKey = new TestBLManager().SaveTest(testDetail, userID, new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList).AttributeID.ToString());

                    // Send a mail to the position profile creator.
                    try
                    {
                        if (testDetail.PositionProfileID != 0)
                        {
                            new EmailHandler().SendMail(EntityType.TestCreatedForPositionProfile, testDetail.TestKey);
                        }
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }

                    Response.Redirect(Constants.TestMakerConstants.EDIT_TEST_WA + "?m=1&s=1&mode=new&parentpage=" + Constants.ParentPage.SEARCH_TEST + "&testkey=" + testKey, false);
                }
                else
                    MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateManualTest_adaptiveYesNoRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindTestDraftGrid();
                BindSearchGridView();
                if (CreateManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                {
                    CreateManualTest_adaptiveRecommendationsClicked.Value = "";
                    CreateManualTest_recommendAdaptiveQuestions.Style.Add("display", "none");
                    CreateManualTest_adaptiveGridView.Visible = false;
                    CreateManualTest_adaptivePagingHiddenField.Value = "1";
                    ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] = null;
                    BindEmptyAdaptiveGridView();
                    return;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                                    false), Convert.ToInt32(CreateManualTest_adaptivePagingHiddenField.Value));                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateManualTest_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = CreateManualTest_previewQuestionAddHiddenField.Value;
                CreateManualTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CreateManualTest_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualTest_adaptivepagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                BindSearchGridView();
                BindTestDraftGrid();
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((CreateManualTest_adaptiveRecommendationsClicked.Value == "") ? false : true)), 
                    e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateManualTest_recommendAdaptiveQuestions_Click(object sender, EventArgs e)
        {
            try
            {
                BindTestDraftGrid();
                BindSearchGridView();
                CreateManualTest_adaptiveRecommendationsClicked.Value = "1";
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    true), Convert.ToInt32(CreateManualTest_adaptivePagingHiddenField.Value));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualTest_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    CreateManualTest_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    string[] QuestionDetails = e.CommandArgument.ToString().Split(':');
                    CreateManualTest_questionDetailSummaryControl.Visible = true;
                    CreateManualTest_questionDetailSummaryControl.ShowAddButton = true;
                    CreateManualTest_questionDetailSummaryControl.LoadQuestionDetails(QuestionDetails[0], Convert.ToInt32(QuestionDetails[1]));

                    CreateManualTest_questionModalPopupExtender.Show();
                }
                if (e.CommandName == "AdaptiveQuestion")
                {
                    CreateManualTest_adaptiveQuestionPreviewModalPopupExtender.Show();

                    ViewState["RowIndex"] = (e.CommandArgument.ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateManualTest_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                ImageButton CreateManualTest_adaptiveSelectImage = (ImageButton)e.Row.FindControl("CreateManualTest_adaptiveSelectImage");
                string questionID = ((HiddenField)e.Row.FindControl("CreateManualTest_adaptiveQuestionhiddenValue")).Value;
                CreateManualTest_adaptiveSelectImage.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + questionID + "','" + CreateManualTest_selectMultipleImage.ClientID + "');");
                Image alertImage = (Image)e.Row.FindControl("CreateManualTest_adaptiveAlertImageButton");
                alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion('" + questionID.Split(':')[0] + "');");
                CreateManualTest_adaptiveSelectImage.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateManualTest_adaptiveGridView.ClientID + "');");
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                Label CreateManualTest_questionLabel =
                     (Label)e.Row.FindControl("CreateManualTest_adaptiveQuestionLabel");
                HoverMenuExtender CreateManualTest_hoverMenuExtender = (HoverMenuExtender)
                    e.Row.FindControl("CreateManualTest_adaptivePreviewhoverMenuExtender");
                CreateManualTest_hoverMenuExtender.TargetControlID = CreateManualTest_questionLabel.ID;
                PlaceHolder CreateManualTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("CreateManualTest_adaptiveAnswerChoicesPlaceHolder");
                CreateManualTest_answerChoicesPlaceHolder.Controls.Add
                                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID.Split(':')[0]),true));
                HiddenField CreateManualTest_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                       "CreateManualTest_adaptiveCorrectAnswerHiddenField");

                List<AnswerChoice> answerChoice = new List<AnswerChoice>();
                answerChoice = new BL.QuestionBLManager().GetQuestionOptions(questionID);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        protected void CreateManualTest_questionDetailTopAddButton_Click(object sender, EventArgs e)
        {
            //CreateManualTest_adaptiveQuestionPreviewModalPopupExtender.Hide();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);

            //ViewState["RowIndex"] = 0;

        }

        #endregion

        #region Private  Methods                                                                      
        
        /// <summary>
        /// Binds the search grid view
        /// </summary>
        protected void BindSearchGridView()
        {
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                return;
            CreateManualTest_searchQuestionGridView.DataSource = (List<QuestionDetail>)ViewState["SEARCHRESULTGRID"];
            CreateManualTest_searchQuestionGridView.DataBind();
        }                         
        
        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            StringBuilder sbQuestionIds = null;
            try
            {
                sbQuestionIds = new StringBuilder();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in CreateManualTest_searchQuestionGridView.Rows)
                    {
                        CheckBox CreateManualTest_searchQuestionCheckbox = (CheckBox)row.FindControl("CreateManualTest_searchQuestionCheckbox");
                        HiddenField CreateManualTest_questionKeyHiddenField = (HiddenField)row.FindControl("CreateManualTest_questionKeyHiddenField");
                        if (CreateManualTest_searchQuestionCheckbox.Checked)
                            sbQuestionIds.Append(CreateManualTest_questionKeyHiddenField.Value + ",");
                    }
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                    if (CreateManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return;
                    if (!CreateManualTest_adaptiveGridView.Visible)
                        return;
                    for (int i = 0; i < CreateManualTest_adaptiveGridView.Rows.Count; i++)
                    {
                        if (((CheckBox)(CreateManualTest_adaptiveGridView.Rows[i].FindControl("CreateManualTest_adaptiveCheckbox"))).Checked)
                            sbQuestionIds.Append(((HiddenField)(CreateManualTest_adaptiveGridView.Rows[i].
                                FindControl("CreateManualTest_adaptiveQuestionKeyHiddenField"))).Value + ",");
                    }
                    hiddenValue.Value = "";
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                }
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionIds)) sbQuestionIds = null;
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>                                         
        private void ExpandRestore()
        {
            if (CreateManualTest_restoreHiddenField.Value == "Y")
            {
                CreateManualTest_searchCriteriasDiv.Style["display"] = "none";
                CreateManualTest_searchResultsUpSpan.Style["display"] = "block";
                CreateManualTest_searchResultsDownSpan.Style["display"] = "none";
                CreateManualTest_questionDiv.Style["height"] =EXPANDED_HEIGHT;
                CreateManualTest_adaptiveQuestionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CreateManualTest_searchCriteriasDiv.Style["display"] = "block";
                CreateManualTest_searchResultsUpSpan.Style["display"] = "none";
                CreateManualTest_searchResultsDownSpan.Style["display"] = "block";
                CreateManualTest_questionDiv.Style["height"] =RESTORED_HEIGHT;
                CreateManualTest_adaptiveQuestionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            CreateManualTest_searchResultsUpImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                CreateManualTest_questionDiv.ClientID + "','" +
                CreateManualTest_searchCriteriasDiv.ClientID + "','" +
                CreateManualTest_searchResultsUpSpan.ClientID + "','" +
                CreateManualTest_searchResultsDownSpan.ClientID + "','" +
                CreateManualTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                CreateManualTest_adaptiveQuestionDiv.ClientID + "');");
            CreateManualTest_searchResultsDownImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                CreateManualTest_questionDiv.ClientID + "','" +
                CreateManualTest_searchCriteriasDiv.ClientID + "','" +
                CreateManualTest_searchResultsUpSpan.ClientID + "','" +
                CreateManualTest_searchResultsDownSpan.ClientID + "','" +
                CreateManualTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                CreateManualTest_adaptiveQuestionDiv.ClientID + "');");
        }

        /// <summary>
        /// Reset the Messeags.
        /// </summary>
        private void MessageLabel_Reset()
        {
            CreateManualTest_topErrorMessageLabel.Text = string.Empty;
            CreateManualTest_bottomErrorMessageLabel.Text = string.Empty;
            CreateManualTest_topSuccessMessageLabel.Text = string.Empty;
            CreateManualTest_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (CreateManualTest_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    CreateManualTest_categoryTextBox.Text.Trim() == "" ?
                                    null : CreateManualTest_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    CreateManualTest_subjectTextBox.Text.Trim() == "" ?
                                    null : CreateManualTest_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateManualTest_keywordsTextBox.Text.Trim() == "" ?
                                    null : CreateManualTest_keywordsTextBox.Text.Trim();
                questionDetailSearchCriteria.Author = base.userID;
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CreateManualTest_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in CreateManualTest_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in CreateManualTest_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    CreateManualTest_questionIDTextBox.Text.Trim() == "" ?
                                    null : CreateManualTest_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateManualTest_keywordTextBox.Text.Trim() == "" ?
                                    null : CreateManualTest_keywordTextBox.Text.Trim();              

                questionDetailSearchCriteria.AuthorName =
                                   CreateManualTest_authorTextBox.Text.Trim() == "" ?
                                   null : CreateManualTest_authorIdHiddenField.Value;
                questionDetailSearchCriteria.ActiveFlag = null;
                questionDetailSearchCriteria.CreditsEarned =
                                    CreateManualTest_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(CreateManualTest_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.ClientRequest = CreateManulTest_positionProfileKeywordTextBox.Text.Trim() == "" ?
                    null : CreateManulTest_positionProfileKeywordTextBox.Text.Trim();

                questionDetailSearchCriteria.Author = base.userID;

            }


            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail = new BL.TestBLManager().GetSearchQuestions(QuestionType.MultipleChoice, questionDetailSearchCriteria, base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                CreateManualTest_searchQuestionGridView.DataSource = null;
                CreateManualTest_searchQuestionGridView.DataBind();
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                 CreateManualTest_bottomErrorMessageLabel,Resources.HCMResource.Common_Empty_Grid);
                CreateManualTest_bottomPagingNavigator.TotalRecords = 0;
                CreateManualTest_bottomPagingNavigator.Reset();
                //CreateManualTest_questionDiv.Visible = false;
            }
            else
            {
                CreateManualTest_searchQuestionGridView.DataSource = questionDetail;
                CreateManualTest_searchQuestionGridView.DataBind();
                CreateManualTest_bottomPagingNavigator.PageSize = base.GridPageSize;
                CreateManualTest_bottomPagingNavigator.TotalRecords = totalRecords;
                //CreateManualTest_questionDiv.Visible = true;
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
            // return questionDetail;
        }
        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            CreateManualTest_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            CreateManualTest_complexityCheckBoxList.DataTextField = "AttributeName";
            CreateManualTest_complexityCheckBoxList.DataValueField = "AttributeID";
            CreateManualTest_complexityCheckBoxList.DataBind();
        }
        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            CreateManualTest_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
              // new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            CreateManualTest_testAreaCheckBoxList.DataTextField = "AttributeName";
            CreateManualTest_testAreaCheckBoxList.DataValueField = "AttributeID";
            CreateManualTest_testAreaCheckBoxList.DataBind();
        }
       
        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < CreateManualTest_testAreaCheckBoxList.Items.Count; i++)
            {

                if (CreateManualTest_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(CreateManualTest_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }
        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < CreateManualTest_complexityCheckBoxList.Items.Count; i++)
            {
                if (CreateManualTest_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(CreateManualTest_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }
        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in CreateManualTest_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateManualTest_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateManualTest_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in CreateManualTest_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateManualTest_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateManualTest_adaptiveCheckbox")).Checked = false;
                }
            }

        }
        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {
            questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }

            int StartIndex = -1;
            int TotalCount = 0;
            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
            {
                questionDetailSearchResultList = new List<QuestionDetail>();
                questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            }
            if ((ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] != null) &&
                (questionDetailSearchResultList != null))
            {
                StartIndex = questionDetailSearchResultList.Count;
                questionDetailSearchResultList.AddRange((List<QuestionDetail>)ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE]);
                TotalCount = questionDetailSearchResultList.Count - StartIndex;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');

           
                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                    string[] idList = ids.Split(':');
                    string id = idList[0];
                    string relationId = idList[1];
                  
                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    base.ShowMessage(CreateManualTest_topSuccessMessageLabel,
                                        CreateManualTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(CreateManualTest_topSuccessMessageLabel,
                                    CreateManualTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            base.ShowMessage(CreateManualTest_topSuccessMessageLabel,
                                       CreateManualTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToTestDraft);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                        }

                    }
                }
            }
            if (CreateManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
            {
                switch(action)
                {
                    case "Add":
                        if (addedQuestionID == "")
                        {
                            CreateManualTest_adaptiveRecommendationsClicked.Value = CreateManualTest_adaptiveRecommendationsClicked.Value;
                            CreateManualTest_adaptivePagingHiddenField.Value = CreateManualTest_adaptivePagingHiddenField.Value;
                        }
                        else
                        {
                            CreateManualTest_adaptiveRecommendationsClicked.Value = "";
                            CreateManualTest_adaptivePagingHiddenField.Value = "1";
                        }
                        break;
                    case "Delete":
                        CreateManualTest_adaptiveRecommendationsClicked.Value = "";
                        CreateManualTest_adaptivePagingHiddenField.Value = "1";
                        break;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((CreateManualTest_adaptiveRecommendationsClicked.Value == "") ? false : true)),
                    Convert.ToInt32(CreateManualTest_adaptivePagingHiddenField.Value));
            }
            if (alertFlag)
            {
                string alertMessage = string.Format(Resources.HCMResource.CreateManualTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                {
                    alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(',')); 
                }
                CreateManualTest_ConfirmPopupPanel.Style.Add("height", "270px");
                CreateManualTest_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                CreateManualTest_ConfirmPopupExtenderControl.Title = "Warning";
                CreateManualTest_ConfirmPopupExtenderControl.Message = alertMessage;
                CreateManualTest_ConfirmPopupExtender.Show();
            }
            hiddenValue.Value = null;
            if (StartIndex != -1)
                questionDetailSearchResultList.RemoveRange(StartIndex, TotalCount);
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            CreateManualTest_testDrftGridView.DataSource = questionDetailTestDraftResultList;
            CreateManualTest_testDrftGridView.DataBind();
            if (questionDetailSearchResultList != null)
            {
                CreateManualTest_searchQuestionGridView.DataSource = questionDetailSearchResultList;
                //CreateManualTest_questionDiv.Visible = true;
            }
            else
            {
                CreateManualTest_searchQuestionGridView.DataSource = null;
                CreateManualTest_bottomPagingNavigator.TotalRecords = 0;
                CreateManualTest_bottomPagingNavigator.Reset();
                //CreateManualTest_questionDiv.Visible = false;
            }
                 
           
            CreateManualTest_searchQuestionGridView.DataBind();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                if (action != "")
                {
                    questionDetailTestDraftResultList = new List<QuestionDetail>();
                    questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    List<decimal> timeTaken = new List<decimal>();

                    for (int i = 0; i < questionDetailTestDraftResultList.Count; i++)
                    {
                        timeTaken.Add(questionDetailTestDraftResultList[i].AverageTimeTaken);
                    }

                    if (questionDetailTestDraftResultList.Count > 0)
                    {
                        CreateManualTest_testDetailsUserControl.SysRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionDetailTestDraftResultList.Count;
                    }
                    else
                    {
                        CreateManualTest_testDetailsUserControl.SysRecommendedTime = 0;
                    }
                }
                TestDraftSummary testDraftSummary = new TestDraftSummary();
                testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList);
                CreateManualTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
                
                
            }
            UncheckCheckBox();
        }

        /// <summary>
        /// This methiod maintains the results state.
        /// </summary>
        private void StateMaintainence()
        {
            switch (CreateManualTest_resultsHiddenField.Value)
            {
                case "0":
                    CreateManualTest_questionDiv.Style.Add("width", "936px");
                    CreateManualTest_adaptiveQuestionDiv.Style.Add("width", "0px");
                    break;
                case "1":
                    CreateManualTest_questionDiv.Style.Add("width", "0px");
                    CreateManualTest_adaptiveQuestionDiv.Style.Add("width", "936px");
                    break;
                case "2":
                    CreateManualTest_questionDiv.Style.Add("width", "450px");
                    CreateManualTest_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
                default:
                    CreateManualTest_questionDiv.Style.Add("width", "450px");
                    CreateManualTest_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
            }
        }

        private void BindTestDraftGrid()
        {
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["TESTDRAFTGRID"]))
                return;
            CreateManualTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
            CreateManualTest_testDrftGridView.DataBind();
        }

        #region Adaptive Question Recommendation Methods                       
 

        /// <summary>
        /// This method gets the question id's that are in the draft panel
        /// </summary>
        /// <param name="questionDetails">List of question details that are in draft panel</param>
        /// <param name="ForceLoad">Whether the questions should load though 
        /// questions exceeded the adaptive limit.</param>
        /// <returns>string contains the draft question ids</returns>
        private string GetSelectedQuestionsIds(List<QuestionDetail> questionDetails, bool ForceLoad)
        {
            StringBuilder sbQuestionDetails = null;
            try
            {
                CreateManualTest_recommendAdaptiveQuestions.Style.Add("display", "none");
                BindEmptyAdaptiveGridView();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetails))
                    return string.Empty;
                if (questionDetails.Count == 0)
                    return string.Empty;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"]))
                    return "";
                if ((ForceLoad == false) && (Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"])
                    < questionDetails.Count))
                {
                    if (CreateManualTest_adaptiveYesNoRadioButtonList.SelectedIndex != 1)
                        CreateManualTest_recommendAdaptiveQuestions.Style.Add("display", "block");
                    return "";
                }
                else
                    if (CreateManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return "";
                sbQuestionDetails = new StringBuilder();
                for (int i = 0; i < questionDetails.Count; i++)
                {
                    sbQuestionDetails.Append(questionDetails[i].QuestionKey);
                    sbQuestionDetails.Append(",");
                }
                return sbQuestionDetails.ToString().TrimEnd(',');
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionDetails)) sbQuestionDetails = null;
            }
        }

        /// <summary>
        /// This will Make adaptive grid view empty
        /// </summary>
        private void BindEmptyAdaptiveGridView()
        {
            CreateManualTest_adaptiveGridView.DataSource = null;
            CreateManualTest_adaptiveGridView.DataBind();
            CreateManualTest_adaptivebottomPagingNavigator.Reset();
            CreateManualTest_adaptivebottomPagingNavigator.TotalRecords = 0;
        }

        /// <summary>
        /// This method communicates with the BL Manager and gets the questions
        /// </summary>
        /// <param name="SelectedQuestionIds">Questions that are in test draft panel</param>
        /// <param name="PageNumber">Page number to load in to the 
        /// adaptive grid.</param>
        private void BindAdaptiveQuestions(string SelectedQuestionIds, int PageNumber)
        {
            if (SelectedQuestionIds == "")
            {
                CreateManualTest_adaptiveGridView.DataSource = null;
                CreateManualTest_adaptiveGridView.DataBind();
                CreateManualTest_adaptiveGridView.Visible = false;
                return;
            }
            int TotalNoOfRecords = 0;
            CreateManualTest_adaptiveGridView.DataSource =
                new TestBLManager().GetAdaptiveQuestions(base.userID, SelectedQuestionIds,
                ADAPTIVE_PAGE_SIZE, PageNumber,base.userID, out TotalNoOfRecords);
            CreateManualTest_adaptiveGridView.DataBind();
            if (TotalNoOfRecords == 0)
            {
                if (CreateManualTest_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
                    CreateManualTest_adaptiveGridView.Visible = true;
                else
                    CreateManualTest_adaptiveGridView.Visible = false;
                //CreateManualTest_adaptiveQuestionDiv.Visible = false;
                CreateManualTest_adaptivebottomPagingNavigator.TotalRecords = 0;
                CreateManualTest_adaptivebottomPagingNavigator.Reset();
                CreateManualTest_adaptivePagingHiddenField.Value = "1";
            }
            else
            {
                CreateManualTest_adaptiveGridView.Visible = true;
                ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] =
                    (List<QuestionDetail>)CreateManualTest_adaptiveGridView.DataSource;
                CreateManualTest_adaptivebottomPagingNavigator.Visible = true;
                CreateManualTest_adaptivePagingHiddenField.Value = PageNumber.ToString();
                CreateManualTest_adaptiveQuestionDiv.Visible = true;
                if (PageNumber == 1)
                    CreateManualTest_adaptivebottomPagingNavigator.Reset();
                else
                    CreateManualTest_adaptivebottomPagingNavigator.MoveToPage(PageNumber);
                CreateManualTest_adaptivebottomPagingNavigator.TotalRecords = TotalNoOfRecords;
                CreateManualTest_adaptivebottomPagingNavigator.PageSize = ADAPTIVE_PAGE_SIZE;
            }
        }

        #endregion Adaptive Question Recommendation Methods

        #endregion

        #region Protected Overridden Methods                                                          

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;

            
            TestDetail testDetail = new TestDetail();
            testDetail = CreateManualTest_testDetailsUserControl.TestDetailDataSource;
            int recommendedTime = testDetail.RecommendedCompletionTime;
            CreateManualTest_topErrorMessageLabel.Text = string.Empty;
            CreateManualTest_bottomErrorMessageLabel.Text = string.Empty;

            questionDetailTestDraftResultList = new List<QuestionDetail>();
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
                
            }
            else if (questionDetailTestDraftResultList.Count == 0)
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
            }


            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Name_Empty);

                isValidData = false;
            }

            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                   CreateManualTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualTest_Test_Description_Empty);

                isValidData = false;
            }
           
            if (recommendedTime==-1)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
               CreateManualTest_bottomErrorMessageLabel,
               Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_Empty);
               isValidData = false;
            }
            else if (recommendedTime == -2)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
               CreateManualTest_bottomErrorMessageLabel,
                string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended"));
                isValidData = false;
            }
            else if (recommendedTime == 0)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                CreateManualTest_bottomErrorMessageLabel,
                Resources.HCMResource.CreateManualTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                isValidData = false;
                
            }
            if(isQuestionTabActive)
                CreateManualTest_mainTabContainer.ActiveTab = CreateManualTest_questionsTabPanel;
            else
                CreateManualTest_mainTabContainer.ActiveTab = CreateManualTest_testdetailsTabPanel;
            if (!(bool)testDetail.IsCertification)
                return isValidData;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateDetailsEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateID <= 0)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateFormatEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.DaysElapseBetweenRetakes < 0)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyDaysRetakeTest);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.PermissibleRetakes <= 0)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificationTest_EmptyRetakes);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MinimumTotalScoreRequired <= 0)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                    Resources.HCMResource.TestDetailsControl_CertificateTest_MinimumQualifacationEmpty);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.CertificateValidity == "0")
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                     Resources.HCMResource.TestDetailsControl_CertificateTest_CertificateValidity);
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -2)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                        CreateManualTest_bottomErrorMessageLabel,
                        string.Format(Resources.HCMResource.Common_TimeLimit_Invalid, "Recommended certification completion"));
                isValidData = false;
            }
            if (testDetail.CertificationDetail.MaximumTimePermissible == -1)
            {
                base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                    CreateManualTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateAutomaticTest_TestDetailsControl_recommendedTimeLabelTextBox_NotValid);
                isValidData = false;
            }
            if ((testDetail.RecommendedCompletionTime > 0) && (testDetail.CertificationDetail.MaximumTimePermissible > 0))
            {
                if (testDetail.RecommendedCompletionTime < testDetail.CertificationDetail.MaximumTimePermissible)
                {
                    base.ShowMessage(CreateManualTest_topErrorMessageLabel,
                        CreateManualTest_bottomErrorMessageLabel,
                        Resources.HCMResource.TestDetailsControl_CertificationTest_CertificationTimeExceeds);
                    isValidData = false;
                }
            }
            CreateManualTest_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
            //CreateManualTest_manualImageButton.Visible = false;
            //CreateManualTest_adaptiveImageButton.Visible = false;
            //CreateManualTest_bothImageButton.Visible = false;
            CreateManualTest_simpleSearchDiv.Visible = true;
            CreateManualTest_advanceSearchDiv.Visible = false;
            //CreateManualTest_questionDiv.Visible = false;
            //CreateManualTest_adaptiveQuestionDiv.Visible = false;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                ViewState["SORTDIRECTIONKEY"] = "A";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                ViewState["SEARCHRESULTGRID"] = null;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                sQryString = Request.QueryString["mode"].ToUpper();

            CreateManualTest_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + CreateManualTest_dummyAuthorID.ClientID + "','"
                + CreateManualTest_authorIdHiddenField.ClientID + "','"
                + CreateManualTest_authorTextBox.ClientID + "','QA')");

            CreateManulTest_positionProfileImageButton.Attributes.Add("onclick",
                    "return ShowClientRequestForManual('" + CreateManulTest_positionProfileTextBox.ClientID + "','" +
                    CreateManulTest_positionProfileKeywordTextBox.ClientID  +"','" +
                     CreateManualTest_positionProfileHiddenField.ClientID + "')");

            CreateManualTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
            CreateManualTest_testDrftGridView.DataBind();
        }
        #endregion Protected Overridden Methods
    }
}
