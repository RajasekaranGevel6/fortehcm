﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateTestSession.aspx.cs"
    MasterPageFile="~/MasterPages/OTMMaster.Master" Inherits="Forte.HCM.UI.TestMaker.CreateTestSession" %>

<%@ Register Src="~/CommonControls/CandidateDetailControl.ascx" TagName="CandidateDetail"
    TagPrefix="uc5" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/TestSessionPreviewControl.ascx" TagName="TestSessionPreview"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="CreateTestSession_bodyContent" ContentPlaceHolderID="OTMMaster_body"
    runat="server">
    <script type="text/javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowWhyDisabled(show)
        {
            if (show == 'Y')
                document.getElementById('CreateTestSession_cyberProctorWhyDisabledDiv').style.display = "block";
            else
                document.getElementById('CreateTestSession_cyberProctorWhyDisabledDiv').style.display = "none";
            return false;
        }

        // Function calls when user clicks on the cancel test session image button
        function CandidateDetailModalPopup()
        {
            $find("<%= CreateTestSession_candidateDetailModalPopupExtender.ClientID %>").show();
            return false;
        }
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" class="header_text_bold">
                            <asp:Literal ID="CreateTestSession_headerLiteral" runat="server" Text="Create Test Session"></asp:Literal>
                        </td>
                        <td width="50%" align="right">
                            <asp:UpdatePanel ID="CreateTestSession_topButtonsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="4">
                                        <tr>
                                            <td>
                                                <asp:Button ID="CreateTestSession_topScheduleCandidateButton" runat="server" Text="Schedule Candidate"
                                                    SkinID="sknButtonId" Visible="false" OnClick="CreateTestSession_scheduleCandidateButton_Click" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="CreateTestSession_emailImageButton" runat="server" ToolTip="Click here to email the test sessions"
                                                    SkinID="sknMailImageButton" Visible="false" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreateTestSession_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                    Text="Reset" OnClick="CreateTestSession_resetButton_Click" />
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreateTestSession_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                    Text="Cancel" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateTestSession_topSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateTestSession_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateTestSession_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="non_tab_body_bg">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="panel_bg">
                            <asp:UpdatePanel ID="CreateTestSession_sessionDetailsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="CreateTestSession_sessionDetailsDIV" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateTestSession_testKeyHeadLabel" runat="server" Text="Test ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateTestSession_testKeyValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateTestSession_testNameHeadLabel" runat="server" Text="Test Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%;">
                                                                            <asp:Label ID="CreateTestSession_testNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateTestSession_sessionKeyHeadLabel" runat="server" Text="Test Session ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateTestSession_sessionKeyValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                Width="60%"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%;display:none">
                                                                            <asp:Label ID="CreateTestSession_creditHeadLabel" runat="server" Text="Total Credit (in $)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;display:none">
                                                                            <asp:Label ID="CreateTestSession_creditValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="8" align="left" style="width: 100%">
                                                                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                                                <tr>
                                                                                    <td style="width: 12%;">
                                                                                        <asp:Label ID="CreateTestSession_positionProfileHeadLabel" runat="server" Text="Position Profile"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 85%">
                                                                                        <asp:Label ID="CreateTestSession_positionProfileValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                            Text=""></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="CreateTestSession_sessionDescHeadLabel" runat="server" Text="Session Description"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="CreateTestSession_instructionsHeadLabel" runat="server" Text="Test Instructions"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="CreateTestSession_sessionDescTextBox" runat="server" TextMode="MultiLine"
                                                                    SkinID="sknMultiLineTextBox" MaxLength="8000" Width="466px" Height="80px" onkeyup="CommentsCount(8000,this)"
                                                                    onchange="CommentsCount(8000,this)" TabIndex="1"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="CreateTestSession_instructionsTextBox" runat="server" TextMode="MultiLine"
                                                                    MaxLength="8000" Width="466px" Height="80px" onkeyup="CommentsCount(8000,this)"
                                                                    onchange="CommentsCount(8000,this)" TabIndex="2"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_bg_testsession">
                                                    Settings
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="non_tab_body_bg">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="2" cellspacing="3" width="100%">
                                                                    <tr>
                                                                        <td style="width: 40%;">
                                                                            <asp:Label ID="CreateTestSession_sessionNoHeadLabel" runat="server" Text="Number of Sessions"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><%--<span class="mandatory">&nbsp;*</span>--%>
                                                                        </td>
                                                                        <td>
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="CreateTestSession_sessionNoTextBox" MaxLength="2" runat="server"
                                                                                            Width="30px" TabIndex="3"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="vertical-align: bottom">
                                                                                                    <asp:ImageButton ID="CreateTestSession_upImageButton" runat="server" ImageAlign="AbsBottom"
                                                                                                        SkinID="sknNumericUpArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="vertical-align: top">
                                                                                                    <asp:ImageButton ID="CreateTestSession_downImageButton" runat="server" ImageAlign="Top"
                                                                                                        SkinID="sknNumericDownArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:NumericUpDownExtender ID="CreateTestSession_NumericUpDownExtender" Width="60"
                                                                                runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="CreateTestSession_sessionNoTextBox"
                                                                                TargetButtonUpID="CreateTestSession_upImageButton" TargetButtonDownID="CreateTestSession_downImageButton">
                                                                            </ajaxToolKit:NumericUpDownExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateTestSession_timeLimitHeadLabel" runat="server" Text="Time Limit"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CreateTestSession_timeLimitTextBox" runat="server" Columns="8" MaxLength="8"
                                                                                TabIndex="4"></asp:TextBox>
                                                                            <ajaxToolKit:MaskedEditExtender ID="CreateTestSession_timeLimitMaskedEditExtender"
                                                                                runat="server" TargetControlID="CreateTestSession_timeLimitTextBox" Mask="99:99:99"
                                                                                MessageValidatorTip="true" MaskType="Time" AcceptAMPM="false" AcceptNegative="None"
                                                                                InputDirection="LeftToRight" ErrorTooltipEnabled="True" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateTestSession_recommendedTimeHeadLabel" runat="server" Text="Recommended Time Limit"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="CreateTestSession_recommendedTimeValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateTestSession_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 40%">
                                                                                        <asp:TextBox ID="CreateTestSession_expiryDateTextBox" runat="server" MaxLength="10"
                                                                                            AutoCompleteType="None" Columns="15" TabIndex="5"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="CreateTestSession_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:MaskedEditExtender ID="CreateTestSession_MaskedEditExtender" runat="server"
                                                                                TargetControlID="CreateTestSession_expiryDateTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                                                DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="CreateTestSession_MaskedEditValidator" runat="server"
                                                                                ControlExtender="CreateTestSession_MaskedEditExtender" ControlToValidate="CreateTestSession_expiryDateTextBox"
                                                                                EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                                TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="CreateTestSession_customCalendarExtender" runat="server"
                                                                                TargetControlID="CreateTestSession_expiryDateTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy"
                                                                                PopupPosition="BottomLeft" PopupButtonID="CreateTestSession_calendarImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="td_v_line">
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table width="100%" cellpadding="2" cellspacing="3" border="0" height="100px">
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:CheckBox ID="CreateTestSession_randomSelectionCheckBox" runat="server" TabIndex="6">
                                                                            </asp:CheckBox>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:Label ID="CreateTestSession_randomSelectionHeadLabel" runat="server" Text="Randomize Questions Ordering"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </div>
                                                                            <div>
                                                                                <asp:ImageButton ID="CreateTestSession_randomSelectionHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" TabIndex="8" OnClientClick="javascript:return false;"
                                                                                    ToolTip="Check this to display the questions in random ordering" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="CreateTestSession_cyberProctorateCheckBox" runat="server" TabIndex="7">
                                                                            </asp:CheckBox>
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 170px">
                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                            <asp:Label ID="CreateTestSession_cyberProctorateHeadLabel" runat="server" Text="Enable Cyber Proctoring"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:ImageButton ID="CreateTestSession_cyberProctorHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                runat="server" ImageAlign="Middle" TabIndex="8" OnClientClick="javascript:return false;"
                                                                                                ToolTip="Check this to proctor the test using the cyber proctoring utility" />
                                                                                        </div>
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <div>
                                                                                            <asp:LinkButton ID="CreateTestSession_cyberProctorWhyDisabledLinkButton" runat="server"
                                                                                                Text="Why this is disabled?" ToolTip="Click here to show the help on why this feature is disabled"
                                                                                                SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowWhyDisabled('Y')" />
                                                                                            <div id="CreateTestSession_cyberProctorWhyDisabledDiv" style="display: none; height: 160px;
                                                                                                width: 270px; left: 390px; top: 340px; z-index: 1; position: absolute" class="popupcontrol_why_disabled">
                                                                                                <table width="100%" cellpadding="10" cellspacing="0" border="0">
                                                                                                    <tr>
                                                                                                        <td style="width: 95%" class="popup_header_text" valign="middle" align="left">
                                                                                                            <asp:Literal ID="CreateTestSession_cyberProctorWhyDisabledDiv_titleLiteral" runat="server"
                                                                                                                Text="Why this is disabled?"></asp:Literal>
                                                                                                        </td>
                                                                                                        <td style="width: 5%" valign="top" align="right">
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:ImageButton ID="CreateTestSession_cyberProctorWhyDisabledDiv_topCancelImageButton" ToolTip="Click here to close the window"
                                                                                                                            runat="server" SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowWhyDisabled('N')" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2">
                                                                                                            <table cellpadding="10" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                                                                        <table width="100%" cellspacing="0" border="0">
                                                                                                                            <tr>
                                                                                                                                <td align="center" class="label_field_text">
                                                                                                                                    <asp:Literal ID="CreateTestSession_cyberProctorWhyDisabledDiv_messageLiteral" runat="server"
                                                                                                                                        Text="This feature is not applicable for your current subscription plan. You need to upgrade your account to use this feature. Please contact support for further information.">
                                                                                                                                    </asp:Literal>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="td_height_20">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display:none">
                                                                        <td>
                                                                            <asp:CheckBox ID="CreateTestSession_displayResultsCheckBox" runat="server" TabIndex="8">
                                                                            </asp:CheckBox>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:Label ID="CreateTestSession_displayResultsLabel" runat="server" Text="Display Results to Candidate"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </div>
                                                                            <div>
                                                                                <asp:ImageButton ID="CreateTestSession_displayResultsHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" TabIndex="8" OnClientClick="javascript:return false;"
                                                                                    ToolTip="Check this to show the results to the candidate after completion of the test" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="td_padding_top_5">
                                                    <asp:Button ID="CreateTestSession_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                        OnClick="CreateTestSession_saveButton_Click" TabIndex="9" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg" align="center">
                                        <asp:UpdatePanel ID="CreateTestSession_maxMinButtonUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="CreateTestSession_searchSessionResultsLiteral" runat="server" Text="Test Sessions"></asp:Literal>
                                                            <asp:Label ID="CreateTestSession_searchSessionResultsHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right" id="CreateTestSession_searchTestSessionResultsTR"
                                                            runat="server">
                                                            <span id="CreateTestSession_searchSessionResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="CreateTestSession_searchSessionResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                    id="CreateTestSession_searchSessionResultsDownSpan" runat="server" style="display: none;"><asp:Image
                                                                        ID="CreateTestSession_searchSessionResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tab_body_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="CreateTestSession_restoreHiddenField" runat="server" />
                                                    <asp:UpdatePanel ID="CreateTestSession_testSessionUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr id="CreateTestSession_testSessionGridviewTR" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <div style="height: 200px; overflow: auto;" runat="server" id="CreateTestSession_testSessionDiv">
                                                                            <asp:GridView ID="CreateTestSession_testSessionGridView" runat="server" AllowSorting="True"
                                                                                AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="CreateTestSession_testSessionGridView_Sorting"
                                                                                OnRowCommand="CreateTestSession_testSessionGridView_RowCommand" OnRowDataBound="CreateTestSession_testSessionGridView_RowDataBound"
                                                                                OnRowCreated="CreateTestSession_testSessionGridView_RowCreated" SkinID="sknWrapHeaderGrid">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="CreateTestSession_candidateDetailImageButton" runat="server"
                                                                                                SkinID="sknCandidateDetailImageButton" ToolTip="Candidate Detail" CommandName="CandidateDetail" />
                                                                                            <asp:ImageButton ID="CreateTestSession_viewTestSessionsImageButton" runat="server"
                                                                                                SkinID="sknViewSessionImageButton" ToolTip="View Test Session" CommandName="ViewTestSession"
                                                                                                CommandArgument='<%# Eval("TestSessionID") %>' Visible="true" />
                                                                                            <asp:ImageButton ID="CreateTestSession_cancelCreateTestSessionImageButton" runat="server"
                                                                                                SkinID="sknCancelImageButton" ToolTip="Cancel Test Session" CommandName="CancelTestSession"
                                                                                                CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:HiddenField runat="server" ID="CreateTestSession_attemptIdHiddenField" Value='<%# Eval("AttemptID") %>' />
                                                                                            <asp:HiddenField runat="server" ID="CreateTestSession_statusHiddenField" Value='<%# Eval("Status") %>' />
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Test&nbsp;Session&nbsp;ID" DataField="TestSessionID"
                                                                                        SortExpression="TestSessionID">
                                                                                        <ItemStyle Width="10%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField HeaderText="Test&nbsp;Key" Visible="false">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateTestSession_byTestSession_testKeyLabel" runat="server" Text='<%# Eval("TestID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Session&nbsp;ID" SortExpression="CANDIDATETESTSESSIONID DESC">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateTestSession_byTestSession_candidateSessionIdLabel" runat="server"
                                                                                                Text='<%# Eval("CandidateTestSessionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Position Profile" DataField="PositionProfileName" SortExpression="PositionProfile">
                                                                                        <ItemStyle Width="12%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField ItemStyle-Width="12%" HeaderText="Date of Purchase" SortExpression="CreatedDate DESC">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateTestSession_purchaseDateLabel" runat="server"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Credits (in&nbsp;$)" DataField="TotalCredit" SortExpression="TotalCredit DESC" Visible="false"
                                                                                        ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="td_padding_right_20" />
                                                                                    <asp:TemplateField HeaderText="Administered By" SortExpression="Administeredby">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateTestSession_testSessionAuthorFullnameLabel" runat="server" Text='<%# Eval("TestSessionAuthor") %>'
                                                                                                ToolTip='<%# Eval("TestSessionAuthorFullName")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Name" SortExpression="Candidatename">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateTestSession_candidateFullnameLabel" runat="server" Text='<%# Eval("CandidateName") %>'
                                                                                                ToolTip='<%# Eval("CandidateFullName")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Date of Test" SortExpression="ScheduledDate DESC">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateTestSession_scheduledDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).ScheduledDate) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="CreateTestSession_pageNavigatorUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <uc1:PageNavigator ID="CreateTestSession_bottomSessionPagingNavigator" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:UpdatePanel ID="CreateTestSession_cancelTestPopupUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="CreateTestSession_cancelTestPanel" runat="server" Style="display: none"
                                                    CssClass="popupcontrol_cancel_session">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateTestSession_hiddenButton" runat="server" Text="Hidden" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                                            <asp:Label ID="CreateTestSession_questionResultLiteral" runat="server" Text="Cancel Test Session"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%" align="right">
                                                                            <asp:ImageButton ID="CreateTestSession_cancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                                    <tr>
                                                                        <td class="popup_td_padding_10">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 100%" align="center">
                                                                                        <asp:Label ID="CreateTestSession_cancelErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="CreateTestSession_cancelReasonLabel" runat="server" Text="Reason"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="CreateTestSession_cancelTestReasonTextBox" runat="server" MaxLength="100"
                                                                                            SkinID="sknMultiLineTextBox" Columns="90" TextMode="MultiLine" Height="70" onkeyup="CommentsCount(100,this)"
                                                                                            onchange="CommentsCount(100,this)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="CreateTestSession_saveCancellationButton" runat="server" Text="Save"
                                                                                SkinID="sknButtonId" OnClick="CreateTestSession_saveCancellationButton_Click" />
                                                                            &nbsp;
                                                                            <asp:LinkButton ID="CreateTestSession_closeCancellationButton" runat="server" Text="Cancel"
                                                                                SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateTestSession_cancelSessionModalPopupExtender"
                                                    runat="server" PopupControlID="CreateTestSession_cancelTestPanel" TargetControlID="CreateTestSession_hiddenButton"
                                                    BackgroundCssClass="modalBackground" CancelControlID="CreateTestSession_closeCancellationButton">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="CreateTestSession_canidateDetailPanel" runat="server" Style="display: none;
                                                    height: auto;" CssClass="popupcontrol_question_detail">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateTestSession_canidateDetailHiddenButton" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc5:CandidateDetail ID="CreateTestSession_candidateDetailControl" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateTestSession_candidateDetailModalPopupExtender"
                                                    runat="server" TargetControlID="CreateTestSession_canidateDetailHiddenButton"
                                                    PopupControlID="CreateTestSession_canidateDetailPanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="CreateTestSession_viewTestSessionSavePanel" runat="server" Style="display: none"
                                                    CssClass="preview_test_session">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateTestSession_viewTestSessionSaveButton" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc2:TestSessionPreview ID="CreateTestSession_viewTestSessionSave_UserControl" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table border="0" cellspacing="3" cellpadding="3" align="left">
                                                                    <tr>
                                                                        <td class="td_padding_top_5" style="padding-left: 10px">
                                                                            <asp:Button ID="CreateTestSession_previewTestSessionControl_createButton" runat="server"
                                                                                Text="Create" OnClick="CreateTestSession_previewTestSessionControl_createButton_Click"
                                                                                SkinID="sknButtonId" />
                                                                        </td>
                                                                        <td class="td_padding_top_5">
                                                                            <asp:LinkButton ID="CreateTestSession_previewTestSessionControl_cancelButton" runat="server"
                                                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateTestSession_viewTestSessionSave_modalpPopupExtender"
                                                    runat="server" TargetControlID="CreateTestSession_viewTestSessionSaveButton"
                                                    PopupControlID="CreateTestSession_viewTestSessionSavePanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="CreateTestSession_previewTestSessionPanel" runat="server" CssClass="popupcontrol_question_detail"
                                                    Style="display: none">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateTestSession_previewTestSessionControl_Button" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc2:TestSessionPreview ID="CreateTestSession_previewTestSessionControl_userControl1"
                                                                    runat="server" Mode="view" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_padding_top_5" style="padding-left: 10px">
                                                                <asp:LinkButton ID="CreateTestSession_previewTestSessionControl_cancelButton2" runat="server"
                                                                    Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateTestSession_previewTestSessionControl_modalpPopupExtender1"
                                                    runat="server" TargetControlID="CreateTestSession_previewTestSessionControl_Button"
                                                    PopupControlID="CreateTestSession_previewTestSessionPanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateTestSession_bottomSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateTestSession_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateTestSession_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="CreateTestSession_TestKeyHiddenField" runat="server" />
    <asp:HiddenField ID="CreateTestSession_positionProfileIDHiddenField" runat="server" />
    <asp:HiddenField ID="CreateTestSession_candidateSessionIDsHiddenField" runat="server" />
    <asp:HiddenField ID="CreateTestSession_testSessionIDHiddenField" runat="server" />
</asp:Content>
