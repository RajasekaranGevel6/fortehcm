﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestScheduler.cs
// File that represents the user interface for searching scheduled candidates.
// This will interact with TestSchedulerBLManager to retrieve the candidate
// information from the database.

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Scheduler
{
    /// <summary>
    /// Class that represents the user interface for searching scheduled candidates.
    /// This will also provide to the recruiter to reschedule, unschedule, view the 
    /// candidate details, and results information.
    /// </summary>
    public partial class InterviewTestScheduler : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when the page is loade. 
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/>that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                InterviewTestScheduler_pagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(InterviewTestScheduler_pagingNavigator_PageNumberClick);

                Master.SetPageCaption("Interview Scheduler");
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    // Load default values.
                    LoadValues();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_ASSESSMENT))
                    {
                        base.ClearSearchCriteriaSession();

                        // Load the candidate session details
                        LoadScheduleDetails(1);
                    }
                    else

                        if (Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER] != null)
                        {
                            // Check if page is redirected from any child page. If the page
                            // if redirected from any child page, fill the search criteria
                            // and apply the search.
                            FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER]
                                as TestScheduleSearchCriteria);

                            // Reload the maximize hidden value.
                            CheckAndSetExpandorRestore();
                        }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will fire when the page number is clicked on gridview.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that contains the event data.
        /// </param>
        void InterviewTestScheduler_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            LoadScheduleDetails(e.PageNumber);
            ViewState["PAGE_NUMBER"] = e.PageNumber;
        }

        /// <summary>
        /// Handler that will call when the header of the gridview column is clicked.
        /// Then, it will sort the column accordingly.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_scheduleGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                // Assign sort expression column to the viewstate
                if (ViewState["SORT_EXPRESSION"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_DIRECTION_KEY"] =
                        ((SortType)ViewState["SORT_DIRECTION_KEY"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;
                else
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;

                ViewState["SORT_EXPRESSION"] = e.SortExpression;

                // Reload the pagination and gridview controls
                InterviewTestScheduler_pagingNavigator.Reset();
                LoadScheduleDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when every row is getting loaded with the data.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_scheduleGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField InterviewTestScheduler_candidateTestSessionIDHiddenField = (HiddenField)
                        e.Row.FindControl("InterviewTestScheduler_candidateTestSessionIDHiddenField");
                    HiddenField InterviewTestScheduler_Grid_testSessionIDHiddenField = (HiddenField)
                        e.Row.FindControl("InterviewTestScheduler_Grid_testSessionIDHiddenField");
                    HiddenField InterviewTestScheduler_Grid_ExpiryDateHiddenField = (HiddenField)
                        e.Row.FindControl("InterviewTestScheduler_Grid_ExpiryDateHiddenField");
                    HiddenField InterviewTestScheduler_attemptIDHiddenField = (HiddenField)
                        e.Row.FindControl("InterviewTestScheduler_attemptIDHiddenField");
                    HiddenField InterviewTestScheduler_Grid_testIDHiddenField = (HiddenField)
                        e.Row.FindControl("InterviewTestScheduler_Grid_testIDHiddenField");

                    Label InterviewTestScheduler_statusLabel = (Label)
                        e.Row.FindControl("InterviewTestScheduler_statusLabel");

                    HyperLink InterviewTestScheduler_viewResultHyperLink =
                        (HyperLink)e.Row.FindControl("InterviewTestScheduler_viewResultHyperLink");

                    ImageButton InterviewTestScheduler_viewShedulerImageButton =
                        (ImageButton)e.Row.FindControl("InterviewTestScheduler_viewShedulerImageButton");

                    ImageButton InterviewTestScheduler_rescheduleImageButton =
                        (ImageButton)e.Row.FindControl("InterviewTestScheduler_rescheduleImageButton");

                    ImageButton InterviewTestScheduler_unscheduleImageButton =
                        (ImageButton)e.Row.FindControl("InterviewTestScheduler_unscheduleImageButton");

                    ImageButton InterviewTestScheduler_viewCancelReasonImageButton =
                        (ImageButton)e.Row.FindControl("InterviewTestScheduler_viewCancelReasonImageButton");

                    HyperLink InterviewTestScheduler_candidateFullNameHyperLink =
                        (HyperLink)e.Row.FindControl("InterviewTestScheduler_candidateFullNameHyperLink");

                    HiddenField InterviewTestScheduler_Grid_candidateIDHiddenField =
                        (HiddenField)e.Row.FindControl("InterviewTestScheduler_Grid_candidateIDHiddenField");

                    // Initialize all the image buttons visibility as false.
                    InterviewTestScheduler_viewShedulerImageButton.Visible = false;
                    InterviewTestScheduler_rescheduleImageButton.Visible = false;
                    InterviewTestScheduler_unscheduleImageButton.Visible = false;
                    InterviewTestScheduler_viewResultHyperLink.Visible = false;
                    InterviewTestScheduler_viewCancelReasonImageButton.Visible = false;

                    // Check the status and display the image buttons accordingly.
                    if (InterviewTestScheduler_statusLabel.Text == "Scheduled" || 
                        InterviewTestScheduler_statusLabel.Text=="Offline Interview Initiated")
                    {
                        InterviewTestScheduler_viewShedulerImageButton.Visible = true;
                        InterviewTestScheduler_rescheduleImageButton.Visible = true;
                        InterviewTestScheduler_unscheduleImageButton.Visible = true;
                    }
                    else if (InterviewTestScheduler_statusLabel.Text == "Not Scheduled")
                        InterviewTestScheduler_rescheduleImageButton.Visible = false;
                    else if (InterviewTestScheduler_statusLabel.Text == "Cancelled")
                        InterviewTestScheduler_viewCancelReasonImageButton.Visible = true;
                    else if (InterviewTestScheduler_statusLabel.Text == "In Progress")
                    {
                        InterviewTestScheduler_viewShedulerImageButton.Visible = true;
                        InterviewTestScheduler_viewResultHyperLink.Visible = true;
                    }
                    if (InterviewTestScheduler_statusLabel.Text == "Completed" ||
                        InterviewTestScheduler_statusLabel.Text == "Quit" ||
                        InterviewTestScheduler_statusLabel.Text == "Elapsed")
                    {
                        InterviewTestScheduler_viewShedulerImageButton.Visible = true;
                        InterviewTestScheduler_viewResultHyperLink.Visible=true;
                    }
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    // View results
                    
                    InterviewTestScheduler_viewResultHyperLink.NavigateUrl="~/InterviewReportCenter/InterviewCandidateTestDetails.aspx?m=4&s=0" +
                        "&candidatesession=" + InterviewTestScheduler_candidateTestSessionIDHiddenField.Value +
                        "&attemptid=" + InterviewTestScheduler_attemptIDHiddenField.Value +
                        "&testkey=" + InterviewTestScheduler_Grid_testIDHiddenField.Value + "&parentpage=INT_TST_SCHD";

                    if (InterviewTestScheduler_candidateFullNameHyperLink != null)
                    {
                        if (InterviewTestScheduler_Grid_candidateIDHiddenField != null)
                        {
                            InterviewTestScheduler_candidateFullNameHyperLink.NavigateUrl =
                                "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&puid="
                                + InterviewTestScheduler_Grid_candidateIDHiddenField.Value;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// While unscheduling the candidate, it asks the user to confirm. Once he confirms to 
        /// unschedule, it triggers the below method to specify the button type 'Yes'/'No'
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ConfirmMessageEventArgs"/> that has a property ButtonType which
        /// specify the Actiontype.
        /// </param>
        void ScheduleCandidate_inactiveConfirmMsgControl_ConfirmMessageThrown
            (object sender, ConfirmMessageEventArgs e)
        {
            try
            {
                if (e.ButtonType ==Forte.HCM.DataObjects.ButtonType.Yes)
                {
                    ClearLabelMessage();

                    string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                    string attemptId = ViewState["ATTEMPT_ID"].ToString();

                    bool isMailSent = true;

                    // Update the session status as 'Not Scheduled'
                    new TestConductionBLManager().UpdateSessionStatus
                        (candidateSessionId, Convert.ToInt32(attemptId),
                        Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                        Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID, out isMailSent);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                   InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will fire when reschedule/unschedule/viewresult/view candidate 
        /// commands are called.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_scheduleGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ClearLabelMessage();

                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField attemptIdHiddenField = ((ImageButton)e.CommandSource).Parent.
                        FindControl("InterviewTestScheduler_attemptIDHiddenField") as HiddenField;
                HiddenField testSessionID = ((ImageButton)e.CommandSource).Parent.
                    FindControl("InterviewTestScheduler_Grid_testSessionIDHiddenField") as HiddenField;
                HiddenField InterviewTestScheduler_Grid_testIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("InterviewTestScheduler_Grid_testIDHiddenField") as HiddenField;
                HiddenField InterviewTestScheduler_Grid_ExpiryDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("InterviewTestScheduler_Grid_ExpiryDateHiddenField") as HiddenField;
                HiddenField InterviewTestScheduler_Grid_candidateNameHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("InterviewTestScheduler_Grid_candidateNameHiddenField") as HiddenField;
                HiddenField InterviewTestScheduler_candidateEmailHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("InterviewTestScheduler_candidateEmailHiddenField") as HiddenField;
                HiddenField InterviewTestScheduler_Grid_candidateIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("InterviewTestScheduler_Grid_candidateIDHiddenField") as HiddenField;
                HiddenField InterviewTestScheduler_Grid_sessionExpiryDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("InterviewTestScheduler_Grid_sessionExpiryDateHiddenField") as HiddenField;

                if (e.CommandName == "reschedule")
                {
                    // Reschedule candidate 
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    InterviewTestScheduler_scheduleCandiateLiteral.Text = "Reschedule";

                    ViewState["RESCHEDULE_STATUS"] = true;

                    ClearSchedulePopup();

                    // This will be used in SAVE button event handler of the schedule popup 
                    // to compare with current date.
                    InterviewTestScheduler_Popup_candidateIDHiddenField.Value = InterviewTestScheduler_Grid_candidateIDHiddenField.Value;
                    InterviewTestScheduler_expiryDateHiddenField.Value = InterviewTestScheduler_Grid_ExpiryDateHiddenField.Value;
                    InterviewTestScheduler_Popup_sessionExpiryDateValueLabel.Text = InterviewTestScheduler_Grid_sessionExpiryDateHiddenField.Value;
                    InterviewTestScheduler_Popup_candidateNameTextBox.Text = InterviewTestScheduler_Grid_candidateNameHiddenField.Value.Trim();
                    InterviewTestScheduler_Popup_emailTextBox.Text = InterviewTestScheduler_candidateEmailHiddenField.Value.Trim();
                    InterviewTestScheduler_Popup_scheduleDateTextBox.Text = InterviewTestScheduler_Grid_ExpiryDateHiddenField.Value;
                    InterviewTestScheduler_Popup_candidateNameImageButton.Visible = false;

                    InterviewTestScheduler_scheduleModalPopupExtender.Show();
                }
                else if (e.CommandName == "unschedule")
                {
                    // Code for unschedule
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;

                    InterviewTestScheduler_unschedulePopupExtenter.Show();
                }
                else if (e.CommandName == "viewSchedule")
                {
                    // Code for view schedule
                    InterviewTestScheduler_viewTestSchedule.CandidateSessionId = e.CommandArgument.ToString();

                    InterviewSessionDetail testSession = new InterviewSchedulerBLManager().
                        GetInterviewTestSessionDetailScheduler(testSessionID.Value.ToString(), e.CommandArgument.ToString());

                    InterviewTestScheduler_viewTestSchedule.InterviewSession = testSession;
                    InterviewTestScheduler_viewScheduleModalPopupExtender.Show();
                }
                else if (e.CommandName == "viewCancelReason")
                {
                    // Code for View cancel reason
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;

                    InterviewSessionDetail testSession =
                        new InterviewSchedulerBLManager().GetInteviewSessionDetail(testSessionID.Value.ToString(),
                        ViewState["CANDIDATE_SESSIONID"].ToString(),"Ascending",SortType.Ascending);

                    // Load the cancel reason text and display the popup.
                    InterviewTestScheduler_cancelTestReasonDiv.InnerHtml =
                        testSession.CandidateInterviewSessions[0].CancelReason.ToString();
                    InterviewTestScheduler_cancelSessionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                   InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on Save button in Schedule popup will trigger this method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();
                bool isMailSent = true;

                // Validate the scheduled date and display the error message if any.
                InterviewTestScheduler_MaskedEditValidator.Validate();

                if (!InterviewTestScheduler_MaskedEditValidator.IsValid)
                {
                    InterviewTestScheduler_scheduleErrorMsgLabel.Text =
                        InterviewTestScheduler_MaskedEditValidator.ErrorMessage;
                    InterviewTestScheduler_scheduleModalPopupExtender.Show();
                    return;
                }

                // Scheduled date should be between current date and test session's expiry date.
                if (DateTime.Parse(InterviewTestScheduler_Popup_scheduleDateTextBox.Text) >
                    DateTime.Parse(InterviewTestScheduler_Popup_sessionExpiryDateValueLabel.Text)
                    || DateTime.Parse(InterviewTestScheduler_Popup_scheduleDateTextBox.Text) < DateTime.Today)
                {
                    InterviewTestScheduler_scheduleErrorMsgLabel.Text =
                        Resources.HCMResource.ScheduleCandidate_ScheduledDateInvalid;
                    InterviewTestScheduler_scheduleModalPopupExtender.Show();

                    // Since the candidate name and email textboxes are readonly, its values are not 
                    // maintained on postback. Use Request[controlName.UniqueID] to get the value.
                    InterviewTestScheduler_Popup_candidateNameTextBox.Text =
                        Request[InterviewTestScheduler_Popup_candidateNameTextBox.UniqueID].Trim();
                    InterviewTestScheduler_Popup_emailTextBox.Text =
                        Request[InterviewTestScheduler_Popup_emailTextBox.UniqueID].Trim();
                    return;
                }

                if (ViewState["CANDIDATE_SESSIONID"] == null || ViewState["ATTEMPT_ID"] == null)
                    return;

                TestScheduleDetail testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = InterviewTestScheduler_Popup_candidateIDHiddenField.Value;
                testSchedule.CandidateTestSessionID = ViewState["CANDIDATE_SESSIONID"].ToString();
                testSchedule.AttemptID = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                testSchedule.EmailId = Request[InterviewTestScheduler_Popup_emailTextBox.UniqueID].Trim();

                // Set the time to maximum in expiry date.
                testSchedule.ExpiryDate = (Convert.ToDateTime(
                    InterviewTestScheduler_Popup_scheduleDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

                if (!Utility.IsNullOrEmpty(Convert.ToBoolean(ViewState["RESCHEDULE_STATUS"])) &&
                    Convert.ToBoolean(ViewState["RESCHEDULE_STATUS"]) == true)
                {
                    testSchedule.IsRescheduled = true;
                    if (!Utility.IsNullOrEmpty(InterviewTestScheduler_Popup_candidateIDHiddenField.Value))
                        testSchedule.CandidateID = InterviewTestScheduler_Popup_candidateIDHiddenField.Value;
                }

                // Reschedule candidate 
                new InterviewSchedulerBLManager().InterviewScheduleCandidate(testSchedule, base.userID, out isMailSent);

                if (isMailSent == false)
                {
                    InterviewTestScheduler_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    InterviewTestScheduler_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // Reload the grid
                LoadScheduleDetails(Convert.ToInt32(ViewState["PAGE_NUMBER"]));

                base.ShowMessage(InterviewTestScheduler_topSuccessMessageLabel,
                    InterviewTestScheduler_bottomSuccessMessageLabel,
                    Resources.HCMResource.TestScheduler_RescheduledSuccessfully);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule 
        /// the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_OkClick(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                bool isMailSent = true;
                bool isUnscheduled = true;

                // Get candidate details for the given candidate test session id and attempt id.
                CandidateInterviewSessionDetail candidateTestSessionDetail = new InterviewSchedulerBLManager().
                         GetCandidateInterviewSessionDetail(candidateSessionId, attemptId);

                // Update the session status as 'Not Scheduled' for the 'Unscheduled' candidates.
                new InterviewSchedulerBLManager().UpdateInterviewSessionStatus(candidateTestSessionDetail ,
                    Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                    Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID,
                    isUnscheduled, out isMailSent);

                if (isMailSent == false)
                {
                    InterviewTestScheduler_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    InterviewTestScheduler_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // TODO : Instead of loading the whole grid, update only that row.

                base.ShowMessage(InterviewTestScheduler_topSuccessMessageLabel,
                         InterviewTestScheduler_bottomSuccessMessageLabel,
                         Resources.HCMResource.ScheduleCandidate_UnScheduledStatus);

                // Reload the grid
                LoadScheduleDetails(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event handler that will load the candidate session detail
        /// whenever its been clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/>that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/>that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // To maintain author name when the page is post backed.
                InterviewTestScheduler_interviewSessionAuthorTextBox.Text =
                    Request[InterviewTestScheduler_interviewSessionAuthorTextBox.UniqueID].Trim();

                ClearLabelMessage();

                InterviewTestScheduler_pagingNavigator.Reset();
                ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;
                ViewState["SORT_EXPRESSION"] = "SESSION_CREATED_ON";

                LoadScheduleDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when a row is created in the gridview.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/>that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/>that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_scheduleGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    // Get last clicked column index
                    int sortColumnIndex = GetSortColumnIndex
                        (InterviewTestScheduler_scheduleGridView,
                        (string)ViewState["SORT_EXPRESSION"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_DIRECTION_KEY"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the reset link button is clicked.
        /// It will load grid as it was when first time loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/>that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/>that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_resetLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// It will clear all the session values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterviewTestScheduler_cancelLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Response.Redirect("~/OTMHome.aspx", false);
        }

        /// <summary>
        /// Handler that will call when schedule candidate button is clicked.
        /// It will redirect to the schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewTestScheduler_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&parentpage=INT_TST_SCHD", false);
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that will get candidate details who are scheduled for the test.
        /// </summary>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        private void LoadScheduleDetails(int pageNumber)
        {
            int totalRecords = 0;
            int sessionAuthorID = 0;
            TestScheduleSearchCriteria scheduleSearchCriteria = new TestScheduleSearchCriteria();

            // Check if the user is currently logged in.
            // Otherwise, pass the selected userid from the search user popup.
            if (Utility.IsNullOrEmpty(InterviewTestScheduler_testSessionAuthorIdHiddenField.Value.Trim()))
                sessionAuthorID = 0;
            else
                sessionAuthorID =
                    Convert.ToInt32(InterviewTestScheduler_testSessionAuthorIdHiddenField.Value.Trim());

            // Check if the search criteria fields are empty.
            // If this is empty, pass null. Or else, pass its value.
            scheduleSearchCriteria.CandidateName = InterviewTestScheduler_candidateNameTextBox.Text.Trim() == "" ?
                null : InterviewTestScheduler_candidateNameTextBox.Text.Trim();
            scheduleSearchCriteria.EMail = InterviewTestScheduler_emailIdTextBox.Text.Trim() == "" ?
                null : InterviewTestScheduler_emailIdTextBox.Text.Trim();
            scheduleSearchCriteria.TestScheduleAuthor = InterviewTestScheduler_interviewSessionAuthorTextBox.Text.Trim() == "" ?
                null : InterviewTestScheduler_interviewSessionAuthorTextBox.Text.Trim();
            scheduleSearchCriteria.TestSessionID = InterviewTestScheduler_interviewSessionIDTextBox.Text.Trim() == "" ?
                null : InterviewTestScheduler_interviewSessionIDTextBox.Text.Trim();
            scheduleSearchCriteria.TestName = InterviewTestScheduler_interviewNameTextBox.Text.Trim() == "" ?
                null : InterviewTestScheduler_interviewNameTextBox.Text.Trim();

            if (InterviewTestScheduler_statusDropDownList.SelectedValue == "0")
                scheduleSearchCriteria.TestCompletedStatus = TestCompletedStatus.None;
            else
                scheduleSearchCriteria.TestCompletedStatus =
                    InterviewTestScheduler_statusDropDownList.SelectedValue == "2" ?
                    TestCompletedStatus.Completed : TestCompletedStatus.NotCompleted;

            scheduleSearchCriteria.CurrentPage = pageNumber;
            scheduleSearchCriteria.IsMaximized =
                InterviewTestScheduler_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

            scheduleSearchCriteria.SortDirection = (SortType)ViewState["SORT_DIRECTION_KEY"];
            scheduleSearchCriteria.SortExpression = ViewState["SORT_EXPRESSION"].ToString();

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER] = scheduleSearchCriteria;

            List<TestScheduleDetail> testScheduleDetails = new InterviewSchedulerBLManager().GetInterviewScheduler
               (sessionAuthorID, pageNumber, base.GridPageSize, out totalRecords, scheduleSearchCriteria,
               scheduleSearchCriteria.SortExpression, scheduleSearchCriteria.SortDirection);

            if (testScheduleDetails.Count == 0)
            {
                base.ShowMessage(InterviewTestScheduler_topErrorMessageLabel,
                    InterviewTestScheduler_bottomErrorMessageLabel,
                    Resources.HCMResource.TestScheduler_NoDataFoundToDisplay);

                // Invisible paging control and gridview DIV.
                InterviewTestScheduler_schedulerDiv.Visible = false;
                InterviewTestScheduler_pagingNavigator.Visible = false;
            }
            else
            {
                InterviewTestScheduler_schedulerDiv.Visible = true;

                InterviewTestScheduler_scheduleGridView.DataSource = testScheduleDetails;
                InterviewTestScheduler_scheduleGridView.DataBind();

                InterviewTestScheduler_pagingNavigator.Visible = true;
                InterviewTestScheduler_pagingNavigator.PageSize = base.GridPageSize;
                InterviewTestScheduler_pagingNavigator.TotalRecords = totalRecords;
            }
        }

        /// <summary>
        /// This method clears the textboxes in Schedule/Reschedule popup.
        /// </summary>
        private void ClearSchedulePopup()
        {
            InterviewTestScheduler_Popup_candidateNameTextBox.Text = "";
            InterviewTestScheduler_Popup_emailTextBox.Text = "";
            InterviewTestScheduler_Popup_scheduleDateTextBox.Text = "";
            InterviewTestScheduler_scheduleErrorMsgLabel.Text = "";
        }

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            InterviewTestScheduler_topSuccessMessageLabel.Text = string.Empty;
            InterviewTestScheduler_bottomSuccessMessageLabel.Text = string.Empty;
            InterviewTestScheduler_topErrorMessageLabel.Text = string.Empty;
            InterviewTestScheduler_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(InterviewTestScheduler_isMaximizedHiddenField.Value) &&
                InterviewTestScheduler_isMaximizedHiddenField.Value == "Y")
            {
                InterviewTestScheduler_searchCriteriasDiv.Style["display"] = "none";
                InterviewTestScheduler_searchResultsUpSpan.Style["display"] = "block";
                InterviewTestScheduler_searchResultsDownSpan.Style["display"] = "none";
                InterviewTestScheduler_schedulerDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                InterviewTestScheduler_searchCriteriasDiv.Style["display"] = "block";
                InterviewTestScheduler_searchResultsUpSpan.Style["display"] = "none";
                InterviewTestScheduler_searchResultsDownSpan.Style["display"] = "block";
                InterviewTestScheduler_schedulerDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(InterviewTestScheduler_isMaximizedHiddenField.Value))
                    ((TestScheduleSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER]).IsMaximized =
                        InterviewTestScheduler_isMaximizedHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(TestScheduleSearchCriteria scheduleSearchCriteria)
        {
            if (scheduleSearchCriteria.CandidateName != null)
                InterviewTestScheduler_candidateNameTextBox.Text = scheduleSearchCriteria.CandidateName;

            if (scheduleSearchCriteria.EMail != null)
                InterviewTestScheduler_emailIdTextBox.Text = scheduleSearchCriteria.EMail;

            if (scheduleSearchCriteria.TestScheduleAuthor != null)
                InterviewTestScheduler_interviewSessionAuthorTextBox.Text = scheduleSearchCriteria.TestScheduleAuthor;

            if (scheduleSearchCriteria.TestSessionID != null)
                InterviewTestScheduler_interviewSessionIDTextBox.Text = scheduleSearchCriteria.TestSessionID;

            if (scheduleSearchCriteria.TestName != null)
                InterviewTestScheduler_interviewNameTextBox.Text = scheduleSearchCriteria.TestName;

            if (scheduleSearchCriteria.TestScheduleAuthor != null)
                InterviewTestScheduler_interviewSessionAuthorTextBox.Text = scheduleSearchCriteria.TestScheduleAuthor;

            if (scheduleSearchCriteria.TestCompletedStatus == TestCompletedStatus.Completed)
                InterviewTestScheduler_statusDropDownList.SelectedValue = "2";
            else if (scheduleSearchCriteria.TestCompletedStatus == TestCompletedStatus.NotCompleted)
                InterviewTestScheduler_statusDropDownList.SelectedValue = "1";
            else
                InterviewTestScheduler_statusDropDownList.SelectedValue = "0";

            InterviewTestScheduler_isMaximizedHiddenField.Value =
                    scheduleSearchCriteria.IsMaximized == true ? "Y" : "N";

            ViewState["SORT_DIRECTION_KEY"] = scheduleSearchCriteria.SortDirection;
            ViewState["SORT_EXPRESSION"] = scheduleSearchCriteria.SortExpression;

            // Apply search
            LoadScheduleDetails(scheduleSearchCriteria.CurrentPage);
            InterviewTestScheduler_pagingNavigator.MoveToPage(scheduleSearchCriteria.CurrentPage);
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            Page.Form.DefaultButton = InterviewTestScheduler_searchButton.UniqueID;

            // Assign the client side onclick events to gridview header
            InterviewTestScheduler_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                InterviewTestScheduler_schedulerDiv.ClientID + "','" +
                InterviewTestScheduler_searchCriteriasDiv.ClientID + "','" +
                InterviewTestScheduler_searchResultsUpSpan.ClientID + "','" +
                InterviewTestScheduler_searchResultsDownSpan.ClientID + "','" +
                InterviewTestScheduler_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            // Candidate Name Popup
            InterviewTestScheduler_candidateNameImageButton.Attributes.Add("onclick",
                "return LoadCandidates('" + InterviewTestScheduler_candidateNameTextBox.ClientID + "','"
                + InterviewTestScheduler_candidateSessionIDHiddenField.ClientID + "')");

            // Author name popup
            InterviewTestScheduler_interviewScheduleAuthorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + InterviewTestScheduler_testSessionAuthorHiddenField.ClientID + "','"
                + InterviewTestScheduler_testSessionAuthorIdHiddenField.ClientID + "','"
                + InterviewTestScheduler_interviewSessionAuthorTextBox.ClientID + "','TC')");

            // Open candidate detail popup for unschedule
            InterviewTestScheduler_Popup_candidateNameImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                    + InterviewTestScheduler_Popup_candidateNameTextBox.ClientID + "','"
                    + InterviewTestScheduler_Popup_emailTextBox.ClientID + "','"
                    + InterviewTestScheduler_Popup_candidateIDHiddenField.ClientID + "')");

            // Validate reschedule popup input fields.
            InterviewTestScheduler_saveButton.Attributes.Add("onclick", "return VaildateSchedulePopup('"
                        + InterviewTestScheduler_Popup_candidateNameTextBox.ClientID + "','"
                        + InterviewTestScheduler_Popup_scheduleDateTextBox.ClientID
                        + "','" + InterviewTestScheduler_scheduleErrorMsgLabel.ClientID + "')");

            // Keep the sort column, sort direction and the page number
            // in ViewState.
            if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;

            if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                ViewState["SORT_EXPRESSION"] = "SESSION_CREATED_ON";

            if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                ViewState["PAGE_NUMBER"] = "1";

            InterviewTestScheduler_interviewSessionAuthorTextBox.Text = new
                CommonBLManager().GetUserDetail(base.userID).FirstName;

            InterviewTestScheduler_testSessionAuthorIdHiddenField.Value = base.userID.ToString();

            //// If the logged in user is admin, then show the imagebutton.
            ////if (base.isAdmin)
            //{
            //    InterviewTestScheduler_testScheduleAuthorImageButton.Visible = true;
            //    InterviewTestScheduler_testSessionAuthorTextBox.Text = string.Empty;
            //    InterviewTestScheduler_testSessionAuthorIdHiddenField.Value = "0";
            //}
        }

        #endregion Protected Overridden Methods
    }
}