﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ScheduleCandidate.aspx.cs
// This page allows the recruiter to schedule the test to candidates. 
// They can reschedule, schedule, unschedule the candidate session tests.

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.InterviewScheduler
{
    /// <summary>
    /// This page allows the recruiter to schedule the test to candidates. 
    /// They can reschedule, schedule, unschedule the candidate session tests.
    /// </summary>
    public partial class InterviewScheduleCandidate : PageBase
    {
        #region Private Members                                                

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "100%";
        private string _tsid = string.Empty;

        #endregion Private Members

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when a page is loaded. During that time,
        /// it'll take the responsibility to implement the default settings
        /// such as page title, default button, and calling javascript
        /// functions etc.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set page title
                Master.SetPageCaption("Schedule Interview for Candidate");
                CheckAndSetExpandorRestore();

                // Set default button.
                Page.Form.DefaultButton = ScheduleCandidate_showButton.UniqueID;
                  UserDetail userDetail =new UserDetail();
                  userDetail = (UserDetail)Session["USER_DETAIL"];

                // MDM added
                  ScheduleCandidate_testSessionImageButton.Visible = true;
                ///MDM 
                //if (userDetail != null && userDetail.Roles != null)
                //{
                //    if (userDetail.Roles.Exists(item => item != UserRole.Recruiter))
                //        ScheduleCandidate_testSessionImageButton.Visible = true;
                //}
                  Session["Tenant_id"] = base.tenantID;
                if (!Page.IsPostBack)
                {
                    LoadValues();

                   

                    // Set default focus
                    ScheduleCandidate_testSessionIdTextBox.Focus();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING))
                    {
                        base.ClearSearchCriteriaSession();
                    }
                    else
                    {
                        // If any querystring is passed(i.e. testsessionid)
                        if (!Utility.IsNullOrEmpty(Request.QueryString["interviewsessionid"]))
                        {
                            _tsid = Convert.ToString(Request.QueryString["interviewsessionid"]);

                            // If test session id is passed in Querystring, then display all the informations
                            // with respect to testSessionId.
                            ScheduleCandidate_testSessionIdTextBox.Text = _tsid;
                            ScheduleCandidate_testDetailsDiv.Style["display"] = "block";
                            ScheduleCandidate_testDetailsGridDiv.Style["display"] = "block";
                            ScheduleCandidate_candidateSessionHeader.Style["display"] = "block";

                            // Load the candidate session details grid.
                            LoadInterviewAndCandidateSessions();
                        }
                        else
                        {
                            if (Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] != null)
                            {
                                // if redirected from any child page, fill the search criteria
                                // and apply the search.
                                FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]
                                    as TestScheduleSearchCriteria);

                                // Reload the maximize hidden value.
                                CheckAndSetExpandorRestore();
                            }
                        }
                    }

                    // Load the search test session popup automatically, when 
                    // navigating from talentscout.
                    // Check if page is navigated from talentscout and position profile is found
                    if (Request.QueryString["parentpage"] != null &&
                        Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                        Request.QueryString["positionprofileid"] != null)
                    {
                        // Get position profile ID.
                        int positionProfileID = 0;
                        if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                        {
                            // Load the position profile popup.
                            ScriptManager.RegisterStartupScript
                                (this, this.GetType(), "LoadTestSessionWithPositionProfile",
                                "javascript: LoadTestSessionWithPositionProfile('"
                                + ScheduleCandidate_testSessionIdTextBox.ClientID + "','" + 
                                positionProfileID + "')", true);
                        }
                    }
                }



                // Clear the error and success messages on every postback.
                ScheduleCandidate_bottomErrorMessageLabel.Text =
                    ScheduleCandidate_bottomSuccessMessageLabel.Text =
                    ScheduleCandidate_topErrorMessageLabel.Text =
                    ScheduleCandidate_topSuccessMessageLabel.Text = "";
                ScheduleCandidate_Popup_createCandidateButton.Attributes.Add("onclick",
                "return ShowCandidatePopup('" + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','" + ScheduleCandidate_Popup_emailTextBox.ClientID + "','" + ScheduleCandidate_candidateIdHiddenField.ClientID + "')");
                
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// While unscheduling the candidate, it asks the user to confirm. Once he confirms to 
        /// unschedule, it triggers the below method to specify the button type 'Yes'/'No'
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ConfirmMessageEventArgs"/> that contains the event data.
        /// </param>
        /// <remarks>
        /// This parameter has a property ButtonType which specify the Actiontype
        /// </remarks>
        void ScheduleCandidate_inactiveConfirmMsgControl_ConfirmMessageThrown
            (object sender, ConfirmMessageEventArgs e)
        {
            try
            {
                if (e.ButtonType ==Forte.HCM.DataObjects.ButtonType.Yes)
                {
                    string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                    string attemptId = ViewState["ATTEMPT_ID"].ToString();

                    bool isMailSent = true;

                    // Update the session status as 'Not Scheduled'
                    new TestConductionBLManager().UpdateSessionStatus
                        (candidateSessionId, Convert.ToInt32(attemptId),
                        Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                        Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID, out isMailSent);

                    // TODO : Instead of loading the whole grid, update only that row.
                    //LoadValues();
                    LoadInterviewAndCandidateSessions();
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_OkClick(object sender, EventArgs e)
        {
            try
            {
                string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());

                bool isMailSent = true;
                bool isUnscheduled = true;

                // Get candidate details for the given candidate test session id and attempt id.
                CandidateInterviewSessionDetail candidateInterviewSessionDetail = new InterviewSchedulerBLManager().
                         GetCandidateInterviewSession(candidateSessionId, attemptId);

                // Update the session status as 'Not Scheduled'
                new InterviewSchedulerBLManager().UpdateInterviewSessionStatus(candidateInterviewSessionDetail,
                     Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                     Constants.CandidateSessionStatus.NOT_SCHEDULED,
                     base.userID, isUnscheduled, out isMailSent);
                

                if (isMailSent == false)
                {
                    ScheduleCandidate_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    ScheduleCandidate_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // TODO : Instead of loading the whole grid, update only that row.
                LoadInterviewAndCandidateSessions();
                base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                         ScheduleCandidate_bottomSuccessMessageLabel,
                         Resources.HCMResource.ScheduleCandidate_UnScheduledStatus);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This handler gets triggered on clicking the reset button.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_resetLinkButton_Click
            (object sender, EventArgs e)
        {
            ScheduleCandidate_testSessionIdTextBox.Text = string.Empty;
            ScheduleCandidate_candidateSessionIdTextBox.Text = string.Empty;
            Response.Redirect("InterviewScheduleCandidate.aspx?m=2&s=1", false);
        }

        /// <summary>
        /// Show button will display the test session details and its corresponding 
        /// candidate session keys.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        public void ScheduleCandidate_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check the user input, if it is valid, get the test session details.
                if (!IsValidData())
                {
                    ScheduleCandidate_testDetailsDiv.Style["display"] = "none";
                    ScheduleCandidate_testDetailsGridDiv.Style["display"] = "none";
                    return;
                }

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CandidateSessionId";

                // Load the candidate session details grid.
                LoadInterviewAndCandidateSessions();
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);

                if (exp.Message.Contains("Interview Session Id not found"))
                {
                    ScheduleCandidate_testDetailsDiv.Style["display"] = "none";
                    ScheduleCandidate_testDetailsGridDiv.Style["display"] = "none";
                }
            }
        }

        /// <summary>
        /// Show button will display the test session details and its corresponding 
        /// candidate session keys.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        public void ScheduleCandidate_refreshRequestExternalButton_Click(object sender, EventArgs e)
        {
            try
            {
                base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,"Request has been sent successfully");
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Display the schedule/unschedule/reschedule/viewresults image buttons 
        /// according to the status field
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_candidateSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                Label ScheduleCandidate_statusLabel =
                    (Label)e.Row.FindControl("ScheduleCandidate_statusLabel");
                ImageButton ScheduleCandidate_scheduleImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_scheduleImageButton");
                ImageButton ScheduleCandidate_rescheduleImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_rescheduleImageButton");
                ImageButton ScheduleCandidate_unscheduleImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_unscheduleImageButton");
                ImageButton ScheduleCandidate_viewShedulerImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_viewShedulerImageButton");
                ImageButton ScheduleCandidate_retakeRequestImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_retakeRequestImageButton");
                ImageButton ScheduleCandidate_viewCancelReasonImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_cancelReasonDisplayImageButton");
                ImageButton ScheduleCandidate_requestExternalAssessorImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_requestExternalAssessorImageButton");

                HiddenField ScheduleCandidate_attemptIDHiddenField =
                    (HiddenField)e.Row.FindControl("ScheduleCandidate_attemptIDHiddenField");
                HiddenField ScheduleCandidate_candidateSessionIDHiddenField =
                    (HiddenField)e.Row.FindControl("ScheduleCandidate_candidateSessionIDHiddenField");

                HyperLink ScheduleCandidate_viewResultHyperLink =
                    (HyperLink)e.Row.FindControl("ScheduleCandidate_viewResultHyperLink");
                HyperLink ScheduleCandidate_viewInterviewRatingHyperLink =
                    (HyperLink)e.Row.FindControl("ScheduleCandidate_viewInterviewRatingHyperLink");

                HyperLink ScheduleCandidate_candidateHyperLink =
                    (HyperLink)e.Row.FindControl("ScheduleCandidate_candidateHyperLink");

                HiddenField ScheduleCandidate_Grid_candidateIDHiddenField =
                    (HiddenField)e.Row.FindControl("ScheduleCandidate_Grid_candidateIDHiddenField");


                // Initialize all the image buttons visibility as false.
                ScheduleCandidate_viewInterviewRatingHyperLink.Visible = false;
                ScheduleCandidate_viewShedulerImageButton.Visible = false;
                ScheduleCandidate_scheduleImageButton.Visible = false;
                ScheduleCandidate_rescheduleImageButton.Visible = false;
                ScheduleCandidate_unscheduleImageButton.Visible = false;
                ScheduleCandidate_retakeRequestImageButton.Visible = false;
                ScheduleCandidate_viewCancelReasonImageButton.Visible = false;
                ScheduleCandidate_viewResultHyperLink.Visible = false;
                ScheduleCandidate_requestExternalAssessorImageButton.Visible = false;

                //Enable request to external assessor icon
                if (ScheduleCandidate_statusLabel.Text == "Scheduled" ||
                    ScheduleCandidate_statusLabel.Text == "Completed")
                {
                    ScheduleCandidate_requestExternalAssessorImageButton.Visible = true;
                }

                // Check the status and display the image buttons accordingly.
                if (ScheduleCandidate_statusLabel.Text == "Not Scheduled")
                    ScheduleCandidate_scheduleImageButton.Visible = true;

                else if (ScheduleCandidate_statusLabel.Text == "Scheduled")
                {
                    ScheduleCandidate_viewShedulerImageButton.Visible = true;
                    ScheduleCandidate_rescheduleImageButton.Visible = true;
                    ScheduleCandidate_unscheduleImageButton.Visible = true;
                }

                else if (ScheduleCandidate_statusLabel.Text == "Cancelled")
                    ScheduleCandidate_viewCancelReasonImageButton.Visible = true;

                else if (ScheduleCandidate_statusLabel.Text == "In Progress")
                {
                    ScheduleCandidate_viewShedulerImageButton.Visible = true;
                    ScheduleCandidate_viewResultHyperLink.Visible = true;
                }
                if (ScheduleCandidate_statusLabel.Text == "Completed" ||
                    ScheduleCandidate_statusLabel.Text == "Quit" ||
                    ScheduleCandidate_statusLabel.Text == "Elapsed")
                {
                    ScheduleCandidate_viewInterviewRatingHyperLink.Visible = true;
                    ScheduleCandidate_viewShedulerImageButton.Visible = true;
                    ScheduleCandidate_viewResultHyperLink.Visible = true;
                    // Check the retake request field and display the RetakeRequest image.
                    HiddenField retakeRequestHiddenField =
                        e.Row.FindControl("ScheduleCandidate_requestRetakeHiddenField") as HiddenField;

                    string retakeRequest = retakeRequestHiddenField.Value;

                    if (retakeRequest == "Y")
                        ScheduleCandidate_retakeRequestImageButton.Visible = true;
                }

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                // View results
                ScheduleCandidate_viewResultHyperLink.NavigateUrl = "~/InterviewReportCenter/InterviewCandidateTestDetails.aspx?m=4&s=0" +
                    "&candidatesession=" + ScheduleCandidate_candidateSessionIDHiddenField.Value +
                    "&attemptid=" + ScheduleCandidate_attemptIDHiddenField.Value +
                    "&testkey=" + ScheduleCandidate_testIDLabel.Text.ToString() + "&parentpage=INT_TST_SCHD";

                ScheduleCandidate_viewInterviewRatingHyperLink.NavigateUrl = "~/Assessments/CandidateAssessorSummary.aspx?m=4&s=0" +
                   "&candidatesessionid=" + ScheduleCandidate_candidateSessionIDHiddenField.Value +
                   "&attemptid=" + ScheduleCandidate_attemptIDHiddenField.Value +
                   "&parentpage=ISCH_CAND";

                if (ScheduleCandidate_statusLabel.Text != "Not Scheduled")
                {
                    if (!Utility.IsNullOrEmpty(ScheduleCandidate_Grid_candidateIDHiddenField.Value))
                        ScheduleCandidate_candidateHyperLink.NavigateUrl =
                            "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&puid=" + ScheduleCandidate_Grid_candidateIDHiddenField.Value;
                }  
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Check whether the clicked button is schedule/reschedule/unschedule/viewschedule
        /// and display the Modal popup accordingly.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_candidateSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ShowAssessor")
                {
                    int attemptId = 0;
                    HiddenField scheduleCandidate_attemptIDHiddenField = null;
                    if (e.CommandSource is ImageButton)
                    {
                        scheduleCandidate_attemptIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                        FindControl("ScheduleCandidate_attemptIDHiddenField") as HiddenField;
                    }
                    else if (e.CommandSource is LinkButton)
                    {
                       scheduleCandidate_attemptIDHiddenField = ((LinkButton)e.CommandSource).Parent.
                       FindControl("ScheduleCandidate_attemptIDHiddenField") as HiddenField;
                    }
                    
                    if (!Utility.IsNullOrEmpty(scheduleCandidate_attemptIDHiddenField.Value))
                        attemptId = Convert.ToInt32(scheduleCandidate_attemptIDHiddenField.Value);

                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    Session["ASSESSOR"] = null;
                    ScriptManager.RegisterStartupScript(ScheduleCandidate_candidateSessionGridViewUpdatePanel,
                        ScheduleCandidate_candidateSessionGridViewUpdatePanel.GetType(),
                        "RecommendWindow", "SearchInterviewRecommendAssessor('CANDIDATEKEY','" + ViewState["CANDIDATE_SESSIONID"].ToString() + "'," + attemptId + ",null,'" + ScheduleCandidate_showButton.ClientID + "')", true);
                    return;
                }

                if (e.CommandName == "RequestExternalAssessor")
                {
                    int attemptId = 0;
                    HiddenField scheduleCandidate_attemptIDHiddenField = null;
                    if (e.CommandSource is ImageButton)
                    {
                        scheduleCandidate_attemptIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                        FindControl("ScheduleCandidate_attemptIDHiddenField") as HiddenField;
                    }
                    else if (e.CommandSource is LinkButton)
                    {
                        scheduleCandidate_attemptIDHiddenField = ((LinkButton)e.CommandSource).Parent.
                        FindControl("ScheduleCandidate_attemptIDHiddenField") as HiddenField;
                    }

                    if (!Utility.IsNullOrEmpty(scheduleCandidate_attemptIDHiddenField.Value))
                        attemptId = Convert.ToInt32(scheduleCandidate_attemptIDHiddenField.Value);

                    ScriptManager.RegisterStartupScript(ScheduleCandidate_candidateSessionGridViewUpdatePanel,
                        ScheduleCandidate_candidateSessionGridViewUpdatePanel.GetType(),
                        "RequestExternalAssessorWindow", "RequestExternalAssessor(" + attemptId + ",'" + e.CommandArgument + "','" + ScheduleCandidate_refreshRequestExternalButton.ClientID + "')", true);
                    return;
                }

                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField attemptIdHiddenField = ((ImageButton)e.CommandSource).Parent.
                        FindControl("ScheduleCandidate_attemptIDHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_modifiedDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_modifiedDateHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_ExpiryDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_ExpiryDateHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_candidateEmailHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_candidateEmailHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_candidateNameHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_candidateNameHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_candidateIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_candidateIDHiddenField") as HiddenField;

                HiddenField ScheduleCandidate_Grid_emailReminderHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_emailReminderHiddenField") as HiddenField; 

                ScheduleCandidate_emailRemainderCheckBox.Checked = false;

                // Schedule Candidate
                if (e.CommandName == "schedule")
                {
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["STATUS"] = "SCHEDULE";
                    ScheduleCandidate_recommendAssessorLinkButton.Visible = true;
                    // Assign modified date in ViewState
                    ViewState["MODIFIED_DATE"] =
                        Convert.ToDateTime(ScheduleCandidate_modifiedDateHiddenField.Value);

                    ScheduleCandidate_questionResultLiteral.Text = "Schedule";

                    // Clear the fields before displaying the schedule popup
                    ClearSchedulePopup();

                    ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                        ScheduleCandidate_expiryDateLabel.Text.Trim();

                    ScheduleCandidate_candidateNameImageButton.Visible = true;
                    ScheduleCandidate_positionProfileCandidateImageButton.Visible = true;
                    ScheduleCandidate_Popup_createCandidateButton.Visible = true;
                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                } 
                else if (e.CommandName == "reschedule")
                {
                    // Reschedule candidate 
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ScheduleCandidate_questionResultLiteral.Text = "Reschedule";
                    ViewState["STATUS"] = "RESCHEDULE";

                    ScheduleCandidate_recommendAssessorLinkButton.Visible = false;
                    // Clear the fields before displaying the reschedule popup
                    ClearSchedulePopup();

                    // This will be used in SAVE button event handler of the schedule popup 
                    // to compare with current date.
                    ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                        ScheduleCandidate_expiryDateLabel.Text;
                    ScheduleCandidate_Popup_candidateNameTextBox.Text =
                        ScheduleCandidate_Grid_candidateNameHiddenField.Value.Trim();
                    ScheduleCandidate_Popup_emailTextBox.Text =
                        ScheduleCandidate_Grid_candidateEmailHiddenField.Value.Trim();
                    ScheduleCandidate_reScheduleCandidateIDHiddenField.Value =
                        ScheduleCandidate_Grid_candidateIDHiddenField.Value.Trim();
                    ScheduleCandidate_scheduleDatePopupTextBox.Text =
                        ScheduleCandidate_Grid_ExpiryDateHiddenField.Value;

                    if (ScheduleCandidate_Grid_emailReminderHiddenField != null)
                        if (ScheduleCandidate_Grid_emailReminderHiddenField.Value == "Y")
                        {
                            ScheduleCandidate_emailRemainderCheckBox.Checked = true;
                        }

                    ScheduleCandidate_candidateNameImageButton.Visible = false;
                    ScheduleCandidate_positionProfileCandidateImageButton.Visible = false;
                    ScheduleCandidate_Popup_createCandidateButton.Visible = false;
                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                }
                else if (e.CommandName == "retakeTest")
                {
                    //Get the values 
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["STATUS"] = "RETAKE";

                    //Check the retake validation . whether the retake are allowed . 
                    //Check the number of elapsed days a
                    string retakeCount = string.Empty;
                    Dictionary<string, string> dicRetakeDetails = new
                        Dictionary<string, string>();
                    string isCertificateTest = string.Empty;
                    string testKey = ScheduleCandidate_testIDLabel.Text;
                    int attemptID = int.Parse(attemptIdHiddenField.Value);
                    string pendingAttempt = new CandidateBLManager().TestRetakeValidation(e.CommandArgument.ToString(), testKey,
                        attemptID, out retakeCount, out isCertificateTest, out dicRetakeDetails);

                    if (Convert.ToInt32(isCertificateTest) > 0)
                    {
                        if (dicRetakeDetails != null && dicRetakeDetails.Count > 0)
                        {
                            foreach (KeyValuePair<string, string> retakeDetail in dicRetakeDetails)//Temporary-will be removed//
                            {
                                if (Convert.ToInt32(retakeDetail.Key) > 0)
                                {
                                    ScheduleCandidate_RetakeValidation_ConfirmMsgControl.Message =
                                        "You are allowed to retake your interview only after " + retakeDetail.Key + " days from "
                                        + retakeDetail.Value;

                                    ScheduleCandidate_RetakeValidation_ModalPopupExtender.Show();
                                }
                                else
                                {
                                    // Code for retaking test request                    
                                    ScheduleCandidate_questionResultLiteral.Text = "Schedule Retake Request";
                                    ClearSchedulePopup();

                                    // Bind all fields to retake request
                                    ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                                        ScheduleCandidate_expiryDateLabel.Text;
                                    ScheduleCandidate_Popup_candidateNameTextBox.Text =
                                        ScheduleCandidate_Grid_candidateNameHiddenField.Value.Trim();
                                    ScheduleCandidate_Popup_emailTextBox.Text =
                                        ScheduleCandidate_Grid_candidateEmailHiddenField.Value.Trim();
                                    ScheduleCandidate_reScheduleCandidateIDHiddenField.Value =
                                        ScheduleCandidate_Grid_candidateIDHiddenField.Value.Trim();

                                    ScheduleCandidate_candidateNameImageButton.Visible = false;
                                    ScheduleCandidate_positionProfileCandidateImageButton.Visible = false;
                                    ScheduleCandidate_Popup_createCandidateButton.Visible = false;
                                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                                }
                            }
                        }
                    }
                    else
                    {
                        // Code for retaking test request                    
                        ScheduleCandidate_questionResultLiteral.Text = "Schedule Retake Request";
                        ClearSchedulePopup();

                        // Bind all fields to retake request
                        ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                            ScheduleCandidate_expiryDateLabel.Text;
                        ScheduleCandidate_Popup_candidateNameTextBox.Text =
                            ScheduleCandidate_Grid_candidateNameHiddenField.Value.Trim();
                        ScheduleCandidate_Popup_emailTextBox.Text =
                            ScheduleCandidate_Grid_candidateEmailHiddenField.Value.Trim();
                        ScheduleCandidate_reScheduleCandidateIDHiddenField.Value =
                            ScheduleCandidate_Grid_candidateIDHiddenField.Value.Trim();

                        ScheduleCandidate_candidateNameImageButton.Visible = false;
                        ScheduleCandidate_positionProfileCandidateImageButton.Visible = false;
                        ScheduleCandidate_Popup_createCandidateButton.Visible = false;
                        ScheduleCandidate_scheduleModalPopupExtender.Show();
 
                    }

                }
                else if (e.CommandName == "unschedule")
                {
                    // Code for unschedule
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;

                    ScheduleCandidate_unscheduleCandidateModalPopupExtender.Show();
                }
                else if (e.CommandName == "viewSchedule")
                {
                    // Code for view schedule
                    ScheduleCandidate_viewTestSchedule.CandidateSessionId = e.CommandArgument.ToString();
                    InterviewSessionDetail testSession = null;
                    if (Cache["SEARCH_DATATABLE"] != null)
                    {
                        testSession = Cache["SEARCH_DATATABLE"] as InterviewSessionDetail;

                        // Assign the TestSessionDetail instance to ViewTestSchedule popup to 
                        // display all information.
                        ScheduleCandidate_viewTestSchedule.InterviewSession = testSession;
                        ScheduleCandidate_viewScheduleModalPopupExtender.Show();
                    }
                }
                else if (e.CommandName == "viewCancelReason")
                {
                    // Code for View cancel reason
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    InterviewSessionDetail testSession = null;

                    if (Cache["SEARCH_DATATABLE"] != null)
                    {
                        testSession = (InterviewSessionDetail)Cache["SEARCH_DATATABLE"];
                        GridViewRow row = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                        int strCurrentRowIndex = row.RowIndex;

                        // Load the cancel reason text and display the popup.
                        ScheduleCandidate_cancelTestReasonDiv.InnerHtml =
                            testSession.CandidateInterviewSessions[strCurrentRowIndex].CancelReason.ToString();
                        ScheduleCandidate_cancelSessionModalPopupExtender.Show();
                    }
                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on Save button in Schedule popup will trigger this method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate the scheduled date and display the error message if any.
                ScheduleCandidate_MaskedEditValidator.Validate();

                if (!ScheduleCandidate_MaskedEditValidator.IsValid)
                {
                    ScheduleCandidate_scheduleErrorMsgLabel.Text =
                        ScheduleCandidate_MaskedEditValidator.ErrorMessage;
                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                    return;
                }

                // Scheduled date should be between current date and test session's expiry date.
                InterviewSessionDetail interviewSession = Cache["SEARCH_DATATABLE"] as InterviewSessionDetail;

                if (interviewSession != null)
                {
                    if (DateTime.Parse(ScheduleCandidate_scheduleDatePopupTextBox.Text) > interviewSession.ExpiryDate
                        || DateTime.Parse(ScheduleCandidate_scheduleDatePopupTextBox.Text) < DateTime.Today)
                    {
                        ScheduleCandidate_scheduleErrorMsgLabel.Text =
                            Resources.HCMResource.ScheduleCandidate_InterviewScheduledDateInvalid;
                        ScheduleCandidate_scheduleModalPopupExtender.Show();

                        // Since the candidate name and email textboxes are readonly, its values are not 
                        // maintained on postback. Use Request[controlName.UniqueID] to get the value.
                        ScheduleCandidate_Popup_candidateNameTextBox.Text =
                            Request[ScheduleCandidate_Popup_candidateNameTextBox.UniqueID].Trim();
                        ScheduleCandidate_Popup_emailTextBox.Text =
                            Request[ScheduleCandidate_Popup_emailTextBox.UniqueID].Trim();
                        return;
                    }
                }

                if (ViewState["CANDIDATE_SESSIONID"] == null || ViewState["ATTEMPT_ID"] == null)
                    return;

                TestScheduleDetail testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = ScheduleCandidate_candidateIdHiddenField.Value;
                testSchedule.CandidateTestSessionID = ViewState["CANDIDATE_SESSIONID"].ToString();
                testSchedule.AttemptID = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                testSchedule.EmailId = Request[ScheduleCandidate_Popup_emailTextBox.UniqueID].Trim();

                if (ScheduleCandidate_emailRemainderCheckBox.Checked)
                    testSchedule.EmailReminder = "Y";
                else
                    testSchedule.EmailReminder = "N";

                // Set the time to maximum in expiry date.
                testSchedule.ExpiryDate = (Convert.ToDateTime(
                    ScheduleCandidate_scheduleDatePopupTextBox.Text)).Add(new TimeSpan(23, 59, 59));

                bool isMailSent = true;
                
                //Assessor & skill
                List<AssessorDetail> assessorDetails = null;

                if (Session["ASSESSOR"] == null)
                    assessorDetails = new List<AssessorDetail>();
                else
                    assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;

                if (!Utility.IsNullOrEmpty(ViewState["STATUS"]) &&
                    ViewState["STATUS"].ToString().ToUpper() == "RESCHEDULE")
                {
                    testSchedule.IsRescheduled = true;

                    if (!Utility.IsNullOrEmpty(ScheduleCandidate_reScheduleCandidateIDHiddenField.Value))
                        testSchedule.CandidateID = ScheduleCandidate_reScheduleCandidateIDHiddenField.Value;

                    new InterviewSchedulerBLManager().ScheduleCandidate(testSchedule,assessorDetails,base.userID,out isMailSent);

                    // Once the data is updated show the success message.
                    base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                            ScheduleCandidate_bottomSuccessMessageLabel,
                            Resources.HCMResource.ScheduleCandidate_ReScheduledSuccesss);

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleCandidate_topErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }
                else if (!Utility.IsNullOrEmpty(ViewState["STATUS"]) &&
                    ViewState["STATUS"].ToString().ToUpper() == "SCHEDULE")
                {

                    //check whether the same candidate is assigned for test session id
                    string isCandidateAlreadyAssignedSchedulerName = new InterviewSchedulerBLManager().CheckCandidateAlreadyAssigned(testSchedule.CandidateTestSessionID, testSchedule.CandidateID);

                    if (isCandidateAlreadyAssignedSchedulerName == null ||
                        isCandidateAlreadyAssignedSchedulerName.Trim().Length == 0)
                    {
                        
                        new InterviewSchedulerBLManager().ScheduleCandidate(testSchedule, assessorDetails, base.userID, out isMailSent);
                        base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                            ScheduleCandidate_bottomSuccessMessageLabel,
                            Resources.HCMResource.ScheduleCandidate_ScheduledSuccess);
                    }
                    else
                    {
                        base.ShowMessage(ScheduleCandidate_scheduleErrorMsgLabel,
                            "Already Candidate " + ScheduleCandidate_Popup_candidateNameTextBox.Text +
                            " has been scheduled in this interview session");
                        ScheduleCandidate_scheduleModalPopupExtender.Show();
                        return;
                    }

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleCandidate_topErrorMessageLabel.Text =
                               Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }
                else // Retake a test
                {
                    //new TestSchedulerBLManager().RetakeTest(testSchedule, base.userID, out isMailSent);
                    base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                        ScheduleCandidate_bottomSuccessMessageLabel,
                        Resources.HCMResource.ScheduleCandidate_ReTakeSuccesss);

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleCandidate_topErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }

                // TODO : Instead of loading the whole grid, update only that row.
                LoadInterviewAndCandidateSessions();
                ViewState["STATUS"] = null;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Methods                                              

        /// <summary>
        /// Return the proper status message according to the DB session values.
        /// </summary>
        /// <param name="pStatus">
        /// A <see cref="string"/> that contains the test status
        /// </param>
        /// <returns>
        /// It will return the test status.
        /// </returns>
        protected string GetStatus(string pStatus)
        {
            string status = "";
            switch (pStatus.Trim())
            {
                case Constants.CandidateSessionStatus.ELAPSED:
                    status = "Elapsed";
                    break;
                case Constants.CandidateSessionStatus.IN_PROGRESS:
                    status = "In Progress";
                    break;
                case Constants.CandidateSessionStatus.NOT_SCHEDULED:
                    status = "Not Scheduled";
                    break;
                case Constants.CandidateSessionStatus.QUIT:
                    status = "Quit";
                    break;
                case Constants.CandidateSessionStatus.SCHEDULED:
                    status = "Scheduled";
                    break;
                case Constants.CandidateSessionStatus.CANCELLED:
                    status = "Cancelled";
                    break;
                case Constants.CandidateSessionStatus.COMPLETED:
                    status = "Completed";
                    break;
                case Constants.CandidateSessionStatus.ENDED:
                    status = "Ended";
                    break;
                case Constants.CandidateSessionStatus.PAUSED:
                    status = "Paused";
                    break;
                default:
                    status = "Not Scheduled";
                    break;
            }
            return status;
        }

        /// <summary>
        /// Return the proper status message according to the DB session values.
        /// </summary>
        /// <param name="pStatus">
        /// A <see cref="string"/> that contains the test status
        /// </param>
        /// <returns>
        /// It will return the test status.
        /// </returns>
        protected bool ShowAssessorLink(int assCount)
        {
            if (assCount > 0)
                return true;
            else
                return false;
        }
       
        /// <summary>
        /// This method clears the textboxes in Schedule/Reschedule popup.
        /// </summary>
        protected void ClearSchedulePopup()
        {
            ScheduleCandidate_Popup_candidateNameTextBox.Text = "";
            ScheduleCandidate_Popup_emailTextBox.Text = "";
            ScheduleCandidate_scheduleDatePopupTextBox.Text = "";
            ScheduleCandidate_scheduleErrorMsgLabel.Text = "";
        }

        /// <summary>
        /// This method clears the test session and candidate session details
        /// </summary>
        protected void ClearSessionValues()
        {
            try
            {
                ScheduleCandidate_testDetailsDiv.Style["display"] = "none";
                ScheduleCandidate_testDetailsGridDiv.Style["display"] = "none";
                ScheduleCandidate_candidateSessionHeader.Style["display"] = "none";

                // Store the scheduled date in view state

                ScheduleCandidate_testNameLabel.Text = "";
                ScheduleCandidate_allowInterviewPauseLabel.Text = "";
                //ScheduleCandidate_noOfAssessorsLabel.Text = "";
                ScheduleCandidate_testIDLabel.Text = "";
                ScheduleCandidate_emailLabel.Text = "";
                ScheduleCandidate_testAuthorLabel.Text = "";
                ScheduleCandidate_testDescLiteral.Text = "";
                ScheduleCandidate_expiryDateLabel.Text = "";
                ScheduleCandidate_positionProfileLabel.Text = "";
                ScheduleCandidate_instructionsLiteral.Text = "";

                Cache["SEARCH_DATATABLE"] = "";
                ViewState["TEST_SESSION_ID"] = "";
                ViewState["CANDIDATE_SESSION_IDS"] = "";
                ScheduleCandidate_candidateSessionGridView.DataSource = null;
                ScheduleCandidate_candidateSessionGridView.DataBind();
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Protected Methods

        #region Sorting Related Methods                                        

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void ScheduleCandidate_candidateSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                LoadInterviewAndCandidateSessions();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ScheduleCandidate_candidateSessionGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                            (ScheduleCandidate_candidateSessionGridView,
                            (string)ViewState["SORT_FIELD"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            (SortType)ViewState["SORT_ORDER"]);
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Sorting Related Methods

        #region Private Methods                                                

        /// <summary>
        /// This method gets the all the subject ids related to this interview test
        /// </summary>
        /// <param name="interviewTestKey"></param>
        private void LoadSubjectIds(string interviewTestKey)
        {
            string subjectIds = new InterviewSessionBLManager().GetInterviewTestSubjectIdsByKey(interviewTestKey);
            Session["SESSION_SUBJECTIDS"] = subjectIds;
        }
        /// <summary>
        /// 
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value) &&
                ScheduleCandidate_isMaximizedHiddenField.Value == "Y")
            {
                ScheduleCandidate_testSessionDetailsDiv.Style["display"] = "none";
                ScheduleCandidate_searchResultsUpSpan.Style["display"] = "block";
                ScheduleCandidate_searchResultsDownSpan.Style["display"] = "none";
                ScheduleCandidate_candidateSessionGridViewDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                ScheduleCandidate_testSessionDetailsDiv.Style["display"] = "block";
                ScheduleCandidate_searchResultsUpSpan.Style["display"] = "none";
                ScheduleCandidate_searchResultsDownSpan.Style["display"] = "block";
                ScheduleCandidate_candidateSessionGridViewDIV.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value))
                    ((TestScheduleSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]).IsMaximized =
                        ScheduleCandidate_isMaximizedHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadInterviewAndCandidateSessions()
        {
            try
            {
                TestScheduleSearchCriteria scheduleCandidateSearchCriteria = new TestScheduleSearchCriteria();

                // Check if the search criteria fields are empty.
                // If this is empty, pass null. Or else, pass its value.

                //Clear session values
                Session["ASSESSOR"] = null;
                ScheduleCandidate_recommendAssessorLinkButton.Attributes.Add("onclick",
                    "return LoadInterviewRecommendAssessor('SESSIONKEY','" + ScheduleCandidate_testSessionIdTextBox.Text.Trim() + "',1,null,null);");

                scheduleCandidateSearchCriteria.TestSessionID =
                    ScheduleCandidate_testSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : ScheduleCandidate_testSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.CandidateSessionIDs =
                    ScheduleCandidate_candidateSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : ScheduleCandidate_candidateSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.IsMaximized =
                    ScheduleCandidate_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                scheduleCandidateSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                scheduleCandidateSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

                // Keep the search criteria in Session if the page is launched from 
                // somewhere else.
                Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] = scheduleCandidateSearchCriteria;

                InterviewSessionDetail interviewSession = new InterviewSchedulerBLManager().
                    GetInteviewSessionDetail(scheduleCandidateSearchCriteria.TestSessionID,
                            scheduleCandidateSearchCriteria.CandidateSessionIDs,scheduleCandidateSearchCriteria.SortExpression, 
                        scheduleCandidateSearchCriteria.SortDirection);

                if (interviewSession != null)
                {
                    UserDetail userDetail = new UserDetail();
                    userDetail = (UserDetail)Session["USER_DETAIL"];

                    //To get list of subjectids
                    Session["SESSION_SUBJECTIDS"] = null;
                    Session["SESSION_POSITION_PROFILE_ID"] = null;
                    LoadSubjectIds(interviewSession.InterviewTestID);

                    ScheduleCandidate_testDetailsDiv.Style["display"] = "block";
                    ScheduleCandidate_testDetailsGridDiv.Style["display"] = "block";
                    ScheduleCandidate_candidateSessionHeader.Style["display"] = "block";

                    // Store the scheduled date in view state
                    ScheduleCandidate_testNameLabel.Text = interviewSession.InterviewTestName;
                    ScheduleCandidate_allowInterviewPauseLabel.Text = 
                        interviewSession.AllowPauseInterview.ToString() == "Y" ? "Yes" : "No";
                    //ScheduleCandidate_noOfAssessorsLabel.Text = "5";
                    ScheduleCandidate_testIDLabel.Text = interviewSession.InterviewTestID;
                    ScheduleCandidate_emailLabel.Text = interviewSession.InterviewSessionAuthorEmail;
                    ScheduleCandidate_testAuthorLabel.Text = interviewSession.InteriewSessionAuthor;
                    ScheduleCandidate_testDescLiteral.Text = interviewSession.InterviewSessionDesc == null ? interviewSession.InterviewSessionDesc : 
                        interviewSession.InterviewSessionDesc.ToString().Replace(Environment.NewLine, "<br />");
                    
                    ScheduleCandidate_expiryDateLabel.Text = GetDateFormat(interviewSession.ExpiryDate);
                    ScheduleCandidate_positionProfileLabel.Text = interviewSession.PositionProfileName;
                    ScheduleCandidate_instructionsLiteral.Text = interviewSession.Instructions == null ? interviewSession.Instructions :
                        interviewSession.Instructions.ToString().Replace(Environment.NewLine, "<br />");
                    
                    Session["SESSION_POSITION_PROFILE_ID"] = interviewSession.PositionProfileID;

                    Cache["SEARCH_DATATABLE"] = interviewSession;
                    ViewState["TEST_SESSION_ID"] = ScheduleCandidate_testSessionIdTextBox.Text.Trim();
                    ViewState["CANDIDATE_SESSION_IDS"] = ScheduleCandidate_candidateSessionIdTextBox.Text.Trim();
                    ScheduleCandidate_candidateSessionGridView.DataSource = interviewSession.CandidateInterviewSessions;
                    ScheduleCandidate_candidateSessionGridView.DataBind();

                    // Remove 'onclick' handler for 'load position profile candidate' icon.
                    if (ScheduleCandidate_positionProfileCandidateImageButton.Attributes["onclick"] != null)
                        ScheduleCandidate_positionProfileCandidateImageButton.Attributes.Remove("onclick");

                    // Assign handler for 'load position profile candidate' icon.
                    ScheduleCandidate_positionProfileCandidateImageButton.Attributes.Add("onclick", "return LoadPositionProfileCandidates('"
                       + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                       + ScheduleCandidate_Popup_emailTextBox.ClientID + "','"
                       + ScheduleCandidate_candidateIdHiddenField.ClientID + "','"
                       + interviewSession.PositionProfileID + "')");
                }
                else
                {
                    base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                   ScheduleCandidate_topErrorMessageLabel, "Invalid Interview session or Candidate Interview Session IDs");

                }

            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
                ClearSessionValues();
            }
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        private void FillSearchCriteria(TestScheduleSearchCriteria scheduleSearchCriteria)
        {
            if (scheduleSearchCriteria.TestSessionID != null)
                ScheduleCandidate_testSessionIdTextBox.Text = scheduleSearchCriteria.TestSessionID;

            if (scheduleSearchCriteria.EMail != null)
                ScheduleCandidate_candidateSessionIdTextBox.Text = scheduleSearchCriteria.CandidateSessionIDs;

            ScheduleCandidate_isMaximizedHiddenField.Value =
                    scheduleSearchCriteria.IsMaximized == true ? "Y" : "N";

            ViewState["SORT_ORDER"] = scheduleSearchCriteria.SortDirection;
            ViewState["SORT_FIELD"] = scheduleSearchCriteria.SortExpression;

            // Apply search
            LoadInterviewAndCandidateSessions();
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// This method checks the user input. If both testSession and CandidateSession IDs 
        /// are not given, then return false.
        /// </summary>
        /// <returns>
        /// </returns>
        protected override bool IsValidData()
        {
            if (ScheduleCandidate_testSessionIdTextBox.Text != "" ||
                ScheduleCandidate_candidateSessionIdTextBox.Text != "")
            {
                // This method passes the list of candidateSession ids separated by comma and
                // it returns the candidate session keys which are not existing in test session.
                //List<string> notMatchedCandSessionKeys =
                //    new TestSchedulerBLManager().ValidateTestSessionInput
                //    (ScheduleCandidate_testSessionIdTextBox.Text.Trim(),
                //    ScheduleCandidate_candidateSessionIdTextBox.Text.Trim());

                //if (notMatchedCandSessionKeys != null && notMatchedCandSessionKeys.Count > 0)
                //{
                //    base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                //        ScheduleCandidate_topErrorMessageLabel, "No matching records found");
                //    return false;
                //}
                //else
                    return true;
            }
            base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                ScheduleCandidate_topErrorMessageLabel,
                Resources.HCMResource.InterviewScheduleCandidate_EmptySessionIds);
            return false;
        }

        /// <summary>
        /// This method loads the test session and scheduled candidate details.
        /// </summary>
        protected override void LoadValues()
        {
            // Assign the client side onclick events to header
            ScheduleCandidate_searchTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                ScheduleCandidate_candidateSessionGridViewDIV.ClientID + "','" +
                ScheduleCandidate_testSessionDetailsDiv.ClientID + "','" +
                ScheduleCandidate_searchResultsUpSpan.ClientID + "','" +
                ScheduleCandidate_searchResultsDownSpan.ClientID + "','" +
                ScheduleCandidate_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            ScheduleCandidate_candidateNameImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                + ScheduleCandidate_Popup_emailTextBox.ClientID + "','"
                + ScheduleCandidate_candidateIdHiddenField.ClientID
                + "')");

           
            // Check if page is navigated from talentscout and position profile is found
            if (Request.QueryString["parentpage"] != null &&
                Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                Request.QueryString["positionprofileid"] != null)
            {
                // Get position profile ID.
                int positionProfileID = 0;
                if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                {
                    ScheduleCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadInterviewTestSessionWithPositionProfile('"
                    + ScheduleCandidate_testSessionIdTextBox.ClientID + "','" + 
                    positionProfileID + "')");
                }
                else
                {
                    ScheduleCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadInterviewTestSession('"
                    + ScheduleCandidate_testSessionIdTextBox.ClientID + "')");
                }
            }
            else
            {
                ScheduleCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadInterviewTestSession('"
                    + ScheduleCandidate_testSessionIdTextBox.ClientID + "')");
            }

            ScheduleCandidate_saveButton.Attributes.Add("onclick", "return VaildateSchedulePopup('"
                + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                + ScheduleCandidate_scheduleDatePopupTextBox.ClientID
                + "','" + ScheduleCandidate_scheduleErrorMsgLabel.ClientID + "')");

            ScheduleCandidate_testSessionIdTextBox.Attributes.Add("onkeydown", "TestSessionKeyDown(event,'"
                + ScheduleCandidate_showButton.ClientID + "')");

            // Assign default sort field and order keys.
            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;

            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "CandidateSessionID";
        }
        #endregion Protected Overridden Methods
    }
}