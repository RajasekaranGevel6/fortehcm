﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InterviewScheduleCandidate.aspx.cs"
    Inherits="Forte.HCM.UI.InterviewScheduler.InterviewScheduleCandidate" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ Register Src="~/CommonControls/InterviewScheduleControl.ascx" TagName="InterviewScheduleControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="ScheduleCandidate_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <script language="javascript" type="text/javascript">
        // Validate the schedule popup. If candidate name or expiry date is empty, display the message to user.
        function VaildateSchedulePopup(nameTxt, expiryDateTxt, errorMsgLbl) {
            if (document.getElementById(nameTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Candidate name cannot be empty";
                return false;
            }
            else if (document.getElementById(expiryDateTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Expiry date cannot be empty";
                return false;
            }
        }

        // Pressing tab key from 'Test Session Id' textbox should 
        // load the test session and schedule candidate details.
        function TestSessionKeyDown(event, showButtonCtrl) {
            if (event.which || event.keyCode) {
                if ((event.which == 9) || (event.keyCode == 9)) {
                    var ctrlId = showButtonCtrl.toString();
                    document.getElementById(showButtonCtrl).click();
                    return;
                }
            }
        }

        // Function that shows the create candidate popup
        function ShowCandidatePopup(nameCtrl, emailCtrl, idCtrl) {
            var height = 540;
            var width = 980;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/AddCandidate.aspx?namectrl=" + nameCtrl;
            queryStringValue += "&emailctrl=" + emailCtrl;
            queryStringValue += "&idctrl=" + idCtrl;
            //window.open(queryStringValue, window.self, sModalFeature);
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

        function LoadInterviewRecommendAssessor(flag, key, attemptId, from, ctrlId) {
            var height = 590;
            var width = 800;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
            var queryStringValue = "../popup/ShowRecommendAssessor.aspx?source=lookup&key=" + key + "&flag=" + flag + "&attemptId=" + attemptId + "&from=" + from + "&ctrlId=" + ctrlId;
            //window.open(queryStringValue, window.self, sModalFeature);
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

        function RequestExternalAssessor(attemptId, candidatSessionId, ctrlId) {
            var height = 450;
            var width = 600;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
            var queryStringValue = "../popup/RequestExternalAssessor.aspx?attemptid=" + attemptId + "&candidatesessionid=" + candidatSessionId + "&ctrlid=" + ctrlId;
            //window.open(queryStringValue, window.self, sModalFeature);
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }
        
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top">
                <asp:UpdatePanel ID="ScheduleCandidate_mainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="3" cellpadding="0">
                            <tr>
                                <td class="header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="72%" class="header_text_bold">
                                                <asp:Literal ID="ScheduleCandidate_headerLiteral" runat="server" Text="Schedule Candidate"></asp:Literal>
                                            </td>
                                            <td width="28%" align="right">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                    <tr>
                                                        <td width="81%" align="right">
                                                            <asp:LinkButton ID="ScheduleCandidate_topResetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="ScheduleCandidate_resetLinkButton_Click" />
                                                        </td>
                                                        <td width="4%" align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:LinkButton ID="ScheduleCandidate_topCancelButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ScheduleCandidate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ScheduleCandidate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tab_body_bg">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <div id="ScheduleCandidate_testSessionDetailsDiv" runat="server" style="display: block">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="ScheduleCandidate_testSessionIdHeadLabel" runat="server" Text="Interview Session ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ScheduleCandidate_testSessionIdTextBox" runat="server"></asp:TextBox></div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ScheduleCandidate_testSessionImageButton" runat="server" ImageAlign="Middle"
                                                                                    SkinID="sknbtnSearchicon" ToolTip="Click here to search session ID" Visible="false" /></div>
                                                                        </td>
                                                                        <td style="width: 14%">
                                                                            <asp:Label ID="ScheduleCandidate_candidateSessionIdHeadLabel" runat="server" Text="Candidate Session ID(s)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ScheduleCandidate_candidateSessionIdTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ScheduleCandidate_candidateSessionHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter one or more candidate session ID's separated by commas"
                                                                                    Height="16px" /></div>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:Button ID="ScheduleCandidate_showButton" runat="server" Text="Show Details"
                                                                                OnClick="ScheduleCandidate_showButton_Click" SkinID="sknButtonId" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div id="ScheduleCandidate_testDetailsDiv" runat="server" style="display: none">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class=" header_bg">
                                                                                <table cellpadding="0" cellspacing="0" width="98%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 93%" align="left">
                                                                                            <asp:Literal ID="ScheduleCandidate_testDetailsLiteral" runat="server" Text="Interview Session Details"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="grid_body_bg">
                                                                                <table cellpadding="0" cellspacing="5" width="100%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="ScheduleCandidate_testIDHeadLabel" runat="server" Text="Interview ID"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="ScheduleCandidate_testIDLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 9%">
                                                                                            <asp:Label ID="ScheduleCandidate_testNameHeadLabel" runat="server" Text="Interview Name"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 21%">
                                                                                            <asp:Label ID="ScheduleCandidate_testNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="ScheduleCandidate_testAuthorHeadLabel" runat="server" Text="Interview Author"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="ScheduleCandidate_testAuthorLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 9%">
                                                                                            <asp:Label ID="ScheduleCandidate_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 17%">
                                                                                            <asp:Label ID="ScheduleCandidate_emailLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_expiryDateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_allowInterviewPauseDateHeadLabel" runat="server"
                                                                                                Text="Allow Interview To Be Paused" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_allowInterviewPauseLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <%-- <td>
                                                                                            <asp:Label ID="ScheduleCandidate_noOfAssessorsHeadLabel" runat="server"
                                                                                                Text="Assessors Assigned" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_noOfAssessorsLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>--%>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_positionProfileHeadLabel" runat="server" Text="Position Profile"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="7">
                                                                                            <asp:Label ID="ScheduleCandidate_positionProfileLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4">
                                                                                            <asp:Label ID="ScheduleCandidate_testDescHeadLabel" runat="server" Text="Interview Session Description"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="4">
                                                                                            <asp:Label ID="ScheduleCandidate_instructionsLabel" runat="server" Text="Instructions"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 112px; overflow: auto; word-wrap: break-word;
                                                                                                white-space: normal;">
                                                                                                <asp:Literal ID="ScheduleCandidate_testDescLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text=""></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 112px; overflow: auto; word-wrap: break-word;
                                                                                                white-space: normal;">
                                                                                                <asp:Literal ID="ScheduleCandidate_instructionsLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text=""></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="ScheduleCandidate_testDetailsGridDiv" runat="server" style="display: none">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr id="ScheduleCandidate_searchTestResultsTR" runat="server">
                                                <td class="header_bg">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td id="ScheduleCandidate_candidateSessionHeader" runat="server">
                                                                <asp:Literal ID="ScheduleCandidate_candidateSessionDetailsLiteral" runat="server"
                                                                    Text="Candidate Session Details"></asp:Literal>
                                                            </td>
                                                            <td align="right" id="ScheduleCandidate_searchResultsTR" runat="server">
                                                                <span id="ScheduleCandidate_searchResultsUpSpan" style="display: none;" runat="server">
                                                                    <asp:Image ID="ScheduleCandidate_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                </span><span id="ScheduleCandidate_searchResultsDownSpan" style="display: block;"
                                                                    runat="server">
                                                                    <asp:Image ID="ScheduleCandidate_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <div style="display:none">
                                                        <asp:Button ID="ScheduleCandidate_refreshRequestExternalButton" OnClick="ScheduleCandidate_refreshRequestExternalButton_Click" runat="server" />
                                                    </div>
                                                    <div id="ScheduleCandidate_candidateSessionGridViewDIV" runat="server" style="height: 200px;
                                                        overflow: auto;">
                                                        <asp:UpdatePanel ID="ScheduleCandidate_candidateSessionGridViewUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="ScheduleCandidate_candidateSessionGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="false" OnRowCommand="ScheduleCandidate_candidateSessionGridView_RowCommand"
                                                                    OnRowCreated="ScheduleCandidate_candidateSessionGridView_RowCreated" OnRowDataBound="ScheduleCandidate_candidateSessionGridView_RowDataBound"
                                                                    OnSorting="ScheduleCandidate_candidateSessionGridView_Sorting" SkinID="sknWrapHeaderGrid">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="12%">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="ScheduleCandidate_viewResultHyperLink" runat="server" Target="_blank"
                                                                                    ToolTip="View Interview Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif">
                                                                                </asp:HyperLink>
                                                                                <asp:HyperLink ID="ScheduleCandidate_viewInterviewRatingHyperLink" runat="server"
                                                                                    Target="_blank" ToolTip="Candidate Rating Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                </asp:HyperLink>
                                                                                <asp:ImageButton ID="ScheduleCandidate_scheduleImageButton" runat="server" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    CommandName="schedule" SkinID="sknScheduleImageButton" ToolTip="Schedule Candidate"
                                                                                    Visible="true" />
                                                                                <asp:ImageButton ID="ScheduleCandidate_rescheduleImageButton" runat="server" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    CommandName="reschedule" SkinID="sknRescheduleImageButton" ToolTip="Reschedule Candidate"
                                                                                    Visible="true" />
                                                                                <asp:ImageButton ID="ScheduleCandidate_viewShedulerImageButton" runat="server" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    CommandName="viewSchedule" SkinID="sknViewScheduleImageButton" ToolTip="View Candidate Schedule" />
                                                                                <asp:ImageButton ID="ScheduleCandidate_unscheduleImageButton" runat="server" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    CommandName="unschedule" SkinID="sknUnscheduleImageButton" ToolTip="Unschedule Candidate" />
                                                                                <asp:ImageButton ID="ScheduleCandidate_retakeRequestImageButton" runat="server" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    CommandName="retakeTest" SkinID="sknScheduleImageButton" ToolTip="Schedule Retake Request"
                                                                                    Visible="true" />
                                                                                <asp:ImageButton ID="ScheduleCandidate_cancelReasonDisplayImageButton" runat="server"
                                                                                    CommandArgument='<%# Eval("CandidateTestSessionID") %>' CommandName="viewCancelReason"
                                                                                    SkinID="sknCancelReasonImageButton" ToolTip="View Cancel Reason" Visible="true" />
                                                                                <asp:ImageButton ID="ScheduleCandidate_viewAssessorImageButton" runat="server" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    CommandName="ShowAssessor" SkinID="sknViewAssessorImageButton" Visible='<%# GetStatus(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).Status)!=null &&  GetStatus(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).Status)!="Not Scheduled"%>'
                                                                                    ToolTip="View/Change Assessors" />
                                                                                <asp:ImageButton ID="ScheduleCandidate_requestExternalAssessorImageButton" runat="server"
                                                                                    CommandName="RequestExternalAssessor" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    ToolTip="Request External Assessor" SkinID="sknMailImageButton"/>
                                                                                <asp:HiddenField ID="ScheduleCandidate_candidateSessionIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("CandidateTestSessionID") %>' />
                                                                                <asp:HiddenField ID="ScheduleCandidate_attemptIDHiddenField" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                                <asp:HiddenField ID="ScheduleCandidate_requestRetakeHiddenField" runat="server" Value='<%# Eval("RetakeRequest") %>' />
                                                                                <asp:HiddenField ID="ScheduleCandidate_modifiedDateHiddenField" runat="server" Value='<%# Eval("ModifiedDate") %>' />
                                                                                <asp:HiddenField ID="ScheduleCandidate_Grid_ExpiryDateHiddenField" runat="server"
                                                                                    Value="<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).ScheduledDate) %>" />
                                                                                <asp:HiddenField ID="ScheduleCandidate_Grid_candidateEmailHiddenField" runat="server"
                                                                                    Value='<%# Eval("Email") %>' />
                                                                                <asp:HiddenField ID="ScheduleCandidate_Grid_candidateNameHiddenField" runat="server"
                                                                                    Value='<%# Eval("CandidateFirstName") %>' />
                                                                                <asp:HiddenField ID="ScheduleCandidate_Grid_candidateIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("CandidateID") %>' />
                                                                                <asp:HiddenField ID="ScheduleCandidate_Grid_emailReminderHiddenField" runat="server"
                                                                                    Value='<%# Eval("EmailRemainder") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-CssClass="td_padding_right_20" HeaderText="Candidate Session ID"
                                                                            ItemStyle-Width="12%" SortExpression="CandidateSessionId">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="ScheduleCandidate_candidateSessionLabel" runat="server" Text='<%# Eval("CandidateTestSessionID") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Candidate Name" ItemStyle-Width="22%" SortExpression="CandidateName">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="ScheduleCandidate_candidateHyperLink" runat="server" Text='<%# Eval("CandidateFirstName") %>'
                                                                                    ToolTip='<%# Eval("CandidateFullName") %>' Target="_blank" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Email ID" SortExpression="EmailId">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="ScheduleCandidate_emailLabel" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Status" SortExpression="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="ScheduleCandidate_statusLabel" runat="server" Text="<%# GetStatus(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).Status) %>"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Expiry Date" SortExpression="ScheduledDate DESC">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="ScheduleCandidate_scheduledDateLabel" runat="server" Text="<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).ScheduledDate) %>"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="No of Assessors" SortExpression="Noofassessor DESC">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="ScheduleCandidate_noOfAssessorsLinkButtion" runat="server" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                    CommandName="ShowAssessor" Text='<%# Eval("CandidateAssessorCount") %>' ToolTip="Click here to view/change assessors">
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Completed Date" SortExpression="DateCompleted DESC">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="ScheduleCandidate_completedDateLabel" runat="server" Text="<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).DateCompleted) %>"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ScheduleCandidate_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ScheduleCandidate_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </td>
                                <tr>
                                    <td align="right" class="header_bg">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="header_text_bold" width="72%">
                                                </td>
                                                <td align="right" width="28%">
                                                    <table border="0" cellpadding="0" cellspacing="2" width="100%">
                                                        <tr>
                                                            <td align="right" width="81%">
                                                                <asp:LinkButton ID="ScheduleCandidate_bottonResetLinkButton" runat="server" OnClick="ScheduleCandidate_resetLinkButton_Click"
                                                                    SkinID="sknActionLinkButton" Text="Reset" />
                                                            </td>
                                                            <td align="center" class="link_button" width="4%">
                                                                |
                                                            </td>
                                                            <td align="left" width="15%">
                                                                <asp:LinkButton ID="ScheduleCandidate_bottonCancelLinkButton" runat="server" OnClick="ParentPageRedirect"
                                                                    SkinID="sknActionLinkButton" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="ScheduleCandidate_schedulePanel" runat="server" CssClass="popupcontrol_scheduler"
                                            Style="display: none">
                                            <div style="display: none;">
                                                <asp:Button ID="ScheduleCandidate_hiddenButton" runat="server" Text="Hidden" />
                                            </div>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="popup_td_padding_10">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" class="popup_header_text" style="width: 50%" valign="middle">
                                                                    <asp:Literal ID="ScheduleCandidate_questionResultLiteral" runat="server" Text="Schedule"></asp:Literal>
                                                                </td>
                                                                <td style="width: 50%" valign="top">
                                                                    <table align="right" border="0" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:ImageButton ID="ScheduleCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popup_td_padding_10">
                                                        <table border="0" cellpadding="0" cellspacing="0" class="popupcontrol_question_inner_bg"
                                                            width="100%">
                                                            <tr>
                                                                <td align="left" class="popup_td_padding_10">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="5" class="tab_body_bg"
                                                                        width="100%">
                                                                        <tr>
                                                                            <td class="msg_align" colspan="4">
                                                                                <asp:Label ID="ScheduleCandidate_scheduleErrorMsgLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="ScheduleCandidate_candidateNamePopHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                    Text="Candidate Name"></asp:Label>
                                                                                <span class="mandatory">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="ScheduleCandidate_Popup_candidateNameTextBox" runat="server" ReadOnly="true"
                                                                                    Text=""></asp:TextBox>
                                                                                &nbsp;<asp:ImageButton ID="ScheduleCandidate_candidateNameImageButton" runat="server"
                                                                                    ImageAlign="Middle" SkinID="sknbtnSearchicon" ToolTip="Click here to search for candidates" />
                                                                                &nbsp;<asp:ImageButton ID="ScheduleCandidate_positionProfileCandidateImageButton"
                                                                                    runat="server" ImageAlign="Middle" SkinID="sknBtnSearchPositinProfileCandidateIcon"
                                                                                    ToolTip="Click here to search for candidates associated with the position profile" />
                                                                                &nbsp;
                                                                                <asp:Button ID="ScheduleCandidate_Popup_createCandidateButton" runat="server" SkinID="sknButtonId"
                                                                                    Text="New" ToolTip="Click here to create a new candidate" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="ScheduleCandidate_Popup_emailHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                    Text="Email"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="ScheduleCandidate_Popup_emailTextBox" runat="server" ReadOnly="true"
                                                                                    Text=""></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="ScheduleCandidate_scheduleDatePopLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                    Text="Interview Expiry Date"></asp:Label>
                                                                                <span class="mandatory">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="97.5%">
                                                                                    <tr>
                                                                                        <td style="width: 40%">
                                                                                            <asp:TextBox ID="ScheduleCandidate_scheduleDatePopupTextBox" runat="server" AutoCompleteType="None"
                                                                                                MaxLength="10" Text=""></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="ScheduleCandidate_Popup_calendarImageButton" runat="server"
                                                                                                ImageAlign="Middle" SkinID="sknCalendarImageButton" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <ajaxToolKit:MaskedEditExtender ID="ScheduleCandidate_MaskedEditExtender" runat="server"
                                                                                    AcceptNegative="Left" DisplayMoney="Left" ErrorTooltipEnabled="True" Mask="99/99/9999"
                                                                                    MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                    OnInvalidCssClass="MaskedEditError" TargetControlID="ScheduleCandidate_scheduleDatePopupTextBox" />
                                                                                <ajaxToolKit:MaskedEditValidator ID="ScheduleCandidate_MaskedEditValidator" runat="server"
                                                                                    ControlExtender="ScheduleCandidate_MaskedEditExtender" ControlToValidate="ScheduleCandidate_scheduleDatePopupTextBox"
                                                                                    Display="None" EmptyValueBlurredText="*" EmptyValueMessage="Expiry Date is required"
                                                                                    InvalidValueBlurredMessage="*" InvalidValueMessage="Expiry Date is invalid" TooltipMessage="Input a date"
                                                                                    ValidationGroup="MKE" />
                                                                                <ajaxToolKit:CalendarExtender ID="ScheduleCandidate_customCalendarExtender" runat="server"
                                                                                    CssClass="MyCalendar" Format="MM/dd/yyyy" PopupButtonID="ScheduleCandidate_Popup_calendarImageButton"
                                                                                    PopupPosition="BottomLeft" TargetControlID="ScheduleCandidate_scheduleDatePopupTextBox" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="ScheduleCandidate_sessionExpiryDateHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                    Text="Session Expiry Date"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="ScheduleCandidate_Popup_sessionExpiryDateValueLabel" runat="server"
                                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:CheckBox ID="ScheduleCandidate_emailRemainderCheckBox" runat="server" Text="Send reminder to candidate every 24 hours"
                                                                                    TextAlign="right" SkinID="sknMyRecordCheckBox" />
                                                                            </td>
                                                                            <td colspan="2" align="right">
                                                                                <asp:LinkButton ID="ScheduleCandidate_recommendAssessorLinkButton" runat="server"
                                                                                    SkinID="sknAddLinkButton" Text="Show Recommended Assessor" ToolTip="Recommend Assessor">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popup_td_padding_5">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                                    <asp:HiddenField ID="ScheduleCandidate_candidateIdHiddenField" runat="server" />
                                                                    <asp:Button ID="ScheduleCandidate_saveButton" runat="server" OnClick="ScheduleCandidate_saveButton_Click"
                                                                        SkinID="sknButtonId" Text="Save" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="ScheduleCandidate_bottomCloseLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                                                        Text="Cancel" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_scheduleModalPopupExtender"
                                            runat="server" BackgroundCssClass="modalBackground" PopupControlID="ScheduleCandidate_schedulePanel"
                                            TargetControlID="ScheduleCandidate_hiddenButton">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="ScheduleCandidate_hiddenViewSchedulePanel" runat="server" CssClass="popupcontrol_question_detail"
                                            Style="display: none">
                                            <div style="display: none;">
                                                <asp:Button ID="ScheduleCandidate_hiddenViewScheduleButton" runat="server" Text="Hidden" />
                                            </div>
                                            <uc1:InterviewScheduleControl ID="ScheduleCandidate_viewTestSchedule" runat="server" />
                                        </asp:Panel>
                                        <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_viewScheduleModalPopupExtender"
                                            runat="server" BackgroundCssClass="modalBackground" PopupControlID="ScheduleCandidate_hiddenViewSchedulePanel"
                                            TargetControlID="ScheduleCandidate_hiddenViewScheduleButton">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="ScheduleCandidate_unscheduleCandidatePanel" runat="server" CssClass="popupcontrol_confirm_remove"
                                            Style="display: none">
                                            <div style="display: none;">
                                                <asp:Button ID="ScheduleCandidate_hiddenUnscheduleButton" runat="server" Text="Hidden" />
                                            </div>
                                            <uc2:ConfirmMsgControl ID="ScheduleCandidate_inactiveConfirmMsgControl" runat="server"
                                                Message="Are you sure do you want to cancel the scheduled interview session?"
                                                OnOkClick="ScheduleCandidate_OkClick" Title="Unschedule" Type="YesNo" />
                                        </asp:Panel>
                                        <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_unscheduleCandidateModalPopupExtender"
                                            runat="server" BackgroundCssClass="modalBackground" PopupControlID="ScheduleCandidate_unscheduleCandidatePanel"
                                            TargetControlID="ScheduleCandidate_hiddenUnscheduleButton">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="ScheduleCandidate_cancelTestPanel" runat="server" CssClass="popupcontrol_cancel_session"
                                            Style="display: none">
                                            <div style="display: none">
                                                <asp:Button ID="ScheduleCandidate_cancelReasonHiddenButton" runat="server" Text="Hidden" />
                                            </div>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="popup_td_padding_10">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" class="popup_header_text" style="width: 75%" valign="middle">
                                                                    <asp:Label ID="ScheduleCandidate_cancelReasonLiteral" runat="server" Text="Cancelled Interview Session Reason"></asp:Label>
                                                                </td>
                                                                <td align="right" style="width: 25%">
                                                                    <asp:ImageButton ID="ScheduleCandidate_cancelReasonCancelImageButton" runat="server"
                                                                        SkinID="sknCloseImageButton" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popup_td_padding_10">
                                                        <table border="0" cellpadding="0" cellspacing="0" class="popupcontrol_question_inner_bg"
                                                            width="100%">
                                                            <tr>
                                                                <td class="popup_td_padding_10">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <div id="ScheduleCandidate_cancelTestReasonDiv" class="label_field_text" runat="server"
                                                                                    style="height: 70px; width: 400px; overflow: auto;">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popup_td_padding_5">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:LinkButton ID="ScheduleCandidate_cancelReasonCancellationButton" runat="server"
                                                                        SkinID="sknPopupLinkButton" Text="Cancel"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_cancelSessionModalPopupExtender"
                                            runat="server" BackgroundCssClass="modalBackground" CancelControlID="ScheduleCandidate_cancelReasonCancellationButton"
                                            PopupControlID="ScheduleCandidate_cancelTestPanel" TargetControlID="ScheduleCandidate_cancelReasonHiddenButton">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="ScheduleCandidate_RetakeValidationPopupPanel" runat="server" CssClass="popupcontrol_confirm"
                                            Style="display: none; height: 202px;">
                                            <div id="ScheduleCandidate_RetakeValidationDiv" style="display: none">
                                                <asp:Button ID="ScheduleCandidate_RetakeValidation_hiddenButton" runat="server" />
                                            </div>
                                            <uc2:ConfirmMsgControl ID="ScheduleCandidate_RetakeValidation_ConfirmMsgControl"
                                                runat="server" Title="Request to Retake" Type="OkSmall" />
                                        </asp:Panel>
                                        <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_RetakeValidation_ModalPopupExtender"
                                            runat="server" BackgroundCssClass="modalBackground" PopupControlID="ScheduleCandidate_RetakeValidationPopupPanel"
                                            TargetControlID="ScheduleCandidate_RetakeValidation_hiddenButton">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="ScheduleCandidate_isMaximizedHiddenField" runat="server" />
                                        <asp:HiddenField ID="ScheduleCandidate_candidateNameHiddenField" runat="server" />
                                        <asp:HiddenField ID="ScheduleCandidate_reScheduleCandidateIDHiddenField" runat="server" />
                                        <asp:HiddenField ID="ScheduleCandidate_sessionExpiryDateHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
