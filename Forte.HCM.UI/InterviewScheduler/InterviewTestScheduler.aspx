<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="InterviewTestScheduler.aspx.cs" Inherits="Forte.HCM.UI.Scheduler.InterviewTestScheduler" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewScheduleControl.ascx" TagName="InterviewScheduleControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="InterviewTestScheduler_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">

    <script language="javascript" type="text/javascript">

        // Validate the schedule popup. If candidate name or expiry date is empty, 
        // display the message to user.
        function VaildateSchedulePopup(nameTxt, expiryDateTxt, errorMsgLbl) {
            if (document.getElementById(nameTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Candidate name cannot be empty";
                return false;
            }
            else if (document.getElementById(expiryDateTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Expiry date cannot be empty";
                return false;
            }
        }
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="InterviewTestScheduler_headerLiteral" runat="server" Text="Scheduler"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="InterviewTestScheduler_topScheduleCandidateButton" runat="server" Text="Schedule Candidate"
                                            SkinID="sknButtonId" OnClick="InterviewTestScheduler_scheduleCandidateButton_Click" />
                                        &nbsp;<asp:LinkButton ID="InterviewTestScheduler_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="InterviewTestScheduler_resetLinkButton_Click"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="InterviewTestScheduler_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="InterviewTestScheduler_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="InterviewTestScheduler_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="panel_bg">
                            <div id="InterviewTestScheduler_searchCriteriasDiv" runat="server" style="display: block;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                <tr>
                                                    <td style="width: 12%;">
                                                        <asp:Label ID="InterviewTestScheduler_candidateNameHeadLabel" runat="server" Text="Candidate Name"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 18%;">
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:TextBox ID="InterviewTestScheduler_candidateNameTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="InterviewTestScheduler_candidateNameImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the candidate" />
                                                        </div>
                                                    </td>
                                                    <td style="width: 9%;">
                                                        <asp:Label ID="InterviewTestScheduler_emailIdHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 17%;">
                                                        <asp:TextBox ID="InterviewTestScheduler_emailIdTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%;">
                                                        <asp:Label ID="InterviewTestScheduler_interviewSessionAuthorHeadLabel" runat="server" Text="Interview Schedule Creator"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:TextBox ID="InterviewTestScheduler_interviewSessionAuthorTextBox" MaxLength="50" runat="server"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="InterviewTestScheduler_interviewScheduleAuthorImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ToolTip="Click here to select the schedule author" Visible="false" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="InterviewTestScheduler_interviewSessionIDHeadLabel" runat="server" Text="Interview Session ID"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="InterviewTestScheduler_interviewSessionIDTextBox" MaxLength="15" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="InterviewTestScheduler_interviewNameLabel" runat="server" Text="Interview Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="InterviewTestScheduler_interviewNameTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="InterviewTestScheduler_interviewCompletionHeadLabel" runat="server" Text="Interview Completed"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:DropDownList ID="InterviewTestScheduler_statusDropDownList" runat="server" Width="130px">
                                                                <asp:ListItem Text="Both" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Yes" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="1"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="InterviewTestScheduler_statusImageButton" SkinID="sknHelpImageButton"
                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the interview completion status here" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="right">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="InterviewTestScheduler_searchButton" runat="server" SkinID="sknButtonId" Text="Search"
                                                            OnClick="InterviewTestScheduler_searchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="InterviewTestScheduler_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="InterviewTestScheduler_searchResultsLiteral" runat="server" Text="Candidate Session Details"></asp:Literal>
                                        &nbsp;<asp:Label ID="InterviewTestScheduler_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="InterviewTestScheduler_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="InterviewTestScheduler_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="InterviewTestScheduler_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="InterviewTestScheduler_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="InterviewTestScheduler_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align="left">
                                        <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="InterviewTestScheduler_schedulerDiv">
                                            <asp:GridView ID="InterviewTestScheduler_scheduleGridView" runat="server" OnSorting="InterviewTestScheduler_scheduleGridView_Sorting"
                                                OnRowDataBound="InterviewTestScheduler_scheduleGridView_RowDataBound" OnRowCommand="InterviewTestScheduler_scheduleGridView_RowCommand"
                                                OnRowCreated="InterviewTestScheduler_scheduleGridView_RowCreated" SkinID="sknWrapHeaderExtenderGrid">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="InterviewTestScheduler_viewShedulerImageButton" runat="server" SkinID="sknViewScheduleImageButton"
                                                                ToolTip="View Candidate Schedule" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                CommandName="viewSchedule" />
                                                            <asp:HyperLink ID="InterviewTestScheduler_viewResultHyperLink" runat="server" Target="_blank"
                                                                ToolTip="View Interview Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif">
                                                            </asp:HyperLink>
                                                            <asp:ImageButton ID="InterviewTestScheduler_rescheduleImageButton" runat="server" SkinID="sknRescheduleImageButton"
                                                                ToolTip="Reschedule Candidate" CommandName="reschedule" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:ImageButton ID="InterviewTestScheduler_unscheduleImageButton" runat="server" SkinID="sknUnscheduleImageButton"
                                                                ToolTip="Unschedule Candidate" CommandName="unschedule" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:ImageButton ID="InterviewTestScheduler_viewCancelReasonImageButton" runat="server" SkinID="sknCancelReasonImageButton"
                                                                ToolTip="View Cancel Reason" CommandName="viewCancelReason" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_candidateTestSessionIDHiddenField" runat="server"
                                                                Value='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_attemptIDHiddenField" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_Grid_testSessionIDHiddenField" runat="server"
                                                                Value='<%# Eval("TestSessionID") %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_Grid_testIDHiddenField" runat="server" Value='<%# Eval("TestID") %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_Grid_ExpiryDateHiddenField" runat="server" Value='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).ExpiryDate) %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_Grid_sessionExpiryDateHiddenField" runat="server"
                                                                Value='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).SessionExpiryDate) %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Client<br>Request<br>Number" DataField="ClientRequestID"
                                                        SortExpression="CLIENT_REQ_NO DESC" HeaderStyle-Width="150px" HtmlEncode="false"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Interview<br>Session ID" DataField="TestSessionID" SortExpression="TEST_SESS_ID DESC"
                                                        HeaderStyle-Width="150px" ItemStyle-Width="150px" HtmlEncode="false"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Candidate Session&nbsp;ID" DataField="CandidateTestSessionID"
                                                        SortExpression="CAND_SESS_KEY DESC" HeaderStyle-Width="180px" ItemStyle-Width="180px">
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Candidate&nbsp;Name" SortExpression="CANDIDATE_NAME"
                                                        HeaderStyle-Width="160px" ItemStyle-Width="160px">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="InterviewTestScheduler_candidateFullNameHyperLink" runat="server" Text='<%# Eval("CandidateName") %>'
                                                                ToolTip='<%# Eval("CandidateFullName") %>' Target="_blank" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Email&nbsp;ID" DataField="EmailId" SortExpression="CANDIDATE_EMAIL"
                                                        HeaderStyle-Width="200px" ItemStyle-Width="200px"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Created<br>Date" SortExpression="SESSION_CREATED_ON DESC" >
                                                        <ItemTemplate>
                                                            <asp:Label ID="InterviewTestScheduler_createdDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).CreatedDate) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Scheduled<br>By" SortExpression="SCHEDULED_BY" HeaderStyle-Width="120px"
                                                        ItemStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="InterviewTestScheduler_schedulerFullNameLabel" runat="server" Text='<%# Eval("SchedulerFirstName") %>'
                                                                ToolTip='<%# Eval("SchedulerFullName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry&nbsp;Date" SortExpression="EXPIRY_DATE DESC">
                                                        <ItemTemplate>
                                                            <asp:Label ID="InterviewTestScheduler_expiryDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).ExpiryDate) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status" SortExpression="TEST_COMPLETED_STATUS" HeaderStyle-Width="120px"
                                                        ItemStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="InterviewTestScheduler_statusLabel" runat="server" Text='<%# Eval("TestStatus") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Completed Date" SortExpression="TEST_COMPLETED_DATE DESC">
                                                        <ItemTemplate>
                                                            <asp:Label ID="InterviewTestScheduler_completedDateLabel" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).CompletedDate) %>'
                                                                runat="server"></asp:Label>
                                                            <asp:HiddenField ID="InterviewTestScheduler_Grid_candidateNameHiddenField" runat="server"
                                                                Value='<%# Eval("CandidateName") %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_candidateEmailHiddenField" runat="server" Value='<%# Eval("EmailId") %>' />
                                                            <asp:HiddenField ID="InterviewTestScheduler_Grid_candidateIDHiddenField" runat="server" Value='<%# Eval("CandidateID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc3:PageNavigator ID="InterviewTestScheduler_pagingNavigator" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="InterviewTestScheduler_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="InterviewTestScheduler_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="InterviewTestScheduler_bottomScheduleCandidateButton" runat="server" Text="Schedule Candidate"
                                SkinID="sknButtonId" OnClick="InterviewTestScheduler_scheduleCandidateButton_Click" />
                            &nbsp;<asp:LinkButton ID="InterviewTestScheduler_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="InterviewTestScheduler_resetLinkButton_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="InterviewTestScheduler_bottomCancelLinkButton" runat="server"
                                Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                        </td>
                    </tr>
                    <%-- Schedule Candiate Popup TR--%>
                    <tr>
                        <td>
                            <asp:Panel ID="InterviewTestScheduler_schedulePanel" runat="server" Style="display: none"
                                CssClass="popupcontrol_scheduler">
                                <div style="display: none;">
                                    <asp:Button ID="InterviewTestScheduler_hiddenButton" runat="server" Text="Hidden" /></div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="popup_td_padding_10">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                        <asp:Literal ID="InterviewTestScheduler_scheduleCandiateLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width: 50%" valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="InterviewTestScheduler_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_10">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                <tr>
                                                    <td align="left" class="popup_td_padding_10">
                                                        <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                            align="left">
                                                            <tr>
                                                                <td class="msg_align" colspan="4">
                                                                    <asp:Label ID="InterviewTestScheduler_scheduleErrorMsgLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="InterviewTestScheduler_Popup_candidateNameHeadLabel" runat="server" Text="Candidate Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class='mandatory'>*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="InterviewTestScheduler_Popup_candidateNameTextBox" runat="server" ReadOnly="true"
                                                                        Text=""></asp:TextBox>&nbsp;<asp:ImageButton ID="InterviewTestScheduler_Popup_candidateNameImageButton"
                                                                            SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="InterviewTestScheduler_Popup_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="InterviewTestScheduler_Popup_emailTextBox" runat="server" Text="" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="InterviewTestScheduler_Popup_scheduleDateLabel" runat="server" Text="Interview Expiry Date"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class='mandatory'>*</span>
                                                                </td>
                                                                <td>
                                                                    <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="width: 40%">
                                                                                <asp:TextBox ID="InterviewTestScheduler_Popup_scheduleDateTextBox" runat="server" MaxLength="10"
                                                                                    AutoCompleteType="None"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="InterviewTestScheduler_Popup_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                    runat="server" ImageAlign="Middle" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <ajaxToolKit:MaskedEditExtender ID="InterviewTestScheduler_MaskedEditExtender" runat="server"
                                                                        TargetControlID="InterviewTestScheduler_Popup_scheduleDateTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                                        DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                    <ajaxToolKit:MaskedEditValidator ID="InterviewTestScheduler_MaskedEditValidator" runat="server"
                                                                        ControlExtender="InterviewTestScheduler_MaskedEditExtender" ControlToValidate="InterviewTestScheduler_Popup_scheduleDateTextBox"
                                                                        EmptyValueMessage="Expiry Date is required" InvalidValueMessage="Expiry Date is invalid"
                                                                        Display="None" TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                        ValidationGroup="MKE" />
                                                                    <ajaxToolKit:CalendarExtender ID="InterviewTestScheduler_customCalendarExtender" runat="server"
                                                                        TargetControlID="InterviewTestScheduler_Popup_scheduleDateTextBox" CssClass="MyCalendar"
                                                                        Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="InterviewTestScheduler_Popup_calendarImageButton" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="InterviewTestScheduler_Popup_sessionExpiryDateHeadLabel" runat="server" Text="Session Expiry Date"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="InterviewTestScheduler_Popup_sessionExpiryDateValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_5">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left" style="width: 40px;">
                                                        <asp:HiddenField ID="InterviewTestScheduler_Popup_candidateIDHiddenField" runat="server" />
                                                        <asp:Button ID="InterviewTestScheduler_saveButton" runat="server" SkinID="sknButtonId" Text="Save"
                                                            OnClick="InterviewTestScheduler_saveButton_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="InterviewTestScheduler_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                                                            runat="server" Text="Cancel" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="InterviewTestScheduler_scheduleModalPopupExtender" runat="server"
                                PopupControlID="InterviewTestScheduler_schedulePanel" TargetControlID="InterviewTestScheduler_hiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="InterviewTestScheduler_unscheduleCandidatePanel" runat="server" Style="display: none"
                                CssClass="popupcontrol_confirm_remove">
                                <div style="display: none;">
                                    <asp:Button ID="InterviewTestScheduler_hiddenUnscheduleButton" runat="server" Text="Hidden"
                                        SkinID="sknButtonId" />
                                </div>
                                <uc3:ConfirmMsgControl ID="InterviewTestScheduler_inactiveConfirmMsgControl" runat="server"
                                    Title="Unschedule" Type="YesNo" Message="Are you sure you want to cancel the scheduled interview session?"
                                    OnOkClick="InterviewTestScheduler_OkClick" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="InterviewTestScheduler_unschedulePopupExtenter" runat="server"
                                PopupControlID="InterviewTestScheduler_unscheduleCandidatePanel" TargetControlID="InterviewTestScheduler_hiddenUnscheduleButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div style="display: none;">
                    <asp:Button ID="InterviewTestScheduler_hiddenViewScheduleButton" runat="server" Text="Hidden"
                        SkinID="sknButtonId" /></div>
                <asp:Panel ID="InterviewTestScheduler_hiddenViewSchedulePanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_question_detail">
                    <uc1:InterviewScheduleControl ID="InterviewTestScheduler_viewTestSchedule" runat="server" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="InterviewTestScheduler_viewScheduleModalPopupExtender"
                    runat="server" PopupControlID="InterviewTestScheduler_hiddenViewSchedulePanel" TargetControlID="InterviewTestScheduler_hiddenViewScheduleButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="InterviewTestScheduler_inactivepopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_remove">
                    <div id="InterviewTestScheduler_hiddenDIV" style="display: none">
                        <asp:Button ID="InterviewTestScheduler_inactiveHiddenButton" runat="server" />
                    </div>
                    <uc3:ConfirmMsgControl ID="InterviewTestScheduler_inactivePopupExtenderControl" runat="server" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="InterviewTestScheduler_inactivepopupExtender" runat="server"
                    PopupControlID="InterviewTestScheduler_inactivepopupPanel" TargetControlID="InterviewTestScheduler_hiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <%-- View Cancel Reason Popup --%>
                <asp:Panel ID="InterviewTestScheduler_cancelTestPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_cancel_session">
                    <div style="display: none">
                        <asp:Button ID="InterviewTestScheduler_cancelReasonHiddenButton" runat="server" Text="Hidden" />
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="td_height_20">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="90%" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Label ID="InterviewTestScheduler_cancelReasonLiteral" runat="server" Text="Cancelled Interview Session Reason"></asp:Label>
                                        </td>
                                        <td style="width: 25%" align="right">
                                            <asp:ImageButton ID="InterviewTestScheduler_cancelReasonCancelImageButton" runat="server"
                                                SkinID="sknCloseImageButton" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_20">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table width="100%" class="popupcontrol_question_inner_bg" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td  class="popup_td_padding_10">
                                                        <div runat="server" id="InterviewTestScheduler_cancelTestReasonDiv" class="label_field_text" style="height: 70px; width: 400px;
                                                            overflow: auto;">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:LinkButton ID="InterviewTestScheduler_cancelReasonCancellationButton" runat="server"
                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_height_8">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="InterviewTestScheduler_cancelSessionModalPopupExtender"
                    runat="server" PopupControlID="InterviewTestScheduler_cancelTestPanel" TargetControlID="InterviewTestScheduler_cancelReasonHiddenButton"
                    CancelControlID="InterviewTestScheduler_cancelReasonCancellationButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:HiddenField ID="InterviewTestScheduler_testSessionAuthorIdHiddenField" runat="server" />
                <asp:HiddenField ID="InterviewTestScheduler_testSessionAuthorHiddenField" runat="server" />
                <asp:HiddenField ID="InterviewTestScheduler_candidateSessionIDHiddenField" runat="server" />
                <asp:HiddenField ID="InterviewTestScheduler_testSessionIDHiddenField" runat="server" />
                <asp:HiddenField ID="InterviewTestScheduler_isMaximizedHiddenField" runat="server" />
                <asp:HiddenField ID="InterviewTestScheduler_expiryDateHiddenField" runat="server" />
                <asp:HiddenField ID="InterviewTestScheduler_candidateEmailHiddenField" runat="server" />
                <asp:HiddenField ID="InterviewTestScheduler_sessoinExpiryDateHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
