﻿#region Namespace                                                              

using System;

using Resources;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Collections.Generic;

#endregion Namespace

namespace Forte.HCM.UI
{
    public partial class TalentScoutHome : PageBase
    {
        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                TalentScoutHome_tsSilverlight.InitParameters = "LoggedInUser=" + base.userID;
                TalentScoutHome_tsSilverlight.InitParameters += ",LoggedInTenant=" + base.tenantID;
                //TalentScoutHome_tsSilverlight.InitParameters += ",UIUrl=http://localhost/HCM/";
                TalentScoutHome_tsSilverlight.InitParameters += ",UIUrl=" + ConfigurationManager.AppSettings["DEFAULT_URL"];
                TalentScoutHome_tsSilverlight.InitParameters += ",OperationTimeOut=" + ConfigurationManager.AppSettings["OPERATION_TIMEOUT"];
                
                Master.SetPageCaption(HCMResource.TalentScout_PageTitle);

                if (IsPostBack)
                    return;
                    try
                    {
                        int positionProfileID = 0;

                        if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING]))
                            positionProfileID = Convert.ToInt32(Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING]);

                        TalentScoutHome_tsSilverlight.InitParameters += ",PositionProfileId=" + positionProfileID;

                        if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]))
                            TalentScoutHome_tsSilverlight.InitParameters += ",ParentPage=" + Request.QueryString["parentpage"];
                        else
                            TalentScoutHome_tsSilverlight.InitParameters += ",ParentPage=";
                        
                        if( positionProfileID != 0)
                        {
                            PositionProfileDetail positionProfileDetails = new PositionProfileDetail();
                            positionProfileDetails = GetPositionProfileDetails(
                           Convert.ToInt32(Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING]));

                            if ((positionProfileDetails != null) && (!Utility.IsNullOrEmpty(positionProfileDetails)))
                            {
                                if ((!Utility.IsNullOrEmpty(positionProfileDetails.PositionProfileName)))
                                {
                                    TalentScoutHome_tsSilverlight.InitParameters += ",ppname=" + positionProfileDetails.PositionProfileName.Replace(',','~');
                                }
                                if (!Utility.IsNullOrEmpty(positionProfileDetails.Keys))
                                {
                                    TalentScoutHome_tsSilverlight.InitParameters += ",KeyWord=" + positionProfileDetails.Keys.Replace(',', '~');
                                }
                            }
                        }
                        else
                        {
                            TalentScoutHome_tsSilverlight.InitParameters += ",ppname=";
                            TalentScoutHome_tsSilverlight.InitParameters += ",KeyWord=";
                        }
                    }
                    catch { }
               

            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        /// <summary>
        /// A Method that gets the keyword based on the position profile id
        /// </summary>
        /// <param name="PositionProfileId">
        /// A <see cref="System.Int32"/> that holds the position profile id
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the position profile keyword
        /// </returns>
        private PositionProfileDetail GetPositionProfileDetails(int PositionProfileId)
        {
            return new PositionProfileBLManager().GetPositionProfileKeyWord(PositionProfileId);
          //  return ((PositionProfileDetail)new PositionProfileBLManager().GetPositionProfileKeyWord(PositionProfileId)).Keys.Replace(',', '~');
        }
        private string GetPositionProfileKeyWord(int PositionProfileId)
        {
            
            return ((PositionProfileDetail)new PositionProfileBLManager().GetPositionProfileKeyWord(PositionProfileId)).Keys.Replace(',', '~');
        }
        
        /// <summary>
        /// A Method that shows the exeption to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            base.ShowMessage(TalentScoutHome_topErrorMessageLabel, Message);
        }

        /// <summary>
        /// A Method that logs the exception message and 
        /// display's the error message to the user.
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception message
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        #endregion Private Methods

        #region Override Methods                                               

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Override Methods
    }
}