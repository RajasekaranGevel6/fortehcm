﻿<%@ Page Title="Workflow" Language="C#" MasterPageFile="~/MasterPages/WorkflowMaster.Master"
    AutoEventWireup="true" CodeBehind="Workflow.aspx.cs" Inherits="Forte.HCM.UI.Workflow" %>

<%@ Register Src="CommonControls/WorkFlowSideLinkButtonControl.ascx" TagName="WorkFlowSideLinkButtonControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/WorkflowMaster.Master" %>
<asp:Content ID="WorkFlow_Content" ContentPlaceHolderID="WorkflowMaster_body" runat="server">
    <br />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="Workflow_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="Workflow_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="Workflow_mainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div id="Workflow_mainDIV" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td height="77">
                                                    <img src="App_Themes/DefaultTheme/Images/wf_top_left_strip.jpg" alt="" width="9"
                                                        height="77" />
                                                </td>
                                                <td valign="middle" class="wf_top_center_strip">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="94%" align="left" class="wf_sub_title">
                                                                <asp:Label ID="Workflow_headerLabel" runat="server" Text="Workflow"></asp:Label>
                                                            </td>
                                                            <td width="6%" align="left" class="wf_back_icon">
                                                                <asp:LinkButton ID="Workflow_backLinkButton" runat="server" Text="Back" CssClass="wf_back"
                                                                    PostBackUrl="~/Landing.aspx"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <img src="App_Themes/DefaultTheme/Images/wf_top_right_strip.jpg" alt="" width="9"
                                                        height="77" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="wf_body">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 30%" valign="top">
                                                    <uc1:WorkFlowSideLinkButtonControl ID="Workflow_workFlowSideLinkButtonControl" runat="server" />
                                                </td>
                                                <td width="70%" valign="middle" align="center">
                                                    <img src="App_Themes/DefaultTheme/Images/workflow_01.jpg" alt="" width="721" height="524"
                                                        border="0" usemap="#Map" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <map name="Map" id="Map">
        <area shape="rect" coords="56,212,141,327" href="ClientCenter/ClientManagement.aspx?m=0&s=0&parentpage=WF_MAJOR" title="Client Management" alt="" />
        <area shape="rect" coords="51,33,142,152" href="PositionProfile/SearchPositionProfile.aspx?m=1&s=1&parentpage=WF_MAJOR" title="Position Profile Search" alt="" />
       <%-- <area shape="rect" coords="175,42,261,139" href="TalentScoutHome.aspx" title="Search Input" alt="" />
        <area shape="rect" coords="294,43,388,154" href="TalentScoutHome.aspx" title="Free Form Weighted Search" alt="" />
        <area shape="rect" coords="413,45,488,145" href="TalentScoutHome.aspx" title="Search" alt="" />
        <area shape="rect" coords="527,44,607,157" href="TalentScoutHome.aspx" title="intelliSPOT Result" alt="" />--%>
        <area shape="rect" coords="157,213,246,312" href="PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0&parentpage=WF_MAJOR" title="Position Profile" alt="" />
        <area shape="rect" coords="269,214,367,323" href="TestMaker/CreateManualTestWithoutAdaptive.aspx?m=1&s=1&parentpage=WF_MAJOR" title="Create Customized Assessment" alt="" />
        <area shape="rect" coords="388,211,483,307" href="InterviewWorkflow.aspx" title="intelliVIEW" alt="" />
        <area shape="rect" coords="531,212,619,305" href="AssessmentWorkflow.aspx" title="intelliTEST" alt="" />
        <area shape="rect" coords="210,378,324,462" href="InterviewReportCenter/InterviewTestReport.aspx?m=4&s=0&parentpage=WF_MAJOR&index=6" title="intelliVIEW Reports" alt="" />
        <area shape="rect" coords="339,375,449,462" href="ReportCenter/TestReport.aspx?m=3&s=0&parentpage=WF_MAJOR&index=6" title="intelliTEST Reports" alt="" />
    </map>
</asp:Content>
