﻿#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives


namespace Forte.HCM.UI.Authentication
{
    public partial class UserRightsMatrix : PageBase
    {

        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "250px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "350px";

        #endregion Private Constants

        #region Event Handler
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Assign User Rights");

                Master.HideUpdateProgress(UserRightsMatrix_topSelectUpdatePanel.UniqueID);

                Page.Form.DefaultButton = UserRightsMatrix_showDetailButton.UniqueID;

                Page.SetFocus(UserRightsMatrix_showDetailButton.ClientID);

                if (!IsPostBack)
                {
                    //Assign the datasource for the modules check box list
                    UserRightsMatrix_modulesCheckBoxList.DataSource = new AuthenticationBLManager().GetModuleList();
                    UserRightsMatrix_modulesCheckBoxList.DataBind();

                    //Add on click event of the select user Image button
                    UserRightsMatrix_userSelectImageButton.Attributes.Add("onclick",
                    "javascript:return LoadUserForUserRightsMatrix('"
                    + UserRightsMatrix_userTextBox.ClientID + "','"
                    + UserRightsMatrix_userIDHiddenField.ClientID + "','"
                    + UserRightsMatrix_userTextBox.ClientID + "','QA','"
                    + UserRightsMatrix_individualRoleNameLabel.ClientID + "')");


                    //Add on click attribute for the check box list
                    //UserRightsMatrix_modulesCheckBoxList.Attributes.Add("onclick", "javascript:return selectedModuleId();");
                    if (Request.QueryString["userID"] != null)
                    {
                        UserDetail userDetail = new CommonBLManager().GetUserDetail(int.Parse(Request.QueryString["userID"]));
                        UserRightsMatrix_userTextBox.Text = userDetail.FirstName;

                        UserRightsMatrix_userIDHiddenField.Value = userDetail.UserID.ToString();
                    }

                }


                UserRightsMatrix_assignRights_expandTD.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                UserRightsMatrix_assignRightsDiv.ClientID + "','" +
                UserRightsMatrix_searchDIV.ClientID + "','" +
                UserRightsMatrix_assignRolesUpSpan.ClientID + "','" +
                UserRightsMatrix_assignRolesDownSpan.ClientID + "','" +
                UserRightsMatrix_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

                UserRightsMatrix_topErrorMessageLabel.Text = string.Empty;
                UserRightsMatrix_topSuccessMessageLabel.Text = string.Empty;

                UserRightsMatrix_bottomErrorMessageLabel.Text = string.Empty;
                UserRightsMatrix_bottomSuccessMessageLabel.Text = string.Empty;

                //Assign on click event handler for the page navigator
                UserRightMatrix_pageNavigator.PageNumberClick += new
                    CommonControls.PageNavigator.PageNumberClickEventHandler
                    (UserRightMatrix_pageNavigator_PageNumberClick);

                CheckAndSetExpandOrRestore();

            }
            catch (Exception exception)
            {
                base.ShowMessage(UserRightsMatrix_topErrorMessageLabel,
                    UserRightsMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the UserRightMatrix_pageNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Forte.HCM.EventSupport.PageNumberEventArgs"/>
        /// instance containing the event data.</param>
        void UserRightMatrix_pageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadUserRights(e.PageNumber);
            }
            catch (Exception exception)
            {
                base.ShowMessage(UserRightsMatrix_topErrorMessageLabel,
                    UserRightsMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the UserRightsMatrix_modulesCheckBoxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void UserRightsMatrix_modulesCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetSelectedRoleIDs();
            }
            catch (Exception exception)
            {
                base.ShowMessage(UserRightsMatrix_topErrorMessageLabel,
                    UserRightsMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Click event of the UserRightsMatrix_showDetailButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void UserRightsMatrix_showDetailButton_Click(object sender, EventArgs e)
        {
            try
            {
                //If the user name and module text box is empty show the
                // error message
                if ((UserRightsMatrix_modulesTextBox.Text.Trim() == "" &&
                    UserRightsMatrix_modulesTextBox.Text.Length == 0) ||
                    (UserRightsMatrix_userTextBox.Text.Trim() == "" &&
                    UserRightsMatrix_userTextBox.Text.Length == 0))
                {
                    base.ShowMessage(UserRightsMatrix_topErrorMessageLabel,
                        UserRightsMatrix_bottomErrorMessageLabel,
                        Resources.HCMResource.UserRightsMatrix_EnterSearchTerm);

                    UserRightsMatrix_assignRolesGridView.DataSource = null;
                    UserRightsMatrix_assignRolesGridView.DataBind();

                    UserRightsMatrix_assignRolesGridView.AllowSorting = true;
                    UserRightMatrix_pageNavigator.TotalRecords = 0;
                    UserRightMatrix_pageNavigator.PageSize = base.GridPageSize;

                    return;
                }

                //Assign the defaul values for the sort field and 
                // sort order 
                ViewState["SORT_FIELD"] = "ROLE_NAME";

                ViewState["SORT_ORDER"] = SortType.Ascending;

                UserRightMatrix_pageNavigator.Reset();
                UserRightMatrix_pageNavigator.TotalRecords = 0;

                //Load the deatils for the first page
                LoadUserRights(1);
            }
            catch (Exception exception)
            {
                base.ShowMessage(UserRightsMatrix_topErrorMessageLabel,
                    UserRightsMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Sorting event of the UserRightsMatrix_assignRolesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/>
        /// instance containing the event data.</param>
        protected void UserRightsMatrix_assignRolesGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                UserRightMatrix_pageNavigator.Reset();

                //Load the user rights from the first page
                LoadUserRights(1);
            }
            catch (Exception exception)
            {
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                   UserRightsMatrix_topErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the RowCreated event of the UserRightsMatrix_assignRolesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void UserRightsMatrix_assignRolesGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.Header)
                //{
                //    //Get the column index for the sorting 
                //    int sortColumnIndex = GetSortColumnIndex
                //        (UserRightsMatrix_assignRolesGridView,
                //        ViewState["SORT_FIELD"].ToString());

                //    if (sortColumnIndex != -1)
                //    {
                //        //Add sort image for the corresponding column
                //        AddSortImage(sortColumnIndex, e.Row,
                //            (SortType)ViewState["SORT_ORDER"]);
                //    }
                //}
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the UserRightsMatrix_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void UserRightsMatrix_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (UserRightsMatrix_assignRolesGridView.Rows.Count == 0)
                {
                    ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                UserRightsMatrix_topErrorMessageLabel, Resources.HCMResource.UserRightsMatrix_PleaseSelectUserRights);
                    return;
                }

                SaveUerRights();

                ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = "ROLE_NAME";

                // Reset and show records for first page.
                UserRightMatrix_pageNavigator.Reset();

                //Load the user rights from the first page
                LoadUserRights(1);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }

        }

        /// <summary>
        /// Handles the Click event of the UserRightsMatrix_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void UserRightsMatrix_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_selectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_selectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem item in UserRightsMatrix_modulesCheckBoxList.Items)
                {
                    item.Selected = true;
                }

                GetSelectedRoleIDs();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_unSelectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_unSelectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem item in UserRightsMatrix_modulesCheckBoxList.Items)
                {
                    item.Selected = false;
                }

                GetSelectedRoleIDs();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }
        }

        protected void UserRightsMatrix_assignRolesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    return;
                }

                //CheckBox checkBox = (CheckBox)e.Row.FindControl("UserRightsMatrix_assignRolesGridView_allowCheckBox");
                //checkBox.CheckedChanged += new EventHandler(checkBox_CheckedChanged);


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }
        }

        protected void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                GridViewRow gridViewRow = (GridViewRow)((CheckBox)(sender)).Parent.Parent;

                if (gridViewRow != null)
                {
                    HiddenField userRightsIDhiddenField = (HiddenField)gridViewRow.FindControl("UserRightsMatrix_assignRolesGridView_userIDHiddenField");
                    CheckBox checkBox = (CheckBox)gridViewRow.FindControl("UserRightsMatrix_assignRolesGridView_allowCheckBox");
                    HiddenField modIdHiddenField = (HiddenField)gridViewRow.FindControl("UserRightsMatrix_assignRolesGridView_modIDHiddenField");
                    HiddenField sModIDHiddenField = (HiddenField)gridViewRow.FindControl("UserRightsMatrix_assignRolesGridView_subModuleIDHiddenField");
                    HiddenField feaIDHiddenField = (HiddenField)gridViewRow.FindControl("UserRightsMatrix_assignRolesGridView_featureIDHiddenField");
                    HiddenField userIDHiddenField = (HiddenField)gridViewRow.FindControl("UserRightsMatrix_assignRolesGridView_usrIDHiddenField");
                    HiddenField isActiveHiddenField = (HiddenField)gridViewRow.FindControl("UserRightsMatrix_assignRolesGridView_isActiveHiddenField");


                    DataObjects.UserRightsMatrix userRight = new DataObjects.UserRightsMatrix();

                    if (userRightsIDhiddenField.Value != "")
                    {
                        userRight.UserRightsID = int.Parse(userRightsIDhiddenField.Value);
                    }
                    if (checkBox != null)
                    {
                        userRight.IsApplicable = checkBox.Checked;
                    }
                    if (!Utility.IsNullOrEmpty(modIdHiddenField) && modIdHiddenField.Value != "")
                    {
                        userRight.ModuleID = int.Parse(modIdHiddenField.Value);
                    }
                    if (!Utility.IsNullOrEmpty(sModIDHiddenField) && sModIDHiddenField.Value != "")
                    {
                        userRight.SubModuleID = int.Parse(sModIDHiddenField.Value);
                    }
                    if (!Utility.IsNullOrEmpty(feaIDHiddenField) && feaIDHiddenField.Value != "")
                    {
                        userRight.FeatureID = int.Parse(feaIDHiddenField.Value);
                    }

                    if (!Utility.IsNullOrEmpty(isActiveHiddenField) && isActiveHiddenField.Value != "")
                    {
                        userRight.IsActive = Convert.ToBoolean(isActiveHiddenField.Value);
                    }


                    userRight.UserID = int.Parse(UserRightsMatrix_userIDHiddenField.Value);

                    userRight.ModifiedBy = base.userID;
                    userRight.CreatedBy = base.userID;

                    new AuthenticationBLManager().UpdateUserRightMatrix(userRight);

                    LoadUserRights(1);

                    base.ShowMessage(UserRightsMatrix_bottomSuccessMessageLabel, UserRightsMatrix_topSuccessMessageLabel,
    Resources.HCMResource.UserRightsMatrix_UpdatedSuccessfully);
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }

        }

        protected void UserRightsMatrix_hiddenButton_Click(object sender, EventArgs e)
        {
            try
            {
                UserRightsMatrix_individualRoleNameLabel.Text =
                new AuthenticationBLManager().GetRolesForUser(int.Parse(UserRightsMatrix_userIDHiddenField.Value));
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRightsMatrix_bottomErrorMessageLabel,
                    UserRightsMatrix_topErrorMessageLabel, exception.Message);
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Gets the selected role I ds.
        /// </summary>
        private void GetSelectedRoleIDs()
        {
            UserRightsMatrix_roleHiddenField.Value = string.Empty;

            string selectedModuleName = "";

            for (int i = 0; i < UserRightsMatrix_modulesCheckBoxList.Items.Count; i++)
            {
                if (UserRightsMatrix_modulesCheckBoxList.Items[i].Selected)
                {
                    UserRightsMatrix_roleHiddenField.Value += ", " + UserRightsMatrix_modulesCheckBoxList.Items[i].Value;

                    selectedModuleName += ", " + UserRightsMatrix_modulesCheckBoxList.Items[i].Text;
                }
            }

            if (selectedModuleName != null || selectedModuleName.Length != 0)
            {
                selectedModuleName = selectedModuleName.TrimStart(',', ' ');
                UserRightsMatrix_roleHiddenField.Value = UserRightsMatrix_roleHiddenField.Value.TrimStart(',', ' ');
            }

            UserRightsMatrix_modulesTextBox.Text = selectedModuleName;
        }

        /// <summary>
        /// Saves the uer rights.
        /// </summary>
        private void SaveUerRights()
        {
            List<DataObjects.UserRightsMatrix> userMatrixDeatils = new List<DataObjects.UserRightsMatrix>();

            DataObjects.UserRightsMatrix userRight = null;

            foreach (GridViewRow row in UserRightsMatrix_assignRolesGridView.Rows)
            {
                userRight = new DataObjects.UserRightsMatrix();

                HiddenField userRightsIDhiddenField = (HiddenField)row.FindControl("UserRightsMatrix_assignRolesGridView_userIDHiddenField");
                CheckBox checkBox = (CheckBox)row.FindControl("UserRightsMatrix_assignRolesGridView_allowCheckBox");
                HiddenField modIdHiddenField = (HiddenField)row.FindControl("UserRightsMatrix_assignRolesGridView_modIDHiddenField");
                HiddenField sModIDHiddenField = (HiddenField)row.FindControl("UserRightsMatrix_assignRolesGridView_subModuleIDHiddenField");
                HiddenField feaIDHiddenField = (HiddenField)row.FindControl("UserRightsMatrix_assignRolesGridView_featureIDHiddenField");
                // HiddenField rolIDHiddenField = (HiddenField)row.FindControl("UserRightsMatrix_assignRolesGridView_rolIDHiddenField");


                if (userRightsIDhiddenField.Value != "")
                {
                    userRight.UserRightsID = int.Parse(userRightsIDhiddenField.Value);
                }
                if (checkBox != null)
                {
                    userRight.IsApplicable = checkBox.Checked;
                }
                if (!Utility.IsNullOrEmpty(modIdHiddenField) && modIdHiddenField.Value != "")
                {
                    userRight.ModuleID = int.Parse(modIdHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(sModIDHiddenField) && sModIDHiddenField.Value != "")
                {
                    userRight.SubModuleID = int.Parse(sModIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(feaIDHiddenField) && feaIDHiddenField.Value != "")
                {
                    userRight.FeatureID = int.Parse(feaIDHiddenField.Value);
                }
                //if (!Utility.IsNullOrEmpty(rolIDHiddenField) && rolIDHiddenField.Value != "")
                //{
                //    userRight.RoleID = int.Parse(rolIDHiddenField.Value);
                //}

                userRight.IsEditable = checkBox.Enabled;

                userRight.UserID = int.Parse(UserRightsMatrix_userIDHiddenField.Value);

                userRight.CreatedBy = base.userID;

                userRight.ModifiedBy = base.userID;

                userMatrixDeatils.Add(userRight);
            }

            new AuthenticationBLManager().UpdateUserRights(userMatrixDeatils);

            base.ShowMessage(UserRightsMatrix_bottomSuccessMessageLabel, UserRightsMatrix_topSuccessMessageLabel,
                Resources.HCMResource.UserRightsMatrix_UpdatedSuccessfully);
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(UserRightsMatrix_restoreHiddenField.Value) &&
                 UserRightsMatrix_restoreHiddenField.Value == "Y")
            {
                UserRightsMatrix_searchDIV.Style["display"] = "none";
                UserRightsMatrix_assignRolesUpSpan.Style["display"] = "block";
                UserRightsMatrix_assignRolesDownSpan.Style["display"] = "none";
                UserRightsMatrix_assignRightsDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                UserRightsMatrix_searchDIV.Style["display"] = "block";
                UserRightsMatrix_assignRolesUpSpan.Style["display"] = "none";
                UserRightsMatrix_assignRolesDownSpan.Style["display"] = "block";
                UserRightsMatrix_assignRightsDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Loads the user rights.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void LoadUserRights(int pageNumber)
        {
            string orderBy = ViewState["SORT_FIELD"].ToString();
            SortType orderByDirection = ((SortType)ViewState["SORT_ORDER"]);

            int totalRecordCount = 0;

            List<DataObjects.UserRightsMatrix> userRightDetails = new List<DataObjects.UserRightsMatrix>();

            int showAllFeature = UserRightsMatrix_showApplicableFeaturesRadioButton.Checked ? 0 : 1;

            userRightDetails = new AuthenticationBLManager().GetUserRightsMatrixDetails
                (UserRightsMatrix_userIDHiddenField.Value,
                UserRightsMatrix_roleHiddenField.Value,
                showAllFeature,
                pageNumber, base.GridPageSize, orderBy, orderByDirection, out totalRecordCount);

            if (userRightDetails.Count == 0)
            {
                base.ShowMessage(UserRightsMatrix_topErrorMessageLabel,
                    UserRightsMatrix_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);

                UserRightsMatrix_assignRolesGridView.DataSource = userRightDetails;
                UserRightsMatrix_assignRolesGridView.DataBind();

                UserRightMatrix_pageNavigator.TotalRecords = 0;
                //UserRightMatrix_pageNavigator.PageSize = 0;

                return;
            }

            UserRightsMatrix_assignRolesGridView.DataSource = userRightDetails;
            UserRightsMatrix_assignRolesGridView.DataBind();

            UserRightsMatrix_assignRolesGridView.AllowSorting = true;
            UserRightMatrix_pageNavigator.TotalRecords = totalRecordCount;
            UserRightMatrix_pageNavigator.PageSize = base.GridPageSize;
        }
        #endregion Private Methods

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Override Methods


    }
}