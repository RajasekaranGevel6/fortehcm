﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    CodeBehind= "UserRoleMatrix.aspx.cs" Inherits="Forte.HCM.UI.Authentication.UserRoleMatrix" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="UserRoleMatrix_Content" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">

        var xGridPos, yGridPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xGridPos = $get('<%= UserRoleMatrix_userDetailsDIV.ClientID %>').scrollLeft;
            yGridPos = $get('<%= UserRoleMatrix_userDetailsDIV.ClientID %>').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('<%= UserRoleMatrix_userDetailsDIV.ClientID %>').scrollLeft = xGridPos;
            $get('<%= UserRoleMatrix_userDetailsDIV.ClientID %>').scrollTop = yGridPos;
        }
    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Label ID="test" runat="server"></asp:Label>
                                    <asp:Literal ID="UserRoleMatrix_headerLiteral" runat="server" Text="Assign User Roles"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="UserRoleMatrix_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                    OnClick="UserRoleMatrix_topSaveButton_Click" Visible="false" />
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="UserRoleMatrix_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="UserRoleMatrix_topResetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="UserRoleMatrix_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="UserRoleMatrix_topMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="UserRoleMatrix_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="UserRoleMatrix_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td class="panel_bg">
                                                <div id="ViewContributorSummary_questionSummaryDIV" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <%-- <td class="header_bg">
                                                                <asp:Literal ID="UserRoleMatrix_selectRoleLiteral" runat="server" Text="Select User"></asp:Literal>
                                                            </td>--%>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <asp:UpdatePanel ID="UserRoleMatrix_selectRoleUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <table width="100%" cellpadding="0" cellspacing="3" border="0" class="panel_inner_body_bg">
                                                                                <tr>
                                                                                    <td style="vertical-align: middle">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td style="width: 3%">
                                                                                                    <asp:Label ID="UserRoleMatrix_selectUser" runat="server" Text="User" SkinID="sknLabelFieldHeaderText">
                                                                                                    </asp:Label>
                                                                                                    <span class='mandatory'>*</span>
                                                                                                </td>
                                                                                                <td style="width: 17%">
                                                                                                    <div style="float: left; padding-right: 5px; width: 90%">
                                                                                                        <asp:TextBox ID="UserRoleMatrix_userIDTextBox" runat="server" ReadOnly="true" Width="95%"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="UserRoleMatrix_userIDHiddenField" runat="server" />
                                                                                                        <asp:HiddenField ID="UserRoleMatrix_browserHiddenField" runat="server" />
                                                                                                    </div>
                                                                                                    <div style="float: left; width: 7%">
                                                                                                        <asp:ImageButton ID="UserRoleMatrix_searchImage" runat="server" SkinID="sknbtnSearchicon"
                                                                                                            ToolTip="Click here to select the user" />
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td style="width: 2%">
                                                                                                    <asp:ImageButton ID="UserRoleMatrix_helpImageButton" SkinID="sknHelpImageButton"
                                                                                                        runat="server" OnClientClick="javascript:return false;" ToolTip="Please select the user here" />
                                                                                                </td>
                                                                                                <td style="width: 40%">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RadioButton ID="UserRoleMatrix_applcableRoleRadioButton" runat="server" Checked="true"
                                                                                                                    GroupName="a" Text="Show Assigned Roles Only" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RadioButton ID="UserRoleMatrix_allRolesRadioButton" runat="server" GroupName="a"
                                                                                                                    Text="Show All Roles" />
                                                                                                            </td>
                                                                                                            <td colspan="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="UserRoleMatrix_showButton" runat="server" OnClick="UserRoleMatrix_showButton_Click"
                                                                    SkinID="sknButtonId" Text="Show Details" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 98%" align="left">
                                                            <asp:Label ID="UserRoleMatrix_assignRolesLabel" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 2%" align="right">
                                                            <asp:HiddenField ID="UserRoleMatrix_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="UserRoleMatrix_userDetailsTR" runat="server">
                                            <td class="header_bg" align="center">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="UserRoleMatrix_userDetailHeaderLiteral" runat="server" Text="Assign Roles"></asp:Literal>
                                                            <asp:Label ID="UserRoleMatrix_userDetailHeaderHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text=" - Click column headers to sort">                                                
                                                            </asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="UserRoleMatrix_userDetailsUparrowSpan" style="display: block;" runat="server">
                                                                <asp:Image ID="UserRoleMatrix_userDetailsUpArrow" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="UserRoleMatrix_userDetailsDownarrowSpan" style="display: none;"
                                                                runat="server">
                                                                <asp:Image ID="UserRoleMatrix_userDetailsDownArrow" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <div id="UserRoleMatrix_userDetailsDIV" runat="server" style="height: 525px; overflow: auto;">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UserRoleMatrix_rolesUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="UserRoleMatrix_rolesGridView" runat="server" AllowSorting="True"
                                                                            OnSorting="UserRoleMatrix_userRolesGridView_Sorting" OnRowCreated="UserRoleMatrix_userRoles_GridView_RowCreated"
                                                                            AutoGenerateColumns="False">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Role" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Left"
                                                                                    ItemStyle-Width="25%" SortExpression="ROLE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="UserRoleMatrix_rolesGridView_rolecategotycodeLabel" runat="server"
                                                                                            Text='<%# Eval("RoleName") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="UserRoleMatrix_rolenAMEGridViewIDHiddenField" runat="server"
                                                                                            Value='<%# Eval("RoleID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Job Description" HeaderStyle-Width="70%" HeaderStyle-HorizontalAlign="Left"
                                                                                    ItemStyle-Width="36%" SortExpression="JOBDISCRIPTION">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="UserRoleMatrix_rolesGridView_rolecategotynameLabel" runat="server"
                                                                                            Text='<%# Eval("RoleJobDescription") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="UserRoleMatrix_rolesjdGridViewIDHiddenField" runat="server"
                                                                                            Value='<%# Eval("RoleID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <%--  <asp:TemplateField HeaderText="Allow" HeaderStyle-Width="46%" HeaderStyle-HorizontalAlign="Left"
                                                                                    ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                    <asp:Button ID="btnTest" runat="server" CommandName="btn" />
                                                                                        <asp:CheckBox ID="UserRoleMatrix_rolesGridView_CheckBox" runat="server" />
                                                                                        <asp:HiddenField ID="UserRoleMatrix_rolesGridViewIDHiddenField" runat="server" Value='<%# Eval("RoleID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>
                                                                                <asp:TemplateField HeaderText="Allow" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left"
                                                                                    ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="UserRoleMatrix_rolesGridView_CheckBox" runat="server" AutoPostBack="true" Checked="false" OnCheckedChanged="checkBox_CheckedChanged" />
                                                                                        <ajaxToolKit:ToggleButtonExtender ID="UserRoleMatrix_assignRolesGridView_allowCheckBoxToggleButton"
                                                                                            runat="server" TargetControlID="UserRoleMatrix_rolesGridView_CheckBox" ImageHeight="25"
                                                                                            ImageWidth="25" CheckedImageUrl="~/App_Themes/DefaultTheme/Images/correct_answer.png"
                                                                                            UncheckedImageUrl="~/App_Themes/DefaultTheme/Images/wrong_answer.png" CheckedImageOverAlternateText="Assign Rights"
                                                                                            UncheckedImageOverAlternateText="Unassign Roles">
                                                                                        </ajaxToolKit:ToggleButtonExtender>
                                                                                        <asp:HiddenField ID="UserRoleMatrix_rolesGridViewIDHiddenField" runat="server" Value='<%# Eval("RoleID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                        <asp:HiddenField ID="UserRoleMatrix_checkedStatusHiddenField" runat="server" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <caption>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                </tr>
            </table>
            </div> </caption> </td> </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="UserRoleMatrix_bottomMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="UserRoleMatrix_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            <asp:Label ID="UserRoleMatrix_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 72%" class="header_text_bold">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td style="width: 52%">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Button ID="UserRoleMatrix_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                OnClick="UserRoleMatrix_topSaveButton_Click"  Visible="false"  />
                                        </td>
                                        <td style="width: 16%" align="right">
                                            <asp:LinkButton ID="UserRoleMatrix_bottomResetLinkButton" runat="server" Text="Reset"
                                                SkinID="sknActionLinkButton" OnClick="UserRoleMatrix_topResetLinkButton_Click"></asp:LinkButton>
                                        </td>
                                        <td width="4%" align="center" class="link_button">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="UserRoleMatrix_bottomCancelLinkButton" runat="server" Text="Cancel"
                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="UserRoleMatrix_isMaximizedHiddenField" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
