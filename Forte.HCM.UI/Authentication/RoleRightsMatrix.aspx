﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    AutoEventWireup="true" CodeBehind="RoleRightsMatrix.aspx.cs" Inherits="Forte.HCM.UI.Authentication.RoleRightsMatrix" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="RoleRightsMatrixContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
        function StoreIdAndValue(id) {
            var hiddenField = document.getElementById('<%=RoleRightsMatrix_checkedStatusHiddenField.ClientID %>');
            hiddenField.value = hiddenField.value + id + ",";
            alert(hiddenField.value);
        }

        function selectedModuleId() {
            var moduleCheckBoxList = document.getElementById('<%=RoleRightsMatrix_moduleCheckBoxList.ClientID %>');
            var moduleIDHiddenField = document.getElementById('<%=RoleRightsMatrix_moduleIDHiddenField.ClientID %>');
            var moduleNameTextBox = document.getElementById('<%=RoleRightsMatrix_moduleComboTextBox.ClientID %>');
            moduleNameTextBox.innerText = "";
            moduleIDHiddenField.value = "";
            for (var i = 0; i < moduleCheckBoxList.cells.length; i++) {
                if (moduleCheckBoxList.cells[i].childNodes[0].checked) {
                    if (moduleIDHiddenField.value == "")
                    { moduleIDHiddenField.value = moduleCheckBoxList.cells[i].innerText; }
                    else {
                        moduleIDHiddenField.value += ", " + moduleCheckBoxList.cells[i].innerText;
                    }
                    moduleNameTextBox.innerText = moduleIDHiddenField.value;

                }
            }

            //            RoleRightsMatrix_moduleIDHiddenField.Value += ", " + RoleRightsMatrix_moduleCheckBoxList.Items[i].Value;

            //            selectedModuleName += ", " + RoleRightsMatrix_moduleCheckBoxList.Items[i].Text;
        }

        var xPos, yPos, xRolPos, yRolPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('<%= RoleRightsMatrix_modulePanel.ClientID %>').scrollLeft;
            yPos = $get('<%= RoleRightsMatrix_modulePanel.ClientID %>').scrollTop;


            xRolPos = $get('<%= RoleRightsMatrix_rolePanel.ClientID %>').scrollLeft;
            yRolPos = $get('<%= RoleRightsMatrix_rolePanel.ClientID %>').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('<%= RoleRightsMatrix_modulePanel.ClientID %>').scrollLeft = xPos;
            $get('<%= RoleRightsMatrix_modulePanel.ClientID %>').scrollTop = yPos;



            $get('<%= RoleRightsMatrix_rolePanel.ClientID %>').scrollLeft = xRolPos;
            $get('<%= RoleRightsMatrix_rolePanel.ClientID %>').scrollTop = yRolPos;
        }

      
        
    </script>
    <table width="100%" cellpadding="0" cellspacing="0" onload="AddHandler();">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                            <asp:Literal ID="RoleRightsMatrix_headerLiteral" runat="server" Text="Role Rights Matrix"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 52%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="RoleRightsMatrix_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="RoleRightsMatrix_saveButton_Click" />
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="RoleRightsMatrix_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="RoleRightsMatrix_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="RoleRightsMatrix_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="RoleRightsMatrix_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="RoleRightsMatrix_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="RoleRightsMatrix_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="RoleRightsMatrix_selectRoleUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div id="RoleRightsMatrix_selectRoleDIV" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="panel_bg">
                                                        <tr>
                                                            <td class="panel_inner_body_bg">
                                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="RoleRightsMatrix_selectModule" runat="server" Text="Modules" SkinID="sknLabelFieldHeaderText">
                                                                            </asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="RoleRightsMatrix_moduleSelectAllLinkButton" runat="server" Text="Select All"
                                                                                SkinID="sknActionLinkButton" OnClick="RoleRightsMatrix_moduleSelectAllLinkButton_Click"></asp:LinkButton>
                                                                            <span class="link_button">&nbsp;|&nbsp;</span>
                                                                            <asp:LinkButton ID="RoleRightsMatrix_moduleClearAllLinkButton" runat="server" Text="Clear All"
                                                                                SkinID="sknActionLinkButton" OnClick="RoleRightsMatrix_moduleClearAllLinkButton_Click"></asp:LinkButton>
                                                                        </td>
                                                                        <td style="width: 5%">
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="RoleRightsMatrix_selectRoleLabel" runat="server" Text="Roles" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="RoleRightsMatrix_roleSelectAllLinkButton" runat="server" Text="Select All"
                                                                                SkinID="sknActionLinkButton" OnClick="RoleRightsMatrix_roleSelectAllLinkButton_Click"></asp:LinkButton>
                                                                            <span class="link_button">&nbsp;|&nbsp;</span>
                                                                            <asp:LinkButton ID="RoleRightsMatrix_roleClearAllLinkButton" runat="server" Text="Clear All"
                                                                                SkinID="sknActionLinkButton" OnClick="RoleRightsMatrix_roleClearAllLinkButton_Click"></asp:LinkButton>
                                                                        </td>
                                                                        <td style="width: 5%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="RoleRightsMatrix_moduleComboTextBox" ReadOnly="true" runat="server"
                                                                                Width="99%" Font-Size="X-Small"></asp:TextBox>
                                                                            <asp:Panel ID="RoleRightsMatrix_modulePanel" runat="server" CssClass="popupControl_module">
                                                                                <asp:CheckBoxList ID="RoleRightsMatrix_moduleCheckBoxList" RepeatColumns="1" runat="server"
                                                                                    CellPadding="0" DataValueField="ModuleID" DataTextField="ModuleName" AutoPostBack="true"
                                                                                    OnSelectedIndexChanged="RoleRightsMatrix_moduleCheckBoxList_SelectedIndexChanged">
                                                                                </asp:CheckBoxList>
                                                                            </asp:Panel>
                                                                            <ajaxToolKit:PopupControlExtender ID="RoleRightsMatrix_modulePopupControlExtender"
                                                                                runat="server" TargetControlID="RoleRightsMatrix_moduleComboTextBox" PopupControlID="RoleRightsMatrix_modulePanel"
                                                                                OffsetX="2" OffsetY="2" Position="Bottom">
                                                                            </ajaxToolKit:PopupControlExtender>
                                                                            <asp:HiddenField ID="RoleRightsMatrix_moduleIDHiddenField" runat="server" />
                                                                        </td>
                                                                        <td style="width: 5%" align="center">
                                                                            <asp:ImageButton ID="RoleRightsMatrix_moduleHelpImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                ToolTip="Please select the modules here" OnClientClick="javascript:return false;"/>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="RoleRightsMatrix_roleTextBox" ReadOnly="true" runat="server" Width="99%"
                                                                                Font-Size="X-Small"></asp:TextBox>
                                                                            <asp:Panel ID="RoleRightsMatrix_rolePanel" runat="server" CssClass="popupControl_role">
                                                                                <asp:CheckBoxList ID="RoleRightsMatrix_roleCheckBoxList" RepeatColumns="1" runat="server"
                                                                                    CellPadding="0" AutoPostBack="true" DataValueField="RoleID" DataTextField="RoleName"
                                                                                    OnSelectedIndexChanged="RoleRightsMatrix_roleCheckBoxList_SelectedIndexChanged">
                                                                                </asp:CheckBoxList>
                                                                            </asp:Panel>
                                                                            <ajaxToolKit:PopupControlExtender ID="RoleRightsMatrix_rolePopupControlExtender"
                                                                                runat="server" TargetControlID="RoleRightsMatrix_roleTextBox" PopupControlID="RoleRightsMatrix_rolePanel"
                                                                                OffsetX="2" OffsetY="2" Position="Bottom">
                                                                            </ajaxToolKit:PopupControlExtender>
                                                                            <asp:HiddenField ID="RoleRightsMatrix_roleHiddenField" runat="server" />
                                                                        </td>
                                                                        <td style="width: 5%" align="center">
                                                                            <asp:ImageButton ID="RoleRightsMatrix_roleHelpImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                ToolTip="Please select the roles here" OnClientClick="javascript:return false;"/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="RoleRightsMatrix_showButton" runat="server" Text="Show Details" SkinID="sknButtonId"
                                                                    OnClick="RoleRightsMatrix_showButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 98%" align="left" class="header_text_bold">
                                                    <asp:Label ID="RoleRightsMatrix_assignRolesLabel" runat="server" Text="Assign Rights"></asp:Label>
                                                </td>
                                                <td style="width: 2%" align="right">
                                                    <span id="RoleRightsMatrix_assignRolesUpSpan" runat="server" style="display: none;">
                                                        <asp:Image ID="RoleRightsMatrix_assignRolesUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                            id="RoleRightsMatrix_assignRolesDownSpan" runat="server" style="display: block;"><asp:Image
                                                                ID="RoleRightsMatrix_assignRolesDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                    <asp:HiddenField ID="RoleRightsMatrix_restoreHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <div id="RoleRightsMatrix_assignRightDIV" runat="server" style="height: 225px; width: 930px;
                                                        overflow: auto;">
                                                        <asp:UpdatePanel ID="RoleRightsMatrix_rolesUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="RoleRightsMatrix_rolesGridView" runat="server" AutoGenerateColumns="true"
                                                                    SkinID="sknNewAutoGridView" 
                                                                    OnRowDataBound="RoleRightsMatrix_rolesGridView_RowDataBound" DataKeyNames="FeatureID">
                                                                </asp:GridView>
                                                                <asp:HiddenField ID="RoleRightsMatrix_checkedStatusHiddenField" runat="server" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="RoleRightsMatrix_autoGeneratedGrid" runat="server" AutoGenerateColumns="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="RoleRightsMatrix_autoGeneratedGrid_featureId" runat="server" Text='<%# Eval("FeatureName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="RoleRightsMatrix_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="RoleRightsMatrix_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="RoleRightsMatrix_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 52%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="RoleRightsMatrix_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="RoleRightsMatrix_saveButton_Click" />
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="RoleRightsMatrix_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="RoleRightsMatrix_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="RoleRightsMatrix_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
