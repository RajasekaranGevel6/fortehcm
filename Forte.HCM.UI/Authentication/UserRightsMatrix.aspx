﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    AutoEventWireup="true" CodeBehind="UserRightsMatrix.aspx.cs" Inherits="Forte.HCM.UI.Authentication.UserRightsMatrix" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<%@ Register TagPrefix="uc1" Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" %>
<asp:Content ID="UserRightsMatrix_content" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
        function selectedModuleId() {
            var moduleCheckBoxList = document.getElementById('<%=UserRightsMatrix_modulesCheckBoxList.ClientID %>');
            var moduleIDHiddenField = document.getElementById('<%=UserRightsMatrix_roleHiddenField.ClientID %>');
            var moduleNameTextBox = document.getElementById('<%=UserRightsMatrix_modulesTextBox.ClientID %>');
            moduleNameTextBox.innerText = "";
            moduleIDHiddenField.value = "";
            for (var i = 0; i < moduleCheckBoxList.cells.length; i++) {

                if (detectBrowser()) {
                    if (moduleCheckBoxList.cells[i].childNodes[1].checked) {
                        if (moduleIDHiddenField.value == "")
                        { moduleIDHiddenField.value = moduleCheckBoxList.cells[i].innerText; }
                        else {
                            moduleIDHiddenField.value += ", " + moduleCheckBoxList.cells[i].innerText;
                        }
                        moduleNameTextBox.innerText = moduleIDHiddenField.value;
                    }
                    else {

                        if (moduleCheckBoxList.cells[i].childNodes[0].checked) {
                            if (moduleIDHiddenField.value == "")
                            { moduleIDHiddenField.value = moduleCheckBoxList.cells[i].innerText; }
                            else {
                                moduleIDHiddenField.value += ", " + moduleCheckBoxList.cells[i].innerText;
                            }
                            moduleNameTextBox.innerText = moduleIDHiddenField.value;

                        }

                    }
                }

                moduleIDHiddenField.Value += ", " + RoleRightsMatrix_moduleCheckBoxList.Items[i].Value;

                selectedModuleName += ", " + RoleRightsMatrix_moduleCheckBoxList.Items[i].Text;
            }
            //find the Browser details
            function detectBrowser() {
                var browser = navigator.appName;
                var b_version = navigator.appVersion;
                var version = parseFloat(b_version);
                if (browser == "Netscape") {
                    return true;
                }
                else {
                    return false; ;
                }
            }
        }

        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('<%= UserRightsMatrix_modulesPanel.ClientID %>').scrollLeft;
            yPos = $get('<%= UserRightsMatrix_modulesPanel.ClientID %>').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('<%= UserRightsMatrix_modulesPanel.ClientID %>').scrollLeft = xPos;
            $get('<%= UserRightsMatrix_modulesPanel.ClientID %>').scrollTop = yPos;
        }




        var xGridPos, yGridPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xGridPos = $get('<%= UserRightsMatrix_assignRightsDiv.ClientID %>').scrollLeft;
            yGridPos = $get('<%= UserRightsMatrix_assignRightsDiv.ClientID %>').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('<%= UserRightsMatrix_assignRightsDiv.ClientID %>').scrollLeft = xGridPos;
            $get('<%= UserRightsMatrix_assignRightsDiv.ClientID %>').scrollTop = yGridPos;
        }
        
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <asp:UpdatePanel ID="UserRightsMatrix_headerUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Literal ID="UserRightsMatrix_headerLiteral" runat="server" Text="Assign User Rights"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="UserRightsMatrix_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                    OnClick="UserRightsMatrix_saveButton_Click" Visible="false" />
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="UserRightsMatrix_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="UserRightsMatrix_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="UserRightsMatrix_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="UserRightsMatrix_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="UserRightsMatrix_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="UserRightsMatrix_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="UserRightsMatrix_searchDIV" runat="server">
                                <asp:UpdatePanel ID="UserRightsMatrix_topSelectUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                                            <tr>
                                                <td class="panel_inner_body_bg ">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="5">
                                                            </td>
                                                            <td align="right">
                                                                <div style="width: 77%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="width: 15%">
                                                                                <asp:LinkButton ID="PositionProfileFormEntry_selectAllLinkButton" runat="server"
                                                                                    SkinID="sknActionLinkButton" Text="Select All" ToolTip="Click here to select all the available segments"
                                                                                    OnClick="PositionProfileFormEntry_selectAllLinkButton_Click"></asp:LinkButton>
                                                                                <span>&nbsp;|&nbsp;</span>
                                                                            </td>
                                                                            <td style="width: 10%" align="left">
                                                                                <asp:LinkButton ID="PositionProfileFormEntry_unSelectAllLinkButton" runat="server"
                                                                                    SkinID="sknActionLinkButton" Text="Clear All" ToolTip="Click here to un select all the selected segments"
                                                                                    OnClick="PositionProfileFormEntry_unSelectAllLinkButton_Click"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="UserRightsMatrix_showApplicableFeaturesRadioButton" runat="server"
                                                                    Text="Show Assigned Features Only" GroupName="a" Checked="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 5%">
                                                                <asp:Label ID="UserRightsMatrix_userLabel" runat="server" Text="User" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <div style="float: left; padding-right: 5px; width: 90%">
                                                                    <asp:TextBox ID="UserRightsMatrix_userTextBox" runat="server" Width="97%" ReadOnly="True"></asp:TextBox>
                                                                    <asp:HiddenField ID="UserRightsMatrix_userIDHiddenField" runat="server" />
                                                                </div>
                                                                <div style="float: left; width: 5%">
                                                                    <asp:ImageButton ID="UserRightsMatrix_userSelectImageButton" SkinID="sknbtnSearchicon"
                                                                        runat="server" ImageAlign="Middle" ToolTip=" Please select the user here to assign the user rights " />
                                                                </div>
                                                            </td>
                                                            <td style="width: 3%;" align="center">
                                                                <asp:ImageButton ID="UserRightsMatrix_userHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the user here" />
                                                            </td>
                                                            <td style="width: 3%;">
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="UserRightsMatrix_modulesLabel" runat="server" Text="Modules" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <div style="float: left; padding-right: 5px; width: 91%">
                                                                    <asp:TextBox ID="UserRightsMatrix_modulesTextBox" runat="server" Width="95%"></asp:TextBox>
                                                                    <asp:Panel ID="UserRightsMatrix_modulesPanel" runat="server" CssClass="popupControl_user">
                                                                        <asp:CheckBoxList ID="UserRightsMatrix_modulesCheckBoxList" RepeatColumns="1" runat="server"
                                                                            CellPadding="0" DataValueField="ModuleID" DataTextField="ModuleName" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="UserRightsMatrix_modulesCheckBoxList_SelectedIndexChanged">
                                                                        </asp:CheckBoxList>
                                                                    </asp:Panel>
                                                                    <ajaxToolKit:PopupControlExtender ID="UserRightsMatrix_modulesPopupControlExtender"
                                                                        runat="server" TargetControlID="UserRightsMatrix_modulesTextBox" PopupControlID="UserRightsMatrix_modulesPanel"
                                                                        OffsetX="2" OffsetY="2" Position="Bottom">
                                                                    </ajaxToolKit:PopupControlExtender>
                                                                    <asp:HiddenField ID="UserRightsMatrix_roleHiddenField" runat="server" />
                                                                </div>
                                                                <div style="float: left; width: 5%">
                                                                    <asp:ImageButton ID="UserRightsMatrix_modulesHelpImageButton" SkinID="sknHelpImageButton"
                                                                        runat="server" ImageAlign="Middle" ToolTip="Please select the modules here" OnClientClick="javascript:return false;"/>
                                                                </div>
                                                            </td>
                                                            <td style="width: 3%;">
                                                            </td>
                                                            <td>
                                                                <div class="alignTop">
                                                                </div>
                                                                <div class="alignBottom">
                                                                    <asp:RadioButton ID="UserRightsMatrix_showAllFeaturesRadioButton" runat="server"
                                                                        Text="Show All Features" GroupName="a" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8" class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 4%">
                                                                <asp:Label ID="UserRightsMatrix_roleNameLabel" runat="server" Text="Roles" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 80%">
                                                                <asp:Label ID="UserRightsMatrix_individualRoleNameLabel" runat="server" Width="90%"></asp:Label>
                                                                <div style="display: none">
                                                                    <asp:Button ID="UserRightsMatrix_hiddenButton" runat="server" OnClick="UserRightsMatrix_hiddenButton_Click" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="UserRightsMatrix_showDetailButton" runat="server" Text="Show Details"
                                                        SkinID="sknButtonId" OnClick="UserRightsMatrix_showDetailButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="UserRightsMatrix_assignRolesGridViewDIV" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="header_bg" id="UserRightsMatrix_assignRights_expandTD" runat="server">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="header_text_bold">
                                                        <asp:Label ID="UserRightsMatrix_assignRights_Label" runat="server" Text="Assign Rights"></asp:Label>
                                                        &nbsp;<asp:Label ID="UserRightsMatrix_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 2%" align="right">
                                                        <span id="UserRightsMatrix_assignRolesUpSpan" runat="server" style="display: none;">
                                                            <asp:Image ID="UserRightsMatrix_assignRolesUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                id="UserRightsMatrix_assignRolesDownSpan" runat="server" style="display: block;"><asp:Image
                                                                    ID="UserRightsMatrix_assignRolesDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                        <asp:HiddenField ID="UserRightsMatrix_restoreHiddenField" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="UserRightsMatrix_assignRolesUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <div id="UserRightsMatrix_assignRightsDiv" runat="server" style="height: 250px; overflow: auto;">
                                                        <asp:GridView ID="UserRightsMatrix_assignRolesGridView" runat="server" SkinID="sknNewNonSortingGridView"
                                                            AutoGenerateColumns="false" OnRowCreated="UserRightsMatrix_assignRolesGridView_RowCreated"
                                                            OnSorting="UserRightsMatrix_assignRolesGridView_Sorting" OnRowDataBound="UserRightsMatrix_assignRolesGridView_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Sub Module Name" ItemStyle-Width="20%" HeaderStyle-Width="20%"
                                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="UserRightsMatrix_assignRolesGridView_subModuleNameLabel" runat="server"
                                                                            Text='<%# Eval("SubModuleName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feature Name" ItemStyle-Width="22%" HeaderStyle-Width="22%"
                                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="UserRightsMatrix_assignRolesGridView_featureNameLabel" runat="server"
                                                                            Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="UserRightsMatrix_assignRolesGridView_modIDHiddenField" runat="server"
                                                                            Value='<%# Eval("ModuleID") %>' />
                                                                        <asp:HiddenField ID="UserRightsMatrix_assignRolesGridView_featureIDHiddenField" runat="server"
                                                                            Value='<%# Eval("FeatureID") %>' />
                                                                        <asp:HiddenField ID="UserRightsMatrix_assignRolesGridView_subModuleIDHiddenField"
                                                                            runat="server" Value='<%# Eval("SubModuleID") %>' />
                                                                        <asp:HiddenField ID="UserRightsMatrix_assignRolesGridView_userIDHiddenField" runat="server"
                                                                            Value='<%# Eval("UserRightsID") %>' />
                                                                        <%--                <asp:HiddenField ID="UserRightsMatrix_assignRolesGridView_rolIDHiddenField" runat="server"
                                                                            Value='<%# Eval("RoleID") %>' />--%>
                                                                        <asp:HiddenField ID="UserRightsMatrix_assignRolesGridView_isActiveHiddenField" runat="server"
                                                                            Value='<%# Eval("IsActive") %>' />
                                                                        <asp:HiddenField ID="UserRightsMatrix_assignRolesGridView_usrIDHiddenField" runat="server"
                                                                            Value='<%# Eval("UserID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feature URL" ItemStyle-Width="40%" HeaderStyle-Width="40%"
                                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>  
                                                                        <asp:Label ID="UserRightsMatrix_assignRolesGridView_featurURLLabel" runat="server"
                                                                            Text='<%# Eval("FeatureURL") %>'></asp:Label>
                                                                     </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Allow" HeaderStyle-Width="4%" ControlStyle-Width="4%"
                                                                    HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="UserRightsMatrix_assignRolesGridView_allowCheckBox" runat="server"
                                                                            Checked='<%# Eval("IsApplicable") %>' AutoPostBack="true" OnCheckedChanged="checkBox_CheckedChanged" />
                                                                        <ajaxToolKit:ToggleButtonExtender ID="UserRightsMatrix_assignRolesGridView_allowCheckBoxToggleButton"
                                                                            runat="server" TargetControlID="UserRightsMatrix_assignRolesGridView_allowCheckBox"
                                                                            ImageHeight="25" ImageWidth="25" CheckedImageUrl="~/App_Themes/DefaultTheme/Images/correct_answer.png"
                                                                            UncheckedImageUrl="~/App_Themes/DefaultTheme/Images/wrong_answer.png" CheckedImageOverAlternateText="Assign Rights"
                                                                            UncheckedImageOverAlternateText="Unassign Rights">
                                                                        </ajaxToolKit:ToggleButtonExtender>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UserRightMatrix_pageNavigatorUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <uc1:PageNavigator ID="UserRightMatrix_pageNavigator" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_2">
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="UserRightsMatrix_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="UserRightsMatrix_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="UserRightsMatrix_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 52%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="UserRightsMatrix_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="UserRightsMatrix_saveButton_Click" Visible="false" />
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="UserRightsMatrix_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="UserRightsMatrix_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="UserRightsMatrix_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
