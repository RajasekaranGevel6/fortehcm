<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CandidateSelfSkill.aspx.cs"
    MasterPageFile="~/MasterPages/SiteAdminMaster.Master" Inherits="Forte.HCM.UI.Authentication.CandidateSelfSkill" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc2" %>
<asp:Content ID="CandidateSelfSkill_bodyContent" runat="server" ContentPlaceHolderID="SiteAdminMaster_body">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="CandidateSelfSkill_manageSkillsLiteral" runat="server" Text="Manage Skills"></asp:Literal>
                            <asp:HiddenField ID="CandidateSelfSkill_browserHiddenField" runat="server" />
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 42%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 20%">
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="CandidateSelfSkill_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CandidateSelfSkill_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="CandidateSelfSkill_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CandidateSelfSkill_messageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CandidateSelfSkill_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateSelfSkill_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CandidateSelfSkill_updatePanel" runat="server">
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr id="CandidateSelfSkill_expandTR" runat="server">
                                            <td class="header_bg">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="CandidateSelfSkill_searchResultHeader" runat="server" Text="New Skills"></asp:Literal>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <asp:HiddenField ID="CandidateSelfSkill_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <div style="display:none">
                                                    <asp:Button ID="CandidateSelfSkill_refreshButton" runat="server" 
                                                        onclick="CandidateSelfSkill_refreshButton_Click" />
                                                </div>
                                                <div id="CandidateSelfSkill_Div" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <div id="CandidateSelfSkill_resultDiv" runat="server" style="height: 190px; overflow: auto;">
                                                                    <asp:GridView ID="CandidateSelfSkill_resultGridView" Width="100%" runat="server"
                                                                        AllowSorting="true" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                        AutoGenerateColumns="false"
                                                                        OnRowDataBound="CandidateSelfSkill_resultGridView_RowDataBound">
                                                                        <RowStyle CssClass="grid_alternate_row" />
                                                                        <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                        <HeaderStyle CssClass="grid_header_row" />
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="CandidateSelfSkill_approveCandidateSelfSkillImageButton" runat="server"
                                                                                        SkinID="sknProcessCreditRequest" ToolTip="Approve/Reject Skill" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Skill">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CandidateSelfSkill_resultGridView_CandidateSkillLabel" runat="server"
                                                                                        Text='<%# Eval("SkillDetail") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Candidate Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CandidateSelfSkill_resultGridView_CandidateNameLabel" runat="server"
                                                                                        Text='<%# Eval("caiFirstName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Candidate Email">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CandidateSelfSkill_resultGridView_CandidateEmailLabel" runat="server"
                                                                                        Text='<%# Eval("caiEmail") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                     <asp:HiddenField ID="CandidateSelfSkill_candidateIdHiddenField" runat="server"
                                                                                        Value='<%#Eval("caiID") %>' />
                                                                                    <asp:HiddenField ID="CandidateSelfSkill_skillIdHiddenField" runat="server"
                                                                                        Value='<%#Eval("SkillID") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="CandidateSelfSkill_questionPanel" runat="server" Style="display: none"
                                                                    CssClass="popupcontrol_question_detail">
                                                                    <div style="display: none">
                                                                        <asp:Button ID="CandidateSelfSkill_questionHiddenButton" runat="server" Text="Hidden" /></div>
                                                                    <uc2:QuestionDetailPreviewControl ID="CandidateSelfSkill_questionDetailPreviewControl"
                                                                        runat="server" />
                                                                </asp:Panel>
                                                                <ajaxToolKit:ModalPopupExtender ID="CandidateSelfSkill_questionModalPopupExtender"
                                                                    runat="server" PopupControlID="CandidateSelfSkill_questionPanel" TargetControlID="CandidateSelfSkill_questionHiddenButton"
                                                                    BackgroundCssClass="modalBackground">
                                                                </ajaxToolKit:ModalPopupExtender>
                                                            </td>
                                                        </tr>
                                                        <asp:HiddenField ID="CandidateSelfSkill_isMaximizedHiddenField" runat="server" />
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <%--<asp:AsyncPostBackTrigger ControlID="CandidateSelfSkill_searchButton" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CandidateSelfSkill_bottomMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CandidateSelfSkill_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateSelfSkill_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table align="right" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td width="72%">
                        </td>
                        <td width="28%">
                            <table align="right" cellpadding="2" cellspacing="4" border="0">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="CandidateSelfSkill_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CandidateSelfSkill_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CandidateSelfSkill_bottomCancelLinkButton" Text="Cancel" runat="server"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
