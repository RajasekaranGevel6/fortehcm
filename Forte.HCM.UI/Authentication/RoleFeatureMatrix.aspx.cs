﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RoleFeatureMatrix.aspx.cs
// File that represents the role feature matrix class that defines the user 
// interface layout and functionalities for the role feature matrix page. This 
// page helps in managing the feature role.

#endregion Header

#region Directives
using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;


using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Authentication
{
    public partial class RoleFeatureMatrix : PageBase
    {

        #region Private Members

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "325px";

        #endregion Private Members

        #region Event Handlers
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance
        /// containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                RoleFeatureMatrix_topSuccessMessageLabel.Text = string.Empty;
                RoleFeatureMatrix_topErrorMessageLabel.Text = string.Empty;
                RoleFeatureMatrix_bottomErrorMessageLabel.Text = string.Empty;
                RoleFeatureMatrix_bottomSuccessMessageLabel.Text = string.Empty;

                Master.SetPageCaption("Assign Role Rights");
                Page.Form.DefaultButton = RoleFeatureMatrix_showDetailsButton.UniqueID;
                Page.SetFocus(RoleFeatureMatrix_moduleDropDownList.ClientID);

                if (!IsPostBack)
                {
                    //Assign the datasource for the modules check box list
                    RoleFeatureMatrix_moduleDropDownList.DataSource =
                        new AuthenticationBLManager().GetModuleList();

                    RoleFeatureMatrix_moduleDropDownList.DataBind();
                    RoleFeatureMatrix_moduleDropDownList.Items.Insert(0,
                   new ListItem("--Select--", "0"));


                    RoleFeatureMatrix_roleDropDownList.DataSource =
                        new AuthenticationBLManager().GetRoles();
                    RoleFeatureMatrix_roleDropDownList.DataBind();
                    RoleFeatureMatrix_roleDropDownList.Items.Insert(0,
                  new ListItem("--Select--", "0"));
                }

                RoleFeatureMatrix_assignRightsUpSpan.Attributes.Add("onclick",
               "ExpandOrRestore('" +
               RoleFeatureMatrix_matrixRightDIV.ClientID + "','" +
               RoleFeatureMatrix_searchHeaderdiv.ClientID + "','" +
               RoleFeatureMatrix_assignRightsUpSpan.ClientID + "','" +
               RoleFeatureMatrix_assignRightsDownSpan.ClientID + "','" +
               RoleFeatureMatrix_restoreHiddenField.ClientID + "','" +
               RESTORED_HEIGHT + "','" +
               EXPANDED_HEIGHT + "')");
                RoleFeatureMatrix_assignRightsDownSpan.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    RoleFeatureMatrix_matrixRightDIV.ClientID + "','" +
                    RoleFeatureMatrix_searchHeaderdiv.ClientID + "','" +
                    RoleFeatureMatrix_assignRightsUpSpan.ClientID + "','" +
                    RoleFeatureMatrix_assignRightsDownSpan.ClientID + "','" +
                    RoleFeatureMatrix_restoreHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exception)
            {
                ShowMessage(RoleFeatureMatrix_topErrorMessageLabel,
                    RoleFeatureMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }

        }


        /// <summary>
        /// Handles the Click event of the RoleFeatureMatrix_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void RoleFeatureMatrix_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (RoleFeatureMatrix_matrixGridView.Rows.Count == 0)
                {
                    ShowMessage(RoleFeatureMatrix_bottomErrorMessageLabel,
                        RoleFeatureMatrix_topErrorMessageLabel,
                        Resources.HCMResource.RoleFeatureMatrix_PleaseSelecteARole);
                    return;
                }

                SaveFeatureRights();

                RoleFeatureMatrix_matrixGridView.DataSource = new AuthenticationBLManager().GetFeatureRightsDetails
               (int.Parse(RoleFeatureMatrix_moduleDropDownList.SelectedValue),
               int.Parse(RoleFeatureMatrix_subModuleDropDownList.SelectedValue),
               int.Parse(RoleFeatureMatrix_roleDropDownList.SelectedValue));

                //assign the column name as the role drop down selected text 
                RoleFeatureMatrix_matrixGridView.Columns[3].HeaderText =
                    RoleFeatureMatrix_roleDropDownList.SelectedItem.Text;
                RoleFeatureMatrix_matrixGridView.DataBind();
            }
            catch (Exception exception)
            {
                ShowMessage(RoleFeatureMatrix_topErrorMessageLabel,
                    RoleFeatureMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }


        }

        /// <summary>
        /// Handles the Click event of the RoleFeatureMatrix_showDetailsButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void RoleFeatureMatrix_showDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exception)
            {
                ShowMessage(RoleFeatureMatrix_topErrorMessageLabel,
                    RoleFeatureMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }

        }

        /// <summary>
        /// Handles the Click event of the RoleFeatureMatrix_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void RoleFeatureMatrix_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);

            }
            catch (Exception exception)
            {
                ShowMessage(RoleFeatureMatrix_topErrorMessageLabel,
                    RoleFeatureMatrix_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }
        #endregion Event Handlers

        #region Private Method

        /// <summary>
        /// Saves the feature rights.
        /// </summary>
        private void SaveFeatureRights()
        {
            List<RoleRightMatrixDetail> fearueRightMatrix = new List<RoleRightMatrixDetail>();

            HiddenField modIDHiddenField = null;

            HiddenField submodIDHiddenField = null;

            HiddenField feaIDHiddenField = null;

            HiddenField roleRightIDHiddenField = null;

            CheckBox isApplicableCheckBox = null;

            RoleRightMatrixDetail roleRightDeatail = null;

            foreach (GridViewRow row in RoleFeatureMatrix_matrixGridView.Rows)
            {

                modIDHiddenField = (HiddenField)row.FindControl(
                    "RoleFeatureMatrix_matrixGridView_modIDHiddenField");

                submodIDHiddenField = (HiddenField)row.FindControl(
                   "RoleFeatureMatrix_matrixGridView_submodIDHiddenField");

                feaIDHiddenField = (HiddenField)row.FindControl(
                   "RoleFeatureMatrix_matrixGridView_featureIDHiddenField");

                roleRightIDHiddenField = (HiddenField)row.FindControl(
                   "RoleFeatureMatrix_matrixGridView_roleRightsIDHiddenField");

                isApplicableCheckBox = (CheckBox)row.FindControl(
                    "RoleFeatureMatrix_matrixGridView_checkedCheckBox");

                if (modIDHiddenField == null ||
                    submodIDHiddenField == null ||
                    feaIDHiddenField == null ||
                    roleRightIDHiddenField == null)
                {

                    continue;
                }

                roleRightDeatail = new RoleRightMatrixDetail();

                if (!Utility.IsNullOrEmpty(modIDHiddenField))
                {
                    roleRightDeatail.ModuleID = int.Parse(modIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(submodIDHiddenField))
                {
                    roleRightDeatail.SubModuleID = int.Parse(submodIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(feaIDHiddenField))
                {
                    roleRightDeatail.FeatureID = int.Parse(feaIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(roleRightIDHiddenField))
                {
                    roleRightDeatail.RoleRightID = int.Parse(roleRightIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(isApplicableCheckBox))
                {
                    roleRightDeatail.IsApplicable = isApplicableCheckBox.Checked;
                }

                roleRightDeatail.RoleID = int.Parse(RoleFeatureMatrix_roleDropDownList.SelectedValue);

                roleRightDeatail.CreatedBy = base.userID;


                fearueRightMatrix.Add(roleRightDeatail);
            }

            new AuthenticationBLManager().SaveFeatureRightsDetails(fearueRightMatrix);

            ShowMessage(RoleFeatureMatrix_bottomSuccessMessageLabel, RoleFeatureMatrix_topSuccessMessageLabel,
                Resources.HCMResource.RoleFeatureMatrix_MatrixDetailsUpdated);
        }
        #endregion Private Method

        #region Protected Override Methods
        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            if (RoleFeatureMatrix_moduleDropDownList.SelectedIndex == 0 ||
               RoleFeatureMatrix_subModuleDropDownList.SelectedItem.Value == "" ||
            RoleFeatureMatrix_roleDropDownList.SelectedIndex == 0)
            {
                ShowMessage(RoleFeatureMatrix_bottomErrorMessageLabel,
                    RoleFeatureMatrix_topErrorMessageLabel,
                    Resources.HCMResource.RoleFeatureMatrix_PleaseSelectAllValues);

                RoleFeatureMatrix_matrixGridView.DataSource = null;
                RoleFeatureMatrix_matrixGridView.DataBind();

                return;
            }

            RoleFeatureMatrix_matrixGridView.DataSource = new AuthenticationBLManager().GetFeatureRightsDetails
                (int.Parse(RoleFeatureMatrix_moduleDropDownList.SelectedValue),
                int.Parse(RoleFeatureMatrix_subModuleDropDownList.SelectedValue),
                int.Parse(RoleFeatureMatrix_roleDropDownList.SelectedValue));

            //assign the column name as the role drop down selected text 
            RoleFeatureMatrix_matrixGridView.Columns[3].HeaderText =
                RoleFeatureMatrix_roleDropDownList.SelectedItem.Text;
            RoleFeatureMatrix_matrixGridView.DataBind();
        }
        #endregion Protected Override Methods
    }
}