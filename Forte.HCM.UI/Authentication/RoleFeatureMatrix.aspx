﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    CodeBehind="RoleFeatureMatrix.aspx.cs" Inherits="Forte.HCM.UI.Authentication.RoleFeatureMatrix" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="RoleRightsMatrixContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <asp:UpdatePanel ID="RoleFeatureMatrix_headerUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Literal ID="RoleFeatureMatrix_headerLiteral" runat="server" Text="Assign Role Rights"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="RoleFeatureMatrix_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                    OnClick="RoleFeatureMatrix_saveButton_Click" />
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="RoleFeatureMatrix_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="RoleFeatureMatrix_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="RoleFeatureMatrix_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="RoleFeatureMatrix_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="RoleFeatureMatrix_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="RoleFeatureMatrix_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="RoleFeatureMatrix_searchHeaderdiv" runat="server">
                                <asp:UpdatePanel ID="RoleFeatureMatrix_searchHeaderUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="panel_inner_body_bg">
                                                        <tr>
                                                            <td style="width: 7%">
                                                                <asp:Label ID="RoleFeatureMatrix_moduleLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Module"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 22%">
                                                                <asp:DropDownList ID="RoleFeatureMatrix_moduleDropDownList" runat="server" Width="70%"
                                                                    SkinID="sknSubjectDropDown" DataValueField="ModuleID" DataTextField="ModuleName">
                                                                </asp:DropDownList>
                                                                <asp:ImageButton ID="RoleFeatureMatrix_moduleHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the module here" />
                                                            </td>
                                                            <td style="width: 9%">
                                                                <asp:Label ID="RoleFeatureMatrix_subModuleLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Sub Module"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 18%">
                                                                <asp:DropDownList ID="RoleFeatureMatrix_subModuleDropDownList" runat="server" Width="70%"
                                                                    SkinID="sknSubjectDropDown">
                                                                </asp:DropDownList>
                                                                <asp:ImageButton ID="RoleFeatureMatrix_subModuleHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the sub module here" />
                                                                <ajaxToolKit:CascadingDropDown ID="RoleFeatureMatrix_subModule_cdd" runat="server"
                                                                    TargetControlID="RoleFeatureMatrix_subModuleDropDownList" Category="SubModule"
                                                                    ServicePath="~/Autocomplete.asmx" LoadingText="Loading Submodules.." PromptText="--Select--"
                                                                    PromptValue="0" ServiceMethod="GetSubModuleByModuleID" ParentControlID="RoleFeatureMatrix_moduleDropDownList">
                                                                </ajaxToolKit:CascadingDropDown>
                                                            </td>
                                                            <td style="width: 5%">
                                                                <asp:Label ID="RoleFeatureMatrix_roleLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Role"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:DropDownList ID="RoleFeatureMatrix_roleDropDownList" runat="server" Width="70%"
                                                                    DataTextField="RoleName" DataValueField="RoleID" SkinID="sknSubjectDropDown">
                                                                </asp:DropDownList>
                                                                <asp:ImageButton ID="RoleFeatureMatrix_roleHelpImageButton1" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the role here" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="RoleFeatureMatrix_showDetailsButton" runat="server" Text="Show Details"
                                                        SkinID="sknButtonId" OnClick="RoleFeatureMatrix_showDetailsButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    <asp:Literal ID="RoleFeatureMatrix_assignRightsLiteral" runat="server" Text="Assign Rights"></asp:Literal>
                                                </td>
                                                <td style="width: 2%" align="right">
                                                    <span id="RoleFeatureMatrix_assignRightsUpSpan" runat="server" style="display: none;">
                                                        <asp:Image ID="RoleFeatureMatrix_assignRightsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                            id="RoleFeatureMatrix_assignRightsDownSpan" runat="server" style="display: block;"><asp:Image
                                                                ID="RoleFeatureMatrix_assignRightsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                    <asp:HiddenField ID="RoleFeatureMatrix_restoreHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <div id="RoleFeatureMatrix_matrixRightDIV" runat="server" style="height: 300px; overflow: auto;">
                                                        <asp:UpdatePanel ID="RoleFeatureMatrix_matrixUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="RoleFeatureMatrix_matrixGridView" runat="server" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Module Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="RoleFeatureMatrix_matrixGridView_moduleNameLabel" runat="server" Text='<%# Eval("ModuleName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Sub Module Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="RoleFeatureMatrix_matrixGridView_subModuleNameLabel" runat="server"
                                                                                    Text='<%# Eval("SubModuleName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Feature Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="RoleFeatureMatrix_matrixGridView_featureNameLabel" runat="server"
                                                                                    Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Role Name">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="RoleFeatureMatrix_matrixGridView_checkedCheckBox" runat="server"
                                                                                    Checked='<%# Eval("IsApplicable") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="RoleFeatureMatrix_matrixGridView_modIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("ModuleID") %>' />
                                                                                <asp:HiddenField ID="RoleFeatureMatrix_matrixGridView_submodIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("SubModuleID") %>' />
                                                                                <asp:HiddenField ID="RoleFeatureMatrix_matrixGridView_featureIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("FeatureID") %>' />
                                                                                <asp:HiddenField ID="RoleFeatureMatrix_matrixGridView_roleRightsIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("RoleRightID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="RoleFeatureMatrix_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="RoleFeatureMatrix_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="RoleFeatureMatrix_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 52%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="RoleFeatureMatrix_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="RoleFeatureMatrix_saveButton_Click" />
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="RoleFeatureMatrix_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="RoleFeatureMatrix_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="RoleFeatureMatrix_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
