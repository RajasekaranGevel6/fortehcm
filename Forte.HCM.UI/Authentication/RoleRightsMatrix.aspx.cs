﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EnrollCustomer.aspx.cs
// File that represents the RoleRightsMatrix class that defines the user 
// interface layout and functionalities for the role rights matrix page. This 
// page helps in managing the Role Rights Matrix. 
 
#endregion Header

#region Directives
using System;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Authentication
{
    public partial class RoleRightsMatrix : PageBase
    {
        #region Private Members

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";


        private int GRIDVIEW_COLUMN = 0;

        #endregion Private Members

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Role Rights Matrix");

            if (!IsPostBack)
            {
                AssignDefaultValue();

                //RoleRightsMatrix_assignRolesDIV.Style["display"] = "none";
            }

            Page.Form.DefaultButton = RoleRightsMatrix_showButton.UniqueID;
            // Page.Form.DefaultFocus = RoleRightsMatrix_showButton.UniqueID;
            Page.SetFocus(RoleRightsMatrix_showButton.ClientID);

            RoleRightsMatrix_topErrorMessageLabel.Text = string.Empty;
            RoleRightsMatrix_bottomErrorMessageLabel.Text = string.Empty;
            RoleRightsMatrix_topSuccessMessageLabel.Text = string.Empty;
            RoleRightsMatrix_bottomSuccessMessageLabel.Text = string.Empty;

            if (!Utility.IsNullOrEmpty(RoleRightsMatrix_checkedStatusHiddenField.Value))
            {
                ChangeCheckedStatus();

                //LoadValues();
            }
            //RoleRightsMatrix_moduleComboTextBox.Text = "";

            //if (RoleRightsMatrix_moduleIDHiddenField.Value != "")
            //{
            //    RoleRightsMatrix_moduleComboTextBox.Text = RoleRightsMatrix_moduleIDHiddenField.Value.Trim(',', ' ');
            //}


            //RoleRightsMatrix_moduleCheckBoxList.Attributes.Add("onclick", "javascript:return selectedModuleId();");

            CheckAndSetExpandOrRestore();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the RoleRightsMatrix_roleDropDownList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void RoleRightsMatrix_roleDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the RoleRightsMatrix_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void RoleRightsMatrix_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //LoadValues();

                if (Utility.IsNullOrEmpty(ViewState["RoleRightsMatrix"]))
                {
                    base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                  RoleRightsMatrix_bottomErrorMessageLabel, "Please select and assign a right");
                    return;
                }

                List<RoleRightMatrixDetail> roleMatrixDetails = new List<RoleRightMatrixDetail>();

                //Keep the list of role rights matrix in viewstate
                if (!Utility.IsNullOrEmpty(ViewState["RoleRightsMatrix"]))
                {
                    roleMatrixDetails = ViewState["RoleRightsMatrix"] as List<RoleRightMatrixDetail>;
                }

                DataTable datatable = ViewState["RoleRightsMatrixDataTable"] as DataTable;

                for (int j = Support.Constants.RoleRightMatrixConstants.STARTING_COLUMN_ID; j < datatable.Columns.Count; j++)
                {
                    for (int i = 0; i < datatable.Rows.Count; i++)
                    {
                        if (RoleRightsMatrix_rolesGridView.Rows[i].Cells[j].Controls[0].Visible)
                        {
                            CheckBox chekc = ((CheckBox)(RoleRightsMatrix_rolesGridView.Rows[i].Cells[j].Controls[0]));

                            datatable.Rows[i][j] = chekc.Checked == true ? 1 : 0;
                        }
                    }
                }

                new AuthenticationBLManager().UpdateRoleMatrixDetails(datatable);

                //new AuthenticationBLManager().UpdateRoleMatrixDetails(roleMatrixDetails);

                base.ShowMessage(RoleRightsMatrix_topSuccessMessageLabel,
                    RoleRightsMatrix_bottomSuccessMessageLabel, Resources.HCMResource.RoleRightsMatrix_UpdatedSuccessfully
                    );

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the RowDataBound event of the RoleRightsMatrix_rolesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void RoleRightsMatrix_rolesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //make the header as visible false
                    e.Row.Cells[Support.Constants.RoleRightMatrixConstants.FEATURE_ID_COLUMN].Visible = false;
                }
                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    return;
                }

                e.Row.Cells[0].Width = Unit.Pixel(150);
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;

                int rowID = 0;

                DataTable dt = null;

                if (ViewState["RoleRightsMatrixDataTable"] != null)
                {
                    dt = ViewState["RoleRightsMatrixDataTable"] as DataTable;

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["FeatureID"].ToString() == e.Row.Cells[2].Text)
                        {
                            break;
                        }
                        rowID++;
                    }
                }
                //Start from the second column of the grid view . 
                for (int i = Support.Constants.RoleRightMatrixConstants.STARTING_COLUMN_ID; i < GRIDVIEW_COLUMN; i++)
                {
                    e.Row.Cells[i].Width = Unit.Pixel(100);
                    e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;

                    CheckBox chekc = ((CheckBox)(e.Row.Cells[i].Controls[0]));

                    //Made visible false for the feature id column
                    e.Row.Cells[Support.Constants.RoleRightMatrixConstants.FEATURE_ID_COLUMN].Visible = false;

                    //Label label = (Label)(e.Row.Cells[0].Controls[0]);

                    //chekc.Text = i + "-" + e.Row.Cells[0].Text;

                    chekc.Enabled = true;

                    if (dt.Rows[rowID][i].ToString() == "")
                    {
                        chekc.Visible = false;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the SelectedIndexChanged event of the RoleRightsMatrix_moduleCheckBoxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void RoleRightsMatrix_moduleCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectModule();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }




        /// <summary>
        /// Handles the SelectedIndexChanged event of the RoleRightsMatrix_roleCheckBoxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void RoleRightsMatrix_roleCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectRole();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }



        /// <summary>
        /// Handles the Click event of the RoleRightsMatrix_showButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void RoleRightsMatrix_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable datatable;

                if (RoleRightsMatrix_roleTextBox.Text == "" || RoleRightsMatrix_moduleComboTextBox.Text == "")
                {
                    base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                        RoleRightsMatrix_bottomErrorMessageLabel,
                        Resources.HCMResource.RoleRightsMatrix_EmptyModuleAndRoleText);

                    datatable = new DataTable();
                    RoleRightsMatrix_rolesGridView.DataSource = datatable;
                    RoleRightsMatrix_rolesGridView.DataBind();

                    return;
                }

                List<RoleRightMatrixDetail> roleRightsMatrix = new List<RoleRightMatrixDetail>();

                //roleRightsMatrix =
                datatable = new AuthenticationBLManager().GetRoleRightsMatrixDataTable(RoleRightsMatrix_roleHiddenField.Value.Trim(), RoleRightsMatrix_moduleIDHiddenField.Value.Trim());


                if (datatable.Rows.Count == 0)
                {
                    base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                      RoleRightsMatrix_bottomErrorMessageLabel,
                      Resources.HCMResource.Common_Empty_Grid);
                    //RoleRightsMatrix_assignRolesDIV.Style["display"] = "none";
                    return;
                }

                //RoleRightsMatrix_assignRolesDIV.Style["display"] = "block";

                GRIDVIEW_COLUMN = datatable.Columns.Count;
                ViewState["RoleRightsMatrixDataTable"] = datatable;

                RoleRightsMatrix_rolesGridView.DataSource = datatable;
                RoleRightsMatrix_rolesGridView.DataBind();


                ViewState["RoleRightsMatrix"] = roleRightsMatrix;

                //LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the Click event of the RoleRightsMatrix_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void RoleRightsMatrix_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the Click event of the RoleRightsMatrix_moduleSelectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void RoleRightsMatrix_moduleSelectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < RoleRightsMatrix_moduleCheckBoxList.Items.Count; i++)
                {
                    RoleRightsMatrix_moduleCheckBoxList.Items[i].Selected = true;
                }
                SelectModule();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the Click event of the RoleRightsMatrix_moduleClearAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void RoleRightsMatrix_moduleClearAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < RoleRightsMatrix_moduleCheckBoxList.Items.Count; i++)
                {
                    RoleRightsMatrix_moduleCheckBoxList.Items[i].Selected = false;
                }

                SelectModule();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the Click event of the RoleRightsMatrix_roleSelectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void RoleRightsMatrix_roleSelectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < RoleRightsMatrix_roleCheckBoxList.Items.Count; i++)
                {
                    RoleRightsMatrix_roleCheckBoxList.Items[i].Selected = true;
                }

                SelectRole();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the Click event of the RoleRightsMatrix_roleClearAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void RoleRightsMatrix_roleClearAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < RoleRightsMatrix_roleCheckBoxList.Items.Count; i++)
                {
                    RoleRightsMatrix_roleCheckBoxList.Items[i].Selected = false;
                }
                SelectRole();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleRightsMatrix_topErrorMessageLabel,
                    RoleRightsMatrix_bottomErrorMessageLabel, exception.Message);
            }

        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Selects the role.
        /// </summary>
        private void SelectRole()
        {
            RoleRightsMatrix_roleHiddenField.Value = string.Empty;

            string selectedRoleName = "";

            for (int i = 0; i < RoleRightsMatrix_roleCheckBoxList.Items.Count; i++)
            {
                if (RoleRightsMatrix_roleCheckBoxList.Items[i].Selected)
                {
                    RoleRightsMatrix_roleHiddenField.Value += ", " + RoleRightsMatrix_roleCheckBoxList.Items[i].Value;

                    selectedRoleName += ", " + RoleRightsMatrix_roleCheckBoxList.Items[i].Text;
                }
            }

            if (selectedRoleName != null || selectedRoleName.Length != 0)
            {
                selectedRoleName = selectedRoleName.TrimStart(',', ' ');
                RoleRightsMatrix_roleHiddenField.Value = RoleRightsMatrix_roleHiddenField.Value.TrimStart(',', ' ');
            }

            RoleRightsMatrix_roleTextBox.Text = selectedRoleName;
        }


        /// <summary>
        /// Selects the module.
        /// </summary>
        private void SelectModule()
        {
            RoleRightsMatrix_moduleIDHiddenField.Value = string.Empty;

            string selectedModuleName = "";

            for (int i = 0; i < RoleRightsMatrix_moduleCheckBoxList.Items.Count; i++)
            {
                if (RoleRightsMatrix_moduleCheckBoxList.Items[i].Selected)
                {
                    RoleRightsMatrix_moduleIDHiddenField.Value += ", " + RoleRightsMatrix_moduleCheckBoxList.Items[i].Value;

                    selectedModuleName += ", " + RoleRightsMatrix_moduleCheckBoxList.Items[i].Text;
                }
            }

            if (selectedModuleName != null || selectedModuleName.Length != 0)
            {
                selectedModuleName = selectedModuleName.TrimStart(',', ' ');
                RoleRightsMatrix_moduleIDHiddenField.Value = RoleRightsMatrix_moduleIDHiddenField.Value.TrimStart(',', ' ');
            }

            RoleRightsMatrix_moduleComboTextBox.Text = selectedModuleName;

        }


        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(RoleRightsMatrix_restoreHiddenField.Value) &&
                 RoleRightsMatrix_restoreHiddenField.Value == "Y")
            {
                RoleRightsMatrix_selectRoleDIV.Style["display"] = "none";
                RoleRightsMatrix_assignRolesUpSpan.Style["display"] = "block";
                RoleRightsMatrix_assignRolesDownSpan.Style["display"] = "none";
                RoleRightsMatrix_assignRightDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                RoleRightsMatrix_selectRoleDIV.Style["display"] = "block";
                RoleRightsMatrix_assignRolesUpSpan.Style["display"] = "none";
                RoleRightsMatrix_assignRolesDownSpan.Style["display"] = "block";
                RoleRightsMatrix_assignRightDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Changes the checked status.
        /// </summary>
        private void ChangeCheckedStatus()
        {
            if (!Utility.IsNullOrEmpty(ViewState["RoleRightsMatrix"]))
            {
                List<RoleRightMatrixDetail> roleRightMatrix = ViewState["RoleRightsMatrix"]
                                                        as List<RoleRightMatrixDetail>;
                string[] splittedIDs = RoleRightsMatrix_checkedStatusHiddenField.Value.Split(',');

                foreach (string id in splittedIDs)
                {
                    if (id == "")
                        return;

                    int count = GetNoOfOccurences(splittedIDs, id);

                    int index = roleRightMatrix.FindIndex(delegate(RoleRightMatrixDetail role)
                    {
                        return role.RoleRightID == int.Parse(id);
                    });

                    int value = count % 2;

                    if (value == 1)
                    {
                        roleRightMatrix[index].IsApplicable = true ? false : true;
                    }
                }

                ViewState["RoleRightsMatrix"] = roleRightMatrix;
            }

            RoleRightsMatrix_checkedStatusHiddenField.Value = string.Empty;
        }

        /// <summary>
        /// Gets the no of occurences.
        /// </summary>
        /// <param name="splittedIDs">The splitted I ds.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        private int GetNoOfOccurences(string[] splittedIDs, string id)
        {
            int count = 0;

            foreach (string ids in splittedIDs)
            {
                if (ids == id)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Assigns the default value.
        /// </summary>
        private void AssignDefaultValue()
        {
            RoleRightsMatrix_assignRolesUpSpan.Attributes.Add("onclick",
            "ExpandOrRestore('" +
            RoleRightsMatrix_assignRightDIV.ClientID + "','" +
            RoleRightsMatrix_selectRoleDIV.ClientID + "','" +
            RoleRightsMatrix_assignRolesUpSpan.ClientID + "','" +
            RoleRightsMatrix_assignRolesDownSpan.ClientID + "','" +
            RoleRightsMatrix_restoreHiddenField.ClientID + "','" +
            RESTORED_HEIGHT + "','" +
            EXPANDED_HEIGHT + "')");
            RoleRightsMatrix_assignRolesDownSpan.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                RoleRightsMatrix_assignRightDIV.ClientID + "','" +
                RoleRightsMatrix_selectRoleDIV.ClientID + "','" +
                RoleRightsMatrix_assignRolesUpSpan.ClientID + "','" +
                RoleRightsMatrix_assignRolesDownSpan.ClientID + "','" +
                RoleRightsMatrix_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            //Assign the values for the modules check box list
            RoleRightsMatrix_moduleCheckBoxList.DataSource = new AuthenticationBLManager().GetModuleList();
            RoleRightsMatrix_moduleCheckBoxList.DataBind();


            //Assign the values for the roles check box list
            RoleRightsMatrix_roleCheckBoxList.DataSource = new AuthenticationBLManager().GetRoles();
            RoleRightsMatrix_roleCheckBoxList.DataBind();

        }
        #endregion Private Methods

        #region Protected Override Methods
        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        { }
        #endregion

        #region USING LIST OF OBJECTS
        //protected override void LoadValues()
        //{

        //    //Initiate the list of matrix details
        //    List<RoleRightMatrixDetail> roleRightsMatrix = new List<RoleRightMatrixDetail>();

        //    RoleRightsMatrix_rolesGridView.DataSource = null;
        //    RoleRightsMatrix_rolesGridView.DataBind();




        //    //Keep the list of role rights matrix in viewstate
        //    if (!Utility.IsNullOrEmpty(ViewState["RoleRightsMatrix"]))
        //    {
        //        roleRightsMatrix = ViewState["RoleRightsMatrix"] as List<RoleRightMatrixDetail>;
        //    }

        //    //Get the list of feature name
        //    var FeatureName = (from roleRight in roleRightsMatrix
        //                       select new { roleRight.FeatureID, roleRight.FeatureName }).Distinct().ToList();

        //    //Assign the feature name list as datasource to the grid
        //    RoleRightsMatrix_rolesGridView.DataSource = FeatureName;
        //    RoleRightsMatrix_rolesGridView.DataBind();

        //    BoundField boundField = null;

        //    //Get the distinct roles name form the list
        //    var rolesName = (from right in roleRightsMatrix
        //                     select new { right.RoleName, right.RoleID }).Distinct().ToList();
        //    List<int> colIds = new List<int>();

        //    int count = RoleRightsMatrix_rolesGridView.Columns.Count;

        //    //foreach (int id in colIds)
        //    //{
        //    //    if (id >= RoleRightsMatrix_rolesGridView.Columns.Count)
        //    //    {
        //    //        RoleRightsMatrix_rolesGridView.Columns[i].
        //    //    }

        //    //}

        //    count = RoleRightsMatrix_rolesGridView.Columns.Count;


        //    foreach (var r in rolesName)
        //    {
        //        boundField = new BoundField();

        //        boundField.HeaderText = r.RoleName;
        //        boundField.FooterText = r.RoleID.ToString();
        //        boundField.Visible = true;
        //        if (IfExist(boundField.HeaderText))
        //        {
        //            RoleRightsMatrix_rolesGridView.Columns.Add(boundField);
        //            RoleRightsMatrix_rolesGridView.DataBind();
        //        }

        //    }

        //    count = RoleRightsMatrix_rolesGridView.Columns.Count;

        //    for (int i = 0; i < RoleRightsMatrix_rolesGridView.Rows.Count; i++)
        //    {
        //        for (int j = 0; j < RoleRightsMatrix_rolesGridView.Columns.Count; j++)
        //        {
        //            if (RoleRightsMatrix_rolesGridView.Columns[j].FooterText == "")
        //                continue;

        //            int column = int.Parse(RoleRightsMatrix_rolesGridView.Columns[j].FooterText);

        //            Label lb = (Label)RoleRightsMatrix_rolesGridView.Rows[i].
        //                FindControl("RoleRightsMatrix_rolesGridView_featureIDLabel");

        //            int row = 0;

        //            if (lb != null)
        //            {
        //                row = int.Parse(lb.Text);
        //            }

        //            var value = from rrm in roleRightsMatrix
        //                        where (rrm.RoleID == column) &&
        //                            (rrm.FeatureID == row)
        //                        select rrm;
        //            //{ PageID = rrm.PageID, RoleRightID = rrm.RoleRightID} ;                    
        //            foreach (var c in value)
        //            {
        //                HiddenField hiddenField = new HiddenField();

        //                hiddenField.ID = "RoleRightsMatrix_rolesGridView_" + "rowId" + j + "HiddenField";

        //                CheckBox checkboxField = new CheckBox();

        //                checkboxField.ID = "RoleRightsMatrix_rolesGridView_" + "rowId" + j + "CheckBox";

        //                checkboxField.Checked = c.IsApplicable;

        //                hiddenField.Value = c.RoleRightID.ToString();

        //                checkboxField.Attributes.Add("onclick", "javascript:return StoreIdAndValue('" + hiddenField.Value + "');");

        //                //checkboxField.CheckedChanged += new EventHandler(checkboxField_CheckedChanged);

        //                TableCell cell = new TableCell();

        //                RoleRightsMatrix_rolesGridView.Rows[i].Cells[j].Controls.Add(checkboxField);

        //                RoleRightsMatrix_rolesGridView.Rows[i].Cells[j].Controls.Add(hiddenField);
        //            }

        //            //RoleRightsMatrix_rolesGridView.DataBind();                
        //        }
        //    }

        //    count = RoleRightsMatrix_rolesGridView.Columns.Count;
        //} 


        //private bool IfExist(string headerText)
        //{
        //    for (int i = 0; i < RoleRightsMatrix_rolesGridView.Columns.Count; i++)
        //    {
        //        if (RoleRightsMatrix_rolesGridView.Columns[i].HeaderText == headerText)
        //            return false;
        //    }

        //    return true;
        //}

        //for (int i = 0; i < RoleRightsMatrix_rolesGridView.Rows.Count; i++)
        //{
        //    for (int j = 0; j < RoleRightsMatrix_rolesGridView.Columns.Count; j++)
        //    {
        //        RoleRightMatrixDetail role = new RoleRightMatrixDetail();

        //        HiddenField hiddenField =
        //            (HiddenField)RoleRightsMatrix_rolesGridView.Rows[i].Cells[j].
        //            FindControl("RoleRightsMatrix_rolesGridView_" + "rowId" + j + "HiddenField");

        //        CheckBox checkBox =
        //            (CheckBox)RoleRightsMatrix_rolesGridView.Rows[i].Cells[j].FindControl("RoleRightsMatrix_rolesGridView_" + "rowId" + j + "CheckBox");


        //        if (hiddenField != null && checkBox != null)
        //        {
        //            role.RoleRightID = int.Parse(hiddenField.Value);
        //            role.IsApplicable = checkBox.Checked;
        //        }

        //        roleMatrixDetails.Add(role);
        //    }
        //}


        //for (int j = 0; j < RoleRightsMatrix_rolesGridView.Columns.Count; j++)
        //{
        //    for (int i = 0; i < RoleRightsMatrix_rolesGridView.Rows.Count; i++)
        //    {
        //        CheckBox chekc = ((CheckBox)(RoleRightsMatrix_rolesGridView.Rows[i].Cells[i].Controls[0]));
        //    }
        //}

        //for (int i = 1; i <= GRIDVIEW_COLUMN - 1; i++)
        //{
        //    CheckBox chekc = ((CheckBox)(e.Row.Cells[i].Controls[0]));

        //    chekc.Enabled = true;
        //}


        #endregion

    }
}