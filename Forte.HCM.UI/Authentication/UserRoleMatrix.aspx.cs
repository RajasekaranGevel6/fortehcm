﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// UserRoleMatrix.aspx.cs
// File that represents the UserRoleMatrix class that defines the user 
// interface layout and functionalities for the FeatureUsage page. This 
// page helps in assigning roles to the user. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives
using System;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
#endregion Directives

namespace Forte.HCM.UI.Authentication
{
    public partial class UserRoleMatrix : PageBase
    {

        #region Private Constants
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "525px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "510px";
        #endregion Private Constants

        #region Event Handlers
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page caption in the page
                Master.SetPageCaption("Assign User Roles");
                Master.HideUpdateProgress(UserRoleMatrix_selectRoleUpdatePanel.UniqueID);
                UserRoleMatrix_topSuccessMessageLabel.Text = string.Empty;
                UserRoleMatrix_bottomSuccessMessageLabel.Text = string.Empty;
                UserRoleMatrix_topErrorMessageLabel.Text = string.Empty;
                UserRoleMatrix_bottomErrorMessageLabel.Text = string.Empty;
                CheckAndSetExpandOrRestore();
                Page.Form.DefaultButton = UserRoleMatrix_showButton.UniqueID;
                Page.SetFocus(UserRoleMatrix_userIDTextBox.ClientID);

                if (!IsPostBack)
                {
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "ROLE";

                    SubscribtClientSideHandlers();

                    LoadValues();


                    //Add on click attribute for the check box list
                    //UserRightsMatrix_modulesCheckBoxList.Attributes.Add("onclick", "javascript:return selectedModuleId();");
                    if (Request.QueryString["userID"] != null)
                    {
                        UserDetail userDetail = new CommonBLManager().GetUserDetail
                            (int.Parse(Request.QueryString["userID"]));
                        UserRoleMatrix_userIDTextBox.Text = userDetail.FirstName;

                        UserRoleMatrix_userIDHiddenField.Value = userDetail.UserID.ToString();

                        LoadRolesGridView();
                    }

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row creation
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void UserRoleMatrix_rolesGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (UserRoleMatrix_rolesGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save is clicked
        /// after assigning the rights to the user.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void UserRoleMatrix_topSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (UserRoleMatrix_rolesGridView.Rows.Count == 0)
                {
                    base.ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                        UserRoleMatrix_bottomErrorMessageLabel,
                        "User role list not available");
                    return;
                }

                int userID = int.Parse(UserRoleMatrix_userIDHiddenField.Value);

                string sortOrder = ViewState["SORT_ORDER"].ToString();

                string sortExpression = ViewState["SORT_FIELD"].ToString();

                List<UsersRoleMatrix> rolesList = new AuthenticationBLManager().
                    GetAppliacableUserRoleRights(sortExpression, sortOrder, userID);

                for (int i = 0; i < UserRoleMatrix_rolesGridView.Rows.Count; i++)
                {

                    HiddenField userRights_HiddenField = (HiddenField)UserRoleMatrix_rolesGridView.Rows[i].FindControl
                                ("UserRoleMatrix_rolenAMEGridViewIDHiddenField");

                    CheckBox userRights_CheckBox = (CheckBox)UserRoleMatrix_rolesGridView.Rows[i].FindControl
                                 ("UserRoleMatrix_rolesGridView_CheckBox");

                    if (userRights_CheckBox.Checked == true)
                    {
                        new AuthenticationBLManager().InsertNewUserRoles(userID, Convert.ToInt32(userRights_HiddenField.Value),
                            base.userID);
                    }

                    for (int j = 0; j < rolesList.Count; j++)
                    {
                        if ((Convert.ToInt32(userRights_HiddenField.Value) == Convert.ToInt32(rolesList[j].RoleID)) &&
                            (userRights_CheckBox.Checked == false))
                        {
                            new AuthenticationBLManager().DeleteUserRoles(Convert.ToInt32(rolesList[j].URLID),
                                userID);
                        }
                    }

                }

                LoadRolesGridView();
                UserRoleMatrix_topErrorMessageLabel.Text = string.Empty;
                UserRoleMatrix_bottomErrorMessageLabel.Text = string.Empty;

                base.ShowMessage(UserRoleMatrix_topSuccessMessageLabel,
                   UserRoleMatrix_bottomSuccessMessageLabel, Resources.HCMResource.UserRoleMatrix_SuccessMessages);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void UserRoleMatrix_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                UserRoleMatrix_userIDTextBox.Text = string.Empty;
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when column sorting action
        /// takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void UserRoleMatrix_userRolesGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                LoadRolesGridView();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when show is clicked
        /// after assigning the rights to the user.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void UserRoleMatrix_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (UserRoleMatrix_userIDTextBox.Text == string.Empty)
                {
                    base.ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                       UserRoleMatrix_bottomErrorMessageLabel,
                      "User field cannot be empty");
                    return;
                }
                LoadRolesGridView();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row creation
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void UserRoleMatrix_userRoles_GridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (UserRoleMatrix_rolesGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                GridViewRow gridViewRow = (GridViewRow)((CheckBox)(sender)).Parent.Parent;

                if (gridViewRow != null)
                {
                    int userID = int.Parse(UserRoleMatrix_userIDHiddenField.Value);

                    string sortOrder = ViewState["SORT_ORDER"].ToString();

                    string sortExpression = ViewState["SORT_FIELD"].ToString();

                    List<UsersRoleMatrix> rolesList = new AuthenticationBLManager().
                        GetAppliacableUserRoleRights(sortExpression, sortOrder, userID);

                    HiddenField userRights_HiddenField = (HiddenField)gridViewRow.FindControl
                                ("UserRoleMatrix_rolenAMEGridViewIDHiddenField");

                    CheckBox userRights_CheckBox = (CheckBox)gridViewRow.FindControl
                                 ("UserRoleMatrix_rolesGridView_CheckBox");

                    if (userRights_CheckBox.Checked == true)
                    {
                        new AuthenticationBLManager().InsertNewUserRoles(userID,
                            Convert.ToInt32(userRights_HiddenField.Value), base.userID);
                    }
                    else
                        new AuthenticationBLManager().DeleteUserRoles(Convert.ToInt32(userRights_HiddenField.Value),
                             userID);

                    LoadRolesGridView();

                    UserRoleMatrix_topErrorMessageLabel.Text = string.Empty;
                    UserRoleMatrix_bottomErrorMessageLabel.Text = string.Empty;

                    base.ShowMessage(UserRoleMatrix_topSuccessMessageLabel,
                       UserRoleMatrix_bottomSuccessMessageLabel, Resources.HCMResource.UserRoleMatrix_SuccessMessages);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(UserRoleMatrix_topErrorMessageLabel,
                    UserRoleMatrix_bottomErrorMessageLabel, exception.Message);
            }

        }
        #endregion Event Handlers

        #region private Methods
        /// <summary>
        /// Method to check for the Expand or Restore images
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(UserRoleMatrix_isMaximizedHiddenField.Value) &&
                 UserRoleMatrix_isMaximizedHiddenField.Value == "Y")
            {
                ViewContributorSummary_questionSummaryDIV.Style["display"] = "none";
                UserRoleMatrix_userDetailsUparrowSpan.Style["display"] = "block";
                UserRoleMatrix_userDetailsDownarrowSpan.Style["display"] = "none";
                UserRoleMatrix_userDetailsDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                ViewContributorSummary_questionSummaryDIV.Style["display"] = "block";
                UserRoleMatrix_userDetailsUparrowSpan.Style["display"] = "none";
                UserRoleMatrix_userDetailsDownarrowSpan.Style["display"] = "block";
                UserRoleMatrix_userDetailsDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }
        /// <summary>
        /// Method to load the roles
        /// </summary>
        private void LoadRolesGridView()
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            int userID = int.Parse(UserRoleMatrix_userIDHiddenField.Value);

            List<UsersRoleMatrix> rolesList = new AuthenticationBLManager().GetAppliacableUserRoleRights(
                sortExpression, sortOrder, userID);

            List<UsersRoleMatrix> rolesAllList = new AuthenticationBLManager().GetAllUserRoleRights(
                sortExpression, sortOrder);

            CheckBox checkRoles = null;

            if (UserRoleMatrix_applcableRoleRadioButton.Checked == true)
            {

                UserRoleMatrix_rolesGridView.DataSource = rolesList;

                UserRoleMatrix_rolesGridView.DataBind();

                for (int i = 0; i < UserRoleMatrix_rolesGridView.Rows.Count; i++)
                {
                    checkRoles = (CheckBox)UserRoleMatrix_rolesGridView.Rows[i].
                        FindControl("UserRoleMatrix_rolesGridView_CheckBox");

                    checkRoles.Checked = true;
                }

                if (rolesList == null || rolesList.Count == 0)
                {
                    ShowMessage(UserRoleMatrix_topErrorMessageLabel, UserRoleMatrix_bottomErrorMessageLabel,
                        "No data found to display");
                }
            }
            else
            {
                UserRoleMatrix_rolesGridView.DataSource = rolesAllList;

                UserRoleMatrix_rolesGridView.DataBind();

                for (int i = 0; i < UserRoleMatrix_rolesGridView.Rows.Count; i++)
                {

                    HiddenField userRights_HiddenField = (HiddenField)UserRoleMatrix_rolesGridView.Rows[i].FindControl
                                ("UserRoleMatrix_rolenAMEGridViewIDHiddenField");


                    for (int j = 0; j < rolesList.Count; j++)
                    {
                        if ((userID == Convert.ToInt32(rolesList[j].UserID)) &&
                            (Convert.ToInt32(userRights_HiddenField.Value) == Convert.ToInt32(rolesList[j].RoleID)))
                        {
                            CheckBox userRights_CheckBox = (CheckBox)UserRoleMatrix_rolesGridView.Rows[i].FindControl
                                  ("UserRoleMatrix_rolesGridView_CheckBox");

                            userRights_CheckBox.Checked = true;
                        }
                    }

                }
            }

            /*for (int i = 0; i < UserRoleMatrix_rolesGridView.Rows.Count; i++)
            {

                HiddenField userRights_HiddenField = (HiddenField)UserRoleMatrix_rolesGridView.Rows[i].FindControl
                            ("UserRoleMatrix_rolenAMEGridViewIDHiddenField");

                for (int j = 0; j < rolesList.Count; j++)
                {
                    if ((userID == Convert.ToInt32(rolesList[j].UserID)) &&
                        (Convert.ToInt32(userRights_HiddenField.Value) == Convert.ToInt32(rolesList[j].RoleID)))
                    {
                        CheckBox userRights_CheckBox = (CheckBox)UserRoleMatrix_rolesGridView.Rows[i].FindControl
                              ("UserRoleMatrix_rolesGridView_CheckBox");

                        userRights_CheckBox.Checked = true;
                    }
                }

            }*/
        }
        /// <summary>
        /// Method to Subscribe the click event for the
        /// Expand or restore.
        /// </summary>
        private void SubscribtClientSideHandlers()
        {
            UserRoleMatrix_userIDTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                   + UserRoleMatrix_userIDTextBox.ClientID + "','NAME','"
                   + UserRoleMatrix_browserHiddenField.ClientID + "');");
            //Assign the on click attribute for the search image
            UserRoleMatrix_searchImage.Attributes.Add("onclick",
                 "javascript:return LoadUserForUserRoleRights('"
                + UserRoleMatrix_userIDTextBox.ClientID + "','"
                + UserRoleMatrix_userIDHiddenField.ClientID + "','"
                + UserRoleMatrix_userIDTextBox.ClientID + "','QA')");


            //Assign the on click attributes for the expand or restore button 
            UserRoleMatrix_userDetailsTR.Attributes.Add("onclick",
             "ExpandOrRestore('" +
             UserRoleMatrix_userDetailsDIV.ClientID + "','" +
             ViewContributorSummary_questionSummaryDIV.ClientID + "','" +
             UserRoleMatrix_userDetailsUparrowSpan.ClientID + "','" +
             UserRoleMatrix_userDetailsDownarrowSpan.ClientID + "','" +
             UserRoleMatrix_isMaximizedHiddenField.ClientID + "','" +
             RESTORED_HEIGHT + "','" +
             EXPANDED_HEIGHT + "')");
            //Adding client side onchange attribute the the authorid textbox.
            /* ViewContributorSummary_questionAuthorIDTextBox.Attributes.Add("onchange", "return TrimUserId('"
                 + ViewContributorSummary_questionAuthorIDTextBox.ClientID + "','" +
                 ViewContributorSummary_authorIDHiddenField.ClientID + "','" +
                 ViewContributorSummary_browserHiddenField.ClientID + "');");*/
        }
        #endregion private Methods

        #region Protected Overridden Methods
        protected override void LoadValues()
        {
        }
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }
        #endregion Protected Overridden Methods

    }
}

