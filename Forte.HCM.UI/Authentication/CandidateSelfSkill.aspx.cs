#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateSelfSkill.aspx.cs
// This page allows the admin to process the credit request 

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

using AjaxControlToolkit;

#endregion Directives

namespace Forte.HCM.UI.Authentication
{
    public partial class CandidateSelfSkill : PageBase
    {
        #region Event Handler                                                  

        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set page title
                Master.SetPageCaption("Manage Skills");

                if (!IsPostBack)
                {
                    // Assign default sort field and order keys.
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "CANDIDATE_NAME";

                    //Call the method to clear the controls in the page
                    ClearControls();

                    CandidateSelfSkill_Div.Visible = false;

                    //Load the credit detail for page number 1
                    LoadCandidateSelfDetails(1);
                    
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(CandidateSelfSkill_topErrorMessageLabel,
               CandidateSelfSkill_bottomErrorMessageLabel, exp.Message);
            }
        }

        
        /// <summary>
        /// Handler method that is called when the row is bounded in the grid
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void CandidateSelfSkill_resultGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    string candidateId = ((HiddenField)e.Row.FindControl("CandidateSelfSkill_candidateIdHiddenField")).Value;
                    string skillId = ((HiddenField)e.Row.FindControl("CandidateSelfSkill_skillIdHiddenField")).Value;
                    ((ImageButton)e.Row.FindControl("CandidateSelfSkill_approveCandidateSelfSkillImageButton")).Attributes.Add
                        ("onclick", "javascript:return ShowApproveCandidateSelfSkill('" + candidateId + "','" + skillId + "','" + CandidateSelfSkill_refreshButton.ClientID + "');");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSelfSkill_topErrorMessageLabel,
                CandidateSelfSkill_bottomErrorMessageLabel, exp.Message);
            }
        }

       
        /// <summary>
        /// Handler method that is called on the cancel button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CandidateSelfSkill_cancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateSelfSkill_bottomErrorMessageLabel,
                    CandidateSelfSkill_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called on the reset button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CandidateSelfSkill_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateSelfSkill_bottomErrorMessageLabel,
                    CandidateSelfSkill_topErrorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handler

        #region Private Methods                                                
        /// <summary>
        /// Represents the method to load the candidate self details 
        /// </summary>
        /// <param name="pageNumber"></param>
        private void LoadCandidateSelfDetails(int pageNumber)
        {
            List<CandidateInformation> candidateInformation = null;
            candidateInformation = new AdminBLManager().GetCandidateSelfSkillPending();

            if (candidateInformation!=null && candidateInformation.Count != 0)
            {
                CandidateSelfSkill_Div.Visible = true;
                CandidateSelfSkill_resultGridView.DataSource = candidateInformation;
                CandidateSelfSkill_resultGridView.DataBind();
            }
            else
            {
                CandidateSelfSkill_Div.Visible = false;
                CandidateSelfSkill_resultGridView.DataSource = candidateInformation;
                CandidateSelfSkill_resultGridView.DataBind();
                
                base.ShowMessage(CandidateSelfSkill_bottomErrorMessageLabel,
                CandidateSelfSkill_topErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
            }
        }

       
        /// <summary>
        /// Method used to clear the controls
        /// </summary>
        private void ClearControls()
        {
            CandidateSelfSkill_topSuccessMessageLabel.Text = string.Empty;
            CandidateSelfSkill_bottomSuccessMessageLabel.Text = string.Empty;
            CandidateSelfSkill_topErrorMessageLabel.Text = string.Empty;
            CandidateSelfSkill_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods       

        #region Protected Methods                                              

        /// <summary>
        /// Method to refresh the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateSelfSkill_refreshButton_Click(object sender, EventArgs e)
        {
            //Reload the grid
            LoadCandidateSelfDetails(1);
        }
        #endregion Protected Methods
    }
}
