﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="MyAvailability.aspx.cs" Inherits="Forte.HCM.UI.Assessor.MyAvailability" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="MyAvailability_content" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">

        function CollapseAllRows(targetControl) {
            var gridCtrl = document.getElementById("<%= MyAvailability_timeSlotsGridView_resultsDiv.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("MyAvailability_timeSlotsGridView_detailsDiv") != "-1") {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }

            return false;
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="MyAvailability_headerLiteral" runat="server" Text="My Availability"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td style="display: none">
                                        <asp:ImageButton ToolTip="View Schedule" ID="imgBtnViewSchedule" runat="server" SkinID="sknSearchExistingTestImageButton"
                                            Visible="true" />
                                    </td>
                                    <td style="display: none">
                                        <asp:ImageButton ToolTip="Search Assessors" ID="imgBtnSearchAssessors" runat="server"
                                            SkinID="sknTalentSearchImageButton" Visible="true" />
                                        <asp:HiddenField ID="hidSelectedAssessors" runat="server" />
                                    </td>
                                    <td style="display: none">
                                        <asp:ImageButton ToolTip="Select Time Slots" ID="imgBtnSearchTimeSlots" runat="server"
                                            SkinID="sknOpenPositionProfile" Visible="true" />
                                        <asp:HiddenField ID="hidSelectedTimeSlotIDs" runat="server" />
                                    </td>
                                    <td style="display: none">
                                        <asp:ImageButton ToolTip="Assessor Availability Request" ID="imgBtnAvailabilityRequest"
                                            runat="server" SkinID="sknViewPositionProfile" Visible="true" />
                                    </td>
                                    <td style="display: none">
                                        <asp:ImageButton ToolTip="View Assessor Profile" ID="imgBtnViewAssessorProfile" runat="server"
                                            SkinID="sknDeletePositionProfile" Visible="true" />
                                    </td>
                                    <td style="display: none">
                                        <asp:ImageButton ToolTip="Request Time Slot" ID="imgBtnShowRequestTimeSlot" runat="server"
                                            SkinID="sknEditPositionProfile" Visible="true" />
                                    </td>
                                    <td>
                                        <asp:Button ID="MyAvailability_topRefreshButton" runat="server" Text="Refresh" SkinID="sknButtonId"
                                            OnClick="MyAvailability_refreshButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="MyAvailability_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="MyAvailability_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="MyAvailability_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyAvailability_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="MyAvailability_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="MyAvailability_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <asp:UpdatePanel ID="MyAvailability_bodyUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 30%" valign="top">
                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header_bg" align="center">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 89%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="MyAvailability_calendarLiteral" runat="server" Text="Calendar"></asp:Literal>
                                                        </td>
                                                        <td style="width: 10%" align="right">
                                                            <asp:ImageButton ID="MyAvailability_nonAvailabilityDateImageButton" SkinID="sknNonAvailabilityDateImageButton"
                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to set vacation dates" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Calendar ID="MyAvailability_selectDateCalendar" runat="server" BackColor="White"
                                                                BorderColor="Black" BorderWidth="1px" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black"
                                                                Height="200px" Width="260px" SelectionMode="Day" OtherMonthDayStyle-Font-Underline="false"
                                                                DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                TodayDayStyle-Font-Underline="false" OnPreRender="MyAvailability_selectDateCalendar_PreRender"
                                                                OnSelectionChanged="MyAvailability_selectDateCalendar_SelectionChanged" OnVisibleMonthChanged="MyAvailability_selectDateCalendar_VisibleMonthChanged"
                                                                OnDayRender="MyAvailability_selectDateCalendar_DayRender">
                                                                <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                <SelectedDayStyle Font-Underline="false" BackColor="#59C1EE" ForeColor="Black"></SelectedDayStyle>
                                                                <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                            </asp:Calendar>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 2px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-left: 1px">
                                                            <table cellpadding="0" cellspacing="1" width="100%" border="0">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #E1D455" title="Request Initiated Dates">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="MyAvailability_initiatedDates_Label" runat="server" Text="Request Initiated Dates"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #7092BE" title="Available Dates">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="MyAvailability_selectedDates_Label" runat="server" Text="Available Dates"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #99DD96" title="Dates With Schedule">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="MyAvailability_scheduledDates_Label" runat="server" Text="Dates With Schedule"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="17px" align="left">
                                                                        <div style="width: 15px; height: 15px; background-color: #F29378" title="Vacation Dates">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="MyAvailability_vocationDates_Label" runat="server" Text="Vacation Dates"
                                                                            SkinID="sknHomePageLabel"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 1%">
                                </td>
                                <td style="width: 69%" valign="top" rowspan="2">
                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header_bg" align="center">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="MyAvailability_timeSlotsLiteral" runat="server" Text="Time Slots"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="center">
                                                            <div style="height: 26px; overflow: auto;">
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:ImageButton ID="MyAvailability_previousDate" runat="server" ToolTip="Go to the previous date"
                                                                                SkinID="sknPreviousDateImage" OnClick="MyAvailability_previousDate_Click" />
                                                                        </td>
                                                                        <td align="center">
                                                                            <asp:Label ID="MyAvailability_selectedDateLabel" runat="server" Text="" SkinID="sknLabelSelectedDateText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:ImageButton ID="MyAvailability_nextDate" runat="server" ToolTip="Go to the next date"
                                                                                SkinID="sknNextDateImage" OnClick="MyAvailability_nextDate_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="height: 200px; overflow: auto;" id="MyAvailability_timeSlotsGridView_resultsDiv"
                                                                runat="server">
                                                                <asp:GridView ID="MyAvailability_timeSlotsGridView" runat="server" AllowSorting="False"
                                                                    AutoGenerateColumns="False" Width="100%" OnRowDataBound="MyAvailability_timeSlotsGridView_RowDataBound"
                                                                    OnRowCommand="MyAvailability_timeSlotsGridView_RowCommand" Height="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="From">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupFromTimeLabel" runat="server"
                                                                                    SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextFrom") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="To">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupToTimeLabel" runat="server"
                                                                                    SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextTo") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupStatusLabel" runat="server"
                                                                                    SkinID="sknHomePageLabel" Text='<%# Eval("RequestStatus") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        <table style="width: 100%; height: 100%">
                                                                            <tr>
                                                                                <td style="height: 150px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                    No time slots found to display
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_10">
                                                            <asp:HiddenField ID="MyAvailability_selectedTimeSlotID" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="height: 200px; overflow: auto;">
                                                                <asp:UpdatePanel ID="MyAvailability_initiatedBy_GridViewUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="MyAvailability_initiatedBy_GridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                                            AutoGenerateColumns="False">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Interview Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyAvailability_initiatedBy_GridView_InterviewName_Label" runat="server"
                                                                                            Text='<%# Eval("InterviewName")  %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Initiated By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyAvailability_initiatedBy_GridView_InitiatedBy_Label" runat="server"
                                                                                            Text='<%# Eval("TestAuthor")  %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="From">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyAvailability_initiatedBy_GridView_FromTimeLabel" runat="server"
                                                                                            SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextFrom") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="To">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyAvailability_initiatedBy_GridView_ToTimeLabel" runat="server" SkinID="sknHomePageLabel"
                                                                                            Text='<%# Eval("TimeSlotTextTo") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="MyAvailability_selectDateCalendar" />
                                                                        <asp:AsyncPostBackTrigger ControlID="MyAvailability_nextDate" />
                                                                        <asp:AsyncPostBackTrigger ControlID="MyAvailability_previousDate" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:LinkButton ID="MyAvailability_addTimeSlotLinkButton" SkinID="sknAddLinkButton"
                                                                ToolTip="Add Time Slot" runat="server" Text="Add" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="bottom">
                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="header_bg" align="center">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="MyAvailability_summaryLiteral" runat="server" Text="Date Summary"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 65%; height: 20px" align="left" valign="middle">
                                                            <asp:Label ID="MyAvailability_summaryDateLabel" runat="server" Text="Selected Date"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%" align="left" valign="middle">
                                                            <asp:Label ID="MyAvailability_summaryDateValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 65%; height: 20px" align="left">
                                                            <asp:Label ID="MyAvailability_totalScheduledTimeSlotsLabel" runat="server" Text="Total Scheduled Slots"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%" align="left">
                                                            <asp:Label ID="MyAvailability_totalScheduledTimeSlotsValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 65%; height: 20px" align="left">
                                                            <asp:Label ID="MyAvailability_totalScheduledHoursLabel" runat="server" Text="Total Scheduled Hours"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%" align="left">
                                                            <asp:Label ID="MyAvailability_totalScheduledHoursValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyAvailability_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="MyAvailability_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="MyAvailability_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="MyAvailability_bottomRefreshButton" runat="server" Text="Refresh"
                                            SkinID="sknButtonId" OnClick="MyAvailability_refreshButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="MyAvailability_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="MyAvailability_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="MyAvailability_cancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="MyAvailability_deleteTimeSlotPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="MyAvailability_deleteTimeSlotHiddenDiv" style="display: none">
                        <asp:Button ID="MyAvailability_deleteTimeSlotHiddenButton" runat="server" />
                    </div>
                    <uc2:ConfirmMsgControl ID="MyAvailability_deleteTimeSlotConfirmMsgControl" runat="server"
                        OnOkClick="MyAvailability_deleteTimeSlot_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="MyAvailability_deleteTimeSlotModalPopupExtender"
                    runat="server" PopupControlID="MyAvailability_deleteTimeSlotPanel" TargetControlID="MyAvailability_deleteTimeSlotHiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
