﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MyAvailability.aspx.cs
// File that represents the user interface layout and functionalities
// for the MyAvailability page. This page helps in viewing the assessor
// availability calender along with time slots. This class inherits the 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Globalization;

#endregion Directives

namespace Forte.HCM.UI.Assessor
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the MyAvailability page. This page helps in viewing the assessor
    /// availability calender along with time slots. This class inherits the 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class MyAvailability : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page caption in the page
                Master.SetPageCaption("My Availability");
                ClearLabelMessage();

                if (!IsPostBack)
                {
                    //Clear viewstate
                    ViewState["ASSESSOR_VACATION_DATE"] = string.Empty;
                    ViewState["ASSESSOR_AVAILABLE_DATES"] = string.Empty;

                    // Clear session.
                    Session["MY_AVAILABILITY_NON_AVAILABLE_DATES"] = null;
                    Session["MY_AVAILABILITY_SCHEDULED_DATES"] = null;
                    Session["MY_AVAILABILITY_VISIBLE_DATE"] = null;

                    // Add handler to add time slot link.
                    MyAvailability_addTimeSlotLinkButton.Attributes.Add("onclick",
                        "return ShowAddTimeSlot(null,'" + MyAvailability_topRefreshButton.ClientID + "','P',null);");

                    // Add handler to set non availability dates.
                    if (Request.QueryString["assessorid"] != null)
                        MyAvailability_nonAvailabilityDateImageButton.Attributes.Add("onclick",
                        "return ShowViewNonAvailability('" + Request.QueryString["assessorid"]  + "','myavb','" +
                        MyAvailability_topRefreshButton.ClientID + "');"); 
                    else
                    MyAvailability_nonAvailabilityDateImageButton.Attributes.Add("onclick",
                        "return ShowViewNonAvailability('" + base.userID + "','myavb','" + 
                        MyAvailability_topRefreshButton.ClientID + "');"); 

                    //set current date
                    MyAvailability_selectDateCalendar.VisibleDate = DateTime.Today;

                    // Set current date as selected date.
                    MyAvailability_selectDateCalendar.SelectedDates.Clear();

                    // Get current date and time.
                    DateTime currentDate = DateTime.Now;
                    if (Session["MY_AVAILABILITY_LAST_SELECTED_DATE"] != null)
                    {
                        currentDate = (DateTime)Session["MY_AVAILABILITY_LAST_SELECTED_DATE"];

                        // Clear last selected date from session.
                        Session["MY_AVAILABILITY_LAST_SELECTED_DATE"] = null;
                    }

                    MyAvailability_selectDateCalendar.SelectedDates.Add(currentDate);

                    // Keep the date in session.
                    Session["MY_AVAILABILITY_DATE"] = currentDate;

                    MyAvailability_selectedDateLabel.Text = currentDate.ToString("dddd, dd MMMM yyyy");

                    if (Request.QueryString["assessorid"] != null)
                        GetAssessorAvailableDates(Convert.ToInt32(Request.QueryString["assessorid"]) ,
                           MyAvailability_selectDateCalendar.VisibleDate.Month);
                    else
                    GetAssessorAvailableDates(base.userID, 
                        MyAvailability_selectDateCalendar.VisibleDate.Month);

                    // Highlight monthly slots.
                    HighlightMonthlySlots(currentDate.Month, currentDate.Year);

                    // Set delete confirmation message and type.
                    MyAvailability_deleteTimeSlotConfirmMsgControl.Message = "Are you sure to delete the time slot ?";
                    MyAvailability_deleteTimeSlotConfirmMsgControl.Title = "Delete Time Slot";
                    MyAvailability_deleteTimeSlotConfirmMsgControl.Type = MessageBoxType.YesNo;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                   MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset link button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void MyAvailability_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                    MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the calender control is 
        /// prerendered.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void MyAvailability_selectDateCalendar_PreRender(object sender, EventArgs e)
        {
            try
            {
                // Clear selected dates.
               /* MyAvailability_selectDateCalendar.SelectedDates.Clear();

                if (Utility.IsNullOrEmpty(Session["MY_AVAILABILITY_DATE"]))
                    return;
                
                // Highlight the selected date.
                MyAvailability_selectDateCalendar.SelectedDates.Add
                    ((DateTime)Session["MY_AVAILABILITY_DATE"]);*/
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when a date in the calender control
        /// is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void MyAvailability_selectDateCalendar_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                // Keep the date in session.
                Session["MY_AVAILABILITY_DATE"] = MyAvailability_selectDateCalendar.SelectedDate;

                // Set the visible date in session.
                Session["MY_AVAILABILITY_VISIBLE_DATE"] = MyAvailability_selectDateCalendar.SelectedDate;

                MyAvailability_selectedDateLabel.Text = MyAvailability_selectDateCalendar.
                    SelectedDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Get the current date.
                DateTime currentDate = (DateTime)Session["MY_AVAILABILITY_DATE"];

                // Set the visible date.
                if (Session["MY_AVAILABILITY_DATE"] != null)
                {
                    MyAvailability_selectDateCalendar.VisibleDate =
                        (DateTime)Session["MY_AVAILABILITY_DATE"];
                }

                // Load time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
               /// HighlightMonthlySlots(currentDate.Month, currentDate.Year);

                //Getting time slots
                if (Request.QueryString["assessorid"] != null)
                {
                    GetAvailableSlots(Convert.ToInt32(Request.QueryString["assessorid"]), MyAvailability_selectDateCalendar.SelectedDate);
                    GetAsssessorInterviews(Convert.ToInt32(Request.QueryString["assessorid"]), MyAvailability_selectDateCalendar.SelectedDate);
                }
                else
                {
                    GetAvailableSlots(base.userID, MyAvailability_selectDateCalendar.SelectedDate);
                    GetAsssessorInterviews(base.userID, MyAvailability_selectDateCalendar.SelectedDate);
                } 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the visible month is changed in
        /// the calendar control.
        /// </summary>
        /// <param name="e">
        /// A <see cref="MonthChangedEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the next date.
        /// </remarks>
        protected void MyAvailability_selectDateCalendar_VisibleMonthChanged
            (object sender, MonthChangedEventArgs e)
        {
            try
            {
                MyAvailability_timeSlotsGridView.DataSource = null;
                MyAvailability_timeSlotsGridView.DataBind();

                // Set the visible date in session.
                Session["MY_AVAILABILITY_VISIBLE_DATE"] = e.NewDate;
                
                // Highlight monthly slots.
                HighlightMonthlySlots(e.NewDate.Month, e.NewDate.Year);

                if (Request.QueryString["assessorid"] != null)
                    GetAssessorAvailableDates(Convert.ToInt32(Request.QueryString["assessorid"]),
                       e.NewDate.Month);
                else
                    GetAssessorAvailableDates(base.userID,
                        e.NewDate.Month);

                //GetAssessorAvailableDates(base.userID, e.NewDate.Month);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the day rendering is happen in 
        /// the calendar control.
        /// </summary>
        /// <param name="e">
        /// A <see cref="DayRenderEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will highlights the non availability dates.
        /// </remarks>
        protected void MyAvailability_selectDateCalendar_DayRender
            (object sender, DayRenderEventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(ViewState["ASSESSOR_AVAILABLE_DATES"]))
                {
                    List<AssessorTimeSlotDetail> availabilityDates = null;

                    availabilityDates = ViewState["ASSESSOR_AVAILABLE_DATES"] as List<AssessorTimeSlotDetail>;

                    foreach (AssessorTimeSlotDetail ass in availabilityDates)
                    {
                        if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Approved" && ass.Scheduled == "S")
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(153, 221, 150);
                            e.Cell.ToolTip = "Scheduled date";  
                        }
                        else if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Approved" && ass.Scheduled == "N")
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(112, 146, 190);
                            e.Cell.ToolTip = "Request Approved but not yet scheduled";
                        }
                        else if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Initiated" && ass.Scheduled == "N")
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(255, 212, 85);
                            e.Cell.ToolTip = "Request initiated";
                        }
                    }
                }


                if (!Utility.IsNullOrEmpty(ViewState["ASSESSOR_VACATION_DATE"]))
                {

                    string[] vacationDates = ViewState["ASSESSOR_VACATION_DATE"].ToString().Split(',');

                    if (vacationDates == null) return;

                    foreach (string vDate in vacationDates)
                    { 
                        //if (e.Day.Date == DateTime.Parse(vDate, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None))
                        //if (e.Day.Date == DateTime.Parse(vDate, CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat))
                        if (e.Day.Date.ToShortDateString() == vDate)
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(242, 147, 120);
                            e.Day.IsSelectable = false;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                 MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the previous date button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the previous date.
        /// </remarks>
        protected void MyAvailability_previousDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["MY_AVAILABILITY_DATE"] == null)
                {
                    base.ShowMessage(MyAvailability_topErrorMessageLabel,
                        MyAvailability_bottomErrorMessageLabel, "No selected date is present");
                    return;
                }

                DateTime actualDate = (DateTime)Session["MY_AVAILABILITY_DATE"];

                // Check if date is min value.
                if (IsMinDate(actualDate))
                {
                    base.ShowMessage(MyAvailability_topErrorMessageLabel,
                        MyAvailability_bottomErrorMessageLabel, "Cannot process dates lesser than this");
                    return;
                }

                // Construct new date.
                DateTime newDate = ((DateTime)Session["MY_AVAILABILITY_DATE"]).AddDays(-1);

                // Keep the date in session.
                Session["MY_AVAILABILITY_DATE"] = newDate;

                MyAvailability_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(newDate.Month, newDate.Year);

                // Set the visible date.
                if (Session["MY_AVAILABILITY_DATE"] != null)
                {
                    MyAvailability_selectDateCalendar.VisibleDate =
                        (DateTime)Session["MY_AVAILABILITY_DATE"];
                }

                //Getting time slots
                if (Request.QueryString["assessorid"] != null)
                {
                    GetAvailableSlots(Convert.ToInt32(Request.QueryString["assessorid"]) , newDate);
                    GetAsssessorInterviews(Convert.ToInt32(Request.QueryString["assessorid"]), newDate);
                }
                else
                {
                    GetAvailableSlots(base.userID, newDate);
                    GetAsssessorInterviews(base.userID, newDate);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the next date button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the next date.
        /// </remarks>
        protected void MyAvailability_nextDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["MY_AVAILABILITY_DATE"] == null)
                {
                    base.ShowMessage(MyAvailability_topErrorMessageLabel,
                        MyAvailability_bottomErrorMessageLabel, "No selected date is present");
                    return;
                }

                DateTime actualDate = (DateTime)Session["MY_AVAILABILITY_DATE"];

                // Check if date is max value.
                if (IsMaxDate(actualDate))
                {
                    base.ShowMessage(MyAvailability_topErrorMessageLabel,
                        MyAvailability_bottomErrorMessageLabel, "Cannot process dates greater than this");
                    return;
                }

                // Construct new date.
                DateTime newDate = ((DateTime)Session["MY_AVAILABILITY_DATE"]).AddDays(1);

                // Keep the date in session.
                Session["MY_AVAILABILITY_DATE"] = newDate;

                MyAvailability_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(newDate.Month, newDate.Year);

                // Set the visible date.
                if (Session["MY_AVAILABILITY_DATE"] != null)
                {
                    MyAvailability_selectDateCalendar.VisibleDate =
                        (DateTime)Session["MY_AVAILABILITY_DATE"];
                }

                //Getting time slots
                if (Request.QueryString["assessorid"] != null)
                {
                    GetAvailableSlots(Convert.ToInt32(Request.QueryString["assessorid"]), newDate);
                    GetAsssessorInterviews(Convert.ToInt32(Request.QueryString["assessorid"]), newDate);
                }
                else
                {
                    GetAvailableSlots(base.userID, newDate);
                    GetAsssessorInterviews(base.userID, newDate);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyAvailability_timeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    /* // Set row styles.
                     e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                     e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                     int id = Convert.ToInt32((e.Row.FindControl("MyAvailability_timeSlotsGridView_timeSlotIDHiddenField") 
                         as HiddenField).Value);

                     // Assign handler to edit time slots image button.
                     ImageButton editTimeSlot = e.Row.FindControl("MyAvailability_timeSlotsGridView_editTimeSlotImageButton")
                         as ImageButton;
                     editTimeSlot.Attributes.Add("OnClick", "javascript:return ShowAddTimeSlot('MYAVB','" + 
                         MyAvailability_topRefreshButton.ClientID + "','E','" + id + "')");

                     LinkButton MyAvailability_timeSlotsGridView_skillLinkButton =
                         (LinkButton)e.Row.FindControl("MyAvailability_timeSlotsGridView_skillLinkButton");

                     HtmlContainerControl MyAvailability_timeSlotsGridView_detailsDiv =
                         (HtmlContainerControl)e.Row.FindControl("MyAvailability_timeSlotsGridView_detailsDiv");

                     HtmlAnchor MyAvailability_timeSlotsGridView_detailsViewFocusDownLink =
                         (HtmlAnchor)e.Row.FindControl("MyAvailability_timeSlotsGridView_detailsViewFocusDownLink");

                     MyAvailability_timeSlotsGridView_skillLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                         MyAvailability_timeSlotsGridView_detailsDiv.ClientID + "','" + MyAvailability_timeSlotsGridView_detailsViewFocusDownLink.ClientID + "')");*/
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void MyAvailability_timeSlotsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteTimeSlot")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    MyAvailability_selectedTimeSlotID.Value = (MyAvailability_timeSlotsGridView.Rows
                        [index].FindControl("MyAvailability_timeSlotsGridView_timeSlotIDHiddenField") as HiddenField).Value;
                    MyAvailability_deleteTimeSlotModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the refresh button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void MyAvailability_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reload time slots.
                LoadTimeSlots();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void MyAvailability_deleteTimeSlot_OkClick(object sender, EventArgs e)
        {
            try
            {
                // Check if the hidden field has the selected time slot ID.
                if (Utility.IsNullOrEmpty(MyAvailability_selectedTimeSlotID.Value))
                    return;

                // Delete the time slot.
                new AssessorBLManager().DeleteAssessorTimeSlot
                    (Convert.ToInt32(MyAvailability_selectedTimeSlotID.Value));

                // Reload time slots.
                LoadTimeSlots();

                // Show success message.
                base.ShowMessage(MyAvailability_topSuccessMessageLabel,
                  MyAvailability_bottomSuccessMessageLabel, "Time slot deleted successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAvailability_topErrorMessageLabel,
                  MyAvailability_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            return true;
        }

        #endregion Protected Overridden Methods

        #region Private Methods           
        
        private void GetAvailableSlots(int assessorId, DateTime dtime)
        {
            MyAvailability_timeSlotsGridView.DataSource = null;
            MyAvailability_timeSlotsGridView.DataBind();

            if (dtime.ToShortDateString() == "01/01/0001" ||
                   dtime.ToShortDateString() == "01-01-0001") return;

            //geting available date genid
            int genId = new AssessorBLManager().GetAssessorAvailableDateGenId(assessorId, dtime);         

            if (genId == 0) return;

            List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                       new OnlineInterviewAssessorBLManager().GetAssessorTimeSlots(genId);

            if (assessorAvailabilityDates == null || assessorAvailabilityDates.Count == 0) return;

            MyAvailability_timeSlotsGridView.DataSource = assessorAvailabilityDates;
            MyAvailability_timeSlotsGridView.DataBind();
             
                       // Assign summary.
            MyAvailability_totalScheduledTimeSlotsValueLabel.Text = assessorAvailabilityDates.Count.ToString();
            MyAvailability_totalScheduledHoursValueLabel.Text = GetTotalScheduledHours(assessorAvailabilityDates); 

            //GetAssessorAvailableDates(base.userID, MyAvailability_selectDateCalendar.VisibleDate.Month);

            if (Request.QueryString["assessorid"] != null)
                GetAssessorAvailableDates(Convert.ToInt32(Request.QueryString["assessorid"]),
                   MyAvailability_selectDateCalendar.VisibleDate.Month);
            else
                GetAssessorAvailableDates(base.userID,
                    MyAvailability_selectDateCalendar.VisibleDate.Month);
        }         

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            MyAvailability_topSuccessMessageLabel.Text = string.Empty;
            MyAvailability_bottomSuccessMessageLabel.Text = string.Empty;
            MyAvailability_topErrorMessageLabel.Text = string.Empty;
            MyAvailability_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that loads the time slots for the selected date.
        /// </summary>
        private void LoadTimeSlots()
        {
            if (Session["MY_AVAILABILITY_DATE"] == null)
                return;

            // Clear data.
            MyAvailability_timeSlotsGridView.DataSource = null;
            MyAvailability_timeSlotsGridView.DataBind();

            // Assign default values to summary.
            MyAvailability_summaryDateValueLabel.Text = ((DateTime)Session["MY_AVAILABILITY_DATE"]).ToString("MM/dd/yyyy");
            MyAvailability_totalScheduledTimeSlotsValueLabel.Text = "0";
            MyAvailability_totalScheduledHoursValueLabel.Text = "00:00";

            // Load assessor detail.
            /* AssessorDetail assesorDetail = new AssessorBLManager().GetAssessorByUserId(base.userID);
             
                 DateTimeFormatInfo dtfi = CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat;
                 Response.Write(dtfi.ShortDatePattern);
                 DateTime dob = DateTime.Parse("2/5/2012", CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat); 
           

               if (assesorDetail != null && !Utility.IsNullOrEmpty( assesorDetail.NonAvailabilityDates))
           { 
               List<DateTime> nonAvailabilityDates = assesorDetail.NonAvailabilityDates.Split
                   (',').Select(n => DateTime.Parse(n, CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat)).ToList();
                 
               // Keep non availability dates in session.
               Session["MY_AVAILABILITY_NON_AVAILABLE_DATES"] = nonAvailabilityDates;
           }

           // Load assessor time slots.
           List<AssessorTimeSlotDetail> timeSlots = new AssessorBLManager().
                 GetAssessorTimeSlots(base.userID, (DateTime)Session["MY_AVAILABILITY_DATE"]);

            if (timeSlots == null || timeSlots.Count == 0)
             {
                 return;
             }

             MyAvailability_timeSlotsGridView.DataSource = timeSlots;
             MyAvailability_timeSlotsGridView.DataBind();

             // Assign summary.
             MyAvailability_totalScheduledTimeSlotsValueLabel.Text = timeSlots.Count.ToString();
             MyAvailability_totalScheduledHoursValueLabel.Text = GetTotalScheduledHours(timeSlots);*/
        }

        /// <summary>
        /// Method that calculates and returns the total hours scheduled from
        /// the list of time slots given for a selected date.
        /// </summary>
        /// <param name="timeSlots">
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the time
        /// slots.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the total scheduled hours in a 
        /// time format.
        /// </returns>
        private string GetTotalScheduledHours(List<AssessorTimeSlotDetail> timeSlots)
        {
            string hours = "00:00";

            int minutes = 0;
            foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
            {
                //minutes += ((timeSlot.TimeSlotIDTo - timeSlot.TimeSlotIDFrom) * 30);
                DateTime dbStartTime = Convert.ToDateTime(timeSlot.TimeSlotTextFrom.ToString(new DateTimeFormatInfo()));
                DateTime dbEndTime = Convert.ToDateTime(timeSlot.TimeSlotTextTo.ToString(new DateTimeFormatInfo()));

                TimeSpan expiryDiff = dbEndTime.Subtract(dbStartTime);
                minutes += ((expiryDiff.Minutes) * 30);

                if (expiryDiff.Hours > 0)
                    minutes += expiryDiff.Hours * 60;
            }

            if (minutes == 1440)
            {
                // For 24 hrs the system will return 00:00. So manually set
                // the value 24:00.
                hours = "24:00";
            }
            else
            {
                TimeSpan timeSpan = TimeSpan.FromMinutes(minutes);
                hours = string.Format("{0:D2}:{1:D2}",
                    timeSpan.Hours, timeSpan.Minutes);
            }

            return hours;
        }

        /// <summary>
        /// Method that highlights the monthly slot dates.
        /// </summary>
        /// <param name="month">
        /// A <see cref="int"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <remarks>
        /// This method will be called whenever a month is changed in the 
        /// calendar control.
        /// </remarks>
        private void HighlightMonthlySlots(int month, int year)
        {
            // Clear all selected dates.
            MyAvailability_selectDateCalendar.SelectedDates.Clear();

            // Get slots for current, previous and next months and keep in the session.
            if (Request.QueryString["assessorid"] != null)
                Session["MY_AVAILABILITY_SCHEDULED_DATES"] = new AssessorBLManager().
                GetAssessorTimeSlotDates(Convert.ToInt32(Request.QueryString["assessorid"]), month, year, true);
            else
            Session["MY_AVAILABILITY_SCHEDULED_DATES"] = new AssessorBLManager().
                GetAssessorTimeSlotDates(base.userID, month, year, true);
        }


        private void GetAssessorAvailableDates(int assessorId, int month)
        {
            int totalRecords = 0;  

            List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                  new OnlineInterviewAssessorBLManager().GetAssessorAvailableDates(assessorId,
                  month, 1, base.GridPageSize, out totalRecords);

            if (Utility.IsNullOrEmpty(assessorAvailabilityDates)) return; 

            ViewState["ASSESSOR_AVAILABLE_DATES"] = assessorAvailabilityDates;

            if (Utility.IsNullOrEmpty(assessorAvailabilityDates[0].VacationDates)) return;

            ViewState["ASSESSOR_VACATION_DATE"] = assessorAvailabilityDates[0].VacationDates.ToString();
        }

        private void GetAsssessorInterviews(int assessorId, DateTime requestDate)
        {
            List<InterviewDetail> interviewDetailList = new OnlineInterviewAssessorBLManager().
                   GetAssessorInterviewDetail(assessorId, requestDate);

            MyAvailability_initiatedBy_GridView.DataSource = null;
            MyAvailability_initiatedBy_GridView.DataBind();

            if (interviewDetailList != null)
            {
                MyAvailability_initiatedBy_GridView.DataSource = interviewDetailList;
                MyAvailability_initiatedBy_GridView.DataBind();
            } 
        }
        #endregion Private Methods

         
    }
}