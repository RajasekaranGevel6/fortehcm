﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchTimeSlotRequest.aspx.cs"
    Inherits="Forte.HCM.UI.Assessors.SearchTimeSlotRequest" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<asp:Content ID="SearchTimeSlotRequest_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <script type="text/javascript" language="javascript">

        // Hide all the expanded panel.
        function CollapseAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= SearchTimeSlotRequest_timeSlotsGridViewDiv.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("SearchTimeSlotRequest_timeSlotsGridView_detailsDiv") != "-1")
                    {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }

            return false;
        }


    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="SearchTimeSlotRequest_headerLiteral" runat="server" Text="Search Time Slot Request"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="SearchTimeSlotRequest_topProposeDateButton" runat="server"
                                            Text="Propose Time Slot" SkinID="sknButtonId" ></asp:Button>
                                    </td>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="SearchTimeSlotRequest_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchTimeSlotRequest_resetLinkButton_Click"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="SearchTimeSlotRequest_topCancelLinkButton"
                                            runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="SearchTimeSlotRequest_cancelLinkButton_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchTimeSlotRequest_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchTimeSlotRequest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchTimeSlotRequest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="SearchTimeSlotRequest_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="SearchTimeSlotRequest_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <asp:UpdatePanel runat="server" ID="SearchTimeSlotRequest_criteriaUpdatePanel">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="SearchTimeSlotRequest_statusLabel" runat="server" Text="Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:DropDownList ID="SearchTimeSlotRequest_statusDropDownList" runat="server" Width="166px">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="SearchTimeSlotRequest_requestStatusHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the status here"
                                                                                    Width="16px" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                        </td>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="SearchTimeSlotRequest_initatedByLabel" runat="server" Text="Initiated By"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="SearchTimeSlotRequest_initiatedByTextBox" MaxLength="50" runat="server"
                                                                                    Width="160px"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="SearchTimeSlotRequest_initiatedByImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the initiator" />
                                                                                <asp:HiddenField ID="SearchTimeSlotRequest_initiatedByHiddenField" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_4" colspan="8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="SearchTimeSlotRequest_requestedDateLabel" runat="server" 
                                                                                SkinID="sknLabelFieldHeaderText" Text="Requested Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                        <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="SearchTimeSlotRequest_requestedDateTextBox" runat="server" Width="160px"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left">
                                                                                <asp:ImageButton ID="SearchTimeSlotRequest_requestedDateImageButton" SkinID="sknCalendarImageButton"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the requested date"/>
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="SearchTimeSlotRequest_requestedDateMaskedEditExtender"
                                                                                runat="server" TargetControlID="SearchTimeSlotRequest_requestedDateTextBox" Mask="99/99/9999"
                                                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="SearchTimeSlotRequest_requestedDateMaskedEditValidator"
                                                                                runat="server" ControlExtender="SearchTimeSlotRequest_requestedDateMaskedEditExtender"
                                                                                ControlToValidate="SearchTimeSlotRequest_requestedDateTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="SearchTimeSlotRequest_requestedDateCustomCalendarExtender"
                                                                                runat="server" TargetControlID="SearchTimeSlotRequest_requestedDateTextBox" CssClass="MyCalendar"
                                                                                Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchTimeSlotRequest_requestedDateImageButton" />
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                        </td>
                                                                       <td style="width: 10%">
                                                                        <asp:Label ID="SearchTimeSlotRequest_initiatedDateLabel" runat="server" 
                                                                            SkinID="sknLabelFieldHeaderText" Text="Initiated Date"></asp:Label>
                                                                         </td>
                                                                        <td style="width: 20%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="SearchTimeSlotRequest_initiatedDateTextBox" runat="server" Width="160px"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left">
                                                                                <asp:ImageButton ID="SearchTimeSlotRequest_initiatedDateImageButton" SkinID="sknCalendarImageButton"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the initiated date"/>
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="SearchTimeSlotRequest_initiatedDateMaskedEditExtender"
                                                                                runat="server" TargetControlID="SearchTimeSlotRequest_initiatedDateTextBox" Mask="99/99/9999"
                                                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="SearchTimeSlotRequest_initiatedDateMaskedEditValidator"
                                                                                runat="server" ControlExtender="SearchTimeSlotRequest_initiatedDateMaskedEditExtender"
                                                                                ControlToValidate="SearchTimeSlotRequest_initiatedDateTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="SearchTimeSlotRequest_initiatedDateCalendarExtender"
                                                                                runat="server" TargetControlID="SearchTimeSlotRequest_initiatedDateTextBox" CssClass="MyCalendar"
                                                                                Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchTimeSlotRequest_initiatedDateImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 4px">
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                            <div style="display: none">
                                                                    <asp:HiddenField ID="SearchTimeSlotRequest_timeSlotStatusHiddenField" runat="server" />
                                                                    <asp:Button ID="SearchTimeSlotRequest_proposeTimeSlotButton" runat="server" OnClick="SearchTimeSlotRequest_proposeTimeSlotButton_Click" />
                                                                </div>
                                                                <asp:Button ID="SearchTimeSlotRequest_searchButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Search" OnClick="SearchTimeSlotRequest_searchButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 6px">
                        </td>
                    </tr>
                    <tr id="SearchTimeSlotRequest_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="SearchTimeSlotRequest_requestsResultsLiteral" runat="server" Text="Time Slots"></asp:Literal>
                                        &nbsp;<asp:Label ID="SearchTimeSlotRequest_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="SearchTimeSlotRequest_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="SearchTimeSlotRequest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="SearchTimeSlotRequest_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="SearchTimeSlotRequest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="SearchTimeSlotRequest_restoreHiddenField" runat="server" />
                                        <asp:HiddenField ID="SearchTimeSlotRequest_isMaximizedHiddenField" runat="server"
                                            Value="N" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <asp:UpdatePanel ID="SearchTimeSlotRequest_assessmentDetailsGridViewUpdatePanel"
                                runat="server">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="SearchTimeSlotRequest_timeSlotsGridViewDiv">
                                                    <asp:GridView ID="SearchTimeSlotRequest_timeSlotsGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" OnSorting="SearchTimeSlotRequest_timeSlotsGridView_Sorting"
                                                        OnRowCommand="SearchTimeSlotRequest_timeSlotsGridView_RowCommand" OnRowCreated="SearchTimeSlotRequest_timeSlotsGridView_RowCreated"
                                                        OnRowDataBound="SearchTimeSlotRequest_timeSlotsGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="SearchTimeSlotRequest_timeSlotsGridView_selectCheckBox" runat="server" />
                                                                    <asp:ImageButton ID="SearchTimeSlotRequest_timeSlotsGridView_acceptTimeSlotImageButton"
                                                                        runat="server" SkinID="sknAcceptTimeSlot" ToolTip="Accept"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" CssClass="showCursor" />
                                                                    <asp:ImageButton ID="SearchTimeSlotRequest_timeSlotsGridView_editTimeSlotImageButton"
                                                                        runat="server" SkinID="sknInteviewQuestionPreviewImageButton" ToolTip="View/Edit Time Slot"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" CssClass="showCursor" Visible="false" />
                                                                    <asp:ImageButton ID="SearchTimeSlotRequest_timeSlotsGridView_rejectTimeSlotImageButton"
                                                                        runat="server" SkinID="sknRejectTimeSlot" ToolTip="Reject" CommandName="Reject"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" />
                                                                    <asp:HiddenField ID="SearchTimeSlotRequest_timeSlotsGridView_timeSlotIDHiddenField"
                                                                        Value='<%# Eval("ID") %>' runat="server" />
                                                                    <asp:HiddenField ID="SearchTimeSlotRequest_timeSlotsGridView_requestStatusHiddenField"
                                                                        Value='<%# Eval("RequestStatus") %>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Requested Date" SortExpression="REQUESTED_DATE DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTimeSlotRequest_timeSlotsGridView_requestedDateLabel" runat="server"
                                                                        Text='<%# Eval("AvailabilityDateString") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Initiated By" SortExpression="INITIATED_BY_NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTimeSlotRequest_timeSlotsGridView_initiatedByNameLabel" runat="server"
                                                                        Text='<%# Eval("InitiatedByName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Initiated Date" SortExpression="INITIATED_DATE DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTimeSlotRequest_timeSlotsGridView_initiatedDateLabel" runat="server"
                                                                        Text='<%# Eval("InitiatedDateString") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" SortExpression="REQUEST_STATUS DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchTimeSlotRequest_timeSlotsGridView_requestStausLabel" runat="server"
                                                                        Text='<%# Eval("RequestStatusName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100%">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td align="left" style="width: 50%">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Button ID="SearchTimeSlotRequest_acceptButton" runat="server" 
                                                                            OnClick="SearchTimeSlotRequest_acceptButton_Click" SkinID="sknButtonId" 
                                                                            Text="Accept" Visible="false" />
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;<asp:Button ID="SearchTimeSlotRequest_rejectButton" runat="server" 
                                                                            OnClick="SearchTimeSlotRequest_rejectButton_Click" SkinID="sknButtonId" 
                                                                            Text="Reject" Visible="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="right" style="width: 50%">
                                                            <uc3:PageNavigator ID="SearchTimeSlotRequest_pagingNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchTimeSlotRequest_searchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchTimeSlotRequest_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchTimeSlotRequest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchTimeSlotRequest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="SearchTimeSlotRequest_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="SearchTimeSlotRequest_bottomProposeDateButton" runat="server" Text="Propose Time Slot"
                                SkinID="sknButtonId">
                            </asp:Button>
                        </td>
                        <td>
                            &nbsp;<asp:LinkButton ID="SearchTimeSlotRequest_bottomResetLinkButton" runat="server"
                                Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchTimeSlotRequest_resetLinkButton_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="SearchTimeSlotRequest_bottomCancelLinkButton"
                                runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="SearchTimeSlotRequest_cancelLinkButton_Click"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
