﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchTimeSlotRequest.aspx.cs
// File that represents the user interface layout and functionalities
// for the SearchTimeSlotRequest page. This page helps in viewing and 
// processing the time slot requests made by users.

#endregion Header

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ExternalService;

namespace Forte.HCM.UI.Assessors
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchTimeSlotRequest page. This page helps in viewing and 
    /// processing the time slot requests made by users. This class inherits 
    /// the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchTimeSlotRequest : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> that holds the status that indicates whether
        /// to display page as search time slots or pending requests.
        /// </summary>
        /// <remarks>
        /// The value 'P' indicates pending requests and empty indicates search 
        /// time slots.
        /// </remarks>
        private string type = string.Empty;

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(AssessorDetail assessorDetail, EntityType entityType);

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]))
                    type = Request.QueryString["type"].ToUpper();

                if (type == "P")
                {
                    Master.SetPageCaption("My Pending Time Slot Requests");
                    SearchTimeSlotRequest_headerLiteral.Text = "My Pending Time Slot Requests";
                    SearchTimeSlotRequest_isMaximizedHiddenField.Value = "Y";
                    SearchTimeSlotRequest_searchResultsUpImage.Visible = false;
                    SearchTimeSlotRequest_searchResultsDownImage.Visible = false;
                }
                else
                {
                    Master.SetPageCaption("Search Time Slot Request");
                    SearchTimeSlotRequest_headerLiteral.Text = "Search Time Slot Request";

                    SearchTimeSlotRequest_searchResultsTR.Attributes.Add("onclick",
                        "ExpandOrRestore('" +
                        SearchTimeSlotRequest_timeSlotsGridViewDiv.ClientID + "','" +
                        SearchTimeSlotRequest_searchCriteriasDiv.ClientID + "','" +
                        SearchTimeSlotRequest_searchResultsUpSpan.ClientID + "','" +
                        SearchTimeSlotRequest_searchResultsDownSpan.ClientID + "','" +
                        SearchTimeSlotRequest_isMaximizedHiddenField.ClientID + "','" +
                        RESTORED_HEIGHT + "','" +
                        EXPANDED_HEIGHT + "')");
                }
                
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    SearchTimeSlotRequest_initiatedByImageButton.Attributes.Add("onclick",
                       "return SearchCorporateUsers('" + SearchTimeSlotRequest_initiatedByHiddenField.ClientID + 
                       "','" + SearchTimeSlotRequest_initiatedByTextBox.ClientID + "');");

                    // Load values.
                    LoadValues();

                    ViewState["SORT_ORDER"] = SortType.Descending;
                    ViewState["SORT_FIELD"] = "REQUEST_STATUS";

                    SearchTimeSlotRequest_topProposeDateButton.Attributes.Add("OnClick", "javascript:return ShowAddTimeSlot('" + SearchTimeSlotRequest_timeSlotStatusHiddenField.ClientID + "','" +
                        SearchTimeSlotRequest_proposeTimeSlotButton.ClientID + "','P',null)");

                    SearchTimeSlotRequest_bottomProposeDateButton.Attributes.Add("OnClick", "javascript:return ShowAddTimeSlot('" + SearchTimeSlotRequest_timeSlotStatusHiddenField.ClientID + "','" +
                       SearchTimeSlotRequest_proposeTimeSlotButton.ClientID + "','P',null)");
                }

                // Subscribes to paging event.
                SearchTimeSlotRequest_pagingNavigator.PageNumberClick += new 
                    PageNavigator.PageNumberClickEventHandler(SearchTimeSlotRequest_pagingNavigator_PageNumberClick);

                if (type == "P" && !IsPostBack)
                {
                    // Search time slots.
                    SearchTimeSlots();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that holds the event data.
        /// </param>
        private void SearchTimeSlotRequest_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadTimeSlotRequests(e.PageNumber);
                ViewState["PAGENUMBER"] = e.PageNumber;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the search button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTimeSlotRequest_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Search time slots.
                SearchTimeSlots();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the availability request button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is called when the save button is clicked in the 
        /// availability request window. This will just show a success message 
        /// to the user.
        /// </remarks>
        protected void SearchTimeSlotRequest_proposeTimeSlotButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchTimeSlotRequest_topSuccessMessageLabel.Text = string.Empty;
                SearchTimeSlotRequest_bottomSuccessMessageLabel.Text = string.Empty;

                if (!Utility.IsNullOrEmpty(SearchTimeSlotRequest_timeSlotStatusHiddenField.Value) &&
                    SearchTimeSlotRequest_timeSlotStatusHiddenField.Value.Trim() == "A")
                {
                    // Show a success message.
                    base.ShowMessage(SearchTimeSlotRequest_topSuccessMessageLabel,
                        SearchTimeSlotRequest_bottomSuccessMessageLabel, "Time slot approved successfully");
                } 
                else if (!Utility.IsNullOrEmpty(SearchTimeSlotRequest_timeSlotStatusHiddenField.Value) &&
                    SearchTimeSlotRequest_timeSlotStatusHiddenField.Value.Trim() == "V")
                {
                    // Show a success message.
                    base.ShowMessage(SearchTimeSlotRequest_topSuccessMessageLabel,
                        SearchTimeSlotRequest_bottomSuccessMessageLabel, "Time slot updated successfully");
                } 
                else 
                {
                    // Show a success message.
                    base.ShowMessage(SearchTimeSlotRequest_topSuccessMessageLabel,
                        SearchTimeSlotRequest_bottomSuccessMessageLabel, "Time slot proposed successfully");
                }

                LoadTimeSlotRequests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the accept button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTimeSlotRequest_acceptButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearLabelMessage();

                int count = 0;
                int timeSlotID = 0;
                CheckBox selectCheckBox = null;

                // Loop through the rows and identify the checked rows.
                foreach (GridViewRow row in SearchTimeSlotRequest_timeSlotsGridView.Rows)
                {
                    timeSlotID = Convert.ToInt32((row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_timeSlotIDHiddenField") as HiddenField).Value);
                    selectCheckBox = row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_selectCheckBox") as CheckBox;

                    if (!selectCheckBox.Checked)
                        continue;

                    count++;

                    // Accept the time slot.
                    //new AssessorBLManager().UpdateAssessorTimeSlotRequestStatus(timeSlotID, 
                       // Constants.TimeSlotRequestStatus.ACCEPTED_CODE, base.userID);
                }

                if (count == 0)
                {
                    base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                        SearchTimeSlotRequest_bottomErrorMessageLabel, "No time slots are selected");
                }
                else
                {
                    base.ShowMessage(SearchTimeSlotRequest_topSuccessMessageLabel,
                        SearchTimeSlotRequest_bottomSuccessMessageLabel,
                        string.Format("{0} time slot(s) accepted", count));

                    // Re-apply search.
                    LoadTimeSlotRequests(1);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the reject button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTimeSlotRequest_rejectButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearLabelMessage();

                int count = 0;
                int timeSlotID = 0;
                CheckBox selectCheckBox = null;

                // Loop through the rows and identify the checked rows.
                foreach (GridViewRow row in SearchTimeSlotRequest_timeSlotsGridView.Rows)
                {
                    timeSlotID = Convert.ToInt32((row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_timeSlotIDHiddenField") as HiddenField).Value);
                    selectCheckBox = row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_selectCheckBox") as CheckBox;

                    if (!selectCheckBox.Checked)
                        continue;

                    count++;
                    // Reject the time slot.
                    //new AssessorBLManager().UpdateAssessorTimeSlotRequestStatus(timeSlotID, 
                    //    Constants.TimeSlotRequestStatus.REJECTED_CODE, base.userID);
                }

                if (count == 0)
                {
                    base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                        SearchTimeSlotRequest_bottomErrorMessageLabel, "No time slots are selected");
                }
                else
                {
                    base.ShowMessage(SearchTimeSlotRequest_topSuccessMessageLabel,
                        SearchTimeSlotRequest_bottomSuccessMessageLabel,
                        string.Format("{0} time slot(s) rejected", count));

                    // Re-apply search.
                    LoadTimeSlotRequests(1);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// It will clear all the session values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchTimeSlotRequest_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/InterviewHome.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the reset link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTimeSlotRequest_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                base.ClearSearchCriteriaSession();
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void SearchTimeSlotRequest_timeSlotsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchTimeSlotRequest_timeSlotsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void SearchTimeSlotRequest_timeSlotsGridView_RowCommand
           (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "Accept" && e.CommandName != "Reject")
                    return;

                // Clear messages.
                ClearLabelMessage();

                int index = Convert.ToInt32(e.CommandArgument);
                int timeSlotID = Convert.ToInt32((SearchTimeSlotRequest_timeSlotsGridView.Rows
                    [index].FindControl("SearchTimeSlotRequest_timeSlotsGridView_timeSlotIDHiddenField") as HiddenField).Value);

                AssessorTimeSlotDetail timeSlotDetail = new AssessorTimeSlotDetail();

                timeSlotDetail.RequestDateGenID = timeSlotID;
                timeSlotDetail.RequestStatus = Constants.TimeSlotRequestStatus.REJECTED_CODE;
                timeSlotDetail.IsAvailableFullTime = null;
                timeSlotDetail.InitiatedBy = base.userID;

                // Reject the time slot.
                new AssessorBLManager().UpdateAssessorAvailabilityDates(timeSlotDetail);

                // While reject send mai
                AssessorDetail assessorMailDetail = new AssessorDetail();
                assessorMailDetail.FirstName = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                assessorMailDetail.LastName = ((UserDetail)Session["USER_DETAIL"]).LastName;
                assessorMailDetail.RequestedSlotID = timeSlotID;

                //Send mail to the requested user
                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendRejectedSlotMailToRequester);
                IAsyncResult result = taskDelegate.BeginInvoke(assessorMailDetail,
                    EntityType.RejectRequestedAvailabilityDate, new AsyncCallback(SendRejectedSlotMailToRequesterCallBack),
                    taskDelegate);
                // End send mail


                // Show a success message.
                base.ShowMessage(SearchTimeSlotRequest_topSuccessMessageLabel,
                    SearchTimeSlotRequest_bottomSuccessMessageLabel, "Time slot rejected");

                // Re-apply search.
                LoadTimeSlotRequests(Convert.ToInt32(ViewState["PAGENUMBER"]));
                
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchTimeSlotRequest_timeSlotsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchTimeSlotRequest_pagingNavigator.Reset();

                //Set question details based on the values
                LoadTimeSlotRequests(1);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchTimeSlotRequest_timeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int id = Convert.ToInt32(((HiddenField)e.Row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_timeSlotIDHiddenField")).Value);
                    HiddenField SearchTimeSlotRequest_timeSlotsGridView_requestStatusHiddenField = (HiddenField)e.Row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_requestStatusHiddenField");
                    ImageButton SearchTimeSlotRequest_timeSlotsGridView_acceptTimeSlotImageButton = (ImageButton)e.Row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_acceptTimeSlotImageButton");
                    ImageButton SearchTimeSlotRequest_timeSlotsGridView_rejectTimeSlotImageButton = (ImageButton)e.Row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_rejectTimeSlotImageButton");
                    CheckBox SearchTimeSlotRequest_timeSlotsGridView_selectCheckBox = (CheckBox)e.Row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_selectCheckBox");

                    HtmlContainerControl SearchTimeSlotRequest_timeSlotsGridView_detailsDiv =
                        (HtmlContainerControl)e.Row.FindControl("SearchTimeSlotRequest_timeSlotsGridView_detailsDiv");

                    HtmlAnchor SearchTimeSlotRequest_timeSlotsGridView_detailsViewFocusDownLink =
                        (HtmlAnchor)e.Row.FindControl("SearchTimeSlotRequest_timeSlotsGridView_detailsViewFocusDownLink");

                    // Assign handler to view/edit time slots
                    ImageButton SearchTimeSlotRequest_timeSlotsGridView_editTimeSlotImageButton = e.Row.FindControl
                        ("SearchTimeSlotRequest_timeSlotsGridView_editTimeSlotImageButton") as ImageButton;
                    
                    // Show or hide the accept/reject image icons
                    if (SearchTimeSlotRequest_timeSlotsGridView_requestStatusHiddenField.Value == "I")
                    {
                        SearchTimeSlotRequest_timeSlotsGridView_acceptTimeSlotImageButton.Visible = true;
                        SearchTimeSlotRequest_timeSlotsGridView_rejectTimeSlotImageButton.Visible = true;
                    }
                    else if (SearchTimeSlotRequest_timeSlotsGridView_requestStatusHiddenField.Value == "A")
                    {
                        SearchTimeSlotRequest_timeSlotsGridView_acceptTimeSlotImageButton.Visible = false;
                        SearchTimeSlotRequest_timeSlotsGridView_rejectTimeSlotImageButton.Visible = false;
                        SearchTimeSlotRequest_timeSlotsGridView_editTimeSlotImageButton.Visible = true;
                    }
                    else if (SearchTimeSlotRequest_timeSlotsGridView_requestStatusHiddenField.Value == "R")
                    {
                        SearchTimeSlotRequest_timeSlotsGridView_acceptTimeSlotImageButton.Visible = false;
                        SearchTimeSlotRequest_timeSlotsGridView_rejectTimeSlotImageButton.Visible = false;
                    }

                    // Show selection checkbox, if single status is searched.
                    if (SearchTimeSlotRequest_statusDropDownList.SelectedValue == "I" ||
                        SearchTimeSlotRequest_statusDropDownList.SelectedValue == "A" ||
                        SearchTimeSlotRequest_statusDropDownList.SelectedValue == "R")
                    {
                        SearchTimeSlotRequest_timeSlotsGridView_selectCheckBox.Visible = false;
                    }
                    else
                    {
                        SearchTimeSlotRequest_timeSlotsGridView_selectCheckBox.Visible = false;
                    }

                    SearchTimeSlotRequest_timeSlotsGridView_editTimeSlotImageButton.Attributes.Add("OnClick", "javascript:return ShowAddTimeSlot('" + SearchTimeSlotRequest_timeSlotStatusHiddenField.ClientID + "','" +
                        SearchTimeSlotRequest_proposeTimeSlotButton.ClientID + "','V','" + id + "')");

                    SearchTimeSlotRequest_timeSlotsGridView_acceptTimeSlotImageButton.Attributes.Add("OnClick", "javascript:return ShowAddTimeSlot('" + SearchTimeSlotRequest_timeSlotStatusHiddenField.ClientID + "','" +
                        SearchTimeSlotRequest_proposeTimeSlotButton.ClientID + "','A','" + id + "')");
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel, exception.Message);
            }
        }

        

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            // Load status dropdown list.
            SearchTimeSlotRequest_statusDropDownList.Items.Clear();
            SearchTimeSlotRequest_statusDropDownList.Items.Add(new ListItem("All", ""));
            SearchTimeSlotRequest_statusDropDownList.Items.Add(new ListItem
                (Constants.TimeSlotRequestStatus.PENDING_NAME, Constants.TimeSlotRequestStatus.PENDING_CODE));
            SearchTimeSlotRequest_statusDropDownList.Items.Add(new ListItem
                (Constants.TimeSlotRequestStatus.ACCEPTED_NAME, Constants.TimeSlotRequestStatus.ACCEPTED_CODE));
            SearchTimeSlotRequest_statusDropDownList.Items.Add(new ListItem
                (Constants.TimeSlotRequestStatus.REJECTED_NAME, Constants.TimeSlotRequestStatus.REJECTED_CODE));

            if (type == "P")
            {
                // By default select the 'Pending' value.
                SearchTimeSlotRequest_statusDropDownList.SelectedValue = Constants.
                    TimeSlotRequestStatus.PENDING_CODE;
            }
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            return true;
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the time slots for the given page number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holdst the page number.
        /// </param>
        private void LoadTimeSlotRequests(int pageNumber)
        {
            int totalRecords = 0;

            // Construct search criteria.
            AssessorTimeSlotSearchCriteria criteria = new AssessorTimeSlotSearchCriteria();

            criteria.AssessorID = base.userID;
            if (!Utility.IsNullOrEmpty(SearchTimeSlotRequest_statusDropDownList.SelectedValue))
                criteria.RequestStatus = SearchTimeSlotRequest_statusDropDownList.SelectedValue.Trim();
            else
                criteria.RequestStatus = string.Empty;

            if (!Utility.IsNullOrEmpty(SearchTimeSlotRequest_initiatedByHiddenField.Value))
                criteria.InitiatedBy = Convert.ToInt32(SearchTimeSlotRequest_initiatedByHiddenField.Value);

            criteria.InitiatedDate = SearchTimeSlotRequest_initiatedDateTextBox.Text.Trim() == string.Empty ?
               DateTime.MinValue : Convert.ToDateTime(SearchTimeSlotRequest_initiatedDateTextBox.Text.Trim());

            criteria.RequestedDate = SearchTimeSlotRequest_requestedDateTextBox.Text.Trim() == string.Empty ?
                DateTime.MinValue : Convert.ToDateTime(SearchTimeSlotRequest_requestedDateTextBox.Text.Trim());

            List<AssessorTimeSlotDetail> timeSlots = new AssessorBLManager().GetTimeSlotRequest
                (criteria, pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                (SortType)ViewState["SORT_ORDER"], out totalRecords);

            if (timeSlots == null || timeSlots.Count == 0)
            {
                base.ShowMessage(SearchTimeSlotRequest_topErrorMessageLabel,
                    SearchTimeSlotRequest_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);

                SearchTimeSlotRequest_acceptButton.Visible = false;
                SearchTimeSlotRequest_rejectButton.Visible = false;
            }
            else
            {
                // Show or hide the accept/reject buttons.
                if (SearchTimeSlotRequest_statusDropDownList.SelectedValue == "I")
                {
                    SearchTimeSlotRequest_acceptButton.Visible = false;
                    SearchTimeSlotRequest_rejectButton.Visible = false;
                }
                else if (SearchTimeSlotRequest_statusDropDownList.SelectedValue == "A")
                {
                    SearchTimeSlotRequest_acceptButton.Visible = false;
                    SearchTimeSlotRequest_rejectButton.Visible = false;
                }
                else if (SearchTimeSlotRequest_statusDropDownList.SelectedValue == "R")
                {
                    SearchTimeSlotRequest_acceptButton.Visible = false;
                    SearchTimeSlotRequest_rejectButton.Visible = false;
                }
                else
                {
                    SearchTimeSlotRequest_acceptButton.Visible = false;
                    SearchTimeSlotRequest_rejectButton.Visible = false;
                } 
            }

            SearchTimeSlotRequest_timeSlotsGridView.DataSource = timeSlots;
            SearchTimeSlotRequest_timeSlotsGridView.DataBind();
            SearchTimeSlotRequest_pagingNavigator.TotalRecords = totalRecords;
            SearchTimeSlotRequest_pagingNavigator.PageSize = base.GridPageSize;
        }

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            SearchTimeSlotRequest_topSuccessMessageLabel.Text = string.Empty;
            SearchTimeSlotRequest_bottomSuccessMessageLabel.Text = string.Empty;
            SearchTimeSlotRequest_topErrorMessageLabel.Text = string.Empty;
            SearchTimeSlotRequest_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(SearchTimeSlotRequest_isMaximizedHiddenField.Value) &&
                SearchTimeSlotRequest_isMaximizedHiddenField.Value == "Y")
            {
                SearchTimeSlotRequest_searchCriteriasDiv.Style["display"] = "none";
                SearchTimeSlotRequest_searchResultsUpSpan.Style["display"] = "block";
                SearchTimeSlotRequest_searchResultsDownSpan.Style["display"] = "none";
                SearchTimeSlotRequest_timeSlotsGridViewDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchTimeSlotRequest_searchCriteriasDiv.Style["display"] = "block";
                SearchTimeSlotRequest_searchResultsUpSpan.Style["display"] = "none";
                SearchTimeSlotRequest_searchResultsDownSpan.Style["display"] = "block";
                SearchTimeSlotRequest_timeSlotsGridViewDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Method that search for time slots.
        /// </summary>
        private void SearchTimeSlots()
        {
            ClearLabelMessage();
            SearchTimeSlotRequest_pagingNavigator.Reset();
            ViewState["PAGENUMBER"] = "1";
            ViewState["SORT_ORDER"] = SortType.Descending;
            ViewState["SORT_FIELD"] = "REQUEST_STATUS";
            LoadTimeSlotRequests(1);
        }

        /// <summary>
        /// Method that send email to the requested assessor
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the interview details
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendRejectedSlotMailToRequester(AssessorDetail assessorDetail,
            EntityType entityType)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(entityType, assessorDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendRejectedSlotMailToRequesterCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Private Methods
    }
}