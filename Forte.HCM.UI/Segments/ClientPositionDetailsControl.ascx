﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPositionDetailsControl.ascx.cs"
    Inherits="Forte.HCM.UI.Segments.ClientPositionDetailsControl" %>
  <script  type="text/javascript">
     function dateSelectionChanged(sender, args) {
            debugger;
           
            let visibleDate = sender._visibleDate.getDateOnly().format("dd-MM-yyyy");
              
                // set the date back to the current date
            sender._textbox.set_Value(visibleDate);
            
        }
</script>
    
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ClientPositionDetailsControl_editTable"
    runat="server" visible="true">
    <tr style="display:none">
        <td class="header_bg">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 75%" class="header_text_bold" valign="middle" align="left">
                        <asp:Literal ID="ClientPositionDetailsControl_headerLiteral" runat="server" Text="Position Details"></asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td ><%--class="grid_body_bg"--%>
            <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                <tr>
                    <td>
                        <table width="100%" cellpadding="3" cellspacing="0" class="panel_inner_body_bg" border="0">
                            <tr class="td_height_20">
                                <td>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 12%;display:none">
                                                <asp:Label ID="ClientPositionDetailsControl_positionNameLabel" runat="server" Text="Position Name"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 17%;display:none">
                                                <asp:TextBox ID="ClientPositionDetailsControl_positionNameTextBox" runat="server"
                                                    MaxLength="50" Width="98%"></asp:TextBox>
                                            </td>
                                            <td style="width: 9%;display:none">
                                                <asp:Label ID="ClientPositionDetailsControl_positionIdLabel" runat="server" Text="Position ID"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 14%;display:none">
                                                <asp:TextBox ID="ClientPositionDetailsControl_positionIdTextBox" runat="server" MaxLength="50"
                                                    Width="90%"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="ClientPositionDetailsControl_targetedStartDateLabel" runat="server"
                                                    Text="Targeted Start Date" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <div style="float: left; padding: right 2px; width: 80%">
                                                    <asp:TextBox ID="ClientPositionDetailsControl_targetedStartDateTextBox" runat="server"
                                                        AutoCompleteType="None" Columns="17" Width="92%" MaxLength="10"></asp:TextBox>
                                                </div>
                                                <div style="float: left; width: 20%;">
                                                    <asp:ImageButton ID="ClientPositionDetailsControl_calendarImageButton" SkinID="sknCalendarImageButton"
                                                        runat="server" ImageAlign="Left" />
                                                </div>
                                                <div >
                                                    <ajaxToolKit:MaskedEditExtender ID="ClientPositionDetailsControl_maskedEditExtender"
                                                        runat="server" TargetControlID="ClientPositionDetailsControl_targetedStartDateTextBox"
                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                        ErrorTooltipEnabled="True" />
                                                    <ajaxToolKit:MaskedEditValidator ID="ClientPositionDetailsControl_maskedEditValidator"
                                                        runat="server" ControlExtender="ClientPositionDetailsControl_maskedEditExtender"
                                                        ControlToValidate="ClientPositionDetailsControl_targetedStartDateTextBox" EmptyValueMessage="Date is required"
                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                    <ajaxToolKit:CalendarExtender ID="ClientPositionDetailsControl_customCalendarExtender"  OnClientDateSelectionChanged="dateSelectionChanged"
                                                        runat="server" TargetControlID="ClientPositionDetailsControl_targetedStartDateTextBox"
                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="TopLeft" 
                                                         PopupButtonID="ClientPositionDetailsControl_calendarImageButton" />
                                                </div>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="ClientPositionDetailsControl_numberOfPositionsLabel" runat="server"
                                                    Text="Number Of Positions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 6%">
                                                <asp:TextBox ID="ClientPositionDetailsControl_numberOfPositionsTextBox" runat="server"
                                                    MaxLength="3" Width="25px"></asp:TextBox>
                                                <ajaxToolKit:FilteredTextBoxExtender ID="ClientPositionDetailsControl_numberOfPositionsFilteredTextBoxExtender"
                                                    runat="server" FilterType="Numbers" TargetControlID="ClientPositionDetailsControl_numberOfPositionsTextBox">
                                                </ajaxToolKit:FilteredTextBoxExtender>
                                            </td>
                                            <Td width="49%"></Td>
                                        </tr>
                                        
                                    </table>
                                </td>
                            </tr>
                            <tr class="td_height_20">
                                <td>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 12%" align="left">
                                                <asp:Label ID="ClientPositionDetailsControl_natureOfPositionsLabel" runat="server"
                                                    Text="Nature Of Positions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 80%" align="left">
                                                <table cellpadding="0" cellspacing="0" width="40%" border="0" class="checkbox_list_bg" >
                                                    <tr>
                                                        <td style="width: 22%;" align="right">
                                                            <%-- <div style="width: 50%; height: 15%; overflow: auto;">--%>
                                                            <asp:CheckBoxList ID="ClientPositionDetailsControl_natureOfPositionsCheckBoxList"
                                                                runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Contract" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Direct Hire" Value="2"></asp:ListItem>
                                                            </asp:CheckBoxList>
                                                            <%--  </div>--%>
                                                        </td>
                                                        <td style="width: 14%" align="left">
                                                            <asp:CheckBox ID="ClientPositionDetailsControl_contractToHireCheckBox" runat="server"
                                                                Text="Contract To Hire" CssClass="check_box_text" />
                                                        </td>
                                                        <td style="width: 30%" align="left">
                                                            <div id="ClientPositionDetailsControl_durationDIV" runat="server" style="display: block;">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="ClientPositionDetailsControl_durationLabel" runat="server" Text="Duration Of Contract"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%">
                                                                            <div style="float: left;">
                                                                                <asp:TextBox ID="ClientPositionDetailsControl_durationTextBox" runat="server" MaxLength="3"
                                                                                    Columns="3"></asp:TextBox>
                                                                                <ajaxToolKit:FilteredTextBoxExtender ID="ClientPositionDetailsControl_durationFilteredTextBoxExtender"
                                                                                    runat="server" FilterType="Numbers" TargetControlID="ClientPositionDetailsControl_durationTextBox">
                                                                                </ajaxToolKit:FilteredTextBoxExtender>
                                                                            </div>
                                                                            <div style="float: left; padding-left: 2px">
                                                                                <asp:ImageButton ID="ClientPositionDetailsControl_durationImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter duration in months" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table id="ClientPositionDetailsControl_previewTable" runat="server" visible="false"
    width="100%">
    <tr>
        <td class="grid_body_bg">
            <table width="100%" cellpadding="2" cellspacing="2" class="panel_bg">
                <tr>
                    <td class="header_text_bold td_height_20">
                        Client Position Details
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" class="panel_inner_body_bg">
                            <tr class="td_height_20" style="display:none">
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewPositionNameLabel" runat="server"
                                        Text="Position Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewPositionNameTextLabel" runat="server"
                                        Text="Position Name" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewPositionIDLabel" runat="server"
                                        Text="Position ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewPositionIDTextLabel" runat="server"
                                        Text="Position Id" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr class="td_height_20">
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewTargetDateLabel" runat="server"
                                        Text="Targeted Start Date" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td style="width: 15%">
                                    <asp:Label ID="ClientPositionDetailsControl_previewTargetDateTextLabel" runat="server"
                                        Text="Date" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewNoOfPositionLabel" runat="server"
                                        Text="Number Of Positions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewNoOfPositionTextLabel" runat="server"
                                        Text="No Position Name" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr class="td_height_20">
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewNatureOfPositionLabel" runat="server"
                                        Text="Nature Of Positions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewNatureOfPositionTextLabel" runat="server"
                                        Text="Position Name" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewDurationLabel" runat="server"
                                        Text="Contract To Hire Duration" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="ClientPositionDetailsControl_previewDurationTextLabel" runat="server"
                                        Text="Position Name" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
