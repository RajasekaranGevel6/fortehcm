﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Segments {
    
    
    public partial class ClientPositionDetailsControl {
        
        /// <summary>
        /// ClientPositionDetailsControl_editTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable ClientPositionDetailsControl_editTable;
        
        /// <summary>
        /// ClientPositionDetailsControl_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ClientPositionDetailsControl_headerLiteral;
        
        /// <summary>
        /// ClientPositionDetailsControl_positionNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_positionNameLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_positionNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ClientPositionDetailsControl_positionNameTextBox;
        
        /// <summary>
        /// ClientPositionDetailsControl_positionIdLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_positionIdLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_positionIdTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ClientPositionDetailsControl_positionIdTextBox;
        
        /// <summary>
        /// ClientPositionDetailsControl_targetedStartDateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_targetedStartDateLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_targetedStartDateTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ClientPositionDetailsControl_targetedStartDateTextBox;
        
        /// <summary>
        /// ClientPositionDetailsControl_calendarImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientPositionDetailsControl_calendarImageButton;
        
        /// <summary>
        /// ClientPositionDetailsControl_maskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender ClientPositionDetailsControl_maskedEditExtender;
        
        /// <summary>
        /// ClientPositionDetailsControl_maskedEditValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditValidator ClientPositionDetailsControl_maskedEditValidator;
        
        /// <summary>
        /// ClientPositionDetailsControl_customCalendarExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CalendarExtender ClientPositionDetailsControl_customCalendarExtender;
        
        /// <summary>
        /// ClientPositionDetailsControl_numberOfPositionsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_numberOfPositionsLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_numberOfPositionsTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ClientPositionDetailsControl_numberOfPositionsTextBox;
        
        /// <summary>
        /// ClientPositionDetailsControl_numberOfPositionsFilteredTextBoxExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.FilteredTextBoxExtender ClientPositionDetailsControl_numberOfPositionsFilteredTextBoxExtender;
        
        /// <summary>
        /// ClientPositionDetailsControl_natureOfPositionsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_natureOfPositionsLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_natureOfPositionsCheckBoxList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList ClientPositionDetailsControl_natureOfPositionsCheckBoxList;
        
        /// <summary>
        /// ClientPositionDetailsControl_contractToHireCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox ClientPositionDetailsControl_contractToHireCheckBox;
        
        /// <summary>
        /// ClientPositionDetailsControl_durationDIV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ClientPositionDetailsControl_durationDIV;
        
        /// <summary>
        /// ClientPositionDetailsControl_durationLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_durationLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_durationTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ClientPositionDetailsControl_durationTextBox;
        
        /// <summary>
        /// ClientPositionDetailsControl_durationFilteredTextBoxExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.FilteredTextBoxExtender ClientPositionDetailsControl_durationFilteredTextBoxExtender;
        
        /// <summary>
        /// ClientPositionDetailsControl_durationImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientPositionDetailsControl_durationImageButton;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable ClientPositionDetailsControl_previewTable;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewPositionNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewPositionNameLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewPositionNameTextLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewPositionNameTextLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewPositionIDLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewPositionIDLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewPositionIDTextLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewPositionIDTextLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewTargetDateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewTargetDateLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewTargetDateTextLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewTargetDateTextLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewNoOfPositionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewNoOfPositionLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewNoOfPositionTextLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewNoOfPositionTextLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewNatureOfPositionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewNatureOfPositionLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewNatureOfPositionTextLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewNatureOfPositionTextLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewDurationLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewDurationLabel;
        
        /// <summary>
        /// ClientPositionDetailsControl_previewDurationTextLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientPositionDetailsControl_previewDurationTextLabel;
    }
}
