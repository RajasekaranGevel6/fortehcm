﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoleRequirementControl.ascx.cs"
    Inherits="Forte.HCM.UI.Segments.RoleRequirement" %>
<table id="RoleRequirementControl_editTable" runat="server" width="100%" cellpadding="0" cellspacing="0">
    <tr style="display:none">
        <td class="header_bg">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 75%" class="header_text_bold" valign="middle" align="left">
                        <asp:Literal ID="RoleRequirementControl_headerLiteral" runat="server" Text="Role Requirement"></asp:Literal>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>  <%--class="grid_body_bg"--%>
            <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="3" class="panel_inner_body_bg" border="0">
                            <tr>
                                <td class="tab_body_bg">
                                    <table width="100%" cellpadding="1" cellspacing="3" border="0">
                                        <tr>
                                            <td style="width: 10%">
                                                <asp:Label ID="RoleRequirementControl_primaryRoleLabel" runat="server" Text="Primary Role"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 50%">
                                                <div style="float: left; width: 88%">
                                                    <asp:TextBox ID="RoleRequirementControl_primaryRoleTextBox" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
                                                </div>
                                                <div style="float: left; padding-left: 5px">
                                                    <asp:ImageButton ID="RoleRequirementControl_primaryRolekeywordsImageButton" SkinID="sknHelpImageButton"
                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter Primary Role,Example – Architect, DBA" /></div>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:Label ID="RoleRequirementControl_noOfExperienceLabel" runat="server" Text="No Of Years Of Experience Required"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 17%">
                                                <div style="float: left; width: 30%">
                                                    <asp:TextBox ID="RoleRequirementControl_noOfExperienceTextBox" runat="server" Width="90%" MaxLength="4"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="RoleRequirementControl_noOfExperienceFilteredTextBoxExtender"
                                                        runat="server" FilterType="Numbers" TargetControlID="RoleRequirementControl_noOfExperienceTextBox">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </div>
                                                <div style="float: left; padding-left: 5px">
                                                    <asp:ImageButton ID="RoleRequirementControl_noOfExperienceImageButton" SkinID="sknHelpImageButton"
                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter Primary Role Experience" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%">
                                                <asp:Label ID="RoleRequirementControl_secondaryRoleLabel" runat="server" Text="Secondary Role"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <div style="float: left; width: 88%">
                                                    <asp:TextBox ID="RoleRequirementControl_secondaryRoleTextBox" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
                                                </div>
                                                <div style="float: left; padding-left: 5px">
                                                    <asp:ImageButton ID="RoleRequirementControl_secondaryRoleKeywordsImageButton" SkinID="sknHelpImageButton"
                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter Secondary Role, Example – Architect, DBA" />
                                                </div>
                                            </td>
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_noOfSecondaryExperienceLabel" runat="server"
                                                    Text="No Of Years Of Experience Required" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 17%">
                                                <div style="float: left; width: 30%">
                                                    <asp:TextBox ID="RoleRequirementControl_noOfSecondaryExperienceTextBox" runat="server"
                                                        Width="90%" MaxLength="4"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="RoleRequirementControl_secondaryRoleFilteredTextBoxExtender"
                                                        runat="server" FilterType="Numbers" TargetControlID="RoleRequirementControl_noOfSecondaryExperienceTextBox">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </div>
                                                <div style="float: left; padding-left: 5px">
                                                    <asp:ImageButton ID="RoleRequirementControl_noOfSecondaryExperienceImageButton" SkinID="sknHelpImageButton"
                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter Secondary Role Experience" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="RoleRequirementControl_additionalRoleCheckBox" runat="server" />
                                    <asp:Label ID="RoleRequirementControl_additionalRoleLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Additional Role Requirement Information"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="RoleRequirementControl_additionalRoleDiv" runat="server" style="display: none">
                                        <asp:TextBox ID="RoleRequirementControl_additionalRoleTextBox" runat="server" SkinID="sknMultiLineTextBox"
                                            Width="99.5%" Height="50px" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="RoleRequirementControl_previewTable" runat="server" visible="false" width="100%">
    <tr>
        <td class="grid_body_bg">
            <table width="100%" cellpadding="2" cellspacing="2" class="panel_bg">
                <tr>
                    <td class="header_text_bold td_height_20">
                        Role Requirement Details
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td class="tab_body_bg">
                                    <table width="100%" cellpadding="1" cellspacing="3" border="0">
                                        <tr class="td_height_20">
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_previewPrimaryRoleLabel" runat="server" Text="Primary Role"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_previewPrimaryRoleTextLabel" runat="server"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_previewNoOfExperienceLabel" runat="server"
                                                    Text="No Of Years Of Experience Required" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_previewNoOfExperienceTextLabel" runat="server"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="td_height_20">
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_PreviewSecondaryRoleLabel" runat="server" Text="Secondary Role"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_previewSecondaryRoleTextLabel" runat="server"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_previewNoOfSecondaryExperienceLabel" runat="server"
                                                    Text="No Of Years Of Experience Required" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="RoleRequirementControl_previewNoOfSecondaryExperienceTextLabel" runat="server"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" colspan="4">
                                                <asp:Label ID="RoleRequirementControl_previewAditionalLabel" runat="server" Text="Additional Role Information"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" colspan="4">
                                                <div style="overflow:auto;height:100px;word-wrap: break-word;white-space:normal;">
                                                    <asp:Label ID="RoleRequirementControl_previewAditionalTextLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
