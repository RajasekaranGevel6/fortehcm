﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TechnicalSkillRequirementControl.ascx.cs"
    Inherits="Forte.HCM.UI.Segments.TechnicalSkillRequirement" %>
<asp:HiddenField ID="TechnicalSkillRequirementControll_gridWeightageHiddenField"
    runat="server" />
<table width="100%" cellpadding="0" cellspacing="0" id="TechnicalSkillRequirementControl_editTable"
    runat="server">
    <tr style="display: none">
        <td class="header_bg">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 75%" class="header_text_bold" valign="middle" align="left">
                        <asp:Literal ID="TechnicalSkillRequirementControl_headerLiteral" runat="server" Text="Technical Skills Requirement"></asp:Literal>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                <%--class="grid_body_bg"--%>
                <tr>
                    <td class="panel_bg">
                        <table width="100%" cellpadding="2" cellspacing="0" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:Label ID="TechnicalSkillRequirementControl_numberOfYearsLabel" runat="server"
                                                    Text="Total Years Of IT Experience Required" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <div style="float: left; padding-right: 5px;">
                                                    <asp:TextBox ID="TechnicalSkillRequirementControl_numberOfYearsTextBox" runat="server"
                                                        MaxLength="4" Width="25px"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="TechnicalSkillRequirementControl_experienceFilteredTextBoxExtender"
                                                        runat="server" FilterType="Custom,Numbers" TargetControlID="TechnicalSkillRequirementControl_numberOfYearsTextBox"
                                                        ValidChars=".">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </div>
                                                <div style="float: left;">
                                                    <asp:ImageButton ID="TechnicalSkillRequirementControl_numberOfYearsImageButton" SkinID="sknHelpImageButton"
                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter the total number of years of IT experience required" />
                                                </div>
                                            </td>
                                            <td style="width: 35%" align="right">
                                                <asp:Label ID="TechnicalSkillRequirementControl_totalweightageLabel" runat="server"
                                                    Text="Total Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 5%" align="right">
                                                <asp:TextBox ID="TechnicalSkillRequirementControl_totalWeightageTextBox" runat="server"
                                                    MaxLength="3" Width="25px" AutoPostBack="false"></asp:TextBox>
                                                <ajaxToolKit:FilteredTextBoxExtender ID="TechnicalSkillRequirementControl_weightageFilteredTextBoxExtender"
                                                    runat="server" FilterType="Numbers" TargetControlID="TechnicalSkillRequirementControl_totalweightageTextBox">
                                                </ajaxToolKit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                        <tr>
                                            <td>
                                                <div id="TechnicalSkillRequirementControl_technicalSkillDiv" style="width: 100%;
                                                    height: 101px; overflow: auto;" runat="server">
                                                    <asp:GridView ID="TechnicalSkillRequirementControl_technicalSkillGridView" runat="server"
                                                        AutoGenerateColumns="false" ShowHeader="true" SkinID="sknWrapHeaderGrid" OnRowCommand="TechnicalSkillRequirementControl_technicalSkillGridView_RowCommand"
                                                        OnRowDataBound="TechnicalSkillRequirementControl_technicalSkillGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="TechnicalSkillRequirementControl_technicalSkillGridView_deleteSkillsImageButton"
                                                                        runat="server" SkinID="sknDeleteImageButton" ToolTip="Delete Skill" CommandName="DeleteSkill" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="15%" />
                                                                <ItemStyle Width="30%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="TechnicalSkillRequirementControl_technicalSkillGridView_skillLabel"
                                                                        runat="server" Text="Skill Name"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="TechnicalSkillRequirementControl_technicalSkillGridView_skillTextBox"
                                                                        runat="server" Width="90%" Text='<%# Eval("SkillName") %>' MaxLength="50"></asp:TextBox>
                                                                    <ajaxToolKit:AutoCompleteExtender ID="SearchCategorySubjectControl_skillNameAutoCompleteExtender"
                                                                        runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GeSkillNameKeywordByCategory"
                                                                        TargetControlID="TechnicalSkillRequirementControl_technicalSkillGridView_skillTextBox"
                                                                        MinimumPrefixLength="0" CompletionListElementID="pn2" CompletionListCssClass="autocomplete_completionListElement"
                                                                        EnableCaching="true" CompletionSetCount="12" UseContextKey="true">
                                                                    </ajaxToolKit:AutoCompleteExtender>
                                                                    <asp:Panel ID="pn2" runat="server">
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="15%" />
                                                                <ItemStyle Width="30%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryLabel"
                                                                        runat="server" Text="Skill Category"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryTextBox"
                                                                        runat="server" Width="90%" Text='<%# Eval("CategoryName") %>' MaxLength="50"></asp:TextBox>
                                                                    <ajaxToolKit:AutoCompleteExtender ID="SearchCategorySubjectControl_categoryAutoCompleteExtender"
                                                                        runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetSkillCategoryKeywords"
                                                                        TargetControlID="TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryTextBox"
                                                                        MinimumPrefixLength="0" CompletionListElementID="pnl" CompletionListCssClass="autocomplete_completionListElement"
                                                                        EnableCaching="true" CompletionSetCount="12">
                                                                    </ajaxToolKit:AutoCompleteExtender>
                                                                    <asp:Panel ID="pnl" runat="server">
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="11%" />
                                                                <ItemStyle Width="10%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="TechnicalSkillRequirementControl_technicalSkillGridView_experienceLabel"
                                                                        runat="server" Text="No Of Years Exp Req"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="TechnicalSkillRequirementControl_technicalSkillGridView_experienceTextBox"
                                                                        runat="server" Width="50%" MaxLength="4" Text='<%# Eval("NoOfYearsExp") %>'></asp:TextBox>
                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="TechnicalSkillRequirementControl_technicalSkillGridView_experienceFilteredTextBoxExtender"
                                                                        runat="server" FilterType="Custom,Numbers" TargetControlID="TechnicalSkillRequirementControl_technicalSkillGridView_experienceTextBox"
                                                                        ValidChars=".">
                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="10%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="TechnicalSkillRequirementControl_technicalSkillGridView_skillLevelLabel"
                                                                        runat="server" Text="Skill Level Required"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="TechnicalSkillRequirementControl_technicalSkillGridView_skillLevelDropdownList"
                                                                        runat="server">
                                                                        <asp:ListItem Text="Expert" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Intermediate" Value="2">
                                                                        </asp:ListItem>
                                                                        <asp:ListItem Text="Beginner" Value="3"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="10%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="TechnicalSkillRequirementControl_technicalSkillGridView_mandatoryLabel"
                                                                        runat="server" Text="Mandatory / Desired"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="TechnicalSkillRequirementControl_technicalSkillGridView_mandatoryDropDownList"
                                                                        runat="server">
                                                                        <asp:ListItem Text="Mandatory" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Desired" Value="1"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                                                <HeaderStyle Width="10%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="TechnicalSkillRequirementControl_technicalSkillGridView_certificationLabel"
                                                                        runat="server" Text="Certification Required"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="TechnicalSkillRequirementControl_technicalSkillGridView_certificationCheckBox"
                                                                        runat="server" Checked='<%# Convert.ToBoolean(Eval("CertificationRequired")) %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="10%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="TechnicalSkillRequirementControl_technicalSkillGridView_weightageLabel"
                                                                        runat="server" Text="Weightage"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox"
                                                                        runat="server" Width="50%" MaxLength="3" Text='<%# Eval("Weightage") %>'></asp:TextBox>
                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="TechnicalSkillRequirementControl_technicalSkillGridView_weightageFilteredTextBoxExtender"
                                                                        runat="server" FilterType="Numbers" TargetControlID="TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox">
                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%">
                                    <asp:LinkButton ID="TechnicalSkillRequirementControl_addRowLinkButton" runat="server"
                                        OnClick="TechnicalSkillRequirementControl_addRowImageButton_Click" SkinID="sknAddLinkButton"
                                        Text="Add" ToolTip="Click here to add new skill" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="TechnicalSkillRequirementControl_additionalSkillCheckBox" runat="server" />
                                    <asp:Label ID="TechnicalSkillRequirementControl_additionalSkillLabel" runat="server"
                                        SkinID="sknLabelFieldHeaderText" Text="Additional Technical Skills Requirement Information"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="TechnicalSkillRequirementControl_additionalSkillDiv" runat="server" style="display: none">
                                        <asp:TextBox ID="TechnicalSkillRequirementControl_additionalSkillTextBox" runat="server"
                                            SkinID="sknMultiLineTextBox" TextMode="MultiLine" Width="99%" Height="50px"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" id="TechnicalSkillRequirementControl_previewTable"
    runat="server" visible="false" width="100%">
    <tr>
        <td class="grid_body_bg">
            <table width="100%" cellpadding="2" cellspacing="2" class="panel_bg">
                <tr>
                    <td class="header_text_bold td_height_20">
                        Technical Skill Requirement Details
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td class="tab_body_bg">
                                    <table width="100%" cellpadding="1" cellspacing="3" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 25%">
                                                            <asp:Label ID="TechnicalSkillRequirementControl_previewNumberOfYearsLabel" runat="server"
                                                                Text="Total Years Of IT Experience Required" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="TechnicalSkillRequirementControl_previewNumberOfYearstextLabel" runat="server"
                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%" align="right">
                                                            <asp:Label ID="TechnicalSkillRequirementControl_previewtotalWeightLabel" runat="server"
                                                                Text="Total Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%" align="right">
                                                            <asp:Label ID="TechnicalSkillRequirementControl_previewtotalWeightageTextLabel" runat="server"
                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                    <tr>
                                                        <td>
                                                            <div id="TechnicalSkillRequirementControl_previewGridViewDiv" style="width: 99%;
                                                                height: 101px; overflow: auto;" runat="server">
                                                                <asp:GridView ID="TechnicalSkillRequirementControl_previewTechnicalSkillGridView"
                                                                    runat="server" AutoGenerateColumns="false" ShowHeader="true" SkinID="sknPositionProfileHeaderGrid">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="15%" />
                                                                            <ItemStyle Width="30%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="Label1" runat="server" Text="Skill Name"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("SkillName") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="15%" />
                                                                            <ItemStyle Width="30%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Skill Category"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("CategoryName") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="11%" />
                                                                            <ItemStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="No Of Years Exp Req"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("NoOfYearsExp") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Skill Level Required"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("SkillRequired") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Mandatory / Desired"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("IsMandatory") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Certification Required"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("CertificationRequired") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Weightage"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("Weightage") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="TechnicalSkillRequirementControl_previewAdditionalSkillLabel" runat="server"
                                                    SkinID="sknLabelFieldHeaderText" Text="Additional Technical Skills Requirement Information"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="overflow: auto; height: 100px; word-wrap: break-word; white-space: normal;">
                                                    <asp:Label ID="TechnicalSkillRequirementControl_previewAdditionalSkillTextLabel"
                                                        runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
