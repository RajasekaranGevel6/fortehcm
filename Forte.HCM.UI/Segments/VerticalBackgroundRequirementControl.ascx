﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VerticalBackgroundRequirementControl.ascx.cs"
    Inherits="Forte.HCM.UI.Segments.VerticalBackgroundRequirement" %>
<asp:HiddenField ID="VerticalBackgroundRequirementControl_gridWeightageHiddenField"
    runat="server" />
<table width="100%" cellpadding="0" cellspacing="0" id="VerticalBackgroundRequirementControl_editTable"
    runat="server">
    <tr>
        <td class="header_bg" style="display:none">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 75%" class="header_text_bold" valign="middle" align="left">
                        <asp:Literal ID="VerticalBackgroundRequirement_headerLiteral" runat="server" Text="Vertical Background Requirement"></asp:Literal>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%" cellpadding="0" cellspacing="0" ><%--class="grid_body_bg"--%>
                <tr class="td_height_20">
                    <td align="right">
                        <table width="100%">
                            <tr>
                                <td style="width: 50%">&nbsp</td>
                                <td align="right" style="width: 45%">
                                    <asp:Label ID="VerticalBackgroundRequirement_totalWeightageLabel" SkinID="sknLabelFieldHeaderText"
                                        runat="server" Text="Total Weightage"></asp:Label>
                                </td>
                                <td align="right" style="width: 5%">
                                    <asp:TextBox ID="VerticalBackgroundRequirement_totalWeightageTextBox" runat="server"
                                        MaxLength="3" Width="25px"></asp:TextBox>
                                    <ajaxToolKit:FilteredTextBoxExtender ID="VerticalBackgroundRequirement_technicalSkillGridView_totalExperienceFilteredTextBoxExtender"
                                        runat="server" FilterType="Numbers" TargetControlID="VerticalBackgroundRequirement_totalWeightageTextBox">
                                    </ajaxToolKit:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_bg">
                        <table width="100%" cellpadding="2" cellspacing="0" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <div id="VerticalBackgroundRequirement_verticalDiv" style="width: 100%; height: 102px;
                                                    overflow: auto;" runat="server">
                                                    <asp:GridView ID="VerticalBackgroundRequirement_verticalBackGroundGridView" runat="server"
                                                        AutoGenerateColumns="false" HeaderStyle-Wrap="true" ShowHeader="true" SkinID="sknWrapHeaderGrid"
                                                        OnRowCommand="VerticalBackgroundRequirement_verticalBackGroundGridView_RowCommand"
                                                        OnRowDataBound="VerticalBackgroundRequirement_verticalBackGroundGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="VerticalBackgroundRequirement_technicalSkillGridView_deleteImageButton"
                                                                        runat="server" SkinID="sknDeleteImageButton" CommandName="deletevertical" CommandArgument='' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="25%" />
                                                                <ItemStyle Width="35%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="VerticalBackgroundRequirement_technicalSkillGridView_skillCategoryLabel"
                                                                        runat="server" Text="Vertical Segment"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="VerticalBackgroundRequirement_technicalSkillGridView_skillCategoryTextBox"
                                                                        runat="server" Width="95%" MaxLength="100" Text='<%# Eval("VerticalSegment") %>'></asp:TextBox>
                                                                    <ajaxToolKit:AutoCompleteExtender ID="SearchCategorySubjectControl_categoryAutoCompleteExtender"
                                                                        runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetPositionProfileKeyword"
                                                                        TargetControlID="VerticalBackgroundRequirement_technicalSkillGridView_skillCategoryTextBox"
                                                                        MinimumPrefixLength="0" CompletionListElementID="pnl" CompletionListCssClass="autocomplete_completionListElement"
                                                                        EnableCaching="true" CompletionSetCount="12">
                                                                    </ajaxToolKit:AutoCompleteExtender>
                                                                    <asp:Panel ID="pnl" runat="server">
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="25%" />
                                                                <ItemStyle Width="35%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="VerticalBackgroundRequirement_technicalSkillGridView_skillLabel" runat="server"
                                                                        Text="Sub Vertical Segment"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="VerticalBackgroundRequirement_technicalSkillGridView_skillTextBox"
                                                                        runat="server" Width="95%" MaxLength="100" Text='<%# Eval("SubVerticalSegment") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="10%" />
                                                                <ItemStyle Width="6%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="VerticalBackgroundRequirement_technicalSkillGridView_experienceLabel"
                                                                        runat="server" Text="No Of Years Exp Req"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="VerticalBackgroundRequirement_technicalSkillGridView_experienceTextBox"
                                                                        runat="server" Width="50%" MaxLength="3" Text='<%# Eval("NoOfYearsExperience") %>'></asp:TextBox>
                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="VerticalBackgroundRequirement_technicalSkillGridView_experienceFilteredTextBoxExtender"
                                                                        runat="server" FilterType="Custom,Numbers" TargetControlID="VerticalBackgroundRequirement_technicalSkillGridView_experienceTextBox" ValidChars=".">
                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle Width="10%" />
                                                                <ItemStyle Width="6%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="VerticalBackgroundRequirement_technicalSkillGridView_mandatoryLabel"
                                                                        runat="server" Text="Mandatory/Desired"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="VerticalBackgroundRequirement_technicalSkillGridView_mandatoryDropDownList"
                                                                        runat="server">
                                                                        <asp:ListItem Text="Mandatory" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Desired" Value="1"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             
                                                            <asp:TemplateField>
                                                                <HeaderStyle />
                                                                <ItemStyle Width="6%" />
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="VerticalBackgroundRequirement_technicalSkillGridView_weightageLabel"
                                                                        runat="server" Text="Weightage"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="VerticalBackgroundRequirement_technicalSkillGridView_weightageTextBox"
                                                                        runat="server" Width="50%" Text='<%# Eval("Weightage") %>' MaxLength="3"></asp:TextBox>
                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="VerticalBackgroundRequirement_technicalSkillGridView_weightageFilteredTextBoxExtender"
                                                                        runat="server" FilterType="Custom,Numbers" TargetControlID="VerticalBackgroundRequirement_technicalSkillGridView_weightageTextBox">
                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 5%">
                                    <asp:LinkButton ID="VerticalBackgroundRequirement_addRowLinkButton" runat="server"
                                        SkinID="sknAddLinkButton" Text="Add" ToolTip="Click here to add new vertical"
                                        OnClick="VerticalBackgroundRequirement_addRowLinkButton_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:CheckBox ID="VerticalBackgroundRequirement_additionalSkillCheckBox" runat="server" />
                                    <asp:Label ID="VerticalBackgroundRequirement_additionalSkillLabel" runat="server"
                                        SkinID="sknLabelFieldHeaderText" Text="Additional Vertical Requirement Information"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div id="VerticalBackgroundRequirement_additionSkillID" runat="server" style="display: none">
                                        <asp:TextBox ID="VerticalBackgroundRequirement_additionalSkillTextBox" runat="server"
                                            SkinID="sknMultiLineTextBox" Width="99%" Height="50px" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" id="VerticalBackgroundRequirementControl_previewTable"
    runat="server" visible="false" width="100%">
    <tr>
        <td class="grid_body_bg">
            <table width="100%" cellpadding="2" cellspacing="2" class="panel_bg">
                <tr>
                    <td class="header_text_bold td_height_20">
                        Vertical Requirement Details
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td class="tab_body_bg">
                                    <table width="100%" cellpadding="1" cellspacing="3" border="0">
                                        <tr class="td_height_20">
                                            <td align="right">
                                                <table width="30%">
                                                    <tr>
                                                        <td align="right" style="width: 75%">
                                                            <asp:Label ID="VerticalBackgroundRequirementControl_previewWeitageLabel" SkinID="sknLabelFieldHeaderText"
                                                                runat="server" Text="Total Weightage"></asp:Label>
                                                        </td>
                                                        <td align="right" style="width: 25%">
                                                            <asp:Label ID="VerticalBackgroundRequirementControl_previewWeitageTextLabel" SkinID="sknLabelFieldText"
                                                                runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                    <tr>
                                                        <td>
                                                            <div id="VerticalBackgroundRequirementControl_previewGridViewDiv" style="width: 99%;
                                                                height: 101px; overflow: auto;" runat="server">
                                                                <asp:GridView ID="VerticalBackgroundRequirementControl_previewTechnicalSkillGridView"
                                                                    runat="server" AutoGenerateColumns="false" ShowHeader="true" SkinID="sknPositionProfileHeaderGrid">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="15%" />
                                                                            <ItemStyle Width="30%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Vertical Segment"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("VerticalSegment") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="15%" />
                                                                            <ItemStyle Width="30%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Sub Vertical Segment"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("SubVerticalSegment") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="11%" />
                                                                            <ItemStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="No Of Years Exp Req"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("NoOfYearsExperience") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Mandatory/Desired"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("IsMandatory") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Testing Required"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("IsTestingRequired") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Interview Required"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("IsInterviewRequired") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle Width="10%" />
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="Weightage"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" Text='<%# Eval("Weightage") %>' Width="90%"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="VerticalBackgroundRequirementControl_previewAdditionalSkillLabel"
                                                    runat="server" SkinID="sknLabelFieldHeaderText" Text="Additional Verticle Skills Requirement Information"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="overflow:auto;height:100px;word-wrap: break-word;white-space:normal;">
                                                <asp:Label ID="VerticalBackgroundRequirementControl_previewAdditionalSkillTextLabel"
                                                    runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
