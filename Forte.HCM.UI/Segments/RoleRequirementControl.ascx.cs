﻿#region Directives                                                   
using System;
using System.Xml;
using System.Web.UI;
using System.Reflection;

using Forte.HCM.DataObjects;
#endregion Directives

namespace Forte.HCM.UI.Segments
{
    public partial class RoleRequirement : UserControl
    {
        #region Properties                                           
        public SegmentDetail DataSource
        {
            set
            {
                if (value.SegmentID != (int)SegmentType.Roles)
                    throw new Exception("Invalid data source");
                ProcessXML(value.DataSource);
            }
            get
            {
                RoleRequirementDetails roleRequirementDetails = new RoleRequirementDetails();
                roleRequirementDetails.PrimaryRole = RoleRequirementControl_primaryRoleTextBox.Text;
                roleRequirementDetails.SecondaryRole = RoleRequirementControl_secondaryRoleTextBox.Text;
                roleRequirementDetails.PrimaryRoleExperience = RoleRequirementControl_noOfExperienceTextBox.Text;
                roleRequirementDetails.SecondaryRoleExperience = RoleRequirementControl_noOfSecondaryExperienceTextBox.Text;
                roleRequirementDetails.AdditionalRole = RoleRequirementControl_additionalRoleTextBox.Text;
                roleRequirementDetails.IsAdditionalRole = RoleRequirementControl_additionalRoleCheckBox.Checked.ToString();
                SegmentDetail segmentDetail = new SegmentDetail();
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode xmlNode = xmlDocument.CreateNode(XmlNodeType.XmlDeclaration, "", "");
                xmlDocument.AppendChild(xmlNode);
                XmlElement xmlElementRoot = xmlDocument.CreateElement("RoleRequirementDetails");
                xmlDocument.AppendChild(xmlElementRoot);
                XmlElement xmlElementSegmentItem = null;
                Type idType = roleRequirementDetails.GetType();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    if (pInfo.Name == roleRequirementDetails.IsAdditionalRole && !Convert.ToBoolean(roleRequirementDetails.IsAdditionalRole))
                        continue;
                    object o = pInfo.GetValue(roleRequirementDetails, null);
                    xmlElementSegmentItem = xmlDocument.CreateElement(pInfo.Name);
                    xmlElementSegmentItem.InnerText = o.ToString();
                    xmlElementRoot.AppendChild(xmlElementSegmentItem);
                }
                segmentDetail.DisplayOrder = 0;
                segmentDetail.SegmentType = SegmentType.Roles;
                segmentDetail.DataSource = xmlDocument.InnerXml;
                segmentDetail.SegmentID = (int)SegmentType.Roles;
                return segmentDetail;
            }
        }

        public SegmentDetail PreviewDataSource
        {
            set
            {
                RoleRequirementControl_editTable.Visible = false;
                RoleRequirementControl_previewTable.Visible = true;
                if (value.SegmentID != (int)SegmentType.Roles)
                    throw new Exception("Invalid data source");

                RoleRequirementDetails roleRequirementDetails = new RoleRequirementDetails();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(value.DataSource);
                Type idType = roleRequirementDetails.GetType();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    pInfo.SetValue(roleRequirementDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
                }
                RoleRequirementControl_previewPrimaryRoleTextLabel.Text = roleRequirementDetails.PrimaryRole;
                RoleRequirementControl_previewNoOfExperienceTextLabel.Text = roleRequirementDetails.PrimaryRoleExperience;
                RoleRequirementControl_previewSecondaryRoleTextLabel.Text = roleRequirementDetails.SecondaryRole;
                //RoleRequirementControl_additionalRoleCheckBox.Text = roleRequirementDetails.;
                RoleRequirementControl_previewNoOfSecondaryExperienceTextLabel.Text = roleRequirementDetails.SecondaryRoleExperience;
                if (Convert.ToBoolean(roleRequirementDetails.IsAdditionalRole))
                {
                    if (roleRequirementDetails.AdditionalRole.Length > 0)
                    {
                        RoleRequirementControl_previewAditionalTextLabel.Text =
                            roleRequirementDetails.AdditionalRole == null ? roleRequirementDetails.AdditionalRole :
                            roleRequirementDetails.AdditionalRole.ToString().Replace(Environment.NewLine, "<br />");
                    }
                    else
                    {
                        RoleRequirementControl_previewAditionalLabel.Text = "";
                    }
                }
                else
                {
                    RoleRequirementControl_previewAditionalLabel.Text = "";
                }
            }
        }
        #endregion Properties

        #region Public Methods

         /// <summary>
        /// Method that populates the rolws from the parsed requirement text.
        /// </summary>
        /// <param name="primaryRole">
        /// A <see cref="string"/> that holds the primary role.
        /// </param>
        /// <param name="secondaryRole">
        /// A <see cref="string"/> that holds the secondary role.
        /// </param>
        public void PopulateData(string primaryRole, string secondaryRole)
        {
            RoleRequirementControl_primaryRoleTextBox.Text = primaryRole;
            RoleRequirementControl_secondaryRoleTextBox.Text = secondaryRole;
        }

        #endregion Public Methods

        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                RoleRequirementControl_additionalRoleCheckBox.Attributes.Add("onclick", "javascript:return ShowDiv('"
                    + RoleRequirementControl_additionalRoleCheckBox.ClientID + "','" + RoleRequirementControl_additionalRoleDiv.ClientID + "','"
                    + RoleRequirementControl_additionalRoleTextBox.ClientID + "');");

                if (RoleRequirementControl_additionalRoleCheckBox.Checked)
                    RoleRequirementControl_additionalRoleDiv.Style.Add("display", "block");
                else
                    RoleRequirementControl_additionalRoleDiv.Style.Add("display", "none");
            }
            catch (Exception)
            {
                throw;
            }

        } 
        #endregion

        #region Private Methods                                      
        private void ProcessXML(string xmlText)
        {
            RoleRequirementDetails roleRequirementDetails = new RoleRequirementDetails();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlText);
            Type idType = roleRequirementDetails.GetType();
            foreach (PropertyInfo pInfo in idType.GetProperties())
            {
                pInfo.SetValue(roleRequirementDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
            }
            RoleRequirementControl_primaryRoleTextBox.Text = roleRequirementDetails.PrimaryRole;
            RoleRequirementControl_noOfExperienceTextBox.Text = roleRequirementDetails.SecondaryRoleExperience;
            RoleRequirementControl_secondaryRoleTextBox.Text = roleRequirementDetails.SecondaryRole;
            //RoleRequirementControl_additionalRoleCheckBox.Text = roleRequirementDetails.;
            RoleRequirementControl_noOfSecondaryExperienceTextBox.Text = roleRequirementDetails.SecondaryRoleExperience;
            if (Convert.ToBoolean(roleRequirementDetails.IsAdditionalRole))
            {
                RoleRequirementControl_additionalRoleCheckBox.Checked = true;
                RoleRequirementControl_additionalRoleTextBox.Text = roleRequirementDetails.AdditionalRole;
            }
        }

        public void ResetControls()
        {
            RoleRequirementControl_primaryRoleTextBox.Text = "";
            RoleRequirementControl_noOfExperienceTextBox.Text = "";
            RoleRequirementControl_secondaryRoleTextBox.Text = "";
            RoleRequirementControl_noOfSecondaryExperienceTextBox.Text = "";
            RoleRequirementControl_additionalRoleCheckBox.Checked = false;
            RoleRequirementControl_additionalRoleTextBox.Text = "";
        }

        public void DisableControls()
        {
            RoleRequirementControl_primaryRoleTextBox.Enabled = false;
            RoleRequirementControl_noOfExperienceTextBox.Enabled = false;
            RoleRequirementControl_secondaryRoleTextBox.Enabled = false;
            RoleRequirementControl_noOfSecondaryExperienceTextBox.Enabled = false;
            RoleRequirementControl_additionalRoleCheckBox.Enabled = false;
            RoleRequirementControl_additionalRoleTextBox.Enabled = false;

        }
        #endregion Private Methods
    }
}