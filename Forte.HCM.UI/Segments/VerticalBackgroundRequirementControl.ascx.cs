﻿#region Directives
using System;
using System.Xml;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.DataObjects;
#endregion Directives

namespace Forte.HCM.UI.Segments
{
    public partial class VerticalBackgroundRequirement : UserControl
    {
        string VERTICAL_VIEWSTATE = "VERTICAL_VIEWSTATE";
        List<VerticalDetails> verticalDetailsList;
        VerticalDetails verticalDetail;

        decimal _totalWeightage = 0;

        private decimal _totalSegmentsWeightage = 0;

        #region Event Handler
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Support.Utility.IsNullOrEmpty(ViewState[VERTICAL_VIEWSTATE]))
                        DefaultGridValue();
                    else
                    {
                        verticalDetailsList = new List<VerticalDetails>();
                        verticalDetailsList = ViewState[VERTICAL_VIEWSTATE] as List<VerticalDetails>;

                        VerticalBackgroundRequirement_verticalBackGroundGridView.DataSource = verticalDetailsList;
                        VerticalBackgroundRequirement_verticalBackGroundGridView.DataBind();
                        MaintainStateInGridView(verticalDetailsList, false);
                    }
                }
                else
                {
                    ViewState[VERTICAL_VIEWSTATE] = VerticalBackGroundGridViewData();
                    VerticalBackgroundRequirement_totalWeightageTextBox.Focus();
                }

                VerticalBackgroundRequirement_additionalSkillCheckBox.Attributes.Add("onclick",
                    "javascript:ShowDiv('" + VerticalBackgroundRequirement_additionalSkillCheckBox.ClientID + "','" +
               VerticalBackgroundRequirement_additionSkillID.ClientID +
               "','" + VerticalBackgroundRequirement_additionalSkillTextBox.ClientID + "');");

                if (VerticalBackgroundRequirement_additionalSkillCheckBox.Checked)
                    VerticalBackgroundRequirement_additionSkillID.Style.Add("display", "block");
                else
                    VerticalBackgroundRequirement_additionSkillID.Style.Add("display", "none");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Handles the Click event of the VerticalBackgroundRequirement_addRowLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void VerticalBackgroundRequirement_addRowLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                verticalDetailsList = new List<VerticalDetails>();
                verticalDetailsList = ViewState[VERTICAL_VIEWSTATE] as List<VerticalDetails>;
                if (verticalDetailsList == null)
                {
                    verticalDetailsList = new List<VerticalDetails>();
                }
                verticalDetail = new VerticalDetails();
                verticalDetailsList.Add(verticalDetail);
                VerticalBackgroundRequirement_verticalBackGroundGridView.DataSource = verticalDetailsList;
                VerticalBackgroundRequirement_verticalBackGroundGridView.DataBind();
                MaintainStateInGridView(verticalDetailsList, true);
                GridViewRow lastRow = VerticalBackgroundRequirement_verticalBackGroundGridView.Rows
                   [VerticalBackgroundRequirement_verticalBackGroundGridView.Rows.Count - 1];
                TextBox skillCategory = (TextBox)lastRow.FindControl
                    ("VerticalBackgroundRequirement_technicalSkillGridView_skillCategoryTextBox");
                skillCategory.Focus();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Maintains the state in grid view.
        /// </summary>
        /// <param name="verticalDetailsList">The vertical details list.</param>
        /// <param name="addControl">if set to <c>true</c> [add control].</param>
        private void MaintainStateInGridView(List<VerticalDetails> verticalDetailsList, bool addControl)
        {
            int j = VerticalBackgroundRequirement_verticalBackGroundGridView.Rows.Count;
            if (addControl)
                j = j - 1;

            for (int i = 0; i < j; i++)
            {
                DropDownList mandatoryDropDownList =
                    ((DropDownList)VerticalBackgroundRequirement_verticalBackGroundGridView.Rows[i].
                    FindControl("VerticalBackgroundRequirement_technicalSkillGridView_mandatoryDropDownList"));
                mandatoryDropDownList.SelectedItem.Selected = false;
                if (!Support.Utility.IsNullOrEmpty(mandatoryDropDownList.Items.FindByText(verticalDetailsList[i].IsMandatory)))
                    mandatoryDropDownList.Items.FindByText(verticalDetailsList[i].IsMandatory).Selected = true;
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the VerticalBackgroundRequirement_verticalBackGroundGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void VerticalBackgroundRequirement_verticalBackGroundGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "deletevertical")
            {
                verticalDetailsList = new List<VerticalDetails>();
                verticalDetailsList = ViewState[VERTICAL_VIEWSTATE] as List<VerticalDetails>;
                if (verticalDetailsList.Count > 0)
                {
                    verticalDetailsList.RemoveAt(int.Parse(e.CommandArgument.ToString()));
                }
                VerticalBackgroundRequirement_verticalBackGroundGridView.DataSource = verticalDetailsList;
                VerticalBackgroundRequirement_verticalBackGroundGridView.DataBind();
                ViewState[VERTICAL_VIEWSTATE] = verticalDetailsList;
            }

        }

        /// <summary>
        /// Handles the RowDataBound event of the VerticalBackgroundRequirement_verticalBackGroundGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void VerticalBackgroundRequirement_verticalBackGroundGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    ImageButton delteImgaeButton = (ImageButton)e.Row.FindControl
                        ("VerticalBackgroundRequirement_technicalSkillGridView_deleteImageButton");
                    delteImgaeButton.CommandArgument = e.Row.RowIndex.ToString();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        #endregion Event Handler

        #region Properties
        /// <summary>
        /// Gets the total segments weightage.
        /// </summary>
        /// <value>The total segments weightage.</value>
        public decimal TotalSegmentsWeightage
        {
            get
            {
                TextBox weightageTextBox = null;
                foreach (GridViewRow row in VerticalBackgroundRequirement_verticalBackGroundGridView.Rows)
                {
                    weightageTextBox = (TextBox)row.FindControl
                        ("VerticalBackgroundRequirement_technicalSkillGridView_weightageTextBox");
                    if (!Support.Utility.IsNullOrEmpty(weightageTextBox.Text.Trim()))
                        _totalSegmentsWeightage = _totalSegmentsWeightage + Convert.ToDecimal(weightageTextBox.Text);
                }
                return _totalSegmentsWeightage;
            }
        }

        private List<SkillWeightage> _skillWeightageDataSource;

        /// <summary>
        /// Gets the skill weightage data source.
        /// </summary>
        /// <value>The skill weightage data source.</value>
        public List<SkillWeightage> SkillWeightageDataSource
        {
            get
            {
                _skillWeightageDataSource = new List<SkillWeightage>();
                SkillWeightage skillWeightage = null;
                decimal totalWeightage = 0;
                if (!Support.Utility.IsNullOrEmpty(VerticalBackgroundRequirement_totalWeightageTextBox.Text.Trim()))
                    totalWeightage = decimal.Parse(VerticalBackgroundRequirement_totalWeightageTextBox.Text.Trim());

                TextBox weightageTextBox = null;
                TextBox skillTextBox = null;
                foreach (GridViewRow row in VerticalBackgroundRequirement_verticalBackGroundGridView.Rows)
                {
                    skillWeightage = new SkillWeightage();
                    weightageTextBox = (TextBox)row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_weightageTextBox");
                    skillTextBox = (TextBox)row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_skillCategoryTextBox");
                    skillWeightage.SkillName = skillTextBox.Text.Trim();
                    skillWeightage.UserSegments = false;
                    skillWeightage.SkillTypes = SkillType.Vertical;
                    skillWeightage.SkillCategory = null;
                    if (Support.Utility.IsNullOrEmpty(weightageTextBox.Text))
                        weightageTextBox.Text = "0";
                    skillWeightage.Weightage = (decimal.Parse(weightageTextBox.Text.Trim()) / 100) * totalWeightage;
                    _skillWeightageDataSource.Add(skillWeightage);
                }
                return _skillWeightageDataSource;
            }
        }

        /// <summary>
        /// Verticals the back ground grid view data.
        /// </summary>
        /// <returns></returns>
        private List<VerticalDetails> VerticalBackGroundGridViewData()
        {
            verticalDetailsList = new List<VerticalDetails>();
            foreach (GridViewRow row in VerticalBackgroundRequirement_verticalBackGroundGridView.Rows)
            {
                verticalDetail = new VerticalDetails();
                TextBox SkillCategoryTextBox = (TextBox)
                   row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_skillCategoryTextBox");
                TextBox SkillTextBox = (TextBox)
                   row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_skillTextBox");
                TextBox ExperienceTextBox = (TextBox)
                   row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_experienceTextBox");
                TextBox WeightageTextBox = (TextBox)
                   row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_weightageTextBox");

                DropDownList MandatoryDropDownList = (DropDownList)
                row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_mandatoryDropDownList");
                CheckBox CertificationCheckBox = (CheckBox)
                    row.FindControl("VerticalBackgroundRequirementControl_technicalSkillGridView_certificationCheckBox");
                
                /*CheckBox TestingRequiredCheckBox = (CheckBox)
                    row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_testingRequiredCheckBox");
                CheckBox InterviewRequiredCheckBox = (CheckBox)
                    row.FindControl("VerticalBackgroundRequirement_technicalSkillGridView_interviewCheckBox");*/

                verticalDetail.VerticalSegment = SkillCategoryTextBox.Text.Trim();
                verticalDetail.SubVerticalSegment = SkillTextBox.Text.Trim();
                //verticalDetail.IsInterviewRequired = InterviewRequiredCheckBox.Checked.ToString();
                verticalDetail.IsInterviewRequired = "N";
                verticalDetail.IsMandatory = MandatoryDropDownList.SelectedItem.Text;
                //verticalDetail.IsTestingRequired = TestingRequiredCheckBox.Checked.ToString();
                verticalDetail.IsTestingRequired = "N";
                verticalDetail.Weightage = WeightageTextBox.Text.Trim();
                verticalDetail.NoOfYearsExperience = ExperienceTextBox.Text.Trim();
                verticalDetailsList.Add(verticalDetail);
            }
            return verticalDetailsList;
        }

        /// <summary>
        /// Sets the preview data source.
        /// </summary>
        /// <value>The preview data source.</value>
        public SegmentDetail PreviewDataSource
        {
            set
            {
                VerticalBackgroundRequirementControl_editTable.Visible = false;
                VerticalBackgroundRequirementControl_previewTable.Visible = true;
                if (value.SegmentID != (int)SegmentType.Verticals)
                    throw new Exception("Invalid data source");
                verticalDetail = new VerticalDetails();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(value.DataSource);
                Type idType = verticalDetail.GetType();
                verticalDetailsList = new List<VerticalDetails>();
                if (xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText.Length > 0)
                {
                    VerticalBackgroundRequirementControl_previewWeitageTextLabel.Text =
                        xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText;
                }
                if (Convert.ToBoolean(xmlDocument.GetElementsByTagName("IsAdditionalSkill")[0].InnerText))
                {
                    if (xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText.Length > 0)
                    {
                        VerticalBackgroundRequirementControl_previewAdditionalSkillTextLabel.Text =
                            xmlDocument.GetElementsByTagName("AdditionalSkill")[0].
                            InnerText.ToString().Replace(Environment.NewLine, "<br />");
                    }
                    else
                    {
                        VerticalBackgroundRequirementControl_previewAdditionalSkillLabel.Text = "";
                    }
                }
                else
                {
                    VerticalBackgroundRequirementControl_previewAdditionalSkillLabel.Text = "";
                }
                for (int i = 0; i < xmlDocument.GetElementsByTagName("VerticalDetail").Count; i++)
                {
                    verticalDetail = new VerticalDetails();
                    foreach (PropertyInfo pInfo in idType.GetProperties())
                    {
                        pInfo.SetValue(verticalDetail, xmlDocument.GetElementsByTagName(pInfo.Name)[i].InnerText, null);
                    }
                    verticalDetail.IsTestingRequired =
                         verticalDetail.IsTestingRequired == "True" ? "Yes" : "No";
                    verticalDetail.IsInterviewRequired =
                        verticalDetail.IsInterviewRequired == "True" ? "Yes" : "No";

                    verticalDetailsList.Add(verticalDetail);
                }
                VerticalBackgroundRequirementControl_previewTechnicalSkillGridView.DataSource = verticalDetailsList;
                VerticalBackgroundRequirementControl_previewTechnicalSkillGridView.DataBind();
            }
        }

        /// <summary>
        /// Gets the total weightage.
        /// </summary>
        /// <value>The total weightage.</value>
        public decimal TotalWeightage
        {
            get
            {
                if (!Support.Utility.IsNullOrEmpty(VerticalBackgroundRequirement_totalWeightageTextBox.Text))
                    _totalWeightage = Convert.ToDecimal(VerticalBackgroundRequirement_totalWeightageTextBox.Text.Trim());
                return _totalWeightage;
            }
        }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>The data source.</value>
        public SegmentDetail DataSource
        {
            set
            {
                if (value.SegmentID != (int)SegmentType.Verticals)
                    throw new Exception("Invalid data source");
                ProcessXML(value.DataSource);
            }
            get
            {
                verticalDetailsList = VerticalBackGroundGridViewData();
                verticalDetail = new VerticalDetails();
                SegmentDetail segmentDetail = new SegmentDetail();
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode xmlNode = xmlDocument.CreateNode(XmlNodeType.XmlDeclaration, "NameSpace", "");
                xmlDocument.AppendChild(xmlNode);
                XmlElement xmlElementRoot = xmlDocument.CreateElement("VerticalDetails");

                XmlElement xmlElementIsAdditionalSkill = xmlDocument.CreateElement("IsAdditionalSkill");
                xmlElementIsAdditionalSkill.InnerText = VerticalBackgroundRequirement_additionalSkillCheckBox.Checked.ToString();
                xmlElementRoot.AppendChild(xmlElementIsAdditionalSkill);

                XmlElement AdditionalSkill = xmlDocument.CreateElement("AdditionalSkill");
                AdditionalSkill.InnerText = VerticalBackgroundRequirement_additionalSkillTextBox.Text.Trim();
                xmlElementRoot.AppendChild(AdditionalSkill);

                XmlElement xmlElementTotalExperience = xmlDocument.CreateElement("TotalWeightage");
                xmlElementTotalExperience.InnerText = VerticalBackgroundRequirement_totalWeightageTextBox.Text.Trim();
                xmlElementRoot.AppendChild(xmlElementTotalExperience);

                XmlElement xmlElementSubRoot = null;
                Type idType = verticalDetail.GetType();

                XmlElement xmlElementSegmentItem = null;
                for (int i = 0; i < verticalDetailsList.Count; i++)
                {
                    xmlElementSubRoot = xmlDocument.CreateElement("VerticalDetail");
                    xmlElementRoot.AppendChild(xmlElementSubRoot);
                    foreach (PropertyInfo pInfo in idType.GetProperties())
                    {
                        object obj = pInfo.GetValue(verticalDetailsList[i], null);
                        xmlElementSegmentItem = xmlDocument.CreateElement(pInfo.Name);
                        if (obj != null)
                        {
                            xmlElementSegmentItem.InnerText = obj.ToString();
                            xmlElementSubRoot.AppendChild(xmlElementSegmentItem);
                        }
                    }
                }

                xmlDocument.AppendChild(xmlElementRoot);
                segmentDetail.DisplayOrder = 0;
                segmentDetail.SegmentType = SegmentType.Verticals;
                segmentDetail.DataSource = xmlDocument.InnerXml;
                segmentDetail.SegmentID = (int)SegmentType.Verticals;
                return segmentDetail;
            }
        }

        /// <summary>
        /// Method that populates the verticals from the parsed requirement text.
        /// </summary>
        /// <param name="newVerticals">
        /// A list of <see cref="VerticalDetails"/> that holds the new verticals.
        /// to be populated.
        /// </param>
        public void PopulateData(List<VerticalDetails> newVerticals)
        {
            // Collect data from the grid.
            verticalDetailsList = VerticalBackGroundGridViewData();

            if (verticalDetailsList == null)
            {
                verticalDetailsList = new List<VerticalDetails>();
            }

            // Remove the bottom vertical if it is empty.
            if (verticalDetailsList.Count > 1)
            {
                if (verticalDetailsList[verticalDetailsList.Count - 1].VerticalSegment == null ||
                    verticalDetailsList[verticalDetailsList.Count - 1].VerticalSegment.Trim().Length == 0)
                {
                    verticalDetailsList.RemoveAt(verticalDetailsList.Count - 1);
                }
            }

            // Append the new verticals to the list.
            verticalDetailsList.AddRange(newVerticals);

            // Add an empty vertical row.
            verticalDetail = new VerticalDetails();
            verticalDetailsList.Add(verticalDetail);

            // Remove the top vertical if it is empty.
            if (verticalDetailsList.Count > 0)
            {
                if (verticalDetailsList[0].VerticalSegment == null || verticalDetailsList[0].VerticalSegment.Trim().Length == 0)
                {
                    verticalDetailsList.RemoveAt(0);
                }
            }

            ViewState[VERTICAL_VIEWSTATE] = verticalDetailsList;
            VerticalBackgroundRequirement_verticalBackGroundGridView.DataSource = verticalDetailsList;
            VerticalBackgroundRequirement_verticalBackGroundGridView.DataBind();
        }

        #endregion Properties

        #region Methods
        /// <summary>
        /// Resets the controls.
        /// </summary>
        public void ResetControls()
        {
            DefaultGridValue();
            VerticalBackgroundRequirement_additionalSkillTextBox.Text = "";
            VerticalBackgroundRequirement_additionalSkillCheckBox.Checked = false;
        }

        /// <summary>
        /// Disables the controls.
        /// </summary>
        public void DisableControls()
        {
            VerticalBackgroundRequirement_additionalSkillTextBox.Enabled = false;
            VerticalBackgroundRequirement_additionalSkillCheckBox.Enabled = false;
            VerticalBackgroundRequirement_verticalBackGroundGridView.Enabled = false;
            VerticalBackgroundRequirement_addRowLinkButton.Enabled = false;
        }

        /// <summary>
        /// Processes the XML.
        /// </summary>
        /// <param name="xmlText">The XML text.</param>
        private void ProcessXML(string xmlText)
        {
            verticalDetail = new VerticalDetails();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlText);
            Type idType = verticalDetail.GetType();
            verticalDetailsList = new List<VerticalDetails>();
            if (Convert.ToBoolean(xmlDocument.GetElementsByTagName("IsAdditionalSkill")[0].InnerText))
            {
                VerticalBackgroundRequirement_additionalSkillCheckBox.Checked = true;

                if (xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText.Length > 0)
                {
                    VerticalBackgroundRequirement_additionalSkillTextBox.Text =
                        xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText;

                }
            }
            if (xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText.Length > 0)
            {
                VerticalBackgroundRequirement_totalWeightageTextBox.Text =
                    xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText;
            }
            for (int i = 0; i < xmlDocument.GetElementsByTagName("VerticalDetail").Count; i++)
            {
                verticalDetail = new VerticalDetails();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    if (xmlDocument.GetElementsByTagName(pInfo.Name)[i] != null)
                        pInfo.SetValue(verticalDetail, xmlDocument.GetElementsByTagName(pInfo.Name)[i].InnerText, null);
                }
                verticalDetailsList.Add(verticalDetail);
            }
            ViewState[VERTICAL_VIEWSTATE] = verticalDetailsList;
            VerticalBackgroundRequirement_verticalBackGroundGridView.DataSource = verticalDetailsList;
            VerticalBackgroundRequirement_verticalBackGroundGridView.DataBind();
            MaintainStateInGridView(verticalDetailsList, false);
            if (VerticalBackgroundRequirement_additionalSkillCheckBox.Checked)
                VerticalBackgroundRequirement_additionSkillID.Style.Add("display", "block");
            else
                VerticalBackgroundRequirement_additionSkillID.Style.Add("display", "none");
        }

        /// <summary>
        /// Defaults the grid value.
        /// </summary>
        public void DefaultGridValue()
        {
            verticalDetailsList = new List<VerticalDetails>();
            verticalDetail = new VerticalDetails();
            verticalDetailsList.Add(verticalDetail);
            VerticalBackgroundRequirement_verticalBackGroundGridView.DataSource = verticalDetailsList;
            VerticalBackgroundRequirement_verticalBackGroundGridView.DataBind();
            ViewState[VERTICAL_VIEWSTATE] = verticalDetailsList;
        }
        #endregion Methods
    }
}