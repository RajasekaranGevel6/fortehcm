﻿#region Directives                                                   
using System;
using System.Web.UI;

#endregion Directives
namespace Forte.HCM.UI.Segments
{
    public partial class DecisionMakingControl : UserControl
    {
        #region Event Handler                                        
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion Event Handler
    }
}