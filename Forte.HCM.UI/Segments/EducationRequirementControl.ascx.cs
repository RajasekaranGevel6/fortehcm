﻿#region Directives                                                   
using System;
using System.Xml;
using System.Text;
using System.Web.UI;
using System.Reflection;

using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Segments
{
    public partial class EducationRequirementControl :UserControl
    {
        #region Properties                                           
        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>The data source.</value>
        public SegmentDetail DataSource
        {
            set
            {
                if (value.SegmentID != (int)SegmentType.Education)
                    throw new Exception("Invalid data source");
                ProcessXML(value.DataSource);
            }
            get
            {
                StringBuilder selectededcucationLevel = new StringBuilder();
                for (int i = 0; i < EducationRequirementControl_educationLevelCheckBoxList.Items.Count; i++)
                {
                    if (EducationRequirementControl_educationLevelCheckBoxList.Items[i].Selected)
                    {
                        selectededcucationLevel.Append(EducationRequirementControl_educationLevelCheckBoxList.Items[i].Value);
                        selectededcucationLevel.Append(",");
                    }
                }

                EducationRequirementDetails educationRequirementDetails = new EducationRequirementDetails();

                educationRequirementDetails.EducationLevel = selectededcucationLevel.ToString().TrimEnd(',');
                educationRequirementDetails.Specialization = EducationRequirementControl_specializationTextBox.Text;

                SegmentDetail segmentDetail = new SegmentDetail();
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode xmlNode = xmlDocument.CreateNode(XmlNodeType.XmlDeclaration, "", "");
                xmlDocument.AppendChild(xmlNode);
                XmlElement xmlElementRoot = xmlDocument.CreateElement("EducationalDetails");
                xmlDocument.AppendChild(xmlElementRoot);
                XmlElement xmlElementSegmentItem = null;
                Type idType = educationRequirementDetails.GetType();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    object o = pInfo.GetValue(educationRequirementDetails, null);
                    xmlElementSegmentItem = xmlDocument.CreateElement(pInfo.Name);
                    xmlElementSegmentItem.InnerText = o.ToString();
                    xmlElementRoot.AppendChild(xmlElementSegmentItem);
                }
                segmentDetail.DisplayOrder = 0;
                segmentDetail.SegmentType = SegmentType.Education;
                segmentDetail.DataSource = xmlDocument.InnerXml.ToString();
                segmentDetail.SegmentID = (int)SegmentType.Education;
                return segmentDetail;
            }
        }

        /// <summary>
        /// Sets the preview data source.
        /// </summary>
        /// <value>The preview data source.</value>
        public SegmentDetail PreviewDataSource
        {
            set
            {
                if (value.SegmentID != (int)SegmentType.Education)
                    throw new Exception("Invalid data source");

                EducationRequirementControl_editTable.Visible = false;
                EducationRequirementControl_previewTable.Visible = true;

                EducationRequirementDetails educationRequirementDetails = new EducationRequirementDetails();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(value.DataSource);
                Type idType = educationRequirementDetails.GetType();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    pInfo.SetValue(educationRequirementDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
                }

                EducationRequirementControl_previewSpecializationTextLabel.Text = educationRequirementDetails.Specialization;
                string[] educationLevel = educationRequirementDetails.EducationLevel.Split(',');
                string educationItemLevel = "";
                foreach (string item in educationLevel)
                {
                    for (int i = 0; i < EducationRequirementControl_educationLevelCheckBoxList.Items.Count; i++)
                    {
                        if (EducationRequirementControl_educationLevelCheckBoxList.Items[i].Value == item)
                            educationItemLevel = educationItemLevel + EducationRequirementControl_educationLevelCheckBoxList.Items[i].Text + ",";
                    }
                }
                EducationRequirementControl_previewEducationLevelTextLabel.Text = educationItemLevel.TrimEnd(',');
            }
        } 
        #endregion

        #region Private Methods                                      
        /// <summary>
        /// Processes the XML. 
        /// </summary>
        /// <param name="xmlText">The XML text.</param>
        private void ProcessXML(string xmlText)
        {
            EducationRequirementDetails educationRequirementDetails = new EducationRequirementDetails();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlText);
            Type idType = educationRequirementDetails.GetType();
            foreach (PropertyInfo pInfo in idType.GetProperties())
            {
                pInfo.SetValue(educationRequirementDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
            }

            EducationRequirementControl_specializationTextBox.Text = educationRequirementDetails.Specialization;
            string[] educationLevel = educationRequirementDetails.EducationLevel.Split(',');

            foreach (string item in educationLevel)
            {
                for (int i = 0; i < EducationRequirementControl_educationLevelCheckBoxList.Items.Count; i++)
                {
                    if (EducationRequirementControl_educationLevelCheckBoxList.Items[i].Value == item)
                        EducationRequirementControl_educationLevelCheckBoxList.Items[i].Selected = true;
                }
            }
        }

        /// <summary>
        /// Resets the controls.
        /// </summary>
        public void ResetControls()
        {
            EducationRequirementControl_educationLevelCheckBoxList.ClearSelection();
            EducationRequirementControl_specializationTextBox.Text = "";
        }

        /// <summary>
        /// Disables the controls.
        /// </summary>
        public void DisableControls()
        {
            EducationRequirementControl_educationLevelCheckBoxList.Enabled = false;
            EducationRequirementControl_specializationTextBox.Enabled = false;
        } 
        #endregion
    }
}