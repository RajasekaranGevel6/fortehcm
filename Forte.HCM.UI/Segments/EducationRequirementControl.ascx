﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EducationRequirementControl.ascx.cs"
    Inherits="Forte.HCM.UI.Segments.EducationRequirementControl" %>
<table width="100%" cellpadding="0" cellspacing="0" id="EducationRequirementControl_editTable"
    runat="server">
    <tr>
        <td class="header_bg">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 75%" class="header_text_bold" valign="middle" align="left">
                        <asp:Literal ID="EducationRequirementControl_headerLiteral" runat="server" Text="Education Requirement"></asp:Literal>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="grid_body_bg">
            <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="3" class="panel_inner_body_bg" border="0">
                            <tr>
                                <td style="width: 12%">
                                    <asp:Label ID="EducationRequirementControl_educationLevelLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Minimum Level Of Education Required"></asp:Label>
                                </td>
                                <td>
                                    <div class="checkbox_list_bg" style="width: 100%; height: 15%; overflow: auto">
                                        <asp:CheckBoxList ID="EducationRequirementControl_educationLevelCheckBoxList" runat="server"
                                            RepeatColumns="20" RepeatDirection="Horizontal" CellSpacing="5" Width="100%"
                                            TextAlign="Right">
                                            <asp:ListItem Text="Masters" Value="1">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Bachelors" Value="2">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Associate" Value="3">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Diploma" Value="4">
                                            </asp:ListItem>
                                        </asp:CheckBoxList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="EducationRequirementControl_specializationLabel" runat="server" Text="Specialization "
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="EducationRequirementControl_specializationTextBox" runat="server"
                                        Width="99.5%" MaxLength="100"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="EducationRequirementControl_previewTable" runat="server" visible="false"
    width="100%">
    <tr>
        <td class="grid_body_bg">
            <table width="100%" cellpadding="2" cellspacing="2" class="panel_bg">
                <tr>
                    <td class="header_text_bold td_height_20">
                        Educational Requirement Details
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td class="tab_body_bg">
                                    <table width="100%" cellpadding="1" cellspacing="3" border="0">
                                        <tr class="td_height_20">
                                            <td>
                                                <asp:Label ID="EducationRequirementControl_previewEducationLevelLabel" runat="server"
                                                    SkinID="sknLabelFieldHeaderText" Text="Minimum Level Of Education Required"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="EducationRequirementControl_previewEducationLevelTextLabel" runat="server"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="td_height_20">
                                            <td>
                                                <asp:Label ID="EducationRequirementControl_previewSpecializationLabel" runat="server"
                                                    Text="Specialization " SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="EducationRequirementControl_previewSpecializationTextLabel" runat="server"
                                                    Text="Specialization " SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
