﻿#region Directives
using System;
using System.Xml;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.DataObjects;

using AjaxControlToolkit;

#endregion Directives

namespace Forte.HCM.UI.Segments
{
    /// <summary>
    /// Represents the class that holds the skill requirement details
    /// </summary>
    public partial class TechnicalSkillRequirement : UserControl
    {

        List<TechnicalSkillDetails> technicalSkillDetailsList;
        TechnicalSkillDetails technicalSkillDetails;
        string TECHNICALSKILLS_VIEWSTATE = "TechnicalSkillsViewState";

        decimal _totalWeightage = 0;

        #region Properties

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>The data source.</value>
        public SegmentDetail DataSource
        {
            set
            {
                if (value.SegmentID != (int)SegmentType.TechnicalSkills)
                    throw new Exception("Invalid data source");
                ProcessXML(value.DataSource);
            }
            get
            {
                List<TechnicalSkillDetails> technicalSkillDetailList = TechnicalSkillGridViewData();
                TechnicalSkillDetails technicalSkillDetails = new TechnicalSkillDetails();
                SegmentDetail segmentDetail = new SegmentDetail();
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode xmlNode = xmlDocument.CreateNode(XmlNodeType.XmlDeclaration, "NameSpace", "");
                xmlDocument.AppendChild(xmlNode);
                XmlElement xmlElementRoot = xmlDocument.CreateElement("TechnicalSkillDetails");
                XmlElement xmlElementChild = null;
                XmlElement xmlElementTotalITExp = xmlDocument.CreateElement("TotalITExperience");
                xmlElementTotalITExp.InnerText = TechnicalSkillRequirementControl_numberOfYearsTextBox.Text.Trim();
                xmlElementRoot.AppendChild(xmlElementTotalITExp);

                XmlElement xmlElementIsAdditionalSkill = xmlDocument.CreateElement("IsAdditionalSkill");
                xmlElementIsAdditionalSkill.InnerText = TechnicalSkillRequirementControl_additionalSkillCheckBox.Checked.ToString();
                xmlElementRoot.AppendChild(xmlElementIsAdditionalSkill);

                XmlElement xmlElementAdditionalSkill = xmlDocument.CreateElement("AdditionalSkill");
                xmlElementAdditionalSkill.InnerText = TechnicalSkillRequirementControl_additionalSkillTextBox.Text.Trim();
                xmlElementRoot.AppendChild(xmlElementAdditionalSkill);

                XmlElement xmlElementTotalExperience = xmlDocument.CreateElement("TotalWeightage");
                xmlElementTotalExperience.InnerText = TechnicalSkillRequirementControl_totalWeightageTextBox.Text.Trim();
                xmlElementRoot.AppendChild(xmlElementTotalExperience);

                Type idType = technicalSkillDetails.GetType();
                XmlElement xmlElementSegmentItem = null;

                for (int i = 0; i < technicalSkillDetailList.Count; i++)
                {
                    xmlElementChild = xmlDocument.CreateElement("TechnicalSkillDetail");
                    xmlElementRoot.AppendChild(xmlElementChild);
                    foreach (PropertyInfo pInfo in idType.GetProperties())
                    {
                        object obj = pInfo.GetValue(technicalSkillDetailList[i], null);
                        xmlElementSegmentItem = xmlDocument.CreateElement(pInfo.Name);
                        if (obj != null)
                        {
                            xmlElementSegmentItem.InnerText = obj.ToString();
                            xmlElementChild.AppendChild(xmlElementSegmentItem);
                        }
                    }
                }
                xmlDocument.AppendChild(xmlElementRoot);

                segmentDetail.DisplayOrder = 0;
                segmentDetail.SegmentType = SegmentType.TechnicalSkills;
                segmentDetail.DataSource = xmlDocument.InnerXml;
                segmentDetail.SegmentID = (int)SegmentType.TechnicalSkills;
                return segmentDetail;
            }
        }

        /// <summary>
        /// Method that populates the technical skills from the parsed requirement 
        /// text.
        /// </summary>
        /// <param name="newTechnicalSkills">
        /// A list of <see cref="TechnicalSkillDetails"/> that holds the new skills
        /// to be populated.
        /// </param>
        public void PopulateData(List<TechnicalSkillDetails> newTechnicalSkills)
        {
            // Collect data from the grid.
            technicalSkillDetailsList = TechnicalSkillGridViewData();

            if (technicalSkillDetailsList == null)
            {
                technicalSkillDetailsList = new List<TechnicalSkillDetails>();
            }

            // Remove the bottom skill if it is empty.
            if (technicalSkillDetailsList.Count > 1)
            {
                if (technicalSkillDetailsList[technicalSkillDetailsList.Count - 1].SkillName == null || 
                    technicalSkillDetailsList[technicalSkillDetailsList.Count - 1].SkillName.Trim().Length == 0)
                {
                    technicalSkillDetailsList.RemoveAt(technicalSkillDetailsList.Count - 1);
                }
            }

            // Append the new skills to the list.
            technicalSkillDetailsList.AddRange(newTechnicalSkills);

            // Add an empty skill row.
            technicalSkillDetails = new TechnicalSkillDetails();
            technicalSkillDetailsList.Add(technicalSkillDetails);

            // Remove the top skill if it is empty.
            if (technicalSkillDetailsList.Count > 0)
            {
                if (technicalSkillDetailsList[0].SkillName == null || technicalSkillDetailsList[0].SkillName.Trim().Length == 0)
                {
                    technicalSkillDetailsList.RemoveAt(0);
                }
            }

            ViewState[TECHNICALSKILLS_VIEWSTATE] = technicalSkillDetailsList;
            TechnicalSkillRequirementControl_technicalSkillGridView.DataSource = technicalSkillDetailsList;
            TechnicalSkillRequirementControl_technicalSkillGridView.DataBind();
            //MaintainStateInGridView(technicalSkillDetailsList, true);
            //GridViewRow lastRow = TechnicalSkillRequirementControl_technicalSkillGridView.Rows
            //  [TechnicalSkillRequirementControl_technicalSkillGridView.Rows.Count - 1];
            //TextBox skillCategory = (TextBox)lastRow.FindControl
            //   ("TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryTextBox");
            //skillCategory.Focus();
        }


        /// <summary>
        /// Technicals the skill grid view data.
        /// </summary>
        /// <returns></returns>
        private List<TechnicalSkillDetails> TechnicalSkillGridViewData()
        {
            List<TechnicalSkillDetails> technicalSkillDetailList = new List<TechnicalSkillDetails>();
            foreach (GridViewRow row in TechnicalSkillRequirementControl_technicalSkillGridView.Rows)
            {
                TechnicalSkillDetails technicalSkillDetail = new TechnicalSkillDetails();
                TextBox SkillCategoryTextBox = (TextBox)
                   row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryTextBox");
                TextBox SkillTextBox = (TextBox)
                   row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillTextBox");
                TextBox ExperienceTextBox = (TextBox)
                   row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_experienceTextBox");
                TextBox WeightageTextBox = (TextBox)
                   row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox");
                DropDownList SkillLevelDropdownList = (DropDownList)
                 row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillLevelDropdownList");
                DropDownList MandatoryDropDownList = (DropDownList)
                row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_mandatoryDropDownList");
                CheckBox CertificationCheckBox = (CheckBox)
                    row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_certificationCheckBox");

                /*CheckBox TestingRequiredCheckBox = (CheckBox)
                    row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_testingRequiredCheckBox");
                CheckBox InterviewRequiredCheckBox = (CheckBox)
                    row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_interviewRequiredCheckBox");*/

                technicalSkillDetail.SkillRequired = SkillLevelDropdownList.SelectedItem.Text;
                technicalSkillDetail.CategoryName = SkillCategoryTextBox.Text.Trim();
                technicalSkillDetail.CertificationRequired = CertificationCheckBox.Checked.ToString();
                //technicalSkillDetail.IsInterviewRequired = InterviewRequiredCheckBox.Checked.ToString();
                technicalSkillDetail.IsInterviewRequired = "N";
                technicalSkillDetail.IsMandatory = MandatoryDropDownList.SelectedItem.Text;
                //technicalSkillDetail.IsTestingRequired = TestingRequiredCheckBox.Checked.ToString();
                technicalSkillDetail.IsTestingRequired = "N";
                technicalSkillDetail.SkillName = SkillTextBox.Text;
                technicalSkillDetail.Weightage = WeightageTextBox.Text.Trim();
                technicalSkillDetail.NoOfYearsExp = ExperienceTextBox.Text.Trim();

                technicalSkillDetailList.Add(technicalSkillDetail);
            }
            return technicalSkillDetailList;
        }

        /// <summary>
        /// Sets the preview data source.
        /// </summary>
        /// <value>The preview data source.</value>
        public SegmentDetail PreviewDataSource
        {
            set
            {
                TechnicalSkillRequirementControl_editTable.Visible = false;
                TechnicalSkillRequirementControl_previewTable.Visible = true;
                if (value.SegmentID != (int)SegmentType.TechnicalSkills)
                    throw new Exception("Invalid data source");
                technicalSkillDetails = new TechnicalSkillDetails();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(value.DataSource);
                Type idType = technicalSkillDetails.GetType();
                technicalSkillDetailsList = new List<TechnicalSkillDetails>();

                if (xmlDocument.GetElementsByTagName("TotalITExperience")[0].InnerText.Length > 0)
                {
                    TechnicalSkillRequirementControl_previewNumberOfYearstextLabel.Text =
                        xmlDocument.GetElementsByTagName("TotalITExperience")[0].InnerText;
                }

                if (xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText.Length > 0)
                {
                    TechnicalSkillRequirementControl_previewtotalWeightageTextLabel.Text =
                        xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText;
                }

                if (Convert.ToBoolean(xmlDocument.GetElementsByTagName("IsAdditionalSkill")[0].InnerText))
                {

                    if (xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText.Length > 0)
                    {

                        TechnicalSkillRequirementControl_previewAdditionalSkillTextLabel.Text = 
                            xmlDocument.GetElementsByTagName("AdditionalSkill")[0].
                                InnerText.ToString().Replace(Environment.NewLine, "<br />");
                    }
                    else
                    {
                        TechnicalSkillRequirementControl_previewAdditionalSkillLabel.Text = "";
                    }
                }
                else
                {
                    TechnicalSkillRequirementControl_previewAdditionalSkillLabel.Text = "";
                }
                for (int i = 0; i < xmlDocument.GetElementsByTagName("TechnicalSkillDetail").Count; i++)
                {
                    technicalSkillDetails = new TechnicalSkillDetails();
                    foreach (PropertyInfo pInfo in idType.GetProperties())
                    {
                        pInfo.SetValue(technicalSkillDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[i].InnerText, null);
                    }
                    technicalSkillDetails.CertificationRequired =
                        technicalSkillDetails.CertificationRequired == "True" ? "Yes" : "No";
                    technicalSkillDetails.IsTestingRequired =
                         technicalSkillDetails.IsTestingRequired == "True" ? "Yes" : "No";
                    technicalSkillDetails.IsInterviewRequired =
                        technicalSkillDetails.IsInterviewRequired == "True" ? "Yes" : "No";
                    technicalSkillDetailsList.Add(technicalSkillDetails);
                }

                TechnicalSkillRequirementControl_previewTechnicalSkillGridView.DataSource = technicalSkillDetailsList;
                TechnicalSkillRequirementControl_previewTechnicalSkillGridView.DataBind();
            }
        }

        /// <summary>
        /// Gets the total weightage.
        /// </summary>
        /// <value>The total weightage.</value>
        public decimal TotalWeightage
        {
            get
            {
                if (!Support.Utility.IsNullOrEmpty(TechnicalSkillRequirementControl_totalWeightageTextBox.Text))
                    _totalWeightage = Convert.ToDecimal(TechnicalSkillRequirementControl_totalWeightageTextBox.Text.Trim());
                return _totalWeightage;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private decimal _totalSegmentsWeightage = 0;

        /// <summary>
        /// Gets the total segments weightage.
        /// </summary>
        /// <value>The total segments weightage.</value>
        public decimal TotalSegmentsWeightage
        {
            get
            {
                TextBox weightageTextBox = null;
                foreach (GridViewRow row in TechnicalSkillRequirementControl_technicalSkillGridView.Rows)
                {
                    weightageTextBox = (TextBox)row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox");
                    if (!Support.Utility.IsNullOrEmpty(weightageTextBox.Text.Trim()))
                        _totalSegmentsWeightage = _totalSegmentsWeightage + Convert.ToDecimal(weightageTextBox.Text);
                }
                return _totalSegmentsWeightage;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<SkillWeightage> _skillWeightageDataSource;

        /// <summary>
        /// Gets the skill weightage data source.
        /// </summary>
        /// <value>The skill weightage data source.</value>
        public List<SkillWeightage> SkillWeightageDataSource
        {
            get
            {
                _skillWeightageDataSource = new List<SkillWeightage>();
                SkillWeightage skillWeightage = null;
                //  if (TotalWeightage > 0)
                {
                    decimal totalWeightage = 0;
                    if (!Support.Utility.IsNullOrEmpty(TechnicalSkillRequirementControl_totalWeightageTextBox.Text.Trim()))
                        totalWeightage = decimal.Parse(TechnicalSkillRequirementControl_totalWeightageTextBox.Text.Trim());

                    //decimal a=0;
                    TextBox weightageTextBox = null;
                    TextBox skillTextBox = null;
                    TextBox skillCategoryTextBox = null;
                    foreach (GridViewRow row in TechnicalSkillRequirementControl_technicalSkillGridView.Rows)
                    {
                        skillWeightage = new SkillWeightage();
                        weightageTextBox = (TextBox)row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox");
                        skillTextBox = (TextBox)row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillTextBox");
                        skillCategoryTextBox = (TextBox)row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryTextBox");
                        skillWeightage.SkillName = skillTextBox.Text.Trim();
                        skillWeightage.UserSegments = false;
                        skillWeightage.SkillTypes = SkillType.Technical;
                        skillWeightage.SkillCategory = skillCategoryTextBox.Text.Trim();
                        if (Support.Utility.IsNullOrEmpty(weightageTextBox.Text))
                            weightageTextBox.Text = "0";
                        skillWeightage.Weightage = (decimal.Parse(weightageTextBox.Text.Trim()) / 100) * totalWeightage;
                        _skillWeightageDataSource.Add(skillWeightage);
                    }
                }
                return _skillWeightageDataSource;
            }
        }
        #endregion Properties

        #region Event Handler
        /// <summary>
        /// Represents the event that is called on page load 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    if (Support.Utility.IsNullOrEmpty(ViewState[TECHNICALSKILLS_VIEWSTATE]))
                        DefaultTechnicalSkill();
                    else
                    {
                        technicalSkillDetailsList = new List<TechnicalSkillDetails>();
                        technicalSkillDetailsList = ViewState[TECHNICALSKILLS_VIEWSTATE] as List<TechnicalSkillDetails>;
                        TechnicalSkillRequirementControl_technicalSkillGridView.DataSource = technicalSkillDetailsList;
                        TechnicalSkillRequirementControl_technicalSkillGridView.DataBind();
                        MaintainStateInGridView(technicalSkillDetailsList, false);
                    }
                }
                else
                {
                    ViewState[TECHNICALSKILLS_VIEWSTATE] = TechnicalSkillGridViewData();
                    TechnicalSkillRequirementControl_numberOfYearsTextBox.Focus();
                }
                TechnicalSkillRequirementControl_additionalSkillCheckBox.Attributes.Add("onclick", "javascript:ShowDiv('" + TechnicalSkillRequirementControl_additionalSkillCheckBox.ClientID + "','" +
                                        TechnicalSkillRequirementControl_additionalSkillDiv.ClientID +
                                        "','" + TechnicalSkillRequirementControl_additionalSkillTextBox.ClientID
                                        + "');");
                if (TechnicalSkillRequirementControl_additionalSkillCheckBox.Checked)
                    TechnicalSkillRequirementControl_additionalSkillDiv.Style.Add("display", "block");
                else
                    TechnicalSkillRequirementControl_additionalSkillDiv.Style.Add("display", "none");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Represents the event handler that is called when add new row button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TechnicalSkillRequirementControl_addRowImageButton_Click(object sender, EventArgs e)
        {
            try
            {
                //if (AddTechnical != null)
                //    this.AddTechnical(sender, e);
                technicalSkillDetailsList = new List<TechnicalSkillDetails>();
                technicalSkillDetailsList = TechnicalSkillGridViewData();
                if (technicalSkillDetailsList == null)
                {
                    technicalSkillDetailsList = new List<TechnicalSkillDetails>();
                }
                technicalSkillDetails = new TechnicalSkillDetails();
                technicalSkillDetailsList.Add(technicalSkillDetails);
                ViewState[TECHNICALSKILLS_VIEWSTATE] = technicalSkillDetailsList;
                TechnicalSkillRequirementControl_technicalSkillGridView.DataSource = technicalSkillDetailsList;
                TechnicalSkillRequirementControl_technicalSkillGridView.DataBind();
                MaintainStateInGridView(technicalSkillDetailsList, true);
                GridViewRow lastRow = TechnicalSkillRequirementControl_technicalSkillGridView.Rows
                  [TechnicalSkillRequirementControl_technicalSkillGridView.Rows.Count - 1];
                
                /*TextBox skillCategory = (TextBox)lastRow.FindControl
                    ("TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryTextBox");
                skillCategory.Focus();*/

                TextBox skillNameTextBox = (TextBox)lastRow.FindControl
                    ("TechnicalSkillRequirementControl_technicalSkillGridView_skillTextBox");
                skillNameTextBox.Focus();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the TechnicalSkillRequirementControl_technicalSkillGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/>
        /// instance containing the event data.</param>
        protected void TechnicalSkillRequirementControl_technicalSkillGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteSkill")
            {
                technicalSkillDetailsList = new List<TechnicalSkillDetails>();
                technicalSkillDetailsList = ViewState[TECHNICALSKILLS_VIEWSTATE] as List<TechnicalSkillDetails>;

                if (technicalSkillDetailsList.Count > 0)
                {
                    technicalSkillDetailsList.RemoveAt(int.Parse(e.CommandArgument.ToString()));
                }
                TechnicalSkillRequirementControl_technicalSkillGridView.DataSource = technicalSkillDetailsList;
                TechnicalSkillRequirementControl_technicalSkillGridView.DataBind();
                ViewState[TECHNICALSKILLS_VIEWSTATE] = technicalSkillDetailsList;

              //  ScriptManager.RegisterStartupScript(this, this.GetType(), "reg", "SetSplitWeightage()", true);  
            }

        }


        /// <summary>
        /// Handles the RowDataBound event of the TechnicalSkillRequirementControl_technicalSkillGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> 
        /// instance containing the event data.</param>
        protected void TechnicalSkillRequirementControl_technicalSkillGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    /*TextBox skillCategoryTextBox = (TextBox)e.Row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillCategoryTextBox");
                    ImageButton delteImgaeButton = (ImageButton)e.Row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_deleteSkillsImageButton");
                    delteImgaeButton.CommandArgument = e.Row.RowIndex.ToString();
                    AutoCompleteExtender skillNameExtender = (AutoCompleteExtender)e.Row.FindControl("SearchCategorySubjectControl_skillNameAutoCompleteExtender");
                    skillCategoryTextBox.Attributes.Add("onblur", "CheckValues('" + skillCategoryTextBox.ClientID + "','" + skillNameExtender.ClientID + "')");*/

                    TextBox skilNameTextBox = (TextBox)e.Row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillTextBox");
                    ImageButton delteImgaeButton = (ImageButton)e.Row.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_deleteSkillsImageButton");
                    delteImgaeButton.CommandArgument = e.Row.RowIndex.ToString();
                    AutoCompleteExtender categoryNameExtender = (AutoCompleteExtender)e.Row.FindControl("SearchCategorySubjectControl_categoryAutoCompleteExtender");
                    skilNameTextBox.Attributes.Add("onblur", "CheckValues('" + skilNameTextBox.ClientID + "','" + categoryNameExtender.ClientID + "')");  


                    // weightageTextBox.Attributes.Add("onblur", "AddTotal('TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox','" + TechnicalSkillRequirementControll_gridWeightageHiddenField.ClientID + "');");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion Event Handler

        #region Private Methods

        public void DefaultTechnicalSkill()
        {
            technicalSkillDetailsList = new List<TechnicalSkillDetails>();
            technicalSkillDetails = new TechnicalSkillDetails();
            technicalSkillDetailsList.Add(technicalSkillDetails);
            TechnicalSkillRequirementControl_technicalSkillGridView.DataSource = technicalSkillDetailsList;
            TechnicalSkillRequirementControl_technicalSkillGridView.DataBind();
            ViewState[TECHNICALSKILLS_VIEWSTATE] = technicalSkillDetailsList;
        }

        private void MaintainStateInGridView(List<TechnicalSkillDetails> technicalSkillDetailsList, bool addControl)
        {
            int j = TechnicalSkillRequirementControl_technicalSkillGridView.Rows.Count;
            if (addControl)
                j = j - 1;

            for (int i = 0; i < j; i++)
            {
                DropDownList mandatoryDropDownList = ((DropDownList)TechnicalSkillRequirementControl_technicalSkillGridView.Rows[i].FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_mandatoryDropDownList"));
                mandatoryDropDownList.SelectedItem.Selected = false;
                mandatoryDropDownList.Items.FindByText(technicalSkillDetailsList[i].IsMandatory).Selected = true;

                DropDownList skillLevelDropDownList = ((DropDownList)TechnicalSkillRequirementControl_technicalSkillGridView.Rows[i].FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_skillLevelDropdownList"));
                skillLevelDropDownList.SelectedItem.Selected = false;
                skillLevelDropDownList.Items.FindByText(technicalSkillDetailsList[i].SkillRequired).Selected = true;
            }
        }

        private void ProcessXML(string xmlText)
        {
            technicalSkillDetails = new TechnicalSkillDetails();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlText);
            Type idType = technicalSkillDetails.GetType();
            technicalSkillDetailsList = new List<TechnicalSkillDetails>();

            if (xmlDocument.GetElementsByTagName("TotalITExperience")[0].InnerText.Length > 0)
            {
                TechnicalSkillRequirementControl_numberOfYearsTextBox.Text =
                    xmlDocument.GetElementsByTagName("TotalITExperience")[0].InnerText;
            }

            if (xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText.Length > 0)
            {
                TechnicalSkillRequirementControl_totalWeightageTextBox.Text =
                    xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText;
            }

            if (Convert.ToBoolean(xmlDocument.GetElementsByTagName("IsAdditionalSkill")[0].InnerText))
            {
                TechnicalSkillRequirementControl_additionalSkillCheckBox.Checked = true;

                if (xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText.Length > 0)
                {
                    TechnicalSkillRequirementControl_additionalSkillTextBox.Text =
                        xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText;

                }
            }

            for (int i = 0; i < xmlDocument.GetElementsByTagName("TechnicalSkillDetail").Count; i++)
            {
                technicalSkillDetails = new TechnicalSkillDetails();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    if (xmlDocument.GetElementsByTagName(pInfo.Name)[i] != null)
                        pInfo.SetValue(technicalSkillDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[i].InnerText, null);
                }
                technicalSkillDetailsList.Add(technicalSkillDetails);
            }
            ViewState[TECHNICALSKILLS_VIEWSTATE] = technicalSkillDetailsList;
            TechnicalSkillRequirementControl_technicalSkillGridView.DataSource = technicalSkillDetailsList;
            TechnicalSkillRequirementControl_technicalSkillGridView.DataBind();
            MaintainStateInGridView(technicalSkillDetailsList, false);
            if (TechnicalSkillRequirementControl_additionalSkillCheckBox.Checked)
                TechnicalSkillRequirementControl_additionalSkillDiv.Style.Add("display", "block");
            else
                TechnicalSkillRequirementControl_additionalSkillDiv.Style.Add("display", "none");
        }

        public void ResetControls()
        {
            TechnicalSkillRequirementControl_numberOfYearsTextBox.Text = "";
            TechnicalSkillRequirementControl_additionalSkillCheckBox.Checked = false;
            TechnicalSkillRequirementControl_additionalSkillTextBox.Text = "";
            DefaultTechnicalSkill();
        }

        public void DisableControls()
        {
            TechnicalSkillRequirementControl_technicalSkillGridView.Enabled = false;
            TechnicalSkillRequirementControl_numberOfYearsTextBox.Enabled = false;
            TechnicalSkillRequirementControl_additionalSkillTextBox.Enabled = false;
            TechnicalSkillRequirementControl_additionalSkillCheckBox.Enabled = false;
            TechnicalSkillRequirementControl_addRowLinkButton.Enabled = false;
        }
        #endregion Private Methods

        /*protected void TechnicalSkillRequirementControl_totalWeightageTextBox_TextChanged(object sender, EventArgs e)
        {
            try 
            {

                if (string.IsNullOrEmpty(TechnicalSkillRequirementControl_totalWeightageTextBox.Text) ||
                    Convert.ToInt32(TechnicalSkillRequirementControl_totalWeightageTextBox.Text) == 0)
                    return;

                SetTotalWeightage(Convert.ToInt32(TechnicalSkillRequirementControl_totalWeightageTextBox.Text));

            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void SetTotalWeightage(int weightage)
        {
            if (weightage == 0) return;

            if (TechnicalSkillRequirementControl_technicalSkillGridView.Rows.Count==0) return;

            int rowCount = TechnicalSkillRequirementControl_technicalSkillGridView.Rows.Count;

            int eachRowWeightage = weightage / rowCount;

            foreach (GridViewRow gRow in TechnicalSkillRequirementControl_technicalSkillGridView.Rows)
            {
                TextBox TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox =
                    (TextBox)gRow.FindControl("TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox");
                if (TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox != null)
                    TechnicalSkillRequirementControl_technicalSkillGridView_weightageTextBox.Text = eachRowWeightage.ToString();
            }
        }*/

    }
}