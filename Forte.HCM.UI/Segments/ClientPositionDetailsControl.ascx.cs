﻿#region Directives                                                   
using System;
using System.Xml;
using System.Text;
using System.Web.UI;

using System.Reflection;

using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Segments
{
    public partial class ClientPositionDetailsControl : UserControl
    {
        #region Properties                                           
        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>The data source.</value>
        public SegmentDetail DataSource
        {
            set
            {
                if (value.SegmentID != (int)SegmentType.ClientPositionDetail)
                    throw new Exception("Invalid data source");
                ProcessXML(value.DataSource);
            }
            get
            {
                StringBuilder selectedNatureOfPositionLevel = new StringBuilder();
                for (int i = 0; i < ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items.Count; i++)
                {
                    if (ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Selected)
                    {
                        selectedNatureOfPositionLevel.Append(ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Value);
                        selectedNatureOfPositionLevel.Append(",");
                    }
                }

                ClientPositionDetails clientPositionDetails = new ClientPositionDetails();
                //clientPositionDetails.ClientName = ClientPositionDetailsControl_clientNameTextBox.Text;
                clientPositionDetails.ClientName = string.Empty;
                //clientPositionDetails.Location = ClientPositionDetailsControl_locationTextBox.Text;
                clientPositionDetails.Location = string.Empty;
                clientPositionDetails.NumberOfPositions = ClientPositionDetailsControl_numberOfPositionsTextBox.Text;

                clientPositionDetails.PositionID = ClientPositionDetailsControl_positionIdTextBox.Text;
                clientPositionDetails.PositionName = ClientPositionDetailsControl_positionNameTextBox.Text;
                clientPositionDetails.TargetedStartDate = ClientPositionDetailsControl_targetedStartDateTextBox.Text;
                clientPositionDetails.ContractToHire = ClientPositionDetailsControl_contractToHireCheckBox.Checked.ToString();
                if (ClientPositionDetailsControl_contractToHireCheckBox.Checked)
                {
                    clientPositionDetails.Duration = ClientPositionDetailsControl_durationTextBox.Text;
                    clientPositionDetails.NatureOfPosition = selectedNatureOfPositionLevel.ToString() + "3";
                }
                else
                {
                    clientPositionDetails.NatureOfPosition = selectedNatureOfPositionLevel.ToString().TrimEnd(',');
                    clientPositionDetails.Duration = "";
                }

                clientPositionDetails.PositionProfileName = "test";
                SegmentDetail segmentDetail = new SegmentDetail();
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode xmlNode = xmlDocument.CreateNode(XmlNodeType.XmlDeclaration, "", "");
                xmlDocument.AppendChild(xmlNode);
                XmlElement xmlElementRoot = xmlDocument.CreateElement("ClientPositionDetails");
                xmlDocument.AppendChild(xmlElementRoot);
                XmlElement xmlElementSegmentItem = null;
                Type idType = clientPositionDetails.GetType();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    object o = pInfo.GetValue(clientPositionDetails, null);
                    xmlElementSegmentItem = xmlDocument.CreateElement(pInfo.Name);
                    xmlElementSegmentItem.InnerText = o.ToString();
                    xmlElementRoot.AppendChild(xmlElementSegmentItem);
                }
                segmentDetail.DisplayOrder = 0;
                segmentDetail.SegmentType = SegmentType.ClientPositionDetail;
                segmentDetail.DataSource = xmlDocument.InnerXml;
                segmentDetail.SegmentID = (int)SegmentType.ClientPositionDetail;
                return segmentDetail;
            }
        }

        /// <summary>
        /// Sets the preview data source.
        /// </summary>
        /// <value>The preview data source.</value>
        public SegmentDetail PreviewDataSource
        {
            set
            {
                if (value.SegmentID != (int)SegmentType.ClientPositionDetail)
                    throw new Exception("Invalid data source");

                ClientPositionDetailsControl_previewTable.Visible = true;
                ClientPositionDetailsControl_editTable.Visible = false;
                ClientPositionDetails clientPositionDetails = new ClientPositionDetails();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(value.DataSource);
                Type idType = clientPositionDetails.GetType();
                foreach (PropertyInfo pInfo in idType.GetProperties())
                {
                    pInfo.SetValue(clientPositionDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
                }
                //ClientPositionDetailsControl_previewClientNameTextNameLabel.Text = clientPositionDetails.ClientName;

                //ClientPositionDetailsControl_previewLocationTextLabel.Text = clientPositionDetails.Location;

                ClientPositionDetailsControl_previewPositionIDTextLabel.Text = clientPositionDetails.PositionID;
                ClientPositionDetailsControl_previewTargetDateTextLabel.Text = clientPositionDetails.TargetedStartDate;
                ClientPositionDetailsControl_previewPositionNameTextLabel.Text = clientPositionDetails.PositionName;
                ClientPositionDetailsControl_previewNoOfPositionTextLabel.Text = clientPositionDetails.NumberOfPositions;
                string[] natureOfPosition = clientPositionDetails.NatureOfPosition.Split(',');
                string natureOfPositionItem = "";
                foreach (string item in natureOfPosition)
                {
                    for (int i = 0; i < ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items.Count; i++)
                    {
                        if (ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Value == item)
                            natureOfPositionItem = natureOfPositionItem + ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Text + ",";
                    }
                }
                ClientPositionDetailsControl_previewNatureOfPositionTextLabel.Text = natureOfPositionItem.TrimEnd(',');
                if (Support.Utility.IsNullOrEmpty(clientPositionDetails.Duration))
                {
                    ClientPositionDetailsControl_previewDurationLabel.Text = "";
                    ClientPositionDetailsControl_previewDurationTextLabel.Text = "";
                }
                else
                    ClientPositionDetailsControl_previewDurationTextLabel.Text = clientPositionDetails.Duration;

            }
        }

        #endregion Properties

        #region Event Handlers                                       
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ClientPositionDetailsControl_contractToHireCheckBox.Attributes.Add("onclick",
                "javascript:ShowDiv('" + ClientPositionDetailsControl_contractToHireCheckBox.ClientID + "','" +
                ClientPositionDetailsControl_durationDIV.ClientID +
                "','" + ClientPositionDetailsControl_durationTextBox.ClientID + "');");

            if (ClientPositionDetailsControl_contractToHireCheckBox.Checked)
                ClientPositionDetailsControl_durationDIV.Style.Add("display", "block");
            else
                ClientPositionDetailsControl_durationDIV.Style.Add("display", "none");

        }
        #endregion Event Handlers

        #region Private Methods                                      
        /// <summary>
        /// Processes the XML.
        /// </summary>
        /// <param name="xmlText">The XML text.</param>
        private void ProcessXML(string xmlText)
        {
            ClientPositionDetails clientPositionDetails = new ClientPositionDetails();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlText);
            Type idType = clientPositionDetails.GetType();
            foreach (PropertyInfo pInfo in idType.GetProperties())
            {
                pInfo.SetValue(clientPositionDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
            }
            //ClientPositionDetailsControl_clientNameTextBox.Text = clientPositionDetails.ClientName;
            ClientPositionDetailsControl_durationTextBox.Text = clientPositionDetails.Duration;
            //ClientPositionDetailsControl_locationTextBox.Text = clientPositionDetails.Location;
            ClientPositionDetailsControl_numberOfPositionsTextBox.Text = clientPositionDetails.NumberOfPositions;
            ClientPositionDetailsControl_positionIdTextBox.Text = clientPositionDetails.PositionID;
            ClientPositionDetailsControl_targetedStartDateTextBox.Text = clientPositionDetails.TargetedStartDate;
            ClientPositionDetailsControl_positionNameTextBox.Text = clientPositionDetails.PositionName;
            if (Convert.ToBoolean(clientPositionDetails.ContractToHire))
            {
                ClientPositionDetailsControl_contractToHireCheckBox.Checked = true;
                ClientPositionDetailsControl_durationTextBox.Text = clientPositionDetails.Duration;
            }

            string[] natureOfPosition = clientPositionDetails.NatureOfPosition.Split(',');
            foreach (string item in natureOfPosition)
            {
                for (int i = 0; i < ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items.Count; i++)
                {
                    if (ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Value == item)
                        ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Selected = true;
                }
            }
        }

        /// <summary>
        /// Resets the controls.
        /// </summary>
        public void ResetControls()
        {
            //ClientPositionDetailsControl_clientNameTextBox.Text = "";
            ClientPositionDetailsControl_positionNameTextBox.Text = "";
            ClientPositionDetailsControl_positionIdTextBox.Text = "";
            //ClientPositionDetailsControl_locationTextBox.Text = "";
            ClientPositionDetailsControl_targetedStartDateTextBox.Text = "";
            ClientPositionDetailsControl_numberOfPositionsTextBox.Text = "";
            ClientPositionDetailsControl_natureOfPositionsCheckBoxList.ClearSelection();
            ClientPositionDetailsControl_durationTextBox.Text = "";
        }

        /// <summary>
        /// Disables the controls.
        /// </summary>
        public void DisableControls()
        {
            //ClientPositionDetailsControl_clientNameTextBox.Enabled = false;
            ClientPositionDetailsControl_positionNameTextBox.Enabled = false;
            ClientPositionDetailsControl_positionIdTextBox.Enabled = false;
            //ClientPositionDetailsControl_locationTextBox.Enabled = false;
            ClientPositionDetailsControl_targetedStartDateTextBox.Enabled = false;
            ClientPositionDetailsControl_numberOfPositionsTextBox.Enabled = false;
            ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Enabled = false;
            ClientPositionDetailsControl_durationTextBox.Enabled = false;
            ClientPositionDetailsControl_calendarImageButton.Enabled = false;
        }
        #endregion Private Methods
    }
}