﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;


using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

namespace Forte.HCM.UI.CommonControls
{
    public partial class PositionProfileViewerControl : UserControl
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Events & Delegates

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;

        #endregion Events & Delegates

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            // Set default focus.
            PositionProfileViewerControl_commentsTextBox.Focus();

            // Check and set expanded or restore state.
            CheckAndSetExpandorRestore();

            // Set default button.
            Page.Form.DefaultButton = PositionProfileViewerControl_searchButton.UniqueID;

            // Add handler to page navigator.
            PositionProfileViewerControl_pageNavigator.PageNumberClick += new
               PageNavigator.PageNumberClickEventHandler
               (PositionProfileViewerControl_pageNavigator_PageNumberClick);

            PositionProfileViewerControl_gridViewHeaderTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                 PositionProfileViewerControl_activityLogDetailDIV.ClientID + "','" +
                 PositionProfileViewerControl_searchDIV.ClientID + "','" +
                 PositionProfileViewerControl_searchResultsUpSpan.ClientID + "','" +
                 PositionProfileViewerControl_searchResultsDownSpan.ClientID + "','" +
                 PositionProfileViewerControl_restoreHiddenField.ClientID + "','" +
                 RESTORED_HEIGHT + "','" +
                 EXPANDED_HEIGHT + "')");

            // Add handler to load candidate popup.
            PositionProfileViewerControl_candidateImageButton.Attributes.Add("onclick",
                "return LoadCandidateInfo('" + PositionProfileViewerControl_positionProfileIDHiddenField.ClientID + "','"
                + PositionProfileViewerControl_positionProfileTextBox.ClientID + "')");

            // Add handler to load user popup.
            PositionProfileViewerControl_activityByImageButton.Attributes.Add("onclick",
                 "javascript:return LoadUserForActivityLog('"
                + PositionProfileViewerControl_activityByHiddenField.ClientID + "','"
                + PositionProfileViewerControl_activityByTextBox.ClientID + "')");

            if (!IsPostBack)
            {
                List<AttributeDetail> attributes = new AttributeBLManager().
                        GetAttributesByType(Constants.AttributeTypes.POSITIOPROFILE_ACTIVITY, "A");
                PositionProfileViewerControl_activityTypeDropDownList.DataSource = attributes;
                PositionProfileViewerControl_activityTypeDropDownList.DataBind();
                PositionProfileViewerControl_activityTypeDropDownList.DataTextField = "AttributeName";
                PositionProfileViewerControl_activityTypeDropDownList.DataValueField = "AttributeID";
                PositionProfileViewerControl_activityTypeDropDownList.DataBind();
                PositionProfileViewerControl_activityTypeDropDownList.Items.Insert
                    (0, new ListItem("--Select--", string.Empty));
            }
        }

        protected void PositionProfileViewerControl_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ThrowMessage(string.Empty);

                // Check if candidate ID is present.
                /*if (this.PositionProfileID == 0)
                {
                    ThrowMessage("No candidate was selected");
                    return;
                }*/

                ViewState["SORT_FIELD"] = "ActivityDate";
                ViewState["SORT_ORDER"] = SortType.Descending;
                LoadValues(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        private void PositionProfileViewerControl_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load values.
                LoadValues(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        protected void PositionProfileViewerControl_activityLogDetailGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] = ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;


                ViewState["SORT_FIELD"] = e.SortExpression;
                PositionProfileViewerControl_pageNavigator.Reset();
                LoadValues(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        protected void PositionProfileViewerControl_activityLogDetailGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = new ControlUtility().GetSortColumnIndex
                    (PositionProfileViewerControl_activityLogDetailGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    new ControlUtility().AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        #endregion Event Handlers

        #region Public Properties

        public int PositionProfileID
        {
            set
            {
                PositionProfileViewerControl_positionProfileIDHiddenField.Value = value.ToString();

                // Check if candidate ID is not empty.
                if (!Utility.IsNullOrEmpty(PositionProfileViewerControl_positionProfileIDHiddenField.Value))
                {
                    int positionProfileId = 0;
                    if (int.TryParse(PositionProfileViewerControl_positionProfileIDHiddenField.Value.Trim(), out positionProfileId) == true)
                    {
                        // Get candidate information.

                        PositionProfileDetail positionprofileInfo = new PositionProfileBLManager().
                         GetPositionProfileInformation(positionProfileId);

                        PositionProfileViewerControl_positionProfileTextBox.Text = positionprofileInfo.PositionProfileName;

                        // Load the default search records.
                        // Clear messages.
                        ThrowMessage(string.Empty);

                        ViewState["SORT_FIELD"] = "ActivityDate";
                        ViewState["SORT_ORDER"] = SortType.Descending;
                        LoadValues(1);
                    }
                }
            }
            get
            {
                if (Utility.IsNullOrEmpty(PositionProfileViewerControl_positionProfileIDHiddenField.Value))
                    return 0;
                else
                    return Convert.ToInt32(PositionProfileViewerControl_positionProfileIDHiddenField.Value);
            }
        }

        public string PositionProfileName
        {
            get
            {
                return PositionProfileViewerControl_positionProfileTextBox.Text.Trim();
            }
        }

        public int GridPageSize
        {
            set
            {
                PositionProfileViewerControl_gridPageSizeHiddenField.Value = value.ToString();
            }
            get
            {
                if (Utility.IsNullOrEmpty(PositionProfileViewerControl_gridPageSizeHiddenField.Value))
                    return 10;
                else
                    return Convert.ToInt32(PositionProfileViewerControl_gridPageSizeHiddenField.Value);
            }
        }

        public bool AllowChangeCandidate
        {
            set
            {
                PositionProfileViewerControl_candidateImageButton.Visible = value;
            }
        }


        #endregion Public Properties

        #region Public Methods

        public void Reset()
        {
            PositionProfileViewerControl_positionProfileTextBox.Text = string.Empty;
            PositionProfileViewerControl_positionProfileIDHiddenField.Value = string.Empty;
            PositionProfileViewerControl_activityByTextBox.Text = string.Empty;
            PositionProfileViewerControl_activityByHiddenField.Value = string.Empty;
            PositionProfileViewerControl_activityTypeDropDownList.SelectedIndex = 0;

            PositionProfileViewerControl_activityDateFromTextBox.Text = string.Empty;
            PositionProfileViewerControl_activityDateToTextBox.Text = string.Empty;
            PositionProfileViewerControl_commentsTextBox.Text = string.Empty;
            PositionProfileViewerControl_activityLogDetailGridView.DataSource = null;
            PositionProfileViewerControl_activityLogDetailGridView.DataBind();

            PositionProfileViewerControl_pageNavigator.Reset();

            PositionProfileViewerControl_restoreHiddenField.Value = string.Empty;
            CheckAndSetExpandorRestore();
        }

        #endregion Public Methods


        #region Private Methods

        private void LoadValues(int pageNumber)
        {
            int totalRecords = 0;

            // Get search critera.
            PositionProfileActivityLog searchCriteria = GetSearchCriteria();

            // Get searched results.
            List<PositionProfileActivityLog> logs = new PositionProfileBLManager().GetPositionProfileActivities(
                    searchCriteria,
                    pageNumber,
                    this.GridPageSize,
                    ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"],
                    out totalRecords);

            if (logs == null || logs.Count == 0)
            {
                PositionProfileViewerControl_activityLogDetailDIV.Visible = false;
                ThrowMessage(Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                PositionProfileViewerControl_activityLogDetailDIV.Visible = true;
            }

            PositionProfileViewerControl_activityLogDetailGridView.DataSource = logs;
            PositionProfileViewerControl_activityLogDetailGridView.DataBind();
            PositionProfileViewerControl_pageNavigator.PageSize = this.GridPageSize;
            PositionProfileViewerControl_pageNavigator.TotalRecords = totalRecords;
        }

        private PositionProfileActivityLog GetSearchCriteria()
        {
            PositionProfileActivityLog searchCriteria = new PositionProfileActivityLog();

            searchCriteria.PositionProfileID = Convert.ToInt32(PositionProfileViewerControl_positionProfileIDHiddenField.Value);
            searchCriteria.ActivityType = PositionProfileViewerControl_activityTypeDropDownList.SelectedValue;

            searchCriteria.PositionProfileName = PositionProfileViewerControl_positionProfileTextBox.Text;

            if (!Utility.IsNullOrEmpty(PositionProfileViewerControl_activityByHiddenField.Value))
                searchCriteria.ActivityBy = Convert.ToInt32(PositionProfileViewerControl_activityByHiddenField.Value);

            searchCriteria.ActivityDateFrom = PositionProfileViewerControl_activityDateFromTextBox.Text.Trim() == string.Empty ?
                Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(PositionProfileViewerControl_activityDateFromTextBox.Text.Trim());

            searchCriteria.ActivityDateTo = PositionProfileViewerControl_activityDateToTextBox.Text.Trim() == string.Empty ?
                Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(PositionProfileViewerControl_activityDateToTextBox.Text.Trim()).AddDays(1);

            searchCriteria.Comments = PositionProfileViewerControl_commentsTextBox.Text.Trim();

            return searchCriteria;
        }

        private void ThrowMessage(string message)
        {
            if (ControlMessageThrown != null)
            {
                ControlMessageThrown(this, new ControlMessageEventArgs
                    (message, MessageType.Error));
            }
        }

        /// <summary>
        /// Method that will return the date in MM/dd/yyyy format. If date not exists, 
        /// then it displays empty value in column.
        /// </summary>
        /// <param name="date">This parameter is passed from aspx page value 
        /// </param>
        /// <returns></returns>
        public string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }

        /// <summary>
        /// Method that retrieves the table that is used for download.
        /// </summary>
        public DataTable GetDownloadTable()
        {
            try
            {
                // Get search critera.
                PositionProfileActivityLog searchCriteria = GetSearchCriteria();

                int totalRecords = 0;

                // Get searched results.
                return new PositionProfileBLManager().GetPositionProfileActivitiesTable(
                    searchCriteria,
                    1,
                    int.MaxValue,
                    ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"],
                    out totalRecords);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
                return null;
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(PositionProfileViewerControl_restoreHiddenField.Value) &&
                PositionProfileViewerControl_restoreHiddenField.Value == "Y")
            {
                PositionProfileViewerControl_searchDIV.Style["display"] = "none";
                PositionProfileViewerControl_searchResultsUpSpan.Style["display"] = "block";
                PositionProfileViewerControl_searchResultsDownSpan.Style["display"] = "none";
                PositionProfileViewerControl_activityLogDetailDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                PositionProfileViewerControl_searchDIV.Style["display"] = "block";
                PositionProfileViewerControl_searchResultsUpSpan.Style["display"] = "none";
                PositionProfileViewerControl_searchResultsDownSpan.Style["display"] = "block";
                PositionProfileViewerControl_activityLogDetailDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods
    }
}