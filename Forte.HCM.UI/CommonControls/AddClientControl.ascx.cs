﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AddClientControl.cs
// File that represents the user interface to add client detail.

#endregion Header

#region Directives

using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Reflection;
using Forte.HCM.Support;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Represents the class for the add client control
    /// This page helps to add client/department/contact details
    /// </summary>
    public partial class AddClientControl : System.Web.UI.UserControl
    {
        #region Private Constants                                              
        
        string CLIENT_ATTRIBUTES_DATASOUCE = "CLIENT_ATTRIBUTES_DATASOUCE";
        string DEPARTMENT_ATTRIBUTES_DATASOURCE = "DEPARTMENT_ATTRIBUTES_DATASOURCE";
        string CLIENT_CONTACT_ATTRIBUTES_DATASOURCE = "CLIENT_CONTACT_ATTRIBUTES_DATASOURCE";

        #endregion Private Constants

        #region Deleagtes                                                      
        
        // The public events must conform to this format
        public delegate void Button_CommandClick(object sender, CommandEventArgs e);

        // These two public events are to be customized as need on the page and will be called
        // by the private events of the buttons in the user control
        public event Button_CommandClick OkCommandClick;

        #endregion Deleagtes

        #region Event Handlers                                                 

        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    AddClientControl_showDepartmentDiv.Style["display"] = "none";
                    AddClientControl_showContactDiv.Style["display"] = "none";

                    AddClientControl_isDepartmentRequiredCheckBox.Checked = false;
                    AddClientControl_isContactRequiredCheckBox.Checked = false;
                    ClientControl_clientNameTextBox.Focus();
                }
                else
                {
                    if (!AddClientControl_isDepartmentRequiredCheckBox.Checked)
                    {
                        AddClientControl_showDepartmentDiv.Style["display"] = "none";
                    }

                    if (!AddClientControl_isContactRequiredCheckBox.Checked)
                    {
                        AddClientControl_showContactDiv.Style["display"] = "none";
                    }
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when the add button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="CommandEventArgs"/> that holds the event data.
        /// </param>
        protected void AddClientControl_createButton_Command(object sender,
            System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (!IsValid())
                return;

            if (OkCommandClick != null)
                this.OkCommandClick(sender, e);
        }

        /// <summary>
        /// Hanlder method that will be called when department primary address checkbox is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void DepartmentControl_primaryAddressCheckbox_CheckedChanged(
            object sender, EventArgs e)
        {
            try
            {
                AssignDepartmentPrimaryAddress();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when contact primary address checkbox is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ContactControl_primaryAddressCheckbox_CheckedChanged(
            object sender, EventArgs e)
        {
            try
            {
                AssignContactPrimaryAddress();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when department required check box is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AddClientControl_isDepartmentRequiredCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                InvokePageBaseMethod("ShowClientControlModalPopUp", null);
                if (AddClientControl_isDepartmentRequiredCheckBox.Checked)
                {
                    AddClientControl_showDepartmentDiv.Style["display"] = "block";
                    DepartmentControl_primaryAddressCheckbox.Checked = true;
                    AssignDepartmentPrimaryAddress();
                }
                else
                {
                    DepartmentControl_primaryAddressCheckbox.Checked = false;
                    AddClientControl_showDepartmentDiv.Style["display"] = "none";
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when contact required check box is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AddClientControl_isContactRequiredCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                InvokePageBaseMethod("ShowClientControlModalPopUp", null);
                if (AddClientControl_isContactRequiredCheckBox.Checked)
                {
                    ContactControl_primaryAddressCheckbox.Checked = true;
                    AddClientControl_showContactDiv.Style["display"] = "block";
                    AssignContactPrimaryAddress();
                }
                else
                {
                    ContactControl_primaryAddressCheckbox.Checked = false;
                    AddClientControl_showContactDiv.Style["display"] = "none";
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }

        #endregion Event Handlers

        #region Public Properties                                              

        /// <summary>
        /// That sets the department required check box value
        /// </summary>
        public bool CheckBoxDepartmentProperty
        {
            get { return AddClientControl_isDepartmentRequiredCheckBox.Checked; }
            set { AddClientControl_isDepartmentRequiredCheckBox.Checked = value; }
        }

        /// <summary>
        /// That sets the contact required check box value
        /// </summary>
        public bool CheckBoxContactProperty
        {
            get { return AddClientControl_isContactRequiredCheckBox.Checked; }
            set { AddClientControl_isContactRequiredCheckBox.Checked = value; }
        }

        /// <summary>
        /// That sets the value to department div style
        /// </summary>
        public string HideDepartmentDiv
        {
            set 
            {
                AddClientControl_showDepartmentDiv.Style["display"] = value; 
            }
        }

        /// <summary>
        /// That sets the value to contact div style
        /// </summary>
        public string HideContactDiv
        {
            set
            {
                AddClientControl_showContactDiv.Style["display"] = value;
            }
        }


        /// <summary>
        /// Method to set the tenant id
        /// </summary>
        public int TenantID
        {
            set
            {
                AddClientControl_tenantIDHiddenField.Value = value.ToString();
            }
            get
            {
                if (Utility.IsNullOrEmpty(AddClientControl_tenantIDHiddenField.Value))
                    return 0;

                return Convert.ToInt32(AddClientControl_tenantIDHiddenField.Value);
            }
        }

        /// <summary>
        /// Method to set the user id
        /// </summary>
        public int UserID
        {
            set
            {
                AddClientControl_userIDHiddenField.Value = value.ToString();
            }
            get
            {
                if (Utility.IsNullOrEmpty(AddClientControl_userIDHiddenField.Value))
                    return 0;

                return Convert.ToInt32(AddClientControl_userIDHiddenField.Value);
            }
        }

        /// <summary>
        /// Method that gets and sets the client information
        /// </summary>
        public ClientInformation ClientDataSource
        {
            set
            {
                if (value == null)
                {
                    ClientControl_additionalInfoTextBox.Text = string.Empty;
                    ClientControl_streetAddressTextBox.Text = string.Empty;
                    ClientControl_eMailIDTextBox.Text = string.Empty;
                    ClientControl_cityTextBox.Text = string.Empty;
                    ClientControl_countryTextBox.Text = string.Empty;
                    ClientControl_stateTextBox.Text = string.Empty;
                    ClientControl_zipCodeTextBox.Text = string.Empty;
                    ClientControl_clientNameTextBox.Text = string.Empty;
                    ClientControl_faxNoTextBox.Text = string.Empty;
                    ClientControl_FEIN_NO_TextBox.Text = string.Empty;
                    ClientControl_phoneNoTextBox.Text = string.Empty;
                    ClientControl_websiteURL_TextBox.Text = string.Empty;
                    return;
                }
                ClientControl_additionalInfoTextBox.Text = value.AdditionalInfo;
                ClientControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                ClientControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                ClientControl_cityTextBox.Text = value.ContactInformation.City;
                ClientControl_countryTextBox.Text = value.ContactInformation.Country;
                ClientControl_stateTextBox.Text = value.ContactInformation.State;
                ClientControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                ClientControl_clientNameTextBox.Text = value.ClientName;
                ClientControl_faxNoTextBox.Text = value.FaxNumber;
                ClientControl_FEIN_NO_TextBox.Text = value.FeinNo;
                ClientControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                ClientControl_websiteURL_TextBox.Text = value.ContactInformation.WebSiteAddress.Personal;
            }
            get
            {
                ClientInformation client = new ClientInformation();
                client.AdditionalInfo = ClientControl_additionalInfoTextBox.Text.Trim();
                ContactInformation contInfo = new ContactInformation();

                contInfo.StreetAddress = ClientControl_streetAddressTextBox.Text.Trim();
                contInfo.EmailAddress = ClientControl_eMailIDTextBox.Text.Trim();
                contInfo.City = ClientControl_cityTextBox.Text.Trim();
                contInfo.Country = ClientControl_countryTextBox.Text.Trim();
                contInfo.State = ClientControl_stateTextBox.Text.Trim();
                contInfo.PostalCode = ClientControl_zipCodeTextBox.Text.Trim();

                client.ClientID = 0;
                client.ClientName = ClientControl_clientNameTextBox.Text.Trim();

                client.FaxNumber = ClientControl_faxNoTextBox.Text.Trim();
                client.FeinNo = ClientControl_FEIN_NO_TextBox.Text.Trim();
                client.IsDeleted = false;
                PhoneNumber phone = new PhoneNumber();
                phone.Mobile = ClientControl_phoneNoTextBox.Text.Trim();
                contInfo.Phone = phone;

                WebSite website = new WebSite();
                website.Personal = ClientControl_websiteURL_TextBox.Text.Trim();
                contInfo.WebSiteAddress = website;
                client.ContactInformation = contInfo;
                return client;
            }
        }

        /// <summary>
        /// Method that gets and sets the client contact information
        /// </summary>
        public ClientContactInformation ContactDataSource
        {
            set
            {
                if (value == null)
                {
                    ContactControl_firstNameTextBox.Text = string.Empty;
                    ContactControl_middleNameTextBox.Text = string.Empty;
                    ContactControl_lastNameTextBox.Text = string.Empty;
                    ContactControl_additionalInfoTextBox.Text = string.Empty;
                    ContactControl_streetAddressTextBox.Text = string.Empty;
                    ContactControl_eMailIDTextBox.Text = string.Empty;
                    ContactControl_cityTextBox.Text = string.Empty;
                    ContactControl_countryTextBox.Text = string.Empty;
                    ContactControl_stateTextBox.Text = string.Empty;
                    ContactControl_zipCodeTextBox.Text = string.Empty;
                    ContactControl_faxNoTextBox.Text = string.Empty;
                    ContactControl_phoneNoTextBox.Text = string.Empty;
                    ContactControl_primaryAddressCheckbox.Visible = true;
                    ContactControl_clientNameTR.Visible = true;
                    return;
                }
                ContactControl_firstNameTextBox.Text = value.FirstName;
                ContactControl_middleNameTextBox.Text = value.MiddleName;
                ContactControl_lastNameTextBox.Text = value.LastName;
                ContactControl_additionalInfoTextBox.Text = value.AdditionalInfo;
                ContactControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                ContactControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                ContactControl_cityTextBox.Text = value.ContactInformation.City;
                ContactControl_countryTextBox.Text = value.ContactInformation.Country;
                ContactControl_stateTextBox.Text = value.ContactInformation.State;
                ContactControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                ContactControl_faxNoTextBox.Text = value.FaxNo;
                ContactControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
            }
            get
            {
                ClientContactInformation clientContactInformation = new ClientContactInformation();
                clientContactInformation.FirstName = ContactControl_firstNameTextBox.Text.Trim();
                clientContactInformation.MiddleName = ContactControl_middleNameTextBox.Text.Trim();
                clientContactInformation.LastName = ContactControl_lastNameTextBox.Text.Trim();
                clientContactInformation.AdditionalInfo = ContactControl_additionalInfoTextBox.Text.Trim();
                ContactInformation contInfo = new ContactInformation();

                contInfo.StreetAddress = ContactControl_streetAddressTextBox.Text.Trim();
                contInfo.EmailAddress = ContactControl_eMailIDTextBox.Text.Trim();
                contInfo.City = ContactControl_cityTextBox.Text.Trim();
                contInfo.Country = ContactControl_countryTextBox.Text.Trim();
                contInfo.State = ContactControl_stateTextBox.Text.Trim();
                contInfo.PostalCode = ContactControl_zipCodeTextBox.Text.Trim();
                clientContactInformation.FaxNo = ContactControl_faxNoTextBox.Text.Trim();
                PhoneNumber phone = new PhoneNumber();
                phone.Mobile = ContactControl_phoneNoTextBox.Text.Trim();
                contInfo.Phone = phone;
                clientContactInformation.ContactInformation = contInfo;

                if (ContactControl_primaryAddressCheckbox.Checked)
                {
                    clientContactInformation.IsContactRequired = true;
                }
                
                return clientContactInformation;
            }
        }

        /// <summary>
        /// Method that gets and sets the client departments information
        /// </summary>
        public Department DepartmentDataSource
        {
            set
            {
                if (value == null)
                {
                    DepartmentControl_descriptionTextBox.Text = string.Empty;
                    DepartmentControl_streetAddressTextBox.Text = string.Empty;
                    DepartmentControl_eMailIDTextBox.Text = string.Empty;
                    DepartmentControl_cityTextBox.Text = string.Empty;
                    DepartmentControl_countryTextBox.Text = string.Empty;
                    DepartmentControl_stateTextBox.Text = string.Empty;
                    DepartmentControl_zipCodeTextBox.Text = string.Empty;
                    DepartmentControl_departmentNameTextBox.Text = string.Empty;
                    DepartmentControl_faxNoTextBox.Text = string.Empty;
                    DepartmentControl_phoneNoTextBox.Text = string.Empty;
                    DepartmentControl_additionalInfoTextBox.Text = string.Empty;
                    return;
                }

                DepartmentControl_descriptionTextBox.Text = value.DepartmentDescription;
                DepartmentControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                DepartmentControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                DepartmentControl_cityTextBox.Text = value.ContactInformation.City;
                DepartmentControl_countryTextBox.Text = value.ContactInformation.Country;
                DepartmentControl_stateTextBox.Text = value.ContactInformation.State;
                DepartmentControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                DepartmentControl_departmentNameTextBox.Text = value.DepartmentName;
                DepartmentControl_faxNoTextBox.Text = value.FaxNumber;
                DepartmentControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                DepartmentControl_additionalInfoTextBox.Text = value.AdditionalInfo;
            }
            get
            {
                Department department = new Department();
                department.DepartmentDescription = DepartmentControl_descriptionTextBox.Text.Trim();
                department.AdditionalInfo = DepartmentControl_additionalInfoTextBox.Text.Trim();
                ContactInformation contInfo = new ContactInformation();

                contInfo.StreetAddress = DepartmentControl_streetAddressTextBox.Text.Trim();
                contInfo.EmailAddress = DepartmentControl_eMailIDTextBox.Text.Trim();
                contInfo.City = DepartmentControl_cityTextBox.Text.Trim();
                contInfo.Country = DepartmentControl_countryTextBox.Text.Trim();
                contInfo.State = DepartmentControl_stateTextBox.Text.Trim();
                contInfo.PostalCode = DepartmentControl_zipCodeTextBox.Text.Trim();
                department.DepartmentName = DepartmentControl_departmentNameTextBox.Text.Trim();
                department.FaxNumber = DepartmentControl_faxNoTextBox.Text.Trim();

                department.IsDeleted = false;
                PhoneNumber phone = new PhoneNumber();
                phone.Mobile = DepartmentControl_phoneNoTextBox.Text.Trim();
                contInfo.Phone = phone;

                department.ContactInformation = contInfo;

                if (DepartmentControl_primaryAddressCheckbox.Checked)
                {
                    department.IsDepartmentRequired = true;
                }

                return department;
            }
        }

        
        #endregion Properties

        #region Private Methods                                                

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        private bool IsValid()
        {
            // Check if feature usage limit exceeds for creating client.
            if (new AdminBLManager().IsFeatureUsageLimitExceeds(this.TenantID,
                Constants.FeatureConstants.CREATE_CLIENT))
            {
                ShowErrorMessage("You have reached the maximum limit based your subscription plan. Cannot create more clients");
                InvokePageBaseMethod("ShowClientControlModalPopUp", null);
                return false;
            }

            StringBuilder requiredFieldMessage = new StringBuilder();
            string validEmail = string.Empty;
            string validWebSite = string.Empty;
            bool isvalid = true;

            #region Validate client information if it is required

            ClientInformation clientInformation = new ClientInformation();
            clientInformation = ClientDataSource;
            NomenclatureCustomize clientAttributes = new NomenclatureCustomize();
            if (Utility.IsNullOrEmpty(ViewState[CLIENT_ATTRIBUTES_DATASOUCE]))
                return isvalid;
            clientAttributes = (NomenclatureCustomize)ViewState[CLIENT_ATTRIBUTES_DATASOUCE];

            if (clientAttributes.ClientNameIsVisible && clientAttributes.ClientNameFieldIsMandatory
              && clientInformation.ClientName.Length == 0)
            {
                requiredFieldMessage.Append(clientAttributes.ClientName);
                isvalid = false;
            }

            //if (clientAttributes.StreetAddressIsVisible && clientAttributes.StreetAddressIsMandatory
            //    && clientInformation.ContactInformation.StreetAddress.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.StreetAddress);
            //    isvalid = false;
            //}
            //if (clientAttributes.CityIsVisible && clientAttributes.CityIsMandatory
            //    && clientInformation.ContactInformation.City.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.City);
            //    isvalid = false;
            //}

            //if (clientAttributes.StateIsVisible && clientAttributes.StateIsMandatory
            //    && clientInformation.ContactInformation.State.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.state);
            //    isvalid = false;
            //}
            //if (clientAttributes.ZipCodeIsVisible && clientAttributes.ZipCodeIsMandatory
            //    && clientInformation.ContactInformation.PostalCode.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.Zipcode);
            //    isvalid = false;
            //}
            //if (clientAttributes.CountryIsVisible && clientAttributes.CountryIsMandatory
            //    && clientInformation.ContactInformation.Country.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.Country);
            //    isvalid = false;
            //}
            //if (clientAttributes.FEINNOIsVisible && clientAttributes.FEINNOIsMandatory
            //    && clientInformation.FeinNo.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.FEINNO);
            //    isvalid = false;
            //}
            if (clientAttributes.PhoneNumberIsVisible && clientAttributes.PhoneNumberFieldIsMandatory
                && clientInformation.ContactInformation.Phone.Mobile.Length == 0)
            {
                if (requiredFieldMessage.Length > 0)
                    requiredFieldMessage.Append(", ");
                requiredFieldMessage.Append(clientAttributes.PhoneNumber);
                isvalid = false;
            }
            //if (clientAttributes.FaxNoIsVisible && clientAttributes.FaxNoIsMandatory
            //    && clientInformation.FaxNumber.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.FaxNo);
            //    isvalid = false;
            //}
            if (clientAttributes.EmailIDIsVisible && clientAttributes.EmailIDFieldIsMandatory)
            {
                if (clientInformation.ContactInformation.EmailAddress.Length == 0)
                {
                    if (requiredFieldMessage.Length > 0)
                        requiredFieldMessage.Append(", ");
                    requiredFieldMessage.Append(clientAttributes.EmailID);
                    isvalid = false;
                }
                else if (!Utility.IsValidEmailAddress(clientInformation.ContactInformation.EmailAddress))
                {
                    validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                    isvalid = false;
                }

            }
            else if (clientInformation.ContactInformation.EmailAddress.Length > 0 && 
                !Utility.IsValidEmailAddress(clientInformation.ContactInformation.EmailAddress))
            {
                validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                isvalid = false;
            }


            //if (clientAttributes.WebsiteURLIsVisible && clientAttributes.WebsiteURLFieldIsMandatory)
            //{
            //    if (clientInformation.ContactInformation.WebSiteAddress.Personal.Length == 0)
            //    {
            //        if (requiredFieldMessage.Length > 0)
            //            requiredFieldMessage.Append(", ");
            //        requiredFieldMessage.Append(clientAttributes.WebsiteURL);
            //        isvalid = false;
            //    }
            //    else if (!Utility.IsValidWebsiteURL(clientInformation.ContactInformation.WebSiteAddress.Personal))
            //    {
            //        validWebSite = "Enter valid website address";
            //        isvalid = false;
            //    }

            //}
            //else if (clientInformation.ContactInformation.WebSiteAddress.Personal.Length > 0 && 
            //    !Utility.IsValidWebsiteURL(clientInformation.ContactInformation.WebSiteAddress.Personal))
            //{
            //    validWebSite = "Enter valid website address";
            //    isvalid = false;
            //}
            //if (clientAttributes.AdditionalInfoIsVisible && clientAttributes.AdditionalFieldInfoIsMandatory
            //    && clientInformation.AdditionalInfo.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.AdditionalInfo);
            //    isvalid = false;
            //}

            #endregion Validate client information if it is required

            #region Validate client department information if it is required

            if (AddClientControl_isDepartmentRequiredCheckBox.Checked)
            {
                Department department = new Department();
                department = DepartmentDataSource;

                NomenclatureCustomize departmentAttributes = new NomenclatureCustomize();
                
                departmentAttributes = (NomenclatureCustomize)Session[DEPARTMENT_ATTRIBUTES_DATASOURCE];
                if (department.DepartmentName.Length == 0)
                {
                    requiredFieldMessage.Append(departmentAttributes.DepartmentName);
                    isvalid = false;
                }

                //if (departmentAttributes.StreetAddressIsVisible && departmentAttributes.StreetAddressIsMandatory
                //    && department.ContactInformation.StreetAddress.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.StreetAddress);
                //    isvalid = false;
                //}
                //if (departmentAttributes.CityIsVisible && departmentAttributes.CityIsMandatory
                //    && department.ContactInformation.City.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.City);
                //    isvalid = false;
                //}

                //if (departmentAttributes.StateIsVisible && departmentAttributes.StateIsMandatory
                //    && department.ContactInformation.State.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.state);
                //    isvalid = false;
                //}
                //if (departmentAttributes.ZipCodeIsVisible && departmentAttributes.ZipCodeIsMandatory
                //    && department.ContactInformation.PostalCode.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.Zipcode);
                //    isvalid = false;
                //}
                //if (departmentAttributes.CountryIsVisible && departmentAttributes.CountryIsMandatory
                //    && department.ContactInformation.Country.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.Country);
                //    isvalid = false;
                //}

                if (departmentAttributes.PhoneNumberIsVisible && departmentAttributes.PhoneNumberFieldIsMandatory
                    && department.ContactInformation.Phone.Mobile.Length == 0)
                {
                    if (requiredFieldMessage.Length > 0)
                        requiredFieldMessage.Append(", ");
                    requiredFieldMessage.Append(departmentAttributes.PhoneNumber);
                    isvalid = false;
                }
                //if (departmentAttributes.FaxNoIsVisible && departmentAttributes.FaxNoIsMandatory
                //    && department.FaxNumber.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.FaxNo);
                //    isvalid = false;
                //}
                if (departmentAttributes.EmailIDIsVisible && departmentAttributes.EmailIDFieldIsMandatory)
                {
                    if (department.ContactInformation.EmailAddress.Length == 0)
                    {
                        if (requiredFieldMessage.Length > 0)
                            requiredFieldMessage.Append(", ");
                        requiredFieldMessage.Append(departmentAttributes.EmailID);
                        isvalid = false;
                    }
                    else if (!Utility.IsValidEmailAddress(department.ContactInformation.EmailAddress))
                    {
                        validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                        isvalid = false;
                    }
                }
                else if (department.ContactInformation.EmailAddress.Length > 0 && 
                    !Utility.IsValidEmailAddress(department.ContactInformation.EmailAddress))
                {
                    validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                    isvalid = false;
                }


                //if (departmentAttributes.DescriptionIsVisible && departmentAttributes.DescriptionIsMandatory
                //    && department.DepartmentDescription.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.Description);
                //    isvalid = false;
                //}

                //if (departmentAttributes.AdditionalInfoIsVisible && departmentAttributes.AdditionalFieldInfoIsMandatory 
                //    && department.AdditionalInfo.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(departmentAttributes.AdditionalInfo);
                //    isvalid = false;
                //}
            }

            #endregion Validate client department information if it is required

            #region Validate client contact information if it is required

            if (AddClientControl_isContactRequiredCheckBox.Checked)
            {
                ClientContactInformation clientContactInformation = new ClientContactInformation();
                clientContactInformation = ContactDataSource;
                NomenclatureCustomize clinetContactAttributes = new NomenclatureCustomize();

                clinetContactAttributes = (NomenclatureCustomize)Session[CLIENT_CONTACT_ATTRIBUTES_DATASOURCE];

                if (clientContactInformation.FirstName.Length == 0)
                {
                    requiredFieldMessage.Append(clinetContactAttributes.FirstName);
                    isvalid = false;
                }
                //if (clientContactInformation.LastName.Length == 0)
                //{
                //    requiredFieldMessage.Append(clinetContactAttributes.LastName);
                //    isvalid = false;
                //}

                //if (clinetContactAttributes.StreetAddressIsVisible && clinetContactAttributes.StreetAddressIsMandatory
                //    && clientContactInformation.ContactInformation.StreetAddress.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(clinetContactAttributes.StreetAddress);
                //    isvalid = false;
                //}
                //if (clinetContactAttributes.CityIsVisible && clinetContactAttributes.CityIsMandatory
                //    && clientContactInformation.ContactInformation.City.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(clinetContactAttributes.City);
                //    isvalid = false;
                //}

                //if (clinetContactAttributes.StateIsVisible && clinetContactAttributes.StateIsMandatory
                //    && clientContactInformation.ContactInformation.State.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(clinetContactAttributes.state);
                //    isvalid = false;
                //}
                //if (clinetContactAttributes.ZipCodeIsVisible && clinetContactAttributes.ZipCodeIsMandatory
                //    && clientContactInformation.ContactInformation.PostalCode.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(clinetContactAttributes.Zipcode);
                //    isvalid = false;
                //}
                //if (clinetContactAttributes.CountryIsVisible && clinetContactAttributes.CountryIsMandatory
                //    && clientContactInformation.ContactInformation.Country.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(clinetContactAttributes.Country);
                //    isvalid = false;
                //}

                if (clinetContactAttributes.PhoneNumberIsVisible && clinetContactAttributes.PhoneNumberFieldIsMandatory
                    && clientContactInformation.ContactInformation.Phone.Mobile.Length == 0)
                {
                    if (requiredFieldMessage.Length > 0)
                        requiredFieldMessage.Append(", ");
                    requiredFieldMessage.Append(clinetContactAttributes.PhoneNumber);
                    isvalid = false;
                }

                if (clinetContactAttributes.EmailIDIsVisible && clinetContactAttributes.EmailIDFieldIsMandatory)
                {
                    if (clientContactInformation.ContactInformation.EmailAddress.Length == 0)
                    {
                        if (requiredFieldMessage.Length > 0)
                            requiredFieldMessage.Append(", ");
                        requiredFieldMessage.Append(clinetContactAttributes.EmailID);
                        isvalid = false;
                    }
                    else if (!Utility.IsValidEmailAddress(clientContactInformation.ContactInformation.EmailAddress))
                    {
                        validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                        isvalid = false;
                    }
                }
                else if (clientContactInformation.ContactInformation.EmailAddress.Length > 0 && 
                    !Utility.IsValidEmailAddress(clientContactInformation.ContactInformation.EmailAddress))
                {
                    validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                    isvalid = false;
                }

                //if (clinetContactAttributes.AdditionalInfoIsVisible && clinetContactAttributes.AdditionalInfoIsMandatory
                //    && clientContactInformation.AdditionalInfo.Length == 0)
                //{
                //    if (requiredFieldMessage.Length > 0)
                //        requiredFieldMessage.Append(", ");
                //    requiredFieldMessage.Append(clinetContactAttributes.AdditionalInfo);
                //    isvalid = false;
                //}
            }

            #endregion Validate client contact information if it is required

            if (!isvalid)
            {
                ShowErrorMessage("Mandatory fields cannot be empty");
                if (validEmail.Length > 0)
                    ShowErrorMessage(validEmail);
                if (validWebSite.Length > 0)
                    ShowErrorMessage(validWebSite);
                InvokePageBaseMethod("ShowClientControlModalPopUp", null);
            }
            return isvalid;
        }

        /// <summary>
        /// Method to show the error message
        /// </summary>
        /// <param name="exp">exp</param>
        private void ShowErrorMessage(string exp)
        {
            if (string.IsNullOrEmpty(AddClientControl_topErrorMessageLabel.Text))
                AddClientControl_topErrorMessageLabel.Text = exp;
            else
                AddClientControl_topErrorMessageLabel.Text += "<br />" + exp;
        }

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <param name="args">The args.</param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod,
                null, this.Page, args);
        }

        /// <summary>
        /// Method that assigns primary information to department controls
        /// </summary>
        private void AssignDepartmentPrimaryAddress()
        {
            InvokePageBaseMethod("ShowClientControlModalPopUp", null);
            ClientInformation clientInformation = new ClientInformation();
            clientInformation = ClientDataSource;

            if (DepartmentControl_primaryAddressCheckbox.Checked)
            {
                if (Utility.IsNullOrEmpty(clientInformation))
                    return;
                DepartmentControl_streetAddressTextBox.Text = clientInformation.ContactInformation.StreetAddress;
                DepartmentControl_cityTextBox.Text = clientInformation.ContactInformation.City;
                DepartmentControl_countryTextBox.Text = clientInformation.ContactInformation.Country;
                DepartmentControl_stateTextBox.Text = clientInformation.ContactInformation.State;
                DepartmentControl_zipCodeTextBox.Text = clientInformation.ContactInformation.PostalCode;
            }
            else
            {
                DepartmentControl_streetAddressTextBox.Text = string.Empty;
                DepartmentControl_cityTextBox.Text = string.Empty;
                DepartmentControl_countryTextBox.Text = string.Empty;
                DepartmentControl_stateTextBox.Text = string.Empty;
                DepartmentControl_zipCodeTextBox.Text = string.Empty;
            }
        }

         /// <summary>
        /// Method that assigns primary information to contact controls
        /// </summary>
        private void AssignContactPrimaryAddress()
        {
            InvokePageBaseMethod("ShowClientControlModalPopUp", null);
            ClientInformation clientInformation = new ClientInformation();
            clientInformation = ClientDataSource;

            if (ContactControl_primaryAddressCheckbox.Checked)
            {
                ContactControl_streetAddressTextBox.Text = clientInformation.ContactInformation.StreetAddress;
                ContactControl_cityTextBox.Text = clientInformation.ContactInformation.City;
                ContactControl_countryTextBox.Text = clientInformation.ContactInformation.Country;
                ContactControl_stateTextBox.Text = clientInformation.ContactInformation.State;
                ContactControl_zipCodeTextBox.Text = clientInformation.ContactInformation.PostalCode;
            }
            else
            {
                ContactControl_streetAddressTextBox.Text = string.Empty;
                ContactControl_cityTextBox.Text = string.Empty;
                ContactControl_countryTextBox.Text = string.Empty;
                ContactControl_stateTextBox.Text = string.Empty;
                ContactControl_zipCodeTextBox.Text = string.Empty;
            }
        }

        #endregion Private Methods                                                        

        #region Public Methods                                                 

        /// <summary>
        /// Method that sets the client attribute data source
        /// </summary>
        public NomenclatureCustomize ClientAttributesDataSource
        {
            set
            {
                if (value == null)
                    return;
                ViewState[CLIENT_ATTRIBUTES_DATASOUCE] = value;

                int i = 1;
                //for (int i = 1; i < 20; i++)
                //{
                HtmlTableCell TableCell = null;
                if (value.ClientNameIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    ClientControl_clientNameLabel.Visible = true;
                    ClientControl_clientNameLabel.Text = value.ClientName;
                    TableCell.Controls.Add(ClientControl_clientNameLabel);
                    if (value.ClientNameFieldIsMandatory)
                    {
                        ClientControl_clientNameSpan.Visible = true;
                        TableCell.Controls.Add(ClientControl_clientNameSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    ClientControl_clientNameTextBox.Visible = true;
                    TableCell.Controls.Add(ClientControl_clientNameTextBox);
                    i++;
                }

                if (value.StreetAddressIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    ClientControl_streetAddressLabel.Visible = true;
                    ClientControl_streetAddressLabel.Text = value.StreetAddress;
                    TableCell.Controls.Add(ClientControl_streetAddressLabel);
                    //if (value.StreetAddressIsMandatory)
                    //{
                    //    ClientControl_streetAddressSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_streetAddressSpan);
                    //}
                    i++;
                    ClientControl_streetAddressTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_streetAddressTextBox);
                    i++;
                }
                if (value.CityIsVisible)
                {
                    ClientControl_cityLabel.Visible = true;
                    ClientControl_cityLabel.Text = value.City;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_cityLabel);
                    //if (value.CityIsMandatory)
                    //{
                    //    ClientControl_citySpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_citySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_cityTextBox);
                    i++;
                    ClientControl_cityTextBox.Visible = true;
                }
                if (value.StateIsVisible)
                {
                    ClientControl_stateLabel.Visible = true;
                    ClientControl_stateLabel.Text = value.state;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_stateLabel);
                    //if (value.StateIsMandatory)
                    //{
                    //    ClientControl_stateSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_stateSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_stateTextBox);
                    i++;
                    ClientControl_stateTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    ClientControl_countryLabel.Text = value.Country;
                    ClientControl_countryLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_countryLabel);
                    //if (value.CountryIsMandatory)
                    //{
                    //    ClientControl_countrySpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_countrySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_countryTextBox);
                    i++;
                    ClientControl_countryTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    ClientControl_zipCodeLabel.Text = value.Zipcode;
                    ClientControl_zipCodeLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_zipCodeLabel);

                    //if (value.ZipCodeIsMandatory)
                    //{
                    //    ClientControl_zipCodeSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_zipCodeSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_zipCodeTextBox);
                    i++;
                    ClientControl_zipCodeTextBox.Visible = true;
                }

                if (value.FEINNOIsVisible)
                {
                    ClientControl_FEIN_NOLabel.Text = value.FEINNO;
                    ClientControl_FEIN_NOLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_FEIN_NOLabel);

                    //if (value.FEINNOIsMandatory)
                    //{
                    //    ClientControl_FEIN_NOSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_FEIN_NOSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_FEIN_NO_TextBox);
                    i++;
                    ClientControl_FEIN_NO_TextBox.Visible = true;
                }

                if (value.FaxNoIsVisible)
                {
                    ClientControl_faxNoLabel.Text = value.FaxNo;
                    ClientControl_faxNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_faxNoLabel);

                    //if (value.FaxNoIsMandatory)
                    //{
                    //    ClientControl_faxNoSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_faxNoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_faxNoTextBox);
                    i++;
                    ClientControl_faxNoTextBox.Visible = true;
                }

                if (value.EmailIDIsVisible)
                {
                    ClientControl_eMailIDLabel.Text = value.EmailID;
                    ClientControl_eMailIDLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_eMailIDLabel);

                    if (value.EmailIDFieldIsMandatory)
                    {
                        ClientControl_eMailIDSpan.Visible = true;
                        TableCell.Controls.Add(ClientControl_eMailIDSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_eMailIDTextBox);
                    i++;
                    ClientControl_eMailIDTextBox.Visible = true;
                }
                if (value.PhoneNumberIsVisible)
                {
                    ClientControl_phoneNoLabel.Text = value.PhoneNumber;
                    ClientControl_phoneNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_phoneNoLabel);

                    if (value.PhoneNumberFieldIsMandatory)
                    {
                        ClientControl_phoneNoSpan.Visible = true;
                        TableCell.Controls.Add(ClientControl_phoneNoSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_phoneNoTextBox);
                    i++;
                    ClientControl_phoneNoTextBox.Visible = true;
                }
                if (value.WebsiteURLIsVisible)
                {
                    ClientControl_websiteURLLabel.Text = value.WebsiteURL;
                    ClientControl_websiteURLLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_websiteURLLabel);

                    //if (value.WebsiteURLFieldIsMandatory)
                    //{
                    //    ClientControl_websiteURLSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_websiteURLSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_websiteURL_TextBox);
                    i++;
                    ClientControl_websiteURL_TextBox.Visible = true;
                }
                if (value.AdditionalInfoIsVisible)
                {
                    ClientControl_additionalInfoLabel.Text = value.AdditionalInfo;
                    ClientControl_additionalInfoLabel.Visible = true;
                    i = 25;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_additionalInfoLabel);

                    //if (value.AdditionalFieldInfoIsMandatory)
                    //{
                    //    ClientControl_additionalInfoSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_additionalInfoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_additionalInfoTextBox);
                    ClientControl_additionalInfoTextBox.Visible = true;
                }
            }
        }

        /// <summary>
        /// Method that sets the department attribute data source
        /// </summary>
        public NomenclatureCustomize DepartmenAttributesDataSource
        {
            set
            {
                if (value == null)
                {
                    Session[DEPARTMENT_ATTRIBUTES_DATASOURCE] = null;
                    return;
                }
                Session[DEPARTMENT_ATTRIBUTES_DATASOURCE] = value;
                ////DepartmentControl_ClientName.Text = value.ClientName;
                HtmlTableCell TableCell = null;

                DepartmentControl_primaryAddressCheckbox.Visible = value.DepartmentPrimaryAddress;
                if (value.DescriptionIsVisible)
                {
                    DepartmentControl_descriptionLabel.Text = value.Description;
                    DepartmentControl_descriptionLabel.Visible = true;
                    DepartmentControl_descriptionTextBox.Visible = true;
                    if (value.DescriptionIsMandatory)
                    {
                        //DepartmentControl_descriptionSpan.Visible = true;
                    }

                }
                if (value.AdditionalInfoIsVisible)
                {
                    DepartmentControl_additionalInfoLabel.Text = value.AdditionalInfo;
                    DepartmentControl_additionalInfoLabel.Visible = true;
                    DepartmentControl_additionalInfoTextBox.Visible = true;
                    //DepartmentControl_additionalInfoSpan.Visible = value.AdditionalFieldInfoIsMandatory;
                }
                int i = 5;
                if (value.StreetAddressIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    DepartmentControl_streetAddressLabel.Visible = true;
                    DepartmentControl_streetAddressLabel.Text = value.StreetAddress;
                    TableCell.Controls.Add(DepartmentControl_streetAddressLabel);
                    //if (value.StreetAddressIsMandatory)
                    //{
                    //    DepartmentControl_streetAddressSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_streetAddressSpan);
                    //}
                    i++;
                    DepartmentControl_streetAddressTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_streetAddressTextBox);
                    i++;
                }
                if (value.CityIsVisible)
                {
                    DepartmentControl_cityLabel.Visible = true;
                    DepartmentControl_cityLabel.Text = value.City;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_cityLabel);
                    //if (value.CityIsMandatory)
                    //{
                    //    DepartmentControl_citySpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_citySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_cityTextBox);
                    i++;
                    DepartmentControl_cityTextBox.Visible = true;
                }
                if (value.StateIsVisible)
                {
                    DepartmentControl_stateLabel.Visible = true;
                    DepartmentControl_stateLabel.Text = value.state;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_stateLabel);
                    //if (value.StateIsMandatory)
                    //{
                    //    DepartmentControl_stateSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_stateSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_stateTextBox);
                    i++;
                    DepartmentControl_stateTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    DepartmentControl_countryLabel.Text = value.Country;
                    DepartmentControl_countryLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_countryLabel);
                    //if (value.CountryIsMandatory)
                    //{
                    //    DepartmentControl_countrySpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_countrySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_countryTextBox);
                    i++;
                    DepartmentControl_countryTextBox.Visible = true;
                }
                if (value.ZipCodeIsVisible)
                {
                    DepartmentControl_zipCodeLabel.Text = value.Zipcode;
                    DepartmentControl_zipCodeLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_zipCodeLabel);

                    //if (value.ZipCodeIsMandatory)
                    //{
                    //    DepartmentControl_zipCodeSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_zipCodeSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_zipCodeTextBox);
                    i++;
                    DepartmentControl_zipCodeTextBox.Visible = true;
                }

                if (value.FaxNoIsVisible)
                {
                    DepartmentControl_faxNoLabel.Text = value.FaxNo;
                    DepartmentControl_faxNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_faxNoLabel);

                    //if (value.FaxNoIsMandatory)
                    //{
                    //    DepartmentControl_faxNoSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_faxNoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_faxNoTextBox);
                    i++;
                    DepartmentControl_faxNoTextBox.Visible = true;
                }

                if (value.EmailIDIsVisible)
                {
                    DepartmentControl_eMailIDLabel.Text = value.EmailID;
                    DepartmentControl_eMailIDLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_eMailIDLabel);

                    if (value.EmailIDFieldIsMandatory)
                    {
                        DepartmentControl_eMailIDSpan.Visible = true;
                        TableCell.Controls.Add(DepartmentControl_eMailIDSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_eMailIDTextBox);
                    i++;
                    DepartmentControl_eMailIDTextBox.Visible = true;
                }
                if (value.PhoneNumberIsVisible)
                {
                    DepartmentControl_phoneNoLabel.Text = value.PhoneNumber;
                    DepartmentControl_phoneNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_phoneNoLabel);

                    if (value.PhoneNumberFieldIsMandatory)
                    {
                        DepartmentControl_phoneNoSpan.Visible = true;
                        TableCell.Controls.Add(DepartmentControl_phoneNoSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_phoneNoTextBox);
                    i++;
                    DepartmentControl_phoneNoTextBox.Visible = true;
                }

            }
        }

        /// <summary>
        /// Method that sets the contact attribute data source
        /// </summary>
        public NomenclatureCustomize ContactAttributesDataSource
        {
            set
            {
                if (value == null)
                {
                    Session[CLIENT_CONTACT_ATTRIBUTES_DATASOURCE] = null;
                    return;
                }

                Session[CLIENT_CONTACT_ATTRIBUTES_DATASOURCE] = value;
                ContactControl_firstNameLabel.Text = value.FirstName;

                ContactControl_clientNameLabel.Text = value.ClientName;
                HtmlTableCell TableCell = null;

                ContactControl_primaryAddressCheckbox.Visible = value.ContactsPrimaryAddress;
                
                if (value.AdditionalInfoIsVisible)
                {
                    ContactControl_additionalInfoLabel.Text = value.AdditionalInfo;
                    ContactControl_additionalInfoLabel.Visible = true;
                    ContactControl_additionalInfoTextBox.Visible = true;
                   // ContactControl_additionalInfoSpan.Visible = value.AdditionalInfoIsMandatory;
                }
                int i = 5;
                if (value.MiddlenameIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    ContactControl_middleNameLabel.Visible = true;
                    ContactControl_middleNameLabel.Text = value.Middlename;
                    TableCell.Controls.Add(ContactControl_middleNameLabel);
                    //if (value.MiddlenameIsMandatory)
                    //{
                    //    ContactControl_middleNameSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_middleNameSpan);
                    //}
                    i++;
                    ContactControl_middleNameTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_middleNameTextBox);
                    i++;
                }
                if (value.LastNameIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    ContactControl_lastNameLabel.Visible = true;
                    ContactControl_lastNameLabel.Text = value.LastName;
                    TableCell.Controls.Add(ContactControl_lastNameLabel);
                    //if (value.LastNameIsMandatory)
                    //{
                    //    ContactControl_lastNameSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_lastNameSpan);
                    //}
                    i++;
                    ContactControl_lastNameTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_lastNameTextBox);
                    i++;
                }
                if (value.StreetAddressIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    ContactControl_streetAddressLabel.Visible = true;
                    ContactControl_streetAddressLabel.Text = value.StreetAddress;
                    TableCell.Controls.Add(ContactControl_streetAddressLabel);
                    //if (value.StreetAddressIsMandatory)
                    //{
                    //    ContactControl_streetAddressSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_streetAddressSpan);
                    //}
                    i++;
                    ContactControl_streetAddressTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_streetAddressTextBox);
                    i++;
                }
                if (value.CityIsVisible)
                {
                    ContactControl_cityLabel.Visible = true;
                    ContactControl_cityLabel.Text = value.City;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_cityLabel);
                    //if (value.CityIsMandatory)
                    //{
                    //    ContactControl_citySpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_citySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_cityTextBox);
                    i++;
                    ContactControl_cityTextBox.Visible = true;
                }
                if (value.StateIsVisible)
                {
                    ContactControl_stateLabel.Visible = true;
                    ContactControl_stateLabel.Text = value.state;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_stateLabel);
                    //if (value.StateIsMandatory)
                    //{
                    //    ContactControl_stateSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_stateSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_stateTextBox);
                    i++;
                    ContactControl_stateTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    ContactControl_countryLabel.Text = value.Country;
                    ContactControl_countryLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_countryLabel);
                    //if (value.CountryIsMandatory)
                    //{
                    //    ContactControl_countrySpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_countrySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_countryTextBox);
                    i++;
                    ContactControl_countryTextBox.Visible = true;
                }
                if (value.ZipCodeIsVisible)
                {
                    ContactControl_zipCodeLabel.Text = value.Zipcode;
                    ContactControl_zipCodeLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_zipCodeLabel);

                    //if (value.ZipCodeIsMandatory)
                    //{
                    //    ContactControl_zipCodeSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_zipCodeSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_zipCodeTextBox);
                    i++;
                    ContactControl_zipCodeTextBox.Visible = true;
                }

                if (value.FaxNoIsVisible)
                {
                    ContactControl_faxNoLabel.Text = value.FaxNo;
                    ContactControl_faxNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_faxNoLabel);

                    //if (value.FaxNoIsMandatory)
                    //{
                    //    ContactControl_faxNoSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_faxNoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_faxNoTextBox);
                    i++;
                    ContactControl_faxNoTextBox.Visible = true;
                }

                if (value.EmailIDIsVisible)
                {
                    ContactControl_eMailIDLabel.Text = value.EmailID;
                    ContactControl_eMailIDLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_eMailIDLabel);

                    if (value.EmailIDFieldIsMandatory)
                    {
                        ContactControl_eMailIDSpan.Visible = true;
                        TableCell.Controls.Add(ContactControl_eMailIDSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_eMailIDTextBox);
                    i++;
                    ContactControl_eMailIDTextBox.Visible = true;
                }
                if (value.PhoneNumberIsVisible)
                {
                    ContactControl_phoneNoLabel.Text = value.PhoneNumber;
                    ContactControl_phoneNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_phoneNoLabel);

                    if (value.PhoneNumberFieldIsMandatory)
                    {
                        ContactControl_phoneNoSpan.Visible = true;
                        TableCell.Controls.Add(ContactControl_phoneNoSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_phoneNoTextBox);
                    i++;
                    ContactControl_phoneNoTextBox.Visible = true;
                }

            }
        }

        #endregion Public Methods

    }
}