﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionPricingPreviewControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SubscriptionPricingPreviewControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="SubscriptionPricingPreviewControl_messageTitleLiteral" runat="server">Selected Subscription Pricing</asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="SubscriptionPricingPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <div id="Forte_SubscriptionPricingPreviowControl_featuresDiv" style="overflow: auto;
                                        height: 200px;">
                                        <asp:UpdatePanel ID="Forte_SubscriptionFeaturePricingSetup_gridViewupdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="Forte_SubscriptionPricingPreviowControl_featureTypeGridView" runat="server"
                                                    AutoGenerateColumns="false" SkinID="sknWrapHeaderGrid" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemStyle Width="60%" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="Forte_SubscriptionPricingPreviowControl_featureTypeNameLabel" runat="server"
                                                                    Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Pricing">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="Forte_SubscriptionPricingPreviowControl_featureValueLabel" runat="server"
                                                                    Text='<%# Eval("FeatureValue") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" align="left">
            <asp:Button ID="Forte_SubscriptionPricingPreviewControl_publishButton" runat="server"
                SkinID="sknButtonId" Text="Publish" OnClick="Forte_SubscriptionPricingPreviewControl_publishButton_Click" />
            &nbsp;
            <asp:LinkButton ID="Forte_SubscriptionPricingPreviewControl_CancelButton" runat="server"
                SkinID="sknPopupLinkButton" Text="Cancel" OnClick="Forte_SubscriptionPricingPreviewControl_CancelButton_Click" ></asp:LinkButton>
            <%--<asp:Button ID="Forte_AddFeatureType_featureCancelButton" runat="server" Text="Cancel"
                SkinID="sknButtonId" />--%>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
