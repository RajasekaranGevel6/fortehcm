﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PositionProfileWorkflowControl.ascx.cs" Inherits="Forte.HCM.UI.CommonControls.PositionProfileWorkflowControl" %>
<table cellpadding="0" cellspacing="0" border="0"  width="100%"  align="center" >
    <tr>
        <td id="MainTd" runat="server" align="center" style="background-repeat:no-repeat;height:44px;"> 
            <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td runat="server" id="tdBI" width="16%" align="center"  class="pp_workflow_text" title="Basic Info"> 
                        <a runat="server" id="PositionProfileWorkflowControl_biAnchor"  >
                        Basic Info 
                        </a>
                     </td> 
                    <td  width="23%" class="pp_workflow_text"  align="center"  runat="server" id="tdDI">
                        <a  runat="server" id="PositionProfileWorkflowControl_diAnchor" title="Detailed Info">
                        Detailed Info 
                        </a>
                     </td>
                    <td width="19%" align="center"  class="pp_workflow_text"  runat="server" id="tdWS" title="Workflow Selection">
                        <a  runat="server" id="PositionProfileWorkflowControl_wsAnchor">
                        Workflow Selection
                        </a>
                    </td>
                    <td align="center"  width="25%" class="pp_workflow_text"  runat="server" id="tdRA" title="Recruiter Assignment">
                        <a  runat="server" id="PositionProfileWorkflowControl_raAnchor" >Recruiter Assignment</a>
                     </td>
                    <td  align="center"  class="pp_workflow_text"  runat="server" id="tdRW" title="Review">
                        <a  runat="server" id="PositionProfileWorkflowControl_rwAnchor">Review</a>
                    </td>
                </tr>
            </table>
        </td> 
    </tr>
</table>