﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ReportHistogramChartControl.cs
// File that represents the user interface for the histogram chart control 
// for design reports

#endregion Header

#region Directives

using System;
using System.Web.UI;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using ReflectionComboItem;
using System.Collections.Generic;
using System.Text;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that represents the User interface and functionalities of the 
    /// ReportHistogramChartControl. This page helps to view the candidate's 
    /// score position among other candidates 
    /// </summary>
    public partial class ReportHistogramChartControl : UserControl, IWidgetControl
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                ReportHistogramChartControl_selectAbsoluteScoreRadioButton.Checked = WidgetMultiSelectControlPrint.SelectedProperties.TrimEnd(',') == "0" ? false : true;
                ReportHistogramChartControl_selectRelativeScoreRadioButton.Checked = WidgetMultiSelectControlPrint.SelectedProperties.TrimEnd(',') == "1" ? false : true;
                //WidgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[0]);
                LoadChart();
            }
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called when the radio button is clicked 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ReportHistogramChartControl_selectAbsoluteScore_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                //Load the chart with the obtained data 
                LoadChart();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// Represents the method that used to load the chart details
        /// </summary>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate report details from the session
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            //Method to load the test Scores histogram chart
            //Get the chart data from the database
            ReportHistogramData reportHistogramData = new ReportBLManager()
                .GetReportCandidateScoreDetails(candidateReportDetail);

            //If chart data is not null save the data in the session 
            if (reportHistogramData != null)
                Session["HistogramData"] = reportHistogramData;

            //Load the chart with corresponding data
            LoadChart();

        }

        /// <summary>
        /// Represents the method that used to load the chart data 
        /// </summary>
        /// <param name="reportHistogramData">
        /// A<see cref="ReportHistogramData"/>that holds the report histogram data
        /// </param>
        private void LoadChart()
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            ReportHistogramData reportHistogramData = null;
            if (Session["HistogramData"] != null)
            {
                reportHistogramData = Session["HistogramData"] as ReportHistogramData;
            }

            //Get the candidate report details from the session
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;



            //Initialize the histogram chart data 
            //Assign the chart details
            ReportHistogramData histogramData = new ReportHistogramData();

            histogramData.ChartLength = 190;

            histogramData.ChartWidth = 380;

            histogramData.IsDisplayLegend = false;

            histogramData.IsShowLabel = true;

            histogramData.SegmentInterval = 10;

            histogramData.IsDisplayChartTitle = false;

            histogramData.IsDisplayAxisTitle = true;

            histogramData.IsDisplayChartTitle = false;

            histogramData.ChartTitle = "Candidate's Score Position";

            histogramData.XAxisTitle = "Scores";

            histogramData.YAxisTitle = "No Of Candidates";

            histogramData.IsDisplayCandidateScore = true;

            histogramData.IsDisplayCandidateName = true;

            histogramData.CandidateName = reportHistogramData.CandidateName;

            List<DropDownItem> dropDownItemList = new List<DropDownItem>();

            //If the absolute score radio button is checked, load absolute score data 
            if (ReportHistogramChartControl_selectAbsoluteScoreRadioButton.Checked)
            {
                selectedPrintProperty.Append("1");
                histogramData.CandidateScore = reportHistogramData.CandidateScore;

                histogramData.ChartData = reportHistogramData.AbsoluteChartData;
                histogramData.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_HISTOGRAM_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID;
                //Assign the data to chart control and load it 
                ReportHistogramChartControl_histogramChart.LoadChartControl(histogramData);
                //ReportHistogramChartControl_histogramChart
                dropDownItemList.Add(new DropDownItem("Title", "Absolute Comparative Score"));
                dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.DESIGN_REPORT_HISTOGRAM_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID + ".png"));
            }
            //If the relative score radio button is checked, load relative score data 
            else
            {
                selectedPrintProperty.Append("0");
                histogramData.CandidateScore = reportHistogramData.CandidateRelativeScore;
                histogramData.ChartData = reportHistogramData.RelativeChartData;
                histogramData.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_HISTOGRAM_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID;
                ReportHistogramChartControl_histogramChart.LoadChartControl(histogramData);
                dropDownItemList.Add(new DropDownItem("Title", "Realtive Histogram Score"));
                dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.DESIGN_REPORT_HISTOGRAM_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID + ".png"));
            }
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }
        #endregion Private Methods

        #region IWidgetControl Members

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>        
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            //return new UpdatePanel[] { ReportHistogramChartControl_updatePanel };
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}