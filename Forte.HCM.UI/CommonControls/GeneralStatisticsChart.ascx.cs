﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// GeneralStatisticsChart.cs
// File that represents the user interface for 
//the general statistics chart control

#endregion Header

#region Directives
using System;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Generic;
using ReflectionComboItem;
#endregion Directives

namespace Forte.HCM.UI.CommonControls
{

    public partial class GeneralStatisticsChart : UserControl, IWidgetControl
    {
        string testkey = string.Empty;
        List<DropDownItem> dropDownItemList = null;

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
              //  WidgetMultiSelectControlPrint.SelectedProperties = "Categories";
                BindProperty(true);
                WidgetMultiSelectControl.Visible = false;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GeneralStatisticsChart_selectProperty(object sender, EventArgs e)
        {
            try
            {
                GeneralStatisticsChart_categoryChartControl.Visible = false;
                GeneralStatisticsChart_subjectChartControl.Visible = false;
                GeneralStatisticsChart_testAreaChartControl.Visible = false;
                GeneralStatisticsChart_complexityChartControl.Visible = false;
                CheckBoxList checkBoxList = (CheckBoxList)sender;
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        switch (checkBoxList.Items[i].Text)
                        {
                            case "Categories":
                                GeneralStatisticsChart_categoryChartControl.Visible = true;
                                break;
                            case "Subjects":
                                GeneralStatisticsChart_subjectChartControl.Visible = true;
                                break;
                            case "Test Areas":
                                GeneralStatisticsChart_testAreaChartControl.Visible = true;
                                break;
                            case "Complexities":
                                GeneralStatisticsChart_complexityChartControl.Visible = true;
                                break;
                        }
                    }
                }
                WidgetMultiSelectControl.WidgetTypePropertyAllSelected(false);
                WidgetMultiSelectControlPrint.SelectedProperties = 
                    WidgetMultiSelectControl.SelectedProperties;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void GeneralStatisticsChart_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                BindProperty(false);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void BindProperty(bool printFlag)
        {
            GeneralStatisticsChart_categoryChartControl.Visible = false;
            GeneralStatisticsChart_subjectChartControl.Visible = false;
            GeneralStatisticsChart_testAreaChartControl.Visible = false;
            GeneralStatisticsChart_complexityChartControl.Visible = false;
            string[] statisticsChartCaption =null;
            if(printFlag)
                statisticsChartCaption = WidgetMultiSelectControlPrint.SelectedProperties.Split(',');
            else
                statisticsChartCaption = WidgetMultiSelectControl.SelectedProperties.Split(',');

            foreach (string word in statisticsChartCaption)
            {
                switch (word)
                {
                    case "Categories":
                        GeneralStatisticsChart_categoryChartControl.Visible = true;
                        break;
                    case "Subjects":
                        GeneralStatisticsChart_subjectChartControl.Visible = true;
                        break;
                    case "Test Areas":
                        GeneralStatisticsChart_testAreaChartControl.Visible = true;
                        break;
                    case "Complexities":
                        GeneralStatisticsChart_complexityChartControl.Visible = true;
                        break;
                }
            }
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(false);
            WidgetMultiSelectControlPrint.SelectedProperties =
                    WidgetMultiSelectControl.SelectedProperties;
        }

        protected void GeneralStatisticsChart_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                BindProperty(false);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
        #endregion Event Handlers

        #region Properties
        /// <summary>
        /// Represents the property for the chart Test statistics
        /// </summary>
        public TestStatistics GeneralStatisticsChartDataSource
        {
            set
            {
                //Assign the chart datasource
                GeneralStatisticsChart_categoryChartControl.DataSource =
                    value.CategoryStatisticsChartData;

                //Assign the data for the subject chart control
                GeneralStatisticsChart_subjectChartControl.DataSource
                    = value.SubjectStatisticsChartData;

                //Assign the test area chart data source
                GeneralStatisticsChart_testAreaChartControl.DataSource
                    = value.TestAreaStatisticsChartData;

                //Assign the data source
                GeneralStatisticsChart_complexityChartControl.DataSource
                    = value.ComplexityStatisticsChartData;
            }
        }
        #endregion Properties

        #region Private Method
        /// <summary>
        /// Represents the method to load the values
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the 
        /// widget
        /// </param>
        private void LoadValues(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            TestStatistics testStatistics = new ReportBLManager().
                GetGeneralStatisticsChartInformation(candidateReportDetail.TestKey);

            if (testStatistics == null)
                return;
            testkey = candidateReportDetail.TestKey;

            //Assign the data for the category chart control
            testStatistics.CategoryStatisticsChartData.
                ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatistics.CategoryStatisticsChartData.
                ChartWidth = 352;

            //Assign the chart length
            testStatistics.CategoryStatisticsChartData.
                ChartLength = 140;

            testStatistics.CategoryStatisticsChartData.
                IsDisplayChartTitle = true;

            //Assign the chart title
            testStatistics.CategoryStatisticsChartData.ChartTitle =
                Resources.HCMResource.AdditionalTestDetail_CategoryTitle;

            //Assign the chart type
            testStatistics.CategoryStatisticsChartData.
               ChartType = SeriesChartType.Pie;

            testStatistics.CategoryStatisticsChartData.ChartImageName =
                Constants.ChartConstants.REPORT_CHART_CATEGORY + "-" + testkey;

            testStatistics.CategoryStatisticsChartData.IsShowLabel = true;
            //Assign the chart datasource
            GeneralStatisticsChart_categoryChartControl.DataSource =
                testStatistics.CategoryStatisticsChartData;

            //Assign the chart type
            testStatistics.SubjectStatisticsChartData.
                ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatistics.SubjectStatisticsChartData.
                ChartWidth = 355;

            //Assign the chart Length
            testStatistics.SubjectStatisticsChartData.
                ChartLength = 134;

            testStatistics.SubjectStatisticsChartData.
                IsDisplayChartTitle = true;

            testStatistics.SubjectStatisticsChartData.ChartImageName =
                Constants.ChartConstants.REPORT_CHART_SUBJECT + "-" + testkey;
            //Assign the chart title
            testStatistics.SubjectStatisticsChartData.
                ChartTitle = Resources.HCMResource.AdditionalTestDetail_SubjectTitle;
            testStatistics.SubjectStatisticsChartData.IsShowLabel = true;
            //Assign the data for the subject chart control
            GeneralStatisticsChart_subjectChartControl.DataSource
                = testStatistics.SubjectStatisticsChartData;

            //Assign the data for the testarea  chart control
            testStatistics.TestAreaStatisticsChartData.
           ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatistics.TestAreaStatisticsChartData.
                ChartWidth = 337;

            //Assign the chart length
            testStatistics.TestAreaStatisticsChartData.
                ChartLength = 130;

            testStatistics.TestAreaStatisticsChartData.
                IsDisplayChartTitle = true;

            //Assign the chart title
            testStatistics.TestAreaStatisticsChartData.
                ChartTitle = Resources.HCMResource.AdditionalTestDetail_TestAreaTitle;

            testStatistics.TestAreaStatisticsChartData.ChartImageName =
                Constants.ChartConstants.REPORT_CHART_TESTAREA + "-" + testkey;

            testStatistics.TestAreaStatisticsChartData.IsShowLabel = true;

            //Assign the test area chart data source
            GeneralStatisticsChart_testAreaChartControl.DataSource
                = testStatistics.TestAreaStatisticsChartData;

            //Assign the data for the complexity  chart control
            testStatistics.ComplexityStatisticsChartData.
           ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatistics.ComplexityStatisticsChartData.
                ChartWidth = 350;

            //Assign the chart length
            testStatistics.ComplexityStatisticsChartData.
                ChartLength = 140;

            testStatistics.ComplexityStatisticsChartData.
                IsDisplayChartTitle = true;

            //Assign the chart title
            testStatistics.ComplexityStatisticsChartData.
                ChartTitle = Resources.HCMResource.AdditionalTestDetail_ComplexityTitle;

            testStatistics.ComplexityStatisticsChartData.ChartImageName =
                 Constants.ChartConstants.REPORT_CHART_COMPLEXITY + "-" + testkey;
            testStatistics.ComplexityStatisticsChartData.IsShowLabel = true;
            //Assign the data source
            GeneralStatisticsChart_complexityChartControl.DataSource
                = testStatistics.ComplexityStatisticsChartData;

            if (IsPostBack)
            {
                WidgetMultiSelectControl.WidgetTypePropertyDataSource = BindDropDown();
            }
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties =
                    WidgetMultiSelectControl.SelectedProperties;
        }

        private List<DropDownItem> BindDropDown()
        {
            dropDownItemList = new List<DropDownItem>();
            dropDownItemList.Add(new DropDownItem("Categories", Constants.ChartConstants.REPORT_CHART_CATEGORY + "-" + testkey + ".png"));
            dropDownItemList.Add(new DropDownItem("Subjects", Constants.ChartConstants.REPORT_CHART_SUBJECT + "-" + testkey + ".png"));
            dropDownItemList.Add(new DropDownItem("Test Areas", Constants.ChartConstants.REPORT_CHART_TESTAREA + "-" + testkey + ".png"));
            dropDownItemList.Add(new DropDownItem("Complexities", Constants.ChartConstants.REPORT_CHART_COMPLEXITY + "-" + testkey + ".png"));
            return dropDownItemList;
        }
        #endregion Private Method

        #region IWidgetControl Members

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>     
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;
            //Load the chart details 
            LoadValues(instance);
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            //return new UpdatePanel[] { GeneralStatisticsChart_updatePanel };
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}