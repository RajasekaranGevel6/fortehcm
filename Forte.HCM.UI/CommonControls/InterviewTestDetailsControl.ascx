<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterviewTestDetailsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.InterviewTestDetailsControl" %>
<script type="text/javascript">

    function ClearPositionProfile() {
        document.getElementById("<%= InterviewTestDetailsControl_positionProfileTextBox.ClientID%>").value = '';
        document.getElementById("<%= InterviewTestDetailsControl_positionProfileIDHiddenField.ClientID%>").value = '';

        return false;
    }


    function isNumeric(keyCode) {
        if (!(keyCode >= 65 && keyCode <= 90)) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td class="panel_bg">
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                                        <tr>
                                            <td colspan="5" class="header_bg">
                                                <asp:Literal ID="InterviewTestDetailsControl_testDetailsMessageLiteral" runat="server"
                                                    Text="Interview Details"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table border="0" cellspacing="3" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="InterviewTestDetailsControl_testIdLabel" Text="Interview ID"
                                                                runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="InterviewTestDetailsControl_testDetailsTestIdReadOnlyLabel" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="InterviewTestDetailsControl_testNameLabel" Text="Interview Name" runat="server"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="InterviewTestDetailsControl_testNameTextBox" Columns="50" runat="server"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="InterviewTestDetailsControl_testDescriptionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Interview Description"></asp:Label><span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="InterviewTestDetailsControl_testDescriptionTextBox" runat="server"
                                                                Columns="80" TextMode="MultiLine" Height="60" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                onchange="CommentsCount(500,this)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="InterviewTestDetailsControl_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Position Profile"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <div style="float: left; padding-right: 2px;">
                                                                <asp:TextBox ID="InterviewTestDetailsControl_positionProfileTextBox" MaxLength="50"
                                                                    ReadOnly="true" Columns="50" runat="server">
                                                                </asp:TextBox>
                                                            </div>
                                                            <div style="float: left; height: 16px; padding-right: 4px">
                                                                <asp:ImageButton ID="InterviewTestDetailsControl_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile and associate with interview" />
                                                                <asp:ImageButton ID="InterviewTestDetailsControl_positionProfileHelpImageButton"
                                                                    SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Click here to select the position profile to associate with the interview" />
                                                            </div>
                                                            <div style="padding-top: 2px; padding-left: 10px">
                                                                <asp:LinkButton ID="InterviewTestDetailsControl_clearPositionProfileLinkButton" runat="server"
                                                                    Text="Clear" SkinID="sknActionLinkButton" OnClientClick="javascript:return ClearPositionProfile()"
                                                                    ToolTip="Click here to clear the 'Position Profile' field" />
                                                            </div>
                                                            <asp:HiddenField ID="InterviewTestDetailsControl_positionProfileIDHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript" language="javascript">
                               
                                
</script>
