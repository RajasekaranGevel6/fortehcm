﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientInfoControl.cs
// File that represents the user interface for the client contact information 

#endregion Header

#region Directives                                                             
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using System.Text;
#endregion

namespace Forte.HCM.UI.CommonControls
{
    public partial class ClientInfoControl : System.Web.UI.UserControl
    {
        #region Custom Event Handler and Delegate                              

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        //   public event ControlMessageThrownDelegate ControlMessageThrown;
        public delegate void AddClientEvent(object sender, EventArgs e);
        public delegate void Button_CommandClick(object sender, CommandEventArgs e);
        
        public event Button_CommandClick CommandEventAdd_Click;
        public event AddClientEvent AddClientEvent_Command;
        
        public delegate void DataListCommand(object sender, DataListCommandEventArgs e);

        public event DataListCommand DataListEvent_Command;
        #endregion Custom Event Handler and Delegate

        #region Event Handlers                                                 

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ClientInfoControl_AddContact_ImageButton.CommandName = Constants.ClientManagementConstants.ADD_CONTACT;
                ClientInfoControl_addclientImageButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT;
                ClientInfoControl_AddDepartment_ImageButton.CommandName = Constants.ClientManagementConstants.ADD_DEPARTMENT;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }


        /// <summary>
        /// Handles the SelectedIndexChanged event of the ClientInfoControl_departmentCheckBoxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ClientInfoControl_departmentCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBoxList checkBoxList = (CheckBoxList)sender;
                StringBuilder selectedPropertyStringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        selectedPropertyStringBuilder.Append(checkBoxList.Items[i].Text);
                        selectedPropertyStringBuilder.Append(",");
                    }
                }
                ClientInfoControl_departmentComboTextBox.Text = selectedPropertyStringBuilder.ToString().TrimEnd(',');
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ClientInfoControl_departmentContactPanelCheckBoxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void ClientInfoControl_departmentContactPanelCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBoxList checkBoxList = (CheckBoxList)sender;
                StringBuilder selectedPropertyStringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        selectedPropertyStringBuilder.Append(checkBoxList.Items[i].Text);
                        selectedPropertyStringBuilder.Append(",");
                    }
                }
                ClientInfoControl_departmentContactComboTextbox.Text = selectedPropertyStringBuilder.ToString().TrimEnd(',');
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }

        /// <summary>
        /// Handles the DeleteCommand event of the ClientInfoControl_clientDataList control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.DataListCommandEventArgs"/>
        /// instance containing the event data.</param>
        protected void ClientInfoControl_clientDataList_DeleteCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    ImageButton SearchClientControl_removeImageButton = (ImageButton)
                        e.Item.FindControl("ClientInfoControl_removeImageButton");
                    if (SearchClientControl_removeImageButton.CommandName == "Delete")
                    {
                        ClientInfoControl_clientNameTextBox.Text = string.Empty;
                        if (DataListEvent_Command != null)
                            DataListEvent_Command(source, e);

                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        protected void ClientInfoControl_addImageButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (CommandEventAdd_Click != null)
                {
                    CommandEventAdd_Click(sender, e);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Handler to show the selected client name in a datalist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClientInfoControl_clientNameTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (AddClientEvent_Command != null)
                    AddClientEvent_Command(sender, e);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
        #endregion

        #region Public Methods                                                 
            
        public List<ClientContactInformation> ClientContactInformations
        {
            get
            {
                try
                {
                    List<ClientContactInformation> clientContactInformationList = null;
                    if (Utility.IsNullOrEmpty(ClientInfoControl_clientNameHiddenField.Value))
                        return clientContactInformationList;
                    for (int val = 0; val < ClientInfoControl_departmentContactPanelCheckBoxList.Items.Count; val++)
                    {
                        if (ClientInfoControl_departmentContactPanelCheckBoxList.Items[val].Selected)
                        {
                            if (clientContactInformationList == null)
                                clientContactInformationList = new List<ClientContactInformation>();
                            ClientContactInformation clientContactInformation = new ClientContactInformation();
                            clientContactInformation.ClientName = ClientInfoControl_clientNameHiddenField.Value;
                            string[] contacts = ClientInfoControl_departmentContactPanelCheckBoxList.Items[val].Text.Trim().Split('-');
                            string[] departmentDetails = ClientInfoControl_departmentContactPanelCheckBoxList.Items[val].Value.Split('-');
                            clientContactInformation.ClientID = int.Parse(departmentDetails[0]);
                            clientContactInformation.DepartmentID = departmentDetails[1] == "0" ? (int?)null : int.Parse(departmentDetails[1]);
                            clientContactInformation.ContactID = int.Parse(departmentDetails[2]);
                            clientContactInformation.DepartmentName = contacts[0];
                            clientContactInformation.ContactName = contacts[1];
                            clientContactInformationList.Add(clientContactInformation);
                        }
                    }

                    for (int val = 0; val < ClientInfoControl_departmentCheckBoxList.Items.Count; val++)
                    {
                        if (ClientInfoControl_departmentCheckBoxList.Items[val].Selected)
                        {
                            if (clientContactInformationList == null)
                                clientContactInformationList = new List<ClientContactInformation>();

                            if (clientContactInformationList.Exists(item => item.DepartmentName == ClientInfoControl_departmentCheckBoxList.Items[val].Text))
                            {
                                continue;
                            }
                            {
                                ClientContactInformation clientContactInformation = new ClientContactInformation();
                                clientContactInformation.ClientName = ClientInfoControl_clientNameHiddenField.Value;
                                clientContactInformation.DepartmentName = ClientInfoControl_departmentCheckBoxList.Items[val].Text.Trim();
                                clientContactInformation.ClientID = int.Parse(ClientInfoControl_clientIDHiddenField.Value);
                                clientContactInformation.DepartmentID = int.Parse(ClientInfoControl_departmentCheckBoxList.Items[val].Value);
                                clientContactInformationList.Add(clientContactInformation);
                            }

                        }
                    }
                    if (Utility.IsNullOrEmpty(clientContactInformationList))
                    {
                        clientContactInformationList = new List<ClientContactInformation>();
                        ClientContactInformation clientContactInformation = new ClientContactInformation();
                        clientContactInformation.ClientID = int.Parse(ClientInfoControl_clientIDHiddenField.Value);
                        clientContactInformation.ClientName = ClientInfoControl_clientNameHiddenField.Value;
                        clientContactInformationList.Add(clientContactInformation);
                    }

                    return clientContactInformationList;
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }

        public List<Department> BindClientName
        {
            set
            {
                try
                {
                    ClientInfoControl_clientNameHiddenField.Value = value == null ? null : value[0].ClientName;
                    ClientInfoControl_clientIDHiddenField.Value = value == null ? null : value[0].ClientID.ToString();
                    ClientInfoControl_clientDataList.DataSource = value;
                    ClientInfoControl_clientDataList.DataBind();
                }
                catch (Exception exp)
                {
                    throw exp;
                }


            }
        }

        public List<Department> BindDepartmentList
        {
            set
            {
                try
                {
                    ClientInfoControl_departmentCheckBoxList.Items.Clear();
                    ////Bind Department list
                    ClientInfoControl_departmentCheckBoxList.DataSource = value;
                    ClientInfoControl_departmentCheckBoxList.DataTextField = "DepartmentName";
                    ClientInfoControl_departmentCheckBoxList.DataValueField = "DepartmentID";
                    ClientInfoControl_departmentCheckBoxList.DataBind();
                    ClientInfoControl_departmentComboTextBox.Text = "";
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }

        public List<int> BindSelectedDepartment
        {
            set
            {
                try
                {
                    if (value == null)
                        return;
                    StringBuilder departmentName = new StringBuilder();

                    for (int val = 0; val < ClientInfoControl_departmentCheckBoxList.Items.Count; val++)
                    {
                        if (value.Exists(item => item.ToString() == ClientInfoControl_departmentCheckBoxList.Items[val].Value))
                        {
                            ClientInfoControl_departmentCheckBoxList.Items[val].Selected = true;
                            departmentName.Append(ClientInfoControl_departmentCheckBoxList.Items[val].Text);
                            departmentName.Append(",");

                        }
                    }
                    ClientInfoControl_departmentComboTextBox.Text = departmentName.ToString().TrimEnd(',');
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public List<string> BindSelectedContact
        {
            set
            {
                try
                {
                    if (value == null)
                        return;
                    StringBuilder contactNames = new StringBuilder();
                    for (int val = 0; val < ClientInfoControl_departmentContactPanelCheckBoxList.Items.Count; val++)
                    {
                       if (value.Exists(item => item.ToString() == ClientInfoControl_departmentContactPanelCheckBoxList.Items[val].Value))
                       {
                            ClientInfoControl_departmentContactPanelCheckBoxList.Items[val].Selected = true;
                            contactNames.Append(ClientInfoControl_departmentContactPanelCheckBoxList.Items[val].Text);
                            contactNames.Append(",");
                        }
                    }
                    ClientInfoControl_departmentContactComboTextbox.Text = contactNames.ToString().TrimEnd(',');
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public List<ClientContactInformation> BindContactList
        {
            set
            {
                try
                {
                    ClientInfoControl_departmentContactPanelCheckBoxList.Items.Clear();
                    ClientInfoControl_departmentContactPanelCheckBoxList.DataSource = value;
                    ClientInfoControl_departmentContactPanelCheckBoxList.DataTextField = "ContactName";
                    ClientInfoControl_departmentContactPanelCheckBoxList.DataValueField = "ClientDepartmentContactID";
                    ClientInfoControl_departmentContactPanelCheckBoxList.DataBind();
                    ClientInfoControl_departmentContactComboTextbox.Text = "";
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }

        #endregion
        
       
    }
}