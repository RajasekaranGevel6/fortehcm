﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupAnalysisTestAreaChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GroupAnalysisTestAreaChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="multiSeriesReport"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="GroupAnalysisTestAreaChartControl_updatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <div style="width: 100%; overflow: auto">
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="GroupAnalysisTestAreaChartControl_testKeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="GroupAnalysisTestAreaChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="GroupAnalysisTestAreaChartControl_selectProperty"
                                            OnClick="GroupAnalysisTestAreaChartControl_widgetMultiSelectControl_Click" 
                                            OncancelClick="GroupAnalysisTestAreaChartControl_widgetMultiSelectControl_CancelClick"/>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="GroupAnalysisTestAreaChartControl_selectScoreLabel" runat="server"
                                                    Text="Score Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisTestAreaChartControl_absoluteScoreRadioButton"
                                                    runat="server" Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="true"
                                                    OnCheckedChanged="GroupAnalysisTestAreaChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisTestAreaChartControl_relativeScoreRadioButton"
                                                    runat="server" Text=" Relative Score" GroupName="1" AutoPostBack="true" OnCheckedChanged="GroupAnalysisTestAreaChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td align="left">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <uc1:multiSeriesReport ID="GroupAnalysisTestAreaChartControl_multiSeriesChart" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
