﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ConfirmMsgControl.cs
// File that represents the confirmation dialog which performs a task 
// based on the button click.

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Text;

using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using System.Web.UI.WebControls;
using Forte.HCM.Support;
using Forte.HCM.Trace;
using System.Configuration;
using Forte.HCM.Utilities;
using Forte.HCM.BL;


#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// The class that represents the layout for confirm message popup
    /// to across all the pages. Based on button click, the respective
    /// event handler will be performed. 
    /// </summary>
    public partial class ConfirmInterviewScoreMsgControl : UserControl
    {
        // This event handler declaration for confirm message control
        public delegate void ConfirmMessageThrownDelegate(object sender, ConfirmMessageEventArgs e);

        #region Public Properties

        // This property allows for a way to add child controls to the user control on the page
        // Any controls added to it are added to the place holder in the OnInit method below
        public Control ContentTemplate { get; set; }

        /// <summary>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    // Add handler for email button.
                    ConfirmInterviewScoreMsgControl_withScoreButton.Attributes.Add
                        ("onclick", "javascript:return ShowEmailInterviewSummaryPublishURL('PCIRSS')");

                    ConfirmInterviewScoreMsgControl_withOutScoreButton.Attributes.Add
                        ("onclick", "javascript:return ShowEmailInterviewSummaryPublishURL('PCIRHS')");

                    // Clear session values.
                    Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITH_SCORE"] = null;
                    Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITHOUT_SCORE"] = null;
                    Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"] = null;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// This property helps to show the buttons in the popup
        /// based on the button type. 
        /// </summary>
        public MessageBoxType Type
        {
            set
            {
                if (value == MessageBoxType.InterviewScoreConfirmType)
                {
                    ConfirmInterviewScoreMsgControl_withOutScoreButton.Visible = true;
                    ConfirmInterviewScoreMsgControl_withScoreButton.Visible = true;
                }
            }
        }

        /// <summary>
        /// Property that accepts a title string from the various pages.
        /// </summary>
        public string Title
        {
            set
            {
                ConfirmInterviewScoreMsgControl_titleLiteral.Text = value;
            }
        }

        /// <summary>
        /// Method to store the interview candidate param details
        /// </summary>
        public InterviewScoreParamDetail InterviewParams
        {
            set
            {
                // Keep the category list in the view state.
                ViewState["INTERVIEW_PARAMS"] = value;

            }
            get
            {
                if (ViewState["INTERVIEW_PARAMS"] == null)
                    return null;

                return ViewState["INTERVIEW_PARAMS"] as InterviewScoreParamDetail;
            }
        }

        /// <summary>
        /// Method to bind the interview candidate publish url
        /// </summary>
        public void ShowScoreUrl()
        {
            string baseURL = ConfigurationManager.AppSettings["PublishAssessorRating"];
            string interviewResponseUrl = string.Empty;

            // Get the existing selected list.
            InterviewScoreParamDetail interviewParams = null;

            if (ViewState["INTERVIEW_PARAMS"] == null)
                interviewParams = new InterviewScoreParamDetail();
            else
                interviewParams = ViewState["INTERVIEW_PARAMS"] as InterviewScoreParamDetail;

            if (interviewParams != null)
            {
                ConfirmInterviewScoreMsgControl_withOutScoreUrlHyperLink.Text = string.Empty;
                ConfirmInterviewScoreMsgControl_withOutScoreUrlHyperLink.NavigateUrl = string.Empty;
                ConfirmInterviewScoreMsgControl_withScoreUrlHyperLink.Text = string.Empty;
                ConfirmInterviewScoreMsgControl_withScoreUrlHyperLink.NavigateUrl = string.Empty;
                ConfirmInterviewScoreMsgControl_withOutScoreButton.PostBackUrl = string.Empty;
                ConfirmInterviewScoreMsgControl_withScoreButton.PostBackUrl = string.Empty;

                bool isExist = true;
                string interviewPublishCode = string.Empty;
                InterviewScoreParamDetail interviewScoreCodeDetail = null;
                while (isExist)
                {
                    interviewPublishCode = Utility.RandomString(9);
                    interviewScoreCodeDetail = new AssessmentSummaryBLManager().
                        UpdateCandidateInterviewScoreCode(interviewParams.CandidateInterviewSessionKey,
                        interviewParams.AttemptID, interviewPublishCode);
                    if (interviewScoreCodeDetail != null)
                    {
                        isExist = interviewScoreCodeDetail.IsCodeExist;
                        interviewPublishCode = interviewScoreCodeDetail.ScoreCode;
                    }
                }

                //To show the score
                string urlWithScore = string.Format(
                           ConfigurationManager.AppSettings["INTERVIEW_RESULT_PUBLISH_URL"],
                           string.Concat(interviewPublishCode, "S"));

                //To hide the score
                string urlWithOutScore = string.Format(
                       ConfigurationManager.AppSettings["INTERVIEW_RESULT_PUBLISH_URL"],
                        string.Concat(interviewPublishCode, "H"));

                ConfirmInterviewScoreMsgControl_withOutScoreUrlHyperLink.Text = urlWithOutScore;
                ConfirmInterviewScoreMsgControl_withOutScoreUrlHyperLink.NavigateUrl = urlWithOutScore;
                ConfirmInterviewScoreMsgControl_withOutScoreButton.PostBackUrl = urlWithOutScore;

                ConfirmInterviewScoreMsgControl_withScoreUrlHyperLink.Text = urlWithScore;
                ConfirmInterviewScoreMsgControl_withScoreUrlHyperLink.NavigateUrl = urlWithScore;
                ConfirmInterviewScoreMsgControl_withScoreButton.PostBackUrl = urlWithScore;

                Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITH_SCORE"] = urlWithScore;
                Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITHOUT_SCORE"] = urlWithOutScore;
                Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"] = interviewParams.CandidateName;
            }
        }

        #endregion Public Properties

        #region Public Events and Delegate

        // The public events must conform to this format
        public delegate void Button_Click(object sender, EventArgs e);
        public delegate void Button_CommandClick(object sender, CommandEventArgs e);

        // These two public events are to be customized as need on the page and will be called
        // by the private events of the buttons in the user control
        public event Button_Click OkClick;
        public event Button_Click CancelClick;

        #endregion Public Events and Delegate

        #region Event Handler

        /// <summary>
        /// Event handler will get fired when the YES button is clicked 
        /// in the confirmation popup.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void ConfirmInterviewScoreMsgControl_yesButton_Click(object sender, EventArgs e)
        {
            
            if (OkClick != null)
                this.OkClick(sender, e);
        }

        /// <summary>
        /// Event handler will get triggered when the NO button is clicked in the popup.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ConfirmInterviewScoreMsgControl_noButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        /// <summary>
        /// When the CANCEL button is clicked in the popup, this event 
        /// will get fired.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void ConfirmInterviewScoreMsgControl_cancelButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        /// <summary>
        /// This handler fires when the OK button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void ConfirmInterviewScoreMsgControl_okButton_Click(object sender, EventArgs e)
        {
            if (OkClick != null)
                this.OkClick(sender, e);
            //if (ConfirmMessageThrown != null)
            //{
            //    ConfirmMessageThrown(this, new ConfirmMessageEventArgs(ActionType.Ok));
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            // base.OnInit(e);

            //add the child controls from the public property to the place holder in the panel
            //  this.phMain.Controls.Add(this.ContentTemplate);
        }

        #endregion Event Handler
    }
}