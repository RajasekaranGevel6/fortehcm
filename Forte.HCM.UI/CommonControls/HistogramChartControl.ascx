﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HistogramChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.HistogramChartControl" %>
<table>
    <tr>
        <td runat="server" id="HistogramChartControl_td">
            <asp:Chart ID="HistogramChartControl_histogramChart" runat="server" Palette="Pastel"
                BackColor="Transparent" AntiAliasing="Graphics" Height="150px" Width="250px"
                ToolTip="Click here to zoom graph" EnableViewState="true">
                <Series>
                    <asp:Series IsValueShownAsLabel="True" ChartArea="HistogramChartControl_chartArea"
                        XValueType="Double" Name="HistogramChartControl_histogramSeries" BorderColor="180, 26, 59, 105"
                        Font="Trebuchet MS, 8.25pt" YValueType="Double">
                        <SmartLabelStyle AllowOutsidePlotArea="Yes" IsMarkerOverlappingAllowed="True" MaxMovingDistance="100" />
                    </asp:Series>
                </Series>
                <Titles>
                    <asp:Title Name="HistogramChartControl_title" TextStyle="Default" ForeColor="180, 65, 140, 240"
                        Font="Arial, 8.25pt, style=Bold">
                    </asp:Title>
                </Titles>
                <ChartAreas>
                    <asp:ChartArea Name="HistogramChartControl_chartArea" BackSecondaryColor="Transparent"
                        BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                        <AxisX TitleFont="Trebuchet MS, 8pt" LineColor="64, 64, 64, 64" IsLabelAutoFit="False"
                            IsStartedFromZero="true">
                            <LabelStyle ForeColor="180, 65, 140, 240" Angle="0" />
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY Interval="1">
                            <MajorGrid Enabled="false" />
                            <LabelStyle ForeColor="180, 65, 140, 240" />
                        </AxisY>
                        <Position Height="91" Width="100" Y="5" />
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </td>
    </tr>
</table>
