﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillsMatrixControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SkillsMatrixControl" %>
  <%@ Register Src="~/CommonControls/MainResumeControl.ascx" TagName="MainResumeControl"
    TagPrefix="uc1" %>
    <script type="text/javascript">

        // Handler method that will be called when the maximize or restore
        // button is clicked. This will maximize or restore the panel to
        // show or hide full view.
        function ExpandOrRestore(ctrlExpandHide, expandImage, compressImage) 
        {
            if (document.getElementById(ctrlExpandHide) != null) {
                var crtlStyle = document.getElementById(ctrlExpandHide).style.display;

                if (crtlStyle == "block") { 
                    document.getElementById(ctrlExpandHide).style.display = "none";
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(compressImage).style.display = "none"; 
                }
                else 
                {
                    document.getElementById(ctrlExpandHide).style.display = "block";
                    document.getElementById(expandImage).style.display = "none";
                    document.getElementById(compressImage).style.display = "block"; 
                }
            }
            return false;
        }
 </script>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align:center;vertical-align:middle;">
    <tr id="SkillsMatrixControl_resumeTR" runat="server">
        <td id="Td1" class="header_bg" align="center" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 50%" align="left" >
                        
                    </td>
                    <td style="width: 50%" align="right">
                        <span id="SkillsMatrixControl_resumeUpSpan" runat="server" style="display: block;">
                            <asp:Image ID="SkillsMatrixControl_resumeUpImage" runat="server" SkinID="sknMinimizeImage" />
                        </span>
                        <span id="SkillsMatrixControl_resumeDownSpan" runat="server" style="display: none;">
                            <asp:Image ID="SkillsMatrixControl_resumeDownImage" runat="server" SkinID="sknMaximizeImage" />
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
             <div style="display:none">
                    <div style="height: 420px; overflow: auto;" id="ResumeEditor_scrollDetailsDiv" runat="server">
                    <uc1:MainResumeControl ID="SkillsMatrixControl_mainResumeControl" runat="server" />
                </div>
            </div>
            <div style="display:block;padding-bottom:15px">
                    <div runat="server" id="SkillsMatrixControl_topPaneInnerDiv" class="RightPaneInnerDiv">
                        <div runat="server" id="ResumeEditor_perviewHtmlDiv" style="height: 220px;">
                        </div>
                </div>
            </div>
        </td>
    </tr>

  <tr id="SkillsMatrixControl_skillTR" runat="server" style="padding-bottom:10px">
        <td id="Td2" class="header_bg" align="center" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 50%" align="left" >
                        
                    </td>
                    <td style="width: 50%" align="right">
                        <span id="SkillsMatrixControl_skillUpSpan" runat="server" style="display: block;">
                            <asp:Image ID="SkillsMatrixControl_skillUpImage" runat="server" SkinID="sknMinimizeImage" />
                        </span>
                        <span id="SkillsMatrixControl_skillDownSpan" runat="server" style="display: none;">
                            <asp:Image ID="SkillsMatrixControl_skillDownImage" runat="server" SkinID="sknMaximizeImage" />
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="padding-top:10px">
        <td style="text-align:center">
            <div runat="server" id="SkillsMatrixControl_skillDIV">  
                <div style="display: inline; float: none; vertical-align: middle;text-align:center;">
                    <asp:UpdatePanel runat="server" ID="SkillsMatrixControl_updatePanel">
                        <ContentTemplate> 
                            <div style="overflow:auto;height:300px">                        
                                <asp:GridView ID="SkillsMatrixControl_competencyGridView" runat="server"
                                    BorderStyle="None" OnPreRender="SkillsMatrixControl_competencyGridView_PreRender" BorderWidth="0" OnRowDataBound="SkillsMatrixControl_competencyGridView_RowDataBound"
                                    AutoGenerateColumns="true" CellPadding="0" CellSpacing="0">
                                    <Columns>                                                       
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div> 
            </div>
        </td>
    </tr>
</table>
