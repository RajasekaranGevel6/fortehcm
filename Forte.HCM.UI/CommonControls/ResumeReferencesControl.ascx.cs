﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ReferencesControl.cs
// File that represents the user interface for the reference information details

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using System.Data;

#endregion Directives
namespace Forte.HCM.UI.CommonControls
{
    public partial class ResumeReferencesControl : System.Web.UI.UserControl
    {
        #region Custom Event Handler and Delegate

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;
        private int _rowCounter;
        #endregion Custom Event Handler and Delegate
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ResumeReferencesControl_listView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void ResumeReferencesControl_addDefaultButton_Click(object sender, EventArgs e)
        {
            SetViewState();

            ResumeReferencesControl_listView.DataSource = null;
            if (ViewState["dataSource"] != null)
            {
                this.dataSource = (List<Reference>)ViewState["dataSource"];
            }
            else
                this.dataSource = new List<Reference>();
            this.dataSource.Add(new Reference());
            ViewState["dataSource"] = this.dataSource;
            _rowCounter = this.dataSource.Count;
            ResumeReferencesControl_listView.DataSource = this.dataSource;
            ResumeReferencesControl_listView.DataBind();
        }        
            
        protected void ResumeReferencesControl_listView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Button ResumeReferencesControl_addButton = ((Button)e.Item.FindControl
                ("ResumeReferencesControl_addButton"));
            Button ResumeReferencesControl_deleteButton = ((Button)e.Item.FindControl
                ("ResumeReferencesControl_deleteButton"));
            TextBox ResumeReferencesControl_nameTextBox = ((TextBox)e.Item.FindControl
                ("ResumeReferencesControl_nameTextBox"));
            ((Panel)e.Item.FindControl("ResumeReferencesControl_referencePanel")).GroupingText =
               "References " +((e.Item as ListViewDataItem).DisplayIndex + 1).ToString();

            ResumeReferencesControl_addButton.Visible = false;
            ResumeReferencesControl_deleteButton.Visible = false;

            if (_rowCounter == ((e.Item as ListViewDataItem).DisplayIndex + 1))
            {
                ResumeReferencesControl_nameTextBox.Focus();
                ResumeReferencesControl_addButton.Visible = true;
                if(_rowCounter!=1)
                    ResumeReferencesControl_deleteButton.Visible = true;
            }
        }

        protected void ResumeReferencesControl_listView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            SetViewState();
            if (e.CommandName == "deleteReference")
            {
                Button ResumeReferencesControl_deleteButton = (Button)e.Item.FindControl("ResumeReferencesControl_deleteButton");
                TextBox txtRowIndex = (TextBox)e.Item.FindControl("ResumeReferencesControl_deleteRowIndex");
                ResumeReferencesControl_deletedRowHiddenField.Value = txtRowIndex.Text;
                ResumeReferencesControl_okPopUpClick(ResumeReferencesControl_deleteButton, new EventArgs());
            }
            else if (e.CommandName == "addReference")
            {
                Button ResumeReferencesControl_addButton = (Button)e.Item.FindControl("ResumeReferencesControl_addButton");
                ResumeReferencesControl_addDefaultButton_Click(ResumeReferencesControl_addButton, new EventArgs());
            }
        }

        protected void ResumeReferencesControl_okPopUpClick(object sender, EventArgs e)
        {            
            int intDeletedRowIndex = Convert.ToInt32(ResumeReferencesControl_deletedRowHiddenField.Value);
            if (intDeletedRowIndex != 0)
            {
                this.dataSource = (List<Reference>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intDeletedRowIndex - 1);                 
            }
            else
            {
                int intLastRecord = ResumeReferencesControl_listView.Items.Count - 1;
                this.dataSource = (List<Reference>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intLastRecord);                  
            }
            _rowCounter = dataSource.Count;
            ResumeReferencesControl_listView.DataSource = dataSource;
            ResumeReferencesControl_listView.DataBind();
            SetViewState();   
            // Fire the message event
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("Reference detail deleted successfully", MessageType.Success));
                   
        }

        #endregion Event Handlers

        #region Public Properties

        public List<Reference> DataSource
        {
            set
            {
                if (value == null)
                {
                    ResumeReferencesControl_addDefaultButton.Visible = true;
                    return;
                }
                // Set values into controls.
                _rowCounter = value.Count;

                if (_rowCounter != 0)
                    ResumeReferencesControl_addDefaultButton.Visible = false;

                ResumeReferencesControl_listView.DataSource = value;
                ResumeReferencesControl_listView.DataBind();
                ViewState["dataSource"] = value;
            }
            get
            {
                if (ViewState["dataSource"] != null)
                    return (List<Reference>)ViewState["dataSource"];
                else
                    return null;
            }
        }

        public string SetContactInfo(ContactInformation pContInfo)
        {
            string contInfo = "";
            if (pContInfo != null)
            {
                contInfo += (pContInfo.Phone != null) ? pContInfo.Phone.Mobile : "";
                //contInfo = (pContInfo.City != null) ? "," + pContInfo.City : "";
                //contInfo += (pContInfo.Country != null) ? "," + pContInfo.Country : "";
                //contInfo += (pContInfo.EmailAddress != null) ? "," + pContInfo.EmailAddress : "";

                contInfo = contInfo.StartsWith(",") ? contInfo.Substring(1) : contInfo;
            }
            return contInfo;
        }

        #endregion Public Properties

        #region Private Properties

        private List<Reference> dataSource;

        #endregion Private Properties

        #region Protected Methods

        protected void SetViewState()
        {

            List<Reference> oReferenceList = null;
            int intRefCnt = 0;
            foreach (ListViewDataItem item in ResumeReferencesControl_listView.Items)
            {
                if (oReferenceList == null)
                {
                    oReferenceList = new List<Reference>();
                }

                Reference oReference = new Reference();
                ContactInformation oContactInfo = new ContactInformation();
                PhoneNumber oPhone = new PhoneNumber();
                TextBox txtRefName = (TextBox)item.FindControl("ResumeReferencesControl_nameTextBox");
                TextBox txtRefOrg = (TextBox)item.FindControl("ResumeReferencesControl_organizationTextBox");
                TextBox txtRefRelation = (TextBox)item.FindControl("ResumeReferencesControl_relationTextBox");
                TextBox txtRefContactInfo = (TextBox)item.FindControl("ResumeReferencesControl_contInfoTextBox");
                intRefCnt = intRefCnt + 1;
                oReference.ReferenceId = intRefCnt;
                oReference.Name = txtRefName.Text.Trim();
                oReference.Organization = txtRefOrg.Text.Trim();
                oReference.Relation = txtRefRelation.Text.Trim();
                oPhone.Mobile = txtRefContactInfo.Text.Trim();
                oContactInfo.Phone = oPhone;
                oReference.ContactInformation = oContactInfo;
                oReferenceList.Add(oReference);
            }
            ResumeReferencesControl_listView.DataSource = oReferenceList;
            ResumeReferencesControl_listView.DataBind();
            ViewState["dataSource"] = oReferenceList;
            ResumeReferencesControl_addDefaultButton.Visible = false;
        }

        #endregion Protected Methods
    }
}