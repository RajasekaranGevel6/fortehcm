﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// This usercontrol is used to display the test session 
    /// details for a particular candidate session.
    /// </summary>
    public partial class InterviewScheduleControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Do Nothing
        }

        public string CandidateSessionId { set; get; }

        public string TitleMessage
        {
            set
            {
                InterviewScheduleControl_questionResultLiteral.Text = value;
            }
        }

        /// <summary>
        /// This property assigns the test session details to labels.
        /// </summary>
        public InterviewSessionDetail InterviewSession
        {
            set
            {
                InterviewScheduleControl_interviewSessionIDValueLabel.Text = value.InterviewTestSessionID;
                InterviewScheduleControl_candidateSessionIDValueLabel.Text = CandidateSessionId;
                InterviewScheduleControl_interviewIDValueLabel.Text = value.InterviewTestID;
                InterviewScheduleControl_interviewNameValueLabel.Text = value.InterviewTestName;
                InterviewScheduleControl_interviewAuthorValueLabel.Text = value.InteriewSessionAuthor;
                InterviewScheduleControl_interviewSessionDescriptionValueLiteral.Text =
                    value.InterviewSessionDesc == null ? value.InterviewSessionDesc : value.InterviewSessionDesc.ToString().Replace(Environment.NewLine, "<br />");
                InterviewScheduleControl_instructionsValueLiteral.Text = 
                    value.Instructions == null ? value.Instructions : value.Instructions.ToString().Replace(Environment.NewLine, "<br />");
                InterviewScheduleControl_allowInterviewToBePausedValueLabel.Text = value.AllowPauseInterview.ToString() == "Y" ? "Yes" : "No";
                InterviewScheduleControl_positionProfileValueLabel.Text = value.PositionProfileName;
                //InterviewScheduleControl_instructionsLabel.Text = value.Instructions;

                //Search the selected candidateSessionId and display the candidate informations.
                for (int index=0; index < value.CandidateInterviewSessions.Count; index++)
                {
                    if (value.CandidateInterviewSessions[index].CandidateTestSessionID == CandidateSessionId)
                    {
                        InterviewScheduleControl_expiryDateValueLabel.Text = GetDateFormat(value.CandidateInterviewSessions[index].ScheduledDate);
                        InterviewScheduleControl_candidateValueLabel.Text = value.CandidateInterviewSessions[index].CandidateFirstName;
                        InterviewScheduleControl_emailValueLabel.Text = value.CandidateInterviewSessions[index].Email;
                        break;
                    }
                }
            }
        }

        #region Private Methods                                                 

        /// <summary>
        /// This method return the date in MM/dd/yyyy format. If date is not given, 
        /// then it displays empty value in column.
        /// </summary>
        /// <param name="date">
        /// This parameter is passed from aspx page values can be ScheduledDate/CompletedDate
        /// </param>
        /// <returns>
        /// Returns the date with the format of MM/DD/YYYY
        /// </returns>
        protected string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }
        #endregion Private Methods                                             
    }
}