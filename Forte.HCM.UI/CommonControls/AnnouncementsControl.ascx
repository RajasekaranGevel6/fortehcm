﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnnouncementsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.AnnouncementsControl" %>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td style="width: 20%">
            <img height="140px" width="140px" src="../App_Themes/DefaultTheme/Images/announcements_blue.png" alt="Announcements"/>
        </td>
        <td style="width: 80%">
            <asp:ListView ID="AnnouncementsControl_listView" runat="server">
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer">
                        <tr runat="server" id="itemPlaceholder">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="width: 10%">
                            <asp:Label runat="server" ID="AnnouncementsControl_dateLabel" Text=""
                                SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="width: 90%">
                            <div class="label_multi_field_text">
                                <asp:Literal ID="AnnouncementsControl_infoLiteral" runat="server" Text=""></asp:Literal>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="AnnouncementsControl_dateLabel" Text=""
                                SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <div class="label_multi_field_text">
                                <asp:Literal ID="AnnouncementsControl_infoLiteral" runat="server" Text=""></asp:Literal>
                            </div>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
            </asp:ListView>
        </td>
    </tr>
</table>
