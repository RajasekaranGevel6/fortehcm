﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionDetailPreviewControl.cs
// File that represents the user interface for question preview.
// User can preview the currently selected question information over here.

#endregion Header

#region Directives                                                             

using System;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class QuestionDetailPreviewControl : UserControl
    {
        #region Event Handlers                                                 

        /// <summary>
        /// This event handler instantiate the modal popup control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Page.SetFocus(QuestionDetailPreviewControl_topCancelImageButton.ClientID);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (!IsPostBack)
                {
                    
                    if (ShowAddButton)
                        QuestionDetailPreviewControl_bottomAddButton.Visible = true;
                    else
                        QuestionDetailPreviewControl_bottomAddButton.Visible = false;
                    //ModalPopupExtender objModalPopupExtender =
                    //    (ModalPopupExtender)this.Page.FindControl("QuestionDetailPreviewControl_questionModalPopupExtender");

                    //if (objModalPopupExtender != null)
                    //{
                    //    objModalPopupExtender.CancelControlID =
                    //        QuestionDetailPreviewControl_topCancelImageButton.ClientID;
                    //}
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        public delegate void Button_Click(object sender, EventArgs e);
        public delegate void Add_Click(object sender, EventArgs e);
        public event Button_Click CancelClick;
        public event Add_Click AddClick;
        /// <summary>
        /// Helps to close question preview popup window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void QuestionDetailPreviewControl_bottomCloseLinkButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        protected void QuestionDetailPreviewControl_bottomAddButton_Click(object sender, EventArgs e)
        {
            if (AddClick != null)
                this.AddClick(sender, e);
        }

        /// <summary>
        /// Helps to cancel/close the question preview popup window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void QuestionDetailPreviewControl_topCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        #endregion Event Handlers

        #region Public properties                                              

        public string Title
        {
            set
            {
                QuestionDetailPreviewControl_questionResultLiteral.Text = value.Trim();
            }
        }

        public bool ShowAddButton { set; get; }

        
        public void SetFocus ()
        {
            Page.SetFocus(QuestionDetailPreviewControl_topCancelImageButton.ClientID);
         }


        /// <summary>
        /// This data source helps to load the question information while user wants to preview
        /// in single or edit question entry page
        /// </summary>
        public QuestionDetail QuestionDatasource
        {
            set
            {
                if (value == null)
                    return;
                if (value.Answer > 0)
                {
                    // Set correct answer.
                    value.AnswerChoices[value.Answer - 1].IsCorrect = true;
                }

                // Assign choices to the placeholder control
                QuestionDetailPreviewControl_answerChoicesPlaceHolder.Controls.Add
                    (new ControlUtility().GetAnswerChoices(value.AnswerChoices,false));

                // Assign subjects which are having selected status is true
                List<Subject> subjects = null;
                subjects = new List<Subject>();

                if (value.Subjects != null)
                {
                    // This loop will fetch the subjects which selected by the user.
                    foreach (Subject subject in value.Subjects)
                    {
                        Subject selectedSubjectList = new Subject();
                        if (subject.IsSelected == true)
                        {
                            selectedSubjectList.CategoryID = subject.CategoryID;
                            selectedSubjectList.CategoryName = subject.CategoryName;
                            selectedSubjectList.SubjectID = subject.SubjectID;
                            selectedSubjectList.SubjectName = subject.SubjectName;
                            selectedSubjectList.IsSelected = true;
                            subjects.Add(selectedSubjectList);
                        }
                    }

                    // Bind the subjects to the grid
                    QuestionDetailPreviewControl_categoryDataGrid.DataSource = subjects;
                    QuestionDetailPreviewControl_categoryDataGrid.DataBind();
                }
                else
                {
                    // This empty datasource gets assigned for showing question details with only category
                    // and subject title in the datagrid control when no category and subject is selected.
                    QuestionDetailPreviewControl_categoryDataGrid.DataSource = new List<Subject>();
                    QuestionDetailPreviewControl_categoryDataGrid.DataBind();
                }

                // Additional options
                QuestionDetailPreviewControl_testAreaValueLabel.Text = value.TestAreaName;
                QuestionDetailPreviewControl_complexityValueLabel.Text = value.ComplexityName;
                QuestionDetailPreviewControl_tagValueLabel.Text = value.Tag;
                QuestionDetailPreviewControl_questionLabel.Text = 
                    value.Question == null ? value.Question : value.Question.ToString().Replace(Environment.NewLine, "<br />");
                
                // Display question image
                if (value.HasImage)
                {
                    Session["POSTED_QUESTION_IMAGE"] = value.QuestionImage as byte[];
                    QuestionDetailPreviewControl_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE&question_key=" + value.QuestionKey;
                    QuestionDetailPreviewControl_questionImage.Visible = true;
                    QuestionDetailPreviewControl_questionDiv.Style.Add("height", "140px");
                }
                else
                {
                    QuestionDetailPreviewControl_questionImage.Visible = false;
                    QuestionDetailPreviewControl_questionDiv.Style.Add("height", "100%");
                }

            }
        }

        /// <summary>
        /// Get the question detail based on QuestionKey.
        /// </summary>
        /// <param name="questionKey"></param>
        public void LoadQuestionDetails(string questionKey,int questionRelationId)
        {
            if (ShowAddButton)
                QuestionDetailPreviewControl_bottomAddButton.Visible = true;
            else
                QuestionDetailPreviewControl_bottomAddButton.Visible = false;
            // Initialize queation detail object by calling BL method.
            QuestionDetail questionDetail = new QuestionBLManager().GetQuestion(questionKey);

            List<Subject> subjectList = new List<Subject>();
            subjectList = new CommonBLManager().GetCategorySubjects(questionKey, questionRelationId);
            // Assign datasource to question preview control's datasource.
            QuestionDetailPreviewControl_categoryDataGrid.DataSource = subjectList;
            QuestionDetailPreviewControl_categoryDataGrid.DataBind();

            // Assign additional setttings
            QuestionDetailPreviewControl_testAreaValueLabel.Text = questionDetail.TestAreaName;
            QuestionDetailPreviewControl_complexityValueLabel.Text = questionDetail.ComplexityName;
            QuestionDetailPreviewControl_tagValueLabel.Text = questionDetail.Tag;
            QuestionDetailPreviewControl_questionLabel.Text =
                questionDetail.Question == null ? questionDetail.Question : questionDetail.Question.ToString().Replace(Environment.NewLine, "<br />");
            
            // Set answer choice
            QuestionDetailPreviewControl_answerChoicesPlaceHolder.DataBind();
            QuestionDetailPreviewControl_answerChoicesPlaceHolder.Controls.Add(new ControlUtility().GetAnswerChoices(questionDetail.AnswerChoices,false));

            // Display question image
            if (questionDetail.HasImage) 
            {
                Session["POSTED_QUESTION_IMAGE"] = questionDetail.QuestionImage as byte[];
                QuestionDetailPreviewControl_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE&question_key=" + questionDetail.QuestionKey;
                QuestionDetailPreviewControl_questionImage.Visible = true;
                QuestionDetailPreviewControl_questionDiv.Style.Add("height", "140px");
            }
            else
            {
                QuestionDetailPreviewControl_questionImage.Visible = false;
                QuestionDetailPreviewControl_questionDiv.Style.Add("height", "100%");
            }
        }


        /// <summary>
        /// Get the question detail based on QuestionKey.
        /// </summary>
        /// <param name="questionKey"></param>
        public void LoadInterviewQuestionDetails(string questionKey, int questionRelationId)
        {
            if (ShowAddButton)
                QuestionDetailPreviewControl_bottomAddButton.Visible = true;
            else
                QuestionDetailPreviewControl_bottomAddButton.Visible = false;
            // Initialize queation detail object by calling BL method.
            QuestionDetail questionDetail = new QuestionBLManager().GetInterviewQuestion(questionKey);

            List<Subject> subjectList = new List<Subject>();
            subjectList = new CommonBLManager().GetInterviewCategorySubjects(questionKey, questionRelationId);
            // Assign datasource to question preview control's datasource.
            QuestionDetailPreviewControl_categoryDataGrid.DataSource = subjectList;
            QuestionDetailPreviewControl_categoryDataGrid.DataBind();

            // Assign additional setttings
            QuestionDetailPreviewControl_testAreaValueLabel.Text = questionDetail.TestAreaName;
            QuestionDetailPreviewControl_complexityValueLabel.Text = questionDetail.ComplexityName;
            QuestionDetailPreviewControl_tagValueLabel.Text = questionDetail.Tag;
            QuestionDetailPreviewControl_questionLabel.Text =
                questionDetail.Question == null ? questionDetail.Question : questionDetail.Question.ToString().Replace(Environment.NewLine, "<br />");
            
            // Set answer choice
            QuestionDetailPreviewControl_answerChoicesPlaceHolder.DataBind();
            QuestionDetailPreviewControl_answerChoicesPlaceHolder.Controls.Add(new ControlUtility().GetAnswerChoices(questionDetail.AnswerChoices, false));

            // Display question image
            if (questionDetail.HasImage)
            {
                Session["POSTED_QUESTION_IMAGE"] = questionDetail.QuestionImage as byte[];
                QuestionDetailPreviewControl_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE&question_key=" + questionDetail.QuestionKey;
                QuestionDetailPreviewControl_questionImage.Visible = true;
                QuestionDetailPreviewControl_questionDiv.Style.Add("height", "140px");
            }
            else
            {
                QuestionDetailPreviewControl_questionImage.Visible = false;
                QuestionDetailPreviewControl_questionDiv.Style.Add("height", "100%");
            }
        }

        #endregion Public Properties
    }
}