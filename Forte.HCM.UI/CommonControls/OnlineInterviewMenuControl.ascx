﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnlineInterviewMenuControl.ascx.cs" Inherits="Forte.HCM.UI.CommonControls.OnlineInterviewMenuControl" %>
<table cellpadding="0" cellspacing="0" border="0"  width="100%"  align="center" >
    <tr>
        <td id="MainTd" runat="server" align="center" style="background-repeat:no-repeat;height:44px;"> 
            <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td runat="server" id="tdBI" width="25%" align="center"  class="pp_workflow_text" title="Interview"> 
                        <a runat="server" id="OnlineInterviewMenuControl_biAnchor">
                        Interview
                        </a>
                     </td> 
                    <td  width="25%" class="pp_workflow_text"  align="center"  runat="server" id="tdDI">
                        <a  runat="server" id="OnlineInterviewMenuControl_diAnchor" title="Question Set">
                        Question Set
                        </a>
                     </td>
                    <td width="25%" align="center"  class="pp_workflow_text"  runat="server" id="tdWS" title="Assessors">
                        <a  runat="server" id="OnlineInterviewMenuControl_wsAnchor">
                        Assessors
                        </a>
                    </td>
                    <td align="center"  width="25%" class="pp_workflow_text"  runat="server" id="tdRA" title="Candidate">
                        <a  runat="server" id="OnlineInterviewMenuControl_raAnchor">Candidate</a>
                     </td>
                </tr>
            </table>
        </td> 
    </tr>
</table>