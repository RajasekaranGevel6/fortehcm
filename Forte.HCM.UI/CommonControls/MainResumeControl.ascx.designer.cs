﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls
{
    
    public partial class MainResumeControl {
        
        /// <summary>
        /// MainResumeControl_nameControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.NameControl MainResumeControl_nameControl;
        
        /// <summary>
        /// MainResumeControl_contactInfoControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ContactInformationControl MainResumeControl_contactInfoControl;
        
        /// <summary>
        /// MainResumeControl_executiveSummaryControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ExecutiveSummaryControl MainResumeControl_executiveSummaryControl;
        
        /// <summary>
        /// MainResumeControl_technicalSkillsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.TechnicalSkillsControl MainResumeControl_technicalSkillsControl;
        
        /// <summary>
        /// MainResumeControl_projectsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ProjectsControl MainResumeControl_projectsControl;
        
        /// <summary>
        /// MainResumeControl_educationControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.EducationControl MainResumeControl_educationControl;
        
        /// <summary>
        /// MainResumeControl_referencesControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ReferencesControl MainResumeControl_referencesControl;
    }
}
