﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EducationControl.cs
// File that represents the user interface for the educational information details

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class EducationControl : System.Web.UI.UserControl
    {
        
        #region Custom Event Handler and Delegate                              

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;

        #endregion Custom Event Handler and Delegate

        #region Event Handlers                                                 
        
        protected void Page_Load(object sender, EventArgs e)
        {
            EducationControl_plusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
                      EducationControl_controlsDiv.ClientID + "','" + EducationControl_plusSpan.ClientID + "','" +
                      EducationControl_minusSpan.ClientID + "','" + EducationControl_hiddenField.ClientID + "')");
            EducationControl_minusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
               EducationControl_controlsDiv.ClientID + "','" + EducationControl_plusSpan.ClientID + "','" +
               EducationControl_minusSpan.ClientID + "','" + EducationControl_hiddenField.ClientID + "')");

            // 
            if (!Utility.IsNullOrEmpty(EducationControl_hiddenField.Value) &&
                EducationControl_hiddenField.Value == "Y")
            {
                EducationControl_controlsDiv.Style["display"] = "none";
                EducationControl_plusSpan.Style["display"] = "block";
                EducationControl_minusSpan.Style["display"] = "none";

            }
            else
            {
                EducationControl_controlsDiv.Style["display"] = "block";
                EducationControl_plusSpan.Style["display"] = "none";
                EducationControl_minusSpan.Style["display"] = "block";
            }

        }

        protected void EducationControl_addRowLinkButton_Click(object sender, EventArgs e)
        {
            SetViewState();
            EducationControl_listView.DataSource = null;
            if (ViewState["dataSource"] != null)
            {
                this.dataSource = (List<Education>)ViewState["dataSource"];
            }
            else
            {
                this.dataSource = new List<Education>();
            }
            this.dataSource.Add(new Education());
            ViewState["dataSource"] = this.dataSource;
            EducationControl_listView.DataSource = this.dataSource;
            EducationControl_listView.DataBind();
            SetDate();

        }

        protected void EducationControl_deleteButton_Click(object sender, EventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((Button)(sender)).Parent;
            this.dataSource = (List<Education>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            EducationControl_listView.DataSource = dataSource;
            EducationControl_listView.DataBind();
            ViewState["dataSource"] = dataSource;
            SetDate();
        }

        protected void DeleteRowImagebutton_Click(object sender, EventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((ImageButton)(sender)).Parent;
            this.dataSource = (List<Education>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            EducationControl_listView.DataSource = dataSource;
            EducationControl_listView.DataBind();
            ViewState["dataSource"] = dataSource;
            SetDate();
        }

        protected void EducationControl_okPopUpClick(object sender, EventArgs e)
        {
            int intDeletedRowIndex = Convert.ToInt32(EducationControl_deletedRowHiddenField.Value);
            if (intDeletedRowIndex != 0)
            {               
                this.dataSource = (List<Education>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intDeletedRowIndex-1);               
            }
            else
            {
                int intLastRecord = EducationControl_listView.Items.Count-1;
                this.dataSource = (List<Education>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intLastRecord);               
            }
            EducationControl_listView.DataSource = dataSource;
            EducationControl_listView.DataBind();
            SetViewState();
            SetDate();       
            
            // Fire the message event
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("Education detail deleted successfully", MessageType.Success));
        }
          
        protected void EducationControl_listView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ImageButton deleteRowImagebutton = ((ImageButton)e.Item.FindControl
                    ("EducationControl_deleteImageButton"));
            //deleteRowImagebutton.Attributes.Add("onClick", "javascript:return RemoveEducation();");
            ((Label)e.Item.FindControl("EducationControl_rowNumberLabel")).Text = ((e.Item as ListViewDataItem).DisplayIndex + 1).ToString();
        }
               
        protected void EducationControl_listView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            SetViewState();
            SetDate();
            if (e.CommandName == "deleteEducation")
            {
                ImageButton deleteRowImagebutton = (ImageButton)e.Item.FindControl("EducationControl_deleteImageButton");
                TextBox txtRowIndex = (TextBox)e.Item.FindControl("EducationControl_deleteRowIndex");
                EducationControl_deletedRowHiddenField.Value = txtRowIndex.Text;
                EducationControl_deleteModalPopupExtender.Show();
            }
        }

        #endregion Event Handlers
            
        #region Private Variables                                              
                                            
        private List<Education> dataSource;

        #endregion

        #region Public Methods                                                 
                                               
        public string SetLocation(Location pLocation)
        {
            string location = "";
            if (pLocation != null)
            {
                location = (pLocation.City != null) ? pLocation.City : "";
                location += (pLocation.State != null) ? "," + pLocation.State : "";
                location += (pLocation.Country != null) ? "," + pLocation.Country : "";
                location = location.StartsWith(",") ? location.Substring(1) : location;
            }
            return location;
        }

        public List<Education> DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                EducationControl_listView.DataSource = value;
                EducationControl_listView.DataBind();
                ViewState["dataSource"] =  value;
            }
            get
            {
                if (ViewState["dataSource"] != null)
                    return (List<Education>)ViewState["dataSource"];
                else
                    return null;
            }
        }

        #endregion Public Methods

        #region Protected Methods                                              

        protected void SetViewState()
        {

            List<Education> oEduList = null;
            int intEduCnt = 0;
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                if (oEduList == null)
                {
                    oEduList = new List<Education>();
                }
                          

                Education oEducation = new Education();
                Location oLocation=new Location();
                TextBox txtEduName = (TextBox)item.FindControl("EducationControl_nameTextBox");
                TextBox txtEduLocation = (TextBox)item.FindControl("EducationControl_locationTextBox");
                TextBox txtEduDegree = (TextBox)item.FindControl("EducationControl_degreeTextBox");
                TextBox txtEduSpec = (TextBox)item.FindControl("EducationControl_specializationTextBox");
                TextBox txtEduGPA = (TextBox)item.FindControl("EducationControl_gpaTextBox");
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                TextBox txtRowIndex = (TextBox)item.FindControl("EducationControl_deleteRowIndex");
                intEduCnt = intEduCnt + 1;              
                oEducation.EducationId = intEduCnt;
                oEducation.DegreeName = txtEduDegree.Text.Trim();
                oLocation.City=txtEduLocation.Text.Trim();
                oEducation.SchoolLocation = oLocation;
                oEducation.Specialization = txtEduSpec.Text.Trim();
                oEducation.SchoolName = txtEduName.Text.Trim();
                DateTime DtEduGradDate;
                if ((DateTime.TryParse(txtEduGradDate.Text, out DtEduGradDate)))
                {
                    oEducation.GraduationDate = DtEduGradDate;
                }               
                oEducation.GPA=txtEduGPA.Text.Trim();
                oEduList.Add(oEducation);
            }
            EducationControl_listView.DataSource = oEduList;
            EducationControl_listView.DataBind();
            ViewState["dataSource"] = oEduList;
        }
        protected void SetDate()
        {
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                if (txtEduGradDate.Text == "01/01/0001" || txtEduGradDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtEduGradDate.Text = "";
                }
            }
        }

        #endregion Protected Methods
    }
}