﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// GroupAnalysisSubjectChartControl.cs
// Control that represents the user interface and assign data source for the 
// GroupAnalysisSubjectChartControl

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class GroupAnalysisSubjectChartControl : UserControl, IWidgetControl
    {
        string testkey = string.Empty;
        List<DropDownItem> dropDownItemList = null;

        #region Event Handlers 
                                               
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                GroupAnalysisSubjectChartControl_testKeyhiddenField.Value = selectedProperty[0];
                GroupAnalysisSubjectChartControl_absoluteScoreRadioButton.Checked = selectedProperty[2] == "0" ? false : true;
                GroupAnalysisSubjectChartControl_relativeScoreRadioButton.Checked = selectedProperty[2] == "1" ? false : true;
                GroupAnalysisSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[1]);
                GroupAnalysisSubjectChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on radio button 
        ///  button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void GroupAnalysisSubjectChartControl_absoluteScoreRadioButton_CheckedChanged
           (object sender, EventArgs e)
        {
            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }

        /// <summary>
        /// Hanlder method that will be called on radio button 
        ///  button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void GroupAnalysisCategoryChartControl_absoluteScoreRadioButton_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                LoadChartDetails(ViewState["instance"] as WidgetInstance);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GroupAnalysisSubjectChartControl_selectProperty(object sender, EventArgs e)
        {
            GroupAnalysisSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(false);

            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }

        /// <summary>
        /// Hanlder method that will be called when click the link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GroupAnalysisSubjectChartControl_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Hanlder method that will be called on multi select link button added
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void GroupAnalysisSubjectChartControl_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Event Handlers

        #region Private Method                                                 

        /// <summary>
        /// Represents the method to get the chart details from the database
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            DataTable multiSeriesDataTable = new ReportBLManager().
                GetGroupAnalysisSubjectDetails(candidateReportDetail);
            GroupAnalysisSubjectChartControl_testKeyhiddenField.Value = candidateReportDetail.TestKey;

            if (multiSeriesDataTable == null)
                return;

            var category = (from r in multiSeriesDataTable.AsEnumerable()
                            select new DropDownItem(r["Name"].ToString().Trim()
                               , r["SUBJECT_ID"].ToString())).Distinct();

            GroupAnalysisSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = category.ToList();

            //Select all the check box in the check box list
            GroupAnalysisSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

            Session["SubjectChartDataSource"] = multiSeriesDataTable;

            LoadChart(instance,"1");
        }

        /// <summary>
        /// Represents the method to load the chart 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChart(WidgetInstance instance,string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            DataTable subjectChartDataTable = null;

            //Use string builder and string for naming the chart
            //This is used to store all the selected category
            StringBuilder selectedSubjectId = new StringBuilder();
            string selectedSubjectIds = "";

            //Take the test key from the hidden field . this is also used for 
            //naming the chart
            testkey = GroupAnalysisSubjectChartControl_testKeyhiddenField.Value;
            selectedPrintProperty.Append(testkey + "|");
            //Assign values for the chart control 
            MultipleSeriesChartData subjectChartDataSource = new MultipleSeriesChartData();

            //Get the datatable from the session 
            if ((Session["SubjectChartDataSource"]) != null)
            {
                subjectChartDataTable = Session["SubjectChartDataSource"]
                    as DataTable;
            }

            DataTable selectedChartData = subjectChartDataTable.Clone();

            List<DropDownItem> selectedList = GroupAnalysisSubjectChartControl_widgetMultiSelectControl.SelectedItems();

            //categoryList = WidgetMultiSelectControl.SelectedProperties;

            if (selectedList != null && selectedList.Count != 0)
            {
                foreach (DropDownItem subject in selectedList)
                {
                    if ((Session["SubjectChartDataSource"]) != null)
                    {
                        DataRow[] datarow = (Session["SubjectChartDataSource"] as DataTable).
                            Select("SUBJECT_ID='" + int.Parse(subject.ValueText.ToString().Trim()) + "'");

                        //DataRow dr = selectedChartData.NewRow();                       
                        //dr.ItemArray = datarow;
                        selectedChartData.Rows.Add(datarow[0].ItemArray);

                        //Append the selected category id to the string 
                        selectedSubjectId.Append(subject.ValueText);
                        selectedSubjectId.Append("-"); 
                    }
                }
            }
            else
            {
                subjectChartDataTable = new DataTable();
            }

                dropDownItemList = new List<DropDownItem>();

                //Remove the last - 
                if (selectedSubjectId.Length > 0)
                    selectedSubjectIds = selectedSubjectId.ToString().TrimEnd('-');

                selectedPrintProperty.Append(selectedSubjectId.Replace('-', ',') + "|");

                //If absolute score is selected remove the relative score details
                if (GroupAnalysisSubjectChartControl_absoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    selectedChartData.Columns.Remove("LT50PERCENTILE_RELATIVE");
                    selectedChartData.Columns.Remove("GT50PERCENTILE_RELATIVE");
                    selectedChartData.Columns.Remove("GT75PERCENTILE_RELATIVE");

                    //Replace the column name as < 50 percentile
                    selectedChartData.Columns["LT50Percentile"].ColumnName = "< 50 Percentile";

                    //Replace the column name as 50-75 percentile
                    selectedChartData.Columns["GT50Percentile"].ColumnName = "50 - 75 Percentile";

                    //Replace the column name as > 75 percentile
                    selectedChartData.Columns["GT75Percentile"].ColumnName = "> 75 Percentile";

                    dropDownItemList.Add(new DropDownItem("Title", "Group Analysis Subject Score"));
                    dropDownItemList.Add(new DropDownItem("Categories", Constants.ChartConstants.GROUP_REPORT_SUBJECT_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.GROUP_REPORT_SUBJECT_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    //else remove the absolute score details
                    selectedChartData.Columns.Remove("LT50PERCENTILE");
                    selectedChartData.Columns.Remove("GT50Percentile");
                    selectedChartData.Columns.Remove("GT75Percentile");


                    //Rename the relative score details
                    //Replace the column name as < 50 percentile
                    selectedChartData.Columns["LT50PERCENTILE_RELATIVE"].ColumnName = "< 50 Percentile";

                    //Replace the column name as 50-75 percentile
                    selectedChartData.Columns["GT50PERCENTILE_RELATIVE"].ColumnName = "50 - 75 Percentile";

                    //Replace the column name as > 75 percentile
                    selectedChartData.Columns["GT75PERCENTILE_RELATIVE"].ColumnName = "> 75 Percentile";
                    dropDownItemList.Add(new DropDownItem("Title", "Group Analysis Relative Subject Score"));
                    dropDownItemList.Add(new DropDownItem("Categories", Constants.ChartConstants.GROUP_REPORT_SUBJECT_RELATIVE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.GROUP_REPORT_SUBJECT_RELATIVE + "-" + testkey + "-" + selectedSubjectIds;
                }

                subjectChartDataTable = selectedChartData;
            

            if (subjectChartDataTable == null)
                return;

            //Remove the cate key column from the displaying table
            if (subjectChartDataTable.Columns.Contains("SUBJECT_ID"))
            {
                subjectChartDataTable.Columns.Remove("SUBJECT_ID");
            }

            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                subjectChartDataSource.IsShowLabel = true;
            }

            subjectChartDataSource.ChartType = SeriesChartType.Column;

            subjectChartDataSource.ChartLength = 300;

            subjectChartDataSource.ChartWidth = 350;

            subjectChartDataSource.IsDisplayAxisTitle = true;

            subjectChartDataSource.IsDisplayChartTitle = false;

            subjectChartDataSource.XAxisTitle = "Subjects";

            subjectChartDataSource.YAxisTitle = "No Of Candidates";

            subjectChartDataSource.ChartTitle = "Candidate's Score Amongst Subjects";

            subjectChartDataSource.IsDisplayChartTitle = false;

            subjectChartDataSource.IsFullNumberYAxis = true;

            subjectChartDataSource.ComparisonReportDataTable = subjectChartDataTable;

            subjectChartDataSource.IsComparisonReport = true;

            GroupAnalysisSubjectChartControl_multiSeriesChart.MultipleChartDataSource =
              subjectChartDataSource;
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        #endregion Private Method

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>   
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}