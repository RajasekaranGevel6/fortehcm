﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionSettings.cs
// File that represents the user interface for question settings page.
// This will helps to view and set the question settings such as categories, 
// subjects and test areas.

#endregion


#region Directives
using System;
using System.Web;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;


#endregion; Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// the question settings page. This page helps to view and set the 
    /// question statistics such as the  time take , question ,
    /// author of the question
    /// </summary>
    public partial class QuestionStatisticsControl : UserControl
    {

        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }

            set { ViewState["sortDirection"] = value; }
        }
        public List<QuestionStatisticsDetail> QuestionStatisticsDataSoure
        {
            set;
            get;
        }

        protected string TrimQuestion(string question)
        {
            return question.Length > 50 ? question.Substring(0, 50) + ".." : question;
        }


        protected void QuestionStatisticsControl_questionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton QuestionStatisticsControl_questionIdLinkButton =
                    (LinkButton)e.Row.FindControl("QuestionStatisticsControl_questionGridLinkButton");

                LinkButton QuestionStatisticsControl_questionHiddenIdLinkButton =
                 (LinkButton)e.Row.FindControl("QuestionStatisticsControl_questionIdGridLinkButton");

                HtmlContainerControl QuestionStatisticsControl_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("QuestionStatisticsControl_detailsDiv");

                HtmlAnchor QuestionStatisticsControl_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("QuestionStatisticsControl_focusDownLink");

                QuestionStatisticsControl_questionIdLinkButton.Attributes.Add
                    ("onclick", "return TestHideDetails('" +
                QuestionStatisticsControl_detailsDiv.ClientID + "','" +
                QuestionStatisticsControl_focusDownLink.ClientID + "')");

                Label QuestionStatisticsControl_questionLabel =
                    (Label)e.Row.FindControl("QuestionStatisticsControl_questionLabel");

                RadioButtonList QuestionStatisticsControl_answerRadioButtonList =
                    (RadioButtonList)e.Row.FindControl("QuestionStatisticsControl_answerRadioButtonList");


                PlaceHolder SearchQuestion_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("QuestionStatisticsControl_answerChoicesPlaceHolder");
                SearchQuestion_answerChoicesPlaceHolder.Controls.Add
                                    (GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(QuestionStatisticsControl_questionHiddenIdLinkButton.Text.ToString())));

                e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
            }
        }
        protected void QuestionStatisticsControl_questionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["SortExpression"].ToString() == e.SortExpression)
            {
                GridViewSortDirection = GridViewSortDirection == SortDirection.Ascending ?
                    SortDirection.Descending : SortDirection.Ascending;
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
            }
            ViewState["SortExpression"] = e.SortExpression;

            SortGridView(e.SortExpression, GridViewSortDirection.ToString());

        }
        private void SortGridView(string sortExpression, string direction)
        {
            if (Cache["SearchDataTables"] == null)
                return;


            List<QuestionStatisticsDetail> questionStatisticsDetails =
               Cache["SearchDataTables"] as List<QuestionStatisticsDetail>;

            List<QuestionStatisticsDetail> sortedQuestionStatisticsDetails =
                new List<QuestionStatisticsDetail>();


            switch (sortExpression)
            {
                case "LocalTime":
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.AverageTimeTakenWithinTest ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.AverageTimeTakenWithinTest descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
                case "GlobalTime":
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.AverageTimeTakenAcrossTest ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.AverageTimeTakenAcrossTest descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
                case "Ratio":
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.Ratio ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.Ratio descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
                case "Author":
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.Author ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.Author descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;

                default:
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.Question ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.Question descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
            }
            //dv.Sort = sortExpression + direction;
            QuestionStatisticsControl_questionGridView.DataSource = sortedQuestionStatisticsDetails;
            QuestionStatisticsControl_questionGridView.DataBind();

        }
        protected void QuestionStatisticsControl_questionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = GetSortColumnIndex();
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in QuestionStatisticsControl_questionGridView.Columns)
            {
                if (field.SortExpression ==
                             (string)ViewState["SortExpression"])
                {
                    return QuestionStatisticsControl_questionGridView.Columns.IndexOf(field);
                }
            }
            return -1;
        }

        private void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            // Add the image to the appropriate header cell.
            headerRow.Cells[columnIndex].Controls.Add(sortImage);

        }

        private Table GetAnswerChoices(List<AnswerChoice> answerChoice)
        {
            return (new ControlUtility().GetAnswerChoices(answerChoice,false));
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Utility.IsNullOrEmpty(ViewState["SortExpression"]))
                ViewState["SortExpression"] = "Question";
            if (Utility.IsNullOrEmpty(GridViewSortDirection))
                GridViewSortDirection = SortDirection.Ascending;

            if (!IsPostBack)
            {
                QuestionStatisticsControl_questionGridView.DataSource = QuestionStatisticsDataSoure;
                QuestionStatisticsControl_questionGridView.DataBind();

                Cache["SearchDataTables"] = QuestionStatisticsDataSoure;    
            }       


        }
      
    }
}