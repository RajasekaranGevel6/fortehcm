﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionTypeControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.QuestionTypeControl" %>
<div>
    <div style="float: left; padding-top: 8px">
        <asp:Label ID="QuestionTypeControl_questionTypeLabel" runat="server" Text="Question Type"
            SkinID="sknLabelFieldHeaderText"></asp:Label>
    </div>
    <div style="float: left; padding-left: 4px; padding-top: 6px">
        <asp:DropDownList ID="QuestionTypeControl_questionTypeDropDownList" SkinID="sknQuestionTypeDropDown"
            runat="server" AutoPostBack="true" OnSelectedIndexChanged="QuestionTypeControl_questionTypeDropDownList_SelectedIndexChanged"
            EnableViewState="true">
            <asp:ListItem Text="Multiple Choice" Value="1"></asp:ListItem>
            <asp:ListItem Text="Open Text" Value="2"></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
