﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInterviewTestStatisticsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GeneralInterviewTestStatisticsControl" %>
<%@ Register Src="PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/SingleSeriesChartControl.ascx" TagName="chart"
    TagPrefix="uc1" %>

<script type="text/javascript" language="javascript">
   
</script>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div id="GeneralInterviewTestStatisticsControl_searchCriteriasDiv" runat="server" style="display: block;">
                <table cellpadding="0" cellspacing="3" width="100%">
                    <tr>
                        <td style="width: 50%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="grid_header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    Interview Summary
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_body_bg" style="width: 100%">
                                        <div style="height: 130px; overflow: auto;">
                                            <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                <tr>
                                                    <td style="width: 40%">
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_testAuthorHeadLabel" runat="server" Text="Interview Author"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_testAuthorLabel" runat="server" Text="Karthick"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_highScoreHeadLabel" runat="server" Text="Highest Score"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_highScoreLabel" runat="server" Text="10"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_noOfQuestionHeadLabel" runat="server"
                                                            Text="Number Of Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_noOfQuestionLabel" runat="server" Text="25"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_lowScoreHeadLabel" runat="server" Text="Lowest Score"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_lowScoreLabel" runat="server" Text="8"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_noOfCandidateHeadLabel" runat="server"
                                                            Text="Number Of Candidates" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_noOfCandidateLabel" runat="server" Text="25"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_meanScoreHeadLabel" runat="server" Text="Mean Score"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_meanScoreLabel" runat="server" Text="10"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_avgTimeHeadLabel" runat="server" Text="Average Time Taken By Candidates"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_avgTimeLabel" runat="server" Text="00:01:25"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_scoreSDHeadLabel" runat="server" Text="Standard Deviation"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_scoreSDLabel" runat="server" Text="10"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_scoreRangeHeadLabel" runat="server" Text="Score Range"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="GeneralInterviewTestStatisticsControl_scoreRangeLabel" runat="server" Text="10"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td colspan="2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 50%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="grid_header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    Categories / Subjects
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_body_bg">
                                        <div style="height: 130px; width: 470px; overflow: auto;">
                                            <asp:GridView ID="GeneralInterviewTestStatisticsControl_categoryGridview" runat="server">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Category" DataField="CategoryName" />
                                                    <asp:BoundField HeaderText="Subject" DataField="SubjectName" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_2" colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="grid_header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    Category Statistics
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_body_bg">
                                        <div style="height: 150px; overflow: auto;">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td align="left" valign="middle">
                                                        <uc1:chart ID="GeneralTestStatistics_categoryStatisticsChartControl" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 50%">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="grid_header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    Subject Statistics
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_body_bg">
                                        <div style="height: 150px; overflow: auto;">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td align="left">
                                                        <uc1:chart ID="GeneralInterviewTestStatisticsControl_subjectChartStatisticsChartControl" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_2" colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="grid_header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    Interview Area Statistics
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_body_bg">
                                        <div style="height: 150px; overflow: auto;">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td align="left">
                                                        <uc1:chart ID="GeneralInterviewTestStatisticsControl_testAreaStatistics" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 50%">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="grid_header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    Complexity Statistics
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="panel_body_bg">
                                        <div style="height: 150px; overflow: auto;">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td align="left">
                                                        <uc1:chart ID="GeneralInterviewTestStatisticsControl_complexityStatisticsChart" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
