﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportHistogramChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ReportHistogramChartControl" %>
<%@ Register Src="~/CommonControls/HistogramChartControl.ascx" TagName="HistogramChart"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
 <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
<table>
    <tr>
        <td>
            <asp:UpdatePanel ID="ReportHistogramChartControl_updatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ReportHistogramChartControl_selectScoreLabel" runat="server" Text="Score Type"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="ReportHistogramChartControl_selectAbsoluteScoreRadioButton"
                                                runat="server" Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="True"
                                                OnCheckedChanged="ReportHistogramChartControl_selectAbsoluteScore_CheckedChanged" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="ReportHistogramChartControl_selectRelativeScoreRadioButton"
                                                runat="server" Text=" Relative Score" GroupName="1" AutoPostBack="True" OnCheckedChanged="ReportHistogramChartControl_selectAbsoluteScore_CheckedChanged" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:HistogramChart ID="ReportHistogramChartControl_histogramChart" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
