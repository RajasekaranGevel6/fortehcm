﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReflectionComboItem;
using System.Text;
using System.Collections;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

namespace Forte.HCM.UI.CommonControls
{
    public partial class MultiSelectControl : System.Web.UI.UserControl
    {
        public delegate void SelectedIndexChanged(object sender, EventArgs e);
        //  public delegate void Click(object sender, EventArgs e);
        public event SelectedIndexChanged selectedIndexChanged;
        public event SelectedIndexChanged Click;
        public event SelectedIndexChanged cancelClick;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public ClientInformation DepartmentDataSource
        {
            set
            {
                SelectedProperties = "";
                if (value ==null)
                    return;
                if(value.Departments == null)
                    return;
                MultiSelectControl_propertyCheckBoxList.DataSource = value.Departments;
                MultiSelectControl_propertyCheckBoxList.DataValueField = "DepartmentID";
                MultiSelectControl_propertyCheckBoxList.DataTextField = "DepartmentName";
                MultiSelectControl_propertyCheckBoxList.DataBind();
                if (value.sellectedDepartmentID > 0)
                {
                    for (int i = 0; i < value.Departments.Count; i++)
                    {

                        if (MultiSelectControl_propertyCheckBoxList.Items[i].Value == value.sellectedDepartmentID.ToString())
                        {
                            MultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                        }
                    }
                }

            }
        }

        public List<Department> DepartmentListDataSource
        {
            set
            {
                if (value == null)
                    return;
                MultiSelectControl_propertyCheckBoxList.DataSource = value;
                MultiSelectControl_propertyCheckBoxList.DataValueField = "DepartmentID";
                MultiSelectControl_propertyCheckBoxList.DataTextField = "DepartmentName";
                MultiSelectControl_propertyCheckBoxList.DataBind();

                for (int i = 0; i < value.Count; i++)
                {
                    if ( value[i].DepartmentIsSelected=="Y")
                    {
                        MultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                    }
                }
            }
        }

        public void WidgetTypePropertyAllSelected(bool selectAll)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < MultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            {
                if (selectAll)
                {
                    MultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                    stringBuilder.Append(MultiSelectControl_propertyCheckBoxList.Items[i].Text);
                    stringBuilder.Append(",");
                }
                else
                {
                    if (MultiSelectControl_propertyCheckBoxList.Items[i].Selected)
                    {
                        MultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                        stringBuilder.Append(MultiSelectControl_propertyCheckBoxList.Items[i].Text);
                        stringBuilder.Append(",");
                    }
                }
            }
            MultiSelectControl_propertyTextBox.Text = stringBuilder.ToString().TrimEnd(',');
            MultiSelectControl_propertyTextBox.ToolTip = stringBuilder.ToString().TrimEnd(',');
        }

        public void WidgetTypePropertyAllSelected(string selectedId)
        {
            ArrayList list = new ArrayList();
            list.AddRange(selectedId.Split(new char[] { ',' }));

            for (int i = 0; i < MultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            {
                foreach (string s in list)
                {
                    if (MultiSelectControl_propertyCheckBoxList.Items[i].Value == s.ToString())
                        MultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                }
            }
        }
        public string SelectedProperties
        {
            set
            {
                MultiSelectControl_propertyCheckBoxList.Items.Clear();
                MultiSelectControl_propertyTextBox.ToolTip = "";
                MultiSelectControl_propertyTextBox.Text = "";
                if (value == null)
                    return;
                MultiSelectControl_propertyTextBox.Text = value;
                MultiSelectControl_propertyTextBox.ToolTip = value;
            }
            get
            {
                return MultiSelectControl_propertyTextBox.Text;
            }
        }

        public string SelledctedIds
        {
            get
            {
                string _SelledctedIds = "";
                for (int i = 0; i < MultiSelectControl_propertyCheckBoxList.Items.Count; i++)
                {
                    if (MultiSelectControl_propertyCheckBoxList.Items[i].Selected)
                    {
                        _SelledctedIds = _SelledctedIds + MultiSelectControl_propertyCheckBoxList.Items[i].Value + ",";
                    }
                }
                return _SelledctedIds.TrimEnd(',');
            }
        }

        protected void MultiSelectControl_propertyCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selectedIndexChanged != null)
                this.selectedIndexChanged(sender, e);
        }

        protected void GroupAnalysisCategoryChartControl_selectAllLinkButton_Click(object sender, EventArgs e)
        {
            WidgetTypePropertyAllSelected(true);
            if (Click != null)
                this.Click(sender, e);
        }

        protected void GroupAnalysisCategoryChartControl_clearAllLinkButton_Click(object sender, EventArgs e)
        {
            MultiSelectControl_propertyCheckBoxList.ClearSelection();
            MultiSelectControl_propertyTextBox.Text = "";
            MultiSelectControl_propertyTextBox.ToolTip = "";
            if (cancelClick != null)
                this.cancelClick(sender, e);
        }
        public void StateMaintainPaging()
        {
            if (MultiSelectControl_propertyTextBox.Text.Trim().Length == 0)
                return;

            string[] selectedItem;
            if (MultiSelectControl_propertyTextBox.Text.Trim().Contains(','))
                selectedItem = MultiSelectControl_propertyTextBox.Text.Split(',');
            else
            {
                selectedItem = MultiSelectControl_propertyTextBox.Text.Split(',');
            }

            // From the last selected items identity and set the selected subject.
            for (int i = 0; i < MultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            {
                foreach (string selectedItemValue in selectedItem)
                {
                    if (MultiSelectControl_propertyCheckBoxList.Items[i].Text == selectedItemValue)
                    {
                        MultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                    }
                }

            }

            //for (int i = 0; i < MultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            //{
            //    foreach(selectedItem in MultiSelectControl_propertyCheckBoxList.Items[i].Text)
            // MultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
            //}

        }

        public List<Department> SelectedItems()
        {
            List<Department> selectedList = new List<Department>();
            for (int i = 0; i < MultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            {
                if (MultiSelectControl_propertyCheckBoxList.Items[i].Selected)
                {
                    Department department = new Department();
                    department.DepartmentID = int.Parse(MultiSelectControl_propertyCheckBoxList.Items[i].Value);
                    department.DepartmentName = MultiSelectControl_propertyCheckBoxList.Items[i].Text;
                    selectedList.Add(department);
                }
            }
            return selectedList;
        }
    }
}