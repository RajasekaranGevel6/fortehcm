﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewTestDetailsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ViewTestDetailsControl" %>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td class="">
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                                        <tr>
                                            <td colspan="5" class="header_bg" valign="middle">
                                                <asp:Literal ID="TestDetailsControl_testDetailsMessageLiteral" runat="server" Text="Test Details"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tab_body_bg">
                                                <table border="0" cellspacing="3" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testIdLabel" Text="Test ID" runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testDetailsTestIdLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testNameLabel" Text="Test Name" runat="server"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testNameTextBox" SkinID="sknLabelFieldText" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testDescriptionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Test Description"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <div style="width: 100%;word-wrap: break-word;white-space:normal;" class="label_multi_field_text">
                                                                <asp:Label ID="TestDetailsControl_testDescriptionTextBox" SkinID="sknLabelFieldMultiText"
                                                                    runat="server"></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <asp:Label ID="TestDetailsControl_systemRecommendedTimeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="System Recommended Time"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="5">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="TestDetailsControl_systemRecommendedTimeTextBox" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="TestDetailsControl_recommendedTimeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Recommended Completion Time"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="TestDetailsControl_recommendedTimeLabelTextBox" SkinID="sknLabelFieldText"
                                                                            runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_positionProfileLabel" Text="Position Profile" runat="server"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_positionProfileTextBox" SkinID="sknLabelFieldText" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr runat="server" id="ViewTestDetailsControl_certificationRow" visible="false">
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                            <tr>
                                <td class="header_bg">
                                    <asp:Label ID="CreateManualTest_CertificationLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Certification"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="width: 100%" class="grid_body_bg">
                                    <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_maxTotalScoreLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                    Text="Minimum total score required to qualify for certification (in %)"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_minTotalScoreLabelsTextBox" SkinID="sknLabelFieldText"
                                                    runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 45%;">
                                                <asp:Label ID="TestDetailsControl_MaximumtimepermissibleScoreLabel" runat="server"
                                                    SkinID="sknLabelFieldHeaderText" Text="Maximum time permissible to complete the test to qualify for certification"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_MaximumtimepermissibleScoreTextBox" SkinID="sknLabelFieldText"
                                                    runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_numberofpermissibleLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                    Text="Number of permissible retakes per candidate"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_numberofpermissibleTextBox" SkinID="sknLabelFieldText"
                                                    runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_timeperiodrequiredLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                    Text="Time period required to elapse between retakes(In Days)"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_timeperiodrequiredTextBox" SkinID="sknLabelFieldText"
                                                    runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_certificateformatLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                    Text="Certificate format"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_certificateformatTextBox" SkinID="sknLabelFieldText"
                                                    runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_periodofcertificationvalidityLabel" runat="server"
                                                    SkinID="sknLabelFieldHeaderText" Text="Period of certification validity"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="TestDetailsControl_periodofcertificationvalidityValueLabel" runat="server"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
