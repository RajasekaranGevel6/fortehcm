﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReflectionComboItem;
using System.Text;
using System.Collections;

namespace Forte.HCM.UI.CommonControls
{
    public partial class WidgetMultiSelectControl : System.Web.UI.UserControl
    {
        public delegate void SelectedIndexChanged(object sender, EventArgs e);
      //  public delegate void Click(object sender, EventArgs e);
        public event SelectedIndexChanged selectedIndexChanged;
        public event SelectedIndexChanged Click;
        public event SelectedIndexChanged cancelClick;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public List<DropDownItem> WidgetTypePropertyDataSource
        {
            set
            {
                if (value == null)
                    return;
                WidgetMultiSelectControl_propertyCheckBoxList.DataSource = value;
                WidgetMultiSelectControl_propertyCheckBoxList.DataValueField = "ValueText";
                WidgetMultiSelectControl_propertyCheckBoxList.DataTextField = "DisplayText";
                WidgetMultiSelectControl_propertyCheckBoxList.DataBind();
            }
        }
        public void WidgetTypePropertyAllSelected(bool selectAll)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < WidgetMultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            {
                if (selectAll)
                {
                    WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                    stringBuilder.Append(WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Text);
                    stringBuilder.Append(",");
                }
                else
                {
                    if (WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected)
                    {
                        WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                        stringBuilder.Append(WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Text);
                        stringBuilder.Append(",");
                    }
                }
            }
            WidgetMultiSelectControl_propertyTextBox.Text = stringBuilder.ToString().TrimEnd(',');
            WidgetMultiSelectControl_propertyTextBox.ToolTip = stringBuilder.ToString().TrimEnd(',');
        }

        public void WidgetTypePropertyAllSelected(string selectedId)
        {

           
            ArrayList list = new ArrayList();
            list.AddRange(selectedId.Split(new char[] { ',' }));

            for (int i = 0; i < WidgetMultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            {
                foreach (string s in list)
                {
                    if (WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Value==s.ToString())
                        WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
                }
            }
        }
        public string SelectedProperties
        {
            set
            {
                if (value == null)
                    return;
                WidgetMultiSelectControl_propertyTextBox.Text = value;
                WidgetMultiSelectControl_propertyTextBox.ToolTip = value;
            }
            get
            { 
                return WidgetMultiSelectControl_propertyTextBox.Text;
            }
        }

        protected void WidgetMultiSelectControl_propertyCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selectedIndexChanged != null)
                this.selectedIndexChanged(sender, e);
        }

        protected void GroupAnalysisCategoryChartControl_selectAllLinkButton_Click(object sender, EventArgs e)
        {
            WidgetTypePropertyAllSelected(true);
            if (Click != null)
                this.Click(sender, e);
        }

        protected void GroupAnalysisCategoryChartControl_clearAllLinkButton_Click(object sender, EventArgs e)
        { 
            WidgetMultiSelectControl_propertyCheckBoxList.ClearSelection();
            WidgetMultiSelectControl_propertyTextBox.Text = "";
            WidgetMultiSelectControl_propertyTextBox.ToolTip = "";
            if (cancelClick != null)
                this.cancelClick(sender, e);
        }
        public void StateMaintainPaging()
        {
            if (WidgetMultiSelectControl_propertyTextBox.Text.Trim().Length == 0)
                return;

            string[] selectedItem ;
            if (WidgetMultiSelectControl_propertyTextBox.Text.Trim().Contains(','))
                selectedItem = WidgetMultiSelectControl_propertyTextBox.Text.Split(',');
            else
            {
                selectedItem = WidgetMultiSelectControl_propertyTextBox.Text.Split(',');
            }

             // From the last selected items identity and set the selected subject.
            for (int i = 0; i < WidgetMultiSelectControl_propertyCheckBoxList.Items.Count; i++)
                {
                    foreach (string selectedItemValue in selectedItem)
                    {
                        if (WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Text == selectedItemValue)
                        {
                            WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected= true;
                        }
                    }

                }

            //for (int i = 0; i < WidgetMultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            //{
            //    foreach(selectedItem in WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Text)
            // WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected = true;
            //}

        }

        public List<DropDownItem> SelectedItems()
        {   
            List<DropDownItem> selectedList = new List<DropDownItem>();
            for (int i = 0; i < WidgetMultiSelectControl_propertyCheckBoxList.Items.Count; i++)
            {                
                if (WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected)
                {
                   DropDownItem dropdownItem = new DropDownItem();
                   dropdownItem.ValueText = WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Value;
                   dropdownItem.DisplayText = WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Text;
                   selectedList.Add(dropdownItem);
                }                
            }
            return selectedList;
        }

        public string SelledctedIds
        {
            get
            {
                string _SelledctedIds = "";
                for (int i = 0; i < WidgetMultiSelectControl_propertyCheckBoxList.Items.Count; i++)
                {
                    if (WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Selected)
                    {

                        _SelledctedIds = _SelledctedIds + WidgetMultiSelectControl_propertyCheckBoxList.Items[i].Value + ",";
                    }
                }
                return _SelledctedIds.TrimEnd(',');
            }
        }

    }
}