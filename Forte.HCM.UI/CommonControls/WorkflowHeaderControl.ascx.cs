﻿
#region Namespace

using System;
using System.Web;
using System.Web.Security;
using System.Collections.Generic;

using Forte.HCM.BL;
using System.Web.UI;
using System.Configuration;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.Utilities;


using Resources;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class WorkflowHeaderControl : UserControl
    {
        #region Variables

        protected string homeUrl = string.Empty;
        protected string landingUrl = string.Empty;
        public string userOptionsUrl = string.Empty;
        protected string helpUrl = string.Empty;
        protected string resumeRepositoryUrl = string.Empty;
        protected string customerAdminUrl = string.Empty;
        protected string siteAdminUrl = string.Empty;
        protected bool isSiteAdmin = false;

        #endregion Variables

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 3)
                {
                    // Compose home url.
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Default.aspx";

                    // Compose dashboard url.
                    landingUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Landing.aspx";

                    // Compose user options url.
                    userOptionsUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Settings/UserOptions.aspx";

                    // Compose help url.
                    helpUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Help.aspx";

                    // Compose resume repository url.
                    resumeRepositoryUrl = uri[0] + "/" + uri[1] + "/" + uri[2] +  "/ResumeRepositoryHome.aspx";

                    // Compose customer admin url.
                    customerAdminUrl = uri[0] + "/" + uri[1] + "/" + uri[2] +  "/CustomerAdminHome.aspx";

                    // Compose site admin url.
                    siteAdminUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/SiteAdminHome.aspx";
                }

                string PageName = Request.Url.AbsoluteUri.Split('/')[Request.Url.AbsoluteUri.Split('/').Length - 1];
                if (PageName.Contains("?"))
                    PageName = PageName.Substring(0, PageName.IndexOf('?'));
                if (PageName == "Default.aspx")
                    SubscriptionMaster_menu.Visible = true;
                else
                    SubscriptionMaster_menu.Visible = false;

                // TODO - This needs to reviewed.
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    WorkflowHeaderControl_changePasswordLinkButton.Visible = true;
                }
                else
                {
                    // Hide the options menu.
                    if (SubscriptionMaster_menu.Items.Count > 5)
                        SubscriptionMaster_menu.Items.RemoveAt(5);
                    WorkflowHeaderControl_changePasswordLinkButton.Visible = false;
                }

                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                    isSiteAdmin = (userDetail.UserType == UserType.SiteAdmin ? true : false);
                }

                // Check if login is requested from web site.
                if (!Utility.IsNullOrEmpty(Request.QueryString["sitesessionkey"]))
                    AuthenticateSessionKey();

                // Retrieve cookies.
                RetrieveCookies();

                WorkflowHeaderControl_flushDataLinkButton.Visible = isSiteAdmin;

                if (IsPostBack)
                    return;

                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                    WorkflowHeaderControl_loggedInTable.Visible = true;
                    WorkflowHeaderControl_loginTable.Visible = false;
                    WorkflowHeaderControl_signUpImage.Visible = false;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    WorkflowHeaderControl_loginUserNameLabel.Text = userDetail.FirstName;
                    WorkflowHeaderControl_lastLoginDateTimeLabel.Text = userDetail.LastLogin.GetDateTimeFormats()[16];
                    isSiteAdmin = (userDetail.UserType == UserType.SiteAdmin ? true : false);
                }
                else
                {
                    WorkflowHeaderControl_loggedInTable.Visible = false;
                    WorkflowHeaderControl_loginTable.Visible = true;
                    WorkflowHeaderControl_signUpImage.Visible = true;
                }

                //if (Session.IsNewSession)
                //{

                //    Session["USER_DETAIL"] = null;
                //    // Clear cookies.
                //    try
                //    {
                //        //Response.Cookies["FORTEHCM_USER_DETAILS"].Expires.AddDays(-1);
                //        //Request.Cookies.Remove("FORTEHCM_USER_DETAILS");
                //        //Response.Cookies.Remove("FORTEHCM_USER_DETAILS");
                //    }
                //    catch (Exception exp)
                //    {
                //        Logger.ExceptionLog(exp);
                //    }

                //    Session.Abandon();
                //    WorkflowHeaderControl_loginUserNameLabel.Text = "Guest";
                //    FormsAuthentication.SignOut();
                //    Response.Redirect("~/Home.aspx?a=1", false);

                //}
                //else
                //{
                //    Session["IsNewSession"] = "Old session";
                //    //  Response.Write(Session["IsNewSession"].ToString());
                //}


            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void WorkflowHeaderControl_flushDataLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                new CommonBLManager().FlushData();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void WorkflowHeaderControl_customerAdminTD_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(customerAdminUrl);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void WorkflowHeaderControl_siteAdminTD_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(siteAdminUrl);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void WorkflowHeaderControl_logoutButton_Click(object sender, EventArgs e)
        {
            Session["USER_DETAIL"] = null;

            // Clear cookies.
            try
            {
                Response.Cookies["FORTEHCM_USER_DETAILS"].Expires.AddDays(-1);
                //Request.Cookies.Remove("FORTEHCM_USER_DETAILS");
                //Response.Cookies.Remove("FORTEHCM_USER_DETAILS");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }

            Session.Abandon();
            WorkflowHeaderControl_loginUserNameLabel.Text = "Guest";
            FormsAuthentication.SignOut();
            //Response.Redirect("~/Home.aspx?a=1", false);
            // FormsAuthentication.RedirectToLoginPage();
            // Response.Redirect(ConfigurationManager.AppSettings["SITE_URL"], false);
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if (uri.Length > 4)
            {
                // Compose home url.

                Response.Redirect(uri[0] + "/" + uri[1] + ConfigurationManager.AppSettings["SITE_URL"], false);
            }
        }

        protected void WorkflowHeaderControl_signUpImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(@"~/Subscription/SubscriptionType.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void WorkflowHeaderControl_goImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Try login.
                TryLogin();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Public Methods

        public List<string> BreadCrumbText
        {
            set
            {
                List<string> breadCrumbList = (List<string>)value;

                if (breadCrumbList != null)
                {
                    foreach (string breadCrumbText in breadCrumbList)
                    {
                    }
                }
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method that try login to the system using the given user name and 
        /// password.
        /// </summary>
        private void TryLogin()
        {
            if (this.IsValidData())
            {
                 UserDetail userDetails = AuthenticateUserDetails(WorkflowHeaderControl_userNametextbox.Text.Trim(),
                    WorkflowHeaderControl_passwordtextbox.Text.Trim());

                //if (Utility.IsNullOrEmpty(userDetails.Roles))
                if (Utility.IsNullOrEmpty(userDetails) || userDetails.UserID == 0)
                {
                    ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                    return;
                }
                Session["USER_DETAIL"] = userDetails;
                if (Request.Params["ReturnUrl"] != null)
                {
                    FormsAuthentication.RedirectFromLoginPage(WorkflowHeaderControl_userNametextbox.Text.Trim(), false);
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(WorkflowHeaderControl_userNametextbox.Text.Trim(), false);
                    Response.Redirect("~/Landing.aspx", false);
                }
            }
        }

        /// <summary>
        /// Method that authenticates the user based on the session key.
        /// </summary>
        private void AuthenticateSessionKey()
        {
            // Check if not post back.
            if (IsPostBack)
                return;

            UserDetail userDetail = null;
            try
            {
                // Get the user detail based on session key.
                userDetail = new AuthenticationBLManager().GetAuthenticateUser
                    (Request.QueryString["sitesessionkey"]);
            }
            catch (Exception exp)
            {
                // An exception will be raised when the guid is in invalid format.
                Logger.ExceptionLog(exp);

                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                return;
            }

            // Check if user is valid or not.
            if (userDetail == null || userDetail.UserID == 0)
            {
                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                return;
            }

            // Check if the user is a candidate.
            if (userDetail.Roles != null && userDetail.Roles.Exists(item => item == UserRole.Candidate))
            {
                // Do not allow non-candidate users to access the application.
                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);

                return;
            }

            // Keep the user detail in session.
            Session["USER_DETAIL"] = userDetail;

            if (Request.Params["ReturnUrl"] != null)
            {
                FormsAuthentication.RedirectFromLoginPage(WorkflowHeaderControl_userNametextbox.Text.Trim(), false);
            }
            else
            {
                FormsAuthentication.SetAuthCookie(WorkflowHeaderControl_userNametextbox.Text.Trim(), false);
                Response.Redirect("~/Landing.aspx", false);
            }
        }

        /// <summary>
        /// Method that retreives the user name and password form cookie and 
        /// tries to login using that.
        /// </summary>
        private void RetrieveCookies()
        {
            try
            {
                // Check if not post back and user already logged in.
                if (IsPostBack || !Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                    return;

                // Try to get cookie information.
                HttpCookie cookie = Request.Cookies["FORTEHCM_USER_DETAILS"];

                if (cookie != null)
                {
                    string userName = cookie["FHCM_USER_NAME"];
                    string password = cookie["FHCM_PASSWORD"];

                    if (!Utility.IsNullOrEmpty(userName) && !Utility.IsNullOrEmpty(password))
                    {
                        // Assign user name and password.
                        WorkflowHeaderControl_userNametextbox.Text = userName;
                        WorkflowHeaderControl_passwordtextbox.Text = password;

                        // Try to login.
                        TryLogin();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message to be 
        /// show to the user.
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            if (string.IsNullOrEmpty(WorkflowHeaderControl_bottomErrorMessageLabel.Text))
                WorkflowHeaderControl_bottomErrorMessageLabel.Text = Message;
            else
                WorkflowHeaderControl_bottomErrorMessageLabel.Text += "<br />" + Message;
        }

        /// <summary>
        /// Shows the error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        /// <summary>
        /// Method that will return a userdetail object for the 
        /// given username and password.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the username.
        /// </param>
        /// <param name="Password">
        /// A <see cref="string"/> that contains the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> object that contains the 
        /// information for the given username and password.
        /// </returns>
        private UserDetail AuthenticateUserDetails(string UserName, string Password)
        {
            AuthenticationBLManager authenticationBLManager = new AuthenticationBLManager();
            Authenticate authenticate = new Authenticate();
            authenticate.UserName = UserName;
            authenticate.Password = new EncryptAndDecrypt().EncryptString(Password.Trim());
            UserDetail userDetails = new UserDetail();
            userDetails = authenticationBLManager.GetAuthenticateUser(authenticate);
            return userDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsValidData()
        {
            bool isValidData = true;
            if (WorkflowHeaderControl_userNametextbox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_UserNameTextBoxEmpty);
                isValidData = false;
            }
            if (WorkflowHeaderControl_passwordtextbox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_PasswordTextBoxEmpty);
                isValidData = false;
            }
            return isValidData;
        }

        public void ShowChangePasswordModelPopup()
        {
            WorkflowHeaderControl_changePassword_ModalpPopupExtender.Show();
        }

        #endregion Private Methods
    }
}