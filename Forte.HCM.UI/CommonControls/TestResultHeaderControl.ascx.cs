﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI.CommonControls
{
    public partial class TestResultHeaderControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Represents the method to load the test and candidate details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <param name="testName">
        /// A<see cref="string"/>that holds the test name
        /// </param>
        /// <param name="candidateName">
        /// A<see cref="string"/>that holds the candidate name
        /// </param>
        /// <param name="creditsEarned">
        /// A<see cref="string"/>that holds the credits earned
        /// </param>
        public void LoadCandidateTestDetails(string testId, string testName,
            string candidateName, string creditsEarned)
        {
            TestResultHeaderControl_testIDValueLabel.Text = testId;
            TestResultHeaderControl_testNameValueTextBox.Text = testName;
            TestResultHeaderControl_candidateNameValueHyperLink.Text = candidateName;
            TestResultHeaderControl_creditsEarnedValueTextBox.Text = creditsEarned;
            if (Request.QueryString["candidatesession"]!=null)
                TestResultHeaderControl_candidateNameValueHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidatesession=" + Request.QueryString["candidatesession"];

        }

    }
}