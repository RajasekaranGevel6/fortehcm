﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ContactInformationControl.cs
// File that represents the user interface for the contact information details

#endregion Header

#region Directives                                                             

using System;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ContactInformationControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ContactInformationControl_plusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
                ContactInformationControl_controlsDiv.ClientID + "','" + ContactInformationControl_plusSpan.ClientID + "','" +
                ContactInformationControl_minusSpan.ClientID + "','" + ContactInformation_hiddenField.ClientID + "')");
            ContactInformationControl_minusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
                ContactInformationControl_controlsDiv.ClientID + "','" + ContactInformationControl_plusSpan.ClientID + "','" +
                ContactInformationControl_minusSpan.ClientID + "','" + ContactInformation_hiddenField.ClientID + "')");

            // 
            if (!Utility.IsNullOrEmpty(ContactInformation_hiddenField.Value) &&
                ContactInformation_hiddenField.Value == "Y")
            {
                ContactInformationControl_controlsDiv.Style["display"] = "none";
                ContactInformationControl_plusSpan.Style["display"] = "block";
                ContactInformationControl_minusSpan.Style["display"] = "none";
            }
            else
            {
                ContactInformationControl_controlsDiv.Style["display"] = "block";
                ContactInformationControl_plusSpan.Style["display"] = "none";
                ContactInformationControl_minusSpan.Style["display"] = "block";
            }
        }

        public ContactInformation DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                ContactInformationControl_cityTextBox.Text = value.City;
                ContactInformationControl_stateTextBox.Text = value.State;
                ContactInformationControl_countryNameTextBox.Text = value.Country;
                ContactInformationControl_emailTextBox.Text = value.EmailAddress;
                ContactInformationControl_fixedLineTextBox.Text = value.Phone.Residence;
                ContactInformationControl_mobileTextBox.Text = value.Phone.Mobile;
                ContactInformationControl_postalCodeTextBox.Text = value.PostalCode;
                ContactInformationControl_streetTextBox.Text = value.StreetAddress;
                ContactInformationControl_websiteTextBox.Text = value.WebSiteAddress.Personal;
            }
        }
    }
}