﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// DepartmentControl.cs
// File that represents the user interface for the Create,Edit and View the client.

#endregion Header

#region Directives
using System;
using System.Web.UI;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Web.UI.WebControls;
using Forte.HCM.Support;
using System.Web.UI.HtmlControls;
using Resources;
using Forte.HCM.BL;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class DepartmentControl : System.Web.UI.UserControl
    {
        private string DEPARTMENT_ATTRIBUTES_DATASOURCE = "DEPARTMENT_ATTRIBUTES_DATASOURCE";
        // The public events must conform to this format
        public delegate void Button_CommandClick(object sender, CommandEventArgs e);
        public delegate void DropDown_sellectedIndexChanged(object sender, EventArgs e);

        // These two public events are to be customized as need on the page and will be called
        // by the private events of the buttons in the user control
        public event Button_CommandClick OkCommandClick;
        //public event Button_Click CancelClick;

        public event DropDown_sellectedIndexChanged SellectedIndexChanged;
        protected void Page_Load(object sender, EventArgs e)
        {
            //DepartmentControl_createButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT;
            DepartmentControl_departmentNameTextBox.Focus();
            DepartmentControl_ClientNameDropDownList.Enabled = true;

            HiddenField ClientManagement_viewClientIDHiddenField = (HiddenField)this.Parent.FindControl("ClientManagement_viewClientIDHiddenField");

            if (ClientManagement_viewClientIDHiddenField != null)
            {
                //DepartmentControl_ClientNameDropDownList.SelectedValue = ClientManagement_viewCliendNameHiddenField.Value;
            }

            if (!Page.IsPostBack)            
                return;
         
            //PrimaryAddress();
        }

        protected void DepartmentControl_ClientNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DepartmentControl_clientIDHiddenField.Value = DepartmentControl_ClientNameDropDownList.SelectedValue.ToString();
            if (SellectedIndexChanged != null)
                this.SellectedIndexChanged(sender, e);
            if (DepartmentControl_ClientNameDropDownList.SelectedValue == "0")
                DepartmentControl_primaryAddressCheckbox.Visible = false;
            else
                DepartmentControl_primaryAddressCheckbox.Visible = true;
       }

        public int Client_ID
        {
            set {
                DepartmentControl_clientIDHiddenField.Value = value.ToString();
            }
            get {
                int client_ID = 0;
                int.TryParse(DepartmentControl_clientIDHiddenField.Value, out client_ID);
                if (client_ID >0)
                    DepartmentControl_primaryAddressCheckbox.Visible = false;
                else
                    DepartmentControl_primaryAddressCheckbox.Visible = true;
                return client_ID;
            }
        }
             

        public List<ClientInformation> ClientInformationListDatasource
        {
            set
            {
                DepartmentControl_ClientNameDropDownList.Items.Clear();
                ListItem listItem = new ListItem();
                listItem.Text = "--Select--";
                listItem.Value = "0";
                DepartmentControl_ClientNameDropDownList.Items.Add(listItem);
                if (value == null)
                    return;

                if (DepartmentControl_ClientNameDropDownList.SelectedValue == "0")
                    DepartmentControl_primaryAddressCheckbox.Visible = false;
                else
                    DepartmentControl_primaryAddressCheckbox.Visible = true;

                DepartmentControl_ClientNameDropDownList.DataTextField = "ClientName";
                DepartmentControl_ClientNameDropDownList.DataValueField = "ClientID";
                DepartmentControl_ClientNameDropDownList.DataSource = value;
                DepartmentControl_ClientNameDropDownList.DataBind();
            }
        }

        public Department DepartmentDataSource
        {
            set
            {
                if (DepartmentControl_ClientNameDropDownList.SelectedValue == "0")
                    DepartmentControl_primaryAddressCheckbox.Visible = false;
                else
                    DepartmentControl_primaryAddressCheckbox.Visible = true;
                if (value == null)
                {
                    DepartmentControl_descriptionTextBox.Text = string.Empty;
                    DepartmentControl_streetAddressTextBox.Text = string.Empty;
                    DepartmentControl_eMailIDTextBox.Text = string.Empty;
                    DepartmentControl_cityTextBox.Text = string.Empty;
                    DepartmentControl_countryTextBox.Text = string.Empty;
                    DepartmentControl_stateTextBox.Text = string.Empty;
                    DepartmentControl_zipCodeTextBox.Text = string.Empty;
                    DepartmentControl_departmentNameTextBox.Text = string.Empty;
                    DepartmentControl_faxNoTextBox.Text = string.Empty;
                    DepartmentControl_phoneNoTextBox.Text = string.Empty;
                    DepartmentControl_additionalInfoTextBox.Text = string.Empty;
                    DepartmentControl_createButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT;
                    //DepartmentControl_createButton.CommandName = "add";
                    DepartmentControl_messagetitleLiteral.Text = "Add Department";
                    DepartmentControl_createButton.CommandArgument = "0";
                  //  DepartmentControl_ClientNameDropDownList.Enabled = true;
                    DepartmentControl_clientNameTR.Visible = true;
                    return;
                }

                DepartmentControl_descriptionTextBox.Text = value.DepartmentDescription;
                DepartmentControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                DepartmentControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                DepartmentControl_cityTextBox.Text = value.ContactInformation.City;
                DepartmentControl_countryTextBox.Text = value.ContactInformation.Country;
                DepartmentControl_stateTextBox.Text = value.ContactInformation.State;
                DepartmentControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                DepartmentControl_departmentNameTextBox.Text = value.DepartmentName;
                DepartmentControl_faxNoTextBox.Text = value.FaxNumber;
                DepartmentControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                DepartmentControl_additionalInfoTextBox.Text = value.AdditionalInfo;
                DepartmentControl_createButton.CommandName = "edit";
                DepartmentControl_createButton.CommandArgument = value.DepartmentID.ToString();
                DepartmentControl_ClientNameDropDownList.SelectedValue = value.ClientID.ToString();
                DepartmentControl_ClientNameDropDownList.Enabled = false;
                DepartmentControl_clientIDHiddenField.Value = value.ClientID.ToString();
                DepartmentControl_primaryAddressCheckbox.Checked = true;
                DepartmentControl_clientNameTR.Visible = false;
                DepartmentControl_primaryAddressCheckbox.Visible = false;
                DepartmentControl_messagetitleLiteral.Text = "Edit Department";

            }
            get
            {
                Department department = new Department();
                department.DepartmentDescription = DepartmentControl_descriptionTextBox.Text.Trim();
                department.AdditionalInfo = DepartmentControl_additionalInfoTextBox.Text.Trim();
                ContactInformation contInfo = new ContactInformation();

                contInfo.StreetAddress = DepartmentControl_streetAddressTextBox.Text.Trim();
                contInfo.EmailAddress = DepartmentControl_eMailIDTextBox.Text.Trim();
                contInfo.City = DepartmentControl_cityTextBox.Text.Trim();
                contInfo.Country = DepartmentControl_countryTextBox.Text.Trim();
                contInfo.State = DepartmentControl_stateTextBox.Text.Trim();
                contInfo.PostalCode = DepartmentControl_zipCodeTextBox.Text.Trim();

                if (Utility.IsNullOrEmpty(DepartmentControl_clientIDHiddenField.Value) || DepartmentControl_clientIDHiddenField.Value == "0")
                {
                    department.ClientID = int.Parse(DepartmentControl_ClientNameDropDownList.SelectedValue);
                }
                else
                {
                    department.ClientID = int.Parse(DepartmentControl_clientIDHiddenField.Value);
                }
                department.DepartmentName = DepartmentControl_departmentNameTextBox.Text.Trim();

                department.FaxNumber = DepartmentControl_faxNoTextBox.Text.Trim();

                department.IsDeleted = false;
                PhoneNumber phone = new PhoneNumber();
                phone.Mobile = DepartmentControl_phoneNoTextBox.Text.Trim();
                contInfo.Phone = phone;

                department.ContactInformation = contInfo;
                return department;
            }
        }


        public ClientInformation ClientDataSource
        {
            set
            {
                if ( DepartmentControl_ClientNameDropDownList.SelectedValue== "0")
                    DepartmentControl_primaryAddressCheckbox.Visible = false;
                else
                    DepartmentControl_primaryAddressCheckbox.Visible = true;
                DepartmentControl_messagetitleLiteral.Text = "Add Department";
                if (value == null)
                {
                    DepartmentControl_descriptionTextBox.Text = string.Empty;
                    DepartmentControl_streetAddressTextBox.Text = string.Empty;
                    DepartmentControl_eMailIDTextBox.Text = string.Empty;
                    DepartmentControl_cityTextBox.Text = string.Empty;
                    DepartmentControl_countryTextBox.Text = string.Empty;
                    DepartmentControl_stateTextBox.Text = string.Empty;
                    DepartmentControl_zipCodeTextBox.Text = string.Empty;
                    DepartmentControl_departmentNameTextBox.Text = string.Empty;
                    DepartmentControl_faxNoTextBox.Text = string.Empty;
                    DepartmentControl_phoneNoTextBox.Text = string.Empty;
                    DepartmentControl_createButton.CommandName = "add";
                    
                    DepartmentControl_createButton.CommandArgument = "0";
                    return;
                }
                Session["CONTACT_INFORMATION"] = value.ContactInformation;
                DepartmentControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                DepartmentControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                DepartmentControl_cityTextBox.Text = value.ContactInformation.City;
                DepartmentControl_countryTextBox.Text = value.ContactInformation.Country;
                DepartmentControl_stateTextBox.Text = value.ContactInformation.State;
                DepartmentControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                DepartmentControl_faxNoTextBox.Text = value.FaxNumber;
                DepartmentControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                DepartmentControl_clientIDHiddenField.Value = value.ClientID.ToString();
                DepartmentControl_primaryAddressCheckbox.Checked = true;
             //   DepartmentControl_ClientNameDropDownList.Enabled = true;
                DepartmentControl_ClientNameDropDownList.SelectedValue = value.ClientID.ToString();
                
            }

        }


        private bool IsValid()
        {
            if (Utility.IsNullOrEmpty(Session[DEPARTMENT_ATTRIBUTES_DATASOURCE]))
                return false;

            StringBuilder requiredFieldMessage = new StringBuilder();
            string validEmail = "";

            Department department = new Department();
            department = DepartmentDataSource;
            NomenclatureCustomize departmentAttributes = new NomenclatureCustomize();
            bool isvalid = true;
            departmentAttributes = (NomenclatureCustomize)Session[DEPARTMENT_ATTRIBUTES_DATASOURCE];
            if (department.DepartmentName.Length == 0)
            {
                requiredFieldMessage.Append(departmentAttributes.DepartmentName);
                isvalid = false;
            }

            //if (departmentAttributes.StreetAddressIsVisible && departmentAttributes.StreetAddressIsMandatory
            //    && department.ContactInformation.StreetAddress.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.StreetAddress);
            //    isvalid = false;
            //}
            //if (departmentAttributes.CityIsVisible && departmentAttributes.CityIsMandatory
            //    && department.ContactInformation.City.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.City);
            //    isvalid = false;
            //}

            //if (departmentAttributes.StateIsVisible && departmentAttributes.StateIsMandatory
            //    && department.ContactInformation.State.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.state);
            //    isvalid = false;
            //}
            //if (departmentAttributes.ZipCodeIsVisible && departmentAttributes.ZipCodeIsMandatory
            //    && department.ContactInformation.PostalCode.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.Zipcode);
            //    isvalid = false;
            //}
            //if (departmentAttributes.CountryIsVisible && departmentAttributes.CountryIsMandatory
            //    && department.ContactInformation.Country.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.Country);
            //    isvalid = false;
            //}

            if (departmentAttributes.PhoneNumberIsVisible && departmentAttributes.PhoneNumberFieldIsMandatory
                && department.ContactInformation.Phone.Mobile.Length == 0)
            {
                if (requiredFieldMessage.Length > 0)
                    requiredFieldMessage.Append(", ");
                requiredFieldMessage.Append(departmentAttributes.PhoneNumber);
                isvalid = false;
            }
            //if (departmentAttributes.FaxNoIsVisible && departmentAttributes.FaxNoIsMandatory
            //    && department.FaxNumber.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.FaxNo);
            //    isvalid = false;
            //}
            if (departmentAttributes.EmailIDIsVisible && departmentAttributes.EmailIDFieldIsMandatory)
            {
                if (department.ContactInformation.EmailAddress.Length == 0)
                {
                    if (requiredFieldMessage.Length > 0)
                        requiredFieldMessage.Append(", ");
                    requiredFieldMessage.Append(departmentAttributes.EmailID);
                    isvalid = false;
                }
                else if (!Utility.IsValidEmailAddress(department.ContactInformation.EmailAddress))
                {
                    validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                    isvalid = false;
                }
            }
            else if (department.ContactInformation.EmailAddress.Length > 0 && !Utility.IsValidEmailAddress(department.ContactInformation.EmailAddress))
            {
                validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                isvalid = false;
            }


            //if (departmentAttributes.DescriptionIsVisible && departmentAttributes.DescriptionIsMandatory
            //    && department.DepartmentDescription.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.Description);
            //    isvalid = false;
            //}

            //if (departmentAttributes.AdditionalInfoIsVisible && departmentAttributes.AdditionalFieldInfoIsMandatory
            //    && department.AdditionalInfo.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(departmentAttributes.AdditionalInfo);
            //    isvalid = false;
            //}

            if (Utility.IsNullOrEmpty(DepartmentControl_clientIDHiddenField.Value) || DepartmentControl_clientIDHiddenField.Value == "0")
            {
              //  DepartmentControl_ClientNameDropDownList.SelectedValue = DepartmentControl_clientIDHiddenField.Value;
                
                requiredFieldMessage.Append(departmentAttributes.AdditionalInfo);
                isvalid = false;
            }

            if (!isvalid)
            {
                if (DepartmentControl_ClientNameDropDownList.SelectedValue == "0")
                    DepartmentControl_primaryAddressCheckbox.Visible = false;
                else
                    DepartmentControl_primaryAddressCheckbox.Visible = true;
                // ShowErrorMessage("The following field(s) are mandatory and require to be filled:"+message.ToString());
                if (requiredFieldMessage.Length > 0)
                    ShowErrorMessage("Mandatory fields cannot be empty");
                if (validEmail.Length > 0)
                    ShowErrorMessage(validEmail);

                InvokePageBaseMethod("ClientManagement_department_ModalpPopupExtender1", null);
                //StringBuilder message = new StringBuilder();
                //bool isvalid = true;
                //Department departmentInformation = new Department();
                //departmentInformation = DepartmentDataSource;
                //if (departmentInformation.DepartmentName.Length == 0)
                //{
                //    message.Append("Department name");
                //    isvalid=false;
                //}

                //if (!isvalid)
                //{
                //    ShowErrorMessage(message.ToString() + " should not empty.");
                //    InvokePageBaseMethod("ClientManagement_department_ModalpPopupExtender", null);
                //}
            }
            return isvalid;
        }

        private void ShowErrorMessage(string exp)
        {
            if (string.IsNullOrEmpty(DepartmentControl_topErrorMessageLabel.Text))
                DepartmentControl_topErrorMessageLabel.Text = exp;
            else
                DepartmentControl_topErrorMessageLabel.Text += "<br />" + exp;
        }

        public string ShowMessage
        {
            set
            {
                ShowErrorMessage(value);
            }
        }

        public NomenclatureCustomize DepartmenAttributesDataSource
        {
            set
            {
                if (value == null)
                {
                    Session[DEPARTMENT_ATTRIBUTES_DATASOURCE] = null;
                    return;
                }
                Session[DEPARTMENT_ATTRIBUTES_DATASOURCE] = value;
                DepartmentControl_ClientName.Text = value.ClientName;
                HtmlTableCell TableCell = null;

                DepartmentControl_primaryAddressCheckbox.Visible = value.DepartmentPrimaryAddress;
                if (value.DescriptionIsVisible)
                {
                    DepartmentControl_descriptionLabel.Text = value.Description;
                    DepartmentControl_descriptionLabel.Visible = true;
                    DepartmentControl_descriptionTextBox.Visible = true;
                    //if (value.DescriptionIsMandatory)
                    //{
                    //    DepartmentControl_descriptionSpan.Visible = true;
                    //}

                }
                if (value.AdditionalInfoIsVisible)
                {
                    DepartmentControl_additionalInfoLabel.Text = value.AdditionalInfo;
                    DepartmentControl_additionalInfoLabel.Visible = true;
                    DepartmentControl_additionalInfoTextBox.Visible = true;
                    DepartmentControl_additionalInfoSpan.Visible = value.AdditionalFieldInfoIsMandatory;
                }
                int i = 5;
                if (value.StreetAddressIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    DepartmentControl_streetAddressLabel.Visible = true;
                    DepartmentControl_streetAddressLabel.Text = value.StreetAddress;
                    TableCell.Controls.Add(DepartmentControl_streetAddressLabel);
                    //if (value.StreetAddressIsMandatory)
                    //{
                    //    DepartmentControl_streetAddressSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_streetAddressSpan);
                    //}
                    i++;
                    DepartmentControl_streetAddressTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_streetAddressTextBox);
                    i++;
                }
                if (value.CityIsVisible)
                {
                    DepartmentControl_cityLabel.Visible = true;
                    DepartmentControl_cityLabel.Text = value.City;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_cityLabel);
                    //if (value.CityIsMandatory)
                    //{
                    //    DepartmentControl_citySpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_citySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_cityTextBox);
                    i++;
                    DepartmentControl_cityTextBox.Visible = true;
                }
                if (value.StateIsVisible)
                {
                    DepartmentControl_stateLabel.Visible = true;
                    DepartmentControl_stateLabel.Text = value.state;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_stateLabel);
                    //if (value.StateIsMandatory)
                    //{
                    //    DepartmentControl_stateSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_stateSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_stateTextBox);
                    i++;
                    DepartmentControl_stateTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    DepartmentControl_countryLabel.Text = value.Country;
                    DepartmentControl_countryLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_countryLabel);
                    //if (value.CountryIsMandatory)
                    //{
                    //    DepartmentControl_countrySpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_countrySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_countryTextBox);
                    i++;
                    DepartmentControl_countryTextBox.Visible = true;
                }
                if (value.ZipCodeIsVisible)
                {
                    DepartmentControl_zipCodeLabel.Text = value.Zipcode;
                    DepartmentControl_zipCodeLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_zipCodeLabel);

                    //if (value.ZipCodeIsMandatory)
                    //{
                    //    DepartmentControl_zipCodeSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_zipCodeSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_zipCodeTextBox);
                    i++;
                    DepartmentControl_zipCodeTextBox.Visible = true;
                }

                if (value.FaxNoIsVisible)
                {
                    DepartmentControl_faxNoLabel.Text = value.FaxNo;
                    DepartmentControl_faxNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_faxNoLabel);

                    //if (value.FaxNoIsMandatory)
                    //{
                    //    DepartmentControl_faxNoSpan.Visible = true;
                    //    TableCell.Controls.Add(DepartmentControl_faxNoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_faxNoTextBox);
                    i++;
                    DepartmentControl_faxNoTextBox.Visible = true;
                }

                if (value.EmailIDIsVisible)
                {
                    DepartmentControl_eMailIDLabel.Text = value.EmailID;
                    DepartmentControl_eMailIDLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_eMailIDLabel);

                    if (value.EmailIDFieldIsMandatory)
                    {
                        DepartmentControl_eMailIDSpan.Visible = true;
                        TableCell.Controls.Add(DepartmentControl_eMailIDSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_eMailIDTextBox);
                    i++;
                    DepartmentControl_eMailIDTextBox.Visible = true;
                }
                if (value.PhoneNumberIsVisible)
                {
                    DepartmentControl_phoneNoLabel.Text = value.PhoneNumber;
                    DepartmentControl_phoneNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_phoneNoLabel);

                    if (value.PhoneNumberFieldIsMandatory)
                    {
                        DepartmentControl_phoneNoSpan.Visible = true;
                        TableCell.Controls.Add(DepartmentControl_phoneNoSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("DepartmentControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(DepartmentControl_phoneNoTextBox);
                    i++;
                    DepartmentControl_phoneNoTextBox.Visible = true;
                }
            }
        }

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <param name="args">The args.</param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        protected void DepartmentControl_createButton_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (!IsValid())
                return;
            
            if (OkCommandClick != null)
                this.OkCommandClick(sender, e);

        }

        protected void DepartmentControl_primaryAddressCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(DepartmentControl_clientIDHiddenField.Value))
                    DepartmentControl_ClientNameDropDownList.SelectedValue = DepartmentControl_clientIDHiddenField.Value.ToString();
                InvokePageBaseMethod("ClientManagement_department_ModalpPopupExtender1", null);
                if (!DepartmentControl_primaryAddressCheckbox.Checked)
                {
                    DepartmentControl_streetAddressTextBox.Text = string.Empty;
                    DepartmentControl_cityTextBox.Text = string.Empty;
                    DepartmentControl_countryTextBox.Text = string.Empty;
                    DepartmentControl_stateTextBox.Text = string.Empty;
                    DepartmentControl_zipCodeTextBox.Text = string.Empty;
                }
                else
                {
                    if (Session["CONTACT_INFORMATION"] == null)
                        return;
                    ContactInformation contactInformation = new ContactInformation();
                    contactInformation = Session["CONTACT_INFORMATION"] as ContactInformation;
                    DepartmentControl_streetAddressTextBox.Text = contactInformation.StreetAddress;
                    DepartmentControl_cityTextBox.Text = contactInformation.City;
                    DepartmentControl_countryTextBox.Text = contactInformation.Country;
                    DepartmentControl_stateTextBox.Text = contactInformation.State;
                    DepartmentControl_zipCodeTextBox.Text = contactInformation.PostalCode;
                }
            }
            catch
            {
                throw;
            }
        }
        public string BindSaveButtonCommand
        {
            set
            {
                DepartmentControl_primaryAddressCheckbox.Visible = true;
                DepartmentControl_primaryAddressCheckbox.Checked=true;

                DepartmentControl_createButton.CommandName = value;
                DepartmentControl_ClientNameDropDownList.Enabled = false;
            }
        }
    }
}