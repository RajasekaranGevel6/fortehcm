﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientContactDetailView.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ClientContactDetailView" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <asp:Label ID="ClientContactDetailView_clientNameLabel" runat="server" Text="Client Name"
                ToolTip="Client Name" SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientContactDetailView_clientDepartmentLabel" runat="server" ToolTip="Email ID"
                Text="Email ID" SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientContactDetailView_eMailIDLabel" runat="server" ToolTip="Email ID"
                Text="Email ID" SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td colspan="5" align="right">
            <table cellpadding="1" cellspacing="3" width="32%" border="0">
                <tr>
                    <td>
                        <asp:ImageButton ID="ClientContactDetailView_editContactImageButton" runat="server"
                            SkinID="sknEditContactImageButton" OnCommand="ClientContactDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientContactDetailView_deleteContactImageButton" runat="server"
                            SkinID="sknDeleteContactImageButton" OnCommand="ClientContactDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientContactDetailView_viewClientImageButton" runat="server"
                            SkinID="sknViewClientImageButton" OnCommand="ClientContactDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientContactDetailView_viewDepartmentImageButton" runat="server"
                            SkinID="sknViewDepartmentImageButton" OnCommand="ClientContactDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientContactDetailView_viewPositionProfileImageButton" runat="server"
                            SkinID="sknViewPositionProfileImageButton1" OnCommand="ClientContactDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientContactDetailView_createPositionProfileImageButton" runat="server"
                            SkinID="sknCreatePositionProfileImageButton1" OnCommand="ClientContactDetailView_ImageButton_Command" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td style="width: 10%">
            <asp:Label ID="ClientContactDetailView_phoneNoHeadLabel" runat="server" Text="Phone No"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientContactDetailView_phoneNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td style="width: 10%">
            <asp:Label ID="ClientContactDetailView_faxNoHeadLabel" runat="server" Text="Fax No"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientContactDetailView_faxNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td style="width: 10%">
            <asp:Label ID="ClientContactDetailView_zipHeadLabel" runat="server" Text="Zip Code"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientContactDetailView_zipLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="ClientContactDetailView_streetAddressHeadLabel" runat="server" Text="Street Address"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientContactDetailView_streetAddressLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientContactDetailView_cityHeadLabel" runat="server" Text="City"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientContactDetailView_cityLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientContactDetailView_stateHeadLabel" runat="server" Text="State Name"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientContactDetailView_statelabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
        <tr>
            <td>
                <asp:Label ID="ClientContactDetailView_countryHeadLabel" runat="server" Text="Country"
                    SkinID="sknLabelFieldHeaderText"></asp:Label>
            </td>
            <td>
                <asp:Label ID="ClientContactDetailView_countryLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr class="td_height_5">
            <td colspan="6">
            </td>
        </tr>
       
        <tr>
            <td colspan="6">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 50%">
                            <asp:Label ID="ClientContactDetailView_additionalInfoHeadLabel" runat="server" Text="Additional Info"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 50%">
                            <asp:Label ID="ClientContactDetailView_departmentListLabel" runat="server" Text="List of Departments"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="height: 50px; width: 100%; overflow: auto; word-wrap: break-word; white-space: normal;">
                                <asp:Label ID="ClientContactDetailView_additionalInfoLabel" runat="server" Text="Additional Info"
                                    SkinID="sknLabelFieldText"></asp:Label>
                                <asp:HiddenField runat="server" ID="HiddenField_Deptid" />
                            </div>
                        </td>
                        <td>
                            <div style="height: 50px; width: 100%; overflow: auto;">
                                <asp:Repeater ID="ClientContactDetailView_departmentListRepeater" 
                                    runat="server" 
                                    onitemcommand="ClientContactDetailView_departmentListRepeater_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="ClientContactDetailView_departmentNameLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text='<%# Eval("DepartmentName") %>'  CommandName="showcontatdepartment" CommandArgument='<%# Eval("DepartmentID") %>' 
                                            ToolTip="Click here to view department details"></asp:LinkButton><br />

                                            <asp:HiddenField ID="ClientContactDetailView_departmentIdHiddenField" Value='<%# Eval("DepartmentID") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
           
</table>
