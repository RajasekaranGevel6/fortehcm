﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileSummary.cs
// File that represents the user interface for the Position Profile Summary
//
#endregion

#region Directives
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;
using System.Reflection;
using Forte.HCM.Trace;
using Forte.HCM.Support;
#endregion Directives
namespace Forte.HCM.UI.CommonControls
{
    /// <summary>                                                  
    /// Handling the User and Segments keywords
    /// </summary>
    public partial class PositionProfileSummary : System.Web.UI.UserControl
    {
        #region Declarations
        List<SkillWeightage> skillWeightageList = null;
        SkillWeightage skillWeightage = null;
        string SKILLS_VIEWSTATE = "SKILLS_VIEWSTATE";
        #endregion Declarations

        #region Event Handlers
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Support.Utility.IsNullOrEmpty(ViewState[SKILLS_VIEWSTATE]))
                        DefaultSkill();
                    // SetFocus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                PositionProfileSummary_errorLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileSummary_addRowLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileSummary_addRowLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                PositionProfileSummary_errorLabel.Text = "";
                skillWeightageList = new List<SkillWeightage>();
                skillWeightageList = SkillWeightageDataSource;
                skillWeightage = new SkillWeightage();
                skillWeightage.Weightage = 0;
                skillWeightage.SkillName = "";
                skillWeightage.UserSegments = true;
                skillWeightageList.Add(skillWeightage);
                ViewState[SKILLS_VIEWSTATE] = skillWeightageList;
                PositionProfileSummary_skillKeywordGridView.DataSource = skillWeightageList;
                PositionProfileSummary_skillKeywordGridView.DataBind();
                SetFocus();
                InvokePageBaseMethod("ShowPositionProfileSummary", null);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                PositionProfileSummary_errorLabel.Text = exp.Message;
            }
        }

        public void SetFocus()
        {
            GridViewRow lastRow = PositionProfileSummary_skillKeywordGridView.Rows
                [PositionProfileSummary_skillKeywordGridView.Rows.Count - 1];
            TextBox skillCategory = (TextBox)lastRow.FindControl("PositionProfileSummary_skillCategoryTextBox");
            skillCategory.Focus();

        }

        /// <summary>
        /// Handles the RowDataBound event of the PositionProfileSummary_skillKeywordGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void PositionProfileSummary_skillKeywordGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton delteImgaeButton = (ImageButton)e.Row.FindControl("PositionProfileSummary_deleteSkillsImageButton");
                    delteImgaeButton.CommandArgument = e.Row.RowIndex.ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                PositionProfileSummary_errorLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the PositionProfileSummary_skillKeywordGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void PositionProfileSummary_skillKeywordGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "deleteskill")
                {
                    List<SkillWeightage> skillWeightageList = new List<SkillWeightage>();
                    skillWeightageList = ViewState[SKILLS_VIEWSTATE] as List<SkillWeightage>;

                    if (skillWeightageList.Count > 0)
                    {
                        skillWeightageList.RemoveAt(int.Parse(e.CommandArgument.ToString()));
                    }
                    PositionProfileSummary_skillKeywordGridView.DataSource = skillWeightageList;
                    PositionProfileSummary_skillKeywordGridView.DataBind();
                    ViewState[SKILLS_VIEWSTATE] = skillWeightageList;
                    InvokePageBaseMethod("ShowPositionProfileSummary", null);
                    SetFocus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                PositionProfileSummary_errorLabel.Text = exp.Message;
            }
        }
        #endregion Event Handlers

        #region Public and Private Methods

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <param name="args">The args.</param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        /// <summary>
        /// Gets or sets the skill weightage data source.
        /// </summary>
        /// <value>
        /// The skill weightage data source.
        /// </value>
        public List<SkillWeightage> SkillWeightageDataSource
        {
            set
            {
                if (value.Count == 0)
                {
                    DefaultSkill();
                }
                else
                {
                    List<SkillWeightage> skillDetails = new List<SkillWeightage>();
                    foreach (SkillWeightage skills in value)
                    {
                        if (!Utility.IsNullOrEmpty(skills.SkillName))
                        {
                            skillDetails.Add(skills);
                        }
                    }
                    if (skillDetails.Count == 0)
                    {
                        DefaultSkill();
                        return;
                    }

                    PositionProfileSummary_skillKeywordGridView.DataSource = skillDetails;
                    PositionProfileSummary_skillKeywordGridView.DataBind();
                    ViewState[SKILLS_VIEWSTATE] = value;
                }
                SetFocus();
            }
            get
            {
                List<SkillWeightage> skillWeightageList = new List<SkillWeightage>();

                int sumOfWeightage = 0;
                int restCount = 0;
                decimal balanceAverageWeightage = 0;
                foreach (GridViewRow row in PositionProfileSummary_skillKeywordGridView.Rows)
                {
                    TextBox WeightageTextbox = (TextBox)
                       row.FindControl("PositionProfileSummary_skillCategoryWeightageTextbox");
                    if (Convert.ToInt32(WeightageTextbox.Text) == 0)
                        restCount = restCount + 1;
                    else
                        sumOfWeightage = sumOfWeightage + Convert.ToInt32(WeightageTextbox.Text);
                }
                if (restCount > 0)
                    balanceAverageWeightage = (100 - sumOfWeightage) / restCount;
                else
                    balanceAverageWeightage = 100 - sumOfWeightage;

                foreach (GridViewRow row in PositionProfileSummary_skillKeywordGridView.Rows)
                {
                    SkillWeightage skillWeightage = new SkillWeightage();
                    TextBox SkillCategoryTextBox = (TextBox)
                       row.FindControl("PositionProfileSummary_skillCategoryTextBox");
                    TextBox WeightageTextbox = (TextBox)
                       row.FindControl("PositionProfileSummary_skillCategoryWeightageTextbox");
                    HiddenField userSegment = (HiddenField)row.FindControl("PositionProfileSummary_isUserKeywordHiddenField");
                    HiddenField skillType = (HiddenField)row.FindControl("PositionProfileSummary_skillTypeHiddenField");
                    HiddenField skillCategory = (HiddenField)row.FindControl("PositionProfileSummary_skillCategoryHiddenField");

                    skillWeightage.SkillName = SkillCategoryTextBox.Text.Trim();
                    if (decimal.Parse(WeightageTextbox.Text.Trim()) == 0)
                        skillWeightage.Weightage = balanceAverageWeightage;
                    else
                        skillWeightage.Weightage = decimal.Parse(WeightageTextbox.Text.Trim());

                    skillWeightage.SkillTypes = (SkillType)Enum.Parse(typeof(SkillType), skillType.Value);
                    skillWeightage.SkillCategory = skillCategory.Value;
                    skillType.Value.ToString();
                    skillWeightage.UserSegments = Convert.ToBoolean(userSegment.Value);
                    skillWeightageList.Add(skillWeightage);
                }
                return skillWeightageList;
            }
        }

        /// <summary>
        /// Sets the set error message.
        /// </summary>
        /// <value>
        /// The set error message.
        /// </value>
        public string SetErrorMessage
        {
            set
            {
                PositionProfileSummary_errorLabel.Text = value;
            }
        }

        /// <summary>
        /// Default skills.
        /// </summary>
        public void DefaultSkill()
        {
            skillWeightageList = new List<SkillWeightage>();
            skillWeightage = new SkillWeightage();
            skillWeightage.Weightage = 0;
            skillWeightage.SkillName = "";
            skillWeightage.UserSegments = true;
            skillWeightageList.Add(skillWeightage);
            PositionProfileSummary_skillKeywordGridView.DataSource = skillWeightageList;
            PositionProfileSummary_skillKeywordGridView.DataBind();
            ViewState[SKILLS_VIEWSTATE] = skillWeightageList;

        }
        #endregion
    }
}