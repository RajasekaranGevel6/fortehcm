﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SkillsMatrixControl.cs
// File that represents the skill matrix dialog which loads a skill matrix
// based on the button click in parent page.

#endregion Header

#region Directives

using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Support;
using System.Collections.Generic;
using System.Xml;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// The class that represents the layout for skill matrix popup
    /// It respective event handler loads the skills matrix data. 
    /// </summary>
    public partial class SkillsMatrixControl : UserControl
    {
        #region Event Handlers                                                 

        /// <summary>
        /// This event handler loads Skill Matrix for a specific candidate id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (!IsPostBack)
            {
                try
                {
                    if (!Utility.IsNullOrEmpty(Request["CandID"].ToString()))
                    {
                        SkillsMatrixControl_resumeTR.Attributes.Add("onclick", "ExpandOrRestore('" + SkillsMatrixControl_topPaneInnerDiv.ClientID + "','" +
                            SkillsMatrixControl_resumeDownSpan.ClientID+"','" +
                              SkillsMatrixControl_resumeUpSpan.ClientID + "')");

                        SkillsMatrixControl_skillTR.Attributes.Add("onclick", "ExpandOrRestore('" + SkillsMatrixControl_skillDIV.ClientID + "','" +
                            SkillsMatrixControl_skillDownSpan.ClientID + "','"+
                            SkillsMatrixControl_skillUpSpan.ClientID + "')");

                        ProcessHRXml(Convert.ToInt32(Request["CandID"].ToString()), Request["Type"].ToString());
                        BindSkillMatrix(Request["CandID"].ToString(), Request["Type"].ToString());
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// This event handler spans Skill Matrix row for a specific candidate id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void SkillsMatrixControl_competencyGridView_PreRender(object sender, EventArgs e)
        {
            SpanRows(SkillsMatrixControl_competencyGridView);
        }

        /// <summary>
        /// This event handler binds skill matrix data for every row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="GridViewRowEventArgs"></param>        
        protected void SkillsMatrixControl_competencyGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Header section
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].BackColor = ColorTranslator.FromHtml("#FFFFFF");
                e.Row.Cells[1].Attributes.Add("style", "text-align:center;height:50px;width:70px;");

                for (int i = 2; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                    e.Row.Cells[i].BackColor = i % 2 == 0 ? ColorTranslator.FromHtml("#DAE6EB") :
                                           ColorTranslator.FromHtml("#BDC9CE");
                }
            }

            //Item section
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Attributes.Add("style", "text-align:center;width:100px;");

                e.Row.Cells[0].Attributes.Add("style", "text-align:center;width:100px;");
                e.Row.Cells[0].BackColor = e.Row.RowIndex % 2 == 0 ? ColorTranslator.FromHtml("#A3C0CD") :
                                           ColorTranslator.FromHtml("#BCD2DC");

                e.Row.Cells[1].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                e.Row.Cells[1].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                e.Row.Cells[1].BackColor = e.Row.RowIndex % 2 == 0 ? ColorTranslator.FromHtml("#DAE6EB") :
                                           ColorTranslator.FromHtml("#BDC9CE");

                string columnColor = "#77CDE9";
                string alternateColoumnColor = "#BEE562";

                if (e.Row.RowIndex % 2 == 0)
                {
                    for (int i = 2; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                        e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                        e.Row.Cells[i].BackColor = i % 2 == 0 ? ColorTranslator.FromHtml(columnColor) :
                                          ColorTranslator.FromHtml(alternateColoumnColor);
                    }
                }
                else
                {
                    for (int i = 2; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                        e.Row.Cells[i].BackColor = i % 2 == 0 ? ColorTranslator.FromHtml(alternateColoumnColor) :
                                          ColorTranslator.FromHtml(columnColor);
                    }
                }

            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// This method loads Skill Matrix for a specific candidate id
        /// </summary>
        /// <param name="candidateSessionID"></param>
        private void BindSkillMatrix(string candidateSessionID,string type)
        {
            DataTable dtSkillMarix = new DataTable();
            DataTable dtBindSkills = new DataTable();
            DataTable dtVector = new DataTable();
            DataSet dsSkillMarix = new DataSet();
 
            if(type.ToString().ToUpper()=="INT")
                dsSkillMarix = new AdminBLManager().GetInterviewCompetencyVectorSkillMatrix(candidateSessionID);
            if (type.ToString().ToUpper() == "NORMAL")
                dsSkillMarix = new AdminBLManager().GetCandidateSkillSubstance(Convert.ToInt32(candidateSessionID),0);
            else if(type.ToString().ToUpper() =="TEMP")
                dsSkillMarix = new AdminBLManager().GetTemporaryResumeSkills(Convert.ToInt32(candidateSessionID));
            else
                dsSkillMarix = new AdminBLManager().GetCompetencyVectorSkillMatrix(candidateSessionID);

            if (dsSkillMarix.Tables[0].Rows.Count <= 0)
                return;

            DataView dvSkillMarix = dsSkillMarix.Tables[0].DefaultView;
            dtSkillMarix = dvSkillMarix.ToTable(true, "PARAMETER_NAME");
            BoundField skillColumn = null;
            skillColumn = new BoundField();
            skillColumn.DataField = "GROUP_NAME";
            skillColumn.HeaderText = "";
            SkillsMatrixControl_competencyGridView.Columns.Add(skillColumn);
            skillColumn = new BoundField();
            skillColumn.DataField = "VECTOR_NAME";
            skillColumn.HeaderText = "";
            SkillsMatrixControl_competencyGridView.Columns.Add(skillColumn);
            for (int i = 0; i < dtSkillMarix.Rows.Count; i++)
            {
                dtBindSkills.Columns.Add(dtSkillMarix.Rows[i]["PARAMETER_NAME"].ToString().Trim());
                skillColumn = new BoundField();
                skillColumn.DataField = dtSkillMarix.Rows[i]["PARAMETER_NAME"].ToString().ToString().Trim();
                skillColumn.HeaderText = dtSkillMarix.Rows[i]["PARAMETER_NAME"].ToString().ToString().Trim();
                SkillsMatrixControl_competencyGridView.Columns.Add(skillColumn);
            }
            dtBindSkills.Columns.Add("VECTOR_NAME");
            dtBindSkills.Columns.Add("GROUP_NAME");
            DataView dvVector = dsSkillMarix.Tables[0].DefaultView;
            dtVector = dvVector.ToTable(true, "VECTOR_NAME");
            DataView dvRowFilter = dsSkillMarix.Tables[0].DefaultView;
            for (int j = 0; j < dtVector.Rows.Count; j++)
            {
                string strFilter = "VECTOR_NAME = '" + dtVector.Rows[j][0].ToString().Trim() + "'";
                dvRowFilter.RowFilter = strFilter;
                DataTable dtFilteredVector = dvRowFilter.ToTable();
                DataRow dRow = dtBindSkills.NewRow();
                for (int k = 0; k < dtFilteredVector.Rows.Count; k++)
                {
                    int i = 0;
                    for (i = 0; i < dtSkillMarix.Rows.Count; i++)
                    {
                        if (dtSkillMarix.Rows[i][0].ToString().Trim() == dtFilteredVector.Rows[k]["PARAMETER_NAME"].ToString().Trim())
                        {
                            dRow[i] = dtFilteredVector.Rows[k]["COMPETENCY_VALUE"].ToString().Trim();
                        }
                    }
                    dRow[i] = dtVector.Rows[j][0].ToString().Trim();
                    dRow[i + 1] = dtFilteredVector.Rows[0]["GROUP_NAME"].ToString().Trim();
                }
                dtBindSkills.Rows.Add(dRow);
            }
            dtBindSkills.Columns["GROUP_NAME"].SetOrdinal(dtBindSkills.Columns[0].Ordinal);
            dtBindSkills.Columns["VECTOR_NAME"].SetOrdinal(dtBindSkills.Columns[1].Ordinal);
            //Bind data table value    
            SkillsMatrixControl_competencyGridView.DataSource = dtBindSkills;
            SkillsMatrixControl_competencyGridView.DataBind();
        }

        /// <summary>
        /// This method spans the rows for group name
        /// </summary>
        /// <param name="GridView"></param>        
        private static void SpanRows(GridView SkillsMatrixControl_competencyGridView)
        {
            for (int rowIndex = SkillsMatrixControl_competencyGridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = SkillsMatrixControl_competencyGridView.Rows[rowIndex];
                GridViewRow previousRow = SkillsMatrixControl_competencyGridView.Rows[rowIndex + 1];
                if (row.Cells[0].Text == previousRow.Cells[0].Text)
                {
                    row.Cells[0].RowSpan = previousRow.Cells[0].RowSpan < 2 ? 2 :
                                           previousRow.Cells[0].RowSpan + 1;
                    previousRow.Cells[0].Visible = false;
                }
            }
        }

        #endregion Private Methods                                             


        #region HRXML Methods
        /// <summary>
        /// This method helps to extract the hrxml data and display its contents.
        /// </summary>
        private void ProcessHRXml(int candidateID,string type)
        {


            List<string> oList = null;
            if (type.ToString().ToUpper() == "TEMP")
                oList = new ResumeRepositoryBLManager().GetTemporaryResumeHRXML(candidateID); 
            else
                oList = new ResumeRepositoryBLManager().GetCandidateHRXml(candidateID); 

            if (oList == null)
            { 
                return;
            }
            int candidateResumeID = Convert.ToInt32(oList[0].ToString()); 
            XmlDocument oXmlDoc = new XmlDocument();
            oXmlDoc.LoadXml(oList[1].ToString());
            //Check the Xml nodes from the database
            CheckHRXMLNodes(oXmlDoc);
            if (oXmlDoc != null)
            {
                // Set Person Name 
                SetPersonalInfo(oXmlDoc);

                // Set Projects
                SetProjects(oXmlDoc);

                // Set EducationDetails
                SetEducation(oXmlDoc);

                // Set References
                SetReferences(oXmlDoc);

                // Set Preview Resume
                SetPreviewResume(oXmlDoc);
            }
            //if (oList[3].ToString() == "Y")
            //    ResumeEditor_topSaveButton.Visible = false;
            //ResumeEditor_saveUpdatePanel.Update();
        }


        /// <summary>
        /// This method helps to extract the hrxml resume contents and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPreviewResume(XmlDocument oXmlDoc)
        {
            // Set the ResumePreview
            XmlNode oPreviewResumeNodeList = oXmlDoc.SelectSingleNode("/Resume/NonXMLResume/TextResume");
            if (oPreviewResumeNodeList == null)
            {
                return;
            }
            if (oPreviewResumeNodeList.InnerText.Trim() != "")
            {
                ResumeEditor_perviewHtmlDiv.InnerHtml =
                    oPreviewResumeNodeList.InnerText.Replace("\r\n", "<br>");

                // Keep the resume contents in session.
                Session["RESUME_CONTENTS"] = oPreviewResumeNodeList.InnerText;
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml references data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetReferences(XmlDocument oXmlDoc)
        {
            //Get the list of references
            XmlNodeList oReferenceNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/References/Reference");
            if (oReferenceNodeList.Count == 0)
            {
                return;
            }
            List<Reference> oListReference = new List<Reference>();
            int intRefCnt = 0;
            foreach (XmlNode node in oReferenceNodeList)
            {
                if (node.InnerXml != "")
                {
                    intRefCnt = intRefCnt + 1;
                    Reference oReference = new Reference();
                    ContactInformation oContactInfo = new ContactInformation();
                    PhoneNumber oPhone = new PhoneNumber();
                    oReference.ReferenceId = intRefCnt;

                    //Get the person name from the xml
                    XmlNode referenceNode = node.SelectSingleNode("PersonName");
                    oReference.Name = GetInnerText(referenceNode, "FormattedName");

                    //Get the position title
                    oReference.Relation = GetInnerText(node, "PositionTitle");

                    //Get the comments
                    oReference.Organization = GetInnerText(node, "Comments");

                    //Get the contact node from the xml
                    XmlNode contactNode = node.SelectSingleNode("ContactMethod");
                    XmlNode phoneNode = contactNode.SelectSingleNode("Telephone");

                    //Get the telephone number from the contact node
                    oPhone.Mobile = GetInnerText(phoneNode, "FormattedNumber");

                    oContactInfo.Phone = oPhone;
                    oReference.ContactInformation = oContactInfo;
                    oListReference.Add(oReference);
                }
            }
            ReferencesControl oRefCntl = (ReferencesControl)SkillsMatrixControl_mainResumeControl.
                FindControl("MainResumeControl_referencesControl");
            oRefCntl.DataSource = oListReference;
            oRefCntl.DataBind();
        }

        /// <summary>
        /// This method helps to extract the hrxml educational data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetEducation(XmlDocument oXmlDoc)
        {
            XmlNodeList oEducationNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/EducationHistory");
            if (oEducationNodeList.Count == 0)
            {
                return;
            }
            List<Education> oListEducation = new List<Education>();

            //Get the education list of the candidate
            //This will 
            XmlNodeList educationSchoolList = oXmlDoc.SelectNodes("//SchoolOrInstitution");

            int intEduCnt = 0;

            string innerXML = string.Empty;

            XmlNode nodeSchool = null;
            XmlNode nodeDegree = null;

            foreach (XmlNode node in educationSchoolList)
            {
                if (node.ChildNodes.Count == 0)
                {
                    continue;
                }

                Education oEducation = new Education();
                Location oLocation = new Location();

                //Get the school name of the candidate 
                nodeSchool = node.SelectSingleNode("School");
                innerXML = GetInnerText(nodeSchool, "SchoolName");

                //Separate the school name using , 
                string[] str = innerXML.Split(',');
                intEduCnt = intEduCnt + 1;
                oEducation.EducationId = intEduCnt;
                //Assign the school name
                oEducation.SchoolName = str[0];

                //If the length >1 assign location
                if (str.Length > 1)
                {
                    oLocation.City = innerXML.Remove(0, str[0].Length);
                    oEducation.SchoolLocation = oLocation;
                }

                nodeDegree = node.SelectSingleNode("Degree");

                //Get the degree name
                oEducation.DegreeName = GetInnerText(nodeDegree, "DegreeName");

                DateTime DtEduGradDate;

                //Get the degree date 
                XmlNode degreeDate = nodeDegree.SelectSingleNode("DegreeDate");

                string date = GetInnerText(degreeDate, "YearMonth");

                if (date.Length != 0)
                {
                    if ((DateTime.TryParse(date, out DtEduGradDate)))
                    {
                        oEducation.GraduationDate = DtEduGradDate;
                    }
                }

                XmlNode degreeNameNode = nodeDegree.SelectSingleNode("DegreeMajor");

                //Get the degree major 
                oEducation.Specialization = GetInnerText(degreeNameNode, "Name");

                //Get the degree comments
                oEducation.GPA = GetInnerText(nodeDegree, "Comments");
                oListEducation.Add(oEducation);
            }

            EducationControl oEduCntl = (EducationControl)SkillsMatrixControl_mainResumeControl.FindControl("MainResumeControl_educationControl");
            oEduCntl.DataSource = oListEducation;
            oEduCntl.DataBind();
            SetEducationDate();
        }

        /// <summary>
        /// This method helps to set the date format empty in education.
        /// </summary>
        private void SetEducationDate()
        {
            EducationControl oEduCntl = (EducationControl)SkillsMatrixControl_mainResumeControl.FindControl("MainResumeControl_educationControl");
            ListView EducationControl_listView = (ListView)oEduCntl.FindControl("EducationControl_listView");
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                if (txtEduGradDate.Text == "01/01/0001" || txtEduGradDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtEduGradDate.Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// This method helps to check the HRXML nodes
        /// </summary>
        /// <param name="oXmlDoc">
        /// A<see cref="XmlDocument"/>that holds the xml document
        /// </param>
        private void CheckHRXMLNodes(XmlDocument oXmlDoc)
        {
            //Get the list of lineage item
            List<LineageItem> lineageItem = new ResumeRepositoryBLManager().GetLineageItem();

            string nodeComment = string.Empty;

            foreach (LineageItem item in lineageItem)
            {
                //for each lineage item check the node in oXmldoc
                XmlNode document = oXmlDoc.SelectSingleNode("//" + item.Lineage);

                if (document == null)
                {
                    //if document is null add this 
                    nodeComment += nodeComment.Contains(item.ComponentName.Trim()) ? "" : item.ComponentName + ", ";
                }
            }
            if (nodeComment.Trim().Length > 0)
            {
                if (nodeComment.Trim().Length < 2)
                    return;

                nodeComment = nodeComment.Remove(nodeComment.Length - 2, 2);

                //ShowMessage(ResumeEditor_topErrorMessageLabel,
                //  "Mismatch in these nodes : " + nodeComment);
            }
        }
        /// <summary>
        /// This method helps to extract the hrxml personal data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetPersonalInfo(XmlDocument oXmlDoc)
        {
            XmlNodeList oPersonNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ContactInfo/PersonName");
            if (oPersonNodeList.Count > 0)
            {
                NameControl nc = (NameControl)SkillsMatrixControl_mainResumeControl.FindControl("MainResumeControl_nameControl");
                TextBox txtFirstName = (TextBox)nc.FindControl("NameControl_firstNameTextBox");
                TextBox txtMiddleName = (TextBox)nc.FindControl("NameControl_middleNameTextBox");
                TextBox txtLastName = (TextBox)nc.FindControl("NameControl_lastNameTextBox");

                XmlNode personalDetail = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo");
                txtFirstName.Text = GetInnerText(personalDetail, "FirstName");
                txtMiddleName.Text = GetInnerText(personalDetail, "MiddleName");
                txtLastName.Text = GetInnerText(personalDetail, "LastName");
            }

            XmlNodeList oContactInfoNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
            if (oContactInfoNodeList.Count > 0)
            {
                ContactInformationControl cil = (ContactInformationControl)SkillsMatrixControl_mainResumeControl.FindControl("MainResumeControl_contactInfoControl");
                TextBox txtAddress = (TextBox)cil.FindControl("ContactInformationControl_streetTextBox");
                TextBox txtCity = (TextBox)cil.FindControl("ContactInformationControl_cityTextBox");
                TextBox txtState = (TextBox)cil.FindControl("ContactInformationControl_stateTextBox");
                TextBox txtCountry = (TextBox)cil.FindControl("ContactInformationControl_countryNameTextBox");
                TextBox txtPostalCode = (TextBox)cil.FindControl("ContactInformationControl_postalCodeTextBox");
                TextBox txtFixedLine = (TextBox)cil.FindControl("ContactInformationControl_fixedLineTextBox");
                TextBox txtMobile = (TextBox)cil.FindControl("ContactInformationControl_mobileTextBox");
                TextBox txtEmail = (TextBox)cil.FindControl("ContactInformationControl_emailTextBox");
                TextBox txtWebSite = (TextBox)cil.FindControl("ContactInformationControl_websiteTextBox");
                // Set the TechnicalSkills Value         
                TechnicalSkillsControl oTechCntl = (TechnicalSkillsControl)SkillsMatrixControl_mainResumeControl.FindControl("MainResumeControl_technicalSkillsControl");
                TextBox txtLangauages = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_languageTextBox");
                TextBox txtOperaSystem = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_osTextBox");
                TextBox txtDatabases = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_databaseTextBox");
                TextBox txtUITools = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_uiToolsTextBox");
                TextBox txtOtherSkills = (TextBox)oTechCntl.FindControl("TechnicalSkillsControl_otherSkillsTextBox");

                XmlNode contactInfo = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
                XmlNode telephoneNode = contactInfo.SelectSingleNode("Telephone");
                txtMobile.Text = GetInnerText(telephoneNode, "FormattedNumber");

                XmlNode fixedLineNode = contactInfo.SelectSingleNode("Fax");
                txtFixedLine.Text = GetInnerText(fixedLineNode, "FormattedNumber");

                txtEmail.Text = GetInnerText(contactInfo, "InternetEmailAddress");
                txtWebSite.Text = GetInnerText(contactInfo, "InternetWebAddress");

                XmlNode postalAddress = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress");
                txtState.Text = GetInnerText(postalAddress, "State");
                txtAddress.Text = GetInnerText(postalAddress, "Street");
                txtCity.Text = GetInnerText(postalAddress, "Municipality");
                txtCountry.Text = GetInnerText(postalAddress, "CountryCode");
                txtPostalCode.Text = GetInnerText(postalAddress, "PostalCode");

                XmlNode techSkill = oXmlDoc.SelectSingleNode("/Resume/StructuredXMLResume/TechnicalSkills");
                txtLangauages.Text = GetInnerText(techSkill, "Languages");
                txtOperaSystem.Text = GetInnerText(techSkill, "OperatingSystem");
                txtDatabases.Text = GetInnerText(techSkill, "Databases");
                txtUITools.Text = GetInnerText(techSkill, "UITools");
                txtOtherSkills.Text = GetInnerText(techSkill, "OtherSkills");
            }
            // Set the ExecutiveSummary Value
            XmlNodeList oExecSummaryNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ExecutiveSummary");
            if (oExecSummaryNodeList.Count > 0)
            {
                ExecutiveSummaryControl oExecCntl = (ExecutiveSummaryControl)SkillsMatrixControl_mainResumeControl.FindControl("MainResumeControl_executiveSummaryControl");
                TextBox txtExecutiveSummary = (TextBox)oExecCntl.FindControl("ExecutiveSummaryControl_execSummaryTextBox");
                txtExecutiveSummary.Text = oExecSummaryNodeList.Item(0).InnerText;
            }
        }

        /// <summary>
        /// This method helps to extract the hrxml projects data and display its contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void SetProjects(XmlDocument oXmlDoc)
        {
            //Select the nodes that are in the project 
            XmlNodeList oProjectNodeList = oXmlDoc.SelectNodes
                ("/Resume/StructuredXMLResume/ProjectHistory/Project");
            if (oProjectNodeList.Count == 0)
            {
                return;
            }
            List<Project> oListProject = new List<Project>();
            int intProjectCnt = 0;

            foreach (XmlNode node in oProjectNodeList)
            {
                Project oProject = new Project();
                Location oLocation = new Location();
                intProjectCnt = intProjectCnt + 1;
                oProject.ProjectId = intProjectCnt;
                //Get the project name from the xml
                oProject.ProjectName = GetInnerText(node, "ProjectName");

                //Get the project description
                oProject.ProjectDescription = GetInnerText(node, "Description");

                //Get the role
                oProject.Role = GetInnerText(node, "Role");

                //Get the position title
                oProject.PositionTitle = GetInnerText(node, "Position");

                //Get the project start date and end date
                DateTime dtProjectStartDate, dtProjectEndDate;
                string startDate = GetInnerText(node, "StartDate");
                string endDate = GetInnerText(node, "EndDate");
                if (startDate.Length > 0)
                {
                    //Parse the string into datetime
                    if ((DateTime.TryParse(startDate, out dtProjectStartDate)))
                    {
                        oProject.StartDate = dtProjectStartDate;
                    }
                }

                if (endDate.Length > 0)
                {
                    if ((DateTime.TryParse(endDate, out dtProjectEndDate)))
                    {
                        oProject.EndDate = dtProjectEndDate;
                    }
                }

                //Get the location
                oLocation.City = GetInnerText(node, "Location");
                oProject.ProjectLocation = oLocation;

                //Get the environment
                oProject.Environment = GetInnerText(node, "Environment");

                //Get the client name
                oProject.ClientName = GetInnerText(node, "ClientName");

                //Get the client industry
                oProject.ClientIndustry = GetInnerText(node, "ClientIndustry");
                oListProject.Add(oProject);
            }

            ProjectsControl oProjCntl = (ProjectsControl)SkillsMatrixControl_mainResumeControl.
                FindControl("MainResumeControl_projectsControl");

            oProjCntl.DataSource = oListProject;
            oProjCntl.DataBind();
            SetProjectDate();
        }

        /// <summary>
        /// This method helps to set the date format empty in projects.
        /// </summary>
        private void SetProjectDate()
        {
            ProjectsControl oProjCntl = (ProjectsControl)SkillsMatrixControl_mainResumeControl.FindControl("MainResumeControl_projectsControl");
            ListView oProjectsControl_listView = (ListView)oProjCntl.FindControl("ProjectsControl_listView");
            foreach (ListViewDataItem item in oProjectsControl_listView.Items)
            {
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                if (txtProjStartDate.Text == "01/01/0001" || txtProjStartDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjStartDate.Text = string.Empty;
                }
                if (txtProjEndDate.Text == "01/01/0001" || txtProjEndDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjEndDate.Text = string.Empty;
                }
            }
        }


        /// <summary>
        /// Represents the method to get the inner text of the node
        /// </summary>
        /// <param name="parentNode">
        /// A<see cref="XmlNode"/>that holds the Xml Node
        /// </param>
        /// <param name="xpath">
        /// A<see cref="string"/>that holds the Xpath
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the string
        /// </returns>
        private string GetInnerText(XmlNode parentNode, string xpath)
        {
            XmlNode node1 = parentNode.SelectSingleNode(".");
            XmlNode xmlNode = node1.SelectSingleNode(xpath);

            //xmlNode = parentNode.FirstChild.SelectSingleNode(xpath);

            string innerXML = string.Empty;

            if (xmlNode != null)
            {
                innerXML = xmlNode.InnerText.Trim();
            }
            return innerXML;
        }

        #endregion
    }
}