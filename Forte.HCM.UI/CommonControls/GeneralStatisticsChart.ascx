﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralStatisticsChart.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GeneralStatisticsChart" %>
<%@ Register Src="~/CommonControls/SingleSeriesChartControl.ascx" TagName="SingleChartControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <asp:UpdatePanel ID = "GeneralStatisticsChart_updatePanel" runat="server" UpdateMode="Conditional"> 
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td align="left">
                        <div style="width: 100%; overflow: auto">
                            <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false"/>
                            <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" OnselectedIndexChanged="GeneralStatisticsChart_selectProperty"
                             OnClick="GeneralStatisticsChart_widgetMultiSelectControl_Click" OncancelClick="GeneralStatisticsChart_widgetMultiSelectControl_CancelClick"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%; height:100px">
                        <div style="width: 100%; float: left;">
                            <uc1:SingleChartControl ID="GeneralStatisticsChart_categoryChartControl" runat="server" />
                        </div>
                        <div style="width: 100%; float: left;">
                            <uc1:SingleChartControl ID="GeneralStatisticsChart_subjectChartControl" runat="server" />
                        </div>
                        <div style="width: 100%; float: left;">
                            <uc1:SingleChartControl ID="GeneralStatisticsChart_testAreaChartControl" runat="server" />
                        </div>
                        <div style="width: 100%; float: left;">
                            <uc1:SingleChartControl ID="GeneralStatisticsChart_complexityChartControl" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
       
    </asp:UpdatePanel>
</div>
