﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiSelectDropDownControl.ascx.cs"
    Inherits="Geval6.PRAS.UI.CommonControls.MultiSelectDropDownControl" %>

<script language="javascript" type="text/javascript">
    function ShowListBox(DivId) {
        if (document.getElementById(DivId).style.display == "none") {
            document.getElementById(DivId).style.display = "block";
        }
        else if (document.getElementById(DivId).style.display == "block") {
            document.getElementById(DivId).style.display = "none";
        }
    }

    function SelectedIndexChanged() {
        var control = document.getElementById('<%= CountriesListBox.ClientID %>');
        var strSelText = '';
        for (var i = 0; i < control.length; i++) {
            if (control.options[i].selected) {
                strSelText += control.options[i].text + ',';
            }
        }
        if (strSelText.length > 0)
            strSelText = strSelText.substring(0, strSelText.length - 1);
        var ddLabel = document.getElementById('<%=txtCountry.ClientID %>');
        ddLabel.value = strSelText;
    }

    function HideListBox(DivId) {
        document.getElementById(DivId).style.display = "none";
    }
</script>

<table cellspacing="0" cellpadding="0">
    <tr>
        <td style="width: 145px">
            <asp:TextBox ID="txtCountry" runat="server" Width="178px"></asp:TextBox>
        </td>
        <td align="left">
            <img src="../App_Themes/DefaultTheme/Images/DDImage.bmp" id="DdlImageButton" border="0"
                alt="img" style="height: 19px" onclick="javascript:ShowListBox('ListBoxPanel_Div')" />
        </td>
    </tr>
    <tr>
        <td align="left">
            <div id="ListBoxPanel_Div" style="display: none">
                <asp:ListBox ID="CountriesListBox" Name="CountriesListBox" runat="server" Width="178px"
                    SelectionMode="Multiple" onchange="SelectedIndexChanged();" onblur="HideListBox('ListBoxPanel_Div');">
                </asp:ListBox>
            </div>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
