﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestResulsCanditateControl.ascx.cs" Inherits="Forte.HCM.UI.CommonControls.TestResulsCanditateControl" %>
<%@ Register Src="~/CommonControls/MultipleSeriesChartControl.ascx" TagName="MultipleSeriesChartControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/HistogramChartControl.ascx" TagName="HistogramChartControl"
    TagPrefix="uc4" %>

    <table width="100%" cellpadding="0" cellspacing="4" border="0">
        <tr>
            <td colspan="2">
                <table width="100%" cellpadding="0" cellspacing="4" border="0">
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td style="width: 25%">
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionHeadLabel" runat="server" Text="Number Of Questions"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td style="width: 13%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_myScoreLabel" runat="server" Text="My Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_myScoreLabelValue" runat="server" SkinID="sknLabelFieldText"
                                Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionattenedLabel" runat="server" Text="Total Questions Attended"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionattenedLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td style="width: 13%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_absoluteScoreLabel" runat="server" Text="Absolute Score"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_absoluteScoreLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionSkippedLabel" runat="server" Text="Total Questions Skipped"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionSkippedLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td style="width: 13%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_relativeScoreLabel" runat="server" Text="Relative Score"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_relativeScoreLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionAnsweredCorrectlyLabel" runat="server" Text="Questions Answered Correctly"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionAnsweredCorrectlyLabelValue" runat="server"
                                SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td style="width: 13%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_percentileLabel" runat="server" Text="Percentile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_percentileLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionAnsweredWronglyLabel" runat="server" Text="Questions Answered Wrongly"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionAnsweredWronglyLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td style="width: 13%">
                        </td>
                        <td style="width: 30%">
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionTimeTakenLabel" runat="server" Text="Total Time Taken "
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionTimeTakenLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_percentageQuestionsAnsweredCorrectlyLabel" runat="server"
                                Text="Questions Answered Correctly (in %)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_percentageQuestionsAnsweredCorrectlyLabelValue" runat="server"
                                SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                        <td style="width: 13%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_avgTimeTakenLabel" runat="server" Text="Average Time Taken To Answer Each Question"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_avgTimeTakenLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 10%">
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_testStatusLabel" runat="server"
                                Text="Test Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="TestResulsCanditateControl_testStatusLabelValue" runat="server"
                                SkinID="sknLabelFieldTextBold"></asp:Label>
                        </td>
                        <td style="width: 13%">
                        </td>
                        <td>
                           
                        </td>
                        <td>
                           
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 50%">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="grid_header_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="header_text_bold">
                                        Test Scores
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="panel_body_bg">
                            <div style="height: 185px; overflow: auto;">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td align="left">
                                            <uc4:HistogramChartControl ID="TestResultsControl_histogramChartControl" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="grid_header_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="header_text_bold">
                                        Score Distribution Amongst Categories
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="panel_body_bg">
                            <div style="height: 185px; overflow: auto;">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td align="left" valign="bottom">
                                            <uc3:MultipleSeriesChartControl ID="TestResultControl_multipleSeriesChartControl"
                                                runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="grid_header_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="header_text_bold">
                                        Score Distribution Amongst Subjects
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="panel_body_bg">
                            <div style="height: 185px; overflow: auto;">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td align="left">
                                            <uc3:MultipleSeriesChartControl ID="MultipleSeriesChartControl_subjectChart" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="grid_header_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="header_text_bold">
                                        Score Distribution Amongst Test Areas
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="panel_body_bg">
                            <div style="height: 185px; overflow: auto;">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td align="left">
                                            <uc3:MultipleSeriesChartControl ID="MultipleSeriesChartControl_testAreaChart" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
      