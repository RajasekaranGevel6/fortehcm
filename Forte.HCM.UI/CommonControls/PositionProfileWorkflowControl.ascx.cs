﻿using System;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class PositionProfileWorkflowControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {    
               /*Basic Info - BI
                Detailed Info - DI
                Workflow Selection - WS
                Recruiter Assignment - RA
                Review - RW*/
            if (Request.QueryString["positionprofileid"] != null)
            {
                string positionprofileid = Request.QueryString["positionprofileid"];
                PositionProfileWorkflowControl_biAnchor.HRef = "../PositionProfile/PositionProfileBasicinfo.aspx?m=1&s=0&positionprofileid=" + positionprofileid + "&parentpage=PP_REVIEW";
                PositionProfileWorkflowControl_diAnchor.HRef = "../PositionProfile/PositionProfileDetailInfo.aspx?m=1&s=0&positionprofileid=" + positionprofileid + "&parentpage=PP_REVIEW";
                PositionProfileWorkflowControl_wsAnchor.HRef = "../PositionProfile/PositionProfileWorkflowSelection.aspx?m=1&s=0&positionprofileid=" + positionprofileid + "&parentpage=PP_REVIEW";
                PositionProfileWorkflowControl_raAnchor.HRef = "../PositionProfile/PositionProfileRecruiterAssignment.aspx?m=1&s=0&positionprofileid=" + positionprofileid + "&parentpage=PP_REVIEW";
                PositionProfileWorkflowControl_rwAnchor.HRef = "../PositionProfile/PositionProfileReview.aspx?m=1&s=0&positionprofileid=" + positionprofileid + "&parentpage=S_POS_PRO";
            }
            else
                PositionProfileWorkflowControl_biAnchor.HRef = "../PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0&parentpage=WF_LAND&index=1";
        }

        public string PageType
        {
            set 
            {
                switch(value)
                {
                    case"BI":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/step_01A_basicinfo.png";
                        tdBI.Attributes.Add("class", "pp_workflow_active_text");
                        break;
                    case "DI":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/step_02A_detailinfo.png";
                        tdDI.Attributes.Add("class", "pp_workflow_active_text");
                        break;
                    case "WS":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/step_03A_workflow.png";
                        tdWS.Attributes.Add("class", "pp_workflow_active_text");
                        break;
                    case "RA":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/step_04A_recruiter.png";
                        tdRA.Attributes.Add("class", "pp_workflow_active_text");
                        break;
                    case "RW":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/step_05A_review.png";
                        tdRW.Attributes.Add("class", "pp_workflow_active_text");
                        break;
                } 
            }
        } 
    }
}