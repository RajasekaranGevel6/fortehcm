﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestScheduleControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestScheduleControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="TestScheduleControl_questionResultLiteral" runat="server" Text="View Candidate Schedule"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="TestScheduleControl_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td align="left"  class="popup_td_padding_10">
                        <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_testSessionIdHeadLabel" runat="server" Text="Test Session ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_testSessionIdLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_candidateSessionIdHeadLabel" runat="server" Text="Candidate Session ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_candidateSessionIdLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="CreateTestSession_testKeyHeadLabel" runat="server" Text="Test ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_testIdLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="CreateTestSession_testNameHeadLabel" runat="server" Text="Test Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_testNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_candidateHeadLabel" runat="server" Text="Candidate Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_candidateLabel" runat="server" MaxLength="250" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_emailLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_recommendedTimeLabel" runat="server" Text="Time Limit"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_recommendedTimeLabelText" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_expiryDateLabel" runat="server" Text="Expiry Date" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_expiryDateLabelText" runat="server" MaxLength="10" AutoCompleteType="None"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_testAuthorHeadLabel" runat="server" Text="Test Author"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_testAuthorLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_displayResultsHeadLabel" runat="server" Text="Display Results to Candidate"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_displayResultsLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_cyberProctorateHeadLabel" SkinID="sknLabelFieldHeaderText"
                                        runat="server" Text="Cyber Proctoring"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSchedule_cyberProctorateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td colspan="2" align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="TestSchedule_testDescHeadLabel" runat="server" Text="Session Description"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="label_multi_field_text" style="height: 55px; width: 600px; overflow: auto;word-wrap: break-word;white-space:normal;">
                                        <asp:Literal ID="TestSchedule_testDescLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_2">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="TestSchedule_instructionsLabel" runat="server" Text="Instructions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="label_multi_field_text" style="height: 55px; width: 600px; overflow: auto;word-wrap: break-word;white-space:normal;">
                                        <asp:Literal ID="TestSchedule_instructionsLiteral" runat="server" SkinID="sknMultiLineText"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_2">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_5">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="TestScheduleControl_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                            runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
