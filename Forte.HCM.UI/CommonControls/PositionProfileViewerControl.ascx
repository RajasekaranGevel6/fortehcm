﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PositionProfileViewerControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.PositionProfileViewerControl" %>
<%@ Register Src="PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<table style="width: 100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tab_body_bg" style="width: 100%">
            <table style="width: 100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="PositionProfileViewerControl_searchDIV" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="panel_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="panel_inner_body_bg">
                                                    <table width="100%" cellpadding="2" cellspacing="3">
                                                        <tr>
                                                            <td>
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="PositionProfileViewerControl_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Position Profile"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="PositionProfileViewerControl_positionProfileTextBox" runat="server"
                                                                                    MaxLength="50" ReadOnly="true" />
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="PositionProfileViewerControl_candidateImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the candidate" />
                                                                                <asp:HiddenField ID="PositionProfileViewerControl_positionProfileIDHiddenField" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="PositionProfileViewerControl_activityByLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Created By"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="PositionProfileViewerControl_activityByTextBox" runat="server"
                                                                                    ReadOnly="true" MaxLength="50" />
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="PositionProfileViewerControl_activityByImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the activity user" />
                                                                                <asp:HiddenField ID="PositionProfileViewerControl_activityByHiddenField" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="PositionProfileViewerControl_activityTypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Activity"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <asp:DropDownList ID="PositionProfileViewerControl_activityTypeDropDownList" runat="server"
                                                                                Width="100%" SkinID="sknSubjectDropDown">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="PositionProfileViewerControl_activityDateFromLabel" runat="server"
                                                                                Text="Date From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="PositionProfileViewerControl_activityDateFromTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="PositionProfileViewerControl_activityDateFromCalenderImageButton"
                                                                                    SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="PositionProfileViewerControl_activityDateFromMaskedEditExtender"
                                                                                runat="server" TargetControlID="PositionProfileViewerControl_activityDateFromTextBox"
                                                                                Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="PositionProfileViewerControl_activityDateFromMaskedEditValidator"
                                                                                runat="server" ControlExtender="PositionProfileViewerControl_activityDateFromMaskedEditExtender"
                                                                                ControlToValidate="PositionProfileViewerControl_activityDateFromTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="PositionProfileViewerControl_activityDateFromCustomCalendarExtender"
                                                                                runat="server" TargetControlID="PositionProfileViewerControl_activityDateFromTextBox"
                                                                                CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="PositionProfileViewerControl_activityDateFromCalenderImageButton" />
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="PositionProfileViewerControl_activityDateToLabel" runat="server"
                                                                                Text="Date To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="PositionProfileViewerControl_activityDateToTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="PositionProfileViewerControl_activityDateToCalenderImageButton"
                                                                                    SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="PositionProfileViewerControl_activityDateToMaskedEditExtender"
                                                                                runat="server" TargetControlID="PositionProfileViewerControl_activityDateToTextBox"
                                                                                Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="PositionProfileViewerControl_activityDateToMaskedEditValidator"
                                                                                runat="server" ControlExtender="PositionProfileViewerControl_activityDateToMaskedEditExtender"
                                                                                ControlToValidate="PositionProfileViewerControl_activityDateToTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="PositionProfileViewerControl_activityDateToCalendarExtender"
                                                                                runat="server" TargetControlID="PositionProfileViewerControl_activityDateToTextBox"
                                                                                CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="PositionProfileViewerControl_activityDateToCalenderImageButton" />
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="PositionProfileViewerControl_commentsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Comments"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <asp:TextBox ID="PositionProfileViewerControl_commentsTextBox" runat="server" Width="97%"
                                                                                MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="PositionProfileViewerControl_searchButton" runat="server" SkinID="sknButtonId"
                                                        Text="Search" OnClick="PositionProfileViewerControl_searchButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="PositionProfileViewerControl_gridViewUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div id="PositionProfileViewerControl_gridViewDIV" runat="server">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr id="PositionProfileViewerControl_gridViewHeaderTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="SearchCandidateRepository_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                ID="PositionProfileViewerControl_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 2%" align="right">
                                                            <span id="PositionProfileViewerControl_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="PositionProfileViewerControl_searchResultsUpImage" runat="server"
                                                                    SkinID="sknMinimizeImage" /></span><span id="PositionProfileViewerControl_searchResultsDownSpan"
                                                                        runat="server" style="display: block;"><asp:Image ID="PositionProfileViewerControl_searchResultsDownImage"
                                                                            runat="server" SkinID="sknMaximizeImage" /></span>
                                                            <asp:HiddenField ID="PositionProfileViewerControl_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <div id="PositionProfileViewerControl_activityLogDetailDIV" runat="server" style="height: 225px;
                                                    overflow: auto;">
                                                    <asp:GridView ID="PositionProfileViewerControl_activityLogDetailGridView" runat="server"
                                                        AllowSorting="true" AutoGenerateColumns="false" OnSorting="PositionProfileViewerControl_activityLogDetailGridView_Sorting"
                                                        OnRowCreated="PositionProfileViewerControl_activityLogDetailGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Activity Date" SortExpression="ActivityDate DESC" HeaderStyle-Width="18%"
                                                                ItemStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="PositionProfileViewerControl_activityLogDetailGridView_activityDateLabel"
                                                                        runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("ActivityDate")))%>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activity By" SortExpression="ActivityBy" HeaderStyle-Width="18%"
                                                                ItemStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="PositionProfileViewerControl_activityLogDetailGridView_activityByLabel"
                                                                        runat="server" Text='<%# Eval("ActivityByName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activity" SortExpression="ActivityType" ItemStyle-Width="18%"
                                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="PositionProfileViewerControl_activityLogDetailGridView_activityTypeLabel"
                                                                        runat="server" Text='<%# Eval("ActivityTypeName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Comments" SortExpression="Comments" ItemStyle-Width="18%"
                                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="PositionProfileViewerControl_activityLogDetailGridView_commentsLabel"
                                                                        runat="server" Text='<%# Eval("Comments") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="PositionProfileViewerControl_pageNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:HiddenField ID="PositionProfileViewerControl_gridPageSizeHiddenField" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
