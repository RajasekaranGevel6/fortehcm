﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestDetailControl.ascx.cs
// File that represents the user interface for the test details
// control

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{

    /// <summary>
    /// Class that represents the user control interface layout and functionalities
    /// for the test details. This page is used to view the test and certification
    /// details.
    /// </summary>
    public partial class InterviewTestDetailsControl : UserControl
    {
        #region Events                                                         
       
        protected void Page_Load(object sender, EventArgs e)
        {
           
            
            if (IsPostBack)
                return;
          
            // Add handler for position profile search icon.
            InterviewTestDetailsControl_positionProfileImageButton.Attributes.Add("onclick",
               "javascript:return LoadPositionProfileName('" + InterviewTestDetailsControl_positionProfileTextBox.ClientID + "','" +
               InterviewTestDetailsControl_positionProfileIDHiddenField.ClientID + "');");
        
            //InterviewTestDetailsControl_CertificationDiv.Visible = false;
        }

        #endregion Events

        #region Properties                                                     

        #region Datasource Property                                            

        public TestDetail TestDetailDataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                InterviewTestDetailsControl_testDetailsTestIdReadOnlyLabel.Text = value.TestKey;
                InterviewTestDetailsControl_testNameTextBox.Text = value.Name;
                InterviewTestDetailsControl_testDescriptionTextBox.Text = value.Description;
               // InterviewTestDetailsControl_recommendedTimeLabelTextBox.Text = Utility.ConvertSecondsToHoursMinutesSeconds(value.RecommendedCompletionTime).ToString();
               // InterviewTestDetailsControl_sysRecommendedTimeFieldLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds(value.SystemRecommendedTime).ToString();
                InterviewTestDetailsControl_positionProfileIDHiddenField.Value = value.PositionProfileID.ToString();
                InterviewTestDetailsControl_positionProfileTextBox.Text = value.PositionProfileName;

                if (Utility.IsNullOrEmpty(value.CertificationDetail))
                    return;
           
            }
            get
            {
                TestDetail testDetail = new TestDetail();
                testDetail.Name = InterviewTestDetailsControl_testNameTextBox.Text;
                testDetail.Description = InterviewTestDetailsControl_testDescriptionTextBox.Text;

                if (!Utility.IsNullOrEmpty(InterviewTestDetailsControl_positionProfileIDHiddenField.Value))
                {
                    testDetail.PositionProfileID = Convert.ToInt32(InterviewTestDetailsControl_positionProfileIDHiddenField.Value);
                }

                if (!Utility.IsNullOrEmpty(InterviewTestDetailsControl_positionProfileTextBox.Text))
                {
                    testDetail.PositionProfileName = InterviewTestDetailsControl_positionProfileTextBox.Text;
                }

               /* DateTime timeLimit;
                if (Utility.IsNullOrEmpty(InterviewTestDetailsControl_recommendedTimeLabelTextBox.Text.Trim()))
                {
                    testDetail.RecommendedCompletionTime = -1;
                }
                else if (DateTime.TryParse(InterviewTestDetailsControl_recommendedTimeLabelTextBox.Text, out timeLimit) == false)
                {
                    testDetail.RecommendedCompletionTime = -2;
                }
                else if (Utility.ConvertHoursMinutesSecondsToSeconds(InterviewTestDetailsControl_recommendedTimeLabelTextBox.Text) < Constants.General.DEFAULT_TEST_MINIMUM_TIME)
                {
                    testDetail.RecommendedCompletionTime = 0;
                }
                else
                {
                    testDetail.RecommendedCompletionTime = Utility.ConvertHoursMinutesSecondsToSeconds(InterviewTestDetailsControl_recommendedTimeLabelTextBox.Text);
                }*/

               // testDetail.SystemRecommendedTime = Utility.ConvertHoursMinutesSecondsToSeconds(InterviewTestDetailsControl_sysRecommendedTimeFieldLabel.Text);
            
               
               
                return testDetail;
            }
        }

        #endregion Datasource Property


        /// <summary>
        /// Property that sets the position profile name.
        /// </summary>
        public string PositionProfileName
        {
            set
            {
                InterviewTestDetailsControl_positionProfileTextBox.Text = value;
            }
        }

        /// <summary>
        /// Property that sets the position profile ID.
        /// </summary>
        public int PositionProfileID
        {
            set
            {
                InterviewTestDetailsControl_positionProfileIDHiddenField.Value = value.ToString();
            }
        }

   

        public int SysRecommendedTime
        {
            set
            {
             //   InterviewTestDetailsControl_sysRecommendedTimeFieldLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds(value);
            }
        }
        public Boolean ReadOnly
        {
            set
            {
                //InterviewTestDetailsControl_testDetailsTestIdReadOnlyLabel.ReadOnly = value;
                //InterviewTestDetailsControl_testDetailsTestIdTextBox.SkinID = "sknReadOnlyTextBox";
                InterviewTestDetailsControl_testNameTextBox.ReadOnly = value;
                InterviewTestDetailsControl_testDescriptionTextBox.ReadOnly = value;
             //   InterviewTestDetailsControl_recommendedTimeLabelTextBox.ReadOnly = value;
           

            }
        }

        #endregion Properties

        #region Private Methods                                                

      

        #endregion Private Methods
    }
}