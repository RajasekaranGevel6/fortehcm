﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionDetailPreviewControl.cs
// File that represents the user interface for question preview.
// User can preview the currently selected question information over here.

#endregion Header

#region Directives                                                             

using System;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class QuestionDetailSummaryControl : UserControl
    {
        #region Event Handlers                                                 

        /// <summary>
        /// This event handler instantiate the modal popup control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (!IsPostBack)
                {
                    if (ShowAddButton)
                        QuestionDetailSummaryControl_bottomAddButton.Visible = true;
                    else
                        QuestionDetailSummaryControl_bottomAddButton.Visible = false;
                    //ModalPopupExtender objModalPopupExtender =
                    //    (ModalPopupExtender)this.Page.FindControl("QuestionDetailSummaryControl_questionModalPopupExtender");

                    //if (objModalPopupExtender != null)
                    //{
                    //    objModalPopupExtender.CancelControlID =
                    //        QuestionDetailSummaryControl_topCancelImageButton.ClientID;
                    //}
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        public delegate void Button_Click(object sender, EventArgs e);
        public delegate void Add_Click(object sender, EventArgs e);
        public event Button_Click CancelClick;
        public event Add_Click AddClick;
        /// <summary>
        /// Helps to close question preview popup window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void QuestionDetailSummaryControl_bottomCloseLinkButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        protected void QuestionDetailSummaryControl_bottomAddButton_Click(object sender, EventArgs e)
        {
            if (AddClick != null)
                this.AddClick(sender, e);
        }

        /// <summary>
        /// Helps to cancel/close the question preview popup window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void QuestionDetailSummaryControl_topCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        #endregion Event Handlers

        #region Public properties                                              

        public string Title
        {
            set
            {
                QuestionDetailSummaryControl_questionResultLiteral.Text = value.Trim();
            }
        }

        public bool ShowAddButton { set; get; }

        /// <summary>
        /// This data source helps to load the question information while user wants to preview
        /// in single or edit question entry page
        /// </summary>
        public QuestionDetail QuestionDatasource
        {
            set
            {
                if (value == null)
                    return;
                if (value.Answer > 0)
                {
                    // Set correct answer.
                    value.AnswerChoices[value.Answer - 1].IsCorrect = true;
                }

                // Assign choices to the placeholder control
                QuestionDetailSummaryControl_answerChoicesPlaceHolder.Controls.Add
                    (new ControlUtility().GetAnswerChoices(value.AnswerChoices,false));

                // Assign subjects which are having selected status is true
                List<Subject> subjects = null;
                subjects = new List<Subject>();

                if (value.Subjects != null)
                {
                    // This loop will fetch the subjects which selected by the user.
                    foreach (Subject subject in value.Subjects)
                    {
                        Subject selectedSubjectList = new Subject();
                        if (subject.IsSelected == true)
                        {
                            selectedSubjectList.CategoryID = subject.CategoryID;
                            selectedSubjectList.CategoryName = subject.CategoryName;
                            selectedSubjectList.SubjectID = subject.SubjectID;
                            selectedSubjectList.SubjectName = subject.SubjectName;
                            selectedSubjectList.IsSelected = true;
                            subjects.Add(selectedSubjectList);
                        }
                    }

                    // Bind the subjects to the grid
                    QuestionDetailSummaryControl_categoryDataGrid.DataSource = subjects;
                    QuestionDetailSummaryControl_categoryDataGrid.DataBind();
                }
                else
                {
                    // This empty datasource gets assigned for showing question details with only category
                    // and subject title in the datagrid control when no category and subject is selected.
                    QuestionDetailSummaryControl_categoryDataGrid.DataSource = new List<Subject>();
                    QuestionDetailSummaryControl_categoryDataGrid.DataBind();
                }

                // Additional options
                QuestionDetailSummaryControl_testAreaValueLabel.Text = value.TestAreaName;
                QuestionDetailSummaryControl_complexityValueLabel.Text = value.ComplexityName;
                QuestionDetailSummaryControl_tagValueLabel.Text = value.Tag;
                QuestionDetailSummaryControl_questionLabel.Text =
                    value.Question == null ? value.Question : value.Question.ToString().Replace(Environment.NewLine, "<br />");

                // Display question image
                if (value.HasImage)
                {
                    Session["POSTED_QUESTION_IMAGE"] = value.QuestionImage as byte[];
                    QuestionDetailSummaryControl_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE&question_key=" + value.QuestionKey;
                    QuestionDetailSummaryControl_questionImage.Visible = true;
                    QuestionDetailSummaryControl_questionDiv.Style.Add("height", "120px");
                }
                else
                {
                    QuestionDetailSummaryControl_questionImage.Visible = false;
                    QuestionDetailSummaryControl_questionDiv.Style.Add("height", "100%");
                }
            }
            
        }

        /// <summary>
        /// Get the question detail based on QuestionKey.
        /// </summary>
        /// <param name="questionKey"></param>
        public void LoadQuestionDetails(string questionKey,int questionRelationId)
        {
            if (ShowAddButton)
                QuestionDetailSummaryControl_bottomAddButton.Visible = true;
            else
                QuestionDetailSummaryControl_bottomAddButton.Visible = false;
            
            // Initialize question detail object by calling BL method.
            QuestionDetail questionDetail = new QuestionBLManager().GetQuestion(questionKey);

            List<Subject> subjectList = new List<Subject>();
            subjectList = new CommonBLManager().GetCategorySubjects(questionKey, questionRelationId);
            
            // Assign datasource to question preview control's datasource.
            QuestionDetailSummaryControl_categoryDataGrid.DataSource = subjectList;
            QuestionDetailSummaryControl_categoryDataGrid.DataBind();

            // Assign additional setttings
            QuestionDetailSummaryControl_testAreaValueLabel.Text = questionDetail.TestAreaName;
            QuestionDetailSummaryControl_complexityValueLabel.Text = questionDetail.ComplexityName;
            QuestionDetailSummaryControl_tagValueLabel.Text = questionDetail.Tag;
            QuestionDetailSummaryControl_questionLabel.Text =
                questionDetail.Question == null ? questionDetail.Question : questionDetail.Question.ToString().Replace(Environment.NewLine, "<br />");
                
            QuestionDetailSummaryControl_noOfAdministeredTestValueLabel.Text = 
                questionDetail.Administered.ToString();
            QuestionDetailSummaryControl_avgTimeTakenValueLabel.Text = 
                Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(questionDetail.AverageTimeTaken));

            if (questionDetail.RatioOfCorrectAnswerToAttended != null)
            {
                QuestionDetailSummaryControl_ratioCorrectAnswerValueLabel.Text =
                    questionDetail.RatioOfCorrectAnswerToAttended.ToString();
            }
            else
                QuestionDetailSummaryControl_ratioCorrectAnswerValueLabel.Text = "";

            // Set answer choice
            QuestionDetailSummaryControl_answerChoicesPlaceHolder.DataBind();
            if (questionDetail.AnswerChoices != null)
            {
                QuestionDetailSummaryControl_answerChoicesPlaceHolder.Controls.Add
                (new ControlUtility().GetAnswerChoices(questionDetail.AnswerChoices, false));
            }
            // Display question image
            if (questionDetail.HasImage)
            {
                Session["POSTED_QUESTION_IMAGE"] = questionDetail.QuestionImage as byte[];
                QuestionDetailSummaryControl_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE&question_key=" + questionDetail.QuestionKey;
                QuestionDetailSummaryControl_questionImage.Visible = true;
                QuestionDetailSummaryControl_questionDiv.Style.Add("height", "120px");
            }
            else
            {
                QuestionDetailSummaryControl_questionImage.Visible = false;
                QuestionDetailSummaryControl_questionDiv.Style.Add("height", "100%");
            }
        }

        #endregion Public Properties
    }
}