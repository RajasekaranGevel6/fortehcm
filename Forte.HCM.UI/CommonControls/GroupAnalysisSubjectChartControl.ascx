﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupAnalysisSubjectChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GroupAnalysisSubjectChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultiReport"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="GroupAnalysisSubjectChartControl_updatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div style="width: 100%; overflow: auto">
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="GroupAnalysisSubjectChartControl_testKeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="GroupAnalysisSubjectChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="GroupAnalysisSubjectChartControl_selectProperty"
                                            OnClick="GroupAnalysisSubjectChartControl_widgetMultiSelectControl_Click"
                                             OncancelClick="GroupAnalysisSubjectChartControl_widgetMultiSelectControl_CancelClick"/>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="GroupAnalysisSubjectChartControl_selectScoreLabel" runat="server"
                                                    Text="Score Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisSubjectChartControl_absoluteScoreRadioButton" runat="server"
                                                    Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="true" OnCheckedChanged="GroupAnalysisSubjectChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisSubjectChartControl_relativeScoreRadioButton" runat="server"
                                                    Text=" Relative Score" GroupName="1" AutoPostBack="true" OnCheckedChanged="GroupAnalysisSubjectChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td align="left">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <uc1:MultiReport ID="GroupAnalysisSubjectChartControl_multiSeriesChart" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
