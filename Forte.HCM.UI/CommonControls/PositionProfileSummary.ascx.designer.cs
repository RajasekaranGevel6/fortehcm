﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class PositionProfileSummary {
        
        /// <summary>
        /// PositionProfileSummary_questionResultLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PositionProfileSummary_questionResultLiteral;
        
        /// <summary>
        /// PositionProfileSummary_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileSummary_topCancelImageButton;
        
        /// <summary>
        /// PositionProfileSummary_errorLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label PositionProfileSummary_errorLabel;
        
        /// <summary>
        /// PositionProfileSummary_skillKeywordGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView PositionProfileSummary_skillKeywordGridView;
        
        /// <summary>
        /// PositionProfileSummary_addRowLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton PositionProfileSummary_addRowLinkButton;
    }
}
