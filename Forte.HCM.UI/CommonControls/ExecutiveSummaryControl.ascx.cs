﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExecutiveSummaryControl.cs
// File that represents the user interface for the executive summary information details

#endregion Header

#region Directives                                                             

using System;
using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ExecutiveSummaryControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExecutiveSummaryControl_plusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
                      ExecutiveSummaryControl_controlsDiv.ClientID + "','" + ExecutiveSummaryControl_plusSpan.ClientID + "','" +
                      ExecutiveSummaryControl_minusSpan.ClientID + "','" + ExecutiveSummaryControl_hiddenField.ClientID + "')");
            ExecutiveSummaryControl_minusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
               ExecutiveSummaryControl_controlsDiv.ClientID + "','" + ExecutiveSummaryControl_plusSpan.ClientID + "','" +
               ExecutiveSummaryControl_minusSpan.ClientID + "','" + ExecutiveSummaryControl_hiddenField.ClientID + "')");

            // 
            if (!Utility.IsNullOrEmpty(ExecutiveSummaryControl_hiddenField.Value) &&
                ExecutiveSummaryControl_hiddenField.Value == "Y")
            {
                ExecutiveSummaryControl_controlsDiv.Style["display"] = "none";
                ExecutiveSummaryControl_plusSpan.Style["display"] = "block";
                ExecutiveSummaryControl_minusSpan.Style["display"] = "none";

            }
            else
            {
                ExecutiveSummaryControl_controlsDiv.Style["display"] = "block";
                ExecutiveSummaryControl_plusSpan.Style["display"] = "none";
                ExecutiveSummaryControl_minusSpan.Style["display"] = "block";
            }
        }
        public string DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                ExecutiveSummaryControl_execSummaryTextBox.Text = value;
            }
        }
    }
}