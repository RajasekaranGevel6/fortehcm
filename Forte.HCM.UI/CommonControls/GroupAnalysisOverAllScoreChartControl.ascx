﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupAnalysisOverAllScoreChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GroupAnalysisOverAllScoreChartControl" %>
<%@ Register Src="~/CommonControls/SingleSeriesReportChartControl.ascx" TagName="ReportChartControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
 <div style="width: 100%; overflow: auto">
    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false"/>
    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
 </div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="GroupAnalysisOverAllScoreChartControl_updatePanel" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="GroupAnalysisOverAllScoreChartControl_selectScoreLabel" runat="server"
                                                    SkinID="sknLabelFieldHeaderText" Text="Score Type"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisOverAllScoreChartControl_absoluteScoreRadioButton"
                                                    runat="server" Checked="true" GroupName="1" Text=" Absolute Score" AutoPostBack="True"
                                                    OnCheckedChanged="GroupAnalysisOverAllScoreChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisOverAllScoreChartControl_relativeScoreRadioButton"
                                                    runat="server" GroupName="1" Text=" Relative Score" 
                                                    AutoPostBack="true" OnCheckedChanged="GroupAnalysisOverAllScoreChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td align="right">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <uc1:ReportChartControl ID="GroupAnalysisOverAllScoreChartControl_scoreChart" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
