﻿#region Namespace                                                              

using System;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using System.Web.UI;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

using Resources;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class LicenseAgreementHeaderControl : UserControl
    {
        #region Variables                                                       

        public string homeUrl = string.Empty;

        #endregion Variables

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 3)
                {
                    // Compose home url.
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Default.aspx";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Events

        #region Public Methods                                                 

        public List<string> BreadCrumbText
        {
            set
            {
               
            }
        }

        #endregion Public Methods
    }
}