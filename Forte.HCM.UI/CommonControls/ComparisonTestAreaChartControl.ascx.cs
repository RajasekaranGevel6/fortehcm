﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ComparisonTestAreaChartControl.cs
// File that represents class for the ComparisonTestArea Chart Control. 
// It displays the chart for the test area in the comparison report page

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ComparisonTestAreaChartControl : UserControl, IWidgetControl
    {
        string testkey = string.Empty;
       
        List<DropDownItem> dropDownItemList = null;

        #region Event Handlers                                                 

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                ComparisonTestAreaChartControl_hiddenField.Value = selectedProperty[2];
                ComparisonTestAreaChartControl_testKeyHiddednfield.Value = selectedProperty[0];
                ComparisonTestAreaChartControl_absoluteRadioButton.Checked = selectedProperty[3] == "0" ? false : true;
                ComparisonTestAreaChartControl_relativeRadioButton.Checked = selectedProperty[3] == "1" ? false : true;
                ComparisonTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[1]);
                ComparisonTestAreaChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
        }               

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on radio button 
        ///  button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonTestAreaChartControl_absoluteRadioButton_CheckedChanged
           (object sender, EventArgs e)
        {
            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }

        /// <summary>
        /// Hanlder method that will be called when the select
        /// link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonTestAreaChartControl_selectDisplayTypeLinkButton_Click
            (object sender, EventArgs e)
        {
            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }
        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void ComparisonTestAreaChartControl_selectProperty(object sender, EventArgs e)
        {
            try
            {
                ComparisonTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(false);

                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on multi select link button added
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonTestAreaChartControl_widgetMultiSelectControl_Click(
            object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Hanlder method that will be called on multi select link button added
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonTestAreaChartControl_widgetMultiSelectControl_CancelClick(
            object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Event Handlers

        #region Private Method                                                 
        /// <summary>
        /// Represents the method to get the chart details from the database
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            ComparisonTestAreaChartControl_testKeyHiddednfield.Value = candidateReportDetail.TestKey;
            DataTable multiSeriesDataTable = new ReportBLManager().
                GetComparisonReportTestAreaDetails(candidateReportDetail);

            var subject = (from r in multiSeriesDataTable.AsEnumerable()
                           select new DropDownItem(r["Name"].ToString().Trim()
                              , r["ID"].ToString())).Distinct();

            ComparisonTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = subject.ToList();

            //Select all the check box in the check box list
            ComparisonTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

            Session["TestAreaChartDataSource"] = multiSeriesDataTable;

            LoadChart(instance,"1");
        }

        /// <summary>
        /// Represents the method to load the chart 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChart(WidgetInstance instance, string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            testkey = ComparisonTestAreaChartControl_testKeyHiddednfield.Value;
            selectedPrintProperty.Append(testkey + "|");
            string selectedTestAreaIds = string.Empty;
            StringBuilder selectedTestAreaId = new StringBuilder();
            DataTable categoryChartDataTable = null;

            if ((Session["TestAreaChartDataSource"]) != null)
            {
                categoryChartDataTable = Session["TestAreaChartDataSource"]
                    as DataTable;
            }
            DataTable selectedChartData = categoryChartDataTable.Clone();

            List<DropDownItem> selectedList = ComparisonTestAreaChartControl_widgetMultiSelectControl.SelectedItems();

            //categoryList = WidgetMultiSelectControl.SelectedProperties;

            if (selectedList != null && selectedList.Count != 0)
            {
                foreach (DropDownItem item in selectedList)
                {
                    if ((Session["TestAreaChartDataSource"]) != null)
                    {
                        selectedTestAreaId.Append(item.ValueText);
                        selectedTestAreaId.Append("-");
                        //select the datarow for the corresponding category id
                        DataRow[] datarow = (Session["TestAreaChartDataSource"] as DataTable).
                            Select("ID='" + item.ValueText.ToString().Trim() + "'");

                        //DataRow dr = selectedChartData.NewRow();                       
                        //dr.ItemArray = datarow;
                        selectedChartData.Rows.Add(datarow[0].ItemArray);
                    }
                }
                categoryChartDataTable = selectedChartData;
            }
            else
            {
                categoryChartDataTable = null;
            }

            MultipleSeriesChartData categoryChartDataSource = new MultipleSeriesChartData();

            categoryChartDataSource.ChartType = SeriesChartType.Column;

            categoryChartDataSource.ChartLength = 300;

            categoryChartDataSource.ChartWidth = 350;

            categoryChartDataSource.IsDisplayAxisTitle = true;

            categoryChartDataSource.IsDisplayChartTitle = false;

            categoryChartDataSource.XAxisTitle = "Test Areas";

            categoryChartDataSource.YAxisTitle = "Scores";

            categoryChartDataSource.ChartTitle = "Candidate's Score Amongst TestAreas";

            categoryChartDataSource.IsDisplayChartTitle = false;

            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                categoryChartDataSource.IsShowLabel = true;
            }

            dropDownItemList = new List<DropDownItem>();
            selectedTestAreaIds = selectedTestAreaId.ToString().TrimEnd('-');
            selectedPrintProperty.Append(selectedTestAreaIds.Replace('-', ',') + "|");

            if (ComparisonTestAreaChartControl_hiddenField.Value == "1")
            {
                selectedPrintProperty.Append("1|");
                ComparisonTestAreaChartControl_selectDisplayTypeLinkButton.Text
                    = "Hide Comparative Score";
                categoryChartDataSource.IsHideComparativeScores = false;

                if (ComparisonTestAreaChartControl_absoluteRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) && (col.ColumnName.Substring(0, 4) == "Rel-"))
                        {
                            columnId.Add(col.ColumnName);
                        }
                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    categoryChartDataTable = selectedChartData;

                    dropDownItemList.Add(new DropDownItem("Title", "Test Area Comparative Score"));

                    dropDownItemList.Add(new DropDownItem("Subjects",
                        Constants.ChartConstants.COMPARISON_REPORT_TESTAREA_COMPARATIVE_ABSOLUTE +
                        "-" + testkey + "-" + selectedTestAreaIds + ".png"));

                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_REPORT_TESTAREA_COMPARATIVE_ABSOLUTE + "-" +
                        testkey + "-" + selectedTestAreaIds;

                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) != "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            columnId.Add(col.ColumnName);
                        }

                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) == "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            col.ColumnName = col.ColumnName.Remove(0, 4);
                        }
                    }
                    categoryChartDataTable = selectedChartData;

                    dropDownItemList.Add(new DropDownItem("Title", "Category Comparative Relative Score"));

                    dropDownItemList.Add(new DropDownItem("Categories",
                        Constants.ChartConstants.COMPARISON_REPORT_TESTAREA_COMPARATIVE_RELATIVE +
                        "-" + testkey + "-" + selectedTestAreaIds + ".png"));

                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_REPORT_TESTAREA_COMPARATIVE_RELATIVE + "-" +
                        testkey + "-" + selectedTestAreaIds;
                }
            }
            else
            {
                selectedPrintProperty.Append("0|");
                ComparisonTestAreaChartControl_selectDisplayTypeLinkButton.Text
                    = "Show Comparative Score";
                categoryChartDataSource.IsHideComparativeScores = true;

                if (ComparisonTestAreaChartControl_absoluteRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) && (col.ColumnName.Substring(0, 4) == "Rel-"))
                        {
                            columnId.Add(col.ColumnName);
                        }
                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    categoryChartDataTable = selectedChartData;


                    dropDownItemList.Add(new DropDownItem("Title", "Subject Absolute Score"));

                    dropDownItemList.Add(new DropDownItem("Subject",
                        Constants.ChartConstants.COMPARISON_REPORT_TESTAREA_ABSOLUTE +
                        "-" + testkey + "-" + selectedTestAreaIds + ".png"));

                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_REPORT_TESTAREA_ABSOLUTE + "-" +
                        testkey + "-" + selectedTestAreaIds;


                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) != "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            columnId.Add(col.ColumnName);
                        }
                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) == "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            col.ColumnName = col.ColumnName.Remove(0, 4);
                        }
                    }
                    categoryChartDataTable = selectedChartData;

                    dropDownItemList.Add(new DropDownItem("Title", "Category Relative Score"));

                    dropDownItemList.Add(new DropDownItem("Categories",
                        Constants.ChartConstants.COMPARISON_TESTAREA_SUBJECT_RELATIVE +
                        "-" + testkey + "-" + selectedTestAreaIds + ".png"));

                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_TESTAREA_SUBJECT_RELATIVE + "-" +
                        testkey + "-" + selectedTestAreaIds;
                }
            }
            categoryChartDataSource.ComparisonReportDataTable = categoryChartDataTable;
            categoryChartDataSource.IsComparisonReport = true;
            ComparisonTestAreaChartControl_multiSeriesChart.MultipleChartDataSource =
              categoryChartDataSource;
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        #endregion Private Method

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>   
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

            //Add attributes for the show or hide link button
            ComparisonTestAreaChartControl_selectDisplayTypeLinkButton.Attributes.
                Add("onclick", "javascript:return ShowOrHideComparativeScore('" +
                ComparisonTestAreaChartControl_selectDisplayTypeLinkButton.ClientID + "','"
                + ComparisonTestAreaChartControl_hiddenField.ClientID + "');");

        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}