﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComparisonSubjectChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ComparisonSubjectChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultiSeriesChart"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="ComparisonSubjectChartControl_updatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div style="width: 100%; overflow: auto">
                                     <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                     <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                     <asp:HiddenField ID="ComparisonSubjectChartControl_testkeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="ComparisonSubjectChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="ComparisonSubjectChartControl_selectProperty"
                                            OnClick="ComparisonSubjectChartControl_widgetMultiSelectControl_Click" 
                                            OncancelClick="ComparisonSubjectChartControl_widgetMultiSelectControl_CancelClick" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:Label ID="ComparisonSubjectChartControl_selectScoreLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonSubjectChartControl_absoluteScoreRadioButton" runat="server"
                                                    Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="True" OnCheckedChanged="ComparisonSubjectChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonSubjectChartControl_relativeScoreRadioButton" runat="server"
                                                    Text=" Relative Score" GroupName="1" AutoPostBack="True" OnCheckedChanged="ComparisonSubjectChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:LinkButton ID="ComparisonSubjectChartControl_selectDisplayLinkButton" runat="server"
                                                    Text="Show Comparative Score" SkinID="sknActionLinkButton" OnClick="ComparisonSubjectChartControl_selectDisplayLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="ComparisonSubjectChartControl_hiddenField" runat="server" Value="0" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <uc1:MultiSeriesChart ID="ComparisonSubjectChartControl_multiSeriesChartControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
