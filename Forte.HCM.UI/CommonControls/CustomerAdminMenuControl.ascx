﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerAdminMenuControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CustomerAdminMenuControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="menustrip_normal">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" style="display:none" id="tdAdmin"  onmouseover="showit(0)" onmouseout="resetit(event)">
                        Admin
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdEnroll" onmouseover="showit(1)" onmouseout="resetit(event)">
                        Enroll
                    </td>
                    <%--<td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="td1" onmouseover="showit(2)" onmouseout="resetit(event)">
                        Candidate
                    </td>--%>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="menustrip_sublink">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="4%">
                        &nbsp;
                    </td>
                    <td align="center" valign="middle" id="describe" onmouseover="clear_delayhide(true)"
                        onmouseout="resetit(event)">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="CustomerAdminMenuControl_hoverMenuHiddenField" runat="server" />
<asp:HiddenField ID="CustomerAdminMenuControl_selectedMenuHiddenField" runat="server" />
<asp:HiddenField ID="CustomerAdminMenuControl_selectedSubMenuHiddenField" runat="server" />
<script type="text/javascript" language="javascript">
    var submenu = new Array();
    var test_url = '<%= absUrl %>';

    submenu[0] = '<div id="div0"><table border="0" cellspacing="0" cellpadding="0"><tr><td> &nbsp;</td><td id="td01" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/CorporateUserManagement.aspx?m=0&s=0">Corporate User Management</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td02" class="menustrip_sublink_text"><a href="' + test_url + '/CustomerAdmin/AssignCustomerRoles.aspx?m=0&s=1">Assign Roles</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td03" class="menustrip_sublink_text"><a href="' + test_url + '/CustomerAdmin/AssignCustomerRoleRights.aspx?m=0&s=2">Assign Role Rights</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td04" class="menustrip_sublink_text"><a href="' + test_url + '/CustomerAdmin/AssignCustomerUserRights.aspx?m=0&s=3">Assign User Rights</a></td></tr></table></div>';
   // submenu[1] = '<div id="div1"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td11" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/EnrollCustomer.aspx?m=1&s=0">New Customer</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td12" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/SearchCustomer.aspx?m=1&s=1&parentpage=MENU">Search Customer</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td13" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/EnrollAssessor.aspx?m=1&s=2&parentpage=MENU">Assessor</a></td></tr></table></div>';
    submenu[1] = '<div id="div1"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td11" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/EnrollCustomer.aspx?m=1&s=0">New Tenant</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td12" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/SearchCustomer.aspx?m=1&s=1&parentpage=MENU">Search Tenant</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td></td><td id="td13" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/EnrollAssessor.aspx?m=1&s=2&parentpage=MENU">Assessor</a></td></tr></table></div>';
//    submenu[2] = '<div id="div2"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td21" class="menustrip_sublink_text"><a href="' + test_url + '/ResumeRepository/CreateCandidate.aspx?m=1&s=0">New Candidate</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td22" class="menustrip_sublink_text"><a href="' + test_url + '/ResumeRepository/SearchCandidateRepository.aspx?m=1&s=1&parentpage=MENU">Search Candidate</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td23" class="menustrip_sublink_text"><a href="' + test_url + '/ResumeRepository/ViewCandidateActivityLog.aspx?m=1&s=2&parentpage=MENU">Candidate Activity Log</a></td></tr></table></div>';


    //Set delay before submenu disappears after mouse moves out of it (in milliseconds)
    var delay_hide = 500;

    //No need to edit beyond here
    var menuobj = document.getElementById ? document.getElementById("describe") : document.all ? document.all.describe : document.layers ? document.dep1.document.dep2 : "";

    function SubMenu(parentMenu, SubMenu) {
        selectedId = "td" + parentMenu.toString() + SubMenu.toString();
        if (document.getElementById("div0") != null) {
            parentControl = document.getElementById("div0");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2) {
                if (tdElements[index] != null && tdElements[index].id != selectedId) {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }
        if (document.getElementById("div1") != null) {
            parentControl = document.getElementById("div1");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2) {
                if (tdElements[index] != null && tdElements[index].id != selectedId) {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById(selectedId) != null) {
            document.getElementById(selectedId).className = "menustrip_sublink_active_text";
        }
    }
    function showDefault() {
        if (document.getElementById("<%= CustomerAdminMenuControl_selectedMenuHiddenField.ClientID %>").value != "") {
            val = document.getElementById("<%= CustomerAdminMenuControl_selectedMenuHiddenField.ClientID %>").value;
            valSub = document.getElementById("<%= CustomerAdminMenuControl_selectedSubMenuHiddenField.ClientID %>").value;
            if (val == 0) 
            {
              //  menuobj.innerHTML = submenu[0];
               // document.getElementById("tdAdmin").className = "menustrip_active";
            }
            else if (val == 1) 
            {
                menuobj.innerHTML = submenu[1];
               // document.getElementById("tdEnroll").className = "menustrip_active";
            }
            SubMenu(val, parseInt(valSub) + 1);
        }
        else 
        {
            document.getElementById("tdAdmin").className = "menustrip_active";
        }
    }

    function showit(which) {
        clear_delayhide(false);
        thecontent = (which == -1) ? "" : submenu[which];
        debugger;

        if (document.getElementById || document.all) {
            menuobj.innerHTML = thecontent;
            if (thecontent != "") {
                if (thecontent.toString().indexOf('div0') != -1) {
                    document.getElementById("<%= CustomerAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";

                    document.getElementById("tdAdmin").className = "menustrip_active";
                    document.getElementById("tdEnroll").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div1') != -1) {
                    document.getElementById("<%= CustomerAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "1";

                    document.getElementById("tdEnroll").className = "menustrip_active";
                    document.getElementById("tdAdmin").className = "menutext_normal";
                }
                else 
                {
                    document.getElementById("<%= CustomerAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";

                    document.getElementById("tdAdmin").className = "menustrip_active";
                    document.getElementById("tdEnroll").className = "menutext_normal";
                }
                if (document.getElementById("<%= CustomerAdminMenuControl_selectedMenuHiddenField.ClientID %>").value != "") {
                    val = document.getElementById("<%= CustomerAdminMenuControl_selectedMenuHiddenField.ClientID %>").value;
                    valSub = document.getElementById("<%= CustomerAdminMenuControl_selectedSubMenuHiddenField.ClientID %>").value;

                    SubMenu(val, parseInt(valSub) + 1);
                }
            }
            else 
            {
                document.getElementById("tdAdmin").className = "menutext_normal";
                document.getElementById("tdEnroll").className = "menutext_normal";
                document.getElementById("<%= CustomerAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "";

                showDefault();
            }

        }
        else if (document.layers) {
            menuobj.document.write(thecontent)
            menuobj.document.close()
        }
    }

    function resetit(e) {
        if (document.all && !menuobj.contains(e.toElement))
            delayhide = setTimeout("showit(-1)", delay_hide)
        else if (document.getElementById && e.currentTarget != e.relatedTarget && !contains_ns6(e.currentTarget, e.relatedTarget))
            delayhide = setTimeout("showit(-1)", delay_hide)
    }

    function clear_delayhide(isSubMenuHover) {
        if (window.delayhide)
            clearTimeout(delayhide);

        if (document.getElementById("<%= CustomerAdminMenuControl_hoverMenuHiddenField.ClientID %>").value == "") {
            val = document.getElementById("<%= CustomerAdminMenuControl_selectedMenuHiddenField.ClientID %>").value;
        }
        else 
        {
            val = document.getElementById("<%= CustomerAdminMenuControl_hoverMenuHiddenField.ClientID %>").value;
        }

        if (val == 0) 
        {
            document.getElementById("tdAdmin").className = "menustrip_active";
        }
        else if (val == 1) 
        {
            document.getElementById("tdEnroll").className = "menustrip_active";
        }
    }

    function contains_ns6(a, b) {
        while (b.parentNode)
            if ((b = b.parentNode) == a)
                return true;
        return false;
    }
    
</script>
<script type="text/javascript" language="javascript">
    showDefault();
</script>

