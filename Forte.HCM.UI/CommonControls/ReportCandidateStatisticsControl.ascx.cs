﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionSettings.cs
// File that represents the user interface for question settings page.
// This will helps to view and set the question settings such as categories, 
// subjects and test areas.

#endregion


#region Directives
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using AjaxControlToolkit;

using Forte.HCM.EventSupport;
using Forte.HCM.BL;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// the test statistics page . This page helps to view and set the 
    /// candidate statistics such as the  name , absolute score , relative score
    /// session creator and schedule creator of the 
    /// </summary>
    public partial class ReportCandidateStatisticsControl : UserControl
    {
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        //private const string ASCENDING = " ASC";
        //private const string DESCENDING = " DESC";
        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }

            set { ViewState["sortDirection"] = value; }
        }
        public string TestID
        {
            set;
            get;
        }
        public int GridPageSize
        {
            set;
            get;
        }
        protected void ReportCandidateStatisticsControl_questionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
            }
        }
        protected void ReportCandidateStatisticsControl_questionGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (ViewState["SortExpression"].ToString() == sortExpression)
            {
                GridViewSortDirection =
                    GridViewSortDirection == SortDirection.Ascending ?
                    SortDirection.Descending : SortDirection.Ascending;
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
            }

            ViewState["SortExpression"] = sortExpression;

            ReportCandidateStatisticsControl_bottomPagingNavigator.Reset();

            SortGridView(sortExpression, GridViewSortDirection.ToString(),1);

        }
        private void SortGridView(string sortExpression, string direction, int pagenumber)
        {
            LoadValues(sortExpression, direction == "Ascending" ? "A" : "D", 1, GridPageSize);
        }
        protected void ReportCandidateStatisticsControl_questionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = GetSortColumnIndex();
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }

        }
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in ReportCandidateStatisticsControl_questionGridView.Columns)
            {
                if (field.SortExpression ==
                             (string)ViewState["SortExpression"])
                {
                    return ReportCandidateStatisticsControl_questionGridView.Columns.IndexOf(field);
                }
            }
            return -1;
        }

        private void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            // Add the image to the appropriate header cell.
            headerRow.Cells[columnIndex].Controls.Add(sortImage);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadValues("CANDIDATE_NAME", "A", 1, GridPageSize);

                ViewState["SortExpression"] = "CandidateName";
            }

            ReportCandidateStatisticsControl_bottomPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                   (ReportCandidateStatisticsControl_pagingNavigator_PageNumberClick);

            ReportCandidateStatisticsControl_questionDiv.Visible = true;

            ReportCandidateStatisticsControl_searchResultsUpSpan.Attributes.Add("onclick", "ExpandorCompress('" +
                ReportCandidateStatisticsControl_questionDiv.ClientID + "','" +
                ReportCandidateStatisticsControl_searchCriteriasDiv.ClientID +
                "','" + ReportCandidateStatisticsControl_searchResultsUpSpan.ClientID + "'," +
                "'" + ReportCandidateStatisticsControl_searchResultsDownSpan.ClientID + "')");

            ReportCandidateStatisticsControl_searchResultsDownSpan.Attributes.Add("onclick", "ExpandorCompress('" +
                ReportCandidateStatisticsControl_questionDiv.ClientID + "','" +
                ReportCandidateStatisticsControl_searchCriteriasDiv.ClientID +
                "','" + ReportCandidateStatisticsControl_searchResultsUpSpan.ClientID + "'," +
                "'" + ReportCandidateStatisticsControl_searchResultsDownSpan.ClientID + "')");

            ReportCandidateStatisticsControl_candidateNameImageButton.Attributes.Add("onclick",
                "return LoadCandidate('" + ReportCandidateStatisticsControl_candidateNameTextBox.ClientID + "')");
            ReportCandidateStatisticsControl_createdByImageButton.Attributes.Add("onclick",
                "return LoadUser('" + ReportCandidateStatisticsControl_createdByTextBox.ClientID + "')");
            ReportCandidateStatisticsControl_scheduledByImageButton.Attributes.Add("onclick",
                "return LoadUser('" + ReportCandidateStatisticsControl_scheduledByTextBox.ClientID + "')");


            foreach (MultiHandleSliderTarget target in ReportCandidateStatisticsControl_testCostMultiHandleSliderExtender.MultiHandleSliderTargets)
                target.ControlID = ReportCandidateStatisticsControl_testCostMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;

            foreach (MultiHandleSliderTarget target in ReportCandidateStatisticsControl_relativeScoreMultiHandleSliderExtender.MultiHandleSliderTargets)
                target.ControlID = ReportCandidateStatisticsControl_relativeScoreMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;
        }

        private void LoadValues(string orderBy,
            string orderByDirection, int pageNumber, int pageSize)
        {
            int totalRecords = 0;
            string candidateName = ReportCandidateStatisticsControl_candidateNameTextBox.Text.Trim();
            string sessionCreatedBy = ReportCandidateStatisticsControl_createdByTextBox.Text.Trim();
            string scheduleCreatedBy = ReportCandidateStatisticsControl_scheduledByTextBox.Text.Trim();
            string testDateFrom = ReportCandidateStatisticsControl_testDateTextBox.Text.Trim();
            string testDateTo = ReportCandidateStatisticsControl_testToDateTextBox.Text.Trim();
            string absoluteScoreFrom = ReportCandidateStatisticsControl_absoluteScoreMinValueTextBox.Text.Trim();
            string absoluteScoreTo = ReportCandidateStatisticsControl_absoluteScoreMaxValueTextBox.Text.Trim();
            string relativeScoreFrom = ReportCandidateStatisticsControl_relativeScoreMinValueTextBox.Text.Trim();
            string relativeScoreTo = ReportCandidateStatisticsControl_relativeScoreMaxValueTextBox.Text.Trim();


            ReportCandidateStatisticsControl_questionGridView.DataSource =
                new ReportBLManager().GetReportCandidateStatisticsDetails
                (candidateName, sessionCreatedBy, scheduleCreatedBy, testDateFrom,
                    testDateTo, absoluteScoreFrom, absoluteScoreTo, relativeScoreFrom,
                    relativeScoreTo, TestID, orderBy, orderByDirection, pageNumber, pageSize,
                    out totalRecords);

            ReportCandidateStatisticsControl_questionGridView.DataBind();

        }


        protected void ReportCandidateStatisticsControl_pagingNavigator_PageNumberClick(object sender,
                   PageNumberEventArgs e)
        {
        }
    }
}