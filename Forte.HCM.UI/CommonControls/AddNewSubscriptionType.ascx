﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewSubscriptionType.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.AddNewSubscriptionType" %>
<asp:Panel ID="pnl" runat="server" DefaultButton="Forte_AddNewSubscriptionType_subscriptionSaveButton">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="AddNewSubscriptionTypeUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                <asp:Literal ID="Forte_AddNewSubscriptionType_messagetitleLiteral" runat="server">Subscription Type</asp:Literal>
                                            </td>
                                            <td style="width: 25%" valign="top" align="right">
                                                <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                                                    SkinID="sknCloseImageButton" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table>
                                                    <tr style="height: 18px; vertical-align: top;">
                                                        <td>
                                                            <asp:Label ID="Forte_AddNewSubscriptionType_subscriptionTopErrorMessageLabel" runat="server"
                                                                EnableViewState="false" SkinID="sknErrorMessage"></asp:Label>
                                                            <asp:Label ID="Forte_AddNewSubscriptionType_subscriptionTopSuccessMessageLabel" runat="server"
                                                                EnableViewState="false" SkinID="sknSuccessMessage"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table style="text-align: left;" width="80%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width: 25%;">
                                                                        <asp:Label ID="Forte_AddNewSubscriptionType_subscriptionNameHeaderLabel" runat="server"
                                                                            Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                        <asp:Label ID="Forte_AddNewSubscriptionType_subscriptionIdHiddenLabel" runat="server"
                                                                            Visible="false"></asp:Label>
                                                                        <asp:Label ID="Forte_AddNewSubscriptionType_editModeHiddenLabel" runat="server" Visible="false"
                                                                            Text="false"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30px;">
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="Forte_AddNewSubscriptionType_subscriptionNameTextBox" runat="server"
                                                                            MaxLength="20" Width="200px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="Forte_AddNewSubscriptionType_subscriptionDescriptionHeaderLabel" runat="server"
                                                                            Text="Description" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Forte_AddNewSubscriptionType_subscriptionDescriptionTextBox" runat="server"
                                                                            TextMode="MultiLine" onkeyup="CommentsCount(200,this)" MaxLength="200" Height="74px"
                                                                            Width="348px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="Forte_AddNewSubscriptionType_roleNameLabel" runat="server" Text="Role"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Forte_AddNewSubscriptionType_roleComboTextBox" ReadOnly="true" runat="server"
                                                                            Width="350px" Font-Size="X-Small" Wrap="true" ToolTip="Test"></asp:TextBox>
                                                                        <asp:Panel ID="Forte_AddNewSubscriptionType_rolePanel" runat="server" CssClass="popupControl_module">
                                                                            <asp:CheckBoxList ID="Forte_AddNewSubscriptionType_roleList" RepeatColumns="1" runat="server"
                                                                                CellPadding="0" DataValueField="RoleID" DataTextField="RoleName" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="Forte_AddNewSubscriptionType_roleList_SelectedIndexChanged">
                                                                            </asp:CheckBoxList>
                                                                        </asp:Panel>
                                                                        <ajaxToolKit:PopupControlExtender ID="Forte_AddNewSubscriptionType_rolemodulePopupControlExtender"
                                                                            runat="server" TargetControlID="Forte_AddNewSubscriptionType_roleComboTextBox"
                                                                            PopupControlID="Forte_AddNewSubscriptionType_rolePanel" OffsetX="2" OffsetY="2"
                                                                            Position="Bottom">
                                                                        </ajaxToolKit:PopupControlExtender>
                                                                        <asp:HiddenField ID="Forte_AddNewSubscriptionType_roleIDHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10" align="left">
                                    <asp:Button ID="Forte_AddNewSubscriptionType_subscriptionSaveButton" runat="server"
                                        Text="Save" OnClick="Forte_AddNewSubscriptionType_subscriptionSaveButton_Click"
                                        SkinID="sknButtonId" />
                                    &nbsp;
                                    <asp:LinkButton ID="Forte_AddNewSubscriptionType_subscriptionCancelButton" runat="server"
                                        Text="Cancel" OnClick="Forte_AddNewSubscriptionType_subscriptionCancelButton_Click"
                                        SkinID="sknPopupLinkButton"></asp:LinkButton>
                                    <%--<asp:Button ID="Forte_AddFeatureType_featureCancelButton" runat="server" Text="Cancel"
                SkinID="sknButtonId" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10">
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>
