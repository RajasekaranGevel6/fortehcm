﻿#region Namespace                                                              

using System;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.Support;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class OverviewHeaderControl : UserControl
    {
        #region Variables                                                       

        /// <summary>
        /// A <see cref="string"/> that holds the home page URL.
        /// </summary>
        protected string homeUrl = string.Empty;

        #endregion Variables

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 3)
                {
                    // Compose home url.
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Default.aspx";
                }

                if (IsPostBack)
                    return;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Events
    }
}