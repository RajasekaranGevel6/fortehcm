﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalResumePreview.cs
// File that represents the user interface for the External Resume Preview details

#endregion Header

#region Directives                                                             
using System;
using System.Web.UI;

using Forte.HCM.DataObjects;
#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ExternalResumePreview : System.Web.UI.UserControl
    {
        #region Custom Event Handler and Delegate                              
        public delegate void Button_Click(object sender, EventArgs e);
        public event Button_Click CancelClick;
        #endregion

        #region Event Handlers                                                 
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Handles the Click event of the ExternalResumePreview_previewFormCancelLinkButton 
        /// control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ExternalResumePreview_previewFormCancelLinkButton_Click
            (object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }
        /// <summary>
        /// Handles the Click event of the ExternalResumePreview_cancelImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/>
        /// instance containing the event data.</param>
        protected void ExternalResumePreview_cancelImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }
        #endregion

        #region Public Methods                                                  
                                                                       
        /// <summary>
        /// Sets the resume preview.
        /// </summary>
        /// <value>
        /// The resume preview.
        /// </value>
        public ExternalResume ResumePreview
        {
            set
            {
                if (value == null)
                    return;

                //personal information
                ExternalResumePreview_personalInfomation_CandidateNameLabel.Text = value.CandidateName;
                ExternalResumePreview_personalInfomation_EmailLabel.Text = value.CandidateEmail;
                ExternalResumePreview_personalInfomation_PhoneLabel.Text = value.CandidatePhone;
                ExternalResumePreview_personalInfomation_Location1Label.Text = value.CandidateAddress;
                ExternalResumePreview_personalInfomation_Location2Label.Text = value.ZipCode;

                ExternalResumePreview_desiredPayLabel.Text = value.DesiredPay;
                ExternalResumePreview_desiredTravelLabel.Text = value.DesiredTravel;
                ExternalResumePreview_desiredRelocationLabel.Text = value.DesiredReLocations;
                ExternalResumePreview_desiredCommuteLabel.Text = value.DesiredCommute;
                ExternalResumePreview_desiredJobTypesLabel.Text = value.DesiredJobTypes;

                //Experience Tab
                ExternalResumePreview_experienceInMonthsLabel.Text = value.ExperienceMonths;
                ExternalResumePreview_experienceRecentTitleLabel.Text = value.MostRecentTitle;
                ExternalResumePreview_experienceHistoryGridView.DataSource = value.ExperienceHistory;
                ExternalResumePreview_experienceHistoryGridView.DataBind();

                //Education Tab
                ExternalResumePreview_educationalGridView.DataSource = value.EducationHistory;
                ExternalResumePreview_educationalGridView.DataBind();

                ExternalResumePreview_educationalManagedOthersLabel.Text = value.ManagedOthers;
                ExternalResumePreview_educationalSecurityClearnceLabel.Text = value.SecurityClearance;
                ExternalResumePreview_educationalFelonyConvictionLabel.Text = value.FelonyConviction;
                ExternalResumePreview_educationalMilitaryExperiencesLabel.Text = value.MilitaryExperience;
                ExternalResumePreview_educationaLanguagesSpokenLabel.Text = value.Languages;

                //Resume Text 
                ExternalResumePreview_resumeContentLiterral.Text = value.ResumeText;
            }
        }
        #endregion
    }
}