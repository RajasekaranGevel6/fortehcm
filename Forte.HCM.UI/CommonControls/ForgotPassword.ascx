﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ForgotPassword" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="ForgotPassword_messagetitleLiteral" runat="server">Forgot Password</asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="ForgotPassword_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" valign="top">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_subscription_type_bg">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="ForgotPassword_messageTd" runat="server" height="20px">
                                <td>
                                    <asp:Label ID="ForgotPassword_topErrorMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                    <asp:Label ID="ForgotPassword_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 1%;">
                                            
                                            </td>
                                            <td style="width: 19%" align="left">
                                                <asp:Label ID="ForgotPassword_userIDLabel" runat="server" Text="User ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                <span class="mandatory" id="ForgotPassword_userIDMandatorySpan" runat="server">*</span>
                                            </td>
                                            <td align="left" style="width: 80%" colspan="3">
                                                <asp:TextBox ID="ForgotPassword_userIDTextBox" runat="server" MaxLength="50" Width="90%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                <td>
                &nbsp;
                </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" align="left">
            <asp:Button ID="ForgotPassword_sendButton" runat="server" Text="Send Password" SkinID="sknButtonId"
                OnClick="ForgotPassword_sendButton_Click" ValidationGroup="a" />
            &nbsp; &nbsp;
            <asp:LinkButton ID="ForgotPassword_featureCancelLinkButton" runat="server" Text="Cancel"
                SkinID="sknPopupLinkButton">
            </asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
