﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportCandidateStatisticsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ReportCandidateStatisticsControl" %>
<%@ Register Src="PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="panel_bg">
            <div id="ReportCandidateStatisticsControl_searchCriteriasDiv" runat="server" style="display: block;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="3" class="panel_inner_body_bg">
                                <tr>
                                    <td>
                                        <asp:Label ID="ReportCandidateStatisticsControl_candidateNameLabel" runat="server"
                                            Text="Candidate Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <div style="float: left; padding-right: 5px;">
                                            <asp:TextBox ID="ReportCandidateStatisticsControl_candidateNameTextBox" runat="server"
                                                Text="" ReadOnly="True"></asp:TextBox></div>
                                        <div style="float: left;">
                                            <asp:ImageButton ID="ReportCandidateStatisticsControl_candidateNameImageButton" SkinID="sknbtnSearchicon"
                                                runat="server" ImageAlign="Middle" /></div>
                                    </td>
                                    <td>
                                        <asp:Label ID="ReportCandidateStatisticsControl_createdByLabel" runat="server" Text="Test Session Creator"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <div style="float: left; padding-right: 5px;">
                                            <asp:TextBox ID="ReportCandidateStatisticsControl_createdByTextBox" runat="server"
                                                Text="" ReadOnly="True"></asp:TextBox></div>
                                        <div style="float: left;">
                                            <asp:ImageButton ID="ReportCandidateStatisticsControl_createdByImageButton" SkinID="sknbtnSearchicon"
                                                runat="server" ImageAlign="Middle" /></div>
                                    </td>
                                    <td>
                                        <asp:Label ID="ReportCandidateStatisticsControl_scheduledByHeadLabel" runat="server"
                                            Text="Test Schedule Creator" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <div style="float: left; padding-right: 5px;">
                                            <asp:TextBox ID="ReportCandidateStatisticsControl_scheduledByTextBox" runat="server"
                                                ReadOnly="True"></asp:TextBox></div>
                                        <div style="float: left;">
                                            <asp:ImageButton ID="ReportCandidateStatisticsControl_scheduledByImageButton" SkinID="sknbtnSearchicon"
                                                runat="server" ImageAlign="Middle" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ReportCandidateStatisticsControl_testDateHeadLabel" runat="server"
                                            Text="Test Date From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_testDateTextBox" runat="server"
                                                        MaxLength="10" AutoCompleteType="None" Columns="17" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ReportCandidateStatisticsControl_calendarImageButton" SkinID="sknCalendarImageButton"
                                                        runat="server" ImageAlign="Middle" />
                                                </td>
                                            </tr>
                                        </table>
                                        <ajaxToolKit:MaskedEditExtender ID="ReportCandidateStatisticsControl_MaskedEditExtender"
                                            runat="server" TargetControlID="ReportCandidateStatisticsControl_testDateTextBox"
                                            Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                            ErrorTooltipEnabled="True" />
                                        <ajaxToolKit:MaskedEditValidator ID="ReportCandidateStatisticsControl_MaskedEditValidator"
                                            runat="server" ControlExtender="ReportCandidateStatisticsControl_MaskedEditExtender"
                                            ControlToValidate="ReportCandidateStatisticsControl_testDateTextBox" EmptyValueMessage="Date is required"
                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                        <ajaxToolKit:CalendarExtender ID="ReportCandidateStatisticsControl_customCalendarExtender"
                                            runat="server" TargetControlID="ReportCandidateStatisticsControl_testDateTextBox"
                                            CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="ReportCandidateStatisticsControl_calendarImageButton" />
                                    </td>
                                    <td>
                                        <asp:Label ID="ReportCandidateStatisticsControl_testToDateHeadLabel" runat="server"
                                            Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_testToDateTextBox" runat="server"
                                                        MaxLength="10" AutoCompleteType="None" Columns="17" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ReportCandidateStatisticsControl_calendarToImageButton" SkinID="sknCalendarImageButton"
                                                        runat="server" ImageAlign="Middle" />
                                                </td>
                                            </tr>
                                        </table>
                                        <ajaxToolKit:MaskedEditExtender ID="ReportCandidateStatisticsControl_MaskedEditExtender1"
                                            runat="server" TargetControlID="ReportCandidateStatisticsControl_testToDateTextBox"
                                            Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                            ErrorTooltipEnabled="True" />
                                        <ajaxToolKit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="ReportCandidateStatisticsControl_MaskedEditExtender"
                                            ControlToValidate="ReportCandidateStatisticsControl_testToDateTextBox" EmptyValueMessage="Date is required"
                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                        <ajaxToolKit:CalendarExtender ID="ReportCandidateStatisticsControl_customCalendarExtender1"
                                            runat="server" TargetControlID="ReportCandidateStatisticsControl_testToDateTextBox"
                                            CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="ReportCandidateStatisticsControl_calendarToImageButton" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ReportCandidateStatisticsControl_absoluteScoreHeadLabel" runat="server"
                                            Text="Absolute Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <table width="50%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_absoluteScoreMinValueTextBox" runat="server"
                                                        MaxLength="5" Width="25" Text=""></asp:TextBox>
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_absoluteScoreTextBox" runat="server"
                                                        MaxLength="250"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_absoluteScoreMaxValueTextBox" runat="server"
                                                        MaxLength="5" Width="25" Text=""></asp:TextBox>
                                                    <ajaxToolKit:MultiHandleSliderExtender ID="ReportCandidateStatisticsControl_testCostMultiHandleSliderExtender"
                                                        runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                        RailCssClass="slider_rail" HandleCssClass="slider_handler" TargetControlID="ReportCandidateStatisticsControl_absoluteScoreTextBox"
                                                        ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                        <MultiHandleSliderTargets>
                                                            <ajaxToolKit:MultiHandleSliderTarget ControlID="ReportCandidateStatisticsControl_absoluteScoreMinValueTextBox"
                                                                HandleCssClass="MultiHandleMinSliderCss" />
                                                            <ajaxToolKit:MultiHandleSliderTarget ControlID="ReportCandidateStatisticsControl_absoluteScoreMaxValueTextBox"
                                                                HandleCssClass="MultiHandleMaxSliderCss" />
                                                        </MultiHandleSliderTargets>
                                                    </ajaxToolKit:MultiHandleSliderExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:Label ID="ReportCandidateStatisticsControl_relativeScoreHeadLabel" runat="server"
                                            Text="Relative Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <table width="50%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_relativeScoreMinValueTextBox" runat="server"
                                                        MaxLength="5" Width="25" Text=""></asp:TextBox>
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_relativeScoreTextBox" runat="server"
                                                        MaxLength="250"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:TextBox ID="ReportCandidateStatisticsControl_relativeScoreMaxValueTextBox" runat="server"
                                                        MaxLength="5" Width="25" Text=""></asp:TextBox>
                                                    <ajaxToolKit:MultiHandleSliderExtender ID="ReportCandidateStatisticsControl_relativeScoreMultiHandleSliderExtender"
                                                        runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                        RailCssClass="slider_rail" HandleCssClass="slider_handler" TargetControlID="ReportCandidateStatisticsControl_relativeScoreTextBox"
                                                        ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                        <MultiHandleSliderTargets>
                                                            <ajaxToolKit:MultiHandleSliderTarget ControlID="ReportCandidateStatisticsControl_relativeScoreMinValueTextBox"
                                                                HandleCssClass="MultiHandleMinSliderCss" />
                                                            <ajaxToolKit:MultiHandleSliderTarget ControlID="ReportCandidateStatisticsControl_relativeScoreMaxValueTextBox"
                                                                HandleCssClass="MultiHandleMaxSliderCss" />
                                                        </MultiHandleSliderTargets>
                                                    </ajaxToolKit:MultiHandleSliderExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <table border="0" cellpadding="0" cellspacing="5" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ReportCandidateStatisticsControl_searchButton" runat="server" SkinID="sknButtonId"
                                            Text="Search" />
                                    </td>
                                    <td class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ReportCandidateStatisticsControl_resetLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td class="td_height_5">
        </td>
    </tr>
    <tr>
        <td class="header_bg" align="center">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 50%" align="left">
                        <asp:Literal ID="ReportCandidateStatisticsControl_searchResultsLiteral" runat="server"
                            Text="Candidate List"></asp:Literal>&nbsp;<asp:Label ID="CandidateStatisticsControl_sortHelpLabel"
                                runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                    </td>
                    <td style="width: 50%" align="right">
                        <span id="ReportCandidateStatisticsControl_searchResultsUpSpan" runat="server" style="display: none;">
                            <asp:Image ID="ReportCandidateStatisticsControl_searchResultsUpImage" runat="server"
                                SkinID="sknMinimizeImage" /></span><span id="ReportCandidateStatisticsControl_searchResultsDownSpan"
                                    runat="server" style="display: block;"><asp:Image ID="ReportCandidateStatisticsControl_searchResultsDownImage"
                                        runat="server" SkinID="sknMaximizeImage" /></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="grid_body_bg">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right" class="td_height_20" valign="middle">
                        <table width="25%" align="right">
                            <tr>
                                <td align="right">
                                    <asp:LinkButton ID="ReportCandidateStatisticsControl_groupAnalysisLinkButton" runat="server"
                                        Text="Group Analysis Report" SkinID="sknActionLinkButton" PostBackUrl="~/ReportCenter/GroupAnalysisReport.aspx?m=3&s=0" />
                                </td>
                                <td class="link_button" align="center">
                                    |
                                </td>
                                <td align="left">
                                    <asp:LinkButton ID="ReportCandidateStatisticsControl_comparisonReportLinkButton"
                                        runat="server" Text="Comparison Report" SkinID="sknActionLinkButton" PostBackUrl="~/ReportCenter/ComparisonReport.aspx?m=3&s=0" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <div style="height: 200px; overflow: auto; width: 950px;" runat="server" id="ReportCandidateStatisticsControl_questionDiv"
                            visible="false">
                            <asp:GridView ID="ReportCandidateStatisticsControl_questionGridView" runat="server"
                                AllowSorting="true" AutoGenerateColumns="false" Width="100%" OnSorting="ReportCandidateStatisticsControl_questionGridView_Sorting"
                                OnRowDataBound="ReportCandidateStatisticsControl_questionGridView_RowDataBound"
                                OnRowCreated="ReportCandidateStatisticsControl_questionGridView_RowCreated">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ReportCandidateStatisticsControl_selectCheckBox" runat="server" />&nbsp;<asp:ImageButton
                                                ID="ReportCandidateStatisticsControl_designReportImageButton" runat="server"
                                                ToolTip="Design Report" SkinID="sknDesignReportImageButton" PostBackUrl="~/ReportCenter/DesignReport.aspx?m=3&s=0" />&nbsp;<asp:ImageButton
                                                    ID="ReportCandidateStatisticsControl_trackingImageButton" runat="server" ToolTip="Cyber Proctoring Information"
                                                    SkinID="sknTrackingImageButton" PostBackUrl="~/ReportCenter/TrackingDetails.aspx?m=3&s=0" />
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Candidate&nbsp;Name" DataField="FirstName" SortExpression="CandidateName"
                                        ItemStyle-Wrap="false" ItemStyle-Width="150px" />
                                    <asp:BoundField HeaderText="Attempt&nbsp;Number" DataField="AttemptNumber" SortExpression="AttemptNumber" />
                                    <asp:BoundField HeaderText="Test Session Creator" DataField="SessionCreator" SortExpression="SessionCreator"
                                        ItemStyle-Wrap="false" ItemStyle-Width="450px" HeaderStyle-CssClass="grid_header_row_wrap"
                                        HeaderStyle-Width="100px"></asp:BoundField>
                                    <asp:BoundField HeaderText="Test Schedule Creator" DataField="ScheduleCreator" ItemStyle-Wrap="false"
                                        SortExpression="ScheduleCreator" ItemStyle-Width="200px" HeaderStyle-CssClass="grid_header_row_wrap"
                                        HeaderStyle-Width="450px"></asp:BoundField>
                                    <asp:BoundField HeaderText="Date" DataField="TestDate" SortExpression="TestDate"
                                        ItemStyle-Wrap="false" ItemStyle-Width="170px" DataFormatString="{0:M-dd-yyyy}"
                                        HtmlEncode="false" />
                                    <asp:BoundField HeaderText="Answered Correctly(%)" DataField="AnsweredCorrectly"
                                        SortExpression="AnsweredCorrectly" HeaderStyle-CssClass="grid_header_row_wrap"
                                        ItemStyle-CssClass="td_padding_right_20" ItemStyle-HorizontalAlign="right" HeaderStyle-Width="250px"
                                        ItemStyle-Width="170px" />
                                    <asp:BoundField HeaderText="Total Time&nbsp;Taken" DataField="TimeTaken" SortExpression="TimeTaken"
                                        HeaderStyle-CssClass="grid_header_row_wrap" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField HeaderText="Avg&nbsp;Time&nbsp;Taken (Per Question)" DataField="AverageTimeTaken"
                                        SortExpression="AverageTimeTaken" ItemStyle-Wrap="false" HeaderStyle-CssClass="grid_header_row_wrap"
                                        HeaderStyle-Width="200px" />
                                    <asp:BoundField HeaderText="Absolute Score" DataField="AbsoluteScore" SortExpression="AbsoluteScore"
                                        ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                        HeaderStyle-CssClass="td_padding_right_20 grid_header_row_wrap" />
                                    <asp:BoundField HeaderText="Relative Score" DataField="RelativeScore" SortExpression="RelativeScore"
                                        ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                        HeaderStyle-CssClass="td_padding_right_20 grid_header_row_wrap" />
                                    <asp:BoundField HeaderText="Percentile" DataField="Percentile" SortExpression="Percentile"
                                        ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                        HeaderStyle-CssClass="td_padding_right_20" />
                                    <asp:BoundField HeaderText="Recruiter" DataField="Recruiter" SortExpression="Recruiter"
                                        HeaderStyle-Width="100px" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc3:PageNavigator ID="ReportCandidateStatisticsControl_bottomPagingNavigator" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
