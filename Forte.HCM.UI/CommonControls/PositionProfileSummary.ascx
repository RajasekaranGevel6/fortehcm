﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PositionProfileSummary.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.PositionProfileSummary" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="PositionProfileSummary_questionResultLiteral" runat="server" Text="Position Profile Summary"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top" align="right">
                        <asp:ImageButton ID="PositionProfileSummary_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="td_height_8">
        </td>
    </tr>
    <tr>
        <td align="center" class="td_height_8">
            <asp:Label ID="PositionProfileSummary_errorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <div style="height: 100px; overflow: auto;">
                            <asp:GridView ID="PositionProfileSummary_skillKeywordGridView" runat="server" AutoGenerateColumns="false"
                                OnRowCommand="PositionProfileSummary_skillKeywordGridView_RowCommand" OnRowDataBound="PositionProfileSummary_skillKeywordGridView_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="8%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="PositionProfileSummary_deleteSkillsImageButton" runat="server"
                                                SkinID="sknDeleteImageButton" ToolTip="Delete Skill" CommandName="deleteskill"/>
                                                <asp:CheckBox  runat="server" Visible="false" Checked='<%# Eval("UserSegments") %>'/>
                                                <asp:HiddenField ID="PositionProfileSummary_isUserKeywordHiddenField" runat="server" Value='<%# Eval("UserSegments") %>' />
                                                <asp:HiddenField ID="PositionProfileSummary_skillTypeHiddenField" runat="server" Value='<%# Eval("SkillTypes") %>' />
                                                <asp:HiddenField ID="PositionProfileSummary_skillCategoryHiddenField" runat="server" Value='<%# Eval("SkillCategory") %>' />
                                                <asp:Label ID="test" Text='<%# Eval("SkillTypes") %>' runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="Label1" Text='<%# Eval("SkillCategory") %>' runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="15%" />
                                        <ItemStyle Width="30%" />
                                        <HeaderTemplate>
                                            <asp:Label ID="PositionProfileSummary_skillCategoryLabel" runat="server" Text="Skills"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="PositionProfileSummary_skillCategoryTextBox" MaxLength="50" runat="server"
                                                Width="90%" Text='<%# Eval("SkillName") %>'></asp:TextBox>
                                            <ajaxToolKit:AutoCompleteExtender ID="PositionProfileSummary_categoryAutoCompleteExtender"
                                                runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetPositionProfileKeyword"
                                                TargetControlID="PositionProfileSummary_skillCategoryTextBox"
                                                MinimumPrefixLength="1" CompletionListElementID="pnl" CompletionListCssClass="autocomplete_completionListElement"
                                                EnableCaching="true" CompletionSetCount="12">
                                            </ajaxToolKit:AutoCompleteExtender>
                                            <asp:Panel ID="pnl" runat="server">
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="15%" />
                                        <ItemStyle Width="30%" />
                                        <HeaderTemplate>
                                            <asp:Label ID="PositionProfileSummary_skillCategoryWeightageLabel" runat="server"
                                                Text="Weightage"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="PositionProfileSummary_skillCategoryWeightageTextbox" runat="server"
                                                Width="90%" Text='<%# Convert.ToInt32(Eval("Weightage"))%>' MaxLength="4"></asp:TextBox>
                                            <ajaxToolKit:FilteredTextBoxExtender ID="PositionProfileSummary_skillCategoryWeightageTextboxExtender"
                                                runat="server" FilterType="Custom,Numbers" TargetControlID="PositionProfileSummary_skillCategoryWeightageTextbox" ValidChars=".">
                                            </ajaxToolKit:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="popup_td_padding_10">
                        <asp:LinkButton ID="PositionProfileSummary_addRowLinkButton" runat="server" SkinID="sknAddLinkButton"
                            Text="Add" ToolTip="Click here to add new skill" OnClick="PositionProfileSummary_addRowLinkButton_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
