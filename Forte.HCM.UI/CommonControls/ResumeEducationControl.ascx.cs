﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ResumeEducationControl.cs
// File that represents the user interface for the educational information details

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ResumeEducationControl : System.Web.UI.UserControl
    {
        #region Custom Event Handler and Delegate

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;
        private int _rowCounter;
        #endregion Custom Event Handler and Delegate

        #region Event Handlers  
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ResumeEducationControl_addDefaultButton_Click(object sender, EventArgs e)
        {
            SetViewState();
            ResumeEducationControl_listView.DataSource = null;
            if (ViewState["dataSource"] != null)
            {
                this.dataSource = (List<Education>)ViewState["dataSource"];
            }
            else
            {
                this.dataSource = new List<Education>();
            }
            this.dataSource.Add(new Education());
            ViewState["dataSource"] = this.dataSource;
            _rowCounter = this.dataSource.Count;
            ResumeEducationControl_listView.DataSource = this.dataSource;
            ResumeEducationControl_listView.DataBind();
            SetDate();

        }

        protected void ResumeEducationControl_deleteButton_Click(object sender, EventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((Button)(sender)).Parent;
            this.dataSource = (List<Education>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            ResumeEducationControl_listView.DataSource = dataSource;
            ResumeEducationControl_listView.DataBind();
            ViewState["dataSource"] = dataSource;
            SetDate();
        }

        protected void DeleteRowImagebutton_Click(object sender, EventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((ImageButton)(sender)).Parent;
            this.dataSource = (List<Education>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            ResumeEducationControl_listView.DataSource = dataSource;
            ResumeEducationControl_listView.DataBind();
            ViewState["dataSource"] = dataSource;
            SetDate();
        }

        protected void ResumeEducationControl_okPopUpClick(object sender, EventArgs e)
        {
            int intDeletedRowIndex = Convert.ToInt32(ResumeEducationControl_deletedRowHiddenField.Value);
            if (intDeletedRowIndex != 0)
            {
                this.dataSource = (List<Education>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intDeletedRowIndex - 1);
            }
            else
            {
                int intLastRecord = ResumeEducationControl_listView.Items.Count - 1;
                this.dataSource = (List<Education>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intLastRecord);
            }
            _rowCounter = dataSource.Count;
            ResumeEducationControl_listView.DataSource = dataSource;
            ResumeEducationControl_listView.DataBind();
            SetViewState();
            SetDate();

            // Fire the message event
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("Education detail deleted successfully", MessageType.Success));
        }

        protected void ResumeEducationControl_listView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Button ResumeEducationControl_addButton = ((Button)e.Item.FindControl
            ("ResumeEducationControl_addButton"));
            Button ResumeEducationControl_deleteButton = ((Button)e.Item.FindControl
                 ("ResumeEducationControl_deleteButton"));
            TextBox ResumeEducationControl_nameTextBox = ((TextBox)e.Item.FindControl
                 ("ResumeEducationControl_nameTextBox"));
            ((Panel)e.Item.FindControl("ResumeEducationControl_schoolPanel")).GroupingText =
                "Institution " + ((e.Item as ListViewDataItem).DisplayIndex + 1).ToString();

            ResumeEducationControl_addButton.Visible = false;
            ResumeEducationControl_deleteButton.Visible = false;

            if (_rowCounter == ((e.Item as ListViewDataItem).DisplayIndex + 1))
            {
                ResumeEducationControl_nameTextBox.Focus();
                ResumeEducationControl_addButton.Visible = true;
                if (_rowCounter != 1)
                    ResumeEducationControl_deleteButton.Visible = true;
            }
        }

        protected void ResumeEducationControl_listView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            SetViewState();
            SetDate();
            if (e.CommandName == "deleteEducation")
            {
                ImageButton deleteRowImagebutton = (ImageButton)e.Item.FindControl("ResumeEducationControl_deleteImageButton");
                TextBox txtRowIndex = (TextBox)e.Item.FindControl("ResumeEducationControl_deleteRowIndex");
                ResumeEducationControl_deletedRowHiddenField.Value = txtRowIndex.Text;
                ResumeEducationControl_okPopUpClick(deleteRowImagebutton, new EventArgs());
            }
            else if (e.CommandName == "addEducation")
            {
                Button ResumeEducationControl_addButton = (Button)e.Item.FindControl("ResumeEducationControl_addButton");
                ResumeEducationControl_addDefaultButton_Click(ResumeEducationControl_addButton, new EventArgs());
            }
        }

        #endregion Event Handlers

        #region Private Variables

        private List<Education> dataSource;

        #endregion

        #region Public Methods

        public string SetLocation(Location pLocation)
        {
            string location = "";
            if (pLocation != null)
            {
                location = (pLocation.City != null) ? pLocation.City : "";
                location += (pLocation.State != null) ? "," + pLocation.State : "";
                location += (pLocation.Country != null) ? "," + pLocation.Country : "";
                location = location.StartsWith(",") ? location.Substring(1) : location;
            }
            return location;
        }

        public List<Education> DataSource
        {
            set
            {
                if (value == null)
                {
                    ResumeEducationControl_addDefaultButton.Visible = true;
                    return;
                }
                // Set values into controls.
                _rowCounter = value.Count;

                if (_rowCounter != 0)
                    ResumeEducationControl_addDefaultButton.Visible = false;

                ResumeEducationControl_listView.DataSource = value;
                ResumeEducationControl_listView.DataBind();
                ViewState["dataSource"] = value;
            }
            get
            {
                if (ViewState["dataSource"] != null)
                    return (List<Education>)ViewState["dataSource"];
                else
                    return null;
            }
        }

        #endregion Public Methods

        #region Protected Methods

        protected void SetViewState()
        {

            List<Education> oEduList = null;
            int intEduCnt = 0;
            foreach (ListViewDataItem item in ResumeEducationControl_listView.Items)
            {
                if (oEduList == null)
                {
                    oEduList = new List<Education>();
                }


                Education oEducation = new Education();
                Location oLocation = new Location();
                TextBox txtEduName = (TextBox)item.FindControl("ResumeEducationControl_nameTextBox");
                TextBox txtEduLocation = (TextBox)item.FindControl("ResumeEducationControl_locationTextBox");
                TextBox txtEduDegree = (TextBox)item.FindControl("ResumeEducationControl_degreeTextBox");
                TextBox txtEduSpec = (TextBox)item.FindControl("ResumeEducationControl_specializationTextBox");
                TextBox txtEduGPA = (TextBox)item.FindControl("ResumeEducationControl_gpaTextBox");
                TextBox txtEduGradDate = (TextBox)item.FindControl("ResumeEducationControl_graduationDtTextBox");
                TextBox txtRowIndex = (TextBox)item.FindControl("ResumeEducationControl_deleteRowIndex");
                intEduCnt = intEduCnt + 1;
                oEducation.EducationId = intEduCnt;
                oEducation.DegreeName = txtEduDegree.Text.Trim();
                oLocation.City = txtEduLocation.Text.Trim();
                oEducation.SchoolLocation = oLocation;
                oEducation.Specialization = txtEduSpec.Text.Trim();
                oEducation.SchoolName = txtEduName.Text.Trim();
                DateTime DtEduGradDate;
                if ((DateTime.TryParse(txtEduGradDate.Text, out DtEduGradDate)))
                {
                    oEducation.GraduationDate = DtEduGradDate;
                }
                oEducation.GPA = txtEduGPA.Text.Trim();
                oEduList.Add(oEducation);
            }
            ResumeEducationControl_listView.DataSource = oEduList;
            ResumeEducationControl_listView.DataBind();
            ViewState["dataSource"] = oEduList;
            ResumeEducationControl_addDefaultButton.Visible = false;
        }
        protected void SetDate()
        {
            foreach (ListViewDataItem item in ResumeEducationControl_listView.Items)
            {
                TextBox txtEduGradDate = (TextBox)item.FindControl("ResumeEducationControl_graduationDtTextBox");
                if (txtEduGradDate.Text == "01/01/0001" || txtEduGradDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtEduGradDate.Text = "";
                }
            }
        }

        #endregion Protected Methods
    }
}