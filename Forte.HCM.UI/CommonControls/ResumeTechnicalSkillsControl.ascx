﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeTechnicalSkillsControl.ascx.cs" 
    Inherits="Forte.HCM.UI.CommonControls.ResumeTechnicalSkillsControl" %>
<div id="MyResume_TechnicalSkill_MainDiv">
    <div id="MyResume_TechnicalSkill_Controls" style="clear: both; float: left; height: 290px;"
        class="resume_Table_Bg">
        <div id="ResumeTechnicalSkillsControl_controlsDiv" style="display: block;" runat="server">
            <div class="can_resume_form_container">
                <div class="can_resume_tech_container_left">
                    <asp:Label ID="ResumeTechnicalSkillsControl_languageLabel" runat="server" Text="Languages"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_tech_container_right">
                    <asp:TextBox ID="ResumeTechnicalSkillsControl_languageTextBox" runat="server" TextMode="MultiLine"
                        Height="30" Width="760" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"
                        SkinID="sknCanResumeTextBox"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container">
                <div class="can_resume_tech_container_left">
                    <asp:Label ID="ResumeTechnicalSkillsControl_osLabel" runat="server" Text="Operating System"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_tech_container_right">
                    <asp:TextBox ID="ResumeTechnicalSkillsControl_osTextBox" runat="server" TextMode="MultiLine"
                        Height="30" Width="760" MaxLength="500" onkeyup="CommentsCount(500,this)" SkinID="sknCanResumeTextBox"
                        onchange="CommentsCount(500,this)"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container">
                <div class="can_resume_tech_container_left">
                    <asp:Label ID="ResumeTechnicalSkillsControl_databaseLabel" runat="server" Text="Database"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_tech_container_right">
                    <asp:TextBox ID="ResumeTechnicalSkillsControl_databaseTextBox" runat="server" TextMode="MultiLine"
                        Height="30" Width="760" MaxLength="500" SkinID="sknCanResumeTextBox" onkeyup="CommentsCount(500,this)"
                        onchange="CommentsCount(500,this)"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container">
                <div class="can_resume_tech_container_left">
                    <asp:Label ID="ResumeTechnicalSkillsControl_uiToolsLabel" runat="server" Text="UI Tools"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_tech_container_right">
                    <asp:TextBox ID="ResumeTechnicalSkillsControl_uiToolsTextBox" runat="server" TextMode="MultiLine"
                        Height="30" Width="760" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"
                        SkinID="sknCanResumeTextBox"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container">
                <div class="can_resume_tech_container_left">
                    <asp:Label ID="ResumeTechnicalSkillsControl_otherSkillsLabel" runat="server" Text="Other Skills"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_tech_container_right">
                    <asp:TextBox ID="ResumeTechnicalSkillsControl_otherSkillsTextBox" runat="server" TextMode="MultiLine"
                        Height="30" Width="760" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"
                        SkinID="sknCanResumeTextBox"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>