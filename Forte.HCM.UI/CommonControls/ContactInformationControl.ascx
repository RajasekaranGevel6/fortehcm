﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInformationControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ContactInformationControl" %>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td class="header_bg" colspan="4">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 93%" align="left">
                        <asp:Literal ID="ContactInformationControl_headerLiteral" runat="server" Text="Contact Information"></asp:Literal>
                         <asp:HiddenField ID="ContactInformation_hiddenField" runat="server" />
                    </td>
                    <td style="width: 2%" align="right">
                        <span id="ContactInformationControl_plusSpan" runat="server" style="display: none;">
                            <asp:Image ID="ContactInformationControl_plusImage" runat="server" SkinID="sknPlusImage" /></span><span
                                id="ContactInformationControl_minusSpan" runat="server" style="display: block;">
                                <asp:Image ID="ContactInformationControl_minusImage" runat="server" SkinID="sknMinusImage" /></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%">
            <div id="ContactInformationControl_controlsDiv" style="display: block;" runat="server">
                <table width="100%" border="0">
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Label ID="ContactInformationControl_streetLabel" runat="server" Text="Street"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="ContactInformationControl_streetTextBox" runat="server" Width="95%"
                                TextMode="MultiLine" Height="30" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                onchange="CommentsCount(500,this)" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%">
                            <asp:Label ID="ContactInformationControl_cityLabel" runat="server" Text="City" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 25%">
                            <asp:TextBox ID="ContactInformationControl_cityTextBox" runat="server" MaxLength="100" ></asp:TextBox>
                        </td>
                         <td style="width: 20%">
                            <asp:Label ID="ContactInformationControl_stateLabel" runat="server" Text="State" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 25%">
                            <asp:TextBox ID="ContactInformationControl_stateTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                      <td style="width: 20%">
                            <asp:Label ID="ContactInformationControl_countryNameLabel" runat="server" Text="Country"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ContactInformationControl_countryNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="ContactInformationControl_postalCodeLabel" runat="server" Text="Postal Code"
                                SkinID="sknLabelFieldHeaderText" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ContactInformationControl_postalCodeTextBox" runat="server" MaxLength="10"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                     <td>
                            <asp:Label ID="ContactInformationControl_fixedLineLabel" runat="server" Text="Fixed Line"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ContactInformationControl_fixedLineTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="ContactInformationControl_mobileLabel" runat="server" Text="Mobile"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ContactInformationControl_mobileTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                    <td>
                            <asp:Label ID="ContactInformationControl_emailLabel" runat="server" Text="Email"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ContactInformationControl_emailTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="ContactInformationControl_websiteLabel" runat="server" Text="Website"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ContactInformationControl_websiteTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
