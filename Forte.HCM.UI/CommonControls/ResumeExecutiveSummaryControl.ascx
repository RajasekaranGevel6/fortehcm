﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeExecutiveSummaryControl.ascx.cs" 
Inherits="Forte.HCM.UI.CommonControls.ResumeExecutiveSummaryControl" %>
<div id="Resume_ExecutiveSummary_MainDiv">
    <div id="Resume_ExecutiveSummary_Controls" >
        <div id="ResumeExecutiveSummaryControl_controlsDiv" style="display: block;" runat="server" >
            <asp:HiddenField ID="ResumeExecutiveSummaryControl_hiddenField" runat="server" />
            <asp:TextBox ID="ResumeExecutiveSummaryControl_execSummaryTextBox" runat="server" Height="290"
                Width="950px" TextMode="MultiLine" MaxLength="5000" 
                SkinID="sknCanResumeTextBox"></asp:TextBox>
        </div>
    </div>
</div>