﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EducationControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.EducationControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<asp:UpdatePanel ID="EducationControl_updatePanel" runat="server">
    <ContentTemplate>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="header_bg">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="width: 93%" align="left">
                                <asp:Literal ID="EducationControl_headerLiteral" runat="server" Text="Education"></asp:Literal>
                                <asp:HiddenField ID="EducationControl_hiddenField" runat="server" />
                                <asp:HiddenField ID="EducationControl_deletedRowHiddenField" runat="server" />
                            </td>
                            <td style="width: 2%" align="right">
                                <span id="EducationControl_plusSpan" runat="server" style="display: none;">
                                    <asp:Image ID="EducationControl_plusImage" runat="server" SkinID="sknPlusImage" /></span><span
                                        id="EducationControl_minusSpan" runat="server" style="display: block;">
                                        <asp:Image ID="EducationControl_minusImage" runat="server" SkinID="sknMinusImage" /></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="width: 100%;" runat="server" id="EducationControl_controlsDiv">
            <asp:LinkButton ID="EducationControl_addRowLinkButton" SkinID="sknAddLinkButton"
                runat="server" Text="Add" OnClick="EducationControl_addRowLinkButton_Click" ToolTip="Add new Row"></asp:LinkButton>
            <br />
            <asp:ListView ID="EducationControl_listView" runat="server" OnItemDataBound="EducationControl_listView_ItemDataBound"
                OnItemCommand="EducationControl_listView_ItemCommand">
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" width="100%">
                        <tr runat="server" id="itemPlaceholder">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <table class="ListViewNormalStyle" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="EducationControl_rowNumberLabel" runat="server" Text="" SkinID="sknRowNumber"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ID="EducationControl_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                            ToolTip="Delete Education" CommandName="deleteEducation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel GroupingText="Institution" runat="server" ID="EducationControl_schoolPanel">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 20%">
                                                        <asp:Label runat="server" ID="EducationControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <asp:TextBox ID="EducationControl_deleteRowIndex" runat="server" Text='<%# Eval("EducationId") %>'
                                                            Style="display: none" />
                                                    </td>
                                                    <td style="width: 80%">
                                                        <asp:TextBox runat="server" ID="EducationControl_nameTextBox" Text='<%# Eval("SchoolName") %>'
                                                            Width="98%" ToolTip="School Name" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="EducationControl_locationLabel" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="EducationControl_locationTextBox" Text='<%# SetLocation(((Forte.HCM.DataObjects.Education)Container.DataItem).SchoolLocation) %>'
                                                            Width="98%" ToolTip="School Location" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel GroupingText="Course" runat="server" ID="EducationControl_otherDetailsPanel">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 12%">
                                                        <asp:Label runat="server" ID="EducationControl_degreeLabel" Text="Degree" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 32%">
                                                        <asp:TextBox runat="server" ID="EducationControl_degreeTextBox" Text='<%# Eval("DegreeName") %>'
                                                            Width="98%" ToolTip="Degree Name" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 2%">
                                                    </td>
                                                    <td style="width: 25%">
                                                        <asp:Label runat="server" ID="EducationControl_specializationLabel" Text="Specialization"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 28%">
                                                        <asp:TextBox runat="server" ID="EducationControl_specializationTextBox" Text='<%# Eval("Specialization") %>'
                                                            Width="95%" ToolTip="Specialization" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="EducationControl_gpaLabel" Text="GPA" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="EducationControl_gpaTextBox" Text='<%# Eval("GPA") %>'
                                                            Width="98%" ToolTip="GPA" MaxLength="5"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="EducationControl_graduationDtLabel" Text="Graduation Date"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <ajaxToolKit:MaskedEditExtender ID="EducationControl_MaskedEditExtender" runat="server"
                                                            TargetControlID="EducationControl_graduationDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                            DisplayMoney="None" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                        <ajaxToolKit:MaskedEditValidator ID="EducationControl_MaskedEditValidator" runat="server"
                                                            ControlExtender="EducationControl_MaskedEditExtender" ControlToValidate="EducationControl_graduationDtTextBox"
                                                            EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                            TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                            ValidationGroup="MKE" />
                                                        <ajaxToolKit:CalendarExtender ID="EducationControl_customCalendarExtender" runat="server"
                                                            TargetControlID="EducationControl_graduationDtTextBox" CssClass="MyCalendar"
                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="EducationControl_calendarGraduationDtImageButton" />
                                                        <asp:TextBox runat="server" ID="EducationControl_graduationDtTextBox" Text='<%# Eval("GraduationDate") %>'
                                                            ToolTip="Graduation Date" Width="72px"></asp:TextBox>
                                                        <asp:ImageButton ID="EducationControl_calendarGraduationDtImageButton" SkinID="sknCalendarImageButton"
                                                            runat="server" ImageAlign="Middle" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr>
                        <td>
                            <table class="ListViewAlternateStyle" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="EducationControl_rowNumberLabel" runat="server" Text="" SkinID="sknRowNumber"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ID="EducationControl_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                            ToolTip="Delete Education" CommandName="deleteEducation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel GroupingText="Institution" runat="server" ID="EducationControl_schoolPanel">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 20%">
                                                        <asp:Label runat="server" ID="EducationControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <asp:TextBox ID="EducationControl_deleteRowIndex" runat="server" Text='<%# Eval("EducationId") %>'
                                                            Style="display: none" />
                                                    </td>
                                                    <td style="width: 80%">
                                                        <asp:TextBox runat="server" ID="EducationControl_nameTextBox" Text='<%# Eval("SchoolName") %>'
                                                            Width="98%" ToolTip="School Name" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="EducationControl_locationLabel" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="EducationControl_locationTextBox" Text='<%# SetLocation(((Forte.HCM.DataObjects.Education)Container.DataItem).SchoolLocation) %>'
                                                            Width="98%" ToolTip="School Location" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel GroupingText="Course" runat="server" ID="EducationControl_otherDetailsPanel">
                                            <table>
                                                <tr>
                                                    <td style="width: 12%">
                                                        <asp:Label runat="server" ID="EducationControl_degreeLabel" Text="Degree" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 32%">
                                                        <asp:TextBox runat="server" ID="EducationControl_degreeTextBox" Text='<%# Eval("DegreeName") %>'
                                                            Width="98%" ToolTip="Degree Name" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 2%">
                                                    </td>
                                                    <td style="width: 25%">
                                                        <asp:Label runat="server" ID="EducationControl_specializationLabel" Text="Specialization"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 28%">
                                                        <asp:TextBox runat="server" ID="EducationControl_specializationTextBox" Text='<%# Eval("Specialization") %>'
                                                            Width="95%" ToolTip="Specialization" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="EducationControl_gpaLabel" Text="GPA" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="EducationControl_gpaTextBox" Text='<%# Eval("GPA") %>'
                                                            Width="98%" ToolTip="GPA" MaxLength="5"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="EducationControl_graduationDtLabel" Text="Graduation Date"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <ajaxToolKit:MaskedEditExtender ID="EducationControl_MaskedEditExtender" runat="server"
                                                            TargetControlID="EducationControl_graduationDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                            DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                        <ajaxToolKit:MaskedEditValidator ID="EducationControl_MaskedEditValidator" runat="server"
                                                            ControlExtender="EducationControl_MaskedEditExtender" ControlToValidate="EducationControl_graduationDtTextBox"
                                                            EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                            TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                            ValidationGroup="MKE" />
                                                        <ajaxToolKit:CalendarExtender ID="EducationControl_customCalendarExtender" runat="server"
                                                            TargetControlID="EducationControl_graduationDtTextBox" CssClass="MyCalendar"
                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="EducationControl_calendarGraduationDtImageButton" />
                                                        <asp:TextBox runat="server" ID="EducationControl_graduationDtTextBox" Text='<%# Eval("GraduationDate") %>'
                                                            ToolTip="Graduation Date" Width="72px"></asp:TextBox>
                                                        <asp:ImageButton ID="EducationControl_calendarGraduationDtImageButton" SkinID="sknCalendarImageButton"
                                                            runat="server" ImageAlign="Middle" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
            </asp:ListView>
            <asp:UpdatePanel ID="EducationControl_confirmPopUpUpdatPanel" runat="server">
                <ContentTemplate>
                    <div id="EducationControl_hiddenDIV" runat="server" style="display: none">
                        <asp:Button ID="EducationControl_hiddenPopupModalButton" runat="server" />
                    </div>
                    <ajaxToolKit:ModalPopupExtender ID="EducationControl_deleteModalPopupExtender" runat="server"
                        PopupControlID="EducationControl_deleteConfirmPopUpPanel" TargetControlID="EducationControl_hiddenPopupModalButton"
                        BackgroundCssClass="modalBackground">
                    </ajaxToolKit:ModalPopupExtender>
                    <asp:Panel ID="EducationControl_deleteConfirmPopUpPanel" runat="server" Style="display: none;
                        width: 30%; height: 210px; background-position: bottom; background-color: #283033;">
                        <uc1:ConfirmMsgControl ID="EducationControl_confirmPopupExtenderControl" runat="server"
                            Type="YesNo" Title="Delete Education" Message="Are you sure want to delete education details?"
                            OnOkClick="EducationControl_okPopUpClick" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
