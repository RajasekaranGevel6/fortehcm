﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterviewSessionPreviewControl.ascx.cs" 
Inherits="Forte.HCM.UI.CommonControls.InterviewSessionPreviewControl" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="InterviewSessionPreviewControl_questionResultLiteral" runat="server"
                            Text="Interview Session"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top" align="right">
                        <asp:ImageButton ID="InterviewSessionPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" cellpadding="2" cellspacing="5" border="0">
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="InterviewSessionPreviewControl_testKeyHeadLabel" runat="server" Text="Interview ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left" style="width: 100%">
                                    <asp:Label ID="InterviewSessionPreviewControl_testKeyLabel" runat="server" Text="" SkinID="sknLabelFieldText"
                                        ReadOnly="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="InterviewSessionPreviewControl_testNameHeadLabel" runat="server" Text="Interview Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" colspan="3" style="width: 35%">
                                    <asp:Label ID="InterviewSessionPreviewControl_testNameLabel" runat="server" Text="" ReadOnly="true"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="InterviewSessionPreviewControl_sessionKeyHeadLabel" runat="server" Text="Interview Session ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" style="width: 35%">
                                    <asp:Label ID="InterviewSessionPreviewControl_sessionKeyLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="InterviewSessionPreviewControl_sessionNoHeadLabel" runat="server" Text="Number of Sessions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" style="width: 35%">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="InterviewSessionPreviewControl_sessionNoLabel" runat="server" Width="30px"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="InterviewSessionPreviewControl_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <table width="60%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="InterviewSessionPreviewControl_expiryDateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewSessionPreviewControl_positionProfileHeadLabel" runat="server"
                                        Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewSessionPreviewControl_positionProfileLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="InterviewSessionPreviewControl_sessionDescLabel" runat="server" Text="Session Description"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="literal_text_wrap" style="height: 40px; width: 750px; overflow: auto;word-wrap: break-word;white-space:normal;">
                                        <asp:Literal ID="InterviewSessionPreviewControl_sessionDescLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="InterviewSessionPreviewControl_instructionsLabel" runat="server" Text="Interview Instructions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="literal_text_wrap" style="height: 40px; width: 750px; overflow: auto;word-wrap: break-word;white-space:normal;">
                                        <asp:Literal ID="InterviewSessionPreviewControl_instructionsLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
