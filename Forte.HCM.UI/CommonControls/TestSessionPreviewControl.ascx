﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestSessionPreviewControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestSessionPreviewControl" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="TestSessionPreviewControl_questionResultLiteral" runat="server"
                            Text="Test Session"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top" align="Right">
                        <asp:ImageButton ID="TestSessionPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" cellpadding="2" cellspacing="5" border="0">
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="TestSessionPreviewControl_testKeyHeadLabel" runat="server" Text="Test ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left" style="width: 100%">
                                    <asp:Label ID="TestSessionPreviewControl_testKeyLabel" runat="server" Text="" SkinID="sknLabelFieldText"
                                        ReadOnly="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="TestSessionPreviewControl_testNameHeadLabel" runat="server" Text="Test Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" colspan="3" style="width: 35%">
                                    <asp:Label ID="TestSessionPreviewControl_testNameLabel" runat="server" Text="" ReadOnly="true"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="TestSessionPreviewControl_sessionKeyHeadLabel" runat="server" Text="Test Session ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" style="width: 35%">
                                    <asp:Label ID="TestSessionPreviewControl_sessionKeyLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="TestSessionPreviewControl_sessionNoHeadLabel" runat="server" Text="Number of Sessions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" style="width: 35%">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="TestSessionPreviewControl_sessionNoLabel" runat="server" Width="30px"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_creditHeadLabel" runat="server" Text="Total Credits (in $)"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_creditLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_positionProfileHeadLabel" runat="server"
                                        Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_positionProfileLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_timeLimitHeadLabel" runat="server" Text="Time Limit"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_timeLimitLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_recommendedTimeHeadLabel" runat="server"
                                        Text="Recommended Time Limit" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_recommendedTimeLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <table width="60%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="TestSessionPreviewControl_expiryDateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_randomSelectionHeadLabel" runat="server"
                                        Text="Randomize Questions Ordering" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:CheckBox ID="TestSessionPreviewControl_randomSelectionCheckBox" runat="server"
                                        Enabled="False"></asp:CheckBox>
                                    <asp:Label ID="TestSessionPreviewControl_randomSelectionTextLabel" runat="server"
                                        SkinID="sknLabelFieldText" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_displayResultsLabel" runat="server" Text="Display Results to Candidate"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:CheckBox ID="TestSessionPreviewControl_displayResultsCheckBox" runat="server"
                                        Enabled="False"></asp:CheckBox>
                                    <asp:Label ID="TestSessionPreviewControl_displayResultsFieldLabel" runat="server"
                                        SkinID="sknLabelFieldText" Visible="false"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="TestSessionPreviewControl_cyberProctorateLabel" runat="server" Text="Enable Cyber Proctoring"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:CheckBox ID="TestSessionPreviewControl_cyberProctorateCheckBox" runat="server"
                                        Enabled="False"></asp:CheckBox>
                                    <asp:Label ID="TestSessionPreviewControl_cyberProctorateFieldLabel" runat="server"
                                        SkinID="sknLabelFieldText" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="TestSessionPreviewControl_sessionDescLabel" runat="server" Text="Session Description"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="literal_text_wrap" style="height: 40px; width: 750px; overflow: auto;word-wrap: break-word;white-space:normal">
                                        <asp:Literal ID="TestSessionPreviewControl_sessionDescLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="TestSessionPreviewControl_instructionsLabel" runat="server" Text="Test Instructions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="literal_text_wrap" style="height: 40px; width: 750px; overflow: auto;word-wrap: break-word;white-space:normal">
                                        <asp:Literal ID="TestSessionPreviewControl_instructionsLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
