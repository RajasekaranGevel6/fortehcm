﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class AutomatedTestSummaryControl : UserControl
    {
        private const string onmouseoverStyle = "className='grid_normal_row'";
        private const string onmouseoutStyle = "className='grid_alternate_row'";

        public TestStatistics DataSource
        {
            set
            {
                if (value == null)
                    return;
                AutomatedTestSummaryControl_avgTimeLabel.Text =
                    Utility.ConvertSecondsToHoursMinutesSeconds
                    (Convert.ToInt32(value.AverageTimeTakenByCandidates));
                AutomatedTestSummaryControl_testSegmemtGridView.DataSource = value.AutomatedTestSummaryGrid;
                AutomatedTestSummaryControl_testSegmemtGridView.DataBind();
                AutomatedTestSummaryControl_noOfQuestionLabel.Text = value.NoOfQuestions.ToString();
//                AutomatedTestSummaryControl_costOfTestLabel.Text = value.TestCost.ToString("0.##");
                AutomatedTestSummaryControl_costOfTestLabel.Text = value.TestCost.ToString("N2");
                //AutomatedTestSummaryControl_avgTimeLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds
                //    (Convert.ToInt32(value.AverageTimeTakenByCandidates) * value.NoOfQuestions);
                AutomatedTestSummaryControl_avgTimeLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds
                    (Convert.ToInt32(value.AverageTimeTakenByCandidates));
                AutomatedTestSummaryControl_avgComplexityLabel.Text = value.AutomatedTestAverageComplexity;
                ManualTestSummaryControl_testAreaChart.DataSource = value.TestAreaStatisticsChartData;
                AutomatedTestSummaryControl_complexityChart.DataSource = value.ComplexityStatisticsChartData;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadDetails();

            //AutomatedTestSummaryControl_testSummaryPlusSpan.Attributes.Add("onclick", "ShoworHideSingleDiv('" +
            //    AutomatedTestSummaryControl_testSummaryDiv.ClientID + "','" + AutomatedTestSummaryControl_testSummaryPlusSpan.ClientID + "','" +
            //    AutomatedTestSummaryControl_testSummaryMinusSpan.ClientID + "')");
            //AutomatedTestSummaryControl_testSummaryMinusSpan.Attributes.Add("onclick", "ShoworHideSingleDiv('" +
            //    AutomatedTestSummaryControl_testSummaryDiv.ClientID + "','" + AutomatedTestSummaryControl_testSummaryPlusSpan.ClientID + "','" +
            //    AutomatedTestSummaryControl_testSummaryMinusSpan.ClientID + "')");

            //AutomatedTestSummaryControl_testSegmentPlusSpan.Attributes.Add("onclick", "ShoworHideSingleDiv('" +
            //    AutomatedTestSummaryControl_testSegmentDiv.ClientID + "','" + AutomatedTestSummaryControl_testSegmentPlusSpan.ClientID + "','" +
            //    AutomatedTestSummaryControl_testSegmentMinusSpan.ClientID + "')");
            //AutomatedTestSummaryControl_testSegmentMinusSpan.Attributes.Add("onclick", "ShoworHideSingleDiv('" +
            //    AutomatedTestSummaryControl_testSegmentDiv.ClientID + "','" + AutomatedTestSummaryControl_testSegmentPlusSpan.ClientID + "','" +
            //    AutomatedTestSummaryControl_testSegmentMinusSpan.ClientID + "')");

            //AutomatedTestSummaryControl_testAreaPlusSpan.Attributes.Add("onclick", "ShoworHideSingleDiv('" +
            //    AutomatedTestSummaryControl_testAreaDiv.ClientID + "','" + AutomatedTestSummaryControl_testAreaPlusSpan.ClientID + "','" +
            //    AutomatedTestSummaryControl_testAreaMinusSpan.ClientID + "')");
            //AutomatedTestSummaryControl_testAreaMinusSpan.Attributes.Add("onclick", "ShoworHideSingleDiv('" +
            //    AutomatedTestSummaryControl_testAreaDiv.ClientID + "','" + AutomatedTestSummaryControl_testAreaPlusSpan.ClientID + "','" +
            //    AutomatedTestSummaryControl_testAreaMinusSpan.ClientID + "')");

            string chartType = "Pie";
            string chartName = "TestAreas";
            string title = "Test Area Statistics";

            ManualTestSummaryControl_testAreaChart.Attributes.Add("onclick", "javascript:return ShowZoomedChart('" + chartType + "','" + chartName + "','" + title + "');");

            chartType = "Pie";
            chartName = "Complexities";
            title = "Complexity Statistics";

            AutomatedTestSummaryControl_complexityChart.Attributes.Add("onclick", "javascript:return ShowZoomedChart('" + chartType + "','" + chartName + "','" + title + "');");

        }

        private void LoadDetails()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Category");
            dt.Columns.Add("Subject");
            dt.Columns.Add("TestArea");
            dt.Columns.Add("Complexity");
            dt.Columns.Add("QuestionCount");

            dt.Rows.Add("Technology", "Informatica Developer", "Concept", "Normal", "4");
            dt.Rows.Add("Technology", "Informatica Developer", "Concept", "Simple", "3");
            dt.Rows.Add("Technology", "Informatica Developer", "Syntax", "Complex", "5");
            dt.Rows.Add("Technology", "Informatica Developer", "Problem Solving", "Simple", "2");
            dt.Rows.Add("Technology", "Informatica Developer", "Problem Solving", "Complex", "2");
            dt.Rows.Add("Technology", "Informatica Developer", "Best Practices", "Normal", "5");
            dt.Rows.Add("Technology", "Informatica Developer", "Best Practices", "Simple", "4");

            Cache["SearchDataTable"] = dt;

            AutomatedTestSummaryControl_testSegmemtGridView.DataSource = dt;
            AutomatedTestSummaryControl_testSegmemtGridView.DataBind();

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("TestArea");
            dataTable.Columns.Add("QuestionCount");
            dataTable.Rows.Add("Concept", "7");
            dataTable.Rows.Add("Syntax", "5");
            dataTable.Rows.Add("Problem Solving", "4");
            dataTable.Rows.Add("Best Practices", "9");

            //AutomatedTestSummaryControl_testAreaGridView.DataSource = dataTable;
            //AutomatedTestSummaryControl_testAreaGridView.DataBind();


        }

        protected void AutomatedTestSummaryControl_testSegmemtGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

            }

        }

        protected void AutomatedTestSummaryControl_testSegmemtGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = GetSortColumnIndex();
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }


        //private void SortGridView(string sortExpression, string direction)
        //{
        //    DataTable dt;
        //    if (Cache["SearchDataTable"] != null)
        //    {
        //        dt = Cache["SearchDataTable"] as DataTable;
        //        DataView dv = new DataView(dt);
        //        dv.Sort = sortExpression + direction;
        //        AutomatedTestSummaryControl_testSegmemtGridView.DataSource = dv;
        //        AutomatedTestSummaryControl_testSegmemtGridView.DataBind();
        //    }
        //}
        //private int GetSortColumnIndex()
        //{
        //    foreach (DataControlField field in AutomatedTestSummaryControl_testSegmemtGridView.Columns)
        //    {
        //        if (field.SortExpression ==
        //                     (string)ViewState["SortExpression"])
        //        {
        //            return AutomatedTestSummaryControl_testSegmemtGridView.Columns.IndexOf(field);
        //        }
        //    }
        //    return -1;
        //}

        //private void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    // Create the sorting image based on the sort direction.
        //    Image sortImage = new Image();
        //    if (GridViewSortDirection == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    // Add the image to the appropriate header cell.
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);

        //}

    }

    



}

