﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class InterviewTestDetailsControl {
        
        /// <summary>
        /// InterviewTestDetailsControl_testDetailsMessageLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal InterviewTestDetailsControl_testDetailsMessageLiteral;
        
        /// <summary>
        /// InterviewTestDetailsControl_testIdLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewTestDetailsControl_testIdLabel;
        
        /// <summary>
        /// InterviewTestDetailsControl_testDetailsTestIdReadOnlyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewTestDetailsControl_testDetailsTestIdReadOnlyLabel;
        
        /// <summary>
        /// InterviewTestDetailsControl_testNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewTestDetailsControl_testNameLabel;
        
        /// <summary>
        /// InterviewTestDetailsControl_testNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox InterviewTestDetailsControl_testNameTextBox;
        
        /// <summary>
        /// InterviewTestDetailsControl_testDescriptionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewTestDetailsControl_testDescriptionLabel;
        
        /// <summary>
        /// InterviewTestDetailsControl_testDescriptionTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox InterviewTestDetailsControl_testDescriptionTextBox;
        
        /// <summary>
        /// InterviewTestDetailsControl_positionProfileLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewTestDetailsControl_positionProfileLabel;
        
        /// <summary>
        /// InterviewTestDetailsControl_positionProfileTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox InterviewTestDetailsControl_positionProfileTextBox;
        
        /// <summary>
        /// InterviewTestDetailsControl_positionProfileImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton InterviewTestDetailsControl_positionProfileImageButton;
        
        /// <summary>
        /// InterviewTestDetailsControl_positionProfileHelpImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton InterviewTestDetailsControl_positionProfileHelpImageButton;
        
        /// <summary>
        /// InterviewTestDetailsControl_clearPositionProfileLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton InterviewTestDetailsControl_clearPositionProfileLinkButton;
        
        /// <summary>
        /// InterviewTestDetailsControl_positionProfileIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField InterviewTestDetailsControl_positionProfileIDHiddenField;
    }
}
