﻿using System;
using System.Web.UI;

using Forte.HCM.Trace;

namespace Forte.HCM.UI.CommonControls
{
    public partial class InterviewMenuControl : UserControl
    {
        protected string absUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // To get the host from the url.
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 3)
                    absUrl = uri[0] + "/" + uri[1] + "/" + uri[2] ;

                if (Request.QueryString["m"] != null)
                {
                    InterviewMenuControl_selectedMenuHiddenField.Value = Convert.ToString(Request.QueryString["m"]);
                }
                if (Request.QueryString["s"] != null)
                {
                    InterviewMenuControl_selectedSubMenuHiddenField.Value = Convert.ToString(Request.QueryString["s"]);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
    }
}