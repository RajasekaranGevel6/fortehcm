﻿using System;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI.CommonControls
{
    public partial class WorkFlowSideLinkButtonControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Workflow_sideButton_Click(object sender, EventArgs e)
        {
            ImageButton sourceLinkButton = (ImageButton)sender;

            if (sourceLinkButton == null)
                return;

            InvokePageBaseMethod("MoveToPage", new object[] { sourceLinkButton.CommandName });
        }

        /// <summary>
        /// Invokes the parent page base public methods
        /// </summary>
        /// <param name="MethodName">
        /// A <see cref="System.String"/> that holds the method name to invoke
        /// </param>
        /// <param name="args">
        /// A <see cref="System.Object"/> that holds the arguments to be passed to the method
        /// </param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }
    }
}