﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExternalResumePreview.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ExternalResumePreview" %>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="popup_td_padding_10" colspan="2">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="width: 50%" class="popup_header_text_grey">
                        <asp:Literal ID="ExternalResumePreview_headerLiteral" runat="server" Text="Preview Resume"></asp:Literal>
                    </td>
                    <td style="width: 50%" align="right">
                        <asp:ImageButton ID="ExternalResumePreview_topCancelImageButton" runat="server" 
                            SkinID="sknCloseImageButton" 
                            OnClick="ExternalResumePreview_cancelImageButton_Click" Height="16px"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" valign="top" colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ExternalResumePreview_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ExternalResumePreview_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="ExternalResumePreview_tabContainerDIV" runat="server" style="width: 100%">
                                        <ajaxToolKit:TabContainer ID="ExternalResumePreview_tabContainer" runat="server"
                                            ActiveTabIndex="0">
                                            <ajaxToolKit:TabPanel ID="TabPanel1" runat="server" HeaderText="Personal">
                                                <ContentTemplate>
                                                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px">
                                                        <tr>
                                                            <td class="header_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="ExternalResumePreview_personalInformationLiterral" runat="server"
                                                                                Text="Personal Information"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" class="panel_inner_body_bg">
                                                                    <tr class="td_height_20">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_CandidateNameHeaderLabel"
                                                                                runat="server" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_CandidateNameLabel" runat="server"
                                                                                Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_LocationHeaderLabel" runat="server"
                                                                                Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_Location1Label" runat="server"
                                                                                Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="td_height_20">
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_EmailHeaderLabel" runat="server"
                                                                                Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_EmailLabel" runat="server"
                                                                                Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_Location2Label" runat="server"
                                                                                Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="td_height_20">
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_PhoneHeaderLabel" runat="server"
                                                                                Text="Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_PhoneLabel" runat="server"
                                                                                Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_personalInfomation_Location3Label" runat="server"
                                                                                Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="header_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="ExternalResumePreview_desiredPositionLiterral" runat="server" Text="Desired Position"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" class="panel_inner_body_bg">
                                                                    <tr class="td_height_20">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_desiredPayHeaderLabel" runat="server" Text="Desired Pay"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_desiredPayLabel" runat="server" Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_desiredTravelHeaderLabel" runat="server" Text="Desired Travel"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_desiredTravelLabel" runat="server" Text="N/A"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="td_height_20">
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_desiredRelocationHeaderLabel" runat="server"
                                                                                Text="Desired Relocation" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_desiredRelocationLabel" runat="server" Text="N/A"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_desiredCommuteHeaderLabel" runat="server" Text="Desired Commute"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_desiredCommuteLabel" runat="server" Text="N/A"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="td_height_20">
                                                                        <td>
                                                                            <asp:Label ID="ExternalResumePreview_desiredJobTypesHeaderLabel" runat="server" Text="Desired Job Types"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="ExternalResumePreview_desiredJobTypesLabel" runat="server" Text="N/A"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                            <ajaxToolKit:TabPanel ID="TabPanel2" runat="server" HeaderText="Experience">
                                                <ContentTemplate>
                                                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px">
                                                        <tr>
                                                            <td class="header_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="ExternalResumePreview_experienceLiterral" runat="server" Text="Experience"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align:top">
                                                                <table width="100%" class="panel_inner_body_bg">
                                                                    <tr class="td_height_20">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_experienceInMonthsHeaderLabel" runat="server"
                                                                                Text="Experience In Months" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_experienceInMonthsLabel" runat="server" Text="N/A"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_experienceRecentTitleHeaderLabel" runat="server"
                                                                                Text="Recent Title" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_experienceRecentTitleLabel" runat="server" Text="N/A"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="header_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="ExternalResumePreview_experienceHistoryLiteral" runat="server" Text="Experience History "></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align:top">
                                                                <div style="overflow: auto; height: 180px">
                                                                    <asp:GridView ID="ExternalResumePreview_experienceHistoryGridView" SkinID="AutoGenerateColumTrueGridView"
                                                                        runat="server">
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                            <ajaxToolKit:TabPanel ID="TabPanel3" runat="server" HeaderText="Skill">
                                                <ContentTemplate>
                                                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px">
                                                        <tr>
                                                            <td class="header_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="ExternalResumePreview_educationalLiterral" runat="server" Text="Education"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align:top">
                                                                <div style="overflow: auto; height: 100px">
                                                                    <asp:GridView ID="ExternalResumePreview_educationalGridView" SkinID="AutoGenerateColumTrueGridView" runat="server">
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="header_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="ExternalResumePreview_additionEducationalLiterral" runat="server" Text="Additional Skills & Qualifications"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align:top">
                                                                <table width="100%" class="panel_inner_body_bg">
                                                                    <tr class="td_height_20">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalManagedOthersHeaderLabel" runat="server" Text="Managed Others"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalManagedOthersLabel" runat="server" Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalSecurityClearnceHeaderLabel" runat="server" Text="Security Clearance" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalSecurityClearnceLabel" runat="server" Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="td_height_20">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalFelonyConvictionHeaderLabel" runat="server" Text="Felony Conviction"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalFelonyConvictionLabel" runat="server" Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalMilitaryExperiencesHeaderLabel" runat="server" Text="Military Experience" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationalMilitaryExperiencesLabel" runat="server" Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                     <tr class="td_height_20">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationaLanguagesSpokenHeaderLabel" runat="server" Text="Languages Spoken" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ExternalResumePreview_educationaLanguagesSpokenLabel" runat="server" Text="N/A" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                            <ajaxToolKit:TabPanel ID="TabPanel4" runat="server" HeaderText="Resume">
                                                <ContentTemplate>
                                                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px">
                                                        <tr>
                                                            <td class="header_bg">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                                            <asp:Literal ID="ExternalResumePreview_resumeDocumentLiterral" runat="server" Text="Resume Document"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align:top">
                                                                <table width="100%" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="overflow: auto; height: 270px;vertical-align:top;">
                                                                                <asp:Literal ID="ExternalResumePreview_resumeContentLiterral" runat="server"></asp:Literal>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                        </ajaxToolKit:TabContainer>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_20">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" align="left">
            <asp:LinkButton ID="ExternalResumePreview_previewFormCancelLinkButton" runat="server"
                Text="Cancel" SkinID="sknPopupLinkButton" OnClick="ExternalResumePreview_previewFormCancelLinkButton_Click"></asp:LinkButton>
        </td>
    </tr>
</table>
