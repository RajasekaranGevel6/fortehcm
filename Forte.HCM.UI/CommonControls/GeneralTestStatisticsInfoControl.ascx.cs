﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// GeneralTestStatisticsInfoControl.cs
// File that represents the user interface for GeneralTestStatisticsInfoControl user control.
// This will helps to view the test statistics information 

#endregion Header

#region Directives
using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the user interface and functionalities for the
    /// GeneralTestStatisticsInfoControl .
    /// This control helps to view the general test statistics information 
    /// </summary>
    public partial class GeneralTestStatisticsInfoControl : UserControl, IWidgetControl
    {
        #region Event Handler
        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                GeneralTestStatisticsInfoControl_categoryDIV.Style.Add("height","100%");
                WidgetMultiSelectControl.Visible = false;
                LoadControlValues(ViewState["instance"] as WidgetInstance);
            }
        }

        protected void WidgetMultiSelectControl_cancelClick(object sender, EventArgs e)
        {
            WidgetMultiSelectControlPrint.SelectedProperties = "";
        }

       protected void WidgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(true);
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GeneralTestStatisticsInfoControl_selectProperty(object sender, EventArgs e)
        {
            try
            {
                CheckBoxList checkBoxList = (CheckBoxList)sender;
                StringBuilder selectedPropertyStringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        selectedPropertyStringBuilder.Append(checkBoxList.Items[i].Text);
                        selectedPropertyStringBuilder.Append(",");
                    }
                }
                WidgetMultiSelectControl.SelectedProperties = selectedPropertyStringBuilder.ToString().TrimEnd(',');
                WidgetMultiSelectControlPrint.SelectedProperties = selectedPropertyStringBuilder.ToString().TrimEnd(',');
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion Event Handler

        #region Private Method
        /// <summary>
        /// Represents the method to load the values 
        /// </summary>
        private void LoadControlValues(WidgetInstance instance)
        {
            string testKey = string.Empty;

            CandidateReportDetail candidateDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            testKey = candidateDetail.TestKey;
            //Get the data from the database 
            TestStatistics testStatisticsInformation = new ReportBLManager().
                GetGeneralTestStatisticsControlInformation(testKey);

            //Check if the data is not null 
            if (testStatisticsInformation == null)
                return;
            //If not null assign it to the control data source
            GeneralTestStatisticsInfoControlDataSource = testStatisticsInformation;
            List<DropDownItem> dropDownItemList = new ReflectionComboItem.ReflectionManager().GetListItems(testStatisticsInformation);
            dropDownItemList.Add(new DropDownItem("subjectDataSet", "subjectDataSet"));
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            if (!(bool)Session["PRINTER"])
            {
                WidgetMultiSelectControlPrint.WidgetTypePropertyDataSource = dropDownItemList;
                WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(true);
            }
        }
        #endregion Private Method
        #region Property
        /// <summary>
        /// Represents the data source for the General statistics info control
        /// </summary>
        public TestStatistics GeneralTestStatisticsInfoControlDataSource
        {
            set
            {
                bool subjectDataset = true;
                List<DropDownItem> testInfoDropDownItemList = new List<DropDownItem>();
                if ((bool)Session["PRINTER"])
                {
                    subjectDataset = false;
                    string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split(',');
                     foreach (string word in selectedProperty)
                     {
                         switch (word)
                         {
                             case "Number Of Questions":
                                 testInfoDropDownItemList.Add(new DropDownItem("Number Of Questions", value.NoOfQuestions.ToString()));
                                 break;
                             case "Mean Score":
                                 testInfoDropDownItemList.Add(new DropDownItem("Mean Score", value.MeanScore.ToString()));
                                 break;
                             case "Standard Deviation":
                                 testInfoDropDownItemList.Add(new DropDownItem("Standard Deviation", value.StandardDeviation.ToString()));
                                 break;
                             case "Score Range":
                                 testInfoDropDownItemList.Add(new DropDownItem("Score Range", "TBD"));
                                 break;
                             case "Highest Score":
                                 testInfoDropDownItemList.Add(new DropDownItem("Highest Score", value.HighestScore.ToString()));
                                 break;
                             case "Lowest Score":
                                 testInfoDropDownItemList.Add(new DropDownItem("Lowest Score", value.LowestScore.ToString()));
                                 break;
                             case "Average Time":
                                 testInfoDropDownItemList.Add(new DropDownItem("Average Time", Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(value.AverageTimeTakenByCandidates))));
                                 break;
                             case "subjectDataSet":
                                 subjectDataset = true;
                             break;
                                 
                         }
                     }
                }
                else
                {
                    testInfoDropDownItemList.Add(new DropDownItem("Number Of Questions", value.NoOfQuestions.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Mean Score", value.MeanScore.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Standard Deviation", value.StandardDeviation.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Score Range", "TBD"));
                    testInfoDropDownItemList.Add(new DropDownItem("Highest Score", value.HighestScore.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Lowest Score", value.LowestScore.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Average Time", Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(value.AverageTimeTakenByCandidates))));
                }
                GeneralTestStatisticsInfoControl_testInformationDataList.DataSource = testInfoDropDownItemList;
                GeneralTestStatisticsInfoControl_testInformationDataList.DataBind();
                ////Asssign values for the labels 
                //GeneralTestStatisticsInfoControl_noOfQuestionValueLabel.Text
                //    = value.NoOfQuestions.ToString();
                //GeneralTestStatisticsInfoControl_scoreSDValueLabel.Text
                //    = value.StandardDeviation.ToString();
                //GeneralTestStatisticsInfoControl_highScoreValueLabel.Text
                //    = value.HighestScore.ToString();
                //string averageTime = Utility.ConvertSecondsToHoursMinutesSeconds
                //    (decimal.ToInt32(value.AverageTimeTakenByCandidates));
                //GeneralTestStatisticsInfoControl_avgTimeValueLabel.
                //    Text = averageTime;
                //GeneralTestStatisticsInfoControl_meanScoreValueLabel.
                //    Text = value.MeanScore.ToString();
                //GeneralTestStatisticsInfoControl_scoreRangeValueLabel.
                //    Text = "TBD";
                ////value.ScoreRange.ToString();
                //GeneralTestStatisticsInfoControl_lowScoreValueLabel.
                //    Text = value.LowestScore.ToString();

                //Assign the datasource for the data grid
                if (subjectDataset)
                {
                    GeneralTestStatisticsInfoControl_categoryDataGrid.
                        DataSource = value.SubjectStatistics;
                    GeneralTestStatisticsInfoControl_categoryDataGrid.
                        DataBind();
                }
            }
        }
        #endregion Property

        #region IWidgetControl Members

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 

                 ViewState["instance"] = instance.InstanceKey;
                // if (!(bool)Session["PRINTER"])
                 {
                     LoadControlValues(instance);
                 }
          
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            //return new UpdatePanel[] { GeneralTestStatisticsInfoControl_updatePanel };
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            // throw new NotImplementedException();
        }

        #endregion
    }
}