﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CategoryChartControl.cs
// File that represents the user interface for the category chart details

#endregion Header

#region Directives

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class CategoryChartControl : UserControl, IWidgetControl
    {
        string testkey = string.Empty;
        List<DropDownItem> dropDownItemList = null;
        #region Event Handler

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                CategoryChartControl_selectOptionsAbsoluteScore.Checked = selectedProperty[2] == "0" ? false : true;
                CategoryChartControl_selectOptionsRelativeScore.Checked = selectedProperty[2] == "1" ? false : true;
                CategoryChartControl_contentStateHiddenField.Value = selectedProperty[1];
                CategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[0]);
                CategoryChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance, "1");
            }
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on the selected index changed 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        void CategoryChartControl_subjectDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Hanlder method that will be called on the show 
        /// comparative score link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CategoryChartControl_showComparativeScoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on radio button click
        /// link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CategoryChartControl_selectOptionsAbsoluteScore_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CategoryChartControl_selectProperty(object sender, EventArgs e)
        {
            try
            {
                CategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(false);

                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on the select all clear all link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CategoryChartControl_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on the select all clear all link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CategoryChartControl_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Event Handler

        #region Private Method

        /// <summary>
        /// Method that is used to load the chart details
        /// </summary>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            CategoryChartControl_testKeyhiddenField.Value = candidateReportDetail.TestKey;

            //Get the categories details for the test 
            MultipleSeriesChartData categoryChartDataSource = new ReportBLManager().
                GetCandidateStatisticsCategoriesChartDetails(candidateReportDetail.CandidateSessionkey,
                candidateReportDetail.TestKey, candidateReportDetail.AttemptID);

            //If chart data is not null save the data in the session 
            if (categoryChartDataSource != null)
                Session["CategoryChartDataSource"] = categoryChartDataSource;

            //Add data for the drop down list          
            var categoryList = (from f in categoryChartDataSource.MultipleSeriesChartDataSource
                                select new DropDownItem(f.Name, f.ID)).Distinct();
            CategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = categoryList.ToList();

            //Select all the check box in the check box list
            CategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

            //Load the chart with corresponding data
            LoadChart(instance,"1");
        }

        /// <summary>
        /// Represents the method to load the chart details 
        /// based on the data source
        /// </summary>
        /// <param name="categoryChartDataSource">
        /// A<see cref="MultipleSeriesChartData"/>that holds the multiseries chart data
        /// </param>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        private void LoadChart(WidgetInstance instance, string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            StringBuilder selectedSubjectId = new StringBuilder();
            string selectedSubjectIds = "";

            MultipleSeriesChartData categoryChartDataSource = null;
            if ((Session["CategoryChartDataSource"]) != null)
            {
                categoryChartDataSource = Session["CategoryChartDataSource"]
                    as MultipleSeriesChartData;
            }
            categoryChartDataSource = new MultipleSeriesChartData();
            MultipleSeriesChartData selectedChartData = new MultipleSeriesChartData();
            selectedChartData.MultipleSeriesChartDataSource = new List<MultipleChartData>();
            CheckBoxList cbxList =
                (CheckBoxList)CategoryChartControl_widgetMultiSelectControl.FindControl
                ("WidgetMultiSelectControl_propertyCheckBoxList");
            List<Category> categoryList = new List<Category>();
            //categoryList = WidgetMultiSelectControl.SelectedProperties;
            for (int i = 0; i < cbxList.Items.Count; i++)
            {
                if (cbxList.Items[i].Selected)
                {
                    Category category = new Category();
                    category.CategoryName = cbxList.Items[i].Text;
                    category.CategoryID = int.Parse(cbxList.Items[i].Value);
                    categoryList.Add(category);
                    selectedSubjectId.Append(cbxList.Items[i].Value);
                    selectedSubjectId.Append("-");
                }
            }
            if (categoryList != null && categoryList.Count != 0)
            {
                foreach (Category category in categoryList)
                {
                    if ((Session["CategoryChartDataSource"]) != null)
                    {
                        //if absolute score is selected get absolute score values
                        if (CategoryChartControl_selectOptionsAbsoluteScore.Checked)
                        {
                            selectedChartData.MultipleSeriesChartDataSource.Add
                           ((Session["CategoryChartDataSource"] as MultipleSeriesChartData).
                           MultipleSeriesChartDataSource.Find
                           (delegate(MultipleChartData multiChartData)
                           { return multiChartData.ID == category.CategoryID.ToString().Trim(); }));
                        }
                        else
                        {
                            //if relative score is selected get absolute score values
                            selectedChartData.MultipleSeriesChartDataSource.Add
                          ((Session["CategoryChartDataSource"] as MultipleSeriesChartData).
                          MultipleSeriesRelativeChartDataSource.Find
                          (delegate(MultipleChartData multiChartData)
                          { return multiChartData.ID == category.CategoryID.ToString().Trim(); }));
                        }
                        categoryChartDataSource.MultipleSeriesChartDataSource =
               selectedChartData.MultipleSeriesChartDataSource;
                    }
                }
            }
            else
            {
                categoryChartDataSource.MultipleSeriesChartDataSource = new List<MultipleChartData>();
            }

            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                categoryChartDataSource.IsShowLabel = true;
            }

            categoryChartDataSource.ChartType = SeriesChartType.Column;

            categoryChartDataSource.ChartLength = 300;

            categoryChartDataSource.ChartWidth = 350;

            categoryChartDataSource.IsDisplayAxisTitle = true;

            categoryChartDataSource.IsDisplayChartTitle = false;

            categoryChartDataSource.XAxisTitle = "Categories";

            categoryChartDataSource.YAxisTitle = "Scores";

            categoryChartDataSource.ChartTitle = "Candidate's Score Amongst Categories";

            categoryChartDataSource.IsDisplayChartTitle = false;
            dropDownItemList = new List<DropDownItem>();
            selectedSubjectIds = selectedSubjectId.ToString().TrimEnd('-');
            selectedPrintProperty.Append(selectedSubjectIds.Replace('-', ',') + "|");
            testkey = CategoryChartControl_testKeyhiddenField.Value;
            if (CategoryChartControl_contentStateHiddenField.Value == "1")
            {
                selectedPrintProperty.Append("1|");
                CategoryChartControl_showComparativeScoreLinkButton.Text = "Hide Comparative Score";
                categoryChartDataSource.IsHideComparativeScores = false;
                if (CategoryChartControl_selectOptionsAbsoluteScore.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    dropDownItemList.Add(new DropDownItem("Title", "Category Comparative Score"));
                    dropDownItemList.Add(new DropDownItem(selectedSubjectIds, Constants.ChartConstants.DESIGN_REPORT_CATEGORY_COMPARATIVE_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_CATEGORY_COMPARATIVE_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    dropDownItemList.Add(new DropDownItem("Title", "Category Comparative Relative Score"));
                    dropDownItemList.Add(new DropDownItem(selectedSubjectIds, Constants.ChartConstants.DESIGN_REPORT_CATEGORY_COMPARATIVE_RELATIVE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_CATEGORY_COMPARATIVE_RELATIVE + "-" + testkey + "-" + selectedSubjectIds;
                }
            }
            else
            {
                selectedPrintProperty.Append("0|");
                CategoryChartControl_showComparativeScoreLinkButton.Text = "Show Comparative Score";
                categoryChartDataSource.IsHideComparativeScores = true;
                if (CategoryChartControl_selectOptionsAbsoluteScore.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    dropDownItemList.Add(new DropDownItem("Title", "Category Score"));
                    dropDownItemList.Add(new DropDownItem(selectedSubjectIds, Constants.ChartConstants.DESIGN_REPORT_CATEGORY_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_CATEGORY_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    dropDownItemList.Add(new DropDownItem("Title", "Category Relative Score"));
                    dropDownItemList.Add(new DropDownItem(selectedSubjectIds, Constants.ChartConstants.DESIGN_REPORT_CATEGORY_RELATIVE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_CATEGORY_RELATIVE + "-" + testkey + "-" + selectedSubjectIds;
                }
            }
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            CategoryChartControl_multipleSeriesChartControl.MultipleChartDataSource =
              categoryChartDataSource;
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }
        #endregion Private Method

        #region IWidgetControl Members

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>                     
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Keep the value in view state for the first time load
            ViewState["IsFirstTime"] = true;

            //Load the chart details 
            LoadChartDetails(instance);

            //Add attributes for the link button 
            CategoryChartControl_showComparativeScoreLinkButton.Attributes.
                Add("onclick", "javascript:return ShowOrHideComparativeScore('" +
                CategoryChartControl_showComparativeScoreLinkButton.ClientID +
                "','" + CategoryChartControl_contentStateHiddenField.ClientID + "');");
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            // return new UpdatePanel[] { CategoryChartControl_updatePanel };
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}