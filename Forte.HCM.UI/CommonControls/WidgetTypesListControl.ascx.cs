﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class WidgetTypesListControl : System.Web.UI.UserControl
    {
        // The public events must conform to this format
        public delegate void LinkButton_Click(object sender, RepeaterCommandEventArgs e);
        public event LinkButton_Click OkClick;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public List<WidgetTypes> WidgetTypeDataSource
        {
            set
            {
                if (value == null)
                    return;

                WidgetTypesListControl_widgetTypeRepeater.DataSource = value;
                WidgetTypesListControl_widgetTypeRepeater.DataBind();

            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {

          //  Theme = "Odeish";

        }

        protected void WidgetTypesListControl_widgetTypeRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (OkClick != null)
                this.OkClick(source, e);
        }

        protected void WidgetTypesListControl_widgetTypeRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item)
            //{
            //    Image DesignReport_addImage = (Image)e.Item.FindControl("DesignReport_addImage");
            //    DesignReport_addImage.SkinID = DesignReport_addImage.AlternateText;
            //}
        }
    }
}