﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterviewQuestionDetailSummaryControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.InterviewQuestionDetailSummaryControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="InterviewQuestionDetailSummaryControl_questionResultLiteral" runat="server"
                            Text=""></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="InterviewQuestionDetailSummaryControl_topCancelImageButton" runat="server"
                                        SkinID="sknCloseImageButton" OnClick="InterviewQuestionDetailSummaryControl_topCancelImageButton_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td align="left" class="popup_td_padding_10">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <div style="height: 120px; overflow: auto;">
                                        <asp:DataGrid ID="InterviewQuestionDetailSummaryControl_categoryDataGrid" runat="server" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:BoundColumn HeaderText="Category" DataField="CategoryName"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Subject" DataField="SubjectName"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_panel_inner_bg">
                                    <table width="100%" cellpadding="0" cellspacing="2" border="0">
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_testAreaValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_complexityHeadLabel" runat="server" Text="Complexity"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_complexityValueLabel" runat="server"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_noOfAdministeredTestHeadLabel" runat="server"
                                                    Text="Interview Included" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_noOfAdministeredTestValueLabel" SkinID="sknLabelFieldText"
                                                    runat="server"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_tagHeadLabel" runat="server" Text="Tag"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="InterviewQuestionDetailSummaryControl_tagValueLabel" class="label_multi_field_text"
                                                    runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                       
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_question_icon">
                                    <div style="overflow: auto;word-wrap: break-word;white-space:normal" id="InterviewQuestionDetailSummaryControl_questionDiv" runat="server">
                                        <asp:Label ID="InterviewQuestionDetailSummaryControl_questionLabel" runat="server" class="label_multi_field_text"></asp:Label><br />
                                        <asp:Image runat="server" ID="InterviewQuestionDetailSummaryControl_questionImage" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_padding_left_20">
                                    <div style="height: 60px; overflow: auto;">
                                        <asp:PlaceHolder ID="InterviewQuestionDetailSummaryControl_answerChoicesPlaceHolder" runat="server">
                                        </asp:PlaceHolder>
                                        <asp:Panel ID="testPanel" runat="server">
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" class="popup_td_padding_5">
            <asp:Button ID="InterviewQuestionDetailSummaryControl_bottomAddButton" runat="server" Text="Add"
                SkinID="sknButtonId" OnClick="InterviewQuestionDetailSummaryControl_bottomAddButton_Click"
                Visible="false" />
            <asp:LinkButton ID="InterviewQuestionDetailSummaryControl_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                runat="server" Text="Cancel" OnClick="InterviewQuestionDetailSummaryControl_bottomCloseLinkButton_Click" />
        </td>
    </tr>
</table>
