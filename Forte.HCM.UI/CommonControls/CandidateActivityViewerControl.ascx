﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateActivityViewerControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CandidateActivityViewerControl" %>
<%@ Register Src="PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<table style="width: 100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tab_body_bg" style="width: 100%">
            <table style="width: 100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="CandidateActivityViewerControl_searchDIV" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="panel_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="panel_inner_body_bg">
                                                    <table width="100%" cellpadding="2" cellspacing="3">
                                                        <tr>
                                                            <td>
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="CandidateActivityViewerControl_candidateLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Candidate"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="CandidateActivityViewerControl_candidateTextBox" runat="server"
                                                                                    ReadOnly="true" MaxLength="50" />
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="CandidateActivityViewerControl_candidateImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the candidate" />
                                                                                <asp:HiddenField ID="CandidateActivityViewerControl_candidateIDHiddenField" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityByLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Activity By"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="CandidateActivityViewerControl_activityByTextBox" runat="server"
                                                                                    ReadOnly="true" MaxLength="50" />
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="CandidateActivityViewerControl_activityByImageButton" SkinID="sknbtnSearchicon"
                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the activity user" />
                                                                                <asp:HiddenField ID="CandidateActivityViewerControl_activityByHiddenField" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityTypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Activity"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <asp:DropDownList ID="CandidateActivityViewerControl_activityTypeDropDownList" runat="server"
                                                                                Width="100%" SkinID="sknSubjectDropDown">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityDateFromLabel" runat="server"
                                                                                Text="Activity Date From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="CandidateActivityViewerControl_activityDateFromTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="CandidateActivityViewerControl_activityDateFromCalenderImageButton"
                                                                                    SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="CandidateActivityViewerControl_activityDateFromMaskedEditExtender"
                                                                                runat="server" TargetControlID="CandidateActivityViewerControl_activityDateFromTextBox"
                                                                                Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="CandidateActivityViewerControl_activityDateFromMaskedEditValidator"
                                                                                runat="server" ControlExtender="CandidateActivityViewerControl_activityDateFromMaskedEditExtender"
                                                                                ControlToValidate="CandidateActivityViewerControl_activityDateFromTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="CandidateActivityViewerControl_activityDateFromCustomCalendarExtender"
                                                                                runat="server" TargetControlID="CandidateActivityViewerControl_activityDateFromTextBox"
                                                                                CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="CandidateActivityViewerControl_activityDateFromCalenderImageButton" />
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityDateToLabel" runat="server"
                                                                                Text="Activity Date To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <div style="float: left; padding-right: 2px;">
                                                                                <asp:TextBox ID="CandidateActivityViewerControl_activityDateToTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="CandidateActivityViewerControl_activityDateToCalenderImageButton"
                                                                                    SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                            </div>
                                                                            <ajaxToolKit:MaskedEditExtender ID="CandidateActivityViewerControl_activityDateToMaskedEditExtender"
                                                                                runat="server" TargetControlID="CandidateActivityViewerControl_activityDateToTextBox"
                                                                                Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="CandidateActivityViewerControl_activityDateToMaskedEditValidator"
                                                                                runat="server" ControlExtender="CandidateActivityViewerControl_activityDateToMaskedEditExtender"
                                                                                ControlToValidate="CandidateActivityViewerControl_activityDateToTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="CandidateActivityViewerControl_activityDateToCalendarExtender"
                                                                                runat="server" TargetControlID="CandidateActivityViewerControl_activityDateToTextBox"
                                                                                CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="CandidateActivityViewerControl_activityDateToCalenderImageButton" />
                                                                        </td>
                                                                        <td style="width: 12%">
                                                                            <asp:Label ID="CandidateActivityViewerControl_commentsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Comments"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 21%">
                                                                            <asp:TextBox ID="CandidateActivityViewerControl_commentsTextBox" runat="server" Width="97%"
                                                                                MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="CandidateActivityViewerControl_searchButton" runat="server" SkinID="sknButtonId"
                                                        Text="Search" OnClick="CandidateActivityViewerControl_searchButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="CandidateActivityViewerControl_gridViewUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div id="CandidateActivityViewerControl_gridViewDIV" runat="server">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr id="CandidateActivityViewerControl_gridViewHeaderTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="SearchCandidateRepository_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                ID="CandidateActivityViewerControl_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 2%" align="right">
                                                            <span id="CandidateActivityViewerControl_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="CandidateActivityViewerControl_searchResultsUpImage" runat="server"
                                                                    SkinID="sknMinimizeImage" /></span><span id="CandidateActivityViewerControl_searchResultsDownSpan"
                                                                        runat="server" style="display: block;"><asp:Image ID="CandidateActivityViewerControl_searchResultsDownImage"
                                                                            runat="server" SkinID="sknMaximizeImage" /></span>
                                                            <asp:HiddenField ID="CandidateActivityViewerControl_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <div id="CandidateActivityViewerControl_activityLogDetailDIV" runat="server" style="height: 225px;
                                                    overflow: auto;">
                                                    <asp:GridView ID="CandidateActivityViewerControl_activityLogDetailGridView" runat="server"
                                                        AllowSorting="true" AutoGenerateColumns="false" OnSorting="CandidateActivityViewerControl_activityLogDetailGridView_Sorting"
                                                        OnRowCreated="CandidateActivityViewerControl_activityLogDetailGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Activity Date" SortExpression="ACTIVITY_DATE" HeaderStyle-Width="18%"
                                                                ItemStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_activityDateLabel"
                                                                        runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("ActivityDate")))%>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activity By" SortExpression="ACTIVITY_BY_NAME" HeaderStyle-Width="18%"
                                                                ItemStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_activityByLabel"
                                                                        runat="server" Text='<%# Eval("ActivityByName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activity" SortExpression="ACTIVITY_TYPE" ItemStyle-Width="18%"
                                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_activityTypeLabel"
                                                                        runat="server" Text='<%# Eval("ActivityTypeName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Comments" SortExpression="COMMENTS" ItemStyle-Width="18%"
                                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_commentsLabel"
                                                                        runat="server" Text='<%# Eval("Comments") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="CandidateActivityViewerControl_pageNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:HiddenField ID="CandidateActivityViewerControl_gridPageSizeHiddenField" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
