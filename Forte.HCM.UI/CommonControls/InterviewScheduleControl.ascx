﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterviewScheduleControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.InterviewScheduleControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="InterviewScheduleControl_questionResultLiteral" runat="server" Text="View Candidate Schedule"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="InterviewScheduleControl_topCancelImageButton" runat="server"
                                        SkinID="sknCloseImageButton" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td align="left" class="popup_td_padding_10">
                        <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewSessionIDHeadLabel" runat="server"
                                        Text="Interview Session ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewSessionIDValueLabel" runat="server"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_candidateSessionIDHeadLabel" runat="server"
                                        Text="Candidate Session ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_candidateSessionIDValueLabel" runat="server"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewIDHeadLabel" runat="server" Text="Interview ID"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewIDValueLabel" runat="server" Text=""
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewNameHeadLabel" runat="server" Text="Interview Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_candidateHeadLabel" runat="server" Text="Candidate Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_candidateValueLabel" runat="server" MaxLength="250"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_emailHeadLabel" runat="server" Text="Email"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_emailValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_expiryDateLabel" runat="server" Text="Expiry Date"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_expiryDateValueLabel" runat="server" MaxLength="10"
                                        AutoCompleteType="None" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_allowInterviewToBePausedHeadLabel" runat="server"
                                        Text="Allow Interview To Be Paused" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_allowInterviewToBePausedValueLabel" runat="server"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewAuthorHeadLabel" runat="server"
                                        Text="Interview Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_interviewAuthorValueLabel" runat="server"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_positionProfileLabel" runat="server" Text="Position Profile"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="InterviewScheduleControl_positionProfileValueLabel" runat="server"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="InterviewScheduleControl_interviewSessionDescriptionHeadLabel" runat="server" Text="Session Description"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="label_multi_field_text" style="height: 55px; width: 700px; overflow: auto;word-wrap: break-word;white-space:normal;">
                                        <asp:Literal ID="InterviewScheduleControl_interviewSessionDescriptionValueLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_2">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="InterviewScheduleControl_instructionsLabel" runat="server" Text="Interview Instructions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="label_multi_field_text" style="height: 55px; width: 700px; overflow: auto;word-wrap: break-word;white-space:normal;">
                                        <asp:Literal ID="InterviewScheduleControl_instructionsValueLiteral" runat="server"
                                            SkinID="sknLabelFieldMultiText"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_2">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_5">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="InterviewScheduleControl_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                            runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
