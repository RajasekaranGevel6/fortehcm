﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class SearchCategorySubjectControl {
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryHeaderLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchCategorySubjectControl_categoryHeaderLiteral;
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchCategorySubjectControl_categoryTextBox;
        
        /// <summary>
        /// SearchCategorySubjectControl_addLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchCategorySubjectControl_addLinkButton;
        
        /// <summary>
        /// SearchCategorySubjectControl_removeLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchCategorySubjectControl_removeLinkButton;
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList SearchCategorySubjectControl_categoryDataList;
        
        /// <summary>
        /// SearchCategorySubjectControl_subjectHeaderLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchCategorySubjectControl_subjectHeaderLiteral;
        
        /// <summary>
        /// SearchCategorySubjectControl_subjectDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList SearchCategorySubjectControl_subjectDataList;
    }
}
