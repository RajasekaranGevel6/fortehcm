//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class ManualInterviewTestSummaryControl {
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_attribute_HiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ManualInterviewTestSummaryControl_attribute_HiddenField;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_testSummaryLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ManualInterviewTestSummaryControl_testSummaryLiteral;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_noOfQuestionHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_noOfQuestionHeadLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_noOfQuestionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_noOfQuestionLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_costOfTestHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_costOfTestHeadLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_costOfTestLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_costOfTestLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_avgComplexityHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_avgComplexityHeadLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_avgComplexityLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_avgComplexityLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_avgTimeHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_avgTimeHeadLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_avgTimeLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ManualInterviewTestSummaryControl_avgTimeLabel;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_testAreaLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ManualInterviewTestSummaryControl_testAreaLiteral;
        
        /// <summary>
        /// ManualInterviewTestSummaryControl_testAreaChart control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.SingleSeriesChartControl ManualInterviewTestSummaryControl_testAreaChart;
        
        /// <summary>
        /// ManualTestSummary_complexityChart control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.SingleSeriesChartControl ManualTestSummary_complexityChart;
    }
}
