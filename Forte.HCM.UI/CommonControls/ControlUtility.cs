﻿#region Header

// Copyright (C) 2010-2011 Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ControlUtility.cs
// File that holds the common utility methods used across the application.

#endregion Header

#region Directives

using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;


using Forte.HCM.DataObjects;
using Forte.HCM.BL;
using System.Xml;
using System;
using System.IO;
using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Represents the class that provide common utility methods 
    /// that will be shared among pages and user controls.
    /// </summary>
    public class ControlUtility
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="int"/> constant that holds the answer image index
        /// in the answer choices table cell.
        /// </summary>
        private const int ANSWER_IMAGE_INDEX = 0;

        /// <summary>
        /// A <see cref="int"/> constant that holds the answer text index
        /// in the answer choices table cell.
        /// </summary>
        private const int ANSWER_TEXT_INDEX = 1;

        #endregion Private Constants

        #region Public Methods

        /// <summary>
        /// Method that retrieves the sort column index for the given grid view
        /// and currently sorted column key.
        /// </summary>
        /// <param name="gridView">
        /// A <see cref="GridView"/> that holds the grid view object.
        /// </param>
        /// <param name="columnKey">
        /// A <see cref="string"/> that holds the currently sorted column key.
        /// </param>
        /// <returns></returns>
        public int GetSortColumnIndex(GridView gridView, string columnKey)
        {
            foreach (DataControlField field in gridView.Columns)
            {
                if (field.SortExpression.Split(' ')[0] == columnKey)
                {
                    return gridView.Columns.IndexOf(field);
                }
            }
            return -1;
        }

        /// <summary>
        /// Method that adds sort image (ascending or descending) to the sorted
        /// row header.
        /// </summary>
        /// <param name="columnIndex">
        /// A <see cref="int"/> that holds the column index.
        /// </param>
        /// <param name="headerRow">
        /// A <see cref="GridViewRow"/> that hollds the grid view row object.
        /// </param>
        /// <param name="sortOrder">
        /// A <see cref="string"/> that holds the sort order. 'A' represents
        /// ascending and 'D' descending.
        /// </param>
        public void AddSortImage(int columnIndex, GridViewRow headerRow,
            SortType sortOrder)
        {
            // Create and assign the sort image based on the sort direction.
            Image sortImage = new Image();

            if (sortOrder == SortType.Ascending)
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            // Add the image to the appropriate header cell.
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// Method that constructs and returns the <see cref="Table"/> for the
        /// answer choice for given list of <see cref="AnswerChoice"/>.
        /// </summary>
        /// <param name="answerChoices">
        /// A list of <see cref="AnswerChoice"/> that holds the answer choices.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the construted answer choices 
        /// table.
        /// </returns>
        public Table GetAnswerChoices(List<AnswerChoice> answerChoices, bool tableHeader)
        {
            Table table = new Table();
            table.Width = new Unit(100, UnitType.Percentage);

            if (answerChoices == null || answerChoices.Count == 0)
            {
                return table;
            }

            // Construct answer choice in 2 columns
            for (int index = 0; index < answerChoices.Count; index += 2)
            {
                TableRow tableRow = null;

                // Add answer choice for column 1.
                if (index >= answerChoices.Count)
                    break;

                // Instantiate table row.
                tableRow = new TableRow();

                // Add answer choice image and text cells to the table row.
                if (tableHeader)
                    tableRow.Cells.AddRange(GetAnswerChoiceTableHeaders(answerChoices[index]));
                else
                    tableRow.Cells.AddRange(GetAnswerChoice(answerChoices[index]));
                // Add answer choice for column 2.
                if (index + 1 >= answerChoices.Count)
                {
                    // Add table row to the table.
                    table.Rows.Add(tableRow);

                    break;
                }

                // Add answer choice image and text cells to the table row.
                if (tableHeader)
                    tableRow.Cells.AddRange(GetAnswerChoiceTableHeaders(answerChoices[index + 1]));
                else
                    tableRow.Cells.AddRange(GetAnswerChoice(answerChoices[index + 1]));


                // Add table row to the table.
                table.Rows.Add(tableRow);
            }

            return table;
        }


        /// <summary>
        /// Get Average Question Complexity value.
        /// </summary>
        /// <param name="questionDetail"></param>
        /// <returns></returns>
        public AttributeDetail GetComplexity(string atributeType, List<QuestionDetail> questionDetail)
        {
            if (questionDetail == null)
                return null;
            else if (questionDetail.Count == 0)
                return null;

            List<AttributeDetail> complexityList = new List<AttributeDetail>();
            AttributeDetail complexity = new AttributeDetail();
            complexityList = new AttributeBLManager().GetAttributesByType(atributeType, "V");

            //   decimal atributeValue;
            decimal minimumAtributeValue;
            decimal maximumAtributeValue;
            string atributeName = "";
            string atributeID = "";
            List<decimal> complexityValue = new List<decimal>();
            for (int i = 0; i < questionDetail.Count; i++)
            {
                complexityValue.Add(questionDetail[i].ComplexityValue);
            }
            for (int i = 0; i < complexityList.Count; i++)
            {

                if (decimal.Parse(complexityList[complexityList.Count - 1].AttributeValue) == complexityValue.Average())
                {
                    atributeName = complexityList[complexityList.Count - 1].AttributeName;
                    atributeID = complexityList[complexityList.Count - 1].AttributeID;
                    break;
                }
                else if (decimal.Parse(complexityList[0].AttributeValue) == complexityValue.Average())
                {
                    atributeName = complexityList[0].AttributeName;
                    atributeID = complexityList[0].AttributeID;
                    break;
                }

                if (i == 0)
                {
                    minimumAtributeValue = decimal.Parse(complexityList[i].AttributeValue);
                    maximumAtributeValue = (decimal.Parse(complexityList[i].AttributeValue) + decimal.Parse(complexityList[i + 1].AttributeValue)) / 2;
                }
                else if (i == (complexityList.Count - 1))
                {
                    atributeName = complexityList[i].AttributeName;
                    atributeID = complexityList[i].AttributeID;
                    break;

                }
                else
                {
                    minimumAtributeValue = ((decimal.Parse(complexityList[i].AttributeValue) + decimal.Parse(complexityList[i - 1].AttributeValue)) / 2) + 0.01m;
                    maximumAtributeValue = (decimal.Parse(complexityList[i].AttributeValue) + decimal.Parse(complexityList[i + 1].AttributeValue)) / 2;
                }

                if (minimumAtributeValue < complexityValue.Average() && complexityValue.Average() <= maximumAtributeValue)
                {
                    atributeName = complexityList[i].AttributeName;
                    atributeID = complexityList[i].AttributeID;
                    break;
                }
            }
            complexity.AttributeName = atributeName;
            complexity.AttributeID = atributeID;
            return complexity;
        }

        public NomenclatureCustomize LoadAttributeDetails(int tenantID, string nomenclatureAttribute)
        {
            List<NomenclatureCustomize> attributeList = new NomenClatureBLManager().GetAttributes(tenantID);

            if (attributeList == null || attributeList.Count == 0)
                return null;

            NomenclatureCustomize attributeDetails = new NomenclatureCustomize();

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.LoadXml(attributeList[0].DataSource);

            XmlElement root = xmlDocument.DocumentElement;

            Type idType = attributeDetails.GetType();

            string[] innerXML = root.InnerXml.Split('>');

            TextReader stringReader = null;


            if (nomenclatureAttribute == "Client")
                stringReader = new StringReader(innerXML[0] + ">");
            if (nomenclatureAttribute == "ClientDepartment")
                stringReader = new StringReader(innerXML[1] + ">");
            if (nomenclatureAttribute == "ClientContacts")
                stringReader = new StringReader(innerXML[2] + ">");

            using (XmlReader xmlReader = XmlReader.Create(stringReader))
            {
                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if ((xmlReader.Name == "Client") &&
                                (xmlReader.HasAttributes))
                            {
                                ClientAttributes(attributeDetails, xmlReader);
                                return attributeDetails;
                            }
                            if ((xmlReader.Name == "ClientDepartment") &&
                                (xmlReader.HasAttributes))
                            {
                                ClientDepartmnetAttributes(attributeDetails, xmlReader);
                                return attributeDetails;
                            }
                            if ((xmlReader.Name == "ClientContacts") &&
                                (xmlReader.HasAttributes))
                            {
                                ClientContactAttributes(attributeDetails, xmlReader);
                                return attributeDetails;
                            }

                            break;
                        default:
                            break;
                    }


                }
            }
            return attributeDetails;
        }

        private void ClientContactAttributes(NomenclatureCustomize attributeDetails, XmlReader xmlReader)
        {
            attributeDetails.Contacts = (xmlReader.GetAttribute("Contact"));
            attributeDetails.FirstName = (xmlReader.GetAttribute("FirstName"));
            attributeDetails.LastName = (xmlReader.GetAttribute("LastName"));
            attributeDetails.Middlename = (xmlReader.GetAttribute("MiddleName"));
            attributeDetails.StreetAddress = (xmlReader.GetAttribute("StreetAddress"));
            attributeDetails.City = (xmlReader.GetAttribute("City"));
            attributeDetails.state = (xmlReader.GetAttribute("State"));
            attributeDetails.Zipcode = (xmlReader.GetAttribute("ZipCode"));
            attributeDetails.Country = (xmlReader.GetAttribute("Country"));
            attributeDetails.FaxNo = (xmlReader.GetAttribute("FaxNO"));
            attributeDetails.PhoneNumber = (xmlReader.GetAttribute("PhoneNo"));
            attributeDetails.EmailID = (xmlReader.GetAttribute("EmailID"));
            attributeDetails.Departments = (xmlReader.GetAttribute("Departments"));
            attributeDetails.AdditionalInfo = (xmlReader.GetAttribute("AdditionalInfo"));
            attributeDetails.MyRecords = (xmlReader.GetAttribute("MyRecords"));
            attributeDetails.CreatedBy = (xmlReader.GetAttribute("CreatedBy"));
            attributeDetails.ClientID = (xmlReader.GetAttribute("ClientID"));
            attributeDetails.NoOfDepartments = (xmlReader.GetAttribute("NoOfDepartments"));
            attributeDetails.NoOfPositionProfiles = (xmlReader.GetAttribute("NoOfPositionProfiles"));
            attributeDetails.ListOfDepartments = (xmlReader.GetAttribute("ListOfDepartments"));

            //To set the is mandatory field values
            attributeDetails.FirstNameIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("FirstNameIsMandatory"));
            attributeDetails.LastNameIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("LastNameIsMandatory"));
            attributeDetails.MiddlenameIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("MiddleNameIsMandatory"));
            attributeDetails.StreetAddressIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("StreetAddressIsMandatory"));
            attributeDetails.CityIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("CityIsMandatory"));
            attributeDetails.StateIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("StateIsMandatory"));
            attributeDetails.ZipCodeIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("ZipCodeIsMandatory"));
            attributeDetails.CountryIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("CountryIsMandatory"));
            attributeDetails.FaxNoIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("FaxNOIsMandatory"));
            attributeDetails.PhoneNumberFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("PhoneNoIsMandatory"));
            attributeDetails.EmailIDFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("EmailIDIsMandatory"));
            attributeDetails.DepartmentsIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("DepartmentsIsMandatory"));
            attributeDetails.AdditionalInfoIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("AdditionalInfoIsMandatory"));


            //To set the is visible field values
            attributeDetails.FirstNameIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("FirstNameIsVisible"));
            attributeDetails.LastNameIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("LastNameIsVisible"));
            attributeDetails.MiddlenameIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("MiddleNameIsVisible"));
            attributeDetails.StreetAddressIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("StreetAddressIsVisible"));
            attributeDetails.CityIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("CityIsVisible"));
            attributeDetails.StateIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("StateIsVisible"));
            attributeDetails.ZipCodeIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("ZipCodeIsVisible"));
            attributeDetails.CountryIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("CountryIsVisible"));
            attributeDetails.FaxNoIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("FaxNOIsVisible"));
            attributeDetails.PhoneNumberIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("PhoneNoIsVisible"));
            attributeDetails.EmailIDIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("EmailIDIsVisible"));
            attributeDetails.DepartmentsIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("DepartmentsIsVisible"));
            attributeDetails.AdditionalInfoIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("AdditionalInfoIsVisible"));


            attributeDetails.ContactsPrimaryAddress = Convert.ToBoolean(xmlReader.GetAttribute("ContactsPrimaryAddress"));

        }

        private void ClientDepartmnetAttributes(NomenclatureCustomize attributeDetails, XmlReader xmlReader)
        {
            attributeDetails.Department = (xmlReader.GetAttribute("Department"));
            attributeDetails.DepartmentName = (xmlReader.GetAttribute("DepartmentName"));
            attributeDetails.Description = (xmlReader.GetAttribute("Description"));
            attributeDetails.StreetAddress = (xmlReader.GetAttribute("StreetAddress"));
            attributeDetails.City = (xmlReader.GetAttribute("City"));
            attributeDetails.state = (xmlReader.GetAttribute("State"));
            attributeDetails.Zipcode = (xmlReader.GetAttribute("ZipCode"));
            attributeDetails.Country = (xmlReader.GetAttribute("Country"));
            attributeDetails.FaxNo = (xmlReader.GetAttribute("FaxNO"));
            attributeDetails.PhoneNumber = (xmlReader.GetAttribute("PhoneNo"));
            attributeDetails.EmailID = (xmlReader.GetAttribute("EmailID"));
            attributeDetails.AdditionalInfo = (xmlReader.GetAttribute("AdditionalInfo"));
            attributeDetails.ContactName = (xmlReader.GetAttribute("ContactName"));
            attributeDetails.MyRecords = (xmlReader.GetAttribute("MyRecords"));
            attributeDetails.CreatedBy = (xmlReader.GetAttribute("CreatedBy"));
            attributeDetails.ClientID = (xmlReader.GetAttribute("ClientID"));
            attributeDetails.NoOfContacts = (xmlReader.GetAttribute("NoOfContacts"));
            attributeDetails.NoOfPositionProfiles = (xmlReader.GetAttribute("NoOfPositionProfiles"));

            //To set the is mandatory field values
            attributeDetails.DepartmentNameIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("DepartmentNameIsmandatory"));
            attributeDetails.DescriptionIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("DescriptionIsmandatory"));
            attributeDetails.StreetAddressIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("StreetAddressIsmandatory"));
            attributeDetails.CityIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("CityIsmandatory"));
            attributeDetails.StateIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("StateIsmandatory"));
            attributeDetails.ZipCodeIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("ZipCodeIsmandatory"));
            attributeDetails.CountryIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("CountryIsmandatory"));
            attributeDetails.FaxNoIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("FaxNOIsmandatory"));
            attributeDetails.PhoneNumberFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("PhoneNoIsmandatory"));
            attributeDetails.EmailIDFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("EmailIDIsmandatory"));
            attributeDetails.AdditionalFieldInfoIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("AdditionalInfoIsMandatory"));

            //To set the is visible field values
            attributeDetails.DepartmentNameIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("DepartmentNameIsVisible"));
            attributeDetails.DescriptionIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("DescriptionIsVisible"));
            attributeDetails.StreetAddressIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("StreetAddressIsVisible"));
            attributeDetails.CityIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("CityIsVisible"));
            attributeDetails.StateIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("StateIsVisible"));
            attributeDetails.ZipCodeIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("ZipCodeIsVisible"));
            attributeDetails.CountryIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("CountryIsVisible"));
            attributeDetails.FaxNoIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("FaxNOIsVisible"));
            attributeDetails.PhoneNumberIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("PhoneNoIsVisible"));
            attributeDetails.EmailIDIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("EmailIDIsVisible"));
            attributeDetails.AdditionalInfoIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("AdditionalInfoIsVisible"));

            attributeDetails.DepartmentPrimaryAddress = Convert.ToBoolean(xmlReader.GetAttribute("DepartmentPrimaryAddress"));
        }

        private void ClientAttributes(NomenclatureCustomize attributeDetails, XmlReader xmlReader)
        {
            attributeDetails.Client = (xmlReader.GetAttribute("Client"));
            attributeDetails.ClientName = (xmlReader.GetAttribute("ClientName"));
            attributeDetails.StreetAddress = (xmlReader.GetAttribute("StreetAddress"));
            attributeDetails.City = (xmlReader.GetAttribute("City"));
            attributeDetails.state = (xmlReader.GetAttribute("State"));
            attributeDetails.Zipcode = (xmlReader.GetAttribute("ZipCode"));
            attributeDetails.Country = (xmlReader.GetAttribute("Country"));
            attributeDetails.FEINNO = (xmlReader.GetAttribute("FEINNO"));
            attributeDetails.FaxNo = (xmlReader.GetAttribute("FaxNO"));
            attributeDetails.PhoneNumber = (xmlReader.GetAttribute("PhoneNo"));
            attributeDetails.EmailID = (xmlReader.GetAttribute("EmailID"));
            attributeDetails.WebsiteURL = (xmlReader.GetAttribute("WebsiteURL"));
            attributeDetails.AdditionalInfo = (xmlReader.GetAttribute("AdditionalInfo"));

            attributeDetails.ContactName = (xmlReader.GetAttribute("ContactName"));
            attributeDetails.MyRecords = (xmlReader.GetAttribute("MyRecords"));
            attributeDetails.Location = (xmlReader.GetAttribute("Location"));
            attributeDetails.CreatedBy = (xmlReader.GetAttribute("CreatedBy"));
            attributeDetails.ClientID = (xmlReader.GetAttribute("ClientID"));
            attributeDetails.NoOfDepartments = (xmlReader.GetAttribute("NoOfDepartments"));
            attributeDetails.NoOfContacts = (xmlReader.GetAttribute("NoOfContacts"));
            attributeDetails.NoOfPositionProfiles = (xmlReader.GetAttribute("NoOfPositionProfiles"));
            attributeDetails.ListOfDepartments = (xmlReader.GetAttribute("ListOfDepartments"));

            //To set the is mandatory field values
            attributeDetails.ClientNameFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("ClientNameIsmandatory"));
            attributeDetails.StreetAddressIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("StreetAddressIsmandatory"));
            attributeDetails.CityIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("CityIsmandatory"));
            attributeDetails.StateIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("StateIsmandatory"));
            attributeDetails.ZipCodeIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("ZipCodeIsmandatory"));
            attributeDetails.CountryIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("CountryIsmandatory"));
            attributeDetails.FEINNOIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("FEINNOIsmandatory"));
            attributeDetails.FaxNoIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("FaxNOIsmandatory"));
            attributeDetails.PhoneNumberFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("PhoneNoIsmandatory"));
            attributeDetails.EmailIDFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("EmailIDIsmandatory"));
            attributeDetails.WebsiteURLFieldIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("WebsiteURLIsmandatory"));
            attributeDetails.AdditionalFieldInfoIsMandatory = Convert.ToBoolean(xmlReader.GetAttribute("AdditionalInfoIsmandatory"));

            //To set the is visible field values
            attributeDetails.ClientNameIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("ClientNameIsVisible"));
            attributeDetails.StreetAddressIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("StreetAddressIsVisible"));
            attributeDetails.CityIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("CityIsVisible"));
            attributeDetails.StateIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("StateIsVisible"));
            attributeDetails.ZipCodeIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("ZipCodeIsVisible"));
            attributeDetails.CountryIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("CountryIsVisible"));
            attributeDetails.FEINNOIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("FEINNOIsVisible"));
            attributeDetails.FaxNoIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("FaxNOIsVisible"));
            attributeDetails.PhoneNumberIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("PhoneNoIsVisible"));
            attributeDetails.EmailIDIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("EmailIDIsVisible"));
            attributeDetails.WebsiteURLIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("WebsiteURLIsVisible"));
            attributeDetails.AdditionalInfoIsVisible = Convert.ToBoolean(xmlReader.GetAttribute("AdditionalInfoIsVisible"));
        }

        public CommonNomenclatureAttributes LoadCommonAttributes(int tenantID)
        {
            List<NomenclatureCustomize> attributeList = new NomenClatureBLManager().GetAttributes(tenantID);

            if (attributeList == null || attributeList.Count == 0)
                return null;

            CommonNomenclatureAttributes attributeDetails = new CommonNomenclatureAttributes();

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.LoadXml(attributeList[0].DataSource);
            attributeDetails.Client = xmlDocument.GetElementsByTagName("NomenclatureCustomization").Item(0).ChildNodes[0].Attributes[0].Value;
            attributeDetails.ClientName = xmlDocument.GetElementsByTagName("NomenclatureCustomization").Item(0).ChildNodes[0].Attributes[1].Value;
            attributeDetails.ClientDepartment = xmlDocument.GetElementsByTagName("NomenclatureCustomization").Item(0).ChildNodes[1].Attributes[0].Value;
            attributeDetails.ClientContact = xmlDocument.GetElementsByTagName("NomenclatureCustomization").Item(0).ChildNodes[2].Attributes[0].Value;
            return attributeDetails;
        }



        public NomenclatureAllAttributes LoadAllAttributeDetails(int tenantID)
        {
            List<NomenclatureCustomize> attributeList = new NomenClatureBLManager().GetAttributes(tenantID);

            if (attributeList == null || attributeList.Count == 0)
                return null;

            NomenclatureCustomize attributeDetails = new NomenclatureCustomize();

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.LoadXml(attributeList[0].DataSource);

            XmlElement root = xmlDocument.DocumentElement;

            Type idType = attributeDetails.GetType();

            string[] innerXML = root.InnerXml.Split('>');

            TextReader stringReader = null;


            //if (nomenclatureAttribute == "Client")
            //    stringReader = new StringReader(innerXML[0] + ">");
            //if (nomenclatureAttribute == "ClientDepartment")
            //    stringReader = new StringReader(innerXML[1] + ">");
            //if (nomenclatureAttribute == "ClientContacts")
            //    stringReader = new StringReader(innerXML[2] + ">");
            NomenclatureAllAttributes nomenclatureAllAttributes = null;
            for (int i = 0; i < 3; i++)
            {
                if (nomenclatureAllAttributes == null)
                    nomenclatureAllAttributes = new NomenclatureAllAttributes();
                stringReader = new StringReader(innerXML[i] + ">");
                using (XmlReader xmlReader = XmlReader.Create(stringReader))
                {
                    while (xmlReader.Read())
                    {
                        switch (xmlReader.NodeType)
                        {
                            case XmlNodeType.Element:
                                if ((xmlReader.Name == Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT) &&
                                    (xmlReader.HasAttributes))
                                {
                                    
                                    ClientAttributes(attributeDetails, xmlReader);
                                    nomenclatureAllAttributes.ClientNomenClatureAttrubutes = attributeDetails;
                                }
                                if ((xmlReader.Name == Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT) &&
                                    (xmlReader.HasAttributes))
                                {
                                    
                                    ClientDepartmnetAttributes(attributeDetails, xmlReader);
                                    nomenclatureAllAttributes.DepartmentNomenClatureAttrubutes = attributeDetails;
                                }
                                if ((xmlReader.Name == Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS) &&
                                    (xmlReader.HasAttributes))
                                {
                                    
                                    ClientContactAttributes(attributeDetails, xmlReader);
                                    nomenclatureAllAttributes.ContactNomenClatureAttrubutes = attributeDetails;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            if (nomenclatureAllAttributes != null)
                nomenclatureAllAttributes.CommonNomenclatureAttributes = LoadCommonAttributes(tenantID);

            return nomenclatureAllAttributes;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method that constructs and returns the image and text table cells 
        /// for the given <see cref="AnswerChoice"/>.
        /// </summary>
        /// <param name="answerChoice">
        /// A <see cref="AnswerChoice"/> that holds the answer choice.
        /// </param>
        /// <returns>
        /// An array of <see cref="TableCell"/> that holds the answer choice 
        /// cells.
        /// </returns>
        /// <remarks>
        /// Answer choice is constructed with 2 cells. One with correct or
        /// wrong answer image and another with answer text.
        /// </remarks>
        private TableCell[] GetAnswerChoice(AnswerChoice answerChoice)
        {
            TableCell[] tableCells = new TableCell[2];
            ImageButton imageButton = null;

            // Column 1 - Answer Image Cell
            tableCells[ANSWER_IMAGE_INDEX] = new TableCell();
            tableCells[ANSWER_IMAGE_INDEX].Width = new Unit(5, UnitType.Percentage);
            imageButton = new ImageButton();
            if (answerChoice.IsCorrect)
                imageButton.SkinID = "sknCorrectAnswer";
            else
                imageButton.SkinID = "sknWrongAnswer";

            // Add java script handler method that returns a false value to 
            // avoid post back.
            imageButton.Attributes.Add("OnClick", "javascript:return false;");

            tableCells[ANSWER_IMAGE_INDEX].Controls.Add(imageButton);
            if (answerChoice.ChoiceID == answerChoice.IsCandidateCorrect)
            {
                Label tempLabel = new Label();
                tempLabel.Width = new Unit(2);
                tableCells[ANSWER_IMAGE_INDEX].Controls.Add(tempLabel);
                tableCells[ANSWER_IMAGE_INDEX].Width = new Unit(28, UnitType.Pixel);
                ImageButton candidateImageButton = new ImageButton();

                if (answerChoice.IsCorrect)
                    candidateImageButton.SkinID = "sknCanidateCorrectAnswer";
                else
                    candidateImageButton.SkinID = "sknCanidateWrongAnswer";

                tableCells[ANSWER_IMAGE_INDEX].Controls.Add(candidateImageButton);
                candidateImageButton.Attributes.Add("OnClick", "javascript:return false;");

            }
            // Column 1 - Answer Text Cell.
            tableCells[ANSWER_TEXT_INDEX] = new TableCell();
            tableCells[ANSWER_TEXT_INDEX].Width = new Unit(45, UnitType.Percentage);
            tableCells[ANSWER_TEXT_INDEX].Text = 
                answerChoice.Choice == null ? answerChoice.Choice : answerChoice.Choice.ToString().Replace(Environment.NewLine, "<br />");
                
            return tableCells;
        }

        private TableHeaderCell[] GetAnswerChoiceTableHeaders(AnswerChoice answerChoice)
        {
            TableHeaderCell[] tableHeaderCells = new TableHeaderCell[2];
            //            TableCell[] tableCells = new TableCell[2];
            ImageButton imageButton = null;

            // Column 1 - Answer Image Cell
            tableHeaderCells[ANSWER_IMAGE_INDEX] = new TableHeaderCell();
            tableHeaderCells[ANSWER_IMAGE_INDEX].Width = new Unit(5, UnitType.Percentage);

            imageButton = new ImageButton();
            if (answerChoice.IsCorrect)
                imageButton.SkinID = "sknCorrectAnswer";
            else
                imageButton.SkinID = "sknWrongAnswer";

            // Add java script handler method that returns a false value to 
            // avoid post back.
            imageButton.Attributes.Add("OnClick", "javascript:return false;");

            tableHeaderCells[ANSWER_IMAGE_INDEX].Controls.Add(imageButton);

            // Column 1 - Answer Text Cell.
            tableHeaderCells[ANSWER_TEXT_INDEX] = new TableHeaderCell();
            tableHeaderCells[ANSWER_TEXT_INDEX].Width = new Unit(45, UnitType.Percentage);
            tableHeaderCells[ANSWER_TEXT_INDEX].Text =
                answerChoice.Choice == null ? answerChoice.Choice : answerChoice.Choice.ToString().Replace(Environment.NewLine, "<br />");
            
            return tableHeaderCells;
        }
        private static void CommonAttributes(NomenclatureCustomize attributeDetails, XmlReader xmlReader)
        {
            /* attributeDetails.ClientName = (xmlReader.GetAttribute("ClientName"));
             attributeDetails.Address = (xmlReader.GetAttribute("Address"));
             attributeDetails.PhoneNumber = (xmlReader.GetAttribute("PhoneNumber"));
             attributeDetails.EmailID = (xmlReader.GetAttribute("EmailID"));
             attributeDetails.WebsiteURL = (xmlReader.GetAttribute("WebsiteURL"));
             attributeDetails.AdditionalInfo = (xmlReader.GetAttribute("AdditionalInfo"));
             attributeDetails.ContentAuthor = (xmlReader.GetAttribute("ContentAuthor"));
             attributeDetails.ClientNameIsMandatory = (xmlReader.GetAttribute("IsClientNameMandatory"));
             attributeDetails.AddressIsMandatory = (xmlReader.GetAttribute("IsClientAddressMandatory"));
             attributeDetails.PhoneNumberIsMandatory = (xmlReader.GetAttribute("IsClientPhoenNumberMandatory"));
             attributeDetails.EmailIDIsMandatory = (xmlReader.GetAttribute("IsClientEmailIDMandatory"));
             attributeDetails.WebsiteURLIsMandatory = (xmlReader.GetAttribute("IsClientWebsiteURLMandatory"));
             attributeDetails.AdditionalInfoIsMandatory = (xmlReader.GetAttribute("IsClientAdditionInfoMandatory"));
             attributeDetails.ContentAuthorIsMandatory = (xmlReader.GetAttribute("IsClientContentAuthorMandatory"));*/
        }
        #endregion Private Methods
    }
}
