﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ChangePositionStatusControl.aspx.cs
// File that defines the user interface layout and functionalities 
// for the change position profile status control. This helps in
// changing a position profile status.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities 
    /// for the change position profile status control. This helps in
    /// changing a position profile status.
    /// </summary>
    public partial class ChangePositionStatusControl : UserControl
    {
        #region Events

        /// <summary>
        /// A <see cref="event"/> that holds the SaveButtonClicked event.
        /// </summary>
        public event EventHandler SaveButtonClicked = null;

        /// <summary>
        /// A <see cref="event"/> that holds the CancelButtonClicked event.
        /// </summary>
        public event EventHandler CancelButtonClicked = null;

        #endregion Events

        #region Public Properties

        /// <summary>
        /// Property that sets the status drop down items.
        /// </summary>
        public List<AttributeDetail> StatusDataSource
        {
            set
            {
                // Load position profile status drop down items.
                ChangePositionStatusControl_statusDropDownList.DataSource = value;
                ChangePositionStatusControl_statusDropDownList.DataTextField = "AttributeName";
                ChangePositionStatusControl_statusDropDownList.DataValueField = "AttributeID";
                ChangePositionStatusControl_statusDropDownList.DataBind();
            }
        }

        /// <summary>
        /// Property that sets or gets the selected status value.
        /// </summary>
        public string SelectedStatusValue
        {
            set
            {
                if (!Utility.IsNullOrEmpty(value) && ChangePositionStatusControl_statusDropDownList.Items.FindByValue(value) != null)
                {
                    ChangePositionStatusControl_statusDropDownList.SelectedValue = value;
                }
            }
            get
            {
                return ChangePositionStatusControl_statusDropDownList.SelectedValue;
            }
        }

        /// <summary>
        /// Property that sets or gets the selected status text.
        /// </summary>
        public string SelectedStatusText
        {
            get
            {
                if (ChangePositionStatusControl_statusDropDownList.SelectedItem != null)
                    return ChangePositionStatusControl_statusDropDownList.SelectedItem.Text;

                return string.Empty;
            }
        }

        #endregion Public Properties

        #region Event Handler

        /// <summary>
        /// Handler method that will get fired when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ChangePositionStatusControl_saveButton_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
                SaveButtonClicked(this, e);
        }

        /// <summary>
        /// Handler method that will get fired when the cancel link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ChangePositionStatusControl_cancelButton_Click(object sender, EventArgs e)
        {
            if (CancelButtonClicked != null)
                CancelButtonClicked(this, e);
        }

        #endregion Event Handler                                               
    }
}