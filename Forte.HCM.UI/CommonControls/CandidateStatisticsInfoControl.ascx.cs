﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CategoryChartControl.cs
// File that represents the user interface for the category chart details

#endregion Header

#region Directives
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;
using ReflectionComboItem;
using System.Text;
#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities
    /// for the CandidateStatisticsInfoControl . This control helps to view the 
    /// candidate statistics information for the candidate along with the test statistics
    /// </summary>
    public partial class CandidateStatisticsInfoControl : UserControl, IWidgetControl
    {
        #region Event Handlers
        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                WidgetMultiSelectControl.Visible = false;
                LoadGrid(ViewState["instance"] as WidgetInstance);
            }
        }

       protected void WidgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(true);
        }

       protected void WidgetMultiSelectControl_cancelClick(object sender, EventArgs e)
        {
            WidgetMultiSelectControlPrint.SelectedProperties = "";
        }
        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Handler metod that is called on the item data bound of the 
        /// candidate details data list 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="DataListItemEventArgs"/>that holds the event args
        /// </param>
        protected void CandidateStatisticsInfo_candidateDetailsDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.FindControl("CandidateStatisticsInfoControl_interviewCommentsLabel")
                    .Visible = false;
            }
        }
        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CandidateStatisticsInfo_selectProperty(object sender, EventArgs e)
        {
            //try
            //{
            //    WidgetMultiSelectControl.WidgetTypePropertyAllSelected(false);
            //    WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(false);
            //}
            //catch( Exception exp)
            //{
            //    throw exp;
            //}
            try
            {
                CheckBoxList checkBoxList = (CheckBoxList)sender;
                StringBuilder selectedPropertyStringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        selectedPropertyStringBuilder.Append(checkBoxList.Items[i].Text);
                        selectedPropertyStringBuilder.Append(",");
                    }
                }
                WidgetMultiSelectControl.SelectedProperties = selectedPropertyStringBuilder.ToString().TrimEnd(',');
                WidgetMultiSelectControlPrint.SelectedProperties = selectedPropertyStringBuilder.ToString().TrimEnd(',');
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion   Event Handlers

        #region Private Methods
        /// <summary>
        /// Represents the method to load the grid 
        /// </summary>
        private void LoadGrid(WidgetInstance instance)
        {
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            List<CandidateStatisticsDetail> candidateStatisticsDetail = new ReportBLManager()
                .GetCandidatesStatistics(candidateReportDetail);

            candidateStatisticsDetails = candidateStatisticsDetail[0];

            List<DropDownItem> dropDownItem = new ReflectionComboItem.ReflectionManager().GetListItems(candidateStatisticsDetail[0]);

            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItem;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            if (!(bool)Session["PRINTER"])
            {
                WidgetMultiSelectControlPrint.WidgetTypePropertyDataSource = dropDownItem;
                WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(true);
            }
        }

        public CandidateStatisticsDetail candidateStatisticsDetails
        {
            set
            {
                List<DropDownItem> testInfoDropDownItemList = new List<DropDownItem>();
                if ((bool)Session["PRINTER"])
                { 
                     string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split(',');
                     foreach (string word in selectedProperty)
                     {
                         switch (word)
                         {
                             case "Test Date":
                                 testInfoDropDownItemList.Add(new DropDownItem("Test Date", value.TestDate.ToShortDateString()));
                                 break;
                             case "Answered Correctly":
                                 testInfoDropDownItemList.Add(new DropDownItem("Questions Answered(%)", value.AnsweredCorrectly.ToString()));
                                 break;
                             case "Total Time":
                                 testInfoDropDownItemList.Add(new DropDownItem("Total Time", value.TimeTaken.ToString()));
                                 break;
                             case "Absolute Score":
                                 testInfoDropDownItemList.Add(new DropDownItem("Absolute Score", value.AbsoluteScore.ToString()));
                                 break;
                             case "Average Time Taken":
                                 testInfoDropDownItemList.Add(new DropDownItem("Average Time For QNS", value.AverageTimeTaken.ToString()));
                                 break;
                             case "Relative Score":
                                 testInfoDropDownItemList.Add(new DropDownItem("Relative Score", value.RelativeScore.ToString()));
                                 break;
                             case "Interview Date":
                                 testInfoDropDownItemList.Add(new DropDownItem("Interview Date", "TBD"));
                                 break;
                             case "Percentile":
                                 testInfoDropDownItemList.Add(new DropDownItem("Percentile", value.Percentile.ToString()));
                                 break;
                             case "Candidate Name":
                                  CandidateStatisticsInfo_nameLabel.Text = value.FirstName;
                                  break;
                             case "Address":
                                 CandidateStatisticsInfo_locationLabel.Text = value.Address;
                                 break;
                             case "Phone":
                                  CandidateStatisticsInfo_contactLabel.Text = value.Phone;
                                 break;
                             case "Synopsis":
                                 CandidateStatisticsInfo_synopsisLiteral.Text = value.Synopsis;
                                 break;
                             
                         }
                     }
                }
                else
                {
                    CandidateStatisticsInfo_nameLabel.Text = value.FirstName;
                    CandidateStatisticsInfo_locationLabel.Text = value.Address;
                    CandidateStatisticsInfo_contactLabel.Text = value.Phone;
                    CandidateStatisticsInfo_synopsisLiteral.Text = value.Synopsis;
                 
                    //testInfoDropDownItemList.Add(new DropDownItem("Candidate Session ID", value.CandidateSessionID.ToString()));
                    //testInfoDropDownItemList.Add(new DropDownItem("Attempt Number", value.AttemptNumber.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Test Date", value.TestDate.ToShortDateString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Questions Answered(%)", value.AnsweredCorrectly.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Total Time", value.TimeTaken.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Absolute Score", value.AbsoluteScore.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Average Time For QNS", value.AverageTimeTaken));
                    testInfoDropDownItemList.Add(new DropDownItem("Relative Score", value.RelativeScore.ToString()));
                    testInfoDropDownItemList.Add(new DropDownItem("Interview Date", "TBD"));
                    testInfoDropDownItemList.Add(new DropDownItem("Percentile", value.Percentile.ToString()));
                }
                CandidateStatisticsInfo_candidateDetailsDataList.DataSource = testInfoDropDownItemList;
                CandidateStatisticsInfo_candidateDetailsDataList.DataBind();
            }
        }

        #endregion Private Methods

        #region Protected Methods
        /// <summary>
        /// Represents the method to get the time in short time format 
        /// </summary>
        /// <param name="TimeTaken">
        /// A<see cref="string "/>that holds the time in its full form 
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the string in its short form
        /// </returns>
        protected string GetTime(string TimeTaken)
        {
            //Convert the string to its datetime format
            DateTime date = Convert.ToDateTime(TimeTaken);

            //Return the short date for the date
            return date.ToString("MM/dd/yyyy");
        }
        #endregion Protected Methods

        #region IWidgetControl Members

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>      
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the grid 
            LoadGrid(instance);
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            //if (commandData.CommandType == Kalitte.Dashboard.Framework.WidgetCommandType.Closed)
            //{
            //    DeleteInstance(Convert.ToString(instance.InstanceKey));
            //}
            //return new UpdatePanel[] { CandidateStatisticsInfoControl_updatePanel };
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            DashboardSurface surface = parameters.Surface;
            WidgetInstance instance = parameters.Instance;
            //surface.WidgetToolbarPrepare +=  new WidgetToolbarPrepareHandler(surface_WidgetToolbarPrepare); 
            //throw new NotImplementedException();
        }
        #endregion
    }

}