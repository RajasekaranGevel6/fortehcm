﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmClientManagementMsgControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ConfirmClientManagementMsgControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="ConfirmMsgControl_titleLiteral" runat="server"></asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" OnClick="ConfirmMsgControl_cancelButton_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="center" valign="middle">
                                    <div id="ConfirmMsgControl_messageDiv" runat="server" style="overflow: auto; height: 50px;
                                        width: 100%">
                                        <asp:Label ID="ConfirmMsgControl_messageLabel" SkinID="sknLabelFieldText" runat="server"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="ClientManagement_confirmContactDeleteAllRadioButton" runat="server"
                                        GroupName="1" Checked="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="ClientManagement_confirmContactDeleteInSpecificRadioButton"
                                        runat="server" GroupName="1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_20">
                                </td>
                            </tr>
                            <tr valign="bottom">
                                <td align="right" style="text-align: center">
                                    <asp:Button ID="ConfirmMsgControl_yesButton" runat="server" SkinID="sknButtonId"
                                        Text="Yes" Width="64px" OnClick="ConfirmMsgControl_yesButton_Click" OnCommand="ConfirmMsgControl_button_Command" />
                                    <asp:Button ID="ConfirmMsgControl_okButton" runat="server" SkinID="sknButtonId" Text="Ok"
                                        Width="45px" OnClick="ConfirmMsgControl_okButton_Click" OnCommand="ConfirmMsgControl_button_Command" />
                                    <asp:Button ID="ConfirmMsgControl_noButton" runat="server" SkinID="sknButtonId" Text="No"
                                        Width="45px" OnClick="ConfirmMsgControl_noButton_Click" />
                                    <asp:Button ID="ConfirmMsgControl_emailAttachmentButton" Visible="false" runat="server"
                                        SkinID="sknButtonId" Text="Attachment" Width="90px" />
                                    <asp:Button ID="ConfirmMsgControl_emailContentButton" Visible="false" runat="server"
                                        SkinID="sknButtonId" Text="Body Content" Width="90px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_20">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="ConfirmMsgControl_hiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
