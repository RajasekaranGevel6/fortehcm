﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmInterviewScoreMsgControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ConfirmInterviewScoreMsgControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10" style="width: 100%">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="ConfirmInterviewScoreMsgControl_titleLiteral" runat="server"></asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" OnClick="ConfirmInterviewScoreMsgControl_cancelButton_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" cellpadding="2" cellspacing="0" border="0">
                            <tr>
                                <td style="width: 100%">
                                    <table cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="publish_candidate_interview_response_instructions">
                                                * You can copy the URL links shown below the buttons, for publishing in social media
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 8px">
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 100%">
                                                <asp:Button ID="ConfirmInterviewScoreMsgControl_withOutScoreButton" Visible="false"
                                                    runat="server" SkinID="sknButtonId" Text="Publish without score(s)" CommandName="attachment" ToolTip="Click here to email the published link"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <div style="width: 650px; word-wrap: break-word;" runat="server">
                                                    <asp:HyperLink ID="ConfirmInterviewScoreMsgControl_withOutScoreUrlHyperLink" runat="server"
                                                        Target="_blank" ToolTip="Click here to view the published results. You can also copy the link and mail to others" SkinID="sknLabelFieldTextHyperLink">
                                                    </asp:HyperLink>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 8px">
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td align="left" style="width: 100%">
                                                <asp:Button ID="ConfirmInterviewScoreMsgControl_withScoreButton" Visible="false"
                                                    runat="server" SkinID="sknButtonId" Text="Publish with score(s)" CommandName="bodycontent" ToolTip="Click here to email the published link"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <div style="width: 650px; word-wrap: break-word;" runat="server">
                                                    <asp:HyperLink ID="ConfirmInterviewScoreMsgControl_withScoreUrlHyperLink" runat="server"
                                                        Target="_blank" ToolTip="Click here to view the published results. You can also copy the link and mail to others" SkinID="sknLabelFieldTextHyperLink">
                                                    </asp:HyperLink>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 8px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="ConfirmInterviewScoreMsgControl_interviewTestKey" runat="server" />
                                    <asp:HiddenField ID="ConfirmInterviewScoreMsgControl_candidateSessionKey" runat="server" />
                                    <asp:HiddenField ID="ConfirmInterviewScoreMsgControl_assessorID" runat="server" />
                                    <asp:HiddenField ID="ConfirmInterviewScoreMsgControl_attemptID" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left">
            &nbsp;&nbsp;
            <asp:LinkButton ID="ConfirmInterviewScoreMsgControl_cancelLinkButton" runat="server"
                Text="Cancel" SkinID="sknCancelLinkButton"></asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
