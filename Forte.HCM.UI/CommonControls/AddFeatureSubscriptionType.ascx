﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddFeatureSubscriptionType.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.AddFeatureSubscriptionType" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="Forte_AddFeatureType_messagetitleLiteral" runat="server">Feature Type</asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_subscription_type_bg">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="Forte_AddFeatureType_messageTd" runat="server">
                                <td>
                                    <asp:Label ID="Forte_AddFeatureType_topErrorMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                    <asp:Label ID="Forte_AddFeatureType_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                        <tr>
                                            <td style="width: 5px;">
                                               </td>
                                            <td style="width: 19%" align="left">
                                                <asp:Label ID="Forte_AddFeatureType_featureNameHeaderLabel" runat="server" Text="Name"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                <span class="mandatory" id="Forte_AddFeatureType_featureNameHeaderMandatorySpan"
                                                    runat="server">*</span>
                                                <asp:Label ID="Forte_AddFeatureType_featureIdHiddenLabel" runat="server" Visible="false"
                                                    Text="0"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 80%" colspan="3">
                                            <asp:TextBox ID="Forte_AddFeatureType_featureNameTextBox" runat="server" MaxLength="200"
                                                    Width="300px"></asp:TextBox>
                                                <asp:Label ID="Forte_AddFeatureType_featureNameLabel" runat="server" SkinID="sknLabelFieldText"
                                                    Visible="false" Width="100%"></asp:Label>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2"></td>
                                        </tr>
                                        <tr>
                                        <td></td>
                                            <td align="left" style="width: 8%">
                                                <asp:Label ID="Forte_AddFeatureType_featureUnitHeaderLabel" runat="server" Text="Unit"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                <span class="mandatory" id="Forte_AddFeatureType_featureUnitHeaderMandatorySpan"
                                                    runat="server">*</span>
                                            </td>
                                            <td align="left" style="width: 25%">
                                                <asp:Label ID="Forte_AddFeatureType_featureUnitLabel" runat="server" SkinID="sknLabelFieldText"
                                                    Visible="false"></asp:Label>
                                                <asp:DropDownList ID="Forte_AddFeatureType_featureUnitDropDownList" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:5px;">
                                            </td>
                                            <td align="left" id="Forte_AddFeatureType_featureDefaultValueHeaderTr" runat="server">
                                                <asp:Label ID="Forte_AddFeatureType_featureDefaultValueHeaderLabel" runat="server"
                                                    Text="Default Value" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td align="left" id="Forte_AddFeatureType_featureDefaultValueTr" runat="server">
                                                <asp:TextBox ID="Forte_AddFeatureType_featureDefaultValueTextBox" runat="server"
                                                    Width="50%"></asp:TextBox>
                                                &nbsp;
                                                <asp:LinkButton ID="Forte_AddFeatureType_restoreDefaultValueLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Restore Default" Visible="false"></asp:LinkButton>
                                            </td>
                                            <td colspan="2" align="left" style="float:left;">
                                                <asp:CheckBox ID="Forte_AddFeatureType_isUnlimitedCheckBox" runat="server" Text="Unlimited" />
                                                &nbsp;<asp:CheckBox ID="Forte_AddFeatureType_isNotApplicableCheckBox" runat="server" Text="Not Applicable" />
                                                <input type="hidden" id="Forte_AddFeatureType_defaultValueHidden" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" align="left">
            <asp:Button ID="Forte_AddFeatureType_featureSaveButton" runat="server" Text="Save"
                OnClick="Forte_AddFeatureType_featureSaveButton_Click" SkinID="sknButtonId" CommandName="New" />
            &nbsp; &nbsp;
            <asp:LinkButton ID="Forte_AddFeatureType_featureCancelLinkButton" runat="server"
                Text="Cancel" SkinID="sknPopupLinkButton" OnClick="Forte_AddFeatureType_featureCancelButton_Click">
            </asp:LinkButton>
            <%--<asp:Button ID="Forte_AddFeatureType_featureCancelButton" runat="server" Text="Cancel"
                SkinID="sknButtonId" />--%>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
<script type="text/javascript">
    function Unlimited(NotApply, Unlimi, Text) {
        if (document.getElementById(Unlimi).checked) {
            document.getElementById(Text).value = "";
            document.getElementById(Text).disabled = true;
            if (document.getElementById(NotApply) != undefined || document.getElementById(NotApply) != null) {
                document.getElementById(NotApply).checked = false;
            }
        }
        else {
            document.getElementById(Text).disabled = false;
        }
    }
</script>
<script type="text/javascript">
    function NotApplicableClick(Notapply, Unlimi, Text) {
        if (document.getElementById(Notapply).checked) {
            document.getElementById(Unlimi).checked = false;
            document.getElementById(Text).value = "";
            document.getElementById(Text).disabled = true;
        }
        else {
            document.getElementById(Text).value = "";
            document.getElementById(Text).disabled = false;
        }
    }

    function DeafultValue(Defaulthn, Text, Unlimi, NotApp) {
        if (document.getElementById(Defaulthn).value == "UNLIMITED") {
            document.getElementById(Unlimi).checked = true;
            document.getElementById(Text).value = "";
            document.getElementById(Text).disabled = true;
        }
        else {
            document.getElementById(Text).value = document.getElementById(Defaulthn).value;
            document.getElementById(Text).disabled = false;
            document.getElementById(Unlimi).checked = false;
        }
        document.getElementById(NotApp).checked = false;
        return false;
    }
</script>
