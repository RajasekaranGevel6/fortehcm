﻿#region Header                                                                 
// Copyright (C) 2010-2011 Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// GroupAnalysisTestAreaChartControl.ascx.cs
// This class represents the test area chart control 
// for the group analysis report page.This control is used to 
// display the chart for the test area 
//
#endregion

#region Directives                                                             

using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class GroupAnalysisTestAreaChartControl : UserControl, IWidgetControl
    {
        string testkey = string.Empty;
        List<DropDownItem> dropDownItemList = null;

        #region Event Handlers                                                 

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                GroupAnalysisTestAreaChartControl_testKeyhiddenField.Value = selectedProperty[0];
                GroupAnalysisTestAreaChartControl_absoluteScoreRadioButton.Checked = selectedProperty[1] == "0" ? false : true;
                GroupAnalysisTestAreaChartControl_relativeScoreRadioButton.Checked = selectedProperty[1] == "1" ? false : true;
                GroupAnalysisTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[2]);
                GroupAnalysisTestAreaChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on radio button 
        ///  button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void GroupAnalysisTestAreaChartControl_absoluteScoreRadioButton_CheckedChanged
           (object sender, EventArgs e)
        {
            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GroupAnalysisTestAreaChartControl_selectProperty(object sender, EventArgs e)
        {
            GroupAnalysisTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(false);

            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }

        /// <summary>
        /// Hanlder method that will be called when click the link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GroupAnalysisTestAreaChartControl_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on multi select link button added
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void GroupAnalysisTestAreaChartControl_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Event Handlers
         
        #region Private Method                                                 
        /// <summary>
        /// Represents the method to get the chart details from the database
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            DataTable multiSeriesDataTable = new ReportBLManager().
                GetGroupAnalysisTestAreaDetails(candidateReportDetail);

            GroupAnalysisTestAreaChartControl_testKeyhiddenField.Value = candidateReportDetail.TestKey;

            if (multiSeriesDataTable == null)
                return;

            var category = (from r in multiSeriesDataTable.AsEnumerable()
                            select new DropDownItem(r["Name"].ToString().Trim()
                               , r["TEST_AREA_ID"].ToString())).Distinct();

            GroupAnalysisTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = category.ToList();

            //Select all the check box in the check box list
            GroupAnalysisTestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

            Session["TestAreaChartDataSource"] = multiSeriesDataTable;

            LoadChart(instance,"1");
        }

        /// <summary>
        /// Represents the method to load the chart 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChart(WidgetInstance instance,string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            DataTable testAreaChartDataTable = null;

            //Use string builder and string for naming the chart
            //This is used to store all the selected category
            StringBuilder selectedTestAreaId = new StringBuilder();
            string selectedTestAreaIds = "";

            //Get the data table from the session 
            if ((Session["TestAreaChartDataSource"]) != null)
            {
                testAreaChartDataTable = Session["TestAreaChartDataSource"]
                    as DataTable;
            }

            //Assign the values for the chart control
            MultipleSeriesChartData testAreaChartDataSource = new MultipleSeriesChartData();

            DataTable selectedChartData = testAreaChartDataTable.Clone();

            List<DropDownItem> selectedList = GroupAnalysisTestAreaChartControl_widgetMultiSelectControl.SelectedItems();

            //categoryList = WidgetMultiSelectControl.SelectedProperties;

            dropDownItemList = new List<DropDownItem>();

            testkey = GroupAnalysisTestAreaChartControl_testKeyhiddenField.Value;
            selectedPrintProperty.Append(testkey + "|");
            if (selectedList != null && selectedList.Count != 0)
            {
                foreach (DropDownItem category in selectedList)
                {
                    if ((Session["TestAreaChartDataSource"]) != null)
                    {
                        DataRow[] datarow = (Session["TestAreaChartDataSource"] as DataTable).
                            Select("TEST_AREA_ID='" + category.ValueText.ToString().Trim() + "'");

                        //DataRow dr = selectedChartData.NewRow();                       
                        //dr.ItemArray = datarow;
                        selectedChartData.Rows.Add(datarow[0].ItemArray);
                        //Append the selected category id to the string 
                        selectedTestAreaId.Append(category.ValueText);
                        selectedTestAreaId.Append("-");     
                    }
                }
            }
            else
            {
                testAreaChartDataTable = new DataTable();
            }

                //If absolute score is selected remove the relative score details
                if (GroupAnalysisTestAreaChartControl_absoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    selectedChartData.Columns.Remove("LT50PERCENTILE_RELATIVE");
                    selectedChartData.Columns.Remove("GT50PERCENTILE_RELATIVE");
                    selectedChartData.Columns.Remove("GT75PERCENTILE_RELATIVE");

                    //Replace the column name as < 50 percentile
                    selectedChartData.Columns["LT50Percentile"].ColumnName = "< 50 Percentile";

                    //Replace the column name as 50-75 percentile
                    selectedChartData.Columns["GT50Percentile"].ColumnName = "50 - 75 Percentile";

                    //Replace the column name as > 75 percentile
                    selectedChartData.Columns["GT75Percentile"].ColumnName = "> 75 Percentile";


                    dropDownItemList.Add(new DropDownItem("Title", "Group Analysis Testarea Score"));
                    dropDownItemList.Add(new DropDownItem("Categories", Constants.ChartConstants.GROUP_REPORT_TESTAREA_ABSOLUTE + "-" + testkey + "-" + selectedTestAreaIds + ".png"));
                    testAreaChartDataSource.ChartImageName = Constants.ChartConstants.GROUP_REPORT_TESTAREA_ABSOLUTE + "-" + testkey + "-" + selectedTestAreaIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    //else remove the absolute score details
                    selectedChartData.Columns.Remove("LT50PERCENTILE");
                    selectedChartData.Columns.Remove("GT50Percentile");
                    selectedChartData.Columns.Remove("GT75Percentile");


                    //Rename the relative score details
                    //Replace the column name as < 50 percentile
                    selectedChartData.Columns["LT50PERCENTILE_RELATIVE"].ColumnName = "< 50 Percentile";

                    //Replace the column name as 50-75 percentile
                    selectedChartData.Columns["GT50PERCENTILE_RELATIVE"].ColumnName = "50 - 75 Percentile";

                    //Replace the column name as > 75 percentile
                    selectedChartData.Columns["GT75PERCENTILE_RELATIVE"].ColumnName = "> 75 Percentile";


                    dropDownItemList.Add(new DropDownItem("Title", "Group Analysis Relative Testarea Score"));
                    dropDownItemList.Add(new DropDownItem("Categories", Constants.ChartConstants.GROUP_TESTAREA_SUBJECT_RELATIVE + "-" + testkey + "-" + selectedTestAreaIds + ".png"));
                    testAreaChartDataSource.ChartImageName = Constants.ChartConstants.GROUP_TESTAREA_SUBJECT_RELATIVE + "-" + testkey + "-" + selectedTestAreaIds;
                }

                testAreaChartDataTable = selectedChartData;
           

            //Remove the last - 
            if (selectedTestAreaId.Length > 0)
                selectedTestAreaIds = selectedTestAreaId.ToString().TrimEnd('-');
            selectedPrintProperty.Append(selectedTestAreaIds.Replace('-', ',') + "|");
            if (testAreaChartDataTable == null)
                return;

            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                testAreaChartDataSource.IsShowLabel = true;
            }
          

            testAreaChartDataSource.ChartType = SeriesChartType.Column;

            testAreaChartDataSource.ChartLength = 300;

            testAreaChartDataSource.ChartWidth = 350;

            testAreaChartDataSource.IsFullNumberYAxis = true;

            testAreaChartDataSource.IsDisplayAxisTitle = true;

            testAreaChartDataSource.IsDisplayChartTitle = false;

            testAreaChartDataSource.XAxisTitle = "Test Areas";

            testAreaChartDataSource.YAxisTitle = "No Of Candidates";

            testAreaChartDataSource.ChartTitle = "Candidate's Score Amongst TestAreas";

            testAreaChartDataSource.IsDisplayChartTitle = false;

            testAreaChartDataSource.ComparisonReportDataTable = testAreaChartDataTable;

            testAreaChartDataSource.IsComparisonReport = true;

            GroupAnalysisTestAreaChartControl_multiSeriesChart.MultipleChartDataSource =
             testAreaChartDataSource;
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        #endregion Private Method

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>   
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}