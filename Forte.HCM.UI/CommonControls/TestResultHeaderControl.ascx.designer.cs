﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class TestResultHeaderControl {
        
        /// <summary>
        /// TestResultHeaderControl_testIDLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_testIDLabel;
        
        /// <summary>
        /// TestResultHeaderControl_testIDValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_testIDValueLabel;
        
        /// <summary>
        /// TestResultHeaderControl_testNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_testNameLabel;
        
        /// <summary>
        /// TestResultHeaderControl_testNameValueTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_testNameValueTextBox;
        
        /// <summary>
        /// TestResultHeaderControl_candidateNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_candidateNameLabel;
        
        /// <summary>
        /// TestResultHeaderControl_candidateNameValueHyperLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink TestResultHeaderControl_candidateNameValueHyperLink;
        
        /// <summary>
        /// TestResultHeaderControl_creditsEarnedLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_creditsEarnedLabel;
        
        /// <summary>
        /// TestResultHeaderControl_creditsEarnedValueTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_creditsEarnedValueTextBox;
    }
}
