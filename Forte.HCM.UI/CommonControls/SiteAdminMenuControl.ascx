﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteAdminMenuControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SiteAdminMenuControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="menustrip_normal">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdSetup" onmouseover="showit(0)" onmouseout="resetit(event)">
                        Setup
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdAdmin" onmouseover="showit(1)" onmouseout="resetit(event)">
                        Admin
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdAuthorization" onmouseover="showit(2)" onmouseout="resetit(event)">
                        Authorization
                    </td>
                     <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdCandidateSelfAdmin" onmouseover="showit(3)" onmouseout="resetit(event)">
                        Candidate Self Admin
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="menustrip_sublink">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="4%">
                        &nbsp;
                    </td>
                    <td align="center" valign="middle" id="describe" onmouseover="clear_delayhide(true)"
                        onmouseout="resetit(event)">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="SiteAdminMenuControl_hoverMenuHiddenField" runat="server" />
<asp:HiddenField ID="SiteAdminMenuControl_selectedMenuHiddenField" runat="server" />
<asp:HiddenField ID="SiteAdminMenuControl_selectedSubMenuHiddenField" runat="server" />
<script type="text/javascript" language="javascript">
    var submenu = new Array();
    var test_url = '<%= absUrl %>';

    submenu[0] = '<div id="div0"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td01" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/CustomerEnrollmentSetup.aspx?m=0&s=0">Tenant Enrollment</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td02" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/SubscriptionFeatureTypeSetup.aspx?m=0&s=1">Subscription & Feature Type</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td03" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/SubscriptionFeaturePricingSetup.aspx?m=0&s=2">Subscription Feature Pricing</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td></td><td id="td04" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/RoleManagement.aspx?m=0&s=3">Role Management</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td></td><td id="td05" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/ModuleManagement.aspx?m=0&s=4">Module Management</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td></td><td id="td06" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/FeatureManagement.aspx?m=0&s=5">Feature Management</a></td></tr></table></div>';
    submenu[1] = '<div id="div1"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td11" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/CorporateSubscriptionManagement.aspx?m=1&s=0">Corporate Subscription Management</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td12" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/FeatureUsage.aspx?m=1&s=1">Feature Usage</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td13" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/CyberProctorLog.aspx?m=1&s=2">Cyber Proctor Log</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td14" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/OfflineInterviewLog.aspx?m=1&s=3">Offline Interview Log</a></td></tr></table></div>';
    submenu[2] = '<div id="div2"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td21" class="menustrip_sublink_text"><a href="' + test_url + '/Authentication/UserRoleMatrix.aspx?m=2&s=0">Assign User Roles</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td22" class="menustrip_sublink_text"><a href="' + test_url + '/Authentication/RoleFeatureMatrix.aspx?m=2&s=1">Assign Role Rights</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td23" class="menustrip_sublink_text"><a href="' + test_url + '/Authentication/RoleRightsMatrix.aspx?m=2&s=2">Role Rights Matrix</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td24" class="menustrip_sublink_text"><a href="' + test_url + '/Authentication/UserRightsMatrix.aspx?m=2&s=3">Assign User Rights</a></td></tr></table></div>';
    submenu[3] = '<div id="div3"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td31" class="menustrip_sublink_text"><a href="' + test_url + '/TestMaker/TestRecommendation.aspx?m=3&s=0&parentpage=menu">Create Test Recommendation</a></td><td class="menustrip_sublink_bullet"></td></td><td id="td32" class="menustrip_sublink_text"><a href="' + test_url + '/TestMaker/SearchTestRecommendation.aspx?m=3&s=1&parentpage=menu">Search Test Recommendation</a></td><td class="menustrip_sublink_bullet"></td></td><td id="td33" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/CandidateOptions.aspx?m=3&s=2&parentpage=menu">Candidate Options</a></td><td class="menustrip_sublink_bullet"></td></td><td id="td34" class="menustrip_sublink_text"><a href="' + test_url + '/Authentication/CandidateSelfSkill.aspx?m=3&s=3&parentpage=menu">Manage Skills</a></td></tr></table></div>';

    //Set delay before submenu disappears after mouse moves out of it (in milliseconds)
    var delay_hide = 500;

    //No need to edit beyond here
    var menuobj = document.getElementById ? document.getElementById("describe") : document.all ? document.all.describe : document.layers ? document.dep1.document.dep2 : "";

    function SubMenu(parentMenu, SubMenu)
    {
        selectedId = "td" + parentMenu.toString() + SubMenu.toString();
        if (document.getElementById("div0") != null)
        {
            parentControl = document.getElementById("div0");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }
        if (document.getElementById("div1") != null)
        {
            parentControl = document.getElementById("div1");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById("div2") != null)
        {
            parentControl = document.getElementById("div2");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }
        if (document.getElementById("div3") != null)
        {
            parentControl = document.getElementById("div3");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }
        if (document.getElementById(selectedId) != null)
        {
            document.getElementById(selectedId).className = "menustrip_sublink_active_text";
        }
    }

    function showDefault()
    {
        if (document.getElementById("<%= SiteAdminMenuControl_selectedMenuHiddenField.ClientID %>").value != "")
        {
            val = document.getElementById("<%= SiteAdminMenuControl_selectedMenuHiddenField.ClientID %>").value;
            valSub = document.getElementById("<%= SiteAdminMenuControl_selectedSubMenuHiddenField.ClientID %>").value;

            if (val == 0)
            {
                menuobj.innerHTML = submenu[0];
                document.getElementById("tdSetup").className = "menustrip_active";
            }
            else if (val == 1)
            {
                menuobj.innerHTML = submenu[1];
                document.getElementById("tdAdmin").className = "menustrip_active";
            }
            else if (val == 2)
            {
                menuobj.innerHTML = submenu[2];
                document.getElementById("tdAuthorization").className = "menustrip_active";
            }
            else if (val == 3)
            {
                menuobj.innerHTML = submenu[3];
                document.getElementById("tdCandidateSelfAdmin").className = "menustrip_active";
            }
            SubMenu(val, parseInt(valSub) + 1);
        }
        else
        {
            document.getElementById("tdSetup").className = "menutext_normal";
            document.getElementById("tdAdmin").className = "menutext_normal";
            document.getElementById("tdAuthorization").className = "menutext_normal";
            document.getElementById("tdCandidateSelfAdmin").className = "menutext_normal";
        }
    }

    function showit(which)
    {
        clear_delayhide(false);
        thecontent = (which == -1) ? "" : submenu[which];

        if (document.getElementById || document.all)
        {
            menuobj.innerHTML = thecontent;
            if (thecontent != "")
            {
                if (thecontent.toString().indexOf('div0') != -1)
                {
                    document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";
                    document.getElementById("tdSetup").className = "menustrip_active";
                    document.getElementById("tdAdmin").className = "menutext_normal";
                    document.getElementById("tdAuthorization").className = "menutext_normal";
                    document.getElementById("tdCandidateSelfAdmin").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div1') != -1)
                {
                    document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "1";

                    document.getElementById("tdSetup").className = "menutext_normal";
                    document.getElementById("tdAdmin").className = "menustrip_active";
                    document.getElementById("tdAuthorization").className = "menutext_normal";
                    document.getElementById("tdCandidateSelfAdmin").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div2') != -1)
                {
                    document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "2";

                    document.getElementById("tdAdmin").className = "menutext_normal";
                    document.getElementById("tdSetup").className = "menutext_normal";
                    document.getElementById("tdAuthorization").className = "menustrip_active";
                    document.getElementById("tdCandidateSelfAdmin").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div3') != -1)
                {
                    document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "3";

                    document.getElementById("tdAdmin").className = "menutext_normal";
                    document.getElementById("tdSetup").className = "menutext_normal";
                    document.getElementById("tdAuthorization").className = "menutext_normal";
                    document.getElementById("tdCandidateSelfAdmin").className = "menustrip_active";
                }
                else
                {
                    document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";
                    document.getElementById("tdSetup").className = "menustrip_active";
                    document.getElementById("tdAdmin").className = "menutext_normal";
                    document.getElementById("tdAuthorization").className = "menutext_normal";
                    document.getElementById("tdCandidateSelfAdmin").className = "menutext_normal";
                }
                if (document.getElementById("<%= SiteAdminMenuControl_selectedMenuHiddenField.ClientID %>").value != "")
                {
                    val = document.getElementById("<%= SiteAdminMenuControl_selectedMenuHiddenField.ClientID %>").value;
                    valSub = document.getElementById("<%= SiteAdminMenuControl_selectedSubMenuHiddenField.ClientID %>").value;

                    SubMenu(val, parseInt(valSub) + 1);
                }
            }
            else
            {
                document.getElementById("tdSetup").className = "menutext_normal";
                document.getElementById("tdAdmin").className = "menutext_normal";
                document.getElementById("tdAuthorization").className = "menutext_normal";
                document.getElementById("tdCandidateSelfAdmin").className = "menutext_normal";

                document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value = "";

                showDefault();
            }
        }
        else if (document.layers)
        {
            menuobj.document.write(thecontent)
            menuobj.document.close()
        }
    }

    function resetit(e)
    {
        if (document.all && !menuobj.contains(e.toElement))
            delayhide = setTimeout("showit(-1)", delay_hide)
        else if (document.getElementById && e.currentTarget != e.relatedTarget && !contains_ns6(e.currentTarget, e.relatedTarget))
            delayhide = setTimeout("showit(-1)", delay_hide)
    }

    function clear_delayhide(isSubMenuHover)
    {
        if (window.delayhide)
            clearTimeout(delayhide);

        if (document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value == "")
        {
            val = document.getElementById("<%= SiteAdminMenuControl_selectedMenuHiddenField.ClientID %>").value;
        }
        else
        {
            val = document.getElementById("<%= SiteAdminMenuControl_hoverMenuHiddenField.ClientID %>").value;
        }

        if (val == 0)
        {
            document.getElementById("tdSetup").className = "menustrip_active";
        }
        else if (val == 1) 
        {
            document.getElementById("tdAdmin").className = "menustrip_active";
        }
        else if (val == 2)
        {
            document.getElementById("tdAuthorization").className = "menustrip_active";
        }
        else if (val == 3)
        {
            document.getElementById("tdCandidateSelfAdmin").className = "menustrip_active";
        }
    }

    function contains_ns6(a, b)
    {
        while (b.parentNode)
            if ((b = b.parentNode) == a)
                return true;
        return false;
    }
    
</script>
<script type="text/javascript" language="javascript">
    showDefault();
</script>
