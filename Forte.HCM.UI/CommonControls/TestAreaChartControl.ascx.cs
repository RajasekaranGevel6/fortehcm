﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestAreaChartControl.cs
// File that represents the class for the test area chart details

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class TestAreaChartControl : UserControl, IWidgetControl
    {
        string testkey = string.Empty;
        List<DropDownItem> dropDownItemList = null;
        #region Handler Event                                                  

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                TestAreaChartControl_selectOptionsAbsoluteScore.Checked = selectedProperty[2] == "0" ? false : true;
                TestAreaChartControl_selectOptionsRelativeScore.Checked = selectedProperty[2] == "1" ? false : true;
                TestAreaChartControl_comparativeStateHiddenField.Value = selectedProperty[1];
                TestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[0]);
                TestAreaChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
        }        
        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on the show 
        /// comparative score link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestAreaChartControl_showComparativeScoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on radio button click
        /// link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestAreaChartControl_selectOptionsAbsoluteScore_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void TestAreaChartControl_selectProperty(object sender, EventArgs e)
        {
            CheckBoxList checkBoxList = (CheckBoxList)sender;

            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }

        /// <summary>
        /// Hanlder method that will be called when click the link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void TestAreaChartControl_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// Hanlder method that will be called when click the link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void TestAreaChartControl_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Handler Event

        #region Private Method                                                 

        /// <summary>
        /// Represents the method to load the chart details
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>                         
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            TestAreaChartControl_testKeyhiddenField.Value = candidateReportDetail.TestKey;

            //Get the categories details for the test 
            MultipleSeriesChartData testAreaChartDataSource = new ReportBLManager().
                GetCandidateStatisticsTestAreaChartDetails(candidateReportDetail.CandidateSessionkey,
                candidateReportDetail.TestKey, candidateReportDetail.AttemptID);

            if (testAreaChartDataSource != null && testAreaChartDataSource.MultipleSeriesChartDataSource.Count > 0)
            {
                Session["TestAreaChartDataSource"] = testAreaChartDataSource;
            }

            //Add data for the drop down list          
            var testAreaList = (from f in testAreaChartDataSource.MultipleSeriesChartDataSource
                                select new DropDownItem(f.Name, f.ID)).Distinct();
            TestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = testAreaList.ToList();

            //Select all the check box in the check box list
            TestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

            LoadChart(instance,"1");

        }

        private void LoadChart(WidgetInstance instance,string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            StringBuilder selectedTestAreaId = new StringBuilder();
            string selectedTestAreaIds = "";
            MultipleSeriesChartData testAreaChartDataSource = new MultipleSeriesChartData();

            if (Session["TestAreaChartDataSource"] != null)
            {
                testAreaChartDataSource = Session["TestAreaChartDataSource"] as MultipleSeriesChartData;
            }

            testAreaChartDataSource = new MultipleSeriesChartData();
            MultipleSeriesChartData selectedChartData = new MultipleSeriesChartData();
            selectedChartData.MultipleSeriesChartDataSource = new List<MultipleChartData>();

            CheckBoxList cbxList =
                (CheckBoxList)TestAreaChartControl_widgetMultiSelectControl.FindControl
                ("WidgetMultiSelectControl_propertyCheckBoxList");

            List<AttributeDetail> testAreaList = new List<AttributeDetail>();

            //categoryList = WidgetMultiSelectControl.SelectedProperties;

            for (int i = 0; i < cbxList.Items.Count; i++)
            {
                if (cbxList.Items[i].Selected)
                {
                    AttributeDetail testArea = new AttributeDetail();
                    testArea.AttributeName = cbxList.Items[i].Text;
                    testArea.AttributeID = cbxList.Items[i].Value;
                    testAreaList.Add(testArea);
                    selectedTestAreaId.Append(cbxList.Items[i].Value);
                    selectedTestAreaId.Append("-");
                }
            }

            if (testAreaList != null && testAreaList.Count != 0)
            {
                foreach (AttributeDetail testArea in testAreaList)
                {
                    if ((Session["TestAreaChartDataSource"]) != null)
                    {
                        if (TestAreaChartControl_selectOptionsAbsoluteScore.Checked)
                        {
                            selectedChartData.MultipleSeriesChartDataSource.Add
                                ((Session["TestAreaChartDataSource"] as MultipleSeriesChartData).
                                MultipleSeriesChartDataSource.Find
                                (delegate(MultipleChartData multiChartData)
                                { return multiChartData.ID == testArea.AttributeID.Trim(); }));
                        }
                        else
                        {
                            selectedChartData.MultipleSeriesChartDataSource.Add
                               ((Session["TestAreaChartDataSource"] as MultipleSeriesChartData).
                               MultipleSeriesRelativeChartDataSource.Find
                               (delegate(MultipleChartData multiChartData)
                               { return multiChartData.ID == testArea.AttributeID.Trim(); }));
                        }
                    }
                }
                testAreaChartDataSource.MultipleSeriesChartDataSource =
                 selectedChartData.MultipleSeriesChartDataSource;
            }
            else
            {
                testAreaChartDataSource.MultipleSeriesChartDataSource = new List<MultipleChartData>();
            }

            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                testAreaChartDataSource.IsShowLabel = true;
            }

            testAreaChartDataSource.ChartType = SeriesChartType.Column;

            testAreaChartDataSource.ChartLength = 300;

            testAreaChartDataSource.ChartWidth = 350;

            testAreaChartDataSource.IsDisplayAxisTitle = true;

            testAreaChartDataSource.IsDisplayChartTitle = false;

            testAreaChartDataSource.XAxisTitle = "Test Areas";

            testAreaChartDataSource.YAxisTitle = "Scores";

            testAreaChartDataSource.IsDisplayChartTitle = false;

            testAreaChartDataSource.ChartTitle = "Candidate's Score Amongst Test Area";

            dropDownItemList = new List<DropDownItem>();
            selectedTestAreaIds = selectedTestAreaId.ToString().TrimEnd('-');
            selectedPrintProperty.Append(selectedTestAreaIds.Replace('-', ',') + "|");
            testkey = TestAreaChartControl_testKeyhiddenField.Value;
            if (TestAreaChartControl_comparativeStateHiddenField.Value == "1")
            {
                selectedPrintProperty.Append("1|");
                TestAreaChartControl_showComparativeScoreLinkButton.Text = "Hide Comparative Score";
                testAreaChartDataSource.IsHideComparativeScores = false;

                if (TestAreaChartControl_selectOptionsAbsoluteScore.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    dropDownItemList.Add(new DropDownItem("Title", "Test Area Comparative Score"));
                    dropDownItemList.Add(new DropDownItem("Test Area", Constants.ChartConstants.DESIGN_REPORT_TESTAREA_COMPARATIVE_ABSOLUTE + "-" + testkey + "-" + selectedTestAreaIds + ".png"));
                    testAreaChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_TESTAREA_COMPARATIVE_ABSOLUTE + "-" + testkey + "-" + selectedTestAreaIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    dropDownItemList.Add(new DropDownItem("Title", "Test Area Relative Comparative Score"));
                    dropDownItemList.Add(new DropDownItem("Test Area", Constants.ChartConstants.DESIGN_REPORT_TESTAREA_COMPARATIVE_RELATIVE + "-" + testkey + "-" + selectedTestAreaIds + ".png"));
                    testAreaChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_TESTAREA_COMPARATIVE_RELATIVE + "-" + testkey + "-" + selectedTestAreaIds;

                }
            }
            else
            {
                selectedPrintProperty.Append("0|");
                TestAreaChartControl_showComparativeScoreLinkButton.Text = "Show Comparative Score";
                testAreaChartDataSource.IsHideComparativeScores = true;

                if (TestAreaChartControl_selectOptionsAbsoluteScore.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    dropDownItemList.Add(new DropDownItem("Title", "Test Area Score"));
                    dropDownItemList.Add(new DropDownItem("Test Area", Constants.ChartConstants.DESIGN_REPORT_TESTAREA_ABSOLUTE + "-" + testkey + "-" + selectedTestAreaIds + ".png"));
                    testAreaChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_TESTAREA_ABSOLUTE + "-" + testkey + "-" + selectedTestAreaIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    dropDownItemList.Add(new DropDownItem("Title", "Test Area Relative Score"));
                    dropDownItemList.Add(new DropDownItem("Test Area", Constants.ChartConstants.DESIGN_REPORT_TESTAREA_ABSOLUTE + "-" + testkey + "-" + selectedTestAreaIds + ".png"));
                    testAreaChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_TESTAREA_RELATIVE + "-" + testkey + "-" + selectedTestAreaIds;

                }
            }
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            TestAreaChartControl_multipleSeriesChartControl.MultipleChartDataSource =
                testAreaChartDataSource;
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        #endregion Private Method

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>        
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

            //Assign the attributes for the link Button 
            TestAreaChartControl_showComparativeScoreLinkButton.
                Attributes.Add("onclick", "javascript:return ShowOrHideComparativeScore('"
                + TestAreaChartControl_showComparativeScoreLinkButton.ClientID + "','" +
                TestAreaChartControl_comparativeStateHiddenField.ClientID + "');");
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return new UpdatePanel[] { TestAreaChartControl_chartUpdatePanel };
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}