﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SingleSeriesReportChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SingleSeriesReportChartControl" %>

<table>
    <tr>
        <td runat="server" id="SingleSeriesReportChartControl_td">
            <asp:Chart ID="SingleSeriesReportChartControl_chart" runat="server" Palette="Pastel" ToolTip="Click here to zoom graph"
                BackColor="Transparent" AntiAliasing="Graphics" Height="150px" Width="250px" EnableViewState="true">
                <Series>
                    <asp:Series Name="SingleSeriesReportChartControl_series" ChartArea="SingleSeriesReportChartControl_chartArea">
                    </asp:Series>
                </Series>
                <Titles>
                    <asp:Title Name="SingleSeriesReportChartControl_title" TextStyle="Default" ForeColor="180, 65, 140, 240"
                        Font="Arial, 8.25pt, style=Bold">
                    </asp:Title>
                </Titles>
                <ChartAreas>
                    <asp:ChartArea Name="SingleSeriesReportChartControl_chartArea" BackSecondaryColor="Transparent"
                        BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                        <AxisY>
                            <MajorGrid Enabled="false" />
                            <LabelStyle ForeColor="180, 65, 140, 240" />
                        </AxisY>
                        <AxisX>
                            <MajorGrid Enabled="false" />
                            <LabelStyle ForeColor="180, 65, 140, 240" />
                        </AxisX>
                        <Position Height="91" Width="100" Y="5" />
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </td>
    </tr>
</table>
