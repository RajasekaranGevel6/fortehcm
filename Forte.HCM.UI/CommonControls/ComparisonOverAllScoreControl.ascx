﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComparisonOverAllScoreControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ComparisonOverAllScoreControl" %>
<%@ Register Src="~/CommonControls/SingleSeriesReportChartControl.ascx" TagName="ReportChartControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <div style="width: 100%; overflow: auto">
        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
    </div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="ComparisonOverAllScoreControl_updatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ComparisonOverAllScoreControl_selectScoreLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonOverAllScoreControl_absoluteScoreRadioButton" runat="server"
                                                    AutoPostBack="true" Text=" Absolute Score" Checked="true" GroupName="1" OnCheckedChanged="ComparisonOverAllScoreControl_selectOptionsRadioButton_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonOverAllScoreControl_relativeScoreRadioButton" runat="server"
                                                    AutoPostBack="true" Text=" Relative Score" GroupName="1" OnCheckedChanged="ComparisonOverAllScoreControl_selectOptionsRadioButton_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 40%">
                                                <asp:LinkButton ID="ComparisonOverAllScoreControl_DisplayLinkButton" runat="server"
                                                    Text="Show Comparative Score" SkinID="sknActionLinkButton" OnClick="ComparisonOverAllScoreControl_displayLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="ComparisonOverAllScoreControl_hiddenField" runat="server" Value="0" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <uc1:ReportChartControl ID="ComparisonOverAllScoreChartControl_scoreChartReportChartControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
