﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LicenseAgreementHeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.LicenseAgreementHeaderControl" %>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left" style="width: 15%; vertical-align: middle; padding-top: 5px">
                        <p onclick="location.href='<%= homeUrl %>'">
                            <asp:Image ID="HomePage_logoImage" runat="server" SkinID="sknHomePageLogoImage" />
                        </p>
                    </td>
                    <td style="width: 85%" align="right" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

