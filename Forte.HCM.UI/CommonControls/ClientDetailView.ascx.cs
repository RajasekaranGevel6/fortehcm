﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

namespace Forte.HCM.UI.CommonControls
{
    public partial class ClientDetailView : System.Web.UI.UserControl
    {
        public delegate void Button_Command(object sender, CommandEventArgs e);
        public event Button_Command OkCommand;

        public delegate void RepeaterDepartment_Details(object sender, RepeaterCommandEventArgs e);
        public event RepeaterDepartment_Details OnRepeaterCommandClick;

        protected void ClientDetailView_ImageButton_Command(object sender, CommandEventArgs e)
        {
            if (OkCommand != null)
            {
                this.OkCommand(sender, e);
            }
        }

        public ClientInformation ClientDetailViewDataSource
        {
            set
            {

                ClientDetailView_editClientImageButton.CommandArgument = value.ClientID.ToString();
                ClientDetailView_deleteClientImageButton.CommandArgument = value.ClientID.ToString();
                ClientDetailView_viewDepartmentImageButton.CommandArgument = value.ClientID.ToString();
                ClientDetailView_addDepartmentImageButton.CommandArgument = value.ClientID.ToString();
                ClientDetailView_viewContactImageButton.CommandArgument = value.ClientID.ToString();
                ClientDetailView_addContactImageButton.CommandArgument = value.ClientID.ToString();
                ClientDetailView_viewPositionProfileImageButton.CommandArgument = value.ClientID.ToString();
                ClientDetailView_createPositionProfileImageButton.CommandArgument = value.ClientID.ToString();

                ClientDetailView_editClientImageButton.CommandName = Constants.ClientManagementConstants.EDIT_CLIENT;
                ClientDetailView_deleteClientImageButton.CommandName = Constants.ClientManagementConstants.DELETE_CLIENT;
                ClientDetailView_viewDepartmentImageButton.CommandName = Constants.ClientManagementConstants.VIEW_CLIENT_DEPARTMENT;
                ClientDetailView_addDepartmentImageButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT;
                ClientDetailView_viewContactImageButton.CommandName = Constants.ClientManagementConstants.VIEW_CLIENT_CONTACT;
                ClientDetailView_addContactImageButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_CONTACT;
                ClientDetailView_viewPositionProfileImageButton.CommandName = Constants.ClientManagementConstants.VIEW_CLIENT_POSITION_PROFILE;
                ClientDetailView_createPositionProfileImageButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_POSITION_PROFILE;


                ClientDetailView_clientNameLabel.Text = value.ClientName;
                //if (value.ContactInformation != null)
                {

                    ClientDetailView_streetAddressLabel.Text = value.ContactInformation.StreetAddress;
                    ClientDetailView_eMailIDLabel.Text = value.ContactInformation.EmailAddress;
                    if (value.ContactInformation.Phone != null)
                        ClientDetailView_phoneNoLabel.Text = value.ContactInformation.Phone.Mobile;
                    if (value.ContactInformation.WebSiteAddress != null)
                        ClientDetailView_wesiteURLLabel.Text = value.ContactInformation.WebSiteAddress.Personal;
                }
                ClientDetailView_cityLabel.Text = value.ContactInformation.City;
                ClientDetailView_statelabel.Text = value.ContactInformation.State;
                ClientDetailView_zipLabel.Text = value.ContactInformation.PostalCode;
                ClientDetailView_countryLabel.Text = value.ContactInformation.Country;

                ClientDetailView_faxNoLabel.Text = value.FaxNumber;
                ClientDetailView_feinNoLabel.Text = value.FeinNo;

                ClientDetailView_additionalInfoLabel.Text = value.AdditionalInfo == null ? value.AdditionalInfo : 
                    value.AdditionalInfo.ToString().Replace(Environment.NewLine, "<br />");

                ClientDetailView_departmentListRepeater.DataSource = value.Departments;
                ClientDetailView_departmentListRepeater.DataBind();
            }

        }

        public NomenclatureCustomize ClientNomenclatureAttribute
        {
            set
            {
                ClientDetailView_clientNameLabel.ToolTip = value.ClientName;
                ClientDetailView_eMailIDLabel.ToolTip = value.EmailID;
                ClientDetailView_phoneNoHeadLabel.Text= value.PhoneNumber;
                ClientDetailView_faxNoHeadLabel.Text = value.FaxNo;
                ClientDetailView_zipHeadLabel.Text = value.Zipcode;
                ClientDetailView_streetAddressHeadLabel.Text = value.StreetAddress;
                ClientDetailView_cityHeadLabel.Text = value.City;
                ClientDetailView_stateHeadLabel.Text = value.state;
                ClientDetailView_countryHeadLabel.Text = value.Country;
                ClientDetailView_feinNoHeadLabel.Text = value.FEINNO;
                ClientDetailView_wesiteURLHeadLabel.Text = value.WebsiteURL;
                ClientDetailView_additionalInfoHeadLabel.Text = value.AdditionalInfo;
                ClientDetailView_departmentsHeadLabel.Text = value.Departments;
            }

        }

        public bool DeleteButtonVisibility
        {
            set
            {
                ClientDetailView_deleteClientImageButton.Visible = value;
            }
        }
        protected void ClientDetailView_departmentListRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (OnRepeaterCommandClick != null)
                this.OnRepeaterCommandClick(source, e);

        }

    }
}