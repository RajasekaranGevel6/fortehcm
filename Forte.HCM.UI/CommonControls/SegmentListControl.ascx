﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SegmentListControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SegmentListControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_left_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="SegmentListControl_titleLiteral" runat="server"></asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="SegmentListControl_topCloseImageButton" runat="server" SkinID="sknCloseImageButton" />
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_left_right_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_left_right_10">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr valign="bottom">
                                <td align="left">
                                    <div style="height: 265px; overflow: auto">
                                        <asp:CheckBoxList ID="SegmentListControl_segmentCheckBoxList" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_20">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
          <asp:Button ID="SegmentListControl_addButton" runat="server" SkinID="sknButtonId"
                Text="Add" OnClick="SegmentListControl_addButton_Click" />
                <asp:LinkButton ID="SegmentListControl_cancelLinkButton" Text="Cancel" runat="server" SkinID="sknCancelLinkButton"></asp:LinkButton>
        </td>
    </tr>
</table>
