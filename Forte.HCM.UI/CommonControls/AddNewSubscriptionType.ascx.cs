﻿
#region Namespace

using System;
using System.Web.UI;
using System.Reflection;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.Admin;
using System.Collections.Generic;

# endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class AddNewSubscriptionType : UserControl
    {

        #region Properties

        public bool IsEditMode { set; get; }

        public SubscriptionTypes SetEditableDataSource
        {
            set
            {
                if (!IsEditMode)
                    throw new Exception("Set edit mode first");
                Forte_AddNewSubscriptionType_subscriptionIdHiddenLabel.Text = value.SubscriptionId.ToString();
                Forte_AddNewSubscriptionType_subscriptionNameTextBox.Text = value.SubscriptionName;
                Forte_AddNewSubscriptionType_subscriptionDescriptionTextBox.Text = value.SubscriptionDescription;
                Forte_AddNewSubscriptionType_editModeHiddenLabel.Text = true.ToString();
                Forte_AddNewSubscriptionType_roleIDHiddenField.Value = value.RoleIDs;
                SetRoleID();

            }
        }

        private void SetRoleID()
        {
            if (Forte_AddNewSubscriptionType_roleIDHiddenField.Value == "")
                return;
            string[] roleId = Forte_AddNewSubscriptionType_roleIDHiddenField.Value.Split(',');
            foreach (string item in roleId)
            {
                if (item != "," || item != "")
                {
                    for (int i = 0; i < Forte_AddNewSubscriptionType_roleList.Items.Count; i++)
                    {
                        if (Forte_AddNewSubscriptionType_roleList.Items[i].Value == item)
                        {
                            Forte_AddNewSubscriptionType_roleList.Items[i].Selected = true;
                            Forte_AddNewSubscriptionType_roleComboTextBox.Text += Forte_AddNewSubscriptionType_roleList.Items[i].Text + ",";
                        }
                    }
                }
            }
            Forte_AddNewSubscriptionType_roleComboTextBox.Text =
            Forte_AddNewSubscriptionType_roleComboTextBox.Text.Length > 2 ?
            Forte_AddNewSubscriptionType_roleComboTextBox.Text.TrimEnd(',') :
            Forte_AddNewSubscriptionType_roleComboTextBox.Text;

        }

        public int Created_By
        {
            set;
            get;
        }

        #endregion Properties

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    Forte_AddNewSubscriptionType_roleComboTextBox.ToolTip = Forte_AddNewSubscriptionType_roleComboTextBox.Text;
                    return;
                }

                Forte_AddNewSubscriptionType_subscriptionNameTextBox.Text = string.Empty;
                Forte_AddNewSubscriptionType_subscriptionDescriptionTextBox.Text = string.Empty;
                Forte_AddNewSubscriptionType_roleComboTextBox.Text = string.Empty;
                
                LoadRoleValues();
                SetRoleID();

                //Page.SetFocus(Forte_AddNewSubscriptionType_subscriptionSaveButton.ClientID);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        private void LoadRoleValues()
        {
            //Assign the values for the roles check box list


            Forte_AddNewSubscriptionType_roleList.DataSource = new AuthenticationBLManager().GetRoles();
            Forte_AddNewSubscriptionType_roleList.DataBind();

            
        }

        protected void Forte_AddNewSubscriptionType_subscriptionCancelButton_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        protected void Forte_AddNewSubscriptionType_subscriptionSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                if (Convert.ToBoolean(Forte_AddNewSubscriptionType_editModeHiddenLabel.Text))
                    UpdateSubscriptionDetails();
                else
                    InsertSubscriptionDetails();

            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_AddNewSubscriptionType_roleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Forte_AddNewSubscriptionType_roleIDHiddenField.Value = string.Empty;

                string selectedRoleName = "";

                for (int i = 0; i < Forte_AddNewSubscriptionType_roleList.Items.Count; i++)
                {
                    if (Forte_AddNewSubscriptionType_roleList.Items[i].Selected)
                    {
                        Forte_AddNewSubscriptionType_roleIDHiddenField.Value += ", " + Forte_AddNewSubscriptionType_roleList.Items[i].Value;

                        selectedRoleName += ", " + Forte_AddNewSubscriptionType_roleList.Items[i].Text;
                    }
                }

                if (selectedRoleName != null || selectedRoleName.Length != 0)
                {
                    selectedRoleName = selectedRoleName.TrimStart(',', ' ');
                    Forte_AddNewSubscriptionType_roleIDHiddenField.Value = Forte_AddNewSubscriptionType_roleIDHiddenField.Value.TrimStart(',', ' ');
                }

                Forte_AddNewSubscriptionType_roleComboTextBox.Text = selectedRoleName;


                InvokePageBaseMethod("ShowSubscriptinTypeModalPopup", null);

            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }

        }


        #endregion Events

        #region Private Methods

        /// <summary>
        /// Updates the subscription type
        /// </summary>
        private void UpdateSubscriptionDetails()
        {
            SubscriptionTypes subscriptionTypes = null;
            try
            {
                subscriptionTypes = new SubscriptionTypes();
                subscriptionTypes.SubscriptionId = Convert.ToInt32(Forte_AddNewSubscriptionType_subscriptionIdHiddenLabel.Text);
                subscriptionTypes.SubscriptionName = Forte_AddNewSubscriptionType_subscriptionNameTextBox.Text;
                subscriptionTypes.SubscriptionDescription = Forte_AddNewSubscriptionType_subscriptionDescriptionTextBox.Text;
                subscriptionTypes.ModifiedBy = Created_By;
                for (int i = 0; i < Forte_AddNewSubscriptionType_roleList.Items.Count; i++)
                {
                    if (Forte_AddNewSubscriptionType_roleList.Items[i].Selected)
                    {
                        subscriptionTypes.RoleIDs += Forte_AddNewSubscriptionType_roleList.Items[i].Value + ",";
                    }
                }
                subscriptionTypes.RoleIDs = subscriptionTypes.RoleIDs.Length > 1 ? subscriptionTypes.RoleIDs.TrimEnd(',') : subscriptionTypes.RoleIDs;
                new SubscriptionBLManager().UpdateSubscriptionTypes(subscriptionTypes);
                ClearControls();
                InvokePageBaseMethod("RebindSubscriptionTypeGridView", null);
                InvokePageBaseMethod("ShowSuccessMessage", new object[] { HCMResource.NewSubscriptionType_UpdateSuccessfull });
            }
            finally
            {
                if (subscriptionTypes != null) subscriptionTypes = null;
                try { GC.Collect(2, GCCollectionMode.Forced); }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Inserts the subscription type
        /// </summary>
        private void InsertSubscriptionDetails()
        {
            SubscriptionTypes subscriptionTypes = null;
            try
            {


                subscriptionTypes = new SubscriptionTypes();
                subscriptionTypes.SubscriptionName = Forte_AddNewSubscriptionType_subscriptionNameTextBox.Text;
                subscriptionTypes.SubscriptionDescription = Forte_AddNewSubscriptionType_subscriptionDescriptionTextBox.Text;
                subscriptionTypes.CreatedBy = Created_By;
                for (int i = 0; i < Forte_AddNewSubscriptionType_roleList.Items.Count; i++)
                {
                    if (Forte_AddNewSubscriptionType_roleList.Items[i].Selected)
                    {
                        subscriptionTypes.RoleIDs += Forte_AddNewSubscriptionType_roleList.Items[i].Value + ",";
                    }

                }
                subscriptionTypes.RoleIDs = subscriptionTypes.RoleIDs.Length > 1 ? subscriptionTypes.RoleIDs.TrimEnd(',') : subscriptionTypes.RoleIDs;
                new SubscriptionBLManager().InsertSubscriptionType(subscriptionTypes);
                ClearControls();
                InvokePageBaseMethod("RebindSubscriptionTypeGridView", null);
                InvokePageBaseMethod("ShowSuccessMessage", new object[] { HCMResource.NewSubscriptionType_InsertSuccessfull });
            }
            finally
            {
                if (subscriptionTypes != null) subscriptionTypes = null;
                try { GC.Collect(2, GCCollectionMode.Forced); }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Clear all the controls
        /// </summary>
        private void ClearControls()
        {
            Forte_AddNewSubscriptionType_subscriptionIdHiddenLabel.Text = "";
            Forte_AddNewSubscriptionType_subscriptionNameTextBox.Text = "";
            Forte_AddNewSubscriptionType_subscriptionDescriptionTextBox.Text = "";
            Forte_AddNewSubscriptionType_editModeHiddenLabel.Text = false.ToString();
            Forte_AddNewSubscriptionType_roleComboTextBox.Text = "";
            for (int i = 0; i < Forte_AddNewSubscriptionType_roleList.Items.Count; i++)
            {
                Forte_AddNewSubscriptionType_roleList.Items[i].Selected = false;
            }

        }

        /// <summary>
        /// Validates the input data
        /// </summary>
        /// <returns>
        /// A <see cref="System.Boolean"/> that returns whether the input data is valid or not.
        /// </returns>
        private bool IsValidData()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(Forte_AddNewSubscriptionType_subscriptionNameTextBox.Text))
            {
                isValid = false;
                ShowErrorMessage(new Exception(HCMResource.AddNewSubscription_EmptyNameTextBox));
            }
            if (Created_By == 0)
            {
                isValid = false;
                ShowErrorMessage(new Exception(HCMResource.AddNewSubscription_InValidUserId));
            }
            if (!isValid)
                InvokePageBaseMethod("ShowSubscriptinTypeModalPopup", null);
            return isValid;
        }

        /// <summary>
        /// Invokes the parent page base public methods
        /// </summary>
        /// <param name="MethodName">
        /// A <see cref="System.String"/> that holds the method name to invoke
        /// </param>
        /// <param name="args">
        /// A <see cref="System.Object"/> that holds the arguments to be passed to the method
        /// </param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        /// <summary>
        /// Shows error message
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            if (string.IsNullOrEmpty(Forte_AddNewSubscriptionType_subscriptionTopErrorMessageLabel.Text))
                Forte_AddNewSubscriptionType_subscriptionTopErrorMessageLabel.Text = exp.Message;
            else
                Forte_AddNewSubscriptionType_subscriptionTopErrorMessageLabel.Text += "<br />" + exp.Message;
        }

        #endregion Private Methods


    }
}