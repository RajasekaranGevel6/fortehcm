﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewSaveAsControl.cs
// File that represents the interview saveas control dialog 
// which performs copy the current interview.

#endregion Header

#region Directives

using System;
using System.Text;
using System.Web.UI;
using System.Reflection;
using System.Configuration;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.Utilities;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// The class that represents the layout for interview saveas control
    /// that will allow us to copy the interview based on button click, the respective
    /// event handler will be performed.
    /// </summary>
    public partial class InterviewSaveAsControl : UserControl
    {
        #region Public Events and Delegate                                     

        // The public events must conform to this format
        public delegate void Button_Click(object sender, EventArgs e);
        
        // These two public events are to be customized as need on the page and will be called
        // by the private events of the buttons in the user control
        public event Button_Click CancelClick;

        #endregion Public Events and Delegate

        #region Event Handler                                                  

        /// <summary>
        /// Handler method that is called when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InterviewSaveAsControl_skillsCheckBox.Checked = false;
                    InterviewSaveAsControl_questionSetCheckBox.Checked = false;
                    InterviewSaveAsControl_assessorDetaillsCheckBox.Checked = false;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

      
        /// <summary>
        /// When the CANCEL button is clicked in the popup, this event 
        /// will get fired.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void InterviewSaveAsControl_cancelButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        /// <summary>
        /// Handler method that will be called when saveas image button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void InterviewSaveAsControl_interviewSaveAsButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                if (!ValidateSaveAsCtrl())
                    return;

                string parentInterviewkey = InterviewSaveAsControl_interviewKeyHiddenField.Value;
                int userID = 0;

                if (!Utility.IsNullOrEmpty(InterviewSaveAsControl_userIDHiddenField.Value))
                {
                    userID =  Convert.ToInt32(InterviewSaveAsControl_userIDHiddenField.Value.Trim());
                }

                //Copy the interview against the requested interview key
                string copySkill = string.Empty;
                string copyQuestionSet = string.Empty;
                string copyAssesor = string.Empty;
                string interviewKey = string.Empty;

                if (InterviewSaveAsControl_skillsCheckBox.Checked)
                    copySkill = "SL";
                if (InterviewSaveAsControl_questionSetCheckBox.Checked)
                    copyQuestionSet = "QS";
                if (InterviewSaveAsControl_assessorDetaillsCheckBox.Checked)
                    copyAssesor = "AS";

                new OnlineInterviewBLManager().CopyOnlineInterview(parentInterviewkey,
                    copySkill, copyQuestionSet, copyAssesor, userID, out interviewKey);

                Response.Redirect("~/OnlineInterview/OnlineInterviewCreation.aspx?m=3&s=0&interviewkey=" + 
                    interviewKey + "&parentpage=S_OITSN&copied=Y", false);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message);
            }
            finally
            {
                InvokePageBaseMethod("ShowSaveModalPopUp", null);
            }
        }

        #endregion Event Handler

        #region Private Methods                                                

        /// <summary>
        /// Method to validate the save as control
        /// </summary>
        /// <returns>That returns the control and returns true or false</returns>
        private bool ValidateSaveAsCtrl()
        {
            bool isValid = true;

            if (InterviewSaveAsControl_skillsCheckBox.Checked == false &&
                InterviewSaveAsControl_questionSetCheckBox.Checked == false &&
                InterviewSaveAsControl_assessorDetaillsCheckBox.Checked == false)
            {
                isValid = false;
            }

            if (!isValid)
            {
                ShowErrorMessage("Please select copy option");
                InvokePageBaseMethod("ShowSaveModalPopUp", null);
            }

            return isValid;
        }


        /// <summary>
        /// Method to show the error message
        /// </summary>
        /// <param name="exp">exp</param>
        private void ShowErrorMessage(string exp)
        {
            if (string.IsNullOrEmpty(InterviewSaveAsControl_topErrorMessageLabel.Text))
                InterviewSaveAsControl_topErrorMessageLabel.Text = exp;
            else
                InterviewSaveAsControl_topErrorMessageLabel.Text += "<br />" + exp;
        }

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <param name="args">The args.</param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod,
                null, this.Page, args);
        }

        #endregion Private Methods

        #region Public Properties                                              

        /// <summary>
        /// Property that gets and sets the interview key from the requested page
        /// </summary>
        public string InterviewKey
        {
            set
            {
                InterviewSaveAsControl_interviewKeyHiddenField.Value =
                    value.ToString();
            }
            get
            {
                if (Utility.IsNullOrEmpty(InterviewSaveAsControl_interviewKeyHiddenField.Value))
                    return string.Empty;

                return InterviewSaveAsControl_interviewKeyHiddenField.Value.Trim();
            }
        }

        /// <summary>
        /// Property that gets and sets the use id from the requested page
        /// </summary>
        public int UserID
        {
            set
            {
                InterviewSaveAsControl_userIDHiddenField.Value =
                    value.ToString();
            }
            get
            {
                if (Utility.IsNullOrEmpty(InterviewSaveAsControl_userIDHiddenField.Value))
                    return 0;

                return Convert.ToInt32(InterviewSaveAsControl_userIDHiddenField.Value.Trim());
            }
        }

        #endregion Public Properties
    }
}