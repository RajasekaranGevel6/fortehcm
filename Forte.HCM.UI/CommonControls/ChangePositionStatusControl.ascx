﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePositionStatusControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ChangePositionStatusControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 80%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="ChangePositionStatusControl_titleLiteral" runat="server" Text="Change Position Status"></asp:Literal>
                    </td>
                    <td style="width: 20%" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ChangePositionStatusControl_topCancelImageButton" runat="server"
                                        SkinID="sknCloseImageButton" OnClick="ChangePositionStatusControl_cancelButton_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10" align="left">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" style="width: 40%">
                                    <asp:Label ID="ChangePositionStatusControl_changePositionStatusLabel" runat="server"
                                        Text="Position Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" style="width: 60%">
                                    <asp:DropDownList ID="ChangePositionStatusControl_statusDropDownList" runat="server" SkinID="sknPositionProfileStatusDropDownList"
                                         Width="100px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td style="padding-left: 8px">
                        <asp:Button ID="ChangePositionStatusControl_saveButton" runat="server" Text="Save"
                            SkinID="sknButtonId" OnClick="ChangePositionStatusControl_saveButton_Click" ToolTip="Click here to save position status" />
                    </td>
                    <td>
                        &nbsp;
                        <asp:LinkButton ID="ChangePositionStatusControl_cancelLinkButton" runat="server"
                            Text="Cancel" OnClick="ChangePositionStatusControl_cancelButton_Click" ToolTip="Click here to save position status"
                            SkinID="sknCancelLinkButton"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 4px">
        </td>
    </tr>
</table>
