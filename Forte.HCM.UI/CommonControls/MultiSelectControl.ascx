﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiSelectControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.MultiSelectControl" %>
<table width="100%" cellpadding="0" cellspacing="2" border="0">
    <tr>
        <td class="label_field_header_text">
       
        </td>
        <td align="right">
            <asp:LinkButton ID="GroupAnalysisCategoryChartControl_selectAllLinkButton" runat="server"
                Text="Select All" SkinID="sknActionLinkButton" OnClick="GroupAnalysisCategoryChartControl_selectAllLinkButton_Click"></asp:LinkButton>
            <span class="link_button">&nbsp;|&nbsp;</span>
            <asp:LinkButton ID="GroupAnalysisCategoryChartControl_clearAllLinkButton" runat="server"
                Text="Clear All" SkinID="sknActionLinkButton" OnClick="GroupAnalysisCategoryChartControl_clearAllLinkButton_Click"></asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td class="td_height_2" colspan="2">
        </td>
    </tr>
    <tr>
        <td valign="middle" colspan="2">
            <asp:TextBox ID="MultiSelectControl_propertyTextBox" ReadOnly="true" runat="server"
                Width="250px" Font-Size="X-Small"></asp:TextBox>
            <asp:Panel ID="MultiSelectControl_popupPanel" runat="server" CssClass="popupControl_list">
                <asp:CheckBoxList ID="MultiSelectControl_propertyCheckBoxList" RepeatColumns="2"
                    Width="100%" runat="server" CellPadding="10" CellSpacing="10" TextAlign="Right"
                    AutoPostBack="true" OnSelectedIndexChanged="MultiSelectControl_propertyCheckBoxList_SelectedIndexChanged">
                </asp:CheckBoxList>
            </asp:Panel>
            <ajaxToolKit:PopupControlExtender ID="MultiSelectControl_propertyPopupControlExtender"
                runat="server" TargetControlID="MultiSelectControl_propertyTextBox" PopupControlID="MultiSelectControl_popupPanel"
                OffsetX="2" OffsetY="2" Position="Top">
            </ajaxToolKit:PopupControlExtender>
        </td>
    </tr>
</table>
