﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageNavigator.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.PageNavigator" %>
<table width="100%" cellpadding="0" cellspacing="0" id="navigate_text">
    <tr>
        <td valign="middle" height="30">
            <asp:Panel ID="PageNavigator_controlsPanel" runat="server" HorizontalAlign="Right">
                <asp:LinkButton ID="PageNavigator_moveFirstLinkButton" runat="server"
                    OnClick="PageNavigator_Move_Click" CommandName="First" ToolTip="Click here to show first set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_first.gif" style="border-style:none;" alt=""/></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="PageNavigator_movePreviousLinkButton" runat="server"
                    OnClick="PageNavigator_Move_Click" CommandName="Previous" ToolTip="Click here to show previous set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_prev.gif" style="border-style:none;" alt=""/></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="PageNavigator_oneLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="Solid">1</asp:LinkButton>
                <asp:Label ID="PageNavigator_oneSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_twoLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">2</asp:LinkButton>
                <asp:Label ID="PageNavigator_twoSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_threeLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">3</asp:LinkButton>
                <asp:Label ID="PageNavigator_threeSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_fourLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">4</asp:LinkButton>
                <asp:Label ID="PageNavigator_fourSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_fiveLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">5</asp:LinkButton>
                <asp:Label ID="PageNavigator_fiveSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_sixLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">6</asp:LinkButton>
                <asp:Label ID="PageNavigator_sixSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_sevenLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">7</asp:LinkButton>
                <asp:Label ID="PageNavigator_sevenSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_eightLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">8</asp:LinkButton>
                <asp:Label ID="PageNavigator_eightSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_nineLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">9</asp:LinkButton>
                <asp:Label ID="PageNavigator_nineSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="PageNavigator_tenLinkButton" runat="server"
                    BorderWidth="2px" BorderColor="#a1a1a1" OnClick="PageNavigator_PageNumber_Click"
                    BorderStyle="None">10</asp:LinkButton>
                &nbsp;<asp:LinkButton ID="PageNavigator_moveNextLinkButton" runat="server"
                    OnClick="PageNavigator_Move_Click" CommandName="Next" ToolTip="Click here to show next set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_next.gif" style="border-style:none;" alt=""/></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="PageNavigator_moveLastLinkButton" runat="server" OnClick="PageNavigator_Move_Click"
                    Text="" CommandName="Last" ToolTip="Click here to show last set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_last.gif" style="border-style:none;" alt=""/> </asp:LinkButton>
            </asp:Panel>
            <input id="PageNavigator_currentPageSetHidden" type="hidden" name="PageNavigator_currentPageSetHidden"
                value="1" runat="server" />
            <input id="PageNavigator_pageSizeHidden" type="hidden" name="PageNavigator_pageSizeHidden"
                value="1" runat="server" />
            <input id="PageNavigator_totalRecordsHidden" type="hidden" name="PageNavigator_totalRecordsHidden"
                value="0" runat="server" />
        </td>
    </tr>
</table>
