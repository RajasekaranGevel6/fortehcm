﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequirementViewerControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.RequirementViewerControl" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0"> <%-- class="popup_body_bg_pp"--%>
    <tr>
        <td  align="left">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr style="display:none">
                    <td class="header_bg">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="2" cellspacing="0" class="panel_inner_body_bg" border="0"> 
                                        <tr>
                                            <td align="center"><%--class="popupcontrol_question_inner_bg" --%>
                                                <asp:UpdatePanel ID="test" runat="server">
                                                    <ContentTemplate>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td align="left"  width="200px">
                                                                    <asp:FileUpload ID="RequirementViewerControl_fileUpload" runat="server" size="15px" />
                                                                </td>
                                                                <td align="left" width="750px" > 
                                                                    <asp:Button ID="RequirementViewerControl_fileUploadButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Upload" OnClick="RequirementViewerControl_fileUploadButton_Click" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    <asp:Label ID="RequirementViewerControl_requirementLabel" runat="server" Text="Requirement"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    <asp:TextBox ID="RequirementViewerControl_requirementTextBox" runat="server" TextMode="MultiLine"
                                                                        SkinID="sknMultiLineTextBox" Height="40px" Width="849px" ReadOnly="false" ></asp:TextBox>
                                                                </td>
                                                            </tr> 
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="RequirementViewerControl_fileUploadButton" />
                                                        <asp:AsyncPostBackTrigger ControlID="RequirementViewerControl_resetLinkButton" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    
                                        <tr>
                                            <td align="left" >
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td align="left" style="padding-left:1px" width="12%">
                                                            <asp:Button ID="RequirementViewerControl_autoPopulateButton" runat="server" Text="Auto Populate "
                                                                SkinID="sknButtonId" 
                                                                onclick="RequirementViewerControl_autoPopulateButton_Click" />
                                                        </td>
                                                        <td  align="left"> 
                                                            <asp:LinkButton ID="RequirementViewerControl_resetLinkButton" runat="server" Text="Clear"
                                                               SkinID="sknDashboardLinkButton" OnClick="RequirementViewerControl_resetLinkButton_Click">
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td align="right" style="padding-right:70px" >
                                                            <asp:LinkButton ID="RequirementViewerControl_additionalInformationLinkButton" 
                                                             SkinID="sknActionLinkButton" runat="server" OnClientClick="javascript:return ShowPositionRequirementDiv('RequirementViewerControl_additionalInformationDiv');"  >
                                                            Additional Information</asp:LinkButton> 
                                                        </td>
                                                    </tr>
                                                    <tr><td height="5px"></td></tr>
                                                    <tr> 
                                                        <td colspan="3" align="left">
                                                            <div id="RequirementViewerControl_additionalInformationDiv"  style="display: none">
                                                                    <asp:TextBox ID="RequirementViewerControl_additionalInformationTextBox" runat="server" TextMode="MultiLine"
                                                                        SkinID="sknMultiLineTextBox" Height="40px" Width="849px" ReadOnly="false" MaxLength="8000" onchange="javascript:return CommentsCount(8000,this)"></asp:TextBox>
                                                            </div>
                                                       </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table> 
                    </td>
                </tr> 
            </table>
        </td>
    </tr>
</table>
