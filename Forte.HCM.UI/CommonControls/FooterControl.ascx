﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.FooterControl" %>
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" >
    <tr>
        <td class="line_thick">
        </td>
    </tr>
    <tr valign="bottom">
        <td class="td_padding_left_right">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="right" valign="top" class="footer_content">
                        © 2021. Forte HCM. All Rights Reserved.
                    </td>
                    <%--<td align="right">
                        <table cellpadding="0" cellspacing="0" border="0" class="footer_menu_text">
                            <tr>
                                <td>
                                   <asp:LinkButton ID="FooterControl_aboutUsLinkButton" runat="server" Text="About Us"
                                        CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/AboutUs.aspx"></asp:LinkButton>
                                </td>
                                <td>
                                    | 
                                    <asp:LinkButton ID="FooterControl_jobsLinkButton" runat="server" Text="Jobs"
                                        CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/Jobs.aspx"></asp:LinkButton>
                                </td>
                                <td>
                                    | 
                                    <asp:LinkButton ID="FooterControl_blogLinkButton" runat="server" Text="Blog"
                                        CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/Blog.aspx"></asp:LinkButton>
                                </td>
                                <td>
                                    | 
                                    <asp:LinkButton ID="FooterControl_partnersLinkButton" runat="server" Text="Partners"
                                        CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/Partners.aspx"></asp:LinkButton>
                                </td>
                                <td>
                                    | 
                                    <asp:LinkButton ID="FooterControl_contactUsLinkButton" runat="server" Text="Contact Us"
                                        CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/ContactUs.aspx"></asp:LinkButton>
                                </td>
                                <td>
                                    | 
                                    <asp:LinkButton ID="FooterControl_termsLinkButton" runat="server" Text="Terms"
                                        CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/Terms.aspx"></asp:LinkButton>
                                </td>
                                <td>
                                    |
                                    <asp:LinkButton ID="FooterControl_feedbackLinkButton" runat="server" Text="Feedback"
                                        CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/Feedback.aspx"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>--%>
                </tr>
            </table>
        </td>
    </tr>
</table>
