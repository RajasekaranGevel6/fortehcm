﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ProjectsControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>

    <asp:UpdatePanel ID="ProjectsControl_updatePanel" runat="server">
        <ContentTemplate>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="header_bg">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="width: 93%" align="left">
                                <asp:Literal ID="ProjectsControl_headerLiteral" runat="server" Text="Projects"></asp:Literal>
                                <asp:HiddenField ID="ProjectsControl_hiddenField" runat="server" />
                                <asp:HiddenField ID="ProjectControl_deletedRowHiddenField" runat="server" />
                            </td>
                            <td style="width: 2%" align="right">
                                <span id="ProjectsControl_plusSpan" runat="server" style="display: none;">
                                    <asp:Image ID="ProjectsControl_plusImage" runat="server" SkinID="sknPlusImage" /></span>
                                <span id="ProjectsControl_minusSpan" runat="server" style="display: block;">
                                    <asp:Image ID="ProjectsControl_minusImage" runat="server" SkinID="sknMinusImage" /></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="overflow: auto; width: 100%;" runat="server" id="ProjectsControl_controlsDiv">
            <asp:LinkButton ID="ProjectsControl_addRowLinkButton" SkinID="sknAddLinkButton" runat="server"
                Text="Add" OnClick="ProjectsControl_addRowLinkButton_Click" ToolTip="Add new Row"></asp:LinkButton>
            <asp:ListView ID="ProjectsControl_listView" runat="server" 
                OnItemDataBound="ProjectsControl_listView_ItemDataBound" 
                onitemcommand="ProjectsControl_listView_ItemCommand">
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" width="100%">
                        <tr runat="server" id="itemPlaceholder">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <table class="ListViewNormalStyle" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="ProjectsControl_rowNumberLabel" runat="server" Text="" SkinID="sknRowNumber"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ID="ProjectsControl_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                            ToolTip="Delete Project" CommandName="deleteProject" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel GroupingText="Project" runat="server" ID="ProjectsControl_projectPanel">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 22%">
                                                        <asp:Label runat="server" ID="ProjectsControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <asp:TextBox ID="ProjectControl_deleteRowIndex" runat="server" Text='<%# Eval("ProjectId") %>'  style="display:none" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_projectNameTextBox" Text='<%# Eval("ProjectName") %>'
                                                            Width="98%" ToolTip="Project Name" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label runat="server" ID="ProjectsControl_descLabel" Text="Description" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_projectDescTextBox" Text='<%# Eval("ProjectDescription") %>'
                                                            TextMode="MultiLine" Width="98%" Height="40px" ToolTip="Project Description" MaxLength="5000" onkeyup="CommentsCount(5000,this)"
                                                                onchange="CommentsCount(5000,this)" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label runat="server" ID="ProjectsControl_roleLabel" Text="Role" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_roleTextBox" Text='<%# Eval("Role") %>'
                                                            Width="98%" ToolTip="Role" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_positionLabel" Text="Position" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_positionTextBox" Text='<%# Eval("PositionTitle") %>'
                                                            Width="98%" ToolTip="Position" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_periodLabel" Text="Date From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 40%">
                                                                    <ajaxToolKit:CalendarExtender ID="ProjectsControl_startDtCalendarExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_startDtTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy "
                                                                        PopupPosition="BottomLeft"  PopupButtonID="ProjectsControl_calendarStartDtImageButton"/>
                                                                    <ajaxToolKit:MaskedEditExtender ID="ProjectsControl_startDtMaskedEditExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_startDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                        InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                        MaskType="Date" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                    <ajaxToolKit:MaskedEditValidator ID="ProjectsControl_startDtMaskedEditValidator" runat="server"
                                                                        ControlExtender="ProjectsControl_startDtMaskedEditExtender" ControlToValidate="ProjectsControl_startDtTextBox"
                                                                        EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                        TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                         />
                                                                    <asp:TextBox runat="server" ID="ProjectsControl_startDtTextBox" Text='<%# Eval("StartDate") %>'
                                                                        ToolTip="Start Date" Width="72px"></asp:TextBox>
                                                                    <asp:ImageButton ID="ProjectsControl_calendarStartDtImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    
                                                                </td>
                                                                <td style="width: 20%" align="center">
                                                                    <asp:Label runat="server" ID="ProjectsControl_toLabel" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:TextBox runat="server" ID="ProjectsControl_endDtTextBox" Text='<%# Eval("EndDate") %>'
                                                                        ToolTip="End Date" Width="72px" ></asp:TextBox>
                                                                        <asp:ImageButton ID="ProjectsControl_calendarEndDtImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                    <ajaxToolKit:CalendarExtender ID="ProjectsControl_endDtCalendarExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_endDtTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy "
                                                                        PopupPosition="BottomLeft"  PopupButtonID="ProjectsControl_calendarendDtImageButton"/>
                                                                    <ajaxToolKit:MaskedEditExtender ID="ProjectsControl_endDtMaskedEditExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_endDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                        InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                        MaskType="Date" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                    <ajaxToolKit:MaskedEditValidator ID="ProjectsControl_endDtMaskedEditValidator" runat="server"
                                                                        ControlExtender="ProjectsControl_endDtMaskedEditExtender" ControlToValidate="ProjectsControl_endDtTextBox"
                                                                        EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                        TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                         />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_locationLabel" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_locationTextBox" Text='<%# SetLocation(Container.DataItem) %>'
                                                            Width="98%" ToolTip="Location"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_environmentLabel" Text="Environment"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_environmentTextBox" Text='<%# Eval("Environment") %>'
                                                            Width="98%" ToolTip="Environment" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel GroupingText="Client" runat="server" ID="Panel1" BorderColor="#2A3680">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 10%">
                                                        <asp:Label runat="server" ID="ProjectsControl_clientNameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:TextBox runat="server" ID="ProjectsControl_clientNameTextBox" Text='<%# Eval("ClientName") %>'
                                                            ToolTip="Client Name" Width="95%" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label runat="server" ID="ProjectsControl_clientIndustryLabel" Text="Industry"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:TextBox runat="server" ID="ProjectsControl_clientIndustryTextBox" Text='<%# Eval("ClientIndustry") %>'
                                                            Width="95%" ToolTip="Client Industry" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr>
                        <td>
                            <table class="ListViewAlternateStyle" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="ProjectsControl_rowNumberLabel" runat="server" Text="" SkinID="sknRowNumber"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ID="ProjectsControl_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                            ToolTip="Delete Project" CommandName="deleteProject" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel GroupingText="Project" runat="server" ID="ProjectsControl_projectPanel">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 22%">
                                                      <asp:Label runat="server" ID="ProjectsControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                      <asp:TextBox ID="ProjectControl_deleteRowIndex" runat="server" Text='<%# Eval("ProjectId") %>'  style="display:none" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_projectNameTextBox" Text='<%# Eval("ProjectName") %>'
                                                            Width="98%" ToolTip="Project Name" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label runat="server" ID="ProjectsControl_descLabel" Text="Description" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_projectDescTextBox" Text='<%# Eval("ProjectDescription") %>'
                                                            TextMode="MultiLine" Width="98%" Height="40px" MaxLength="5000"  onkeyup="CommentsCount(5000,this)"
                                                                onchange="CommentsCount(5000,this)" ToolTip="Project Description" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label runat="server" ID="ProjectsControl_roleLabel" Text="Role" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_roleTextBox" Text='<%# Eval("Role") %>'
                                                            Width="98%" ToolTip="Role" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_positionLabel" Text="Position" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_positionTextBox" Text='<%# Eval("PositionTitle") %>'
                                                            Width="98%" ToolTip="Position" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_periodLabel" Text="Date From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 40%">
                                                                    <ajaxToolKit:CalendarExtender ID="ProjectsControl_startDtCalendarExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_startDtTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy "
                                                                        PopupPosition="BottomLeft"  PopupButtonID="ProjectsControl_calendarStartDtImageButton"/>
                                                                    <ajaxToolKit:MaskedEditExtender ID="ProjectsControl_startDtMaskedEditExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_startDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                        InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                        MaskType="Date" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                    <ajaxToolKit:MaskedEditValidator ID="ProjectsControl_startDtMaskedEditValidator" runat="server"
                                                                        ControlExtender="ProjectsControl_startDtMaskedEditExtender" ControlToValidate="ProjectsControl_startDtTextBox"
                                                                        EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                        TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                         />
                                                                    <asp:TextBox runat="server" ID="ProjectsControl_startDtTextBox" Text='<%# Eval("StartDate") %>'
                                                                        ToolTip="Start Date" Width="72px"></asp:TextBox>
                                                                               <asp:ImageButton ID="ProjectsControl_calendarStartDtImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                </td>
                                                                <td style="width: 20%" align="center">
                                                                    <asp:Label runat="server" ID="ProjectsControl_toLabel" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:TextBox runat="server" ID="ProjectsControl_endDtTextBox" Text='<%# Eval("EndDate") %>'
                                                                        ToolTip="End Date" Width="72px"></asp:TextBox>
                                                                        <asp:ImageButton ID="ProjectsControl_calendarEndDtImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                    <ajaxToolKit:CalendarExtender ID="ProjectsControl_endDtCalendarExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_endDtTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy "
                                                                        PopupPosition="BottomLeft"  PopupButtonID="ProjectsControl_calendarendDtImageButton"/>
                                                                    <ajaxToolKit:MaskedEditExtender ID="ProjectsControl_endDtMaskedEditExtender" runat="server"
                                                                        TargetControlID="ProjectsControl_endDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                        InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                        MaskType="Date" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                    <ajaxToolKit:MaskedEditValidator ID="ProjectsControl_endDtMaskedEditValidator" runat="server"
                                                                        ControlExtender="ProjectsControl_endDtMaskedEditExtender" ControlToValidate="ProjectsControl_endDtTextBox"
                                                                        EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                        TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                         />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_locationLabel" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_locationTextBox" Text='<%# SetLocation(Container.DataItem) %>'
                                                            Width="98%" ToolTip="Location" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="ProjectsControl_environmentLabel" Text="Environment"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="ProjectsControl_environmentTextBox" Text='<%# Eval("Environment") %>'
                                                            Width="98%" ToolTip="Environment" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel GroupingText="Client" runat="server" ID="Panel1" BorderColor="#2A3680">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 10%">
                                                        <asp:Label runat="server" ID="ProjectsControl_clientNameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:TextBox runat="server" ID="ProjectsControl_clientNameTextBox" Text='<%# Eval("ClientName") %>'
                                                            ToolTip="Client Name" Width="95%" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label runat="server" ID="ProjectsControl_clientIndustryLabel" Text="Industry"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:TextBox runat="server" ID="ProjectsControl_clientIndustryTextBox" Text='<%# Eval("ClientIndustry") %>'
                                                            Width="95%" ToolTip="Client Industry" MaxLength="500" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
            </asp:ListView>
            <asp:UpdatePanel ID="ProjectControl_confirmPopUpUpdatPanel" runat="server">
            <ContentTemplate>
                <div id="ProjectControl_hiddenDIV" runat="server" style="display: none">
                    <asp:Button ID="ProjectControl_hiddenPopupModalButton" runat="server" />
                 </div>
                 <ajaxToolKit:ModalPopupExtender ID="ProjectControl_deleteModalPopupExtender" runat="server"
                         PopupControlID="ProjectControl_deleteConfirmPopUpPanel" TargetControlID="ProjectControl_hiddenPopupModalButton"
                         BackgroundCssClass="modalBackground">
                 </ajaxToolKit:ModalPopupExtender>
                 <asp:Panel ID="ProjectControl_deleteConfirmPopUpPanel" runat="server" 
                    Style="display: none; width:30%; height:210px; background-position:bottom; background-color:#283033;">
                  <uc1:ConfirmMsgControl ID="ProjectControl_confirmPopupExtenderControl" runat="server" Type="YesNo"  Title="Delete Project" Message="Are you sure want to delete project details?" OnOkClick="ProjectControl_okPopUpClick" />
                 </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

