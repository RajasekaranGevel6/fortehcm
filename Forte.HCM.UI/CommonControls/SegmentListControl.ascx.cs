﻿using System;
using System.Collections.Generic;

using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class SegmentListControl : System.Web.UI.UserControl
    {
        public delegate void Button_Click(object sender, EventArgs e);
        public event Button_Click OkClick;
        // public event Button_Click CancelClick;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SegmentListControl_titleLiteral.Text = "Segments";
            }
        }

        protected void SegmentListControl_addButton_Click(object sender, EventArgs e)
        {
            string[] segmentPath;

            for (int i = 0; i < SegmentListControl_segmentCheckBoxList.Items.Count; i++)
            {
                if (SegmentListControl_segmentCheckBoxList.Items[i].Selected)
                {
                    segmentPath = SegmentListControl_segmentCheckBoxList.Items[i].Value.Split(',');
                    SegmentDetail segmentDetail = new SegmentDetail();
                    segmentDetail.SegmentID = int.Parse(segmentPath[0]);
                    segmentDetail.SegmentName = SegmentListControl_segmentCheckBoxList.Items[i].Text;
                    segmentDetail.segmentPath = segmentPath[1];

                    SelectedSegmentDetail.Add(segmentDetail);
                }
            }
            if (OkClick != null)
            {
                OkClick(sender, e);
            }

        }
        //  List<Segment> segmentList;
        List<SegmentDetail> selectedSegmentDetail = new List<SegmentDetail>();

        public List<Segment> SegmentList
        {
            set
            {
                List<Segment> segmentList = new List<Segment>();
                segmentList = value;
                SegmentListControl_segmentCheckBoxList.DataSource = value;
                SegmentListControl_segmentCheckBoxList.DataTextField = "SegmentName";
                SegmentListControl_segmentCheckBoxList.DataValueField = "SegmentIDAndPath";
                SegmentListControl_segmentCheckBoxList.DataBind();
                for (int i = 0; i < segmentList.Count; i++)
                {
                    if (selectedSegmentDetail.Exists(item => item.SegmentID == segmentList[i].SegmentID))
                    {
                        SegmentListControl_segmentCheckBoxList.Items[i].Selected = true;
                        SegmentListControl_segmentCheckBoxList.Items[i].Enabled = false;
                    }
                }
            }
        }

        public List<SegmentDetail> SelectedSegmentDetail
        {
            set
            {
                selectedSegmentDetail = value;
            }
            get
            {
                return selectedSegmentDetail;
            }
        }
    }
}