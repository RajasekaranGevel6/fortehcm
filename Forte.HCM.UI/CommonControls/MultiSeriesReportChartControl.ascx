﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiSeriesReportChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.MultiSeriesReportChartControl" %>

<table>
    <tr>
        <td runat="server" id="MultiSeriesReportChartControl_td">
            <asp:Chart ID="MultiSeriesReportChartControl_chart" runat="server" Height="170px"
                Width="454px" Palette="Pastel" BackColor="Transparent" AntiAliasing="Graphics"
                ForeColor="180, 65, 140, 240" Font="Arial, 8.25pt, style=Bold" ToolTip="Click here to zoom graph"  EnableViewState="true">
                <Legends>
                    <asp:Legend Name="MultiSeriesReportChartControl_legend" Font="Arial, 8.25pt" ForeColor="114, 142, 192"
                        BackColor="Transparent" Docking="Top">
                    </asp:Legend>
                </Legends>
                <Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="MultiSeriesReportChartControl_chartArea" BackSecondaryColor="Transparent"
                        BackColor="Transparent" ShadowColor="Transparent">
                        <AxisY>
                            <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                            <LabelStyle ForeColor="180, 65, 140, 240" Angle="90" />
                        </AxisY>
                        <AxisX IsLabelAutoFit="False" Interval="1">
                            <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                            <LabelStyle ForeColor="180, 65, 140, 240" Angle="0" TruncatedLabels="True" />
                        </AxisX>
                        <AxisX2 TitleForeColor="180, 65, 140, 240">
                        </AxisX2>
                    </asp:ChartArea>
                </ChartAreas>
                <Titles>
                    <asp:Title Name="MultiSeriesReportChartControl_title">
                    </asp:Title>
                </Titles>
            </asp:Chart>
        </td>
    </tr>
</table>
