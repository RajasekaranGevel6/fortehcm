﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubjectChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SubjectChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultipleSeriesChartControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="SubjectChartControl_updatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left">
                                    <div style="width: 100%; overflow: auto">
                                     <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                     <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="SubjectChartControl_testKeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="SubjectChartControl_widgetMultiSelectControl" runat="server"
                                            OnselectedIndexChanged="SubjectChartControl_selectProperty" 
                                            OnClick="SubjectChartControl_widgetMultiSelectControl_Click" 
                                            OncancelClick="SubjectChartControl_widgetMultiSelectControl_CancelClick" 
                                            />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:Label ID="SubjectChartControl_selectScoreLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="SubjectChartControl_selectOptionsAbsoluteScore" runat="server"
                                                    Text="  Absolute Score" Checked="true" GroupName="1" AutoPostBack="True" OnCheckedChanged="SubjectChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="SubjectChartControl_selectOptionsRelativeScore" runat="server"
                                                    Text="  Relative Score" GroupName="1" AutoPostBack="True" OnCheckedChanged="SubjectChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="SubjectChartControl_showComparativeScoreLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Show Comparative Score" OnClick="SubjectChartControl_showComparativeScoreLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="SubjectChartControl_scoreHiddenField" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <uc1:MultipleSeriesChartControl ID="SubjectChartControl_multipleSeriesChartControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
