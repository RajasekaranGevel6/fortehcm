﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

namespace Forte.HCM.UI.CommonControls
{
    public partial class QuestionTypeControl : System.Web.UI.UserControl
    {
        public event QuestionTypeDelegate QuestionTypeChanged;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void QuestionTypeControl_questionTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (QuestionTypeChanged != null )
                QuestionTypeChanged(this, new QuestionTypeEventArgs ((QuestionType)Convert.ToInt32(QuestionTypeControl_questionTypeDropDownList.SelectedValue)));
        }

        public bool Enabled
        {
            set
            {
                QuestionTypeControl_questionTypeDropDownList.Enabled = value;
            }
        }

        public QuestionType QuestionType
        {
            set
            {
                if (value == QuestionType.MultipleChoice)
                    QuestionTypeControl_questionTypeDropDownList.SelectedValue = "1";
                else if (value == QuestionType.OpenText)
                    QuestionTypeControl_questionTypeDropDownList.SelectedValue = "2";
            }
            get
            {
                return (QuestionType)Convert.ToInt32(QuestionTypeControl_questionTypeDropDownList.SelectedValue);
            }
        }
    }
}