﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ChangePassword" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="ChangePassword_messagetitleLiteral" runat="server">Change Password</asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="ChangePassword_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_subscription_type_bg">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="ChangePassword_messageTd" runat="server">
                                <td>
                                    <asp:Label ID="ChangePassword_topErrorMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                    <asp:Label ID="ChangePassword_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                <asp:Panel ID="ChangePassword_panel" runat="server">
                                    <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 39%" align="left">
                                                <asp:Label ID="ChangePassword_oldPasswordLabel" runat="server" Text="Current Password"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                <span class="mandatory">*</span>
                                            </td>
                                            <td align="left" style="width: 60%" colspan="3">
                                                <asp:TextBox ID="ChangePassword_oldPasswordTextBox" runat="server" MaxLength="50"
                                                    Width="90%" TextMode="Password"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 39%" align="left">
                                                <asp:Label ID="ChangePassword_newPasswordLabel" runat="server" Text="New Password"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                <span class="mandatory">*</span>
                                            </td>
                                            <td align="left" style="width: 60%" colspan="3">
                                                <asp:TextBox ID="ChangePassword_newPasswordTextBox" runat="server" MaxLength="50"
                                                    Width="50%" TextMode="Password"></asp:TextBox>
                                                <!--  TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"
                                                    StrengthStyles="barIndicator_poor; barIndicator_weak; barIndicator_good; barIndicator_strong; barIndicator_excellent"-->
                                                <br />
                                                <asp:Label ID="ChangePassword_passwordHelpLabel" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 39%" align="left">
                                                <asp:Label ID="ChangePassword_confirmPasswordLabel" runat="server" Text="Confirm Password"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                <span class="mandatory">*</span>
                                            </td>
                                            <td align="left" style="width: 60%" colspan="3">
                                                <asp:TextBox ID="ChangePassword_confirmPasswordTextBox" runat="server" MaxLength="50"
                                                    Width="90%" TextMode="Password"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                    </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" align="left">
            <asp:Button ID="ChangePassword_sendButton" runat="server" Text="Change Password"
                SkinID="sknButtonId" ValidationGroup="a" OnClick="ChangePassword_sendButton_Click" />
            &nbsp; &nbsp;
            <asp:LinkButton ID="ChangePassword_featureCancelLinkButton" runat="server" Text="Cancel"
                SkinID="sknPopupLinkButton">
            </asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
