﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnlineInterviewSummaryControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.OnlineInterviewSummaryControl" %>
<%@ Register Src="SingleSeriesChartControl.ascx" TagName="SingleSeriesChartControl"
    TagPrefix="uc1" %>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td width="30%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    <asp:Literal ID="OnlineInterviewSummaryControl_interviewSummaryLiteral" runat="server" Text="Interview Summary"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg" valign="middle">
                        <div style="width: 100%; height: 150px; overflow: auto;">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="td_height_10"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                            <tr>
                                                <td style="width:200px; text-align: left">
                                                    <asp:Label ID="OnlineInterviewSummaryControl_noOfQuestionHeadLabel" runat="server"
                                                        Text="Number Of Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="text-align: right;padding-right:40px">
                                                    <asp:Label ID="OnlineInterviewSummaryControl_noOfQuestionLabel" runat="server"
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_10"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataList ID="OnlineInterviewSummaryControl_skillDataList"
                                            runat="server" RepeatColumns="1" Width="100%" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td style="width: 200px; text-align: left">
                                                            <asp:Label ID="OnlineInterviewSummaryControl_skillNameLabel" runat="server"
                                                                SkinID="sknLabelFieldHeaderText" Text='<%# Eval("SkillName") %>'>
                                                            </asp:Label>
                                                        </td>
                                                        <td style="text-align:right;padding-right:40px">
                                                            <asp:Label ID="OnlineInterviewSummaryControl_skillCountLabel" runat="server"
                                                                SkinID="sknLabelFieldText" Text='<%# Eval("SkillCount") %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5"></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td width="1%">
        </td>
        <td width="36%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    <asp:Literal ID="OnlineInterviewSummaryControl_interviewAreaLiteral" runat="server" Text="Interview Area Statistics"> </asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg" valign="middle">
                        <div style="width: 100%; height: 150px; overflow: auto;">
                            <uc1:SingleSeriesChartControl ID="OnlineInterviewSummaryControl_interviewAreaChart" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td width="1%">
        </td>
        <td width="30%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    Complexity Statistics
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg">
                        <div style="height: 150px; overflow: auto;">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td align="left">
                                        <uc1:SingleSeriesChartControl ID="OnlineInterviewSummaryControl_complexityChart" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="td_height_5" colspan="3">
        </td>
    </tr>
</table>
