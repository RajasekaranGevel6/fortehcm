﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientControl.cs
// File that represents the user interface for the Create,Edit and View the client.

#endregion Header

#region Directives
using System;
using System.Web.UI;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Web.UI.WebControls;
using Forte.HCM.Support;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ClientControl : System.Web.UI.UserControl
    {
        // The public events must conform to this format
        public delegate void Button_CommandClick(object sender, CommandEventArgs e);

        // These two public events are to be customized as need on the page and will be called
        // by the private events of the buttons in the user control
        public event Button_CommandClick OkCommandClick;
        //public event Button_Click CancelClick;

        string CLIENT_ATTRIBUTES_DATASOUCE = "CLIENT_ATTRIBUTES_DATASOUCE";
        protected void Page_Load(object sender, EventArgs e)
        {
            ClientControl_clientNameTextBox.Focus();
            //ClientControl_clientNameTextBox.
        }

        public int TenantID
        {
            set
            {
                ClientControl_tenantIDHiddenField.Value = value.ToString();
            }
            get
            {
                if (Utility.IsNullOrEmpty(ClientControl_tenantIDHiddenField.Value))
                    return 0;

                return Convert.ToInt32(ClientControl_tenantIDHiddenField.Value);
            }
        }

        public ClientInformation ClientDataSource
        {
            set
            {
                if (value == null)
                {
                    ClientControl_additionalInfoTextBox.Text = string.Empty;
                    ClientControl_streetAddressTextBox.Text = string.Empty;
                    ClientControl_eMailIDTextBox.Text = string.Empty;
                    ClientControl_cityTextBox.Text = string.Empty;
                    ClientControl_countryTextBox.Text = string.Empty;
                    ClientControl_stateTextBox.Text = string.Empty;
                    ClientControl_zipCodeTextBox.Text = string.Empty;
                    ClientControl_clientNameTextBox.Text = string.Empty;
                    ClientControl_faxNoTextBox.Text = string.Empty;
                    ClientControl_FEIN_NO_TextBox.Text = string.Empty;
                    ClientControl_phoneNoTextBox.Text = string.Empty;
                    ClientControl_websiteURL_TextBox.Text = string.Empty;
                    ClientControl_createButton.CommandName = "add";
                    ClientControl_messagetitleLiteral.Text = "Add Client";
                    ClientControl_createButton.CommandArgument = "0";
                    return;
                }
                ClientControl_additionalInfoTextBox.Text = value.AdditionalInfo;
                ClientControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                ClientControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                ClientControl_cityTextBox.Text = value.ContactInformation.City;
                ClientControl_countryTextBox.Text = value.ContactInformation.Country;
                ClientControl_stateTextBox.Text = value.ContactInformation.State;
                ClientControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                ClientControl_clientNameTextBox.Text = value.ClientName;
                ClientControl_faxNoTextBox.Text = value.FaxNumber;
                ClientControl_FEIN_NO_TextBox.Text = value.FeinNo;
                ClientControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                ClientControl_websiteURL_TextBox.Text = value.ContactInformation.WebSiteAddress.Personal;
                ClientControl_createButton.CommandName = "edit";
                ClientControl_messagetitleLiteral.Text = "Edit Client";
                ClientControl_createButton.CommandArgument = value.ClientID.ToString();
            }
            get
            {
                ClientInformation client = new ClientInformation();
                client.AdditionalInfo = ClientControl_additionalInfoTextBox.Text.Trim();
                ContactInformation contInfo = new ContactInformation();

                contInfo.StreetAddress = ClientControl_streetAddressTextBox.Text.Trim();
                contInfo.EmailAddress = ClientControl_eMailIDTextBox.Text.Trim();
                contInfo.City = ClientControl_cityTextBox.Text.Trim();
                contInfo.Country = ClientControl_countryTextBox.Text.Trim();
                contInfo.State = ClientControl_stateTextBox.Text.Trim();
                contInfo.PostalCode = ClientControl_zipCodeTextBox.Text.Trim();

                client.ClientID = 0;
                client.ClientName = ClientControl_clientNameTextBox.Text.Trim();

                client.FaxNumber = ClientControl_faxNoTextBox.Text.Trim();
                client.FeinNo = ClientControl_FEIN_NO_TextBox.Text.Trim();
                client.IsDeleted = false;
                PhoneNumber phone = new PhoneNumber();
                phone.Mobile = ClientControl_phoneNoTextBox.Text.Trim();
                contInfo.Phone = phone;

                WebSite website = new WebSite();
                website.Personal = ClientControl_websiteURL_TextBox.Text.Trim();
                contInfo.WebSiteAddress = website;
                client.ContactInformation = contInfo;
                return client;
            }
        }

        private bool IsValid()
        {
            // Check if feature usage limit exceeds for creating client.
            if (ClientControl_createButton.CommandName == "add")
            {
                if (new AdminBLManager().IsFeatureUsageLimitExceeds(this.TenantID, Constants.FeatureConstants.CREATE_CLIENT))
                {
                    ShowErrorMessage("You have reached the maximum limit based your subscription plan. Cannot create more clients");
                    InvokePageBaseMethod("ClientManagement_client_ModalpPopupExtender", null);
                    return false;
                }
            }

            StringBuilder requiredFieldMessage = new StringBuilder();
            string validEmail = "";
            string validWebSite = "";
            bool isvalid = true;
            ClientInformation clientInformation = new ClientInformation();
            clientInformation = ClientDataSource;
            NomenclatureCustomize clientAttributes = new NomenclatureCustomize();
            if (Utility.IsNullOrEmpty(ViewState[CLIENT_ATTRIBUTES_DATASOUCE]))
                return isvalid;
            clientAttributes = (NomenclatureCustomize)ViewState[CLIENT_ATTRIBUTES_DATASOUCE];

            if (clientAttributes.ClientNameIsVisible&& clientAttributes.ClientNameFieldIsMandatory
              && clientInformation.ClientName.Length == 0)
            {
                requiredFieldMessage.Append(clientAttributes.ClientName);
                isvalid = false;
            }

            //if (clientAttributes.StreetAddressIsVisible && clientAttributes.StreetAddressIsMandatory
            //    && clientInformation.ContactInformation.StreetAddress.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.StreetAddress);
            //    isvalid = false;
            //}
            //if (clientAttributes.CityIsVisible && clientAttributes.CityIsMandatory
            //    && clientInformation.ContactInformation.City.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.City);
            //    isvalid = false;
            //}

            //if (clientAttributes.StateIsVisible && clientAttributes.StateIsMandatory
            //    && clientInformation.ContactInformation.State.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.state);
            //    isvalid = false;
            //}
            //if (clientAttributes.ZipCodeIsVisible && clientAttributes.ZipCodeIsMandatory
            //    && clientInformation.ContactInformation.PostalCode.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.Zipcode);
            //    isvalid = false;
            //}
            //if (clientAttributes.CountryIsVisible && clientAttributes.CountryIsMandatory
            //    && clientInformation.ContactInformation.Country.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.Country);
            //    isvalid = false;
            //}
            //if (clientAttributes.FEINNOIsVisible && clientAttributes.FEINNOIsMandatory
            //    && clientInformation.FeinNo.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.FEINNO);
            //    isvalid = false;
            //}
            if (clientAttributes.PhoneNumberIsVisible && clientAttributes.PhoneNumberFieldIsMandatory
                && clientInformation.ContactInformation.Phone.Mobile.Length == 0)
            {
                if (requiredFieldMessage.Length > 0)
                    requiredFieldMessage.Append(", ");
                requiredFieldMessage.Append(clientAttributes.PhoneNumber);
                isvalid = false;
            }
            //if (clientAttributes.FaxNoIsVisible && clientAttributes.FaxNoIsMandatory
            //    && clientInformation.FaxNumber.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.FaxNo);
            //    isvalid = false;
            //}
            if (clientAttributes.EmailIDIsVisible && clientAttributes.EmailIDFieldIsMandatory)
            {
                if (clientInformation.ContactInformation.EmailAddress.Length == 0)
                {
                    if (requiredFieldMessage.Length > 0)
                        requiredFieldMessage.Append(", ");
                    requiredFieldMessage.Append(clientAttributes.EmailID);
                    isvalid = false;
                }
                else if (!Utility.IsValidEmailAddress(clientInformation.ContactInformation.EmailAddress))
                {
                    validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                    isvalid = false;
                }

            }
            else if (clientInformation.ContactInformation.EmailAddress.Length>0 && !Utility.IsValidEmailAddress(clientInformation.ContactInformation.EmailAddress))
            {
                validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                isvalid = false;
            }


            //if (clientAttributes.WebsiteURLIsVisible && clientAttributes.WebsiteURLFieldIsMandatory)
            //{
            //    if (clientInformation.ContactInformation.WebSiteAddress.Personal.Length == 0)
            //    {
            //        if (requiredFieldMessage.Length > 0)
            //            requiredFieldMessage.Append(", ");
            //        requiredFieldMessage.Append(clientAttributes.WebsiteURL);
            //        isvalid = false;
            //    }
            //    else if (!Utility.IsValidWebsiteURL(clientInformation.ContactInformation.WebSiteAddress.Personal))
            //    {
            //        validWebSite = "Enter valid website address";
            //        isvalid = false;
            //    }

            //}
            //else if (clientInformation.ContactInformation.WebSiteAddress.Personal.Length > 0 && !Utility.IsValidWebsiteURL(clientInformation.ContactInformation.WebSiteAddress.Personal))
            //{
            //    validWebSite = "Enter valid website address";
            //    isvalid = false;
            //}
            //if (clientAttributes.AdditionalInfoIsVisible && clientAttributes.AdditionalFieldInfoIsMandatory
            //    && clientInformation.AdditionalInfo.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clientAttributes.AdditionalInfo);
            //    isvalid = false;
            //}

            if (!isvalid)
            {
                // ShowErrorMessage("The following field(s) are mandatory and require to be filled:"+message.ToString());
                if (requiredFieldMessage.Length > 0)
                    ShowErrorMessage("Mandatory fields cannot be empty");
                if (validEmail.Length > 0)
                    ShowErrorMessage(validEmail);
                if (validWebSite.Length > 0)
                    ShowErrorMessage(validWebSite);
                InvokePageBaseMethod("ClientManagement_client_ModalpPopupExtender", null);
            }

            return isvalid;
        }

        private void ShowErrorMessage(string exp)
        {
            if (string.IsNullOrEmpty(ClientControl_topErrorMessageLabel.Text))
                ClientControl_topErrorMessageLabel.Text = exp;
            else
                ClientControl_topErrorMessageLabel.Text += "<br />" + exp;
        }

        public string ShowMessage
        {
            set
            {
                ShowErrorMessage(value);
            }
        }

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <param name="args">The args.</param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        protected void ClientControl_createButton_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (!IsValid())
                return;

            if (OkCommandClick != null)
                this.OkCommandClick(sender, e);
        }

        public NomenclatureCustomize ClientAttributesDataSouce
        {
            set
            {
                //NomenClatureCustomize clientAttributes = new NomenClatureCustomize();
                ////clientAttributes = new ControlUtility().LoadAttributeDetails(1,Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT);
                //clientAttributes = ClientAttributesDataSouce;
                if (value == null)
                    return;
                ViewState[CLIENT_ATTRIBUTES_DATASOUCE] = value;

                int i = 1;
                //for (int i = 1; i < 20; i++)
                //{
                HtmlTableCell TableCell = null;
                if (value.ClientNameIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    ClientControl_clientNameLabel.Visible = true;
                    ClientControl_clientNameLabel.Text = value.ClientName;
                    TableCell.Controls.Add(ClientControl_clientNameLabel);
                    if (value.ClientNameFieldIsMandatory)
                    {
                        ClientControl_clientNameSpan.Visible = true;
                        TableCell.Controls.Add(ClientControl_clientNameSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    ClientControl_clientNameTextBox.Visible = true;
                    TableCell.Controls.Add(ClientControl_clientNameTextBox);
                    i++;
                }

                if (value.StreetAddressIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    ClientControl_streetAddressLabel.Visible = true;
                    ClientControl_streetAddressLabel.Text = value.StreetAddress;
                    TableCell.Controls.Add(ClientControl_streetAddressLabel);
                    //if (value.StreetAddressIsMandatory)
                    //{
                    //    ClientControl_streetAddressSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_streetAddressSpan);
                    //}
                    i++;
                    ClientControl_streetAddressTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_streetAddressTextBox);
                    i++;
                }
                if (value.CityIsVisible)
                {
                    ClientControl_cityLabel.Visible = true;
                    ClientControl_cityLabel.Text = value.City;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_cityLabel);
                    //if (value.CityIsMandatory)
                    //{
                    //    ClientControl_citySpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_citySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_cityTextBox);
                    i++;
                    ClientControl_cityTextBox.Visible = true;
                }
                if (value.StateIsVisible)
                {
                    ClientControl_stateLabel.Visible = true;
                    ClientControl_stateLabel.Text = value.state;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_stateLabel);
                    //if (value.StateIsMandatory)
                    //{
                    //    ClientControl_stateSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_stateSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_stateTextBox);
                    i++;
                    ClientControl_stateTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    ClientControl_countryLabel.Text = value.Country;
                    ClientControl_countryLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_countryLabel);
                    //if (value.CountryIsMandatory)
                    //{
                    //    ClientControl_countrySpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_countrySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_countryTextBox);
                    i++;
                    ClientControl_countryTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    ClientControl_zipCodeLabel.Text = value.Zipcode;
                    ClientControl_zipCodeLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_zipCodeLabel);

                    //if (value.ZipCodeIsMandatory)
                    //{
                    //    ClientControl_zipCodeSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_zipCodeSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_zipCodeTextBox);
                    i++;
                    ClientControl_zipCodeTextBox.Visible = true;
                }

                if (value.FEINNOIsVisible)
                {
                    ClientControl_FEIN_NOLabel.Text = value.FEINNO;
                    ClientControl_FEIN_NOLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_FEIN_NOLabel);

                    //if (value.FEINNOIsMandatory)
                    //{
                    //    ClientControl_FEIN_NOSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_FEIN_NOSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_FEIN_NO_TextBox);
                    i++;
                    ClientControl_FEIN_NO_TextBox.Visible = true;
                }

                if (value.FaxNoIsVisible)
                {
                    ClientControl_faxNoLabel.Text = value.FaxNo;
                    ClientControl_faxNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_faxNoLabel);

                    //if (value.FaxNoIsMandatory)
                    //{
                    //    ClientControl_faxNoSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_faxNoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_faxNoTextBox);
                    i++;
                    ClientControl_faxNoTextBox.Visible = true;
                }

                if (value.EmailIDIsVisible)
                {
                    ClientControl_eMailIDLabel.Text = value.EmailID;
                    ClientControl_eMailIDLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_eMailIDLabel);

                    if (value.EmailIDFieldIsMandatory)
                    {
                        ClientControl_eMailIDSpan.Visible = true;
                        TableCell.Controls.Add(ClientControl_eMailIDSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_eMailIDTextBox);
                    i++;
                    ClientControl_eMailIDTextBox.Visible = true;
                }
                if (value.PhoneNumberIsVisible)
                {
                    ClientControl_phoneNoLabel.Text = value.PhoneNumber;
                    ClientControl_phoneNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_phoneNoLabel);

                    if (value.PhoneNumberFieldIsMandatory)
                    {
                        ClientControl_phoneNoSpan.Visible = true;
                        TableCell.Controls.Add(ClientControl_phoneNoSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_phoneNoTextBox);
                    i++;
                    ClientControl_phoneNoTextBox.Visible = true;
                }
                if (value.WebsiteURLIsVisible)
                {
                    ClientControl_websiteURLLabel.Text = value.WebsiteURL;
                    ClientControl_websiteURLLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_websiteURLLabel);

                    //if (value.WebsiteURLFieldIsMandatory)
                    //{
                    //    ClientControl_websiteURLSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_websiteURLSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_websiteURL_TextBox);
                    i++;
                    ClientControl_websiteURL_TextBox.Visible = true;
                }
                if (value.AdditionalInfoIsVisible)
                {
                    ClientControl_additionalInfoLabel.Text = value.AdditionalInfo;
                    ClientControl_additionalInfoLabel.Visible = true;
                    i = 25;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_additionalInfoLabel);

                    //if (value.AdditionalFieldInfoIsMandatory)
                    //{
                    //    ClientControl_additionalInfoSpan.Visible = true;
                    //    TableCell.Controls.Add(ClientControl_additionalInfoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ClientControl_td" + i);
                    TableCell.Controls.Add(ClientControl_additionalInfoTextBox);
                    ClientControl_additionalInfoTextBox.Visible = true;
                }

                // }

            }

        }

        public string BindSaveButtonCommand
        {
            set
            {
                ClientControl_createButton.CommandName = value;
            }
        }
    }
}