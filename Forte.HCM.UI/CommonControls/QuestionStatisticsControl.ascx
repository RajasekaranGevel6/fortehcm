﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionStatisticsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.QuestionStatisticsControl" %>
<%@ Register Src="PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>

<script type="text/javascript" language="javascript">
    function ExpORCollapseQuestRows(targetControl) {
        var ctrl = document.getElementById('<%=QuestionStatisticsControl_stateExpandHiddenField.ClientID %>');
        targetValue = ctrl.value;
        if (targetValue == "0") {
            targetControl.innerHTML = "Collapse All";
            ExpandAllQuestRows(targetControl);
            ctrl.value = 1;
        }
        else {
            targetControl.innerHTML = "Expand All";
            CollapseAllRows(targetControl);
            ctrl.value = 0;
        }
        return false;
    }


    function ExpandAllQuestRows(targetControl) {
        var gridCtrl = document.getElementById("<%= QuestionStatisticsControl_questionDiv.ClientID %>");
        if (gridCtrl != null) {
            var rowItems = gridCtrl.getElementsByTagName("div");
            for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                if (rowItems[indexRow].id.indexOf("QuestionStatisticsControl_detailsDiv") != "-1") {
                    rowItems[indexRow].style.display = "block";
                }
            }
        }
        return false;
    }
    function CollapseAllRows(targetControl) {
        var gridCtrl = document.getElementById("<%= QuestionStatisticsControl_questionDiv.ClientID %>");
        if (gridCtrl != null) {
            var rowItems = gridCtrl.getElementsByTagName("div");
            for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                if (rowItems[indexRow].id.indexOf("QuestionStatisticsControl_detailsDiv") != "-1") {
                    rowItems[indexRow].style.display = "none";
                }
            }
        }
        return false;
    }

    function HideQuestDetails(ctrlId) {
        if (document.getElementById(ctrlId).style.display == "none") {
            CollapseAllRows(ctrlId);
            document.getElementById(ctrlId).style.display = "block";
        }
        else {
            CollapseAllRows(ctrlId);
            document.getElementById(ctrlId).style.display = "none";
        }
        return false;
    }

    function GetMouseClickedPos(ctrlID) {
        if (document.getElementById(ctrlID) != null) {
            document.getElementById(ctrlID).focus();
        }
        return true;
    }
</script>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right">
                        <asp:LinkButton ID="QuestionStatisticsControl_testDraftExpandLinkButton" runat="server"
                            Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseQuestRows(this);"></asp:LinkButton>
                        <asp:HiddenField ID="QuestionStatisticsControl_stateExpandHiddenField" runat="server"
                            Value="0" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:UpdatePanel ID="QuestionStatisticsControl_gridViewUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div style="height: 320px; overflow: auto;" runat="server" id="QuestionStatisticsControl_questionDiv">
                                    <asp:GridView ID="QuestionStatisticsControl_questionGridView" runat="server" AllowSorting="true"
                                        AutoGenerateColumns="false" Width="100%" OnSorting="QuestionStatisticsControl_questionGridView_Sorting"
                                        OnRowDataBound="QuestionStatisticsControl_questionGridView_RowDataBound" OnRowCreated="QuestionStatisticsControl_questionGridView_RowCreated"
                                        SkinID="sknWrapHeaderGrid">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Question ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="QuestionStatisticsControl_questionIdGridLinkButton" runat="server"
                                                        Text='<%# Eval("QuestionKey") %>' ToolTip="Question Details"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Font-Underline="False" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Question" SortExpression="Question" HeaderStyle-CssClass="td_padding_right_20"
                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="350px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="QuestionStatisticsControl_questionGridLinkButton" runat="server"
                                                        Text='<%# TrimQuestion(Eval("Question").ToString()) %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Avg&nbsp;Time&nbsp;Taken (Within Test in Sec)" DataField="AVERAGETIMETAKENWITHINTEST"
                                                SortExpression="LocalTime" ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                ItemStyle-HorizontalAlign="right" ItemStyle-Width="14%" />
                                            <asp:BoundField HeaderText="Avg&nbsp;Time&nbsp;Taken (Across All Test in Sec)" DataField="AVERAGETIMETAKENACROSSTEST"
                                                SortExpression="GlobalTime" ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                ItemStyle-HorizontalAlign="right" ItemStyle-Width="16%" />
                                            <asp:BoundField HeaderText="Right Answer : Administered" DataField="Ratio" HeaderStyle-CssClass="td_padding_right_20"
                                                SortExpression="Ratio" ItemStyle-HorizontalAlign="center" ItemStyle-Width="15%" />
                                            <asp:TemplateField HeaderText="Author" SortExpression="Author" ItemStyle-Width="16%">
                                                <ItemTemplate>
                                                    <asp:Label ID="QuestionStatisticsControl_modifiedDateLabel" runat="server" Text='<%# Eval("AuthorName") %>'></asp:Label>
                                                    <tr>
                                                        <td colspan="8" style="width: 100%">
                                                            <div id="QuestionStatisticsControl_detailsDiv" runat="server" style="display: none;">
                                                                <table border="0" cellpadding="3" cellspacing="3" class="table_outline_bg" width="100%">
                                                                    <tr>
                                                                        <td class="popup_question_icon">
                                                                            <asp:Label ID="QuestionStatisticsControl_questionLabel" runat="server" SkinID="sknLabelText"
                                                                                Text='<%# Eval("Question").ToString()%>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_padding_left_20">
                                                                            <asp:PlaceHolder ID="QuestionStatisticsControl_answerChoicesPlaceHolder" runat="server">
                                                                            </asp:PlaceHolder>
                                                                            <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_padding_10">
                                                                            <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_categoryHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Category"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_categoryTextBox" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text='<%#Eval("CATEGORYNAME") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 6%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_subjectHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Subject"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_subjectTextBox" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text='<%#Eval("SUBJECTNAME") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_testAreaHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Test Area"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 12%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_testAreaTextBox" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text='<%#Eval("TestAreaName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_complexityHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Complexity"></asp:Label>
                                                                                    </td>
                                                                
                                                                                    <td style="width: 21%">
                                                                                        <asp:Label ID="QuestionStatisticsControl_complexityTextBox" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text='<%#Eval("ComplexityName") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <%-- popup DIV --%>
                                                            <a href="#SearchQuestion_focusmeLink" id="QuestionStatisticsControl_focusDownLink"
                                                                runat="server"></a><a href="#" id="QuestionStatisticsControl_focusmeLink" runat="server">
                                                                </a>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="display: none">
                            <uc4:QuestionDetailPreviewControl ID="QuestionStatisticsControl_hiddenPreviewControl"
                                runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
