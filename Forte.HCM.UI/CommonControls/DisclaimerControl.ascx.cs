﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// DisclaimerControl.aspx.cs
// This page shows the disclaimer message before save the details. 
// This control has been implemented in several pages like SingleQuestionEntry,
// BatchQuestion.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;

#endregion Directives                                                          

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// This page that represents the layout for the user interface 
    /// to show the disclaimer message for the user before saving 
    /// their question in SingleQuestionEntry, EditQuestion, Batch
    /// Question pages.
    /// </summary>
    public partial class DisclaimerControl : UserControl
    {
        #region Public Properties                                              

        /// <summary>
        /// Gets the title message from the page who is called this property.
        /// </summary>
        public string Title
        {
            set
            {
                DisclaimerControl_titleLiteral.Text = value;
            }
        }

        /// <summary>
        /// Property that receives the message string from the page.
        /// </summary>
        public string Message
        {
            set
            {
                DisclaimerControl_messageLabel.Text = value;
            }
        }

        /// <summary>
        /// Redirects to the target URL
        /// </summary>
        public string RedirectURL
        {
            set
            {
                DisclaimerControl_hiddenField.Value = value;
            }
        }

        #endregion Public Properties                                           

        #region Event Handler                                                  

        /// <summary>
        /// Handler will get fired when the YES button is clicked in the popup
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void DisclaimerControl_yesButton_Click(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(DisclaimerControl_hiddenField.Value))
            //{
              // Response.Redirect(DisclaimerControl_hiddenField.Value,false);
            //}
        }

        /// <summary>
        /// Event will get triggered when the NO button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void DisclaimerControl_noLinkButton_Click(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Event will get triggered when the CANCEL button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void DisclaimerControl_cancelButton_Click(object sender, EventArgs e)
        {

        }

        #endregion Event Handler                                               
    }
}