﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CategoryChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultipleSeriesChartControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="CategoryChartControl_updatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left">
                                    <div style="width: 100%; overflow: auto">
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="CategoryChartControl_testKeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="CategoryChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="CategoryChartControl_selectProperty"
                                            OnClick="CategoryChartControl_widgetMultiSelectControl_Click"
                                            OncancelClick="CategoryChartControl_widgetMultiSelectControl_CancelClick"  />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:Label ID="CategoryChartControl_selectScoreLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="CategoryChartControl_selectOptionsAbsoluteScore" runat="server"
                                                    Text=" Absolute Score" Checked="true" GroupName="1" OnCheckedChanged="CategoryChartControl_selectOptionsAbsoluteScore_CheckedChanged"
                                                    AutoPostBack="true" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="CategoryChartControl_selectOptionsRelativeScore" runat="server"
                                                    AutoPostBack="true" Text=" Relative Score" GroupName="1" OnCheckedChanged="CategoryChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:LinkButton ID="CategoryChartControl_showComparativeScoreLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Show Comparative Score" OnClick="CategoryChartControl_showComparativeScoreLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="CategoryChartControl_contentStateHiddenField" runat="server"
                                                    Value="0" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <uc1:MultipleSeriesChartControl ID="CategoryChartControl_multipleSeriesChartControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
