﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkFlowSideLinkButtonControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.WorkFlowSideLinkButtonControl" %>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <ajaxToolKit:Accordion ID="Workflow_accordion" runat="server" RequireOpenedPane="false"
                SelectedIndex="-1" AutoSize="None">
                <Panes>
                    <ajaxToolKit:AccordionPane ID="Workflow_clientManagementAccordionPane1" runat="server">
                        <Header>
                            <asp:ImageButton ID="Workflow_clientManagementHeaderImageButton" runat="server" SkinID="skn_sideClientManagementHeader"
                                OnClick="Workflow_sideButton_Click" CommandName="Client Management" />
                        </Header>
                    </ajaxToolKit:AccordionPane>
                    <ajaxToolKit:AccordionPane ID="Workflow_positionProfileAccordionPane" runat="server">
                        <Header>
                            <asp:ImageButton ID="Workflow_positionProfileHeaderImageButton" runat="server" SkinID="skn_sidePositionProfileHeader"
                                OnClick="Workflow_sideButton_Click" CommandName="Position Profile" />
                        </Header>
                    </ajaxToolKit:AccordionPane>
                    <ajaxToolKit:AccordionPane Visible="false" ID="Workflow_weightedSearchAccordionPane" runat="server">
                        <Header>
                            <asp:ImageButton ID="Workflow_weightedSearchImageButton" runat="server" SkinID="skn_sideWSHeader"
                                PostBackUrl="~/TalentScoutHome.aspx" />
                        </Header>
                    </ajaxToolKit:AccordionPane>
                    <ajaxToolKit:AccordionPane ID="Workflow_assessmentAccordionPane" runat="server">
                        <Header>
                            <asp:ImageButton ID="Workflow_assessmentHeaderImageButton" runat="server" SkinID="skn_sideAssessmentHeader"
                                OnClick="Workflow_sideButton_Click" CommandName="Assessment" />
                        </Header>
                    </ajaxToolKit:AccordionPane>
                    <ajaxToolKit:AccordionPane Visible="false" ID="Workflow_interviewAccordionPane" runat="server">
                        <Header>
                            <asp:ImageButton ID="Workflow_interviewHeaderImageButton" runat="server" SkinID="skn_sideInterviewHeader"
                                OnClick="Workflow_sideButton_Click" CommandName="Interview" />
                        </Header>
                    </ajaxToolKit:AccordionPane>
                     <ajaxToolKit:AccordionPane Visible="false" ID="Workflow_onlineInterviewAccordionPane" runat="server">
                        <Header>
                            <asp:ImageButton ID="Workflow_onlineInterviewHeaderImageButton" runat="server" SkinID="skn_sideOnlineInterviewHeader"
                                OnClick="Workflow_sideButton_Click" CommandName="OnlineInterview" />
                        </Header>
                    </ajaxToolKit:AccordionPane>
                    <ajaxToolKit:AccordionPane ID="Workflow_viewReportsAccordionPane" runat="server">
                        <Header>
                            <asp:ImageButton ID="Workflow_viewReportsHeaderImageButton" runat="server" SkinID="skn_sideViewReportsHeader"
                                OnClick="Workflow_sideButton_Click" CommandName="Reports" />
                        </Header>
                    </ajaxToolKit:AccordionPane>
                </Panes>
            </ajaxToolKit:Accordion>
        </td>
    </tr>
</table>
