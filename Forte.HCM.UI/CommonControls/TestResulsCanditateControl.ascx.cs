﻿using System;
using System.Web.UI;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class TestResulsCanditateControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Used to assign the values for the candidate details
        /// </summary>
        public void LoadCandidateStatisticsDetails(CandidateStatisticsDetail CandidateStatisticsDataSource)
        {
            TestResulsCanditateControl_noOfQuestionLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestion.ToString();

            TestResulsCanditateControl_noOfQuestionattenedLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestionAttended.ToString();

            TestResulsCanditateControl_absoluteScoreLabelValue.Text =
                CandidateStatisticsDataSource.AbsoluteScore.ToString();

            TestResulsCanditateControl_noOfQuestionSkippedLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestionSkipped.ToString();

            TestResulsCanditateControl_relativeScoreLabelValue.Text = CandidateStatisticsDataSource.RelativeScore.ToString();

            TestResulsCanditateControl_noOfQuestionAnsweredCorrectlyLabelValue.Text =
                CandidateStatisticsDataSource.QuestionsAnsweredCorrectly.ToString();

            TestResulsCanditateControl_percentileLabelValue.Text =
            CandidateStatisticsDataSource.Percentile.ToString();

            TestResulsCanditateControl_noOfQuestionAnsweredWronglyLabelValue.Text
                = CandidateStatisticsDataSource.QuestionsAnsweredWrongly.ToString();

            TestResulsCanditateControl_noOfQuestionTimeTakenLabelValue.Text
                = CandidateStatisticsDataSource.TimeTaken;

            TestResulsCanditateControl_percentageQuestionsAnsweredCorrectlyLabelValue.Text
                = CandidateStatisticsDataSource.AnsweredCorrectly.ToString();

            TestResulsCanditateControl_myScoreLabelValue.Text =
                CandidateStatisticsDataSource.MyScore.ToString();

            decimal averageTimeTaken = 0;
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.AverageTimeTaken)
                && CandidateStatisticsDataSource.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(CandidateStatisticsDataSource.AverageTimeTaken);
            }
            TestResulsCanditateControl_avgTimeTakenLabelValue.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds
                (decimal.ToInt32(averageTimeTaken));
            TestResulsCanditateControl_testStatusLabelValue.Text = CandidateStatisticsDataSource.TestStatus;
        }
        public void chartData(MultipleSeriesChartData chartCategoryDataSource, MultipleSeriesChartData chartSubjectDataSource, MultipleSeriesChartData chartTestAreaDataSource)
        {
            TestResultControl_multipleSeriesChartControl.MultipleChartDataSource = chartCategoryDataSource;
            MultipleSeriesChartControl_subjectChart.MultipleChartDataSource = chartSubjectDataSource;
            MultipleSeriesChartControl_testAreaChart.MultipleChartDataSource = chartTestAreaDataSource;
        }
        public void chartHistogramData(ReportHistogramData chartComplexityDataSource)
        {
            TestResultsControl_histogramChartControl.LoadChartControl(chartComplexityDataSource);
        }
    }
}