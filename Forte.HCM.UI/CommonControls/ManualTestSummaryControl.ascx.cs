﻿using System;
using System.Linq;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class ManualTestSummaryControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void LoadTestAreaChart(SingleChartData testChartdata)
        {
            ManualTestSummaryControl_testAreaChart.DataSource = testChartdata;
        }

        private void LoadComplexityChart(SingleChartData complexityChartData)
        {
            ManualTestSummary_complexityChart.DataSource = complexityChartData;
        }

        #region Not Used 

        //public void LoadQuestionDetailsSummary(List<QuestionDetail> questionDetail, List<AttributeDetail> complexityDetails)
        //{
        //    if (questionDetail != null)
        //    {
        //        if (questionDetail.Count > 0)
        //        {

        //            List<decimal> complexityValue = new List<decimal>();
        //            List<decimal> creditEarned = new List<decimal>();
        //            List<decimal> timeTaken = new List<decimal>();
        //            List<TestArea> testAreaList = new List<TestArea>();

        //            for (int i = 0; i < questionDetail.Count; i++)
        //            {
        //                complexityValue.Add(questionDetail[i].ComplexityValue);
        //                creditEarned.Add(questionDetail[i].CreditsEarned);
        //                timeTaken.Add(questionDetail[i].AvarageTimeTaken);
        //            }

        //            ManualTestSummaryControl_noOfQuestionLabel.Text = questionDetail.Count.ToString();
        //            ManualTestSummaryControl_costOfTestLabel.Text = creditEarned.Average().ToString("0.##");
        //            //ManualTestSummaryControl_avgComplexityLabel.Text = Utility.GetComplexity(complexityValue.Average());
        //            int avgtimeTaken = Convert.ToInt32(timeTaken.Average()) * questionDetail.Count; 
        //            ManualTestSummaryControl_avgTimeLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds(avgtimeTaken);

                   


        //            //   decimal atributeValue;
        //            decimal minimumAtributeValue;
        //            decimal maximumAtributeValue;
        //            string atributeName = "";
        //            string atributeID = "";

        //            for (int i = 0; i < complexityDetails.Count; i++)
        //            {

        //                if (decimal.Parse(complexityDetails[complexityDetails.Count - 1].AttributeValue) == complexityValue.Average())
        //                {
        //                    atributeName = complexityDetails[complexityDetails.Count - 1].AttributeName;
        //                    atributeID = complexityDetails[complexityDetails.Count - 1].AttributeID;
        //                    break;
        //                }
        //                else if (decimal.Parse(complexityDetails[0].AttributeValue) == complexityValue.Average())
        //                {
        //                    atributeName = complexityDetails[0].AttributeName;
        //                    atributeID = complexityDetails[0].AttributeID;
        //                    break;
        //                }

        //                if (i == 0)
        //                {
        //                    minimumAtributeValue = decimal.Parse(complexityDetails[i].AttributeValue);
        //                    maximumAtributeValue = (decimal.Parse(complexityDetails[i].AttributeValue) + decimal.Parse(complexityDetails[i + 1].AttributeValue)) / 2;
        //                }
        //                else if (i == (complexityDetails.Count - 1))
        //                {
        //                    atributeName = complexityDetails[i].AttributeName;
        //                    atributeID = complexityDetails[i].AttributeID;
        //                    break;

        //                }
        //                else
        //                {
        //                    minimumAtributeValue = ((decimal.Parse(complexityDetails[i].AttributeValue) + decimal.Parse(complexityDetails[i - 1].AttributeValue)) / 2) + 0.01m;
        //                    maximumAtributeValue = (decimal.Parse(complexityDetails[i].AttributeValue) + decimal.Parse(complexityDetails[i + 1].AttributeValue)) / 2;
        //                }

        //                if (minimumAtributeValue < complexityValue.Average() && complexityValue.Average() <= maximumAtributeValue)
        //                {
        //                    atributeName = complexityDetails[i].AttributeName;
        //                    atributeID = complexityDetails[i].AttributeID;
        //                    break;
        //                }

                     
        //            }
        //            ManualTestSummaryControl_avgComplexityLabel.Text = atributeName;
        //            ManualTestSummaryControl_attribute_HiddenField.Value = atributeID;

                    
        //            //Find the count of complexity 
        //            var complexityGroups =
        //                   from q in questionDetail
        //                   group q by q.Complexity into g
        //                   select new { ComplexityName = g.Key, ComplexityCount = g.Count() };

        //            List<ChartData> chartDatum = new List<ChartData>();

        //            foreach (var complexity in complexityGroups)
        //            {
        //                ChartData chartData = new ChartData();

        //                chartData.ChartXValue = complexity.ComplexityName;

        //                chartData.ChartYValue = complexity.ComplexityCount;

        //                chartDatum.Add(chartData);
        //            }
        //            //Assign the values for the complexity chart
        //            SingleChartData singleChartData = new SingleChartData();

        //            singleChartData.ChartType = SeriesChartType.Pie;

        //            singleChartData.ChartData = chartDatum;

        //            singleChartData.ChartLength = 140;

        //            singleChartData.ChartWidth = 300;

        //            singleChartData.ChartTitle = "Complexity Statistics";

        //            LoadComplexityChart(singleChartData);

        //            //Get the test area count 
        //            var testAreaCounts =
        //                from t in questionDetail
        //                group t by t.TestAreaName into g
        //                select new { TestAreaName = g.Key, TestAreaCOunt = g.Count() };

        //            List<ChartData> testAreaChartDatum = new List<ChartData>();

        //            foreach (var testArea in testAreaCounts)
        //            {
        //                ChartData chartData = new ChartData();

        //                chartData.ChartXValue = testArea.TestAreaName;

        //                chartData.ChartYValue = testArea.TestAreaCOunt;

        //                testAreaChartDatum.Add(chartData);
        //            }

        //            //Assign the test area chart details
        //            SingleChartData singleTestChartData = new SingleChartData();

        //            singleTestChartData.ChartType = SeriesChartType.Pie;

        //            singleTestChartData.ChartData = testAreaChartDatum;

        //            singleTestChartData.ChartLength = 140;

        //            singleTestChartData.ChartWidth = 300;
        //            singleTestChartData.ChartTitle = "Test Area Statistics";

        //            LoadTestAreaChart(singleTestChartData);

        //        }
        //        else
        //        {
        //            ManualTestSummaryControl_noOfQuestionLabel.Text = "0.00";
        //            ManualTestSummaryControl_costOfTestLabel.Text = "0.00";
        //            ManualTestSummaryControl_avgComplexityLabel.Text = "--";
        //            ManualTestSummaryControl_avgTimeLabel.Text = "0.00";
        //        }
        //    }

        //}

        #endregion

        public TestDraftSummary TestDraftSummaryDataSource
        {
            set
            {
                if (value.QuestionDetail == null)
                    return;

                TestDraftSummary testDraftSummary= new TestDraftSummary();
                AttributeDetail complexityDetailsList = new AttributeDetail();
                List<QuestionDetail> questionDetailList = new List<QuestionDetail>();
                testDraftSummary = value;
                complexityDetailsList = testDraftSummary.AttributeDetail;
                questionDetailList = testDraftSummary.QuestionDetail;
                if (questionDetailList.Count > 0)
                {
                    List<decimal> complexityValue = new List<decimal>();
                    List<decimal> creditEarned = new List<decimal>();
                    List<decimal> timeTaken = new List<decimal>();
                    List<TestArea> testAreaList = new List<TestArea>();

                    for (int i = 0; i < questionDetailList.Count; i++)
                    {
                        complexityValue.Add(questionDetailList[i].ComplexityValue);
                        creditEarned.Add(questionDetailList[i].CreditsEarned);
                        timeTaken.Add(questionDetailList[i].AverageTimeTaken);
                    }
                    ManualTestSummaryControl_noOfQuestionLabel.Text = questionDetailList.Count.ToString();
                    ManualTestSummaryControl_costOfTestLabel.Text = creditEarned.Average().ToString("N2");
                    int avgtimeTaken = Convert.ToInt32(timeTaken.Average()) * questionDetailList.Count;
                    ManualTestSummaryControl_avgTimeLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds(avgtimeTaken);
                    ManualTestSummaryControl_avgComplexityLabel.Text = complexityDetailsList.AttributeName;
                    
                    // Hide Average Time Label for Interview


                    //Find the count of complexity 
                    var complexityGroups =
                           from q in questionDetailList
                           group q by q.Complexity into g
                           select new { ComplexityName = g.Key, ComplexityCount = g.Count() };

                    List<ChartData> chartDatum = new List<ChartData>();

                    foreach (var complexity in complexityGroups)
                    {
                        ChartData chartData = new ChartData();

                        chartData.ChartXValue = complexity.ComplexityName;

                        chartData.ChartYValue = complexity.ComplexityCount;

                        chartDatum.Add(chartData);
                    }
                    //Assign the values for the complexity chart
                    SingleChartData singleChartData = new SingleChartData();

                    singleChartData.ChartType = SeriesChartType.Pie;

                    singleChartData.ChartData = chartDatum;

                    singleChartData.ChartLength = 140;

                    singleChartData.ChartWidth = 300;

                    singleChartData.ChartTitle = "Complexity Statistics";

                    LoadComplexityChart(singleChartData);

                    //Get the test area count 
                    var testAreaCounts =
                        from t in questionDetailList
                        group t by t.TestAreaName into g
                        select new { TestAreaName = g.Key, TestAreaCOunt = g.Count() };

                    List<ChartData> testAreaChartDatum = new List<ChartData>();

                    foreach (var testArea in testAreaCounts)
                    {
                        ChartData chartData = new ChartData();

                        chartData.ChartXValue = testArea.TestAreaName;

                        chartData.ChartYValue = testArea.TestAreaCOunt;

                        testAreaChartDatum.Add(chartData);
                    }

                    //Assign the test area chart details
                    SingleChartData singleTestChartData = new SingleChartData();

                    singleTestChartData.ChartType = SeriesChartType.Pie;

                    singleTestChartData.ChartData = testAreaChartDatum;

                    singleTestChartData.ChartLength = 140;

                    singleTestChartData.ChartWidth = 300;
                    singleTestChartData.ChartTitle = "Test Area Statistics";

                    LoadTestAreaChart(singleTestChartData);

                }
                else
                {
                    ChartData chartData = new ChartData();
                    chartData.ChartXValue = null;
                    chartData.ChartYValue = 0;

                    List<ChartData> testAreaChartDatum = new List<ChartData>();

                    SingleChartData singleTestChartData = new SingleChartData();
                    singleTestChartData.ChartType = SeriesChartType.Pie;
                    singleTestChartData.ChartLength = 140;

                    singleTestChartData.ChartWidth = 300;
                    singleTestChartData.ChartTitle = "Test Area Statistics";
                    testAreaChartDatum.Add(chartData);
                    singleTestChartData.ChartData = testAreaChartDatum;


                    ManualTestSummaryControl_noOfQuestionLabel.Text = "0.00";
                    ManualTestSummaryControl_costOfTestLabel.Text = "0.00";
                    ManualTestSummaryControl_avgComplexityLabel.Text = "--";
                    ManualTestSummaryControl_avgTimeLabel.Text = "00:00:00";
                    LoadTestAreaChart(singleTestChartData);
                    LoadComplexityChart(singleTestChartData);
                }
            }
            
        }

        public bool AverageTimeTakenByCandidatesLabel
        {
            get { return ManualTestSummaryControl_avgTimeHeadLabel.Visible; }
            set { ManualTestSummaryControl_avgTimeHeadLabel.Visible = value; }
        }

        public bool AverageTimeTakenByCandidatesText
        {
            get { return ManualTestSummaryControl_avgTimeLabel.Visible; }
            set { ManualTestSummaryControl_avgTimeLabel.Visible = value; }
        }

        public string SummaryType
        {
            set
            {
                if (value != null && value.Trim().ToUpper() == "I")
                {
                    ManualTestSummaryControl_testSummaryLiteral.Text = "Interview Summary";
                    ManualTestSummaryControl_testAreaLiteral.Text = "Interview Area Statistics";
                    ManualTestSummaryControl_costOfTestHeadLabel.Text = "Cost Of Interview (in terms of credits)";
                }
                else
                {
                    ManualTestSummaryControl_testSummaryLiteral.Text = "Test Summary";
                    ManualTestSummaryControl_testAreaLiteral.Text = "Test Area Statistics";
                    ManualTestSummaryControl_costOfTestHeadLabel.Text = "Cost Of Test (in terms of credits)";
                }
            }
        }
    }
}