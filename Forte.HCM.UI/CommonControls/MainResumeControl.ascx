﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainResumeControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.MainResumeControl" %>
<%@ Register Src="NameControl.ascx" TagName="NameControl" TagPrefix="uc1" %>
<%@ Register Src="ContactInformationControl.ascx" TagName="ContactInformationControl"
    TagPrefix="uc2" %>
<%@ Register Src="ExecutiveSummaryControl.ascx" TagName="ExecutiveSummaryControl"
    TagPrefix="uc3" %>
<%@ Register Src="TechnicalSkillsControl.ascx" TagName="TechnicalSkillsControl" TagPrefix="uc4" %>
<%@ Register Src="ProjectsControl.ascx" TagName="ProjectsControl" TagPrefix="uc5" %>
<%@ Register Src="EducationControl.ascx" TagName="EducationControl" TagPrefix="uc6" %>
<%@ Register Src="ReferencesControl.ascx" TagName="ReferencesControl" TagPrefix="uc8" %>

<div>
    <uc1:NameControl ID="MainResumeControl_nameControl" runat="server" />
    
</div>
<div>
    <uc2:ContactInformationControl ID="MainResumeControl_contactInfoControl" runat="server" />
    
</div>
<div>
    <uc3:ExecutiveSummaryControl ID="MainResumeControl_executiveSummaryControl" runat="server" />
   
</div>
<div>
    <uc4:TechnicalSkillsControl ID="MainResumeControl_technicalSkillsControl" runat="server" />

</div>
<div>
    <uc5:ProjectsControl ID="MainResumeControl_projectsControl" runat="server" />
   
</div>
<div>
    <uc6:EducationControl ID="MainResumeControl_educationControl" runat="server"/>
   
</div>
<div>
    <uc8:ReferencesControl ID="MainResumeControl_referencesControl" runat="server" />
    
</div>
