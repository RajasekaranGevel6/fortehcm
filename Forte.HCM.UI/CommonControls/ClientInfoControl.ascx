﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientInfoControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ClientInfoControl" %>
    <%@ Register Src="~/CommonControls/MultiSelectControl.ascx" TagName="MultiSelectControl"
    TagPrefix="uc1" %>
<script language="javascript" type="text/javascript">
    function ShowLoadingImage() {
        document.getElementById("<%=ClientInfoControl_clientNameTextBox.ClientID%>").className = "position_profile_client_bg";
    }
    function HideLoadingImage() {
        document.getElementById("<%=ClientInfoControl_clientNameTextBox.ClientID%>").className = "";
    }
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="header_bg">
            <table width="100%" border="0" cellspacing="3" cellpadding="3">
                <tr>
                    <td width="35%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="grid_header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" class="header_text_bold" width="25%">
                                                <asp:Literal ID="ClientInfoControl_clientLiteral" runat="server" Text="Client"></asp:Literal>
                                                <asp:HiddenField ID="ClientInfoControl_clientID" runat="server" />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td width="50%">
                                                <asp:UpdatePanel ID="ClientInfoControl_clientUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="ClientInfoControl_clientNameTextBox" runat="server" MaxLength="50"
                                                            Width="252px" AutoPostBack="true" 
                                                            OnTextChanged="ClientInfoControl_clientNameTextBox_TextChanged"></asp:TextBox>
                                                        <ajaxToolKit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender" runat="server"
                                                            TargetControlID="ClientInfoControl_clientNameTextBox" WatermarkText="Enter client name"
                                                            WatermarkCssClass="position_profile_client_water">
                                                        </ajaxToolKit:TextBoxWatermarkExtender>
                                                        <div id="ClientInfoControl_showClientList" style="height: 100px;width:250px;">
                                                        </div>
                                                        <ajaxToolKit:AutoCompleteExtender ID="ClientInfoControl_clientNameAutoCompleteExtender"
                                                            BehaviorID="ClientInfoControl_clientNameBehaviorID" runat="server" ServicePath="~/AutoComplete.asmx"
                                                            ServiceMethod="GetClientList" TargetControlID="ClientInfoControl_clientNameTextBox"
                                                            MinimumPrefixLength="1" CompletionListElementID="ClientInfoControl_showClientList"
                                                            OnClientPopulating="ShowLoadingImage" CompletionListCssClass="position_profile_client_completion_list" 
                                                            CompletionListItemCssClass="position_profile_client_completion_list_item"
                                                            OnClientPopulated="HideLoadingImage" OnClientShowing="ShowLoadingImage" OnClientHiding="HideLoadingImage"
                                                            CompletionListHighlightedItemCssClass="position_profile_client_completion_list_highlight"
                                                            EnableCaching="true" CompletionSetCount="12">
                                                        </ajaxToolKit:AutoCompleteExtender>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td align="right" width="25%">
                                                <asp:ImageButton ID="ClientInfoControl_addclientImageButton" runat="server" SkinID="sknAddClientImageButton"
                                                    OnCommand="ClientInfoControl_addImageButton_Command" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr height="20px">
                                <td class="grid_body_bg" valign="top">
                                    <div style="width: 100%; height: 17px; float: left;">
                                        <asp:DataList ID="ClientInfoControl_clientDataList" runat="server" RepeatColumns="2"
                                            RepeatDirection="Vertical" Width="100%" CaptionAlign="Top" OnDeleteCommand="ClientInfoControl_clientDataList_DeleteCommand">
                                            <ItemTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 20px;" class="td_padding_top_2">
                                                            <asp:ImageButton ID="ClientInfoControl_removeImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                ToolTip="Remove Client" CommandName="Delete" CommandArgument='<%# Bind("ClientID") %>' />
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="ClientInfoControl_categoryNameLabel" runat="server" SkinID="sknLabelText"
                                                                Text='<%# Bind("ClientName") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="grid_header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ClientInfoControl_departmentHeaderLiteral" runat="server" Text="Department"></asp:Literal>
                                            </td>
                                            <td align="right">
                                                <asp:ImageButton ID="ClientInfoControl_AddDepartment_ImageButton" runat="server"
                                                    SkinID="sknAddDepartmentImageButton" OnCommand="ClientInfoControl_addImageButton_Command"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" class="grid_body_bg">
                              <%--  <uc1:MultiSelectControl ID="ClientInfoControl_departmentMultiSelectControl" runat="server" />--%>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="ClientInfoControl_departmentComboTextBox" ReadOnly="true" runat="server"
                                                    Width="98%" Font-Size="X-Small"></asp:TextBox>
                                                <asp:Panel ID="ClientInfoControl_departmentPanel" runat="server" CssClass="popupControl_positionProfile">
                                                    <asp:CheckBoxList ID="ClientInfoControl_departmentCheckBoxList" RepeatColumns="1"
                                                        runat="server" CellPadding="0" OnSelectedIndexChanged="ClientInfoControl_departmentCheckBoxList_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                                <ajaxToolKit:PopupControlExtender ID="ClientInfoControl_departmentListPopupControlExtender"
                                                    runat="server" TargetControlID="ClientInfoControl_departmentComboTextBox" PopupControlID="ClientInfoControl_departmentPanel"
                                                    OffsetX="2" OffsetY="2" Position="Bottom">
                                                </ajaxToolKit:PopupControlExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="grid_header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ClientInfoControl_departmentcontactHeaderLiteral" runat="server"
                                                    Text="Contact"></asp:Literal>
                                            </td>
                                            <td align="right">
                                                <asp:ImageButton ID="ClientInfoControl_AddContact_ImageButton" runat="server" SkinID="sknAddContactImageButton"
                                                    OnCommand="ClientInfoControl_addImageButton_Command"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" class="grid_body_bg">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="ClientInfoControl_departmentContactComboTextbox" runat="server"
                                                    Width="98%" Font-Size="X-Small"></asp:TextBox>
                                                <asp:Panel ID="ClientInfoControl_departmentContactPanel" runat="server" CssClass="popupControl_positionProfile">
                                                    <asp:CheckBoxList ID="ClientInfoControl_departmentContactPanelCheckBoxList" RepeatColumns="1"
                                                        runat="server" CellPadding="0" OnSelectedIndexChanged="ClientInfoControl_departmentContactPanelCheckBoxList_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                                <ajaxToolKit:PopupControlExtender ID="ClientInfoControl_departmentContactPanelPopupControlExtender"
                                                    runat="server" TargetControlID="ClientInfoControl_departmentContactComboTextbox"
                                                    PopupControlID="ClientInfoControl_departmentContactPanel" OffsetX="2" OffsetY="2"
                                                    Position="Bottom">
                                                </ajaxToolKit:PopupControlExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:HiddenField ID="ClientInfoControl_categoryIdToBeDeleted" runat="server" />
            <asp:HiddenField ID="ClientInfoControl_departmentID_hiddenField" runat="server" />
             <asp:HiddenField ID="ClientInfoControl_clientNameHiddenField" runat="server" />
             <asp:HiddenField ID="ClientInfoControl_clientIDHiddenField" runat="server" />
        </td>
    </tr>
</table>
<%--<asp:UpdatePanel ID="ClientInfoControl_clientModalPopUpUpdatePanel" runat="server"
    UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="ClientInfoControl_clientIDHiddenField1" runat="server" Value="0" />
        <span style="display: none">
            <asp:Button ID="ClientInfoControl_clientDummybutton" runat="server" />
        </span>
        <asp:Panel ID="ClientInfoControl_clientPaenl" runat="server" CssClass="client_details">
            <uc1:Client ID="ClientInfoControl_clientControl" runat="server" />
        </asp:Panel>
        <ajaxToolKit:ModalPopupExtender ID="ClientInfoControl_clientModalpPopupExtender"
            runat="server" TargetControlID="ClientInfoControl_clientDummybutton" PopupControlID="ClientInfoControl_clientPaenl"
            BackgroundCssClass="modalBackground">
        </ajaxToolKit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="ClientInfoControl_departmentModalPopUpUpdatePanel" runat="server"
    UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="ClientInfoControl_departIDHiddenField" runat="server" Value="0" />
        <span style="display: none">
            <asp:Button ID="ClientInfoControl_departmentDummybutton" runat="server" />
        </span>
        <asp:Panel ID="ClientInfoControl_departmentPaenl" runat="server" CssClass="client_details">
            <uc4:Department ID="ClientInfoControl_departmentControl" runat="server" />
        </asp:Panel>
        <ajaxToolKit:ModalPopupExtender ID="ClientInfoControl_departmentModalpPopupExtender"
            runat="server" TargetControlID="ClientInfoControl_departmentDummybutton" PopupControlID="ClientInfoControl_departmentPaenl"
            BackgroundCssClass="modalBackground">
        </ajaxToolKit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="ClientInfoControl_contactModalPopUpUpdatePanel" runat="server"
    UpdateMode="Conditional">
    <ContentTemplate>
        <span style="display: none">
            <asp:Button ID="ClientInfoControl_contactDummybutton" runat="server" />
        </span>
        <asp:Panel ID="ClientInfoControl_contactPaenl" runat="server" CssClass="contact_details">
            <uc5:Contact ID="ClientInfoControl_contactControl" runat="server" />
        </asp:Panel>
        <ajaxToolKit:ModalPopupExtender ID="ClientInfoControl_contactModalpPopupExtender"
            runat="server" TargetControlID="ClientInfoControl_contactDummybutton" PopupControlID="ClientInfoControl_contactPaenl"
            BackgroundCssClass="modalBackground">
        </ajaxToolKit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>--%>
