﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class HomeHeaderControl {
        
        /// <summary>
        /// HomePage_logoImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image HomePage_logoImage;
        
        /// <summary>
        /// HomeHeaderControl_loginTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable HomeHeaderControl_loginTable;
        
        /// <summary>
        /// HomePage_userNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label HomePage_userNameLabel;
        
        /// <summary>
        /// HomePage_userNametextbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox HomePage_userNametextbox;
        
        /// <summary>
        /// HomePage_passwordLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label HomePage_passwordLabel;
        
        /// <summary>
        /// HomePage_passwordtextbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox HomePage_passwordtextbox;
        
        /// <summary>
        /// HomePage_goImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton HomePage_goImageButton;
        
        /// <summary>
        /// HomePage_rememberMeCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox HomePage_rememberMeCheckBox;
        
        /// <summary>
        /// HomePage_forgotPasswordLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton HomePage_forgotPasswordLinkButton;
        
        /// <summary>
        /// HomeHeaderControl_loggedInTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable HomeHeaderControl_loggedInTable;
        
        /// <summary>
        /// HomeHeaderControl_loginUserNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label HomeHeaderControl_loginUserNameLabel;
        
        /// <summary>
        /// HomeHeaderControl_lastLoginDateTimeLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label HomeHeaderControl_lastLoginDateTimeLabel;
        
        /// <summary>
        /// HeaderControl_logoutButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button HeaderControl_logoutButton;
        
        /// <summary>
        /// HeaderControl_changePasswordLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton HeaderControl_changePasswordLinkButton;
        
        /// <summary>
        /// HomeHeaderControl_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label HomeHeaderControl_bottomErrorMessageLabel;
        
        /// <summary>
        /// SubscriptionMaster_menu control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Menu SubscriptionMaster_menu;
        
        /// <summary>
        /// HomePage_signUpImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton HomePage_signUpImage;
        
        /// <summary>
        /// HomeHeaderControl_forgetPasswordPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel HomeHeaderControl_forgetPasswordPanel;
        
        /// <summary>
        /// HomeHeaderControl_forgotPassword control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ForgotPassword HomeHeaderControl_forgotPassword;
        
        /// <summary>
        /// HomeHeaderControl_forgotPassword_ModalpPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender HomeHeaderControl_forgotPassword_ModalpPopupExtender;
        
        /// <summary>
        /// HomeHeaderControl_changePasswordPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel HomeHeaderControl_changePasswordPanel;
        
        /// <summary>
        /// HomeHeaderControl_changePasswordControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ChangePassword HomeHeaderControl_changePasswordControl;
        
        /// <summary>
        /// HomeHeaderControl_changePassword_ModalpPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender HomeHeaderControl_changePassword_ModalpPopupExtender;
    }
}
