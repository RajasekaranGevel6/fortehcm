﻿#region Namespace                                                              

using System;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using System.Web.UI;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

using Resources;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class HomeHeaderControl : UserControl
    {
        #region Variables                                                       

        public string homeUrl = string.Empty;
        public string landingUrl = string.Empty;
        public string userOptionsUrl = string.Empty;
        public string helpUrl = string.Empty;

        #endregion Variables

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //HeaderControl_logoutButton.Attributes.Add("onclick", "javascript:return OnLogoutClick()");

                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 3)
                {
                    // Compose home url.
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Default.aspx";

                    // Compose dashboard url.
                    landingUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Landing.aspx";

                    // Compose user options url.
                    userOptionsUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Settings/UserOptions.aspx";

                    // Compose help url.
                    helpUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Help.aspx";
                }

                Page.Form.DefaultButton = HomePage_goImageButton.UniqueID;
                Page.Form.DefaultFocus = HomePage_userNametextbox.UniqueID;
                string PageName = Request.Url.AbsoluteUri.Split('/')[Request.Url.AbsoluteUri.Split('/').Length - 1];
                if (PageName.Contains("?"))
                    PageName = PageName.Substring(0, PageName.IndexOf('?'));
                if (PageName == "Default.aspx")
                    SubscriptionMaster_menu.Visible = true;
                else
                    SubscriptionMaster_menu.Visible = false;

                // TODO - This needs to reviewed.
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    HeaderControl_changePasswordLinkButton.Visible = true;
                }
                else
                {
                    // Hide the options menu.
                    if (SubscriptionMaster_menu.Items.Count > 5)
                        SubscriptionMaster_menu.Items.RemoveAt(5);
                    HeaderControl_changePasswordLinkButton.Visible = false;
                }

                //if (IsPostBack)
                //    return;
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    HomeHeaderControl_loggedInTable.Visible = true;
                    HomeHeaderControl_loginTable.Visible = false;
                    HomePage_signUpImage.Visible = false;
                    //HomeHeaderControl_logoLoginTD.Visible = false;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HomeHeaderControl_loginUserNameLabel.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                    HomeHeaderControl_lastLoginDateTimeLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
                }
                else
                {
                    HomeHeaderControl_loggedInTable.Visible = false;
                    HomeHeaderControl_loginTable.Visible = true;
                    HomePage_signUpImage.Visible = true;
                    //HomeHeaderControl_logoLoginTD.Visible = true;
                    //6FormsAuthentication.RedirectToLoginPage();
                }

                // Check if login is requested from web site.
                if (!Utility.IsNullOrEmpty(Request.QueryString["sitesessionkey"]))
                    AuthenticateSessionKey();

                // Retrieve cookies.
                RetrieveCookies();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void HomeHeaderControl_logoutButton_Click(object sender, EventArgs e)
        {
            // Clear user details from session.
            Session["USER_DETAIL"] = null;

            // Clear cookies.
            try
            {
                Response.Cookies["FORTEHCM_USER_DETAILS"].Expires.AddDays(-1);
                //Request.Cookies.Remove("FORTEHCM_USER_DETAILS");
                //Response.Cookies.Remove("FORTEHCM_USER_DETAILS");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }

            Session.Abandon();
            HomeHeaderControl_loginUserNameLabel.Text = "Guest";
            FormsAuthentication.SignOut();
            Session["LOGOUT"] = true;
            //Response.Redirect("~/Home.aspx?a=1", false);
            // FormsAuthentication.RedirectToLoginPage();
            // Response.Redirect(ConfigurationManager.AppSettings["SITE_URL"], false);
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if (uri.Length > 4)
            {
                // Compose home url.

                Response.Redirect(uri[0] + "/" + uri[1] + System.Configuration.ConfigurationManager.AppSettings["SITE_URL"], false);
            }
        }

        protected void HomePage_signUpImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(@"~/Subscription/SubscriptionType.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void HomePage_goImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Try login.
                TryLogin();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Public Methods                                                 

        public List<string> BreadCrumbText
        {
            set
            {
                List<string> breadCrumbList = (List<string>)value;

                if (breadCrumbList != null)
                {
                    foreach (string breadCrumbText in breadCrumbList)
                    {
                        //if (string.IsNullOrEmpty(HomeHeaderControl_breadCrumbLabel.Text.Trim()))
                        //{
                        //    HomeHeaderControl_breadCrumbLabel.Text = breadCrumbText;
                        //}
                        //else
                        //{
                        //    HomeHeaderControl_breadCrumbLabel.Text += " &raquo; " + breadCrumbText;
                        //}
                    }
                }
            }
        }

        #endregion Public Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that authenticates the user based on the session key.
        /// </summary>
        private void AuthenticateSessionKey()
        {
            // Check if not post back.
            if (IsPostBack)
                return;

            UserDetail userDetail = null;
            try
            {
                // Get the user detail based on session key.
                userDetail = new AuthenticationBLManager().GetAuthenticateUser
                    (Request.QueryString["sitesessionkey"]);
            }
            catch (Exception exp)
            {
                // An exception will be raised when the guid is in invalid format.
                Logger.ExceptionLog(exp);

                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                return;
            }

            // Check if user is valid or not.
            if (userDetail == null || userDetail.UserID == 0)
            {
                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                return;
            }

            // Check if the user is a candidate.
            if (userDetail.Roles != null && userDetail.Roles.Exists(item => item == UserRole.Candidate))
            {
                // Do not allow non-candidate users to access the application.
                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);

                return;
            }

            // Keep the user detail in session.
            Session["USER_DETAIL"] = userDetail;

            if (Request.Params["ReturnUrl"] != null)
            {
                FormsAuthentication.RedirectFromLoginPage(HomePage_userNametextbox.Text.Trim(), false);
            }
            else
            {
                FormsAuthentication.SetAuthCookie(HomePage_userNametextbox.Text.Trim(), false);
                Response.Redirect("~/Landing.aspx", false);
            }
        }

        /// <summary>
        /// Method that retreives the user name and password form cookie and 
        /// tries to login using that.
        /// </summary>
        private void RetrieveCookies()
        {
            try
            {
                // Check if not post back and user already logged in.
                if (IsPostBack || !Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                    return;
                    
                // Try to get cookie information.
                HttpCookie cookie = Request.Cookies["FORTEHCM_USER_DETAILS"];

                if (cookie != null)
                {
                    string userName = cookie["FHCM_USER_NAME"];
                    string password = cookie["FHCM_PASSWORD"];

                    if (!Utility.IsNullOrEmpty(userName) && !Utility.IsNullOrEmpty(password))
                    {
                        // Assign user name and password.
                        HomePage_userNametextbox.Text = userName;
                        HomePage_passwordtextbox.Text = password;

                        // Try to login.
                        TryLogin();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that try login to the system using the given user name and 
        /// password.
        /// </summary>
        private void TryLogin()
        {
            UserDetail userDetails = null;

            if (this.IsValidData())
            {
                userDetails = AuthenticateUserDetails(HomePage_userNametextbox.Text.Trim(),
                    HomePage_passwordtextbox.Text.Trim());

                //if (Utility.IsNullOrEmpty(userDetails.Roles))
                if (Utility.IsNullOrEmpty(userDetails) || userDetails.UserID == 0)
                {
                    ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                    return;
                }

                // Check if the user is a candidate.
                if (userDetails.Roles != null && userDetails.Roles.Exists(item => item == UserRole.Candidate))
                {
                    // Do not allow non-candidate users to access the application.
                    ShowErrorMessage(HCMResource.Login_InvalidUserLogin);

                    return;
                }

                // Keep the user detail in session.
                Session["USER_DETAIL"] = userDetails;
               
                // Try to add the cookie information.
                if (HomePage_rememberMeCheckBox.Checked)
                {
                    try
                    {
                        HttpCookie cookie = Request.Cookies["FORTEHCM_USER_DETAILS"];
                        if (cookie == null)
                        {
                            cookie = new HttpCookie("FORTEHCM_USER_DETAILS");
                        }

                        cookie["FHCM_USER_NAME"] = HomePage_userNametextbox.Text;
                        cookie["FHCM_PASSWORD"] = HomePage_passwordtextbox.Text;

                        cookie.Expires = DateTime.Now.AddMonths
                            (Convert.ToInt32(ConfigurationManager.AppSettings["REMEMBER_ME_MONTHS"]));
                        Response.AppendCookie(cookie);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }
                }
               
                if (Request.Params["ReturnUrl"] != null)
                {
                    FormsAuthentication.RedirectFromLoginPage(HomePage_userNametextbox.Text.Trim(), false);
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(HomePage_userNametextbox.Text.Trim(), false);
                    Response.Redirect("~/Landing.aspx", false);
                }
            }
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message to be 
        /// show to the user.
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            if (string.IsNullOrEmpty(HomeHeaderControl_bottomErrorMessageLabel.Text))
                HomeHeaderControl_bottomErrorMessageLabel.Text = Message;
            else
                HomeHeaderControl_bottomErrorMessageLabel.Text += "<br />" + Message;
        }

        /// <summary>
        /// Shows the error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        /// <summary>
        /// Method that will return a userdetail object for the 
        /// given username and password.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the username.
        /// </param>
        /// <param name="Password">
        /// A <see cref="string"/> that contains the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> object that contains the 
        /// information for the given username and password.
        /// </returns>
        private UserDetail AuthenticateUserDetails(string UserName, string Password)
        {
            AuthenticationBLManager authenticationBLManager = new AuthenticationBLManager();
            Authenticate authenticate = new Authenticate();
            authenticate.UserName = UserName;
            authenticate.Password = new EncryptAndDecrypt().EncryptString(Password.Trim());
            UserDetail userDetails = new UserDetail();
            userDetails = authenticationBLManager.GetAuthenticateUser(authenticate);
            return userDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsValidData()
        {
            bool isValidData = true;
            if (HomePage_userNametextbox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_UserNameTextBoxEmpty);
                isValidData = false;
            }
            if (HomePage_passwordtextbox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_PasswordTextBoxEmpty);
                isValidData = false;
            }
            return isValidData;
        }

      

        #endregion Private Methods

        internal void ShowChangePasswordModelPopup()
        {
            HomeHeaderControl_changePassword_ModalpPopupExtender.Show();
        }
    }
}