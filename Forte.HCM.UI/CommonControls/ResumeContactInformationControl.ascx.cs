﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ResumeContactInformationControl.cs
// File that represents the user interface for the contact information details

#endregion Header
#region Directives

using System;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ResumeContactInformationControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public ContactInformation DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                ResumeContactInformationControl_firstNameTextBox.Text = value.FirstName;
                ResumeContactInformationControl_middleNameTextBox.Text = value.MiddleName;
                ResumeContactInformationControl_lastNameTextBox.Text = value.LastName;
                ResumeContactInformationControl_cityTextBox.Text = value.City;
                ResumeContactInformationControl_stateTextBox.Text = value.State;
                ResumeContactInformationControl_countryNameTextBox.Text = value.Country;
                ResumeContactInformationControl_emailTextBox.Text = value.EmailAddress;
                ResumeContactInformationControl_fixedLineTextBox.Text = value.Phone.Residence;
                ResumeContactInformationControl_mobileTextBox.Text = value.Phone.Mobile;
                ResumeContactInformationControl_postalCodeTextBox.Text = value.PostalCode;
                ResumeContactInformationControl_streetTextBox.Text = value.StreetAddress;
                ResumeContactInformationControl_websiteTextBox.Text = value.WebSiteAddress.Personal;
            }
        }
    }
}