﻿using System;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class InterviewSessionPreviewControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Do Nothing
        }
        /// <summary>
        /// This property assigns the test session details to labels.
        /// </summary>
        public InterviewSessionDetail DataSource
        {
            set
            {
                if (value == null)
                    return;

                InterviewSessionPreviewControl_testKeyLabel.Text = value.InterviewTestID;
                InterviewSessionPreviewControl_testNameLabel.Text = value.InterviewTestName;
                InterviewSessionPreviewControl_sessionKeyLabel.Text = value.InterviewTestSessionID;
                InterviewSessionPreviewControl_sessionNoLabel.Text = value.NumberOfCandidateSessions.ToString();
                InterviewSessionPreviewControl_sessionDescLiteral.Text = value.InterviewSessionDesc == null ? value.InterviewSessionDesc : 
                    value.InterviewSessionDesc.ToString().Replace(Environment.NewLine, "<br />");

                InterviewSessionPreviewControl_positionProfileLabel.Text = value.PositionProfileName;

                InterviewSessionPreviewControl_expiryDateLabel.Text = GetDateFormat(Convert.ToDateTime(value.ExpiryDate.ToString()));

                InterviewSessionPreviewControl_instructionsLiteral.Text = 
                    value.Instructions == null ? value.Instructions : value.Instructions.ToString().Replace(Environment.NewLine, "<br />");
            }
        }
        /// <summary>
        /// This method returns the date in MM/dd/yyyy format. If date value is not given,
        /// then it returns empty.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }
        /// <summary>
        /// This property stores either edit or view.
        /// </summary>
        private string mode;

        public string Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
            }
        }
    }
}