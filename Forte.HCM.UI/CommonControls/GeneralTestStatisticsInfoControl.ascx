﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralTestStatisticsInfoControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GeneralTestStatisticsInfoControl" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc1" %>
<div>
    <asp:UpdatePanel ID="GeneralTestStatisticsInfoControl_updatePanel" runat="server"
        UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="3" border="0">
                            <tr>
                                <td align="left">
                                    <div style="width: 100%; overflow: auto">
                                        <uc1:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" 
                                        OnselectedIndexChanged="GeneralTestStatisticsInfoControl_selectProperty" 
                                        OncancelClick="WidgetMultiSelectControl_cancelClick" OnClick="WidgetMultiSelectControl_Click"/>
                                        <uc1:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                    </div>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <asp:DataList ID="GeneralTestStatisticsInfoControl_testInformationDataList" runat="server"
                                RepeatLayout="Table" RepeatColumns="2" RepeatDirection="Horizontal">
                                <HeaderTemplate>
                                    <tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <td style="width: 35%;height:15px">
                                        <asp:Label ID="GeneralTestStatisticsInfoControl_textField" runat="server" Text='<%# Eval("DisplayText") %>'
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 15%">
                                    </td>
                                    <td style="width: 20%; height:15px">
                                        <asp:Label ID="GeneralTestStatisticsInfoControl_valueField" runat="server" SkinID="sknLabelFieldText"
                                            Text='<%# Eval("ValueText") %>'></asp:Label>
                                    </td>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tr>
                                </FooterTemplate>
                            </asp:DataList>
                            <%--  <tr>
                                <td style="width: 35%">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_noOfQuestionLabel" runat="server"
                                        Text="Number Of Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right" style="width: 10%">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_noOfQuestionValueLabel" runat="server"
                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                </td>
                                <td style="width: 15%">
                                </td>
                                <td>
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_meanScoreHeadLabel" runat="server"
                                        Text="Mean Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right" style="width: 10%">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_meanScoreValueLabel" runat="server"
                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_scoreSDHeadLabel" runat="server"
                                        Text="Standard Deviation" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right" style="width: 10%">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_scoreSDValueLabel" runat="server"
                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                </td>
                                <td style="width: 15%">
                                </td>
                                <td>
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_scoreRangeHeadLabel" runat="server"
                                        Text="Score Range" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right" style="width: 10%">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_scoreRangeValueLabel" runat="server"
                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_highScoreHeadLabel" runat="server"
                                        Text="Highest Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right" style="width: 10%">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_highScoreValueLabel" runat="server"
                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                </td>
                                <td style="width: 15%">
                                </td>
                                <td>
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_lowScoreHeadLabel" runat="server"
                                        Text="Lowest Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right" style="width: 10%">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_lowScoreValueLabel" runat="server"
                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_avgTimeLabel" runat="server" Text="Average Time "
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Label ID="GeneralTestStatisticsInfoControl_avgTimeValueLabel" runat="server"
                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                </td>
                                <td style="width: 15%">
                                </td>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                                <td>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="td_height_2">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_padding_10" colspan="2">
                                    <div id="GeneralTestStatisticsInfoControl_categoryDIV" runat="server" style="width: 355px; height: 200px; float: left; overflow: auto">
                                        <asp:GridView ID="GeneralTestStatisticsInfoControl_categoryDataGrid" runat="server"
                                            AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:BoundField HeaderText="Category" DataField="CategoryName" />
                                                <asp:BoundField HeaderText="Subject" DataField="SubjectName" />
                                                <asp:BoundField HeaderText="Question Count" DataField="QuestionCount"/>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
