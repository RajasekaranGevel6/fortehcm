﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class GeneralTestStatisticsControl {
        
        /// <summary>
        /// GeneralTestStatisticsControl_searchCriteriasDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl GeneralTestStatisticsControl_searchCriteriasDiv;
        
        /// <summary>
        /// GeneralTestStatisticsControl_testAuthorHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_testAuthorHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_testAuthorLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_testAuthorLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_highScoreHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_highScoreHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_highScoreLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_highScoreLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_noOfQuestionHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_noOfQuestionHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_noOfQuestionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_noOfQuestionLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_lowScoreHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_lowScoreHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_lowScoreLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_lowScoreLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_noOfCandidateHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_noOfCandidateHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_noOfCandidateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_noOfCandidateLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_meanScoreHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_meanScoreHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_meanScoreLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_meanScoreLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_avgTimeHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_avgTimeHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_avgTimeLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_avgTimeLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_scoreSDHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_scoreSDHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_scoreSDLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_scoreSDLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_scoreRangeHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_scoreRangeHeadLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_scoreRangeLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label GeneralTestStatisticsControl_scoreRangeLabel;
        
        /// <summary>
        /// GeneralTestStatisticsControl_categoryGridview control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GeneralTestStatisticsControl_categoryGridview;
        
        /// <summary>
        /// GeneralTestStatistics_categoryStatisticsChartControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.SingleSeriesChartControl GeneralTestStatistics_categoryStatisticsChartControl;
        
        /// <summary>
        /// GeneralTestStatisticsControl_subjectChartStatisticsChartControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.SingleSeriesChartControl GeneralTestStatisticsControl_subjectChartStatisticsChartControl;
        
        /// <summary>
        /// GeneralTestStatisticsControl_testAreaStatistics control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.SingleSeriesChartControl GeneralTestStatisticsControl_testAreaStatistics;
        
        /// <summary>
        /// GeneralTestStatisticsControl_complexityStatisticsChart control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.SingleSeriesChartControl GeneralTestStatisticsControl_complexityStatisticsChart;
    }
}
