﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

namespace Forte.HCM.UI.CommonControls
{
    public partial class CandidateActivityViewerControl : UserControl
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Events & Delegates

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;

        #endregion Events & Delegates

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the control is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default focus.
                CandidateActivityViewerControl_commentsTextBox.Focus();

                // Check and set expanded or restore state.
                CheckAndSetExpandorRestore();

                // Set default button.
                Page.Form.DefaultButton = CandidateActivityViewerControl_searchButton.UniqueID;

                // Add handler to page navigator.
                CandidateActivityViewerControl_pageNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                   (CandidateActivityViewerControl_pageNavigator_PageNumberClick);

                CandidateActivityViewerControl_gridViewHeaderTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                     CandidateActivityViewerControl_activityLogDetailDIV.ClientID + "','" +
                     CandidateActivityViewerControl_searchDIV.ClientID + "','" +
                     CandidateActivityViewerControl_searchResultsUpSpan.ClientID + "','" +
                     CandidateActivityViewerControl_searchResultsDownSpan.ClientID + "','" +
                     CandidateActivityViewerControl_restoreHiddenField.ClientID + "','" +
                     RESTORED_HEIGHT + "','" +
                     EXPANDED_HEIGHT + "')");

                // Add handler to load candidate popup.
                CandidateActivityViewerControl_candidateImageButton.Attributes.Add("onclick",
                    "return LoadCandidateInfo('" + CandidateActivityViewerControl_candidateIDHiddenField.ClientID + "','"
                    + CandidateActivityViewerControl_candidateTextBox.ClientID + "')");

                // Add handler to load user popup.
                CandidateActivityViewerControl_activityByImageButton.Attributes.Add("onclick",
                     "javascript:return LoadUserForActivityLog('"
                    + CandidateActivityViewerControl_activityByHiddenField.ClientID + "','"
                    + CandidateActivityViewerControl_activityByTextBox.ClientID + "')");

                if (!IsPostBack)
                {
                    List<AttributeDetail> attributes = new AttributeBLManager().
                            GetAttributesByType(Constants.AttributeTypes.CANDIDATE_ACTIVITY, "A");
                    CandidateActivityViewerControl_activityTypeDropDownList.DataSource = attributes;
                    CandidateActivityViewerControl_activityTypeDropDownList.DataBind();
                    CandidateActivityViewerControl_activityTypeDropDownList.DataTextField = "AttributeName";
                    CandidateActivityViewerControl_activityTypeDropDownList.DataValueField = "AttributeID";
                    CandidateActivityViewerControl_activityTypeDropDownList.DataBind();
                    CandidateActivityViewerControl_activityTypeDropDownList.Items.Insert
                        (0, new ListItem("--Select--", string.Empty));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        protected void CandidateActivityViewerControl_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ThrowMessage(string.Empty);

                // Check if candidate ID is present.
                if (this.CandidateID == 0)
                {
                    ThrowMessage("No candidate was selected");
                    return;
                }

                ViewState["SORT_FIELD"] = "ACTIVITY_DATE";
                ViewState["SORT_ORDER"] = SortType.Descending;
                LoadActivities(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        private void CandidateActivityViewerControl_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load values.
                LoadActivities(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        protected void CandidateActivityViewerControl_activityLogDetailGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                CandidateActivityViewerControl_pageNavigator.Reset();
                LoadActivities(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        protected void CandidateActivityViewerControl_activityLogDetailGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = new ControlUtility().GetSortColumnIndex
                    (CandidateActivityViewerControl_activityLogDetailGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    new ControlUtility().AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
            }
        }

        #endregion Event Handlers

        #region Properties

        public int CandidateID
        {
            set
            {
                CandidateActivityViewerControl_candidateIDHiddenField.Value = value.ToString();

                // Check if candidate ID is not empty.
                if (!Utility.IsNullOrEmpty(CandidateActivityViewerControl_candidateIDHiddenField.Value))
                {
                    int candidateID = 0;
                    if (int.TryParse(CandidateActivityViewerControl_candidateIDHiddenField.Value.Trim(), out candidateID) == true)
                    {
                        // Get candidate information.
                        CandidateInformation candidateInfo =  new ResumeRepositoryBLManager().
                            GetCandidateInformation(candidateID);

                        CandidateActivityViewerControl_candidateTextBox.Text = candidateInfo.caiFirstName;

                        // Load the default search records.
                        // Clear messages.
                        ThrowMessage(string.Empty);

                        ViewState["SORT_FIELD"] = "ACTIVITY_DATE";
                        ViewState["SORT_ORDER"] = SortType.Descending;
                        LoadActivities(1);
                    }
                }
            }
            get
            {
                if (Utility.IsNullOrEmpty(CandidateActivityViewerControl_candidateIDHiddenField.Value))
                    return 0;
                else
                    return Convert.ToInt32(CandidateActivityViewerControl_candidateIDHiddenField.Value);
            }
        }

        public string CandidateName
        {
            get
            {
                return CandidateActivityViewerControl_candidateTextBox.Text.Trim();
            }
        }

        public int GridPageSize
        {
            set
            {
                CandidateActivityViewerControl_gridPageSizeHiddenField.Value = value.ToString();
            }
            get
            {
                if (Utility.IsNullOrEmpty(CandidateActivityViewerControl_gridPageSizeHiddenField.Value))
                    return 10;
                else
                    return Convert.ToInt32(CandidateActivityViewerControl_gridPageSizeHiddenField.Value);
            }
        }

        public bool AllowChangeCandidate
        {
            set
            {
                CandidateActivityViewerControl_candidateImageButton.Visible = value;
            }
        }

        
        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Method that refresh the activities.
        /// </summary>
        public void RefreshActivities()
        {
            // Clear messages.
            ThrowMessage(string.Empty);

            // Check if candidate ID is present.
            if (this.CandidateID == 0)
            {
                ThrowMessage("No candidate was selected");
                return;
            }

            ViewState["SORT_FIELD"] = "ACTIVITY_DATE";
            ViewState["SORT_ORDER"] = SortType.Descending;
            LoadActivities(1);
            CandidateActivityViewerControl_pageNavigator.Reset();
        }

        /// <summary>
        /// Method that retrieves the table that is used for download.
        /// </summary>
        public DataTable GetDownloadTable()
        {
            try
            {
                // Check if candidate ID is present.
                if (this.CandidateID == 0)
                {
                    ThrowMessage("No candidate was selected");
                    return null;
                }

                // Get search critera.
                CandidateActivityLogSearchCriteria searchCriteria = GetSearchCriteria();

                int totalRecords = 0;

                // Get searched results.
                return new ResumeRepositoryBLManager().GetCandidateActivitiesTable(
                     searchCriteria,
                     1,
                     int.MaxValue,
                     ViewState["SORT_FIELD"].ToString(),
                     (SortType)ViewState["SORT_ORDER"],
                     out totalRecords);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ThrowMessage(exp.Message);
                return null;
            }
        }

        public void Reset()
        {
            CandidateActivityViewerControl_candidateTextBox.Text = string.Empty;
            CandidateActivityViewerControl_candidateIDHiddenField.Value = string.Empty;
            CandidateActivityViewerControl_activityByTextBox.ValidationGroup = string.Empty;
            CandidateActivityViewerControl_activityByHiddenField.Value = string.Empty;
            CandidateActivityViewerControl_activityTypeDropDownList.SelectedIndex = 0;

            CandidateActivityViewerControl_activityDateFromTextBox.Text = string.Empty;
            CandidateActivityViewerControl_activityDateToTextBox.Text = string.Empty;
            CandidateActivityViewerControl_commentsTextBox.Text = string.Empty;
            CandidateActivityViewerControl_activityLogDetailGridView.DataSource = null;
            CandidateActivityViewerControl_activityLogDetailGridView.DataBind();

            CandidateActivityViewerControl_pageNavigator.Reset();

            CandidateActivityViewerControl_restoreHiddenField.Value = string.Empty;
            CheckAndSetExpandorRestore();
        }

        #endregion Public Methods


        #region Private Methods

        private void LoadActivities(int pageNumber)
        {
            int totalRecords = 0;

            // Get search critera.
            CandidateActivityLogSearchCriteria searchCriteria = GetSearchCriteria();

            // Get searched results.
            List<CandidateActivityLog> logs = new ResumeRepositoryBLManager().GetCandidateActivities(
                    searchCriteria, 
                    pageNumber, 
                    this.GridPageSize, 
                    ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"], 
                    out totalRecords);

            if (logs == null || logs.Count == 0)
            {
                CandidateActivityViewerControl_activityLogDetailDIV.Visible = false;
                ThrowMessage("No activities found to display");
            }
            else
            {
                CandidateActivityViewerControl_activityLogDetailDIV.Visible = true;
            }

            CandidateActivityViewerControl_activityLogDetailGridView.DataSource = logs;
            CandidateActivityViewerControl_activityLogDetailGridView.DataBind();
            CandidateActivityViewerControl_pageNavigator.PageSize = this.GridPageSize;
            CandidateActivityViewerControl_pageNavigator.TotalRecords = totalRecords;
        }

        private CandidateActivityLogSearchCriteria GetSearchCriteria()
        {
            CandidateActivityLogSearchCriteria searchCriteria = new CandidateActivityLogSearchCriteria();
            searchCriteria.CandidateID = Convert.ToInt32(CandidateActivityViewerControl_candidateIDHiddenField.Value);
            searchCriteria.ActivityType = CandidateActivityViewerControl_activityTypeDropDownList.SelectedValue;

            if (!Utility.IsNullOrEmpty(CandidateActivityViewerControl_activityByHiddenField.Value))
                searchCriteria.ActivityBy = Convert.ToInt32(CandidateActivityViewerControl_activityByHiddenField.Value);

            searchCriteria.ActivityDateFrom = CandidateActivityViewerControl_activityDateFromTextBox.Text.Trim() == string.Empty ?
                Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(CandidateActivityViewerControl_activityDateFromTextBox.Text.Trim());

            searchCriteria.ActivityDateTo = CandidateActivityViewerControl_activityDateToTextBox.Text.Trim() == string.Empty ?
                Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(CandidateActivityViewerControl_activityDateToTextBox.Text.Trim()).AddDays(1);

            searchCriteria.Comments = CandidateActivityViewerControl_commentsTextBox.Text.Trim();
            
            return searchCriteria;
        }

        private void ThrowMessage(string message)
        {
            if (ControlMessageThrown != null)
            {
                ControlMessageThrown(this, new ControlMessageEventArgs
                    (message, MessageType.Error));
            }
        }

        /// <summary>
        /// Method that will return the date in MM/dd/yyyy format. If date not exists, 
        /// then it displays empty value in column.
        /// </summary>
        /// <param name="date">This parameter is passed from aspx page value 
        /// </param>
        /// <returns></returns>
        public string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(CandidateActivityViewerControl_restoreHiddenField.Value) &&
                CandidateActivityViewerControl_restoreHiddenField.Value == "Y")
            {
                CandidateActivityViewerControl_searchDIV.Style["display"] = "none";
                CandidateActivityViewerControl_searchResultsUpSpan.Style["display"] = "block";
                CandidateActivityViewerControl_searchResultsDownSpan.Style["display"] = "none";
                CandidateActivityViewerControl_activityLogDetailDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CandidateActivityViewerControl_searchDIV.Style["display"] = "block";
                CandidateActivityViewerControl_searchResultsUpSpan.Style["display"] = "none";
                CandidateActivityViewerControl_searchResultsDownSpan.Style["display"] = "block";
                CandidateActivityViewerControl_activityLogDetailDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods
    }
}