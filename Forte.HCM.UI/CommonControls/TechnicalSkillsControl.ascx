﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TechnicalSkillsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TechnicalSkillsControl" %>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="header_bg" colspan="2">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 93%" align="left">
                        <asp:Literal ID="TechnicalSkillsControl_headerLiteral" runat="server" Text="Technical Skills"></asp:Literal>
                        <asp:HiddenField ID="TechnicalSkillsControl_hiddenField" runat="server" />
                    </td>
                    <td style="width: 2%" align="right">
                        <span id="TechnicalSkillsControl_plusSpan" runat="server" style="display: none;">
                            <asp:Image ID="TechnicalSkillsControl_plusImage" runat="server" SkinID="sknPlusImage" /></span><span
                                id="TechnicalSkillsControl_minusSpan" runat="server" style="display: block;">
                                <asp:Image ID="TechnicalSkillsControl_minusImage" runat="server" SkinID="sknMinusImage" /></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%">
            <div id="TechnicalSkillsControl_controlsDiv" style="display: block;" runat="server">
                <table width="100%">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 15%">
                            <asp:Label ID="TechnicalSkillsControl_languageLabel" runat="server" Text="Languages"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 75%">
                            <asp:TextBox ID="TechnicalSkillsControl_languageTextBox" runat="server" TextMode="MultiLine"
                                Height="30" Width="97%" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 15%">
                            <asp:Label ID="TechnicalSkillsControl_osLabel" runat="server" Text="Operating System"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 75%">
                            <asp:TextBox ID="TechnicalSkillsControl_osTextBox" runat="server" TextMode="MultiLine"
                                Height="30" Width="97%" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 15%">
                            <asp:Label ID="TechnicalSkillsControl_databaseLabel" runat="server" Text="Database"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 75%">
                            <asp:TextBox ID="TechnicalSkillsControl_databaseTextBox" runat="server" TextMode="MultiLine"
                                Height="30" Width="97%" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 15%">
                            <asp:Label ID="TechnicalSkillsControl_uiToolsLabel" runat="server" Text="UI Tools"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 75%">
                            <asp:TextBox ID="TechnicalSkillsControl_uiToolsTextBox" runat="server" TextMode="MultiLine"
                                Height="30" Width="97%" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 15%">
                            <asp:Label ID="TechnicalSkillsControl_otherSkillsLabel" runat="server" Text="Other Skills"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 75%">
                            <asp:TextBox ID="TechnicalSkillsControl_otherSkillsTextBox" runat="server" TextMode="MultiLine" Height="30"
                                 Width="97%" MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
