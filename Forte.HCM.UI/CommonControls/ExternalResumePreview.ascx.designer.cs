﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class ExternalResumePreview {
        
        /// <summary>
        /// ExternalResumePreview_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_headerLiteral;
        
        /// <summary>
        /// ExternalResumePreview_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ExternalResumePreview_topCancelImageButton;
        
        /// <summary>
        /// ExternalResumePreview_successMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_successMessageLabel;
        
        /// <summary>
        /// ExternalResumePreview_errorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_errorMessageLabel;
        
        /// <summary>
        /// ExternalResumePreview_tabContainerDIV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ExternalResumePreview_tabContainerDIV;
        
        /// <summary>
        /// ExternalResumePreview_tabContainer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabContainer ExternalResumePreview_tabContainer;
        
        /// <summary>
        /// TabPanel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel TabPanel1;
        
        /// <summary>
        /// ExternalResumePreview_personalInformationLiterral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_personalInformationLiterral;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_CandidateNameHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_CandidateNameHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_CandidateNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_CandidateNameLabel;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_LocationHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_LocationHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_Location1Label control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_Location1Label;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_EmailHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_EmailHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_EmailLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_EmailLabel;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_Location2Label control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_Location2Label;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_PhoneHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_PhoneHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_PhoneLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_PhoneLabel;
        
        /// <summary>
        /// ExternalResumePreview_personalInfomation_Location3Label control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_personalInfomation_Location3Label;
        
        /// <summary>
        /// ExternalResumePreview_desiredPositionLiterral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_desiredPositionLiterral;
        
        /// <summary>
        /// ExternalResumePreview_desiredPayHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredPayHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredPayLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredPayLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredTravelHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredTravelHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredTravelLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredTravelLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredRelocationHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredRelocationHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredRelocationLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredRelocationLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredCommuteHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredCommuteHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredCommuteLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredCommuteLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredJobTypesHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredJobTypesHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_desiredJobTypesLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_desiredJobTypesLabel;
        
        /// <summary>
        /// TabPanel2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel TabPanel2;
        
        /// <summary>
        /// ExternalResumePreview_experienceLiterral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_experienceLiterral;
        
        /// <summary>
        /// ExternalResumePreview_experienceInMonthsHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_experienceInMonthsHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_experienceInMonthsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_experienceInMonthsLabel;
        
        /// <summary>
        /// ExternalResumePreview_experienceRecentTitleHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_experienceRecentTitleHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_experienceRecentTitleLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_experienceRecentTitleLabel;
        
        /// <summary>
        /// ExternalResumePreview_experienceHistoryLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_experienceHistoryLiteral;
        
        /// <summary>
        /// ExternalResumePreview_experienceHistoryGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView ExternalResumePreview_experienceHistoryGridView;
        
        /// <summary>
        /// TabPanel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel TabPanel3;
        
        /// <summary>
        /// ExternalResumePreview_educationalLiterral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_educationalLiterral;
        
        /// <summary>
        /// ExternalResumePreview_educationalGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView ExternalResumePreview_educationalGridView;
        
        /// <summary>
        /// ExternalResumePreview_additionEducationalLiterral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_additionEducationalLiterral;
        
        /// <summary>
        /// ExternalResumePreview_educationalManagedOthersHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalManagedOthersHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationalManagedOthersLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalManagedOthersLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationalSecurityClearnceHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalSecurityClearnceHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationalSecurityClearnceLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalSecurityClearnceLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationalFelonyConvictionHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalFelonyConvictionHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationalFelonyConvictionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalFelonyConvictionLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationalMilitaryExperiencesHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalMilitaryExperiencesHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationalMilitaryExperiencesLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationalMilitaryExperiencesLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationaLanguagesSpokenHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationaLanguagesSpokenHeaderLabel;
        
        /// <summary>
        /// ExternalResumePreview_educationaLanguagesSpokenLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ExternalResumePreview_educationaLanguagesSpokenLabel;
        
        /// <summary>
        /// TabPanel4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel TabPanel4;
        
        /// <summary>
        /// ExternalResumePreview_resumeDocumentLiterral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_resumeDocumentLiterral;
        
        /// <summary>
        /// ExternalResumePreview_resumeContentLiterral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ExternalResumePreview_resumeContentLiterral;
        
        /// <summary>
        /// ExternalResumePreview_previewFormCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ExternalResumePreview_previewFormCancelLinkButton;
    }
}
