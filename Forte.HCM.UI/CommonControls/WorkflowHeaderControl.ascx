﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkflowHeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.WorkflowHeaderControl" %>
<%@ Register Src="ChangePassword.ascx" TagName="ChangePassword" TagPrefix="uc1" %>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left" style="width: 15%; vertical-align: middle; padding-top: 5px">
                        <p onclick="location.href='<%= homeUrl %>'">
                            <asp:Image ID="WorkflowHeaderControl_logoImage" runat="server" SkinID="sknHomePageLogoImage" />
                        </p>
                    </td>
                    <td style="width: 85%" align="right" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td valign="top" align="right">
                                    <table cellpadding="3" cellspacing="2" border="0" class="login_bg" style="float: right;"
                                        id="WorkflowHeaderControl_loginTable" runat="server">
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="WorkflowHeaderControl_userNameLabel" Text="User Name"
                                                    SkinID="sknHomePageLabel"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" SkinID="sknHomePageTextBox" ID="WorkflowHeaderControl_userNametextbox"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="WorkflowHeaderControl_passwordLabel" runat="server" Text="Password"
                                                    SkinID="sknHomePageLabel"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="WorkflowHeaderControl_passwordtextbox" TextMode="Password"
                                                    SkinID="sknHomePageTextBox"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="WorkflowHeaderControl_goImageButton" runat="server" SkinID="sknHomePageGoImageButton"
                                                    OnClick="WorkflowHeaderControl_goImageButton_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td colspan="3" align="left">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:CheckBox ID="WorkflowHeaderControl_rememberMeCheckBox" runat="server" Text="Remember me"
                                                                CssClass="labletext" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td align="right">
                                                            <asp:LinkButton ID="WorkflowHeaderControl_forgotPasswordLinkButton" runat="server"
                                                                Text="Forgot Password?" CssClass="link_btn"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="2" cellpadding="2" id="WorkflowHeaderControl_loggedInTable"
                                        runat="server">
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Signed in as <span class="username_text">
                                                    <asp:Label ID="WorkflowHeaderControl_loginUserNameLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Last Login: <span class="date_time_text">
                                                    <asp:Label ID="WorkflowHeaderControl_lastLoginDateTimeLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="WorkflowHeaderControl_logoutButton" runat="server" Text="Log Out"
                                                                SkinID="sknButtonId" OnClick="WorkflowHeaderControl_logoutButton_Click" Width="60px" />
                                                        </td>
                                                        <td style="padding-left: 5px">
                                                        </td>
                                                        <td class="help_icon_hcm" title="Help" onclick="location.href='<%= helpUrl %>'">
                                                        </td>
                                                        <td class="home_icon_hcm" title="Home" onclick="location.href='<%= homeUrl %>'">
                                                        </td>
                                                        <td class="dashboard_icon_hcm" title="Dashboard" onclick="location.href='<%= landingUrl %>'">
                                                        </td>
                                                        <td class="user_options_icon_hcm" title="User Options" onclick="location.href='<%= userOptionsUrl %>'">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:LinkButton ID="WorkflowHeaderControl_changePasswordLinkButton" runat="server"
                                                    Text="Change Password" CssClass="link_btn" Visible="false">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:LinkButton ID="WorkflowHeaderControl_flushDataLinkButton" runat="server" OnClick="WorkflowHeaderControl_flushDataLinkButton_Click"
                                                    CssClass="link_btn" Text="Flush Data" Visible="false" OnClientClick="javascript:return confirm('This will flush all data. Do you want to continue?')">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td>
                                            <div style="float: left">
                                                <asp:LinkButton ID="WorkflowHeaderControl_customerAdminTD" runat="server" OnClick="WorkflowHeaderControl_customerAdminTD_Click"
                                                    CssClass="link_btn" Text="Tenants" Visible="false" title="Create/View Tenant">
                                                </asp:LinkButton>
                                            </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <div style="float: left">
                                                <asp:LinkButton ID="WorkflowHeaderControl_siteAdminTD" runat="server" OnClick="WorkflowHeaderControl_siteAdminTD_Click"
                                                    CssClass="link_btn" Text="Super Admin" Visible="false">
                                                </asp:LinkButton>
                                            </div>
                                            </td>
                                        </tr>--%>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="WorkflowHeaderControl_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                                        EnableViewState="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px; padding-bottom: 10px">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="right" style="width: 100%;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="topmenu">
                            <tr>
                                <td align="right">
                                    <asp:Menu ID="SubscriptionMaster_menu" StaticMenuItemStyle-HorizontalPadding="25"
                                        runat="server" Orientation="Horizontal" PathSeparator="/" DynamicMenuItemStyle-VerticalPadding="10"
                                        DynamicMenuStyle-BorderColor="#ced9df" DynamicMenuStyle-BorderStyle="Solid" DynamicMenuStyle-BorderWidth="1">
                                        <StaticMenuItemStyle CssClass="primaryStaticMenu" />
                                        <StaticHoverStyle CssClass="primaryStaticHover" />
                                        <DynamicMenuItemStyle CssClass="primaryDynamicMenuItem" />
                                        <DynamicHoverStyle CssClass="primaryDynamicHover" />
                                        <Items>
                                            <asp:MenuItem Text="Solutions" Selectable="true" Value="1">
                                                <asp:MenuItem Text="Enterprise Application Performance" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="Data Asset Performance" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="Software Architecture" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="Enterprise Content Management" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Products" Selectable="true" Value="2">
                                                <asp:MenuItem Text="Professional Resource Augmentation" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="TalentScout" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Pricing" Selectable="true" Value="3">
                                                <asp:MenuItem Text="Compare Subscriptions" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Company" Selectable="true" Value="4">
                                                <asp:MenuItem Text="Explore" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="History & Name" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="Strategic Indent" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="Management" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="Value System" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                                <asp:MenuItem Text="Service Model" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Partners" Selectable="true" Value="5">
                                                <asp:MenuItem Text="Show List" NavigateUrl="~/Common/PageUnderConstruction.aspx">
                                                </asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Options" Selectable="true" Value="6">
                                                <asp:MenuItem Text="My Account" NavigateUrl="~/Admin/ViewAccount.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Edit Account" NavigateUrl="~/Admin/EditAccount.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Upgrade Account" NavigateUrl="~/Admin/UpgradeAccount.aspx"></asp:MenuItem>
                                            </asp:MenuItem>
                                        </Items>
                                    </asp:Menu>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="WorkflowHeaderControl_signUpImage" runat="server" OnClick="WorkflowHeaderControl_signUpImage_Click"
                            SkinID="sknHomePageSignUpImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:Panel ID="WorkflowHeaderControl_changePasswordPanel" runat="server" Style="display: none"
    CssClass="popupcontrol_forget_password">
    <uc1:ChangePassword ID="WorkflowHeaderControl_ChangePassword" runat="server" />
</asp:Panel>
<ajaxToolKit:ModalPopupExtender ID="WorkflowHeaderControl_changePassword_ModalpPopupExtender"
    runat="server" TargetControlID="WorkflowHeaderControl_changePasswordLinkButton"
    PopupControlID="WorkflowHeaderControl_changePasswordPanel" BackgroundCssClass="modalBackground">
</ajaxToolKit:ModalPopupExtender>
<%--<script type="text/javascript" language="javascript">
    function ShowOrHideSiteAdmin() {
        if ('<%= isSiteAdmin %>'.toUpperCase() == 'TRUE') {
            document.getElementById('WorkflowHeaderControl_siteAdminTD').style.display = "block";
            document.getElementById('WorkflowHeaderControl_customerAdminTD').style.display = "block";
        }
        else {
            document.getElementById('WorkflowHeaderControl_siteAdminTD').style.display = "none";
            document.getElementById('WorkflowHeaderControl_customerAdminTD').style.display = "none";
        }
    }


</script>--%>
<script type="text/javascript" language="javascript">
    ShowOrHideSiteAdmin();
</script>
