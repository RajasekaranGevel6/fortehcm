﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionHeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SubscriptionHeaderControl" %>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="width: 15%; vertical-align: middle; padding-top: 5px">
                        <p onclick="location.href='<%= homeUrl %>'">
                            <asp:Image ID="HomePage_logoImage" runat="server" SkinID="sknHomePageLogoImage" />
                        </p>
                        <%--<asp:ImageButton ID="HomePage_logoImageButton" runat="server" SkinID="sknHomePageLogoImageButton" OnClick="HomePage_logoImage_Home_Click" />--%>
                    </td>
                    <td style="width: 85%" align="right" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td valign="top" align="right">
                                    <table border="0" cellspacing="2" cellpadding="2" id="SubscriptionSubscriptionHeaderControl_loggedInTable"
                                        runat="server">
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Signed in as <span class="username_text">
                                                    <asp:Label ID="SubscriptionHeaderControl_loginUserNameLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Last Login: <span class="date_time_text">
                                                    <asp:Label ID="SubscriptionHeaderControl_lastLoginDateTimeLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="45%">
                                                            <asp:Button ID="SubscriptionHeaderControl_logoutButton" runat="server" Text="Log Out"
                                                                SkinID="sknButtonId" OnClick="SubscriptionHeaderControl_logoutButton_Click" Width="60px" />
                                                        </td>
                                                        <td width="25%" class="help_button">
                                                            <a href="#">Help</a>
                                                        </td>
                                                        <td width="25%">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px; padding-bottom: 10px">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="right" style="width: 100%;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="topmenu">
                            <tr>
                                <td align="right">
                                    <asp:Menu ID="SubscriptionMaster_menu" StaticMenuItemStyle-HorizontalPadding="25"
                                        runat="server" Orientation="Horizontal" PathSeparator="/" DynamicMenuItemStyle-VerticalPadding="10"
                                        DynamicMenuStyle-BorderColor="#ced9df" DynamicMenuStyle-BorderStyle="Solid" DynamicMenuStyle-BorderWidth="1">
                                        <StaticMenuItemStyle CssClass="primaryStaticMenu" />
                                        <StaticHoverStyle CssClass="primaryStaticHover" />
                                        <DynamicMenuItemStyle CssClass="primaryDynamicMenuItem" />
                                        <DynamicHoverStyle CssClass="primaryDynamicHover" />
                                        <Items>
                                            <asp:MenuItem Text="Solutions" Selectable="true" Value="1">
                                                <asp:MenuItem Text="Enterprise Application Performance"></asp:MenuItem>
                                                <asp:MenuItem Text="Data Asset Performance"></asp:MenuItem>
                                                <asp:MenuItem Text="Software Architecture"></asp:MenuItem>
                                                <asp:MenuItem Text="Enterprise Content Management"></asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Products" Selectable="true" Value="2">
                                                <asp:MenuItem Text="Professional Resource Augmentation"></asp:MenuItem>
                                                <asp:MenuItem Text="TalentScout"></asp:MenuItem>
                                            </asp:MenuItem>
                                             <asp:MenuItem Text="Pricing" Selectable="true" Value="3">
                                                <asp:MenuItem Text="Compare Subscriptions"></asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Company" Selectable="true" Value="4">
                                                <asp:MenuItem Text="Explore"></asp:MenuItem>
                                                <asp:MenuItem Text="History & Name"></asp:MenuItem>
                                                <asp:MenuItem Text="Strategic Indent"></asp:MenuItem>
                                                <asp:MenuItem Text="Management"></asp:MenuItem>
                                                <asp:MenuItem Text="Value System"></asp:MenuItem>
                                                <asp:MenuItem Text="Service Model"></asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Partners" Selectable="true" Value="5">
                                                <asp:MenuItem Text="Show List"></asp:MenuItem>
                                            </asp:MenuItem>
                                        </Items>
                                    </asp:Menu>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
