﻿
#region Namespace                                                              

using System;
using System.Text;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class UpgradeAccountConfirmationControl : UserControl
    {

        #region Enum                                                           

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2
        }

        #endregion

        #region DataScource                                                    

        public UserRegistrationInfo SubscriptionDetailsDataSource
        {
            set
            {
                if (value == null)
                    throw new Exception("value should not be null");
                BindBussinessTypes();
                UpgradeAccountConfirmationControl_otherTypeOfBussinessCheckBox.Attributes.Add("onclick", "ShowValue(this.checked);");
                SetSubscriptionDetails(value.UserID);
                UpgradeAccountConfirmationControl_subscriptionDisplayLabel.Text = value.SubscriptionName;
                UpgradeAccountConfirmationControl_selectedSubscriptionIdHidden.Value = value.SubscriptionId.ToString();
                UpgradeAccountConfirmationControl_isTrialHidden.Value = value.IsTrial.ToString();
                if (value.IsTrial == 1)
                    UpgradeAccountConfirmationControl_trialDaysHidden.Value = value.TrialDays.ToString();
                UpgradeAccountConfirmationControl_userIdHidden.Value = value.UserID.ToString();
                if (value.SubscriptionName.ToLower() == "corporate")
                    UpgradeAccountConfirmationControl_numberOfUsersTr.Visible = true;
                else
                    UpgradeAccountConfirmationControl_numberOfUsersTr.Visible = false;
                UpgradeAccountConfirmationControl_MaximumCorporateUsersHidden.Value = new UserRegistrationBLManager().GetMaximumCorporateUsers().ToString();
                UpgradeAccountConfirmationControl_numberOfUsersTextBoxHelpImageButton.ToolTip = "Maximum number of users allowed is " +
                    UpgradeAccountConfirmationControl_MaximumCorporateUsersHidden.Value;
            }
        }

        #endregion DataSource

        #region Events                                                         

        protected void UpgradeSubscription_CancelClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Admin/UpgradeAccount.aspx?m=0&s=2", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void UpgradeAccountConfirmationControl_registerButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                UpdateSubscriptionDetails();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
            finally
            {
                InvokePageBaseMethod("ShowUpgradeModalPopUp", null);
            }

        }

        #endregion Events

        #region Private Methods                                                

        /// <summary>
        /// A Method that updates the subscription details
        /// </summary>
        private void UpdateSubscriptionDetails()
        {
            UserRegistrationInfo userRegistrationInfo = null;
            try
            {
                userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.UserID = Convert.ToInt32(UpgradeAccountConfirmationControl_userIdHidden.Value);
                userRegistrationInfo.FirstName = UpgradeAccountConfirmationControl_firstNameTextBox.Text.Trim();
                userRegistrationInfo.LastName = UpgradeAccountConfirmationControl_lastNameTextBox.Text.Trim();
                userRegistrationInfo.Title = UpgradeAccountConfirmationControl_titleTextBox.Text.Trim();
                userRegistrationInfo.Company = UpgradeAccountConfirmationControl_companyNameTextBox.Text.Trim();
                userRegistrationInfo.Phone = UpgradeAccountConfirmationControl_phoneTextBox.Text.Trim();
                if (UpgradeAccountConfirmationControl_numberOfUsersTr.Visible)
                    userRegistrationInfo.NumberOfUsers = Convert.ToInt16(UpgradeAccountConfirmationControl_numberOfUsersTextBox.Text.Trim());
                userRegistrationInfo.SubscriptionId = Convert.ToInt32(UpgradeAccountConfirmationControl_selectedSubscriptionIdHidden.Value);
                userRegistrationInfo.BussinessTypesIds = GetSelectedBussinessTypesFromList();
                userRegistrationInfo.BussinessTypeOther = UpgradeAccountConfirmationControl_otherBussinessTextBox.Text.Trim();
                userRegistrationInfo.IsTrial = Convert.ToInt16(UpgradeAccountConfirmationControl_isTrialHidden.Value);
                userRegistrationInfo.SubscriptionRole = Enum.GetName(typeof(SubscriptionRolesEnum), userRegistrationInfo.SubscriptionId);
                if (Utility.IsNullOrEmpty(userRegistrationInfo.SubscriptionRole))
                    userRegistrationInfo.SubscriptionRole = "SR_OTHER";
                if (UpgradeAccountConfirmationControl_isTrialHidden.Value == "1")
                    userRegistrationInfo.TrialDays = Convert.ToInt32(UpgradeAccountConfirmationControl_trialDaysHidden.Value);
                userRegistrationInfo.ModifiedBy = userRegistrationInfo.UserID;
                new UserRegistrationBLManager().UpgradeAccountSubscriptionDetails(userRegistrationInfo);
                ShowSuccessMessage("Your Account Details Updated Successfully");
                UpdateSessionDetails(userRegistrationInfo.SubscriptionId, userRegistrationInfo.SubscriptionRole);
                InvokePageBaseMethod("InvokeSubscriptionUpdateModalPopUp", null);
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                InvokeGarbageCollector();
            }
        }

        /// <summary>
        /// A Method that updates the subscription details of user detail session
        /// </summary>
        /// <param name="SubscriptionId">
        /// A <see cref="System.Int32"/> that holds the 
        /// changed subscription id for the logged in user
        /// </param>
        /// <param name="SubscriptionRole">
        /// A <see cref="System.String"/> that holds the changed subscription 
        /// role for the logged in user
        /// </param>
        private void UpdateSessionDetails(int SubscriptionId, string SubscriptionRole)
        {
            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
            userDetail.SubscriptionId = SubscriptionId;
            userDetail.SubscriptionRole = SubscriptionRole;
            UserDetail us1 = Session["USER_DETAIL"] as UserDetail;
        }

        /// <summary>
        /// A Method that gets the selected bussiness types from 
        /// the check box list.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the multiple selected
        /// bussiness types id's with comma seperated operator
        /// </returns>
        private string GetSelectedBussinessTypesFromList()
        {
            StringBuilder SelectedItems = new StringBuilder();
            for (int ItemsCount = 0; ItemsCount < UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.Items.Count; ItemsCount++)
                if (UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.Items[ItemsCount].Selected)
                    SelectedItems.Append(UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.Items[ItemsCount].Value + ",");
            return SelectedItems.ToString().TrimEnd(',');
        }
        
        /// <summary>
        /// A Method that loads the logged in subscription details in to the
        /// text box
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="System.Int32"/> that holds the user id
        /// of the logged in user to load the susbcription details.
        /// </param>
        private void SetSubscriptionDetails(int UserId)
        {
            UserRegistrationInfo userRegistrationInfo = null;
            try
            {
                userRegistrationInfo = GetUserRegistrationInfoDetails(UserId);
                if (userRegistrationInfo == null)
                    throw new Exception("User details not found");
                UpgradeAccountConfirmationControl_userNameDisplayLabel.Text = userRegistrationInfo.UserEmail;
                UpgradeAccountConfirmationControl_previousSubscriptionIdHidden.Value = userRegistrationInfo.SubscriptionId.ToString();
                UpgradeAccountConfirmationControl_firstNameTextBox.Text = userRegistrationInfo.FirstName;
                UpgradeAccountConfirmationControl_lastNameTextBox.Text = userRegistrationInfo.LastName;
                UpgradeAccountConfirmationControl_phoneTextBox.Text = userRegistrationInfo.Phone;
                UpgradeAccountConfirmationControl_companyNameTextBox.Text = userRegistrationInfo.Company;
                UpgradeAccountConfirmationControl_titleTextBox.Text = userRegistrationInfo.Title;
                if (userRegistrationInfo.NumberOfUsers == 0)
                    UpgradeAccountConfirmationControl_numberOfUsersTr.Visible = false;
                else
                    UpgradeAccountConfirmationControl_numberOfUsersTr.Visible = true;
                UpgradeAccountConfirmationControl_numberOfUsersTextBox.Text = userRegistrationInfo.NumberOfUsers.ToString();
                SelectBussinessType(userRegistrationInfo.BussinessTypesIds, userRegistrationInfo.BussinessTypeOther);
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                InvokeGarbageCollector();
            }
        }

        /// <summary>
        /// A Method that check the business types that user
        /// selected
        /// </summary>
        /// <param name="BusinessTypeIds">
        /// A <see cref="System.String"/> that holds the 
        /// various business type ids with comma as delimiter
        /// </param>
        /// <param name="BusinessTypeOther">
        /// A <see cref="System.String"/> that holds that other business type
        /// to load in the other business type textbox
        /// </param>
        private void SelectBussinessType(string BusinessTypeIds,string BusinessTypeOther)
        {
            string[] strSplitBusinessId = BusinessTypeIds.Split(',');
            for (int i = 0; i < strSplitBusinessId.Length; i++)
                try
                {
                    UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.Items.FindByValue(strSplitBusinessId[i]).Selected = true;
                }
                catch { }
            if (string.IsNullOrEmpty(BusinessTypeOther))
                return;
            UpgradeAccountConfirmationControl_otherTypeOfBussinessCheckBox.Checked = true;
            UpgradeAccountConfirmationControl_otherBussinessDiv.Style.Add("display", "block");
            UpgradeAccountConfirmationControl_otherBussinessTextBox.Text = BusinessTypeOther;
        }

        /// <summary>
        /// A Method that communicates with the BL manager to get the
        /// subscription details
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="System.Int32"/> that holds the user id
        /// of the logged in user to load the susbcription details.
        /// </param>
        /// <returns>
        /// A <see cref="Forte.HCM.DataObjects.UserRegistrationInfo"/> that
        /// holds the user registration information
        /// </returns>
        private UserRegistrationInfo GetUserRegistrationInfoDetails(int UserId)
        {
            return new UserRegistrationBLManager().GetUserRegistrationInfo(UserId);
        }

        /// <summary>
        /// A Method that invokes the garbage collector
        /// </summary>
        private void InvokeGarbageCollector()
        {
            try
            {
                GC.Collect(2, GCCollectionMode.Forced);
            }
            catch
            {
                try
                {
                    GC.Collect();
                }
                catch { }
            }
        }

        /// <summary>
        /// Invokes the parent page base public methods
        /// </summary>
        /// <param name="MethodName">
        /// A <see cref="System.String"/> that holds the method name to invoke
        /// </param>
        /// <param name="args">
        /// A <see cref="System.Object"/> that holds the arguments to be passed to the method
        /// </param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        /// <summary>
        /// A Method that binds the bussiness type that are available in 
        /// repository to the checkboxlist
        /// </summary>
        private void BindBussinessTypes()
        {
            UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.DataSource = GetBussinessTypes();
            UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.DataValueField = "BusinessTypeID";
            UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.DataTextField = "BusinessTypeName";
            UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.DataBind();
        }

        /// <summary>
        /// A Method that communicates with the BL Manager to get the
        /// list of business types available in the repository
        /// </summary>
        /// <returns></returns>
        private List<BusinessTypeDetail> GetBussinessTypes()
        {
            int TotalRecords = 0;
            return new AdminBLManager().GetBusinessTypes(string.Empty, "NAME", "A", 1, null, out TotalRecords);
        }

        /// <summary>
        /// A Method that shows message to the user
        /// </summary>
        /// <param name="TopMessageLabel">
        /// A <see cref="System.Web.UI.WebControls.Label"/> that holds
        /// the top message label
        /// </param>
        /// <param name="BottomMessageLabel">
        /// A <see cref="System.Web.UI.WebControls.Label"/> that holds
        /// the bottom message label
        /// </param>
        /// <param name="Message"></param>
        private void ShowMessage(Label TopMessageLabel, Label BottomMessageLabel, string Message)
        {
            if (string.IsNullOrEmpty(TopMessageLabel.Text))
                TopMessageLabel.Text = Message;
            else
                TopMessageLabel.Text += "<br />" + Message;
            //
            if (string.IsNullOrEmpty(BottomMessageLabel.Text))
                BottomMessageLabel.Text = Message;
            else
                BottomMessageLabel.Text += "<br />" + Message;
        }

        /// <summary>
        /// A Method that shows the success message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// </param>
        private void ShowSuccessMessage(string Message)
        {
            ShowMessage(UpgradeAccountConfirmationControl_topSuccessMessageLabel,
                UpgradeAccountConfirmationControl_bottomSuccessMessageLabel, Message);
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            ShowMessage(UpgradeAccountConfirmationControl_topErrorMessageLabel,
                UpgradeAccountConfirmationControl_bottomErrorMessageLabel, Message);
        }

        /// <summary>
        /// A method that logs the exception message and shows
        /// the exception to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            if (exp == null)
                return;
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        /// <summary>
        /// A Method that validates the user input data
        /// </summary>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the valid input data or not
        /// </returns>
        protected bool IsValidData()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(UpgradeAccountConfirmationControl_firstNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyFirstName);
                isValid = false;
            }
            if (string.IsNullOrEmpty(UpgradeAccountConfirmationControl_lastNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyLastName);
                isValid = false;
            }
            if (string.IsNullOrEmpty(UpgradeAccountConfirmationControl_phoneTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyPhoneNo);
                isValid = false;
            }
            if (string.IsNullOrEmpty(UpgradeAccountConfirmationControl_companyNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyCompanyName);
                isValid = false;
            }
            if (!UpgradeAccountConfirmationControl_otherTypeOfBussinessCheckBox.Checked && UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList.SelectedIndex < 0)
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyTypeOfBusiness);
                isValid = false;
            }
            //if (UserRegistration_typeOfBussinessCheckBoxList.SelectedIndex == UserRegistration_typeOfBussinessCheckBoxList.Items.Count - 1)
            if (UpgradeAccountConfirmationControl_otherTypeOfBussinessCheckBox.Checked)
            {
                if (string.IsNullOrEmpty(UpgradeAccountConfirmationControl_otherBussinessTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyOtherTypeOfBussiness);
                    isValid = false;
                }
                UpgradeAccountConfirmationControl_otherBussinessDiv.Style.Add("display", "block");
            }
            else
                UpgradeAccountConfirmationControl_otherBussinessDiv.Style.Add("display", "none");
            if (UpgradeAccountConfirmationControl_subscriptionDisplayLabel.Text.ToLower() == "corporate")
            {
                if (string.IsNullOrEmpty(UpgradeAccountConfirmationControl_numberOfUsersTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyNumberOfUsers);
                    isValid = false;
                }
                else
                {
                    int Temp = 0;
                    int.TryParse(UpgradeAccountConfirmationControl_numberOfUsersTextBox.Text.Trim(), out Temp);
                    if (Temp == 0)
                    {
                        ShowErrorMessage(HCMResource.UserRegistration_InvalidNumberOfUsers);
                        isValid = false;
                    }
                    if (Temp > Convert.ToInt32(UpgradeAccountConfirmationControl_MaximumCorporateUsersHidden.Value))
                    {
                        ShowErrorMessage(string.Format(HCMResource.UserRegistration_MaximumSystemUserExceed,
                            UpgradeAccountConfirmationControl_MaximumCorporateUsersHidden.Value));
                        isValid = false;
                    }
                }
            }
            if (!isValid)
                InvokePageBaseMethod("ShowUpgradeModalPopUp", null);
            return isValid;
        }

        #endregion Private Methods

    }
}