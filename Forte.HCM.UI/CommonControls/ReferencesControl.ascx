﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReferencesControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ReferencesControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>

<asp:UpdatePanel ID="ReferencesControl_updatePanel" runat="server">
    <ContentTemplate>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="header_bg">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="width: 93%" align="left">
                                <asp:Literal ID="ReferencesControl_headerLiteral" runat="server" Text="References"></asp:Literal>
                                 <asp:HiddenField ID="ReferencesControl_hiddenField" runat="server" />
                                <asp:HiddenField ID="ReferencesControl_deletedRowHiddenField" runat="server" />
                            </td>
                            <td style="width: 2%" align="right">
                                <span id="ReferencesControl_plusSpan" runat="server" style="display: none;">
                                    <asp:Image ID="ReferencesControl_plusImage" runat="server" SkinID="sknPlusImage" /></span><span
                                        id="ReferencesControl_minusSpan" runat="server" style="display: block;">
                                        <asp:Image ID="ReferencesControl_minusImage" runat="server" SkinID="sknMinusImage" /></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="width: 100%;" runat="server" id="ReferencesControl_controlsDiv">
            <asp:LinkButton ID="ReferencesControl_addRowLinkButton" SkinID="sknAddLinkButton"
                runat="server" Text="Add" OnClick="ReferencesControl_addRowLinkButton_Click"
                ToolTip="Add new Row"></asp:LinkButton>
            <br />
            <asp:ListView ID="ReferencesControl_listView" runat="server" 
                OnItemDataBound="ReferencesControl_listView_ItemDataBound" 
                onitemcommand="ReferencesControl_listView_ItemCommand">
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" width="100%">
                        <tr runat="server" id="itemPlaceholder">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr >
                        <td>
                            <table class="ListViewNormalStyle" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="ReferencesControl_rowNumberLabel" runat="server" Text="" SkinID="sknRowNumber"></asp:Label>
                                    </td>
                                    <td align="right" colspan="4">
                                        <asp:ImageButton ID="ReferencesControl_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                            ToolTip="Delete Reference" CommandName="deleteReference" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 12%">                                  
                                        <asp:Label runat="server" ID="ReferencesControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                         <asp:TextBox ID="ReferencesControl_deleteRowIndex" runat="server" Text='<%# Eval("ReferenceId") %>'  style="display:none" />
                                    </td>
                                    <td style="width: 30%">
                                        <asp:TextBox runat="server" ID="ReferencesControl_nameTextBox" Text='<%# Eval("Name") %>'
                                            Width="100%" ToolTip="Name" MaxLength="500" ></asp:TextBox>
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td style="width: 15%">
                                        <asp:Label runat="server" ID="ReferencesControl_organizationLabel" Text="Organization"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 40%">
                                        <asp:TextBox runat="server" ID="ReferencesControl_organizationTextBox" Text='<%# Eval("Organization") %>'
                                            Width="95%" ToolTip="Organization" MaxLength="500" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="ReferencesControl_relationLabel" Text="Relation" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="ReferencesControl_relationTextBox" Text='<%# Eval("Relation") %>'
                                            Width="100%" ToolTip="Relation" MaxLength="500" ></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="ReferencesControl_contInfoLabel" Text="Contact Info"
                                            SkinID="sknLabelFieldHeaderText" ></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="ReferencesControl_contInfoTextBox" Text='<%# SetContactInfo(((Forte.HCM.DataObjects.Reference)Container.DataItem).ContactInformation)%>'
                                            Width="95%" ToolTip="Contact Info" MaxLength="500" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="ListViewAlternateStyle">
                        <td>
                            <table class="ListViewNormalStyle" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="ReferencesControl_rowNumberLabel" runat="server" Text="" SkinID="sknRowNumber"></asp:Label>
                                    </td>
                                    <td align="right"  colspan="4">
                                        <asp:ImageButton ID="ReferencesControl_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                            CommandName="deleteReference" ToolTip="Delete Reference" />
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 12%">
                                        <asp:Label runat="server" ID="ReferencesControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        <asp:TextBox ID="ReferencesControl_deleteRowIndex" runat="server" Text='<%# Eval("ReferenceId") %>'  style="display:none" />
                                    </td>
                                    <td style="width: 30%">
                                        <asp:TextBox runat="server" ID="ReferencesControl_nameTextBox" Text='<%# Eval("Name") %>'
                                            Width="100%" ToolTip="Name" MaxLength="500" ></asp:TextBox>
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td style="width: 15%">
                                        <asp:Label runat="server" ID="ReferencesControl_organizationLabel" Text="Organization"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 40%">
                                        <asp:TextBox runat="server" ID="ReferencesControl_organizationTextBox" Text='<%# Eval("Organization") %>'
                                            Width="95%" ToolTip="Organization" MaxLength="500" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="ReferencesControl_relationLabel" Text="Relation" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="ReferencesControl_relationTextBox" Text='<%# Eval("Relation") %>'
                                            Width="100%" ToolTip="Relation" MaxLength="500" ></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="ReferencesControl_contInfoLabel" Text="Contact Info"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="ReferencesControl_contInfoTextBox" Text='<%# SetContactInfo(((Forte.HCM.DataObjects.Reference)Container.DataItem).ContactInformation)%>'
                                            Width="95%" ToolTip="Contact Info" MaxLength="500" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
            </asp:ListView>
                  <asp:UpdatePanel ID="ReferencesControl_confirmPopUpUpdatPanel" runat="server">
            <ContentTemplate>
                <div id="ReferencesControl_hiddenDIV" runat="server" style="display: none">
                    <asp:Button ID="ReferencesControl_hiddenPopupModalButton" runat="server" />
                 </div>
                 <ajaxToolKit:ModalPopupExtender ID="ReferencesControl_deleteModalPopupExtender" runat="server"
                         PopupControlID="ReferencesControl_deleteConfirmPopUpPanel" TargetControlID="ReferencesControl_hiddenPopupModalButton"
                         BackgroundCssClass="modalBackground">
                 </ajaxToolKit:ModalPopupExtender>
                 <asp:Panel ID="ReferencesControl_deleteConfirmPopUpPanel" runat="server" 
                    Style="display: none; width:30%; height:210px; background-position:bottom; background-color:#283033;">
                  <uc1:ConfirmMsgControl ID="ReferencesControl_confirmPopupExtenderControl" runat="server" Type="YesNo"  Title="Delete Reference" Message="Are you sure want to delete reference details?" OnOkClick="ReferencesControl_okPopUpClick" />
                 </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
            
        </div>
        <br />
    </ContentTemplate>
</asp:UpdatePanel>
