﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddClientControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.AddClientControl" %>
<asp:Panel ID="AddClientControl_mainPanel" runat="server" DefaultButton="AddClientControl_createButton">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="AddClientControl_messagetitleLiteral" Text="Add Client" runat="server"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="AddClientControl_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="client_modal_popup_bg">
                    <tr>
                        <td valign="top">
                            <div style="height:340px;overflow:auto">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr id="AddClientControl_messageTd" runat="server" class="td_height_20">
                                    <td>
                                        <asp:Label ID="AddClientControl_topErrorMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                        <asp:Label ID="AddClientControl_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                        <asp:HiddenField ID="AddClientControl_tenantIDHiddenField" runat="server" />
                                        <asp:HiddenField ID="AddClientControl_userIDHiddenField" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="ClientControl_panel" runat="server">
                                            <table border="0" cellspacing="3" cellpadding="3">
                                                <tr>
                                                    <td id="ClientControl_td1" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td2" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td3" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td4" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ClientControl_td5" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td6" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td7" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td8" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ClientControl_td9" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td10" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td11" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td12" runat="server">
                                                    </td>
                                                </tr>
                                                <%--  <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ClientControl_td13" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td14" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td15" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td16" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ClientControl_td17" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td18" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td19" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td20" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ClientControl_td21" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td22" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td23" runat="server">
                                                    </td>
                                                    <td id="ClientControl_td24" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ClientControl_td25" runat="server">
                                                    </td>
                                                    <td colspan="3" id="ClientControl_td26" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_10">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table cellpadding="0" cellspacing="0" width="98%" align="center">
                                            <tr>
                                                <td style="width:100px">
                                                    <asp:Label ID="AddClientControl_departmentTitleLabel" Text="Department" CssClass="position_profile_header_title"  runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="AddClientControl_isDepartmentRequiredCheckBox" 
                                                        runat="server" 
                                                        oncheckedchanged="AddClientControl_isDepartmentRequiredCheckBox_CheckedChanged" 
                                                        AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><hr /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <div id="AddClientControl_showDepartmentDiv" runat="server" style="display:none">
                                        <asp:Panel ID="DepartmentControl_panel" runat="server" EnableViewState="true">
                                            <table border="0" cellspacing="3" cellpadding="3">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="DepartmentControl_departmentNameLabel" Text="Department<br>Name" runat="server"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <span class="mandatory" id="DepartmentControl_departmentNamespan" runat="server">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="DepartmentControl_departmentNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="DepartmentControl_primaryAddressCheckbox" Text="Primary Address"
                                                            EnableViewState="true" Visible="false" runat="server" AutoPostBack="True" OnCheckedChanged="DepartmentControl_primaryAddressCheckbox_CheckedChanged"  />
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="DepartmentControl_descriptionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Description" Visible="false" EnableViewState="false"></asp:Label>
                                                       
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="DepartmentControl_descriptionTextBox" Columns="75" runat="server"
                                                            MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
                                                            onchange="CommentsCount(500,this)" EnableViewState="false" Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="DepartmentControl_td5" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td6" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td7" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td8" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td9" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td10" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td11" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td12" runat="server">
                                                    </td>
                                                </tr>
                                                <%--  <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td13" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td14" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td15" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td16" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td17" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td18" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td19" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td20" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td21" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td22" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td23" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td24" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="DepartmentControl_additionalInfoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Additional Info" Visible="false"></asp:Label>
                                                      
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="DepartmentControl_additionalInfoTextBox" Columns="75" runat="server"
                                                            MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
                                                            onchange="CommentsCount(500,this)" Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        </div>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="td_height_10">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table cellpadding="0" cellspacing="0" width="98%" align="center">
                                            <tr>
                                                <td style="width:100px">
                                                    <asp:Label ID="AddClientControl_contactTitleLabel" Text="Contact" CssClass="position_profile_header_title" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="AddClientControl_isContactRequiredCheckBox" runat="server" 
                                                    oncheckedchanged="AddClientControl_isContactRequiredCheckBox_CheckedChanged"
                                                     AutoPostBack="true"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <div id="AddClientControl_showContactDiv" runat="server" style="display:none">
                                        <asp:Panel ID="ContactControl_panel" runat="server">
                                            <table border="0" cellspacing="3" cellpadding="3">
                                            
                                                <tr id="ContactControl_clientNameTR" runat="server">
                                                    <td>
                                                        <asp:Label ID="ContactControl_clientNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Client Name"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ContactControl_firstNameLabel" Text="First Name" runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <span class="mandatory">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="ContactControl_firstNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="AdClientControl_departmentCheckBoxUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:CheckBox ID="ContactControl_primaryAddressCheckbox" Text="Primary Address" EnableViewState="true"
                                                                    Visible="false" runat="server" AutoPostBack="True" 
                                                                    OnCheckedChanged="ContactControl_primaryAddressCheckbox_CheckedChanged" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td5" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td6" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td7" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td8" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td9" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td10" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td11" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td12" runat="server">
                                                    </td>
                                                </tr>
                                                <%--  <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td13" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td14" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td15" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td16" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td17" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td18" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td19" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td20" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td21" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td22" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td23" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td24" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ContactControl_additionalInfoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Additional Info" Visible="false"></asp:Label>
                                                       
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="ContactControl_additionalInfoTextBox" Columns="80" runat="server"
                                                            MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
                                                            onchange="CommentsCount(500,this)" Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        </div>
                                    </td>
                                </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" align="left">
                <asp:Button ID="AddClientControl_createButton" runat="server" Text="Save" SkinID="sknButtonId"
                    ValidationGroup="a" OnCommand="AddClientControl_createButton_Command" />
                &nbsp; &nbsp;
                <asp:LinkButton ID="AddClientControl_featureCancelLinkButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton">
                </asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
            </td>
        </tr>
    </table>
    <!-- Client Information Controls -->
    <asp:Label ID="ClientControl_clientNameLabel" Text="Client Name" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
    <span class="mandatory" id="ClientControl_clientNameSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="ClientControl_clientNameTextBox" runat="server" MaxLength="100"
        Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_streetAddressLabel" Text="Street Address" runat="server"
        SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>
   
    <asp:TextBox ID="ClientControl_streetAddressTextBox" runat="server" MaxLength="100"
        Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_cityLabel" Text="City" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
    
    <asp:TextBox ID="ClientControl_cityTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_stateLabel" Text="State" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
   
    <asp:TextBox ID="ClientControl_stateTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_zipCodeLabel" Text="Zipcode" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
  
    <asp:TextBox ID="ClientControl_zipCodeTextBox" runat="server" MaxLength="6" Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_countryLabel" Text="Country" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
   
    <asp:TextBox ID="ClientControl_countryTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_FEIN_NOLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="FEIN NO" Visible="false"></asp:Label>
   
    <asp:TextBox ID="ClientControl_FEIN_NO_TextBox" runat="server" MaxLength="9" Visible="false"></asp:TextBox>
    <ajaxToolKit:FilteredTextBoxExtender ID="ClientControl_FEIN_NO_TextBoxTextBoxExtender"
        runat="server" FilterType="Numbers" TargetControlID="ClientControl_FEIN_NO_TextBox">
    </ajaxToolKit:FilteredTextBoxExtender>
    <asp:Label ID="ClientControl_phoneNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Phone No" Visible="false"></asp:Label>
    <span class="mandatory" id="ClientControl_phoneNoSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="ClientControl_phoneNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_faxNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Fax No" Visible="false"></asp:Label>
   
    <asp:TextBox ID="ClientControl_faxNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_eMailIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Email ID" Visible="false"></asp:Label>
    <span class="mandatory" id="ClientControl_eMailIDSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="ClientControl_eMailIDTextBox" runat="server" MaxLength="200" Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_websiteURLLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Website URL" Visible="false"></asp:Label>
    
    <asp:TextBox ID="ClientControl_websiteURL_TextBox" runat="server" MaxLength="200"
        Visible="false"></asp:TextBox>
    <asp:Label ID="ClientControl_additionalInfoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Additional Info" Visible="false"></asp:Label>
  
    <asp:TextBox ID="ClientControl_additionalInfoTextBox" Columns="80" runat="server"
        MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
        onchange="CommentsCount(500,this)" Visible="false"></asp:TextBox>
    <!-- End Client Information Controls -->

    <!-- Department Controls -->
     <asp:Label ID="DepartmentControl_streetAddressLabel" Text="Street Address" runat="server"
        SkinID="sknLabelFieldHeaderText" Visible="false" EnableViewState="true"></asp:Label>
  
    <asp:TextBox ID="DepartmentControl_streetAddressTextBox" runat="server" MaxLength="100"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_cityLabel" Text="City" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false" EnableViewState="true"></asp:Label>
   
    <asp:TextBox ID="DepartmentControl_cityTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_stateLabel" Text="State" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
   
    <asp:TextBox ID="DepartmentControl_stateTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_zipCodeLabel" Text="Zipcode" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
    
    <asp:TextBox ID="DepartmentControl_zipCodeTextBox" runat="server" MaxLength="6" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_countryLabel" Text="Country" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
  
    <asp:TextBox ID="DepartmentControl_countryTextBox" runat="server" MaxLength="50"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_phoneNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Phone No" Visible="false"></asp:Label>
    <span class="mandatory" id="DepartmentControl_phoneNoSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="DepartmentControl_phoneNoTextBox" runat="server" MaxLength="50"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_faxNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Fax No" Visible="false"></asp:Label>
   
    <asp:TextBox ID="DepartmentControl_faxNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="DepartmentControl_eMailIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Email ID" Visible="false"></asp:Label>
    <span class="mandatory" id="DepartmentControl_eMailIDSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="DepartmentControl_eMailIDTextBox" runat="server" MaxLength="200"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_websiteURLLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Website URL" Visible="false"></asp:Label>
    
    <!-- End Controls -->
    <asp:Label ID="ContactControl_middleNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Middle Name" Visible="false" EnableViewState="true"></asp:Label>
   
    <asp:TextBox ID="ContactControl_middleNameTextBox" runat="server" MaxLength="50"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_lastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Lsat Name"></asp:Label>
    
    <asp:TextBox ID="ContactControl_lastNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
    <asp:Label ID="ContactControl_streetAddressLabel" Text="Street Address" runat="server"
        SkinID="sknLabelFieldHeaderText" Visible="false" EnableViewState="true"></asp:Label>
    
    <asp:TextBox ID="ContactControl_streetAddressTextBox" runat="server" MaxLength="100"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_cityLabel" Text="City" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false" EnableViewState="true"></asp:Label>
  
    <asp:TextBox ID="ContactControl_cityTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_stateLabel" Text="State" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
  
    <asp:TextBox ID="ContactControl_stateTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_zipCodeLabel" Text="Zipcode" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
    
    <asp:TextBox ID="ContactControl_zipCodeTextBox" runat="server" MaxLength="6" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_countryLabel" Text="Country" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
   
    <asp:TextBox ID="ContactControl_countryTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_phoneNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Phone No" Visible="false"></asp:Label>
    <span class="mandatory" id="ContactControl_phoneNoSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="ContactControl_phoneNoTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_faxNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Fax No" Visible="false"></asp:Label>
  
    <asp:TextBox ID="ContactControl_faxNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="ContactControl_eMailIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Email ID" Visible="false"></asp:Label>
    <span class="mandatory" id="ContactControl_eMailIDSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="ContactControl_eMailIDTextBox" runat="server" MaxLength="200" Visible="false"
        EnableViewState="true"></asp:TextBox>
</asp:Panel>
