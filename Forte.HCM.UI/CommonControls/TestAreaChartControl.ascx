﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestAreaChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestAreaChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultipleSeriesChartControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="TestAreaChartControl_chartUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <div style="width: 100%; overflow: auto">
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="TestAreaChartControl_testKeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="TestAreaChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="TestAreaChartControl_selectProperty" OnClick="TestAreaChartControl_widgetMultiSelectControl_Click" 
                                             OncancelClick="TestAreaChartControl_widgetMultiSelectControl_CancelClick" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:Label ID="TestAreaChartControl_selectScoreLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="TestAreaChartControl_selectOptionsAbsoluteScore" runat="server"
                                                    Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="True" OnCheckedChanged="TestAreaChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="TestAreaChartControl_selectOptionsRelativeScore" runat="server"
                                                    Text=" Relative Score" GroupName="1" AutoPostBack="True" OnCheckedChanged="TestAreaChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="TestAreaChartControl_showComparativeScoreLinkButton" runat="server"
                                                    Text="Show Comparative Score" SkinID="sknActionLinkButton" OnClick="TestAreaChartControl_showComparativeScoreLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="TestAreaChartControl_comparativeStateHiddenField" runat="server"
                                                    Value="0" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <uc1:MultipleSeriesChartControl ID="TestAreaChartControl_multipleSeriesChartControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
