﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

namespace Forte.HCM.UI.CommonControls
{
    public partial class ClientContactDetailView : System.Web.UI.UserControl
    {
        public delegate void Button_Command(object sender, CommandEventArgs e);
        public event Button_Command OkCommand;

        public delegate void RepeaterDepartment_Details(object sender, RepeaterCommandEventArgs e);
        public event RepeaterDepartment_Details OnRepeaterCommandClick;

        protected void ClientContactDetailView_ImageButton_Command(object sender, CommandEventArgs e)
        {
            if (ClientContactDetailView_departmentListRepeater.Controls != null && 
                ClientContactDetailView_departmentListRepeater.Controls.Count > 0)
            {
                HiddenField ClientContactDetailView_departmentIdHiddenField = 
                    (HiddenField)ClientContactDetailView_departmentListRepeater.
                    Controls[ClientContactDetailView_departmentListRepeater.Controls.Count - 1].
                    Controls[0].FindControl("ClientContactDetailView_departmentIdHiddenField");
                HiddenField_Deptid.Value = ClientContactDetailView_departmentIdHiddenField.Value;
                if (HiddenField_Deptid.Value == "")
                    HiddenField_Deptid.Value = "0";
            }
          //  Session["CLIENT_DEPT_ID"] = HiddenField_Deptid.Value;
            if (OkCommand != null)
            {
                this.OkCommand(sender, e);
            }
        }
        
        public ClientContactInformation ClientContactDetailViewDataSource
        {
            set
            {
                if (value == null)
                    return;
                string contactId = value.ContactID.ToString();
                ClientContactDetailView_editContactImageButton.CommandArgument = contactId;
                ClientContactDetailView_deleteContactImageButton.CommandArgument = contactId;
                ClientContactDetailView_viewClientImageButton.CommandArgument = contactId;
                ClientContactDetailView_viewDepartmentImageButton.CommandArgument = contactId;
                ClientContactDetailView_viewPositionProfileImageButton.CommandArgument = contactId;
                ClientContactDetailView_createPositionProfileImageButton.CommandArgument = contactId;

                ClientContactDetailView_editContactImageButton.CommandName = Constants.ClientManagementConstants.EDIT_CONTACT;
                ClientContactDetailView_deleteContactImageButton.CommandName = Constants.ClientManagementConstants.DELETE_CONTACT;
                ClientContactDetailView_viewClientImageButton.CommandName = Constants.ClientManagementConstants.VIEW_CONTACT_CLIENT;
                ClientContactDetailView_viewDepartmentImageButton.CommandName = Constants.ClientManagementConstants.VIEW_CONTACT_DEPARTMENT;
                ClientContactDetailView_viewPositionProfileImageButton.CommandName = Constants.ClientManagementConstants.VIEW_CONTACT_POSITION_PROFILE;
                ClientContactDetailView_createPositionProfileImageButton.CommandName = Constants.ClientManagementConstants.ADD_CONTACT_POSITION_PROFILE;

                ClientContactDetailView_clientNameLabel.Text = value.ClientName;
                ClientContactDetailView_clientDepartmentLabel.Text = value.ContactName;
                ClientContactDetailView_streetAddressLabel.Text = value.ContactInformation.StreetAddress;
                ClientContactDetailView_eMailIDLabel.Text = value.ContactInformation.EmailAddress;
                if (value.ContactInformation.Phone != null)
                    ClientContactDetailView_phoneNoLabel.Text = value.ContactInformation.Phone.Mobile;
                ClientContactDetailView_cityLabel.Text = value.ContactInformation.City;
                ClientContactDetailView_statelabel.Text = value.ContactInformation.State;
                ClientContactDetailView_zipLabel.Text = value.ContactInformation.PostalCode;
                ClientContactDetailView_countryLabel.Text = value.ContactInformation.Country;
                ClientContactDetailView_faxNoLabel.Text = value.FaxNo;
                ClientContactDetailView_additionalInfoLabel.Text = value.AdditionalInfo == null ? 
                    value.AdditionalInfo : value.AdditionalInfo.ToString().Replace(Environment.NewLine, "<br />");
                ClientContactDetailView_departmentListRepeater.DataSource = value.Departments;
                ClientContactDetailView_departmentListRepeater.DataBind();
            }
        }


        public NomenclatureCustomize ContactNomenclatureAttribute
        {
            set
            {
                ClientContactDetailView_clientNameLabel.ToolTip = value.ClientName;
                ClientContactDetailView_clientDepartmentLabel.ToolTip = value.DepartmentName;
                ClientContactDetailView_phoneNoHeadLabel.Text = value.PhoneNumber;
                ClientContactDetailView_faxNoHeadLabel.Text = value.FaxNo;
                ClientContactDetailView_zipHeadLabel.Text = value.Zipcode;
                ClientContactDetailView_streetAddressHeadLabel.Text = value.StreetAddress;
                ClientContactDetailView_cityHeadLabel.Text = value.City;
                ClientContactDetailView_stateHeadLabel.Text = value.state;
                ClientContactDetailView_countryHeadLabel.Text = value.Country;
                ClientContactDetailView_additionalInfoHeadLabel.Text = value.AdditionalInfo;
                ClientContactDetailView_departmentListLabel.Text = value.Departments;
            }

        }
        public bool DeleteButtonVisibility
        {
            set
            {
                ClientContactDetailView_deleteContactImageButton.Visible = value;
            }
        }
               

        protected void ClientContactDetailView_departmentListRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (OnRepeaterCommandClick != null)
                this.OnRepeaterCommandClick(source, e);

        }
    }
}