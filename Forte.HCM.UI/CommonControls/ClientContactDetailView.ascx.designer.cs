﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class ClientContactDetailView {
        
        /// <summary>
        /// ClientContactDetailView_clientNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_clientNameLabel;
        
        /// <summary>
        /// ClientContactDetailView_clientDepartmentLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_clientDepartmentLabel;
        
        /// <summary>
        /// ClientContactDetailView_eMailIDLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_eMailIDLabel;
        
        /// <summary>
        /// ClientContactDetailView_editContactImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientContactDetailView_editContactImageButton;
        
        /// <summary>
        /// ClientContactDetailView_deleteContactImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientContactDetailView_deleteContactImageButton;
        
        /// <summary>
        /// ClientContactDetailView_viewClientImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientContactDetailView_viewClientImageButton;
        
        /// <summary>
        /// ClientContactDetailView_viewDepartmentImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientContactDetailView_viewDepartmentImageButton;
        
        /// <summary>
        /// ClientContactDetailView_viewPositionProfileImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientContactDetailView_viewPositionProfileImageButton;
        
        /// <summary>
        /// ClientContactDetailView_createPositionProfileImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ClientContactDetailView_createPositionProfileImageButton;
        
        /// <summary>
        /// ClientContactDetailView_phoneNoHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_phoneNoHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_phoneNoLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_phoneNoLabel;
        
        /// <summary>
        /// ClientContactDetailView_faxNoHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_faxNoHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_faxNoLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_faxNoLabel;
        
        /// <summary>
        /// ClientContactDetailView_zipHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_zipHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_zipLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_zipLabel;
        
        /// <summary>
        /// ClientContactDetailView_streetAddressHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_streetAddressHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_streetAddressLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_streetAddressLabel;
        
        /// <summary>
        /// ClientContactDetailView_cityHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_cityHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_cityLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_cityLabel;
        
        /// <summary>
        /// ClientContactDetailView_stateHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_stateHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_statelabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_statelabel;
        
        /// <summary>
        /// ClientContactDetailView_countryHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_countryHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_countryLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_countryLabel;
        
        /// <summary>
        /// ClientContactDetailView_additionalInfoHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_additionalInfoHeadLabel;
        
        /// <summary>
        /// ClientContactDetailView_departmentListLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_departmentListLabel;
        
        /// <summary>
        /// ClientContactDetailView_additionalInfoLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ClientContactDetailView_additionalInfoLabel;
        
        /// <summary>
        /// HiddenField_Deptid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField HiddenField_Deptid;
        
        /// <summary>
        /// ClientContactDetailView_departmentListRepeater control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater ClientContactDetailView_departmentListRepeater;
    }
}
