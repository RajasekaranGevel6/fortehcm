﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PositionProfileMenuControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.PositionProfileMenuControl" %>
<table id="tblMenuControl" width="100%" border="0" cellspacing="0" cellpadding="0" runat="server">
    <tr>
        <td class="menustrip_normal">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdClientManagement" onmouseover="showit(0)" onmouseout="resetit(event)">
                        Client Management
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdPositionProfile" onmouseover="showit(1)" onmouseout="resetit(event)">
                        Job description
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdAdmin" onmouseover="showit(2)" onmouseout="resetit(event)">
                        Admin
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="menustrip_sublink">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="4%">
                        &nbsp;
                    </td>
                    <td align="center" valign="middle" id="describe" onmouseover="clear_delayhide(true)"
                        onmouseout="resetit(event)">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="PositionProfileMenuControl_hoverMenuHiddenField" runat="server" />
<asp:HiddenField ID="PositionProfileMenuControl_selectedMenuHiddenField" runat="server" />
<asp:HiddenField ID="PositionProfileMenuControl_selectedSubMenuHiddenField" runat="server" />
<script type="text/javascript" language="javascript">
    var submenu = new Array();
    var test_url = '<%= absUrl %>';

    submenu[0] = '<div id="div0"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td01" class="menustrip_sublink_text"><a href="' + test_url + '/ClientCenter/ClientManagement.aspx?m=0&s=0&parentpage=MENU">Client Management</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td02" class="menustrip_sublink_text"><a href="' + test_url + '/ClientCenter/NomenclatureCustomization.aspx?m=0&s=1&parentpage=MENU">Nomenclature Customization</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td03" class="menustrip_sublink_text"><a href="' + test_url + '/ClientCenter/ClientManagementSettings.aspx?m=0&s=2&parentpage=MENU">Settings</a></td></tr></table></div>';
    submenu[1] = '<div id="div1"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td11" class="menustrip_sublink_text"><a href="' + test_url + '/PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0">Create Job description</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td12" class="menustrip_sublink_text"><a href="' + test_url + '/PositionProfile/SearchPositionProfile.aspx?m=1&s=1&parentpage=MENU">Search Job description</a></td><td>&nbsp;</td></tr></table></div>';
    submenu[2] = '<div id="div2"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td21" class="menustrip_sublink_text"><a href="' + test_url + '/PositionProfile/PositionProfileUserOptions.aspx?m=2&s=2">User Options</a></td></tr></table></div>';

    //submenu[2] = '<div id="div2"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td21" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/PositionProfileFormEntry.aspx?m=2&s=0">Create Form</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td22" class="menustrip_sublink_text"><a href="' + test_url + '/Admin/SearchPositionProfileForm.aspx?m=2&s=1&parentpage=MENU">Search Form</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td23" class="menustrip_sublink_text"><a href="' + test_url + '/PositionProfile/PositionProfileUserOptions.aspx?m=2&s=2">User Options</a></td></tr></table></div>';

    //Set delay before submenu disappears after mouse moves out of it (in milliseconds)
    var delay_hide = 500;

    //No need to edit beyond here
    var menuobj = document.getElementById ? document.getElementById("describe") : document.all ? document.all.describe : document.layers ? document.dep1.document.dep2 : "";

    function SubMenu(parentMenu, SubMenu)
    {
        selectedId = "td" + parentMenu.toString() + SubMenu.toString();
        if (document.getElementById("div0") != null)
        {
            parentControl = document.getElementById("div0");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }
        if (document.getElementById("div1") != null)
        {
            parentControl = document.getElementById("div1");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }
        if (document.getElementById("div2") != null)
        {
            parentControl = document.getElementById("div2");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById(selectedId) != null)
        {
            document.getElementById(selectedId).className = "menustrip_sublink_active_text";
        }
    }
    function showDefault()
    {
        if (document.getElementById("<%= PositionProfileMenuControl_selectedMenuHiddenField.ClientID %>").value != "")
        {
            val = document.getElementById("<%= PositionProfileMenuControl_selectedMenuHiddenField.ClientID %>").value;
            valSub = document.getElementById("<%= PositionProfileMenuControl_selectedSubMenuHiddenField.ClientID %>").value;
            if (val == 0)
            {
                menuobj.innerHTML = submenu[0];
                document.getElementById("tdClientManagement").className = "menustrip_active";
            }
            else if (val == 1)
            {
                menuobj.innerHTML = submenu[1];
                document.getElementById("tdPositionProfile").className = "menustrip_active";
            }
            else if (val == 2)
            {
                menuobj.innerHTML = submenu[2];
                document.getElementById("tdAdmin").className = "menustrip_active";
            }
            SubMenu(val, parseInt(valSub) + 1);
        }
        else
        {
            document.getElementById("tdClientManagement").className = "menutext_normal";
            document.getElementById("tdPositionProfile").className = "menutext_normal";
            document.getElementById("tdAdmin").className = "menutext_normal";
        }
    }

    function showit(which)
    {
        clear_delayhide(false);
        thecontent = (which == -1) ? "" : submenu[which];

        if (document.getElementById || document.all)
        {
            menuobj.innerHTML = thecontent;
            if (thecontent != "")
            {
                if (thecontent.toString().indexOf('div0') != -1)
                {
                    document.getElementById("<%= PositionProfileMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";
                    document.getElementById("tdClientManagement").className = "menustrip_active";
                    document.getElementById("tdPositionProfile").className = "menutext_normal";
                    document.getElementById("tdAdmin").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div1') != -1)
                {
                    document.getElementById("<%= PositionProfileMenuControl_hoverMenuHiddenField.ClientID %>").value = "1";

                    document.getElementById("tdClientManagement").className = "menutext_normal";
                    document.getElementById("tdPositionProfile").className = "menustrip_active";
                    document.getElementById("tdAdmin").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div2') != -1)
                {
                    document.getElementById("<%= PositionProfileMenuControl_hoverMenuHiddenField.ClientID %>").value = "2";

                    document.getElementById("tdClientManagement").className = "menutext_normal";
                    document.getElementById("tdPositionProfile").className = "menutext_normal";
                    document.getElementById("tdAdmin").className = "menustrip_active";
                }
                else
                {
                    document.getElementById("<%= PositionProfileMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";
                    document.getElementById("tdClientManagement").className = "menustrip_active";
                    document.getElementById("tdPositionProfile").className = "menutext_normal";
                    document.getElementById("tdAdmin").className = "menutext_normal";
                }
                if (document.getElementById("<%= PositionProfileMenuControl_selectedMenuHiddenField.ClientID %>").value != "")
                {
                    val = document.getElementById("<%= PositionProfileMenuControl_selectedMenuHiddenField.ClientID %>").value;
                    valSub = document.getElementById("<%= PositionProfileMenuControl_selectedSubMenuHiddenField.ClientID %>").value;

                    SubMenu(val, parseInt(valSub) + 1);
                }
            }
            else
            {
                document.getElementById("tdClientManagement").className = "menutext_normal";
                document.getElementById("tdPositionProfile").className = "menutext_normal";
                document.getElementById("tdAdmin").className = "menutext_normal";
                document.getElementById("<%= PositionProfileMenuControl_hoverMenuHiddenField.ClientID %>").value = "";

                showDefault();
            }

        }
        else if (document.layers)
        {
            menuobj.document.write(thecontent)
            menuobj.document.close()
        }
    }

    function resetit(e)
    {
        if (document.all && !menuobj.contains(e.toElement))
            delayhide = setTimeout("showit(-1)", delay_hide)
        else if (document.getElementById && e.currentTarget != e.relatedTarget && !contains_ns6(e.currentTarget, e.relatedTarget))
            delayhide = setTimeout("showit(-1)", delay_hide)
    }

    function clear_delayhide(isSubMenuHover)
    {
        if (window.delayhide)
            clearTimeout(delayhide);

        if (document.getElementById("<%= PositionProfileMenuControl_hoverMenuHiddenField.ClientID %>").value == "")
        {
            val = document.getElementById("<%= PositionProfileMenuControl_selectedMenuHiddenField.ClientID %>").value;
        }
        else
        {
            val = document.getElementById("<%= PositionProfileMenuControl_hoverMenuHiddenField.ClientID %>").value;
        }

        if (val == 0)
        {
            document.getElementById("tdClientManagement").className = "menustrip_active";
        }
        else if (val == 1)
        {
            document.getElementById("tdPositionProfile").className = "menustrip_active";
        }
        else if (val == 2)
        {
            document.getElementById("tdAdmin").className = "menustrip_active";
        }
    }

    function contains_ns6(a, b)
    {
        while (b.parentNode)
            if ((b = b.parentNode) == a)
                return true;
        return false;
    }
    
</script>
<script type="text/javascript" language="javascript">
    showDefault();
</script>
