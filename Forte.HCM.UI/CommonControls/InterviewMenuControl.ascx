﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterviewMenuControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.InterviewMenuControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="menustrip_normal">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdQuest" onmouseover="showit(0)" onmouseout="resetit(event)">
                        Question Repository
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdTest" onmouseover="showit(1)" onmouseout="resetit(event)">
                        Interview Maker
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdScheduler" onmouseover="showit(2)" onmouseout="resetit(event)">
                        Interview Scheduler
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdOnlineInterview" onmouseover="showit(3)" onmouseout="resetit(event)">
                        Online Interview
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdReport" onmouseover="showit(4)" onmouseout="resetit(event)">
                        Reports
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td class="menutext_normal" id="tdAssessor" onmouseover="showit(5)" onmouseout="resetit(event)">
                        Assessor
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="menustrip_sublink">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="4%">
                        &nbsp;
                    </td>
                    <td align="center" valign="middle" id="describe" onmouseover="clear_delayhide(true)"
                        onmouseout="resetit(event)">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="InterviewMenuControl_hoverMenuHiddenField" runat="server" />
<asp:HiddenField ID="InterviewMenuControl_selectedMenuHiddenField" runat="server" />
<asp:HiddenField ID="InterviewMenuControl_selectedSubMenuHiddenField" runat="server" />

<script type="text/javascript" language="javascript">
    var submenu = new Array();
    var test_url = '<%= absUrl %>';

    submenu[0] = '<div id="div0"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td01" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewQuestions/InterviewBatchQuestionEntry.aspx?m=0&s=0">Batch Interview Question Entry</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td02" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewQuestions/InterviewSingleQuestionEntry.aspx?m=0&s=1">Single Interview Question Entry</a></td><td class="menustrip_sublink_bullet">&nbsp;</td><td class="menustrip_sublink_text" id="td03"><a href="' + test_url + '/InterviewQuestions/SearchInterviewQuestion.aspx?m=0&s=2&parentpage=MENU">Search Interview Question</a></td><td class="menustrip_sublink_bullet">&nbsp;</td><td class="menustrip_sublink_text" id="td04"><a href="' + test_url + '/InterviewQuestions/ViewInterviewContributorSummary.aspx?m=0&s=3">View Contributor Summary</a></td><td>&nbsp;</td></tr></table></div>';
    submenu[1] = '<div id="div1"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td11" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0">Automated Generation</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td12" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewTestMaker/CreateManualInterviewTest.aspx?m=1&s=1">Manual Composition</a></td><td class="menustrip_sublink_bullet">&nbsp;</td><td id="td13" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewTestMaker/SearchInterviewTest.aspx?m=1&s=2&parentpage=MENU">Search Interview</a></td><td class="menustrip_sublink_bullet">&nbsp;</td><td id="td14" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewTestMaker/InterviewTestSession.aspx?m=1&s=3&parentpage=MENU">Interview Session</a></td><td>&nbsp;</td></tr></table></div>';
    submenu[2] = '<div id="div2"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td21" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewScheduler/InterviewTestScheduler.aspx?m=2&s=0&parentpage=MENU">Interview Scheduler</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td22" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&parentpage=MENU">Schedule Candidate</a></td><td>&nbsp;</td></tr></table></div>';
    submenu[3] = '<div id="div3"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td31" class="menustrip_sublink_text"><a href="' + test_url + '/OnlineInterview/OnlineInterviewCreation.aspx?m=3&s=0&parentpage=MENU">Create Interview</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td class="menustrip_sublink_text" id="td32"><a href="' + test_url + '/OnlineInterview/OnlineInterviewSession.aspx?m=3&s=1&parentpage=MENU">Search Interview</a></td><td>&nbsp;</td></tr></table></div>';
    submenu[4] = '<div id="div4"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td41" class="menustrip_sublink_text"><a href="' + test_url + '/InterviewReportCenter/InterviewTestReport.aspx?m=4&s=0&parentpage=MENU">Interview Report</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td class="menustrip_sublink_text" id="td42"><a href="' + test_url + '/InterviewAssessment/CandidateReport.aspx?m=4&s=1&parentpage=MENU">Candidate Report</a></td></tr></table></div>';
    submenu[5] = '<div id="div5"><table border="0" cellspacing="0" cellpadding="0"><tr><td>&nbsp;</td><td id="td51" class="menustrip_sublink_text"><a href="' + test_url + '/Assessments/MyAssessments.aspx?m=5&s=0&parentpage=MENU">My Assessments</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td52" class="menustrip_sublink_text"><a href="' + test_url + '/Assessor/MyAvailability.aspx?m=5&s=1&parentpage=MENU">My Availability</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td id="td53" class="menustrip_sublink_text"><a href="' + test_url + '/Assessor/SearchTimeSlotRequest.aspx?m=5&s=2&parentpage=MENU&type=P">My Pending Time Slot Requests</a></td><td valign="middle" align="center" class="menustrip_sublink_bullet"></td><td class="menustrip_sublink_text" id="td54"><a href="' + test_url + '/Assessor/SearchTimeSlotRequest.aspx?m=5&s=3&parentpage=MENU">Search Time Slot Request</a></td></tr></table></div>';

    //Set delay before submenu disappears after mouse moves out of it (in milliseconds)
    var delay_hide = 500;

    //No need to edit beyond here
    var menuobj = document.getElementById ? document.getElementById("describe") : document.all ? document.all.describe : document.layers ? document.dep1.document.dep2 : "";

    function SubMenu(parentMenu, SubMenu)
    {
        selectedId = "td" + parentMenu.toString() + SubMenu.toString();

        if (document.getElementById("div0") != null) 
        {
            parentControl = document.getElementById("div0");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2) 
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId) 
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById("div1") != null) 
        {
            parentControl = document.getElementById("div1");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2) 
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById("div2") != null) 
        {
            parentControl = document.getElementById("div2");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2) 
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId) 
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById("div3") != null) 
        {
            parentControl = document.getElementById("div3");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2) 
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId) 
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById("div4") != null)
        {
            parentControl = document.getElementById("div4");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById("div5") != null)
        {
            parentControl = document.getElementById("div5");
            tdElements = parentControl.getElementsByTagName("td");
            for (index = 1; index < tdElements.length; index = index + 2)
            {
                if (tdElements[index] != null && tdElements[index].id != selectedId)
                {
                    tdElements[index].className = "menustrip_sublink_text";
                }
            }
        }

        if (document.getElementById(selectedId) != null) 
        {
            document.getElementById(selectedId).className = "menustrip_sublink_active_text";
        }
    }

    function showDefault() 
    {
        if (document.getElementById("<%= InterviewMenuControl_selectedMenuHiddenField.ClientID %>").value != "") 
        {
            val = document.getElementById("<%= InterviewMenuControl_selectedMenuHiddenField.ClientID %>").value;
            valSub = document.getElementById("<%= InterviewMenuControl_selectedSubMenuHiddenField.ClientID %>").value;

            if (val == 0) 
            {
                menuobj.innerHTML = submenu[0];
                document.getElementById("tdQuest").className = "menustrip_active";
            }
            else if (val == 1) 
            {
                menuobj.innerHTML = submenu[1];
                document.getElementById("tdTest").className = "menustrip_active";
            }
            else if (val == 2) 
            {
                menuobj.innerHTML = submenu[2];
                document.getElementById("tdScheduler").className = "menustrip_active";
            }
            else if (val == 3)
            {
                menuobj.innerHTML = submenu[3];
                document.getElementById("tdOnlineInterview").className = "menustrip_active";
            }
            else if (val == 4) 
            {
                menuobj.innerHTML = submenu[4];
                document.getElementById("tdReport").className = "menustrip_active";
            }
            else if (val == 5)
            {
                menuobj.innerHTML = submenu[5];
                document.getElementById("tdAssessor").className = "menustrip_active";
            }

            SubMenu(val, parseInt(valSub) + 1);
        }
        else 
        {
            document.getElementById("tdQuest").className = "menutext_normal";
            document.getElementById("tdTest").className = "menutext_normal";
            document.getElementById("tdScheduler").className = "menutext_normal";
            document.getElementById("tdOnlineInterview").className = "menutext_normal";
            document.getElementById("tdReport").className = "menutext_normal";
            document.getElementById("tdAssessor").className = "menutext_normal";
        }
    }

    function showit(which)
    {
        clear_delayhide(false);
        thecontent = (which == -1) ? "" : submenu[which];

        if (document.getElementById || document.all) 
        {
            menuobj.innerHTML = thecontent;
            if (thecontent != "") 
            {
                if (thecontent.toString().indexOf('div0') != -1) 
                {
                    document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";
                    document.getElementById("tdQuest").className = "menustrip_active";
                    document.getElementById("tdTest").className = "menutext_normal";
                    document.getElementById("tdScheduler").className = "menutext_normal";
                    document.getElementById("tdOnlineInterview").className = "menutext_normal";
                    document.getElementById("tdReport").className = "menutext_normal";
                    document.getElementById("tdAssessor").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div1') != -1) 
                {
                    document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "1";
                    document.getElementById("tdTest").className = "menustrip_active";
                    document.getElementById("tdQuest").className = "menutext_normal";
                    document.getElementById("tdScheduler").className = "menutext_normal";
                    document.getElementById("tdOnlineInterview").className = "menutext_normal";
                    document.getElementById("tdReport").className = "menutext_normal";
                    document.getElementById("tdAssessor").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div2') != -1) 
                {
                    document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "2";
                    document.getElementById("tdScheduler").className = "menustrip_active";
                    document.getElementById("tdQuest").className = "menutext_normal";
                    document.getElementById("tdTest").className = "menutext_normal";
                    document.getElementById("tdOnlineInterview").className = "menutext_normal";
                    document.getElementById("tdReport").className = "menutext_normal";
                    document.getElementById("tdAssessor").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div3') != -1) 
                {
                    document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "3";
                    document.getElementById("tdOnlineInterview").className = "menustrip_active";
                    document.getElementById("tdQuest").className = "menutext_normal";
                    document.getElementById("tdTest").className = "menutext_normal";
                    document.getElementById("tdScheduler").className = "menutext_normal";
                    document.getElementById("tdReport").className = "menutext_normal";
                    document.getElementById("tdAssessor").className = "menutext_normal";

                }
                else if (thecontent.toString().indexOf('div4') != -1)
                {
                    document.getElementById("tdReport").className = "menustrip_active";
                    
                    document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "4";
                    document.getElementById("tdQuest").className = "menutext_normal";
                    document.getElementById("tdTest").className = "menutext_normal";
                    document.getElementById("tdScheduler").className = "menutext_normal";
                    document.getElementById("tdOnlineInterview").className = "menutext_normal";
                    document.getElementById("tdAssessor").className = "menutext_normal";
                }
                else if (thecontent.toString().indexOf('div5') != -1)
                {
                    document.getElementById("tdAssessor").className = "menustrip_active";
                    document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "5";
                    document.getElementById("tdQuest").className = "menutext_normal";
                    document.getElementById("tdTest").className = "menutext_normal";
                    document.getElementById("tdScheduler").className = "menutext_normal";
                    document.getElementById("tdOnlineInterview").className = "menutext_normal";
                    document.getElementById("tdReport").className = "menutext_normal";
                }
                else 
                {
                    document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "0";
                    document.getElementById("tdQuest").className = "menustrip_active";
                    document.getElementById("tdTest").className = "menutext_normal";
                    document.getElementById("tdScheduler").className = "menutext_normal";
                    document.getElementById("tdOnlineInterview").className = "menutext_normal";
                    document.getElementById("tdReport").className = "menutext_normal";
                    document.getElementById("tdAssessor").className = "menutext_normal";
                }
                if (document.getElementById("<%= InterviewMenuControl_selectedMenuHiddenField.ClientID %>").value != "") 
                {
                    val = document.getElementById("<%= InterviewMenuControl_selectedMenuHiddenField.ClientID %>").value;
                    valSub = document.getElementById("<%= InterviewMenuControl_selectedSubMenuHiddenField.ClientID %>").value;

                    SubMenu(val, parseInt(valSub) + 1);
                }
            }
            else 
            {
                document.getElementById("tdQuest").className = "menutext_normal";
                document.getElementById("tdTest").className = "menutext_normal";
                document.getElementById("tdScheduler").className = "menutext_normal";
                document.getElementById("tdOnlineInterview").className = "menutext_normal";
                document.getElementById("tdReport").className = "menutext_normal";
                document.getElementById("tdAssessor").className = "menutext_normal";

                document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value = "";

                showDefault();
            }
        }
        else if (document.layers) 
        {
            menuobj.document.write(thecontent)
            menuobj.document.close()
        }
    }

    function resetit(e) 
    {
        if (document.all && !menuobj.contains(e.toElement))
            delayhide = setTimeout("showit(-1)", delay_hide)
        else if (document.getElementById && e.currentTarget != e.relatedTarget && !contains_ns6(e.currentTarget, e.relatedTarget))
            delayhide = setTimeout("showit(-1)", delay_hide)
    }

    function clear_delayhide(isSubMenuHover)
    {
        if (window.delayhide)
            clearTimeout(delayhide);

        if (document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value == "") 
        {
            val = document.getElementById("<%= InterviewMenuControl_selectedMenuHiddenField.ClientID %>").value;
        }
        else 
        {
            val = document.getElementById("<%= InterviewMenuControl_hoverMenuHiddenField.ClientID %>").value;
        }

        if (val == 0) 
        {
            document.getElementById("tdQuest").className = "menustrip_active";
        }
        else if (val == 1) 
        {
            document.getElementById("tdTest").className = "menustrip_active";
        }
        else if (val == 2) 
        {
            document.getElementById("tdScheduler").className = "menustrip_active";
        }
        else if (val == 3)
        {
            document.getElementById("tdOnlineInterview").className = "menustrip_active";
        }
        else if (val == 4) 
        {
            document.getElementById("tdReport").className = "menustrip_active";
        }
        else if (val == 5)
        {
            document.getElementById("tdAssessor").className = "menustrip_active";
        }
    }

    function contains_ns6(a, b)
    {
        while (b.parentNode)
            if ((b = b.parentNode) == a)
                return true;
        return false;
    }
    
</script>
<script type="text/javascript" language="javascript">
    showDefault();
</script>
