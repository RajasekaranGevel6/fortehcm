﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// GroupAnalysisOverAllScoreChartControl.cs
// Control that represents the user interface and assign data source for the 
// GroupAnalysisOverAllScoreChartControl

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;
using System.Text;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class GroupAnalysisOverAllScoreChartControl : UserControl, IWidgetControl
    {
        #region Event Handler   
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.TrimEnd(',');
                GroupAnalysisOverAllScoreChartControl_absoluteScoreRadioButton.Checked = selectedProperty == "0" ? false : true;
                GroupAnalysisOverAllScoreChartControl_relativeScoreRadioButton.Checked = selectedProperty == "1" ? false : true;
                LoadChartDetails(ViewState["instance"] as WidgetInstance);
            }
        }                               
        /// <summary>
        /// Represents the method that is called on the 
        /// selected radio button changed 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GroupAnalysisOverAllScoreChartControl_absoluteScoreRadioButton_CheckedChanged
            (object sender, EventArgs e)
        {
            //Get the instance from the view state
            WidgetInstance instance = DashboardFramework.
                GetWidgetInstance(ViewState["instance"]);

            //Load the chart with the instance
            LoadChartDetails(instance);
        }
        #endregion Event Handler

        #region Private Methods                                                

        /// <summary>
        /// Represents the method to load the chart details 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the 
        /// widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            //Get the score details from the data base
            OverAllCandidateScoreDetail scoreDetail = new ReportBLManager().
                GetGroupAnalysisOverallScoreDetails(candidateReportDetail);

            if (scoreDetail == null)
                return;

            //Save the score details in the session 
            Session["CANDIDATESCOREDETAIL"] = scoreDetail;

            //Remove the candidate score details
            // scoreDetail.AbsoluteScoreDetails.RemoveAt(0);

            //Load the chart with the details
            LoadChart(instance);
        }

        /// <summary>
        /// Method used to bind the details to the chart
        /// </summary>
        /// <param name="scoreDetail">
        /// A<see cref="OverAllCandidateScoreDetail"/>that holds the 
        /// candidate and overall score details
        /// </param>
        private void LoadChart(WidgetInstance instance)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            CandidateReportDetail candidateReportDetail = null;

            OverAllCandidateScoreDetail scoreDetail = null;

            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] 
                    as CandidateReportDetail;

            if (!Utility.IsNullOrEmpty(Session["CANDIDATESCOREDETAIL"]))
                scoreDetail = Session["CANDIDATESCOREDETAIL"] 
                    as OverAllCandidateScoreDetail;

            if (scoreDetail == null)
                return;

            scoreDetail.ChartScoreDetails = new SingleChartData();

            //Assign the chart properties 
            scoreDetail.ChartScoreDetails.ChartWidth = 325;

            scoreDetail.ChartScoreDetails.ChartType = SeriesChartType.Column;

            scoreDetail.ChartScoreDetails.IsDisplayChartTitle = true;

            scoreDetail.ChartScoreDetails.ChartTitle = "Total Questions Authored";

            scoreDetail.ChartScoreDetails.IsDisplayAxisTitle = true;

            scoreDetail.ChartScoreDetails.XAxisTitle = "Values";

            scoreDetail.ChartScoreDetails.YAxisTitle = "Scores";

            scoreDetail.ChartScoreDetails.IsDisplayLegend = false;

            scoreDetail.ChartScoreDetails.IsDisplayChartTitle = false;

            scoreDetail.ChartScoreDetails.ChartTitle = "Candidate's Overall Test Score Detail";

            scoreDetail.ChartScoreDetails.IsShowLabel = true;

            List<DropDownItem> dropDownItemList = new List<DropDownItem>();

            //If absolute score radion button is clicked show absolute score
            if (GroupAnalysisOverAllScoreChartControl_absoluteScoreRadioButton.Checked)
            {
                selectedPrintProperty.Append("1");
                scoreDetail.ChartScoreDetails.ChartData = 
                    scoreDetail.AbsoluteScoreDetails;
                scoreDetail.ChartScoreDetails.ChartImageName = 
                    Constants.ChartConstants.GROUP_REPORT_OVERALL_ABSOLUTE 
                    + "-" + candidateReportDetail.TestKey + "-" +
                    candidateReportDetail.CandidateSessionkey.Replace
                    (',', '-') + "-" + candidateReportDetail.AttemptsID.
                    Replace(',', '-');

                dropDownItemList.Add(new DropDownItem("Title",
                    "Absolute Comparative Score"));

                dropDownItemList.Add(new DropDownItem("ChratImagePath",
                    Constants.ChartConstants.GROUP_REPORT_OVERALL_ABSOLUTE
                    + "-" + candidateReportDetail.TestKey + "-" +
                    candidateReportDetail.CandidateSessionkey.Replace
                    (',', '-') + "-" + candidateReportDetail.AttemptsID.
                    Replace(',', '-') + ".png"));
            }
            //else show relative score
            else
            {
                selectedPrintProperty.Append("0");
                //Show the rlative score . assign relative score 
                //details as chart data 
                scoreDetail.ChartScoreDetails.ChartData = scoreDetail.
                    RelativeScoreDetails;
               
                scoreDetail.ChartScoreDetails.ChartImageName = 
                    Constants.ChartConstants.GROUP_REPORT_OVERALL_RELATIVE 
                    + "-" + candidateReportDetail.TestKey + "-" +
                    candidateReportDetail.CandidateSessionkey.Replace
                    (',', '-') + "-" + candidateReportDetail.AttemptsID.
                    Replace(',', '-');

                dropDownItemList.Add(new DropDownItem("Title",
                    "Relative Comparative Score"));

                dropDownItemList.Add(new DropDownItem("ChratImagePath", 
                    Constants.ChartConstants.GROUP_REPORT_OVERALL_RELATIVE
                    + "-" + candidateReportDetail.TestKey + "-" + 
                    candidateReportDetail.CandidateSessionkey.Replace(',', '-')
                    + "-" + candidateReportDetail.AttemptsID.Replace
                    (',', '-') + ".png"));
            }

            //Finally assign the datasource for the chart control
            GroupAnalysisOverAllScoreChartControl_scoreChart.
                DataSource = scoreDetail.ChartScoreDetails;

            WidgetMultiSelectControl.
                WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.
                WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }
        #endregion Private Methods                                             

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param> 
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            // throw new NotImplementedException();
        }

        #endregion
    }
}