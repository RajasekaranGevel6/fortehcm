﻿using System;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class InterviewTestSessionPreviewControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Do Nothing
        }
        /// <summary>
        /// This property assigns the test session details to labels.
        /// </summary>
        public InterviewTestSessionDetail DataSource
        {
            set
            {
                if (value == null)
                    return;

                TestSessionPreviewControl_testKeyLabel.Text = value.TestID;
                TestSessionPreviewControl_testNameLabel.Text = value.TestName;
                TestSessionPreviewControl_sessionKeyLabel.Text = value.TestSessionID;
                TestSessionPreviewControl_sessionNoLabel.Text = value.NumberOfCandidateSessions.ToString();
                TestSessionPreviewControl_sessionDescLiteral.Text = value.TestSessionDesc == null ? value.TestSessionDesc :
                    value.TestSessionDesc.ToString().Replace(Environment.NewLine, "<br />");
                    
                //TestSessionPreviewControl_creditLabel.Text = value.TotalCredit.ToString();
                TestSessionPreviewControl_positionProfileLabel.Text = value.PositionProfileName;
                    
                //TestSessionPreviewControl_timeLimitLabel.Text = 
                //    Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(value.TimeLimit.ToString()));
                    
                //TestSessionPreviewControl_recommendedTimeLabel.Text = 
                //    Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(value.RecommendedTimeLimit.ToString()));
                TestSessionPreviewControl_expiryDateLabel.Text = GetDateFormat(Convert.ToDateTime(value.ExpiryDate.ToString()));
                //TestSessionPreviewControl_randomSelectionTextLabel.Text = (value.IsRandomizeQuestionsOrdering ? "Yes" : "No");
                
                //if(value.AllowPauseInterview.ToString().Trim() == "Y")
                //TestSessionPreviewControl_allowResultsFieldLabel.Text = "Yes";
                //else
                // TestSessionPreviewControl_allowResultsFieldLabel.Text = "No";

                //TestSessionPreviewControl_cyberProctorateFieldLabel.Text = (value.IsCyberProctoringEnabled ? "Yes" : "No");
                TestSessionPreviewControl_instructionsLiteral.Text = 
                    value.Instructions == null ? value.Instructions : value.Instructions.ToString().Replace(Environment.NewLine, "<br />");

                if (mode != "view")
                {
                   // TestSessionPreviewControl_allowPauseInterviewResultsCheckBox.Visible = false;

                    //TestSessionPreviewControl_randomSelectionCheckBox.Checked = value.IsRandomizeQuestionsOrdering;
                    //TestSessionPreviewControl_displayResultsCheckBox.Checked = value.IsDisplayResultsToCandidate;
                    //TestSessionPreviewControl_cyberProctorateCheckBox.Checked = value.IsCyberProctoringEnabled;
                }
            }
        }
        /// <summary>
        /// This method returns the date in MM/dd/yyyy format. If date value is not given,
        /// then it returns empty.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }
        /// <summary>
        /// This property stores either edit or view.
        /// </summary>
        private string mode;

        public string Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
                //if (value.ToLower() == "view")
                //{
                //    TestSessionPreviewControl_allowPauseInterviewResultsCheckBox.Visible = false;
                //    TestSessionPreviewControl_allowResultsFieldLabel.Visible = true;
                //}
                //else
                //{

                //}
            }
        }
    }
}