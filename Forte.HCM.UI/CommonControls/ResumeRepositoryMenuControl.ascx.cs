﻿using System;
using System.Web.UI;

namespace Forte.HCM.UI.CommonControls
{
    public partial class ResumeRepositoryMenuControl : UserControl
    {
        public string absUrl = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            // To get the host from the url.
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if (uri.Length > 3)
                absUrl = uri[0] + "/" + uri[1] + "/" + uri[2] ;

            if (Request.QueryString["m"] != null)
            {
                ResumeRepositoryMenuControl_selectedMenuHiddenField.Value = Convert.ToString(Request.QueryString["m"]);
            }
            if (Request.QueryString["s"] != null)
            {
                ResumeRepositoryMenuControl_selectedSubMenuHiddenField.Value = Convert.ToString(Request.QueryString["s"]);
            }
            //if (Session["USER_DETAIL"] != null)
            //{
            //    tdAdmin.Visible = (Session["USER_DETAIL"] as UserDetail).IsAdmin;
            //    tdAdminSeparator.Visible = tdAdmin.Visible;
            //}
        }
    }
}