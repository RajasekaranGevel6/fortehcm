﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class InterviewSessionPreviewControl {
        
        /// <summary>
        /// InterviewSessionPreviewControl_questionResultLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal InterviewSessionPreviewControl_questionResultLiteral;
        
        /// <summary>
        /// InterviewSessionPreviewControl_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton InterviewSessionPreviewControl_topCancelImageButton;
        
        /// <summary>
        /// InterviewSessionPreviewControl_testKeyHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_testKeyHeadLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_testKeyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_testKeyLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_testNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_testNameHeadLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_testNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_testNameLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_sessionKeyHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_sessionKeyHeadLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_sessionKeyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_sessionKeyLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_sessionNoHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_sessionNoHeadLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_sessionNoLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_sessionNoLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_expiryDateHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_expiryDateHeadLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_expiryDateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_expiryDateLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_positionProfileHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_positionProfileHeadLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_positionProfileLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_positionProfileLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_sessionDescLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_sessionDescLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_sessionDescLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal InterviewSessionPreviewControl_sessionDescLiteral;
        
        /// <summary>
        /// InterviewSessionPreviewControl_instructionsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewSessionPreviewControl_instructionsLabel;
        
        /// <summary>
        /// InterviewSessionPreviewControl_instructionsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal InterviewSessionPreviewControl_instructionsLiteral;
    }
}
