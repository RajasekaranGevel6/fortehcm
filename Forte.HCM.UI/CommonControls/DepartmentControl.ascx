﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DepartmentControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.DepartmentControl" EnableViewState="true" %>
<asp:Panel ID="DepartmentControl_mainPanel" runat="server" DefaultButton="DepartmentControl_createButton">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="DepartmentControl_messagetitleLiteral" runat="server">Department</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="DepartmentControl_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="department_details_modal_popup_bg">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr id="DepartmentControl_messageTd" runat="server" class="td_height_20">
                                    <td>
                                        <asp:Label ID="DepartmentControl_topErrorMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                        <asp:Label ID="DepartmentControl_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Panel ID="DepartmentControl_panel" runat="server" EnableViewState="true">
                                            <table border="0" cellspacing="3" cellpadding="3">
                                                <tr id="DepartmentControl_clientNameTR" runat="server">
                                                    <td>
                                                        <asp:Label ID="DepartmentControl_ClientName" runat="server" Text="Client Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <span class="mandatory">*</span>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:DropDownList ID="DepartmentControl_ClientNameDropDownList" runat="server" OnSelectedIndexChanged="DepartmentControl_ClientNameDropDownList_SelectedIndexChanged"
                                                            AutoPostBack="true" AppendDataBoundItems="true">
                                                            <asp:ListItem Text="--Sellect--" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="DepartmentControl_departmentNameLabel" Text="Department Name" runat="server"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <span class="mandatory" id="DepartmentControl_departmentNamespan" runat="server">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="DepartmentControl_departmentNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="DepartmentControl_primaryAddressCheckbox" Text="Primary Address"
                                                            EnableViewState="true" Visible="false" runat="server" AutoPostBack="True" OnCheckedChanged="DepartmentControl_primaryAddressCheckbox_CheckedChanged" />
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="DepartmentControl_descriptionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Description" Visible="false" EnableViewState="false"></asp:Label>
                                                       
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="DepartmentControl_descriptionTextBox" Columns="75" runat="server"
                                                            MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
                                                            onchange="CommentsCount(500,this)" EnableViewState="false" Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="DepartmentControl_td5" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td6" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td7" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td8" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td9" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td10" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td11" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td12" runat="server">
                                                    </td>
                                                </tr>
                                                <%--  <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td13" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td14" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td15" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td16" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td17" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td18" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td19" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td20" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="DepartmentControl_td21" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td22" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td23" runat="server">
                                                    </td>
                                                    <td id="DepartmentControl_td24" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="DepartmentControl_additionalInfoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Additional Info" Visible="false"></asp:Label>
                                                        <span class="mandatory" id="DepartmentControl_additionalInfoSpan" runat="server" visible="false">
                                                            *</span>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="DepartmentControl_additionalInfoTextBox" Columns="75" runat="server"
                                                            MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
                                                            onchange="CommentsCount(500,this)" Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" align="left">
                <asp:Button ID="DepartmentControl_createButton" runat="server" Text="Save" SkinID="sknButtonId"
                    ValidationGroup="a" OnCommand="DepartmentControl_createButton_Command" />
                &nbsp; &nbsp;
                <asp:LinkButton ID="DepartmentControl_featureCancelLinkButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton">
                </asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="DepartmentControl_clientIDHiddenField" runat="server" />
    <asp:Label ID="DepartmentControl_streetAddressLabel" Text="Street Address" runat="server"
        SkinID="sknLabelFieldHeaderText" Visible="false" EnableViewState="true"></asp:Label>
  
    <asp:TextBox ID="DepartmentControl_streetAddressTextBox" runat="server" MaxLength="100"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_cityLabel" Text="City" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false" EnableViewState="true"></asp:Label>
   
    <asp:TextBox ID="DepartmentControl_cityTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_stateLabel" Text="State" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
    
    <asp:TextBox ID="DepartmentControl_stateTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_zipCodeLabel" Text="Zipcode" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
    
    <asp:TextBox ID="DepartmentControl_zipCodeTextBox" runat="server" MaxLength="6" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_countryLabel" Text="Country" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
    
    <asp:TextBox ID="DepartmentControl_countryTextBox" runat="server" MaxLength="50"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_phoneNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Phone No" Visible="false"></asp:Label>
    <span class="mandatory" id="DepartmentControl_phoneNoSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="DepartmentControl_phoneNoTextBox" runat="server" MaxLength="50"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_faxNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Fax No" Visible="false"></asp:Label>
  
    <asp:TextBox ID="DepartmentControl_faxNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="DepartmentControl_eMailIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Email ID" Visible="false"></asp:Label>
    <span class="mandatory" id="DepartmentControl_eMailIDSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="DepartmentControl_eMailIDTextBox" runat="server" MaxLength="200"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="DepartmentControl_websiteURLLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Website URL" Visible="false"></asp:Label>
    
</asp:Panel>
