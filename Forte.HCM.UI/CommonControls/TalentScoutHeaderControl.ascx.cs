﻿#region Namespace                                                              

using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Collections.Generic;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class TalentScoutHeaderControl : UserControl
    {
        #region Variables                                                      

        public string homeUrl = string.Empty;
        public string landingUrl = string.Empty;
        public string userOptionsUrl = string.Empty;
        public string helpUrl = string.Empty;

        #endregion Variables

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 3)
                {
                    // Compose home url.
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Default.aspx";

                    // Compose dashboard url.
                    landingUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Landing.aspx";

                    // Compose user options url.
                    userOptionsUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Settings/UserOptions.aspx";

                    // Compose help url.
                    helpUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Help.aspx";
                }

                //Page.Form.DefaultButton = HomePage_goImageButton.UniqueID;
                //Page.Form.DefaultFocus = HomePage_userNametextbox.UniqueID;
                string PageName = Request.Url.AbsoluteUri.Split('/')[Request.Url.AbsoluteUri.Split('/').Length - 1];
                if (PageName.Contains("?"))
                    PageName = PageName.Substring(0, PageName.IndexOf('?'));

                // TODO - This needs to reviewed.
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    HeaderControl_changePasswordLinkButton.Visible = true;
                }
                else
                {
                    HeaderControl_changePasswordLinkButton.Visible = false;
                }

                if (IsPostBack)
                    return;
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    HeaderControl_loggedInTable.Visible = true;
                    HeaderControl_loginTable.Visible = false;
                    //HeaderControl_logoLoginTD.Visible = false;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HeaderControl_loginUserNameLabel.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                    HeaderControl_lastLoginDateTimeLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
                }
                else
                {
                    HeaderControl_loggedInTable.Visible = false;
                    HeaderControl_loginTable.Visible = true;
                    //HeaderControl_logoLoginTD.Visible = true;
                    //6FormsAuthentication.RedirectToLoginPage();
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void HomePage_logoImage_Home_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // To get the host from the url.


            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void HeaderControl_logoutButton_Click(object sender, EventArgs e)
        {
            Session["USER_DETAIL"] = null;
            Session.Abandon();
            HeaderControl_loginUserNameLabel.Text = "Guest";
            FormsAuthentication.SignOut();
            //Response.Redirect("~/Home.aspx?a=1", false);
            // FormsAuthentication.RedirectToLoginPage();
            // Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["SITE_URL"], false);
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if (uri.Length > 4)
            {
                // Compose home url.

                Response.Redirect(uri[0] + "/" + uri[1] + System.Configuration.ConfigurationManager.AppSettings["SITE_URL"], false);
            }

        }

        protected void HomePage_signUpImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(@"~/Subscription/SubscriptionType.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void HomePage_goImageButton_Click(object sender, ImageClickEventArgs e)
        {
            UserDetail userDetails = null;
            try
            {
                if (this.IsValidData())
                {
                    userDetails = AuthenticateUserDetails(HomePage_userNametextbox.Text.Trim(),
                        HomePage_passwordtextbox.Text.Trim());

                    //if (Utility.IsNullOrEmpty(userDetails.Roles))
                    if (Utility.IsNullOrEmpty(userDetails) || userDetails.UserID == 0)
                    {
                        ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                        return;
                    }
                    Session["USER_DETAIL"] = userDetails;
                    if (Request.Params["ReturnUrl"] != null)
                    {
                        FormsAuthentication.RedirectFromLoginPage(HomePage_userNametextbox.Text.Trim(), false);
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(HomePage_userNametextbox.Text.Trim(), false);
                        Response.Redirect("~/Landing.aspx", false);
                    }
                    //FormsAuthentication.SetAuthCookie(HomePage_userNametextbox.Text.Trim(), true);
                    //Response.Redirect("~/Home.aspx", false);
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Public Methods                                                 

        public List<string> BreadCrumbText
        {
            set
            {
                List<string> breadCrumbList = (List<string>)value;

                if (breadCrumbList != null)
                {
                    foreach (string breadCrumbText in breadCrumbList)
                    {
                        //if (string.IsNullOrEmpty(HeaderControl_breadCrumbLabel.Text.Trim()))
                        //{
                        //    HeaderControl_breadCrumbLabel.Text = breadCrumbText;
                        //}
                        //else
                        //{
                        //    HeaderControl_breadCrumbLabel.Text += " &raquo; " + breadCrumbText;
                        //}
                    }
                }
            }
        }

        #endregion Public Methods

        #region Private Methods                                                

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message to be 
        /// show to the user.
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            if (string.IsNullOrEmpty(HeaderControl_bottomErrorMessageLabel.Text))
                HeaderControl_bottomErrorMessageLabel.Text = Message;
            else
                HeaderControl_bottomErrorMessageLabel.Text += "<br />" + Message;
        }

        /// <summary>
        /// Shows the error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        /// <summary>
        /// Method that will return a userdetail object for the 
        /// given username and password.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the username.
        /// </param>
        /// <param name="Password">
        /// A <see cref="string"/> that contains the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> object that contains the 
        /// information for the given username and password.
        /// </returns>
        private UserDetail AuthenticateUserDetails(string UserName, string Password)
        {
            AuthenticationBLManager authenticationBLManager = new AuthenticationBLManager();
            Authenticate authenticate = new Authenticate();
            authenticate.UserName = UserName;
            authenticate.Password = new EncryptAndDecrypt().EncryptString(Password.Trim());
            UserDetail userDetails = new UserDetail();
            userDetails = authenticationBLManager.GetAuthenticateUser(authenticate);
            return userDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsValidData()
        {
            bool isValidData = true;
            if (HomePage_userNametextbox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_UserNameTextBoxEmpty);
                isValidData = false;
            }
            if (HomePage_passwordtextbox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_PasswordTextBoxEmpty);
                isValidData = false;
            }
            return isValidData;
        }

        #endregion Private Methods
    }
}