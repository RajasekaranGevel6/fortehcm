﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class InterviewMenuControl {
        
        /// <summary>
        /// InterviewMenuControl_hoverMenuHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField InterviewMenuControl_hoverMenuHiddenField;
        
        /// <summary>
        /// InterviewMenuControl_selectedMenuHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField InterviewMenuControl_selectedMenuHiddenField;
        
        /// <summary>
        /// InterviewMenuControl_selectedSubMenuHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField InterviewMenuControl_selectedSubMenuHiddenField;
    }
}
