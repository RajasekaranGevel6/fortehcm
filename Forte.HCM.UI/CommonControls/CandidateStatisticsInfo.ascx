﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateStatisticsInfo.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CandidateStatisticsInfo" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="CandidateStatisticsInfo_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="middle" style="height: 30px;">
                                                <div style="width: 100%; overflow: auto">
                                                    <uc2:WidgetMultiSelectControl ID="CandidateStatisticsInfo_WidgetMultiSelectControl"
                                                        runat="server" OnselectedIndexChanged="CandidateStatisticsInfo_selectProperty"
                                                        OnClick="CandidateStatisticsInfo_widgetMultiSelectControl_Click" OncancelClick="CandidateStatisticsInfo_widgetMultiSelectControl_CancelClick" />
                                                    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                                    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            
                                             <asp:DataList ID="CandidateStatisticsInfo_candidateDetailsDataList" runat="server"
                                                    RepeatColumns="1" RepeatDirection="Vertical"
                                                    Width="100%">
                                                    <ItemTemplate>
                                                        <table width="100%" cellpadding="3" cellspacing="3" border="0">
                                                            <tr>
                                                                <td>
                                                                    <div style="float: left">
                                                                        <asp:Label ID="CandidateStatisticsInfo_nameLiteral" runat="server" Text='<%# Eval("FirstName") %>'
                                                                            SkinID="sknLabelFieldWidgetHeaderTexts"></asp:Label></div>
                                                                    <div style="float: right">
                                                                        <asp:Label ID="CandidateStatisticsInfo_locationLiteral" runat="server" Text='<%# Eval("Address") %>'
                                                                            SkinID="sknLabelFieldWidgetHeaderText"></asp:Label></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:Label ID="CandidateStatisticsInfo_contactLiteral" runat="server" Text='<%# Eval("Phone") %>'
                                                                        SkinID="sknLabelFieldWidgetHeaderText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="label_multi_field_text_widget">
                                                                        <asp:Literal ID="CandidateStatisticsInfo_synopsisLiteral" runat="server" Text='<%#Eval("Synopsis") %>'></asp:Literal>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="354px" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_scoreSDHeadLabel" runat="server" Text="Test Date"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="CandidateStatisticsInfo_scoreSDLabelValue" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                    Text='<%# GetTime(Eval("TestDate").ToString()) %>'></asp:Label>
                                                                            </td>
                                                                             <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_avgTimeLabel" runat="server" Text="Average Time"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="CandidateStatisticsInfo_lowScoreLabelValue" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                    Text='<%#Eval("AverageTimeTaken") %>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                         <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_interviewDateLabel" runat="server" Text="Interview Date"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="CandidateStatisticsInfo_interviewDateLabelValue" runat="server" Text="TBD"
                                                                                    SkinID="sknLabelFieldWidgetText"></asp:Label>
                                                                            </td>
                                                                           
                                                                            
                                                                            <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_scoreRangeHeadLabel" runat="server" Text="QNS Answered (%)"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Label ID="CandidateStatisticsInfo_scoreRangeLabelValue" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                    Text='<%#Eval("AnsweredCorrectly") %>'></asp:Label>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_absoluteScoreLabel" runat="server" Text="Absolute Score"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Label ID="CandidateStatisticsInfo_avgTimeLabelValue" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                    Text='<%#Eval("AbsoluteScore") %>'></asp:Label>
                                                                            </td>
                                                                           
                                                                           
                                                                            <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_relativeScoreLabel" runat="server" Text="Relative Score"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="right" align="right">
                                                                                <asp:Label ID="CandidateStatisticsInfo_relativeScoreLabelValue" runat="server" Text='<%#Eval("RelativeScore") %>'
                                                                                    SkinID="sknLabelFieldWidgetText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           
                                                                            <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_percentileLabel" runat="server" Text="Percentile"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Label ID="CandidateStatisticsInfo_percentileLabelValue" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                    Text='<%#Eval("Percentile") %>'></asp:Label>
                                                                            </td>
                                                                             <td>
                                                                                <asp:Label ID="CandidateStatisticsInfo_highScoreHeadLabel" runat="server" Text="Total Time "
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="CandidateStatisticsInfo_highScoreLabelValue" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                    Text='<%#Eval("TimeTaken") %>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 42%">
                                                                                <asp:Label ID="CandidateStatisticsInfo_scoreCandidateSessionIDHeadLabel" runat="server"
                                                                                    Text="Session ID" SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="CandidateStatisticsInfo_scoreCandidateSessionIDLabelValue" runat="server"
                                                                                    SkinID="sknLabelFieldWidgetText" Text='<%# Eval("CandidateSessionID") %>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 48%">
                                                                                <asp:Label ID="CandidateStatisticsInfo_attemptIDHeadLabel" runat="server" Text="Attempt Number"
                                                                                    SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Label ID="CandidateStatisticsInfo_attemptIDLabelValue" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                    Text='<%#Eval("AttemptNumber") %>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                         <%--                               <tr>
                                                                            <td>
                                                                                <asp:Label ID="CandidateStatisticsInfoControl_interviewCommentsLabel" runat="server"
                                                                                    Text="Interview Comments" SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <div class="label_multi_field_text_widget">
                                                                                    <asp:Literal ID="CandidateStatisticsInfoControl_interviewCommentsLiteral" runat="server"
                                                                                        SkinID="sknLabelFieldWidgetText" Text="TBD"></asp:Literal>
                                                                                </div>
                                                                            </td>
                                                                        </tr>--%>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="cand_bg_dark" />
                                                    <AlternatingItemStyle CssClass="cand_bg_light" />
                                                </asp:DataList>
                                            
                                            
                                                <asp:DataList ID="CandidateStatisticsInfo_printCandidateDetailsDataList" runat="server"
                                                    RepeatColumns="1" RepeatDirection="Vertical" 
                                                    Width="100%">
                                                    <ItemTemplate>
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="3%" cellspacing="3%" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div style="float: left">
                                                                                    <asp:Label ID="CandidateStatisticsInfo_nameLiteral" runat="server" Text='<%# Eval("FirstName") %>'
                                                                                        SkinID="sknLabelFieldWidgetHeaderTexts"></asp:Label></div>
                                                                                <div style="float: right">
                                                                                    <asp:Label ID="CandidateStatisticsInfo_locationLiteral" runat="server" Text='<%# Eval("Address") %>'
                                                                                        SkinID="sknLabelFieldWidgetHeaderText"></asp:Label></div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="CandidateStatisticsInfo_contactLiteral" runat="server" Text='<%# Eval("Phone") %>'
                                                                                    SkinID="sknLabelFieldWidgetHeaderText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="label_multi_field_text_widget">
                                                                                    <asp:Literal ID="CandidateStatisticsInfo_synopsisLiteral" runat="server" Text='<%#Eval("Synopsis") %>'></asp:Literal>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <asp:DataList ID="CandidateStatisticsInfo_candidateDetailsDataList" runat="server"
                                                                            DataSource='<%# Eval("TestDetails") %>' RepeatLayout="Table" RepeatColumns="2"
                                                                            RepeatDirection="Horizontal">
                                                                            <HeaderTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="100%" cellpadding="0" cellspacing="1" border="0">
                                                                                            <tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <td>
                                                                                    <asp:Label ID="CandidateStatisticsInfo_scoreSDHeadLabel" runat="server" Text='<%# Eval("DisplayText") %>'
                                                                                        SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:Label ID="CandidateStatisticsInfo_scoreSDValueLabel" runat="server" SkinID="sknLabelFieldWidgetText"
                                                                                        Text='<%# Eval("ValueText") %>'></asp:Label>
                                                                                </td>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </tr> </table> </td> </tr>
                                                                            </FooterTemplate>
                                                                        </asp:DataList>
                                                                        <tr>
                                                                            <td class="td_h_line">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 5px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="cand_bg_dark" />
                                                    <AlternatingItemStyle CssClass="cand_bg_light" />
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:PageNavigator ID="CandidateStatisticsInfo_bottomPageNavigator1" runat="server" Visible="true"  />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
