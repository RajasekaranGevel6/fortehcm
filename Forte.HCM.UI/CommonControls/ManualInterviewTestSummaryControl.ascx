﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManualInterviewTestSummaryControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ManualInterviewTestSummaryControl" %>
<%@ Register Src="~/CommonControls/SingleSeriesChartControl.ascx" TagName="SingleSeriesChart"
    TagPrefix="uc1" %>
<asp:HiddenField ID="ManualInterviewTestSummaryControl_attribute_HiddenField" runat="server" />
<table cellpadding="0" cellspacing="0" width="100%" border="0">
    <tr>
        <td width="32%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    <asp:Literal ID="ManualInterviewTestSummaryControl_testSummaryLiteral" runat="server" Text="Interview Summary"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg" valign="middle">
                        <div style="width: 100%; height: 160px; overflow: auto;">
                            <table width="100%" cellpadding="2" cellspacing="4">
                                <tr>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_noOfQuestionHeadLabel" runat="server" Text="Number Of Questions"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_noOfQuestionLabel" runat="server" Text="0"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_costOfTestHeadLabel" runat="server" Text="Cost Of Interview (in terms of credits)"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_costOfTestLabel" runat="server" Text="0.00"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_avgComplexityHeadLabel" runat="server" Text="Average Level Of Complexity"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_avgComplexityLabel" runat="server" Text="--"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_avgTimeHeadLabel" runat="server" Text="Average Time Taken By Candidates"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ManualInterviewTestSummaryControl_avgTimeLabel" runat="server" Text="00:00:00"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td width="1%">
            &nbsp;
        </td>
        <td width="32%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    <asp:Literal ID="ManualInterviewTestSummaryControl_testAreaLiteral" runat="server" Text="Interview Area Statistics">
                                    </asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg">
                        <div style="width: 100%; height: 160px; overflow: auto;">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td>
                                        <uc1:SingleSeriesChart ID="ManualInterviewTestSummaryControl_testAreaChart" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td width="1%">
            &nbsp;
        </td>
        <td width="32%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    Complexity Statistics
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg">
                        <div style="height: 160px; overflow: auto;">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td>
                                        <uc1:SingleSeriesChart ID="ManualTestSummary_complexityChart" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
