﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineInterviewMenuControl.cs
// File that represents the user to navigate the online interview pages.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Represents the class to navigate the online interview menus
    /// </summary>
    public partial class OnlineInterviewMenuControl : System.Web.UI.UserControl
    {
        #region Event Handlers                                                 
        
        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {    
               /*
                Interview Creation - IC
                Question Set - QS
                Assessors - AS
                Schedule Candidate - SC
                */

            if (Request.QueryString["interviewkey"] != null)
            {
                string interviewkey = Request.QueryString["interviewkey"];
                OnlineInterviewMenuControl_biAnchor.HRef = "../OnlineInterview/OnlineInterviewCreation.aspx?m=3&s=0&interviewkey=" + interviewkey + "&parentpage=S_OITSN";
                OnlineInterviewMenuControl_diAnchor.HRef = "../OnlineInterview/OnlineInterviewQuestionSet.aspx?m=3&s=0&interviewkey=" + interviewkey + "&parentpage=S_OITSN";
                OnlineInterviewMenuControl_wsAnchor.HRef = "../OnlineInterview/OnlineInterviewAssessor.aspx?m=3&s=0&interviewkey=" + interviewkey + "&parentpage=S_OITSN";
                OnlineInterviewMenuControl_raAnchor.HRef = "../OnlineInterview/OnlineInterviewSchduleCandidate.aspx?m=3&s=0&interviewkey=" + interviewkey + "&parentpage=S_OITSN";
            }
            else
                OnlineInterviewMenuControl_biAnchor.HRef = "../OnlineInterview/OnlineInterviewCreation.aspx?m=3&s=0&parentpage=S_OITSN";
        }

        #endregion Event Handlers

        #region Public Methods                                                 

        /// <summary>
        /// Method that assign the navigate url to the selected menus
        /// </summary>
        public string PageType
        {
            set 
            {
                switch(value)
                {
                    case "IC":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/online_interview_step_1.png";
                        tdBI.Attributes.Add("class", "online_interview_menu_active");
                        break;
                    case "QS":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/online_interview_step_2.png";
                        tdDI.Attributes.Add("class", "online_interview_menu_active");
                        break;
                    case "AS":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/online_interview_step_3.png";
                        tdWS.Attributes.Add("class", "online_interview_menu_active");
                        break;
                    case "SC":
                        MainTd.Style["background-image"] = "../App_Themes/DefaultTheme/Images/online_interview_step_4.png";
                        tdRA.Attributes.Add("class", "online_interview_menu_active");
                        break;
                } 
            }
        }

        #endregion Public Methods
    }
}