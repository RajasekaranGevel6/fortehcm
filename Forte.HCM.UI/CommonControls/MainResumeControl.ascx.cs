﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MainResumeControl.cs
// File that represents the user interface for the Resume information details

#endregion Header

#region Directives                                                             

using System;

using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class MainResumeControl : System.Web.UI.UserControl
    {
        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Subscribe to message thrown events from user controls.
            MainResumeControl_educationControl.ControlMessageThrown += new EducationControl.ControlMessageThrownDelegate(UserControl_ControlMessageThrown);
            MainResumeControl_projectsControl.ControlMessageThrown += new ProjectsControl.ControlMessageThrownDelegate(UserControl_ControlMessageThrown);
            MainResumeControl_referencesControl.ControlMessageThrown += new ReferencesControl.ControlMessageThrownDelegate(UserControl_ControlMessageThrown);
        }

        void UserControl_ControlMessageThrown(object sender, Forte.HCM.EventSupport.ControlMessageEventArgs c)
        {
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, c);
        }
        #region Public Properties                                              
        public Name name     { set; get; }

        public ContactInformation ContactInfo
        {
            set;
            get;
        }

        public Education Education
        { set; get; }
        
        public String ExecutiveSummary
        {
            set;
            get;
        }

        public Project Project
        {
            set;
            get;
        }
       
        public TechnicalSkills TechnicalSkills
        {
            set;
            get;
        }
       
        public Reference Reference
        {
            set;
            get;
        }
       
        public Resume DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                MainResumeControl_contactInfoControl.DataSource = value.ContactInformation;
                MainResumeControl_educationControl.DataSource = value.Education;
                MainResumeControl_executiveSummaryControl.DataSource = value.ExecutiveSummary;
                MainResumeControl_nameControl.DataSource = value.Name;
                MainResumeControl_projectsControl.DataSource = value.Project;
                MainResumeControl_referencesControl.DataSource = value.References;
                MainResumeControl_technicalSkillsControl.DataSource = value.TechnicalSkills;
                //MainResumeControl_idValueLabel.Text = "";
               // MainResumeControl_nameValueLabel.Text = value.Name.FirstName + " " + value.Name.MiddleName + value.Name.LastName;
                
            }


        }
        #endregion Public Properties
    }
}