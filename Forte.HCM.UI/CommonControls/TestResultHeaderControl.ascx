﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestResultHeaderControl.ascx.cs" Inherits="Forte.HCM.UI.CommonControls.TestResultHeaderControl" %>
         <table width="100%" cellpadding="0" cellspacing="3" border="0">
                <tr>
                    <td style="width: 6%">
                        <asp:Label ID="TestResultHeaderControl_testIDLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                    </td>
                    <td style="width: 17%">
                        <asp:Label ID="TestResultHeaderControl_testIDValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                    </td>
                    <td style="width: 7%">
                        <asp:Label ID="TestResultHeaderControl_testNameLabel" runat="server" Text="Test Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                    </td>
                    <td style="width: 17%">
                        <asp:Label ID="TestResultHeaderControl_testNameValueTextBox" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                    </td>
                    <td style="width: 10%">
                        <asp:Label ID="TestResultHeaderControl_candidateNameLabel" runat="server" Text="Candidate Name"
                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                    </td>
                    <td>
                        <asp:HyperLink ID="TestResultHeaderControl_candidateNameValueHyperLink" runat="server" Text="" Width="60%"
                            SkinID="sknLabelFieldTextHyperLink" Target="_blank" ToolTip="Click here to view the candidate activity dashboard"></asp:HyperLink>
                    </td>
                    <td style="width: 13%">
                        <asp:Label ID="TestResultHeaderControl_creditsEarnedLabel" runat="server" Text="Credits Earned in ($)"
                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="TestResultHeaderControl_creditsEarnedValueTextBox" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                    </td>
                </tr>
            </table>