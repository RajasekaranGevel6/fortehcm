﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;

using Forte.HCM.BL;
using Forte.HCM.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using System.Data;

namespace Forte.HCM.UI.CommonControls
{
    public partial class SearchCategorySubjectControl : System.Web.UI.UserControl
    {
        DataTable dtSubject;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            DataTable dtCategory = new DataTable();
            dtCategory.Columns.Add("Id");
            dtCategory.Columns.Add("Name");
            dtCategory.Columns.Add("Selected");
            dtCategory.Rows.Add(1, "Technology/Industry", "True");
            dtCategory.Rows.Add(2, "Technology/Domain/Industry", "True");
            //dtCategory.Rows.Add(3, "Technology", "False");
            //dtCategory.Rows.Add(4, "Technology/Domain", "False");
            //dtCategory.Rows.Add(5, "Domain", "False");
            SearchCategorySubjectControl_categoryDataList.DataSource = dtCategory;
            SearchCategorySubjectControl_categoryDataList.DataBind();

            dtCategory = new DataTable();
            dtCategory.Columns.Add("Id");
            dtCategory.Columns.Add("Name");
            dtCategory.Columns.Add("Selected");
            dtCategory.Rows.Add(1, "Technology/Industry", "True");
            dtCategory.Rows.Add(2, "Technology/Domain/Industry", "True");

            SearchCategorySubjectControl_subjectDataList.DataSource = dtCategory;
            SearchCategorySubjectControl_subjectDataList.DataBind();

        }

        protected void SearchCategorySubjectControl_subjectDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            DropDownList SearchQuestion_subjectDropDownList = (DropDownList)
                e.Item.FindControl("SearchQuestion_subjectDropDownList");
            if (e.Item.ItemType == ListItemType.Item)
            {
                dtSubject = new DataTable();
                dtSubject.Columns.Add("Name");
                dtSubject.Rows.Add("ETL Concepts");
                dtSubject.Rows.Add("Data Integration Concepts");
                SearchQuestion_subjectDropDownList.DataSource = dtSubject;
                SearchQuestion_subjectDropDownList.DataTextField = "Name";
                SearchQuestion_subjectDropDownList.DataBind();
            }
            if (e.Item.ItemType == ListItemType.AlternatingItem)
            {
                dtSubject = new DataTable();
                dtSubject.Columns.Add("Name");
                dtSubject.Rows.Add("Informatica Developer");
                dtSubject.Rows.Add("Informatica Testing");
                SearchQuestion_subjectDropDownList.DataSource = dtSubject;
                SearchQuestion_subjectDropDownList.DataTextField = "Name";
                SearchQuestion_subjectDropDownList.DataBind();
            }

        }
    }
}