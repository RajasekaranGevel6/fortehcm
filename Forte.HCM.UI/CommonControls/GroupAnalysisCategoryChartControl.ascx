﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupAnalysisCategoryChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GroupAnalysisCategoryChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultiSeriesChart"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="GroupAnalysisCategoryChartControl_updatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div style="width: 100%; overflow: auto">
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                         <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="GroupAnalysisCategoryChartControl_testKeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="GroupAnalysisCategoryChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="GroupAnalysisCategoryChartControl_selectProperty"
                                            OnClick="GroupAnalysisCategoryChartControl_widgetMultiSelectControl_Click" 
                                            OncancelClick="GroupAnalysisCategoryChartControl_widgetMultiSelectControl_CancelClick"
                                            />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td>
                                                <asp:Label ID="GroupAnalysisCategoryChartControl_selectScoreLabel" runat="server"
                                                    Text="Score Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisCategoryChartControl_absoluteScoreRadioButton"
                                                    runat="server" Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="true"
                                                    OnCheckedChanged="GroupAnalysisCategoryChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="GroupAnalysisCategoryChartControl_relativeScoreRadioButton"
                                                    runat="server" Text=" Relative Score" GroupName="1" AutoPostBack="true" OnCheckedChanged="GroupAnalysisCategoryChartControl_absoluteScoreRadioButton_CheckedChanged" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <uc1:MultiSeriesChart ID="GroupAnalysisCategoryChartControl_categoryChartControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
<p>
    &nbsp;</p>
<p>
    &nbsp;</p>
