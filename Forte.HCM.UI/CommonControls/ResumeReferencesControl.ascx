﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeReferencesControl.ascx.cs" Inherits="Forte.HCM.UI.CommonControls.ResumeReferencesControl" %>
<asp:UpdatePanel ID="ResumeReferencesControl_updatePanel" runat="server">
    <ContentTemplate>
        <div id="MyResume_referenceInfoMainDiv">
            <div style="clear: both; overflow: auto; height: 290px" runat="server" id="ResumeReferencesControl_controlsDiv"
                class="resume_Table_Bg">
                <asp:HiddenField ID="ResumeReferencesControl_hiddenField" runat="server" />
                <asp:HiddenField ID="ResumeReferencesControl_deletedRowHiddenField" runat="server" />
                <div style="float:right;">
                <asp:Button ID="ResumeReferencesControl_addDefaultButton" runat="server" 
                SkinID="sknButtonId" ToolTip="Add Reference" Text="Add" 
                    onclick="ResumeReferencesControl_addDefaultButton_Click" />
                </div>
                <asp:ListView ID="ResumeReferencesControl_listView" runat="server" OnItemDataBound="ResumeReferencesControl_listView_ItemDataBound"
                    OnItemCommand="ResumeReferencesControl_listView_ItemCommand">
                    <LayoutTemplate>
                        <div id="itemPlaceholderContainer" runat="server">
                            <div runat="server" id="itemPlaceholder">
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="resume_panel_list_view">
                            <asp:Panel GroupingText="References" runat="server" ID="ResumeReferencesControl_referencePanel" CssClass="can_resume_panel_text">
                                <div id="MyResume_referencesDeleteImage" style="clear: both; float: right;">
                                    <asp:Button ID="ResumeReferencesControl_addButton" runat="server" CommandName="addReference" SkinID="sknButtonId" ToolTip="Add Reference" Text="Add" />
                                    <asp:Button ID="ResumeReferencesControl_deleteButton" runat="server" CommandName="deleteReference" SkinID="sknButtonId" ToolTip="Delete Reference" Text="Remove"/>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="ResumeReferencesControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        <asp:TextBox ID="ResumeReferencesControl_deleteRowIndex" runat="server" Text='<%# Eval("ReferenceId") %>'
                                            Style="display: none" />
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeReferencesControl_nameTextBox" Text='<%# Eval("Name") %>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Name" MaxLength="500"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="ResumeReferencesControl_organizationLabel" Text="Organization"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeReferencesControl_organizationTextBox" Text='<%# Eval("Organization") %>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Organization" MaxLength="500"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="ResumeReferencesControl_relationLabel" Text="Relation" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeReferencesControl_relationTextBox" Text='<%# Eval("Relation") %>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Relation" MaxLength="500"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="ResumeReferencesControl_contInfoLabel" Text="Contact Info"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeReferencesControl_contInfoTextBox" 
                                            Text='<%# SetContactInfo(((Forte.HCM.DataObjects.Reference)Container.DataItem).ContactInformation)%>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Contact Info" MaxLength="500"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>