using System;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class OnlineInterviewSessionPreviewControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Do Nothing
        }
        /// <summary>
        /// This property assigns interview session details to labels.
        /// </summary>
        public OnlineInterviewSessionDetail DataSource
        {
            set
            {
                if (value == null)
                    return;

                OnlineInterviewSessionPreviewControl_testNameLabel.Text = value.OnlineInterviewSessionName;
                OnlineInterviewSessionPreviewControl_sessionNoLabel.Text = value.NumberOfCandidateSessions.ToString();
                OnlineInterviewSessionPreviewControl_sessionDescLiteral.Text = value.OnlineInterviewSessionDescription;
                OnlineInterviewSessionPreviewControl_positionProfileLabel.Text = value.PositionProfileName;
                OnlineInterviewSessionPreviewControl_expiryDateLabel.Text = 
                    GetDateFormat(Convert.ToDateTime(value.ExpiryDate.ToString()));
                OnlineInterviewSessionPreviewControl_instructionsLiteral.Text = value.OnlineInterviewInstruction;
            }
        }
        
        /// <summary>
        /// This method returns the date in MM/dd/yyyy format. If date value is not given,
        /// then it returns empty.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }
        /// <summary>
        /// This property stores either edit or view.
        /// </summary>
        private string mode;

        public string Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
            }
        }
    }
}