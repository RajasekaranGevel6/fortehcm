﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class TestDetailsControl {
        
        /// <summary>
        /// TestDetailsControl_testDetailsMessageLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal TestDetailsControl_testDetailsMessageLiteral;
        
        /// <summary>
        /// TestDetailsControl_testIdLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_testIdLabel;
        
        /// <summary>
        /// TestDetailsControl_testDetailsTestIdReadOnlyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_testDetailsTestIdReadOnlyLabel;
        
        /// <summary>
        /// TestDetailsControl_testNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_testNameLabel;
        
        /// <summary>
        /// TestDetailsControl_testNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_testNameTextBox;
        
        /// <summary>
        /// TestDetailsControl_testDescriptionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_testDescriptionLabel;
        
        /// <summary>
        /// TestDetailsControl_testDescriptionTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_testDescriptionTextBox;
        
        /// <summary>
        /// TestDetailsControl_systemRecommendedTimeLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_systemRecommendedTimeLabel;
        
        /// <summary>
        /// TestDetailsControl_sysRecommendedTimeFieldLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_sysRecommendedTimeFieldLabel;
        
        /// <summary>
        /// TestDetailsControl_recommendedTimeLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_recommendedTimeLabel;
        
        /// <summary>
        /// TestDetailsControl_recommendedTimeLabelTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_recommendedTimeLabelTextBox;
        
        /// <summary>
        /// MaskedEditExtender1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender MaskedEditExtender1;
        
        /// <summary>
        /// TestDetailsControl_positionProfileLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_positionProfileLabel;
        
        /// <summary>
        /// TestDetailsControl_positionProfileTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_positionProfileTextBox;
        
        /// <summary>
        /// TestDetailsControl_positionProfileImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestDetailsControl_positionProfileImageButton;
        
        /// <summary>
        /// TestDetailsControl_positionProfileHelpImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestDetailsControl_positionProfileHelpImageButton;
        
        /// <summary>
        /// TestDetailsControl_clearPositionProfileLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton TestDetailsControl_clearPositionProfileLinkButton;
        
        /// <summary>
        /// TestDetailsControl_positionProfileIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestDetailsControl_positionProfileIDHiddenField;
        
        /// <summary>
        /// TestDetailsControl_isCertificationtestCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox TestDetailsControl_isCertificationtestCheckBox;
        
        /// <summary>
        /// TestDetailsControl_CertificationDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl TestDetailsControl_CertificationDiv;
        
        /// <summary>
        /// TestDetailsControl_maxTotalScoreLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_maxTotalScoreLabel;
        
        /// <summary>
        /// TestDetailsControl_maxTotalScoreTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_maxTotalScoreTextBox;
        
        /// <summary>
        /// interestRateRangeValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RangeValidator interestRateRangeValidator;
        
        /// <summary>
        /// TestDetailsControl_MaximumtimepermissibleScoreLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_MaximumtimepermissibleScoreLabel;
        
        /// <summary>
        /// TestDetailsControl_MaximumtimepermissibleScoreTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_MaximumtimepermissibleScoreTextBox;
        
        /// <summary>
        /// CreateTestSession_timeLimitMaskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender CreateTestSession_timeLimitMaskedEditExtender;
        
        /// <summary>
        /// TestDetailsControl_numberofpermissibleLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_numberofpermissibleLabel;
        
        /// <summary>
        /// TestDetailsControl_numberofpermissibleTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_numberofpermissibleTextBox;
        
        /// <summary>
        /// TestDetailsControl_numberofpermissibleMaskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender TestDetailsControl_numberofpermissibleMaskedEditExtender;
        
        /// <summary>
        /// TestDetailsControl_timeperiodrequiredLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_timeperiodrequiredLabel;
        
        /// <summary>
        /// TestDetailsControl_timeperiodrequiredTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_timeperiodrequiredTextBox;
        
        /// <summary>
        /// TestDetailsControl_timeperiodrequiredMaskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender TestDetailsControl_timeperiodrequiredMaskedEditExtender;
        
        /// <summary>
        /// TestDetailsControl_MaskedEditValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditValidator TestDetailsControl_MaskedEditValidator;
        
        /// <summary>
        /// TestDetailsControl_certificateformatLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_certificateformatLabel;
        
        /// <summary>
        /// TestDetailsControl_certificateFormatNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestDetailsControl_certificateFormatNameTextBox;
        
        /// <summary>
        /// TestDetailsControl_certificateformatSearchImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestDetailsControl_certificateformatSearchImageButton;
        
        /// <summary>
        /// TestDetailsControl_certificateFormatNameHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestDetailsControl_certificateFormatNameHiddenField;
        
        /// <summary>
        /// TestDetailsCotnrol_certificateIdHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestDetailsCotnrol_certificateIdHiddenField;
        
        /// <summary>
        /// TestDetailsControl_periodofcertificationvalidityLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestDetailsControl_periodofcertificationvalidityLabel;
        
        /// <summary>
        /// TestDetailsControl_certificationValidityDropDownList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList TestDetailsControl_certificationValidityDropDownList;
    }
}
