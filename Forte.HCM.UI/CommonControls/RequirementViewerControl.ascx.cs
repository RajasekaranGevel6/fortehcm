﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

namespace Forte.HCM.UI.CommonControls
{
    public partial class RequirementViewerControl : UserControl
    {
        public delegate void FileUploadedDelegate (object sender, EventArgs e);
        public event FileUploadedDelegate FileUploaded;

        public delegate void KeywordPopupatedDelegate(object sender, KeywordEventArgs e);
        public event KeywordPopupatedDelegate KeywordPopupated;

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;

        protected void RequirementViewerControl_fileUploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (RequirementViewerControl_fileUpload.HasFile)
                {
                    string filepath = string.Empty;

                    filepath = Server.MapPath(@"..\Requirementdoc\") +Path.GetFileName(RequirementViewerControl_fileUpload.PostedFile.FileName);
                    //if (fileType.Exists(P => P == Path.GetExtension(filepath)))
                    if (FileType.Exists(P => P == Path.GetExtension(filepath)))
                    {
                        RequirementViewerControl_fileUpload.PostedFile.SaveAs(filepath);
                        RequirementViewerControl_requirementTextBox.Text = new PositionProfileRequirementReader().GetPositionProfileRequirement(filepath);
                       
                        _message = MessageType.Success;
                    }
                    else
                    {
                        _message = MessageType.Error;
                    }
                }
                else
                {
                    _message = MessageType.Warning;
                }
                if (FileUploaded != null)
                {
                    // Fire the upload click event.
                    FileUploaded(sender, e);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                _message = MessageType.Unexpected;

                if (FileUploaded != null)
                {
                    // Fire the upload click event.
                    FileUploaded(sender, e);
                }
            }
        }

        public string RequirementPanelValue
        {
            get {
                return RequirementViewerControl_requirementTextBox.Text;
            }
            set
            {
                RequirementViewerControl_requirementTextBox.Text = value;
            }
        }

        public string AdditionalInformation
        {
            get
            {
                return RequirementViewerControl_additionalInformationTextBox.Text;
            }
            set
            {
                RequirementViewerControl_additionalInformationTextBox.Text = value;
            }
        }

        private MessageType _message;
        public MessageType Message
        {
            set {
                value = _message;
            }
            get
            {
                return _message;
            }
        }

        private List<string> fileType;

        private List<string> FileType
        {
            get {
                fileType = new List<string>();
                fileType.Add(".doc");
                fileType.Add(".txt");
                fileType.Add(".eml");
                fileType.Add(".pdf");
                fileType.Add(".docx");
                return fileType;
            }
            set {
                value = fileType;
            }
        }

        protected void RequirementViewerControl_resetLinkButton_Click(object sender, EventArgs e)
        {
            RequirementViewerControl_requirementTextBox.Text = "";
        }

        /// <summary>
        /// Method that is called when the auto populate button is clicked. This will 
        /// parse the position profile requirements text information and compose the 
        /// list of technical skills, verticals & roles.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RequirementViewerControl_autoPopulateButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if requirements text is present.
                if (RequirementViewerControl_requirementTextBox.Text.Trim().Length == 0)
                {
                    // Fire the message thrown event.
                    if (ControlMessageThrown != null)
                        ControlMessageThrown(this, new ControlMessageEventArgs("Requirement cannot be empty", MessageType.Error));

                    return;
                }

                // Retrieve the pattern for position profile keyword.
                PositionProfileKeywordPattern pattern = new PositionProfileBLManager().GetKeywordDictionaryPattern();

                // Retrieve the keyword dictionary list.
                List<KeywordSkill> keywordSkills = new PositionProfileBLManager().GetKeywordDictionaryList();

                if (pattern == null)
                    return;

                // Construct event data.
                KeywordEventArgs eventArgs = new KeywordEventArgs();

                Regex regex = null;

                #region Compose Technical Skills

                // Apply technical skills pattern.
                if (!Utility.IsNullOrEmpty(pattern.TechnicalSkillsPattern))
                {
                    // Construct regex object.
                    regex = new Regex(pattern.TechnicalSkillsPattern, RegexOptions.IgnoreCase);

                    // Retrieve matches.
                    MatchCollection matches = regex.Matches(RequirementViewerControl_requirementTextBox.Text.Trim());

                    if (matches != null && matches.Count > 0)
                    {
                        // Loop through the matched skills and construct the technical
                        // skills list.
                        foreach (Match match in matches)
                        {
                            // Construct a technical skills list object.
                            var keyword = from m in keywordSkills
                                        where m.SkillName.ToUpper() ==
                                        match.Value.ToUpper()
                                    select new TechnicalSkillDetails(m.SkillCategory, match.Value);

                            if (keyword != null && keyword.Count() > 0)
                            {
                                // Instantiate technicals skills list.
                                if (eventArgs.TechnicalSkills == null)
                                    eventArgs.TechnicalSkills = new List<TechnicalSkillDetails>();

                                // Check if matched value not in the list.
                                if (eventArgs.TechnicalSkills.Find(item => item.SkillName.ToUpper() == match.Value.ToUpper()) == null)
                                {
                                    eventArgs.TechnicalSkills.Add(keyword.ToList().First());
                                }
                            }
                        }
                    }
                }

                #endregion Compose Technical Skills

                #region Compose Verticals

                // Apply verticals pattern.
                if (!Utility.IsNullOrEmpty(pattern.VerticalsPattern))
                {
                    // Construct regex object.
                    regex = new Regex(pattern.VerticalsPattern, RegexOptions.IgnoreCase);

                    // Retrieve matches.
                    MatchCollection matches = regex.Matches(RequirementViewerControl_requirementTextBox.Text.Trim());

                    if (matches != null && matches.Count > 0)
                    {
                        // Loop through the matched skills and construct the technical
                        // skills list.
                        foreach (Match match in matches)
                        {
                            // Construct a technical skills list object.
                            var keyword = from m in keywordSkills
                                    where m.SkillName.ToUpper() ==
                                        match.Value.ToUpper()
                                    select new VerticalDetails(match.Value, string.Empty);

                            if (keyword != null && keyword.Count() > 0)
                            {
                                // Instantiate verticals list.
                                if (eventArgs.Verticals == null)
                                    eventArgs.Verticals = new List<VerticalDetails>();

                                // Check if matched value not in the list.
                                if (eventArgs.Verticals.Find(item => item.VerticalSegment.ToUpper() == match.Value.ToUpper()) == null)
                                {
                                    eventArgs.Verticals.Add(keyword.ToList().First());
                                }
                            }
                        }
                    }
                }

                #endregion Compose Verticals

                #region Compose Roles

                // Apply roles pattern.
                if (!Utility.IsNullOrEmpty(pattern.RolesPattern))
                {
                    // Construct regex object.
                    regex = new Regex(pattern.RolesPattern, RegexOptions.IgnoreCase);

                    // Retrieve matches.
                    MatchCollection matches = regex.Matches(RequirementViewerControl_requirementTextBox.Text.Trim());

                    if (matches != null && matches.Count > 0)
                    {
                        // Consider the first 2 matches as primary and secondary role.
                        eventArgs.PrimaryRole = matches[0].Value;

                        if (matches.Count > 1)
                            eventArgs.SecondaryRole = matches[1].Value;
                    }
                }

                #endregion Compose Verticals

                // Fire the keyword populated event.
                if (KeywordPopupated != null)
                    KeywordPopupated(this, eventArgs);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

         
    }
}