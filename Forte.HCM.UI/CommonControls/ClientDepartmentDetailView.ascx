﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientDepartmentDetailView.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ClientDepartmentDetailView" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_clientNameLabel" runat="server" Text="Client Name"
                ToolTip="Client Name" SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_clientDepartmentLabel" runat="server" ToolTip="Client Department Name"
                Text="Email ID" SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_eMailIDLabel" runat="server" ToolTip="Email ID"
                Text="Email ID" SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td colspan="5" align="right">
            <table cellpadding="1" cellspacing="3" width="32%" border="0">
                <tr>
                    <td>
                        <asp:ImageButton ID="ClientDepartmentDetailView_editClientDepartmentImageButton"
                            runat="server" SkinID="sknEditClientDepartmentImageButton" OnCommand="ClientDepartmentDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDepartmentDetailView_deleteClientDepartmentImageButton"
                            runat="server" SkinID="sknDeleteClientDepartmentImageButton" OnCommand="ClientDepartmentDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDepartmentDetailView_viewClientImageButton" runat="server"
                            SkinID="sknViewClientImageButton" OnCommand="ClientDepartmentDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDepartmentDetailView_viewContactImageButton" runat="server"
                            SkinID="sknViewContactImageButton" OnCommand="ClientDepartmentDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDepartmentDetailView_addContactImageButton" runat="server"
                            SkinID="sknAddContactImageButton" OnCommand="ClientDepartmentDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDepartmentDetailView_viewPositionProfileImageButton" runat="server"
                            SkinID="sknViewPositionProfileImageButton1" OnCommand="ClientDepartmentDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDepartmentDetailView_createPositionProfileImageButton"
                            runat="server" SkinID="sknCreatePositionProfileImageButton1" OnCommand="ClientDepartmentDetailView_ImageButton_Command" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td style="width: 10%">
            <asp:Label ID="ClientDepartmentDetailView_phoneNoHeadLabel" runat="server" Text="Phone No"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientDepartmentDetailView_phoneNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td style="width: 10%">
            <asp:Label ID="ClientDepartmentDetailView_faxNoHeadLabel" runat="server" Text="Fax No"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientDepartmentDetailView_faxNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td style="width: 10%">
            <asp:Label ID="ClientDepartmentDetailView_zipHeadLabel" runat="server" Text="Zip Code"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientDepartmentDetailView_zipLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_streetAddressHeadLabel" runat="server"
                Text="Street Address" SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_streetAddressLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_cityHeadLabel" runat="server" Text="City"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_cityLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_stateHeadLabel" runat="server" Text="State Name"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_statelabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_countryHeadLabel" runat="server" Text="Country"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDepartmentDetailView_countryLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 50%">
                        <asp:Label ID="ClientDepartmentDetailView_descriptionHeadLabel" runat="server" Text="Description"
                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="height: 50px; width: 100%; overflow: auto; word-wrap: break-word; white-space: normal;">
                            <asp:Label ID="ClientDepartmentDetailView_descriptionLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="td_height_2">
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 50%">
                        <asp:Label ID="ClientDepartmentDetailView_additionalInfoHeadLabel" runat="server" 
                            Text="Additional Info" SkinID="sknLabelFieldHeaderText"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="height: 50px; width: 100%; overflow: auto; word-wrap: break-word; white-space: normal;">
                            <asp:Label ID="ClientDepartmentDetailView_additionalInfoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
