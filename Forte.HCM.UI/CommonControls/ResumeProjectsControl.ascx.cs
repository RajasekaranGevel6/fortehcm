﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ProjectsControl.cs
// File that represents the user interface for the project information details

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives


namespace Forte.HCM.UI.CommonControls
{
    public partial class ResumeProjectsControl : System.Web.UI.UserControl
    {
        #region Custom Event Handler and Delegate

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;
        private int _rowCounter;
        #endregion Custom Event Handler and Delegate

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ResumeProjectsControl_addDefaultButton_Click(object sender, EventArgs e)
        {
            SetViewState();

            ResumeProjectsControl_listView.DataSource = null;
            if (ViewState["dataSource"] != null)
            {
                this.dataSource = (List<Project>)ViewState["dataSource"];
            }
            else
                this.dataSource = new List<Project>();

            this.dataSource.Add(new Project());

            ViewState["dataSource"] = this.dataSource;
            _rowCounter = this.dataSource.Count;
            ResumeProjectsControl_listView.DataSource = this.dataSource;
            ResumeProjectsControl_listView.DataBind();
            SetDate();

        }

        protected void DeleteRowImagebutton_Click(object sender, ImageClickEventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((ImageButton)(sender)).Parent;
            this.dataSource = (List<Project>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            ResumeProjectsControl_listView.DataSource = dataSource;
            ResumeProjectsControl_listView.DataBind();

            ViewState["dataSource"] = ResumeProjectsControl_listView.DataSource;
            SetDate();
        }

        protected void ResumeProjectsControl_listView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Button ResumeProjectsControl_addButton = ((Button)e.Item.FindControl
                    ("ResumeProjectsControl_addButton"));
            Button ResumeProjectsControl_deleteButton = ((Button)e.Item.FindControl
                    ("ResumeProjectsControl_deleteButton"));
            TextBox ResumeProjectsControl_projectNameTextBox = ((TextBox)e.Item.FindControl
                    ("ResumeProjectsControl_projectNameTextBox"));
            ((Panel)e.Item.FindControl("ResumeProjectsControl_projectPanel")).GroupingText =
                    "Project " + ((e.Item as ListViewDataItem).DisplayIndex + 1).ToString();

            ResumeProjectsControl_addButton.Visible = false;
            ResumeProjectsControl_deleteButton.Visible = false;

            if (_rowCounter == ((e.Item as ListViewDataItem).DisplayIndex + 1))
            {
                ResumeProjectsControl_projectNameTextBox.Focus();
                ResumeProjectsControl_addButton.Visible = true;
                if (_rowCounter != 1)
                    ResumeProjectsControl_deleteButton.Visible = true;
            }
        }

        protected void ResumeProjectsControl_listView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            SetViewState();
            SetDate();
            if (e.CommandName == "deleteProject")
            {
                Button deleteRowImagebutton = (Button)e.Item.FindControl("ResumeProjectsControl_deleteButton");
                TextBox txtRowIndex = (TextBox)e.Item.FindControl("ResumeProjectControl_deleteRowIndex");

                ResumeProjectControl_deletedRowHiddenField.Value = txtRowIndex.Text;
                ResumeProjectControl_okPopUpClick(deleteRowImagebutton, new EventArgs());

            }
            else if (e.CommandName == "addProject")
            {
                Button ResumeProjectsControl_addButton = (Button)e.Item.FindControl("ResumeProjectsControl_addButton");
                ResumeProjectsControl_addDefaultButton_Click(ResumeProjectsControl_addButton, new EventArgs());
            }
        }

        protected void ResumeProjectControl_okPopUpClick(object sender, EventArgs e)
        {
            int intDeletedRowIndex = Convert.ToInt32(ResumeProjectControl_deletedRowHiddenField.Value);
            if (intDeletedRowIndex != 0)
            {
                this.dataSource = (List<Project>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intDeletedRowIndex - 1);
            }
            else
            {
                int intLastRecord = ResumeProjectsControl_listView.Items.Count - 1;
                this.dataSource = (List<Project>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intLastRecord);
            }
            _rowCounter = dataSource.Count;
            ResumeProjectsControl_listView.DataSource = dataSource;
            ResumeProjectsControl_listView.DataBind();
            SetViewState();
            SetDate();

            // Fire the message event
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("Project detail deleted successfully", MessageType.Success));
        }

        #endregion Event Handlers

        #region Public Properties

        public string SetLocation(object project)
        {
            Project oProject = project as Project;
            Location oLocation = oProject.ProjectLocation;
            string location = "";
            if (oLocation != null)
            {
                location = (oLocation.City != null) ? oLocation.City : "";
                location += (oLocation.State != null) ? "," + oLocation.State : "";
                location += (oLocation.Country != null) ? "," + oLocation.Country : "";
                location = location.StartsWith(",") ? location.Substring(1) : location;
            }

            return location;
        }

        public string SetRole(object project)
        {
            Project oProject = project as Project;
            if (oProject.Role == null || oProject.Role == "")
                return "Role";
            else
                return oProject.Role;
        }

        public List<Project> DataSource
        {
            set
            {
                if (value == null)
                {
                    ResumeProjectsControl_addDefaultButton.Visible = true;
                    return;
                }
                // Set values into controls.
                ResumeProjectsControl_addDefaultButton.Visible = false;
                _rowCounter = value.Count;
                ResumeProjectsControl_listView.DataSource = value;
                ResumeProjectsControl_listView.DataBind();
                ViewState["dataSource"] = value;
            }
            get
            {
                if (ViewState["dataSource"] != null)
                    return (List<Project>)ViewState["dataSource"];
                else
                    return null;
            }
        }

        #endregion Event Handlers

        #region Private Properties

        private List<Project> dataSource;

        #endregion Private Properties

        #region Protected Methods

        protected void SetViewState()
        {

            List<Project> oProjectList = null;
            int intProjCnt = 0;
            foreach (ListViewDataItem item in ResumeProjectsControl_listView.Items)
            {
                if (oProjectList == null)
                {
                    oProjectList = new List<Project>();
                }


                Project oProject = new Project();
                Location oLocation = new Location();

                TextBox txtProjName = (TextBox)item.FindControl("ResumeProjectsControl_projectNameTextBox");
                TextBox txtProjDesc = (TextBox)item.FindControl("ResumeProjectsControl_projectDescTextBox");
                TextBox txtProjRole = (TextBox)item.FindControl("ResumeProjectsControl_roleTextBox");
                TextBox txtProjPosition = (TextBox)item.FindControl("ResumeProjectsControl_positionTextBox");
                TextBox txtProjStartDate = (TextBox)item.FindControl("ResumeProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ResumeProjectsControl_endDtTextBox");
                TextBox txtProjLocation = (TextBox)item.FindControl("ResumeProjectsControl_locationTextBox");
                TextBox txtProjEnvironment = (TextBox)item.FindControl("ResumeProjectsControl_environmentTextBox");
                TextBox txtProjClientName = (TextBox)item.FindControl("ResumeProjectsControl_clientNameTextBox");
                TextBox txtProjClientIndustry = (TextBox)item.FindControl("ResumeProjectsControl_clientIndustryTextBox");
                intProjCnt = intProjCnt + 1;
                oProject.ProjectId = intProjCnt;
                oProject.ProjectName = txtProjName.Text.Trim();
                oProject.ProjectDescription = txtProjDesc.Text.Trim();
                oProject.Role = txtProjRole.Text.Trim();
                oProject.PositionTitle = txtProjPosition.Text.Trim();

                DateTime dtProjStartDate, dtProjEndDate;
                if ((DateTime.TryParse(txtProjStartDate.Text, out dtProjStartDate)))
                {
                    oProject.StartDate = dtProjStartDate;
                }
                if ((DateTime.TryParse(txtProjEndDate.Text, out dtProjEndDate)))
                {
                    oProject.EndDate = dtProjEndDate;
                }
                oLocation.City = txtProjLocation.Text.Trim();
                oProject.ProjectLocation = oLocation;
                oProject.Environment = txtProjEnvironment.Text.Trim();
                oProject.ClientName = txtProjClientName.Text.Trim();
                oProject.ClientIndustry = txtProjClientIndustry.Text.Trim();
                oProjectList.Add(oProject);
            }
            ResumeProjectsControl_listView.DataSource = oProjectList;
            ResumeProjectsControl_listView.DataBind();
            ViewState["dataSource"] = oProjectList;
            ResumeProjectsControl_addDefaultButton.Visible = false;
        }

        protected void SetDate()
        {
            foreach (ListViewDataItem item in ResumeProjectsControl_listView.Items)
            {
                TextBox txtProjStartDate = (TextBox)item.FindControl("ResumeProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ResumeProjectsControl_endDtTextBox");
                if (txtProjStartDate.Text == "01/01/0001" || txtProjStartDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjStartDate.Text = "";
                }
                if (txtProjEndDate.Text == "01/01/0001" || txtProjEndDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjEndDate.Text = "";
                }
            }
        }

        #endregion Protected Methods
    }
}