﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeEducationControl.ascx.cs" Inherits="Forte.HCM.UI.CommonControls.ResumeEducationControl" %>
<asp:UpdatePanel ID="ResumeEducationControl_updatePanel" runat="server">
    <ContentTemplate>
        <div id="MyResume_educationMainDiv">
            <div style="clear: both; overflow: auto; height: 290px" runat="server" id="ResumeEducationControl_controlsDiv"
                class="resume_Table_Bg">
                <asp:HiddenField ID="ResumeEducationControl_hiddenField" runat="server" />
                <asp:HiddenField ID="ResumeEducationControl_deletedRowHiddenField" runat="server" />
                <div style="float:right;">
                    <asp:Button ID="ResumeEducationControl_addDefaultButton" runat="server" 
                        SkinID="sknButtonId" ToolTip="Add Education" Text="Add" 
                        onclick="ResumeEducationControl_addDefaultButton_Click" />
                </div>
                <asp:ListView ID="ResumeEducationControl_listView" runat="server" OnItemDataBound="ResumeEducationControl_listView_ItemDataBound"
                    OnItemCommand="ResumeEducationControl_listView_ItemCommand">
                    <LayoutTemplate>
                        <div id="itemPlaceholderContainer" runat="server">
                            <div runat="server" id="itemPlaceholder">
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="resume_panel_list_view">
                            <asp:Panel GroupingText="Institution" runat="server" ID="ResumeEducationControl_schoolPanel" CssClass="can_resume_panel_text">
                                <div id="MyResume_educationDeleteImage" style="clear: both; float: right;">
                                    <asp:Button ID="ResumeEducationControl_addButton" runat="server" CommandName="addEducation" SkinID="sknButtonId" ToolTip="Add Education" Text="Add" />
                                    <asp:Button ID="ResumeEducationControl_deleteButton" runat="server" CommandName="deleteEducation" SkinID="sknButtonId" ToolTip="Delete Education" Text="Remove"/>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="ResumeEducationControl_nameLabel" Text="Name" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                        <asp:TextBox ID="ResumeEducationControl_deleteRowIndex" runat="server" Text='<%# Eval("EducationId") %>'
                                            Style="display: none" />
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeEducationControl_nameTextBox" Text='<%# Eval("SchoolName") %>'
                                            ToolTip="School Name" MaxLength="500" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="ResumeEducationControl_locationLabel" Text="Location" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeEducationControl_locationTextBox" Text='<%# SetLocation(((Forte.HCM.DataObjects.Education)Container.DataItem).SchoolLocation) %>'
                                            ToolTip="School Location" MaxLength="500" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="ResumeEducationControl_degreeLabel" Text="Degree" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeEducationControl_degreeTextBox" Text='<%# Eval("DegreeName") %>'
                                            ToolTip="Degree Name" MaxLength="100" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="ResumeEducationControl_specializationLabel" Text="Specialization"
                                            SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeEducationControl_specializationTextBox" Text='<%# Eval("Specialization") %>'
                                            ToolTip="Specialization" MaxLength="500" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="ResumeEducationControl_gpaLabel" Text="GPA" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ResumeEducationControl_gpaTextBox" Text='<%# Eval("GPA") %>'
                                            ToolTip="GPA" MaxLength="5" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="ResumeEducationControl_graduationDtLabel" Text="Graduation Date"
                                            SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <ajaxToolKit:MaskedEditExtender ID="ResumeEducationControl_MaskedEditExtender" runat="server"
                                            TargetControlID="ResumeEducationControl_graduationDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                            DisplayMoney="None" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                        <ajaxToolKit:MaskedEditValidator ID="ResumeEducationControl_MaskedEditValidator" runat="server"
                                            ControlExtender="ResumeEducationControl_MaskedEditExtender" ControlToValidate="ResumeEducationControl_graduationDtTextBox"
                                            EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                            TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                            ValidationGroup="MKE" />
                                        <ajaxToolKit:CalendarExtender ID="ResumeEducationControl_customCalendarExtender" runat="server"
                                            TargetControlID="ResumeEducationControl_graduationDtTextBox" CssClass="MyCalendar"
                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="ResumeEducationControl_calendarGraduationDtImageButton" />
                                        <asp:TextBox runat="server" ID="ResumeEducationControl_graduationDtTextBox" Text='<%# Eval("GraduationDate") %>'
                                            ToolTip="Graduation Date" Width="72px" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                        <asp:ImageButton ID="ResumeEducationControl_calendarGraduationDtImageButton" SkinID="sknCalendarImageButton"
                                            runat="server" ImageAlign="AbsMiddle" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
