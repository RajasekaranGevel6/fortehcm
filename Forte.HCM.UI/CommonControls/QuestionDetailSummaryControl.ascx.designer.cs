﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class QuestionDetailSummaryControl {
        
        /// <summary>
        /// QuestionDetailSummaryControl_questionResultLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal QuestionDetailSummaryControl_questionResultLiteral;
        
        /// <summary>
        /// QuestionDetailSummaryControl_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton QuestionDetailSummaryControl_topCancelImageButton;
        
        /// <summary>
        /// QuestionDetailSummaryControl_categoryDataGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataGrid QuestionDetailSummaryControl_categoryDataGrid;
        
        /// <summary>
        /// QuestionDetailSummaryControl_testAreaHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_testAreaHeadLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_testAreaValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_testAreaValueLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_complexityHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_complexityHeadLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_complexityValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_complexityValueLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_noOfAdministeredTestHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_noOfAdministeredTestHeadLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_noOfAdministeredTestValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_noOfAdministeredTestValueLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_avgTimeTakenHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_avgTimeTakenHeadLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_avgTimeTakenValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_avgTimeTakenValueLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_ratioCorrectAnswerHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_ratioCorrectAnswerHeadLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_ratioCorrectAnswerValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_ratioCorrectAnswerValueLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_tagHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_tagHeadLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_tagValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_tagValueLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_questionDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl QuestionDetailSummaryControl_questionDiv;
        
        /// <summary>
        /// QuestionDetailSummaryControl_questionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label QuestionDetailSummaryControl_questionLabel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_questionImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image QuestionDetailSummaryControl_questionImage;
        
        /// <summary>
        /// QuestionDetailSummaryControl_answerChoicesPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder QuestionDetailSummaryControl_answerChoicesPlaceHolder;
        
        /// <summary>
        /// testPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel testPanel;
        
        /// <summary>
        /// QuestionDetailSummaryControl_bottomAddButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button QuestionDetailSummaryControl_bottomAddButton;
        
        /// <summary>
        /// QuestionDetailSummaryControl_bottomCloseLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton QuestionDetailSummaryControl_bottomCloseLinkButton;
    }
}
