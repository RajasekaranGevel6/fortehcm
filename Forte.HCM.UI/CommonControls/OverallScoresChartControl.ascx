﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OverallScoresChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.OverallScoresChartControl" %>
<%@ Register Src="~/CommonControls/SingleSeriesReportChartControl.ascx" TagName="ReportChartControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
 <div style="width: 100%; overflow: auto">
    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false"/>
    <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false"/>
 </div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="OverallScoresChartControl_updatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td width="146px">
                                                <asp:Label ID="OverallScoresChartControl_selectScoreLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="OverallScoresChartControl_selectAbsoluteScoreRadioButton" runat="server"
                                                    Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="True" OnCheckedChanged="OverallScoresChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="OverallScoresChartControl_selectRelativeScoreRadioButton" runat="server"
                                                    Text=" Relative Score" GroupName="1" AutoPostBack="True" OnCheckedChanged="OverallScoresChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="OverallScoresChartControl_selectDisplayLinkButton" runat="server"
                                                    Text="Show Comparative Score" SkinID="sknActionLinkButton" OnClick="OverallScoresChartControl_selectDisplayLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="OverAllScoresChartControl_hiddenfield" runat="server" Value="0" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                           
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <uc1:ReportChartControl ID="OverallScoreChartControl_scoreChart" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
