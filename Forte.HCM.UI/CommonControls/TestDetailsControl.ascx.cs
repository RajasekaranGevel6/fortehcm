﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestDetailControl.ascx.cs
// File that represents the user interface for the test details
// control

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{

    /// <summary>
    /// Class that represents the user control interface layout and functionalities
    /// for the test details. This page is used to view the test and certification
    /// details.
    /// </summary>
    public partial class TestDetailsControl : UserControl
    {
        #region Events                                                         
       
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableOrDisableCertificationDiv();
            SetCertificateFormat();
            if (IsPostBack)
                return;
            TestDetailsControl_isCertificationtestCheckBox.Attributes.Add("onClick", "javascript:return ToggleCertificationDiv();");
            TestDetailsControl_certificateformatSearchImageButton.Attributes.Add("onClick",
                "javascript:return SearchCertificateFormatPopUp('" +
                TestDetailsControl_certificateFormatNameTextBox.ClientID + "','" +
                TestDetailsCotnrol_certificateIdHiddenField.ClientID + "','" +
                TestDetailsControl_certificateFormatNameHiddenField.ClientID + "');");

            // Add handler for position profile search icon.
            TestDetailsControl_positionProfileImageButton.Attributes.Add("onclick",
               "javascript:return LoadPositionProfileName('" + TestDetailsControl_positionProfileTextBox.ClientID + "','" +
               TestDetailsControl_positionProfileIDHiddenField.ClientID + "');");
            LoadCertificationValidityDropDownList();
            //TestDetailsControl_CertificationDiv.Visible = false;
        }

        #endregion Events

        #region Properties                                                     

        #region Datasource Property                                            

        public TestDetail TestDetailDataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                TestDetailsControl_testDetailsTestIdReadOnlyLabel.Text = value.TestKey;
                TestDetailsControl_testNameTextBox.Text = value.Name;
                TestDetailsControl_testDescriptionTextBox.Text = value.Description;
                TestDetailsControl_recommendedTimeLabelTextBox.Text = Utility.ConvertSecondsToHoursMinutesSeconds(value.RecommendedCompletionTime).ToString();
                TestDetailsControl_sysRecommendedTimeFieldLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds(value.SystemRecommendedTime).ToString();
                TestDetailsControl_positionProfileIDHiddenField.Value = value.PositionProfileID.ToString();
                TestDetailsControl_positionProfileTextBox.Text = value.PositionProfileName;

                if (Utility.IsNullOrEmpty(value.CertificationDetail))
                    return;
                if ((bool)value.IsCertification)
                {
                    TestDetailsControl_isCertificationtestCheckBox.Checked = true;
                    TestDetailsControl_maxTotalScoreTextBox.Text = (value.CertificationDetail.MinimumTotalScoreRequired <= 0) ? "" :
                        value.CertificationDetail.MinimumTotalScoreRequired.ToString();
                    TestDetailsControl_MaximumtimepermissibleScoreTextBox.Text =
                        (value.CertificationDetail.MaximumTimePermissible == 0) ? "" : (value.CertificationDetail.MaximumTimePermissible < 0) ? "00:00:00" : Utility.ConvertSecondsToHoursMinutesSeconds(
                         value.CertificationDetail.MaximumTimePermissible);
                    TestDetailsControl_numberofpermissibleTextBox.Text = (value.CertificationDetail.PermissibleRetakes <= 0) ? "" :
                        value.CertificationDetail.PermissibleRetakes.ToString();
                    TestDetailsControl_timeperiodrequiredTextBox.Text = (value.CertificationDetail.DaysElapseBetweenRetakes < 0) ? "" :
                        value.CertificationDetail.DaysElapseBetweenRetakes.ToString();
                    TestDetailsControl_certificateFormatNameTextBox.Text = value.CertificationDetail.CertificateFormat;
                    TestDetailsControl_certificateFormatNameHiddenField.Value = TestDetailsControl_certificateFormatNameTextBox.Text;
                    TestDetailsCotnrol_certificateIdHiddenField.Value = value.CertificationDetail.CertificateID.ToString();
                    LoadCertificationValidityDropDownList();
                    TestDetailsControl_certificationValidityDropDownList.Items.FindByValue(value.CertificationDetail.CertificateValidity).Selected = true;
                }
            }
            get
            {
                TestDetail testDetail = new TestDetail();
                testDetail.Name = TestDetailsControl_testNameTextBox.Text;
                testDetail.Description = TestDetailsControl_testDescriptionTextBox.Text;

                if (!Utility.IsNullOrEmpty(TestDetailsControl_positionProfileIDHiddenField.Value))
                {
                    testDetail.PositionProfileID = Convert.ToInt32(TestDetailsControl_positionProfileIDHiddenField.Value);
                }

                if (!Utility.IsNullOrEmpty(TestDetailsControl_positionProfileTextBox.Text))
                {
                    testDetail.PositionProfileName = TestDetailsControl_positionProfileTextBox.Text;
                }

                DateTime timeLimit;
                if (Utility.IsNullOrEmpty(TestDetailsControl_recommendedTimeLabelTextBox.Text.Trim()))
                {
                    testDetail.RecommendedCompletionTime = -1;
                }
                else if (DateTime.TryParse(TestDetailsControl_recommendedTimeLabelTextBox.Text, out timeLimit) == false)
                {
                    testDetail.RecommendedCompletionTime = -2;
                }
                else if (Utility.ConvertHoursMinutesSecondsToSeconds(TestDetailsControl_recommendedTimeLabelTextBox.Text) < Constants.General.DEFAULT_TEST_MINIMUM_TIME)
                {
                    testDetail.RecommendedCompletionTime = 0;
                }
                else
                {
                    testDetail.RecommendedCompletionTime = Utility.ConvertHoursMinutesSecondsToSeconds(TestDetailsControl_recommendedTimeLabelTextBox.Text);
                }

                testDetail.SystemRecommendedTime = Utility.ConvertHoursMinutesSecondsToSeconds(TestDetailsControl_sysRecommendedTimeFieldLabel.Text);
                if (TestDetailsControl_isCertificationtestCheckBox.Checked)
                {
                    if (Utility.IsNullOrEmpty(testDetail.CertificationDetail))
                        testDetail.CertificationDetail = new CertificationDetail();
                    testDetail.IsCertification = true;
                    testDetail.CertificationDetail.CertificateFormat = TestDetailsControl_certificateFormatNameHiddenField.Value;
                    if (TestDetailsCotnrol_certificateIdHiddenField.Value != "")
                        testDetail.CertificationDetail.CertificateID = Convert.ToInt32(TestDetailsCotnrol_certificateIdHiddenField.Value);
                    if (TestDetailsControl_maxTotalScoreTextBox.Text != "")
                        testDetail.CertificationDetail.MinimumTotalScoreRequired = decimal.Parse(TestDetailsControl_maxTotalScoreTextBox.Text);
                    if (TestDetailsControl_MaximumtimepermissibleScoreTextBox.Text != "")
                    {
                        if (DateTime.TryParse(TestDetailsControl_MaximumtimepermissibleScoreTextBox.Text, out timeLimit) == false)
                            testDetail.CertificationDetail.MaximumTimePermissible = -2;
                        else if (Utility.ConvertHoursMinutesSecondsToSeconds(TestDetailsControl_MaximumtimepermissibleScoreTextBox.Text) < Constants.General.DEFAULT_TEST_MINIMUM_TIME)
                            testDetail.CertificationDetail.MaximumTimePermissible = -1;
                        else
                            testDetail.CertificationDetail.MaximumTimePermissible = Utility.ConvertHoursMinutesSecondsToSeconds(
                                TestDetailsControl_MaximumtimepermissibleScoreTextBox.Text);
                    }
                    if (TestDetailsControl_numberofpermissibleTextBox.Text != "")
                        testDetail.CertificationDetail.PermissibleRetakes = int.Parse(TestDetailsControl_numberofpermissibleTextBox.Text);
                    if (TestDetailsControl_timeperiodrequiredTextBox.Text != "")
                        testDetail.CertificationDetail.DaysElapseBetweenRetakes = int.Parse(TestDetailsControl_timeperiodrequiredTextBox.Text);
                    testDetail.CertificationDetail.CertificateValidity = TestDetailsControl_certificationValidityDropDownList.SelectedValue;
                }
                else
                    testDetail.IsCertification = false;
                return testDetail;
            }
        }

        #endregion Datasource Property


        /// <summary>
        /// Property that sets the position profile name.
        /// </summary>
        public string PositionProfileName
        {
            set
            {
                TestDetailsControl_positionProfileTextBox.Text = value;
            }
        }

        /// <summary>
        /// Property that sets the position profile ID.
        /// </summary>
        public int PositionProfileID
        {
            set
            {
                TestDetailsControl_positionProfileIDHiddenField.Value = value.ToString();
            }
        }

        public void SetCertificateFormat()
        {
            TestDetailsControl_certificateFormatNameTextBox.Text = TestDetailsControl_certificateFormatNameHiddenField.Value;
        }

        public int SysRecommendedTime
        {
            set
            {
                TestDetailsControl_sysRecommendedTimeFieldLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds(value);
            }
        }
        public Boolean ReadOnly
        {
            set
            {
                //TestDetailsControl_testDetailsTestIdReadOnlyLabel.ReadOnly = value;
                //TestDetailsControl_testDetailsTestIdTextBox.SkinID = "sknReadOnlyTextBox";
                TestDetailsControl_testNameTextBox.ReadOnly = value;
                TestDetailsControl_testDescriptionTextBox.ReadOnly = value;
                TestDetailsControl_recommendedTimeLabelTextBox.ReadOnly = value;
                TestDetailsControl_isCertificationtestCheckBox.Enabled = !value;
                TestDetailsControl_maxTotalScoreTextBox.ReadOnly = value;
                TestDetailsControl_MaximumtimepermissibleScoreTextBox.ReadOnly = value;
                TestDetailsControl_numberofpermissibleTextBox.ReadOnly = value;
                TestDetailsControl_timeperiodrequiredTextBox.ReadOnly = value;
                TestDetailsControl_certificateFormatNameTextBox.ReadOnly = value;
                TestDetailsControl_certificateformatSearchImageButton.Visible = !value;
                TestDetailsControl_certificationValidityDropDownList.SelectedIndex = 1;
                TestDetailsControl_certificationValidityDropDownList.Enabled = false;

            }
        }

        #endregion Properties

        #region Private Methods                                                

        /// <summary>
        /// This Method Load Valid Certificate Details to the Drop down
        /// </summary>
        private void LoadCertificationValidityDropDownList()
        {
            if (TestDetailsControl_certificationValidityDropDownList.Items.Count > 0)
                return;
            TestDetailsControl_certificationValidityDropDownList.DataSource =
                new Forte.HCM.BL.AttributeBLManager().GetAttributesByType("CERT_ID");
            TestDetailsControl_certificationValidityDropDownList.DataTextField = "AttributeName";
            TestDetailsControl_certificationValidityDropDownList.DataValueField = "AttributeID";
            TestDetailsControl_certificationValidityDropDownList.DataBind();
            TestDetailsControl_certificationValidityDropDownList.Items.Insert(0,
                new System.Web.UI.WebControls.ListItem("--Select--", "0"));
        }

        /// <summary>
        /// This method enables or disables the certificate div
        /// according from the certificate check box checked property
        /// </summary>
        private void EnableOrDisableCertificationDiv()
        {
            if (TestDetailsControl_isCertificationtestCheckBox.Checked)
                TestDetailsControl_CertificationDiv.Style.Add("display", "block");
            else
                TestDetailsControl_CertificationDiv.Style.Add("display", "none");
        }

        #endregion Private Methods
    }
}