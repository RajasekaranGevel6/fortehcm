﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComparisonTestAreaChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ComparisonTestAreaChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultiSeriesChart"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="ComparisonTestAreaChartControl_updatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <div style="width: 100%; overflow: auto">
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="ComparisonTestAreaChartControl_testKeyHiddednfield" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="ComparisonTestAreaChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="ComparisonTestAreaChartControl_selectProperty"
                                            OnClick="ComparisonTestAreaChartControl_widgetMultiSelectControl_Click"
                                            OncancelClick="ComparisonTestAreaChartControl_widgetMultiSelectControl_CancelClick" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:Label ID="ComparisonTestAreaChartControl_scoreTypeLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonTestAreaChartControl_absoluteRadioButton" runat="server"
                                                    Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="true" OnCheckedChanged="ComparisonTestAreaChartControl_absoluteRadioButton_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonTestAreaChartControl_relativeRadioButton" runat="server"
                                                    Text=" Relative Score" GroupName="1" AutoPostBack="true" OnCheckedChanged="ComparisonTestAreaChartControl_absoluteRadioButton_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:LinkButton ID="ComparisonTestAreaChartControl_selectDisplayTypeLinkButton" runat="server"
                                                    Text="Show Comparative Score" SkinID="sknActionLinkButton" OnClick="ComparisonTestAreaChartControl_selectDisplayTypeLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="ComparisonTestAreaChartControl_hiddenField" runat="server" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <uc1:MultiSeriesChart ID="ComparisonTestAreaChartControl_multiSeriesChart" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
