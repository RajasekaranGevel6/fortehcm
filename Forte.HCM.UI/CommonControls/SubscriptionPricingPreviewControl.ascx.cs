﻿
#region Namespace                                                              

using System;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class SubscriptionPricingPreviewControl : System.Web.UI.UserControl
    {

        #region Delegates                                                      

        /// <summary>
        /// A Delete method that holds the button click event
        /// </summary>
        /// <param name="sender">
        /// A <see cref="System.Object"/> that holds the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="System.EventArgs"/> that holds the 
        /// events for the button click
        /// </param>
        public delegate void ButtonClick(object sender, EventArgs e);

        #endregion Delegates

        #region Public Events                                                  

        public event ButtonClick PublishClick;

        public event ButtonClick CancelClick;

        #endregion Public Events

        #region Public Property                                                

        public List<SubscriptionFeatures> BindSubscriptionPricingGridView
        {
            set
            {
                if (Utility.IsNullOrEmpty(value))
                    throw new Exception("datasource should not be empty");
                Forte_SubscriptionPricingPreviowControl_featureTypeGridView.DataSource = value;
                Forte_SubscriptionPricingPreviowControl_featureTypeGridView.DataBind();
            }
        }

        #endregion Public Property

        #region Events                                                         

        protected void Forte_SubscriptionPricingPreviewControl_CancelButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                CancelClick(sender, e);
        }

        protected void Forte_SubscriptionPricingPreviewControl_publishButton_Click(object sender, EventArgs e)
        {
            if (PublishClick != null)
                PublishClick(sender, e);
        }

        #endregion Events

    }
}