﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CategoryChartControl.cs
// File that represents the user interface for the category chart details

#endregion Header

#region Directives
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;
#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class CandidateStatisticsInfo : UserControl, IWidgetControl
    {
        List<DropDownItem> dropDownItemList = null;
        #region Event Handler
        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                ViewState["PAGE_NUMBER"] = 1;
            CandidateStatisticsInfo_bottomPageNavigator1.PageNumberClick +=
                         new PageNavigator.PageNumberClickEventHandler(
                             CandidateStatisticsInfo_bottomPageNavigator_PageNumberClick);
            if ((bool)Session["PRINTER"])
            {
                CandidateStatisticsInfo_WidgetMultiSelectControl.Visible = false;
                CandidateStatisticsInfo_bottomPageNavigator1.Visible = false;
                LoadGrid(ViewState["instance"] as WidgetInstance, 1);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when the page number is clicked 
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        void CandidateStatisticsInfo_bottomPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            ViewState["PAGE_NUMBER"] = e.PageNumber;
            LoadGrid(ViewState["instance"] as WidgetInstance, e.PageNumber);
            CandidateStatisticsInfo_WidgetMultiSelectControl.StateMaintainPaging();
            WidgetMultiSelectControl.StateMaintainPaging();
            WidgetMultiSelectControlPrint.StateMaintainPaging();
            
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CandidateStatisticsInfo_selectProperty(object sender, EventArgs e)
        {
            try
            {
                SellectAll(false);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on the select all clear all link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CandidateStatisticsInfo_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                SellectAll(true);
                // LoadGrid(ViewState["instance"] as WidgetInstance, int.Parse(ViewState["PAGE_NUMBER"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on the select all clear all link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void CandidateStatisticsInfo_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                SellectAll(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SellectAll(bool IsSellectAll)
        {
            CandidateStatisticsInfo_WidgetMultiSelectControl.WidgetTypePropertyAllSelected(IsSellectAll);
            dropDownItemList = new List<DropDownItem>();
            dropDownItemList.Add(new DropDownItem(CandidateStatisticsInfo_WidgetMultiSelectControl.SelectedProperties.ToString(), "CandidateList"));
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControlPrint.WidgetTypePropertyAllSelected(true);
        }
        #endregion Event Handler

        #region Private Methods

        /// <summary>
        /// Represents the method to get the time in short time format 
        /// </summary>
        /// <param name="TimeTaken">
        /// A<see cref="string "/>that holds the time in its full form 
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the string in its short form
        /// </returns>
        protected string GetTime(string TimeTaken)
        {
            //Convert the string to its datetime format
            DateTime date = Convert.ToDateTime(TimeTaken);

            //Return the short date for the date
            return date.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Represents the method to load the grid 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pagenumber
        /// </param>
        private void LoadGrid(WidgetInstance instance, int pageNumber)
        {
            int pageSize = Constants.General.DEFAULT_DASHBOARD_PAGE_SIZE;
            if ((bool)Session["PRINTER"]) // command will be remove.
                pageSize = 5;
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            int totalRecords = 0;
            List<CandidateStatisticsDetail> candidateStatisticsDetail = null;
            candidateStatisticsDetail = new ReportBLManager().
                GetComparisonCandidateStatistics(candidateReportDetail,
            out  totalRecords, pageNumber, pageSize);

            if (candidateStatisticsDetail == null)
                return;

            if ((bool)Session["PRINTER"])
            {
                candidateStatisticsDetails = candidateStatisticsDetail;
                
            }
            else
            {
                CandidateStatisticsInfo_candidateDetailsDataList.DataSource = candidateStatisticsDetail;
                CandidateStatisticsInfo_candidateDetailsDataList.DataBind();
                CandidateStatisticsInfo_bottomPageNavigator1.Reset();
                CandidateStatisticsInfo_bottomPageNavigator1.TotalRecords = totalRecords;
                CandidateStatisticsInfo_bottomPageNavigator1.PageSize = pageSize;
            }
            dropDownItemList = new ReflectionManager().GetListItems(candidateStatisticsDetail[0]);
            dropDownItemList.Add(new DropDownItem("Candidate Session ID", ""));
            dropDownItemList.Add(new DropDownItem("Attempt Number", ""));
            CandidateStatisticsInfo_WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            dropDownItemList.Add(new DropDownItem(CandidateStatisticsInfo_WidgetMultiSelectControl.SelectedProperties.ToString(), "CandidateList"));
        }
        public List<CandidateStatisticsDetail> candidateStatisticsDetails
        {
            set
            {
                List<CandidateStatisticsDetail> candidateStatisticsPrintDetails = new List<CandidateStatisticsDetail>();
                List<DropDownItem> testInfoDropDownItemList = null;
                CandidateStatisticsDetail candidateStatisticsDetail = null;
                if ((bool)Session["PRINTER"])
                {
                    bool isPrintFlag = false;
                    string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split(',');
                    for (int i = 0; i < value.Count; i++)
                    {
                        testInfoDropDownItemList = new List<DropDownItem>();
                        candidateStatisticsDetail = new CandidateStatisticsDetail();
                        foreach (string word in selectedProperty)
                        {
                            switch (word)
                            {
                                case "Test Date":
                                    testInfoDropDownItemList.Add(new DropDownItem("Test Date", value[i].TestDate.ToShortDateString()));
                                    break;
                                case "Answered Correctly":
                                    testInfoDropDownItemList.Add(new DropDownItem("QNS Answered(%)", value[i].AnsweredCorrectly.ToString()));
                                    break;
                                case "Total Time":
                                    testInfoDropDownItemList.Add(new DropDownItem("Total Time", value[i].TimeTaken.ToString()));
                                    break;
                                case "Absolute Score":
                                    testInfoDropDownItemList.Add(new DropDownItem("Absolute Score", value[i].AbsoluteScore.ToString()));
                                    break;
                                case "Average Time Taken":
                                    testInfoDropDownItemList.Add(new DropDownItem("Average Time", value[i].AverageTimeTaken.ToString()));
                                    break;
                                case "Relative Score":
                                    testInfoDropDownItemList.Add(new DropDownItem("Relative Score", value[i].RelativeScore.ToString()));
                                    break;
                                case "Interview Date":
                                    testInfoDropDownItemList.Add(new DropDownItem("Interview Date", "TBD"));
                                    break;
                                case "Percentile":
                                    testInfoDropDownItemList.Add(new DropDownItem("Percentile", value[i].Percentile.ToString()));
                                    break;
                                case "Candidate Session ID":
                                    testInfoDropDownItemList.Add(new DropDownItem("Session ID", value[i].CandidateSessionID.ToString()));
                                    break;
                                case "Attempt Number":
                                    testInfoDropDownItemList.Add(new DropDownItem("Attempt Number", value[i].AttemptNumber.ToString()));
                                    break;
                                case "Candidate Name":
                                    {
                                        isPrintFlag = true;
                                        if (!Utility.IsNullOrEmpty(value[i].FirstName))
                                            candidateStatisticsDetail.FirstName = value[i].FirstName;
                                    }
                                    break;
                                case "Address":
                                    {
                                        isPrintFlag = true;
                                        if (!Utility.IsNullOrEmpty(value[i].Address))
                                            candidateStatisticsDetail.Address = value[i].Address;
                                    }
                                    break;
                                case "Phone":
                                    {
                                        isPrintFlag = true;
                                        if (!Utility.IsNullOrEmpty(value[i].Phone))
                                            candidateStatisticsDetail.Phone = value[i].Phone;
                                    }
                                    break;
                                case "Synopsis":
                                    {
                                        isPrintFlag = true;
                                        if (!Utility.IsNullOrEmpty(value[i].Synopsis))
                                            candidateStatisticsDetail.Synopsis = value[i].Synopsis;
                                    }
                                    break;
                            }
                        }
                        if (testInfoDropDownItemList.Count > 0 || isPrintFlag)
                        {
                            candidateStatisticsDetail.TestDetails = testInfoDropDownItemList;
                            candidateStatisticsPrintDetails.Add(candidateStatisticsDetail);
                        }
                    }
                }
                CandidateStatisticsInfo_printCandidateDetailsDataList.DataSource = candidateStatisticsPrintDetails;
                CandidateStatisticsInfo_printCandidateDetailsDataList.DataBind();
                //CandidateStatisticsInfo_candidateDetailsDataList.DataSource = testInfoDropDownItemList;
                //CandidateStatisticsInfo_candidateDetailsDataList.DataBind();
            }
        }


        #endregion Private Methods

        #region IWidgetControl Members

        /// <summary>
        /// Represents the method to bind the data source
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;
            //Load the grid 
            LoadGrid(instance, 1);
            SellectAll(true);
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo
            commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }
        #endregion IWidgetControl Members
    }
}