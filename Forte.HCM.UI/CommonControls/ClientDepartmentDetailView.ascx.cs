﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

namespace Forte.HCM.UI.CommonControls
{
    public partial class ClientDepartmentDetailView : System.Web.UI.UserControl
    {
        public delegate void Button_Command(object sender, CommandEventArgs e);
        public event Button_Command OkCommand;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ClientDepartmentDetailView_ImageButton_Command(object sender, CommandEventArgs e)
        {
            if (OkCommand != null)
            {
                this.OkCommand(sender, e);
            }
        }

        public Department ClientDepartmentDetailViewDataSource
        {
            set
            {
                if (value == null)
                    return;
                ClientDepartmentDetailView_editClientDepartmentImageButton.CommandArgument = value.DepartmentID.ToString();
                ClientDepartmentDetailView_deleteClientDepartmentImageButton.CommandArgument = value.DepartmentID.ToString();
                ClientDepartmentDetailView_viewClientImageButton.CommandArgument = value.DepartmentID.ToString();
                ClientDepartmentDetailView_viewContactImageButton.CommandArgument = value.DepartmentID.ToString();
                ClientDepartmentDetailView_addContactImageButton.CommandArgument = value.DepartmentID.ToString();
                ClientDepartmentDetailView_viewPositionProfileImageButton.CommandArgument = value.DepartmentID.ToString();
                ClientDepartmentDetailView_createPositionProfileImageButton.CommandArgument = value.DepartmentID.ToString();

                ClientDepartmentDetailView_editClientDepartmentImageButton.CommandName = Constants.ClientManagementConstants.EDIT_DEPARTMENT;
                ClientDepartmentDetailView_deleteClientDepartmentImageButton.CommandName = Constants.ClientManagementConstants.DELETE_DEPARTMENT;
                ClientDepartmentDetailView_viewClientImageButton.CommandName = Constants.ClientManagementConstants.VIEW_DEPARTMENT_CLIENT;
                ClientDepartmentDetailView_viewContactImageButton.CommandName = Constants.ClientManagementConstants.VIEW_DEPARTMENT_CONTACT;
                ClientDepartmentDetailView_addContactImageButton.CommandName = Constants.ClientManagementConstants.ADD_DEPARTMENT_CONTACT;
                ClientDepartmentDetailView_viewPositionProfileImageButton.CommandName = Constants.ClientManagementConstants.VIEW_DEPARTMENT_POSITION_PROFILE;
                ClientDepartmentDetailView_createPositionProfileImageButton.CommandName = Constants.ClientManagementConstants.ADD_DEPARTMENT_POSITION_PROFILE;

                ClientDepartmentDetailView_clientNameLabel.Text = value.ClientName;
                ClientDepartmentDetailView_clientDepartmentLabel.Text = value.DepartmentName;
                ClientDepartmentDetailView_streetAddressLabel.Text = value.ContactInformation.StreetAddress;
                ClientDepartmentDetailView_eMailIDLabel.Text = value.ContactInformation.EmailAddress;
                if (value.ContactInformation.Phone != null)
                    ClientDepartmentDetailView_phoneNoLabel.Text = value.ContactInformation.Phone.Mobile;
                ClientDepartmentDetailView_cityLabel.Text = value.ContactInformation.City;
                ClientDepartmentDetailView_statelabel.Text = value.ContactInformation.State;
                ClientDepartmentDetailView_zipLabel.Text = value.ContactInformation.PostalCode;
                ClientDepartmentDetailView_countryLabel.Text = value.ContactInformation.Country;
                ClientDepartmentDetailView_faxNoLabel.Text = value.FaxNumber;
                ClientDepartmentDetailView_descriptionLabel.Text = value.DepartmentDescription == null ? 
                    value.DepartmentDescription : value.DepartmentDescription.ToString().Replace(Environment.NewLine, "<br />");
                ClientDepartmentDetailView_additionalInfoLabel.Text = value.AdditionalInfo == null ?
                    value.AdditionalInfo : value.AdditionalInfo.ToString().Replace(Environment.NewLine, "<br />");
            }
        }


        public NomenclatureCustomize DepartmentNomenClature
        {
            set
            {
                ClientDepartmentDetailView_clientNameLabel.ToolTip = value.ClientName;
                ClientDepartmentDetailView_clientDepartmentLabel.ToolTip = value.DepartmentName;
                ClientDepartmentDetailView_eMailIDLabel.ToolTip = value.EmailID;
                ClientDepartmentDetailView_phoneNoHeadLabel.Text = value.PhoneNumber;
                ClientDepartmentDetailView_faxNoHeadLabel.Text = value.FaxNo;
                ClientDepartmentDetailView_zipHeadLabel.Text = value.Zipcode;
                ClientDepartmentDetailView_streetAddressHeadLabel.Text = value.StreetAddress;
                ClientDepartmentDetailView_cityHeadLabel.Text = value.City;
                ClientDepartmentDetailView_stateHeadLabel.Text = value.state;
                ClientDepartmentDetailView_countryHeadLabel.Text = value.Country;
                ClientDepartmentDetailView_descriptionHeadLabel.Text = value.Description;
            }
        }

        public bool DeleteButtonVisibility
        {
            set
            {
                ClientDepartmentDetailView_deleteClientDepartmentImageButton.Visible = value;
            }
        }
    }
}