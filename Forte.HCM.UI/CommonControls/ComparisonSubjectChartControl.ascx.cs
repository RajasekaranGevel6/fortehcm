﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ComparisonSubjectChartControl.cs
// File that represents the user interface for the subject chart control
// for the comparison chart 

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ComparisonSubjectChartControl : UserControl, IWidgetControl
    {
        string testkey = string.Empty;

        List<DropDownItem> dropDownItemList = null;

        #region Event Handlers                                                 

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                ComparisonSubjectChartControl_hiddenField.Value = selectedProperty[2];
                ComparisonSubjectChartControl_testkeyhiddenField.Value = selectedProperty[0];
                ComparisonSubjectChartControl_absoluteScoreRadioButton.Checked = selectedProperty[3] == "0" ? false : true;
                ComparisonSubjectChartControl_relativeScoreRadioButton.Checked = selectedProperty[3] == "1" ? false : true;
                ComparisonSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[1]);
                ComparisonSubjectChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
        }                

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on radio button 
        ///  button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonSubjectChartControl_absoluteScoreRadioButton_CheckedChanged
           (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when the select
        /// link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonSubjectChartControl_selectDisplayLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void ComparisonSubjectChartControl_selectProperty(object sender, EventArgs e)
        {
            try
            {
                ComparisonSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(false);

                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Hanlder method that will be called on multi select link button added
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonSubjectChartControl_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Hanlder method that will be called on multi select link button added
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonSubjectChartControl_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Event Handlers

        #region Private Method                                                 

        /// <summary>
        /// Represents the method to get the chart details from the database
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            ComparisonSubjectChartControl_testkeyhiddenField.Value = candidateReportDetail.TestKey;
            DataTable multiSeriesDataTable = new ReportBLManager().
                GetComparisonReportSubjectDetails(candidateReportDetail);

            var subject = (from r in multiSeriesDataTable.AsEnumerable()
                           select new DropDownItem(r["Name"].ToString().Trim()
                              , r["ID"].ToString())).Distinct();

            ComparisonSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = subject.ToList();

            //Select all the check box in the check box list
            ComparisonSubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

            Session["SubjectChartDataSource"] = multiSeriesDataTable;

            LoadChart(instance,"1");
        }

        /// <summary>
        /// Represents the method to load the chart 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChart(WidgetInstance instance, string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            StringBuilder selectedSubjectId = new StringBuilder();
            string selectedSubjectIds = string.Empty;
            testkey = ComparisonSubjectChartControl_testkeyhiddenField.Value;
            selectedPrintProperty.Append(testkey + "|");
            DataTable categoryChartDataTable = null;

            if ((Session["SubjectChartDataSource"]) != null)
            {
                categoryChartDataTable = Session["SubjectChartDataSource"]
                    as DataTable;
            }
            DataTable selectedChartData = categoryChartDataTable.Clone();

            List<DropDownItem> selectedList = ComparisonSubjectChartControl_widgetMultiSelectControl.SelectedItems();

            //categoryList = WidgetMultiSelectControl.SelectedProperties;

            if (selectedList != null && selectedList.Count != 0)
            {
                foreach (DropDownItem category in selectedList)
                {
                    if ((Session["SubjectChartDataSource"]) != null)
                    {
                        selectedSubjectId.Append(category.ValueText);
                        selectedSubjectId.Append("-");
                        //select the datarow for the corresponding category id
                        DataRow[] datarow = (Session["SubjectChartDataSource"] as DataTable).
                            Select("ID='" + int.Parse(category.ValueText.ToString().Trim()) + "'");

                        //DataRow dr = selectedChartData.NewRow();                       
                        //dr.ItemArray = datarow;
                        selectedChartData.Rows.Add(datarow[0].ItemArray);
                    }
                }
                categoryChartDataTable = selectedChartData;
            }
            else
            {
                categoryChartDataTable = new DataTable();
            }

            MultipleSeriesChartData subjectChartDataSource = new MultipleSeriesChartData();

            subjectChartDataSource.ChartType = SeriesChartType.Column;

            subjectChartDataSource.ChartLength = 300;

            subjectChartDataSource.ChartWidth = 350;

            subjectChartDataSource.IsDisplayAxisTitle = true;

            subjectChartDataSource.IsDisplayChartTitle = false;

            subjectChartDataSource.XAxisTitle = "Subjects";

            subjectChartDataSource.YAxisTitle = "Scores";

            subjectChartDataSource.ChartTitle = "Candidate's Score Amongst Subjects";

            subjectChartDataSource.IsDisplayChartTitle = false;

            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                subjectChartDataSource.IsShowLabel = true;
            }

            dropDownItemList = new List<DropDownItem>();
            selectedSubjectIds = selectedSubjectId.ToString().TrimEnd('-');
            selectedPrintProperty.Append(selectedSubjectId.Replace('-', ',') + "|");
            if (ComparisonSubjectChartControl_hiddenField.Value == "1")
            {
                selectedPrintProperty.Append("1|");
                ComparisonSubjectChartControl_selectDisplayLinkButton.Text
                    = "Hide Comparative Score";
                subjectChartDataSource.IsHideComparativeScores = false;

                if (ComparisonSubjectChartControl_absoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) && (col.ColumnName.Substring(0, 4) == "Rel-"))
                        {
                            columnId.Add(col.ColumnName);
                        }
                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    categoryChartDataTable = selectedChartData;

                    dropDownItemList.Add(new DropDownItem("Title", "Subjects Comparative Score"));

                    dropDownItemList.Add(new DropDownItem("Subjects",
                        Constants.ChartConstants.COMPARISON_REPORT_SUBJECT_COMPARATIVE_ABSOLUTE +
                        "-" + testkey + "-" + selectedSubjectIds + ".png"));

                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_REPORT_SUBJECT_COMPARATIVE_ABSOLUTE + "-" +
                        testkey + "-" + selectedSubjectIds;

                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) != "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            columnId.Add(col.ColumnName);
                        }

                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) == "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            col.ColumnName = col.ColumnName.Remove(0, 4);
                        }
                    }
                    categoryChartDataTable = selectedChartData;

                    dropDownItemList.Add(new DropDownItem("Title", "Category Comparative Relative Score"));

                    dropDownItemList.Add(new DropDownItem("Categories",
                        Constants.ChartConstants.COMPARISON_REPORT_CATEGORY_COMPARATIVE_RELATIVE +
                        "-" + testkey + "-" + selectedSubjectIds + ".png"));

                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_REPORT_CATEGORY_COMPARATIVE_RELATIVE + "-" +
                        testkey + "-" + selectedSubjectIds;
                }
            }
            else
            {
                selectedPrintProperty.Append("0|");
                ComparisonSubjectChartControl_selectDisplayLinkButton.Text
                    = "Show Comparative Score";
                subjectChartDataSource.IsHideComparativeScores = true;

                if (ComparisonSubjectChartControl_absoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) && (col.ColumnName.Substring(0, 4) == "Rel-"))
                        {
                            columnId.Add(col.ColumnName);
                        }
                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    categoryChartDataTable = selectedChartData;


                    dropDownItemList.Add(new DropDownItem("Title", "Subject Absolute Score"));

                    dropDownItemList.Add(new DropDownItem("Subject",
                        Constants.ChartConstants.COMPARISON_REPORT_SUBJECT_ABSOLUTE +
                        "-" + testkey + "-" + selectedSubjectIds + ".png"));

                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_REPORT_SUBJECT_ABSOLUTE + "-" +
                        testkey + "-" + selectedSubjectIds;


                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    List<string> columnId = new List<string>();

                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) != "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            columnId.Add(col.ColumnName);
                        }
                    }
                    foreach (string id in columnId)
                    {
                        selectedChartData.Columns.Remove(id);
                    }
                    foreach (DataColumn col in selectedChartData.Columns)
                    {
                        if ((col.ColumnName.Length > 3) &&
                            (col.ColumnName.Substring(0, 4) == "Rel-") &&
                            (col.ColumnName != "ShortName") &&
                            (col.ColumnName != "Name") &&
                            (col.ColumnName != "ID"))
                        {
                            col.ColumnName = col.ColumnName.Remove(0, 4);
                        }
                    }
                    categoryChartDataTable = selectedChartData;

                    dropDownItemList.Add(new DropDownItem("Title", "Category Relative Score"));

                    dropDownItemList.Add(new DropDownItem("Categories",
                        Constants.ChartConstants.COMPARISON_REPORT_SUBJECT_RELATIVE +
                        "-" + testkey + "-" + selectedSubjectIds + ".png"));

                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.
                        COMPARISON_REPORT_SUBJECT_RELATIVE + "-" +
                        testkey + "-" + selectedSubjectIds;
                }
            }
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            subjectChartDataSource.ComparisonReportDataTable = categoryChartDataTable;
            subjectChartDataSource.IsComparisonReport = true;
            ComparisonSubjectChartControl_multiSeriesChartControl.MultipleChartDataSource =
              subjectChartDataSource;
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        #endregion Private Method

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>   
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

            //Add attributes for the show or hide link button
            ComparisonSubjectChartControl_selectDisplayLinkButton.Attributes.
                Add("onclick", "javascript:return ShowOrHideComparativeScore('" +
                ComparisonSubjectChartControl_selectDisplayLinkButton.ClientID + "','"
                + ComparisonSubjectChartControl_hiddenField.ClientID + "');");

        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}