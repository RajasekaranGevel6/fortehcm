﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExecutiveSummaryControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ExecutiveSummaryControl" %>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td class="header_bg">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 93%" align="left">
                        <asp:Literal ID="ExecutiveSummaryControl_headerLiteral" runat="server" Text="Executive Summary"></asp:Literal>
                         <asp:HiddenField ID="ExecutiveSummaryControl_hiddenField" runat="server" />
                    </td>
                    <td style="width: 2%" align="right">
                        <span id="ExecutiveSummaryControl_plusSpan" runat="server" style="display: none;">
                            <asp:Image ID="ExecutiveSummaryControl_plusImage" runat="server" SkinID="sknPlusImage" /></span><span
                                id="ExecutiveSummaryControl_minusSpan" runat="server" style="display: block;">
                                <asp:Image ID="ExecutiveSummaryControl_minusImage" runat="server" SkinID="sknMinusImage" /></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <div id="ExecutiveSummaryControl_controlsDiv" style="display: block;" runat="server">
                <table width="98%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="ExecutiveSummaryControl_execSummaryTextBox" runat="server" Height="100"
                                Width="100%" TextMode="MultiLine" MaxLength="5000"   onkeyup="CommentsCount(5000,this)"
                                                                onchange="CommentsCount(5000,this)" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
