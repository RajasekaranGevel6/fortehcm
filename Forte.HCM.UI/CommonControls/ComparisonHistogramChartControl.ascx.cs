﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ComparisonHistogramChartControl.cs
// File that represents the user interface for the histogram chart control 
// for comparison reports

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Collections.Generic;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using ReflectionComboItem;
using System.Text;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ComparisonHistogramChartControl : UserControl, IWidgetControl
    {
        #region Event Handlers                                                 

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                ComparisonHistogramChartControl_absoluteScore_RadioButton.Checked = WidgetMultiSelectControlPrint.SelectedProperties.TrimEnd(',') == "0" ? false : true;
                ComparisonHistogramChartControl_relativeScore_RadioButton.Checked = WidgetMultiSelectControlPrint.SelectedProperties.TrimEnd(',') == "1" ? false : true;
                //WidgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[0]);
                LoadChart(ViewState["instance"] as WidgetInstance);
            }
        }
        /// <summary>
        /// Represents the event handler that is called when the 
        /// select option radio button is clicked 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event arguements
        /// </param>
        protected void ComparisonHistogramChartControl_selectOptions_CheckedChanged
          (object sender, EventArgs e)
        {
            //Keep the instance key in view state 
            WidgetInstance instance =  ViewState["instance"] as WidgetInstance;

            LoadChartDetails(instance);
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that is used to load the chart details
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate report details from the session
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            //Method to load the test Scores histogram chart
            //Get the chart data from the database
            ReportHistogramData reportHistogramData = new ReportBLManager()
                .GetComparisonReportHistogramScoreDetails(candidateReportDetail);

            if (reportHistogramData != null)
                Session["HistogramData"] = reportHistogramData;


            //Load the chart with corresponding data
            LoadChart(instance);
        }

        /// <summary>
        /// Represents the method that used to load the chart data 
        /// </summary>
        /// <param name="reportHistogramData">
        /// A<see cref="ReportHistogramData"/>that holds the report histogram data
        /// </param>
        private void LoadChart(WidgetInstance instance)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            ReportHistogramData reportHistogramData = null;
            if (Session["HistogramData"] != null)
            {
                reportHistogramData = Session["HistogramData"] as ReportHistogramData;
            }

            //Get the candidate report details from the session
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            //Initialize the histogram chart data 
            //Assign the chart details

            reportHistogramData.ChartLength = 190;

            reportHistogramData.ChartWidth = 380;

            reportHistogramData.IsDisplayLegend = false;

            reportHistogramData.IsShowLabel = true;

            reportHistogramData.SegmentInterval = 10;

            reportHistogramData.IsDisplayChartTitle = false;

            reportHistogramData.IsDisplayAxisTitle = true;

            reportHistogramData.IsDisplayChartTitle = false;

            reportHistogramData.ChartTitle = "Candidate's Score Position";

            reportHistogramData.XAxisTitle = "Scores";

            reportHistogramData.YAxisTitle = "No Of Candidates";

            reportHistogramData.IsDisplayCandidateScore = true;

            reportHistogramData.IsDisplayCandidateName = true;

            reportHistogramData.CandidateName = reportHistogramData.CandidateName;

            List<DropDownItem> dropDownItemList = new List<DropDownItem>();

            //If the absolute score radio button is checked, load absolute score data 
            if (ComparisonHistogramChartControl_absoluteScore_RadioButton.Checked)
            {
                selectedPrintProperty.Append("1");
                reportHistogramData.CandidateScore = reportHistogramData.CandidateScore;
                reportHistogramData.ChartData = reportHistogramData.AbsoluteChartData;
                reportHistogramData.ChartImageName = Constants.ChartConstants.COMPARISON_REPORT_HISTOGRAM_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-');
                //Assign the data to chart control and load it 

                dropDownItemList.Add(new DropDownItem("Title", "Absolute Comparative Score"));
                dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.COMPARISON_REPORT_HISTOGRAM_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-') + ".png"));               
            }
            //If the relative score radio button is checked, load relative score data 
            else
            {
                selectedPrintProperty.Append("0");
                reportHistogramData.CandidateScore = reportHistogramData.CandidateRelativeScore;
                reportHistogramData.ChartData = reportHistogramData.RelativeChartData;
                reportHistogramData.CandidateScoreData = reportHistogramData.CandidateRelativeScoreData;

                reportHistogramData.ChartImageName = Constants.ChartConstants.COMPARISON_REPORT_HISTOGRAM_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-');
                //Assign the data to chart control and load it 

                dropDownItemList.Add(new DropDownItem("Title", "Relative Comparative Score"));
                dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.COMPARISON_REPORT_HISTOGRAM_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-') + ".png"));

            }
            ComparisonHistogramChartControl_histogramChart.LoadChartControl(reportHistogramData);
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }
        #endregion Private Methods

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }
        #endregion IWidgetControl Members
    }
}