﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class InterviewTestSessionPreviewControl {
        
        /// <summary>
        /// TestSessionPreviewControl_questionResultLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal TestSessionPreviewControl_questionResultLiteral;
        
        /// <summary>
        /// TestSessionPreviewControl_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestSessionPreviewControl_topCancelImageButton;
        
        /// <summary>
        /// TestSessionPreviewControl_testKeyHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_testKeyHeadLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_testKeyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_testKeyLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_testNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_testNameHeadLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_testNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_testNameLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_sessionKeyHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_sessionKeyHeadLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_sessionKeyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_sessionKeyLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_sessionNoHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_sessionNoHeadLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_sessionNoLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_sessionNoLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_expiryDateHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_expiryDateHeadLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_expiryDateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_expiryDateLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_positionProfileHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_positionProfileHeadLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_positionProfileLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_positionProfileLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_sessionDescLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_sessionDescLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_sessionDescLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal TestSessionPreviewControl_sessionDescLiteral;
        
        /// <summary>
        /// TestSessionPreviewControl_instructionsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestSessionPreviewControl_instructionsLabel;
        
        /// <summary>
        /// TestSessionPreviewControl_instructionsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal TestSessionPreviewControl_instructionsLiteral;
    }
}
