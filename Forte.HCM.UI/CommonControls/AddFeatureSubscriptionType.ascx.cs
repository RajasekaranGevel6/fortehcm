﻿
#region Namespace                                                              

using System;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.DataObjects;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class AddFeatureSubscriptionType : System.Web.UI.UserControl
    {

        #region Public Properties                                              

        public int Created_By { set; get; }

        public SubscriptionFeatureTypes SetAddSubscriptionFeatureTypeDataSource
        {
            set
            {
                Forte_AddFeatureType_isNotApplicableCheckBox.Visible = false;
                Forte_AddFeatureType_isNotApplicableCheckBox.Checked = false;
                Forte_AddFeatureType_featureSaveButton.CommandName = "New";
                Forte_AddFeatureType_messagetitleLiteral.Text = "Add Feature Type";
                Forte_AddFeatureType_featureDefaultValueHeaderTr.Visible = false;
                Forte_AddFeatureType_featureDefaultValueTr.Visible = false;
                Forte_AddFeatureType_isUnlimitedCheckBox.Checked = true;
                Forte_AddFeatureType_isUnlimitedCheckBox.Enabled = false;
            }
        }

        //public SubscriptionFeatureTypes SetEditSubscriptionFeatureTypeDataSource
        //{
        //    set
        //    {
        //        //Forte_AddFeatureType_messageTd.Style.Add("height", "36px");
        //        try
        //        {
        //            Forte_AddFeatureType_featureUnitDropDownList.SelectedItem.Selected = false;
        //            Forte_AddFeatureType_featureUnitDropDownList.Items.FindByText(value.Unitname.Trim()).Selected = true;
        //        }
        //        catch { }
        //        Forte_AddFeatureType_featureNameTextBox.Text = value.FeatureName;
        //        Forte_AddFeatureType_featureDefaultValueTextBox.Text = value.IsUnlimited == 1 ? "" : value.DefaultValue;
        //        Forte_AddFeatureType_featureIdHiddenLabel.Text = value.FeatureId.ToString();
        //        Forte_AddFeatureType_isUnlimitedCheckBox.Checked = value.IsUnlimited == 1 ? true : false;
        //        Forte_AddFeatureType_featureDefaultValueHeaderLabel.Text = "Default Value";
        //        if (Forte_AddFeatureType_isUnlimitedCheckBox.Checked)
        //            Forte_AddFeatureType_featureDefaultValueTextBox.Enabled = false;
        //        else
        //            Forte_AddFeatureType_featureDefaultValueTextBox.Enabled = true;
        //        Forte_AddFeatureType_featureSaveButton.CommandName = "Edit";
        //        SetVisibleStatus(false);
        //        Forte_AddFeatureType_isNotApplicableCheckBox.Checked = false;
        //        Forte_AddFeatureType_messagetitleLiteral.Text = "Edit Feature Type";
        //        Forte_AddFeatureType_featureDefaultValueHeaderTr.Visible = true;
        //        Forte_AddFeatureType_featureDefaultValueTr.Visible = true;
        //    }
        //}

        //public SubscriptionFeatures SetPricingSubscriptionFeaturesDataSource
        //{
        //    set
        //    {
        //        Forte_AddFeatureType_messageTd.Style.Remove("height");
        //        Forte_AddFeatureType_featureNameHeaderMandatorySpan.Visible = false;
        //        Forte_AddFeatureType_featureUnitHeaderMandatorySpan.Visible = false;
        //        Forte_AddFeatureType_featureIdHiddenLabel.Text = value.FeatureId.ToString();
        //        Forte_AddFeatureType_featureNameLabel.Text = value.FeatureName;
        //        Forte_AddFeatureType_featureIdHiddenLabel.Text = value.SubscriptionFeatureId.ToString();
        //        Forte_AddFeatureType_messagetitleLiteral.Text = "Subscription Pricing";
        //        SetVisibleStatus(true);
        //        Forte_AddFeatureType_featureDefaultValueTextBox.Text = value.IsNotApplicable == 1 || value.IsUnlimited == 1 ? 
        //                                                                "" : value.FeatureValue;
        //        Forte_AddFeatureType_defaultValueHidden.Value = value.DefaultValue;
        //        Forte_AddFeatureType_restoreDefaultValueLinkButton.Visible = true;
        //        Forte_AddFeatureType_isUnlimitedCheckBox.Checked = value.IsUnlimited == 1 ? true : false;
        //        Forte_AddFeatureType_isNotApplicableCheckBox.Checked = value.IsNotApplicable == 1 ? true : false;
        //        Forte_AddFeatureType_featureUnitLabel.Text = value.FeatureUnit;
        //        Forte_AddFeatureType_featureDefaultValueHeaderLabel.Text = "Feature Value";
        //        Forte_AddFeatureType_featureSaveButton.CommandName = "Pricing";
        //        if (Forte_AddFeatureType_isNotApplicableCheckBox.Checked || Forte_AddFeatureType_isUnlimitedCheckBox.Checked)
        //            Forte_AddFeatureType_featureDefaultValueTextBox.Enabled = false;
        //        else
        //            Forte_AddFeatureType_featureDefaultValueTextBox.Enabled = true;
        //    }
        //}

        #endregion Public Properties

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                    return;
                BindFeatureUnitsDropDownList();
                //Forte_AddFeatureType_isUnlimitedCheckBox.Attributes.Add("onclick", "Unlimited('" +
                //    Forte_AddFeatureType_isNotApplicableCheckBox.ClientID + "','" +
                //    Forte_AddFeatureType_isUnlimitedCheckBox.ClientID + "','" +
                //    Forte_AddFeatureType_featureDefaultValueTextBox.ClientID + "');");
                Forte_AddFeatureType_isNotApplicableCheckBox.Attributes.Add("onclick", "NotApplicableClick('" +
                    Forte_AddFeatureType_isNotApplicableCheckBox.ClientID + "','" +
                    Forte_AddFeatureType_isUnlimitedCheckBox.ClientID + "','" +
                    Forte_AddFeatureType_featureDefaultValueTextBox.ClientID + "');");
                Forte_AddFeatureType_restoreDefaultValueLinkButton.Attributes.Add("onclick", "return DeafultValue('" +
                    Forte_AddFeatureType_defaultValueHidden.ClientID + "','" +
                    Forte_AddFeatureType_featureDefaultValueTextBox.ClientID + "','" +
                    Forte_AddFeatureType_isUnlimitedCheckBox.ClientID + "','" +
                    Forte_AddFeatureType_isNotApplicableCheckBox.ClientID + "');");
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_AddFeatureType_featureSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                switch(Forte_AddFeatureType_featureSaveButton.CommandName)
                {
                    case "New":
                        if (!IsNewOrEditValidData())
                            return;
                    InsertFeatureSubscriptionType();
                        break;
                    case "Edit":
                        if (!IsNewOrEditValidData())
                            return;
                    UpdateFeatureSubscriptionType();
                        break;
                    case "Pricing":
                        UpdateSubscriptionFeaturePricing();
                        break;
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_AddFeatureType_featureCancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        private void SetVisibleStatus(bool VisibleStatus)
        {
            Forte_AddFeatureType_isNotApplicableCheckBox.Visible = VisibleStatus;
            Forte_AddFeatureType_featureNameLabel.Visible = VisibleStatus;
            Forte_AddFeatureType_featureUnitLabel.Visible = VisibleStatus;
            Forte_AddFeatureType_featureNameTextBox.Visible = !VisibleStatus;
            //Forte_AddFeatureType_isUnlimitedCheckBox.Visible = !VisibleStatus;
            Forte_AddFeatureType_featureUnitDropDownList.Visible = !VisibleStatus;
        }

        private void UpdateSubscriptionFeaturePricing()
        {
            SubscriptionFeatures subscriptionFeatures = new SubscriptionFeatures();
            subscriptionFeatures.SubscriptionFeatureId = Convert.ToInt32(Forte_AddFeatureType_featureIdHiddenLabel.Text);
            subscriptionFeatures.FeatureValue = Forte_AddFeatureType_featureDefaultValueTextBox.Text;
            subscriptionFeatures.IsNotApplicable = Convert.ToInt16(Forte_AddFeatureType_isNotApplicableCheckBox.Checked ? 1 : 0);
            subscriptionFeatures.IsUnlimited = Convert.ToInt16(Forte_AddFeatureType_isUnlimitedCheckBox.Checked ? 1 : 0);
            subscriptionFeatures.ModifiedBy = Created_By;
            new SubscriptionBLManager().UpdateSubscriptionFeaturePricing(subscriptionFeatures);
            ClearControls();
            InvokePageBaseMethod("ShowSuccessMessage", new object[] { HCMResource.FeatureSubscription_PricingUpdatedSuccessfully });
            InvokePageBaseMethod("RebindGridView", null);
            InvokePageBaseMethod("CheckAndInvokeRePublishModalPopUp", null);
        }

        private void UpdateFeatureSubscriptionType()
        {
            SubscriptionFeatureTypes subscriptionFeatureTypes = new SubscriptionFeatureTypes();
            subscriptionFeatureTypes.FeatureId = Convert.ToInt32(Forte_AddFeatureType_featureIdHiddenLabel.Text);
            subscriptionFeatureTypes.FeatureName = Forte_AddFeatureType_featureNameTextBox.Text.Trim();
            subscriptionFeatureTypes.Unit = Forte_AddFeatureType_featureUnitDropDownList.SelectedValue;
            subscriptionFeatureTypes.DefaultValue = Forte_AddFeatureType_featureDefaultValueTextBox.Text.Trim();
            subscriptionFeatureTypes.IsUnlimited = Forte_AddFeatureType_isUnlimitedCheckBox.Checked ? Convert.ToInt16(1) : Convert.ToInt16(0);
            subscriptionFeatureTypes.ModifiedBy = Created_By;
            new SubscriptionBLManager().UpdateFeatureSubscriptionType(subscriptionFeatureTypes);
            ClearControls();
            InvokePageBaseMethod("RebindSubscriptionFeatureTypeGridView", null);
            InvokePageBaseMethod("ShowSuccessMessage", new object[] { HCMResource.SubscriptionFeatureType_UpdatedSuccessfully });
        }

        private void InsertFeatureSubscriptionType()
        {
            SubscriptionFeatureTypes subscriptionFeatureTypes = new SubscriptionFeatureTypes();
            subscriptionFeatureTypes.FeatureName = Forte_AddFeatureType_featureNameTextBox.Text.Trim();
            subscriptionFeatureTypes.Unit = Forte_AddFeatureType_featureUnitDropDownList.SelectedValue;
            subscriptionFeatureTypes.DefaultValue = Forte_AddFeatureType_featureDefaultValueTextBox.Text.Trim();
            subscriptionFeatureTypes.IsUnlimited = Forte_AddFeatureType_isUnlimitedCheckBox.Checked ? Convert.ToInt16(1) : Convert.ToInt16(0);
            subscriptionFeatureTypes.CreatedBy = Created_By;
            new SubscriptionBLManager().InsertFeatureSubscriptionType(subscriptionFeatureTypes);
            ClearControls();
            InvokePageBaseMethod("RebindSubscriptionFeatureTypeGridView", null);
            InvokePageBaseMethod("ShowSuccessMessage", new object[] { HCMResource.SubscriptionFeatureType_InsertSuccessfull });
        }

        private void BindFeatureUnitsDropDownList()
        {
            Forte_AddFeatureType_featureUnitDropDownList.DataSource = GetFeatureUnitsDropDownLists();
            Forte_AddFeatureType_featureUnitDropDownList.DataTextField = "AttributeName";
            Forte_AddFeatureType_featureUnitDropDownList.DataValueField = "AttributeID";
            Forte_AddFeatureType_featureUnitDropDownList.DataBind();
            Forte_AddFeatureType_featureUnitDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));
            //Forte_AddFeatureType_featureUnitDropDownList.Items.Insert(
              //  Forte_AddFeatureType_featureUnitDropDownList.Items.Count, new ListItem("--Other--", Forte_AddFeatureType_featureUnitDropDownList.Items.Count.ToString()));
        }

        private List<AttributeDetail> GetFeatureUnitsDropDownLists()
        {
            return new AttributeBLManager().GetAttributesByType("FEAT_UNIT ");
        }

        private void ClearControls()
        {
            Forte_AddFeatureType_featureNameTextBox.Text = "";
            Forte_AddFeatureType_featureDefaultValueTextBox.Text = "";
            Forte_AddFeatureType_featureUnitDropDownList.SelectedIndex = 0;
            Forte_AddFeatureType_featureIdHiddenLabel.Text = "";
            //Forte_AddFeatureType_editModeHiddenLabel.Text = false.ToString();
            //Forte_AddFeatureType_pricingModeHiddenLabel.Text = false.ToString();
        }

        private bool IsNewOrEditValidData()
        {
            bool IsValid = true;
            if (string.IsNullOrEmpty(Forte_AddFeatureType_featureNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(new Exception(HCMResource.SubscriptionFeatureType_EmptyFeatureName));
                IsValid = false;
            }
            if (Forte_AddFeatureType_featureUnitDropDownList.SelectedIndex == 0)
            {
                ShowErrorMessage(new Exception(HCMResource.SubscriptionFeatureType_UnitTypeNotSelected));
                IsValid = false;
            }
            if (!(Forte_AddFeatureType_featureUnitDropDownList.SelectedIndex == Forte_AddFeatureType_featureUnitDropDownList.Items.Count - 1))
            {
                int Temp = 0;
                if (Forte_AddFeatureType_featureDefaultValueTextBox.Text.Trim() != "")
                {
                    int.TryParse(Forte_AddFeatureType_featureDefaultValueTextBox.Text.Trim(), out Temp);
                    if (Temp == 0)
                    {
                        ShowErrorMessage(new Exception(HCMResource.SubscriptionFeatureType_InvalidDefaultValue));
                        IsValid = false;
                    }
                }
            }
            if (!IsValid)
                InvokePageBaseMethod("ShowSubscriptionFeatureTypeModalPopUp", null);
            return IsValid;
        }

        /// <summary>
        /// Invokes the parent page base public methods
        /// </summary>
        /// <param name="MethodName">
        /// A <see cref="System.String"/> that holds the method name to invoke
        /// </param>
        /// <param name="args">
        /// A <see cref="System.Object"/> that holds the arguments to be passed to the method
        /// </param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        private void ShowErrorMessage(Exception exp)
        {
            if (string.IsNullOrEmpty(Forte_AddFeatureType_topErrorMessageLabel.Text))
                Forte_AddFeatureType_topErrorMessageLabel.Text = exp.Message;
            else
                Forte_AddFeatureType_topErrorMessageLabel.Text += "<br />" + exp.Message;
        }

        #endregion Private Methods
    }
}