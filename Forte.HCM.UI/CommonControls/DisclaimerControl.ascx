﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisclaimerControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.DisclaimerControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="DisclaimerControl_titleLiteral" runat="server"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="DisclaimerControl_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                        OnClick="DisclaimerControl_cancelButton_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10" align="left">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left">
                                    <div style="overflow: auto; height: 90px; width: 480px;">
                                        <asp:Label ID="DisclaimerControl_messageLabel" SkinID="sknLabelFieldText" runat="server"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="DisclaimerControl_hiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
