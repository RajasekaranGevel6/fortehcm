#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateDetailControl.cs
// File that represents the layout for the user interface 
// to show the candidate details based on selected candidate session ID

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Text;
using System.Web.UI;
using System.Reflection;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that represents the layout for the user interface 
    /// to show the candidate details based on selected candidate session ID
    /// </summary>
    public partial class CandidateInterviewDetailControl : UserControl
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Event handler that helps to set default button, focus and page title.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            CandidateInterviewDetailControl_topErrorMessageLabel.Text = string.Empty;

            if (!IsPostBack)
            {
                Page.Form.DefaultButton = CandidateInterviewDetailControl_candidateSkillMatrixLinkButton.UniqueID;

                CandidateInterviewDetailControl_headerLiteral.Text =
                    Resources.HCMResource.CandidateDetailControl_TitleMessage;

               
                
            }
        }

        /// <summary>
        /// Handler method that will be called when the download link is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This applies to download the resume source for selected candidate session ID.
        /// </remarks>
        protected void CandidateInterviewDetailControl_downloadResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value == null ||
                    CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value.Trim().Length == 0)
                {
                    CandidateInterviewDetailControl_topErrorMessageLabel.Text = "Candidate resume not found <br><br>";
                    Page.GetType().InvokeMember("ShowModalPopup", BindingFlags.InvokeMethod, null,
                        this.Page, null);
                    return;
                }

                string mimeType;
                string fileName;
                Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = null;

                byte[] resumeContent = new CandidateBLManager().GetResumeContents
                    (CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value, out mimeType, out fileName);
                
                if ((resumeContent == null) || (resumeContent.Length == 0))
                {
                    CandidateInterviewDetailControl_topErrorMessageLabel.Text = "No resume found " +
                        "for candidate : " +
                         CandidateInterviewDetailControl_candidateNameHiddenField.Value + "<br><br>";
                    Page.GetType().InvokeMember("ShowModalPopup", BindingFlags.InvokeMethod,null, 
                        this.Page, null);
                    return;
                }

                Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = resumeContent;
                Response.Redirect("../Common/Download.aspx?fname=" + fileName + "&mtype=" +
                    mimeType + " &type=DownloadResume", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the show test score link is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This applies to view the test score of selected candidate session ID.
        /// </remarks>
        protected void CandidateInterviewDetailControl_showTestScoreButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Assessments/CandidateAssessorSummary.aspx?m=4&s=0" +
                "&candidatesessionid=" + CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value.Trim() +
                "&attemptid=" + int.Parse(CandidateInterviewDetailControl_candidateattemptIDHiddenField.Value.Trim()) +
                "&testkey=" + CandidateInterviewDetailControl_candidateTestKeyHiddenField.Value.Trim() +
                "&parentpage=S_ITSN", false);
        }

        #endregion Event Handlers                                              

        #region Public Properties                                              

        /// <summary>
        /// This property assigns candidate session id,attempt id and test id to hidden controls.
        /// It also loads candidate details.
        /// </summary>
        public CandidateTestDetail Datasource
        {
            set
            {
                CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value = value.CandidateSessionID;
                CandidateInterviewDetailControl_candidateattemptIDHiddenField.Value = value.AttemptID.ToString();
                CandidateInterviewDetailControl_candidateTestKeyHiddenField.Value = value.TestID;
                CandidateInterviewDetailControl_candidateNameHiddenField.Value = value.CandidateName;             
                
                CandidateInterviewDetailControl_candidateSkillMatrixLinkButton.Attributes.Add("onclick",
                    "javascript: return OpenSkillsMatrixWithCandidateSessionId('"
                    + CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value + "','INT');");

                CandidateInterviewDetailControl_showTestScoreButton.Visible = value.ShowTestScore;                  

                CandidateDetail();
            }
        }

        #endregion Public Properties                                           

        #region Private Properties                                             

        /// <summary>
        /// Method that loads candidate details like name,location,email,phone,synopsis and notes.
        /// </summary>
        private void CandidateDetail()
        {
            Resume resume = null;
            StringBuilder locationStringBuilder = null;
            try
            {
                ClearValues();

                if (Utility.IsNullOrEmpty(CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value))
                    return;

                string hrXMLSource = string.Empty;

                if (Utility.IsNullOrEmpty(hrXMLSource =
                    new CandidateBLManager().GetInterviewCandidateHRXml(
                        CandidateInterviewDetailControl_candidateSessionIDHiddenField.Value.Trim())))
                    return;
                try
                {
                    resume = new HRXMLManager(hrXMLSource).Resume;
                }
                catch (NullReferenceException)
                {
                    return;
                }
                if (Utility.IsNullOrEmpty(resume))
                    return;

                CandidateInterviewDetailControl_candidateNameTextBox.Text = resume.Name.FirstName;

                locationStringBuilder = new StringBuilder();

                if (!Utility.IsNullOrEmpty(resume.ContactInformation.StreetAddress))
                    locationStringBuilder.Append(resume.ContactInformation.StreetAddress + " ");
                if (!Utility.IsNullOrEmpty(resume.ContactInformation.City))
                    locationStringBuilder.Append(resume.ContactInformation.City + " ");
                if (!Utility.IsNullOrEmpty(resume.ContactInformation.State))
                    locationStringBuilder.Append(resume.ContactInformation.State);

                CandidateInterviewDetailControl_candidateLocationTextBox.Text =
                    locationStringBuilder.ToString().TrimEnd(' ');
                CandidateInterviewDetailControl_candidateEmailTextBox.Text =
                    resume.ContactInformation.EmailAddress;
                CandidateInterviewDetailControl_candidatePhoneTextBox.Text =
                    resume.ContactInformation.Phone.Mobile;
                CandidateInterviewDetailControl_candidateSynopsisLiteral.Text =
                    resume.ExecutiveSummary;
            }
            finally
            {
                if (resume != null)resume = null;
                if (!Utility.IsNullOrEmpty(locationStringBuilder)) locationStringBuilder = null;
            }
        }

        /// <summary>
        /// Method that clears all textbox and literal values
        /// </summary>
        private void ClearValues()
        {
            CandidateInterviewDetailControl_candidateNameTextBox.Text = string.Empty;
            CandidateInterviewDetailControl_candidateEmailTextBox.Text = string.Empty;
            CandidateInterviewDetailControl_candidateLocationTextBox.Text = string.Empty;
            CandidateInterviewDetailControl_candidatePhoneTextBox.Text = string.Empty;
            CandidateInterviewDetailControl_candidateNotesLiteral.Text = string.Empty;
            CandidateInterviewDetailControl_candidateSynopsisLiteral.Text = string.Empty;
        }

        #endregion Private Properties                                          

       
    }
}