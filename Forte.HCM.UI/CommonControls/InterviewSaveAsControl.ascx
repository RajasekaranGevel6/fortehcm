﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterviewSaveAsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.InterviewSaveAsControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10" style="width: 100%">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="InterviewSaveAsControl_titleLiteral" Text="Create a Copy" runat="server"></asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" OnClick="InterviewSaveAsControl_cancelButton_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td style="height: 5px"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="InterviewSaveAsControl_topErrorMessageLabel" runat="server" EnableViewState="false"
                            SkinID="sknErrorMessage" Width="100%"></asp:Label>
                        <asp:Label ID="InterviewSaveAsControl_topSuccessMessageLabel" runat="server" EnableViewState="false"
                            SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" cellpadding="2" cellspacing="0" border="0">
                            <tr>
                                <td style="width: 100%">
                                    <table cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="publish_candidate_interview_response_instructions">
                                                Do you want to copy a interview with
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:5px">
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 100%">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="InterviewSaveAsControl_skillsCheckBox" Text="Skills"  runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:5px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="InterviewSaveAsControl_questionSetCheckBox" Text="Question Set"  runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:5px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="InterviewSaveAsControl_assessorDetaillsCheckBox" Text="Assessor Details"  runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:50px">
                        <asp:HiddenField ID="InterviewSaveAsControl_interviewKeyHiddenField" runat="server" />
                        <asp:HiddenField ID="InterviewSaveAsControl_userIDHiddenField" runat="server" />
                        <asp:Button ID="InterviewSaveAsControl_interviewSaveAsButton" runat="server" Text="Save"
                            SkinID="sknButtonId" 
                            onclick="InterviewSaveAsControl_interviewSaveAsButton_Click"/>
                    </td>
                    <td>
                        <asp:LinkButton ID="InterviewSaveAsControl_cancelLinkButton" runat="server" 
                            Text="Cancel" SkinID="sknCancelLinkButton"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
