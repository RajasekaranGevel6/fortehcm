﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ContactControl" %>
<%@ Register Src="~/CommonControls/MultiSelectControl.ascx" TagName="MultiSelectControl"
    TagPrefix="uc1" %>
<asp:Panel ID="ContactControl_mainPanel" runat="server" DefaultButton="ContactControl_createButton">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="ContactControl_messagetitleLiteral" runat="server">Contact</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="ContactControl_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="contact_details_modal_popup_bg">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr id="ContactControl_messageTd" runat="server" class="td_height_20">
                                    <td>
                                        <asp:Label ID="ContactControl_topErrorMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                        <asp:Label ID="ContactControl_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="ContactControl_panel" runat="server">
                                            <table border="0" cellspacing="3" cellpadding="3">
                                                <tr id="ContactControl_clientNameTR" runat="server">
                                                    <td>
                                                        <asp:Label ID="ContactControl_clientNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Client Name"></asp:Label>
                                                        <span class="mandatory">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ContactControl_clientNameDropDownList" runat="server" AutoPostBack="true"
                                                            AppendDataBoundItems="true">
                                                            <asp:ListItem Text="--Sellect--" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ContactControl_firstNameLabel" Text="First Name" runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <span class="mandatory">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="ContactControl_firstNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="ContactControl_primaryAddressCheckbox" Text="Primary Address" EnableViewState="true"
                                                            Visible="false" runat="server" AutoPostBack="True" OnCheckedChanged="ContactControl_primaryAddressCheckbox_CheckedChanged" />
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td5" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td6" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td7" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td8" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td9" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td10" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td11" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td12" runat="server">
                                                    </td>
                                                </tr>
                                                <%--  <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td13" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td14" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td15" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td16" runat="server">
                                                    </td>
                                                </tr>
                                                <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td17" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td18" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td19" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td20" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td id="ContactControl_td21" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td22" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td23" runat="server">
                                                    </td>
                                                    <td id="ContactControl_td24" runat="server">
                                                    </td>
                                                </tr>
                                                <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ContactControl_additionalInfoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Additional Info" Visible="false"></asp:Label>
                                                       
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="ContactControl_additionalInfoTextBox" Columns="80" runat="server"
                                                            MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
                                                            onchange="CommentsCount(500,this)" Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ContactControl_deparmentListLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Departments"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <div style="height: 50px; width:450px; overflow: auto; vertical-align: middle; text-align: left">
                                                            <asp:CheckBoxList ID="ContactControl_departmentCheckboxList" runat="server" RepeatColumns="2"
                                                                Width="100%">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                        <%--  <uc1:MultiSelectControl ID="ContactControl_departmentMultiSelectControl" runat="server" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" align="left">
                <asp:Button ID="ContactControl_createButton" runat="server" Text="Save" SkinID="sknButtonId"
                    ValidationGroup="a" OnCommand="ContactControl_createButton_Command" />
                &nbsp; &nbsp;
                <asp:LinkButton ID="ContactControl_featureCancelLinkButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton">
                </asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="ContactControl_clientIDHiddenField" runat="server" />
    <asp:Label ID="ContactControl_middleNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Middle Name" Visible="false" EnableViewState="true"></asp:Label>
   
    <asp:TextBox ID="ContactControl_middleNameTextBox" runat="server" MaxLength="50"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_lastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Lsat Name"></asp:Label>
  
    <asp:TextBox ID="ContactControl_lastNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
    <asp:Label ID="ContactControl_streetAddressLabel" Text="Street Address" runat="server"
        SkinID="sknLabelFieldHeaderText" Visible="false" EnableViewState="true"></asp:Label>
  
    <asp:TextBox ID="ContactControl_streetAddressTextBox" runat="server" MaxLength="100"
        Visible="false" EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_cityLabel" Text="City" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false" EnableViewState="true"></asp:Label>
    
    <asp:TextBox ID="ContactControl_cityTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_stateLabel" Text="State" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
   
    <asp:TextBox ID="ContactControl_stateTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_zipCodeLabel" Text="Zipcode" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
   
    <asp:TextBox ID="ContactControl_zipCodeTextBox" runat="server" MaxLength="6" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_countryLabel" Text="Country" runat="server" SkinID="sknLabelFieldHeaderText"
        Visible="false"></asp:Label>
  
    <asp:TextBox ID="ContactControl_countryTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_phoneNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Phone No" Visible="false"></asp:Label>
    <span class="mandatory" id="ContactControl_phoneNoSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="ContactControl_phoneNoTextBox" runat="server" MaxLength="50" Visible="false"
        EnableViewState="true"></asp:TextBox>
    <asp:Label ID="ContactControl_faxNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Fax No" Visible="false"></asp:Label>
   
    <asp:TextBox ID="ContactControl_faxNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
    <asp:Label ID="ContactControl_eMailIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
        Text="Email ID" Visible="false"></asp:Label>
    <span class="mandatory" id="ContactControl_eMailIDSpan" runat="server" visible="false">
        *</span>
    <asp:TextBox ID="ContactControl_eMailIDTextBox" runat="server" MaxLength="200" Visible="false"
        EnableViewState="true"></asp:TextBox>
</asp:Panel>
