<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestDetailsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestDetailsControl" %>
<script type="text/javascript">

    function ClearPositionProfile()
    {
        document.getElementById("<%= TestDetailsControl_positionProfileTextBox.ClientID%>").value = '';
        document.getElementById("<%= TestDetailsControl_positionProfileIDHiddenField.ClientID%>").value = '';

        return false;
    }

    function ToggleCertificationDiv()
    {
        var objDiv = document.getElementById('<%=TestDetailsControl_CertificationDiv.ClientID %>');
        var objChk = document.getElementById('<%=TestDetailsControl_isCertificationtestCheckBox.ClientID %>');

        if (null != objChk && true == objChk.checked)
        {

            objDiv.style.display = 'block';
        } else
        {
            objDiv.style.display = 'none';
        }
        return true;
    }

    function SearchCertificateFormatPopUp(ctrlCertificateId, ctrlNameId, ctrlCertificateNameHiddenId)
    {

        var height = 630;
        var width = 680;
        var top = (screen.availHeight - parseInt(height)) / 2;
        var left = (screen.availWidth - parseInt(width)) / 2;
        var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
        var queryStringValue = "../popup/SearchCertificateFormat.aspx?ctrlNameId=" + ctrlCertificateId + "&ctrlId=" + ctrlNameId + "&ctrlNameHiddendId=" + ctrlCertificateNameHiddenId;
        //window.open(queryStringValue, window.self, sModalFeature);
        window.open(queryStringValue, window.self, sModalFeature);
        return false;
    }
    function isNumeric(keyCode)
    {
        if (!(keyCode >= 65 && keyCode <= 90))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
</script>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td class="panel_bg">
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                                        <tr>
                                            <td colspan="5" class="header_bg">
                                                <asp:Literal ID="TestDetailsControl_testDetailsMessageLiteral" runat="server" Text="Test Details"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table border="0" cellspacing="3" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testIdLabel" Text="Test ID" runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testDetailsTestIdReadOnlyLabel" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testNameLabel" Text="Test Name" runat="server"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TestDetailsControl_testNameTextBox" Columns="50" runat="server"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_testDescriptionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Test Description"></asp:Label><span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TestDetailsControl_testDescriptionTextBox" runat="server" Columns="80"
                                                                TextMode="MultiLine" Height="60" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                onchange="CommentsCount(500,this)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_systemRecommendedTimeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="System Recommended Time"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellspacing="5">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="TestDetailsControl_sysRecommendedTimeFieldLabel" runat="server" SkinID="sknLabelFieldText"
                                                                            Text="00:00:00"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="TestDetailsControl_recommendedTimeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Recommended Completion Time"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TestDetailsControl_recommendedTimeLabelTextBox" Columns="5" runat="server"
                                                                            Text="00:00:00"></asp:TextBox>
                                                                        <ajaxToolKit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder=""
                                                                            CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                                                            CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                                            Enabled="True" ErrorTooltipEnabled="True" UserTimeFormat="TwentyFourHour" Mask="99:99:99"
                                                                            MaskType="Time" TargetControlID="TestDetailsControl_recommendedTimeLabelTextBox"
                                                                            AutoComplete="true" AutoCompleteValue="00:00:00">
                                                                        </ajaxToolKit:MaskedEditExtender>
                                                                        <%--   <ajaxToolkit:MaskedEditValidator runat="server"
                                                                                ControlExtender="MaskedEditExtender1"
                                                                                ControlToValidate="TestDetailsControl_recommendedTimeLabelTextBox" 
                                                                                IsValidEmpty="true" 
                                                                                MaximumValue="23:59:59" 
                                                                                EmptyValueBlurredText="*" 
                                                                                InvalidValueBlurredMessage="*" 
                                                                                MaximumValueBlurredMessage="*" 
                                                                                MinimumValueBlurredText="*"
                                                                                Display="Dynamic" 
                                                                                TooltipMessage="Input a number: -00:00:00 up to 23:59:59"/>--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="TestDetailsControl_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Position Profile"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <div style="float: left; padding-right: 2px;">
                                                                <asp:TextBox ID="TestDetailsControl_positionProfileTextBox" MaxLength="50" ReadOnly="true" Columns="50"
                                                                    runat="server">
                                                            </asp:TextBox>
                                                            </div>
                                                            <div style="float: left; height: 16px;padding-right: 4px">
                                                                <asp:ImageButton ID="TestDetailsControl_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile and associate with test" />
                                                                 <asp:ImageButton ID="TestDetailsControl_positionProfileHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Click here to select the position profile to associate with the test" />
                                                            </div>
                                                            <div style="padding-top: 2px; padding-left: 10px">
                                                                 <asp:LinkButton ID="TestDetailsControl_clearPositionProfileLinkButton" runat="server"
                                                                        Text="Clear" SkinID="sknActionLinkButton" OnClientClick="javascript:return ClearPositionProfile()"
                                                                        ToolTip="Click here to clear the 'Position Profile' field" />
                                                            </div>
                                                            <asp:HiddenField ID="TestDetailsControl_positionProfileIDHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                            <tr style="display:none">
                                <td class="header_bg" >
                                    <asp:CheckBox ID="TestDetailsControl_isCertificationtestCheckBox" runat="server"
                                        Checked="false" Text="Certification" SkinID="sknLabelFieldHeaderText" />
                                    <%--<asp:Label ID="CreateManualTest_CertificationLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Certification"></asp:Label>--%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="width: 100%" class="grid_body_bg">
                                    <div id="TestDetailsControl_CertificationDiv" runat="server" style="display: block;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="TestDetailsControl_maxTotalScoreLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                        Text="Minimum total score required to qualify for certification (in %)"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="TestDetailsControl_maxTotalScoreTextBox" runat="server" Width="33px"
                                                        onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                    <%--  <ajaxToolKit:MaskedEditExtender ID="TestDetailsControl_maxTotalScoreMaskedEditExtender"
                                                        runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True" ErrorTooltipEnabled="True" Mask="999"
                                                        MaskType="Number" AutoComplete="true" AutoCompleteValue=" " InputDirection="LeftToRight"
                                                        TargetControlID="TestDetailsControl_maxTotalScoreTextBox">
                                                    </ajaxToolKit:MaskedEditExtender>
                                                    <ajaxToolKit:MaskedEditValidator ID="TestDetailsControl_maxTotalScoreMaskedEditValidator"
                                                        runat="server" ControlExtender="TestDetailsControl_maxTotalScoreMaskedEditExtender"
                                                        ControlToValidate="TestDetailsControl_maxTotalScoreTextBox" IsValidEmpty="true"
                                                        MaximumValue="100.00" InvalidValueMessage="Number is not valid" MaximumValueMessage="Percentage should not exceed 100.00"
                                                        Display="Dynamic" EmptyValueBlurredText="" EmptyValueMessage=""></ajaxToolKit:MaskedEditValidator>--%>
                                                    <%--<asp:RangeValidator ID="TestDetailsControl_maxTotalScoreRangeValidator" runat="server"
                                                        ControlToValidate="TestDetailsControl_maxTotalScoreTextBox" ErrorMessage="Percentage value should be between 1% and 100%"
                                                        MaximumValue="100.00" MinimumValue="1" Text="" Type="Double" SetFocusOnError="True"
                                                        ValidationGroup="1"></asp:RangeValidator>--%>
                                                    <asp:RangeValidator ID="interestRateRangeValidator" runat="server" ControlToValidate="TestDetailsControl_maxTotalScoreTextBox"
                                                        MaximumValue="100" MinimumValue="1" Type="Double" ErrorMessage="Please enter a value between 0% and 100%."
                                                        SetFocusOnError="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="TestDetailsControl_MaximumtimepermissibleScoreLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Maximum time permissible to complete the test to qualify for certification">
                                                    </asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TestDetailsControl_MaximumtimepermissibleScoreTextBox" runat="server"
                                                        Columns="5"></asp:TextBox>
                                                    <ajaxToolKit:MaskedEditExtender ID="CreateTestSession_timeLimitMaskedEditExtender"
                                                        runat="server" Enabled="True" ErrorTooltipEnabled="True" Mask="99:99:99" MaskType="Time"
                                                        AutoComplete="true" AutoCompleteValue="00:00:00" TargetControlID="TestDetailsControl_MaximumtimepermissibleScoreTextBox">
                                                    </ajaxToolKit:MaskedEditExtender>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="TestDetailsControl_numberofpermissibleLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                        Text="Number of permissible retakes per candidate"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TestDetailsControl_numberofpermissibleTextBox" runat="server" Width="30px"
                                                        MaxLength="2"></asp:TextBox>
                                                    <ajaxToolKit:MaskedEditExtender ID="TestDetailsControl_numberofpermissibleMaskedEditExtender"
                                                        runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True" ErrorTooltipEnabled="True" Mask="99"
                                                        MaskType="None" AutoCompleteValue="00" AcceptNegative="None" AutoComplete="true"
                                                        TargetControlID="TestDetailsControl_numberofpermissibleTextBox">
                                                    </ajaxToolKit:MaskedEditExtender>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="TestDetailsControl_timeperiodrequiredLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                        Text="Time period required to elapse between retakes(In Days)"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="TestDetailsControl_timeperiodrequiredTextBox" runat="server" Width="30px"></asp:TextBox>
                                                    <ajaxToolKit:MaskedEditExtender ID="TestDetailsControl_timeperiodrequiredMaskedEditExtender"
                                                        runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True" ErrorTooltipEnabled="True" Mask="999"
                                                        MaskType="Number" AutoCompleteValue="00" AutoComplete="false" TargetControlID="TestDetailsControl_timeperiodrequiredTextBox">
                                                    </ajaxToolKit:MaskedEditExtender>
                                                    <ajaxToolKit:MaskedEditValidator ID="TestDetailsControl_MaskedEditValidator" runat="server"
                                                        ControlExtender="TestDetailsControl_timeperiodrequiredMaskedEditExtender" ControlToValidate="TestDetailsControl_timeperiodrequiredTextBox"
                                                        IsValidEmpty="true" MaximumValue="999" MinimumValue="0" MinimumValueMessage="Time period should not be zero"
                                                        InvalidValueMessage="Invalid time period" Display="Dynamic">
                                                    </ajaxToolKit:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="TestDetailsControl_certificateformatLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                        Text="Certificate format"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td colspan="2">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:TextBox ID="TestDetailsControl_certificateFormatNameTextBox" runat="server"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="TestDetailsControl_certificateformatSearchImageButton" runat="server"
                                                            ImageAlign="Middle" SkinID="sknbtnSearchicon" ToolTip="Click here to select certificate format" />
                                                    </div>
                                                    <asp:HiddenField ID="TestDetailsControl_certificateFormatNameHiddenField" runat="server" />
                                                    <asp:HiddenField ID="TestDetailsCotnrol_certificateIdHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="TestDetailsControl_periodofcertificationvalidityLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Period of certification validity"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td colspan="2">
                                                    <div style="width: 150px;">
                                                        <asp:DropDownList ID="TestDetailsControl_certificationValidityDropDownList" runat="server"
                                                            Width="50%">
                                                            <%--<asp:ListItem Selected="True" Text="--Select--">                                                  
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="6 Months">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="1 Year">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="1.6 Years">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="2 Years">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="2.6 Years">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="Never Expires">
                                                        </asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <%--<asp:TextBox ID="TestDetailsControl_periodofcertificationvalidityTextBox" runat="server"></asp:TextBox>--%>
                                                    <%-- <ajaxToolKit:CalendarExtender ID="TestDetailsControl_periodofcertificationvalidityCalendarExtender"
                                                        runat="server" CssClass="MyCalendar" Enabled="True" Format="MM/dd/yyyy" TargetControlID="TestDetailsControl_periodofcertificationvalidityTextBox">
                                                    </ajaxToolKit:CalendarExtender>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript" language="javascript">
                               
                                
</script>
