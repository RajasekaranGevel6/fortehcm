﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// GroupAnalysisCategoryChartControl.cs
// Control that represents the user interface and assign data source for the 
// GroupAnalysisCategoryChartControl

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class GroupAnalysisCategoryChartControl : UserControl, IWidgetControl
    {       
        string testkey = string.Empty;
        List<DropDownItem> dropDownItemList = null;

        #region Event Handlers                                                 

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                GroupAnalysisCategoryChartControl_testKeyhiddenField.Value = selectedProperty[0];
                GroupAnalysisCategoryChartControl_absoluteScoreRadioButton.Checked = selectedProperty[2] == "0" ? false : true;
                GroupAnalysisCategoryChartControl_relativeScoreRadioButton.Checked = selectedProperty[2] == "1" ? false : true;
                GroupAnalysisCategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[1]);
                GroupAnalysisCategoryChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on radio button 
        ///  button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void GroupAnalysisCategoryChartControl_absoluteScoreRadioButton_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GroupAnalysisCategoryChartControl_selectProperty(object sender, EventArgs e)
        {
            GroupAnalysisCategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(false);

            LoadChart(ViewState["instance"] as WidgetInstance,"1");
        }

        /// <summary>
        /// Hanlder method that will be called when click the link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void GroupAnalysisCategoryChartControl_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on multi select link button added
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void GroupAnalysisCategoryChartControl_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion Event Handlers

        #region Private Methods                                                
        /// <summary>
        /// Represents the method to get the chart details from the database
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            GroupAnalysisCategoryChartControl_testKeyhiddenField.Value = candidateReportDetail.TestKey;

            DataTable multiSeriesDataTable = new ReportBLManager().
                GetGroupAnalysisReportCategoryDetails(candidateReportDetail);

            if (multiSeriesDataTable == null)
                return;

            var category = (from r in multiSeriesDataTable.AsEnumerable()
                            select new DropDownItem(r["Name"].ToString().Trim()
                               , r["CAT_KEY"].ToString())).Distinct();

            GroupAnalysisCategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = category.ToList();

            //Select all the check box in the check box list
            GroupAnalysisCategoryChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
      
            Session["CategoryChartDataSource"] = multiSeriesDataTable;

            LoadChart(instance,"1");

        }

        /// <summary>
        /// Represents the method to load the chart 
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the 
        /// instance of the widget
        /// </param>
        private void LoadChart(WidgetInstance instance,string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            //Declare the data table for the category chart 
            DataTable categoryChartDataTable = null;

            //Use string builder and string for naming the chart
            //This is used to store all the selected category
            StringBuilder selectedCategoryId = new StringBuilder();
            string selectedCategoryIds = "";

            //Take the test key from the hidden field . this is also used for 
            //naming the chart
            testkey = GroupAnalysisCategoryChartControl_testKeyhiddenField.Value;
            selectedPrintProperty.Append(testkey+"|");
            //Assign the category  chart data source
            MultipleSeriesChartData categoryChartDataSource = new MultipleSeriesChartData();

            //Get the datatable from the session 
            if ((Session["CategoryChartDataSource"]) != null)
            {
                categoryChartDataTable = Session["CategoryChartDataSource"]
                    as DataTable;
            }

            //Declare another datatable for storing the selected chart details
            DataTable selectedChartData = categoryChartDataTable.Clone();
           
            //Get the list of selected item in the multi select control
            List<DropDownItem> selectedList = GroupAnalysisCategoryChartControl_widgetMultiSelectControl.
                SelectedItems();

            #region Coding using LINQ
            //var chartDatatable = categoryChartDataTable.AsEnumerable();
            //var selectedListVar = selectedList.AsEnumerable();

            //var checkedList = from chart in chartDatatable
            //                  from list in selectedList
            //                  where chart.Field<int>("CAT_KEY") == int.Parse(list.ValueText.ToString().Trim())
            //                  select new
            //                  {
            //                      CAT_KEY = (int)chart["CAT_KEY"],
            //                      SHORTNAME = (string)chart["SHORTNAME"],
            //                      NAME = (string)chart["NAME"],
            //                      LT50PERCENTILE = (decimal)chart["LT50Percentile"],
            //                      GT50PERCENTILE = (decimal)chart["GT50Percentile"],
            //                      GT75PERCENTILE = (decimal)chart["GT75Percentile"],
            //                      LT50PERCENTILE_RELATIVE = (decimal)chart["LT50PERCENTILE_RELATIVE"],
            //                      GT50PERCENTILE_RELATIVE = (decimal)chart["GT50PERCENTILE_RELATIVE"],
            //                      GT75PERCENTILE_RELATIVE = (decimal)chart["GT75PERCENTILE_RELATIVE"]
            //                  };

            //foreach (var result in checkedList)
            //{
            //    selectedChartData.Rows.Add(new object[]{result.CAT_KEY,result.SHORTNAME,result.NAME,
            //     result.LT50PERCENTILE,result.GT50PERCENTILE,result.GT75PERCENTILE,
            //     result.LT50PERCENTILE_RELATIVE,result.GT50PERCENTILE_RELATIVE,result.GT75PERCENTILE_RELATIVE
            //     });
            //} 
            #endregion
            

            if (selectedList != null && selectedList.Count != 0)
            {
                foreach (DropDownItem category in selectedList)
                {
                    if ((Session["CategoryChartDataSource"]) != null)
                    {
                        DataRow[] datarow = (Session["CategoryChartDataSource"] as DataTable).
                            Select("CAT_KEY='" +int.Parse(category.ValueText.ToString().Trim()) + "'");                                              

                        selectedChartData.Rows.Add(datarow[0].ItemArray);
                        //Append the selected category id to the string 
                        selectedCategoryId.Append(category.ValueText);
                        selectedCategoryId.Append("-");                       
                    }
                }
            }
            else
            {
                categoryChartDataTable = new DataTable();
            }   
                dropDownItemList = new List<DropDownItem>();             

                //Remove the last - 
                if (selectedCategoryId.Length > 0)
                    selectedCategoryIds = selectedCategoryId.ToString().TrimEnd('-');

                selectedPrintProperty.Append(selectedCategoryIds.Replace('-', ',') + "|");


                //If absolute score is selected remove the relative score details
                if (GroupAnalysisCategoryChartControl_absoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    selectedChartData.Columns.Remove("LT50PERCENTILE_RELATIVE");
                    selectedChartData.Columns.Remove("GT50PERCENTILE_RELATIVE");
                    selectedChartData.Columns.Remove("GT75PERCENTILE_RELATIVE");

                    //Replace the column name as < 50 percentile
                    selectedChartData.Columns["LT50Percentile"].ColumnName = "< 50 Percentile";

                    //Replace the column name as 50-75 percentile
                    selectedChartData.Columns["GT50Percentile"].ColumnName = "50 - 75 Percentile";

                    //Replace the column name as > 75 percentile
                    selectedChartData.Columns["GT75Percentile"].ColumnName = "> 75 Percentile";


                    dropDownItemList.Add(new DropDownItem("Title", "Group Analysis Category Score"));
                    dropDownItemList.Add(new DropDownItem("Categories", Constants.ChartConstants.GROUP_REPORT_CATEGORY_ABSOLUTE + "-" + testkey + "-" + selectedCategoryIds + ".png"));
                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.GROUP_REPORT_CATEGORY_ABSOLUTE + "-" + testkey + "-" + selectedCategoryIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    //else remove the absolute score details
                    selectedChartData.Columns.Remove("LT50PERCENTILE");
                    selectedChartData.Columns.Remove("GT50Percentile");
                    selectedChartData.Columns.Remove("GT75Percentile");


                    //Rename the relative score details
                    //Replace the column name as < 50 percentile
                    selectedChartData.Columns["LT50PERCENTILE_RELATIVE"].ColumnName = "< 50 Percentile";

                    //Replace the column name as 50-75 percentile
                    selectedChartData.Columns["GT50PERCENTILE_RELATIVE"].ColumnName = "50 - 75 Percentile";

                    //Replace the column name as > 75 percentile
                    selectedChartData.Columns["GT75PERCENTILE_RELATIVE"].ColumnName = "> 75 Percentile";


                    dropDownItemList.Add(new DropDownItem("Title", "Group Analysis Relative Category Score"));
                    dropDownItemList.Add(new DropDownItem("Categories", Constants.ChartConstants.GROUP_REPORT_CATEGORY_RELATIVE + "-" + testkey + "-" + selectedCategoryIds + ".png"));
                    categoryChartDataSource.ChartImageName = Constants.ChartConstants.GROUP_REPORT_CATEGORY_RELATIVE + "-" + testkey + "-" + selectedCategoryIds;
                }

                WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
                WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

                categoryChartDataTable = selectedChartData;
           
            //Remove the cate key column from the displaying table
            if (categoryChartDataTable.Columns.Contains("CAT_KEY"))
            {
                categoryChartDataTable.Columns.Remove("CAT_KEY");
            }

            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                categoryChartDataSource.IsShowLabel = true;
            }
            categoryChartDataSource.ChartType = SeriesChartType.Column;

            categoryChartDataSource.ChartLength = 300;

            categoryChartDataSource.ChartWidth = 350;

            categoryChartDataSource.IsDisplayAxisTitle = true;

            categoryChartDataSource.IsDisplayChartTitle = false;

            categoryChartDataSource.XAxisTitle = "Categories";

            categoryChartDataSource.YAxisTitle = "No Of Candidates";

            categoryChartDataSource.ChartTitle = "Candidate's Score Amongst Categories";

            categoryChartDataSource.IsFullNumberYAxis = true;

            categoryChartDataSource.IsDisplayChartTitle = false;

            categoryChartDataSource.ComparisonReportDataTable = categoryChartDataTable;

            categoryChartDataSource.IsComparisonReport = true;

            GroupAnalysisCategoryChartControl_categoryChartControl.MultipleChartDataSource =
              categoryChartDataSource;
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        #endregion Private Methods

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>   
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }
        #endregion
    }
}