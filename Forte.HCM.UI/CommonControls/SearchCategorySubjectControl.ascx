﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchCategorySubjectControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SearchCategorySubjectControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="15%" class="header_text_bold">
                                    <asp:Literal ID="SearchCategorySubjectControl_categoryHeaderLiteral" runat="server"
                                        Text="Category"></asp:Literal>
                                </td>
                                <td width="31%">
                                    <asp:TextBox ID="SearchCategorySubjectControl_categoryTextBox" runat="server"></asp:TextBox>
                                </td>
                                <td width="42%">
                                    <asp:LinkButton ID="SearchCategorySubjectControl_addLinkButton" runat="server" SkinID="sknActionLinkButton"
                                        Text="Add" />
                                </td>
                                <td width="12%">
                                    <asp:LinkButton ID="SearchCategorySubjectControl_removeLinkButton" runat="server"
                                        SkinID="sknActionLinkButton" Text="Remove All" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="grid_body_bg" valign="top">
                        <div style="width: 100%; height: 105px; float: left; overflow: auto">
                            <asp:DataList ID="SearchCategorySubjectControl_categoryDataList" runat="server" RepeatColumns="2"
                                RepeatDirection="Vertical" Width="100%">
                                <ItemTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 20px;" class="td_padding_top_2">
                                                <asp:ImageButton ID="SearchCategorySubjectControl_removeImageButton" runat="server"
                                                    SkinID="sknDeleteImageButton" ToolTip="Remove Category" />
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="SearchCategorySubjectControl_categoryNameLabel" runat="server" SkinID="sknLabelText"
                                                    Text='<%# Bind("Name") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td width="1%">
        </td>
        <td width="49%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    <asp:Literal ID="SearchCategorySubjectControl_subjectHeaderLiteral" runat="server"
                                        Text="Subject"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="grid_body_bg" valign="top">
                        <div style="width: 100%; height: 105px; float: left; overflow: auto">
                            <asp:DataList ID="SearchCategorySubjectControl_subjectDataList" runat="server" RepeatColumns="1"
                                RepeatDirection="Vertical" OnItemDataBound="SearchCategorySubjectControl_subjectDataList_ItemDataBound"
                                Width="100%">
                                <ItemTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="2" border="0">
                                        <tr>
                                            <td class="label_field_header_text">
                                                <asp:Literal ID="SearchCategorySubjectControl_subjectNameLabel" runat="server" Text='<%# Bind("Name") %>'></asp:Literal>
                                            </td>
                                            <td align="right">
                                                <asp:LinkButton ID="SearchCategorySubjectControl_selectAllLinkButton" runat="server"
                                                    Text="Select All" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                <span class="link_button">&nbsp;|&nbsp;</span>
                                                <asp:LinkButton ID="SearchCategorySubjectControl_clearAllLinkButton" runat="server"
                                                    Text="Clear All" SkinID="sknActionLinkButton"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_2" colspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" colspan="2">
                                                <asp:DropDownList ID="SearchQuestion_subjectDropDownList" runat="server" SkinID="sknSubjectDropDown">
                                                    <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="All" Value="Simple"></asp:ListItem>
                                                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                                    <asp:ListItem Text="Inactive" Value="Inactive"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
