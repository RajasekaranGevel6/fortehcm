﻿#region Header                                                                 
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MultiSeriesReportChartControl.cs
// File that represents the user interface for the multiple series report chart
//
#endregion

#region Directives                                                             

using System;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class MultiSeriesReportChartControl : UserControl
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that is called when the page is loaded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion Event Handlers

        #region Properties                                                     
        /// <summary>
        /// Property used to assign the 
        /// datasource of the Multiple series chart control
        /// </summary>
        public MultipleSeriesChartData MultipleChartDataSource
        {
            set
            {
                if (value == null)
                    return;

                //Set the height of the control 
                MultiSeriesReportChartControl_chart.Height = Unit.Pixel(value.ChartLength);

                //Set the width of the control
                MultiSeriesReportChartControl_chart.Width = Unit.Pixel(value.ChartWidth);

                //Set the y axis interval to the whole number 
                if (value.IsFullNumberYAxis)
                    MultiSeriesReportChartControl_chart.ChartAreas
                        ["MultiSeriesReportChartControl_chartArea"].AxisY.Interval = 1;

                //Defines whether the chart title has to display or not
                MultiSeriesReportChartControl_chart.Titles[0].Visible =
                    value.IsDisplayChartTitle;

                //Assign the chart title to the chart
                if (value.IsDisplayChartTitle)
                {
                    MultiSeriesReportChartControl_chart.
                        Titles["MultiSeriesReportChartControl_title"].Text = value.ChartTitle;
                }

                //Defines whether the axis title has to display or not
                if (value.IsDisplayAxisTitle)
                {
                    MultiSeriesReportChartControl_chart.ChartAreas
                        ["MultiSeriesReportChartControl_chartArea"].AxisX.Title = value.XAxisTitle;

                    MultiSeriesReportChartControl_chart.ChartAreas
                        ["MultiSeriesReportChartControl_chartArea"].AxisY.Title = value.YAxisTitle;
                }

                MultiSeriesReportChartControl_chart.Series.Clear();

                if (!value.IsComparisonReport)
                {
                    if (value.MultipleSeriesChartDataSource != null && value.MultipleSeriesChartDataSource.Count > 0)
                    {
                        //Databind the values to the chart
                        MultiSeriesReportChartControl_chart.DataBindTable(value.MultipleSeriesChartDataSource,
                            "ShortName");
                    }
                }
                else
                {
                    if (value.ComparisonReportDataTable != null && value.ComparisonReportDataTable.Rows.Count > 0)
                    {
                        DataView dataView = value.ComparisonReportDataTable.DefaultView;

                        //Databind the values to the chart
                        MultiSeriesReportChartControl_chart.DataBindTable(dataView,
                            "ShortName");
                    }
                }

                //Assign the tooltip for each chart series 
                foreach (Series item in MultiSeriesReportChartControl_chart.Series)
                {
                    item.ToolTip = "#VAL";
                    item.ChartType = value.ChartType;
                    item.IsValueShownAsLabel = false;
                    item.LabelFormat = "#.##";
                }

                if (value.IsShowLabel)
                {
                    foreach (Series item in MultiSeriesReportChartControl_chart.Series)
                    {
                        item.SmartLabelStyle.Enabled = false;
                        item.IsValueShownAsLabel = true;
                        item.LabelAngle = -90;
                        //item.CustomProperties = "LabelStyle = Center";
                    }
                }

                if (!value.IsComparisonReport)
                {
                    if (MultiSeriesReportChartControl_chart.Series.Count > 0)
                    {
                        MultiSeriesReportChartControl_chart.Series["MyScore"].LabelFormat = "#.##";
                        MultiSeriesReportChartControl_chart.Series["MinScore"].LabelFormat = "#.##";
                        MultiSeriesReportChartControl_chart.Series["MaxScore"].LabelFormat = "#.##";
                        MultiSeriesReportChartControl_chart.Series["AvgScore"].LabelFormat = "#.##";
                    }
                }

                if (value.IsHideComparativeScores)
                {
                    if (MultiSeriesReportChartControl_chart.Series.Count > 0)
                    {
                        if (!value.IsComparisonReport)
                        {
                            MultiSeriesReportChartControl_chart.Series["MinScore"].Enabled = false;
                            MultiSeriesReportChartControl_chart.Series["MaxScore"].Enabled = false;
                            MultiSeriesReportChartControl_chart.Series["AvgScore"].Enabled = false;
                        }
                        else
                        {

                            MultiSeriesReportChartControl_chart.Series["Min Score"].Enabled = false;
                            MultiSeriesReportChartControl_chart.Series["Max Score"].Enabled = false;
                            MultiSeriesReportChartControl_chart.Series["Avg Score"].Enabled = false;

                        }
                    }
                }
                if (!Utility.IsNullOrEmpty(value.ChartImageName))
                {
                    //if (!new FileInfo(Server.MapPath("../chart/") + value.ChartImageName + ".png").Exists)
                        MultiSeriesReportChartControl_chart.SaveImage(Server.MapPath("../chart/") + value.ChartImageName + ".png", ChartImageFormat.Png);
                }

                //Get the new session ID
                string sessionId = System.Guid.NewGuid().ToString();

                //Assign the session ID to the session 
                Session[sessionId] = value;

                if ((value.MultipleSeriesChartDataSource != null && value.MultipleSeriesChartDataSource.Count > 0) ||
                    (value.ComparisonReportDataTable != null && value.ComparisonReportDataTable.Rows.Count > 0))
                {
                    //Assign the java script to show the zoomed chart for the td 
                    MultiSeriesReportChartControl_td.Attributes.Add("onclick",
                        "javascript:return ShowZoomedChart('" + sessionId + "','MultiSeriesReport');");
                }
                else
                {
                    //Assign return false for the empty chart
                    MultiSeriesReportChartControl_td.Attributes.Add("onclick",
                        "javascript:return false;");

                }
            }
        }
        #endregion Properties
    }
}