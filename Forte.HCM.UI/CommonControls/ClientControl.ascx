﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ClientControl" %>
 <asp:Panel ID="ClientControl_mainPanel" runat="server" DefaultButton="ClientControl_createButton">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="ClientControl_messagetitleLiteral" runat="server">Client</asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="ClientControl_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" valign="top">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="client_details_modal_popup_bg">
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="ClientControl_messageTd" runat="server" class="td_height_20">
                                <td>
                                    <asp:Label ID="ClientControl_topErrorMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                    <asp:Label ID="ClientControl_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                        <asp:HiddenField ID="ClientControl_tenantIDHiddenField" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Panel ID="ClientControl_panel" runat="server">
                                        <table border="0" cellspacing="3" cellpadding="3">
                                            <tr>
                                                <td id="ClientControl_td1" runat="server">
                                                </td>
                                                <td id="ClientControl_td2" runat="server">
                                                </td>
                                                <td id="ClientControl_td3" runat="server">
                                                </td>
                                                <td id="ClientControl_td4" runat="server">
                                                </td>
                                            </tr>
                                           <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td id="ClientControl_td5" runat="server">
                                                </td>
                                                <td id="ClientControl_td6" runat="server">
                                                </td>
                                                <td id="ClientControl_td7" runat="server">
                                                </td>
                                                <td id="ClientControl_td8" runat="server">
                                                </td>
                                            </tr>
                                            <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td id="ClientControl_td9" runat="server">
                                                </td>
                                                <td id="ClientControl_td10" runat="server">
                                                </td>
                                                <td id="ClientControl_td11" runat="server">
                                                </td>
                                                <td id="ClientControl_td12" runat="server">
                                                </td>
                                            </tr>
                                          <%--  <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td id="ClientControl_td13" runat="server">
                                                </td>
                                                <td id="ClientControl_td14" runat="server">
                                                </td>
                                                <td id="ClientControl_td15" runat="server">
                                                </td>
                                                <td id="ClientControl_td16" runat="server">
                                                </td>
                                            </tr>
                                            <%--<tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td id="ClientControl_td17" runat="server">
                                                </td>
                                                <td id="ClientControl_td18" runat="server">
                                                </td>
                                                <td id="ClientControl_td19" runat="server">
                                                </td>
                                                <td id="ClientControl_td20" runat="server">
                                                </td>
                                            </tr>
                                           <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td id="ClientControl_td21" runat="server">
                                                </td>
                                                <td id="ClientControl_td22" runat="server">
                                                </td>
                                                <td id="ClientControl_td23" runat="server">
                                                </td>
                                                <td id="ClientControl_td24" runat="server">
                                                </td>
                                            </tr>
                                           <%-- <tr class="td_height_5">
                                                <td colspan="4">
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td id="ClientControl_td25" runat="server">
                                                </td>
                                                <td colspan="3" id="ClientControl_td26" runat="server">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" align="left">
            <asp:Button ID="ClientControl_createButton" runat="server" Text="Save" SkinID="sknButtonId"
                ValidationGroup="a" OnCommand="ClientControl_createButton_Command" />
            &nbsp; &nbsp;
            <asp:LinkButton ID="ClientControl_featureCancelLinkButton" runat="server" Text="Cancel"
                SkinID="sknPopupLinkButton">
            </asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
    </tr>
</table>
<asp:Label ID="ClientControl_clientNameLabel" Text="Client Name" runat="server" SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>
<span class="mandatory" id="ClientControl_clientNameSpan" runat="server" visible="false">
    *</span>
<asp:TextBox ID="ClientControl_clientNameTextBox" runat="server" MaxLength="100" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_streetAddressLabel" Text="Street Address" runat="server"
    SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_streetAddressTextBox" runat="server" MaxLength="100" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_cityLabel" Text="City" runat="server" SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_cityTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_stateLabel" Text="State" runat="server" SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_stateTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_zipCodeLabel" Text="Zipcode" runat="server" SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_zipCodeTextBox" runat="server" MaxLength="6" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_countryLabel" Text="Country" runat="server" SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_countryTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_FEIN_NOLabel" runat="server" SkinID="sknLabelFieldHeaderText"
    Text="FEIN NO" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_FEIN_NO_TextBox" runat="server" MaxLength="9" Visible="false"></asp:TextBox>
<ajaxToolKit:FilteredTextBoxExtender ID="ClientControl_FEIN_NO_TextBoxTextBoxExtender"
    runat="server" FilterType="Numbers" TargetControlID="ClientControl_FEIN_NO_TextBox">
</ajaxToolKit:FilteredTextBoxExtender>
<asp:Label ID="ClientControl_phoneNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
    Text="Phone No" Visible="false"></asp:Label>

<span class="mandatory" id="ClientControl_phoneNoSpan" runat="server" visible="false">
    *</span>
<asp:TextBox ID="ClientControl_phoneNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_faxNoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
    Text="Fax No" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_faxNoTextBox" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_eMailIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
    Text="Email ID" Visible="false"></asp:Label>
<span class="mandatory" id="ClientControl_eMailIDSpan" runat="server" visible="false">
    *</span>
<asp:TextBox ID="ClientControl_eMailIDTextBox" runat="server" MaxLength="200" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_websiteURLLabel" runat="server" SkinID="sknLabelFieldHeaderText"
    Text="Website URL" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_websiteURL_TextBox" runat="server" MaxLength="200" Visible="false"></asp:TextBox>
<asp:Label ID="ClientControl_additionalInfoLabel" runat="server" SkinID="sknLabelFieldHeaderText"
    Text="Additional Info" Visible="false"></asp:Label>

<asp:TextBox ID="ClientControl_additionalInfoTextBox" Columns="80" runat="server"
    MaxLength="500" Height="60" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
    onchange="CommentsCount(500,this)" Visible="false"></asp:TextBox>
</asp:Panel>