﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateDetailControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CandidateDetailControl" %>

<script type="text/javascript" language="javascript">
    // This function helps to close the window
    function CloseMe() {
        self.close();
    }
</script>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="CandidateDetailControl_headerLiteral" runat="server" Text="Candidate Details"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top" align="right">
                        <asp:ImageButton ID="CandidateDetailControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" />
                    </td>
                </tr>
                <tr>
                    <td class="msg_align" colspan="2">
                        <asp:Label ID="CandidateDetailControl_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table border="0" cellpadding="0" cellspacing="4" class="tab_body_bg">
                            <tr>
                                <td style="width: 15%">
                                    <asp:Label ID="CandidateDetailControl_candidateNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Name"></asp:Label>
                                </td>
                                <td style="width: 35%">
                                    <asp:Label ID="CandidateDetailControl_candidateNameTextBox" runat="server" SkinID="sknLabelFieldText"
                                        Text="James Smith"></asp:Label>
                                </td>
                                <td style="width: 15%">
                                    <asp:Label ID="CandidateDetailControl_candidateLocationLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Location"></asp:Label>
                                </td>
                                <td style="width: 35%">
                                    <asp:Label ID="CandidateDetailControl_candidateLocationTextBox" runat="server" SkinID="sknLabelFieldText"
                                        Text="Peoria"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="CandidateDetailControl_candidateEmailLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Email"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="CandidateDetailControl_candidateEmailTextBox" runat="server" SkinID="sknLabelFieldText"
                                        Text="james_tl@aol.us"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="CandidateDetailControl_candidatePhoneLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Phone"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="CandidateDetailControl_candidatePhoneTextBox" runat="server" SkinID="sknLabelFieldText"
                                        Text="234-762-4144"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                    <asp:Label ID="CandidateDetailControl_candidateSynopsisLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Synopsis"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="label_multi_field_text" style="width: 100%; height: 150px; overflow: auto;">
                                        <asp:Literal ID="CandidateDetailControl_candidateSynopsisLiteral" runat="server"
                                            Text=""></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <asp:Label ID="CandidateDetailControl_candidateNotesLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                        Text="Notes"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="label_multi_field_text">
                                        <asp:Literal ID="CandidateDetailControl_candidateNotesLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="CandidateDetailControl_candidateSkillMatrixLinkButton" runat="server"
                                        SkinID="sknActionLinkButton" Text="Skills Matrix"></asp:LinkButton>
                                </td>
                                <td colspan="3">
                                    <asp:HiddenField ID="CandidateDetailControl_candidateSessionIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="CandidateDetailControl_candidateattemptIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="CandidateDetailControl_candidateTestKeyHiddenField" runat="server" />
                                    <asp:HiddenField ID="CandidateDetailControl_candidateNameHiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_5">
            <table cellpadding="0" cellspacing="3" width="100%">
                <tr>
                    <td style="width: 19.5%">
                        <asp:Button ID="CandidateDetailControl_downloadResumeButton" runat="server" SkinID="sknButtonId"
                            Text="Download Resume" OnClick="CandidateDetailControl_downloadResumeButton_Click" />
                        <asp:Button ID="CandidateDetailControl_showTestScoreButton" runat="server" SkinID="sknButtonId"
                            Text="Show Test Score" OnClick="CandidateDetailControl_showTestScoreButton_Click" />
                        <asp:LinkButton ID="CandidateDetailControl_cancelButton" runat="server" SkinID="sknPopupLinkButton"
                            Text="Cancel"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
