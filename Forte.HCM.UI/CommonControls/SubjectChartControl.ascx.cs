﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SubjectChartControl.cs
// File that represents the user interface for the Subject Chart Control details

#endregion Header

#region Directives                                                             
using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class SubjectChartControl : UserControl, IWidgetControl
    {
        string testkey = string.Empty;
        List<DropDownItem> dropDownItemList = null;
        #region Event Handlers     
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                SubjectChartControl_selectOptionsAbsoluteScore.Checked = selectedProperty[2] == "0" ? false : true;
                SubjectChartControl_selectOptionsRelativeScore.Checked = selectedProperty[2] == "1" ? false : true;
                SubjectChartControl_scoreHiddenField.Value = selectedProperty[1];
                SubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(selectedProperty[0]);
                SubjectChartControl_widgetMultiSelectControl.Visible = false;
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
        }                            

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        /// <summary>
        /// Hanlder method that will be called on the show 
        /// comparative score link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SubjectChartControl_showComparativeScoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on radio button click
        /// link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SubjectChartControl_selectOptionsAbsoluteScore_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        /// <summary>
        /// Hanlder method that will be called when click the check box.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void SubjectChartControl_selectProperty(object sender, EventArgs e)
        {
            try
            {
                SubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(false);

                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void SubjectChartControl_widgetMultiSelectControl_Click(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Hanlder method that will be called when click the link button
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        protected void SubjectChartControl_widgetMultiSelectControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadChart(ViewState["instance"] as WidgetInstance,"1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Event Handlers

        #region Private Method                                                 

        private void LoadChart(WidgetInstance widgetInstance,string flag)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            StringBuilder selectedSubjectId = new StringBuilder();
            string selectedSubjectIds = "";
            MultipleSeriesChartData subjectChartDataSource = null;
            if (Session["SubjectChartDataSource"] != null)
                subjectChartDataSource = Session["SubjectChartDataSource"] as MultipleSeriesChartData;
            subjectChartDataSource = new MultipleSeriesChartData();
            MultipleSeriesChartData selectedChartData = new MultipleSeriesChartData();
            selectedChartData.MultipleSeriesChartDataSource = new List<MultipleChartData>();
            CheckBoxList cbxList =
                (CheckBoxList)SubjectChartControl_widgetMultiSelectControl.FindControl
                ("WidgetMultiSelectControl_propertyCheckBoxList");
            List<Subject> subjectList = new List<Subject>();
            //categoryList = WidgetMultiSelectControl.SelectedProperties;
            for (int i = 0; i < cbxList.Items.Count; i++)
            {
                if (cbxList.Items[i].Selected)
                {
                    Subject subject = new Subject();
                    subject.SubjectName = cbxList.Items[i].Text;
                    subject.SubjectID = int.Parse(cbxList.Items[i].Value);
                    subjectList.Add(subject);
                    selectedSubjectId.Append(cbxList.Items[i].Value);
                    selectedSubjectId.Append("-");
                }
            }
            if (subjectList != null && subjectList.Count != 0)
            {
                foreach (Subject subject in subjectList)
                {
                    if ((Session["SubjectChartDataSource"]) != null)
                    {
                        if (SubjectChartControl_selectOptionsAbsoluteScore.Checked)
                        {
                            selectedChartData.MultipleSeriesChartDataSource.Add
                                ((Session["SubjectChartDataSource"] as MultipleSeriesChartData).
                                MultipleSeriesChartDataSource.Find
                                (delegate(MultipleChartData multiChartData)
                                { return multiChartData.ID == subject.SubjectID.ToString().Trim(); }));
                        }
                        else
                        {
                            selectedChartData.MultipleSeriesChartDataSource.Add
                               ((Session["SubjectChartDataSource"] as MultipleSeriesChartData).
                               MultipleSeriesRelativeChartDataSource.Find
                               (delegate(MultipleChartData multiChartData)
                               { return multiChartData.ID == subject.SubjectID.ToString().Trim(); }));
                        }
                    }
                }
                subjectChartDataSource.MultipleSeriesChartDataSource =
                 selectedChartData.MultipleSeriesChartDataSource;
            }
            else
            {
                subjectChartDataSource.MultipleSeriesChartDataSource = new List<MultipleChartData>();
            }
            //flag denotes whether the chart is displayed for printer version or not.
            //for printer version the values are displayed in the chart
            if (flag == "1")
            {
                subjectChartDataSource.IsShowLabel = true;
            }
            subjectChartDataSource.ChartType = SeriesChartType.Column;
            subjectChartDataSource.ChartLength = 300;
            subjectChartDataSource.ChartWidth = 350;
            subjectChartDataSource.IsDisplayAxisTitle = true;
            subjectChartDataSource.IsDisplayChartTitle = false;
            subjectChartDataSource.XAxisTitle = "Subjects";
            subjectChartDataSource.YAxisTitle = "Scores";
            subjectChartDataSource.IsDisplayChartTitle = false;
            subjectChartDataSource.ChartTitle = "Candidate's Score Amongst Subjects";
            dropDownItemList = new List<DropDownItem>();
            selectedSubjectIds = selectedSubjectId.ToString().TrimEnd('-');
            selectedPrintProperty.Append(selectedSubjectIds.Replace('-', ',') + "|");
            testkey = SubjectChartControl_testKeyhiddenField.Value;
            if (SubjectChartControl_scoreHiddenField.Value == "1")
            {
                selectedPrintProperty.Append("1|");
                SubjectChartControl_showComparativeScoreLinkButton.Text = "Hide Comparative Score";
                subjectChartDataSource.IsHideComparativeScores = false;
                if (SubjectChartControl_selectOptionsAbsoluteScore.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    dropDownItemList.Add(new DropDownItem("Title", "Subject Comparative Score"));
                    dropDownItemList.Add(new DropDownItem("Subjects", Constants.ChartConstants.DESIGN_REPORT_SUBJECT_COMPARATIVE_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_SUBJECT_COMPARATIVE_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    dropDownItemList.Add(new DropDownItem("Title", "Subject Comparative Relative Score"));
                    dropDownItemList.Add(new DropDownItem("Subjects", Constants.ChartConstants.DESIGN_REPORT_SUBJECT_COMPARATIVE_RELATIVE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_SUBJECT_COMPARATIVE_RELATIVE + "-" + testkey + "-" + selectedSubjectIds;
                }
            }
            else
            {
                selectedPrintProperty.Append("0|");
                subjectChartDataSource.IsHideComparativeScores = true;
                SubjectChartControl_showComparativeScoreLinkButton.Text = "Show Comparative Score";
                if (SubjectChartControl_selectOptionsAbsoluteScore.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    dropDownItemList.Add(new DropDownItem("Title", "Subject Score"));
                    dropDownItemList.Add(new DropDownItem("Subjects", Constants.ChartConstants.DESIGN_REPORT_SUBJECT_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_SUBJECT_ABSOLUTE + "-" + testkey + "-" + selectedSubjectIds;
                }
                else
                {
                    selectedPrintProperty.Append("0|");
                    dropDownItemList.Add(new DropDownItem("Title", "Subject Relative Score"));
                    dropDownItemList.Add(new DropDownItem("Subjects", Constants.ChartConstants.DESIGN_REPORT_SUBJECT_RELATIVE + "-" + testkey + "-" + selectedSubjectIds + ".png"));
                    subjectChartDataSource.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_SUBJECT_RELATIVE + "-" + testkey + "-" + selectedSubjectIds; 
                }
            }
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            SubjectChartControl_multipleSeriesChartControl.MultipleChartDataSource =
              subjectChartDataSource;
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        /// <summary>
        /// Method that is used to load the chart details
        /// </summary>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            SubjectChartControl_testKeyhiddenField.Value = candidateReportDetail.TestKey;

            //Get the categories details for the test 
            MultipleSeriesChartData subjectChartDataSource = new ReportBLManager().
                GetCandidateStatisticsSubjectsChartDetails(candidateReportDetail.CandidateSessionkey,
                candidateReportDetail.TestKey, candidateReportDetail.AttemptID);

            if (subjectChartDataSource != null && subjectChartDataSource.
                MultipleSeriesChartDataSource.Count > 0)
            {
                Session["SubjectChartDataSource"] = subjectChartDataSource;
            }

            //Load the drop down 
            var subjectsList = (from p in subjectChartDataSource.MultipleSeriesChartDataSource
                                select new DropDownItem(p.Name, p.ID)).Distinct();

            SubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource =
                subjectsList.ToList();

            SubjectChartControl_widgetMultiSelectControl.WidgetTypePropertyAllSelected(true);

            LoadChart(ViewState["instance"] as WidgetInstance,"1");

        }
        #endregion Private Method

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>
        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

            //Assign the attributes for the link Button 
            SubjectChartControl_showComparativeScoreLinkButton.Attributes.
                Add("onclick", "javascript:return ShowOrHideComparativeScore('"
                + SubjectChartControl_showComparativeScoreLinkButton.ClientID +
                "','" + SubjectChartControl_scoreHiddenField.ClientID + "');");
        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            // return new UpdatePanel[] { SubjectChartControl_updatePanel };
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {

        }

        #endregion
    }
}