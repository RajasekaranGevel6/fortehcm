﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineInterviewSummaryControl.cs
// File that represents the user interface to add client detail.

#endregion Header

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Represents the class for the online interview summary control
    /// This page helps to display the interview summary details
    /// </summary>
    public partial class OnlineInterviewSummaryControl : UserControl
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string chartType = "Pie";
            string chartName = "TestAreas";
            string title = "Interview Area Statistics";

            OnlineInterviewSummaryControl_interviewAreaChart.Attributes.Add("onclick", "javascript:return ShowZoomedChart('" + chartType + "','" + chartName + "','" + title + "');");

            chartType = "Pie";
            chartName = "Complexities";
            title = "Complexity Statistics";

            OnlineInterviewSummaryControl_complexityChart.Attributes.Add("onclick", "javascript:return ShowZoomedChart('" + chartType + "','" + chartName + "','" + title + "');");

        }

        #endregion Event Handlers

        #region Public Methods                                                 
        
        /// <summary>
        /// Method that sets the online inteview statistics
        /// </summary>
        public TestStatistics DataSource
        {
            set
            {
                if (value == null)
                    return;
                
                OnlineInterviewSummaryControl_noOfQuestionLabel.Text = value.NoOfQuestions.ToString();
                OnlineInterviewSummaryControl_interviewAreaChart.DataSource = value.TestAreaStatisticsChartData;
                OnlineInterviewSummaryControl_complexityChart.DataSource = value.ComplexityStatisticsChartData;
            }
        }

        /// <summary>
        /// Method that sets the skill set summary
        /// </summary>
        public List<QuestionDetail> skillSet
        {
            set
            {
                if (value == null)
                    return;

                OnlineInterviewSummaryControl_skillDataList.DataSource = value;
                OnlineInterviewSummaryControl_skillDataList.DataBind();
            }
        }

        #endregion Public Methods
    }
}

