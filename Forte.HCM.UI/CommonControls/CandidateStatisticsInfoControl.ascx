﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateStatisticsInfoControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CandidateStatisticsInfoControl" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc1" %>
<div>
    <asp:UpdatePanel ID="CandidateStatisticsInfoControl_updatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <div style="width: 100%; overflow: auto">
                            <uc1:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" OnselectedIndexChanged="CandidateStatisticsInfo_selectProperty" 
                            OncancelClick="WidgetMultiSelectControl_cancelClick" OnClick="WidgetMultiSelectControl_Click" />
                            <uc1:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="3%" cellspacing="3%" border="0">
                                        <tr>
                                            <td valign="middle" style="width: 100%;">
                                                <div style="float: left">
                                                    <asp:Label ID="CandidateStatisticsInfo_nameLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldWidgetHeaderTexts"></asp:Label>
                                                </div>
                                                <div style="float: right">
                                                    <asp:Label ID="CandidateStatisticsInfo_locationLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldWidgetHeaderText"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="CandidateStatisticsInfo_contactLabel" runat="server" Text=""
                                                    SkinID="sknLabelFieldWidgetHeaderText"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="label_multi_field_text_widget">
                                                    <asp:Literal ID="CandidateStatisticsInfo_synopsisLiteral" runat="server" Text=""></asp:Literal>
                                                </div>
                                            </td>
                                        </tr>
                                        <asp:DataList ID="CandidateStatisticsInfo_candidateDetailsDataList" runat="server"
                                            RepeatLayout="Table" RepeatColumns="2" RepeatDirection="Horizontal">
                                            <HeaderTemplate>
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                            <tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <td>
                                                    <asp:Label ID="CandidateStatisticsInfo_scoreSDHeadLabel" runat="server" Text='<%# Eval("DisplayText") %>'
                                                        SkinID="sknLabelFieldWidgetLabelText"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="CandidateStatisticsInfo_scoreSDValueLabel" runat="server" SkinID="sknLabelFieldWidgetText"
                                                        Text='<%# Eval("ValueText") %>'></asp:Label>
                                                </td>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tr> </table> </td> </tr>
                                            </FooterTemplate>
                                        </asp:DataList>
                                        <tr>
                                            <td class="td_h_line">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 5px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
