﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CorporateUserSignUp.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CorporateUserSignUp" %>
<style type="text/css">
    .style1
    {
        width: 25%;
    }
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="Forte_CorporateUserSignUp_messagetitleLiteral" runat="server">Add New User</asp:Literal>
                    </td>
                    <td style="width: 25%" valign="top" align="right">
                        <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" OnClick="ClosePopUpClick" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_SignUp_type_bg">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="Forte_CorporateUserSignUp_messageTd" runat="server">
                                <td>
                                    <asp:Label ID="Forte_CorporateUserSignUp_topErrorMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                    <asp:Label ID="Forte_CorporateUserSignUp_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                        SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 50px;">
                                            </td>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                    <tr>
                                                        <td class="style1">
                                                            <asp:Label ID="Forte_CorporateUserSignUp_userEmailHeadLable" runat="server" Text="User ID"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory" id="Forte_CorporateUserSignUp_userEmailMandatorySpan" runat="server">
                                                                *</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <%--<asp:UpdatePanel ID="Update1" runat="server">
                                                    <ContentTemplate>--%>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width: 40%;" align="left">
                                                                        <asp:TextBox ID="Forte_CorporateUserSignUp_userIdTextBox" runat="server" Width="200px"
                                                                            MaxLength="50" AutoCompleteType="None"></asp:TextBox>
                                                                        <asp:Label ID="Forte_CorporateUserSignUp_userIdLabel" runat="server" SkinID="sknLabelFieldText"
                                                                            Visible="false"></asp:Label>
                                                                        <div id="Forte_CorporateUserSignUp_userEmailAvailableStatusDiv" runat="server" style="display: none;">
                                                                            <asp:Label ID="Forte_CorporateUserSignUp_inValidEmailAvailableStatus" runat="server"
                                                                                Width="100%" EnableViewState="false" SkinID="sknErrorMessage"></asp:Label>
                                                                            <asp:Label ID="Forte_CorporateUserSignUp_validEmailAvailableStatus" runat="server"
                                                                                Width="100%" EnableViewState="false" SkinID="sknSuccessMessage"></asp:Label>
                                                                        </div>
                                                                        <input type="hidden" id="Forte_CorporateSignUp_editUserTenantIdHidden" runat="server" />
                                                                        <input type="hidden" id="Forte_CorporateSignUp_userTenantIdHidden" runat="server" />
                                                                        <input type="hidden" id="Forte_CorporateSignUp_userCompanyHidden" runat="server" />
                                                                        <input type="hidden" id="Forte_CorporateSignUp_userPhoneHidden" runat="server" />
                                                                        <input type="hidden" id="Forte_CorporateSignUp_userTitleHidden" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                        <asp:LinkButton ID="Forte_CorporateUserSignUp_checkButton" runat="server" Text="Check"
                                                                            OnClick="Forte_CorporateUserSignUp_checkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <%--  </ContentTemplate>
                                                </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>
                                                    <tr id="Forte_CorporateUserSignUp_passwordTr" runat="server">
                                                        <td class="style1">
                                                            <asp:Label ID="Forte_CorporateUserSignUp_passwordHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Password"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Forte_CorporateUserSignUp_passwordTextBox" runat="server" TextMode="Password"
                                                                MaxLength="20"></asp:TextBox>
                                                            <ajaxToolKit:PasswordStrength ID="Forte_CorporateUserSignUp_passwordTextPasswordStrength"
                                                                runat="server" DisplayPosition="RightSide" HelpHandlePosition="BelowRight" RequiresUpperAndLowerCaseCharacters="true"
                                                                PreferredPasswordLength="6" HelpStatusLabelID="Forte_CorporateUserSignUp_passwordHelpLabel"
                                                                StrengthIndicatorType="Text" TargetControlID="Forte_CorporateUserSignUp_passwordTextBox"
                                                                PrefixText="Excellent" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"
                                                                StrengthStyles="barIndicator_poor; barIndicator_weak; barIndicator_good; barIndicator_strong; barIndicator_excellent">
                                                            </ajaxToolKit:PasswordStrength>
                                                            <br />
                                                            <asp:Label ID="Forte_CorporateUserSignUp_passwordHelpLabel" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="Forte_CorporateUserSignUp_reTypePasswordTr" runat="server">
                                                        <td class="style1">
                                                            <asp:Label ID="Forte_CorporateUserSignUp_reEnterPasswordHeaderLabel" runat="server"
                                                                SkinID="sknLabelFieldHeaderText" Text="Re-type Password"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Forte_CorporateUserSignUp_confirmPasswordTextBox" runat="server"
                                                                TextMode="Password" MaxLength="20"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1">
                                                            <asp:Label ID="Forte_CorporateUserSignUp_firstNameHeadLabel" runat="server" Text="First Name"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Forte_CorporateUserSignUp_firstNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1">
                                                            <asp:Label ID="Forte_CorporateUserSignUp_lastNameHeadLabel" runat="server" Text="Last Name"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Forte_CorporateUserSignUp_lastNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1">
                                                            <asp:Label ID="Forte_CorporateUserSignUp_customerAdministratorLabel" runat="server"
                                                                Text="Tenant Admin" SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:CheckBox ID="Forte_CorporateUserSignUp_customerAdminCheckBox" runat="server"
                                                                    Visible="false" />
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="Forte_CorporateUserSignUp_customeAdminImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" Visible="false"
                                                                    ToolTip="Selecting this check box will assign the user as a Tenant administrator" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1" valign="middle">
                                                            <asp:Label ID="Forte_CorporateUserSignUp_rolesLabel" runat="server" Text="Roles"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width:100%">
                                                                        <%-- <asp:UpdatePanel ID="Forte_CorporateUserSignUp_updatePanel" runat="server" UpdateMode="Always">
                                                                            <ContentTemplate>--%>
                                                                        <asp:CheckBoxList ID="Forte_CorporateUserSignUp_rolesCheckBoxList" RepeatColumns="2"
                                                                            runat="server" CellPadding="0" AutoPostBack="false" OnSelectedIndexChanged="Forte_CorporateUserSignUp_rolesCheckBoxList_SelectedIndexChanged">
                                                                        </asp:CheckBoxList>
                                                                        <asp:TextBox ID="Forte_CorporateUserSignUp_rolesComboTextBox" ReadOnly="true" runat="server"
                                                                            Width="50%" Font-Size="X-Small" Visible="false"></asp:TextBox>
                                                                        <asp:Panel ID="Forte_CorporateUserSignUp_rolesPanel" runat="server" CssClass="popupControl_corporateSignupRoles">
                                                                        </asp:Panel>
                                                                        <%-- <ajaxToolKit:PopupControlExtender ID="Forte_CorporateUserSignUp_rolesListPopupControlExtender"
                                                                                    runat="server" TargetControlID="Forte_CorporateUserSignUp_rolesComboTextBox"
                                                                                    PopupControlID="Forte_CorporateUserSignUp_rolesPanel" OffsetX="2" OffsetY="2"
                                                                                    Position="Bottom">
                                                                                </ajaxToolKit:PopupControlExtender>--%>
                                                                        <%-- </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="Forte_CorporateUserSignUp_rolesCheckBoxList" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <%--<tr id="Forte_CorporateUsersSignUp_formNameTR" runat="server">
                                                        <td>
                                                            <asp:Label ID="Forte_CorporateUsersSignUp_formNameHeadLabel" runat="server" Text="Default Form Layout"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="Forte_CorporateUserSignUp_formNameDropDownList" runat="server"
                                                                Width="200px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>--%>
                                                    <tr id="Forte_CorporateUsersSignUp_activeStatusTr" runat="server">
                                                        <td class="style1">
                                                            <asp:HiddenField ID="Forte_CorporateUserSignUp_activeStateHiddenField" runat="server" />
                                                            <%--<asp:Label ID="Forte_CorporateUsersSignUp_activeStateLabel" runat="server" Text="Active"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>--%>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                           <%-- <asp:CheckBox ID="Forte_CorporateUsersSignUp_activeCheckBox" runat="server" Text="" />--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10" align="left">
            <asp:Button ID="Forte_CorporateUserSignUp_featureSaveButton" runat="server" Text="Save"
                SkinID="sknButtonId" OnClick="Forte_CorporateUserSignUp_featureSaveButton_Click" />
            &nbsp; &nbsp;
            <asp:LinkButton ID="Forte_CorporateUserSignUp_featureCancelLinkButton" runat="server"
                Text="Cancel" SkinID="sknPopupLinkButton" OnClick="ClosePopUpClick">
            </asp:LinkButton>
            <%--<asp:Button ID="Forte_CorporateUserSignUp_featureCancelButton" runat="server" Text="Cancel"
                SkinID="sknButtonId" />--%>
                <asp:HiddenField ID="Forte_CorporateUserSignUp_NoOfUsersHiddenField" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
        </td>
        <td>
            <asp:HiddenField ID="Forte_CorporateUserSignUp_rolesHiddenField" runat="server" />
        </td>
    </tr>
</table>
