﻿#region Header                                                                 
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MultipleSeriesChartControl.cs
// File that represents the user interface for the nultiple series chart
//
#endregion

#region Directives                                                             
                                                      
using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using System.Drawing;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// MultipleSeriesChartControl. 
    /// </summary>
    public partial class MultipleSeriesChartControl : UserControl
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that is called when the page is loaded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion Event Handlers

        #region Properties                                                     
        /// <summary>
        /// Property used to assign the 
        /// datasource of the Multiple series chart control
        /// </summary>
        public MultipleSeriesChartData MultipleChartDataSource
        {
            set
            {
                //Set the height of the control 
                MultipleSeriesChartControl_chart.Height = Unit.Pixel(value.ChartLength);

                //Set the width of the control
                MultipleSeriesChartControl_chart.Width = Unit.Pixel(value.ChartWidth);

                //Defines whether the chart title has to display or not
                MultipleSeriesChartControl_chart.Titles[0].Visible =
                    value.IsDisplayChartTitle;

                //Assign the chart title to the chart
                if (value.IsDisplayChartTitle)
                {
                    MultipleSeriesChartControl_chart.
                        Titles["MultipleSeriesChartControl_title"].Text = value.ChartTitle;
                }

                //Defines whether the axis title has to display or not
                if (value.IsDisplayAxisTitle)
                {
                    MultipleSeriesChartControl_chart.ChartAreas
                        ["MultipleSeriesChartControl_chartArea"].AxisX.Title = value.XAxisTitle;

                    MultipleSeriesChartControl_chart.ChartAreas
                        ["MultipleSeriesChartControl_chartArea"].AxisY.Title = value.YAxisTitle;
                }

                //Databind the values to the chart
                MultipleSeriesChartControl_chart.DataBindTable(value.MultipleSeriesChartDataSource,
                    "ShortName");                

                //Assign the tooltip for each chart series 
                foreach (Series item in MultipleSeriesChartControl_chart.Series)
                {
                    item.ToolTip = "#VAL";
                    item.ChartType = value.ChartType;
                    //item.IsXValueIndexed = true;
                }

                if (value.IsShowLabel)
                {
                    foreach (Series item in MultipleSeriesChartControl_chart.Series)
                    {
                        
                        item.SmartLabelStyle.Enabled = false;
                        item.IsValueShownAsLabel = true;
                        item.LabelAngle=-90;
                        //item.CustomProperties = "LabelStyle = Center";
                    }
                }

                //Get the new session ID
                string sessionId = System.Guid.NewGuid().ToString();

                //Assign the session ID to the session 
                Session[sessionId] = value;

                //Assign the java script to show the zoomed chart for the td 
                MultipleSeriesChartControl_td.Attributes.Add("onclick",
                    "javascript:return ShowZoomedChart('" + sessionId + "','MultiSeries');");

                if (!Utility.IsNullOrEmpty(value.ChartImageName))
                {
                    //if (!new FileInfo(Server.MapPath("../chart/") + value.ChartImageName + ".png").Exists)
                        MultipleSeriesChartControl_chart.SaveImage(Server.MapPath("../chart/") + value.ChartImageName + ".png", ChartImageFormat.Png);
                }

            }
        }
        #endregion Properties
    }
}