﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnlineInterviewSessionPreviewControl.ascx.cs" 
Inherits="Forte.HCM.UI.CommonControls.OnlineInterviewSessionPreviewControl" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                        <asp:Literal ID="OnlineInterviewSessionPreviewControl_questionResultLiteral" runat="server"
                            Text="Online Interview Session"></asp:Literal>
                    </td>
                    <td style="width: 50%" valign="top" align="right">
                        <asp:ImageButton ID="OnlineInterviewSessionPreviewControl_topCancelImageButton" runat="server"
                            SkinID="sknCloseImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="popup_td_padding_10">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                <tr>
                    <td class="popup_td_padding_10">
                        <table width="100%" cellpadding="2" cellspacing="5" border="0">
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_testNameHeadLabel" runat="server" Text="Interview Session Name"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" colspan="3" style="width: 35%">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_testNameLabel" runat="server" Text="" ReadOnly="true"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_sessionNoHeadLabel" runat="server" Text="Number of Sessions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left" style="width: 35%" colspan="3">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="OnlineInterviewSessionPreviewControl_sessionNoLabel" runat="server" Width="30px"
                                                    SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <table width="60%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="OnlineInterviewSessionPreviewControl_expiryDateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_positionProfileHeadLabel" runat="server"
                                        Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_positionProfileLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_sessionDescLabel" runat="server" Text="Session Description"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="literal_text_wrap" style="height: 40px; width: 450px; overflow: auto;">
                                        <asp:Literal ID="OnlineInterviewSessionPreviewControl_sessionDescLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="OnlineInterviewSessionPreviewControl_instructionsLabel" runat="server" Text="Interview Instructions"
                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td colspan="3" align="left">
                                    <div class="literal_text_wrap" style="height: 40px; width: 450px; overflow: auto;">
                                        <asp:Literal ID="OnlineInterviewSessionPreviewControl_instructionsLiteral" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
