﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientDetailView.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ClientDetailView" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <asp:Label ID="ClientDetailView_clientNameLabel" runat="server" Text="Client Name"
                ToolTip="Client Name" SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDetailView_eMailIDLabel" runat="server" ToolTip="Email ID" Text="Email ID"
                SkinID="sknLabelFieldTextBold"></asp:Label>
        </td>
        <td colspan="6" align="right">
            <table cellpadding="1" cellspacing="3" width="32%" border="0">
                <tr>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_editClientImageButton" runat="server" SkinID="sknEditClientImageButton"
                            OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_deleteClientImageButton" runat="server" SkinID="sknDeleteClientImageButton"
                            OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_viewDepartmentImageButton" runat="server" SkinID="sknViewDepartmentImageButton"
                            OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_addDepartmentImageButton" runat="server" SkinID="sknAddDepartmentImageButton"
                            OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_viewContactImageButton" runat="server" SkinID="sknViewContactImageButton"
                            OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_addContactImageButton" runat="server" SkinID="sknAddContactImageButton"
                            OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_viewPositionProfileImageButton" runat="server"
                            SkinID="sknViewPositionProfileImageButton1" OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                    <td>
                        <asp:ImageButton ID="ClientDetailView_createPositionProfileImageButton" runat="server"
                            SkinID="sknCreatePositionProfileImageButton1" OnCommand="ClientDetailView_ImageButton_Command" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td style="width: 10%">
            <asp:Label ID="ClientDetailView_phoneNoHeadLabel" runat="server" Text="Phone No"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientDetailView_phoneNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td style="width: 10%">
            <asp:Label ID="ClientDetailView_faxNoHeadLabel" runat="server" Text="Fax No" SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientDetailView_faxNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td style="width: 10%">
            <asp:Label ID="ClientDetailView_zipHeadLabel" runat="server" Text="Zip Code" SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td style="width: 20%">
            <asp:Label ID="ClientDetailView_zipLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="ClientDetailView_streetAddressHeadLabel" runat="server" Text="Street Address"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDetailView_streetAddressLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDetailView_cityHeadLabel" runat="server" Text="City" SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDetailView_cityLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDetailView_stateHeadLabel" runat="server" Text="State Name"
                SkinID="sknLabelFieldHeaderText"></asp:Label>
        </td>
        <td>
            <asp:Label ID="ClientDetailView_statelabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
        </td>
    </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    
    <tr>
            <td>
                <asp:Label ID="ClientDetailView_countryHeadLabel" runat="server" Text="Country" SkinID="sknLabelFieldHeaderText"></asp:Label>
            </td>
            <td>
                <asp:Label ID="ClientDetailView_countryLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
            </td>
            <td>
                <asp:Label ID="ClientDetailView_feinNoHeadLabel" runat="server" Text="FEIN NO" SkinID="sknLabelFieldHeaderText"></asp:Label>
            </td>
            <td>
                <asp:Label ID="ClientDetailView_feinNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
            </td>
            <td>
                <asp:Label ID="ClientDetailView_wesiteURLHeadLabel" runat="server" Text="Website URL"
                    SkinID="sknLabelFieldHeaderText"></asp:Label>
            </td>
            <td>
                <asp:Label ID="ClientDetailView_wesiteURLLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
            </td>
        </tr>
    <tr class="td_height_5">
        <td colspan="6">
        </td>
    </tr>
    <tr>
            <td colspan="6">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 50%">
                            <asp:Label ID="ClientDetailView_additionalInfoHeadLabel" runat="server" Text="Additional Info"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 50%">
                            <asp:Label ID="ClientDetailView_departmentsHeadLabel" runat="server" Text="List of Departments"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="height: 50px; width: 100%; overflow: auto;word-wrap: break-word;white-space:normal;">
                                <asp:Label ID="ClientDetailView_additionalInfoLabel" runat="server" Text="Additional Info"
                                    SkinID="sknLabelFieldText"></asp:Label>
                            </div>
                        </td>
                        <td>
                            <div style="height: 50px; width: 100%; overflow: auto;word-wrap: break-word;white-space:normal;">
                                <asp:Repeater ID="ClientDetailView_departmentListRepeater" runat="server" 
                                    OnItemCommand="ClientDetailView_departmentListRepeater_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="ClientContactDetailView_departmentNameLinkButton" runat="server"
                                            SkinID="sknActionLinkButton" Text='<%# Eval("DepartmentName") %>' CommandName="showclientdepartment"
                                            CommandArgument='<%# Eval("DepartmentID") %>' ToolTip="Click here to view department details"></asp:LinkButton><br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
</table>
