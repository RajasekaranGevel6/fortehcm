﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutomatedInterviewSummaryControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.AutomatedInterviewSummaryControl" %>
<%@ Register Src="SingleSeriesChartControl.ascx" TagName="SingleSeriesChartControl"
    TagPrefix="uc1" %>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td width="30%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    <asp:Literal ID="AutomatedInterviewSummaryControl_testSummaryLiteral" runat="server" Text="Interview Summary"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg" valign="middle">
                        <div style="width: 100%; height: 150px; overflow: auto;">
                            <table width="100%" cellpadding="2" cellspacing="4" border="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_noOfQuestionHeadLabel" runat="server"
                                            Text="Number Of Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_noOfQuestionLabel" runat="server" Text="25"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_costOfTestHeadLabel" runat="server" Text="Cost Of Interview (in terms of credits)"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_costOfTestLabel" runat="server" Text="4.75"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_avgTimeHeadLabel" runat="server" Text="Average Time Taken By Candidates"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_avgTimeLabel" runat="server" Text="00:50:00"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_avgComplexityHeadLabel" runat="server"
                                            Text="Average Level Of Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="AutomatedInterviewSummaryControl_avgComplexityLabel" runat="server" Text="Normal"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td width="1%">
        </td>
        <td width="36%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    <asp:Literal ID="AutomatedInterviewSummaryControl_testAreaLiteral" runat="server" Text="Interview Area Statistics">
                                    </asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg" valign="middle">
                        <div style="width: 100%; height: 150px; overflow: auto;">
                            <%--<asp:GridView ID="AutomatedInterviewSummaryControl_testAreaGridView" runat="server" AutoGenerateColumns="False"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="TestArea" HeaderText="Test Area" />
                                    <asp:BoundField DataField="QuestionCount" HeaderText="Question Count" />
                                </Columns>
                            </asp:GridView>--%>
                            <uc1:SingleSeriesChartControl ID="ManualTestSummaryControl_testAreaChart" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td width="1%">
        </td>
        <td width="30%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="header_text_bold">
                                    Complexity Statistics
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="panel_body_bg">
                        <div style="height: 150px; overflow: auto;">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td align="left">
                                        <uc1:SingleSeriesChartControl ID="AutomatedInterviewSummaryControl_complexityChart" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="td_height_5" colspan="3">
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="15%" class="header_text_bold">
                                    <asp:Literal ID="AutomatedInterviewSummaryControl_testSegmemtLiteral" runat="server" Text="Interview Statistics">
                                    </asp:Literal>&nbsp;<%--<asp:Label ID="AutomatedInterviewSummaryControl_sortHelpLabel" runat="server"
                                        SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" class="grid_body_bg">
                        <asp:UpdatePanel ID="AutomatedTestSummarControl" runat="server">
                            <ContentTemplate>
                                <div id="AutomatedInterviewSummaryControl_testSegmentDiv" runat="server">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:GridView ID="AutomatedInterviewSummaryControl_testSegmemtGridView" runat="server"
                                                    GridLines="Horizontal" BorderColor="white" BorderWidth="1px" AutoGenerateColumns="False"
                                                    Width="100%" OnRowDataBound="AutomatedInterviewSummaryControl_testSegmemtGridView_RowDataBound"
                                                    OnRowCreated="AutomatedInterviewSummaryControl_testSegmemtGridView_RowCreated">
                                                    <RowStyle CssClass="grid_alternate_row" />
                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                    <HeaderStyle CssClass="grid_header_row" />
                                                    <Columns>
                                                        <asp:BoundField DataField="CategoryName" HeaderText="Category" />
                                                        <asp:BoundField DataField="SubjectName" HeaderText="Subject" />
                                                        <asp:BoundField DataField="TestAreaName" HeaderText="Interview Area" />
                                                        <asp:BoundField DataField="Complexity" HeaderText="Complexity" />
                                                        <asp:BoundField DataField="NoofQuestionsInCategory" HeaderText="Question Count" ItemStyle-CssClass="td_padding_right_20"
                                                            ItemStyle-Width="18px" ItemStyle-HorizontalAlign="right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
