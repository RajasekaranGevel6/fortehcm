﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WidgetTypesListControl.ascx.cs" Inherits="Forte.HCM.UI.CommonControls.WidgetTypesListControl" %>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="dashboard_header_bg">
            <asp:Literal ID="DesignReport_reportLiteral" runat="server" Text="Dashboard"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td class="dashboard_body_bg">
             <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <asp:Repeater ID="WidgetTypesListControl_widgetTypeRepeater" runat="server" 
                            onitemcommand="WidgetTypesListControl_widgetTypeRepeater_ItemCommand" 
                            onitemdatabound="WidgetTypesListControl_widgetTypeRepeater_ItemDataBound">
                        <ItemTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Image ID="DesignReport_addImage" runat="server" Width="77px" Height="75px" 
                                        AlternateText='<%# Eval("Title") %>' ToolTip='<%# "Add " + Eval("Title") %>' ImageUrl='<%# "~/App_Themes/DefaultTheme/Images/" + Eval("SkinID") %>' />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="right">
                                                <asp:LinkButton ID="DesignReport_generalStatisticsInfoAddLeftLinkButton" runat="server"
                                                    SkinID="sknDashboardLinkButton" Text="Add" CommandName='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'
                                                    CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>