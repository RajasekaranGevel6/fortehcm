﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.HeaderControl" %>

    <%@ Register Src="ChangePassword.ascx" TagName="ChangePassword" TagPrefix="uc1" %>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left" style="width: 15%; vertical-align: middle; padding-top: 5px">
                        <p onclick="location.href='<%= homeUrl %>'">
                            <asp:Image ID="HomePage_logoImage" runat="server" SkinID="sknHomePageLogoImage" />
                        </p>
                        <%--<asp:ImageButton ID="HomePage_logoImageButton" runat="server" SkinID="sknHomePageLogoImageButton" OnClick="HomePage_logoImage_Home_Click" />--%>
                    </td>
                    <td style="width: 85%" align="right" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td valign="top" align="right">
                                    <table cellpadding="3" cellspacing="2" border="0" class="login_bg" style="float: right;"
                                        id="HeaderControl_loginTable" runat="server">
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="HomePage_userNameLabel" Text="User Name" SkinID="sknHomePageLabel"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" SkinID="sknHomePageTextBox" ID="HomePage_userNametextbox"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="HomePage_passwordLabel" runat="server" Text="Password" SkinID="sknHomePageLabel"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="HomePage_passwordtextbox" TextMode="Password" SkinID="sknHomePageTextBox"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="HomePage_goImageButton" runat="server" SkinID="sknHomePageGoImageButton"
                                                    OnClick="HomePage_goImageButton_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td colspan="3" align="left">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:CheckBox ID="HomePage_rememberMeCheckBox" runat="server" Text="Remember me"
                                                                CssClass="labletext" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td align="right">
                                                            <asp:LinkButton ID="HomePage_forgotPasswordLinkButton" runat="server" Text="Forgot Password?"
                                                                CssClass="link_btn"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="2" cellpadding="2" id="HeaderControl_loggedInTable"
                                        runat="server">
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Signed in as <span class="username_text">
                                                    <asp:Label ID="HeaderControl_loginUserNameLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Last Login: <span class="date_time_text">
                                                    <asp:Label ID="HeaderControl_lastLoginDateTimeLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="HeaderControl_logoutButton" runat="server" Text="Log Out" SkinID="sknButtonId"
                                                                OnClick="HeaderControl_logoutButton_Click" Width="60px" />
                                                        </td>
                                                        <td style="padding-left:5px">
                                                        
                                                        </td>
                                                        <td class="help_icon_hcm" title="Help" onclick="location.href='<%= helpUrl %>'">
                                                        </td>
                                                        <td class="home_icon_hcm" title="Home" onclick="location.href='<%= homeUrl %>'">
                                                        </td>
                                                        <td class="dashboard_icon_hcm" title="Dashboard" onclick="location.href='<%= landingUrl %>'">
                                                        </td>
                                                        <td class="user_options_icon_hcm" title="User Options" onclick="location.href='<%= userOptionsUrl %>'">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td align="left">
                                                <asp:LinkButton ID="HeaderControl_changePasswordLinkButton" runat="server" Text="Change Password"
                                                    CssClass="link_btn" Visible="false">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="HeaderControl_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                                        EnableViewState="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px; padding-bottom: 10px">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="right" style="width: 100%;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="topmenu">
                            <tr>
                                <td align="right">
                                    <asp:Menu ID="SubscriptionMaster_menu" StaticMenuItemStyle-HorizontalPadding="25"
                                        runat="server" Orientation="Horizontal" PathSeparator="/" DynamicMenuItemStyle-VerticalPadding="10"
                                        DynamicMenuStyle-BorderColor="#ced9df" DynamicMenuStyle-BorderStyle="Solid" DynamicMenuStyle-BorderWidth="1">
                                        <StaticMenuItemStyle CssClass="primaryStaticMenu" />
                                        <StaticHoverStyle CssClass="primaryStaticHover" />
                                        <DynamicMenuItemStyle CssClass="primaryDynamicMenuItem" />
                                        <DynamicHoverStyle CssClass="primaryDynamicHover" />
                                        <Items>
                                            <asp:MenuItem Text="Solutions" Selectable="true" Value="1">
                                                <asp:MenuItem Text="Enterprise Application Performance" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Data Asset Performance" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Software Architecture" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Enterprise Content Management" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Products" Selectable="true" Value="2">
                                                <asp:MenuItem Text="Professional Resource Augmentation" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="TalentScout" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Pricing" Selectable="true" Value="3">
                                                <asp:MenuItem Text="Compare Subscriptions" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Company" Selectable="true" Value="4">
                                                <asp:MenuItem Text="Explore" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="History & Name" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Strategic Indent" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Management" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Value System" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Service Model" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                            </asp:MenuItem>
                                            <asp:MenuItem Text="Partners" Selectable="true" Value="5">
                                                <asp:MenuItem Text="Show List" NavigateUrl="~/Common/PageUnderConstruction.aspx"></asp:MenuItem>
                                            </asp:MenuItem>
                                             <asp:MenuItem Text="Options" Selectable="true" Value="6">
                                                <asp:MenuItem Text="My Account" NavigateUrl="~/Admin/ViewAccount.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Edit Account" NavigateUrl="~/Admin/EditAccount.aspx"></asp:MenuItem>
                                                <asp:MenuItem Text="Upgrade Account" NavigateUrl="~/Admin/UpgradeAccount.aspx"></asp:MenuItem>
                                            </asp:MenuItem>
                                        </Items>
                                    </asp:Menu>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="HomePage_signUpImage" runat="server" OnClick="HomePage_signUpImage_Click"
                            SkinID="sknHomePageSignUpImageButton" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<asp:Panel ID="HomePage_changePasswordPanel" runat="server" Style="display: none"
    CssClass="popupcontrol_forget_password">
    <uc1:ChangePassword ID="HomePage_ChangePassword" runat="server" />
</asp:Panel>
<ajaxToolKit:ModalPopupExtender ID="HomePage_changePassword_ModalpPopupExtender"
    runat="server" TargetControlID="HeaderControl_changePasswordLinkButton" PopupControlID="HomePage_changePasswordPanel"
    BackgroundCssClass="modalBackground">
</ajaxToolKit:ModalPopupExtender>
