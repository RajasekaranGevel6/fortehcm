﻿
#region Namespace

using System;
using System.Text;
using System.Web.UI;
using System.Reflection;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using System.Web.UI.WebControls;
using System.Data;
using Forte.HCM.Common.DL;
using System.Data.Common;
using Forte.HCM.Support;

#endregion Namespace

namespace Forte.HCM.UI.CommonControls
{
    public partial class CorporateUserSignUp : UserControl
    {


        string selectedRoles = string.Empty;

        string selectedRoleIDs = string.Empty;

        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion Enum

        #region Public Properties

        public int CREATED_BY
        {
            set;
            get;
        }

        public UserRegistrationInfo InsertNewDataSource
        {
            set
            {
                if (value == null)
                    throw new Exception("Datasource should not be null");
                Forte_CorporateUserSignUp_userIdLabel.Visible = false;
                Forte_CorporateUserSignUp_userIdTextBox.Visible = true;
                Forte_CorporateSignUp_userTenantIdHidden.Value = value.UserID.ToString();
                Forte_CorporateSignUp_userCompanyHidden.Value = value.Company;
                Forte_CorporateSignUp_userPhoneHidden.Value = value.Phone;
                Forte_CorporateSignUp_userTitleHidden.Value = value.Title;
                Forte_CorporateUserSignUp_passwordTextBox.Text = "";
                Forte_CorporateUserSignUp_messagetitleLiteral.Text = "Add New User";
                Forte_CorporateUserSignUp_featureSaveButton.CommandName = "New";
                Forte_CorporateUserSignUp_passwordTr.Visible = true;
                Forte_CorporateUserSignUp_checkButton.Visible = true;
                //Forte_CorporateUsersSignUp_formNameTR.Visible = false;
                Forte_CorporateUserSignUp_userIdTextBox.ReadOnly = false;
                Forte_CorporateUserSignUp_reTypePasswordTr.Visible = true;
                Forte_CorporateUserSignUp_userEmailMandatorySpan.Visible = true;
                Forte_CorporateUsersSignUp_activeStatusTr.Visible = false;
            }
        }

        public UserRegistrationInfo EditUserDataSource
        {
            set
            {
                if (value == null)
                    throw new Exception("Datasource should not be null");
                Forte_CorporateUserSignUp_userIdLabel.Visible = true;
                Forte_CorporateUserSignUp_userIdTextBox.Visible = false;
                Forte_CorporateUserSignUp_userIdTextBox.Text = value.UserEmail;
                Forte_CorporateUserSignUp_userIdLabel.Text = value.UserEmail;
                Forte_CorporateUserSignUp_passwordTr.Visible = false;
                Forte_CorporateUserSignUp_checkButton.Visible = false;
                //Forte_CorporateUsersSignUp_formNameTR.Visible = false;
                Forte_CorporateUserSignUp_messagetitleLiteral.Text = "Edit User";
                Forte_CorporateUserSignUp_userIdTextBox.ReadOnly = true;
                Forte_CorporateUserSignUp_firstNameTextBox.Text = value.FirstName;
                Forte_CorporateUserSignUp_lastNameTextBox.Text = value.LastName;
                Forte_CorporateSignUp_editUserTenantIdHidden.Value = value.UserID.ToString();
                Forte_CorporateUserSignUp_reTypePasswordTr.Visible = false;
                Forte_CorporateUserSignUp_userEmailMandatorySpan.Visible = false;
                Forte_CorporateUserSignUp_NoOfUsersHiddenField.Value =  value.NoOfUsers.ToString();
                Forte_CorporateUserSignUp_featureSaveButton.CommandName = "Edit";
                if (value.IsActiveStatus == 1)
                    Forte_CorporateUsersSignUp_activeStatusTr.Visible = true;
                else
                    Forte_CorporateUsersSignUp_activeStatusTr.Visible = false;
                //if (value.IsActive == 1)
                //    Forte_CorporateUsersSignUp_activeCheckBox.Checked = true;
                //else
                //    Forte_CorporateUsersSignUp_activeCheckBox.Checked = false;
                Forte_CorporateUserSignUp_activeStateHiddenField.Value = value.IsActive.ToString();
                if (value.IsCustomerAdmin == true)
                    Forte_CorporateUserSignUp_customerAdminCheckBox.Checked = true;
                else
                    Forte_CorporateUserSignUp_customerAdminCheckBox.Checked = false;

                for (int i = 0; i < value.CorporateRoles.Split(',').Length; i++)
                {
                    for (int j = 0; j < Forte_CorporateUserSignUp_rolesCheckBoxList.Items.Count; j++)
                    {
                        if (Forte_CorporateUserSignUp_rolesCheckBoxList.Items[j].Value == value.CorporateRoles.Split(',')[i])
                        {
                            Forte_CorporateUserSignUp_rolesCheckBoxList.Items[j].Selected = true;

                            selectedRoles = selectedRoles + Forte_CorporateUserSignUp_rolesCheckBoxList.Items[j].Text + ",";

                            selectedRoleIDs = selectedRoleIDs + Forte_CorporateUserSignUp_rolesCheckBoxList.Items[j].Value + ",";
                        }
                    }
                }
                Forte_CorporateUserSignUp_rolesComboTextBox.Text = selectedRoles.TrimEnd(',');
                Forte_CorporateUserSignUp_rolesHiddenField.Value = selectedRoleIDs.TrimEnd(',');
                Forte_CorporateUserSignUp_rolesComboTextBox.ToolTip = Forte_CorporateUserSignUp_rolesComboTextBox.Text;
            }
        }

        #endregion Public Properties

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Forte_CorporateUserSignUp_passwordTextBox.Text = "";

                BindCorporateRoles();
                //LoadFormNamesInDropDownList();
            }

            Forte_CorporateUserSignUp_passwordTextBox.Attributes.Add("value", Forte_CorporateUserSignUp_passwordTextBox.Text);
            Forte_CorporateUserSignUp_confirmPasswordTextBox.Attributes.Add("value", Forte_CorporateUserSignUp_confirmPasswordTextBox.Text);
        }

        private void BindCorporateRoles()
        {
            Forte_CorporateUserSignUp_rolesCheckBoxList.DataSource = null;
            
            List<Roles> roleList = null;
            
            if(Session["Tenant_id"]!=null)
                roleList  =new AuthenticationBLManager().GetAssignedCorporateRole(
                Convert.ToInt16(Session["Tenant_id"].ToString()));

            if (roleList == null) return;

            Forte_CorporateUserSignUp_rolesCheckBoxList.DataSource = roleList;
            Forte_CorporateUserSignUp_rolesCheckBoxList.DataTextField = "RoleName";
            Forte_CorporateUserSignUp_rolesCheckBoxList.DataValueField = "RoleID";
            Forte_CorporateUserSignUp_rolesCheckBoxList.DataBind();
            Forte_CorporateUserSignUp_rolesCheckBoxList.Text = "";
        }

        protected void ClosePopUpClick(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch { }
        }

        protected void Forte_CorporateUserSignUp_checkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_userIdTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistraion_EmptyUserEmail);
                    return;
                }
                if (!IsValidEmailAddress(Forte_CorporateUserSignUp_userIdTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_InvalidEmailAddress);
                    return;
                }
                if (CheckForEmailAddressAvailability(Forte_CorporateUserSignUp_userIdTextBox.Text.Trim()))
                    Forte_CorporateUserSignUp_validEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailAvailable;
                else
                    Forte_CorporateUserSignUp_inValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                Forte_CorporateUserSignUp_userEmailAvailableStatusDiv.Style.Add("display", "block");
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
            finally
            {
                InvokePageBaseMethod("ShowSignUpPopUp", null);
            }
        }

        protected void Forte_CorporateUserSignUp_featureSaveButton_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (ListItem item in Forte_CorporateUserSignUp_rolesCheckBoxList.Items)
                {
                    if (item.Selected)
                    {
                        selectedRoles = selectedRoles + item.Text + ",";

                        selectedRoleIDs = selectedRoleIDs + item.Value + ",";
                    }
                }

                Forte_CorporateUserSignUp_rolesComboTextBox.Text = selectedRoles.TrimEnd(',');
                Forte_CorporateUserSignUp_rolesHiddenField.Value = selectedRoleIDs.TrimEnd(',');
                Forte_CorporateUserSignUp_rolesComboTextBox.ToolTip = Forte_CorporateUserSignUp_rolesComboTextBox.Text;

                if (!IsValidData())
                {
                    InvokePageBaseMethod("ShowSignUpPopUp", null);
                    return;
                }

               // Forte_CorporateUserSignUp_rolesComboTextBox.Focus();

               // InvokePageBaseMethod("ShowSignUpPopUp", null);

             
                switch (Forte_CorporateUserSignUp_featureSaveButton.CommandName)
                {
                    case "New":
                        string Confirmation_Code = string.Empty;
                        SendConfirmationCodeEmail(Forte_CorporateUserSignUp_userIdTextBox.Text.Trim(), out Confirmation_Code);
                        InsertCorporateUser(Confirmation_Code);
                        InvokePageBaseMethod("ShowSuccessMessage", new object[] { HCMResource.UserRegistration_UserInserted });
                        break;
                    case "Edit":
                       
                            if (CanAddNewUser())
                            {
                                UpdateUser();
                                InvokePageBaseMethod("ShowSuccessMessage", new object[] { HCMResource.CorporateUserManagement_CorporateUserUpdatedSuccessfully });

                            }
                            else
                                InvokePageBaseMethod("ShowErrMessage", new object[] { HCMResource.DeactivateUsersMessage });
                  
                     
                        
                        break;
                }
                ClearControls();
                InvokePageBaseMethod("RebindGridView", null);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        private bool CanAddNewUser()
        {
            UserRegistrationInfo userEditInfo = new AdminBLManager().GetCorporateUserDetails(Convert.ToInt16(Forte_CorporateSignUp_editUserTenantIdHidden.Value));

            /*   if (Forte_CorporateUsersSignUp_activeCheckBox.Checked == true)
                   return Convert.ToInt32(userEditInfo.ActiveNoOfUsers) <=
                        Convert.ToInt32(Forte_CorporateUserSignUp_NoOfUsersHiddenField.Value) ? true : false;
               else*/
            return true;
        }

        #endregion Events

        #region Private Methods

        /// <summary>
        /// A Method that updates the user corporate account details
        /// </summary>
        private void UpdateUser()
        {
            UserRegistrationInfo userRegistrationInfo = null;
            try
            {
                userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.UserEmail = Forte_CorporateUserSignUp_userIdTextBox.Text.Trim();
                userRegistrationInfo.FirstName = Forte_CorporateUserSignUp_firstNameTextBox.Text.Trim();
                userRegistrationInfo.LastName = Forte_CorporateUserSignUp_lastNameTextBox.Text.Trim();
                userRegistrationInfo.ModifiedBy = CREATED_BY;
                //userRegistrationInfo.FormId = Convert.ToInt32(Forte_CorporateUserSignUp_formNameDropDownList.SelectedValue);
                userRegistrationInfo.UserID = Convert.ToInt32(Forte_CorporateSignUp_editUserTenantIdHidden.Value);
                userRegistrationInfo.IsActive = Convert.ToInt16(Forte_CorporateUserSignUp_activeStateHiddenField.Value);
               // userRegistrationInfo.IsActive = Convert.ToInt16(Forte_CorporateUsersSignUp_activeCheckBox.Checked ? 1 : 0);
                if (Forte_CorporateUserSignUp_customerAdminCheckBox.Checked == false)
                    userRegistrationInfo.IsCustomerAdmin = false;
                else
                    userRegistrationInfo.IsCustomerAdmin = true;
                userRegistrationInfo.CorporateRoles = Forte_CorporateUserSignUp_rolesHiddenField.Value;

                new UserRegistrationBLManager().UpdateCorporateUserAdmin(userRegistrationInfo);
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that insert the new corporate child user
        /// in to the repository
        /// </summary>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the confirmation code to be 
        /// sent to the user 
        /// </param>
        private void InsertCorporateUser(string Confirmation_Code)
        {
            UserRegistrationInfo userRegistrationInfo = null;
            RegistrationConfirmation registrationConfirmation = null;
            IDbTransaction transaction = null;
            try
            {
                userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.UserEmail = Forte_CorporateUserSignUp_userIdTextBox.Text.Trim();
                userRegistrationInfo.Password = new EncryptAndDecrypt().EncryptString(Forte_CorporateUserSignUp_passwordTextBox.Text.Trim());
                userRegistrationInfo.FirstName = Forte_CorporateUserSignUp_firstNameTextBox.Text.Trim();
                userRegistrationInfo.LastName = Forte_CorporateUserSignUp_lastNameTextBox.Text.Trim();
                userRegistrationInfo.Phone = Forte_CorporateSignUp_userPhoneHidden.Value.Trim();
                userRegistrationInfo.Company = Forte_CorporateSignUp_userCompanyHidden.Value.Trim();
                userRegistrationInfo.Title = Forte_CorporateSignUp_userTitleHidden.Value.Trim();
                //userRegistrationInfo.NUMBER_OF_USERS = UserRegistration_numberOfUsersTextBox.Text.Trim() == "" ? Convert.ToInt16(0) :
                //  Convert.ToInt16(UserRegistration_numberOfUsersTextBox.Text.Trim());
                if (Forte_CorporateUserSignUp_customerAdminCheckBox.Checked)
                {
                    userRegistrationInfo.IsCustomerAdmin = true;
                    userRegistrationInfo.SubscriptionRole = Enum.GetName(typeof(SubscriptionRolesEnum), 4);
                }
                else
                {
                    userRegistrationInfo.IsCustomerAdmin = false;
                    userRegistrationInfo.SubscriptionRole = Enum.GetName(typeof(SubscriptionRolesEnum), 4);
                }
                userRegistrationInfo.CorporateRoles = Forte_CorporateUserSignUp_rolesHiddenField.Value;
                registrationConfirmation = new RegistrationConfirmation();
                registrationConfirmation.ConfirmationCode = Confirmation_Code;
                //Confirmation_Code = registrationConfirmation.CONFIRMATION_CODE;
                transaction = new TransactionManager().Transaction;
                new UserRegistrationBLManager().InsertCorporateUser(Convert.ToInt32(Forte_CorporateSignUp_userTenantIdHidden.Value),
                    userRegistrationInfo, registrationConfirmation, transaction as DbTransaction);

               // new UserRegistrationBLManager().InsertCorporateUserRoles(userRegistrationInfo, transaction as DbTransaction);
                transaction.Commit();
            }
            catch
            {
                if (!Utility.IsNullOrEmpty(transaction))
                    transaction.Rollback();
                throw;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction = null;
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                if (registrationConfirmation != null) registrationConfirmation = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }

        }

        /// <summary>
        /// A Method that sends the email to the user.
        /// </summary>
        /// <param name="UserEmail">
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the confiration code
        /// to activate the account.
        /// </param>
        private void SendConfirmationCodeEmail(string UserEmail, out string Confirmation_Code)
        {
            Confirmation_Code = new EncryptAndDecrypt().EncryptString(
                                                RandomString(15));
            MailDetail mailDetails = new MailDetail();
            EmailHandler Email = new EmailHandler();
            Email.SendMail(UserEmail, HCMResource.UserRegistration_MailActivationCodeSubject,
                GetMailBody(new EncryptAndDecrypt().EncryptString(UserEmail), Confirmation_Code));
        }

        /// <summary>
        /// A Method that gets the body content of the email
        /// </summary>
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that contains the activation mail body
        /// </returns>
        private string GetMailBody(string UserEmail, string ConfirmationCode)
        {
            StringBuilder sbBody = new StringBuilder();
            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(Forte_CorporateUserSignUp_firstNameTextBox.Text.Trim());
                sbBody.Append(" ");
                sbBody.Append(Forte_CorporateUserSignUp_lastNameTextBox.Text.Trim());
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This is to acknowledge that your request to create an account was processed successfully.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("To Activate your account, please copy and paste the ");
                sbBody.Append("below URL in the browser or click on the link");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(WebConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    WebConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    UserEmail, ConfirmationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// A Method that validates the user email
        /// using regular expression
        /// </summary>
        /// <param name="User_Email">
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds whether the user
        /// email is valid email or not.
        /// </returns>
        private bool IsValidEmailAddress(string User_Email)
        {
            //Regex regex = new Regex(".+@.+\\.[a-z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(User_Email);
        }

        /// <summary>
        /// A Method that checks in the repository for user email
        /// </summary>
        /// <param name="User_Email">
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the whether the user email
        /// exists or not in the repository.
        /// </returns>
        private bool CheckForEmailAddressAvailability(string User_Email)
        {
            return new UserRegistrationBLManager().CheckUserEmailExists(User_Email, 1);
        }

        /// <summary>
        /// A Method that clear all the controls in the user control
        /// </summary>
        private void ClearControls()
        {
            Forte_CorporateUserSignUp_userIdTextBox.Text = "";
            Forte_CorporateUserSignUp_passwordTextBox.Attributes.Add("value", "");
            Forte_CorporateUserSignUp_confirmPasswordTextBox.Attributes.Add("value", "");
            Forte_CorporateUserSignUp_confirmPasswordTextBox.Text = "";
            Forte_CorporateUserSignUp_passwordTextBox.Text = "";
            Forte_CorporateUserSignUp_firstNameTextBox.Text = "";
            Forte_CorporateUserSignUp_lastNameTextBox.Text = "";

            for (int i = 0; i < Forte_CorporateUserSignUp_rolesCheckBoxList.Items.Count; i++)
            {
                Forte_CorporateUserSignUp_rolesCheckBoxList.Items[i].Selected = false;
            }
            Forte_CorporateUserSignUp_rolesComboTextBox.Text = "";
        }

        /// <summary>
        /// A Method that validates the user input data
        /// </summary>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the 
        /// user input data is valid or not
        /// </returns>
        private bool IsValidData()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_userIdTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistraion_EmptyUserEmail);
                isValid = false;
            }
            else if (!IsValidEmailAddress(Forte_CorporateUserSignUp_userIdTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_InvalidEmailAddress);
                isValid = false;
            }
            else if (!CheckForEmailAddressAvailability(Forte_CorporateUserSignUp_userIdTextBox.Text.Trim()) &&
                Forte_CorporateUserSignUp_passwordTr.Visible)
            {
                ShowErrorMessage(HCMResource.UserRegistration_UserEmailNotAvailable);
                isValid = false;
            }
            if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_passwordTextBox.Text.Trim()) &&
                Forte_CorporateUserSignUp_passwordTr.Visible)
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyPassword);
                isValid = false;
            }
            if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_confirmPasswordTextBox.Text.Trim()) &&
                Forte_CorporateUserSignUp_reTypePasswordTr.Visible)
            {
                ShowErrorMessage(HCMResource.UserRegistration_ConfirmPasswordEmpty);
                isValid = false;
            }
            if (Forte_CorporateUserSignUp_passwordTr.Visible &&
                !string.IsNullOrEmpty(Forte_CorporateUserSignUp_passwordTextBox.Text.Trim()) &&
                !string.IsNullOrEmpty(Forte_CorporateUserSignUp_confirmPasswordTextBox.Text.Trim()) &&
                Forte_CorporateUserSignUp_passwordTextBox.Text.Trim() != Forte_CorporateUserSignUp_confirmPasswordTextBox.Text.Trim())
            {
                ShowErrorMessage(HCMResource.UserRegistration_PasswordMisMatch);
                isValid = false;
            }
            else if (Forte_CorporateUserSignUp_passwordTr.Visible &&
                Forte_CorporateUserSignUp_passwordTextBox.Text.Trim().Length < 6)
            {
                ShowErrorMessage(HCMResource.UserRegistration_MinimumPasswordFailed);
                isValid = false;
            }
            if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_firstNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyFirstName);
                isValid = false;
            }
            if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_lastNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyLastName);
                isValid = false;
            }
            //if (Forte_CorporateUserSignUp_formNameDropDownList.SelectedIndex == 0 && Forte_CorporateUsersSignUp_formNameTR.Visible)
            //{
            //    ShowErrorMessage(HCMResource.UserRegistration_FormDropDownNotSelected);
            //    isValid = false;
            //}
            if (Forte_CorporateUserSignUp_rolesHiddenField.Value.Length == 0)
            {
                ShowErrorMessage("Select role(s) for the user");
                isValid = false;
            }


            return isValid;
        }

        private void ShowErrorMessage(string Message)
        {
            if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_topErrorMessageLabel.Text))
                Forte_CorporateUserSignUp_topErrorMessageLabel.Text = Message;
            else
                Forte_CorporateUserSignUp_topErrorMessageLabel.Text += "<br />" + Message;
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_topErrorMessageLabel.Text))
                Forte_CorporateUserSignUp_topErrorMessageLabel.Text = exp.Message;
            else
                Forte_CorporateUserSignUp_topErrorMessageLabel.Text += "<br />" + exp.Message;
            InvokePageBaseMethod("ShowSignUpPopUp", null);
        }

        //private void ShowSuccessMessage(string Message)
        //{
        //    if (string.IsNullOrEmpty(Forte_CorporateUserSignUp_topSuccessMessageLabel.Text))
        //        Forte_CorporateUserSignUp_topSuccessMessageLabel.Text = Message;
        //    else
        //        Forte_CorporateUserSignUp_topSuccessMessageLabel.Text += "<br />" + Message;
        //}

        /// <summary>
        /// Invokes the parent page base public methods
        /// </summary>
        /// <param name="MethodName">
        /// A <see cref="System.String"/> that holds the method name to invoke
        /// </param>
        /// <param name="args">
        /// A <see cref="System.Object"/> that holds the arguments to be passed to the method
        /// </param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        #endregion Private Methods

        protected void Forte_CorporateUserSignUp_rolesCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Forte_CorporateUserSignUp_rolesCheckBoxList.Focus();

        
            

        }

    }
}