﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ContactControl.cs
// File that represents the user interface for the Create,Edit and View the client.

#endregion Header

#region Directives
using System;
using System.Web.UI;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Web.UI.WebControls;
using Forte.HCM.Support;
using ReflectionComboItem;
using Resources;
using System.Web.UI.HtmlControls;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ContactControl : System.Web.UI.UserControl
    {
        // The public events must conform to this format
        public delegate void Button_CommandClick(object sender, CommandEventArgs e);
        public delegate void DropDown_sellectedIndexChanged(object sender, EventArgs e);
        string CLIENT_CONTACT_ATTRIBUTES_DATASOURCE = "CLIENT_CONTACT_ATTRIBUTES_DATASOURCE";
        // These two public events are to be customized as need on the page and will be called
        // by the private events of the buttons in the user control
        public event Button_CommandClick OkCommandClick;
        //public event Button_Click CancelClick;

        public event DropDown_sellectedIndexChanged SellectedIndexChanged;
        protected void Page_Load(object sender, EventArgs e)
        {
            ContactControl_firstNameTextBox.Focus();
            ContactControl_clientNameDropDownList.SelectedIndexChanged += new EventHandler(ContactControl_clientNameDropDownList_SelectedIndexChanged);
            // ContactControl_createButton.CommandName = Constants.ClientManagementConstants.ADD_CONTACT;
        }

        void ContactControl_clientNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDepartemnts(null);
            // ContactControl_departmentMultiSelectControl.SelectedProperties = "";
            ContactControl_clientIDHiddenField.Value = ContactControl_clientNameDropDownList.SelectedValue.ToString();
            if (SellectedIndexChanged != null)
                this.SellectedIndexChanged(sender, e);
        }


        public int Client_ID
        {
            set
            {
                ContactControl_clientIDHiddenField.Value = value.ToString();
            }
            get
            {
                int client_ID = 0;
                int.TryParse(ContactControl_clientIDHiddenField.Value, out client_ID);
                return client_ID;
            }
        }
        public ClientInformation ClientDataSource
        {
            set
            {
                if (value == null)
                {
                    // ContactControl_descriptionTextBox.Text = string.Empty;
                    ContactControl_streetAddressTextBox.Text = string.Empty;
                    ContactControl_eMailIDTextBox.Text = string.Empty;
                    ContactControl_cityTextBox.Text = string.Empty;
                    ContactControl_countryTextBox.Text = string.Empty;
                    ContactControl_stateTextBox.Text = string.Empty;
                    ContactControl_zipCodeTextBox.Text = string.Empty;
                    //  ContactControl_departmentNameTextBox.Text = string.Empty;
                    ContactControl_faxNoTextBox.Text = string.Empty;
                    ContactControl_phoneNoTextBox.Text = string.Empty;
                    ContactControl_createButton.CommandName = "add";
                    ContactControl_createButton.CommandArgument = "0";
                    // ContactControl_departmentMultiSelectControl.SelectedProperties = "";
                    ContactControl_messagetitleLiteral.Text = "Add Contact";
                    //   ContactControl_departmentMultiSelectControl.DepartmentDataSource = null;
                    BindDepartemnts(value);
                    return;
                }
                //   ContactControl_descriptionTextBox.Text = value.DepartmentName;

                ContactControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                ContactControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                ContactControl_cityTextBox.Text = value.ContactInformation.City;
                ContactControl_countryTextBox.Text = value.ContactInformation.Country;
                ContactControl_stateTextBox.Text = value.ContactInformation.State;
                ContactControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                //   ContactControl_departmentNameTextBox.Text = value.DepartmentDescription;
                ContactControl_faxNoTextBox.Text = value.FaxNumber;
                ContactControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                ContactControl_messagetitleLiteral.Text = "Add Contact";
                ContactControl_createButton.CommandName = "add";
                ContactControl_createButton.CommandArgument = "0";
                ContactControl_clientIDHiddenField.Value = value.ClientID.ToString();
                //ContactControl_departmentMultiSelectControl.DepartmentDataSource = value;
                BindDepartemnts(value);

                ContactControl_primaryAddressCheckbox.Checked = true;
                ContactControl_clientNameDropDownList.SelectedValue = value.ClientID.ToString();
                ContactControl_clientNameDropDownList.Enabled = true;
                ViewState["CONTACT_INFORMATION"] = value.ContactInformation;
            }
        }



        public List<ClientInformation> ClientInformationListDatasource
        {
            set
            {
                ContactControl_clientNameDropDownList.Items.Clear();
                ListItem listItem = new ListItem();
                listItem.Text = "--Select--";
                listItem.Value = "0";
                ContactControl_clientNameDropDownList.Items.Add(listItem);
                if (value == null)
                    return;

                ContactControl_clientNameDropDownList.DataTextField = "ClientName";
                ContactControl_clientNameDropDownList.DataValueField = "ClientID";
                ContactControl_clientNameDropDownList.DataSource = value;
                ContactControl_clientNameDropDownList.DataBind();

            }
        }
        public Department DepartmentListDataSource
        {
            set
            {
                if (value == null)
                {
                    // ContactControl_descriptionTextBox.Text = string.Empty;
                    ContactControl_streetAddressTextBox.Text = string.Empty;
                    ContactControl_eMailIDTextBox.Text = string.Empty;
                    ContactControl_cityTextBox.Text = string.Empty;
                    ContactControl_countryTextBox.Text = string.Empty;
                    ContactControl_stateTextBox.Text = string.Empty;
                    ContactControl_zipCodeTextBox.Text = string.Empty;
                    //  ContactControl_departmentNameTextBox.Text = string.Empty;
                    ContactControl_faxNoTextBox.Text = string.Empty;
                    ContactControl_phoneNoTextBox.Text = string.Empty;
                    ContactControl_messagetitleLiteral.Text = "Add Contact";
                    ContactControl_createButton.CommandName = "add";
                    ContactControl_createButton.CommandArgument = "0";
                    BindDepartment(value);
                    //  ContactControl_departmentMultiSelectControl.SelectedProperties = "";
                    return;
                }
                //   ContactControl_descriptionTextBox.Text = value.DepartmentName;

                ContactControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                ContactControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                ContactControl_cityTextBox.Text = value.ContactInformation.City;
                ContactControl_countryTextBox.Text = value.ContactInformation.Country;
                ContactControl_stateTextBox.Text = value.ContactInformation.State;
                ContactControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                //   ContactControl_departmentNameTextBox.Text = value.DepartmentDescription;
                ContactControl_faxNoTextBox.Text = value.FaxNumber;
                ContactControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                ContactControl_messagetitleLiteral.Text = "Add Contact";
                ContactControl_createButton.CommandName = "add";
                ContactControl_createButton.CommandArgument = value.DepartmentID.ToString();
                ContactControl_clientIDHiddenField.Value = value.ClientID.ToString();
                ContactControl_clientNameDropDownList.SelectedValue = value.ClientID.ToString();
                ViewState["CONTACT_INFORMATION"] = value.ContactInformation;
                BindDepartment(value);
                // BindDepartemnts(value.DepartmentList);
                //   ContactControl_departmentMultiSelectControl.DepartmentListDataSource = value.DepartmentList;
                //  ContactControl_departmentMultiSelectControl.WidgetTypePropertyAllSelected(value.DepartmentIsSelected);
                //List<DropDownItem> departmentList = new List<DropDownItem>();
                //for (int i = 0; i < value.DepartmentList.Count; i++)
                //{
                //    departmentList.Add(new DropDownItem(value.DepartmentList[i].DepartmentName, value.DepartmentList[i].DepartmentID.ToString()));
                //}
                //  ContactControl_departmentMultiSelectControl.DepartmentyDataSource = value.DepartmentList;
            }
        }

        public NomenclatureCustomize ContactAttributesDataSource
        {
            set
            {
                if (value == null)
                {
                    Session[CLIENT_CONTACT_ATTRIBUTES_DATASOURCE] = null;
                    return;
                }

                Session[CLIENT_CONTACT_ATTRIBUTES_DATASOURCE] = value;
                ContactControl_firstNameLabel.Text = value.FirstName;

                ContactControl_clientNameLabel.Text = value.ClientName;
                HtmlTableCell TableCell = null;

                ContactControl_primaryAddressCheckbox.Visible = value.ContactsPrimaryAddress;
                if (value.AdditionalInfoIsVisible)
                {
                    ContactControl_additionalInfoLabel.Text = value.AdditionalInfo;
                    ContactControl_additionalInfoLabel.Visible = true;
                    ContactControl_additionalInfoTextBox.Visible = true;
                   // ContactControl_additionalInfoSpan.Visible = value.AdditionalInfoIsMandatory;
                }
                int i = 5;
                if (value.MiddlenameIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    ContactControl_middleNameLabel.Visible = true;
                    ContactControl_middleNameLabel.Text = value.Middlename;
                    TableCell.Controls.Add(ContactControl_middleNameLabel);
                    //if (value.MiddlenameIsMandatory)
                    //{
                    //    ContactControl_middleNameSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_middleNameSpan);
                    //}
                    i++;
                    ContactControl_middleNameTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_middleNameTextBox);
                    i++;
                }
                if (value.LastNameIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    ContactControl_lastNameLabel.Visible = true;
                    ContactControl_lastNameLabel.Text = value.LastName;
                    TableCell.Controls.Add(ContactControl_lastNameLabel);
                    //if (value.LastNameIsMandatory)
                    //{
                    //    ContactControl_lastNameSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_lastNameSpan);
                    //}
                    i++;
                    ContactControl_lastNameTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_lastNameTextBox);
                    i++;
                }
                if (value.StreetAddressIsVisible)
                {
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    ContactControl_streetAddressLabel.Visible = true;
                    ContactControl_streetAddressLabel.Text = value.StreetAddress;
                    TableCell.Controls.Add(ContactControl_streetAddressLabel);
                    //if (value.StreetAddressIsMandatory)
                    //{
                    //    ContactControl_streetAddressSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_streetAddressSpan);
                    //}
                    i++;
                    ContactControl_streetAddressTextBox.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_streetAddressTextBox);
                    i++;
                }
                if (value.CityIsVisible)
                {
                    ContactControl_cityLabel.Visible = true;
                    ContactControl_cityLabel.Text = value.City;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_cityLabel);
                    //if (value.CityIsMandatory)
                    //{
                    //    ContactControl_citySpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_citySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_cityTextBox);
                    i++;
                    ContactControl_cityTextBox.Visible = true;
                }
                if (value.StateIsVisible)
                {
                    ContactControl_stateLabel.Visible = true;
                    ContactControl_stateLabel.Text = value.state;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_stateLabel);
                    //if (value.StateIsMandatory)
                    //{
                    //    ContactControl_stateSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_stateSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_stateTextBox);
                    i++;
                    ContactControl_stateTextBox.Visible = true;
                }
                if (value.CountryIsVisible)
                {
                    ContactControl_countryLabel.Text = value.Country;
                    ContactControl_countryLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_countryLabel);
                    //if (value.CountryIsMandatory)
                    //{
                    //    ContactControl_countrySpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_countrySpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_countryTextBox);
                    i++;
                    ContactControl_countryTextBox.Visible = true;
                }
                if (value.ZipCodeIsVisible)
                {
                    ContactControl_zipCodeLabel.Text = value.Zipcode;
                    ContactControl_zipCodeLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_zipCodeLabel);

                    //if (value.ZipCodeIsMandatory)
                    //{
                    //    ContactControl_zipCodeSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_zipCodeSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_zipCodeTextBox);
                    i++;
                    ContactControl_zipCodeTextBox.Visible = true;
                }

                if (value.FaxNoIsVisible)
                {
                    ContactControl_faxNoLabel.Text = value.FaxNo;
                    ContactControl_faxNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_faxNoLabel);

                    //if (value.FaxNoIsMandatory)
                    //{
                    //    ContactControl_faxNoSpan.Visible = true;
                    //    TableCell.Controls.Add(ContactControl_faxNoSpan);
                    //}
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_faxNoTextBox);
                    i++;
                    ContactControl_faxNoTextBox.Visible = true;
                }

                if (value.EmailIDIsVisible)
                {
                    ContactControl_eMailIDLabel.Text = value.EmailID;
                    ContactControl_eMailIDLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_eMailIDLabel);

                    if (value.EmailIDFieldIsMandatory)
                    {
                        ContactControl_eMailIDSpan.Visible = true;
                        TableCell.Controls.Add(ContactControl_eMailIDSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_eMailIDTextBox);
                    i++;
                    ContactControl_eMailIDTextBox.Visible = true;
                }
                if (value.PhoneNumberIsVisible)
                {
                    ContactControl_phoneNoLabel.Text = value.PhoneNumber;
                    ContactControl_phoneNoLabel.Visible = true;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_phoneNoLabel);

                    if (value.PhoneNumberFieldIsMandatory)
                    {
                        ContactControl_phoneNoSpan.Visible = true;
                        TableCell.Controls.Add(ContactControl_phoneNoSpan);
                    }
                    i++;
                    TableCell = (HtmlTableCell)this.FindControl("ContactControl_td" + i);
                    TableCell.EnableViewState = true;
                    TableCell.Controls.Add(ContactControl_phoneNoTextBox);
                    i++;
                    ContactControl_phoneNoTextBox.Visible = true;
                }

            }
        }

        private bool IsValid()
        {
            if (Utility.IsNullOrEmpty(Session[CLIENT_CONTACT_ATTRIBUTES_DATASOURCE]))
                return false;

            StringBuilder requiredFieldMessage = new StringBuilder();
            string validEmail = "";

            ClientContactInformation clientContactInformation = new ClientContactInformation();
            clientContactInformation = clientContactInformationDataSource;
            NomenclatureCustomize clinetContactAttributes = new NomenclatureCustomize();
            bool isvalid = true;
            clinetContactAttributes = (NomenclatureCustomize)Session[CLIENT_CONTACT_ATTRIBUTES_DATASOURCE];

            if (clientContactInformation.FirstName.Length == 0)
            {
                requiredFieldMessage.Append(clinetContactAttributes.FirstName);
                isvalid = false;
            }
            //if (clientContactInformation.LastName.Length == 0)
            //{
            //    requiredFieldMessage.Append(clinetContactAttributes.LastName);
            //    isvalid = false;
            //}

            //if (clinetContactAttributes.StreetAddressIsVisible && clinetContactAttributes.StreetAddressIsMandatory
            //    && clientContactInformation.ContactInformation.StreetAddress.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clinetContactAttributes.StreetAddress);
            //    isvalid = false;
            //}
            //if (clinetContactAttributes.CityIsVisible && clinetContactAttributes.CityIsMandatory
            //    && clientContactInformation.ContactInformation.City.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clinetContactAttributes.City);
            //    isvalid = false;
            //}

            //if (clinetContactAttributes.StateIsVisible && clinetContactAttributes.StateIsMandatory
            //    && clientContactInformation.ContactInformation.State.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clinetContactAttributes.state);
            //    isvalid = false;
            //}
            //if (clinetContactAttributes.ZipCodeIsVisible && clinetContactAttributes.ZipCodeIsMandatory
            //    && clientContactInformation.ContactInformation.PostalCode.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clinetContactAttributes.Zipcode);
            //    isvalid = false;
            //}
            //if (clinetContactAttributes.CountryIsVisible && clinetContactAttributes.CountryIsMandatory
            //    && clientContactInformation.ContactInformation.Country.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clinetContactAttributes.Country);
            //    isvalid = false;
            //}

            if (clinetContactAttributes.PhoneNumberIsVisible && clinetContactAttributes.PhoneNumberFieldIsMandatory
                && clientContactInformation.ContactInformation.Phone.Mobile.Length == 0)
            {
                if (requiredFieldMessage.Length > 0)
                    requiredFieldMessage.Append(", ");
                requiredFieldMessage.Append(clinetContactAttributes.PhoneNumber);
                isvalid = false;
            }

            if (clinetContactAttributes.EmailIDIsVisible && clinetContactAttributes.EmailIDFieldIsMandatory)
            {
                if (clientContactInformation.ContactInformation.EmailAddress.Length == 0)
                {
                    if (requiredFieldMessage.Length > 0)
                        requiredFieldMessage.Append(", ");
                    requiredFieldMessage.Append(clinetContactAttributes.EmailID);
                    isvalid = false;
                }
                else if (!Utility.IsValidEmailAddress(clientContactInformation.ContactInformation.EmailAddress))
                {
                    validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                    isvalid = false;
                }
            }
            else if (clientContactInformation.ContactInformation.EmailAddress.Length > 0 && !Utility.IsValidEmailAddress(clientContactInformation.ContactInformation.EmailAddress))
            {
                validEmail = HCMResource.UserRegistration_InvalidEmailAddress;
                isvalid = false;
            }

            //if (clinetContactAttributes.AdditionalInfoIsVisible && clinetContactAttributes.AdditionalInfoIsMandatory
            //    && clientContactInformation.AdditionalInfo.Length == 0)
            //{
            //    if (requiredFieldMessage.Length > 0)
            //        requiredFieldMessage.Append(", ");
            //    requiredFieldMessage.Append(clinetContactAttributes.AdditionalInfo);
            //    isvalid = false;
            //}
            if (Utility.IsNullOrEmpty(ContactControl_clientIDHiddenField.Value) || ContactControl_clientIDHiddenField.Value == "0")
            {
                requiredFieldMessage.Append(clinetContactAttributes.AdditionalInfo);
                isvalid = false;
            }
            if (!isvalid)
            {

                // ShowErrorMessage("The following field(s) are mandatory and require to be filled:"+message.ToString());
                if (requiredFieldMessage.Length > 0)
                    ShowErrorMessage("Mandatory fields cannot be empty");
                if (validEmail.Length > 0)
                    ShowErrorMessage(validEmail);
                SelectedClient();
                InvokePageBaseMethod("ClientManagement_contact_ModalpPopupExtender1", null);
                //StringBuilder message = new StringBuilder();
                //bool isvalid = true;
                //Department departmentInformation = new Department();
                //departmentInformation = DepartmentDataSource;
                //if (departmentInformation.DepartmentName.Length == 0)
                //{
                //    message.Append("Department name");
                //    isvalid=false;
                //}

                //if (!isvalid)
                //{
                //    ShowErrorMessage(message.ToString() + " should not empty.");
                //    InvokePageBaseMethod("ClientManagement_department_ModalpPopupExtender", null);
                //}
            }
            return isvalid;
        }

        private void ShowErrorMessage(string exp)
        {
            if (string.IsNullOrEmpty(ContactControl_topErrorMessageLabel.Text))
                ContactControl_topErrorMessageLabel.Text = exp;
            else
                ContactControl_topErrorMessageLabel.Text += "<br />" + exp;
        }

        public string ShowMessage
        {
            set
            {
                ShowErrorMessage(value);
            }
        }

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <param name="args">The args.</param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        protected void ContactControl_createButton_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (!IsValid())
                return;
            if (OkCommandClick != null)
                this.OkCommandClick(sender, e);

        }

        protected void ContactControl_primaryAddressCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (!ContactControl_primaryAddressCheckbox.Checked)
            {
                ContactControl_streetAddressTextBox.Text = string.Empty;
                ContactControl_cityTextBox.Text = string.Empty;
                ContactControl_countryTextBox.Text = string.Empty;
                ContactControl_stateTextBox.Text = string.Empty;
                ContactControl_zipCodeTextBox.Text = string.Empty;

            }
            else if (!Utility.IsNullOrEmpty(ViewState["CONTACT_INFORMATION"]))
            {

                ContactInformation contactInformation = new ContactInformation();
                contactInformation = ViewState["CONTACT_INFORMATION"] as ContactInformation;
                ContactControl_streetAddressTextBox.Text = contactInformation.StreetAddress;
                ContactControl_cityTextBox.Text = contactInformation.City;
                ContactControl_countryTextBox.Text = contactInformation.Country;
                ContactControl_stateTextBox.Text = contactInformation.State;
                ContactControl_zipCodeTextBox.Text = contactInformation.PostalCode;
            }
            InvokePageBaseMethod("ClientManagement_contact_ModalpPopupExtender1", null);
        }

        private void SelectedClient()
        {
            if (!Utility.IsNullOrEmpty(ContactControl_clientIDHiddenField.Value))
            {
                ContactControl_clientNameDropDownList.SelectedValue = ContactControl_clientIDHiddenField.Value.ToString();
                if (ContactControl_clientIDHiddenField.Value == "0")
                    ContactControl_primaryAddressCheckbox.Checked = false;
                else
                    ContactControl_primaryAddressCheckbox.Checked = true;
            }
        }

        public ClientContactInformation clientContactInformationDataSource
        {
            set
            {
                if (value == null)
                {
                    ContactControl_firstNameTextBox.Text = string.Empty;
                    ContactControl_middleNameTextBox.Text = string.Empty;
                    ContactControl_lastNameTextBox.Text = string.Empty;
                    ContactControl_additionalInfoTextBox.Text = string.Empty;
                    ContactControl_streetAddressTextBox.Text = string.Empty;
                    ContactControl_eMailIDTextBox.Text = string.Empty;
                    ContactControl_cityTextBox.Text = string.Empty;
                    ContactControl_countryTextBox.Text = string.Empty;
                    ContactControl_stateTextBox.Text = string.Empty;
                    ContactControl_zipCodeTextBox.Text = string.Empty;
                    ContactControl_faxNoTextBox.Text = string.Empty;
                    ContactControl_phoneNoTextBox.Text = string.Empty;
                    ContactControl_messagetitleLiteral.Text = "Add Contact";
                    ContactControl_createButton.CommandName = "add";
                    ContactControl_createButton.CommandArgument = "0";
                    ContactControl_primaryAddressCheckbox.Visible = true;
                    ContactControl_clientNameTR.Visible = true;
                    BindSelectedDepartment(value);
                    return;
                }
                ContactControl_firstNameTextBox.Text = value.FirstName;
                ContactControl_middleNameTextBox.Text = value.MiddleName;
                ContactControl_lastNameTextBox.Text = value.LastName;
                ContactControl_additionalInfoTextBox.Text = value.AdditionalInfo;
                ContactControl_streetAddressTextBox.Text = value.ContactInformation.StreetAddress;
                ContactControl_eMailIDTextBox.Text = value.ContactInformation.EmailAddress;
                ContactControl_cityTextBox.Text = value.ContactInformation.City;
                ContactControl_countryTextBox.Text = value.ContactInformation.Country;
                ContactControl_stateTextBox.Text = value.ContactInformation.State;
                ContactControl_zipCodeTextBox.Text = value.ContactInformation.PostalCode;
                ContactControl_faxNoTextBox.Text = value.FaxNo;
                ContactControl_phoneNoTextBox.Text = value.ContactInformation.Phone.Mobile;
                ContactControl_messagetitleLiteral.Text = "Edit Contact";
                ContactControl_createButton.CommandName = "edit";
                ContactControl_createButton.CommandArgument = value.ContactID.ToString();
                ContactControl_primaryAddressCheckbox.Visible = false;
                ContactControl_clientNameTR.Visible = false;
                // ContactControl_departmentMultiSelectControl.DepartmentListDataSource = value.Departments;
                //  ContactControl_departmentMultiSelectControl.SelectedItems();
                BindSelectedDepartment(value);
                ViewState["CONTACT_INFORMATION"] = value.ContactInformation;
                ContactControl_clientIDHiddenField.Value = value.ClientID.ToString();

            }
            get
            {
                ClientContactInformation clientContactInformation = new ClientContactInformation();

                clientContactInformation.ClientID = int.Parse(ContactControl_clientIDHiddenField.Value);
                //int.Parse(ContactControl_clientNameDropDownList.SelectedValue);
                clientContactInformation.FirstName = ContactControl_firstNameTextBox.Text.Trim();
                clientContactInformation.MiddleName = ContactControl_middleNameTextBox.Text.Trim();
                clientContactInformation.LastName = ContactControl_lastNameTextBox.Text.Trim();
                clientContactInformation.AdditionalInfo = ContactControl_additionalInfoTextBox.Text.Trim();
                //clientContactInformation.ClientID = int.Parse(ContactControl_clientIDHiddenField.Value); 
                ContactInformation contInfo = new ContactInformation();

                contInfo.StreetAddress = ContactControl_streetAddressTextBox.Text.Trim();
                contInfo.EmailAddress = ContactControl_eMailIDTextBox.Text.Trim();
                contInfo.City = ContactControl_cityTextBox.Text.Trim();
                contInfo.Country = ContactControl_countryTextBox.Text.Trim();
                contInfo.State = ContactControl_stateTextBox.Text.Trim();
                contInfo.PostalCode = ContactControl_zipCodeTextBox.Text.Trim();
                clientContactInformation.FaxNo = ContactControl_faxNoTextBox.Text.Trim();
                PhoneNumber phone = new PhoneNumber();
                phone.Mobile = ContactControl_phoneNoTextBox.Text.Trim();
                contInfo.Phone = phone;
                clientContactInformation.ContactInformation = contInfo;
                StringBuilder selectedDepartemnts = new StringBuilder();
                for (int i = 0; i < ContactControl_departmentCheckboxList.Items.Count; i++)
                {
                    if (ContactControl_departmentCheckboxList.Items[i].Selected)
                    {
                        selectedDepartemnts.Append(ContactControl_departmentCheckboxList.Items[i].Value);
                        selectedDepartemnts.Append(",");
                    }
                }
                // clientContactInformation.SellectedDeparments = ContactControl_departmentMultiSelectControl.SelledctedIds;
                //  ContactControl_departmentMultiSelectControl.SelectedItems();
                clientContactInformation.SellectedDeparments = selectedDepartemnts.ToString().TrimEnd(',');
                return clientContactInformation;

            }
        }

        private void BindSelectedDepartment(ClientContactInformation value)
        {
            ContactControl_departmentCheckboxList.Items.Clear();
            if (value != null && value.Departments != null)
            {

                ContactControl_departmentCheckboxList.DataSource = value.Departments;
                ContactControl_departmentCheckboxList.DataValueField = "DepartmentID";
                ContactControl_departmentCheckboxList.DataTextField = "DepartmentName";
                ContactControl_departmentCheckboxList.DataBind();

                for (int i = 0; i < value.Departments.Count; i++)
                {
                    if (value.Departments[i].DepartmentIsSelected == "Y")
                    {
                        ContactControl_departmentCheckboxList.Items[i].Selected = true;
                    }
                }
            }
        }

        private void BindDepartemnts(ClientInformation value)
        {
            ContactControl_departmentCheckboxList.Items.Clear();
            if (value != null && value.Departments != null)
            {
                ContactControl_departmentCheckboxList.DataSource = value.Departments;
                ContactControl_departmentCheckboxList.DataValueField = "DepartmentID";
                ContactControl_departmentCheckboxList.DataTextField = "DepartmentName";
                ContactControl_departmentCheckboxList.DataBind();

            }
        }

        private void BindDepartment(Department value)
        {
            ContactControl_departmentCheckboxList.Items.Clear();
            if (value != null && value != null)
            {
                ContactControl_departmentCheckboxList.DataSource = value.DepartmentList;
                ContactControl_departmentCheckboxList.DataValueField = "DepartmentID";
                ContactControl_departmentCheckboxList.DataTextField = "DepartmentName";
                ContactControl_departmentCheckboxList.DataBind();
                ContactControl_departmentCheckboxList.SelectedValue = value.DepartmentIsSelected;

            }
        }

        public string BindSaveButtonCommand
        {
            set
            {
                ContactControl_createButton.CommandName = value;
                ContactControl_clientNameDropDownList.Enabled = false;
            }
        }
    }
}