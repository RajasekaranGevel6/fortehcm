﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;
using System.Web.Security;

namespace Forte.HCM.UI.CommonControls
{
    public partial class SubscriptionHeaderControl : UserControl
    {
        public string homeUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            // To get the host from the url.
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if (uri.Length > 3)
                homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Default.aspx";

            try
            {
                if (Session["USER_DETAIL"] == null)
                    SubscriptionSubscriptionHeaderControl_loggedInTable.Visible = false;
                else
                {
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                    SubscriptionHeaderControl_loginUserNameLabel.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                    SubscriptionHeaderControl_lastLoginDateTimeLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
                    SubscriptionSubscriptionHeaderControl_loggedInTable.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void SubscriptionHeaderControl_logoutButton_Click(object sender, EventArgs e)
        {
            Session["USER_DETAIL"] = null;
            Session.Abandon();
            SubscriptionHeaderControl_loginUserNameLabel.Text = "Guest";
            SubscriptionHeaderControl_lastLoginDateTimeLabel.Text = "";
            FormsAuthentication.SignOut();
            //Response.Redirect("~/Home.aspx?a=1", false);
            // FormsAuthentication.RedirectToLoginPage();
            //  Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["SITE_URL"], false);
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if ( uri.Length > 4)
            {
                // Compose home url.

                Response.Redirect(uri[0] + "/" + uri[1] + System.Configuration.ConfigurationManager.AppSettings["SITE_URL"], false);
            }
        }

        public List<string> BreadCrumbText
        {
            set
            {
                List<string> breadCrumbList = (List<string>)value;

                if (breadCrumbList != null)
                {
                    foreach (string breadCrumbText in breadCrumbList)
                    {
                    }
                }
            }
        }
    }
}