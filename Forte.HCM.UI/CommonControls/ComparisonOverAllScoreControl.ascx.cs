﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ComparisonOverAllScoreControl.cs
// File that represents the user interface for the overall score chart details
// for the comparison report

#endregion Header
 
#region Directives                                                             

using System;
using System.Data;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using ReflectionComboItem;
using Forte.HCM.Trace;
using System.Text;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ComparisonOverAllScoreControl : UserControl, IWidgetControl
    {
        #region Event Handlers   
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                ComparisonOverAllScoreControl_absoluteScoreRadioButton.Checked = selectedProperty[1] == "0" ? false : true;
                ComparisonOverAllScoreControl_relativeScoreRadioButton.Checked = selectedProperty[1] == "1" ? false : true;
                ComparisonOverAllScoreControl_hiddenField.Value = selectedProperty[0];
                LoadChartDetails(ViewState["instance"] as WidgetInstance);
            }
        }                              
        
        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called on radio button click
        /// link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonOverAllScoreControl_selectOptionsRadioButton_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                WidgetInstance instance = ViewState["instance"] as WidgetInstance;

                LoadChartDetails(instance);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Hanlder method that will be called on the show 
        /// comparative score link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ComparisonOverAllScoreControl_displayLinkButton_Click
            (object sender, EventArgs e)
        {
            WidgetInstance instance = ViewState["instance"] as WidgetInstance;

            LoadChartDetails(instance);
        }
        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that used to load the chart details
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the 
        /// widget
        /// </param>
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;

            //Get the score details from the data base
            OverAllCandidateScoreDetail scoreDetail = new ReportBLManager().
                GetComparisonOverallScoreDetails(candidateReportDetail);

            //Assign the score details in the session
            Session["SCOREDETAIL"] = scoreDetail;

            //Load the chart with the details
            LoadChart(instance);
        }

        /// <summary>
        /// Represents the method that used to display the 
        /// chart details
        /// </summary>
        /// <param name="scoreDetail">
        /// A<see cref="OverAllCandidateScoreDetail"/>that holds the
        /// overall candidate Score details
        /// </param>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the
        /// widget
        /// </param>
        private void LoadChart(WidgetInstance instance)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            CandidateReportDetail candidateReportDetail = null;

            OverAllCandidateScoreDetail scoreDetail = null;

            if (Session["SCOREDETAIL"] != null)
            {
                scoreDetail = Session["SCOREDETAIL"] as OverAllCandidateScoreDetail;
            }
            if (Session["CANDIDATEDETAIL"] != null)
            {
                candidateReportDetail =
                      Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            }

            if (scoreDetail == null)
                return;

            scoreDetail.ChartScoreDetails = new SingleChartData();

            //Assign the chart properties 
            scoreDetail.ChartScoreDetails.ChartWidth = 325;

            scoreDetail.ChartScoreDetails.ChartType = SeriesChartType.Column;

            scoreDetail.ChartScoreDetails.IsDisplayChartTitle = true;

            scoreDetail.ChartScoreDetails.ChartTitle = "Total Questions Authored";

            scoreDetail.ChartScoreDetails.IsDisplayAxisTitle = true;

            scoreDetail.ChartScoreDetails.XAxisTitle = "Values";

            scoreDetail.ChartScoreDetails.YAxisTitle = "Scores";

            scoreDetail.ChartScoreDetails.IsDisplayLegend = false;

            scoreDetail.ChartScoreDetails.IsDisplayChartTitle = false;

            scoreDetail.ChartScoreDetails.ChartTitle = "Candidate's Overall Test Score Detail";

            scoreDetail.ChartScoreDetails.IsShowLabel = true;
            List<DropDownItem> dropDownItemList = new List<DropDownItem>();
            if (ComparisonOverAllScoreControl_hiddenField.Value == "1")
            {
                selectedPrintProperty.Append("1|");
                ComparisonOverAllScoreControl_DisplayLinkButton.Text = "Hide Comparative Score";

                //If absolute score radion button is clicked show absolute score
                if (ComparisonOverAllScoreControl_absoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    scoreDetail.ChartScoreDetails.IsComparisonReport = true;
                    scoreDetail.ChartScoreDetails.PointColor = ColorTranslator.FromHtml("#BA55D3");
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.AbsoluteScoreDetails;
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.COMPARISON_REPORT_OVERALL_COMPARATIVE_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-');
                    dropDownItemList.Add(new DropDownItem("Title", "Absolute Comparative Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.COMPARISON_REPORT_OVERALL_COMPARATIVE_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-') + ".png"));
                }
                //else show relative score
                else
                {
                    selectedPrintProperty.Append("0|");
                    scoreDetail.ChartScoreDetails.IsComparisonReport = true;
                    scoreDetail.ChartScoreDetails.PointColor = ColorTranslator.FromHtml("#BA55D3");
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.RelativeScoreDetails;
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.COMPARISON_REPORT_OVERALL_COMPARATIVE_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-');
                    dropDownItemList.Add(new DropDownItem("Title", "Relative Comparative Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.COMPARISON_REPORT_OVERALL_COMPARATIVE_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-') + ".png"));
                }
            }
            else
            {
                selectedPrintProperty.Append("0|");
                ComparisonOverAllScoreControl_DisplayLinkButton.Text = "Show Comparative Score";
                if (ComparisonOverAllScoreControl_absoluteScoreRadioButton.Checked)
                {
                    //if the selection is hide comparitive absolute score 
                    //then hide the first three scores in the chart
                    scoreDetail.ChartScoreDetails.IsComparisonReport = false;
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.AbsoluteScoreDetails;
                    scoreDetail.ChartScoreDetails.ChartData.RemoveRange(scoreDetail.ChartScoreDetails.
                    ChartData.Count - 3, 3);
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.COMPARISON_REPORT_OVERALL_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-');
                    dropDownItemList.Add(new DropDownItem("Title", "Absolute Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.COMPARISON_REPORT_OVERALL_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-') + ".png"));
                }
                else
                {
                    //if the selection is hide comparitive relative score 
                    //then hide the first three scores in the chart
                    scoreDetail.ChartScoreDetails.IsComparisonReport = false;
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.RelativeScoreDetails;
                    scoreDetail.ChartScoreDetails.ChartData.RemoveRange(scoreDetail.ChartScoreDetails.
                    ChartData.Count - 3, 3);
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.COMPARISON_REPORT_OVERALL_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-');
                    dropDownItemList.Add(new DropDownItem("Title", "Relative Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.COMPARISON_REPORT_OVERALL_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey.Replace(',', '-') + "-" + candidateReportDetail.AttemptsID.Replace(',', '-') + ".png"));
                }
            }

            //Finally assign the datasource for the chart control
            ComparisonOverAllScoreChartControl_scoreChartReportChartControl.DataSource =
                scoreDetail.ChartScoreDetails;
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }

        #endregion Private Methods

        #region IWidgetControl Members                                         

        public void Bind(WidgetInstance instance)
        {
            //Keep the instance key in view state 
            ViewState["instance"] = instance.InstanceKey;

            //Load the chart details 
            LoadChartDetails(instance);

            ComparisonOverAllScoreControl_DisplayLinkButton.Attributes.Add
              ("onclick", "javascript:return ShowOrHideComparativeScore('"
              + ComparisonOverAllScoreControl_DisplayLinkButton.ClientID + "','"
              + ComparisonOverAllScoreControl_hiddenField.ClientID + "');");
        }

        public UpdatePanel[] Command(WidgetInstance instance, WidgetCommandInfo commandData,
            ref UpdateMode updateMode)
        {
            return null;
        }

        public void InitControl(WidgetInitParameters parameters)
        {

        }

        #endregion
    }
}