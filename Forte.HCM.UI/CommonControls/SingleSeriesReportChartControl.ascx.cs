﻿#region Directives                                                             
using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class SingleSeriesReportChartControl : UserControl
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that is called when the page is loaded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region Properties                                                     
        /// <summary>
        /// Property used to assign the 
        /// datasource of the chart control
        /// </summary>
        public SingleChartData DataSource
        {
            set
            {
                //Set the chart type of the control
                SingleSeriesReportChartControl_chart.
                    Series["SingleSeriesReportChartControl_series"].ChartType = value.ChartType;

                //Set the height of the control 
                SingleSeriesReportChartControl_chart.Height = Unit.Pixel(value.ChartLength);

                //Set the width of the control
                SingleSeriesReportChartControl_chart.Width = Unit.Pixel(value.ChartWidth);

                //Assign the datasource of the chart
                SingleSeriesReportChartControl_chart.DataSource = value.ChartData;

                //Defines whether the chart title has to display or not
                SingleSeriesReportChartControl_chart.Titles[0].Visible =
                    value.IsDisplayChartTitle;

                //Assign the chart title to the chart
                if (value.IsDisplayChartTitle)
                {
                    SingleSeriesReportChartControl_chart.
                        Titles["SingleSeriesReportChartControl_title"].Text = value.ChartTitle;
                }

                //Defines whether the axis title has to display or not
                if (value.IsDisplayAxisTitle)
                {
                    SingleSeriesReportChartControl_chart.ChartAreas
                        ["SingleSeriesReportChartControl_chartArea"].AxisX.Title = value.XAxisTitle;

                    SingleSeriesReportChartControl_chart.ChartAreas
                        ["SingleSeriesReportChartControl_chartArea"].AxisY.Title = value.YAxisTitle;
                }

                //Assign the datasource for the chart control
                SingleSeriesReportChartControl_chart.Series
                    ["SingleSeriesReportChartControl_series"].Points.DataBindXY
                    (value.ChartData, "ReportChartXValue", value.ChartData, "ReportChartYValue");

                //Defines whether has the legend has to display or not         
                if (!value.IsDisplayLegend)
                {
                    //If the legend is not displayed then changed the chart area width to 100
                    SingleSeriesReportChartControl_chart.ChartAreas
                        ["SingleSeriesReportChartControl_chartArea"].Position.Width = 100;

                    //change the x axis label  angle to - 90 for vertical alignment of label
                    SingleSeriesReportChartControl_chart.ChartAreas
                       ["SingleSeriesReportChartControl_chartArea"].AxisX.LabelStyle.Angle = 0;
                }

                //Assign the tool tip for the chart series point
                SingleSeriesReportChartControl_chart.Series
                    ["SingleSeriesReportChartControl_series"].ToolTip = "#VAL";

                //Defines whether the value has to be shown as label in chart
                SingleSeriesReportChartControl_chart.Series
                    ["SingleSeriesReportChartControl_series"].IsValueShownAsLabel = value.IsShowLabel;

                //Assign the value in the point in the series
                if (value.IsShowLabel)
                {
                    SingleSeriesReportChartControl_chart.
                        Series["SingleSeriesReportChartControl_series"]["PieLabelStyle"] = "Inside";
                    SingleSeriesReportChartControl_chart.
                        Series["SingleSeriesReportChartControl_series"].LabelAngle = -90;
                    //SingleSeriesReportChartControl_chart.
                    //    Series["SingleSeriesReportChartControl_series"].CustomProperties = "LabelStyle = Center";
                    SingleSeriesReportChartControl_chart.
                        Series["SingleSeriesReportChartControl_series"].SmartLabelStyle.Enabled = false;
                    
                }
                if ((value.ChartType == SeriesChartType.Pie) && (!value.IsShowLabel))
                {
                    //Disabled the values in the chart 
                    SingleSeriesReportChartControl_chart.
                       Series["SingleSeriesReportChartControl_series"]["PieLabelStyle"] = "Disabled";
                }
                //to change the series color
                if (value.IsChangeSeriesColor)
                {
                    SingleSeriesReportChartControl_chart.Palette = value.PaletteName;
                }
                //to change the point color
                if (value.IsChangePointsColor)
                {
                    SingleSeriesReportChartControl_chart.Series[0].Points
                        [value.PointNumber].Color = value.PointColor;
                }
                if (value.IsComparisonReport)
                {
                    //If the report is the comparison report , then need to change color of the points
                    int length = SingleSeriesReportChartControl_chart.Series[0].Points.Count;

                    SingleSeriesReportChartControl_chart.Series[0].Points[length - 1].Color = value.PointColor;

                    SingleSeriesReportChartControl_chart.Series[0].Points[length - 2].Color = value.PointColor;
                    SingleSeriesReportChartControl_chart.Series[0].Points[length - 3].Color = value.PointColor;
                }
                if (!Utility.IsNullOrEmpty(value.ChartImageName))
                {
                    //if (!new FileInfo(Server.MapPath("../chart/") + value.ChartImageName + ".png").Exists)
                        SingleSeriesReportChartControl_chart.SaveImage(Server.MapPath("../chart/") + value.ChartImageName + ".png", ChartImageFormat.Png);
                }
                

                //Get the new session ID
                string sessionId = System.Guid.NewGuid().ToString();

                //Assign the session ID to the session 
                Session[sessionId] = value;


                //Display only 15 characters in the graph x axis. 
                foreach (DataPoint point in SingleSeriesReportChartControl_chart.Series[0].Points)
                {
                    if (point.AxisLabel.Trim().Length > 15)
                    {
                        point.AxisLabel = point.AxisLabel.Substring(0, 15);
                    }
                    else
                    {
                        point.AxisLabel = point.AxisLabel.ToString();
                    }
                }

                //Assign the java script to show the zoomed chart for the td 
                SingleSeriesReportChartControl_td.Attributes.Add("onclick",
                    "javascript:return ShowZoomedChart('" + sessionId + "','SingleSeriesReport');");
            }

        }
        #endregion
    }
}