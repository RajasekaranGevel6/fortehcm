﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionDetailPreviewControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.QuestionDetailPreviewControl" %>
<asp:Panel ID="Panel1" runat="server" DefaultButton="QuestionDetailPreviewControl_topCancelImageButton">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="QuestionDetailPreviewControl_questionResultLiteral" runat="server"
                                Text=""></asp:Literal>
                        </td>
                        <td style="width: 50%" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                                            SkinID="sknCloseImageButton" OnClick="QuestionDetailPreviewControl_topCancelImageButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="left" class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <div style="height: 120px; overflow: auto;">
                                            <asp:DataGrid ID="QuestionDetailPreviewControl_categoryDataGrid" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:BoundColumn HeaderText="Category" DataField="CategoryName"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Subject" DataField="SubjectName"></asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_panel_inner_bg">
                                        <table width="100%" cellpadding="0" cellspacing="2" border="0">
                                            <tr>
                                                <td style="width: 12%">
                                                    <asp:Label ID="QuestionDetailPreviewControl_testAreaHeadLabel" runat="server" Text="Test Area"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:Label ID="QuestionDetailPreviewControl_testAreaValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td style="width: 14%">
                                                    <asp:Label ID="QuestionDetailPreviewControl_complexityHeadLabel" runat="server" Text="Complexity"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:Label ID="QuestionDetailPreviewControl_complexityValueLabel" runat="server"
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:Label ID="QuestionDetailPreviewControl_tagHeadLabel" runat="server" Text="Tag"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 37%">
                                                    <asp:Label ID="QuestionDetailPreviewControl_tagValueLabel" class="label_multi_field_text"
                                                        runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_question_icon">
                                        <div style="overflow: auto;word-wrap: break-word;white-space:normal" id="QuestionDetailPreviewControl_questionDiv" runat="server">
                                            <asp:Label ID="QuestionDetailPreviewControl_questionLabel" runat="server" class="label_multi_field_text"></asp:Label><br />
                                            <asp:Image runat="server" ID="QuestionDetailPreviewControl_questionImage"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_padding_left_20">
                                        <div style="height: 80px; overflow: auto;">
                                            <asp:PlaceHolder ID="QuestionDetailPreviewControl_answerChoicesPlaceHolder" runat="server">
                                            </asp:PlaceHolder>
                                            <asp:Panel ID="testPanel" runat="server">
                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_2">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:Button ID="QuestionDetailPreviewControl_bottomAddButton" runat="server" Text="Add"
                    SkinID="sknButtonId" OnClick="QuestionDetailPreviewControl_bottomAddButton_Click"
                    Visible="false" />
                <asp:LinkButton ID="QuestionDetailPreviewControl_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClick="QuestionDetailPreviewControl_bottomCloseLinkButton_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
