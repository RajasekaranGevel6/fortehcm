﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OverviewHeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.OverviewHeaderControl" %>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left" style="width: 15%; vertical-align: middle; padding-top: 5px">
                        <p onclick="location.href='<%= homeUrl %>'">
                            <asp:Image ID="OverviewHeaderControl_logoImage" runat="server" SkinID="sknHomePageLogoImage" />
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
