﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OverallScoresChartControl.cs
// File that represents the user interface for the over all score chart
// details

#endregion Header                                                              

#region Directives                                                             
using System;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using ReflectionComboItem;
using System.Collections.Generic;
using System.Text;
#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities
    /// for Over all candidate score details . This page helps to view
    /// the candidate score details for a corresponding test 
    /// </summary>
    public partial class OverallScoresChartControl : UserControl, IWidgetControl
    {
        #region Event Handler                                                  

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Session["PRINTER"])
            {
                string[] selectedProperty = WidgetMultiSelectControlPrint.SelectedProperties.Split('|');
                OverallScoresChartControl_selectAbsoluteScoreRadioButton.Checked = selectedProperty[1] == "0" ? false : true;
                OverallScoresChartControl_selectRelativeScoreRadioButton.Checked = selectedProperty[1] == "1" ? false : true;
                OverAllScoresChartControl_hiddenfield.Value = selectedProperty[0];
                LoadChartDetails(ViewState["instance"] as WidgetInstance);
            }
        }

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Hanlder method that will be called when show comparitive link button 
        /// is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OverallScoresChartControl_selectDisplayLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the instance from the view state
                WidgetInstance instance = DashboardFramework.GetWidgetInstance
                       (ViewState["instance"]);

                //Get the chart details from the session 
                OverAllCandidateScoreDetail candidateScoreDetail = new OverAllCandidateScoreDetail();
                if (!Utility.IsNullOrEmpty(Session["CANDIDATESCOREDETAIL"]))
                    candidateScoreDetail = Session["CANDIDATESCOREDETAIL"] as OverAllCandidateScoreDetail;
                
                //Load the chart with the session details
              //  LoadChart(candidateScoreDetail,"", instance);
                LoadChartDetails(instance);
            }
            catch (Exception exception)
            {                
                throw exception;
            }
        }
        /// <summary>
        /// Hanlder method that will be called when the option button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void OverallScoresChartControl_selectOptionsAbsoluteScore_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                //Get the chart details from the session 
                OverAllCandidateScoreDetail candidateScoreDetail = new OverAllCandidateScoreDetail();
                if (!Utility.IsNullOrEmpty(Session["CANDIDATESCOREDETAIL"]))
                    candidateScoreDetail = Session["CANDIDATESCOREDETAIL"] as OverAllCandidateScoreDetail;
                
                //Get the instance from the view state
                WidgetInstance instance = DashboardFramework.GetWidgetInstance
                       (ViewState["instance"]);

                //Load the chart with the instance
                LoadChartDetails(instance);
            }
            catch (Exception exception)
            {                
                throw exception;
            }
        }
        #endregion Event Handler

        #region Private Methods                                                

        /// <summary>
        /// Method used to get the chart details from the 
        /// database and store the details in the session 
        /// </summary>                            
        private void LoadChartDetails(WidgetInstance instance)
        {
            //Get the candidate details from the session 
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            if (!Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                candidateReportDetail = Session["CANDIDATEDETAIL"] as CandidateReportDetail;
            
            //Get the score details from the data base
            OverAllCandidateScoreDetail scoreDetail = new ReportBLManager().
                GetOverallScoreDetails(candidateReportDetail);

            //Save the score details in the session 
            Session["CANDIDATESCOREDETAIL"] = scoreDetail;

            //Load the chart with the details
            LoadChart(scoreDetail,candidateReportDetail, instance);

        }

        /// <summary>
        /// Method used to bind the details to the chart
        /// </summary>
        /// <param name="scoreDetail">
        /// A<see cref="OverAllCandidateScoreDetail"/>that holds the 
        /// candidate and overall score details
        /// </param>
        private void LoadChart(OverAllCandidateScoreDetail scoreDetail,
            CandidateReportDetail candidateReportDetail, WidgetInstance instance)
        {
            StringBuilder selectedPrintProperty = new StringBuilder();
            scoreDetail.ChartScoreDetails = new SingleChartData();

            //Assign the chart properties 
            scoreDetail.ChartScoreDetails.ChartWidth = 325;

            scoreDetail.ChartScoreDetails.ChartType = SeriesChartType.Column;

            scoreDetail.ChartScoreDetails.IsDisplayChartTitle = true;

            scoreDetail.ChartScoreDetails.ChartTitle = "Total Questions Authored";

            scoreDetail.ChartScoreDetails.IsDisplayAxisTitle = true;

            scoreDetail.ChartScoreDetails.XAxisTitle = "Values";

            scoreDetail.ChartScoreDetails.YAxisTitle = "Scores";

            scoreDetail.ChartScoreDetails.IsDisplayLegend = false;

            scoreDetail.ChartScoreDetails.IsDisplayChartTitle = false;

            scoreDetail.ChartScoreDetails.ChartTitle = "Candidate's Overall Test Score Detail";

            scoreDetail.ChartScoreDetails.IsShowLabel = true;

            List<DropDownItem> dropDownItemList = new List<DropDownItem>();

            //Check the hidden field
            if (OverAllScoresChartControl_hiddenfield.Value == "1")
            {
                selectedPrintProperty.Append("1|");
                //If the hidden field is 1 show comparitive score
                OverallScoresChartControl_selectDisplayLinkButton.Text = "Hide Comparative Score";

                //If absolute score radion button is clicked show absolute score
                if (OverallScoresChartControl_selectAbsoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.AbsoluteScoreDetails;
                    scoreDetail.ChartScoreDetails.IsChangePointsColor = true;
                    scoreDetail.ChartScoreDetails.PointNumber = 0;
                    scoreDetail.ChartScoreDetails.PointColor = ColorTranslator.FromHtml("#BA55D3");
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_OVERALL_COMPARATIVE_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID;
                    dropDownItemList.Add(new DropDownItem("Title", "Absolute Comparative Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.DESIGN_REPORT_OVERALL_COMPARATIVE_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID + ".png"));
                }
                //else show relative score
                else
                {
                    selectedPrintProperty.Append("0|");
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.RelativeScoreDetails;
                    scoreDetail.ChartScoreDetails.IsChangePointsColor = true;
                    scoreDetail.ChartScoreDetails.PointNumber = 0;
                    scoreDetail.ChartScoreDetails.PointColor = ColorTranslator.FromHtml("#BA55D3");
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_OVERALL_COMPARATIVE_RELATIVE+ "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID;
                    dropDownItemList.Add(new DropDownItem("Title", "Relative Comparative Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.DESIGN_REPORT_OVERALL_COMPARATIVE_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID + ".png"));
                }
               
            }
            //Check the hidden field .If the hidden field is 0 hide comparitive score
            else
            {
                selectedPrintProperty.Append("0|");
                OverallScoresChartControl_selectDisplayLinkButton.Text = "Show Comparative Score";

                //If absolute score radion button is clicked show absolute score
                if (OverallScoresChartControl_selectAbsoluteScoreRadioButton.Checked)
                {
                    selectedPrintProperty.Append("1|");
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.AbsoluteCandidateScoreDetails;
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_OVERALL_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID;
                    dropDownItemList.Add(new DropDownItem("Title", "Absolute Candidate Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.DESIGN_REPORT_OVERALL_ABSOLUTE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID + ".png"));
                }
                //else show relative score
                else
                {
                    selectedPrintProperty.Append("0|");
                    scoreDetail.ChartScoreDetails.ChartData = scoreDetail.RelativeCandidateScoreDetails;
                    scoreDetail.ChartScoreDetails.ChartImageName = Constants.ChartConstants.DESIGN_REPORT_OVERALL_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID;
                    dropDownItemList.Add(new DropDownItem("Title", "Relative Candidate Score"));
                    dropDownItemList.Add(new DropDownItem("ChratImagePath", Constants.ChartConstants.DESIGN_REPORT_OVERALL_RELATIVE + "-" + candidateReportDetail.TestKey + "-" + candidateReportDetail.CandidateSessionkey + "-" + candidateReportDetail.AttemptID + ".png"));
                }
            }
            //Finally assign the datasource for the chart control
            OverallScoreChartControl_scoreChart.DataSource = scoreDetail.ChartScoreDetails;
            WidgetMultiSelectControl.WidgetTypePropertyDataSource = dropDownItemList;
            WidgetMultiSelectControl.WidgetTypePropertyAllSelected(true);
            WidgetMultiSelectControlPrint.SelectedProperties = selectedPrintProperty.ToString();
        }
        #endregion

        #region IWidgetControl Members                                         

        /// <summary>
        /// Method that is used to bind the widget instance
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the widget instance
        /// </param>     
        public void Bind(WidgetInstance instance)
        {
            try
            {
                //Keep the instance key in view state 
                ViewState["instance"] = instance.InstanceKey;

                //Load the chart details 
                LoadChartDetails(instance);

                //Assign the attributes for the link button 
                OverallScoresChartControl_selectDisplayLinkButton.Attributes.Add
                    ("onclick", "javascript:return ShowOrHideComparativeScore('"
                    + OverallScoresChartControl_selectDisplayLinkButton.ClientID +
                    "','" + OverAllScoresChartControl_hiddenfield.ClientID + "');");
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }

        /// <summary>
        /// Method that is used to return the update panel
        /// </summary>
        /// <param name="instance">
        /// A<see cref="WidgetInstance"/>that holds the instance of the widget
        /// </param>
        /// <param name="commandData">
        /// A<see cref="WidgetCommandInfo"/>that holds the command data 
        /// </param>
        /// <param name="updateMode">
        /// A<see cref="UpdateMode"/>that holds the update mode
        /// </param>
        /// <returns>
        /// A<see cref="UpdatePanel"/>that returns the update panel
        /// </returns>
        public UpdatePanel[] Command(WidgetInstance instance,
            WidgetCommandInfo commandData, ref UpdateMode updateMode)
        {
            //throw new NotImplementedException();
            //return new UpdatePanel[] { OverallScoresChartControl_updatePanel };
            return null;
        }

        /// <summary>
        /// Method that is used to init the control
        /// </summary>
        /// <param name="parameters">
        /// A<see cref="WidgetInitParameters"/>that holds the 
        /// widget init parameters
        /// </param>
        public void InitControl(WidgetInitParameters parameters)
        {
            //throw new NotImplementedException();
        }
        #endregion
    }
}