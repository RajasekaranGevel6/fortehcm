﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NameControl.cs
// File that represents the user interface for the person name information details

#endregion Header

#region Directives                                                             

using System;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class NameControl : System.Web.UI.UserControl
    {
        #region Event Handlers                                                 
        
        protected void Page_Load(object sender, EventArgs e)
        {
            NameControl_plusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
                NameControl_controlsDiv.ClientID + "','" + NameControl_plusSpan.ClientID + "','" +
                NameControl_minusSpan.ClientID + "','" + NameControl_hiddenField.ClientID + "')");
            NameControl_minusSpan.Attributes.Add("onclick", "ExpandOrRestoreResumeControls('" +
                   NameControl_controlsDiv.ClientID + "','" + NameControl_plusSpan.ClientID + "','" +
                   NameControl_minusSpan.ClientID + "','" + NameControl_hiddenField.ClientID + "')");
            // 
            if (!Utility.IsNullOrEmpty(NameControl_hiddenField.Value) &&
                NameControl_hiddenField.Value == "Y")
            {
                NameControl_controlsDiv.Style["display"] = "none";
                NameControl_plusSpan.Style["display"] = "block";
                NameControl_minusSpan.Style["display"] = "none";
            }
            else
            {
                NameControl_controlsDiv.Style["display"] = "block";
                NameControl_plusSpan.Style["display"] = "none";
                NameControl_minusSpan.Style["display"] = "block";
            }
        }
        
        #endregion Event Handlers
       
        #region Public Properties                                              
        
        public string FirstName { set; get; }

        public string MiddleName { set; get; }

        public string LastName { set; get; }

        public Name DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                NameControl_firstNameTextBox.Text = value.FirstName;
                NameControl_middleNameTextBox.Text = value.MiddleName;
                NameControl_lastNameTextBox.Text = value.LastName;
            }
        }

        #endregion Public Properties
    }
}