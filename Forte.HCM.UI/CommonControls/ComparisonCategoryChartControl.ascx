﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComparisonCategoryChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ComparisonCategoryChartControl" %>
<%@ Register Src="~/CommonControls/MultiSeriesReportChartControl.ascx" TagName="MultiSeriesChart"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/WidgetMultiSelectControl.ascx" TagName="WidgetMultiSelectControl"
    TagPrefix="uc2" %>
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="CategoryChartControl_updatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div style="width: 100%; overflow: auto">
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControl" runat="server" Visible="false" />
                                        <uc2:WidgetMultiSelectControl ID="WidgetMultiSelectControlPrint" runat="server" Visible="false" />
                                        <asp:HiddenField ID="ComparisonCategoryChartControl_testKeyhiddenField" runat="server" />
                                        <uc2:WidgetMultiSelectControl ID="ComparisonCategoryChartControl_widgetMultiSelectControl"
                                            runat="server" OnselectedIndexChanged="ComparisonCategoryChartControl_selectProperty"
                                            OnClick="ComparisonCategoryChartControl_widgetMultiSelectControl_Click"
                                            OncancelClick="ComparisonCategoryChartControl_widgetMultiSelectControl_CancelClick"
                                             />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:Label ID="ComparisonCategoryChartControl_selectScoreLabel" runat="server" Text="Score Type"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonCategoryChartControl_selectOptionsAbsoluteScore" runat="server"
                                                    Text=" Absolute Score" Checked="true" GroupName="1" AutoPostBack="True" OnCheckedChanged="ComparisonCategoryChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="ComparisonCategoryChartControl_selectOptionsRelativeScore" runat="server"
                                                    Text=" Relative Score" GroupName="1" AutoPostBack="True" OnCheckedChanged="ComparisonCategoryChartControl_selectOptionsAbsoluteScore_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 40%">
                                                <asp:LinkButton ID="ComparisonCategoryChartControl_selectDisplayLinkButton" runat="server"
                                                    Text="Show Comparative Score" SkinID="sknActionLinkButton" OnClick="ComparisonCategoryChartControl_selectDisplayLinkButton_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="ComparisonCategoryChartControl_hiddenField" runat="server" Value="0" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <uc1:MultiSeriesChart ID="ComparisonCategoryChartControl_multiSeriesChartControl"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
