﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NameControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.NameControl" %>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td class="header_bg" colspan="2">
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="width: 93%" align="left">
                        <asp:Literal ID="NameControl_headerLiteral" runat="server" Text="Name"></asp:Literal>
                         <asp:HiddenField ID="NameControl_hiddenField" runat="server" />
                    </td>
                    <td style="width: 2%" align="right">
                        <span id="NameControl_plusSpan" runat="server" style="display: none;">
                            <asp:Image ID="NameControl_plusImage" runat="server" SkinID="sknPlusImage" /></span><span
                                id="NameControl_minusSpan" runat="server" style="display: block;">
                                <asp:Image ID="NameControl_minusImage" runat="server" SkinID="sknMinusImage" /></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%">
            <div id="NameControl_controlsDiv" style="display: block;" runat="server">
                <table width="100%">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="NameControl_firstNameLabel" runat="server" Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="NameControl_firstNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="NameControl_middleNameLabel" runat="server" Text="Middle Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="NameControl_middleNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="NameControl_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="NameControl_lastNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
