﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// This usercontrol is used to display the test session 
    /// details for a particular candidate session.
    /// </summary>
    public partial class TestScheduleControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Do Nothing
        }
        public string CandidateSessionId
        { set; get; }
        public string TitleMessage
        {
            set
            {
                TestScheduleControl_questionResultLiteral.Text = value;
            }
        }
        /// <summary>
        /// This property assigns the test session details to labels.
        /// </summary>
        public TestSessionDetail TestSession
        {
            set
            {
                TestSchedule_testSessionIdLabel.Text = value.TestSessionID;
                TestSchedule_candidateSessionIdLabel.Text = CandidateSessionId;
                TestSchedule_testIdLabel.Text = value.TestID;
                TestSchedule_testNameLabel.Text = value.TestName;
                
                TestSchedule_testAuthorLabel.Text = value.TestSessionAuthor;
                TestSchedule_testDescLiteral.Text = 
                    value.TestSessionDesc == null ? value.TestSessionDesc : value.TestSessionDesc.ToString().Replace(Environment.NewLine, "<br />");

                TestSchedule_recommendedTimeLabelText.Text = 
                    Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(value.TimeLimit.ToString()));  //.ToShortDateString();

                TestSchedule_instructionsLiteral.Text = 
                    value.Instructions == null ? value.Instructions : value.Instructions.ToString().Replace(Environment.NewLine, "<br />");

                TestSchedule_displayResultsLabel.Text = value.IsDisplayResultsToCandidate == true ? "Yes" : "No";
                TestSchedule_cyberProctorateLabel.Text = value.IsCyberProctoringEnabled == true ? "Yes" : "No";

                if(value.CandidateTestSessions!=null)
                //Search the selected candidateSessionId and display the candidate informations.
                for (int index=0; index < value.CandidateTestSessions.Count; index++)
                {
                    if (value.CandidateTestSessions[index].CandidateTestSessionID == CandidateSessionId)
                    {
                        TestSchedule_expiryDateLabelText.Text = GetDateFormat(value.CandidateTestSessions[index].ScheduledDate);
                        TestSchedule_candidateLabel.Text = value.CandidateTestSessions[index].CandidateFirstName;
                        TestSchedule_emailLabel.Text = value.CandidateTestSessions[index].Email;
                        break;
                    }
                }
            }
        }

        #region Private Methods                                                 

        /// <summary>
        /// This method return the date in MM/dd/yyyy format. If date is not given, 
        /// then it displays empty value in column.
        /// </summary>
        /// <param name="date">
        /// This parameter is passed from aspx page values can be ScheduledDate/CompletedDate
        /// </param>
        /// <returns>
        /// Returns the date with the format of MM/DD/YYYY
        /// </returns>
        protected string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }
        #endregion Private Methods                                             
    }
}