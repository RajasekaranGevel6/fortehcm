﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewTestDetailsControl.cs
// File that represents the user interface for the Test details.

#endregion Header

#region Directives

using System;
using System.Web.UI;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ViewInterviewTestDetailsControl : UserControl
    {
        #region Event Handler

        protected void Page_Load(object sender, EventArgs e)
        { }

        #endregion Event Handler

        #region Public Properties

        /// <summary>
        /// This datasource holds the test detail information
        /// </summary>
        public InterviewDetail TestDetailDataSource
        {
            set
            {
                if (value == null)
                    return;

                // Set values into controls.

                TestDetailsControl_testDetailsTestIdLabel.Text = value.InterviewTestKey;
                TestDetailsControl_testNameTextBox.Text = value.InterviewName;
                TestDetailsControl_testDescriptionTextBox.Text = value.InterviewDescription == null ? value.InterviewDescription : 
                    value.InterviewDescription.ToString().Replace(Environment.NewLine, "<br />");
                    
                TestDetailsControl_positionProfileTextBox.Text = value.PositionProfileName;


                ViewInterviewTestDetailsControl_certificationRow.Visible = false;


                //// Convert recommendedtime as hours/minutes/seconds format
                //TestDetailsControl_recommendedTimeLabelTextBox.Text =
                //    Utility.ConvertSecondsToHoursMinutesSeconds(value.RecommendedCompletionTime);

                //TestDetailsControl_systemRecommendedTimeTextBox.Text =
                //    Utility.ConvertSecondsToHoursMinutesSeconds(value.SystemRecommendedTime);

                //TestDetailsControl_minTotalScoreLabelsTextBox.Text =
                //    value.CertificationDetail.MinimumTotalScoreRequired.ToString();

                //TestDetailsControl_MaximumtimepermissibleScoreTextBox.Text =
                //    Utility.ConvertSecondsToHoursMinutesSeconds
                //    (Convert.ToInt32(value.CertificationDetail.MaximumTimePermissible));

                //TestDetailsControl_numberofpermissibleTextBox.Text =
                //    value.CertificationDetail.PermissibleRetakes.ToString();

                //TestDetailsControl_timeperiodrequiredTextBox.Text =
                //    value.CertificationDetail.DaysElapseBetweenRetakes.ToString();

                //TestDetailsControl_certificateformatTextBox.Text =
                //    value.CertificationDetail.CertificateFormat;

                //TestDetailsControl_periodofcertificationvalidityValueLabel.Text =
                //    value.CertificationDetail.CertificateValiditiyText;
            }
        }

        #endregion Public Properties
    }
}