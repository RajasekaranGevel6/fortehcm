﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpgradeAccountConfirmationControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.UpgradeAccountConfirmationControl" %>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td class="popupcontrol_upgrade_account_inner_bg">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td class="msg_align">
                        <div>
                            <asp:UpdatePanel ID="UpgradeAccountConfirmationControl_topMessageLabelUpdatePanel"
                                runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_topErrorMessageLabel" runat="server"
                                        EnableViewState="false" SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_topSuccessMessageLabel" runat="server"
                                        EnableViewState="false" Width="100%" SkinID="sknSuccessMessage"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="popup_td_padding_10" style="width: 100%;">
                        <table cellpadding="1" cellspacing="5" border="0" width="100%">
                            <tr>
                                <td style="width: 10%">
                                    <asp:Label ID="UpgradeAccountConfirmationControl_userNameHeaderLabel" runat="server"
                                        Text="User ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_userNameDisplayLabel" runat="server"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_subscriptionHeadLabel" runat="server"
                                        SkinID="sknLabelFieldHeaderText" Text="Subscription Name"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <input type="hidden" runat="server" id="UpgradeAccountConfirmationControl_selectedSubscriptionIdHidden" />
                                    <input type="hidden" runat="server" id="UpgradeAccountConfirmationControl_previousSubscriptionIdHidden" />
                                    <input type="hidden" runat="server" id="UpgradeAccountConfirmationControl_isTrialHidden" />
                                    <input type="hidden" runat="server" id="UpgradeAccountConfirmationControl_trialDaysHidden" />
                                    <input type="hidden" runat="server" id="UpgradeAccountConfirmationControl_userIdHidden" />
                                    <input type="hidden" runat="server" id="UpgradeAccountConfirmationControl_MaximumCorporateUsersHidden"
                                        value="0" />
                                    <asp:Label ID="UpgradeAccountConfirmationControl_subscriptionDisplayLabel" runat="server"
                                        SkinID="sknLabelFieldText"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_firstNameHeaderLabel" runat="server"
                                        Text="FirstName" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                    <span class="mandatory">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="UpgradeAccountConfirmationControl_firstNameTextBox" runat="server"
                                        MaxLength="100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_lastNameHeaderLabel" runat="server"
                                        Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                    <span class="mandatory">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="UpgradeAccountConfirmationControl_lastNameTextBox" runat="server"
                                        MaxLength="100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_phoneHeaderLabel" runat="server"
                                        Text="Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                    <span class="mandatory">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="UpgradeAccountConfirmationControl_phoneTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_companyNameHeaderLabe2l" runat="server"
                                        Text="Company Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                    <span class="mandatory">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="UpgradeAccountConfirmationControl_companyNameTextBox" runat="server"
                                        MaxLength="100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_tileHeaderLabel" runat="server"
                                        Text="Title" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                </td>
                                <td>
                                    <asp:TextBox ID="UpgradeAccountConfirmationControl_titleTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_typeOfBussinessHeaderLabel" runat="server"
                                        Text="Type of Business" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                    <span class="mandatory">*</span>
                                </td>
                                <td align="left">
                                    <div style="overflow: auto; height: 100px; width: 95%;">
                                        <asp:CheckBoxList ID="UpgradeAccountConfirmationControl_typeOfBussinessCheckBoxList"
                                            runat="server" RepeatDirection="Horizontal" RepeatColumns="3" TextAlign="Right"
                                            CellSpacing="5" Width="100%">
                                        </asp:CheckBoxList>
                                        &nbsp;
                                        <asp:CheckBox ID="UpgradeAccountConfirmationControl_otherTypeOfBussinessCheckBox"
                                            runat="server" Text="Other" />
                                    </div>
                                    <div id="UpgradeAccountConfirmationControl_otherBussinessDiv" runat="server" style="display: none;">
                                        <asp:TextBox ID="UpgradeAccountConfirmationControl_otherBussinessTextBox" runat="server"
                                            MaxLength="30"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr id="UpgradeAccountConfirmationControl_numberOfUsersTr" runat="server" visible="false">
                                <td>
                                    <asp:Label ID="UpgradeAccountConfirmationControl_numberOfUsersHeaderLabel" runat="server"
                                        Text="Number of Users" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                </td>
                                <td align="right">
                                    <span class="mandatory">*</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td style="width: 42%;">
                                                <asp:TextBox ID="UpgradeAccountConfirmationControl_numberOfUsersTextBox" runat="server"
                                                    MaxLength="2"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:ImageButton ID="UpgradeAccountConfirmationControl_numberOfUsersTextBoxHelpImageButton"
                                                    runat="server" SkinID="sknHelpImageButton" OnClientClick="javascript:return false;"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="UpgradeAccountConfirmationControl_bottonMessageLabelUpdatePanel"
                            runat="server">
                            <ContentTemplate>
                                <asp:Label ID="UpgradeAccountConfirmationControl_bottomErrorMessageLabel" runat="server"
                                    EnableViewState="false" SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                <asp:Label ID="UpgradeAccountConfirmationControl_bottomSuccessMessageLabel" runat="server"
                                    EnableViewState="False" Width="100%" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align" valign="top">
                        <asp:Button ID="UpgradeAccountConfirmationControl_registerButton" SkinID="sknSignUpregisterButton"
                            BackColor="Transparent" BorderStyle="None" runat="server" OnClick="UpgradeAccountConfirmationControl_registerButton_Click" />&nbsp;
                        <asp:Button ID="UpgradeAccountConfirmationControl_cancelRegistrationButton" runat="server"
                            SkinID="sknSignUpcancelButton" BackColor="Transparent" BorderStyle="None" OnClick="UpgradeSubscription_CancelClick" />
                    </td>
                </tr>
                <tr>
                    <td class="td_height_20">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript">
    var Text = '<%= UpgradeAccountConfirmationControl_otherBussinessTextBox.ClientID %>';
    var Div = '<%= UpgradeAccountConfirmationControl_otherBussinessDiv.ClientID %>';

    function ShowValue(CheckBoxch) {
        if (CheckBoxch) {
            document.getElementById(Text).value = "";
            document.getElementById(Div).style.display = "block";
        }
        else {
            document.getElementById(Text).value = "";
            document.getElementById(Div).style.display = "none";
        }
    }
    // Method that will show/hide the captcha image help panel.
   
</script>
