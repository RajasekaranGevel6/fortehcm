﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ResumeExecutiveSummaryControl.cs
// File that represents the user interface for the executive summary information details

#endregion Header

#region Directives

using System;
using Forte.HCM.Support;

#endregion Directives


namespace Forte.HCM.UI.CommonControls
{
    public partial class ResumeExecutiveSummaryControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                ResumeExecutiveSummaryControl_execSummaryTextBox.Text = value;
            }
        }
    }
}