﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class ResumeTechnicalSkillsControl {
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_controlsDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ResumeTechnicalSkillsControl_controlsDiv;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_languageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeTechnicalSkillsControl_languageLabel;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_languageTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ResumeTechnicalSkillsControl_languageTextBox;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_osLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeTechnicalSkillsControl_osLabel;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_osTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ResumeTechnicalSkillsControl_osTextBox;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_databaseLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeTechnicalSkillsControl_databaseLabel;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_databaseTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ResumeTechnicalSkillsControl_databaseTextBox;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_uiToolsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeTechnicalSkillsControl_uiToolsLabel;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_uiToolsTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ResumeTechnicalSkillsControl_uiToolsTextBox;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_otherSkillsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ResumeTechnicalSkillsControl_otherSkillsLabel;
        
        /// <summary>
        /// ResumeTechnicalSkillsControl_otherSkillsTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ResumeTechnicalSkillsControl_otherSkillsTextBox;
    }
}
