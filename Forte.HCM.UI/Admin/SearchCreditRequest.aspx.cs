﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchCreditRequest.aspx.cs
// This page allows the admin to process the credit request 

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

using AjaxControlToolkit;
using System.Globalization;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    public partial class SearchCreditRequest : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handler                                                  

        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //Page.Form.DefaultButton = SearchCreditRequest_searchButton.UniqueID;
                //Page.Form.DefaultFocus = SearchCreditRequest_searchButton.UniqueID;

                SearchCreditRequest_firstNameTextBox.Focus();
                //Call the method to add attributes for various controls
                AddAttributesForControl();

                // Set page title
                Master.SetPageCaption("Credit Management");

                //Add the click event for the page navigator
                SearchCreditRequest_pageNavigator.PageNumberClick += new PageNavigator.
                    PageNumberClickEventHandler(SearchCreditRequest_pageNavigator_PageNumberClick);

                SearchCreditRequest_firstNameTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                + SearchCreditRequest_searchButton.ClientID + "','NAME','"
                + SearchCreditRequest_browserHiddenField.ClientID + "');");

                SearchCreditRequest_emailTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + SearchCreditRequest_searchButton.ClientID + "','EMAIL','"
                    + SearchCreditRequest_browserHiddenField.ClientID + "');");

                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchCreditRequest_browserHiddenField.Value))
                {
                    ValidateEnterKey(SearchCreditRequest_browserHiddenField.Value.Trim());
                    SearchCreditRequest_browserHiddenField.Value = string.Empty;
                }

                if (!IsPostBack)
                {
                    // Assign default sort field and order keys.
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "CANDIDATE_NAME";

                    //Call the method to clear the controls in the page
                    ClearControls();

                    AssignCreditType();

                    SearchCreditRequest_Div.Visible = false;

                    
                }

                foreach (MultiHandleSliderTarget target in SearchCreditRequest_amountMultiHandleSliderExtender.MultiHandleSliderTargets)
                    target.ControlID = SearchCreditRequest_amountMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;

                SearchCreditRequest_bottomErrorMessageLabel.Text = string.Empty;
                SearchCreditRequest_topErrorMessageLabel.Text = string.Empty;


                //Page.SetFocus(SearchCreditRequest_searchButton);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
               SearchCreditRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event that is called on page number click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="PageNumberEventArgs"/>that holds the events args
        /// </param>
        void SearchCreditRequest_pageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                //Load the grid with the current page number
                LoadCreditDetails(e.PageNumber);

                ViewState["PAGENUMBER"] = e.PageNumber;
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                ShowMessage(SearchCreditRequest_bottomErrorMessageLabel,
                    SearchCreditRequest_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the search button is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void SearchCreditRequest_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidDate())
                    return;

                //Remove the error and  success messages 
                SearchCreditRequest_topSuccessMessageLabel.Text = string.Empty;

                SearchCreditRequest_bottomSuccessMessageLabel.Text = string.Empty;

                SearchCreditRequest_topErrorMessageLabel.Text = string.Empty;

                SearchCreditRequest_bottomErrorMessageLabel.Text = string.Empty;

                //Make visible the grid view div
                SearchCreditRequest_Div.Visible = true;

                //Set the values in the first name and email text box
                SearchCreditRequest_firstNameTextBox.Text =
                    Request[SearchCreditRequest_firstNameTextBox.UniqueID].Trim();

                SearchCreditRequest_emailTextBox.Text =
                 Request[SearchCreditRequest_emailTextBox.UniqueID].Trim();

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CANDIDATE_NAME";

                //Load the credit detail for page number 1
                LoadCreditDetails(1);

                ViewState["PAGENUMBER"] = 1;

                //Reset the page navigator 
                SearchCreditRequest_pageNavigator.Reset();
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                ShowMessage(SearchCreditRequest_bottomErrorMessageLabel,
                    SearchCreditRequest_topErrorMessageLabel, exception.Message);
            }
        }      

        /// <summary>
        /// Handler method that is called when the row is created in the grid
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void SearchCreditRequest_resultGridView_RowCreated(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                //Add the sort image for the header row
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchCreditRequest_resultGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
                    SearchCreditRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row is sorted in the grid
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void SearchCreditRequest_resultGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                //if the sort expression is different from the sort field
                //assign the sort expression as  
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchCreditRequest_pageNavigator.Reset();
                LoadCreditDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
                    SearchCreditRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the row is bounded in the grid
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void SearchCreditRequest_resultGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                    string candidateId = ((HiddenField)e.Row.FindControl("SearchCreditRequest_resultGridView_candidateIdHiddenField")).Value;
                    //Process credit request only if is processed is false
                    ((ImageButton)e.Row.FindControl("SearchCreditRequest_processCreditRequestImageButton")).Visible =
                        ((Label)e.Row.FindControl("SearchCreditRequest_resultGridView_processedLabel")).Text == "Yes" ? false : true;
                    ((ImageButton)e.Row.FindControl("SearchCreditRequest_viewCreditsImageButton")).Attributes.Add
                        ("onclick", "javascript:return ShowCredit('CreditSearch','" + candidateId + "');");
                    ((CheckBox)e.Row.FindControl("SearchCreditRequest_selectRequestCheckBox")).Visible =
                       ((Label)e.Row.FindControl("SearchCreditRequest_resultGridView_processedLabel")).Text == "Yes" ? false : true;
                    string credit_id = ((HiddenField)e.Row.FindControl("SearchCreditRequest_resultGridView_requestIDHiddenField")).Value;
                    if (credit_id.Trim() == "CR_QSN_C" || credit_id.Trim() == "CR_QSN_S" || credit_id.Trim() == "CR_QSN_M")
                    {
                        ((ImageButton)e.Row.FindControl("SearchCreditRequest_questionDetailsImageButton")).Visible = true;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
                SearchCreditRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called on the row command
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewRowEventArgs"/>that holds the event args
        /// </param>
        protected void SearchCreditRequest_resultGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField SearchCreditRequest_resultGridView_candidateIdHiddenField = ((ImageButton)e.CommandSource).Parent.
                          FindControl("SearchCreditRequest_resultGridView_candidateIdHiddenField") as HiddenField;
                HiddenField SearchCreditRequest_resultGridView_remarksHiddenField = ((ImageButton)e.CommandSource).Parent.
                     FindControl("SearchCreditRequest_resultGridView_remarksHiddenField") as HiddenField;
                HiddenField SearchCreditRequest_resultGridView_creditValueHiddenField = ((ImageButton)e.CommandSource).Parent.
                     FindControl("SearchCreditRequest_resultGridView_creditValueHiddenField") as HiddenField;
                HiddenField SearchCreditRequest_resultGridView_candidateEmailIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                     FindControl("SearchCreditRequest_resultGridView_candidateEmailIDHiddenField") as HiddenField;

                if (e.CommandName == "ProcessRequest")
                {
                    ViewState["SELECTED_GEN_ID"] = e.CommandArgument.ToString();

                    ViewState["CANDIDATE_ID"] = SearchCreditRequest_resultGridView_candidateIdHiddenField.Value;

                    ViewState["CANDIDATE_EMAIL_ID"] = SearchCreditRequest_resultGridView_candidateEmailIDHiddenField.Value;

                    ClearControls();

                    //make the grid view visible as false.
                    SearchCreditRequest_processCreditRequestPanel_tdDiv.Style["display"] = "none";


                    SearchCreditRequest_processCreditRequestPanel_amountDiv.Style["display"] = "block";

                    SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked = true;

                    SearchCreditRequest_processCreditRequestPanel_amountTextBox.Enabled = true;

                    SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text =
                    SearchCreditRequest_resultGridView_creditValueHiddenField.Value.Trim();

                    SearchCreditRequest_processCreditRequestPanel_amountTextBox.Focus();

                    //change the css for the request panel
                    SearchCreditRequest_processCreditRequestPanel.CssClass = "popupcontrol_credit_request";

                    SearchCreditRequest_processRequestModalPopUpExtender.Show();
                }
                else if (e.CommandName == "ShowRemarks")
                {
                    SearchCreditRequest_showRemarksPanel_remarksLiteral.Text =
                        SearchCreditRequest_resultGridView_remarksHiddenField.Value;

                    SearchCreditRequest_showRemarksPanel_modalPopupExtender.Show();
                }
                else if (e.CommandName == "ViewQuestions")
                {
                    //Load the question details for the corresponding question id 
                    SearchCreditRequest_questionDetailPreviewControl.LoadQuestionDetails
                        (e.CommandArgument.ToString(), 0);
                    //Assign the title 
                    SearchCreditRequest_questionDetailPreviewControl.Title = "Question Detail";
                    //show the modal popup
                    SearchCreditRequest_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
                SearchCreditRequest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called on the submit button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void SearchCreditRequest_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchCreditRequest_topSuccessMessageLabel.Text = string.Empty;
                SearchCreditRequest_bottomSuccessMessageLabel.Text = string.Empty;

                if (!IsValidData())
                {
                    if ((SearchCreditRequest_processCreditRequestPanel_tdDiv.Style["display"] == "none") &&
                        (!SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked))
                    {
                        SearchCreditRequest_processCreditRequestPanel_amountDiv.Style["display"] = "none";
                    }
                    if ((SearchCreditRequest_processCreditRequestPanel_tdDiv.Style["display"] == "none") &&
                     (SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked))
                    {
                        SearchCreditRequest_processCreditRequestPanel_amountDiv.Style["display"] = "block";
                    }

                    SearchCreditRequest_processRequestModalPopUpExtender.Show();
                    return;
                }

                bool approved = false;

                List<CreditRequestDetail> creditRequestDetails = CheckSelection();

                if (creditRequestDetails.Count == 0)
                {
                    CreditRequestDetail creditRequestDetail = new CreditRequestDetail();

                    creditRequestDetail.GenId = int.Parse(ViewState["SELECTED_GEN_ID"].ToString());

                    if (ViewState["CANDIDATE_EMAIL_ID"] != null && ViewState["CANDIDATE_EMAIL_ID"].ToString().Length > 0)
                    {
                        creditRequestDetail.CandidateEMail = ViewState["CANDIDATE_EMAIL_ID"].ToString();
                    }

                    creditRequestDetail.IsApproved = SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.
                                                                                                    Checked;
                    if (creditRequestDetail.IsApproved)
                        creditRequestDetail.Amount = SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text.Trim().Length == 0 ? 0 :
                            decimal.Parse(SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text);
                    else
                        creditRequestDetail.Amount = 0;

                    creditRequestDetail.Remarks =
                        SearchCreditRequest_processCreditRequestPanel_remarksTextBox.Text.Trim();

                    creditRequestDetail.CandidateId = int.Parse(ViewState["CANDIDATE_ID"].ToString());

                    creditRequestDetail.CreatedBy = base.userID;

                    creditRequestDetail.ModifiedBy = base.userID;

                    creditRequestDetail.IsProcessed = true;

                    approved = new CreditBLManager().ApproveCreditRequest(creditRequestDetail);

                    // Once the data is updated show the success message.
                    base.ShowMessage(SearchCreditRequest_topSuccessMessageLabel,
                    SearchCreditRequest_bottomSuccessMessageLabel,
                        "Credits processed successfully");


                    //Load the credit details 
                    LoadCreditDetails(int.Parse(ViewState["PAGENUMBER"].ToString()));

                    if (creditRequestDetail.IsApproved)
                        new EmailHandler().SendMail(EntityType.CreditApproved, creditRequestDetail);
                    else
                        new EmailHandler().SendMail(EntityType.CreditDisapproved, creditRequestDetail);

                }
                else
                {
                    List<CreditRequestDetail> requestCollection = new List<CreditRequestDetail>();
                    CreditRequestDetail creditRequestDetail = null;

                    foreach (GridViewRow row in SearchCreditRequest_processCreditRequestPanel_processGridView.Rows)
                    {
                        creditRequestDetail = new CreditRequestDetail();
                        creditRequestDetail.IsApproved = SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.
                                                                                Checked;

                        TextBox amountTextBox = (TextBox)row.FindControl("SearchCreditRequest_processCreditRequestPanel_processGridView_amountTextBox");

                        if (creditRequestDetail.IsApproved)
                        {
                            if (amountTextBox != null)
                            {
                                creditRequestDetail.Amount = amountTextBox.Text.Trim().Length == 0 ? 0 :
                               decimal.Parse(amountTextBox.Text);
                            }
                        }
                        else
                            creditRequestDetail.Amount = 0;

                        creditRequestDetail.Remarks =
                            SearchCreditRequest_processCreditRequestPanel_remarksTextBox.Text.Trim();

                        creditRequestDetail.CreatedBy = base.userID;

                        creditRequestDetail.ModifiedBy = base.userID;

                        creditRequestDetail.IsProcessed = true;

                        creditRequestDetail.GenId = int.Parse(((HiddenField)row.FindControl("SearchCreditRequest_processCreditRequestPanel_processGridView_GenIdHiddenField")).Value);

                        creditRequestDetail.CandidateEMail = ((HiddenField)row.FindControl("SearchCreditRequest_processCreditRequestPanel_candidateEmailIDHiddenField")).Value;

                        creditRequestDetail.CandidateId = int.Parse(((HiddenField)row.FindControl
                            ("SearchCreditRequest_processCreditRequestPanel_processGridView_CandidateIdHiddenField")).Value);

                        creditRequestDetail.CandidateFirstName = ((HiddenField)row.FindControl
                            ("SearchCreditRequest_processCreditRequestPanel_processGridView_CandidateNameHiddenField")).Value;

                        requestCollection.Add(creditRequestDetail);

                        new CreditBLManager().ApproveCreditRequest(creditRequestDetail);

                        // Once the data is updated show the success message.
                        base.ShowMessage(SearchCreditRequest_topSuccessMessageLabel,
                        SearchCreditRequest_bottomSuccessMessageLabel,
                            string.Format("Credits processed successfully for {0} of amount {1}", creditRequestDetail.CandidateFirstName,
                            creditRequestDetail.Amount));
                    }
                    //Load the credit details 
                    LoadCreditDetails(int.Parse(ViewState["PAGENUMBER"].ToString()));

                    foreach (CreditRequestDetail credit in requestCollection)
                    {
                        if (credit.IsApproved)
                            new EmailHandler().SendMail(EntityType.CreditApproved, creditRequestDetail);
                        else
                            new EmailHandler().SendMail(EntityType.CreditDisapproved, creditRequestDetail);
                    }
                }

                SearchCreditRequest_processCreditRequestPanel_remarksTextBox.Text = string.Empty;

                SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text = string.Empty;

                SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked = false;

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                if (SearchCreditRequest_topSuccessMessageLabel.Text.Trim().Length > 0)
                {
                    base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
                SearchCreditRequest_bottomErrorMessageLabel, "<br>" + exp.Message);
                }
                else
                {
                    base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
               SearchCreditRequest_bottomErrorMessageLabel, exp.Message);
                }

            }
        }

        /// <summary>
        /// Handler method that is called on the cancel button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void SearchCreditRequest_cancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                ShowMessage(SearchCreditRequest_bottomErrorMessageLabel,
                    SearchCreditRequest_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called on the reset button click
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void SearchCreditRequest_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                ShowMessage(SearchCreditRequest_bottomErrorMessageLabel,
                    SearchCreditRequest_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchCreditRequest_approveCreditButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<CreditRequestDetail> creditRequestDetail = CheckSelection();

                //if no request is checked return the control 
                if (creditRequestDetail.Count == 0)
                {
                    base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
                        SearchCreditRequest_bottomErrorMessageLabel,
                        "Please select atleast one request");

                    return;
                }

                //change the css for the request panel
                SearchCreditRequest_processCreditRequestPanel.CssClass = "popupcontrol_credit_request_extended";

                //make the SearchCreditRequest_processCreditRequestPanel_amountDiv as invisible. 
                //that is for single approval
                SearchCreditRequest_processCreditRequestPanel_amountDiv.Style["display"] = "none";

                SearchCreditRequest_processCreditRequestPanel_tdDiv.Style["display"] = "block";

                //Assign the grid view with the selected details
                SearchCreditRequest_processCreditRequestPanel_processGridView.DataSource = creditRequestDetail;
                SearchCreditRequest_processCreditRequestPanel_processGridView.DataBind();


                var q = (from c in creditRequestDetail
                         select c.Amount).Distinct();

                decimal amount = 0.00M;

                if (q.Count() == 1)
                {
                    List<decimal> ids = q.ToList();
                    amount = ids[0];
                    SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text = amount.ToString();
                }
                else
                {
                    SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text = string.Empty;
                }

                SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked = true;

               // SearchCreditRequest_processCreditRequestPanel_remarksTextBox.Focus();

                SearchCreditRequest_processCreditRequestPanel_remarksTextBox.Text = string.Empty;

                SearchCreditRequest_processCreditRequestPanel_errorLabel.Text = string.Empty;

                SearchCreditRequest_processRequestModalPopUpExtender.Show();
            }
            catch (Exception exception)
            {
                //Log the exception and show the error to the user
                Logger.ExceptionLog(exception);
                ShowMessage(SearchCreditRequest_bottomErrorMessageLabel,
                    SearchCreditRequest_topErrorMessageLabel, exception.Message);
            }

        }

        #endregion Event Handler

        #region Private Methods                                                
        /// <summary>
        /// Represents the method to load the credit details 
        /// </summary>
        /// <param name="pageNumber"></param>
        private void LoadCreditDetails(int pageNumber)
        {
            int total = 0;

            CreditRequestSearchCriteria creditRequestSearchCriteria = new CreditRequestSearchCriteria();

            creditRequestSearchCriteria.CandidateName = SearchCreditRequest_firstNameTextBox.Text;

            creditRequestSearchCriteria.CandidateEmail = SearchCreditRequest_emailTextBox.Text;

            creditRequestSearchCriteria.ProcessedStatus =
                SearchCreditRequest_processedDropDownList.SelectedValue;

            creditRequestSearchCriteria.ApprovedStatus =
                SearchCreditRequest_approvedDropDownList.SelectedValue;

            creditRequestSearchCriteria.RequestDateFrom = SearchCreditRequest_requestDateFromTextBox.Text.Trim() == string.Empty ?
                Convert.ToDateTime("1/1/1753", new CultureInfo("en-US")) : Convert.ToDateTime(SearchCreditRequest_requestDateFromTextBox.Text.Trim());

            creditRequestSearchCriteria.RequestDateTo = SearchCreditRequest_requestDateToTextBox.Text.Trim() == string.Empty ?
               Convert.ToDateTime("12/31/9999", new CultureInfo("en-US")) : Convert.ToDateTime(SearchCreditRequest_requestDateToTextBox.Text.Trim()).AddDays(1);

            creditRequestSearchCriteria.ProcessedDateFrom = SearchCreditRequest_processedDateFromTextBox.Text.Trim() == string.Empty ?
               Convert.ToDateTime("1/1/1753", new CultureInfo("en-US")) : Convert.ToDateTime(SearchCreditRequest_processedDateFromTextBox.Text.Trim());

            creditRequestSearchCriteria.ProcessedDateTo = SearchCreditRequest_processedDateToTextBox.Text.Trim() == string.Empty ?
                Convert.ToDateTime("12/31/9999", new CultureInfo("en-US")) : Convert.ToDateTime(SearchCreditRequest_processedDateToTextBox.Text.Trim()).AddDays(1);

            creditRequestSearchCriteria.AmountFrom = Convert.ToDecimal(SearchCreditRequest_amountMinValueTextBox.Text.Trim());

            creditRequestSearchCriteria.AmountTo = SearchCreditRequest_amountMaxValueTextBox.Text.Trim() == "0" ?
                99 : Convert.ToDecimal(SearchCreditRequest_amountMaxValueTextBox.Text.Trim());

            creditRequestSearchCriteria.CreditType =
                SearchCreditRequest_creditTypeDropDownList.SelectedValue == "0" ? null : SearchCreditRequest_creditTypeDropDownList.SelectedValue;

            List<CreditRequestDetail> creditRequest = null;
            creditRequest = new CreditBLManager().GetCreditDetails
                (creditRequestSearchCriteria, pageNumber,
                base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                (SortType)ViewState["SORT_ORDER"], out total);

            if (creditRequest.Count != 0)
            {
                SearchCreditRequest_resultGridView.DataSource = creditRequest;
                SearchCreditRequest_resultGridView.DataBind();
                SearchCreditRequest_Div.Visible = true;
                SearchCreditRequest_pageNavigator.Visible = true;
                SearchCreditRequest_pageNavigator.TotalRecords = total;
                SearchCreditRequest_pageNavigator.PageSize = base.GridPageSize;
            }
            else
            {
                SearchCreditRequest_Div.Visible = false;
                //SearchCreditRequest_pageNavigator.Visible = false;
                SearchCreditRequest_resultGridView.DataSource = creditRequest;
                SearchCreditRequest_resultGridView.DataBind();
                if (SearchCreditRequest_bottomSuccessMessageLabel.Text.Length > 0)
                {
                    base.ShowMessage(SearchCreditRequest_bottomErrorMessageLabel,
                   SearchCreditRequest_topErrorMessageLabel, "<br>" + Resources.HCMResource.Common_Empty_Grid);
                }
                else
                {
                    base.ShowMessage(SearchCreditRequest_bottomErrorMessageLabel,
                   SearchCreditRequest_topErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                }
            }
        }

        /// <summary>
        /// Represents the method that is used to get the correct date
        /// </summary>
        /// <param name="date">
        /// A<see cref="DateTime"/>that holds the date
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the correct formatted date
        /// </returns>
        protected string GetCorrectDate(DateTime date)
        {
            string correctDate = date.ToString("MM/dd/yyyy");
            if (correctDate == "01/01/0001")
                correctDate = "";
            return correctDate;
        }

        /// <summary>
        /// Represents the method that is used to get the correct amount
        /// </summary>
        /// <param name="date">
        /// A<see cref="DateTime"/>that holds the date
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the correct formatted date
        /// </returns>
        protected string GetCorrectAmount(Decimal amount)
        {
            return amount == 0.00M ? "0" : amount.ToString();
        }

        /// <summary>
        /// Method used to clear the controls
        /// </summary>
        private void ClearControls()
        {
            SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked = false;

            SearchCreditRequest_processCreditRequestPanel_errorLabel.Text = string.Empty;

            SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text = string.Empty;

            SearchCreditRequest_processCreditRequestPanel_remarksTextBox.Text = string.Empty;

            SearchCreditRequest_firstNameTextBox.Text = string.Empty;

            SearchCreditRequest_emailTextBox.Text = string.Empty;

            SearchCreditRequest_processedDropDownList.SelectedIndex = 0;

            SearchCreditRequest_approvedDropDownList.SelectedIndex = 0;

            SearchCreditRequest_topSuccessMessageLabel.Text = string.Empty;

            SearchCreditRequest_bottomSuccessMessageLabel.Text = string.Empty;

            SearchCreditRequest_topErrorMessageLabel.Text = string.Empty;

            SearchCreditRequest_bottomErrorMessageLabel.Text = string.Empty;

        }

        /// <summary>
        /// Method used to set the attribute values 
        /// </summary>
        private void AddAttributesForControl()
        {

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchCreditRequest_isMaximizedHiddenField.Value) &&
                   SearchCreditRequest_isMaximizedHiddenField.Value == "Y")
            {
                SearchCreditRequest_searchDIV.Style["display"] = "none";
                SearchCreditRequest_searchResultsUpSpan.Style["display"] = "block";
                SearchCreditRequest_searchResultsDownSpan.Style["display"] = "none";
                SearchCreditRequest_resultDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchCreditRequest_searchDIV.Style["display"] = "block";
                SearchCreditRequest_searchResultsUpSpan.Style["display"] = "none";
                SearchCreditRequest_searchResultsDownSpan.Style["display"] = "block";
                SearchCreditRequest_resultDiv.Style["height"] = RESTORED_HEIGHT;
            }

            //Add attributes for the first name image button 
            SearchCreditRequest_firstNameImageButton.Attributes.Add("onclick",
                    "return LoadUserForCreditManagement('" + SearchCreditRequest_userNameHiddenField.ClientID + "','"
                    + SearchCreditRequest_userNameHiddenField.ClientID + "','" + SearchCreditRequest_firstNameTextBox.ClientID + "')");

            //Add attributes for the email name image button 
            SearchCreditRequest_emailImageButton.Attributes.Add("onclick",
                "return LoadUserForCreditManagement('" + SearchCreditRequest_userNameHiddenField.ClientID + "','"
                + SearchCreditRequest_emailTextBox.ClientID + "','')");


            //Add attributes for expand or compress click
            SearchCreditRequest_expandTR.Attributes.Add("onclick",
          "ExpandOrRestore('" +
          SearchCreditRequest_resultDiv.ClientID + "','" +
          SearchCreditRequest_searchDIV.ClientID + "','" +
          SearchCreditRequest_searchResultsUpSpan.ClientID + "','" +
          SearchCreditRequest_searchResultsDownSpan.ClientID + "','" +
          SearchCreditRequest_isMaximizedHiddenField.ClientID + "','" +
          RESTORED_HEIGHT + "','" +
          EXPANDED_HEIGHT + "')");

            // Add attributes for the checked changed in the check box
            SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.
                Attributes.Add("onclick", "javascript:CheckChanged('" +
                SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.ClientID + "','" +
                SearchCreditRequest_processCreditRequestPanel_amountDiv.ClientID + "','"
                + SearchCreditRequest_processCreditRequestPanel_tdDiv.ClientID + "');");

            //Add attributes to check the null value
            //SearchCreditRequest_submitButton.Attributes.Add("onclick", "javascript:return CheckNullFields('" +
            //    SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.ClientID + "','" +
            //    SearchCreditRequest_processCreditRequestPanel_amountTextBox.ClientID + "','" +
            //    SearchCreditRequest_processCreditRequestPanel_remarksTextBox.ClientID + "','" +
            //    SearchCreditRequest_processCreditRequestPanel_errorLabel.ClientID + "','"
            //    + SearchCreditRequest_processCreditRequestPanel_tdDiv.ClientID + "')");

        }

        /// <summary>
        /// Represents the method to assign the credit type
        /// </summary>
        private void AssignCreditType()
        {
            List<AttributeDetail> attributeDetail = new AttributeBLManager().GetAttributeCreditType();

            if (attributeDetail == null)
                return;

            SearchCreditRequest_creditTypeDropDownList.DataSource = attributeDetail;
            SearchCreditRequest_creditTypeDropDownList.DataTextField = "AttributeName";
            SearchCreditRequest_creditTypeDropDownList.DataValueField = "AttributeID";
            SearchCreditRequest_creditTypeDropDownList.DataBind();
            SearchCreditRequest_creditTypeDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));
            SearchCreditRequest_creditTypeDropDownList.Items[0].Selected = true;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<CreditRequestDetail> CheckSelection()
        {
            HiddenField requestIDHiddenField;

            HiddenField amountHiddenField;

            HiddenField genIDHiddenField;

            HiddenField candidateHiddenField;

            HiddenField candidateEmailIdHiddenField;

            List<CreditRequestDetail> creditDetails = new List<CreditRequestDetail>();

            foreach (GridViewRow row in SearchCreditRequest_resultGridView.Rows)
            {
                CreditRequestDetail creditRequestDetail = new CreditRequestDetail();
                CheckBox checkBox = (CheckBox)row.FindControl("SearchCreditRequest_selectRequestCheckBox");

                if (!checkBox.Checked)
                    continue;

                requestIDHiddenField = (HiddenField)row.FindControl("SearchCreditRequest_resultGridView_requestIDHiddenField");
                amountHiddenField = (HiddenField)row.FindControl("SearchCreditRequest_resultGridView_creditValueHiddenField");
                genIDHiddenField = (HiddenField)row.FindControl("SearchCreditRequest_resultGridView_GenIdHiddenField");
                candidateHiddenField = (HiddenField)row.FindControl("SearchCreditRequest_resultGridView_candidateIdHiddenField");
                candidateEmailIdHiddenField = (HiddenField)row.FindControl("SearchCreditRequest_resultGridView_candidateEmailIDHiddenField");
                creditRequestDetail.CreditRequestDescription = ((Label)row.FindControl("SearchCreditRequest_resultGridView_requestDescriptionLabel")).Text;
                creditRequestDetail.CandidateFirstName = ((Label)row.FindControl("SearchCreditRequest_resultGridView_CandidateNameLabel")).Text;
                creditRequestDetail.GenId = int.Parse(genIDHiddenField.Value);
                creditRequestDetail.Amount = decimal.Parse(amountHiddenField.Value);
                creditRequestDetail.CreditRequestID = requestIDHiddenField.Value;
                creditRequestDetail.IsApproved = SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked;
                creditRequestDetail.CandidateId = int.Parse(candidateHiddenField.Value);
                creditRequestDetail.CandidateEMail = candidateEmailIdHiddenField.Value;
                creditDetails.Add(creditRequestDetail);
            }

            return creditDetails;

        }

        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "NAME":
                    this.SearchCreditRequest_searchButton_Click(this, new EventArgs());
                    break;
                case "EMAIL":
                    this.SearchCreditRequest_searchButton_Click(this, new EventArgs());
                    break;
            }
        }

        /// <summary>
        /// Represents the method to valid date 
        /// </summary>
        /// <returns>
        /// A<see cref="bool"/>that holds whether the date is valid or not
        /// </returns>
        private bool ValidDate()
        {
            if (SearchCreditRequest_requestDateToTextBox.Text != string.Empty)
            {
                if (!SearchCreditRequest_requestDateToMaskedEditValidator.IsValid)
                {
                    base.ShowMessage(SearchCreditRequest_topErrorMessageLabel,
                        SearchCreditRequest_bottomErrorMessageLabel,
                    Resources.HCMResource.SearchCreditRequest_RequestToEnterCorrectDate);
                    return false;
                }
                else
                {
                    DateTime toDate = Convert.ToDateTime
                       (SearchCreditRequest_requestDateToTextBox.Text.Trim());

                    if (toDate > Convert.ToDateTime("12/31/9999") || toDate < Convert.ToDateTime("1/1/1753"))
                    {
                        base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                          Resources.HCMResource.
                            TestStatisticsInfo_EnterCorrectEndDateValidation);
                        return false;
                    }

                }
            }
            if (SearchCreditRequest_requestDateFromTextBox.Text != string.Empty)
            {
                if (!SearchCreditRequest_requestDateFromMaskedEditValidator.IsValid)
                {
                    base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                    Resources.HCMResource.
                    SearchCreditRequest_RequestFromEnterCorrectDate);
                    return false;
                }
                else
                {
                    DateTime fromDate = Convert.ToDateTime
                        (SearchCreditRequest_requestDateFromTextBox.Text.Trim());

                    if (fromDate < Convert.ToDateTime("1/1/1753"))
                    {
                        base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                         Resources.HCMResource.
                         TestStatisticsInfo_EnterCorrectStartDateValidation);
                        return false;
                    }


                }
            }
            if (SearchCreditRequest_requestDateToTextBox.Text != string.Empty &&
              SearchCreditRequest_requestDateFromTextBox.Text != string.Empty)
            {
                if (SearchCreditRequest_requestDateToMaskedEditValidator.IsValid
                    && SearchCreditRequest_requestDateFromMaskedEditValidator.IsValid)
                {
                    DateTime fromDate = Convert.ToDateTime
                    (SearchCreditRequest_requestDateFromTextBox.Text.Trim());
                    DateTime toDate = Convert.ToDateTime
                        (SearchCreditRequest_requestDateToTextBox.Text.Trim());

                    if (toDate < fromDate)
                    {
                        base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                            Resources.HCMResource.SearchCreditRequest_EnterCorrectRequestEndDate);
                        return false;
                    }
                }
            }
            if (SearchCreditRequest_processedDateToTextBox.Text != string.Empty)
            {
                if (!SearchCreditRequest_processedDateToMaskedEditValidator.IsValid)
                {
                    base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                    Resources.HCMResource.SearchCreditRequest_ProcessedToEnterCorrectDate);
                    return false;
                }
                else
                {
                    DateTime toDate = Convert.ToDateTime
                       (SearchCreditRequest_processedDateToTextBox.Text.Trim());

                    if (toDate > Convert.ToDateTime("12/31/9999") || toDate < Convert.ToDateTime("1/1/1753"))
                    {
                        base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                          Resources.HCMResource.
                            TestStatisticsInfo_EnterCorrectEndDateValidation);
                        return false;
                    }

                }
            }
            if (SearchCreditRequest_processedDateFromTextBox.Text != string.Empty)
            {
                if (!SearchCreditRequest_processedDateFromMaskedEditValidator.IsValid)
                {
                    base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                    Resources.HCMResource.
                    SearchCreditRequest_ProcessedFromEnterCorrectDate);
                    return false;
                }
                else
                {
                    DateTime fromDate = Convert.ToDateTime
                        (SearchCreditRequest_processedDateFromTextBox.Text.Trim());

                    if (fromDate < Convert.ToDateTime("1/1/1753"))
                    {
                        base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                         Resources.HCMResource.
                         TestStatisticsInfo_EnterCorrectStartDateValidation);
                        return false;
                    }
                }
            }
            if (SearchCreditRequest_processedDateToTextBox.Text != string.Empty &&
              SearchCreditRequest_processedDateFromTextBox.Text != string.Empty)
            {
                if (SearchCreditRequest_processedDateToMaskedEditValidator.IsValid
                    && SearchCreditRequest_processedDateFromMaskedEditValidator.IsValid)
                {
                    DateTime fromDate = Convert.ToDateTime
                    (SearchCreditRequest_processedDateFromTextBox.Text.Trim());
                    DateTime toDate = Convert.ToDateTime
                        (SearchCreditRequest_processedDateToTextBox.Text.Trim());

                    if (toDate < fromDate)
                    {
                        base.ShowMessage(SearchCreditRequest_topErrorMessageLabel, SearchCreditRequest_bottomErrorMessageLabel,
                         Resources.HCMResource.SearchCreditRequest_EnterCorrectProcessedEndDate);
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            SearchCreditRequest_processCreditRequestPanel_errorLabel.Text = string.Empty;

            //Check the remarks label. It should not be empty
            if (SearchCreditRequest_processCreditRequestPanel_remarksTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(SearchCreditRequest_processCreditRequestPanel_errorLabel, "Remarks cannot be empty");
                return false;
            }

            //If the approval is for single request check the amount text box
            if (SearchCreditRequest_processCreditRequestPanel_amountDiv.Style["display"] == "block")
            {
                if (SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked &&
               SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(SearchCreditRequest_processCreditRequestPanel_errorLabel, "Please enter amount");
                    return false;
                }
                if (SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked &&
                SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text.Trim().Length > 0 &&
                decimal.Parse(SearchCreditRequest_processCreditRequestPanel_amountTextBox.Text.Trim()) <= 0)
                {
                    base.ShowMessage(SearchCreditRequest_processCreditRequestPanel_errorLabel, "Amount should be greater than 0");
                    return false;
                }
            }


            //If the grid div is not visible return true
            if (SearchCreditRequest_processCreditRequestPanel_tdDiv.Style["display"] == "none")
                return true;

            if (!SearchCreditRequest_processCreditRequestPanel_approvedCheckBox.Checked)
            {
                return true;
            }
            else
            {
                //Check each amount field in grid view . It should be greater than 0
                foreach (GridViewRow row in SearchCreditRequest_processCreditRequestPanel_processGridView.Rows)
                {
                    TextBox amountTextBox = (TextBox)row.FindControl
                        ("SearchCreditRequest_processCreditRequestPanel_processGridView_amountTextBox");

                    if (amountTextBox.Text.Trim().Length == 0)
                    {
                        base.ShowMessage(SearchCreditRequest_processCreditRequestPanel_errorLabel, "Amount should be greater than 0");
                        return false;
                    }

                    decimal amount = decimal.Parse(amountTextBox.Text.Trim());

                    if (amount <= 0)
                    {
                        base.ShowMessage(SearchCreditRequest_processCreditRequestPanel_errorLabel, "Amount should be greater than 0");
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods       
    }
}
