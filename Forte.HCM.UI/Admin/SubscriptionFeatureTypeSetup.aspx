﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionFeatureTypeSetup.aspx.cs"
    MaintainScrollPositionOnPostback="true" Inherits="Forte.HCM.UI.Admin.SubscriptionFeatureTypeSetup"
    MasterPageFile="~/MasterPages/SiteAdminMaster.Master" %>

<%@ Register Src="../CommonControls/AddNewSubscriptionType.ascx" TagName="AddNewSubscriptionType"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/AddFeatureSubscriptionType.ascx" TagName="AddFeatureSubscriptionType"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteAdminMaster_body" runat="server">
    <asp:UpdatePanel ID="Forte_SubscriptionFeatureTypeSetup_UpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td>
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="header_bg" align="right">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" class="header_text_bold">
                                                <asp:Literal ID="Forte_SubscriptionFeatureTypeSetup_subsciptionTypeAndFeatureTypeHeaderLiteral"
                                                    runat="server" Text="Subscription & Feature Types Setup"></asp:Literal>
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_SubscriptionFeatureTypeSetup_topResetLinkButton" runat="server"
                                                                Text="Reset" OnClick="ResetLinkButtonclick" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_SubscriptionFeatureTypeSetup_topCancelLinkButton" runat="server"
                                                                Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <%--<asp:UpdatePanel ID="Forte_SubscriptionFeatureTypeSetup_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>--%>
                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage" EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage" EnableViewState="false"></asp:Label>
                        <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Literal ID="Forte_SubscriptionFeatureTypeSetup_subscriptionTypeHeaderLLiteral"
                                                    runat="server" Text="Subscription Types"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <div id="Forte_SubscriptionFeatureTypeSetup_subscriptionTypeGridViewDiv" style="overflow: auto;
                                                                height: 125px;">
                                                                <asp:UpdatePanel ID="Forte_SubscriptionFeatureTypeSetup_subscriptionTypeGridViewUpatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <input type="hidden" id="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionId"
                                                                            runat="server" />
                                                                        <asp:GridView ID="Forte_SubscriptionFeatureTypeSetup_subscriptionTypeGridview" runat="server"
                                                                            AutoGenerateColumns="false" SkinID="sknWrapHeaderGrid" Width="100%" OnRowCommand="Forte_SubscriptionFeatureTypeSetup_subscriptionTypeGridview_RowCommand">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemStyle Width="8%" />
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="Forte_SubscriptionFeatureTypeSetup_editSubscriptionTypeImageButton"
                                                                                            runat="server" ToolTip="Edit Subscription" SkinID="sknViewCredits" CommandName="Edit Subscription"
                                                                                            CommandArgument='<%# Eval("SubscriptionId") %>' />
                                                                                        <asp:ImageButton ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeImageButton"
                                                                                            runat="server" ToolTip="Delete Subscription" SkinID="sknDeleteImageButton" CommandName="Delete Subscription"
                                                                                            CommandArgument='<%# Eval("SubscriptionId") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Name">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_subscriptionNameLabel" runat="server"
                                                                                            Text='<%# Eval("SubscriptionName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Description">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_subscriptionDescriptionLabel" runat="server"
                                                                                            Text='<%# Eval("SubscriptionDescription") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Published">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_subscriptionPublishStatus" runat="server"
                                                                                            Text='<%# Eval("Published") %>'></asp:Label>
                                                                                        <asp:HiddenField ID="Forte_SubscriptionFeatureTypeSetup_roleIDHiddenField" runat="server"
                                                                                            Value='<%# Eval("RoleIDs") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:LinkButton ID="Forte_SubscriptionFeatureTypeSetup_addSubscriptionTypeLinkButton"
                                                                runat="server" SkinID="sknAddLinkButton" Text="Add" OnClick="Forte_SubscriptionFeatureTypeSetup_addSubscriptionTypeLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="Forte_SubscriptionFeatureTypeSetup_pnl" runat="server" DefaultButton="Forte_SubscriptionFeatureTypeSetup_featureTypesTopSaveButton">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="header_text_bold">
                                                                <asp:Literal ID="Forte_SubscriptionFeatureTypeSetup_featureTypeHeadLLiteral" runat="server"
                                                                    Text="Feature Types"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="Forte_SubscriptionFeatureTypeSetup_featureTypesTopSaveButton" runat="server"
                                                                                Text="Save" SkinID="sknButtonId" ValidationGroup="a" OnClick="FeatureSubscriptionType_SaveClick" />
                                                                            <div style="display: none">
                                                                                <asp:TextBox ID="Forte_SubscriptionFeatureTypeSetup_hiddenTextBox" runat="server"></asp:TextBox>
                                                                                <asp:Button ID="Forte_SubscriptionFeatureTypeSetup_hiddenButton" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="grid_body_bg">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                        <tr>
                                                            <td align="left">
                                                                <div id="Forte_SubscriptionFeatureTypeSetup_featuresDiv" style="overflow: auto;">
                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                        <ContentTemplate>
                                                                            <input type="hidden" id="Forte_SubscriptionFeatureTypeSetup_deleteFeatureHiddenType"
                                                                                runat="server" />
                                                                            <asp:GridView ID="Forte_SubscriptionFeatureTypeSetup_featureTypeGridView" runat="server"
                                                                                AutoGenerateColumns="false" SkinID="sknNewGridView" Width="100%" OnRowCommand="Forte_SubscriptionFeatureTypeSetup_featureTypeGridView_RowCommand">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="Forte_SubscriptionFeatureTypeSetup_deleteFeatureImageButton"
                                                                                                runat="server" ToolTip="Delete Feature" SkinID="sknDeleteImageButton" CommandName="Delete Feature"
                                                                                                CommandArgument='<%# Eval("FeatureId") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="7%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Image ID="Forte_SubscriptionFeatureTypeSetup_featureImage" runat="server" SkinID="sknFeatureTypeImage" />
                                                                                            <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_rowNumberHeaderLabel" runat="server"
                                                                                                Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                                            <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_rowNumberDotHeaderLabel" runat="server"
                                                                                                Text="."></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <HeaderTemplate>
                                                                                            <div style="float: left; width: 100%;">
                                                                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                                    <tr>
                                                                                                        <td style="height: 5px;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 40%" align="center">
                                                                                                            Feature Name <span class="mandatory">*</span>
                                                                                                        </td>
                                                                                                        <td align="center" style="width: 30%">
                                                                                                            Default Value
                                                                                                        </td>
                                                                                                        <td align="center" style="width: 20%">
                                                                                                            Unit <span class="mandatory">*</span>
                                                                                                        </td>
                                                                                                        <td align="center" style="width: 10%">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </HeaderTemplate>
                                                                                        <ItemStyle Width="93%" />
                                                                                        <ItemTemplate>
                                                                                            <div style="float: left; width: 100%;">
                                                                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                                    <tr>
                                                                                                        <td style="height: 5px;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="center" style="height: 40px; width: 100%;">
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td style="width: 40%" align="left">
                                                                                                                        <asp:TextBox ID="Forte_SubscriptionFeatureTypeSetup_featureNameTextBox" runat="server"
                                                                                                                            Text='<%# Eval("FeatureName") %>' Width="325px" MaxLength="200"></asp:TextBox>
                                                                                                                        <asp:RequiredFieldValidator ID="Forte_SubscriptionFeatureTypeSetup_featureNameRequiredFieldValidator"
                                                                                                                            runat="server" ControlToValidate="Forte_SubscriptionFeatureTypeSetup_featureNameTextBox"
                                                                                                                            ErrorMessage="Enter feature name" ValidationGroup="a" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                                                        <ajaxToolKit:ValidatorCalloutExtender ID="Forte_SubscriptionFeatureTypeSetup_featureNameValidatorCalloutExtender"
                                                                                                                            runat="server" TargetControlID="Forte_SubscriptionFeatureTypeSetup_featureNameRequiredFieldValidator"
                                                                                                                            PopupPosition="Right" CssClass="customCalloutStyle">
                                                                                                                        </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_featureIdHiddenLabel" runat="server"
                                                                                                                            Visible="false" Text='<%# Eval("FeatureId") %>'></asp:Label>
                                                                                                                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_isUnlimitedLabel" runat="server"
                                                                                                                            Text='<%# Eval("IsUnlimited") %>' Visible="false"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td align="center" style="width: 30%">
                                                                                                                        <asp:TextBox ID="Forte_SubscriptionFeatureTypeSetup_featureDefaultValueTextBox" runat="server"
                                                                                                                            Width="120px" Text='<%# Eval("DefaultValue") %>' MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td align="left" style="width: 20%">
                                                                                                                        <asp:DropDownList ID="Forte_SubscriptionFeatureTypeSetup_featureUnitValueDropDownList"
                                                                                                                            runat="server" Width="150px">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_featureUnitValue" runat="server"
                                                                                                                            Text='<%# Eval("Unit") %>' SkinID="sknLabelFieldText" Visible="false"></asp:Label>
                                                                                                                        <asp:RequiredFieldValidator ID="Forte_SubscriptionFeatureTypeSetup_featureUnitRequiredFieldValidator"
                                                                                                                            runat="server" ErrorMessage="Select feature unit value" Display="None" ValidationGroup="a"
                                                                                                                            ControlToValidate="Forte_SubscriptionFeatureTypeSetup_featureUnitValueDropDownList"
                                                                                                                            InitialValue="0" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                                                        <ajaxToolKit:ValidatorCalloutExtender ID="Forte_SubscriptionFeatureTypeSetup_featureUnitValidatorCalloutExtender"
                                                                                                                            runat="server" TargetControlID="Forte_SubscriptionFeatureTypeSetup_featureUnitRequiredFieldValidator"
                                                                                                                            PopupPosition="Right" CssClass="customCalloutStyle">
                                                                                                                        </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                                    </td>
                                                                                                                    <td align="left" style="width: 10%">
                                                                                                                        <asp:CheckBox ID="Forte_SubscriptionFeatureTypeSetup_isUnlimitedCheckBox" runat="server"
                                                                                                                            Text="Unlimited" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="td_height_5">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <%--<tr>
                                                                                                    <td style="height: 8px;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="height: 8px;">
                                                                                                    </td>
                                                                                                </tr>--%>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:LinkButton ID="Forte_SubscriptionFeatureTypeSetup_addFeatureLinkButton" runat="server"
                                                                    SkinID="sknAddLinkButton" Text="Add" OnClick="Forte_SubscriptionFeatureTypeSetup_addFeatureLinkButton_Click"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="header_text_bold">
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="Forte_SubscriptionFeatureTypeSetup_featureTypesBottomSaveButton"
                                                                                runat="server" Text="Save" SkinID="sknButtonId" ValidationGroup="a" OnClick="FeatureSubscriptionType_SaveClick" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage" EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_SubscriptionFeatureTypeSetup_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage" EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg" align="right">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Forte_SubscriptionFeatureTypeSetup_bottonResetLinkButton" runat="server"
                                                    Text="Reset" OnClick="ResetLinkButtonclick" SkinID="sknActionLinkButton"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Forte_SubscriptionFeatureTypeSetup_bottomCancelLinkButton" runat="server"
                                                    Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%--<asp:UpdatePanel ID="Forte_SubscriptionFeatureTypeSetup_popUpUpdatePanel" runat="server">
        <ContentTemplate>--%>
            <table>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Forte_SubscriptionFeatureTypeSetup_newSubscriptionTypePanel" runat="server"
                            Style="display: none" Height="280px" CssClass="popupcontrol_subscription_types">
                            <uc1:AddNewSubscriptionType ID="Forte_SubscriptionFeatureTypeSetup_addNewSubscriptionType"
                                runat="server" />
                        </asp:Panel>
                        <div style="display: none">
                            <asp:Button ID="Forte_SubscriptionFeatureTypeSetup_addSubscriptionTypeHiddenButton"
                                runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
            <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeatureTypeSetup_addNweSubscriptionTypeModalPopupExtender"
                runat="server" PopupControlID="Forte_SubscriptionFeatureTypeSetup_newSubscriptionTypePanel"
                TargetControlID="Forte_SubscriptionFeatureTypeSetup_addSubscriptionTypeHiddenButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionPanel" runat="server"
                            Style="display: none" CssClass="popupcontrol_subscription_types_popup">
                            <uc3:AddFeatureSubscriptionType ID="Forte_SubscriptionFeatureTypeSetup_AddFeatureSubscriptionType"
                                runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <div style="display: none;">
                <asp:Button ID="Forte_SubscriptionFeatureTypeSetUp_addFeatureTypeHiddenButton" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionModalPopupExtender"
                runat="server" PopupControlID="Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionPanel"
                TargetControlID="Forte_SubscriptionFeatureTypeSetUp_addFeatureTypeHiddenButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypehiddenDIV" runat="server"
        style="display: none">
        <asp:Button ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypehiddenPopupModalButton"
            runat="server" />
    </div>
    <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeModalPopupExtender"
        runat="server" PopupControlID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeConfrimPopUpPanel"
        TargetControlID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypehiddenPopupModalButton"
        BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeConfrimPopUpPanel"
        runat="server" Style="display: none" Height="210px" CssClass="popupcontrol_confirm">
        <uc2:ConfirmMsgControl ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeconfirmPopupExtenderControl"
            runat="server" OnOkClick="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeokPopUpClick"
            Message="Are you sure you want to delete?" Title="Warning" Type="YesNo" OnCancelClick="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypecancelPopUpClick" />
    </asp:Panel>
    <div id="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypehiddenDIV"
        runat="server" style="display: none">
        <asp:Button ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypehiddenPopupModalButton"
            runat="server" />
    </div>
    <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatuerTypeModalPopupExtender"
        runat="server" PopupControlID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypeConfrimPopUpPanel"
        TargetControlID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypehiddenPopupModalButton"
        BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypeConfrimPopUpPanel"
        runat="server" Style="display: none" Height="210px" CssClass="popupcontrol_confirm">
        <uc2:ConfirmMsgControl ID="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypeconfirmPopupExtenderControl"
            runat="server" Message="Are you sure you want to delete?" Title="Warning" Type="YesNo"
            OnOkClick="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypeokPopUpClick"
            OnCancelClick="Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypecancelPopUpClick" />
    </asp:Panel>
    <script type="text/javascript">
        function UnlimitedClick(textbox, checkbox) {
            if (document.getElementById(checkbox).checked) {
                document.getElementById(textbox).disabled = true;
                document.getElementById(textbox).value = "";
            }
            else
                document.getElementById(textbox).disabled = false;
        }
    </script>
</asp:Content>
