<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompetencyVectors.aspx.cs"
    MasterPageFile="~/MasterPages/OTMMaster.Master" Inherits="Forte.HCM.UI.Admin.CompetencyVectors" %>

<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<asp:Content ID="CompetencyVectors_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">

    <script type="text/javascript">
        function ShowCompetencyVectorGroupsConfirm() {
            //displays Vector Group pop up extender
            $find("<%= CompetencyVectors_deleteVectorGroup_ModalPopupExtender.ClientID %>").show();
            return false;
        }
        function ShowCompetencyVectorConfirm() {
            //displays Vector pop up extender
            $find("<%= CompetencyVectors_deleteCompetencyVectorModalPopupExtender.ClientID %>").show();
            return false;
        }
        function ShowVectorParameterConfirm() {
            //displays parameter pop up extender
            $find("<%= CompetencyVectors_VectorParameterModalPopupExtender.ClientID %>").show();
            return false;
        }

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CompetencyVectors_headerLiteral" runat="server" Text="Competency Vectors"></asp:Literal>
                            <asp:HiddenField ID="CompetencyVectors_browserHiddenField" runat="server" />
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr align="right">
                                    <td>
                                        <asp:LinkButton ID="CompetencyVectors_topResetLinkButton" Text="Reset" runat="server"
                                            SkinID="sknActionLinkButton" OnClick="CompetencyVectors_topResetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CompetencyVectors_topCancelLinkButton" runat="server" Text="Cancel"
                                            PostBackUrl="~/OTMHome.aspx" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="CompetencyVectors_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="CompetencyVectors_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" border="0" cellpadding="2" cellspacing="3">
                    <tr>
                        <td class="header_bg">
                            <table width="100%" border="0"cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_text_bold">
                                        <asp:Literal ID="CompetencyVectors_competencyVectorGroupsLiteral" runat="server"
                                            Text="Competency Vector Groups"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CompetencyVectors_competencyVectorGroupsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="2" cellspacing="3" border="0">
                                                    <tr>
                                                        <td style="width: 10%;">
                                                        
                                                            <asp:ListBox ID="CompetencyVectors_competencyVectorGroupsListBox" SelectionMode="Single" 
                                                                runat="server" Width="400px" OnSelectedIndexChanged="CompetencyVectors_competencyVectorGroupsListBox_SelectedIndexChanged"
                                                                AutoPostBack="True" SkinID="sknListTextBox"></asp:ListBox>
                                                        </td>
                                                        <td valign="bottom">
                                                            <div style="float: left; padding-left: 6px;">
                                                                <asp:LinkButton ID="CompetencyVectors_competencyVectorGroupsDeleteLinkButton" runat="server"
                                                                    Text="Delete" ToolTip="Click here to delete the selected competency vector group"
                                                                    OnClientClick="javascript:return ShowCompetencyVectorGroupsConfirm();" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table cellpadding="2" cellspacing="3" border="0">
                                            <tr>
                                                <td style="width: 120px">
                                                    <asp:Label ID="CompetencyVectors_newCompetencyVectorGroupLabel" runat="server" Text="New Group"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 120px">
                                                    <asp:TextBox ID="CompetencyVectors_newCompetencyVectorGroupTextBox" MaxLength="50"
                                                        runat="server"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="CompetencyVectors_newCompetencyVectorGroupFilteredTextBox"
                                                        runat="server" TargetControlID="CompetencyVectors_newCompetencyVectorGroupTextBox"
                                                        FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars="1234567890 _">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 120px">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:LinkButton ID="CompetencyVectors_newCompetencyVectorGroupLinkButton" runat="server"
                                                            Text="Add New" SkinID="sknActionLinkButton" OnClick="CompetencyVectors_newCompetencyVectorGroupLinkButton_Click"
                                                            ToolTip="Click here to add the newly entered competency vector group"></asp:LinkButton>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="CompetencyVectors_newCompetencyVectorGroupImageButton" SkinID="sknHelpImageButton"
                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Click here to add the newly entered competency vector group" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Literal ID="CompetencyVectors_competencyVectorsLiteral" runat="server" Text="Competency Vectors"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CompetencyVectors_competencyVectorsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="2" cellspacing="3">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <asp:ListBox ID="CompetencyVectors_competencyVectorsListBox" SelectionMode="Multiple"
                                                                runat="server" Width="400px" SkinID="sknListTextBox"></asp:ListBox>
                                                        </td>
                                                        <td valign="bottom">
                                                            <div style="float: left; padding-left: 6px;">
                                                                <asp:LinkButton ID="CompetencyVectors_competencyVectorDeleteLinkButton" runat="server"
                                                                    ToolTip="Click here to delete the selected competency vector" Text="Delete" OnClientClick="javascript:return ShowCompetencyVectorConfirm();"
                                                                    SkinID="sknActionLinkButton"></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="2" cellspacing="3" border="0">
                                            <tr>
                                                <td style="width: 120px">
                                                    <asp:Label ID="CompetencyVectors_newCompetencyVectorLabel" runat="server" Text="New Vector"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 120px">
                                                    <asp:TextBox ID="CompetencyVectors_newCompetencyVectorTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="CompetencyVectors_newCompetencyVectorFilteredTextBoxExtender"
                                                        runat="server" TargetControlID="CompetencyVectors_newCompetencyVectorTextBox"
                                                        FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars="1234567890 _">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:LinkButton ID="CompetencyVectors_newCompetencyVectorLinkButton" runat="server"
                                                            Text="Add New" SkinID="sknActionLinkButton" OnClick="CompetencyVectors_newCompetencyVectorLinkButton_Click"
                                                            ToolTip="Click here to add the newly entered competency vector"></asp:LinkButton>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="CompetencyVectors_newCompetencyVectorImageButton" SkinID="sknHelpImageButton"
                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Click here to add the newly entered competency vector" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 6%;">
                                        <asp:Literal ID="CompetencyVectors_vectorParametersLiteral" runat="server" Text="Vector Parameters"></asp:Literal>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CompetencyVectors_vectorParametersUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="2" cellspacing="3">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <asp:ListBox ID="CompetencyVectors_vectorParametersListBox" SelectionMode="Multiple"
                                                                runat="server" Width="400px" SkinID="sknListTextBox"></asp:ListBox>
                                                        </td>
                                                        <td valign="bottom">
                                                            <div style="float: left; padding-left: 6px;">
                                                                <asp:LinkButton ID="CompetencyVectors_vectorParametersDeleteLinkButton" runat="server"
                                                                    ToolTip="Click here to delete the selected vector parameter" SkinID="sknActionLinkButton"
                                                                    Text="Delete" OnClientClick="javascript:return ShowVectorParameterConfirm();"></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 120px">
                                                    <asp:Label ID="CompetencyVectors_newVectorParameterLabel" runat="server" Text="New Parameter"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 120px">
                                                    <asp:TextBox ID="CompetencyVectors_newVectorParameterTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="CompetencyVectors_newVectorParameterFilteredTextBoxExtender"
                                                        runat="server" TargetControlID="CompetencyVectors_newVectorParameterTextBox"
                                                        FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars="1234567890 _">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 120px" align="left">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:LinkButton ID="CompetencyVectors_newVectorParameterLinkButton" runat="server"
                                                            Text="Add New" SkinID="sknActionLinkButton" OnClick="CompetencyVectors_newVectorParameterLinkButton_Click"
                                                            ToolTip="Click here to add the newly entered vector parameter"></asp:LinkButton>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="CompetencyVectors_newVectorParameterImageButton" SkinID="sknHelpImageButton"
                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Click here to add the newly entered vector parameter" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="CompetencyVectors_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CompetencyVectors_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table align="right" cellpadding="0" cellspacing="4" border="0">
                    <tr>
                        <td>
                            <asp:LinkButton ID="CompetencyVectors_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="CompetencyVectors_bottomResetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="CompetencyVectors_bottomCancelLinkButton" Text="Cancel" runat="server"
                                PostBackUrl="~/OTMHome.aspx" SkinID="sknActionLinkButton"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="CompetencyVectors_deleteVectorGroupPanel" runat="server" Style="display: none;
                    height: 250px;" CssClass="popupcontrol_confirm">
                    <div id="CompetencyVectors_deleteVectorGroupDiv" style="display: none">
                        <asp:Button ID="CompetencyVectors_deleteVectorGroup_hiddenButton" runat="server" />
                    </div>
                    <uc3:ConfirmMsgControl ID="CompetencyVectors_deleteVectorGroup_ConfirmMsgControl"
                        runat="server" Title="Delete Competency Vector Group" Type="YesNo" Message="Are you sure to delete competency vector group ?" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="CompetencyVectors_deleteVectorGroup_ModalPopupExtender"
                    runat="server" PopupControlID="CompetencyVectors_deleteVectorGroupPanel" TargetControlID="CompetencyVectors_deleteVectorGroup_hiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="CompetencyVectors_deleteCompetencyVectorPanel" runat="server" Style="display: none;
                    height: 205px;" CssClass="popupcontrol_confirm">
                    <div id="CompetencyVectors_deleteCompetencyVectorDiv" style="display: none">
                        <asp:Button ID="CompetencyVectors_deleteCompetencyVectorhiddenButton" runat="server" />
                    </div>
                    <uc3:ConfirmMsgControl ID="CompetencyVectors_deleteCompetencyVector_ConfirmMsgControl"
                        runat="server" Type="YesNo" Title="Delete Competency Vector" Message="Are you sure to delete competency vector(s) ?" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="CompetencyVectors_deleteCompetencyVectorModalPopupExtender"
                    runat="server" PopupControlID="CompetencyVectors_deleteCompetencyVectorPanel"
                    TargetControlID="CompetencyVectors_deleteCompetencyVectorhiddenButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="CompetencyVectors_VectorParameterPanel" runat="server" Style="display: none;
                    height: 205px;" CssClass="popupcontrol_confirm">
                    <div id="CompetencyVectors_VectorParameterDiv" style="display: none">
                        <asp:Button ID="CompetencyVectors_VectorParameterhiddenButton" runat="server" />
                    </div>
                    <uc3:ConfirmMsgControl ID="CompetencyVectors_deleteVectorParameter_ConfirmMsgControl"
                        runat="server" Title="Delete Vector Parameters" Type="YesNo" Message="Are you sure to delete vector parameter(s) ?" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="CompetencyVectors_VectorParameterModalPopupExtender"
                    runat="server" PopupControlID="CompetencyVectors_VectorParameterPanel" TargetControlID="CompetencyVectors_VectorParameterhiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
