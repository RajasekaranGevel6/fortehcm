﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// FeatureManagement.aspx.cs
// File that represents the FeatureManagement class that defines the user 
// interface layout and functionalities for the FeatureManagement page. This 
// page helps in managing the features. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
#endregion Directives


namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the FeatureManagement page. This page helps in managing the features
    /// This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class FeatureManagement : PageBase
    {
        #region Private Constants
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";
        #endregion Private Constants

        #region Event Handlers
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Set the page caption in the page
            Master.SetPageCaption("Feature Management");

            FeatureManagement_topSuccessMessageLabel.Text = string.Empty;
            FeatureManagement_bottomSuccessMessageLabel.Text = string.Empty;
            FeatureManagement_topErrorMessageLabel.Text = string.Empty;
            FeatureManagement_bottomErrorMessageLabel.Text = string.Empty;
            FeatureManagement_featurePageNavigator.PageNumberClick +=
                new CommonControls.PageNavigator.PageNumberClickEventHandler
                    (FeatureManagement_featurePageNavigator_PageNumberClick);
            if (!IsPostBack)
            {
                //Load the default values
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "MODULE";
                BindFeaturesGridView(1);
            }

        }
        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of features.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureManagement_featurePageNavigator_PageNumberClick
                (object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                ViewState["pagenumber"] = Convert.ToInt32(e.PageNumber);
                FeatureManagement_featureGridView.EditIndex = -1;
                FeatureManagement_featurePageNumberHidden.Value = e.PageNumber.ToString();
                BindFeaturesGridView(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureManagement_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when add image button is clicked
        /// to add new feature.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureManagement_featureAddImageButton_Click
                (object sender, EventArgs e)
        {
            try
            {
                FeatureManagement_updateFeatureNameErrorLabel.Text = null;
                if (FeatureManagement_featureGridView.EditIndex != -1)
                {
                    base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                  FeatureManagement_bottomErrorMessageLabel, Resources.HCMResource.
                  ModuleManagement_PleaseUpdateModule);
                    return;
                }
                FeatureManagement_updateFeatureIsEditORSaveHiddenField.Value = "ADDNEW";
                FeatureManagement_updateFeatureLiteral.Text = "Add Feature";
                FeatureManagement_updateFeatureNameTextBox.Focus();
                FeatureManagement_updateFeatureNameTextBox.Text = string.Empty;
                FeatureManagement_updateFeatureURLTextBox.Text = string.Empty;
                FeatureManagement_updateFeaureeModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the cancel edit action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureManagement_featureGridView_RowCancelingEdit
                (object sender, GridViewCancelEditEventArgs e)
        {
            try
            {

                FeatureManagement_topSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_bottomSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_topErrorMessageLabel.Text = string.Empty;
                FeatureManagement_bottomErrorMessageLabel.Text = string.Empty;

                if (FeatureManagement_featureGridView.EditIndex != -1)
                {
                    base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                        FeatureManagement_bottomErrorMessageLabel, Resources.HCMResource.
                        FeatureManagemnt_PleaseUpdateTheFeature);
                    return;
                }


                FeatureManagement_featureGridView.EditIndex = -1;
                BindFeaturesGridView(Convert.ToInt32
                    (FeatureManagement_featurePageNumberHidden.Value));
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_editImageButton")).Visible = true;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_deleteImageButton")).Visible = true;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_cancelUpdateImageButton")).Visible = false;
                FeatureManagement_deleteHiddenField.Value = "";


            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row editing action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureManagement_featureGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                FeatureManagement_topSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_bottomSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_topErrorMessageLabel.Text = string.Empty;
                FeatureManagement_bottomErrorMessageLabel.Text = string.Empty;



                FeatureManagement_featureGridView.EditIndex = e.NewEditIndex;
                BindFeaturesGridView(Convert.ToInt32(FeatureManagement_featurePageNumberHidden.Value));
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                    FindControl("FeatureManagement_featureGridView_UpdateImageButton")).Visible = true;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                    FindControl("FeatureManagement_featureGridView_editImageButton")).Visible = false;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                    FindControl("FeatureManagement_featureGridView_deleteImageButton")).Visible = false;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                    FindControl("FeatureManagement_featureGridView_cancelUpdateImageButton")).Visible = true;


                FeatureManagement_deleteHiddenField.Value =
                    ((HiddenField)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                        FindControl("FeatureManagement_editfeaturenameIDHiddenField")).Value;

                //((ImageButton)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                //    FindControl("FeatureManagement_featureGridView_UpdateImageButton")).b();

                //Page.Form.DefaultButton = ((ImageButton)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                //    FindControl("FeatureManagement_featureGridView_UpdateImageButton")).UniqueID;
                //Page.SetFocus(((ImageButton)FeatureManagement_featureGridView.Rows[e.NewEditIndex].
                //    FindControl("FeatureManagement_featureGridView_UpdateImageButton")).ClientID);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row updating action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureManagement_featureGridView_RowUpdating
            (object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                FeatureManagement_featureGridView.EditIndex = -1;

                FeatureManagement_topSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_bottomSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_topErrorMessageLabel.Text = string.Empty;
                FeatureManagement_bottomErrorMessageLabel.Text = string.Empty;

                int rowAffected = 0;

                rowAffected = new AdminBLManager().UpdateFeature(
                          ((TextBox)FeatureManagement_featureGridView.Rows[e.RowIndex].
                        FindControl("FeatureManagement_moduleGridView_editFeatureNameTextBox")).Text,
                        ((TextBox)FeatureManagement_featureGridView.Rows[e.RowIndex].
                        FindControl("FeatureManagement_moduleGridView_editFeatureURLTextBox")).Text,
                        Convert.ToInt32(FeatureManagement_deleteHiddenField.Value), base.userID);


                if (rowAffected == 0)
                    base.ShowMessage(FeatureManagement_topSuccessMessageLabel,
                     FeatureManagement_bottomSuccessMessageLabel, Resources.HCMResource.
                     FeatureManagement_FeatureUpdatedSuccessfully);
                else

                    base.ShowMessage(FeatureManagement_topSuccessMessageLabel,
                     FeatureManagement_bottomSuccessMessageLabel,
                     Resources.HCMResource.FeatureManagement_FeatureAlreadyExists);


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
            finally
            {


                BindFeaturesGridView(Convert.ToInt32(FeatureManagement_featurePageNumberHidden.Value));
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_editImageButton")).Visible = true;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_deleteImageButton")).Visible = true;
                ((ImageButton)FeatureManagement_featureGridView.Rows[e.RowIndex].
                    FindControl("FeatureManagement_featureGridView_cancelUpdateImageButton")).Visible = false;
                FeatureManagement_deleteHiddenField.Value = "";




            }
        }
        /// <summary>
        /// Handler method that will be called when the delete action with row command
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureManagement_featureGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                FeatureManagement_topSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_bottomSuccessMessageLabel.Text = string.Empty;
                FeatureManagement_topErrorMessageLabel.Text = string.Empty;
                FeatureManagement_bottomErrorMessageLabel.Text = string.Empty;

                if (FeatureManagement_featureGridView.EditIndex != -1)
                {
                    base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                        FeatureManagement_bottomErrorMessageLabel, Resources.HCMResource.
                        FeatureManagemnt_PleaseUpdateTheFeature);
                    return;
                }

                if (e.CommandName != "DeleteFeature")
                    return;

                FeatureManagement_deleteHiddenField.Value = e.CommandArgument.ToString();

                FeatureManagement_deleteFeatureConfirmMsgControl.Message = string.Format
                    (Resources.HCMResource.FeatureManagement_FeatureDeleteConfirmation,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("FeatureManagement_featuerGridView_modulenameLabel")).Text);



                FeatureManagement_deleteFeatureConfirmMsgControl.Type = MessageBoxType.YesNo;
                FeatureManagement_deleteFeatureConfirmMsgControl.Title = "Warning";
                FeatureManagement_deleteFeatureModalPopupExtender.Show();


            }

            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureManagement_featureGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (FeatureManagement_featureGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureManagement_featureGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            //Assign the sorting and sort order
            if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
            {
                ViewState["SORT_ORDER"] =
                    ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                    SortType.Descending : SortType.Ascending;
            }
            else
                ViewState["SORT_ORDER"] = SortType.Ascending;

            ViewState["SORT_FIELD"] = e.SortExpression;
            FeatureManagement_featurePageNavigator.Reset();
            BindFeaturesGridView(1);
        }
        /// <summary>
        /// Handler method that will be called when the save is clicked
        /// in adding new feature.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureManagement_updateFeatureNameSaveButton_Clicked
         (object sender, EventArgs e)
        {
            FeatureManagement_updateFeatureNameErrorLabel.Text = null;
            try
            {

                if (FeatureManagement_updateModuleDropdownList.SelectedIndex == 0)
                {
                    base.ShowMessage(FeatureManagement_updateFeatureNameErrorLabel,
                        Resources.HCMResource.FeatureManagement_SelectModule);


                    FeatureManagement_updateFeaureeModalPopupExtender.Show();

                    return;
                }

                if (FeatureManagement_updateSubModuleDropdownList.SelectedIndex == 0)
                {
                    base.ShowMessage(FeatureManagement_updateFeatureNameErrorLabel,
                        Resources.HCMResource.FeatureManagement_SelectSubModule);

                    FeatureManagement_updateFeaureeModalPopupExtender.Show();

                    return;
                }

                if (FeatureManagement_updateFeatureNameTextBox.Text.Length == 0)
                {
                    base.ShowMessage(FeatureManagement_updateFeatureNameErrorLabel,
                        Resources.HCMResource.FeatureManagement_EnterFeatureName);

                    FeatureManagement_updateFeaureeModalPopupExtender.Show();

                    return;
                }
                if (FeatureManagement_updateFeatureURLTextBox.Text.Length == 0)
                {
                    base.ShowMessage(FeatureManagement_updateFeatureNameErrorLabel,
                        Resources.HCMResource.FeatureManagement_EnterFeatureURL);

                    FeatureManagement_updateFeaureeModalPopupExtender.Show();

                    return;

                }
                if (FeatureManagement_updateFeatureIsEditORSaveHiddenField.Value.ToUpper() == "ADDNEW")
                {

                    FeatureManagement_updateFeatureNameTextBox.Focus();
                    int rowsAffected = 0;


                    rowsAffected = new AdminBLManager().InsertNewFeature(
                        Convert.ToInt32(FeatureManagement_updateModuleDropdownList.SelectedValue),
                        Convert.ToInt32(FeatureManagement_updateSubModuleDropdownList.SelectedValue),
                        FeatureManagement_updateFeatureNameTextBox.Text,
                        FeatureManagement_updateFeatureURLTextBox.Text, base.userID);


                    if (rowsAffected == 0)
                    {

                        base.ShowMessage(FeatureManagement_topSuccessMessageLabel,
                      FeatureManagement_bottomSuccessMessageLabel,
                       Resources.HCMResource.FeatureManagement_NewFeatureInsertedSuccessfully);
                    }
                    else
                    {
                        base.ShowMessage(FeatureManagement_updateFeatureNameErrorLabel,
                        Resources.HCMResource.FeatureManagement_FeatureAlreadyExists);

                        FeatureManagement_updateFeaureeModalPopupExtender.Show();
                        return;
                    }
                }

                //Reset the page navigator
                FeatureManagement_featurePageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "MODULE";

                //Get the form details
                BindFeaturesGridView(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the OK is clicked
        /// while confirming to delete the feature .
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureManagement_deleteFeatureConfirmMsgControl_okClick
          (object sender, EventArgs e)
        {
            try
            {
                int feature = int.Parse(FeatureManagement_deleteHiddenField.Value);

                new AdminBLManager().DeleteFeature(feature);

                //Reset the page navigator
                //  ModuleManagement_rolesPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "MODULE";
                base.ShowMessage(FeatureManagement_topSuccessMessageLabel,
                    FeatureManagement_bottomSuccessMessageLabel, Resources.HCMResource.
                       FeatureManagement_FeatureDeletedSuccessfully);
                BindFeaturesGridView(1);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the Cancel is clicked
        /// while canceling the delete .
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureManagement_deleteFeatureConfirmMsgControl_cancelClick
          (object sender, EventArgs e)
        {
            try
            {
                FeatureManagement_deleteHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        #endregion

        #region private Methods
        /// <summary>
        /// Method to bind the features
        /// </summary>
        /// <param name="pageNumber">holds the page number</param>
        private void BindFeaturesGridView(int pageNumber)
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            int totalPage = 0;

            List<FeatureManagementDetails> featureList = new AdminBLManager().
               GetFeatures(sortExpression, sortOrder, base.GridPageSize, pageNumber, out totalPage);

            FeatureManagement_featureGridView.DataSource = featureList;

            FeatureManagement_featureGridView.DataBind();

            FeatureManagement_featurePageNavigator.Visible = true;

            FeatureManagement_featurePageNavigator.TotalRecords = totalPage;

            FeatureManagement_featurePageNavigator.PageSize = base.GridPageSize;

            FeatureManagement_featureGridViewDIV.Style["display"] = "block";

            if (featureList == null || featureList.Count == 0)
            {
                base.ShowMessage(FeatureManagement_topErrorMessageLabel,
                    FeatureManagement_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);

                FeatureManagement_featureGridViewDIV.Style["display"] = "none";
            }

            LoadModule(featureList);

            LoadSubModule(featureList);
        }

        private void LoadSubModule(List<FeatureManagementDetails> featureList)
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            List<ModuleManagementDetails> subModuleList = new AdminBLManager().GetSubModules(sortExpression, sortOrder,
                base.GridPageSize);

            FeatureManagement_updateSubModuleDropdownList.DataSource = subModuleList;
            FeatureManagement_updateSubModuleDropdownList.DataTextField = "SubModNameCode";
            FeatureManagement_updateSubModuleDropdownList.DataValueField = "SubModuleID";
            FeatureManagement_updateSubModuleDropdownList.DataBind();
            FeatureManagement_updateSubModuleDropdownList.Items.Insert(0, new ListItem("--Select--"));
        }

        private void LoadModule(List<FeatureManagementDetails> featureList)
        {
            int totalPage = 0;

            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            List<ModuleManagementDetails> moduleList = new AdminBLManager().GetModules(sortExpression, sortOrder,
                base.GridPageSize, 1, out totalPage);

            FeatureManagement_updateModuleDropdownList.DataSource = moduleList;
            FeatureManagement_updateModuleDropdownList.DataTextField = "ModeCodeName";
            FeatureManagement_updateModuleDropdownList.DataValueField = "ModID";
            FeatureManagement_updateModuleDropdownList.DataBind();
            FeatureManagement_updateModuleDropdownList.Items.Insert(0, new ListItem("--Select--"));
        }
        #endregion private Methods

        #region Protected Overridden Methods
        protected override void LoadValues()
        {


        }
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }
        #endregion Protected Overridden Methods
    }
}