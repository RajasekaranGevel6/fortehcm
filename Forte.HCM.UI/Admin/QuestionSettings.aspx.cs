﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionSettings.cs
// File that represents the user interface for question settings page.
// This will helps to view and set the question settings such as categories, 
// subjects and test areas.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities
    /// for the question settings page. This page helps to view and set 
    /// the question settings such as categories, subjects and test areas.
    /// </summary>
    public partial class QuestionSettings : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                // Clear the error/success message label.
                QuestionSettings_bottomSuccessMessageLabel.Text = string.Empty;
                QuestionSettings_bottomErrorMessageLabel.Text = string.Empty;
                QuestionSettings_topSuccessMessageLabel.Text = string.Empty;
                QuestionSettings_topErrorMessageLabel.Text = string.Empty;

                // Show the subjects added message, when the subject added
                // status is Y'.
                if (QuestionSettings_subjectAddedHiddenField.Value != null &&
                    QuestionSettings_subjectAddedHiddenField.Value == "Y")
                {
                    ShowMessage(QuestionSettings_topSuccessMessageLabel,
                        QuestionSettings_bottomSuccessMessageLabel,
                        "Subject(s) added successfully");

                    QuestionSettings_subjectAddedHiddenField.Value = string.Empty;
                }

                QuestionSettings_newCategoryTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + QuestionSettings_addNewCategoryLinkButton.ClientID + "','CAT','" 
                    + QuestionSettings_browserHiddenField.ClientID + "');");

                QuestionSettings_newSubjectTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + QuestionSettings_addNewSubjectLinkButton.ClientID + "','SUB','"
                    + QuestionSettings_browserHiddenField.ClientID + "');");

                //QuestionSettings_newTestAreaTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                //    + QuestionSettings_addNewTestAreaLinkButton.ClientID + "','TST_AREA_ID','"
                //    + QuestionSettings_browserHiddenField.ClientID + "');");

                QuestionSettings_newTestAreaNameTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + QuestionSettings_addNewTestAreaLinkButton.ClientID + "','TST_AREA','"
                    + QuestionSettings_browserHiddenField.ClientID + "');");

                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Utility.IsNullOrEmpty(QuestionSettings_browserHiddenField.Value))
                {
                    ValidateEnterKey(QuestionSettings_browserHiddenField.Value.Trim());
                    QuestionSettings_browserHiddenField.Value = string.Empty;
                }

                Master.SetPageCaption(Resources.HCMResource.QuestionSettings_Title);

                if (!IsPostBack)
                {
                    //Load the category
                    LoadCategory();

                    //Load the Test Areas
                    LoadValues();

                    if (QuestionSettings_categoriesListBox.Items.Count == 0)
                        return;

                    //Select a first item at page load
                    QuestionSettings_categoriesListBox.Items[0].Selected = true;

                    //Load the subjects 
                    LoadSubjects(int.Parse
                        (QuestionSettings_categoriesListBox.Items[0].Value));
                }

                if (QuestionSettings_categoriesListBox.Items.Count == 0)
                    return;

                if (QuestionSettings_categoriesListBox.SelectedItem != null)
                {
                    string selectedItem = QuestionSettings_categoriesListBox.SelectedItem.Text;
                    int selectedID = int.Parse(QuestionSettings_categoriesListBox.SelectedItem.Value);
                    // Add click handler for 'Add Availabel Subject' link.
                    QuestionSettings_addAvailableSubjectsLinkButton.
                        Attributes.Add("OnClick", "javascript:return ShowSearchSubject('" +
                        selectedItem + "','" + selectedID + "');");
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when an item is selected in the 
        /// categories list box.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void QuestionSettings_categoriesListBox_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                // Get the selected value
                int categoryId = int.Parse
                        (QuestionSettings_categoriesListBox.SelectedValue);

                // Load the subject for the selected category
                LoadSubjects(categoryId);

                string selectedItem = QuestionSettings_categoriesListBox.SelectedItem.Text;
                int selectedId = int.Parse(QuestionSettings_categoriesListBox.SelectedItem.Value);

                // Add click handler for 'Add Available Subject' link.
                QuestionSettings_addAvailableSubjectsLinkButton.Attributes.Add
                    ("OnClick", "javascript:return ShowSearchSubject('" +
                    selectedItem + "','" + selectedId + "');");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(QuestionSettings_topErrorMessageLabel,
                   QuestionSettings_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when add category link button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="objects"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_addNewCategoryLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Check if the category text box is empty
                if (QuestionSettings_newCategoryTextBox.Text.Trim().Length == 0)
                {
                    // If it is, show the error message and retain the focus in the textbox itself.
                    ShowMessage(QuestionSettings_topErrorMessageLabel,
                        QuestionSettings_bottomErrorMessageLabel,
                        Resources.HCMResource.QuestionSettings_CategoryCannotBeEmpty);

                    QuestionSettings_newCategoryTextBox.Focus();
                    return;
                }

                // Check if the new category is already exist.
                if (FindByText(QuestionSettings_categoriesListBox,
                    QuestionSettings_newCategoryTextBox.Text.Trim()))
                {
                    ShowMessage(QuestionSettings_bottomErrorMessageLabel,
                        QuestionSettings_topErrorMessageLabel, string.Format(
                        "Category '{0}' already exist",
                        QuestionSettings_newCategoryTextBox.Text.Trim()));

                    QuestionSettings_newCategoryTextBox.Focus();

                    return;
                }

                // Construct the category object.
                Category category = new Category();
                category.CategoryName = QuestionSettings_newCategoryTextBox.Text.Trim();
                category.CreatedBy = userID;
                category.CreatedDate = DateTime.Now;
                category.ModifiedBy = userID;
                category.ModifiedDate = DateTime.Now;

                // Insert the category.
                new AdminBLManager().InsertCategory(category, userID, base.tenantID);


                // Reload categories.
                LoadCategory();

                // Show success message.
                ShowMessage(QuestionSettings_topSuccessMessageLabel,
                    QuestionSettings_bottomSuccessMessageLabel,
                    "Category added successfully");

                // Clear the category text box.
                QuestionSettings_newCategoryTextBox.Text = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add subject link button
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_addNewSubjectLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Check if the category is selected or not.
                if (QuestionSettings_categoriesListBox.SelectedValue == null ||
                    QuestionSettings_categoriesListBox.SelectedValue.Trim().Length == 0)
                {
                    ShowMessage(QuestionSettings_topErrorMessageLabel,
                        QuestionSettings_bottomErrorMessageLabel,
                            "Please select a category");

                    return;
                }

                // Check if the new subject is entered or not.
                if (QuestionSettings_newSubjectTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(QuestionSettings_topErrorMessageLabel,
                        QuestionSettings_bottomErrorMessageLabel,
                        "Subject cannot be empty");

                    QuestionSettings_newSubjectTextBox.Focus();

                    return;
                }

                // Check if the new subject is already exist or not.
                if (FindByText(QuestionSettings_subjectsListBox,
                    QuestionSettings_newSubjectTextBox.Text.Trim()))
                {
                    ShowMessage(QuestionSettings_bottomErrorMessageLabel,
                        QuestionSettings_topErrorMessageLabel, string.Format(
                        "Subject '{0}' already exist",
                        QuestionSettings_newSubjectTextBox.Text.Trim()));

                    QuestionSettings_newSubjectTextBox.Focus();

                    return;
                }

                // Retrieve the category ID.
                int categoryID = int.Parse(QuestionSettings_categoriesListBox.
                    SelectedValue);

                // Create and construct a subject object.
                Subject subject = new Subject();
                subject.CategoryID = categoryID;
                subject.SubjectName = QuestionSettings_newSubjectTextBox.Text.Trim();
                subject.CreatedBy = base.userID;
                subject.CreatedDate = DateTime.Now;
                subject.ModifiedBy = base.userID;
                subject.ModifiedDate = DateTime.Now;

                // Insert the subject.
                new AdminBLManager().InsertSubject(subject, base.tenantID);//base.tenantID

                // Show success message.
                ShowMessage(QuestionSettings_topSuccessMessageLabel,
                    QuestionSettings_bottomSuccessMessageLabel,
                    "Subject added successfully");

                // Load categories.
                LoadCategory();

                // Set the last selected category as default selected.
                QuestionSettings_categoriesListBox.SelectedValue = categoryID.ToString();

                // Load subjects for the selected category ID.
                LoadSubjects(categoryID);

                // Clear the subject text box.
                QuestionSettings_newSubjectTextBox.Text = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                   QuestionSettings_bottomErrorMessageLabel,
                   exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when add test area link button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_addNewTestAreaLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Check if test area ID and name is valid for save.
                if (!IsValidTestArea())
                    return;

                // Insert test area.
                new AdminBLManager().InsertTestArea(QuestionSettings_newTestAreaNameTextBox.Text.Trim(), base.tenantID, userID);

                ShowMessage(QuestionSettings_bottomSuccessMessageLabel,
                    QuestionSettings_topSuccessMessageLabel,
                    "Test area added successfully");

                // Load testarea.
                LoadValues();

                QuestionSettings_newTestAreaNameTextBox.Text = string.Empty;
                //QuestionSettings_newTestAreaTextBox.Text = string.Empty;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                   QuestionSettings_bottomErrorMessageLabel,
                   exception.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when delete subject link button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_deleteSubjectLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                QuestionSettings_deleteTypeHiddenField.Value = "S";

                QuestionSettings_deleteConfirmMsgControl.Message =
                    "Are you sure you want to delete the subject(s)? ";
                QuestionSettings_deleteConfirmMsgControl.Title = "Delete Subject(s)";
                QuestionSettings_deleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                QuestionSettings_deleteConfirmPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called delete test area button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void QuestionSettings_deleteTestAreaLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                QuestionSettings_deleteTypeHiddenField.Value = "T";

                QuestionSettings_deleteConfirmMsgControl.Message =
                    "Are you sure you want to delete the test area(s)? ";
                QuestionSettings_deleteConfirmMsgControl.Title = "Delete Test Area";
                QuestionSettings_deleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                QuestionSettings_deleteConfirmPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when delete
        /// category button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="object"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_deleteCategoryLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                QuestionSettings_deleteTypeHiddenField.Value = "C";

                QuestionSettings_deleteConfirmMsgControl.Message =
                    "Are you sure you want to delete the category ? ";
                QuestionSettings_deleteConfirmMsgControl.Title = "Delete Category";
                QuestionSettings_deleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                QuestionSettings_deleteConfirmPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when flush category button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="object"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_flushCategoryLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                QuestionSettings_deleteTypeHiddenField.Value = "F";

                QuestionSettings_deleteConfirmMsgControl.Message =
                    "This will flush all data including question, test related to the selected category. Are you sure you want to proceed?";
                QuestionSettings_deleteConfirmMsgControl.Title = "Flush Category";
                QuestionSettings_deleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                QuestionSettings_deleteConfirmPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the add 
        /// available subject link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="object"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_addAvailableSubjectsLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (QuestionSettings_categoriesListBox.SelectedItem == null)
                {
                    ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    "Please select a category");
                    return;
                }

                LoadSubjects(int.Parse(QuestionSettings_categoriesListBox.SelectedItem.Value));
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    exception.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when the cancel
        /// link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="object"/>that holds the event data.
        /// </param>
        protected void QuestionSettings_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Ok button is clicked 
        /// in the delete confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void QuestionSettings_deleteConfirmOkClick(object sender, EventArgs e)
        {
            try
            {
                if (QuestionSettings_deleteTypeHiddenField.Value == null)
                    return;

                if (QuestionSettings_deleteTypeHiddenField.Value == "C")
                    DeleteCategory();
                else if (QuestionSettings_deleteTypeHiddenField.Value == "S")
                    DeleteSubject();
                else if (QuestionSettings_deleteTypeHiddenField.Value == "T")
                    DeleteTestArea();
                else if (QuestionSettings_deleteTypeHiddenField.Value == "F")
                    FlushCategory();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(QuestionSettings_topErrorMessageLabel,
                   QuestionSettings_bottomErrorMessageLabel,
                   exception.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that checks if the test area ID and name is valid for
        /// save. 
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True 
        /// represents valid and false invalid.
        /// </returns>
        private bool IsValidTestArea()
        {
            bool isValid = true;

            //// Check if the test area ID is entered or not.
            //if (QuestionSettings_newTestAreaTextBox.Text.Trim().Length == 0)
            //{
            //    ShowMessage(QuestionSettings_topErrorMessageLabel,
            //        QuestionSettings_bottomErrorMessageLabel,
            //        "Test area ID cannot be empty");
            //    QuestionSettings_newTestAreaTextBox.Focus();

            //    isValid = false;
            //}

            // Check if the test area name is entered or not.
            if (QuestionSettings_newTestAreaNameTextBox.Text.Trim().Length == 0)
            {
                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    "Test area name cannot be empty");
                QuestionSettings_newTestAreaNameTextBox.Focus();

                isValid = false;
            }

            // Checks if the new test area  already exists.
            if (QuestionSettings_newTestAreaNameTextBox.Text.Trim().Length != 0 &&
                new AttributeBLManager().IsAttributeExist(base.tenantID,
                QuestionSettings_newTestAreaNameTextBox.Text.Trim()))
            {
                ShowMessage(QuestionSettings_bottomErrorMessageLabel,
                   QuestionSettings_topErrorMessageLabel,
                   string.Format("Test area  '{0}' already exist in the attribute table.",
                   QuestionSettings_newTestAreaNameTextBox.Text.Trim()));

                QuestionSettings_newTestAreaNameTextBox.Focus();

                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Method that will bind the categories to the list box.
        /// </summary>
        private void LoadCategory()
        {
            QuestionSettings_categoriesListBox.DataSource = new AdminBLManager().GetCategories(base.tenantID);
            QuestionSettings_categoriesListBox.DataValueField = "CategoryID";
            QuestionSettings_categoriesListBox.DataTextField = "CategoryName";
            QuestionSettings_categoriesListBox.DataBind();
        }

        /// <summary>
        /// Method that will bind the subjects associated with the select 
        /// category into the list box.
        /// </summary>
        /// <param name="categoryID">
        /// A <see cref="int"/> that holds the category ID.
        /// </param>
        private void LoadSubjects(int categoryID)
        {
            QuestionSettings_subjectsListBox.Items.Clear();

            QuestionSettings_subjectsListBox.DataSource = new
                AdminBLManager().GetSubjects(categoryID);

            QuestionSettings_subjectsListBox.DataValueField = "SubjectID";
            QuestionSettings_subjectsListBox.DataTextField = "SubjectName";
            QuestionSettings_subjectsListBox.DataBind();
        }

        /// <summary>
        /// Method that checks if the given text is found in the list box.
        /// </summary>
        /// <param name="listBox">
        /// A <see cref="ListBox"/> that holds the list box.
        /// </param>
        /// <param name="searchText">
        /// A <see cref="string"/> that holds the search text.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the found status. If found returns
        /// true else returns false.
        /// </returns>
        private bool FindByText(ListBox listBox, string searchText)
        {
            if (listBox == null)
                throw new Exception("List box object cannot be null");

            if (searchText == null)
                searchText = string.Empty;

            foreach (ListItem item in listBox.Items)
            {
                if (item.Text.ToUpper() == searchText.Trim().ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Method that will delete the selected category.
        /// </summary>
        private void DeleteCategory()
        {
            // Checks if a category is selected
            if (QuestionSettings_categoriesListBox.Items.FindByValue
                (QuestionSettings_categoriesListBox.SelectedValue) == null)
            {
                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    "Please select a category");

                return;
            }

            // Checks if a subject is associated with the category
            if (QuestionSettings_subjectsListBox.Items.Count > 0)
            {
                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    string.Format("Category '{0}' cannot be deleted. It is associated with subject",
                    QuestionSettings_categoriesListBox.SelectedItem.Text));

                return;
            }

            // Delete the category.
            new AdminBLManager().DeleteCategory(int.Parse
                (QuestionSettings_categoriesListBox.SelectedValue));

            ShowMessage(QuestionSettings_topSuccessMessageLabel,
                QuestionSettings_bottomSuccessMessageLabel,
                string.Format("Category '{0}' deleted successfully",
                QuestionSettings_categoriesListBox.SelectedItem.Text));

            // Reload categories.
            List<Category> categories = new AdminBLManager().GetCategories(base.tenantID);

            if (categories != null)
            {
                QuestionSettings_categoriesListBox.DataSource = categories;
                QuestionSettings_categoriesListBox.DataValueField = "CategoryID";
                QuestionSettings_categoriesListBox.DataTextField = "CategoryName";
                QuestionSettings_categoriesListBox.DataBind();
            }
            else
                Response.Redirect("~/Admin/QuestionSettings.aspx?m=4&s=1", false);
        }

        /// <summary>
        /// Method that will flush the selected category.
        /// </summary>
        private void FlushCategory()
        {
            // Checks if a category is selected
            if (QuestionSettings_categoriesListBox.Items.FindByValue
                (QuestionSettings_categoriesListBox.SelectedValue) == null)
            {
                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    "Please select a category");

                return;
            }

            // Flush the category.
            new CommonBLManager().FlushData(int.Parse
                (QuestionSettings_categoriesListBox.SelectedValue));

            ShowMessage(QuestionSettings_topSuccessMessageLabel,
                QuestionSettings_bottomSuccessMessageLabel,
                string.Format("Category '{0}' flushed successfully",
                QuestionSettings_categoriesListBox.SelectedItem.Text));

            // Reload categories.
            List<Category> categories = new AdminBLManager().GetCategories(base.tenantID);

            if (categories != null)
            {
                QuestionSettings_categoriesListBox.DataSource = categories;
                QuestionSettings_categoriesListBox.DataValueField = "CategoryID";
                QuestionSettings_categoriesListBox.DataTextField = "CategoryName";
                QuestionSettings_categoriesListBox.DataBind();
            }
            else
                Response.Redirect("~/Admin/QuestionSettings.aspx?m=4&s=1", false);
        }

        /// <summary>
        /// Method that will delete the selected subject.
        /// </summary>
        private void DeleteSubject()
        {
            // Checks whether a subject is selected or not
            if (QuestionSettings_subjectsListBox.SelectedItem == null)
            {
                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel,
                    "Please select subject(s) to delete");

                return;
            }
            // Declare message variables.
            string errorMessage = string.Empty;
            string successMessage = string.Empty;
            // Retrieve the category ID.
            int categoryID = int.Parse(QuestionSettings_categoriesListBox.
                SelectedValue);
            // Create an instance of business layer object.
            AdminBLManager adminBLManager = new AdminBLManager();
            // Loop through the subjects list box and delete the selected 
            // subjects.
            for (int subjectIndex = 0; subjectIndex <
                QuestionSettings_subjectsListBox.Items.Count; subjectIndex++)
            {
                if (!QuestionSettings_subjectsListBox.Items[subjectIndex].Selected)
                    continue;

                int subjectID = int.Parse(QuestionSettings_subjectsListBox.Items
                    [subjectIndex].Value);
                string subjectName = QuestionSettings_subjectsListBox.Items
                    [subjectIndex].Text;

                // Check if the subject is associated with question or contributor 
                // summary.
                if (adminBLManager.IsSubjectUsed(subjectID))
                    errorMessage += subjectName + ",";
                else
                {
                    // Delete the selected subject.
                    adminBLManager.DeleteSubject(subjectID);
                    successMessage += subjectName + ",";
                }
            }
            if (errorMessage != string.Empty)
                errorMessage = "There are questions assgined under the subject" +
                    (errorMessage.TrimEnd(',').Split(',').Length > 1 ? "s " : " ") + errorMessage.TrimEnd(',');
            if (successMessage != string.Empty)
                successMessage = "Subject" +
                    (successMessage.TrimEnd(',').Split(',').Length > 1 ? "s " : " ") + successMessage.TrimEnd(',') +
                    " deleted successfully";
            // Reload the subject for the selected category.
            LoadSubjects(categoryID);
            ShowMessage(QuestionSettings_topSuccessMessageLabel,
                QuestionSettings_bottomSuccessMessageLabel, successMessage);
            ShowMessage(QuestionSettings_topErrorMessageLabel,
                QuestionSettings_bottomErrorMessageLabel, errorMessage);
        }

        /// <summary>
        /// Method that will delete the selected test area.
        /// </summary>
        private void DeleteTestArea()
        {
            if (QuestionSettings_testAreasListBox.SelectedItem == null)
            {
                ShowMessage(QuestionSettings_topErrorMessageLabel,
                    QuestionSettings_bottomErrorMessageLabel, "Please select test area(s) to delete");

                return;
            }

            // Declare message variables.
            string errorMessage = string.Empty;
            string successMessage = string.Empty;

            AttributeBLManager attributeBLManager = new AttributeBLManager();

            // Loop through the subjects list box and delete the selected 
            // subjects.
            for (int testAreaIndex = 0; testAreaIndex <
                QuestionSettings_testAreasListBox.Items.Count; testAreaIndex++)
            {
                if (!QuestionSettings_testAreasListBox.Items[testAreaIndex].Selected)
                    continue;

                string testAreaID = QuestionSettings_testAreasListBox.Items
                    [testAreaIndex].Value;
                string testAreaName = QuestionSettings_testAreasListBox.Items
                    [testAreaIndex].Text;

                // Check if the subject is associated with question or contributor 
                // summary.
                if (attributeBLManager.IsTestAreaUsed(testAreaID))
                {
                    if (errorMessage == string.Empty)
                    {
                        errorMessage = string.Format
                            ("Test area '{0}' cannot be deleted. It is associated with question", testAreaName);
                    }
                    else
                    {
                        errorMessage = errorMessage + "<br>" + string.Format
                            ("Test area '{0}' cannot be deleted. It is associated with question", testAreaName);
                    }
                }
                else
                {
                    // Delete the selected test area.
                    new AdminBLManager().DeleteTestArea(testAreaID);

                    if (successMessage == string.Empty)
                    {
                        successMessage = string.Format
                            ("Test area '{0}' deleted successfully", testAreaName);
                    }
                    else
                    {
                        successMessage = successMessage + "<br>" + string.Format
                            ("Test area '{0}' deleted successfully", testAreaName);
                    }
                }
            }

            ShowMessage(QuestionSettings_topErrorMessageLabel,
                QuestionSettings_bottomErrorMessageLabel,
                errorMessage);

            ShowMessage(QuestionSettings_topSuccessMessageLabel,
               QuestionSettings_bottomSuccessMessageLabel,
                successMessage);

            LoadValues();
        }

        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "CAT":
                    this.QuestionSettings_addNewCategoryLinkButton_Click(this, new EventArgs());
                    break;
                case "SUB":
                    this.QuestionSettings_addNewSubjectLinkButton_Click(this, new EventArgs());
                    break;
                case "TST_AREA":
                case "TST_AREA_ID":
                    this.QuestionSettings_addNewTestAreaLinkButton_Click(this, new EventArgs());
                    break;
            }
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Set default action for Add new category linkbutton.
            QuestionSettings_newCategoryTextBox.Focus();

            QuestionSettings_testAreasListBox.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
            QuestionSettings_testAreasListBox.DataValueField = "AttributeID";
            QuestionSettings_testAreasListBox.DataTextField = "AttributeName";
            QuestionSettings_testAreasListBox.DataBind();
        }

        #endregion Protected Overridden Methods                                
    }
}