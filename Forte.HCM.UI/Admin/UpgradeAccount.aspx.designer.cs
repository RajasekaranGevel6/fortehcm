﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Admin {
    
    
    public partial class UpgradeAccount {
        
        /// <summary>
        /// Forte_UpgradeAccount_UpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel Forte_UpgradeAccount_UpdatePanel;
        
        /// <summary>
        /// Forte_UpgradeAccount_UpgradeAccountHeaderLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Forte_UpgradeAccount_UpgradeAccountHeaderLiteral;
        
        /// <summary>
        /// Forte_Updgrade_topAcceptButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Forte_Updgrade_topAcceptButton;
        
        /// <summary>
        /// Forte_UpgradeAccount_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Forte_UpgradeAccount_topResetLinkButton;
        
        /// <summary>
        /// Forte_UpgradeAccount_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Forte_UpgradeAccount_topCancelLinkButton;
        
        /// <summary>
        /// Forte_UpgradeAccount_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_topErrorMessageLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_topSuccessMessageLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_accountDetailsHeaderLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Forte_UpgradeAccount_accountDetailsHeaderLiteral;
        
        /// <summary>
        /// Forte_UpgradeAccount_subscriptionTypeHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_subscriptionTypeHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_subscriptionTypeLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_subscriptionTypeLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_subscriptionIdHidden control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden Forte_UpgradeAccount_subscriptionIdHidden;
        
        /// <summary>
        /// Forte_UpgradeAccount_lastLoginHeaderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_lastLoginHeaderLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_lastLoginLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_lastLoginLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_statusHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_statusHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_statusLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_statusLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_subscribedOnHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_subscribedOnHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_subscribedOnLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_subscribedOnLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_activatedOnHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_activatedOnHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_activatedOnLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_activatedOnLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_noOfUsersHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_noOfUsersHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_noOfUsersLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_noOfUsersLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_titleHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_titleHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_titleLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_titleLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_companyHeadLable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_companyHeadLable;
        
        /// <summary>
        /// Forte_UpgradeAccount_companyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_companyLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_phoneHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_phoneHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_phoneLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_phoneLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_firstNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_firstNameHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_firstNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_firstNameLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_lastNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_lastNameHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_lastNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_lastNameLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_isTrialHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_isTrialHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_isTrialLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_isTrialLabel;
        
        /// <summary>
        /// CreateTestSession_maxMinButtonUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel CreateTestSession_maxMinButtonUpdatePanel;
        
        /// <summary>
        /// Forte_CorporateUserManangement_usersResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Forte_CorporateUserManangement_usersResultsLiteral;
        
        /// <summary>
        /// UpdatePanel2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanel2;
        
        /// <summary>
        /// Forte_UpgradeAccount_featureAndPricingGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView Forte_UpgradeAccount_featureAndPricingGridView;
        
        /// <summary>
        /// Forte_UpgradeAccount_rulesHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_rulesHeadLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_rule1Label control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_rule1Label;
        
        /// <summary>
        /// Forte_UpgradeAccount_rule2Label control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_rule2Label;
        
        /// <summary>
        /// Forte_UpgradeAccount_rule3Label control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_rule3Label;
        
        /// <summary>
        /// Forte_UpgradeAccount_rule4Label control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_rule4Label;
        
        /// <summary>
        /// Forte_UpgradeAccount_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_bottomErrorMessageLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Forte_UpgradeAccount_bottomSuccessMessageLabel;
        
        /// <summary>
        /// Forte_UpgradeAccount_bottomAcceptButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Forte_UpgradeAccount_bottomAcceptButton;
        
        /// <summary>
        /// Forte_UpgradeAccount_bottomResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Forte_UpgradeAccount_bottomResetLinkButton;
        
        /// <summary>
        /// Forte_UpgradeAccount_bottomCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton Forte_UpgradeAccount_bottomCancelLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.CustomerAdminMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.CustomerAdminMaster)(base.Master));
            }
        }
    }
}
