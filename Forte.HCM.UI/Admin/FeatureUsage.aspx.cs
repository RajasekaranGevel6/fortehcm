﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// FeatureUsage.aspx.cs
// File that represents the FeatureUsage class that defines the user 
// interface layout and functionalities for the FeatureUsage page. This 
// page helps in displaying the feature usage. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the FeatureUsage page. This page help to display the feature usage
    /// of a particular user. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class FeatureUsage : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "335px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Usage Summary");

                FeatureUsage_topSuccessMessageLabel.Text = string.Empty;
                FeatureUsage_bottomSuccessLabel.Text = string.Empty;
                FeatureUsage_topErrorMessageLabel.Text = string.Empty;
                FeatureUsage_bottomErrorLabel.Text = string.Empty;

                CheckAndSetExpandOrRestore();

                Page.Form.DefaultButton = FeatureUsage_showButton.UniqueID;
                Page.SetFocus(FeatureUsage_userIDTextBox.ClientID);

                if (!IsPostBack)
                {
                    SubscribtClientSideHandlers();

                    LoadMonthYear();

                    FeatureUsage_monthDropDownList.SelectedValue = DateTime.Now.Month.ToString();
                    FeatureUsage_yearDropDownList.SelectedValue = DateTime.Now.Year.ToString();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsage_topSuccessMessageLabel,
                   FeatureUsage_bottomSuccessLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset is clicked       
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureUsage_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsage_topSuccessMessageLabel,
                   FeatureUsage_bottomSuccessLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when save is clicked
        /// in the paging control of role category section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureUsage_bottomSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (FeatureUsage_userIDTextBox.Text == string.Empty)
                {
                    return;
                }
                LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsage_topSuccessMessageLabel,
                   FeatureUsage_bottomSuccessLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the icon selected on
        /// every feature action with row command
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsage_userFeaturesGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "POPUP")
                    return;
                FeatureUsage_checkedStatusHiddenField.Value = e.CommandArgument.ToString();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsage_topSuccessMessageLabel,
                   FeatureUsage_bottomSuccessLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the binding of values in grid
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsage_userFeaturesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton UserRoleMatrix_rolesGridView_viewDetails =
                    (ImageButton)(e.Row.FindControl("UserRoleMatrix_rolesGridView_viewDetails"));

                HiddenField UserRoleMatrix_rolenAMEGridViewIDHiddenField =
                    (HiddenField)(e.Row.FindControl("UserRoleMatrix_rolenAMEGridViewIDHiddenField"));

                HiddenField UserRoleMatrix_mmmYYYYGridViewIDHiddenField =
                    (HiddenField)(e.Row.FindControl("UserRoleMatrix_mmmYYYYGridViewIDHiddenField"));

                UserRoleMatrix_rolesGridView_viewDetails.Attributes.Add("onclick",
                        "javascript:return LoadUsageDetails('"
                    + UserRoleMatrix_rolenAMEGridViewIDHiddenField.Value + "','"
                    + FeatureUsage_userIDHiddenField.Value + "','"
                    + UserRoleMatrix_mmmYYYYGridViewIDHiddenField.Value + "')");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsage_topSuccessMessageLabel,
                   FeatureUsage_bottomSuccessLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the show details is clicked     
        /// to show the feature usage details of the particular user.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void FeatureUsage_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (FeatureUsage_userIDTextBox.Text == string.Empty)
                {
                    base.ShowMessage(FeatureUsage_topErrorMessageLabel,
                       FeatureUsage_bottomErrorLabel,
                       Resources.HCMResource.FeatureUsageUserFieldEmpty);
                    return;
                }

                LoadValues();

                FeatureUsage_yearDropDownList.SelectedItem.Text = year_hd.Value;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsage_topSuccessMessageLabel,
                   FeatureUsage_bottomSuccessLabel, exception.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method to check for Expand or reset image
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            if (!Utility.IsNullOrEmpty(FeatureUsage_isMaximizedHiddenField.Value) &&
               FeatureUsage_isMaximizedHiddenField.Value == "Y")
            {
                FeatureUsage_SummaryDIV.Style["display"] = "none";
                FeatureUsage_UparrowSpan.Style["display"] = "block";
                FeatureUsage_DownarrowSpan.Style["display"] = "none";
                FeatureUsage_DetailsDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                FeatureUsage_SummaryDIV.Style["display"] = "block";
                FeatureUsage_UparrowSpan.Style["display"] = "none";
                FeatureUsage_DownarrowSpan.Style["display"] = "block";
                FeatureUsage_DetailsDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Method to subscribe the handler for the click of the Expand
        /// or restore
        /// </summary>
        private void SubscribtClientSideHandlers()
        {
            FeatureUsage_userIDTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                   + FeatureUsage_userIDTextBox.ClientID + "','NAME','"
                   + FeatureUsage_browserHiddenField.ClientID + "');");
            //Assign the on click attribute for the search image
            FeatureUsage_userSearchImage.Attributes.Add("onclick",
                 "javascript:return LoadUserForFeatureUsage('"
                + FeatureUsage_userIDTextBox.ClientID + "','"
                + FeatureUsage_userIDHiddenField.ClientID + "','"
                + FeatureUsage_userIDTextBox.ClientID + "','QA')");

            //Assign the on click attributes for the expand or restore button 
            FeatureUsage_TR.Attributes.Add("onclick",
             "ExpandOrRestore('" +
             FeatureUsage_DetailsDIV.ClientID + "','" +
             FeatureUsage_SummaryDIV.ClientID + "','" +
             FeatureUsage_UparrowSpan.ClientID + "','" +
             FeatureUsage_DownarrowSpan.ClientID + "','" +
             FeatureUsage_isMaximizedHiddenField.ClientID + "','" +
             RESTORED_HEIGHT + "','" +
             EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Method to load the month and year
        /// </summary>
        private void LoadMonthYear()
        {
            DateTime month = Convert.ToDateTime("1/1/2000");
            for (int i = 0; i < 12; i++)
            {
                DateTime NextMont = month.AddMonths(i);
                ListItem list = new ListItem();
                list.Text = NextMont.ToString("MMM");
                list.Value = NextMont.Month.ToString();
                FeatureUsage_monthDropDownList.Items.Add(list);
            }

            List<UsageSummary> userFeatures = new AdminBLManager().GetYear();

            for (int i = 0; i < userFeatures.Count; i++)
            {
                FeatureUsage_yearDropDownList.Items.Add(userFeatures[i].FeatureYear.ToString());
                FeatureUsage_yearDropDownList.SelectedItem.Value = userFeatures[i].FeatureYear.ToString();
            }
        }

        /// <summary>
        /// Method to load feature details of the selected user 
        /// for the select month and year
        /// </summary>
        /// <param name="userID"></param>
        private void LoadFeaturesGridView(int userID)
        {
            string month = FeatureUsage_monthDropDownList.SelectedItem.Text;

            int year = Convert.ToInt32(FeatureUsage_yearDropDownList.SelectedValue.ToString());

            List<UsageSummary> userFeatures = new AdminBLManager().GetFeatureUsageDetails
                (userID, month, year);

            if (userFeatures.Count == 0)
            {
                base.ShowMessage(FeatureUsage_topErrorMessageLabel,
                 FeatureUsage_bottomErrorLabel, Resources.HCMResource.FeatureUsageNoDataFound);

                FeatureUsage_userFeaturesGridView.DataSource = userFeatures;

                FeatureUsage_userFeaturesGridView.DataBind();

                year_hd.Value = FeatureUsage_yearDropDownList.SelectedValue.ToString();

                FeatureUsage_SubscriptionTypeLabel.Text = string.Empty;

                FeatureUsage_SubscribedOnLable.Text = string.Empty;

                FeatureUsage_CompanyLabel.Text = string.Empty;

                FeatureUsage_titleLabel.Text = string.Empty;

                return;
            }
            FeatureUsage_userFeaturesGridView.DataSource = userFeatures;

            FeatureUsage_userFeaturesGridView.DataBind();

            year_hd.Value = FeatureUsage_yearDropDownList.SelectedValue.ToString();
        }

        /// <summary>
        /// Method to show the user subscription details
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        private void ShowUserSubscription(int userID)
        {
            UsageSummary userSubscription = new AdminBLManager().GetUserSubscriptionDetails(userID);

            if (userSubscription == null)
                return;
            if (userSubscription.SubscriptionName != null)
                FeatureUsage_SubscriptionTypeLabel.Text = userSubscription.SubscriptionName.ToString();
            if (userSubscription.SusbscribedOn != null)
                FeatureUsage_SubscribedOnLable.Text = Convert.ToDateTime
                    (userSubscription.SusbscribedOn.ToString()).ToShortDateString();
            if (userSubscription.Company != null)
                FeatureUsage_CompanyLabel.Text = userSubscription.Company.ToString();
            if (userSubscription.Title != null)
                FeatureUsage_titleLabel.Text = userSubscription.Title.ToString();
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            int userID = int.Parse(FeatureUsage_userIDHiddenField.Value);

            ShowUserSubscription(userID);

            LoadFeaturesGridView(userID);
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }

        #endregion Protected Overridden Methods
    }
}