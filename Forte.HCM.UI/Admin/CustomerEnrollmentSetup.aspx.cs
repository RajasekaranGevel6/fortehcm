﻿#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Represent the class that used to update the customer enrollment setup
    /// details.Used to update details like business types and free trial validity 
    /// for various subscription
    /// </summary>
    public partial class CustomerEnrollmentSetup : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        string selectedRoles = string.Empty;

        string selectedRoleIDs = string.Empty;

        #region Event Handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Check and expand or restore
                CheckAndSetExpandOrRestore();

                //Set the default buton for page
                Page.Form.DefaultButton = CustomerEnrollmentSetup_searchBusinessTypeButton.UniqueID;
                Page.SetFocus(CustomerEnrollmentSetup_searchBusinessTypeButton.ClientID);

                //Set the page caption in the page
                Master.SetPageCaption("Tenant Enrollment Setup");

                //Assign the on click attributes for the expand or restore button 
                CustomerEnrollmentSetup_expandAllTR.Attributes.Add("onclick",
                  "ExpandOrRestore('" +
                  CustomerEnrollmentSetup_businessTypesGridViewDIV.ClientID + "','" +
                  CustomerEnrollmentSetup_searchDIV.ClientID + "','" +
                  CustomerEnrollmentSetup_searchResultsUparrowSpan.ClientID + "','" +
                  CustomerEnrollmentSetup_searchResultsDownarrowSpan.ClientID + "','" +
                  CustomerEnrollmentSetup_restoreHiddenField.ClientID + "','" +
                  RESTORED_HEIGHT + "','" +
                  EXPANDED_HEIGHT + "')");

                CustomerEnrollmentSetup_topSuccessMessageLabel.Text = string.Empty;
                CustomerEnrollmentSetup_bottomSuccessLabel.Text = string.Empty;
                CustomerEnrollmentSetup_topErrorMessageLabel.Text = string.Empty;
                CustomerEnrollmentSetup_bottomErrorLabel.Text = string.Empty;
                CustomerEnrollmentSetup_updateBusinessNameErrorLabel.Text = string.Empty;

                CustomerEnrollmentSetup_businessTypesPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (CustomerEnrollmentSetup_businessTypesPageNavigator_PageNumberClick);



                if (!IsPostBack)
                {
                    //Load the default values
                    LoadValues();

                    BindRoles();

                    CustomerEnrollmentSetup_previewLinkButton.Attributes.Add("onclick", "return LoadGetForm('" +
                        CustomerEnrollmentSetup_formLayoutDropDownList.ClientID + "');");
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        private void BindRoles()
        {

            string selectedRoles = string.Empty;

            List<Roles> corporateroleList = new AuthenticationBLManager().GetCorporateSelectRole();

            CustomerEnrollmentSetup_rolesCheckBoxList.DataSource = corporateroleList;
            CustomerEnrollmentSetup_rolesCheckBoxList.DataTextField = "RoleName";
            CustomerEnrollmentSetup_rolesCheckBoxList.DataValueField = "RoleID";
            CustomerEnrollmentSetup_rolesCheckBoxList.DataBind();
            CustomerEnrollmentSetup_rolesCheckBoxList.Text = "";          

            for (int i = 0; i < corporateroleList.Count; i++)
            {
                if (corporateroleList[i].IsSelected == "Y")
                {
                    CustomerEnrollmentSetup_rolesCheckBoxList.Items[i].Selected = true;

                    selectedRoles = selectedRoles + CustomerEnrollmentSetup_rolesCheckBoxList.Items[i].Text + ",";
                }
            }

            CustomerEnrollmentSetup_rolesComboTextBox.Text = selectedRoles.TrimEnd(',');

            CustomerEnrollmentSetup_rolesComboTextBox.ToolTip = selectedRoles.TrimEnd(','); 

        }

        protected void CustomerEnrollmentSetup_previewLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerEnrollmentSetup_formLayoutDropDownList.SelectedValue == "0")
                {
                    base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                  CustomerEnrollmentSetup_bottomErrorLabel,
                  Resources.HCMResource.PositionProfileUserOptions_PleaseSelectAFormType);

                    return;
                }
            }
            catch (Exception exception)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                  CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);

                Logger.TraceLog(exception);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the CustomerEnrollmentSetup_businessTypesPageNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Forte.HCM.EventSupport.PageNumberEventArgs"/>
        /// instance containing the event data.</param>
        void CustomerEnrollmentSetup_businessTypesPageNavigator_PageNumberClick
            (object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                CustomerEnrollmentSetup_businessTypesGridView.EditIndex = -1;
                CustomerEnrollmentSetup_businessTypesPageNumberHidden.Value = e.PageNumber.ToString();
                GetBusinessDetails(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the CustomerEnrollmentSetup_searchBusinessTypeButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_searchBusinessTypeButton_Click(object sender, EventArgs e)
        {
            try
            {
                //If the business type is in edit mode , display the message to 
                //update the business types
                if (CustomerEnrollmentSetup_businessTypesGridView.EditIndex != -1)
                {
                    base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                  CustomerEnrollmentSetup_bottomErrorLabel, Resources.HCMResource.
                  CustomerEnrollmentSetup_PleaseUpdateTheBusinessType);
                    return;
                }

                //Reset the page navigator
                CustomerEnrollmentSetup_businessTypesPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";

                //Get the form details
                GetBusinessDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Sorting event of the CustomerEnrollmentSetup_businessTypesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The 
        /// <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/>
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_businessTypesGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                CustomerEnrollmentSetup_businessTypesPageNavigator.Reset();
                GetBusinessDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCreated event of the 
        /// CustomerEnrollmentSetup_businessTypesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The 
        /// <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_businessTypesGridView_RowCreated(
            object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (CustomerEnrollmentSetup_businessTypesGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Represents the event that is called on the row editing event of the 
        /// CustomerEnrollmentSetup_businessTypesGridView 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewEditEventArgs"/>that holds the event args
        /// </param>
        protected void CustomerEnrollmentSetup_businessTypesGridView_RowEditing(
            object sender, GridViewEditEventArgs e)
        {
            try
            {
                CustomerEnrollmentSetup_businessTypesGridView.EditIndex = e.NewEditIndex;
                GetBusinessDetails(Convert.ToInt32(CustomerEnrollmentSetup_businessTypesPageNumberHidden.Value));
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.NewEditIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_UpdateImageButton")).Visible = true;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.NewEditIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_editImageButton")).Visible = false;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.NewEditIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_deleteImageButton")).Visible = false;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.NewEditIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_cancelUpdateImageButton")).Visible = true;
                CustomerEnrollmentSetup_deleteBusinessHiddenField.Value = ((HiddenField)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.NewEditIndex].
                    FindControl("CustomerEnrollmentSetup_editBusinessTypeIDHiddenField")).Value;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the event that is called on the row updating of the 
        /// CustomerEnrollmentSetup_businessTypesGridView
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewUpdateEventArgs"/>that holds the event args
        /// </param>
        protected void CustomerEnrollmentSetup_businessTypesGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                CustomerEnrollmentSetup_businessTypesGridView.EditIndex = -1;
                new AdminBLManager().UpdateBusinessTypeName(((TextBox)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_editNameTextBox")).Text, Convert.ToInt32(CustomerEnrollmentSetup_deleteBusinessHiddenField.Value));
                base.ShowMessage(CustomerEnrollmentSetup_topSuccessMessageLabel,
                   CustomerEnrollmentSetup_bottomSuccessLabel, Resources.HCMResource.
                   CustomerEnrollmentSetup_BusinessTypeUpdatedSuccessfully);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exp.Message);
            }
            finally
            {
                GetBusinessDetails(Convert.ToInt32(CustomerEnrollmentSetup_businessTypesPageNumberHidden.Value));
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_editImageButton")).Visible = true;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_deleteImageButton")).Visible = true;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_cancelUpdateImageButton")).Visible = false;
                CustomerEnrollmentSetup_deleteBusinessHiddenField.Value = "";
            }
        }


        /// <summary>
        /// Handles the RowCancelingEdit event of the CustomerEnrollmentSetup_businessTypesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GridViewCancelEditEventArgs"/>
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_businessTypesGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                CustomerEnrollmentSetup_businessTypesGridView.EditIndex = -1;
                GetBusinessDetails(Convert.ToInt32(CustomerEnrollmentSetup_businessTypesPageNumberHidden.Value));
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_editImageButton")).Visible = true;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_deleteImageButton")).Visible = true;
                ((ImageButton)CustomerEnrollmentSetup_businessTypesGridView.Rows[e.RowIndex].
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_cancelUpdateImageButton")).Visible = false;
                CustomerEnrollmentSetup_deleteBusinessHiddenField.Value = "";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the CustomerEnrollmentSetup_businessTypesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The 
        /// <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/>
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_businessTypesGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteBusiness")
                    return;
                CustomerEnrollmentSetup_deleteBusinessHiddenField.Value = e.CommandArgument.ToString();
                CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl.Message = string.Format
                    (Resources.HCMResource.PositionProfileFormEntry_DeleteConfirmation,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("CustomerEnrollmentSetup_businessTypesGridView_nameLabel")).Text);

                CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl.Type = MessageBoxType.YesNo;
                CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl.Title = "Warning";
                CustomerEnrollmentSetup_deleteFormModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                int businessID = int.Parse(CustomerEnrollmentSetup_deleteBusinessHiddenField.Value);

                new AdminBLManager().DeleteBusinessType(businessID);

                //Reset the page navigator
                CustomerEnrollmentSetup_businessTypesPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";

                //Get the update business details
                GetBusinessDetails(1);

                base.ShowMessage(CustomerEnrollmentSetup_topSuccessMessageLabel,
                    CustomerEnrollmentSetup_bottomSuccessLabel, Resources.HCMResource.
                    CustomerEnrollmentSetup_BusinessTypeDeletedSuccessfully);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }

        }
        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                //Remove the business ype id from the hidden field
                CustomerEnrollmentSetup_deleteBusinessHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }

        }

        /// <summary>
        /// Handles the Clicked event of the
        /// CustomerEnrollmentSetup_updateBusinessNameSaveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_updateBusinessNameSaveButton_Clicked
            (object sender, EventArgs e)
        {
            try
            {
                if (CustomerEnrollmentSetup_updateBusinessNameTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(CustomerEnrollmentSetup_updateBusinessNameErrorLabel,
                        Resources.HCMResource.CustomerEnrollmentSetup_PleaseEnterTheName);

                    CustomerEnrollmentSetup_updateBusinessNameSaveButton.Focus();
                    CustomerEnrollmentSetup_updateBusinessNameModalPopupExtender.Show();

                    return;
                }

                if (CustomerEnrollmentSetup_updateBusinessIsEditORSaveHiddenField.Value.ToUpper() == "EDIT")
                {
                    int id = int.Parse(CustomerEnrollmentSetup_deleteBusinessHiddenField.Value);

                    new AdminBLManager().UpdateBusinessTypeName(
                    CustomerEnrollmentSetup_updateBusinessNameTextBox.Text.Trim(), id);

                    base.ShowMessage(CustomerEnrollmentSetup_topSuccessMessageLabel,
                       CustomerEnrollmentSetup_bottomSuccessLabel,
                       Resources.HCMResource.CustomerEnrollmentSetup_BusinessTypeUpdatedSuccessfully);
                }
                if (CustomerEnrollmentSetup_updateBusinessIsEditORSaveHiddenField.Value.ToUpper() == "ADDNEW")
                {
                    new AdminBLManager().InsertNewBusinessTypeName(
                    CustomerEnrollmentSetup_updateBusinessNameTextBox.Text.Trim(), base.userID);

                    base.ShowMessage(CustomerEnrollmentSetup_topSuccessMessageLabel,
                       CustomerEnrollmentSetup_bottomSuccessLabel,
                       Resources.HCMResource.CustomerEnrollmentSetup_NewBusinessTypeInsertedSuccessfully);
                }

                //Reset the page navigator
                CustomerEnrollmentSetup_businessTypesPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";

                //Get the form details
                GetBusinessDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Clicked event of the 
        /// CustomerEnrollmentSetup_updateBusinessNameCloseLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_updateBusinessNameCloseLinkButton_Clicked
            (object sender, EventArgs e)
        {
            try
            {
                CustomerEnrollmentSetup_deleteBusinessHiddenField.Value = string.Empty;
                CustomerEnrollmentSetup_updateBusinessIsEditORSaveHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the 
        /// CustomerEnrollmentSetup_businessTypesAddImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_businessTypesAddImageButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (CustomerEnrollmentSetup_businessTypesGridView.EditIndex != -1)
                {
                    base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                  CustomerEnrollmentSetup_bottomErrorLabel, Resources.HCMResource.
                  CustomerEnrollmentSetup_PleaseUpdateTheBusinessType);
                    return;
                }
                CustomerEnrollmentSetup_updateBusinessIsEditORSaveHiddenField.Value = "ADDNEW";
                CustomerEnrollmentSetup_updateBusinessLiteral.Text = "Add Business Type";
                CustomerEnrollmentSetup_updateBusinessNameTextBox.Text = string.Empty;
                CustomerEnrollmentSetup_updateBusinessNameSaveButton.Focus();
                //CustomerEnrollmentSetup_updateBusinessPanel.DefaultButton =
                //CustomerEnrollmentSetup_updateBusinessNameSaveButton.ClientID;
                CustomerEnrollmentSetup_updateBusinessNameModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }


        /// <summary>
        /// Handles the Click event of the CustomerEnrollmentSetup_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;

                CustomerEnrollmentDetail enrollmentDetail = new CustomerEnrollmentDetail();

                enrollmentDetail.FreeSubscriptionTrialValidiy = int.Parse
                    (CustomerEnrollmentSetup_freeSubscriptionDaysTextBox.Text.Trim());

                enrollmentDetail.StandardSubscriptionTrialValidity = int.Parse
                    (CustomerEnrollmentSetup_standardSubscriptionFreeTrialTextBox.Text.Trim());

                enrollmentDetail.CorporateSubscriptionTrialValidity = int.Parse
                    (CustomerEnrollmentSetup_corporateSubscriptionFreeTrialTextBox.Text.Trim());

                enrollmentDetail.CorporateSubscriptionNoOfUser = int.Parse
                    (CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersTextBox.Text.Trim());

                enrollmentDetail.DefaultForm = int.Parse
                  (CustomerEnrollmentSetup_formLayoutDropDownList.SelectedValue);

                enrollmentDetail.ModifiedBy = base.userID;

                enrollmentDetail.UserID = base.userID;

                foreach (ListItem item in CustomerEnrollmentSetup_rolesCheckBoxList.Items)
                {
                    if (item.Selected)
                    {
                        selectedRoles = selectedRoles + item.Text + ",";

                        selectedRoleIDs = selectedRoleIDs + item.Value + ",";
                    }
                }
                enrollmentDetail.corporateRoleIDs = selectedRoleIDs.TrimEnd(',');

                new AdminBLManager().UpdateSubscriptionOptions(enrollmentDetail);

                base.ShowMessage(CustomerEnrollmentSetup_topSuccessMessageLabel,
                    CustomerEnrollmentSetup_bottomSuccessLabel, Resources.HCMResource.
                CustomerEnrollmentSetup_CustomerDetailsUpdatedSuccessfully);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the 
        /// CustomerEnrollmentSetup_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void CustomerEnrollmentSetup_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, exception.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// Checks the and set expand or restore.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(CustomerEnrollmentSetup_restoreHiddenField.Value) &&
                 CustomerEnrollmentSetup_restoreHiddenField.Value == "Y")
            {
                CustomerEnrollmentSetup_searchDIV.Style["display"] = "none";
                CustomerEnrollmentSetup_searchResultsUparrowSpan.Style["display"] = "block";
                CustomerEnrollmentSetup_searchResultsDownarrowSpan.Style["display"] = "none";
                CustomerEnrollmentSetup_businessTypesGridViewDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CustomerEnrollmentSetup_searchDIV.Style["display"] = "block";
                CustomerEnrollmentSetup_searchResultsUparrowSpan.Style["display"] = "none";
                CustomerEnrollmentSetup_searchResultsDownarrowSpan.Style["display"] = "block";
                CustomerEnrollmentSetup_businessTypesGridViewDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Gets the business details.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void GetBusinessDetails(int pageNumber)
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            int totalPage = 0;

            string businessName = CustomerEnrollmentSetup_businessNameTextBox.Text.Trim();

            List<BusinessTypeDetail> businessTypeList = new AdminBLManager().GetBusinessTypes
                (businessName, sortExpression, sortOrder, base.GridPageSize, pageNumber, out totalPage);

            CustomerEnrollmentSetup_businessTypesGridView.DataSource = businessTypeList;

            CustomerEnrollmentSetup_businessTypesGridView.DataBind();

            CustomerEnrollmentSetup_businessTypesPageNavigator.Visible = true;

            CustomerEnrollmentSetup_businessTypesPageNavigator.TotalRecords = totalPage;

            CustomerEnrollmentSetup_businessTypesPageNavigator.PageSize = base.GridPageSize;

            CustomerEnrollmentSetup_businessTypesGridViewDIV.Style["display"] = "block";

            if (businessTypeList == null || businessTypeList.Count == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, Resources.HCMResource.Common_Empty_Grid);

                CustomerEnrollmentSetup_businessTypesGridViewDIV.Style["display"] = "none";
            }
        }

        #endregion Private Methods

        #region Protected Override Methods

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            bool value = true;
            //int isValidInteger = 0;

            if (CustomerEnrollmentSetup_freeSubscriptionDaysTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel, Resources.HCMResource.
                     CustomerEnrollmentSetup_PleaseEnterTrialValidityForFreeSubscription);
                value = false;
            }

            else if (int.Parse(CustomerEnrollmentSetup_freeSubscriptionDaysTextBox.Text.Trim()) == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel,
                    Resources.HCMResource.CustomerEnrollmentSetup_TrialValidityForFreeSubscriptionShouldBeGreaterThan0);
                value = false;
            }


            if (CustomerEnrollmentSetup_standardSubscriptionFreeTrialTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel, Resources.HCMResource.
                     CustomerEnrollmentSetup_PleaseEnterTrialValidityForStandardSubscription);
                value = false;
            }

            else if (int.Parse(CustomerEnrollmentSetup_standardSubscriptionFreeTrialTextBox.Text.Trim()) == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel,
                    Resources.HCMResource.
                    CustomerEntrollmentSetup_TrialValidityForStandardSubscriptionShouldBeGreaterThan0);
                value = false;
            }

            if (CustomerEnrollmentSetup_corporateSubscriptionFreeTrialTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel, Resources.HCMResource.
                     CustomerEnrollmentSetup_PleaseEnterTrialValidityForCorporateSubscription);

                value = false;
            }
            else if (int.Parse(CustomerEnrollmentSetup_corporateSubscriptionFreeTrialTextBox.Text.Trim()) == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel,
                    Resources.HCMResource.
                    CustomerEnrollmentSetup_TrialValidityForCorporateSubscriptionShouldBeGreaterThan0);
                value = false;
            }

            if (CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel,
                     Resources.HCMResource.
                     CustomerEnrollmentSetup_PleasEnterMaximumNumberOfUsersForCorporateSubscription);

                value = false;
            }
            else if (int.Parse(CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersTextBox.Text.Trim()) == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel,
                    Resources.HCMResource.
                    CustomerEnrollmentSetup_MaximumNumberOfUsersForCorporateSubscriptionShouldBeGreaterThan0);
                value = false;
            }

            if (CustomerEnrollmentSetup_formLayoutDropDownList.SelectedValue == "0")
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                     CustomerEnrollmentSetup_bottomErrorLabel,
                     Resources.HCMResource.CustomerEnrollmentSetup_PleaseSelectAForm);

                value = false;
            }
            if (CustomerEnrollmentSetup_businessTypesGridView.EditIndex != -1)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, Resources.HCMResource.
                    CustomerEnrollmentSetup_PleaseUpdateTheBusinessType);
                value = false;
            }
            if (CustomerEnrollmentSetup_rolesComboTextBox.Text.Length == 0)
            {
                base.ShowMessage(CustomerEnrollmentSetup_topErrorMessageLabel,
                    CustomerEnrollmentSetup_bottomErrorLabel, "Select atlest one role for the corporate users");
                value = false;
            }
            return value;
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            //Load the business type details
            //Reset the page navigator
            CustomerEnrollmentSetup_businessTypesPageNavigator.Reset();
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD"] = "NAME";


            //Get the form details
            GetBusinessDetails(1);

            //Load the default values for the subscriptions
            CustomerEnrollmentDetail enrollmentDetail = new AdminBLManager().GetDefaultSubscrptionDetails();

            CustomerEnrollmentSetup_freeSubscriptionDaysTextBox.Text =
                enrollmentDetail.FreeSubscriptionTrialValidiy.ToString();

            CustomerEnrollmentSetup_standardSubscriptionFreeTrialTextBox.Text =
                enrollmentDetail.StandardSubscriptionTrialValidity.ToString();

            CustomerEnrollmentSetup_corporateSubscriptionFreeTrialTextBox.Text =
                enrollmentDetail.CorporateSubscriptionTrialValidity.ToString();

            CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersTextBox.Text =
                enrollmentDetail.CorporateSubscriptionNoOfUser.ToString();

            CustomerEnrollmentSetup_formLayoutDropDownList.DataSource = new PositionProfileBLManager().GetPredefinedForm();
            CustomerEnrollmentSetup_formLayoutDropDownList.DataBind();

            CustomerEnrollmentSetup_formLayoutDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));

            //CustomerEnrollmentSetup_formLayoutDropDownList.Items[0].Selected = true;

            foreach (ListItem item in CustomerEnrollmentSetup_formLayoutDropDownList.Items)
            {
                if (item.Value == enrollmentDetail.DefaultForm.ToString())
                {
                    item.Selected = true;
                }
            }
        }
        #endregion Protected Override Methods
        protected void CustomerEnrollmentSetup_rolesCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            CustomerEnrollmentSetup_rolesComboTextBox.Focus();

            foreach (ListItem item in CustomerEnrollmentSetup_rolesCheckBoxList.Items)
            {
                if (item.Selected)
                {
                    selectedRoles = selectedRoles + item.Text + ",";

                    selectedRoleIDs = selectedRoleIDs + item.Value + ",";
                }
            }

            CustomerEnrollmentSetup_rolesComboTextBox.Text = selectedRoles.TrimEnd(',');
            CustomerEnrollmentSetup_rolesHiddenField.Value = selectedRoleIDs.TrimEnd(',');
            CustomerEnrollmentSetup_rolesComboTextBox.ToolTip = CustomerEnrollmentSetup_rolesComboTextBox.Text;
        }
    }
}