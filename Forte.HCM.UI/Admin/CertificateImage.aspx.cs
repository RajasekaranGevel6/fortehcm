﻿
using System;
using System.IO;
using System.Web.UI;
using System.Drawing.Imaging;

namespace Forte.HCM.UI.Admin
{
    public partial class CertificateImage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["CERTIFICATE_IMAGE"] != null)
                {
                    // Change the response headers to output a JPEG image.
                    this.Response.Clear();
                    this.Response.ContentType = "image/jpeg";

                    MemoryStream ms = new MemoryStream(Session["CERTIFICATE_IMAGE"] as byte[]);
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms);

                    // Write the image to the response stream in JPEG format.
                    image.Save(this.Response.OutputStream, ImageFormat.Jpeg);
                }
            }
        }
    }
}
