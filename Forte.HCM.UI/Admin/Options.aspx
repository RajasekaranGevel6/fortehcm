<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="Options.aspx.cs" Inherits="Forte.HCM.UI.Admin.Options" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="Options_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="Options_headerLiteral" runat="server" Text="Options"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="Options_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="Options_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="Options_topResetLinkButton" runat="server" Text="Reset" OnClick="Options_resetLinkButton_Click"
                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="Options_topCancelLinkButton" runat="server" Text="Cancel" OnClick="ParentPageRedirect"
                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="Options_messageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="Options_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="Options_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Options_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="Options_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="Options_formulaSettingLabel" runat="server" Text="Formula Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="Options_complexityUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="Options_complexityFactorLabel" runat="server" Text="Complexity Factor"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="Options_helpComplexityFactorImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the complexity factor here" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:Label ID="Options_simpleLabel" runat="server" Text="Simple" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="Options_simpleTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <%-- Here, InputDirection = LeftToRight assigned to avoid by entering
                                                         decimal value after period (.). If we use RightToLeft, we can't key-in after the period.
                                                         Eg: 1. --%>
                                                        <ajaxToolKit:MaskedEditExtender ID="Options_simpleMaskedEditExtender" runat="server"
                                                            TargetControlID="Options_simpleTextBox" Mask="9.99" MessageValidatorTip="true"
                                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Number"
                                                            AcceptNegative="None" ErrorTooltipEnabled="True" InputDirection="LeftToRight" />
                                                        <td style="width: 6%;">
                                                            <asp:Label ID="Options_mediumLabel" runat="server" Text="Medium" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="Options_mediumTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="Options_mediumMaskedEditExtender" runat="server"
                                                            TargetControlID="Options_mediumTextBox" Mask="9.99" MessageValidatorTip="true"
                                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Number"
                                                            AcceptNegative="None" ErrorTooltipEnabled="True" InputDirection="LeftToRight" />
                                                        <td style="width: 6%;">
                                                            <asp:Label ID="Options_complexLabel" runat="server" Text="Complex" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="Options_complexTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="Options_complexMaskedEditExtender" runat="server"
                                                            TargetControlID="Options_complexTextBox" Mask="9.99" MessageValidatorTip="true"
                                                            InputDirection="LeftToRight" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="Number" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                        <td align="left">
                                                            <asp:LinkButton ID="Options_restoreComplexityLinkButton" runat="server" Text="Restore Defaults"
                                                                ToolTip="Click here to restore default complexity factor values" SkinID="sknActionLinkButton"
                                                                OnClick="Options_restoreComplexityLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="Options_restoreComplexityLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="Options_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="Options_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="Options_computationUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="Options_computationFactorLabel" Text="Computation Factor" runat="server"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="Options_helpComputationFactorImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the computation factor here" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="Options_computationFactorTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Options_restoreComputationLinkButton" runat="server" Text="Restore Defaults"
                                                                ToolTip="Click here to restore default computation factor value" SkinID="sknActionLinkButton"
                                                                OnClick="Options_restoreComputationLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="Options_compFactorMaskedEditExtender" runat="server"
                                                            TargetControlID="Options_computationFactorTextBox" Mask="9.99" MessageValidatorTip="true"
                                                            InputDirection="LeftToRight" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="Number" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="Options_restoreComputationLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="Options_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="Options_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="Options_maxRecencyUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="Options_maximumRecencyFactorLabel" runat="server" Text="Maximum Recency Factor"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="Options_helpMaxRecencyFactorImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the maximum recency factor here" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="Options_maxRecencyFactorTextBox" runat="server" Width="30px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="Options_maxRecencyMaskedEditExtender" runat="server"
                                                            TargetControlID="Options_maxRecencyFactorTextBox" Mask="99" MessageValidatorTip="true"
                                                            InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="Number" AutoComplete="false" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="Options_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="Options_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="Options_generalSettingLiteral" runat="server" Text="General Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="Options_generalSettingUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="Options_gridPageSizeLabel" runat="server" Text="Grid Page Size" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="Options_helpGridPgSizeImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the grid page size here" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Options_gridPageSizeTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <ajaxToolKit:MaskedEditExtender ID="Options_gridSizeMaskedEditExtender" runat="server"
                                                    TargetControlID="Options_gridPageSizeTextBox" Mask="999" MessageValidatorTip="true"
                                                    InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                    MaskType="Number" AcceptNegative="None" AutoComplete="false" ErrorTooltipEnabled="True" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="Options_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="Options_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="Options_talentScoutSettingsLiteral" runat="server" Text="TalentScout Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="Options_numberOfSpheresUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="Options_defaultNumberOfSpheresLabel" runat="server" Text="Default Number Of Spheres"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="Options_defaultNumberOfSpheresImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the default number of spheres to be shown in TalentScout here" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Options_defaultNumberOfSpheresTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <ajaxToolKit:MaskedEditExtender ID="Options_defaultNumberOfSpheresMaskedEditExtender"
                                                    runat="server" TargetControlID="Options_defaultNumberOfSpheresTextBox" Mask="999"
                                                    MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                    OnInvalidCssClass="MaskedEditError" MaskType="Number" AcceptNegative="None" AutoComplete="false"
                                                    ErrorTooltipEnabled="True" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="Options_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="Options_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="Options_proximityFactorLiteral" runat="server" Text="Proximity Factor"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <div style="height: 120px; overflow: auto;" runat="server">
                                            <asp:GridView ID="Options_proximityGridView" runat="server" DataKeyNames="AttributeID,AttributeName"
                                                GridLines="Horizontal" BorderColor="white" BorderWidth="1px" Width="100%" AutoGenerateColumns="false">
                                                <RowStyle CssClass="grid_alternate_row" />
                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                <HeaderStyle CssClass="grid_header_row" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="120px" DataField="AttributeName" HeaderText="Factor" />
                                                    <asp:TemplateField ItemStyle-Width="60px" HeaderText="Value">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Options_proximityFactorTextBox" runat="server" Text='<%# Eval("AttributeValue") %>'></asp:TextBox>
                                                            <ajaxToolKit:MaskedEditExtender ID="Options_proximityFactorMaskedEditExtender" runat="server"
                                                                TargetControlID="Options_proximityFactorTextBox" Mask="99.99" MessageValidatorTip="true"
                                                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Number"
                                                                AcceptNegative="None" ErrorTooltipEnabled="True" InputDirection="RightToLeft" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="Options_bottomMessageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="Options_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="Options_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Options_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="Options_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="4" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="Options_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                OnClick="Options_saveButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="Options_bottomResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                OnClick="Options_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="Options_bottomCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
