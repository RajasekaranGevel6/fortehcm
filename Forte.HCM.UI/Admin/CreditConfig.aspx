﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="CreditConfig.aspx.cs" Inherits="Forte.HCM.UI.Admin.CreditConfig"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="CreditConfig_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CreditConfig_headerLiteral" runat="server" Text="Credit Config"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="CreditConfig_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="CreditConfig_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CreditConfig_topResetLinkButton" runat="server" Text="Reset"
                                            OnClick="CreditConfig_resetLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CreditConfig_topCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreditConfig_messageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CreditConfig_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreditConfig_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="CreditConfig_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="CreditConfig_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CreditConfig_questionCreditsLiteral" runat="server" Text="Question Credits"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <div style="height: 110px; overflow: auto;" runat="server">
                                            <asp:GridView ID="CreditConfig_questionCreditsGridView" DataKeyNames="QUESTION_GENID,QUESTION_CREDIT_TYPE"
                                                runat="server" width="100%" AutoGenerateColumns="false" GridLines="Horizontal" BorderColor="white"
                                                BorderWidth="1px">
                                                <RowStyle CssClass="grid_alternate_row" />
                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                <HeaderStyle CssClass="grid_header_row" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="50%" HeaderText="Credit Type" DataField="QUESTION_CREDIT_TYPE" />
                                                    <asp:TemplateField HeaderText="Amount (in $)" ItemStyle-Width="25%">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="CreditConfig_questionCreditsTextBox" runat="server" Text='<%# Eval("QUESTION_CREDIT_VALUE") %>'></asp:TextBox>
                                                            <ajaxToolKit:MaskedEditExtender ID="CreditConfig_questionCreditsMaskedEditExtender"
                                                                runat="server" TargetControlID="CreditConfig_questionCreditsTextBox" Mask="99.99"
                                                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Number"
                                                                InputDirection="RightToLeft" AcceptNegative="None" AutoComplete="true" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="25%" HeaderText="Usage Type" DataField="QUESTION_USAGE_TYPE" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CreditConfig_testCreditsLiteral" runat="server" Text="Test Credits"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <div style="height: 77px; overflow: auto;" runat="server">
                                            <asp:GridView ID="CreditConfig_testCreditsGridView" DataKeyNames="TEST_GENID,TEST_CREDIT_TYPE"
                                                runat="server" AllowSorting="true" AutoGenerateColumns="false" GridLines="Horizontal"
                                                BorderColor="white" BorderWidth="1px" width="100%">
                                                <RowStyle CssClass="grid_alternate_row" />
                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                <HeaderStyle CssClass="grid_header_row" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="50%" HeaderText="Credit Type" DataField="TEST_CREDIT_TYPE" />
                                                    <asp:TemplateField ItemStyle-Width="25%" HeaderText="Amount (in $)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="CreditConfig_testCreditsTextBox" runat="server" Text='<%# Eval("TEST_CREDIT_VALUE") %>'></asp:TextBox>
                                                            <ajaxToolKit:MaskedEditExtender ID="CreditConfig_testCreditsMaskedEditExtender" runat="server"
                                                                TargetControlID="CreditConfig_testCreditsTextBox" Mask="99.99" MessageValidatorTip="true"
                                                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Number"
                                                                AcceptNegative="None" InputDirection="RightToLeft" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="25%" HeaderText="Usage Type" DataField="TEST_USAGE_TYPE" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CreditConfig_testSessionCreditsLiteral" runat="server" Text="Test Session Credits"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <div style="height: 77px; overflow: auto;" runat="server">
                                            <asp:GridView ID="CreditConfig_testSessionCreditsGridView" DataKeyNames="TEST_SESSION_GENID,TEST_SESSION_CREDIT_TYPE"
                                                runat="server" AllowSorting="true" AutoGenerateColumns="false" GridLines="Horizontal"
                                                BorderColor="white" BorderWidth="1px" width="100%">
                                                <RowStyle CssClass="grid_alternate_row" />
                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                <HeaderStyle CssClass="grid_header_row" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="50%" HeaderText="Credit Type" DataField="TEST_SESSION_CREDIT_TYPE" />
                                                    <asp:TemplateField ItemStyle-Width="25%" HeaderText="Amount (in $)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="CreditConfig_testSessionCreditsTextBox" runat="server" Text='<%# Eval("TEST_SESSION_CREDIT_VALUE") %>'></asp:TextBox>
                                                            <ajaxToolKit:MaskedEditExtender ID="CreditConfig_testCreditsMaskedEditExtender" runat="server"
                                                                TargetControlID="CreditConfig_testSessionCreditsTextBox" Mask="99.99" OnFocusCssClass="MaskedEditFocus"
                                                                OnInvalidCssClass="MaskedEditError" MaskType="Number" AcceptNegative="None" InputDirection="RightToLeft" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="25%" HeaderText="Usage Type" DataField="TEST_SESSION_USAGE_TYPE" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="Literal1" runat="server" Text="Request Credits"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <div style="height: 52px; overflow: auto;" runat="server">
                                            <asp:GridView ID="CreditConfig_requestCreditsGridView" runat="server" DataKeyNames="REQUEST_SESSION_GENID,REQUEST_CREDIT_TYPE"
                                                AutoGenerateColumns="false" GridLines="Horizontal" BorderColor="white" BorderWidth="1px" width="100%">
                                                <RowStyle CssClass="grid_alternate_row" />
                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                <HeaderStyle CssClass="grid_header_row" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="50%" HeaderText="Credit Type" DataField="REQUEST_CREDIT_TYPE" />
                                                    <asp:TemplateField ItemStyle-Width="25%" HeaderText="Amount (in $)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="CreditConfig_requestCreditsTextBox" runat="server" Text='<%# Eval("REQUEST_CREDIT_VALUE") %>'></asp:TextBox>
                                                            <ajaxToolKit:MaskedEditExtender ID="CreditConfig_testCreditsMaskedEditExtender" runat="server"
                                                                TargetControlID="CreditConfig_requestCreditsTextBox" Mask="99.99" MessageValidatorTip="true"
                                                                MaskType="Number" AcceptNegative="None" InputDirection="RightToLeft" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="25%" HeaderText="Usage Type" DataField="REQUEST_USAGE_TYPE" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreditConfig_bottomMessageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CreditConfig_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreditConfig_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="CreditConfig_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="CreditConfig_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="4" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="CreditConfig_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                OnClick="CreditConfig_saveButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="CreditConfig_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="CreditConfig_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="CreditConfig_bottomCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
