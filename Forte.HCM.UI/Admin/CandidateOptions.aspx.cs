﻿#region Header

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateOptions.aspx.cs
// File that represents the user interface layout and functionalities for
// the CandidateOptions page. This page helps in configuring the candidate
// settings such as max number of retakes for self admin tests, max number 
// of self admin tests taken in a month, session expiry days for self 
// admin tests etc.

#endregion Header

#region Directives

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the CandidateOptions page. This page helps in configuring the candidate
    /// settings such as max number of retakes for self admin tests, max number 
    /// of self admin tests taken in a month, session expiry days for self 
    /// admin tests etc. This class inherits the Forte.HCM.UI.Common.PageBase
    /// class.
    /// </summary>
    public partial class CandidateOptions : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus
                Page.Form.DefaultButton = CandidateOptions_topSaveButton.UniqueID;
                CandidateOptions_maxNoOfRetakesTextBox.Focus();

                // Set page title
                Master.SetPageCaption("Candidate Options");

                if (!IsPostBack)
                {
                    // Clear all top and bottom label test
                    ClearAllLabelMessage();

                    // Load default values
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CandidateOptions_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAllLabelMessage();

                // Validate the input.
                bool isValid = IsValidData();

                if (isValid == false)
                    return;

                // Create a candidate admin settings object.
                CandidateAdminSettings adminSettings = new CandidateAdminSettings();

                // Assign the data to the corresponding fields.
                adminSettings.MaxSelfAdminTestRetakes = Convert.ToInt32
                    (CandidateOptions_maxNoOfRetakesTextBox.Text);
                adminSettings.MaxSelfAdminTestPerMonth = Convert.ToInt32
                    (CandidateOptions_maxNoOfTestsTextBox.Text);
                adminSettings.CandidateSessionExpiryInDays = Convert.ToInt32
                   (CandidateOptions_sessionExpiryDaysTextBox.Text);
                adminSettings.ResumeExpiryDays = Convert.ToInt32
                   (CandidateOptions_resumeExpiryDaysTextBox.Text);
                adminSettings.ActivitiesGridPageSize = Convert.ToInt32
                   (CandidateOptions_activitiesGridPageSizeTextBox.Text);
                adminSettings.SearchTestGridPageSize = Convert.ToInt32
                   (CandidateOptions_searchTestGridPageSizeTextBox.Text);
                adminSettings.MinQuestionPerSelfAdminTest = Convert.ToInt32
                   (CandidateOptions_minSelfAdminTestQuestionTextBox.Text);
                adminSettings.MaxQuestionPerSelfAdminTest = Convert.ToInt32
                   (CandidateOptions_maxSelfAdminTestQuestionTextBox.Text);

                // Update the settings into the database.
                new AdminBLManager().UpdateCandidateAdminSettings(adminSettings, base.userID);

                // Show a success message.
                base.ShowMessage(CandidateOptions_topSuccessMessageLabel,
                    CandidateOptions_bottomSuccessMessageLabel,
                    "Candidate options saved successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the max number of tests
        /// restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the max number of tests value to default.
        /// </remarks>
        protected void CandidateOptions_maxNoOfTestsRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_maxNoOfTestsTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.MAX_NO_OF_TESTS;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the max number of retakes
        /// restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the max number of retakes value to default.
        /// </remarks>
        protected void CandidateOptions_maxNoOfRetakesRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_maxNoOfRetakesTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.MAX_NO_OF_RETAKES;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the session expiry days 
        /// restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the session expiry days value to default.
        /// </remarks>
        protected void CandidateOptions_sessionExpiryDaysRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_sessionExpiryDaysTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.SESSION_EXPIRY_DAYS;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the resume expiry days 
        /// restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the session expiry days value to default.
        /// </remarks>
        protected void CandidateOptions_resumeExpiryDaysRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_resumeExpiryDaysTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.RESUME_EXPIRY_DAYS;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the activities grid page 
        /// size restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the activities grid page size value to default.
        /// </remarks>
        protected void CandidateOptions_activitiesGridPageSizeRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_activitiesGridPageSizeTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.ACTIVITIES_GRID_PAGE_SIZE;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search test grid 
        /// page size restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the search test grid page size value to default.
        /// </remarks>
        protected void CandidateOptions_searchTestGridPageSizeRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_searchTestGridPageSizeTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.SEARCH_TEST_GRID_PAGE_SIZE;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the min number of questions
        /// for self admin test restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the  min number of questions for self admin test value to default.
        /// </remarks>
        protected void CandidateOptions_minSelfAdminTestQuestionRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_minSelfAdminTestQuestionTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.MIN_QUESTION_PER_SELF_ADMIN_TEST;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the max number of questions
        /// for self admin test restore link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the  max number of questions for self admin test value to default.
        /// </remarks>
        protected void CandidateOptions_maxSelfAdminTestQuestionRestoreLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                CandidateOptions_maxSelfAdminTestQuestionTextBox.Text = Constants.
                    CandidateOptionsDefaultValuesConstants.MAX_QUESTION_PER_SELF_ADMIN_TEST;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirects to the same page by loading the previously
        /// saved values.
        /// </remarks>
        protected void CandidateOptions_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateOptions_topErrorMessageLabel,
                    CandidateOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that clear the message labels.
        /// </summary>
        private void ClearAllLabelMessage()
        {
            CandidateOptions_topErrorMessageLabel.Text = string.Empty;
            CandidateOptions_bottomErrorMessageLabel.Text = string.Empty;
            CandidateOptions_topSuccessMessageLabel.Text = string.Empty;
            CandidateOptions_bottomSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            int maxNoOfRetakes = 0;
            if (Int32.TryParse(CandidateOptions_maxNoOfRetakesTextBox.Text.Trim(), out  maxNoOfRetakes) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Max number of retakes is invalid");
                isValidData = false;
            }

            int maxNoOfTests = 0;
            if (Int32.TryParse(CandidateOptions_maxNoOfTestsTextBox.Text.Trim(), out  maxNoOfTests) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Max number of tests per month is invalid");
                isValidData = false;
            }
            else
            {
                if (maxNoOfTests == 0)
                {
                    base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                        "Max number of tests per month cannot be zero");
                    isValidData = false;
                }
            }

            int sessionExpiryDays = 0;
            if (Int32.TryParse(CandidateOptions_sessionExpiryDaysTextBox.Text.Trim(), out  sessionExpiryDays) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Session expiry days is invalid");
                isValidData = false;
            }
            else
            {
                if (sessionExpiryDays == 0)
                {
                    base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                        "Session expiry days cannot be zero");
                    isValidData = false;
                }
            }

            int resumeExpiryDays = 0;
            if (Int32.TryParse(CandidateOptions_resumeExpiryDaysTextBox.Text.Trim(), out  resumeExpiryDays) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Resume expiry days is invalid");
                isValidData = false;
            }
            else
            {
                if (resumeExpiryDays == 0)
                {
                    base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                        "Resume expiry days cannot be zero");
                    isValidData = false;
                }
            }

            int activitesGridPageSize = 0;
            if (Int32.TryParse(CandidateOptions_activitiesGridPageSizeTextBox.Text.Trim(), out  activitesGridPageSize) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Activities grid page size is invalid");
                isValidData = false;
            }
            else
            {
                if (activitesGridPageSize == 0)
                {
                    base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                        "Activities grid page size cannot be zero");
                    isValidData = false;
                }
            }

            int searchTestGridPageSize = 0;
            if (Int32.TryParse(CandidateOptions_searchTestGridPageSizeTextBox.Text.Trim(), out  searchTestGridPageSize) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Search test grid page size is invalid");
                isValidData = false;
            }
            else
            {
                if (searchTestGridPageSize == 0)
                {
                    base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                        "Search test grid page size cannot be zero");
                    isValidData = false;
                }
            }

            int minSelfAdminTestQuestion = 0;
            if (Int32.TryParse(CandidateOptions_minSelfAdminTestQuestionTextBox.Text.Trim(), out  minSelfAdminTestQuestion) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Min number of questions for self admin test is invalid");
                isValidData = false;
            }
            else
            {
                if (minSelfAdminTestQuestion == 0)
                {
                    base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                        "Min number of questions for self admin test cannot be zero");
                    isValidData = false;
                }
            }

            int maxSelfAdminTestQuestion = 0;
            if (Int32.TryParse(CandidateOptions_maxSelfAdminTestQuestionTextBox.Text.Trim(), out  maxSelfAdminTestQuestion) == false)
            {
                base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                    "Max number of questions for self admin test is invalid");
                isValidData = false;
            }
            else
            {
                if (maxSelfAdminTestQuestion == 0)
                {
                    base.ShowMessage(CandidateOptions_topErrorMessageLabel, CandidateOptions_bottomErrorMessageLabel,
                        "Max number of questions for self admin test cannot be zero");
                    isValidData = false;
                }
            }

            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Load current candidate admin settings.
            CandidateAdminSettings adminSettings = new AdminBLManager().
                GetCandidateAdminSettings();

            if (adminSettings == null)
                return;

            CandidateOptions_maxNoOfRetakesTextBox.Text = adminSettings.
                MaxSelfAdminTestRetakes.ToString();

            CandidateOptions_maxNoOfTestsTextBox.Text = adminSettings.
                MaxSelfAdminTestPerMonth.ToString();

            CandidateOptions_sessionExpiryDaysTextBox.Text = adminSettings.
                CandidateSessionExpiryInDays.ToString();

            CandidateOptions_resumeExpiryDaysTextBox.Text = adminSettings.
                ResumeExpiryDays.ToString();

            CandidateOptions_activitiesGridPageSizeTextBox.Text = adminSettings.
                ActivitiesGridPageSize.ToString();

            CandidateOptions_searchTestGridPageSizeTextBox.Text = adminSettings.
                SearchTestGridPageSize.ToString();

            CandidateOptions_minSelfAdminTestQuestionTextBox.Text = adminSettings.
               MinQuestionPerSelfAdminTest.ToString();

            CandidateOptions_maxSelfAdminTestQuestionTextBox.Text = adminSettings.
               MaxQuestionPerSelfAdminTest.ToString();
        }

        #endregion Protected Overridden Methods
    }
}