﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    AutoEventWireup="true" CodeBehind="CorporateSubscriptionManagement.aspx.cs" Inherits="Forte.HCM.UI.Admin.CorporateSubscriptionManagement" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<asp:Content ID="CorporateSubscriptionManagement_bodyContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CorporateSubscriptionManagement_headerLiteral" runat="server" Text="Corporate Subscription Management"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="CorporateSubscriptionManagement_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="CorporateSubscriptionManagement_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CorporateSubscriptionManagement_topCancelLinkButton" runat="server"
                                            Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CorporateSubscriptionManagement_messageUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CorporateSubscriptionManagement_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CorporateSubscriptionManagement_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="CorporateSubscriptionManagement_searchDivUpdatePanel">
                                <ContentTemplate>
                                    <div id="CorporateSubscriptionManagement_searchDiv" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="3">
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="CorporateSubscriptionManagement_simpleLinkButton" runat="server"
                                                        Text="Advanced" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="panel_bg">
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="panel_inner_body_bg" border="0"
                                                        style="width: 100%">
                                                        <tr>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="CorporateSubscriptionManagement_nameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Name"></asp:Label>
                                                            </td>
                                                            <td style="width: 19%">
                                                                <asp:TextBox ID="CorporateSubscriptionManagement_nameTextBox" runat="server" Width="175PX"
                                                                    MaxLength="100"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="CorporateSubscriptionManagement_titleLabel" runat="server" Text="Title"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 21%">
                                                                <asp:TextBox ID="CorporateSubscriptionManagement_titleTextBox" runat="server" Width="185px"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 8%" align="center">
                                                                <asp:Label ID="CorporateSubscriptionManagement_companyLabel" runat="server" Text="Company"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="CorporateSubscriptionManagement_companyTextBox" runat="server" MaxLength="100"
                                                                    Width="150px"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 2%">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5" colspan="7">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="CorporateSubscriptionManagement_countryLabel" runat="server" Text="Country"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="CorporateSubscriptionManagement_countryTextBox" runat="server" MaxLength="50"
                                                                    Width="175px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="CorporateSubscriptionManagement_activeStatusLabel" runat="server"
                                                                    Text="Active Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <div style="float: left; padding-right: 5px; width: 50%;">
                                                                    <asp:DropDownList ID="CorporateSubscriptionManagement_activeStatusDropDownList" runat="server"
                                                                        Width="100%" SkinID="sknSubjectDropDown">
                                                                        <asp:ListItem Value="-1" Text="Both"></asp:ListItem>
                                                                        <asp:ListItem Value="0" Text="Inactive"></asp:ListItem>
                                                                        <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div style="float: left; height: 16px;">
                                                                    <asp:ImageButton ID="CorporateSubscriptionManagement_activeStatusHelpImageButton" OnClientClick="javascript:return false;"
                                                                        SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" ToolTip="Select the active status" />
                                                                </div>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td style="width: 2%">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="CorporateSubscriptionManagement_businessTypeLabel" runat="server"
                                                                    SkinID="sknLabelFieldHeaderText" Text="Business Type"></asp:Label>
                                                            </td>
                                                            <td class="checkbox_list_bg" align="left" colspan="5">
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:CheckBoxList ID="CorporateSubscriptionManagement_businessTypeCheckBoxList" runat="server"
                                                                        RepeatDirection="Horizontal" RepeatColumns="8" CellSpacing="5" TextAlign="Right"
                                                                        DataTextField="BusinessTypeName" DataValueField="BusinessTypeID" Width="100%"
                                                                        TabIndex="5">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </td>
                                                            <td align="center">
                                                                <asp:ImageButton ID="CorporateSubscriptionManagement_businessTypeImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" ToolTip="Select the business type" OnClientClick="javascript:return false;"/> 
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="right" class="td_padding_top_5">
                                                                <asp:Button ID="CorporateSubscriptionManagement_topSearchButton" runat="server" Text="Search"
                                                                    SkinID="sknButtonId" OnClick="CorporateSubscriptionManagement_topSearchButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="CorporateSubscriptionManagement_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr id="CorporateSubscriptionManagement_searchTR" runat="server">
                                    <td style="width: 50%" class="header_text_bold">
                                        <asp:Literal ID="CorporateSubscriptionManagement_usersSearchResults" runat="server"
                                            Text="Users"></asp:Literal>&nbsp;<asp:Label ID="TestReport_sortHelpLabel" runat="server"
                                                SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 2%" align="right">
                                        <span id="CorporateSubscriptionManagement_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="CorporateSubscriptionManagement_searchResultsUpImage" runat="server"
                                                SkinID="sknMinimizeImage" /></span><span id="CorporateSubscriptionManagement_searchResultsDownSpan"
                                                    runat="server" style="display: block;"><asp:Image ID="CorporateSubscriptionManagement_searchResultsDownImage"
                                                        runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="CorporateSubscriptionManagement_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel ID="CorporateSubscriptionManagement_usersGridUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <div style="height: 200px; overflow: auto; display: none;" runat="server" id="CorporateSubscriptionManagement_usersDiv">
                                                    <asp:GridView ID="CorporateSubscriptionManagement_usersGridView" runat="server" AutoGenerateColumns="false"
                                                        OnSorting="CorporateSubscriptionManagement_usersGridView_Sorting" OnRowCommand="CorporateSubscriptionManagement_usersGridView_RowCommand"
                                                        OnRowCreated="CorporateSubscriptionManagement_usersGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_viewUsersIamgeButton"
                                                                        runat="server" SkinID="sknViewFormImageButton" ToolTip="Preview" CommandName="ViewUser" Visible="false"
                                                                        CommandArgument='<% #Eval("UserID") %>' />
                                                                    <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_editUsersImageButton"
                                                                        runat="server" SkinID="sknEditFormImageButton" ToolTip="Edit User" CommandName="EditUser"
                                                                        CommandArgument='<% #Eval("UserID") %>' />
                                                                    <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_deleteUserButton"
                                                                        runat="server" SkinID="sknDeleteImageButton" ToolTip="Delete User " CommandName="DeleteUser"
                                                                        CommandArgument='<% #Eval("UserID") %>' />
                                                                    <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_activeOrDeactiveImageButton"
                                                                        runat="server" SkinID="sknActiveImageButton" ToolTip="Activate User" Visible='<%# GetActiveIcon(Convert.ToInt16(Eval("IsActive"))) %>'
                                                                        CommandName="ActivateUser" CommandArgument='<% #Eval("UserID") %>' />
                                                                    <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_inActiveImageButton"
                                                                        runat="server" SkinID="sknInactiveImageButton" ToolTip="Deactivate User" Visible='<%# GetInActiveIcon(Convert.ToInt16(Eval("IsActive"))) %>'
                                                                        CommandName="DeactivateUser" CommandArgument='<% #Eval("UserID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name" SortExpression="NAME" ItemStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CorporateSubscriptionManagement_usersGridView_nameLabel" runat="server"
                                                                        Text='<% # Eval("FirstName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Title" SortExpression="TITLE" ItemStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CorporateSubscriptionManagement_usersGridView_titleLabel" runat="server"
                                                                        Text='<% # Eval("Title") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Company" SortExpression="COMPANY" ItemStyle-Width="18%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CorporateSubscriptionManagement_usersGridView_companyLabel" runat="server"
                                                                        Text='<% # Eval("Company") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Country" SortExpression="COUNTRY" ItemStyle-Width="18%"
                                                                Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CorporateSubscriptionManagement_usersGridView_countryLabel" runat="server"
                                                                        Text='<% # Eval("Country") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No Of Users" SortExpression="NUMBEROFUSERS" ItemStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CorporateSubscriptionManagement_usersGridView_noOfUsersLabel" runat="server"
                                                                        Text='<% # Eval("NoOfUsers") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Active " SortExpression="ISACTIVE" ItemStyle-Width="7%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CorporateSubscriptionManagement_usersGridView_activeLabel" runat="server"
                                                                        Text='<%# GetValue(Convert.ToInt16(Eval("IsActive"))) %>'></asp:Label>
                                                                    <asp:HiddenField ID="CorporateSubscriptionManagement_usersGridView_userIdHiddenField"
                                                                        runat="server" Value='<% #Eval("UserID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="CorporateSubscriptionManagement_pageNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CorporateSubscriptionManagement_bottomMessageUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CorporateSubscriptionManagement_bottomSuccessLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CorporateSubscriptionManagement_bottomErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="CorporateSubscriptionManagement_bottomCancelLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="CorporateSubscriptionManagement_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CorporateSubscriptionManagement_bottomResetLinkButton" runat="server"
                                            Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CorporateSubscriptionManagement_deleteUserUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="CorporateSubscriptionManagement_deleteUserHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="CorporateSubscriptionManagement_deleteUserPopupPanel" runat="server"
                            Style="display: none" CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="CorporateSubscriptionManagement_deleteUserConfirmMsgControl"
                                runat="server" OnOkClick="CorporateSubscriptionManagement_deleteUserConfirmMsgControl_OKClick"
                                OnCancelClick="CorporateSubscriptionManagement_deleteUserConfirmMsgControl_CancelClick" />
                            <asp:HiddenField ID="CorporateSubscriptionManagement_deleteUserHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CorporateSubscriptionManagement_deleteUserModalPopupExtender"
                            runat="server" PopupControlID="CorporateSubscriptionManagement_deleteUserPopupPanel"
                            TargetControlID="CorporateSubscriptionManagement_deleteUserHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CorporateSubscriptionManagement_activateUserUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="CorporateSubscriptionManagement_activateUserHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="CorporateSubscriptionManagement_activateUserPanel" runat="server"
                            Style="display: none" CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="CorporateSubscriptionManagement_activateUserConfirmMsgControl"
                                runat="server" OnOkClick="CorporateSubscriptionManagement_activateUserConfirmMsgControl_OKClick"
                                OnCancelClick="CorporateSubscriptionManagement_activateUserConfirmMsgControl_CancelClick" />
                            <asp:HiddenField ID="CorporateSubscriptionManagement_activateUserHiddenField" runat="server" />
                            <asp:HiddenField ID="CorporateSubscriptionManagement_activateStatusHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CorporateSubscriptionManagement_activateUserModalPopupExtender"
                            runat="server" PopupControlID="CorporateSubscriptionManagement_activateUserPanel"
                            TargetControlID="CorporateSubscriptionManagement_activateUserHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="panel_bg">
                <%-- <asp:UpdatePanel ID="CorporateSubscriptionManagement_previewUserUpdatePanel" runat="server">
                    <ContentTemplate>--%>
                <asp:Panel ID="CorporateSubscriptionManagement_previewUserPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_editCorporateAdmin">
                    <div style="display: none;">
                        <asp:Button ID="CorporateSubscriptionManagement_previewUserhiddenButton" runat="server"
                            Text="Hidden" /></div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="popup_td_padding_10">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Literal ID="CorporateSubscriptionManagement_previewUserLiteral" runat="server"
                                                Text="Preview"></asp:Literal>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="CorporateSubscriptionManagement_previewUserTopCancelImageButton"
                                                            runat="server" SkinID="sknCloseImageButton" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_10">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                    <tr>
                                        <td align="left" class="popup_td_padding_10">
                                            <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                align="left">
                                                <tr>
                                                    <td class="msg_align" colspan="2">
                                                        <asp:Label ID="CorporateSubscriptionManagement_previewUserErrorLabel" runat="server"
                                                            SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="CorporateSubscriptionManagement_previewUserLabel" runat="server" Text="Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 80%">
                                                                    <asp:Label ID="CorporateSubscriptionManagement_previewUserNameLabel" runat="server"
                                                                        SkinID="sknLabelFieldHeaderText">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="CorporateSubscriptionManagement_previewUserCompanyLabel" runat="server"
                                                                        Text="Company" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 80%">
                                                                    <asp:Label ID="CorporateSubscriptionManagement_previewUserCompanyNameLabel" runat="server"
                                                                        SkinID="sknLabelFieldHeaderText">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="CorporateSubscriptionManagement_previewUserCountryLabel" runat="server"
                                                                        Text="Country" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 80%">
                                                                    <asp:Label ID="CorporateSubscriptionManagement_previewUserCountryNameLabel" runat="server"
                                                                        SkinID="sknLabelFieldHeaderText">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                </td>
                                                                <td style="width: 80%">
                                                                    <asp:CheckBox ID="CorporateSubscriptionManagement_previewUserIsActiveCheckbox" Text="Is Active"
                                                                        Enabled="false" Visible="false" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="CorporateSubscriptionManagement_previewUserModalPopupExtender"
                    runat="server" PopupControlID="CorporateSubscriptionManagement_previewUserPanel"
                    TargetControlID="CorporateSubscriptionManagement_previewUserhiddenButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CorporateSubscriptionManagement_editUserUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Panel ID="CorporateSubscriptionManagement_editUserPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_editCorporateAdmin">
                            <div style="display: none;">
                                <asp:Button ID="CorporateSubscriptionManagement_editUserButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="CorporateSubscriptionManagement_editUserLiteral" runat="server"
                                                        Text="Edit User"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="CorporateSubscriptionManagement_editUserImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="2">
                                                                <asp:Label ID="CorporateSubscriptionManagement_editUserLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CorporateSubscriptionManagement_editUserNameLabel" runat="server"
                                                                                Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CorporateSubscriptionManagement_editUserTextbox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="CorporateSubscriptionManagement_editLastNameLabel" runat="server"
                                                                                Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CorporateSubscriptionManagement_editUserLastnameTextbox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CorporateSubscriptionManagement_editUserPhoneNumberLabel" runat="server"
                                                                                Text="Phone Number" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CorporateSubscriptionManagement_editUserPhoneNumberTextbox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="CorporateSubscriptionManagement_editUserCompanyLabel" runat="server"
                                                                                Text="Company Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CorporateSubscriptionManagement_editUserCompanyTextbox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CorporateSubscriptionManagement_editUserTitleLabel" runat="server"
                                                                                Text="Title" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CorporateSubscriptionManagement_editUserTitleTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="CorporateSubscriptionManagement_editUserNoOfUsersLabel" runat="server"
                                                                                Text="Number of Users" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                            <asp:HiddenField ID="CorporateSubscriptionManagement_usersGridView_noOfUsersHiddenField"
                                                                                runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="CorporateSubscriptionManagement_editUserNoOfUsersTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CorporateSubscriptionManagement_editUserRolesLabel" runat="server"
                                                                                Text="Roles" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td class="checkbox_list_bg" align="left" colspan="4">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:CheckBoxList ID="CorporateSubscriptionManagement_editUserRolesCheckBoxList"
                                                                                    runat="server" RepeatDirection="Horizontal" RepeatColumns="3" CellSpacing="5"
                                                                                    TextAlign="Right" DataTextField="RoleName" DataValueField="RoleID" Width="100%"
                                                                                    TabIndex="5">
                                                                                </asp:CheckBoxList>
                                                                            </div>
                                                                        </td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 20%">
                                                            </td>
                                                            <td style="width: 80%">
                                                                <asp:CheckBox ID="CorporateSubscriptionManagement_editUserIsActiveCheckBox" Text="Is Active"
                                                                    runat="server" Visible="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            </td> </tr>
                            <tr>
                                <td class="popup_td_padding_5">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td align="left" style="width: 20px; padding-right: 5px">
                                                <asp:HiddenField ID="CorporateSubscriptionManagement_editUserHiddenField" runat="server" />
                                                <asp:Button ID="CorporateSubscriptionManagement_editUserSaveButton" runat="server"
                                                    SkinID="sknButtonId" Text="Save" OnClick="CorporateSubscriptionManagement_editUserButton_Clicked" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CorporateSubscriptionManagement_editUserCloseLinkButton" SkinID="sknPopupLinkButton"
                                                    runat="server" Text="Cancel" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CorporateSubscriptionManagement_editUserModalPopupExtender"
                            runat="server" PopupControlID="CorporateSubscriptionManagement_editUserPanel"
                            TargetControlID="CorporateSubscriptionManagement_editUserButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
