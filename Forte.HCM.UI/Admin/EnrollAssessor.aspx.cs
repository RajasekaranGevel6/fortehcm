﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EnrollAssessor.aspx.cs
// File that represents the enrollCutomer class that defines the user 
// interface layout and functionalities for the customer enrollment page. This 
// page helps in managing the customer enrollment page. 

#endregion Header

#region Directives

using System;
using System.IO;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

using AjaxControlToolkit;


#endregion Directives

namespace Forte.HCM.UI.Admin
{
    public partial class EnrollAssessor : PageBase
    {
        #region Public Methods
        DataTable dttempTable = null;
        //public event Forte.HCM.UI.CommonControls.ClientInfoControl.AddClientEvent AddClientEvent_Command;

        /// <summary> 
        /// Selected Dates Collection of Calendar
        /// </summary> 
        public List<DateTime> SelectedDates
        {
            get
            {
                if (ViewState["Dates"] == null)
                    // Add a hidden dateTime to clear the selection of Date when 
                    // there is only one date in the selected Dates 
                    ViewState["Dates"] = new List<DateTime>() { DateTime.MaxValue.AddDays(-2) };
                return (List<DateTime>)ViewState["Dates"];
            }
            set
            {
                ViewState["Dates"] = value;
            }
        }

        #endregion Public Methods

        #region Event Handler

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage(); 

                if (Request.QueryString.Get("assessorid") != null)
                {
                    EnrollAssessor_headerLiteral.Text = "Edit Assessor";
                    Master.SetPageCaption("Edit Assessor");
                }
                else
                {
                    EnrollAssessor_headerLiteral.Text = "Assessor";
                    Master.SetPageCaption("Assessor");
                }

                if (!IsPostBack)
                { 

                    EnrollAssessor_topSaveButton.Text = "Save";
                    EnrollAssessor_bottomSaveButton.Text = "Save";
                    EnrollAssessor_profileLastUpdatedLabel.Text = string.Empty;
                    // Clear assessor photo from the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = null;
                    Session["PHOTO_CHANGED"] = false;

                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "CANDIDATE_NAME";
                    Session["PHOTO_SIZE"] = ConfigurationManager.AppSettings["CANDIDATE_PHOTO_MAX_SIZE_IN_BYTES"].ToString();
                    EnrollAssessor_assessorEventsTable.Visible = false;
                    EnrollAssessor_assessmentCompletedCountLinkButton.Enabled = false;
                    EnrollAssessor_assessmentInprogressCountLinkButton.Enabled = false;
                    EnrollAssessor_assessmentInitiatedCountLinkButton.Enabled = false;
                    EnrollAssessor_assessmentSchduledCountLinkButton.Enabled = false;

                     
                    UserDetail userdetail = null;

                    if (Request.QueryString.Get("assessorid") != null)
                    {
                        userdetail = new CommonBLManager().GetUserDetail(Convert.ToInt32(Request.QueryString.Get("assessorid")));
                         
                    }
                    else
                    {
                       userdetail= Session["USER_DETAIL"] as UserDetail;
                    }

                    if (userdetail.SubscriptionRole == "SR_COR_AMN")
                        EnrollAssessor_newAssessorSearchImageButton.Visible = true;
                    else
                    {
                        EnrollAssessor_newAssessorSearchImageButton.Visible = false;

                        EnrollAssessor_newAssessorUserIDhiddenField.Value = userdetail.UserID.ToString();

                        LoadAssessorDetail();
                        LoadAssessorEvents(Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value)); 
                        LoadAssessmentDetails(1, null);
                    }
                    

                    /*if (Request.QueryString.Get("assessorid") != null)
                    { 
                        EnrollAssessor_newAssessorSearchImageButton.Visible = false;
                        EnrollAssessor_newAssessorUserIDhiddenField.Value = Request.QueryString.Get("assessorid");
                        EnrollAssessor_headerLiteral.Text = "Edit Assessor";
                        Master.SetPageCaption("Edit Assessor");
                        LoadAssessorDetail();
                        LoadAssessorEvents(Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value));
                        // Load assessment details.
                        LoadAssessmentDetails(1,null);
                    }
                    else
                    {
                        EnrollAssessor_headerLiteral.Text = "Assessor";
                        Master.SetPageCaption("Assessor");
                        //EnrollAssessor_newAssessorSearchImageButton.Visible = true;
                    }*/

                    Page.SetFocus(EnrollAssessor_topSaveButton.ClientID);
                    
                    EnrollAssessor_newAssessorSearchImageButton.Attributes.Add("onclick",
                       "return SearchCorporateUsers('" + EnrollAssessor_newAssessorUserIDhiddenField.ClientID + "','" +
                        EnrollAssessor_newAssessorUserNameTextBox.ClientID + "','" +
                        EnrollAssessor_newAssessorFirstNameTextBox.ClientID + "','" +
                        EnrollAssessor_newAssessorLastNameTextBox.ClientID + "')");

                    EnrollAssesssor_SearchSkillButton.Attributes.Add("onclick",
                        "return LoadSubjectLookUp('" + EnrollAssessor_CaegoryIDHiddenField.ClientID + "','" +
                        EnrollAssessor_newAssessorSkillTextBox.ClientID +"','" +
                        EnrollAssessor_SkillIDHiddenField.ClientID + "','" + EnrollAssessor_skillHiddenField .ClientID+ "','" + EnrollAssesssor_NewSkillButton.ClientID + "')");  
                }

                // Check is post back is due to photo upload. If yes then show 
                // the preview.
                if (!string.IsNullOrEmpty(EnrollAssessor_photoUploadedHiddenFiled.Value) &&
                    EnrollAssessor_photoUploadedHiddenFiled.Value.Trim().ToUpper() == "Y")
                {
                    // Show preview.  
                    if (!string.IsNullOrEmpty(EnrollAssessor_newAssessorUserIDhiddenField.Value))
                        ShowPreview(Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value));
                }  

                // Set the visibility of the 'clear' photo link.
                if(Session["ASSESSOR_THUMBNAIL_PHOTO"]==null)
                    EnrollAssessor_photoClearLinkButton.Style.Add("display", "none");

                if (string.IsNullOrEmpty(EnrollAssessor_newAssessorUserIDhiddenField.Value))
                    EnrollAssessor_calendarImageButton.Enabled = false;
                else
                    EnrollAssessor_calendarImageButton.Enabled = true;

                EnrollAssessor_pagingNavigator.PageNumberClick += new CommonControls.
                PageNavigator.PageNumberClickEventHandler(EnrollAssessor_pagingNavigator_PageNumberClick); 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        

        protected void EnrollAssesssor_NewSkillButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(EnrollAssessor_newAssessorUserIDhiddenField.Value)) return;

                if (EnrollAssessor_newAssessorSkillTextBox.
                    Text.Trim() == "Enter/Select skill name") return;

                if (EnrollAssessor_newAssessorSkillTextBox.
                    Text.Trim() == "" && EnrollAssessor_skillHiddenField.Value.ToString() =="") return; 
                 
                dttempTable = new DataTable();

                if (ViewState["AssessorSkills"] == null)
                {
                    dttempTable.Columns.Add("Category", typeof(string));
                    dttempTable.Columns.Add("SkillID", typeof(int));
                    dttempTable.Columns.Add("Skill", typeof(string));
                    dttempTable.AcceptChanges();
                }
                else
                    dttempTable = (DataTable)ViewState["AssessorSkills"];

                string skill = null;
                if (EnrollAssessor_newAssessorSkillTextBox.Text != "")
                    skill = EnrollAssessor_newAssessorSkillTextBox.Text.Trim();
                else if (EnrollAssessor_skillHiddenField.Value.ToString() != "")
                    skill = EnrollAssessor_skillHiddenField.Value.ToString().Trim();

                 
                string[] skillArray = null;
                skillArray = skill.Split(',');

                /*if (skill.IndexOf(',') > 0)
                {
                     skill.Remove(skill.LastIndexOf(',')); 
                     skillArray = skill.Split(',');
                }
                else
                    skillArray  = skill.Split(',');*/

                foreach (string strSkill in skillArray)
                {
                    if (strSkill == "") break;

                    DataTable dttemp = new InterviewBLManager().getSubjectCategoryID(strSkill.Trim());

                    if (dttemp == null) break;

                    if (dttemp.Rows.Count > 0)
                    {
                        int rowcount = 0;

                        while(rowcount < dttempTable.Rows.Count)
                        {
                            if (dttempTable.Rows[rowcount][2].ToString().Trim() 
                                == Convert.ToString(dttemp.Rows[0][2]).Trim())
                            {
                                dttempTable.Rows.RemoveAt(rowcount);
                            }

                            rowcount += 1;
                        }
 
                        dttempTable.AcceptChanges();

                        DataRow dr = dttempTable.NewRow();
                        dr[0] = Convert.ToString(dttemp.Rows[0][0]); ;
                        dr[1] = Convert.ToString(dttemp.Rows[0][1]); ;
                        dr[2] = Convert.ToString(dttemp.Rows[0][2]);
 
                        dttempTable.Rows.Add(dr);
                        dttempTable.AcceptChanges(); 
                    }
                }
               

                ViewState["AssessorSkills"] = dttempTable;

                IDbTransaction transaction = new TransactionManager().Transaction;
                int resultVal = new InterviewBLManager().
                    InsertAssessorSkill(Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value), dttempTable, transaction);

                transaction.Commit();

                EnrollAssessor_newAssessorSkillTextBox.Text = "";
                EnrollAssessor_CaegoryIDHiddenField.Value = "";
                EnrollAssessor_SkillIDHiddenField.Value = "";

                EnrollAssessor_skillGridView.DataSource = null;
                EnrollAssessor_skillGridView.DataBind();

                EnrollAssessor_skillGridView.DataSource = dttempTable;
                EnrollAssessor_skillGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, "Unable to add this skill");
            }
        } 

        #region Protected Override Methods

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        void EnrollAssessor_pagingNavigator_PageNumberClick(object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;
                if(ViewState["ASSESSMENT_STATUS"] ==null)
                    LoadAssessmentDetails(e.PageNumber,null);
                else
                    LoadAssessmentDetails(e.PageNumber, ViewState["ASSESSMENT_STATUS"].ToString());

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
       
        protected override bool IsValidData()
        {
            return true;
        }
        #endregion Protected Override Methods
        /// <summary>
        /// Method to save the Assessor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollAssessor_topSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string nonAvailabilityDates = string.Empty;
                foreach (DateTime dt in SelectedDates)
                {
                    if (dt.ToShortDateString() != "12/29/9999")
                        nonAvailabilityDates = nonAvailabilityDates + dt.ToShortDateString() + ",";
                }
                nonAvailabilityDates = nonAvailabilityDates.TrimEnd(',');

                IDbTransaction transaction = new TransactionManager().Transaction;

                if (this.IsValid_Data())
                {
                    try
                    {
                        AssessorDetail assessorDetails = new AssessorDetail();
                        assessorDetails.UserID = Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value);
                        assessorDetails.UserEmail = EnrollAssessor_newAssessorUserNameTextBox.Text;
                        assessorDetails.Skill = EnrollAssessor_newAssessorSkillTextBox.Text;
                        assessorDetails.AlternateEmailID = EnrollAssessor_newAssessorAlternateEmailIDTextBox.Text;
                        assessorDetails.FirstName = EnrollAssessor_newAssessorFirstNameTextBox.Text;
                        assessorDetails.LastName = EnrollAssessor_newAssessorLastNameTextBox.Text;
                        assessorDetails.CreatedBy = base.userID;
                        assessorDetails.NonAvailabilityDates = nonAvailabilityDates;
                        assessorDetails.Image =null;
                        assessorDetails.Mobile = EnrollAssessor_newAssessorMobileNumberTextBox.Text;
                        assessorDetails.HomePhone = EnrollAssessor_newAssessorHomePhoneTextBox.Text;
                        assessorDetails.AdditionalInfo = EnrollAssessor_newAssessorAdditionalTextBox.Text;
                        assessorDetails.LinkedInProfile = EnrollAssessor_linkedInProfileTextBox.Text.Trim();

                        //Save assessor details
                        string retval = new AssessorBLManager().SaveAssessorDetails(assessorDetails);
                        
                        //Insert assessor skill details
                        /*dttempTable = (DataTable)ViewState["AssessorSkills"];
                        int resultVal = new InterviewBLManager().
                            InsertAssessorSkill(assessorDetails.UserID, dttempTable, transaction);
                        */
                        transaction.Commit();

                        //Save assessor photo
                        //SaveAssessorPhoto(assessorDetails.UserID);

                        EnrollAssessor_topSuccessMessageLabel.Text = string.Empty;
                        EnrollAssessor_bottomSuccessMessageLabel.Text = string.Empty;

                        EnrollAssessor_topSuccessMessageLabel.Visible = true;
                        EnrollAssessor_bottomSuccessMessageLabel.Visible = true;

                        EnrollAssessor_topErrorMessageLabel.Visible = false;
                        EnrollAssessor_bottomErrorMessageLabel.Visible = false;

                        if (retval == "1")
                            base.ShowMessage(EnrollAssessor_topSuccessMessageLabel,
                                EnrollAssessor_bottomSuccessMessageLabel, "Assessor added successfully");
                        else if (retval == "0")
                            base.ShowMessage(EnrollAssessor_topSuccessMessageLabel,
                               EnrollAssessor_bottomSuccessMessageLabel, "Assessor saved successfully");

                        EnrollAssessor_profileLastUpdatedLabel.Text = "Your profile last updated on " + base.GetDateFormat(DateTime.Now);
                    }
                    catch (Exception exception)
                    {
                        Logger.ExceptionLog(exception);
                        transaction.Rollback();
                        EnrollAssessor_topSuccessMessageLabel.Visible = false;
                        EnrollAssessor_bottomSuccessMessageLabel.Visible = false;

                        EnrollAssessor_topErrorMessageLabel.Visible = true;
                        EnrollAssessor_bottomErrorMessageLabel.Visible = true;

                        EnrollAssessor_topErrorMessageLabel.Text = string.Empty;
                        EnrollAssessor_bottomErrorMessageLabel.Text = string.Empty;
                        base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                            EnrollAssessor_bottomErrorMessageLabel, exception.Message);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }
      
        /// <summary>
        /// Handler will clear all error messages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollAssessor_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset assessor detail.
               /*if (Request.QueryString.Get("assessorid") != null)
                {
                    EnrollAssessor_newAssessorSearchImageButton.Visible = true; 
                    EnrollAssessor_headerLiteral.Text = "Assessor";
                }
                ResetAssessorDetail(true);*/

                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }

        }
        /// <summary>
        /// Handler will add the selected dates to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollAssessor_calendarNotAvailable_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedDates.Contains(EnrollAssessor_calendarNotAvailable.SelectedDate))
                    SelectedDates.Remove(EnrollAssessor_calendarNotAvailable.SelectedDate);
                else
                    SelectedDates.Add(EnrollAssessor_calendarNotAvailable.SelectedDate);

                //update selection
                ViewState["Dates"] = SelectedDates;
                EnrollAssessor_calendarNotAvailable.Font.Underline = false;

                EnrollAssessor_newAssessorModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler will highlight the selected dates in calendar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollAssessor_calendarNotAvailable_PreRender(object sender, EventArgs e)
        {
            try
            {
                // Reset Selected Dates
                EnrollAssessor_calendarNotAvailable.Font.Underline = false;
                EnrollAssessor_calendarNotAvailable.SelectedDates.Clear();
                string s = EnrollAssessor_bottomErrorMessageLabel.Text;
                EnrollAssessor_bottomErrorMessageLabel.Visible = true;
                // Select previously Selected Dates
                foreach (DateTime dt in SelectedDates)
                {
                    EnrollAssessor_calendarNotAvailable.SelectedDates.Add(dt);

                }
                //EnrollAssessor_newAssessorModalPopupExtender.Show();
                // Load assessor photo
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(EnrollAssessor_newAssessorUserIDhiddenField.Value))
                {
                    userID = Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value);
                    EnrollAssessor_assessorPhotoPreview.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_ASS_PHOTO&userid=" + userID;
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on the row created of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void EnrollAssessor_assessmentDetailsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (EnrollAssessor_assessmentDetailsGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on row command of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void EnrollAssessor_assessmentDetailsGridView_RowCommand(object sender, 
            GridViewCommandEventArgs e)
        {
            try
            {
                // Moved to row data bound   
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on the sorting of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void EnrollAssessor_assessmentDetailsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                EnrollAssessor_pagingNavigator.Reset();

                //Set question details based on the values
                if (ViewState["ASSESSMENT_STATUS"] == null)
                    LoadAssessmentDetails(1, null);
                else
                    LoadAssessmentDetails(1, ViewState["ASSESSMENT_STATUS"].ToString());
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on the row data of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void EnrollAssessor_assessmentDetailsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField EnrollAssessor_assessmentDetailsTestStatusHiddenField = (HiddenField)e.Row.FindControl
                        ("EnrollAssessor_assessmentDetailsTestStatusHiddenField");

                    string value = ((HiddenField)e.Row.
                    FindControl("EnrollAssessor_assessmentDetailsGridView_CandidateInterviewSessionKey")).Value;
                    string assessorId = ((HiddenField)e.Row.
                    FindControl("EnrollAssessor_assessmentDetailsGridView_AssessorId")).Value;
                    string interviewSessionvalue = ((HiddenField)e.Row.
                    FindControl("EnrollAssessor_assessmentDetailsGridView_InterviewSessionKey")).Value;
                    string attemptID = ((HiddenField)e.Row.
                    FindControl("EnrollAssessor_assessmentDetailsGridView_attemptIDHiddenField")).Value;
                    string interviewTestKey = ((HiddenField)e.Row.
                    FindControl("EnrollAssessor_assessmentDetailsGridView_testKeyHiddenField")).Value;

                    #region Open view interview summary in new window
                    HyperLink EnrollAssessor_assessmentDetailsSelectHyperLink =
                        (HyperLink)e.Row.FindControl("EnrollAssessor_assessmentDetailsSelectHyperLink");

                    EnrollAssessor_assessmentDetailsSelectHyperLink.NavigateUrl =
                        "~/InterviewReportCenter/InterviewCandidateTestDetails.aspx?candidatesession="
                        + value.Trim() + "&testkey=" + interviewTestKey.Trim() + "&sessionkey=" + interviewSessionvalue.Trim() +
                        "&assessorId=" + assessorId + "&attemptid=" + attemptID.Trim() + "&m=4&s=0&parentpage=E_ASS";
                    #endregion

                    #region Open candidate rating summary in new window
                    HyperLink EnrollAssessor_assessmentDetailsCandidateRatingSummaryHyperLink =
                        (HyperLink)e.Row.FindControl("EnrollAssessor_assessmentDetailsCandidateRatingSummaryHyperLink");

                    EnrollAssessor_assessmentDetailsCandidateRatingSummaryHyperLink.NavigateUrl =
                        "~/Assessments/CandidateAssessorSummary.aspx?m=4&s=0&candidatesessionid="
                          + value.Trim() + "&assessorId=" + assessorId + "&attemptid=" + attemptID.Trim() +
                          "&parentpage=E_ASS";

                    #endregion

                    #region Open tracking details in new window
                    HyperLink EnrollAssessor_assessmentDetailsViewTrackingDetailsHyperLink =
                        (HyperLink)e.Row.FindControl("EnrollAssessor_assessmentDetailsViewTrackingDetailsHyperLink");

                    EnrollAssessor_assessmentDetailsViewTrackingDetailsHyperLink.NavigateUrl =
                        "~/InterviewReportCenter/OfflineInterviewTrackingDetails.aspx?candidatesession="
                        + value.Trim() + "&assessorId=" + assessorId + "&attemptid=" + attemptID.Trim() + "&parentpage=E_ASS";
                    #endregion

                    if (EnrollAssessor_assessmentDetailsTestStatusHiddenField.Value == "Completed")
                    {
                        EnrollAssessor_assessmentDetailsSelectHyperLink.Visible = true;
                        EnrollAssessor_assessmentDetailsCandidateRatingSummaryHyperLink.Visible = true;
                        EnrollAssessor_assessmentDetailsViewTrackingDetailsHyperLink.Visible = true;
                    }
                    else
                    {
                        EnrollAssessor_assessmentDetailsSelectHyperLink.Visible = true;
                        EnrollAssessor_assessmentDetailsCandidateRatingSummaryHyperLink.Visible = false;
                        EnrollAssessor_assessmentDetailsViewTrackingDetailsHyperLink.Visible = false;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on FileUploadContorl
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollAssessor_assessorPhotoUploadedComplete(object sender,
            AsyncFileUploadEventArgs e)
        {

            if (EnrollAssessor_assessorPhotoAsyncFileUpload.PostedFile != null)
            {
                // Validation for file extension
                if (((!Path.GetExtension(e.FileName).ToLower().Contains(".jpg")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".gif")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".png")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".jpeg"))))
                {
                    ShowMessage(EnrollAssessor_topErrorMessageLabel,EnrollAssessor_bottomErrorMessageLabel,
                        "Only gif/png/jpg/jpeg files are allowed)");
                    return;
                }

                // Check if photo size exceeds the maximum limit.
                int maxFileSize = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_MAX_SIZE_IN_BYTES"]);

                if (EnrollAssessor_assessorPhotoAsyncFileUpload.FileBytes.Length > maxFileSize)
                {
                    string scriptMessage = "<script>alert('" + string.Format("File size exceeds the maximum limit of {0} bytes", maxFileSize) + "')</script>";

                    base.ShowMessage(EnrollAssessor_topErrorMessageLabel,EnrollAssessor_bottomErrorMessageLabel,
                       string.Format("File size exceeds the maximum limit of {0} bytes", maxFileSize));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", scriptMessage, false);
                    EnrollAssessor_assessorPhotoAsyncFileUpload.ClearFileFromPersistedStore();
                    EnrollAssessor_assessorPhotoAsyncFileUpload.ClearAllFilesFromPersistedStore(); 
                     
                    return;
                }

                // Retrieve the original photo from the file upload control.
                System.Drawing.Image originalPhoto = System.Drawing.Image.FromStream
                    (new MemoryStream(EnrollAssessor_assessorPhotoAsyncFileUpload.FileBytes));

                int thumbnailWidth = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_WIDTH"]);
                int thumbnailHeight = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_HEIGHT"]);

                // Check the width and height of the original image exceeds the
                // size of the standard size. If exceeds convert the image to
                // thumbnail of the standard size and store.
                if (originalPhoto.Width > thumbnailWidth && originalPhoto.Height > thumbnailHeight)
                {
                    // Resize both width and height.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (thumbnailWidth, thumbnailHeight, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else if (originalPhoto.Width > thumbnailWidth)
                {
                    // Resize only the width.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (thumbnailWidth, originalPhoto.Height, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else if (originalPhoto.Height > thumbnailHeight)
                {
                    // Resize only the height.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (originalPhoto.Width, thumbnailHeight, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else
                {
                    // Keep the original photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = originalPhoto;
                }

                // Set the phto changed status.
                Session["PHOTO_CHANGED"] = true;

                // Set the visibility of the 'clear' photo link.
                EnrollAssessor_photoClearLinkButton.Style.Add("display", "block");
            }
        }

        /// <summary>
        /// Handler method that will be called on search user icon clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollAssessor_newAssessorSearchImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                // Clear messages.
                ClearLabelMessage();

                // Reset assessor detail.
                ResetAssessorDetail(false);

                // Load assessor detail.
                LoadAssessorDetail();

                
                // Load assessment details.
                LoadAssessmentDetails(1, null);
                /* if (ViewState["ASSESSMENT_STATUS"] == null)
                     LoadAssessmentDetails(1, null);
                 else
                     LoadAssessmentDetails(1, ViewState["ASSESSMENT_STATUS"].ToString());*/
                EnrollAssessor_calendarImageButton.Enabled = true;
                if (!string.IsNullOrEmpty(EnrollAssessor_newAssessorUserIDhiddenField.Value))
                    LoadAssessorEvents(Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value));

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the clear link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will clear the selected photo.
        /// </remarks>
        protected void EnrollAssessor_photoClearLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear candidate photo from the session.
                Session["ASSESSOR_THUMBNAIL_PHOTO"] = null;
                Session["ASSESSOR_PHOTO_CHANGED"] = true;

                // Set the visibility of the 'clear' photo link.
                EnrollAssessor_photoClearLinkButton.Style.Add("display", "none");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method to save the assessor photo 
        /// </summary>
        /// <param name="userID">userID</param>
        private void SaveAssessorPhoto(int userID)
        {
            // Save assessor photo.
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session["PHOTO_CHANGED"]) &&
                Convert.ToBoolean(Session["PHOTO_CHANGED"]) == true)
            {
                System.Drawing.Image thumbnail = null;
                if (Session["ASSESSOR_THUMBNAIL_PHOTO"] != null)
                {
                    thumbnail = Session["ASSESSOR_THUMBNAIL_PHOTO"] as
                        System.Drawing.Image;
                }

                // Save the thumbnail image into server path.
                string filePath = Server.MapPath("~/") +
                    ConfigurationManager.AppSettings["ASSESSOR_PHOTOS_FOLDER"] +
                    "\\" + userID + "." +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

                // Check if the file exist. If exist, then remove it.
                if (File.Exists(filePath))
                    File.Delete(filePath);

                thumbnail.Save(filePath, base.GetCandidatePhotoFormat());

                // Reset the status.
                Session["PHOTO_CHANGED_POPUP"] = false;

                // Clear photo
                Session["ASSESSOR_THUMBNAIL_PHOTO"] = null;
            }
        }

        /// <summary>
        /// Method to validate
        /// </summary>
        /// <returns></returns>
        private bool IsValid_Data()
        {
            bool isValidData = true;
            if (EnrollAssessor_newAssessorUserNameTextBox.Text.Trim() == "")
            {
                EnrollAssessor_topSuccessMessageLabel.Visible = false;
                EnrollAssessor_bottomSuccessMessageLabel.Visible = false;

                EnrollAssessor_topErrorMessageLabel.Text = string.Empty;
                EnrollAssessor_bottomErrorMessageLabel.Text = string.Empty;

                EnrollAssessor_topErrorMessageLabel.Visible = true;
                EnrollAssessor_bottomErrorMessageLabel.Visible = true;

                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, "Please select the user");
                isValidData = false;
            }
            return isValidData;
        }
        /// <summary>
        /// This method helps to load the Assessment Details
        /// </summary>
        /// <param name="pageNumber"></param>
        private void LoadAssessmentDetails(int pageNumber,string status)
        {
            try
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(EnrollAssessor_newAssessorUserIDhiddenField.Value))
                    return;

                // Get assessor ID.
                int assessorId = Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value);

                AssessmentSearchCriteria assessmentSearchCriteria = new AssessmentSearchCriteria();
                assessmentSearchCriteria.InterviewName = null;
                assessmentSearchCriteria.InterviewDescription = null;
                assessmentSearchCriteria.AssessmentStatus = status;
                assessmentSearchCriteria.PositionProfileID = null;

                assessmentSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                assessmentSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
                assessmentSearchCriteria.AssessorID = assessorId;
                assessmentSearchCriteria.CurrentPage = pageNumber;

                int totalRecords = 0;

                List<AssessorAssessmentDetail> assessmentDetails = new AssessorBLManager().
                   GetAssessmentDetailsForAssessor(assessmentSearchCriteria,
                   pageNumber, base.GridPageSize, out totalRecords);

                if (assessmentDetails == null || assessmentDetails.Count == 0)
                {
                    // Hide grid view & paging control.
                    EnrollAssessor_assessmentDetailsGridViewDiv.Visible = false;
                    EnrollAssessor_pagingNavigator.Visible = false;
                }
                else
                {
                    EnrollAssessor_assessmentDetailsGridViewDiv.Visible = true;
                    EnrollAssessor_pagingNavigator.Visible = true;
                    EnrollAssessor_pagingNavigator.TotalRecords = totalRecords;
                    EnrollAssessor_pagingNavigator.PageSize = base.GridPageSize;
                    EnrollAssessor_pagingNavigator.MoveToPage(pageNumber);
                }

                EnrollAssessor_assessmentDetailsGridView.DataSource = assessmentDetails;
                EnrollAssessor_assessmentDetailsGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            EnrollAssessor_topSuccessMessageLabel.Text = string.Empty;
            EnrollAssessor_bottomSuccessMessageLabel.Text = string.Empty;
            EnrollAssessor_topErrorMessageLabel.Text = string.Empty;
            EnrollAssessor_bottomErrorMessageLabel.Text = string.Empty;
            
        }


        /// <summary>
        /// This method will load the assessor details based on the assessor selected
        /// </summary>
        private void LoadAssessorDetail()
        {
            try
            {
                String[] arrNonAvailDates = null;
                SelectedDates = null;
                int userID = 0;

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(EnrollAssessor_newAssessorUserIDhiddenField.Value))
                    return;

                // Get user ID.
                userID = Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value);

                // Get the assessor detail.
                AssessorDetail assessorDetail = new AssessorBLManager().
                    GetAssessorByUserId(userID);

                // Check if assessor exist.
                if (assessorDetail == null)
                    return;

                if (!string.IsNullOrEmpty(assessorDetail.NonAvailabilityDates))
                {
                    arrNonAvailDates = assessorDetail.NonAvailabilityDates.ToString().Split(',');
                    DateTime nonAvailDates = DateTime.Now;
                    foreach (string dt in arrNonAvailDates)
                    {
                        nonAvailDates = Convert.ToDateTime(dt);
                        SelectedDates.Add(nonAvailDates);
                    }
                }
                EnrollAssessor_newAssessorFirstNameTextBox.Text = assessorDetail.FirstName;
                EnrollAssessor_newAssessorLastNameTextBox.Text = assessorDetail.LastName;
                EnrollAssessor_newAssessorUserNameTextBox.Text = assessorDetail.UserEmail;
                //EnrollAssessor_newAssessorSkillTextBox.Text = assessorDetail.Skill;
                EnrollAssessor_newAssessorAlternateEmailIDTextBox.Text = assessorDetail.AlternateEmailID;
                EnrollAssessor_newAssessorMobileNumberTextBox.Text = assessorDetail.Mobile;
                EnrollAssessor_newAssessorHomePhoneTextBox.Text = assessorDetail.HomePhone;
                EnrollAssessor_newAssessorAdditionalTextBox.Text = assessorDetail.AdditionalInfo;
                EnrollAssessor_linkedInProfileTextBox.Text = assessorDetail.LinkedInProfile;
                EnrollAssessor_profileLastUpdatedLabel.Text = "Your profile last updated on " + base.GetDateFormat(assessorDetail.ModifiedDate);
                // Set assessor skills.
                dttempTable = new InterviewBLManager().getAssessorSkills(userID);
                ViewState["AssessorSkills"] = dttempTable;

                EnrollAssessor_skillGridView.DataSource = dttempTable;// assessorDetail.Skills;
                EnrollAssessor_skillGridView.DataBind();

                // EnrollAssessor_newAssessorPhotoImage.ImageUrl =
                //"~/Common/CandidateImageHandler.ashx?source=VIEW_ASS_PHOTO&userid=" + userID;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }


        private void LoadAssessorEvents(int assessorId)
        {
            try
            {
                List<AttributeDetail> attributeList =
                    new AssessorBLManager().GetAssessorEventsCount(assessorId);

                if (attributeList == null) return;

                EnrollAssessor_assessorEventsTable.Visible = true;

                if (Convert.ToInt32(attributeList[0].count) > 0)
                    EnrollAssessor_assessmentCompletedCountLinkButton.Enabled = true;

                EnrollAssessor_assessmentCompletedCountLinkButton.Text = attributeList[0].count.ToString();
                EnrollAssessor_assessmentCompletedCountLinkButton.CommandArgument = attributeList[0].AttributeID;

                if (Convert.ToInt32(attributeList[1].count) > 0)
                    EnrollAssessor_assessmentInprogressCountLinkButton.Enabled = true;

                EnrollAssessor_assessmentInprogressCountLinkButton.Text = attributeList[1].count.ToString();
                EnrollAssessor_assessmentInprogressCountLinkButton.CommandArgument = attributeList[1].AttributeID;

                if (Convert.ToInt32(attributeList[2].count) > 0)
                    EnrollAssessor_assessmentInitiatedCountLinkButton.Enabled = true;

                EnrollAssessor_assessmentInitiatedCountLinkButton.Text = attributeList[2].count.ToString();
                EnrollAssessor_assessmentInitiatedCountLinkButton.CommandArgument = attributeList[2].AttributeID;

                if (Convert.ToInt32(attributeList[2].count) > 0)
                    EnrollAssessor_assessmentSchduledCountLinkButton.Enabled = true;

                EnrollAssessor_assessmentSchduledCountLinkButton.Text = attributeList[3].count.ToString();
                EnrollAssessor_assessmentSchduledCountLinkButton.CommandArgument = attributeList[3].AttributeID;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that resets the assessor detail.
        /// </summary>
        /// <remarks>
        /// A <see cref="bool"/> that holds the status whether to reset all 
        /// values including the ones that are assigned from the user popup.
        /// </remarks>
        private void ResetAssessorDetail(bool resetAll)
        {
            if (resetAll)
            {
                EnrollAssessor_newAssessorUserIDhiddenField.Value = null;
                EnrollAssessor_newAssessorUserNameTextBox.Text = string.Empty;
                EnrollAssessor_newAssessorFirstNameTextBox.Text = string.Empty;
                EnrollAssessor_newAssessorLastNameTextBox.Text = string.Empty;
            }
            
            EnrollAssessor_newAssessorAlternateEmailIDTextBox.Text = string.Empty;
            EnrollAssessor_newAssessorSkillTextBox.Text = string.Empty;
            EnrollAssessor_calendarNotAvailable.SelectedDates.Clear();
            EnrollAssessor_newAssessorMobileNumberTextBox.Text = string.Empty;
            EnrollAssessor_newAssessorHomePhoneTextBox.Text = string.Empty;
            EnrollAssessor_newAssessorAdditionalTextBox.Text = string.Empty;
            EnrollAssessor_linkedInProfileTextBox.Text = string.Empty;
            SelectedDates = null;
            EnrollAssessor_assessmentDetailsGridView.DataSource = null;
            EnrollAssessor_assessmentDetailsGridView.DataBind();
            EnrollAssessor_topErrorMessageLabel.Text = string.Empty;
            EnrollAssessor_bottomErrorMessageLabel.Text = string.Empty;
            EnrollAssessor_topSuccessMessageLabel.Text = string.Empty;
            EnrollAssessor_bottomSuccessMessageLabel.Text = string.Empty;
            
            // Clear assessor skills.
            EnrollAssessor_skillGridView.DataSource = null;
            EnrollAssessor_skillGridView.DataBind();

            //Clear assessor events 
            EnrollAssessor_assessmentCompletedCountLinkButton.Text = "0";
            EnrollAssessor_assessmentCompletedCountLinkButton.CommandArgument = string.Empty;

            EnrollAssessor_assessmentInprogressCountLinkButton.Text = "0";
            EnrollAssessor_assessmentInprogressCountLinkButton.CommandArgument = string.Empty;

            EnrollAssessor_assessmentInitiatedCountLinkButton.Text = "0";
            EnrollAssessor_assessmentInitiatedCountLinkButton.CommandArgument = string.Empty; 

            EnrollAssessor_assessmentSchduledCountLinkButton.Text = "0";
            EnrollAssessor_assessmentSchduledCountLinkButton.CommandArgument = string.Empty;
            EnrollAssessor_assessorEventsTable.Visible = false;
        }

        #endregion Private Methodss

        
        /// <summary>
        /// 
        /// </summary> 
        /// <param name="skill">
        /// <see cref="string"/>
        /// </param>
        private void AddManual_Skill(string skill)
        {
           if (skill == "Enter/Select skill name") return;

           DataTable dttemp = new InterviewBLManager().getSubjectCategoryID(skill);

           if (dttemp.Rows.Count != 0)
            {
                EnrollAssessor_CaegoryIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][0]);
                EnrollAssessor_SkillIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][1]);
                EnrollAssessor_newAssessorSkillTextBox.Text = Convert.ToString(dttemp.Rows[0][2]);
            }
        }

        protected void EnrollAssessor_skillGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                HiddenField SubjectValue_HiddenFiled = (HiddenField)EnrollAssessor_skillGridView.
                    Rows[e.RowIndex].FindControl("EnrollAssessor_skillGridView_skillIDHiddenField");

                if (string.IsNullOrEmpty(SubjectValue_HiddenFiled.Value)) return;

                dttempTable = (DataTable)ViewState["AssessorSkills"];

                foreach (DataRow _dr in dttempTable.Rows)
                {
                    if (SubjectValue_HiddenFiled.Value.ToString().Trim() ==
                        Convert.ToString(_dr["SkillID"]))
                    {
                        dttempTable.Rows.Remove(_dr);
                        dttempTable.AcceptChanges();
                        break;
                    }
                }

                ViewState["AssessorSkills"] = dttempTable;

                EnrollAssessor_skillGridView.DataSource = dttempTable;
                EnrollAssessor_skillGridView.DataBind();

                IDbTransaction transaction = new TransactionManager().Transaction;
                int resultVal = new InterviewBLManager().
                           InsertAssessorSkill(Convert.ToInt32(EnrollAssessor_newAssessorUserIDhiddenField.Value), dttempTable, transaction);
                transaction.Commit();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to show the selected client name in a datalist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollAssessor_newAssessorSkillTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            { 
                EnrollAssesssor_NewSkillButton_Click(sender,null); 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }


        protected void EnrollAssessor_assessmentLinkButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Select")
                {
                    ViewState["ASSESSMENT_STATUS"] = e.CommandArgument.ToString();
                    int pageNo = 1;
                    if (ViewState["PAGENUMBER"] != null)
                        pageNo = Convert.ToInt32(ViewState["PAGENUMBER"]);

                    switch (e.CommandArgument.ToString())
                    {
                        case "ASS_COMP":
                            LoadAssessmentDetails(1, e.CommandArgument.ToString());
                            break;
                        case "ASS_INPRG":
                            LoadAssessmentDetails(1, e.CommandArgument.ToString());
                            break;
                        case "ASS_INIT":
                            LoadAssessmentDetails(1, e.CommandArgument.ToString());
                            break;
                        case "ASS_CSCH":
                            LoadAssessmentDetails(1, e.CommandArgument.ToString());
                            break;
                        default:
                            break;
                    }
                }
            }
            catch(Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void ShowPreview(int assesorId)
        {
            // Reset the status.
            EnrollAssessor_photoUploadedHiddenFiled.Value = "N";

            // Check if photo upload control has file.
            if (!EnrollAssessor_selectPhotoFileUpload.HasFile)
            {
                base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel,
                    "No photo was selected");

                return;
            }
             
                // Validation for file extension
            string extension = Path.GetExtension(EnrollAssessor_selectPhotoFileUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(EnrollAssessor_topErrorMessageLabel,
                    EnrollAssessor_bottomErrorMessageLabel,
                        "Only gif/png/jpg/jpeg files are allowed");

                    return;
                }

                // Check if photo size exceeds the maximum limit.
                int maxFileSize = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_MAX_SIZE_IN_BYTES"]);

                if (EnrollAssessor_selectPhotoFileUpload.FileBytes.Length > maxFileSize)
                { 
                    base.ShowMessage(EnrollAssessor_topErrorMessageLabel, EnrollAssessor_bottomErrorMessageLabel,
                       string.Format("File size exceeds the maximum limit of {0} bytes", maxFileSize)); 

                    return;
                }

                // Retrieve the original photo from the file upload control.
                System.Drawing.Image originalPhoto = System.Drawing.Image.FromStream
                    (new MemoryStream(EnrollAssessor_selectPhotoFileUpload.FileBytes));

                int thumbnailWidth = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_WIDTH"]);
                int thumbnailHeight = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_HEIGHT"]);

                // Check the width and height of the original image exceeds the
                // size of the standard size. If exceeds convert the image to
                // thumbnail of the standard size and store.
                if (originalPhoto.Width > thumbnailWidth && originalPhoto.Height > thumbnailHeight)
                {
                    // Resize both width and height.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (thumbnailWidth, thumbnailHeight, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else if (originalPhoto.Width > thumbnailWidth)
                {
                    // Resize only the width.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (thumbnailWidth, originalPhoto.Height, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else if (originalPhoto.Height > thumbnailHeight)
                {
                    // Resize only the height.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (originalPhoto.Width, thumbnailHeight, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else
                {
                    // Keep the original photo in the session.
                    Session["ASSESSOR_THUMBNAIL_PHOTO"] = originalPhoto;
                }

                // Set the phto changed status.
                Session["PHOTO_CHANGED"] = true;
                SaveAssessorPhoto(assesorId);
                // Set the visibility of the 'clear' photo link.
                EnrollAssessor_photoClearLinkButton.Style.Add("display", "none");
             
        }


        protected void EnrollAssessor_calendarNotAvailable_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            EnrollAssessor_newAssessorModalPopupExtender.Show();
        }
}
}