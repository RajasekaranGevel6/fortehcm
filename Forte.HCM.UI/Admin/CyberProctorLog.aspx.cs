﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CyberProctorLog.aspx.cs
// File that represents the user interface layout and functionalities
// for the CyberProctorLog page. This page help to display the log 
// information (such as application, event, system, exception, desktop 
// image, webcam image) captured during the test session. This class 
// inherits the Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using System.Web.UI.HtmlControls;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the CyberProctorLog page. This page help to display the log 
    /// information (such as application, event, system, exception, desktop 
    /// image, webcam image) captured during the test session. This class 
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CyberProctorLog : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "335px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Private Variables                                              

        /// <summary>
        /// A <seealso cref="int"/> that holds the page number for desktop
        /// thumbnail image view.
        /// </summary>
        private int desktopThumbImagepageNo;

        /// <summary>
        /// A <seealso cref="int"/> that holds the page number for webcam
        /// thumbnail image view.
        /// </summary>
        private int webCamThumbImagepageNo;

        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Cyber Proctor Log");

                CyberProctorLog_topSuccessMessageLabel.Text = string.Empty;
                CyberProctorLog_bottomSuccessLabel.Text = string.Empty;
                CyberProctorLog_topErrorMessageLabel.Text = string.Empty;
                CyberProctorLog_bottomErrorLabel.Text = string.Empty;

                CheckAndSetExpandOrRestore();

                Page.Form.DefaultButton = CyberProctorLog_searchButton.UniqueID;

                if (!IsPostBack)
                {
                    desktopThumbImagepageNo = 1;
                    webCamThumbImagepageNo = 1;
                    Session["TrackingDetailsDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                    Session["TrackingDetailsWebCamImagePageNo"] = webCamThumbImagepageNo;
                }

                //Assign the on click attributes for the expand or restore button 
                CyberProctorLog_UparrowSpan.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    CyberProctorLog_DetailsDIV.ClientID + "','" +
                    CyberProctorLog_SummaryDIV.ClientID + "','" +
                    CyberProctorLog_UparrowSpan.ClientID + "','" +
                    CyberProctorLog_DownarrowSpan.ClientID + "','" +
                    CyberProctorLog_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");

                CyberProctorLog_DownarrowSpan.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    CyberProctorLog_DetailsDIV.ClientID + "','" +
                    CyberProctorLog_SummaryDIV.ClientID + "','" +
                    CyberProctorLog_UparrowSpan.ClientID + "','" +
                    CyberProctorLog_DownarrowSpan.ClientID + "','" +
                    CyberProctorLog_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");

                CyberProctorLog_bottomPagingNavigator.PageNumberClick +=
                   new PageNavigator.PageNumberClickEventHandler
                       (CyberProctorLog_bottomPagingNavigator_PageNumberClick);

                CyberProctorLog_stateExpandHiddenField.Value = "0";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                    CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CyberProctorLog_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "DATE";

                CyberProctorLog_bottomPagingNavigator.Reset();

                LoadValues(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                    CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void CyberProctorLog_bottomPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["pagenumber"] = Convert.ToInt32(e.PageNumber);
                CyberProctorLog_resultsGridView.EditIndex = -1;
                CyberProctorLog_PageNumberHidden.Value = e.PageNumber.ToString();

                LoadValues(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                    CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset is clicked       
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CyberProctorLog_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topSuccessMessageLabel,
                   CyberProctorLog_bottomSuccessLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CyberProctorLog_resultsGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                CyberProctorLog_stateExpandHiddenField.Value = "0";
                CyberProctorLog_resultsExpandLinkButton.Text = "Expand All";
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                CyberProctorLog_bottomPagingNavigator.Reset();
                LoadValues(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                    CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void CyberProctorLog_resultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (CyberProctorLog_resultsGridView, (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                    CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CyberProctorLog_resultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int deskTopPageIndex = 0;
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton CyberProctorLog_showDesktopImageButton = (ImageButton)e.Row.FindControl(
                    "CyberProctorLog_showDesktopImageButton");
                ImageButton CyberProctorLog_showWebcamImageButton = (ImageButton)e.Row.FindControl(
                    "CyberProctorLog_showWebcamImageButton");
                ImageButton CyberProctorLog_viewLogFileImageButton = (ImageButton)e.Row.FindControl(
                    "CyberProctorLog_viewLogFileImageButton");
                HiddenField CyberProctorLog_logIDHiddenField = (HiddenField)e.Row.FindControl(
                    "CyberProctorLog_logIDHiddenField");

                LinkButton CyberProctorLog_systemNameLinkButton =
                    (LinkButton)e.Row.FindControl("CyberProctorLog_systemNameLinkButton");
                HtmlContainerControl CyberProctorLog_MessagedetailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("CyberProctorLog_MessagedetailsDiv");
                HtmlAnchor CyberProctorLog_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("CyberProctorLog_focusDownLink");

                CyberProctorLog_systemNameLinkButton.Attributes.Add("onclick", "return MessageDetails('" +
                CyberProctorLog_MessagedetailsDiv.ClientID + "','" + CyberProctorLog_focusDownLink.ClientID + "')");

                GridView CyberProctorLog_detailsGridView = (GridView)e.Row.FindControl(
                    "CyberProctorLog_detailsGridView");

                List<SystemLogDetail> systemLogDetails = new TrackingBLManager().
                    GetCyberProctorLogMesageDetails(CyberProctorLog_logIDHiddenField.Value);

                CyberProctorLog_detailsGridView.DataSource = systemLogDetails;
                CyberProctorLog_detailsGridView.DataBind();

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                desktopThumbImagepageNo = (int)Session["TrackingDetailsDesktopThumbImagePageNo"];
                int.TryParse(e.Row.DataItem.ToString(), out deskTopPageIndex);

                CyberProctorLog_showDesktopImageButton.Attributes.Add("onclick", "javascript:return OpenScreenShot('CP','" +
                    (deskTopPageIndex + ((desktopThumbImagepageNo - 1) * base.GridPageSize)).ToString()
                    + "','SCREEN_IMG','" + CyberProctorLog_logIDHiddenField.Value + "','LOG');");

                CyberProctorLog_showWebcamImageButton.Attributes.Add("onclick", "javascript:return OpenScreenShot('CP','" +
                    (deskTopPageIndex + ((desktopThumbImagepageNo - 1) * base.GridPageSize)).ToString()
                    + "','USER_IMG','" + CyberProctorLog_logIDHiddenField.Value + "','LOG');");

                CyberProctorLog_viewLogFileImageButton.Attributes.Add("onclick", "javascript:return OpenTrackingLogFile('CP','" +
                    CyberProctorLog_logIDHiddenField.Value + "','" + CyberProctorLog_systemNameLinkButton.Text + "');");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void CyberProctorLog_resultsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteMessage")
                    return;

                CyberProctorLog_LogIDHiddenFied.Value = e.CommandArgument.ToString();

                string result = string.Empty;

                CyberProctorLog_deleteMessageConfirmMsgControl.Message = string.Format
                    (Resources.HCMResource.CyberProctorLogGridView_MessageDeleteConfirmation);

                CyberProctorLog_deleteMessageConfirmMsgControl.Type = MessageBoxType.YesNo;
                CyberProctorLog_deleteMessageConfirmMsgControl.Title = "Warning";
                CyberProctorLog_deleteFormModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is 
        /// clicked in the delete message confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CyberProctorLog_deleteMessageConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                int logID = int.Parse(CyberProctorLog_LogIDHiddenFied.Value);

                new TrackingBLManager().DeleteLogMessage(logID);

                //Reset the page navigator
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "DATE";

                //Get the update business details
                LoadValues(1);

                base.ShowMessage(CyberProctorLog_topSuccessMessageLabel,
                  CyberProctorLog_bottomSuccessLabel, "Log entry deleted successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the delete message confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CyberProctorLog_deleteMessageConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                CyberProctorLog_LogIDHiddenFied.Value = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                CyberProctorLog_bottomErrorLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        private void LoadValues(int pageNumber)
        {
            string systemName = CyberProctorLog_systemNameTextBox.Text.Trim();

            string macAddress = CyberProctorLog_macAddressTextBox.Text.Trim();

            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            int totalRecords = 0;

            List<SystemLogDetail> cyberProctorLogList = new TrackingBLManager().GetCyberProctorCommonLog(
                systemName, macAddress, CyberProctorLog_datePopupTextBox.Text.ToString(), sortOrder, sortExpression, pageNumber,
                base.GridPageSize, out totalRecords);

            if (cyberProctorLogList.Count != 0)
            {
                CyberProctorLog_resultsExpandLinkButton.Visible = true;

                CyberProctorLog_resultsGridView.DataSource = cyberProctorLogList;
                CyberProctorLog_resultsGridView.DataBind();

                CyberProctorLog_bottomPagingNavigator.Visible = true;

                CyberProctorLog_bottomPagingNavigator.TotalRecords = totalRecords;

                CyberProctorLog_bottomPagingNavigator.PageSize = base.GridPageSize;
            }
            else
            {
                CyberProctorLog_resultsGridView.DataSource = cyberProctorLogList;
                CyberProctorLog_resultsGridView.DataBind();
                CyberProctorLog_bottomPagingNavigator.Visible = true;

                CyberProctorLog_bottomPagingNavigator.TotalRecords = totalRecords;

                CyberProctorLog_bottomPagingNavigator.PageSize = base.GridPageSize;

                CyberProctorLog_resultsExpandLinkButton.Visible = false;
                base.ShowMessage(CyberProctorLog_topErrorMessageLabel,
                    CyberProctorLog_bottomErrorLabel, "No data found to display");
            }
        }

        /// <summary>
        /// Method that sets the expand or restored state.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            if (!Utility.IsNullOrEmpty(CyberProctorLog_isMaximizedHiddenField.Value) &&
                CyberProctorLog_isMaximizedHiddenField.Value == "Y")
            {
                CyberProctorLog_SummaryDIV.Style["display"] = "none";
                CyberProctorLog_UparrowSpan.Style["display"] = "block";
                CyberProctorLog_DownarrowSpan.Style["display"] = "none";
                CyberProctorLog_DetailsDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CyberProctorLog_SummaryDIV.Style["display"] = "block";
                CyberProctorLog_UparrowSpan.Style["display"] = "none";
                CyberProctorLog_DownarrowSpan.Style["display"] = "block";
                CyberProctorLog_DetailsDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        #endregion Private Methods

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the log file availability status. This
        /// helps to show or hide the view log file icon.
        /// </summary>
        /// <param name="logFileID">
        /// A <see cref="string"/> that holds the log file ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of log file 
        /// availability status. True represents available and false not 
        /// available.
        /// </returns>
        /// <remarks>
        /// The applicable return values are:
        /// 1. True - Available.
        /// 2. False - Not available.
        /// </remarks>
        protected bool IsLogFileAvailable(string logFileID)
        {
            return (logFileID.Trim() == "0" ? false : true);
        }

        /// <summary>
        /// Method that retrieves the image availability status. This
        /// helps to show or hide show/hide desktop and webcam image icon.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds status as string.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of given status. 
        /// True represents available and false not available.
        /// </returns>
        /// <remarks>
        /// The applicable return values are:
        /// 1. True - Available.
        /// 2. False - Not available.
        /// </remarks>
        protected bool IsImageAvailable(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        #endregion Protected Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }

        #endregion Protected Overridden Methods
    }
}