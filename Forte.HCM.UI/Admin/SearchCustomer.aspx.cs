﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchCustomer.aspx.cs
// File that represents the user interface layout and functionalities for
// the SearchCustomer page. This page helps in searching for users by
// providing search criteria for user name, first name, middle name, last 
// name, customer type, activation status, etc. This class inherits the 
// Forte.HCM.UI.Common.PageBase class

#endregion Header

#region Directives                                                             

using System;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

using Resources;
using System.Reflection;
using System.Data;
using OfficeOpenXml;
using System.Web;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the SearchCustomer page. This page helps in searching for users by
    /// providing search criteria for user name, first name, middle name, last 
    /// name, customer type, activation status, etc. This class inherits the 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchCustomer : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user object details.
        /// </param>
        private delegate void AsyncTaskDelegate(UserDetail userDetail);

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="EntityType">
        /// A <see cref="entityType"/> that holds the entity type.
        /// </param>
        /// <param name="userid">
        /// A <see cref="string"/> that holds the user id object details.
        /// </param>
        private delegate void UserActiveStatusDelegate(EntityType entityType, string userid);

        #endregion Declaration

        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear the messages.
                ClearMessages();

                Master.SetPageCaption("Search Customer");

                // Set default button and focus.
                Page.Form.DefaultButton = SearchUserInformation_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchUserInformation_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchUserInformation_pageNavigator_PageNumberClick);

                if (!Utility.IsNullOrEmpty(SearchUserInformation_isMaximizedHiddenField.Value) &&
                    SearchUserInformation_isMaximizedHiddenField.Value == "Y")
                {
                    SearchUserInformation_searchCriteriasDiv.Style["display"] = "none";
                    SearchUserInformation_searchResultsUpSpan.Style["display"] = "block";
                    SearchUserInformation_searchResultsDownSpan.Style["display"] = "none";
                    SearchUserInformation_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchUserInformation_searchCriteriasDiv.Style["display"] = "block";
                    SearchUserInformation_searchResultsUpSpan.Style["display"] = "none";
                    SearchUserInformation_searchResultsDownSpan.Style["display"] = "block";
                    SearchUserInformation_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchUserInformation_browserHiddenField.Value))
                {
                    ValidateEnterKey(SearchUserInformation_browserHiddenField.Value.Trim());
                    SearchUserInformation_browserHiddenField.Value = string.Empty;
                }

                if (!IsPostBack)
                {
                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                    }

                    // Set default focus.
                    // Page.Form.DefaultFocus = SearchCustomer_userNameTextBox.UniqueID;
                    // SearchCustomer_userNameTextBox.Focus();
                    SearchCustomer_userNameTextBox.Text = "1";
                    SearchCustomer_userNameTextBox.Text = "";

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "USERNAME";

                    SearchUserInformation_testDiv.Visible = false;

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_CUSTOMER] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_CUSTOMER]
                            as UserDetail);
                }
                else
                {
                    SearchUserInformation_testDiv.Visible = true;
                }

                SearchUserInformation_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchUserInformation_testDiv.ClientID + "','" +
                    SearchUserInformation_searchCriteriasDiv.ClientID + "','" +
                    SearchUserInformation_searchResultsUpSpan.ClientID + "','" +
                    SearchUserInformation_searchResultsDownSpan.ClientID + "','" +
                    SearchUserInformation_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
                SearchUserInformation_searchButton_Click(null, null);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchUserInformation_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset the paging control.
                SearchUserInformation_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchUserInformation_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchCustomer_userNameTextBox.Text = string.Empty;
                SearchUserInformation_firstNameTextBox.Text = string.Empty;
                SearchUserInformation_lastNameTextBox.Text = string.Empty;

                SearchUserInformation_testGridView.DataSource = null;
                SearchUserInformation_testGridView.DataBind();

                SearchUserInformation_pageNavigator.PageSize = base.GridPageSize;
                SearchUserInformation_pageNavigator.TotalRecords = 0;
                SearchUserInformation_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset to the restored state.
                SearchUserInformation_searchCriteriasDiv.Style["display"] = "block";
                SearchUserInformation_searchResultsUpSpan.Style["display"] = "none";
                SearchUserInformation_searchResultsDownSpan.Style["display"] = "block";
                SearchUserInformation_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchUserInformation_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchCustomer_topSuccessMessageLabel.Text = string.Empty;
                SearchCustomer_topErrorMessageLabel.Text = string.Empty;
                SearchCustomer_bottomErrorMessageLabel.Text = string.Empty;
                SearchCustomer_bottomSuccessMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchCustomer_userNameTextBox.UniqueID;
                SearchCustomer_userNameTextBox.Focus();
                SearchCustomer_userNameTextBox.Attributes.Add("autocomplete", "off");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchUserInformation_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchUserInformation_testGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchUserInformation_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchUserInformation_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                   
                    HiddenField SearchUserInformation_subscriptionTypeHiddenField=
                        (HiddenField)e.Row.FindControl("SearchUserInformation_subscriptionTypeHiddenField");
                    ImageButton SearchUserInformation_editUserImageButton =
                        (ImageButton)e.Row.FindControl("SearchUserInformation_editUserImageButton");
                    HiddenField SearchUserInformation_userTypeHiddenField =
                        (HiddenField)e.Row.FindControl("SearchUserInformation_userTypeHiddenField");
                    

                    if((SearchUserInformation_subscriptionTypeHiddenField.Value=="Standard")||
                    (SearchUserInformation_subscriptionTypeHiddenField.Value=="Free")||
                        (SearchUserInformation_userTypeHiddenField.Value == "Internal"))
                    {
                        SearchUserInformation_editUserImageButton.Visible=true;
                    }
                    else
                        SearchUserInformation_editUserImageButton.Visible = false;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchUserInformation_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchUserInformation_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void SearchUserInformation_testGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditUser")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string userID = (SearchUserInformation_testGridView.Rows
                        [index].FindControl("SearchUserInformation_userIDHiddenfield") as HiddenField).Value;
                    string usertype = (SearchUserInformation_testGridView.Rows
                        [index].FindControl("SearchUserInformation_subscriptionTypeHiddenField") as HiddenField).Value;

                    Response.Redirect("~/Admin/EnrollCustomer.aspx?m=2&s=0" +
                        "&userID=" + userID + "&usertype=" + usertype +
                        "&parentpage=SRCH_CUST", false);
                }
                else if (e.CommandName == "AssignUserRoles")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string userID = (SearchUserInformation_testGridView.Rows
                        [index].FindControl("SearchUserInformation_userIDHiddenfield") as HiddenField).Value;

                    Response.Redirect("~/Authentication/UserRoleMatrix.aspx?m=3&s=0" +
                        "&userID=" + userID +
                        "&parentpage=SRCH_CUST", false);
                }
                else if (e.CommandName == "AssignUserRights")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string userID = (SearchUserInformation_testGridView.Rows
                        [index].FindControl("SearchUserInformation_userIDHiddenfield") as HiddenField).Value;

                    Response.Redirect("~/Authentication/UserRightsMatrix.aspx?m=3&s=3" +
                        "&userID=" + userID +
                        "&parentpage=SRCH_CUST", false);
                }
                else if (e.CommandName == "EmailActivationCode")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    
                    string userID = (SearchUserInformation_testGridView.Rows
                        [index].FindControl("SearchUserInformation_userIDHiddenfield") as HiddenField).Value;

                    try
                    {
                        // Compose and send activation email.
                        SendConfirmationCodeEmail(Convert.ToInt32(userID));

                        // Clear the messages.
                        ClearMessages();

                        base.ShowMessage(SearchCustomer_topSuccessMessageLabel,
                            SearchCustomer_bottomSuccessMessageLabel, "Activation code mailed successfully");
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                        base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                            SearchCustomer_bottomErrorMessageLabel, "Unable to mail the activation code");
                    }
                }
                else if (e.CommandName == "DeactivateUser")
                {
                    SearchCustomer_activateUserConfirmMsgControl.Message =Resources.HCMResource.CorporateSubscriptionManagement_AreYouSureToDeactivateThisUser;
                    SearchCustomer_activateUserConfirmMsgControl.Type = MessageBoxType.YesNo;
                    SearchCustomer_activateUserConfirmMsgControl.Title = "Warning";
                    SearchCustomer_activateUserHiddenField.Value = e.CommandArgument.ToString();
                    SearchCustomer_activateStatusHiddenField.Value = "0";
                    SearchCustomer_activateUserModalPopupExtender.Show();
                }
                else if (e.CommandName == "ActivateUser")
                {
                    UserRegistrationInfo userRegistrationInfo = new UserRegistrationInfo();
                    userRegistrationInfo.UserID = Convert.ToInt32(e.CommandArgument);
                    userRegistrationInfo.UserEmail = ((HiddenField)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                            "SearchUserInformation_userNameHiddenfield")).Value;
                    userRegistrationInfo.FirstName = ((HiddenField)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                            "SearchUserInformation_userFirstNameHiddenfield")).Value;
                    userRegistrationInfo.LastName = ((HiddenField)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                            "SearchUserInformation_userLastNameHiddenfield")).Value;

                    if (Convert.ToBoolean(((HiddenField)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                            "SearchUserInformation_activateStatusHiddenFieldButton")).Value))
                    {
                        userRegistrationInfo.IsActiveStatus = 1;
                    }
                    else
                    {
                        userRegistrationInfo.IsActiveStatus = 0;
                    }
                    
                    userRegistrationInfo.IsActive = 1;
                    userRegistrationInfo.IsCustomerAdmin = new UserRegistrationBLManager().GetCustomerAdmin(userRegistrationInfo.UserID);
                    userRegistrationInfo.CorporateRoles = new UserRegistrationBLManager().GetcorporateRoles(userRegistrationInfo.UserID);

                    new UserRegistrationBLManager().UpdateCorporateUserAdmin(userRegistrationInfo);
                    
                    LoadValues();

                    base.ShowMessage(SearchCustomer_topSuccessMessageLabel,
                        "User activated successfully");

                    try
                    {
                        // Send mail to user on activation and deactivation.
                        UserActiveStatusDelegate taskActiveDelegate = new UserActiveStatusDelegate(SendUserActiveStatusMail);
                        IAsyncResult result = taskActiveDelegate.BeginInvoke(EntityType.UserActivated,
                            userRegistrationInfo.UserID.ToString(),
                            new AsyncCallback(SendUserActiveStatusMailCallBack), taskActiveDelegate);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                        base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                        SearchCustomer_topErrorMessageLabel, exp.Message);
                    }
                }
                else if (e.CommandName == "ResendPassword")
                {
                    string userEmail = ((HiddenField)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                            "SearchUserInformation_userNameHiddenfield")).Value;

                    // Get user detail along with password.
                    UserDetail userDetail = new UserDetail();
                    userDetail = new UserRegistrationBLManager().GetPassword(userEmail.Trim());

                    if (userDetail == null)
                    {
                        base.ShowMessage(SearchCustomer_topErrorMessageLabel,"No such user name exist !");
                        return;
                    }

                    // Decrypt the password.
                    userDetail.Password = new EncryptAndDecrypt().DecryptString(userDetail.Password);

                    try
                    {
                        // Send the password through mail.
                        AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(ResendPasswordToUser);
                        IAsyncResult result = taskDelegate.BeginInvoke(userDetail,
                            new AsyncCallback(ResendPasswordToUserCallBack), taskDelegate);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }

                    // Show a message to the user.
                    base.ShowMessage(SearchCustomer_topSuccessMessageLabel, 
                        string.Format("Password has been sent to {0}",userEmail.Trim()));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                    SearchCustomer_bottomErrorMessageLabel, exp.Message);
            }
        }

        

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchCustomer_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear the search criteria in session.
                Session[Constants.SearchCriteriaSessionKey.SEARCH_CUSTOMER] = null;

                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                   SearchCustomer_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the export to excel button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will export and download user information in excel format.
        /// </remarks>
        protected void SearchUserInformation_exportExcelLinkButton_Click(object sender, EventArgs e)
        {
           
             
            DataTable rawTable = new CommonBLManager().GetUsers
                (SearchCustomer_userNameTextBox.Text.Trim(),
                SearchUserInformation_firstNameTextBox.Text.Trim(),
                SearchUserInformation_middleNameTextBox.Text.Trim(),
                SearchUserInformation_lastNameTextBox.Text.Trim(),
                SearchCustomer_userTypeDropDownList.SelectedValue.ToUpper().ToString().Trim(),
                SearchCustomer_activatedDropDownList.SelectedValue.ToUpper().Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                1,
                int.MaxValue);
            

                if (rawTable == null && rawTable.Rows.Count <= 0)
                {
                    return;
                }

                // Remove total rows.
                //DataView dv = new DataView(rawTable);
                //dv.RowFilter = "TOTAL IS NULL";
                //DataTable table = dv.ToTable();

                // Remove the unncessary columns from the table.
                rawTable.Columns.Remove("TOTAL");
                rawTable.Columns.Remove("USER_ID");
                rawTable.Columns.Remove("MIDDLE_NAME");
                rawTable.Columns.Remove("IS_ACTIVE");
                rawTable.Columns.Remove("ACTIVATED");
                rawTable.Columns.Remove("LEGAL_ACCEPTED");

                // Change the column names.
                rawTable.Columns["ROWNUMBER"].ColumnName = "S.No";
                rawTable.Columns["USRUSERTYPENAME"].ColumnName = "User Type";
                rawTable.Columns["USER_NAME"].ColumnName = "User Name";
                rawTable.Columns["FIRST_NAME"].ColumnName = "First Name";
                rawTable.Columns["LAST_NAME"].ColumnName = "Last Name";
                rawTable.Columns["SUBSCRIPTION_ROLE"].ColumnName = "Subscription Role";
                rawTable.Columns["EMAIL"].ColumnName = "Email";
                rawTable.Columns["PHONE"].ColumnName = "Contact Number";
                rawTable.Columns["SUBSCRIPTION_TYPE"].ColumnName = "Subscription Type";                            

                //table.Columns["CANDIDATE_FULL_NAME"].ColumnName = "Candidate Name";

                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(Constants.ExcelExportSheetName.USER_DETAILS);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(rawTable, true);

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                    foreach (DataColumn column in rawTable.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + rawTable.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + rawTable.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    /*for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }*/
                                    col.Style.Numberformat.Format = "0.00";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }
                    Response.ClearHeaders();
                    Response.ClearContent();
                    // Construct file name.
                    string fileName = string.Format("{0}_{1}.xlsx",
                        Constants.ExcelExportFileName.CANDIDATE_DETAILS, DateTime.Now.ToString());
                   
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(pck.GetAsByteArray());
                    //Response.End();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //Response.End();
                    Response.Flush();
                
            }
        }

        #endregion Events Handlers

        #region Proctected Methods                                             

        /// <summary>
        /// Method that retrieves the show results to candidate status. This
        /// helps to show or hide the show results link icon in the completed 
        /// test grid section.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the show results to candidate status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of show results
        /// to candidate status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for show results to candidates are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsShowEditUser(string userType)
        {
            return (userType.Trim().ToUpper() == "INTERNAL" ? true : false);
        }

        /// <summary>
        /// Method that retrieves the user activated status. This helps to show
        /// or hide the 'Email Activation Code' icon in the grid
        /// </summary>
        /// <param name="activated">
        /// A <see cref="string"/> that holds the activated flag.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of user activated
        /// status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for user activated status are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsUserNotActivated(string activated)
        {
            return (activated.Trim().ToUpper() == "NO" ? true : false);
        }

        #endregion Proctected Methods

        #region Private Methods                                                

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        /// <summary>
        /// Method that send mail to user indicating that password has been resend
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user deail object
        /// detail.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void ResendPasswordToUser(UserDetail userDetail)
        {
            try
            {
                new EmailHandler().SendMail(EntityType.ResendPassword, userDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that send mail to user indicating that password has been resend
        /// </summary>
        /// <param name="EntityType">
        /// A <see cref="entityType"/> that holds the entity type object
        /// </param>
        /// <param name="userid">
        /// A <see cref="string"/> that holds the user id
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendUserActiveStatusMail(EntityType entityType, string userid)
        {
            try
            {
                new EmailHandler().SendMail(entityType, userid);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that fills the search criteria into the fields.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        private void FillSearchCriteria(UserDetail userDetail)
        {
            SearchCustomer_userNameTextBox.Text = userDetail.UserName;
            SearchUserInformation_firstNameTextBox.Text = userDetail.FirstName;
            SearchUserInformation_lastNameTextBox.Text = userDetail.LastName;

            string userTypeName = string.Empty;

            if (userDetail.UserTypeName != null)
                userTypeName = userDetail.UserTypeName.ToUpper();

            if (userTypeName.Length > 0)
                SearchCustomer_userTypeDropDownList.SelectedValue = userTypeName;

            ViewState["SORT_ORDER"] = userDetail.SortDirection;

            ViewState["SORT_FIELD"] = userDetail.SortExpression;

            Search(userDetail.PageNumber);

            SearchUserInformation_pageNavigator.MoveToPage
                (userDetail.PageNumber);
        }

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            UserDetail userDetail = new UserDetail();
            userDetail.UserName = SearchCustomer_userNameTextBox.Text.Trim();
            userDetail.FirstName = SearchUserInformation_firstNameTextBox.Text.Trim();
            userDetail.MiddleName = SearchUserInformation_middleNameTextBox.Text.Trim();
            userDetail.LastName = SearchUserInformation_lastNameTextBox.Text.Trim();
            userDetail.UserTypeName = SearchCustomer_userTypeDropDownList.SelectedValue.ToUpper().Trim();
            userDetail.Activated = SearchCustomer_activatedDropDownList.SelectedValue.ToUpper().Trim();
            userDetail.PageNumber = pageNumber;
            userDetail.SortDirection = (SortType)ViewState["SORT_ORDER"];
            userDetail.SortExpression = ViewState["SORT_FIELD"].ToString();

            // Keep the search criteria in session.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CUSTOMER] = userDetail;

            List<UserDetail> users = new CommonBLManager().GetCustomers
                (SearchCustomer_userNameTextBox.Text.Trim(),
                SearchUserInformation_firstNameTextBox.Text.Trim(),
                SearchUserInformation_middleNameTextBox.Text.Trim(),
                SearchUserInformation_lastNameTextBox.Text.Trim(),
                SearchCustomer_userTypeDropDownList.SelectedValue.ToUpper().ToString().Trim(),
                SearchCustomer_activatedDropDownList.SelectedValue.ToUpper().Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize, 
                out totalRecords);

            if (users == null || users.Count == 0)
            {
                SearchUserInformation_testDiv.Visible = false;
                SearchUserInformation_exportExcelLinkButton.Visible = false;
                base.ShowMessage(SearchCustomer_topErrorMessageLabel, 
                    SearchCustomer_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchUserInformation_exportExcelLinkButton.Visible = true;
                SearchUserInformation_testDiv.Visible = true;
            }

            SearchUserInformation_testGridView.DataSource = users;
            SearchUserInformation_testGridView.DataBind();
            SearchUserInformation_pageNavigator.PageSize = base.GridPageSize;
            SearchUserInformation_pageNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "dummy":
                    foreach (GridViewRow row in SearchUserInformation_testGridView.Rows)
                    {
                        LinkButton SearchUserInformation_selectLinkButton = (LinkButton)
                            row.FindControl("SearchUserInformation_selectLinkButton");

                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "OnSelectClickMethod",
                            "javascript:OnSelectClick('" + SearchUserInformation_selectLinkButton.ClientID + "');", true);
                    }
                    break;
            }
        }

        /// <summary>
        /// Method that compose and send the activation email.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="String"/> that holds the user email
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        private void SendConfirmationCodeEmail(int userID)
        {
            // Get user info detail.
            UserRegistrationInfo userInfo = new UserRegistrationBLManager().
                GetUserRegistrationInfo(userID);

            if (userInfo == null)
                throw new Exception("User does not exist");

            string strCC = string.Empty;
            
            strCC = ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"];
            // Assign User's Admin Corporate Email Id
            if (!string.IsNullOrEmpty(userInfo.UserCorporateAdminEmail))
                strCC = string.Concat(userInfo.UserCorporateAdminEmail, ",", 
                    ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"]);
            
            // Send mail.
            new EmailHandler().SendMail
                (userInfo.UserEmail, strCC,
                HCMResource.UserRegistration_MailActivationCodeSubject,
                GetMailBody(userInfo));
        }

        /// <summary>
        /// Method that constructs the body content of the email.
        /// </summary>
        /// <param name="userInfo">
        /// A <see cref="UserRegistrationInfo"/> that holds the user details.
        /// </param>
        /// A <see cref="string"/> that contains the mail body.
        /// </returns>
        private string GetMailBody(UserRegistrationInfo userInfo)
        {
            string encryptedEmail = new EncryptAndDecrypt().EncryptString(userInfo.UserEmail);

            StringBuilder sbBody = new StringBuilder();

            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(userInfo.FirstName);
                sbBody.Append(" ");
                sbBody.Append(userInfo.LastName);
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This is to acknowledge that your request to create an account was processed successfully.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("To Activate your account, please copy and paste the ");
                sbBody.Append("below URL in the browser or click on the link");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(ConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    ConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    ConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    encryptedEmail, userInfo.ActivationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }


        /// <summary>
        /// Method that clears the messages.
        /// </summary>
        private void ClearMessages()
        {
            SearchCustomer_topSuccessMessageLabel.Text = string.Empty;
            SearchCustomer_topErrorMessageLabel.Text = string.Empty;
            SearchCustomer_bottomErrorMessageLabel.Text = string.Empty;
            SearchCustomer_bottomSuccessMessageLabel.Text = string.Empty;
        }

        protected void SearchCustomer_activateUserConfirmMsgControl_OKClick(object sender, EventArgs e)
        {
            try
            {
                if (SearchCustomer_activateUserHiddenField.Value != null
                    && SearchCustomer_activateUserHiddenField.Value.Trim() != string.Empty)
                {
                    int userID = int.Parse(SearchCustomer_activateUserHiddenField.Value);
                    Int16 activateID = Int16.Parse(SearchCustomer_activateStatusHiddenField.Value);

                    // Activate/deactivate the user.
                    new AdminBLManager().ActivateUsers(userID, activateID, base.userID);

                    base.ShowMessage(SearchCustomer_topSuccessMessageLabel,
                        activateID == 1 ? "User activated successfully" : "User deactivated successfully");

                    LoadValues();

                    try
                    {
                        // Send mail to user on activation and deactivation.
                        UserActiveStatusDelegate taskActiveDelegate = new UserActiveStatusDelegate(SendUserActiveStatusMail);
                        IAsyncResult result = taskActiveDelegate.BeginInvoke(activateID == 1 ? EntityType.UserActivated : EntityType.UserDeactivated,
                            userID.ToString(),
                            new AsyncCallback(SendUserActiveStatusMailCallBack), taskActiveDelegate);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                        base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                             SearchCustomer_topErrorMessageLabel, exp.Message);
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(SearchCustomer_topErrorMessageLabel,
                           SearchCustomer_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        protected bool GetActiveIcon(int value)
        {
            return value == 0 ? true : false;
        }
        protected bool GetInActiveIcon(int value)
        {
            return value == 1 ? true : false;
        }

        #endregion Private Methods

        #region Asynchronous Method Handlers                                 

        /// <summary>
        /// Handler method that acts as the callback method for send mail.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void ResendPasswordToUserCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Handler method that acts as the callback method for send mail.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendUserActiveStatusMailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                UserActiveStatusDelegate caller = (UserActiveStatusDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Reset default sort field and order keys.
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD"] = "USERNAME";

            // Reset the paging control.
            SearchUserInformation_pageNavigator.Reset();

            // By default search button click retrieves data for
            // page number 1.
            Search(1);
        }

        #endregion Protected Overridden Methods
    }
}