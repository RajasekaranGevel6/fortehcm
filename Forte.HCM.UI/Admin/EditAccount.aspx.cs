﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditAccount.cs
// File that represents the user interface to edit the account
// details of the coroprate user admin. He/she can edit their 
// account details or upgrade or change his/her password from this page.

#endregion Header                                                              

#region Namespace                                                              

using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Resources;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.BL;
using Forte.HCM.Support;

#endregion Namespace

namespace Forte.HCM.UI.Admin
{
    public partial class EditAccount : PageBase
    {

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(HCMResource.EditAccount_PageTitle);
                if (IsPostBack)
                    return;
                LoadValues();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void EditAccount_ResetLinkButtonClick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void EditAccount_UpgradeAccountButtonClick(object sender, EventArgs e)
        {
            Response.Redirect("UpgradeAccount.aspx?m=0&s=2&parentpage=" + Constants.ParentPage.EDIT_ACCOUNT, false);
        }

        protected void EditAccount_SaveButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                UpdateCorporateUserDetails();
                ShowMessage(Forte_EditAccount_topSuccessMessageLabel,
                    Forte_EditAccount_bottomSuccessMessageLabel, "Account details updated");
                //ParentPageRedirect(null, null);
                //Response.Redirect("ViewAccount.aspx?m=0&s=0", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        private void UpdateCorporateUserDetails()
        {
            UserRegistrationInfo userInfo = new UserRegistrationInfo();
            userInfo.Title = Forte_EditAccount_titleTextBox.Text;
            userInfo.Company = Forte_EditAccount_companyTextBox.Text;
            userInfo.Phone = Forte_EditAccount_phoneTextBox.Text;
            userInfo.FirstName = Forte_EditAccount_firstNameTextBox.Text;
            userInfo.LastName = Forte_EditAccount_lastNameTextBox.Text;
            userInfo.SubscriptionRole = Forte_EditAccount_subscriptionRoleHidden.Value;
            userInfo.UserID = base.userID;
            userInfo.ModifiedBy = base.userID;
            new UserRegistrationBLManager().UpdateCorporateUserSubscriptionDetails(userInfo);
        }

        private void GetSubscriptionAndFeatureDetails(out UserRegistrationInfo userInfo, out SubscriptionTypes subscriptionTypes, out List<SubscriptionFeatures> subscriptionFeatures)
        {
            new Forte.HCM.BL.UserRegistrationBLManager().GetSubscriptionAccountDetailsUser(base.userID, base.tenantID,
                out userInfo, out subscriptionTypes, out subscriptionFeatures, null);
        }

        private void BindSubscriptionAndFeatureDetails()
        {
            UserRegistrationInfo userInfo = null;
            SubscriptionTypes subscriptionTypes = null;
            List<SubscriptionFeatures> subscriptionFeatures = null;
            try
            {
                GetSubscriptionAndFeatureDetails(out userInfo, out subscriptionTypes, out subscriptionFeatures);
                if ((userInfo == null || subscriptionTypes == null) && base.isSiteAdmin == false)
                {
                    RedirectToAccessDeniedPage();
                    return;
                }
                Forte_EditAccount_subscriptionTypeLabel.Text = subscriptionTypes.SubscriptionName;
                Forte_EditAccount_statusLabel.Text = userInfo.IsActive == 1 ? "Active" : "In-Active";
                Forte_EditAccount_subscribedOnLabel.Text = Convert.ToDateTime(userInfo.CreatedDate).ToString("MM/dd/yyyy");
                Forte_EditAccount_activatedOnLabel.Text = Convert.ToDateTime(userInfo.ActivatedOn).ToString("MM/dd/yyyy");
                Forte_EditAccount_noOfUsersLabel.Text = userInfo.NumberOfUsers.ToString();
                Forte_EditAccount_titleTextBox.Text = userInfo.Title;
                Forte_EditAccount_companyTextBox.Text = userInfo.Company;
                Forte_EditAccount_subscriptionRoleHidden.Value = userInfo.SubscriptionRole;
                Forte_EditAccount_isTrialLabel.Text = userInfo.IsTrial == 1 ? "Yes" : "No";
                Forte_EditAccount_phoneTextBox.Text = userInfo.Phone;
                Forte_EditAccount_firstNameTextBox.Text = userInfo.FirstName;
                Forte_EditAccount_lastNameTextBox.Text = userInfo.LastName;
                Forte_EditAccount_featureAndPricingGridView.DataSource = subscriptionFeatures;
                Forte_EditAccount_featureAndPricingGridView.DataBind();
                Forte_EditAccount_lastLoginLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
            }
            finally
            {
                if (userInfo != null) userInfo = null;
                if (subscriptionTypes != null) subscriptionTypes = null;
                if (subscriptionFeatures != null) subscriptionFeatures = null;
            }
        }

        /// <summary>
        /// A method that redirects to the unzuthorized page
        /// </summary>
        private void RedirectToAccessDeniedPage()
        {
            Response.Redirect("../Common/AccessDenied.aspx", false);
        }

        /// <summary>
        /// A Method that logs the exception message and 
        /// display's the error message to the user.
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception message
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            base.ShowMessage(Forte_EditAccount_topErrorMessageLabel,
                Forte_EditAccount_bottomErrorMessageLabel, exp.Message);
        }

        /// <summary>
        /// A Method that logs the exception message and 
        /// display's the error message to the user.
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.String"/> that holds the exception message
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            base.ShowMessage(Forte_EditAccount_topErrorMessageLabel,
                Forte_EditAccount_bottomErrorMessageLabel, Message);
        }

        #endregion Private Methods

        #region Override Methods                                               

        protected override bool IsValidData()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(Forte_EditAccount_firstNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyFirstName);
                isValid = false;
            }
            if (string.IsNullOrEmpty(Forte_EditAccount_lastNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyLastName);
                isValid = false;
            }
            if (string.IsNullOrEmpty(Forte_EditAccount_phoneTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyPhoneNo);
                isValid = false;
            }
            if (string.IsNullOrEmpty(Forte_EditAccount_companyTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_EmptyCompanyName);
                isValid = false;
            }
            return isValid;
        }

        protected override void LoadValues()
        {
            BindSubscriptionAndFeatureDetails();           
        }

        #endregion Override Methods
    }
}