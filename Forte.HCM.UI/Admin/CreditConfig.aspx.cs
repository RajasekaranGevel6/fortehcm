﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreditConfig.aspx.cs
// File that represents the user interface layout and functionalities 
// for credits config page. This page helps in  configuring the admin credit 
// config settings such as credit values.This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;

#endregion Directives                                                          

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities 
    /// for credits config page. This page helps in  configuring the admin credit 
    /// config settings such as credit values.This class inherits Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CreditConfig : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus
                Page.Form.DefaultButton = CreditConfig_topSaveButton.UniqueID;

                // Set page title
                Master.SetPageCaption("Credit Config");

                if (!IsPostBack)
                {
                    //Clear all labels
                    ClearAllLabelMessage();

                    // Load default values
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CreditConfig_topErrorMessageLabel,
                    CreditConfig_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreditConfig_saveButton_Click(object sender, EventArgs e)
        {
            DataSet dsCreditConfigModified = null;
            try
            {
                //Clear all labels
                ClearAllLabelMessage();
                string errorMessage = string.Empty;

                //Dataset loaded from viewstate
                dsCreditConfigModified = ViewState["CREDIT_CONFIG_EXISTING"] as DataSet;

                if (dsCreditConfigModified == null)
                {
                    ShowMessage(CreditConfig_topErrorMessageLabel,
                        CreditConfig_bottomErrorMessageLabel,
                        Resources.HCMResource.CreditConfig_RecordNotFoundToUpdate);
                    return;
                }
                if (dsCreditConfigModified.Tables[0].Rows.Count == 0 &&
                       dsCreditConfigModified.Tables[1].Rows.Count == 0 &&
                       dsCreditConfigModified.Tables[2].Rows.Count == 0 &&
                       dsCreditConfigModified.Tables[3].Rows.Count == 0)
                {
                    ShowMessage(CreditConfig_topErrorMessageLabel,
                        CreditConfig_bottomErrorMessageLabel,
                        Resources.HCMResource.CreditConfig_RecordNotFoundToUpdate);
                    return;
                }

                errorMessage = ValidateEmptyText(CreditConfig_questionCreditsGridView,
                     "CreditConfig_questionCreditsTextBox", errorMessage);
                errorMessage = ValidateEmptyText(CreditConfig_testCreditsGridView,
                     "CreditConfig_testCreditsTextBox", errorMessage);
                errorMessage = ValidateEmptyText(CreditConfig_testSessionCreditsGridView,
                     "CreditConfig_testSessionCreditsTextBox", errorMessage);
                errorMessage = ValidateEmptyText(CreditConfig_requestCreditsGridView,
                     "CreditConfig_requestCreditsTextBox", errorMessage);

                if (errorMessage != string.Empty)
                {
                    ShowMessage(CreditConfig_topErrorMessageLabel,
                        CreditConfig_bottomErrorMessageLabel,
                        errorMessage);
                    return;
                }

                if (CreditConfig_questionCreditsGridView.Rows.Count > 0)
                {
                    UpdateCreditValues(CreditConfig_questionCreditsGridView,
                        "CreditConfig_questionCreditsTextBox", dsCreditConfigModified.Tables[0]);
                }
                if (CreditConfig_testCreditsGridView.Rows.Count > 0)
                {
                    UpdateCreditValues(CreditConfig_testCreditsGridView,
                        "CreditConfig_testCreditsTextBox", dsCreditConfigModified.Tables[1]);
                }
                if (CreditConfig_testSessionCreditsGridView.Rows.Count > 0)
                {
                    UpdateCreditValues(CreditConfig_testSessionCreditsGridView,
                        "CreditConfig_testSessionCreditsTextBox", dsCreditConfigModified.Tables[2]);
                }
                if (CreditConfig_requestCreditsGridView.Rows.Count > 0)
                {
                    UpdateCreditValues(CreditConfig_requestCreditsGridView,
                        "CreditConfig_requestCreditsTextBox", dsCreditConfigModified.Tables[3]);
                }

                dsCreditConfigModified = new CreditBLManager().GetCreditConfig();
                ViewState["CREDIT_CONFIG_EXISTING"] = dsCreditConfigModified;

                base.ShowMessage(CreditConfig_topSuccessMessageLabel,
                    CreditConfig_bottomSuccessMessageLabel,
                    Resources.HCMResource.CreditConfig_SavedSuccessfully);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreditConfig_topErrorMessageLabel,
                    CreditConfig_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {   
                if (dsCreditConfigModified != null) dsCreditConfigModified = null;
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirects to the same page by loading the previously
        /// saved values.
        /// </remarks>
        protected void CreditConfig_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreditConfig_topErrorMessageLabel,
                    CreditConfig_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that validates credit config values.
        /// </summary>
        /// <param name="creditConfig_GridView">
        /// A <see cref="GridView"/> that holds the gridview.
        /// </param>
        /// <param name="textBoxID">
        /// A <see cref="string"/> that holds textbox ID.
        /// </param> 
        /// <param name="errorMessage">
        /// A <see cref="string"/> that holds error message.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains error message.
        /// </returns>
        private string ValidateEmptyText(GridView creditConfig_GridView, string textBoxID, string errorMessage)
        {
            decimal CreditValue = 0;
            foreach (GridViewRow row in creditConfig_GridView.Rows)
            {
                CreditValue = 0;
                decimal.TryParse(((TextBox)row.FindControl(textBoxID)).Text.Trim(), out  CreditValue);
                
                if (CreditValue > 0)
                    continue;

                if (errorMessage == string.Empty)
                {
                    errorMessage = string.Format
                        ("'{0}' should not be empty or zero",
                         creditConfig_GridView.DataKeys[row.RowIndex].Values[1].ToString());
                }
                else
                {
                    errorMessage = errorMessage + "<br>" + string.Format
                       ("'{0}' should not be empty or zero",
                         creditConfig_GridView.DataKeys[row.RowIndex].Values[1].ToString());
                }
            }
            return errorMessage;
        }

        /// <summary>
        /// Method that updates credit config values into database.
        /// </summary>
        /// <param name="creditConfig_GridView">
        /// A <see cref="GridView"/> that holds the gridview.
        /// </param>
        /// <param name="textBoxID">
        /// A <see cref="string"/> that holds textbox ID.
        /// </param>
        /// <param name="dtExistingCredits">
        /// A <see cref="DataTable"/> that holds credit config.
        /// </param> 
        private void UpdateCreditValues(GridView creditConfig_GridView, string textBoxID,
            DataTable dtExistingCredits)
        {
            for (int i = 0; i < creditConfig_GridView.Rows.Count; i++)
            {
                if (creditConfig_GridView.DataKeys[i].Value.ToString() !=
                        dtExistingCredits.Rows[i][0].ToString())
                    continue;

                if (((TextBox)creditConfig_GridView.Rows[i].FindControl(textBoxID)).Text ==
                        dtExistingCredits.Rows[i][1].ToString())
                    continue;

                new CreditBLManager().UpdateCreditConfig(
                    Convert.ToDecimal(((TextBox)creditConfig_GridView.Rows[i].FindControl(textBoxID)).Text),
                    Convert.ToInt32(creditConfig_GridView.DataKeys[i].Value),
                    base.userID);
            }
        }

        /// <summary>
        /// Clears all label messages
        /// </summary>
        private void ClearAllLabelMessage()
        {
            CreditConfig_topErrorMessageLabel.Text = string.Empty;
            CreditConfig_bottomErrorMessageLabel.Text = string.Empty;
            CreditConfig_topSuccessMessageLabel.Text = string.Empty;
            CreditConfig_bottomSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Load credit config details
            DataSet dsCreditConfig = null;
            dsCreditConfig = new CreditBLManager().GetCreditConfig();

            if (dsCreditConfig == null)
            {
                ShowMessage(CreditConfig_topErrorMessageLabel,
                    CreditConfig_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
                return;
            }
            if (dsCreditConfig.Tables[0].Rows.Count == 0 &&
                   dsCreditConfig.Tables[1].Rows.Count == 0 &&
                   dsCreditConfig.Tables[2].Rows.Count == 0 &&
                   dsCreditConfig.Tables[3].Rows.Count == 0)
            {
                ShowMessage(CreditConfig_topErrorMessageLabel,
                   CreditConfig_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
                return;
            }

            if (dsCreditConfig.Tables.Count >= 1)
            {
                CreditConfig_questionCreditsGridView.DataSource = dsCreditConfig.Tables[0];
                CreditConfig_questionCreditsGridView.DataBind();
            }
            if (dsCreditConfig.Tables.Count >= 2)
            {
                CreditConfig_testCreditsGridView.DataSource = dsCreditConfig.Tables[1];
                CreditConfig_testCreditsGridView.DataBind();
            }
            if (dsCreditConfig.Tables.Count >= 3)
            {
                CreditConfig_testSessionCreditsGridView.DataSource = dsCreditConfig.Tables[2];
                CreditConfig_testSessionCreditsGridView.DataBind();
            }
            if (dsCreditConfig.Tables.Count >= 4)
            {
                CreditConfig_requestCreditsGridView.DataSource = dsCreditConfig.Tables[3];
                CreditConfig_requestCreditsGridView.DataBind();
            }
            ViewState["CREDIT_CONFIG_EXISTING"] = dsCreditConfig;
        }

        #endregion Protected Overridden Methods                                
    }
}
