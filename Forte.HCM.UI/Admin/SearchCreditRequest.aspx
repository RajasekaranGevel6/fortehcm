<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCreditRequest.aspx.cs"
    MasterPageFile="~/MasterPages/OTMMaster.Master" Inherits="Forte.HCM.UI.Admin.SearchCreditRequest" %>

<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc2" %>
<asp:Content ID="SearchCreditRequest_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <script type="text/javascript">
        function CheckChanged(ctriID, textCtrl, GridDiv) {

            var ctrl = document.getElementById(ctriID);
            var DivCtrl = document.getElementById(textCtrl);
            var parentCtrl = document.getElementById(GridDiv);

            if (ctrl.checked) {
                if (parentCtrl.style["display"] == "none") {
                    DivCtrl.style["display"] = "block";
                }
                else {
                    var table = document.getElementById('<%=SearchCreditRequest_processCreditRequestPanel_processGridView.ClientID %>');
                    var rowCount = table.rows.length;
                    if (parentCtrl.style["display"] == "block") {
                        for (var i = 1; i < rowCount; i++) {
                            if (detectBrowser()) {
                                var textCtrl = table.rows[i].cells[2].childNodes[1];
                            } else {
                                var textCtrl = table.rows[i].cells[2].childNodes[0];
                            }
                            textCtrl.style.display = "block";

                        }
                    }

                }
            }
            else {
                DivCtrl.style["display"] = "none";

                var table = document.getElementById('<%=SearchCreditRequest_processCreditRequestPanel_processGridView.ClientID %>');
                if (table != null) {
                    var rowCount = table.rows.length;
                    if (parentCtrl.style["display"] == "block") {
                        for (var i = 1; i < rowCount; i++) {
                            if (detectBrowser()) {
                                var textCtrl = table.rows[i].cells[2].childNodes[1];
                            } else {
                                var textCtrl = table.rows[i].cells[2].childNodes[0];
                            }
                            textCtrl.style.display = "none";
                        }
                    }
                }
            }
        }



        //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }
      
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="SearchCreditRequest_headerLiteral" runat="server" Text="Credit Management"></asp:Literal>
                            <asp:HiddenField ID="SearchCreditRequest_browserHiddenField" runat="server" />
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 42%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 20%">
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchCreditRequest_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="SearchCreditRequest_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchCreditRequest_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SearchCreditRequest_messageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SearchCreditRequest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchCreditRequest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <div id="SearchCreditRequest_searchDIV" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="panel_inner_body_bg">
                                                <tr>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="SearchCreditRequest_firstNameLabel" runat="server" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 17%">
                                                        <div style="float: left; padding-right: 2px;">
                                                            <asp:TextBox ID="SearchCreditRequest_firstNameTextBox" MaxLength="50" runat="server"
                                                                Width="80%"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_firstNameImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the candidate" />
                                                            <asp:HiddenField ID="SearchCreditRequest_userNameHiddenField" runat="server" />
                                                        </div>
                                                    </td>
                                                    <td style="width: 4%">
                                                        <asp:Label ID="SearchCreditRequest_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 17%">
                                                        <div style="float: left; padding-right: 2px;">
                                                            <asp:TextBox ID="SearchCreditRequest_emailTextBox" MaxLength="50" runat="server"
                                                                Width="80%"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_emailImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the email ID" />
                                                        </div>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="SearchCreditRequest_processedHeadLabel" runat="server" Text="Processed"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 16%">
                                                        <div style="float: left; padding-right: 2px;">
                                                            <asp:DropDownList ID="SearchCreditRequest_processedDropDownList" runat="server" Width="94px">
                                                                <asp:ListItem Text="Both" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_statusImageButton" SkinID="sknHelpImageButton"
                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the processed status here" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="SearchCreditRequest_approvedLabel" runat="server" Text="Approved"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:DropDownList ID="SearchCreditRequest_approvedDropDownList" runat="server" Width="94px">
                                                                <asp:ListItem Text="Both" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_approvedImageButton" SkinID="sknHelpImageButton"
                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the approved status here" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2" colspan="8">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="SearchCreditRequest_requestDateFromLabel" runat="server" Text="Request Date From"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 2px;">
                                                            <asp:TextBox ID="SearchCreditRequest_requestDateFromTextBox" runat="server" Width="80%"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_requestDateFromCalenderImageButton" SkinID="sknCalendarImageButton"
                                                                runat="server" ImageAlign="Middle" />
                                                        </div>
                                                        <ajaxToolKit:MaskedEditExtender ID="SearchCreditRequest_requestDateFromMaskedEditExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_requestDateFromTextBox" Mask="99/99/9999"
                                                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                        <ajaxToolKit:MaskedEditValidator ID="SearchCreditRequest_requestDateFromMaskedEditValidator"
                                                            runat="server" ControlExtender="SearchCreditRequest_requestDateFromMaskedEditExtender"
                                                            ControlToValidate="SearchCreditRequest_requestDateFromTextBox" EmptyValueMessage="Date is required"
                                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                        <ajaxToolKit:CalendarExtender ID="SearchCreditRequest_requestDateFromCustomCalendarExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_requestDateFromTextBox" CssClass="MyCalendar"
                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCreditRequest_requestDateFromCalenderImageButton" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="SearchCreditRequest_requestDateToLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 2px;">
                                                            <asp:TextBox ID="SearchCreditRequest_requestDateToTextBox" runat="server" Width="80%"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_requestDateToCalenderImageButton" SkinID="sknCalendarImageButton"
                                                                runat="server" ImageAlign="Middle" />
                                                        </div>
                                                        <ajaxToolKit:MaskedEditExtender ID="SearchCreditRequest_requestDateToMaskedEditExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_requestDateToTextBox" Mask="99/99/9999"
                                                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                        <ajaxToolKit:MaskedEditValidator ID="SearchCreditRequest_requestDateToMaskedEditValidator"
                                                            runat="server" ControlExtender="SearchCreditRequest_requestDateToMaskedEditExtender"
                                                            ControlToValidate="SearchCreditRequest_requestDateToTextBox" EmptyValueMessage="Date is required"
                                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                        <ajaxToolKit:CalendarExtender ID="SearchCreditRequest_requestDateToCalendarExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_requestDateToTextBox" CssClass="MyCalendar"
                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCreditRequest_requestDateToCalenderImageButton" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="SearchCreditRequest_processedDateFromLabel" runat="server" Text="Processed Date From"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 2px;">
                                                            <asp:TextBox ID="SearchCreditRequest_processedDateFromTextBox" runat="server" Width="80px"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_processedDateFromCalenderImageButton" SkinID="sknCalendarImageButton"
                                                                runat="server" ImageAlign="Middle" />
                                                        </div>
                                                        <ajaxToolKit:MaskedEditExtender ID="SearchCreditRequest_processedDateFromMaskedEditExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_processedDateFromTextBox"
                                                            Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                            ErrorTooltipEnabled="True" />
                                                        <ajaxToolKit:MaskedEditValidator ID="SearchCreditRequest_processedDateFromMaskedEditValidator"
                                                            runat="server" ControlExtender="SearchCreditRequest_processedDateFromMaskedEditExtender"
                                                            ControlToValidate="SearchCreditRequest_processedDateFromTextBox" EmptyValueMessage="Date is required"
                                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                        <ajaxToolKit:CalendarExtender ID="SearchCreditRequest_processedDateFromCalendarExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_processedDateFromTextBox"
                                                            CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCreditRequest_processedDateFromCalenderImageButton" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="SearchCreditRequest_processedDateToLabel" runat="server" Text="To"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 2px;">
                                                            <asp:TextBox ID="SearchCreditRequest_processedDateToTextBox" runat="server" Width="80px"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="SearchCreditRequest_processedDateToCalenderImageButton" SkinID="sknCalendarImageButton"
                                                                runat="server" ImageAlign="Middle" />
                                                        </div>
                                                        <ajaxToolKit:MaskedEditExtender ID="SearchCreditRequest_processedDateToMaskedEditExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_processedDateToTextBox" Mask="99/99/9999"
                                                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                        <ajaxToolKit:MaskedEditValidator ID="SearchCreditRequest_processedDateToMaskedEditValidator"
                                                            runat="server" ControlExtender="SearchCreditRequest_processedDateToMaskedEditExtender"
                                                            ControlToValidate="SearchCreditRequest_processedDateToTextBox" EmptyValueMessage="Date is required"
                                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                        <ajaxToolKit:CalendarExtender ID="SearchCreditRequest_processedDateToCalendarExtender"
                                                            runat="server" TargetControlID="SearchCreditRequest_processedDateToTextBox" CssClass="MyCalendar"
                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchCreditRequest_processedDateToCalenderImageButton" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2" colspan="8">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="SearchCreditRequest_amountLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                            Text="Amount"></asp:Label>
                                                    </td>
                                                    <td colspan="2">
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="width: 7%">
                                                                    <asp:TextBox ID="SearchCreditRequest_amountMinValueTextBox" runat="server" MaxLength="2"
                                                                        Width="25" Text=""></asp:TextBox>
                                                                </td>
                                                                <td style="width: 21%">
                                                                    <asp:TextBox ID="SearchCreditRequest_amountTextBox" runat="server" MaxLength="250"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:TextBox ID="SearchCreditRequest_amountMaxValueTextBox" runat="server" MaxLength="2"
                                                                        Width="25" Text=""></asp:TextBox>
                                                                    <ajaxToolKit:MultiHandleSliderExtender ID="SearchCreditRequest_amountMultiHandleSliderExtender"
                                                                        runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="99" Minimum="0"
                                                                        RailCssClass="slider_rail" Steps="100" HandleCssClass="slider_handler" TargetControlID="SearchCreditRequest_amountTextBox"
                                                                        ShowHandleDragStyle="true" ShowHandleHoverStyle="true" Length="100">
                                                                        <MultiHandleSliderTargets>
                                                                            <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchCreditRequest_amountMinValueTextBox"
                                                                                HandleCssClass="MultiHandleMinSliderCss" />
                                                                            <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchCreditRequest_amountMaxValueTextBox"
                                                                                HandleCssClass="MultiHandleMaxSliderCss" />
                                                                        </MultiHandleSliderTargets>
                                                                    </ajaxToolKit:MultiHandleSliderExtender>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td colspan="5">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="width: 28%">
                                                                    <asp:Label ID="SearchCreditRequest_creditTypeLabel" runat="server" Text="Credit Type"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="SearchCreditRequest_creditTypeDropDownList" runat="server"
                                                                        Width="200px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="right">
                                                <tr>
                                                    <td>
                                                       
                                                                <asp:Button ID="SearchCreditRequest_searchButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Search" OnClick="SearchCreditRequest_searchButton_Click" />
                                                     
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="SearchCreditRequest_updatePanel" runat="server">
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr id="SearchCreditRequest_expandTR" runat="server">
                                            <td class="header_bg">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="SearchCreditRequest_searchResultHeader" runat="server" Text="Search Results"></asp:Literal>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="SearchCreditRequest_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="SearchCreditRequest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                                            <span id="SearchCreditRequest_searchResultsDownSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="SearchCreditRequest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage"
                                                                    Height="16px" /></span>
                                                            <asp:HiddenField ID="SearchCreditRequest_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <div id="SearchCreditRequest_Div" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <div id="SearchCreditRequest_resultDiv" runat="server" style="height: 190px; overflow: auto;">
                                                                    <asp:GridView ID="SearchCreditRequest_resultGridView" Width="100%" runat="server"
                                                                        AllowSorting="true" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                        AutoGenerateColumns="false" OnRowCreated="SearchCreditRequest_resultGridView_RowCreated"
                                                                        OnRowDataBound="SearchCreditRequest_resultGridView_RowDataBound" OnSorting="SearchCreditRequest_resultGridView_Sorting"
                                                                        OnRowCommand="SearchCreditRequest_resultGridView_RowCommand">
                                                                        <RowStyle CssClass="grid_alternate_row" />
                                                                        <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                        <HeaderStyle CssClass="grid_header_row" />
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="SearchCreditRequest_selectRequestCheckBox" runat="server" />
                                                                                    <asp:ImageButton ID="SearchCreditRequest_processCreditRequestImageButton" runat="server"
                                                                                        SkinID="sknProcessCreditRequest" ToolTip="Process Credit Request" CommandName="ProcessRequest"
                                                                                        CommandArgument='<%#Eval("GenId") %>' />
                                                                                    <asp:ImageButton ID="SearchCreditRequest_showRemarksImageButton" runat="server" SkinID="sknShowCreditRemarks"
                                                                                        ToolTip="Show Remarks" CommandName="ShowRemarks" Visible='<%# Eval("IsProcessed") %>' />
                                                                                    <asp:ImageButton ID="SearchCreditRequest_viewCreditsImageButton" runat="server" SkinID="sknViewCredits"
                                                                                        ToolTip="View Credits" CommandName="ViewCredits" />
                                                                                    <asp:ImageButton ID="SearchCreditRequest_questionDetailsImageButton" runat="server"
                                                                                        SkinID="sknPreviewImageButton" ToolTip="View Question Details" CommandName="ViewQuestions"
                                                                                        CommandArgument='<%# Eval("EntityID") %>' Visible="false" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Name" SortExpression="CANDIDATE_NAME">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchCreditRequest_resultGridView_CandidateNameLabel" runat="server"
                                                                                        Text='<%# Eval("CandidateFirstName") %>' ToolTip='<%# Eval("CandidateFullName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Credit Type" SortExpression="CREDIT_TYPE">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchCreditRequest_resultGridView_requestDescriptionLabel" runat="server"
                                                                                        Text='<%# Eval("CreditRequestDescription") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Request Date" SortExpression="REQUEST_DATE">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchCreditRequest_resultGridView_requestDateLabel" runat="server"
                                                                                        Text='<%# GetCorrectDate(Convert.ToDateTime(Eval("RequestDate"))) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Processed" SortExpression="PROCESSED">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchCreditRequest_resultGridView_processedLabel" runat="server"
                                                                                        Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsProcessed"))) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Processed Date" SortExpression="PROCESSED_DATE">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchCreditRequest_resultGridView_processedDateLabel" runat="server"
                                                                                        Text='<%# GetCorrectDate(Convert.ToDateTime(Eval("ProcessedDate"))) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Approved" SortExpression="APPROVED">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchCreditRequest_resultGridView_approvedLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsApproved"))) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Amount (in $)" SortExpression="AMOUNT" ItemStyle-HorizontalAlign="Right"
                                                                                ItemStyle-CssClass="td_padding_right_20" HeaderStyle-HorizontalAlign="Right"
                                                                                ItemStyle-Width="40px" HeaderStyle-CssClass="td_padding_right_20" HeaderStyle-Width="45px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchCreditRequest_amountLabel" runat="server" Text='<%# GetCorrectAmount(Convert.ToDecimal(Eval("Amount"))) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <%--                                                                     <asp:BoundField HeaderText="Amount (in $)" DataField="Amount" SortExpression="AMOUNT"
                                                                                ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="td_padding_right_20" HeaderStyle-HorizontalAlign="Right"
                                                                                ItemStyle-Width="40px" HeaderStyle-CssClass="td_padding_right_20" HeaderStyle-Width="45px" />--%>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="SearchCreditRequest_resultGridView_candidateIdHiddenField" runat="server"
                                                                                        Value='<%#Eval("CandidateId") %>' />
                                                                                    <asp:HiddenField ID="SearchCreditRequest_resultGridView_remarksHiddenField" runat="server"
                                                                                        Value='<%#Eval("Remarks") %>' />
                                                                                    <asp:HiddenField ID="SearchCreditRequest_resultGridView_creditValueHiddenField" runat="server"
                                                                                        Value='<%#Eval("CreditValue") %>' />
                                                                                    <asp:HiddenField ID="SearchCreditRequest_resultGridView_requestIDHiddenField" runat="server"
                                                                                        Value='<%#Eval("CreditRequestID") %>' />
                                                                                    <asp:HiddenField ID="SearchCreditRequest_resultGridView_GenIdHiddenField" runat="server"
                                                                                        Value='<%#Eval("GenId") %>' />
                                                                                    <asp:HiddenField ID="SearchCreditRequest_resultGridView_candidateEmailIDHiddenField"
                                                                                        runat="server" Value='<%#Eval("CandidateEMail") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="SearchCreditRequest_approveCreditButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Approve" OnClick="SearchCreditRequest_approveCreditButton_Click" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <uc1:PageNavigator ID="SearchCreditRequest_pageNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="SearchCreditRequest_questionPanel" runat="server" Style="display: none"
                                                                    CssClass="popupcontrol_question_detail">
                                                                    <div style="display: none">
                                                                        <asp:Button ID="SearchCreditRequest_questionHiddenButton" runat="server" Text="Hidden" /></div>
                                                                    <uc2:QuestionDetailPreviewControl ID="SearchCreditRequest_questionDetailPreviewControl"
                                                                        runat="server" />
                                                                </asp:Panel>
                                                                <ajaxToolKit:ModalPopupExtender ID="SearchCreditRequest_questionModalPopupExtender"
                                                                    runat="server" PopupControlID="SearchCreditRequest_questionPanel" TargetControlID="SearchCreditRequest_questionHiddenButton"
                                                                    BackgroundCssClass="modalBackground">
                                                                </ajaxToolKit:ModalPopupExtender>
                                                            </td>
                                                        </tr>
                                                        <asp:HiddenField ID="SearchCreditRequest_isMaximizedHiddenField" runat="server" />
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchCreditRequest_searchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchCreditRequest_processCreditRequestUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="SearchCreditRequest_processCreditRequestPanel" runat="server" CssClass="popupcontrol_credit_request"
                            Style="display: none" DefaultButton="SearchCreditRequest_submitButton">
                            <div style="display: none;">
                                <asp:Button ID="SearchCreditRequest_hiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="SearchCreditRequest_processCreditRequestLiteral" runat="server"
                                                        Text="Process Credit Request"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="SearchCreditRequest_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="3" cellspacing="3" width="100%">
                                                                    <tr>
                                                                        <td class="msg_align">
                                                                            <asp:Label ID="SearchCreditRequest_processCreditRequestPanel_errorLabel" runat="server"
                                                                                SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div style="float: left">
                                                                                            <asp:CheckBox ID="SearchCreditRequest_processCreditRequestPanel_approvedCheckBox"
                                                                                                runat="server" />
                                                                                        </div>
                                                                                        <asp:Label ID="SearchCreditRequest_processCreditRequestPanel_approvedLabel" runat="server"
                                                                                            SkinID="sknLabelFieldText" Text="Approve"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 60%" align="right" valign="top">
                                                                                        <div id="SearchCreditRequest_processCreditRequestPanel_amountDiv" runat="server"
                                                                                            style="height: 5px; display: none">
                                                                                            <table width="60%">
                                                                                                <tr>
                                                                                                    <td style="width: 70%" align="right">
                                                                                                        <asp:Label ID="SearchCreditRequest_processCreditRequestPanel_amoutHeadLabel" runat="server"
                                                                                                            Text="Amount (in $)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        <span class="mandatory">*</span>
                                                                                                    </td>
                                                                                                    <td style="width: 30%" align="right">
                                                                                                        <asp:TextBox ID="SearchCreditRequest_processCreditRequestPanel_amountTextBox" runat="server"
                                                                                                            Width="90%" Enabled="true"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="SearchCreditRequest_processCreditRequestPanel_amountmaskedEditExtender"
                                                                                        TargetControlID="SearchCreditRequest_processCreditRequestPanel_amountTextBox"
                                                                                        MaskType="Number" runat="server" Mask="99.99" OnFocusCssClass="MaskedEditFocus"
                                                                                        AutoComplete="false" OnInvalidCssClass="MaskedEditError" MessageValidatorTip="true"
                                                                                        InputDirection="LeftToRight" ErrorTooltipEnabled="True">
                                                                                    </ajaxToolKit:MaskedEditExtender>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tab_body_bg" id="SearchCreditRequest_processCreditRequestPanel_tdDiv"
                                                                            style="width: 360px; display: none" runat="server">
                                                                            <div id="SearchCreditRequest_processCreditRequestPanel_gridDiv" runat="server" style="height: 100px;
                                                                                overflow: auto; width: 410px">
                                                                                <asp:GridView ID="SearchCreditRequest_processCreditRequestPanel_processGridView"
                                                                                    runat="server" Width="100%" GridLines="Horizontal" BorderColor="white" BorderWidth="1px">
                                                                                    <RowStyle CssClass="grid_alternate_row" />
                                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                                    <Columns>
                                                                                        <asp:BoundField HeaderText="Name" DataField="CandidateFirstName" ControlStyle-Width="20%"
                                                                                            HeaderStyle-Width="30%" />
                                                                                        <asp:BoundField HeaderText="Credit Type" DataField="CreditRequestDescription" ControlStyle-Width="40%"
                                                                                            HeaderStyle-Width="40%" />
                                                                                        <asp:TemplateField HeaderText="Amount" ControlStyle-Width="60%" HeaderStyle-Width="20%">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="SearchCreditRequest_processCreditRequestPanel_processGridView_amountTextBox"
                                                                                                    runat="server" Text='<%#Eval("Amount") %>'></asp:TextBox>
                                                                                                <asp:HiddenField ID="SearchCreditRequest_processCreditRequestPanel_candidateEmailIDHiddenField"
                                                                                                    runat="server" Value='<%#Eval("CandidateEMail") %>' />
                                                                                                <asp:HiddenField ID="SearchCreditRequest_processCreditRequestPanel_requestIDHiddenField"
                                                                                                    runat="server" Value='<%#Eval("CreditRequestID") %>' />
                                                                                                <asp:HiddenField ID="SearchCreditRequest_processCreditRequestPanel_processGridView_GenIdHiddenField"
                                                                                                    runat="server" Value='<%#Eval("GenId") %>' />
                                                                                                <asp:HiddenField ID="SearchCreditRequest_processCreditRequestPanel_processGridView_CandidateIdHiddenField"
                                                                                                    runat="server" Value='<%#Eval("CandidateId") %>' />
                                                                                                <asp:HiddenField ID="SearchCreditRequest_processCreditRequestPanel_processGridView_CandidateNameHiddenField"
                                                                                                    runat="server" Value='<%#Eval("CandidateFirstName") %>' />
                                                                                                <ajaxToolKit:MaskedEditExtender ID="SearchCreditRequest_processCreditRequestPanel_amountmaskedEditExtender"
                                                                                                    TargetControlID="SearchCreditRequest_processCreditRequestPanel_processGridView_amountTextBox"
                                                                                                    MaskType="Number" runat="server" Mask="99.99" OnFocusCssClass="MaskedEditFocus"
                                                                                                    AutoComplete="false" OnInvalidCssClass="MaskedEditError" MessageValidatorTip="true"
                                                                                                    InputDirection="LeftToRight" ErrorTooltipEnabled="True">
                                                                                                </ajaxToolKit:MaskedEditExtender>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchCreditRequest_processCreditRequestPanel_remarksHeadLabel" runat="server"
                                                                                Text="Remarks" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCreditRequest_processCreditRequestPanel_remarksTextBox" TextMode="MultiLine"
                                                                                Height="100" runat="server" Width="99%" SkinID="sknMultiLineTextBox" MaxLength="500"
                                                                                onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" TabIndex="2"> </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="SearchCreditRequest_submitButton" runat="server" Text="Submit" SkinID="sknButtonId"
                                                        OnClick="SearchCreditRequest_submitButton_Click" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="SearchCreditRequest_cancelButton" runat="server" Text="Cancel"
                                                        SkinID="sknPopupLinkButton" OnClick="SearchCreditRequest_cancelButton_Click"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchCreditRequest_processRequestModalPopUpExtender"
                            runat="server" TargetControlID="SearchCreditRequest_hiddenButton" PopupControlID="SearchCreditRequest_processCreditRequestPanel"
                            BackgroundCssClass="modalBackground" CancelControlID="SearchCreditRequest_cancelButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchCreditRequest_showRemarksUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="SearchCreditRequest_showRemarksPanel" runat="server" CssClass="popupcontrol_credit_remarks"
                            Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="SearchCreditRequest_showRemarksPanel_hiddenButton" runat="server"
                                    Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="SearchCreditRequest_showRemarksPanel_headerLiteral" runat="server"
                                                        Text="Remarks"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="SearchCreditRequest_showRemarksPanel_closeImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" align="left" colspan="2">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td>
                                                                <div class="label_multi_field_text" style="height: 100px; overflow: auto; width: 410px;">
                                                                    <asp:Literal ID="SearchCreditRequest_showRemarksPanel_remarksLiteral" runat="server"></asp:Literal>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8" colspan="2">
                                                </td>
                                            </tr>
                                            <tr align="left">
                                                <td colspan="2">
                                                    <asp:LinkButton ID="SearchCreditRequest_showRemarksPanel_cancelButton" runat="server"
                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchCreditRequest_showRemarksPanel_modalPopupExtender"
                            runat="server" TargetControlID="SearchCreditRequest_showRemarksPanel_hiddenButton"
                            PopupControlID="SearchCreditRequest_showRemarksPanel" BackgroundCssClass="modalBackground"
                            CancelControlID="SearchCreditRequest_showRemarksPanel_cancelButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SearchCreditRequest_bottomMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SearchCreditRequest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchCreditRequest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table align="right" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td width="72%">
                        </td>
                        <td width="28%">
                            <table align="right" cellpadding="2" cellspacing="4" border="0">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="SearchCreditRequest_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="SearchCreditRequest_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SearchCreditRequest_bottomCancelLinkButton" Text="Cancel" runat="server"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
