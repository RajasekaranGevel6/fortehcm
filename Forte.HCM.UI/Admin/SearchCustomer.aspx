﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/CustomerAdminMaster.Master" Inherits="Forte.HCM.UI.Admin.SearchCustomer" CodeBehind="SearchCustomer.aspx.cs" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchCustomer_content" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <asp:UpdatePanel ID="up1" runat="server">
     <Triggers>
                <asp:PostBackTrigger ControlID="SearchUserInformation_exportExcelLinkButton" />
            </Triggers>
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Label ID="test" runat="server"></asp:Label>
                                    <asp:Literal ID="FeatureUsage_headerLiteral" runat="server" Text="Search Tenant"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="SearchCustomer_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="SearchCustomer_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="SearchCustomer_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="SearchCustomer_topMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="SearchCustomer_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="SearchCustomer_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td class="tab_body_bg">
                                                <div id="FeatureUsage_SummaryDIV" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="td_padding_top_5" style="width: 100%">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 100%">
                                                                            <div id="SearchUserInformation_searchCriteriasDiv" runat="server" style="display: block;">
                                                                                <asp:HiddenField ID="SearchUserInformation_browserHiddenField" runat="server" />
                                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                                    <tr>
                                                                                        <td valign="top" class="panel_bg" style="width: 100%">
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td style="width: 100%">
                                                                                                        <table border="0" cellpadding="1" cellspacing="1" width="100%" class="panel_inner_body_bg">
                                                                                                            <tr>
                                                                                                                <td class="td_padding_bottom_5">
                                                                                                                    <asp:Label ID="SearchCustomer_userNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="User ID"></asp:Label>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="SearchCustomer_userNameTextBox" autocomplete="off" runat="server"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="SearchCustomer_userTypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="User Type"></asp:Label>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:DropDownList ID="SearchCustomer_userTypeDropDownList" runat="server" Width="128px">
                                                                                                                        <asp:ListItem Value="A">All</asp:ListItem>
                                                                                                                        <asp:ListItem Value="I">Internal</asp:ListItem>
                                                                                                                        <asp:ListItem Value="S">Subscription</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="SearchCustomer_activatedLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Activated"></asp:Label>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:DropDownList ID="SearchCustomer_activatedDropDownList" runat="server" Width="128px">
                                                                                                                        <asp:ListItem Value="B">Both</asp:ListItem>
                                                                                                                        <asp:ListItem Value="A">Yes</asp:ListItem>
                                                                                                                        <asp:ListItem Value="N">No</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="SearchUserInformation_firstNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="First Name"></asp:Label>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="SearchUserInformation_firstNameTextBox" runat="server"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="SearchUserInformation_middleNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Middle Name"></asp:Label>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="SearchUserInformation_middleNameTextBox" runat="server"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="SearchUserInformation_lastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Last Name"></asp:Label>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="SearchUserInformation_lastNameTextBox" runat="server"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="right" colspan="4" class="td_padding_top_5">
                                                                                                        <asp:Button ID="SearchUserInformation_searchButton" runat="server" SkinID="sknButtonId"
                                                                                                            Text="Search" OnClick="SearchUserInformation_searchButton_Click" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr id="SearchUserInformation_searchTestResultsTR" runat="server">
                                                                        <td class="header_bg">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td style="width: 50%">
                                                                                        <asp:Literal ID="SearchUserInformation_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                                    </td>
                                                                                   
                                                                                    <td style="width: 50%" align="right">
                                                                                        <span id="SearchUserInformation_searchResultsUpSpan" style="display: none;" runat="server">
                                                                                            <asp:Image ID="SearchUserInformation_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                                        </span><span id="SearchUserInformation_searchResultsDownSpan" style="display: block;"
                                                                                            runat="server">
                                                                                            <asp:Image ID="SearchUserInformation_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="grid_body_bg">  
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr> <td style="float:right" class="link_button">
                                                                                    <asp:LinkButton runat="server" Text="Export To excel" ID="SearchUserInformation_exportExcelLinkButton" OnClick="SearchUserInformation_exportExcelLinkButton_Click"></asp:LinkButton>
                                                                                    </td></tr>
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <div style="height: 300px; overflow: auto;" runat="server" id="SearchUserInformation_testDiv">
                                                                                            <asp:GridView ID="SearchUserInformation_testGridView" runat="server" AllowSorting="True"
                                                                                                AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                                Width="100%" OnSorting="SearchUserInformation_testGridView_Sorting" OnRowDataBound="SearchUserInformation_testGridView_RowDataBound"
                                                                                                OnRowCreated="SearchUserInformation_testGridView_RowCreated" OnRowCommand="SearchUserInformation_testGridView_RowCommand">
                                                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                                <HeaderStyle CssClass="grid_header_row" />
                                                                                                <Columns>
                                                                                                    <asp:TemplateField>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="SearchUserInformation_activateUserImageButton"
                                                                                                                runat="server" SkinID="sknActiveImageButton" ToolTip="Activate User" Visible='<%# GetActiveIcon(Convert.ToInt16(Eval("IsActive"))) %>'
                                                                                                                CommandName="ActivateUser" CommandArgument='<% #Eval("UserID") %>' />
                                                                                                            <asp:ImageButton ID="SearchUserInformation_deActivateUserImageButton"
                                                                                                                runat="server" SkinID="sknInactiveImageButton" ToolTip="Deactivate User" Visible='<%# GetInActiveIcon(Convert.ToInt16(Eval("IsActive"))) %>'
                                                                                                                CommandName="DeactivateUser" CommandArgument='<% #Eval("UserID") %>' />
                                                                                                            <asp:ImageButton ID="SearchUserInformation_resendPasswordImageButton" ToolTip="Resend Password" 
                                                                                                                runat="server" SkinID="sknPasswordResendImageButton" CommandName="ResendPassword" />
                                                                                                            <asp:ImageButton ID="SearchUserInformation_emailActivationCodeImageButton" runat="server"
                                                                                                                SkinID="sknMailImageButton" ToolTip="Email Activation Code" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                CommandName="EmailActivationCode" Visible='<%# IsUserNotActivated(Eval("Activated").ToString())%>'
                                                                                                                CssClass="showCursor" />
                                                                                                            <asp:ImageButton ID="SearchUserInformation_editUserImageButton" runat="server" SkinID="sknEditUser"
                                                                                                                ToolTip="Edit User" CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditUser"
                                                                                                                Visible="false" CssClass="showCursor" />
                                                                                                            <asp:HiddenField ID="SearchUserInformation_subscriptionTypeHiddenField" runat="server"
                                                                                                                Value='<%# Eval("SubscriptionType") %>' />
                                                                                                            <asp:HiddenField ID="SearchUserInformation_userTypeHiddenField" runat="server" Value='<%# Eval("UserTypeName") %>' />
                                                                                                            <asp:ImageButton ID="SearchUserInformation_assignUserRolesImageButton" runat="server"
                                                                                                                SkinID="sknAssignUserRoles" ToolTip="Assign User Roles" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                CommandName="AssignUserRoles" CssClass="showCursor" />
                                                                                                            <asp:ImageButton ID="SearchUserInformation_assignUserRightsImageButton" runat="server"
                                                                                                                SkinID="sknAssignUserRights" ToolTip="Assign User Rights" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                CommandName="AssignUserRights" CssClass="showCursor" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderStyle-Wrap="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:HiddenField ID="SearchUserInformation_userNameHiddenfield" runat="server" Value='<%# Eval("UserName") %>' />
                                                                                                            <asp:HiddenField ID="SearchUserInformation_userIDHiddenfield" runat="server" Value='<%# Eval("UserID") %>' />
                                                                                                            <asp:HiddenField ID="SearchUserInformation_userFirstNameHiddenfield" runat="server"
                                                                                                                Value='<%# Eval("FIRSTNAME") %>' />
                                                                                                            <asp:HiddenField ID="SearchUserInformation_userLastNameHiddenfield" runat="server"
                                                                                                                Value='<%# Eval("LastName") %>' />
                                                                                                            <asp:HiddenField ID="SearchUserInformation_activateStatusHiddenFieldButton" runat="server"
                                                                                                                Value='<%# Eval("IsActive") %>' />
                                                                                                            <asp:HiddenField ID="SearchUserInformation_userEmailHiddenfield" runat="server" Value='<%# Eval("Email") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField HeaderText="User_ID" DataField="UserID" Visible="false" />
                                                                                                    <asp:BoundField HeaderText="User ID" DataField="UserName" SortExpression="USERNAME" />
                                                                                                    <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FIRSTNAME" />
                                                                                                    <asp:BoundField HeaderText="Middle Name" DataField="MiddleName" SortExpression="MIDDLENAME" />
                                                                                                    <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LASTNAME" />
                                                                                                    <asp:BoundField HeaderText="User Type" DataField="UserTypeName" SortExpression="USERTYPENAME" />
                                                                                                    <asp:BoundField HeaderText="Subscription Type" DataField="SubscriptionType" SortExpression="USERSUBSCRIPTIONTYPE" />
                                                                                                    <asp:BoundField HeaderText="Legally Accepted" DataField="LegalAcceptedDesc" SortExpression="USERLEGALYACCEPTED" />
                                                                                                    <asp:BoundField HeaderText="Activated" DataField="Activated" SortExpression="ACTIVATED" />
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <uc1:PageNavigator ID="SearchUserInformation_pageNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="msg_align">
                                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:Label ID="SearchCustomer_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                                                    <asp:Label ID="SearchCustomer_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="SearchCustomer_activateUserUpdatePanel" runat="server"
                                                                                UpdateMode="Always">
                                                                                <ContentTemplate>
                                                                                    <div style="display: none">
                                                                                        <asp:Button ID="SearchCustomer_activateUserHiddenButton" runat="server" />
                                                                                    </div>
                                                                                    <asp:Panel ID="SearchCustomer_activateUserPanel" runat="server"
                                                                                        Style="display: none" CssClass="popupcontrol_confirm_remove">
                                                                                        <uc2:confirmmsgcontrol id="SearchCustomer_activateUserConfirmMsgControl"
                                                                                            runat="server" onokclick="SearchCustomer_activateUserConfirmMsgControl_OKClick" />
                                                                                        <asp:HiddenField ID="SearchCustomer_activateUserHiddenField" runat="server" />
                                                                                        <asp:HiddenField ID="SearchCustomer_activateStatusHiddenField" runat="server" />
                                                                                    </asp:Panel>
                                                                                    <ajaxToolKit:ModalPopupExtender ID="SearchCustomer_activateUserModalPopupExtender"
                                                                                        runat="server" PopupControlID="SearchCustomer_activateUserPanel"
                                                                                        TargetControlID="SearchCustomer_activateUserHiddenButton" BackgroundCssClass="modalBackground">
                                                                                    </ajaxToolKit:ModalPopupExtender>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:HiddenField ID="SearchUserInformation_isMaximizedHiddenField" runat="server" />
                                                                <%-- </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="SearchUserInformation_searchButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    &nbsp;
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="SearchCustomer_bottomResetButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="SearchCustomer_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="SearchCustomer_bottomCancelButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
     
</asp:Content>
