﻿
#region Namespace

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;


using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Namespace

namespace Forte.HCM.UI.Admin
{
    public partial class SubscriptionFeatureTypeSetup : PageBase
    {
        protected string ButtonID = null;

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(HCMResource.SubscriptionFeatureType_TitleMessage);
               // Page.SetFocus(Forte_SubscriptionFeatureTypeSetup_featureTypesTopSaveButton.ClientID);
              //  Page.Form.DefaultButton = Forte_SubscriptionFeatureTypeSetup_hiddenButton.UniqueID;

                Forte_SubscriptionFeatureTypeSetup_addNewSubscriptionType.Created_By = base.userID;
                Forte_SubscriptionFeatureTypeSetup_AddFeatureSubscriptionType.Created_By = base.userID;
                if (IsPostBack)
                    return;
                LoadValues();

                if (!IsPostBack)
                {
                    Panel modalPopUpPanel = (Panel)this.FindControl("Forte_SubscriptionFeatureTypeSetup_newSubscriptionTypePanel");

                    ButtonID =
                         ((Button)Forte_SubscriptionFeatureTypeSetup_addNewSubscriptionType.FindControl
                         ("Forte_AddNewSubscriptionType_subscriptionSaveButton")).ID;

                    if (modalPopUpPanel != null && ButtonID != string.Empty)
                    {
                        modalPopUpPanel.DefaultButton = ButtonID;
                    }
                }

            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void ResetLinkButtonclick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        #region Subscription Type Events

        protected void Forte_SubscriptionFeatureTypeSetup_subscriptionTypeGridview_RowCommand(
            object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete Subscription")
                {
                    Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionId.Value = e.CommandArgument.ToString();
                    Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeModalPopupExtender.Show();
                }
                else if (e.CommandName == "Edit Subscription")
                {
                    Forte_SubscriptionFeatureTypeSetup_addNewSubscriptionType.IsEditMode = true;
                    Forte_SubscriptionFeatureTypeSetup_addNweSubscriptionTypeModalPopupExtender.Show();
                    SubscriptionTypes subscriptionType = new SubscriptionTypes();
                    subscriptionType.SubscriptionId = Convert.ToInt32(e.CommandArgument);
                    subscriptionType.SubscriptionName =
                        ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_SubscriptionFeatureTypeSetup_subscriptionNameLabel")).Text;
                    subscriptionType.SubscriptionDescription =
                        ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_SubscriptionFeatureTypeSetup_subscriptionDescriptionLabel")).Text;
                    subscriptionType.RoleIDs = ((HiddenField)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_SubscriptionFeatureTypeSetup_roleIDHiddenField")).Value;

                    Forte_SubscriptionFeatureTypeSetup_addNewSubscriptionType.SetEditableDataSource = subscriptionType;
                }

                Button button =
                       ((Button)Forte_SubscriptionFeatureTypeSetup_addNewSubscriptionType.FindControl
                       ("Forte_AddNewSubscriptionType_subscriptionSaveButton"));

                if (button != null)
                {
                    //Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionPanel.DefaultButton = button.UniqueID;
                    button.Focus();

                }
                
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypeokPopUpClick(object sender, EventArgs e)
        {
            try
            {
                DeleteSubscriptionType(Convert.ToInt32(Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionId.Value));
                Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionId.Value = "";
                BindSubscyptionTypeGirdView();
                ShowSuccessMessage(HCMResource.SubscriptionType_DeleteSubscriptionTypeSuccessFull);
            }
            catch (Exception exp)
            {
                if (exp.Message.Contains("SUBSCRIPTIONS AVAILABLE"))
                    ShowErrorMessage(new Exception(HCMResource.SubscriptionType_DeleteSubscriptionErrorMessage));
                else
                    ShowErrorMessage(exp);
            }
        }

        protected void Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionTypecancelPopUpClick(object sender, EventArgs e)
        {
            Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionId.Value = "";
        }

        #endregion Subscription Type Events

        #region Feature Type Events

        protected void Forte_SubscriptionFeatureTypeSetup_featureTypeGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete Feature")
                {
                    Forte_SubscriptionFeatureTypeSetup_deleteFeatureHiddenType.Value = e.CommandArgument.ToString();
                    Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatuerTypeModalPopupExtender.Show();
                }
                //else if (e.CommandName == "Edit Feature")
                //{
                //    //Forte_SubscriptionFeatureTypeSetup_AddFeatureSubscriptionType.IsEditMode = true;
                //    SubscriptionFeatureTypes subscriptionFeatureTypes = new SubscriptionFeatureTypes();
                //    subscriptionFeatureTypes.FeatureId = Convert.ToInt32(e.CommandArgument);
                //    subscriptionFeatureTypes.FeatureName = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                //           "Forte_SubscriptionFeatureTypeSetup_featureTypeNameLabel")).Text;
                //    subscriptionFeatureTypes.Unit = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                //           "Forte_SubscriptionFeatureTypeSetup_featureTypeUnitLabel")).Text;
                //    subscriptionFeatureTypes.DefaultValue = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                //           "Forte_SubscriptionFeatureTypeSetup_featureTypeDefaultValueLabel")).Text;
                //    subscriptionFeatureTypes.IsUnlimited = Convert.ToInt16(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                //        "Forte_SubscriptionFeatureTypeSetup_featureTypeIsUnLimitedLabel")).Text);
                //    subscriptionFeatureTypes.Unitname = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                //           "Forte_SubscriptionFeatureTypeSetup_featureTypeUnitNameLabel")).Text;
                //    Forte_SubscriptionFeatureTypeSetup_AddFeatureSubscriptionType.SetEditSubscriptionFeatureTypeDataSource = subscriptionFeatureTypes;
                //    Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionModalPopupExtender.Show();
                //}

            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypeokPopUpClick(object sender, EventArgs e)
        {
            try
            {
                DeleteSubscriptionFeatureType(Convert.ToInt32(Forte_SubscriptionFeatureTypeSetup_deleteFeatureHiddenType.Value));
                Forte_SubscriptionFeatureTypeSetup_deleteFeatureHiddenType.Value = "";
                BindSubscriptionFeaturesTypeGridView();
                ShowSuccessMessage(HCMResource.SubscriptionFeatureType_DeleteSubscriptionFeatureTypeSuccessfull);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_SubscriptionFeatureTypeSetup_deleteSubscriptionFeatureTypecancelPopUpClick(object sender, EventArgs e)
        {
            try
            {
                Forte_SubscriptionFeatureTypeSetup_deleteFeatureHiddenType.Value = "";
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_SubscriptionFeatureTypeSetup_addFeatureLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Forte_SubscriptionFeatureTypeSetup_AddFeatureSubscriptionType.SetAddSubscriptionFeatureTypeDataSource = null;
                Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void FeatureSubscriptionType_SaveClick(object sender, EventArgs e)
        {
            try
            {
                UpdateFeatureSubscriptionType();
                BindSubscriptionFeaturesTypeGridView();
                ShowSuccessMessage(HCMResource.SubscriptionFeatureType_UpdatedSuccessfully);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion

        #endregion Events

        #region Private Methods

        /// <summary>
        /// A Method that updates the subscription feature type by
        /// looping the subscription feature type gridview
        /// </summary>
        private void UpdateFeatureSubscriptionType()
        {
            List<SubscriptionFeatureTypes> subscriptionFeatureTypes = null;
            SubscriptionFeatureTypes subscriptionFeatureType = null;
            try
            {
                for (int i = 0; i < Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows.Count; i++)
                {
                    if (subscriptionFeatureType == null)
                        subscriptionFeatureType = new SubscriptionFeatureTypes();
                    subscriptionFeatureType.FeatureId = Convert.ToInt32(
                        ((Label)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                        FindControl("Forte_SubscriptionFeatureTypeSetup_featureIdHiddenLabel")).Text);
                    subscriptionFeatureType.FeatureName = ((TextBox)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                                     FindControl("Forte_SubscriptionFeatureTypeSetup_featureNameTextBox")).Text.Trim();
                    subscriptionFeatureType.Unit = ((DropDownList)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                                     FindControl("Forte_SubscriptionFeatureTypeSetup_featureUnitValueDropDownList")).SelectedValue;
                    subscriptionFeatureType.DefaultValue = ((TextBox)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                                     FindControl("Forte_SubscriptionFeatureTypeSetup_featureDefaultValueTextBox")).Text.Trim();
                    subscriptionFeatureType.IsUnlimited = ((CheckBox)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                                     FindControl("Forte_SubscriptionFeatureTypeSetup_isUnlimitedCheckBox")).Checked ? Convert.ToInt16(1) : Convert.ToInt16(0);
                    subscriptionFeatureType.ModifiedBy = base.userID;
                    if (subscriptionFeatureTypes == null)
                        subscriptionFeatureTypes = new List<SubscriptionFeatureTypes>();
                    subscriptionFeatureTypes.Add(subscriptionFeatureType);
                    subscriptionFeatureType = null;
                }
                new SubscriptionBLManager().UpdateFeatureSubscriptionType(subscriptionFeatureTypes);
            }
            finally
            {
                if (subscriptionFeatureTypes != null) subscriptionFeatureTypes = null;
                if (subscriptionFeatureType != null) subscriptionFeatureType = null;
            }
        }

        /// <summary>
        /// Deletes the subscription from the repository
        /// </summary>
        /// <param name="FeatureId">
        /// A <see cref="System.Integer"/> that holds the feature id
        /// </param>
        private void DeleteSubscriptionFeatureType(int FeatureId)
        {
            SubscriptionFeatureTypes subscriptionFeatureType = new SubscriptionFeatureTypes();
            subscriptionFeatureType.FeatureId = FeatureId;
            subscriptionFeatureType.DeletedBy = base.userID;
            new SubscriptionBLManager().DeleteFeatureSubscriptionType(subscriptionFeatureType);
        }

        /// <summary>
        /// Deletes the subscription type from the repository
        /// </summary>
        /// <param name="SubscriptionId">
        /// A <see cref="System.Integer"/> that holds the subscription id
        /// </param>
        private void DeleteSubscriptionType(int SubscriptionId)
        {
            SubscriptionTypes subscriptionType = new SubscriptionTypes();
            subscriptionType.SubscriptionId = SubscriptionId;
            subscriptionType.DeletedBy = base.userID;
            new SubscriptionBLManager().DeleteSubscriptionTypes(subscriptionType);
        }

        /// <summary>
        /// Shows the error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            base.ShowMessage(Forte_SubscriptionFeatureTypeSetup_topErrorMessageLabel,
                Forte_SubscriptionFeatureTypeSetup_bottomErrorMessageLabel, exp.Message);
        }

        /// <summary>
        /// Binds the subscription type grid view
        /// </summary>
        private void BindSubscyptionTypeGirdView()
        {
            Forte_SubscriptionFeatureTypeSetup_subscriptionTypeGridview.DataSource = GetSubscriptionTypesList();
            Forte_SubscriptionFeatureTypeSetup_subscriptionTypeGridview.DataBind();
        }

        /// <summary>
        /// Binds the feature type grid view.
        /// </summary>
        private void BindSubscriptionFeaturesTypeGridView()
        {
            Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.DataSource = GetSubscriptionFeatureTypesList();
            Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.DataBind();
            BindFeatureTypeUnitDropDownList();
        }

        /// <summary>
        /// A Method that updates the drop down list in 
        /// the subscription feature type gridview
        /// </summary>
        private void BindFeatureTypeUnitDropDownList()
        {
            List<AttributeDetail> FeatureUnitsList = null;
            DropDownList ddlFeatureUnits = null;
            TextBox DefaultValueTextBox = null;
            CheckBox UnlimitedCheckBox = null;
            try
            {
                FeatureUnitsList = GetFeatureUnitsLists();
                for (int i = 0; i < Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows.Count; i++)
                {
                    ddlFeatureUnits = (DropDownList)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                        FindControl("Forte_SubscriptionFeatureTypeSetup_featureUnitValueDropDownList");
                    ddlFeatureUnits.DataSource = FeatureUnitsList;
                    ddlFeatureUnits.DataTextField = "AttributeName";
                    ddlFeatureUnits.DataValueField = "AttributeID";
                    ddlFeatureUnits.DataBind();
                    ddlFeatureUnits.Items.Insert(0, new ListItem("--Select--", "0"));
                    try
                    {
                        ddlFeatureUnits.SelectedItem.Selected = false;
                        ddlFeatureUnits.Items.FindByValue(((Label)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                            FindControl("Forte_SubscriptionFeatureTypeSetup_featureUnitValue")).Text.Trim()).Selected = true;
                    }
                    catch { }
                    DefaultValueTextBox = (TextBox)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                            FindControl("Forte_SubscriptionFeatureTypeSetup_featureDefaultValueTextBox");
                    UnlimitedCheckBox = (CheckBox)Forte_SubscriptionFeatureTypeSetup_featureTypeGridView.Rows[i].
                                    FindControl("Forte_SubscriptionFeatureTypeSetup_isUnlimitedCheckBox");
                    if (DefaultValueTextBox.Text.ToLower() == "unlimited")
                    {
                        DefaultValueTextBox.Text = "";
                        UnlimitedCheckBox.Checked = true;
                        //DefaultValueTextBox.Attributes.Add("enabled", "false");
                        DefaultValueTextBox.Enabled = false;
                    }
                    UnlimitedCheckBox.Attributes.Add("onclick", "UnlimitedClick('" + DefaultValueTextBox.ClientID + "','" + UnlimitedCheckBox.ClientID + "');");
                    ddlFeatureUnits = null;
                    DefaultValueTextBox = null;
                    UnlimitedCheckBox = null;
                }
            }
            finally
            {
                if (UnlimitedCheckBox != null) UnlimitedCheckBox = null;
                if (DefaultValueTextBox != null) DefaultValueTextBox = null;
                if (FeatureUnitsList != null) FeatureUnitsList = null;
                if (ddlFeatureUnits != null) ddlFeatureUnits = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that communicates with the BL manager to get
        /// the available features in the repository
        /// </summary>
        /// <returns>
        /// A <see cref="Forte.HCM.DataObjects.AttributeDetail"/> that contains
        /// the available feature units
        /// </returns>
        private List<AttributeDetail> GetFeatureUnitsLists()
        {
            return new AttributeBLManager().GetAttributesByType("FEAT_UNIT ");
        }

        /// <summary>
        /// Method that gets the available feature types from the repository
        /// </summary>
        /// <returns>
        /// A <see cref="System.Collections.Generic.List"/> that contains the subscriptionFetuerTypes
        /// </returns>
        private List<SubscriptionFeatureTypes> GetSubscriptionFeatureTypesList()
        {
            return new SubscriptionBLManager().GetAllFeatureTypes().ToList();
        }

        /// <summary>
        /// Method that gets the available subscription types from the repository
        /// </summary>
        /// <returns></returns>
        private List<SubscriptionTypes> GetSubscriptionTypesList()
        {
            return new SubscriptionBLManager().GetAllSubscriptionTypes().ToList();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method that will show subscription pop up extender 
        /// </summary>
        public void ShowSubscriptinTypeModalPopup()
        {
            Forte_SubscriptionFeatureTypeSetup_addNweSubscriptionTypeModalPopupExtender.Show();
        }

        /// <summary>
        /// Method that will show feature pop up extender
        /// </summary>
        public void ShowSubscriptionFeatureTypeModalPopUp()
        {
            Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionModalPopupExtender.Show();
        }

        /// <summary>
        /// Public methods that rebinds subscription gridview
        /// </summary>
        public void RebindSubscriptionTypeGridView()
        {
            BindSubscyptionTypeGirdView();
        }

        /// <summary>
        /// Public method that shows message to the user.
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// </param>
        public void ShowSuccessMessage(string Message)
        {
            base.ShowMessage(Forte_SubscriptionFeatureTypeSetup_topSuccessMessageLabel,
                Forte_SubscriptionFeatureTypeSetup_bottomSuccessMessageLabel, Message);
        }

        /// <summary>
        /// Public methods that rebinds feature gridview
        /// </summary>
        public void RebindSubscriptionFeatureTypeGridView()
        {
            BindSubscriptionFeaturesTypeGridView();
        }

        #endregion Public Methods

        #region Override Methods

        /// <summary>
        /// Override methods 
        /// </summary>
        /// <returns></returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Override method that will be called when page loads
        /// </summary>
        protected override void LoadValues()
        {
            BindSubscyptionTypeGirdView();
            BindSubscriptionFeaturesTypeGridView();
        }

        #endregion Override Methods

        protected void Forte_SubscriptionFeatureTypeSetup_addSubscriptionTypeLinkButton_Click(object sender, EventArgs e)
        {
            try
            {


                Button button =
                        ((Button)Forte_SubscriptionFeatureTypeSetup_addNewSubscriptionType.FindControl
                        ("Forte_AddNewSubscriptionType_subscriptionSaveButton"));

                if (button != null)
                {
                    //Forte_SubscriptionFeatureTypeSetup_addFeatureSubscriptionPanel.DefaultButton = button.UniqueID;
                    button.Focus();
                    
                }

                Forte_SubscriptionFeatureTypeSetup_addNweSubscriptionTypeModalPopupExtender.Show();

            }
            catch (Exception exception)
            {
                ShowErrorMessage(exception);
            }
        }
    }
}