﻿#region Directives

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.Segments;

using Resources;

using AjaxControlToolkit;
#endregion Directives

namespace Forte.HCM.UI.Admin
{    /// <summary>
    /// Represents the class that used to enter the new form 
    /// </summary>
    public partial class PositionProfileFormEntry : PageBase
    {
        #region Event Handler

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                PositionProfileFormEntry_topErrorMessageLabel.Text = string.Empty;
                PositionProfileFormEntry_bottomErrorMessageLabel.Text = string.Empty;
                PositionProfileFormEntry_bottomSuccessMessageLabel.Text = string.Empty;
                PositionProfileFormEntry_topSuccessMessageLabel.Text = string.Empty;

                //Assign the default button for the page
                Page.Form.DefaultButton = PositionProfileFormEntry_topSaveButton.UniqueID;
                Master.SetPageCaption("Create Form");

                if (!IsPostBack)
                {
                    CheckIfFeatureApplicableForSubscription();

                    //On page load clear the segments in session
                    Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = null;

                    //Page.SetFocus(PositionProfileFormEntry_nameTextBox.Text);
                    LoadAvailableSegments();

                    //Assign the default focus
                    Page.Form.DefaultFocus = PositionProfileFormEntry_nameTextBox.UniqueID;

                    PositionProfileFormEntry_nameTextBox.Focus() ;

                    ShowMessage(0);

                    // Open Search User popup by calling javascript funciton
                    PositionProfileFormEntry_designedByIamgeButton.Attributes.Add("onclick",
                        "return LoadUserForPositionProfileModule('"
                        + PositionProfileFormEntry_designedBydummyAuthorIdHidenField.ClientID + "','"
                        + PositionProfileFormEntry_designedByHiddenField.ClientID + "','"
                        + PositionProfileFormEntry_designedByTextBox.ClientID + "')");

                    // Add on click attribute for the top preview button
                    PositionProfileFormEntry_topPreviewButton.Attributes.Add("onclick", "return ShowPreviewForm('"
                       + PositionProfileFormEntry_selectedSegmentsGridView.ClientID + "','" +
                       Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS + "');");

                    //Add on click attribute for the bottom preview button
                    PositionProfileFormEntry_bottomPreviewButton.Attributes.Add("onclick", "return ShowPreviewForm('"
                       + PositionProfileFormEntry_selectedSegmentsGridView.ClientID + "','" +
                       Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS + "');");

                    //Assign the user Id to the designed by hidden field
                    PositionProfileFormEntry_designedByHiddenField.Value = base.userID.ToString();
                    PositionProfileFormEntry_designedByTextBox.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;

                    //If the mode is the edit mode load values for the form ID
                    if (Request.QueryString["mode"] == "edit")
                    {
                        PositionProfileFormEntry_topSaveAsButton.Visible = true;

                        PositionProfileFormEntry_bottomSaveAsButton.Visible = true;

                        PositionProfileFormEntry_topSaveButton.Visible = true;

                        PositionProfileFormEntry_bottomSaveButton.Visible = true;

                        PositionProfileFormEntry_topCreatePositionProfileButton.Visible = true;

                        PositionProfileFormEntry_bottomCreatePositionProfileButton.Visible = true;

                        //Get the form id from the query string
                        int formID = int.Parse(Request.QueryString["form_id"]);

                        ViewState["Form_ID"] = formID;

                        //Load the form deatails by passing form id
                        LoadExistingForm(formID);

                        //Set the header literal as Edit Form
                        PositionProfileFormEntry_headerLiteral.Text = "Edit Form";

                        //Set the page caption as Edit Form
                        Master.SetPageCaption("Edit Form");

                        // Check if the page is redirected from creat form  after creating a
                        // form. If so, show the success message accordingly.
                        if (!Support.Utility.IsNullOrEmpty(Request.QueryString["fromcreate"]) &&
                            Request.QueryString["fromcreate"].ToUpper() == "Y")
                        {
                            base.ShowMessage(PositionProfileFormEntry_topSuccessMessageLabel,
                                PositionProfileFormEntry_bottomSuccessMessageLabel,
                                string.Format(Resources.HCMResource.PositionProfileFormEntry_formCreatedSuccessfully,
                                Request.QueryString["formName"], Request.QueryString["segmentCount"]));
                        }

                    }
                    //if the mode is the create new form load only the segment details
                    else if (Request.QueryString["mode"] == "create")
                    {
                        PositionProfileFormEntry_topSaveAsButton.Visible = false;

                        PositionProfileFormEntry_bottomSaveAsButton.Visible = false;

                        //Get the form id from the query string 
                        int formID = int.Parse(Request.QueryString["form_id"]);

                        ViewState["Form_ID"] = formID;

                        PositionProfileFormEntry_topSaveButton.Visible = true;
                        PositionProfileFormEntry_bottomSaveButton.Visible = true;

                        //Load the form deatails by passing form id
                        LoadExistingForm(formID);

                        //Change the header literal as Create Form
                        PositionProfileFormEntry_headerLiteral.Text = "Create Form";

                        bool isFormCountExceeds = new AdminBLManager().IsFeatureUsageLimitExceeds
                            (base.tenantID, Constants.FeatureConstants.STANDARD_FORMS_FOR_POSITION_PROFILE_GENERATION);

                        if (isFormCountExceeds)
                        {
                            PositionProfileFormEntry_topErrorMessageLabel.Text =
                                 Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;

                            PositionProfileFormEntry_bottomErrorMessageLabel.Text =
                                Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;
                        }
                    }

                    if (Request.QueryString["mode"] == null)
                    {
                        bool isFormCountExceeds = new AdminBLManager().IsFeatureUsageLimitExceeds
                            (base.tenantID, Constants.FeatureConstants.STANDARD_FORMS_FOR_POSITION_PROFILE_GENERATION);

                        if (isFormCountExceeds)
                        {
                            PositionProfileFormEntry_topErrorMessageLabel.Text =
                             Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;

                            PositionProfileFormEntry_bottomErrorMessageLabel.Text =
                                Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;
                        }
                    }

                    PositionProfileFormEntry_globalTD.Visible = false;

                    if (base.isSiteAdmin)
                    {
                        PositionProfileFormEntry_globalTD.Visible = true;
                    }
                }

                string selectedID = PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value;

                if (selectedID.Length != 0)
                {
                    //if the post back value is 1 and if there is id in the select id hidden field 
                    // move the segments to the selected panel
                    if (PositionProfileFormEntry_postbackHiddenField.Value == "1")
                    {
                        MoveSelectedSegments();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                    PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        private void CheckIfFeatureApplicableForSubscription()
        {
            if (Session["USER_DETAIL"] != null)
            {
                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                bool value = new CommonBLManager().CheckFeatureApplicableOrNot(userDetail.SubscriptionId, Constants.FeatureConstants.STANDARD_FORMS_FOR_POSITION_PROFILE_GENERATION);

                if (value)
                {
                    Response.Redirect("~/Common/FeatureDenied.aspx?", false);
                }
            }
            //PositionProfileFeatureID
        }

        /// <summary>
        /// Handles the RowDataBound event of the PositionProfileFormEntry_availableSegmentsGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_availableSegmentsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                CheckBox checkBox = (CheckBox)e.Row.FindControl
                    ("PositionProfileFormEntry_availableSegmentsGridView_moveCheckBox");

                HiddenField segmentIDHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileFormEntry_availableSegmentsGridView_segmentIDHiddenField");

                Label segmentLabel = (Label)e.Row.FindControl
                    ("PositionProfileFormEntry_availableSegmentsGridView_segmentNameLabel");

                HiddenField activeHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileFormEntry_availableSegmentsGridView_activeHiddenField");

                Image moveImage = (Image)e.Row.FindControl
                    ("PositionProfileFormEntry_availableSegmentsGridView_moveImageButton");

                Image previewImage = (Image)e.Row.FindControl
                    ("PositionProfileFormEntry_availableSegmentsGridView_viewSegmentImageButton");

                // Check if segment is active.
                if (activeHiddenField != null && activeHiddenField.Value != null &&
                    activeHiddenField.Value.Trim().ToUpper() == "TRUE")
                {
                    moveImage.Attributes.Add("onmousedown",
                        "javascript:return mousedown('" +
                        e.Row.RowIndex.ToString() + "','" +
                        PositionProfileFormEntry_availableSegmentsGridView.ClientID + "');");

                    moveImage.Attributes.Add("onclick",
                        "javascript:return singleqnsclick('"
                        + segmentIDHiddenField.Value + "');");

                    if (checkBox != null)
                        checkBox.Attributes.Add("onclick", "javascript:return AddIdToHiddenField('"
                            + segmentIDHiddenField.Value + "','" + checkBox.ClientID + "');");
                }
                else
                {
                    // Hide the icons.
                    moveImage.Visible = false;
                    previewImage.Visible = false;

                    // Disable the check box.
                    checkBox.Enabled = false;

                    // Set the disabled color.
                    segmentLabel.CssClass = "segment_label_disabled";
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handles the RowCommand event of the 
        /// PositionProfileFormEntry_availableSegmentsGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_availableSegmentsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "PreviewControl")
                    LoadSegments(e.CommandArgument.ToString());
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the 
        /// PositionProfileFormEntry_selectedSegmentsMoveRightImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_selectedSegmentsMoveRightImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                MoveSelectedSegments();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the 
        /// PositionProfileFormEntry_selectedSegmentsMoveLeftImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void PositionProfileFormEntry_selectedSegmentsMoveLeftImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                // If the selected segments is 0 display a warning to the user
                if (PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.Value.ToString().Length == 0)
                {
                    base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                        PositionProfileFormEntry_bottomErrorMessageLabel,
                        Resources.HCMResource.PositionProfileFormEntry_NoSegmentsSelected);

                    return;
                }

                // If the selected segment is not null then show a warning to the user
                string alertMessage = string.Format(Resources.HCMResource.PositionProfileFormEntry_DeleteConfirmation,
                    PositionProfileFormEntry_selectedSegmentsSelectedNamesHiddenField.Value.TrimEnd(','));
                PositionProfileFormEntry_existingSegmentsPopupPanel.Style.Add("height", "270px");
                PositionProfileFormEntry_ConfirmMsgControl.Type = MessageBoxType.YesNo;
                PositionProfileFormEntry_ConfirmMsgControl.Title = "Warning";
                PositionProfileFormEntry_ConfirmMsgControl.Message = alertMessage;
                PositionProfileFormEntry_ConfirmPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the 
        /// PositionProfileFormEntry_selectedSegmentsGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_selectedSegmentsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    return;
                }

                CheckBox checkBox = (CheckBox)e.Row.FindControl
                    ("PositionProfileFormEntry_selectedSegmentsGridView_moveCheckBox");

                HiddenField hiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileFormEntry_selectedSegmentsGridView_segmentIDHiddenField");

                ImageButton deleteImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileFormEntry_selectedSegmentsGridView_moveImageButton");

                ImageButton dowmImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileFormEntry_selectedSegmentsGridView_moveDownSegmentImageButton");

                ImageButton upImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileFormEntry_selectedSegmentsGridView_moveUpImageButton");

                HiddenField displayOrderHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileFormEntry_selectedSegmentsGridView_displayOrderHiddenField");

                Label segmentNameLabel = (Label)e.Row.FindControl
                    ("PositionProfileFormEntry_selectedSegmentsGridView_segmentNameLabel");

                if (checkBox != null)
                {
                    checkBox.Attributes.Add("onclick",
                        "javascript:return AddIdToSelectedHiddenField('"
                        + hiddenField.Value + "','" + checkBox.ClientID + "','"
                        + segmentNameLabel.Text + "');");
                }

                if (e.Row.RowIndex == 0)
                {
                    upImageButton.Visible = false;
                }

                if (hiddenField.Value == "0")
                {
                    dowmImageButton.Visible = false;

                    checkBox.Visible = false;

                    upImageButton.Visible = false;

                    deleteImageButton.Visible = false;

                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void PositionProfileFormEntry_ConfirmMsgControl_okClick(object sender, EventArgs e)
        {
            try
            {
                if (PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.Value.ToString().Length == 0)
                    return;

                //Get the selected id array from the selected Id hidden field
                string[] separatedIDs = PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.Value.Split(',');

                //Select the distinct ids 
                List<string> selectedIDs = separatedIDs.Distinct().ToList();

                if (ViewState["SelectedSegments"] == null)
                    return;

                List<Segment> segments = ViewState["SelectedSegments"] as List<Segment>;

                //Remove the selected id from the list
                foreach (string id in selectedIDs)
                {
                    segments.RemoveAll(delegate(Segment segment)
                    {
                        return segment.SegmentID.ToString() == id;
                    });
                }

                //Assign  the datasource for the selected segments gridview
                PositionProfileFormEntry_selectedSegmentsGridView.DataSource = segments;
                PositionProfileFormEntry_selectedSegmentsGridView.DataBind();

                //Hide the last down arrow for the record
                HideLastRecord();

                ViewState["SelectedSegments"] = segments;

                Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = segments;

                if (segments == null || segments.Count == 0)
                {
                    LoadEmptyGrid();
                }

                PositionProfileFormEntry_selectedSegmentsSelectedNamesHiddenField.Value = string.Empty;
                PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.Value = string.Empty;

                ShowMessage(segments.Count);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void PositionProfileFormEntry_ConfirmMsgControl_cancelClick(object sender, EventArgs e)
        {
            try
            {
                PositionProfileFormEntry_selectedSegmentsSelectedNamesHiddenField.Value = string.Empty;
                PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the 
        /// PositionProfileFormEntry_selectedSegmentsGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_selectedSegmentsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                List<Segment> Segments = ViewState["SelectedSegments"] as List<Segment>;

                int order = int.Parse(e.CommandArgument.ToString());

                //if the command name is move down , move the record one line down 
                if (e.CommandName == "MoveDown")
                {

                    foreach (Segment segment in Segments)
                    {
                        //Change the display order of the segment
                        if (segment.DisplayOrder == order)
                        {
                            segment.DisplayOrder = order + 1;
                            continue;
                        }
                        if (segment.DisplayOrder == order + 1)
                        {
                            segment.DisplayOrder = order;
                            continue;
                        }
                    }
                }

                //if the command name is moveup, move the record one line up
                if (e.CommandName == "MoveUp")
                {
                    foreach (Segment segment in Segments)
                    {
                        if (segment.DisplayOrder == order - 1)
                        {
                            segment.DisplayOrder = order;
                            continue;
                        }
                        if (segment.DisplayOrder == order)
                        {
                            segment.DisplayOrder = order - 1;
                            continue;
                        }
                    }
                }

                //if the command name is delete segment, delete the segment
                if (e.CommandName == "DeleteSegment")
                {
                    ImageButton sourceButton = (ImageButton)e.CommandSource;

                    GridViewRow row = (GridViewRow)sourceButton.Parent.Parent;

                    Label nameLable = (Label)row.FindControl("PositionProfileFormEntry_selectedSegmentsGridView_segmentNameLabel");
                    PositionProfileFormEntry_deleteSegmentHiddenField.Value = e.CommandArgument.ToString();

                    PositionProfileFormEntry_deleteSegmentConfirmMsgControl.Message = string.Format
                        (Resources.HCMResource.PositionProfileFormEntry_DeleteConfirmation, nameLable.Text);

                    PositionProfileFormEntry_deleteSegmentPopupPanel.Style.Add("height", "200px");
                    PositionProfileFormEntry_deleteSegmentConfirmMsgControl.Type = MessageBoxType.YesNo;
                    PositionProfileFormEntry_deleteSegmentConfirmMsgControl.Title = "Warning";

                    PositionProfileFormEntry_deleteSegmentModalPopupExtender.Show();
                }


                PositionProfileFormEntry_selectedSegmentsGridView.DataSource = Segments.OrderBy(p => p.DisplayOrder);
                PositionProfileFormEntry_selectedSegmentsGridView.DataBind();

                // Hide the last down arrow button in the grid
                HideLastRecord();

                ViewState["SelectedSegments"] = Segments;

                //Save the segments in the session
                Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = Segments;

                ShowMessage(Segments.Count);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void PositionProfileFormEntry_DeleteSegmentConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                if (ViewState["SelectedSegments"] == null)
                    return;

                if (PositionProfileFormEntry_deleteSegmentHiddenField.Value.ToString() == string.Empty)
                    return;

                int selectedID = int.Parse(PositionProfileFormEntry_deleteSegmentHiddenField.Value);

                List<Segment> segments = ViewState["SelectedSegments"] as List<Segment>;

                segments.RemoveAll(delegate(Segment segment)
                  {
                      return segment.SegmentID.ToString() == selectedID.ToString();
                  });

                ArrangeByDisplayOrder(segments);

                PositionProfileFormEntry_selectedSegmentsGridView.DataSource = segments;
                PositionProfileFormEntry_selectedSegmentsGridView.DataBind();

                HideLastRecord();

                PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.Value = string.Empty;

                ViewState["SelectedSegments"] = segments;

                Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = segments;

                if (segments != null && segments.Count == 0)
                {
                    LoadEmptyGrid();
                }
                ShowMessage(segments.Count);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void PositionProfileFormEntry_DeleteSegmentConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                PositionProfileFormEntry_deleteSegmentHiddenField.Value = string.Empty;
                PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.Value = string.Empty;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                  PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                InsertOrUpdateForm();

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_topResetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileFormEntry_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]) &&
                    Request.QueryString["mode"] == "edit")
                {
                    if (!Support.Utility.IsNullOrEmpty(Request.QueryString["parentpage"]) &&
                        Request.QueryString["parentpage"] == Constants.ParentPage.SEARCH_POSITION_PROFILE_FORM)
                    {
                        Response.Redirect("~/Admin/PositionProfileFormEntry.aspx?m=2&s=1" +
                       "&mode=edit&form_id=" + Request.QueryString["form_id"] + "&parentpage="
                       + Constants.ParentPage.SEARCH_POSITION_PROFILE_FORM, false);
                    }
                    else
                    {
                        Response.Redirect("~/Admin/PositionProfileFormEntry.aspx?m=2&s=0" +
                            "&mode=edit&form_id=" + Request.QueryString["form_id"], false);
                    }
                }
                else if (!Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]) &&
                    Request.QueryString["mode"] == "create")
                {
                    Response.Redirect("~/Admin/PositionProfileFormEntry.aspx?m=2&s=1&mode=create&form_id=" +
                        Request.QueryString["form_id"] + "&parentpage="
                       + Constants.ParentPage.SEARCH_POSITION_PROFILE_FORM, false);
                }
                else
                {
                    Response.Redirect("~/Admin/PositionProfileFormEntry.aspx?m=2&s=0", false);
                }
            }

            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_previewButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileFormEntry_previewButton_Click(object sender, EventArgs e)
        {
            if (Support.Utility.IsNullOrEmpty(ViewState["SelectedSegments"])
                || ((List<Segment>)ViewState["SelectedSegments"]).Count == 0)
            {
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
            PositionProfileFormEntry_bottomErrorMessageLabel, "Please select atleast one segment");

                return;
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_createPositionProfileButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileFormEntry_createPositionProfileButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0&form_id=" +
                      ViewState["Form_ID"].ToString().Trim() + "&parentpage=" + Constants.ParentPage.EDIT_FORM, false);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_topSaveAsButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileFormEntry_topSaveAsButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;

                FormDetail formDetail = InsertNewForm();

                if (formDetail.FormID == 0)
                {
                    PositionProfileFormEntry_topErrorMessageLabel.Text = string.Format
                        (Resources.HCMResource.PositionProfileFormEntry_FormAlreadyExist, formDetail.FormName);

                    PositionProfileFormEntry_bottomErrorMessageLabel.Text = string.Format
                        (Resources.HCMResource.PositionProfileFormEntry_FormAlreadyExist, formDetail.FormName);

                    return;
                }

                if (formDetail.FormID == -1)
                {
                    PositionProfileFormEntry_topErrorMessageLabel.Text =
                       Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;

                    PositionProfileFormEntry_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;

                    return;
                }

                PositionProfileFormEntry_topSuccessMessageLabel.Text = string.Format
                    (Resources.HCMResource.PositionProfileFormEntry_formCreatedSuccessfully,
                    formDetail.FormName, formDetail.segmentList.Count);

                ViewState["Form_ID"] = formDetail.FormID;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                    PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the 
        /// PositionProfileFormEntry_selectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_selectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gridviewRow in PositionProfileFormEntry_availableSegmentsGridView.Rows)
                {
                    CheckBox checkbox = (CheckBox)gridviewRow.FindControl("PositionProfileFormEntry_availableSegmentsGridView_moveCheckBox");
                    HiddenField idHiddenField = (HiddenField)gridviewRow.FindControl("PositionProfileFormEntry_availableSegmentsGridView_segmentIDHiddenField");


                    if (checkbox == null || idHiddenField == null)
                        return;

                    // Do not process non-active segments.
                    if (checkbox.Enabled == false)
                        return;

                    checkbox.Checked = true;

                    if (gridviewRow.RowIndex == 0)
                    {
                        PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value = idHiddenField.Value;
                    }
                    else
                    {
                        PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value = PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value + "," + idHiddenField.Value;
                    }
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                    PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }

        }

        /// <summary>
        /// Handles the Click event of the 
        /// PositionProfileFormEntry_unSelectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_unSelectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                UncheckAvailableSegments();
                PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                    PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the ActiveTabChanged event of the PositionProfileEntry_tabContainer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileFormEntry_tabContainer_ActiveTabChanged(object sender, EventArgs e)
        {
            try
            {
                if (Support.Utility.IsNullOrEmpty(ViewState["SelectedSegments"]))
                    return;

                ControlVisibility();
                if (PositionProfileFormEntry_tabContainer.ActiveTab != null)
                {
                    ControlVisibility(int.Parse(PositionProfileFormEntry_tabContainer.ActiveTab.ID.Remove(0, 3)));

                    PositionProfileFormEntry_previewFormPopupExtender.Show();
                }


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
                    PositionProfileFormEntry_bottomErrorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handler

        #region Private Methods

        /// <summary>
        /// Updates the form detail.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        private void UpdateFormDetail(int formID)
        {
            List<Segment> Segments = ViewState["SelectedSegments"] as List<Segment>;

            FormDetail formDetail = new FormDetail();

            formDetail.FormName = PositionProfileFormEntry_nameTextBox.Text.Trim();

            formDetail.DesignedBy = int.Parse(PositionProfileFormEntry_designedByHiddenField.Value);

            formDetail.Tags = PositionProfileFormEntry_tagTextBox.Text.Trim();

            formDetail.IsGlobalForm = PositionProfileFormEntry_isGlobalCheckBox.Checked;

            formDetail.ModifiedBy = base.userID;

            formDetail.CreatedBy = base.userID;

            formDetail.FormID = formID;

            formDetail.segmentList = Segments;

            int returnFormID = new AdminBLManager().UpdateFormDetail(formDetail, base.tenantID);

            if (returnFormID == 1)
            {
                base.ShowMessage(PositionProfileFormEntry_topSuccessMessageLabel,
                    PositionProfileFormEntry_bottomSuccessMessageLabel,
                    Resources.HCMResource.PositionProfileFormEntry_UpdateSuccessfully);

                ViewState["Form_ID"] = formID;
            }
            else
            {
                PositionProfileFormEntry_topErrorMessageLabel.Text = string.Format
                    (Resources.HCMResource.PositionProfileFormEntry_FormAlreadyExist, formDetail.FormName);
                PositionProfileFormEntry_bottomErrorMessageLabel.Text = string.Format
                    (Resources.HCMResource.PositionProfileFormEntry_FormAlreadyExist, formDetail.FormName);
            }
        }

        /// <summary>
        /// Controls the visibility.
        /// </summary>
        private void ControlVisibility()
        {
            PositionProfileFormEntry_verticalBackgroundRequirementControl.Visible = false;
            PositionProfileFormEntry_technicalSkillRequirementControl.Visible = false;
            PositionProfileFormEntry_roleRequirementControl.Visible = false;
            PositionProfileFormEntry_educationRequirementControl.Visible = false;
            PositionProfileFormEntry_clientPositionDetailsControl.Visible = false;
        }

        /// <summary>
        /// Controls the visibility.
        /// </summary>
        /// <param name="segmentId">The segment id.</param>
        private void ControlVisibility(int segmentId)
        {
            switch (segmentId)
            {
                case (int)SegmentType.ClientPositionDetail:
                    PositionProfileFormEntry_clientPositionDetailsControl.Visible = true;
                    break;
                case (int)SegmentType.CommunicationSkills:
                    break;
                case (int)SegmentType.CulturalFit:
                    break;
                case (int)SegmentType.DecisionMaking:
                    break;
                case (int)SegmentType.Education:
                    PositionProfileFormEntry_educationRequirementControl.Visible = true;
                    break;
                case (int)SegmentType.InterpersonalSkills:
                    break;
                case (int)SegmentType.Roles:
                    PositionProfileFormEntry_roleRequirementControl.Visible = true;
                    break;
                case (int)SegmentType.Screening:
                    break;
                case (int)SegmentType.TechnicalSkills:
                    PositionProfileFormEntry_technicalSkillRequirementControl.Visible = true;
                    break;
                case (int)SegmentType.Verticals:
                    PositionProfileFormEntry_verticalBackgroundRequirementControl.Visible = true;
                    break;
            }
        }

        /// <summary>
        /// Inserts the or update form.
        /// </summary>
        private void InsertOrUpdateForm()
        {
            if (!IsValidData())
                return;
            List<Segment> Segments = ViewState["SelectedSegments"] as List<Segment>;

            //Check if the form is in update mode
            if (Request.QueryString["mode"] != null && Request.QueryString["mode"] == "edit")
            {
                //Update form detail and return the content
                UpdateForm();
                return;
            }


            //if the form is not in edit mode insert new form
            FormDetail formDetail = InsertNewForm();

            if (formDetail.FormID == 0)
            {
                PositionProfileFormEntry_topErrorMessageLabel.Text = string.Format
                    (Resources.HCMResource.PositionProfileFormEntry_FormAlreadyExist, formDetail.FormName);

                PositionProfileFormEntry_bottomErrorMessageLabel.Text = string.Format
                    (Resources.HCMResource.PositionProfileFormEntry_FormAlreadyExist, formDetail.FormName);

                return;
            }

            if (formDetail.FormID == -1)
            {
                PositionProfileFormEntry_topErrorMessageLabel.Text =
                   Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;

                PositionProfileFormEntry_bottomErrorMessageLabel.Text =
                    Resources.HCMResource.PositionProfileFormEntry_FormCountExceedsLimit;

                return;
            }

            //Save the form Id in the view state
            ViewState["Form_ID"] = formDetail.FormID;

            Response.Redirect("~/Admin/PositionProfileFormEntry.aspx?m=2&s=0" +
                         "&mode=edit&form_id=" + formDetail.FormID + "&fromcreate=y&formName="
                         + formDetail.FormName + "&segmentCount=" + formDetail.segmentList.Count,
                          false);
        }

        /// <summary>
        /// Inserts the new form.
        /// </summary>
        /// <returns></returns>
        private FormDetail InsertNewForm()
        {
            List<Segment> Segments = ViewState["SelectedSegments"] as List<Segment>;

            FormDetail formDetail = new FormDetail();

            formDetail.FormName = PositionProfileFormEntry_nameTextBox.Text.Trim();

            formDetail.DesignedBy = int.Parse(PositionProfileFormEntry_designedByHiddenField.Value);

            formDetail.Tags = PositionProfileFormEntry_tagTextBox.Text.Trim();

            formDetail.CreatedBy = base.userID;

            formDetail.segmentList = Segments;

            formDetail.IsGlobalForm = PositionProfileFormEntry_isGlobalCheckBox.Checked;

            int formID = new AdminBLManager().InsertNewForm(formDetail, base.tenantID);

            formDetail.FormID = formID;

            return formDetail;
        }

        /// <summary>
        /// Loads the existing segments.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        private void LoadExistingForm(int formID)
        {
            FormDetail form = new FormDetail();
            //List<Segment> segments = new List<Segment>();

            form = new AdminBLManager().GetExistingSegments(formID);

            PositionProfileFormEntry_nameTextBox.Text = form.FormName;

            PositionProfileFormEntry_designedByTextBox.Text = form.UserFirstName;

            PositionProfileFormEntry_designedByHiddenField.Value = form.DesignedBy.ToString();

            PositionProfileFormEntry_isGlobalCheckBox.Checked = form.IsGlobalForm;

            PositionProfileFormEntry_tagTextBox.Text = form.Tags;

            form.segmentList = form.segmentList.OrderBy(p => p.DisplayOrder).ToList();

            PositionProfileFormEntry_selectedSegmentsGridView.DataSource = form.segmentList;
            PositionProfileFormEntry_selectedSegmentsGridView.DataBind();

            ViewState["SelectedSegments"] = form.segmentList;
            Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = form.segmentList;

            //Hide last row move down record
            HideLastRecord();

            PositionProfileFormEntry_updateFormHiddenField.Value = formID.ToString();

            ShowMessage(form.segmentList.Count);
        }

        /// <summary>
        /// Arranges the by display order.
        /// </summary>
        /// <param name="segments">The segments.</param>
        private void ArrangeByDisplayOrder(List<Segment> segments)
        {
            int displayOrder = 1;

            foreach (Segment seg in segments)
            {
                seg.DisplayOrder = displayOrder;

                displayOrder = displayOrder + 1;
            }
        }

        /// <summary>
        /// Hides the last record.
        /// </summary>
        private void HideLastRecord()
        {
            foreach (GridViewRow row in PositionProfileFormEntry_selectedSegmentsGridView.Rows)
            {
                if (row.RowIndex == PositionProfileFormEntry_selectedSegmentsGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("PositionProfileFormEntry_selectedSegmentsGridView_moveDownSegmentImageButton");

                    dowmImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Unchecks the available segments.
        /// </summary>
        private void UncheckAvailableSegments()
        {
            foreach (GridViewRow row in PositionProfileFormEntry_availableSegmentsGridView.Rows)
            {
                CheckBox checkBox = (CheckBox)row.FindControl
                    ("PositionProfileFormEntry_availableSegmentsGridView_moveCheckBox");

                if (checkBox.Checked)
                {
                    checkBox.Checked = false;
                }

            }
        }

        /// <summary>
        /// Loads the available segments.
        /// </summary>
        private void LoadAvailableSegments()
        {
            List<Segment> segments = new AdminBLManager().GetAvailableSegments();

            PositionProfileFormEntry_availableSegmentsGridView.DataSource = segments;

            PositionProfileFormEntry_availableSegmentsGridView.DataBind();

            if (segments != null)
            {
                ViewState["AvailableSegments"] = segments;
            }

            PositionProfileFormEntry_segmentsAvailableLiteral.Text = segments.Count + " Segments Available";

            ShowMessage(segments.Count);

            LoadEmptyGrid();
        }

        /// <summary>
        /// Moves the selected segments.
        /// </summary>
        private void MoveSelectedSegments()
        {
            //Select the list of item checked
            if (PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value == "")
            {

                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel, PositionProfileFormEntry_bottomErrorMessageLabel,
                     Resources.HCMResource.PositionProfileFormEntry_NoSegmentsSelectedInAvailableSegments);
                return;
            }

            string existingID = string.Empty;
            string existingIDName = string.Empty;
            string[] separatedIDs = PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value.Split(',');


            //Select the distinct ids 
            separatedIDs = separatedIDs.Distinct().ToArray();

            //Select the list of item checked
            if (separatedIDs.Length == 1 && separatedIDs[0] == "")
            {
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel, PositionProfileFormEntry_bottomErrorMessageLabel,
                Resources.HCMResource.PositionProfileFormEntry_NoSegmentsSelectedInAvailableSegments);

                return;
            }

            List<string> separedIdList = separatedIDs.ToList();

            List<Segment> selectedSegments = new List<Segment>();

            Segment Segment;

            if (ViewState["AvailableSegments"] != null)
            {
                List<Segment> Segments = ViewState["AvailableSegments"] as List<Segment>;

                if (ViewState["SelectedSegments"] != null)
                {
                    selectedSegments = ViewState["SelectedSegments"] as List<Segment>;

                    var existingdIds = from c in separatedIDs
                                       join p in selectedSegments on c equals p.SegmentID.ToString()
                                       select new { segment = c, p.SegmentID, p.SegmentName };


                    foreach (var id in existingdIds)
                    {
                        separedIdList.Remove(id.SegmentID.ToString());
                        existingID = existingID + id.SegmentID + ",";
                        existingIDName = existingIDName + id.SegmentName + ",";
                    }

                    separatedIDs = separedIdList.ToArray();
                }

                var selectedId = from c in separatedIDs
                                 join p in Segments on c equals p.SegmentID.ToString()
                                 select new { category = c, p.SegmentID, p.SegmentName };

                int displayOrder = 1;

                foreach (var segment in selectedId)
                {
                    Segment = new Segment();

                    Segment.SegmentID = segment.SegmentID;

                    Segment.SegmentName = segment.SegmentName;

                    Segment.DisplayOrder = displayOrder;

                    selectedSegments.Add(Segment);

                    displayOrder = displayOrder + 1;
                }

                ArrangeByDisplayOrder(selectedSegments);

                PositionProfileFormEntry_selectedSegmentsGridView.DataSource = selectedSegments;
                PositionProfileFormEntry_selectedSegmentsGridView.DataBind();

                if (selectedSegments == null || selectedSegments.Count == 0)
                {
                    LoadEmptyGrid();
                }


                if (existingID.Length != 0)
                {
                    existingID = existingID.TrimEnd(',');
                    existingIDName = existingIDName.TrimEnd(',');

                    string alertMessage = string.Format(Resources.HCMResource.PositionProfileFormEntry_TheSegmentAlreadyExist, existingIDName);


                    PositionProfileFormEntry_existingSegmentsPopupPanel.Style.Add("height", "270px");
                    PositionProfileFormEntry_ConfirmMsgControl.Type = MessageBoxType.Ok;
                    PositionProfileFormEntry_ConfirmMsgControl.Title = "Warning";
                    PositionProfileFormEntry_ConfirmMsgControl.Message = alertMessage;
                    PositionProfileFormEntry_ConfirmPopupExtender.Show();
                }


                if (selectedSegments != null)
                {
                    ViewState["SelectedSegments"] = selectedSegments;

                    Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = ViewState["SelectedSegments"];
                }

                PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.Value = string.Empty;

                PositionProfileFormEntry_postbackHiddenField.Value = "0";

                HideLastRecord();

                ShowMessage(selectedSegments.Count);

                UncheckAvailableSegments();
            }

        }

        /// <summary>
        /// Loads the segments.
        /// </summary>
        /// <param name="segmentID">The segment ID.</param>
        private void LoadSegments(string segmentID)
        {
            string segmentPath = new AdminBLManager().GetSegmentPath(segmentID);

            Control segmentControl = this.LoadControl("../" + segmentPath);

            if ((segmentControl.TemplateControl).AppRelativeVirtualPath.Contains("TechnicalSkillRequirementControl"))
            {
                TechnicalSkillRequirement tech = (TechnicalSkillRequirement)segmentControl;

                tech.DefaultTechnicalSkill();
            }

            if ((segmentControl.TemplateControl).AppRelativeVirtualPath.Contains("VerticalBackgroundRequirementControl"))
            {
                Segments.VerticalBackgroundRequirement vertical = (Segments.VerticalBackgroundRequirement)segmentControl;

                vertical.DefaultGridValue();
            }

            PositionProfileFormEntry_controlsPlaceHolder.Controls.Add(segmentControl);

            PositionProfileFormEntry_placeHolderModalPopupExtender.Show();

        }

        /// <summary>
        /// Loads the empty grid.
        /// </summary>
        private void LoadEmptyGrid()
        {
            List<Segment> nullSegments = new List<Segment>();

            Segment nullSegment = new Segment();

            nullSegment.SegmentName = "";

            nullSegments.Add(nullSegment);

            PositionProfileFormEntry_selectedSegmentsGridView.DataSource = nullSegments;

            PositionProfileFormEntry_selectedSegmentsGridView.DataBind();
        }

        /// <summary>
        /// Shows the selected segments message.
        /// </summary>
        /// <param name="count">The count.</param>
        private void ShowMessage(int count)
        {
            PositionProfileFormEntry_selectedLiteral.Text = count + " Segment(s) Selected";
        }

        /// <summary>
        /// Updates the form.
        /// </summary>
        private void UpdateForm()
        {
            if (Request.QueryString["form_id"] != null)
            {
                int formID = int.Parse(Request.QueryString["form_id"]);

                if (!IsValidData())
                    return;

                UpdateFormDetail(formID);

            }
        }

        #endregion

        #region Protected Overridden Methods

        protected override bool IsValidData()
        {
            if (PositionProfileFormEntry_nameTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
          PositionProfileFormEntry_bottomErrorMessageLabel, Resources.HCMResource.
          PositionProfileFormEntry_PleaseEnterFormName);

                return false;
            }
            List<Segment> Segments = ViewState["SelectedSegments"] as List<Segment>;

            if (Segments == null || Segments.Count == 0)
            {
                base.ShowMessage(PositionProfileFormEntry_topErrorMessageLabel,
            PositionProfileFormEntry_bottomErrorMessageLabel, Resources.HCMResource.
            PositionProfileFormEntry_PleaseSelectOneSegment);

                return false;
            }

            return true;
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}