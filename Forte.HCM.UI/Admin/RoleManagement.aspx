﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteAdminMaster.Master" AutoEventWireup="true"
    CodeBehind="RoleManagement.aspx.cs" Inherits="Forte.HCM.UI.Admin.RoleManagement" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="RoleManagement_Content" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script language="javascript" type="text/javascript">

        function textboxMultilineMaxNumber(txt) {
            if (txt.value.length > 251)
                return false;
        }

        function disableEnterKey(e) {
            var key;
            if (window.event)
                key = window.event.keyCode;     //IE          
            else
                key = e.which;     //firefox          
            if (key == 13)
                return false;
            else
                return true;
        } 

    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                    <asp:Literal ID="RoleManagement_headerLiteral" runat="server" Text="Role Management"></asp:Literal>
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="RoleManagement_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="RoleManagement_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="RoleManagement_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="RoleManagement_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="RoleManagement_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="TAB_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr id="RoleManagement_expandAllTR" runat="server">
                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                        <asp:Label ID="RoleManagement_searchResultsHeaderHelpLabel" runat="server" Text="Role Categories"> </asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="RoleManagement_searchResultsDownarrowSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="RoleManagement_searchResultsDownArrow" runat="server" SkinID="sknMinimizeImage" />
                                                                            <asp:HiddenField ID="RoleManagement_restoreHiddenField" runat="server" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <div id="RoleManagement_rolecategorySearchDIV" runat="server">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <div id="RoleManagement_rolecategoryGridViewDIV" style="height: 100px; display: block;
                                                                                overflow: auto;" runat="server" onkeypress="javascript:return disableEnterKey(event);">
                                                                                <asp:GridView ID="RoleManagement_rolecategoryGridView" runat="server" AllowSorting="True"
                                                                                    AutoGenerateColumns="False" OnRowCancelingEdit="RoleManagement_rolecategoryGridView_RowCancelingEdit"
                                                                                    OnRowEditing="RoleManagement_rolecategoryGridView_RowEditing" OnRowCommand="RoleManagement_rolecategoryGridView_RowCommand"
                                                                                    OnRowUpdating="RoleManagement_rolecategoryGridView_RowUpdating" OnRowCreated="RoleManagement_rolecategoryGridView_RowCreated"
                                                                                    OnSorting="RoleManagement_rolecategoryGridView_Sorting">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderStyle-Width="7%" ItemStyle-Width="7%">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="RoleManagement_rolecategoryGridView_editImageButton" runat="server"
                                                                                                    SkinID="sknEditVectorImageButton" ToolTip="Edit Role Category" CommandArgument='<%# Eval("RoleID") %>'
                                                                                                    CommandName="Edit" />
                                                                                                <asp:ImageButton ID="RoleManagement_rolecategoryGridView_UpdateImageButton" runat="server"
                                                                                                    SkinID="sknEditSaveVectorImageButton" ToolTip="Update Role Category" ValidationGroup="b"
                                                                                                    CommandArgument='<%# Eval("RoleID") %>' CommandName="Update" Visible="false" />
                                                                                                <asp:ImageButton ID="RoleManagement_rolecategoryGridView_deleteImageButton" runat="server"
                                                                                                    SkinID="sknDeleteImageButton" ToolTip="Delete Role Category" CommandArgument='<%# Eval("RoleID") %>'
                                                                                                    CommandName="DeleteRoleName" />
                                                                                                <asp:ImageButton ID="RoleManagement_rolecategoryGridView_cancelUpdateImageButton"
                                                                                                    runat="server" SkinID="sknDeleteVectorImageButton" ToolTip="Cancel Update" CommandArgument='<%# Eval("RoleID") %>'
                                                                                                    CommandName="Cancel" Visible="false" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Role Category Code" HeaderStyle-Width="47%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="37%" SortExpression="ROLECODE">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="RoleManagement_rolecategoryGridView_rolecategotycodeLabel" runat="server"
                                                                                                    Text='<%# Eval("RoleCategoryCode") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:HiddenField ID="RoleManagement_rolecategoryIDHiddenField" runat="server" Value='<%# Eval("RoleID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:HiddenField ID="RoleManagement_editrolecategoryIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("RoleID") %>' />
                                                                                                <asp:TextBox ID="RoleManagement_businessTypesGridView_editcodeTextBox" runat="server"
                                                                                                    Width="30%" AutoCompleteType="None" Text='<%# Eval("RoleCategoryCode") %>' MaxLength="10"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="RoleManagement_roleCategorynameRequiredFieldValidator"
                                                                                                    runat="server" ControlToValidate="RoleManagement_businessTypesGridView_editcodeTextBox"
                                                                                                    ErrorMessage="Enter category code" ValidationGroup="b" Display="None" SetFocusOnError="true"
                                                                                                    ForeColor="White">
                                                                                                </asp:RequiredFieldValidator>
                                                                                                <ajaxToolKit:ValidatorCalloutExtender ID="RoleManagement_roleCategorynameValidatorCalloutExtender"
                                                                                                    runat="server" TargetControlID="RoleManagement_roleCategorynameRequiredFieldValidator"
                                                                                                    PopupPosition="Right" CssClass="customCalloutStyle">
                                                                                                </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                <asp:Label ID="RoleManagement_roleCATNAMEHiddenLabel" runat="server" Visible="false"
                                                                                                    Text='<%# Eval("RoleCategoryCode") %>'></asp:Label>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Role Category Name" HeaderStyle-Width="46%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="30%" SortExpression="ROLENAME">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="RoleManagement_rolecategoryGridView_rolecategotynameLabel" runat="server"
                                                                                                    Text='<%# Eval("RoleName") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:HiddenField ID="RoleManagement_namerolecategoryIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("RoleID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:HiddenField ID="RoleManagement_editrolecategorynameIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("RoleID") %>' />
                                                                                                <asp:TextBox ID="RoleManagement_businessTypesGridView_editNameTextBox" runat="server"
                                                                                                    Width="36%" AutoCompleteType="None" Text='<%# Eval("RoleName") %>' MaxLength="50"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="RoleManagement_roleCategorycodeRequiredFieldValidator"
                                                                                                    runat="server" ControlToValidate="RoleManagement_businessTypesGridView_editNameTextBox"
                                                                                                    ErrorMessage="Enter category name" ValidationGroup="b" Display="None" SetFocusOnError="true"
                                                                                                    ForeColor="White">
                                                                                                </asp:RequiredFieldValidator>
                                                                                                <ajaxToolKit:ValidatorCalloutExtender ID="RoleManagement_roleCategorycodeValidatorCalloutExtender"
                                                                                                    runat="server" TargetControlID="RoleManagement_roleCategorycodeRequiredFieldValidator"
                                                                                                    PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                <asp:Label ID="RoleManagement_roleCATcodeHiddenLabel" runat="server" Visible="false"
                                                                                                    Text='<%# Eval("RoleName") %>'></asp:Label>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="RoleManagement_rolecategoryAddImageButton" runat="server" SkinID="sknAddLinkButton"
                                                                                Text="Add" OnClick="RoleManagement_rolecategoryAddImageButton_Click">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td align="right">
                                                                            <input type="hidden" runat="server" id="RoleManagement_categoryTypePageNumberHidden"
                                                                                value="1" />
                                                                            <uc1:PageNavigator ID="RoleManagement_rolecategoryPageNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 15px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="pnl" runat="server" DefaultButton="RoleManagement_Save">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="grid_header_bg">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td style="width: 50%" class="header_text_bold">
                                                                                                    <asp:Literal ID="RoleManagement_rolesLiteral" runat="server" Text="Roles"></asp:Literal>
                                                                                                </td>
                                                                                                <td style="width: 50%" align="right">
                                                                                                    <asp:LinkButton ID="RoleManagement_showhide" runat="server" OnClick="RoleManagement_showhideClick"
                                                                                                        SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                                    <span id="RoleManagement_DownarrowSpan" style="display: none;" runat="server">
                                                                                                        <asp:Image ID="RoleManagement_DownArrow" runat="server" SkinID="sknMinimizeImage" />
                                                                                                        <asp:HiddenField ID="CRoleManagement_restoreHiddenField" runat="server" />
                                                                                                    </span>
                                                                                                </td>
                                                                                                <td style="width: 50%">
                                                                                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Button ID="RoleManagement_Save" runat="server" Text="Save" SkinID="sknButtonId"
                                                                                                                    OnClick="RoleManagement_SaveClick" ValidationGroup="X" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="grid_body_bg">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div id="RoleManagement_rolesSearchDIV" style="overflow: auto;">
                                                                                            <input type="hidden" id="RoleManagement_deleteRoleHiddenType" runat="server" />
                                                                                            <asp:GridView ID="RoleManagement_rolesGridView" runat="server" AutoGenerateColumns="false"
                                                                                                SkinID="sknNewGridView" Width="100%" OnRowCommand="RoleManagement_roleGridView_RowCommand">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField>
                                                                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="RoleManagement_rolesGridView_deleteImageButton" runat="server"
                                                                                                                ToolTip="Delete Role" SkinID="sknDeleteImageButton" CommandName="DeleteRoleName"
                                                                                                                CommandArgument='<%# Eval("RoleId") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField>
                                                                                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="7%" />
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Image ID="RoleManagement_RoleImage" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/role_icon_hcm.png" />
                                                                                                            <asp:Label ID="RoleManagement_rowNumberHeaderLabel" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                                                            <asp:Label ID="RoleManagement_rowNumberDotHeaderLabel" runat="server" Text="."></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField>
                                                                                                        <HeaderTemplate>
                                                                                                            <div style="float: left; width: 100%;">
                                                                                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                                                    <tr>
                                                                                                                        <td style="height: 5px;">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 30%" align="left">
                                                                                                                            Role Category <span class="mandatory">*</span>
                                                                                                                        </td>
                                                                                                                        <td align="left" style="width: 30%">
                                                                                                                            Role Code <span class="mandatory">*</span>
                                                                                                                        </td>
                                                                                                                        <td align="left" style="width: 30%">
                                                                                                                            Role Name <span class="mandatory">*</span>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemStyle Width="93%" />
                                                                                                        <ItemTemplate>
                                                                                                            <div style="float: left; width: 100%;">
                                                                                                                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                                                    <tr>
                                                                                                                        <td style="height: 5px;">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td align="center" style="height: 40px; width: 100%;">
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 30%" align="left">
                                                                                                                                        <asp:DropDownList ID="RoleManagement_updateRoleCategoryListBox" runat="server" Width="60%">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                        <asp:HiddenField ID="RoleManagment_roleCodeNameHiddenFiled" runat="server" Value='<%# Eval("RoleCodeName") %>' />
                                                                                                                                        <asp:Label ID="RoleManagment_roleCodeName" runat="server" Text='<%# Eval("RoleCodeName") %>'
                                                                                                                                            SkinID="sknLabelFieldText" Visible="false"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td align="left" style="width: 30%">
                                                                                                                                        <asp:TextBox ID="RoleManagement_roleGridView_rolecodeTextBox" runat="server" Width="50%"
                                                                                                                                            Text='<%# Eval("RoleCode") %>' MaxLength="10"></asp:TextBox>
                                                                                                                                        <asp:RequiredFieldValidator ID="RoleManagement_rolecodeRequiredFieldValidator" runat="server"
                                                                                                                                            ControlToValidate="RoleManagement_roleGridView_rolecodeTextBox" ErrorMessage="Enter role code"
                                                                                                                                            ValidationGroup="X" Display="None" SetFocusOnError="true">
                                                                                                                                        </asp:RequiredFieldValidator>
                                                                                                                                        <ajaxToolKit:ValidatorCalloutExtender ID="RoleManagement_roleGridView_roleCodeValidatorCalloutExtender"
                                                                                                                                            runat="server" TargetControlID="RoleManagement_rolecodeRequiredFieldValidator"
                                                                                                                                            PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                                                        </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                                                        <asp:Label ID="RoleManagement_rolecodeHiddenLabel" runat="server" Visible="false"
                                                                                                                                            Text='<%# Eval("RoleCode") %>'></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td align="left" style="width: 30%">
                                                                                                                                        <asp:TextBox ID="RoleManagement_roleGridView_rolenameTextBox" runat="server" Text='<%# Eval("RolesName") %>'
                                                                                                                                            Width="95%" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:RequiredFieldValidator ID="RoleManagement_rolenameRequiredFieldValidator" runat="server"
                                                                                                                                            ControlToValidate="RoleManagement_roleGridView_rolenameTextBox" ErrorMessage="Enter role name"
                                                                                                                                            ValidationGroup="X" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                                                                        <ajaxToolKit:ValidatorCalloutExtender ID="RoleManagement_roleGridView_rolenameValidatorCalloutExtender"
                                                                                                                                            runat="server" TargetControlID="RoleManagement_rolenameRequiredFieldValidator"
                                                                                                                                            PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                                                        </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                                                        <asp:Label ID="RoleManagement_rolenameHiddenLabel" runat="server" Visible="false"
                                                                                                                                            Text='<%# Eval("RolesName") %>'></asp:Label>
                                                                                                                                        <asp:HiddenField ID="RoleManagement_roleidHiddenLabel" runat="server" Value='<%# Eval("RoleID") %>' />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="td_height_5">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td colspan="3" align="left">
                                                                                                                                        <asp:TextBox ID="RoleManagement_roleGridView_roleJobDescriptionTextBox" runat="server"
                                                                                                                                            Text='<%# Eval("RolesJobDesc") %>' Width="850px" MaxLength="250" Wrap="true"
                                                                                                                                            TextMode="MultiLine" Height="50" onkeypress="javascript:return textboxMultilineMaxNumber(this);"></asp:TextBox>
                                                                                                                                        <asp:RequiredFieldValidator ID="RoleManagement_roleJobDescriptionRequiredFieldValidator"
                                                                                                                                            runat="server" ControlToValidate="RoleManagement_roleGridView_roleJobDescriptionTextBox"
                                                                                                                                            ErrorMessage="Enter job description" ValidationGroup="X" Display="None" SetFocusOnError="true">
                                                                                                                                        </asp:RequiredFieldValidator>
                                                                                                                                        <ajaxToolKit:ValidatorCalloutExtender ID="RoleManagement_roleGridView_roleJobDescriptionValidatorCalloutExtender"
                                                                                                                                            runat="server" TargetControlID="RoleManagement_roleJobDescriptionRequiredFieldValidator"
                                                                                                                                            PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                                                        </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                                                        <asp:Label ID="Label1" runat="server" Visible="false" Text='<%# Eval("RoleId") %>'></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="td_height_5">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_10">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="RoleManagement_rolesAddImageButton" runat="server" SkinID="sknAddLinkButton"
                                                                                            Text="Add" OnClick="RoleManagement_rolesAddImageButton_Click"> </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_10">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" class="header_text_bold">
                                                &nbsp;
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="RoleManagement_reset_LinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="RoleManagement_resetLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="RoleManagement_cancel_LinkButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="RoleManagement_bottomSuccessLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="RoleManagement_bottomErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="display: none">
                            <asp:Button ID="RoleManagement_deleteBusinessHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="RoleManagement_deleteBusinessPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="RoleManagement_deleteBusinessConfirmMsgControl" runat="server"
                                OnOkClick="RoleManagemen_deleteBusinessConfirmMsgControl_okClick" OnCancelClick="RoleManagemen_deleteBusinessConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="RoleManagement_deleteHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="RoleManagement_deleteFormModalPopupExtender"
                            runat="server" PopupControlID="RoleManagement_deleteBusinessPopupPanel" TargetControlID="RoleManagement_deleteBusinessHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="RoleManagement_updateBusinessPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_roleCategory" DefaultButton="RoleManagement_updateBusinessNameSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="RoleManagement_updateBusinesshiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="RoleManagement_updateBusinessLiteral" runat="server" Text="Edit Business Type"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="RoleManagement_updateBusinessTopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="RoleManagement_updateBusinessNameErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="RoleManagement_updateRoleCategoryCodeLabel" runat="server" Text="Role Category Code"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="RoleManagement_updateRoleCategoryCodeTextBox" runat="server" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="RoleManagement_updateRoleCategoryNameLabel" runat="server" Text="Role Category Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="RoleManagement_updateRoleCategoryNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:HiddenField ID="RoleManagement_updateBusinessIsEditORSaveHiddenField" runat="server" />
                                                    <asp:Button ID="RoleManagement_updateBusinessNameSaveButton" runat="server" SkinID="sknButtonId"
                                                        Text="Save" OnClick="RoleManagement_updateCategoryRoleCodeNameSaveButton_Clicked" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="RoleManagement_updateBusinessNameCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" OnClick="RoleManagement_updateRoleCategoryCloseLinkButton_Clicked" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="RoleManagement_updateRoleCategoryNameModalPopupExtender"
                            runat="server" PopupControlID="RoleManagement_updateBusinessPanel" TargetControlID="RoleManagement_updateBusinesshiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="display: none">
                            <asp:Button ID="RoleManagement_deleteRolesHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="RoleManagement_deleteRolesPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="RoleManagement_deleteRolesConfirmMsgControl" OnOkClick="RoleManagemen_deleteRolesConfirmMsgControl_okClick"
                                OnCancelClick="RoleManagemen_deleteRolesConfirmMsgControl_cancelClick" runat="server" />
                            <asp:HiddenField ID="RoleManagement_deleteRoleHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="RoleManagement_deleteRoleFormModalPopupExtender"
                            runat="server" PopupControlID="RoleManagement_deleteRolesPopupPanel" TargetControlID="RoleManagement_deleteRolesHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="RoleManagement_updateRolesPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_role" DefaultButton="RoleManagement_updateRoleSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="RoleManagement_updateRoleshiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="RoleManagement_updateRoleLiteral" runat="server" Text="Edit Business Type"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="RoleManagement_updateRolesTopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="2">
                                                                <asp:Label ID="RoleManagement_updateRoleCategoryNameErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="RoleManagement_roleCategory_Label" runat="server" Text="Role Category"
                                                                                SkinID="RoleManagement_updateRoleCategoryLabel"></asp:Label>
                                                                            <span class='mandatory'>*</span>
                                                                        </td>
                                                                        <td style="width: 80%">
                                                                            <asp:DropDownList ID="RoleManagement_updateRoleCategoryListBox" runat="server" Width="570px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="RoleManagement_updateRoleCodeLabel" runat="server" Text="Role Code"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        <span class='mandatory'>*</span>
                                                                                    </td>
                                                                                    <td style="width: 37%">
                                                                                        <asp:TextBox ID="RoleManagement_updateRoleCodeTextBox" runat="server" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 13%">
                                                                                        <asp:Label ID="RoleManagement_updateRoleNameLabel" runat="server" Text="Role Name"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        <span class='mandatory'>*</span>
                                                                                    </td>
                                                                                    <td style="width: 37%">
                                                                                        <asp:TextBox ID="RoleManagement_updateRoleNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="99%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="RoleManagement_updateRoleJobDescriptionLabel" runat="server" Text="Role Job Description"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class='mandatory'>*</span>
                                                                        </td>
                                                                        <td style="width: 79%">
                                                                            <asp:TextBox ID="RoleManagement_updateRoleJobDescriptionTextbox" runat="server" Width="570px"
                                                                                Wrap="true" MaxLength="250" TextMode="MultiLine" Height="35px" onkeypress="javascript:return textboxMultilineMaxNumber(this);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:HiddenField ID="RoleManagement_updateRoleIsEditORSaveHiddenField" runat="server" />
                                                    <asp:Button ID="RoleManagement_updateRoleSaveButton" OnClick="RoleManagement_updateRoleSaveButton_Clicked"
                                                        runat="server" SkinID="sknButtonId" Text="Save" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="RoleManagement_updateRoleCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="RoleManagement_updateRoleModalPopupExtender"
                            runat="server" PopupControlID="RoleManagement_updateRolesPanel" TargetControlID="RoleManagement_updateRoleshiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
