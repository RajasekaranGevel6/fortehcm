﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpgradeAccount.aspx.cs" MasterPageFile="~/MasterPages/CustomerAdminMaster.Master"
    Inherits="Forte.HCM.UI.Admin.UpgradeAccount" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<asp:Content ID="UpgradeAccount_bodyContent" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <asp:UpdatePanel ID="Forte_UpgradeAccount_UpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td>
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="header_bg" align="right">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" class="header_text_bold">
                                                <asp:Literal ID="Forte_UpgradeAccount_UpgradeAccountHeaderLiteral" runat="server"
                                                    Text="Upgrade Account"></asp:Literal>
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="Forte_Updgrade_topAcceptButton" runat="server" Text="Accept" SkinID="sknButtonId"
                                                                OnClick="UpgradeAccount_AcceptButtonClick" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_UpgradeAccount_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                Text="Reset" OnClick="UpdgradeAccount_ResetButtonClick" />
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_UpgradeAccount_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                Text="Cancel" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_UpgradeAccount_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                            EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_UpgradeAccount_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                            EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Literal ID="Forte_UpgradeAccount_accountDetailsHeaderLiteral" runat="server"
                                                    Text="Account Details"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_subscriptionTypeHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Subscription Type"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20px;">
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_subscriptionTypeLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        <input type="hidden" runat="server" id="Forte_UpgradeAccount_subscriptionIdHidden" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_lastLoginHeaderLabel" runat="server" Text="Last Login"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_lastLoginLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_statusHeadLabel" runat="server" Text="Status"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_statusLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_subscribedOnHeadLabel" runat="server" Text="Subscribed On"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_subscribedOnLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_activatedOnHeadLabel" runat="server" Text="Activated On"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_activatedOnLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_noOfUsersHeadLabel" runat="server" Text="No Of Users"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_noOfUsersLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_titleHeadLabel" runat="server" Text="Title" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_titleLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_companyHeadLable" runat="server" Text="Company"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_companyLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_phoneHeadLabel" runat="server" Text="Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_phoneLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_firstNameHeadLabel" runat="server" Text="First Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_firstNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_lastNameHeadLabel" runat="server" Text="Last Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_lastNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_isTrialHeadLabel" runat="server" Text="Trial"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_UpgradeAccount_isTrialLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg" align="center">
                                                <asp:UpdatePanel ID="CreateTestSession_maxMinButtonUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                    <asp:Literal ID="Forte_CorporateUserManangement_usersResultsLiteral" runat="server"
                                                                        Text="Feature & Pricing"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <div id="Forte_UpgradeAccount_featuresAndPricingDiv" style="overflow: auto; height: 200px;">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="Forte_UpgradeAccount_featureAndPricingGridView" runat="server"
                                                                            AutoGenerateColumns="false" SkinID="sknSubsciptionAccountsGridView" Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Name">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_UpgradeAccount_featureNameLabel" runat="server" Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Pricing">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_UpgradeAccount_firstNameLabel" runat="server" Text='<%# Eval("FeatureValue") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Label ID="Forte_UpgradeAccount_rulesHeadLabel" runat="server" Text="Rules" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table style="float: left;">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Label ID="Forte_UpgradeAccount_rule1Label" runat="server" SkinID="sknLabelFieldText"
                                                                Text="1. Your current account will be deactivated"></asp:Label><br />
                                                            <asp:Label ID="Forte_UpgradeAccount_rule2Label" runat="server" SkinID="sknLabelFieldText"
                                                                Text="2. No carry over of balance information to the upgraded one"></asp:Label><br />
                                                            <asp:Label ID="Forte_UpgradeAccount_rule3Label" runat="server" SkinID="sknLabelFieldText"
                                                                Text="3. Application data can be carried to the upgraded one"></asp:Label><br />
                                                            <asp:Label ID="Forte_UpgradeAccount_rule4Label" runat="server" SkinID="sknLabelFieldText"
                                                                Text="4. By clicking the accept you are abide to the rules and regulations"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_UpgradeAccount_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                            EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_UpgradeAccount_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                            EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="right">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%">
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="Forte_UpgradeAccount_bottomAcceptButton" runat="server" Text="Accept"
                                                                SkinID="sknButtonId" OnClick="UpgradeAccount_AcceptButtonClick" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_UpgradeAccount_bottomResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                Text="Reset" OnClick="UpdgradeAccount_ResetButtonClick" />
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_UpgradeAccount_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                Text="Cancel" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
