﻿
#region Namespace                                                              

using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using System.Web.UI.HtmlControls;

#endregion Namespace

namespace Forte.HCM.UI.Admin
{
    public partial class SubscriptionFeaturePricingSetup : PageBase
    {

        #region Events                                                         

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Forte_SubscriptionFeaturePricingSetup_bottomPageNavigator.PageNumberClick += new CommonControls.PageNavigator.PageNumberClickEventHandler(Forte_SubscriptionFeaturePricingSetup_bottomPageNavigator_PageNumberClick);
                Master.SetPageCaption(HCMResource.SubscriptionPricing_Title);
                Forte_SubscriptionFeaturePricingSetup_pricingAddFeatureSubscriptionType.Created_By = base.userID;
                if (IsPostBack)
                    return;
                LoadValues();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetLinkButtonclick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                if (Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedIndex == 0)
                    BindEmptyGridView();
                else
                {
                    SetPublishStatus(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(',') + 1));
                    BindSelectedFeaturesGrid(Convert.ToInt32(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))));
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingconfirmPopupExtenderControlokPopUpClick
            (object sender, EventArgs e)
        {
            try
            {
                UpdateNotApplicableStatus(Convert.ToInt32(Forte_SubscriptionFeaturePricing_deleteSubsriptionFeatureIdHidden.Value));
                RebindGridView();
                Forte_SubscriptionFeaturePricing_deleteSubsriptionFeatureIdHidden.Value = "";
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingconfirmPopupExtenderControlcancelPopUpClick
            (object sender, EventArgs e)
        {
            try
            {
                Forte_SubscriptionFeaturePricing_deleteSubsriptionFeatureIdHidden.Value = "";
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void Forte_SubscriptionFeaturePricingSetup_featureTypeGridView_RowCommand
        //    (object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Delete Pricing")
        //        {
        //            Forte_SubscriptionFeaturePricing_deleteSubsriptionFeatureIdHidden.Value = e.CommandArgument.ToString();
        //            Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingModalPopupExtender.Show();
        //        }
        //        else if (e.CommandName == "Edit Pricing")
        //        {
        //            SubscriptionFeatures subscriptionFeatures = new SubscriptionFeatures();
        //            subscriptionFeatures.SubscriptionFeatureId = Convert.ToInt32(e.CommandArgument);
        //            subscriptionFeatures.FeatureName = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
        //                   "Forte_SubscriptionFeaturePricingSetup_featureTypeNameLabel")).Text;
        //            subscriptionFeatures.IsNotApplicable = Convert.ToInt16(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
        //                   "Forte_SubscriptionFeaturePricingSetup_isNotAppliationLabel")).Text);
        //            subscriptionFeatures.IsUnlimited = Convert.ToInt16(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
        //                   "Forte_SubscriptionFeaturePricingSetup_isUnlimitedLabel")).Text);
        //            subscriptionFeatures.FeatureValue = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
        //                   "Forte_SubscriptionFeaturePricingSetup_featureValueLabel")).Text;
        //            subscriptionFeatures.FeatureUnit = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
        //                   "Forte_SubscriptionFeaturePricingSetup_featureUnitValue")).Text;
        //            subscriptionFeatures.DefaultValue = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
        //                   "Forte_SubscriptionFeaturePricingSetup_defaultValueLable")).Text;
        //            Forte_SubscriptionFeaturePricingSetup_pricingAddFeatureSubscriptionType.SetPricingSubscriptionFeaturesDataSource = subscriptionFeatures;
        //            Forte_SubscriptionFeaturePricingSetup_pricingFeatureSubscriptionModalPopupExtender.Show();
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        ShowErrorMessage(exp);
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Forte_SubscriptionFeaturePricingSetup_SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                UpdateSubscription();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Forte_SubscriptionFeaturePricingSetup_PublishButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControl.BindSubscriptionPricingGridView = 
                    GenerateDataSourceForPreviewControl();
                Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControlModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviewControlPublishClick(object sender, EventArgs e)
        {
            try
            {
                UpdatePublishStatusAndRebindDropDown("1");
                base.ShowMessage(Forte_SubscriptionFeaturePricingSetup_topSuccessMessageLabel,
                    Forte_SubscriptionFeaturePricingSetup_bottomSuccessMessageLabel, HCMResource.SubscriptionFeaturePricing_PublishSuccessfull);
                BindSelectedFeaturesGrid(Convert.ToInt32(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))));
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void PreviewControlCancelClick(object sender, EventArgs e)
        {
            try
            {
                BindSelectedFeaturesGrid(Convert.ToInt32(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))));
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void RePublishConfirmCancelClick(object sender, EventArgs e)
        {
            try
            {
                BindSelectedFeaturesGrid(Convert.ToInt32(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))));
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        /// <summary>
        /// A Method that updates the feature value for a particular subscription
        /// </summary>
        private void UpdateSubscription()
        {
            List<SubscriptionFeatures> subscriptionFeatures = null;
            SubscriptionFeatures subscriptionFeature = null;
            try
            {
                for (int Count = 0; Count < Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows.Count; Count++)
                {
                    if (subscriptionFeature == null)
                        subscriptionFeature = new SubscriptionFeatures();
                    subscriptionFeature.SubscriptionFeatureId = Convert.ToInt32(
                        ((Label)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_featureIdHiddenLabel")).Text);
                    subscriptionFeature.FeatureValue = ((TextBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_featureDefaultValueTextBox")).Text;
                    subscriptionFeature.IsNotApplicable = Convert.ToInt16(
                        ((CheckBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_isNotApplicableCheckBox")).Checked ? 1 : 0);
                    subscriptionFeature.IsUnlimited = Convert.ToInt16(
                        ((CheckBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_isUnlimitedCheckBox")).Checked ? 1 : 0);
                    subscriptionFeature.ModifiedBy = base.tenantID;
                    if (subscriptionFeatures == null)
                        subscriptionFeatures = new List<SubscriptionFeatures>();
                    subscriptionFeatures.Add(subscriptionFeature);
                    subscriptionFeature = null;
                }
                new SubscriptionBLManager().UpdateSubscriptionFeaturePricingListWithTransaction(subscriptionFeatures);
                CheckAndInvokeRePublishModalPopUp();
                ShowSuccessMessage(HCMResource.SubscriptionFeatureType_UpdatedSuccessfully);
            }
            finally
            {
                if (subscriptionFeatures != null) subscriptionFeatures = null;
                if (subscriptionFeature != null) subscriptionFeature = null;
                InvokeGarbageCollector();
            }
        }

        /// <summary>
        /// This method loops through the gridview and binds the
        /// feature unit drop down list and subscribes the client side
        /// events
        /// </summary>
        private void BindAndLoadUnitTypeDropdownList()
        {
            CheckBox UnlimitedCheckBox = null;
            CheckBox NotApplicableCheckBox = null;
            int intNotApplicable = 0;
            int intUnlimited = 0;
            TextBox DefaultValueTextbox = null;
            HtmlInputHidden DefaultValueHidden = null;
            try
            {
                for (int Count = 0; Count < Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows.Count; Count++)
                {
                    UnlimitedCheckBox = (CheckBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_isUnlimitedCheckBox");
                    NotApplicableCheckBox = (CheckBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_isNotApplicableCheckBox");
                    DefaultValueHidden = (HtmlInputHidden)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_defaultValueHidden");
                    DefaultValueTextbox = (TextBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_featureDefaultValueTextBox");
                    intNotApplicable = Convert.ToInt32(((Label)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_isNotAppliationLabel")).Text);
                    intUnlimited = Convert.ToInt32(((Label)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_isUnlimitedLabel")).Text);
                    DefaultValueTextbox.Text = intNotApplicable == 1 || intUnlimited == 1 ? "" : DefaultValueTextbox.Text;
                    NotApplicableCheckBox.Checked = intNotApplicable == 1 ? true : false;
                    UnlimitedCheckBox.Checked = intUnlimited == 1 ? true : false;
                    if (NotApplicableCheckBox.Checked || UnlimitedCheckBox.Checked)
                        DefaultValueTextbox.Enabled = false;
                        //DefaultValueTextbox.Attributes.Add("readonly", "true");
                    UnlimitedCheckBox.Attributes.Add("onclick", "Unlimited('" +
                    NotApplicableCheckBox.ClientID + "','" +
                    UnlimitedCheckBox.ClientID + "','" +
                    DefaultValueTextbox.ClientID + "');");
                    NotApplicableCheckBox.Attributes.Add("onclick", "NotApplicableClick('" +
                        NotApplicableCheckBox.ClientID + "','" +
                        UnlimitedCheckBox.ClientID + "','" +
                        DefaultValueTextbox.ClientID + "');");
                    ((LinkButton)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[Count].
                        FindControl("Forte_SubscriptionFeaturePricingSetup_restoreDefaultValueLinkButton")).Attributes.Add("onclick", "return DeafultValue('" +
                        DefaultValueHidden.ClientID + "','" +
                        DefaultValueTextbox.ClientID + "','" +
                        UnlimitedCheckBox.ClientID + "','" +
                        NotApplicableCheckBox.ClientID + "');");
                    UnlimitedCheckBox = null;
                    NotApplicableCheckBox = null;
                    DefaultValueTextbox = null;
                    DefaultValueHidden = null;
                }
            }
            finally
            {
                if (UnlimitedCheckBox != null) UnlimitedCheckBox = null;
                if (NotApplicableCheckBox != null) NotApplicableCheckBox = null;
                if (DefaultValueTextbox != null) DefaultValueTextbox = null;
                if (DefaultValueHidden != null) DefaultValueHidden = null;
                InvokeGarbageCollector();
            }
        }

        /// <summary>
        /// A Method that invokes the garbage collector to free
        /// the objects
        /// </summary>
        /// <remarks>
        /// This method will not raise any exception while invoking Garbage collector
        /// </remarks>
        private void InvokeGarbageCollector()
        {
            try
            {
                GC.Collect(2, GCCollectionMode.Forced);
            }
            catch
            {
                try
                {
                    GC.Collect();
                }
                catch { }
            }
        }

        /// <summary>
        /// A Method that updates the publish status and rebinds the 
        /// subscription drop down
        /// </summary>
        /// <param name="PublishStatus">
        /// A <see cref="System.String"/> that holds the publish status
        /// of the subscription
        /// </param>
        private void UpdatePublishStatusAndRebindDropDown(string PublishStatus)
        {
            UpdatePublishStatus(Convert.ToInt32(
                    Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                    Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))), PublishStatus);
            int PreviousSelectedIndex = Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedIndex;
            BindSubscriptionDropDownList();
            Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedIndex = PreviousSelectedIndex;
            SetPublishStatus(
                    Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(
                    Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(',') + 1));
        }

        /// <summary>
        /// Update the publish satus in to the database
        /// </summary>
        /// <param name="SubscriptionId">
        /// A <see cref="System.Int32"/> that holds the subscription id
        /// </param>
        /// <param name="PublishStatus">
        /// A <see cref="System.String"/> that holds the publish status
        /// of the subscription
        /// </param>
        private void UpdatePublishStatus(int SubscriptionId, string PublishStatus)
        {
            SubscriptionTypes subscriptionTypes = new SubscriptionTypes();
            subscriptionTypes.SubscriptionId = SubscriptionId;
            subscriptionTypes.Published = PublishStatus;
            new SubscriptionBLManager().UpdateSubscriptionPublishStatus(subscriptionTypes);
        }

        /// <summary>
        /// A Method that generates the datasource of the subscription
        /// </summary>
        /// <returns>
        /// A List of subscription features
        /// </returns>
        private List<SubscriptionFeatures> GenerateDataSourceForPreviewControl()
        {
            int Temp=0;
            var List = GetSelectedSubscriptionFeatures(Convert.ToInt32(
                    Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                    Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))), out Temp, null, 1);
            var ReturnList = System.Linq.Enumerable.Where(List, p => p.IsNotApplicable == 0);
            return ReturnList.ToList();
        }

        /// <summary>
        /// Updates the not applicable status for a subscription
        /// feature
        /// </summary>
        /// <param name="Subscription_Feature_id">
        /// A <see cref="System.Int32"/> that holds the subscription feature id
        /// </param>
        private void UpdateNotApplicableStatus(int Subscription_Feature_id)
        {
            SubscriptionFeatures subscriptionFeatures = new SubscriptionFeatures();
            subscriptionFeatures.SubscriptionFeatureId = Subscription_Feature_id;
            subscriptionFeatures.IsNotApplicable = 1;
            subscriptionFeatures.IsUnlimited = 0;
            subscriptionFeatures.FeatureValue = "";
            subscriptionFeatures.ModifiedBy = base.userID;
            new SubscriptionBLManager().UpdateSubscriptionFeaturePricing(subscriptionFeatures);
            ShowSuccessMessage(HCMResource.SubscriptionPricing_PricingDeletedSuccessfully);
        }

        /// <summary>
        /// Method that binds the grid view according to the selected
        /// subscription id
        /// </summary>
        /// <param name="SubscriptionId">
        /// A <see cref="System.Int32"/> that holds user selected 
        /// subscription id
        /// </param>
        /// <param name="Total_Records">
        private void BindSelectedFeaturesGrid(int SubscriptionId)
        {
            int Total_Records = 0;
            Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.DataSource =
                GetSelectedSubscriptionFeatures(SubscriptionId, out Total_Records, null, 1);
            Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.DataBind();
            BindAndLoadUnitTypeDropdownList();
        }

        /// <summary>
        /// Method that get the subscription features list according 
        /// to the selected subscription id
        /// </summary>
        /// <param name="SubscriptionId">
        /// A <see cref="System.Int32"/> that holds user selected 
        /// subscription id
        /// </param>
        /// <param name="Total_Records">
        /// A <see cref="System.Int32"/> out parameter that holds the total number of records
        /// in the repository
        /// </param>
        /// <param name="PageSize">
        /// A <see cref="System.Int32"/> that holds the page size of the grid
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="System.Int32"/> that holds the page number of the grid
        /// </param>
        /// <returns></returns>
        private List<SubscriptionFeatures> GetSelectedSubscriptionFeatures(int SubscriptionId, out int Total_Records, int? PageSize, int PageNumber)
        {
            return new SubscriptionBLManager().GetSelectedSubscriptionFeatures(SubscriptionId, out Total_Records, PageSize, PageNumber).ToList();
        }

        /// <summary>
        /// Sets the publish status label, image and buttons
        /// </summary>
        /// <param name="Status">
        /// A <see cref="System.String"/> that holds the status 
        /// of the subscription
        /// </param>
        private void SetPublishStatus(string Status)
        {
            Forte_SubscriptionFeaturePricingSetup_publishStatusImage.Visible = true;
            switch (Status)
            {
                case "Yes":
                    Forte_SubscriptionFeaturePricingSetup_bottomPublishButton.Visible = false;
                    Forte_SubscriptionFeaturePricingSetup_topPublishButton.Visible = false;
                    Forte_SubscriptionFeaturePricingSetup_publishStatusLabel.Text = "Published";
                    Forte_SubscriptionFeaturePricingSetup_publishStatusImage.ImageUrl = "~/App_Themes/DefaultTheme/Images/SubscriptionPublished.gif";
                    break;
                default:
                    Forte_SubscriptionFeaturePricingSetup_bottomPublishButton.Visible = true;
                    Forte_SubscriptionFeaturePricingSetup_topPublishButton.Visible = true;
                    Forte_SubscriptionFeaturePricingSetup_publishStatusLabel.Text = "Not-Published";
                    Forte_SubscriptionFeaturePricingSetup_publishStatusImage.ImageUrl = "~/App_Themes/DefaultTheme/Images/SubscriptionNotPublished.gif";
                    break;
            }
        }

        /// <summary>
        /// Binds the empty gridview
        /// </summary>
        private void BindEmptyGridView()
        {
            Forte_SubscriptionFeaturePricingSetup_publishStatusLabel.Text = "";
            Forte_SubscriptionFeaturePricingSetup_publishStatusImage.Visible = false;
            Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.DataSource = null;
            Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.DataBind();
        }

        /// <summary>
        /// Binds the available subscriptions to the drop down list
        /// </summary>
        private void BindSubscriptionDropDownList()
        {
            Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.DataSource = GetAvailableSubscriptionsList(); ;
            Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.DataValueField = "ConcatenatedSubscriptionIdPublishStatus";
            Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.DataTextField = "SubscriptionName";
            Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.DataBind();
            Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// Gets the available subscriptions in the repository
        /// </summary>
        /// <returns>
        /// A List of subscriptions available in the repository
        /// </returns>
        private List<SubscriptionTypes> GetAvailableSubscriptionsList()
        {
            return new SubscriptionBLManager().GetAllSubscriptionTypes().ToList();
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message to be 
        /// show to the user.
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            base.ShowMessage(Forte_SubscriptionFeaturePricingSetup_topErrorMessageLabel,
                Forte_SubscriptionFeaturePricingSetup_bottomErrorMessageLabel, Message);
        }

        /// <summary>
        /// Show error message
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        #endregion Private Methods

        #region Public Methods                                                 

        /// <summary>
        /// Check's the publish status.
        /// If published then change to re-publish and invokes the
        /// modal popup extender
        /// </summary>
        public void CheckAndInvokeRePublishModalPopUp()
        {
            string PublishStatus = Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(',') + 1);
            Forte_SubscriptionFeaturePricingSetup_rePublishSubscriptionPricingConfirmMsgControl.Message = HCMResource.SubscriptionPricing_RePublishConfirmMessage;
            if(PublishStatus.ToLower().Contains("yes"))
                UpdatePublishStatusAndRebindDropDown("2");
            PublishStatus = Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(',') + 1);
            if (PublishStatus.ToLower().Contains("repub"))
                Forte_SubscriptionFeaturePricingSetup_rePublishSubscriptionPricingModalPopupExtender.Show();
            else
                BindSelectedFeaturesGrid(Convert.ToInt32(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))));
        }

        /// <summary>
        /// Show success message
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message to
        /// the user.
        /// </param>
        public void ShowSuccessMessage(string Message)
        {
            base.ShowMessage(Forte_SubscriptionFeaturePricingSetup_topSuccessMessageLabel,
                Forte_SubscriptionFeaturePricingSetup_bottomSuccessMessageLabel, Message);
        }

        /// <summary>
        /// Rebinds the grid view
        /// </summary>
        public void RebindGridView()
        {
            BindSelectedFeaturesGrid(Convert.ToInt32(
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.Substring(0,
                        Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedValue.IndexOf(','))));
        }

        #endregion Public Methods

        #region Override methods                                               

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override bool IsValidData()
        {
            bool IsValid = true;
            if (Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList.SelectedIndex == 0)
            {
                IsValid = false;
                ShowMessage(Forte_SubscriptionFeaturePricingSetup_topErrorMessageLabel,
                    Forte_SubscriptionFeaturePricingSetup_bottomErrorMessageLabel, HCMResource.SubscriptionPricing_SelectSubscription);
            }
             CheckBox unlimitedCheckbox=null;
             CheckBox notApplicableCheckbox=null;
             TextBox  featueValuTextbox=null;
             Label labelFeatureID =null;



            for (int checkVal = 0; checkVal < Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows.Count; checkVal++)
            {
               
                    unlimitedCheckbox=(CheckBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[checkVal].
                    FindControl("Forte_SubscriptionFeaturePricingSetup_isUnlimitedCheckBox");
                 
                    notApplicableCheckbox=(CheckBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[checkVal].
                    FindControl("Forte_SubscriptionFeaturePricingSetup_isNotApplicableCheckBox");

                    featueValuTextbox=(TextBox)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[checkVal].
                    FindControl("Forte_SubscriptionFeaturePricingSetup_featureDefaultValueTextBox");

                    labelFeatureID=(Label)Forte_SubscriptionFeaturePricingSetup_featureTypeGridView.Rows[checkVal].
                    FindControl("Forte_SubscriptionFeaturePricingSetup_rowNumberHeaderLabel");


                    if (unlimitedCheckbox.Checked == false && notApplicableCheckbox.Checked == false &&
                        featueValuTextbox.Text == string.Empty)
                    {
                        IsValid = false;
                        ShowMessage(Forte_SubscriptionFeaturePricingSetup_topErrorMessageLabel,
                        Forte_SubscriptionFeaturePricingSetup_bottomErrorMessageLabel,
                        String.Format(HCMResource.SubscriptionPricing_EnterFeatureValue, labelFeatureID.Text));
                        return IsValid;
                    }
            }
            return IsValid;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void LoadValues()
        {
            Forte_SubscriptionFeaturePricingSetup_publishStatusImage.Visible = false;
            BindSubscriptionDropDownList();
            Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingconfirmPopupExtenderControl.Message = HCMResource.SubscriptionPricing_DeletePricingConfirmMessage;
        }

        #endregion Override methods
    }
}