﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// UpgradeAccount.cs
// File that represents the user interface to 
// upgrade his/her account.

#endregion Header                                                              

#region Namespace                                                              

using System;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Resources;


#endregion Namespace

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for UpgradeAccount page. This is used
    /// to upgrade his/her account details.
    /// This class is inherited from Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class UpgradeAccount : PageBase
    {

        #region Enum                                                           

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(HCMResource.UpgradeAccount_PageTitle);
                if (IsPostBack)
                    return;
                LoadValues();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void UpdgradeAccount_ResetButtonClick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void UpgradeAccount_AcceptButtonClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("../Subscription/SubscriptionType.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        private void GetSubscriptionAndFeatureDetails(out UserRegistrationInfo userInfo, out SubscriptionTypes subscriptionTypes, out List<SubscriptionFeatures> subscriptionFeatures)
        {
            new Forte.HCM.BL.UserRegistrationBLManager().GetSubscriptionAccountDetailsUser(base.userID, base.tenantID,
                 out userInfo, out subscriptionTypes, out subscriptionFeatures, null);
        }

        private void BindSubscriptionAndFeatureDetails()
        {
            UserRegistrationInfo userInfo = null;
            SubscriptionTypes subscriptionTypes = null;
            List<SubscriptionFeatures> subscriptionFeatures = null;
            try
            {
                GetSubscriptionAndFeatureDetails(out userInfo, out subscriptionTypes, out subscriptionFeatures);
                if ((userInfo == null || subscriptionTypes == null || userInfo.SubscriptionRole == Enum.GetName(typeof(SubscriptionRolesEnum), 4)) && base.isSiteAdmin == false)
                {
                    RedirectToAccessDeniedPage();
                    return;
                }
                Forte_UpgradeAccount_subscriptionIdHidden.Value = subscriptionTypes.SubscriptionId.ToString();
                Forte_UpgradeAccount_subscriptionTypeLabel.Text = subscriptionTypes.SubscriptionName;
                Forte_UpgradeAccount_statusLabel.Text = userInfo.IsActive == 1 ? "Active" : "In-Active";
                Forte_UpgradeAccount_subscribedOnLabel.Text = Convert.ToDateTime(userInfo.CreatedDate).ToString("MM/dd/yyyy");
                Forte_UpgradeAccount_activatedOnLabel.Text = Convert.ToDateTime(userInfo.ActivatedOn).ToString("MM/dd/yyyy");
                Forte_UpgradeAccount_noOfUsersLabel.Text = userInfo.NumberOfUsers.ToString();
                Forte_UpgradeAccount_isTrialLabel.Text = userInfo.IsTrial == 1 ? "Yes" : "No";
                Forte_UpgradeAccount_titleLabel.Text = userInfo.Title;
                Forte_UpgradeAccount_companyLabel.Text = userInfo.Company;
                Forte_UpgradeAccount_phoneLabel.Text = userInfo.Phone;
                Forte_UpgradeAccount_firstNameLabel.Text = userInfo.FirstName;
                Forte_UpgradeAccount_lastNameLabel.Text = userInfo.LastName;
                Forte_UpgradeAccount_featureAndPricingGridView.DataSource = subscriptionFeatures;
                Forte_UpgradeAccount_featureAndPricingGridView.DataBind();
                Forte_UpgradeAccount_lastLoginLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
            }
            finally
            {
                if (userInfo != null) userInfo = null;
                if (subscriptionTypes != null) subscriptionTypes = null;
                if (subscriptionFeatures != null) subscriptionFeatures = null;
            }
        }

        /// <summary>
        /// A method that redirects to the unzuthorized page
        /// </summary>
        private void RedirectToAccessDeniedPage()
        {
            Response.Redirect("../Common/AccessDenied.aspx", false);
        }

        /// <summary>
        /// A Method that logs the exception message and 
        /// display's the error message to the user.
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception message
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            base.ShowMessage(Forte_UpgradeAccount_topErrorMessageLabel,
                Forte_UpgradeAccount_bottomErrorMessageLabel, exp.Message);
        }

        #endregion Private Methods

        #region Override Methods                                               

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            BindSubscriptionAndFeatureDetails();
        }

        #endregion Override Methods
    }
}