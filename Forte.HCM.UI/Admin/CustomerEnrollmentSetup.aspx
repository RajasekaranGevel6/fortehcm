﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    AutoEventWireup="true" CodeBehind="CustomerEnrollmentSetup.aspx.cs" Inherits="Forte.HCM.UI.Admin.CustomerEnrollmentSetup" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="CustomerEnrollmentSetup_Content" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
        function LoadGetForm(dropDownLisID) {
            var dropdownlist = document.getElementById(dropDownLisID);
            var value = dropdownlist.options[dropdownlist.selectedIndex].value;
            if (value == 0) {
                return;
            }
            else {
                LoadPreviewForm('', value);
            }

        }
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <asp:UpdatePanel ID="CustomerEnrollmentSetup_topSaveUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                    <asp:Literal ID="CustomerEnrollmentSetup_headerLiteral" runat="server" Text="Tenant Enrollment Setup"></asp:Literal>
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                <asp:Button ID="CustomerEnrollmentSetup_topSaveButton" runat="server" Text="Save"
                                                    SkinID="sknButtonId" OnClick="CustomerEnrollmentSetup_saveButton_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CustomerEnrollmentSetup_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="CustomerEnrollmentSetup_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CustomerEnrollmentSetup_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CustomerEnrollmentSetup_messageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CustomerEnrollmentSetup_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CustomerEnrollmentSetup_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg">
                                        <asp:Literal ID="CustomerEnrollmentSetup_businessTypesLiteral" runat="server" Text="Business Types"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <div id="CustomerEnrollmentSetup_searchDIV" runat="server">
                                                        <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tab_body_bg">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%">
                                                                                <asp:Label ID="CustomerEnrollmentSetup_businessNameLabel" runat="server" Text="Name"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 90%">
                                                                                <asp:TextBox ID="CustomerEnrollmentSetup_businessNameTextBox" runat="server" Width="99%"
                                                                                    MaxLength="100"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="right">
                                                                    <asp:Button ID="CustomerEnrollmentSetup_searchBusinessTypeButton" runat="server"
                                                                        Text="Search" SkinID="sknButtonId" OnClick="CustomerEnrollmentSetup_searchBusinessTypeButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr id="CustomerEnrollmentSetup_expandAllTR" runat="server">
                                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                                <asp:Literal ID="CustomerEnrollmentSetup_searchResultsLiteral" runat="server" Text="Search Results">
                                                                </asp:Literal>
                                                                <asp:Label ID="CustomerEnrollmentSetup_searchResultsHeaderHelpLabel" runat="server"
                                                                    SkinID="sknLabelText" Text=" - Click column headers to sort">                                                
                                                                </asp:Label>
                                                            </td>
                                                            <td style="width: 50%" align="right">
                                                                <span id="CustomerEnrollmentSetup_searchResultsUparrowSpan" style="display: block;"
                                                                    runat="server">
                                                                    <asp:Image ID="CustomerEnrollmentSetup_searchResultsUpArrow" runat="server" SkinID="sknMaximizeImage" />
                                                                </span><span id="CustomerEnrollmentSetup_searchResultsDownarrowSpan" style="display: none;"
                                                                    runat="server">
                                                                    <asp:Image ID="CustomerEnrollmentSetup_searchResultsDownArrow" runat="server" SkinID="sknMinimizeImage" />
                                                                    <asp:HiddenField ID="CustomerEnrollmentSetup_restoreHiddenField" runat="server" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <asp:UpdatePanel ID="CustomerEnrollmentSetup_businessTypesUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div id="CustomerEnrollmentSetup_businessTypesSearchDIV" runat="server">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <div id="CustomerEnrollmentSetup_businessTypesGridViewDIV" style="height: 200px;
                                                                                display: block; overflow: auto;" runat="server">
                                                                                <asp:GridView ID="CustomerEnrollmentSetup_businessTypesGridView" runat="server" AllowSorting="True"
                                                                                    AutoGenerateColumns="False" OnRowCreated="CustomerEnrollmentSetup_businessTypesGridView_RowCreated"
                                                                                    OnSorting="CustomerEnrollmentSetup_businessTypesGridView_Sorting" OnRowEditing="CustomerEnrollmentSetup_businessTypesGridView_RowEditing"
                                                                                    OnRowCommand="CustomerEnrollmentSetup_businessTypesGridView_RowCommand" OnRowUpdating="CustomerEnrollmentSetup_businessTypesGridView_RowUpdating"
                                                                                    OnRowCancelingEdit="CustomerEnrollmentSetup_businessTypesGridView_RowCancelingEdit">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderStyle-Width="7%" ItemStyle-Width="7%">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_businessTypesGridView_editImageButton"
                                                                                                    runat="server" SkinID="sknEditVectorImageButton" ToolTip="Edit business type"
                                                                                                    CommandArgument='<%# Eval("BusinessTypeID") %>' CommandName="Edit" />
                                                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_businessTypesGridView_UpdateImageButton"
                                                                                                    runat="server" SkinID="sknEditSaveVectorImageButton" ToolTip="Update business type"
                                                                                                    CommandArgument='<%# Eval("BusinessTypeID") %>' CommandName="Update" Visible="false" />
                                                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_businessTypesGridView_deleteImageButton"
                                                                                                    runat="server" SkinID="sknDeleteImageButton" ToolTip="Delete business type" CommandArgument='<%# Eval("BusinessTypeID") %>'
                                                                                                    CommandName="DeleteBusiness" />
                                                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_businessTypesGridView_cancelUpdateImageButton"
                                                                                                    runat="server" SkinID="sknDeleteVectorImageButton" ToolTip="Cancel Update" CommandArgument='<%# Eval("BusinessTypeID") %>'
                                                                                                    CommandName="Cancel" Visible="false" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Name" HeaderStyle-Width="93%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="93%" SortExpression="NAME">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="CustomerEnrollmentSetup_businessTypesGridView_nameLabel" runat="server"
                                                                                                    Text='<%# Eval("BusinessTypeName") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:HiddenField ID="CustomerEnrollmentSetup_businessTypeIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("BusinessTypeID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:HiddenField ID="CustomerEnrollmentSetup_editBusinessTypeIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("BusinessTypeID") %>' />
                                                                                                <asp:TextBox ID="CustomerEnrollmentSetup_businessTypesGridView_editNameTextBox" runat="server"
                                                                                                    Width="90%" AutoCompleteType="None" Text='<%# Eval("BusinessTypeName") %>'></asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="CustomerEnrollmentSetup_businessTypesAddImageButton" runat="server"
                                                                                SkinID="sknAddLinkButton" Text="Add" OnClick="CustomerEnrollmentSetup_businessTypesAddImageButton_Click">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td align="right">
                                                                            <input type="hidden" runat="server" id="CustomerEnrollmentSetup_businessTypesPageNumberHidden"
                                                                                value="1" />
                                                                            <uc1:PageNavigator ID="CustomerEnrollmentSetup_businessTypesPageNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="CustomerEnrollmentSetup_searchBusinessTypeButton" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CustomerEnrollmentSetup_freeSubscriptionLiteral" runat="server"
                                Text="Free Subscription"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CustomerEnrollmentSetup_freeSubscriptionUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="CustomerEnrollmentSetup_freeSubscriptionDaysLabel" runat="server"
                                                                Text="Free Trial Validity(In Days)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:TextBox ID="CustomerEnrollmentSetup_freeSubscriptionDaysTextBox" runat="server"
                                                                    Width="70px"></asp:TextBox>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_freeSubscriptionDaysImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the number of trials for the free subscription " />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <ajaxToolKit:MaskedEditExtender ID="CustomerEnrollmentSetup_freeSubscriptionDaysMaskedEditExtender"
                                                    runat="server" TargetControlID="CustomerEnrollmentSetup_freeSubscriptionDaysTextBox"
                                                    Mask="999" MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                    OnInvalidCssClass="MaskedEditError" MaskType="Number" AcceptNegative="None" AutoComplete="false"
                                                    ErrorTooltipEnabled="True" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CustomerEnrollmentSetup_standardSubscriptionLiteral" runat="server"
                                Text="Standard Subscription"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CustomerEnrollmentSetup_standardSubscriptionUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="CustomerEnrollmentSetup_standardSubscriptionFreeTrialLabel" runat="server"
                                                                Text="Free Trial Validity(In Days)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:TextBox ID="CustomerEnrollmentSetup_standardSubscriptionFreeTrialTextBox" runat="server"
                                                                    Width="70px"></asp:TextBox>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_standardSubscriptionFreeTrialImageButton"
                                                                    SkinID="sknHelpImageButton" runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the number of trials for the standard subscription " />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <ajaxToolKit:MaskedEditExtender ID="CustomerEnrollmentSetup_standardSubscriptionMaskedEditExtender"
                                                    runat="server" TargetControlID="CustomerEnrollmentSetup_standardSubscriptionFreeTrialTextBox"
                                                    Mask="999" MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                    OnInvalidCssClass="MaskedEditError" MaskType="Number" AcceptNegative="None" AutoComplete="false"
                                                    ErrorTooltipEnabled="True" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CustomerEnrollmentSetup_corporateSubscriptionLiteral" runat="server"
                                Text="Corporate Subscription"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CustomerEnrollmentSetup_corporateSubscriptionUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="CustomerEnrollmentSetup_corporateSubscriptionFreeTrialLabel" runat="server"
                                                                Text="Free Trial Validity(In Days)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 13%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:TextBox ID="CustomerEnrollmentSetup_corporateSubscriptionFreeTrialTextBox" runat="server"
                                                                    Width="70px"></asp:TextBox>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_corporateSubscriptionFreeTrialImageButton"
                                                                    SkinID="sknHelpImageButton" runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the number of trials for the corporate subscription " />
                                                            </div>
                                                        </td>
                                                        <td style="width: 13%;">
                                                            <asp:Label ID="CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersLabel" runat="server"
                                                                Text="Maximum No Of Users" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:TextBox ID="CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersTextBox" runat="server"
                                                                    Width="70px"></asp:TextBox>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersImageButton"
                                                                    SkinID="sknHelpImageButton" runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the maximum number of users for the corporate subscription " />
                                                            </div>
                                                        </td>
                                                        <td valign="middle" >
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="width: 5%">
                                                                        <asp:Label ID="CustomerEnrollmentSetup_rolesLabel" runat="server" Text="Roles" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CustomerEnrollmentSetup_rolesComboTextBox" ReadOnly="true" runat="server"
                                                                            Width="98%" Font-Size="X-Small"></asp:TextBox>
                                                                        <asp:Panel ID="CustomerEnrollmentSetup_rolesPanel" runat="server" CssClass="popupControl_positionProfile">
                                                                            <asp:CheckBoxList ID="CustomerEnrollmentSetup_rolesCheckBoxList" RepeatColumns="1" 
                                                                                runat="server" CellPadding="0"
                                                                                AutoPostBack="true" 
                                                                                onselectedindexchanged="CustomerEnrollmentSetup_rolesCheckBoxList_SelectedIndexChanged">
                                                                            </asp:CheckBoxList>
                                                                        </asp:Panel>
                                                                        <ajaxToolKit:PopupControlExtender ID="CustomerEnrollmentSetup_rolesListPopupControlExtender"
                                                                            runat="server" TargetControlID="CustomerEnrollmentSetup_rolesComboTextBox" PopupControlID="CustomerEnrollmentSetup_rolesPanel"
                                                                            OffsetX="2" OffsetY="2" Position="Bottom">
                                                                        </ajaxToolKit:PopupControlExtender>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <ajaxToolKit:MaskedEditExtender ID="CustomerEnrollmentSetup_corporateSubscriptionFreeTrialMaskedEditExtender"
                                                    runat="server" TargetControlID="CustomerEnrollmentSetup_corporateSubscriptionFreeTrialTextBox"
                                                    Mask="999" MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                    OnInvalidCssClass="MaskedEditError" MaskType="Number" AcceptNegative="None" AutoComplete="false"
                                                    ErrorTooltipEnabled="True" />
                                                <ajaxToolKit:MaskedEditExtender ID="CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersMaskedEditExtender"
                                                    runat="server" TargetControlID="CustomerEnrollmentSetup_corporateSubscriptionNoOfUsersTextBox"
                                                    Mask="999" MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                    OnInvalidCssClass="MaskedEditError" MaskType="Number" AcceptNegative="None" AutoComplete="false"
                                                    ErrorTooltipEnabled="True" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CustomerEnrollmentSetup_formSettingsLiteral" runat="server" Text="Form Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 12%;">
                                                            <asp:Label ID="CustomerEnrollmentSetup_formLayoutLabel" runat="server" Text="Default Form Layout"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;">
                                                            <div style="float: left; padding-right: 5px; width: 80%">
                                                                <asp:DropDownList ID="CustomerEnrollmentSetup_formLayoutDropDownList" runat="server"
                                                                    SkinID="sknSubjectDropDown" DataTextField="FormName" DataValueField="FormID">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_formLayoutHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the default form for the users" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CustomerEnrollmentSetup_previewLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                Text="Preview" ToolTip="Click here to have the preview of the form " OnClick="CustomerEnrollmentSetup_previewLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CustomerEnrollmentSetup_bottomUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CustomerEnrollmentSetup_bottomSuccessLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CustomerEnrollmentSetup_bottomErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <asp:UpdatePanel ID="CustomerEnrollmentSetup_bottomSaveUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                <asp:Button ID="CustomerEnrollmentSetup_bottomSaveButton" runat="server" Text="Save"
                                                    SkinID="sknButtonId" OnClick="CustomerEnrollmentSetup_saveButton_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CustomerEnrollmentSetup_bottomResetLinkButton" runat="server"
                                                    Text="Reset" SkinID="sknActionLinkButton" OnClick="CustomerEnrollmentSetup_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CustomerEnrollmentSetup_bottomCancelLinkButton" runat="server"
                                                    Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CustomerEnrollmentSetup_deleteBusinessUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="CustomerEnrollmentSetup_deleteBusinessHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="CustomerEnrollmentSetup_deleteBusinessPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl"
                                runat="server" OnOkClick="CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl_okClick"
                                OnCancelClick="CustomerEnrollmentSetup_deleteBusinessConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="CustomerEnrollmentSetup_deleteBusinessHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CustomerEnrollmentSetup_deleteFormModalPopupExtender"
                            runat="server" PopupControlID="CustomerEnrollmentSetup_deleteBusinessPopupPanel"
                            TargetControlID="CustomerEnrollmentSetup_deleteBusinessHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CustomerEnrollmentSetup_updateBusinessUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="CustomerEnrollmentSetup_updateBusinessPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_scheduler" DefaultButton="CustomerEnrollmentSetup_updateBusinessNameSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="CustomerEnrollmentSetup_updateBusinesshiddenButton" runat="server"
                                    Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="CustomerEnrollmentSetup_updateBusinessLiteral" runat="server" Text="Edit Business Type"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="CustomerEnrollmentSetup_updateBusinessTopCancelImageButton"
                                                                    runat="server" SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="2">
                                                                <asp:Label ID="CustomerEnrollmentSetup_updateBusinessNameErrorLabel" runat="server"
                                                                    SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%">
                                                                <asp:Label ID="CustomerEnrollmentSetup_updateBusinessNameLabel" runat="server" Text="Business Type"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td style="width: 82%">
                                                                <asp:TextBox ID="CustomerEnrollmentSetup_updateBusinessNameTextBox" runat="server"
                                                                    Width="90%" MaxLength="100"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:HiddenField ID="CustomerEnrollmentSetup_updateBusinessIsEditORSaveHiddenField"
                                                        runat="server" />
                                                    <asp:Button ID="CustomerEnrollmentSetup_updateBusinessNameSaveButton" runat="server"
                                                        SkinID="sknButtonId" Text="Save" OnClick="CustomerEnrollmentSetup_updateBusinessNameSaveButton_Clicked" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="CustomerEnrollmentSetup_updateBusinessNameCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" OnClick="CustomerEnrollmentSetup_updateBusinessNameCloseLinkButton_Clicked" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CustomerEnrollmentSetup_updateBusinessNameModalPopupExtender"
                            runat="server" PopupControlID="CustomerEnrollmentSetup_updateBusinessPanel" TargetControlID="CustomerEnrollmentSetup_updateBusinesshiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="CustomerEnrollmentSetup_rolesHiddenField" runat="server" />
</asp:Content>
