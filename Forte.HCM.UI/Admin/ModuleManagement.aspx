﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
CodeBehind="ModuleManagement.aspx.cs" Inherits="Forte.HCM.UI.Admin.ModuleManagement" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="ModuleManagement_Content" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script language="javascript" type="text/javascript">


        function disableEnterKey(e) {
            var key;
            if (window.event)
                key = window.event.keyCode;     //IE          
            else
                key = e.which;     //firefox          
            if (key == 13)
                return false;
            else
                return true;
        } 

    </script>
      <asp:UpdatePanel ID="ModuleManagement_mainUpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                 
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                    <asp:Literal ID="ModuleManagement_headerLiteral" runat="server" Text="Module Management"></asp:Literal>
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="ModuleManagement_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="ModuleManagement_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="ModuleManagement_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                   
                        <asp:Label ID="ModuleManagement_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ModuleManagement_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                      
                    </td>
                </tr>
                <tr>
                    <td class="TAB_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr id="ModuleManagement_expandAllTR" runat="server">
                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                        <asp:Label ID="ModuleManagement_searchResultsHeaderHelpLabel" runat="server" Text="Modules"> </asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="ModuleManagement_searchResultsDownarrowSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="ModuleManagement_searchResultsDownArrow" runat="server" SkinID="sknMinimizeImage" />
                                                                            <asp:HiddenField ID="ModuleManagement_restoreHiddenField" runat="server" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            
                                                            <div id="ModuleManagement_moduleSearchDIV1" runat="server">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <div id="ModuleManagement_moduleGridViewDIV" style="height: 225px; display: block;
                                                                                overflow: auto;" runat="server" onkeypress="javascript:return disableEnterKey(event);">
                                                                                <asp:GridView ID="ModuleManagement_moduleGridView" runat="server" AllowSorting="True"
                                                                                    AutoGenerateColumns="False" OnRowCancelingEdit="ModuleManagement_moduleGridView_RowCancelingEdit"
                                                                                    OnRowEditing="ModuleManagement_moduleGridView_RowEditing" OnRowCommand="ModuleManagement_moduleGridView_RowCommand"
                                                                                    OnRowUpdating="ModuleManagement_moduleGridView_RowUpdating" OnRowCreated="ModuleManagement_moduleGridView_RowCreated"
                                                                                    OnSorting="ModuleManagement_moduleGridView_Sorting">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderStyle-Width="7%" ItemStyle-Width="7%">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="ModuleManagement_moduleGridView_editImageButton" runat="server"
                                                                                                    SkinID="sknEditVectorImageButton" ToolTip="Edit Module" CommandArgument='<%# Eval("ModID") %>'
                                                                                                    CommandName="Edit" />
                                                                                                <asp:ImageButton ID="ModuleManagement_moduleGridView_UpdateImageButton" runat="server"
                                                                                                    SkinID="sknEditSaveVectorImageButton" ToolTip="Update Module" ValidationGroup="b"
                                                                                                    CommandArgument='<%# Eval("ModID") %>' CommandName="Update" Visible="false" />
                                                                                                <asp:ImageButton ID="ModuleManagement_moduleGridView_deleteImageButton" runat="server"
                                                                                                    SkinID="sknDeleteImageButton" ToolTip="Delete Module" CommandArgument='<%# Eval("ModID") %>'
                                                                                                    CommandName="DeleteModule" />
                                                                                                <asp:ImageButton ID="ModuleManagement_moduleGridView_cancelUpdateImageButton"
                                                                                                    runat="server" SkinID="sknDeleteVectorImageButton" ToolTip="Cancel Update" CommandArgument='<%# Eval("ModID") %>'
                                                                                                    CommandName="Cancel" Visible="false" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Module Code" HeaderStyle-Width="47%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="37%" SortExpression="MODCODE">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="ModuleManagement_moduleGridView_modcodeLabel" runat="server"
                                                                                                    Text='<%# Eval("ModCode") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:HiddenField ID="ModuleManagement_moduleIDHiddenField" runat="server" Value='<%# Eval("ModID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:HiddenField ID="ModuleManagement_editModuleIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("ModID") %>' />
                                                                                                <asp:TextBox ID="ModuleManagement_moduleGridView_editcodeTextBox" runat="server"
                                                                                                    Width="30%" AutoCompleteType="None" Text='<%# Eval("ModCode") %>' MaxLength="10"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="ModuleManagement_moduleCodeRequiredFieldValidator"
                                                                                                    runat="server" ControlToValidate="ModuleManagement_moduleGridView_editcodeTextBox"
                                                                                                    ErrorMessage="Enter module code" ValidationGroup="b" Display="None" SetFocusOnError="true"
                                                                                                    ForeColor="White">
                                                                                                </asp:RequiredFieldValidator>
                                                                                                <ajaxToolKit:ValidatorCalloutExtender ID="ModuleManagement_moduleCodeValidatorCalloutExtender"
                                                                                                    runat="server" TargetControlID="ModuleManagement_moduleCodeRequiredFieldValidator"
                                                                                                    PopupPosition="Right" CssClass="customCalloutStyle">
                                                                                                </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                <asp:Label ID="ModuleManagement_moduleCodeHiddenLabel" runat="server" Visible="false"
                                                                                                    Text='<%# Eval("ModCode") %>'></asp:Label>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Module Name" HeaderStyle-Width="46%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="30%" SortExpression="MODNAME">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="ModuleManagement_moduleGridView_modulenameLabel" runat="server"
                                                                                                    Text='<%# Eval("ModName") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:HiddenField ID="ModuleManagement_moduleNameIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("ModID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:HiddenField ID="ModuleManagement_editmodulenameIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("ModID") %>' />
                                                                                                <asp:TextBox ID="ModuleManagement_moduleGridView_editmoduleNameTextBox" runat="server"
                                                                                                    Width="36%" AutoCompleteType="None" Text='<%# Eval("ModName") %>' MaxLength="50"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="ModuleManagement_moduleNameRequiredFieldValidator"
                                                                                                    runat="server" ControlToValidate="ModuleManagement_moduleGridView_editmoduleNameTextBox"
                                                                                                    ErrorMessage="Enter module name" ValidationGroup="b" Display="None" SetFocusOnError="true"
                                                                                                    ForeColor="White">
                                                                                                </asp:RequiredFieldValidator>
                                                                                                <ajaxToolKit:ValidatorCalloutExtender ID="ModuleManagement_moduleNameValidatorCalloutExtender"
                                                                                                    runat="server" TargetControlID="ModuleManagement_moduleNameRequiredFieldValidator"
                                                                                                    PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                <asp:Label ID="ModuleManagement_moduleNameHiddenLabel" runat="server" Visible="false"
                                                                                                    Text='<%# Eval("ModName") %>'></asp:Label>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="ModuleManagement_moduleAddImageButton" runat="server" SkinID="sknAddLinkButton"
                                                                                Text="Add" OnClick="ModuleManagement_moduleAddImageButton_Click">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td align="right">
                                                                            <input type="hidden" runat="server" id="ModuleManagement_categoryTypePageNumberHidden"
                                                                                value="1" />
                                                                            <uc1:PageNavigator ID="ModuleManagement_modulePageNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                          
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 15px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="pnl" runat="server" DefaultButton="ModuleManagement_Save">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td class="grid_header_bg">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td style="width: 50%" class="header_text_bold">
                                                                                                <asp:Literal ID="ModuleManagement_rolesLiteral" runat="server" Text="Sub Modules"></asp:Literal>
                                                                                            </td>
                                                                                            <td style="width: 50%" align="right">
                                                                                                <asp:LinkButton ID="ModuleManagement_showhide" Visible="false" runat="server" OnClick="ModuleManagement_showhideClick"
                                                                                                    SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                                <span id="ModuleManagement_DownarrowSpan" style="display: none;" runat="server">
                                                                                                    <asp:Image ID="ModuleManagement_DownArrow" runat="server" SkinID="sknMinimizeImage" />
                                                                                                    <asp:HiddenField ID="CModuleManagement_restoreHiddenField" runat="server" />
                                                                                                </span>
                                                                                            </td>
                                                                                            <td style="width: 50%">
                                                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Button ID="ModuleManagement_Save" runat="server" Text="Save" SkinID="sknButtonId"
                                                                                                                OnClick="ModuleManagement_SaveClick" ValidationGroup="X" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="grid_body_bg">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div id="ModuleManagement_moduleSearchDIV" style="overflow: auto;">
                                                                                        <input type="hidden" id="ModuleManagement_deletesubmoduleHiddenType" runat="server" />
                                                                                        <asp:GridView ID="ModuleManagement_submoduleGridView" runat="server" AutoGenerateColumns="false"
                                                                                            SkinID="sknNewGridView" Width="100%" OnRowCommand="ModuleManagement_submoduleGridView_RowCommand">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="ModuleManagement_rolesGridView_deleteImageButton" runat="server"
                                                                                                            ToolTip="Delete Sub Module" SkinID="sknDeleteImageButton" CommandName="DeleteSubModule"
                                                                                                            CommandArgument='<%# Eval("SubModuleID") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="7%" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Image ID="ModuleManagement_RoleImage" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/role_icon_hcm.png" />
                                                                                                        <asp:Label ID="ModuleManagement_rowNumberHeaderLabel" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                                                        <asp:Label ID="ModuleManagement_rowNumberDotHeaderLabel" runat="server" Text="."></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <HeaderTemplate>
                                                                                                        <div style="float: left; width: 100%;">
                                                                                                            <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                                                <tr>
                                                                                                                    <td style="height: 5px;">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 30%" align="left">
                                                                                                                        Module <span class="mandatory">*</span>
                                                                                                                    </td>
                                                                                                                    <td align="left" style="width: 30%">
                                                                                                                        Sub Module Code <span class="mandatory">*</span>
                                                                                                                    </td>
                                                                                                                    <td align="left" style="width: 30%">
                                                                                                                        Sub Module Name <span class="mandatory">*</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemStyle Width="93%" />
                                                                                                    <ItemTemplate>
                                                                                                        <div style="float: left; width: 100%;">
                                                                                                            <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                                                <tr>
                                                                                                                    <td style="height: 5px;">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="center" style="height: 40px; width: 100%;">
                                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 30%" align="left">
                                                                                                                                    <asp:Label ID="ModuleManagement_updateSubmoduleLabel" runat="server"  Text='<%# Eval("ModeCodeName") %>'></asp:Label>
                                                                                                                                    <asp:HiddenField ID="ModuleManagment_submoduleCodeNameHiddenFiled" runat="server" Value='<%# Eval("SubModuleName") %>' />
                                                                                                                                    <asp:Label ID="ModuleManagment_submoduleCodeName" runat="server" Text='<%# Eval("SubModuleName") %>'
                                                                                                                                        SkinID="sknLabelFieldText" Visible="false"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td align="left" style="width: 30%">
                                                                                                                                    <asp:TextBox ID="ModuleManagement_submoduleGridView_submodulecodeTextBox" runat="server" Width="50%"
                                                                                                                                        Text='<%# Eval("SubModuleCode") %>' MaxLength="10"></asp:TextBox>
                                                                                                                                    <asp:RequiredFieldValidator ID="ModuleManagement_submodulecodeRequiredFieldValidator" runat="server"
                                                                                                                                        ControlToValidate="ModuleManagement_submoduleGridView_submodulecodeTextBox" ErrorMessage="Enter submodule code"
                                                                                                                                        ValidationGroup="X" Display="None" SetFocusOnError="true">
                                                                                                                                    </asp:RequiredFieldValidator>
                                                                                                                                    <ajaxToolKit:ValidatorCalloutExtender ID="ModuleManagement_submoduleGridView_submoduleCodeValidatorCalloutExtender"
                                                                                                                                        runat="server" TargetControlID="ModuleManagement_submodulecodeRequiredFieldValidator"
                                                                                                                                        PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                                                    </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                                                    <asp:Label ID="ModuleManagement_submodulecodeHiddenLabel" runat="server" Visible="false"
                                                                                                                                        Text='<%# Eval("SubModuleCode") %>'></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td align="left" style="width: 30%">
                                                                                                                                    <asp:TextBox ID="ModuleManagement_submoduleGridView_submodulenameTextBox" runat="server" Text='<%# Eval("SubModuleName") %>'
                                                                                                                                        Width="95%" MaxLength="50"></asp:TextBox>
                                                                                                                                    <asp:RequiredFieldValidator ID="ModuleManagement_submodulenameRequiredFieldValidator" runat="server"
                                                                                                                                        ControlToValidate="ModuleManagement_submoduleGridView_submodulenameTextBox" ErrorMessage="Enter submodule name"
                                                                                                                                        ValidationGroup="X" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                                                                    <ajaxToolKit:ValidatorCalloutExtender ID="ModuleManagement_submoduleGridView_submodulenameValidatorCalloutExtender"
                                                                                                                                        runat="server" TargetControlID="ModuleManagement_submodulenameRequiredFieldValidator"
                                                                                                                                        PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                                                    </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                                                    <asp:Label ID="ModuleManagement_submodulenameHiddenLabel" runat="server" Visible="false"
                                                                                                                                        Text='<%# Eval("SubModuleName") %>'></asp:Label>
                                                                                                                                    <asp:HiddenField ID="ModuleManagement_submoduleidHiddenLabel" runat="server" Value='<%# Eval("SubModuleID") %>' />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="td_height_5">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="td_height_5">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_10">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:LinkButton ID="ModuleManagement_subModuleAddImageButton" runat="server" SkinID="sknAddLinkButton"
                                                                                        Text="Add" OnClick="ModuleManagement_submoduleAddImageButton_Click"> </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </asp:Panel>
                                                           
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_10">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg">
                              
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" class="header_text_bold">
                                                &nbsp;
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                                                OnClick="ModuleManagement_resetLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                                                OnClick="ParentPageRedirect"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                             
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                    
                        <asp:Label ID="ModuleManagement_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ModuleManagement_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                      
                    </td>
                </tr>
                <tr>
                    <td>
                    
                        <div style="display: none">
                            <asp:Button ID="ModuleManagement_deleteModuleHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="ModuleManagement_deleteModulePopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="ModuleManagement_deletemoduleConfirmMsgControl" runat="server"
                                OnOkClick="ModuleManagement_deleteModuleConfirmMsgControl_okClick" OnCancelClick="ModuleManagement_deleteModuleConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="ModuleManagement_deleteHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="ModuleManagement_deleteModuleModalPopupExtender"
                            runat="server" PopupControlID="ModuleManagement_deleteModulePopupPanel" TargetControlID="ModuleManagement_deleteModuleHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                     
                    </td>
                </tr>

                <tr>
                    <td>
                        
                        <asp:Panel ID="ModuleManagement_updatemodulePanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_roleCategory" DefaultButton="ModuleManagement_updateModuleNameSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="ModuleManagement_updateModulehiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="ModuleManagement_updateModuleLiteral" runat="server" Text="Edit Module"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="ModuleManagement_updateModuleTopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="ModuleManagement_updateModuleNameErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ModuleManagement_updateModuleCodeLabel" runat="server" Text="Module Code"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="ModuleManagement_updateModuleCodeTextBox" runat="server" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="ModuleManagement_updateModuleNameLabel" runat="server" Text="Module Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="ModuleManagement_updateModuleNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:HiddenField ID="ModuleManagement_updateModuleIsEditORSaveHiddenField" runat="server" />
                                                    <asp:Button ID="ModuleManagement_updateModuleNameSaveButton" runat="server" SkinID="sknButtonId"
                                                        Text="Save" OnClick="ModuleManagement_updateModuleCodeNameSaveButton_Clicked" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ModuleManagement_updateModuleNameCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" OnClick="ModuleManagement_updateModuleCloseLinkButton_Clicked" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="ModuleManagement_updateModuleNameModalPopupExtender"
                            runat="server" PopupControlID="ModuleManagement_updatemodulePanel" TargetControlID="ModuleManagement_updateModulehiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                      
                    </td>
                </tr>
                <tr>
                    <td>
                      
                        <div style="display: none">
                            <asp:Button ID="ModuleManagement_deleteRolesHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="ModuleManagement_deleteRolesPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="ModuleManagement_deleteRolesConfirmMsgControl" OnOkClick="ModuleManagement_deleteSubModuleModuleConfirmMsgControl_okClick	"
                                OnCancelClick="ModuleManagement_deleteSubModuleConfirmMsgControl_cancelClick" runat="server" />
                            <asp:HiddenField ID="ModuleManagement_deleteRoleHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="ModuleManagement_deleteRoleFormModalPopupExtender"
                            runat="server" PopupControlID="ModuleManagement_deleteRolesPopupPanel" TargetControlID="ModuleManagement_deleteRolesHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%-- <asp:UpdatePanel ID="ModuleManagement_updateRolesUpdatePanel" runat="server">
                    <ContentTemplate>--%>
                        <asp:Panel ID="ModuleManagement_updateSubModulePanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_addSubModule" DefaultButton="ModuleManagement_updateSubModuleSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="ModuleManagement_updateSubModulehiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="ModuleManagement_updateRoleLiteral" runat="server" Text="Edit Business Type"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="ModuleManagement_updateRolesTopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                        align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="2">
                                                                <asp:Label ID="ModuleManagement_updateSubModuleNameErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="ModuleManagement_SubmoduleLabel" runat="server" Text="Module" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class='mandatory'>*</span>
                                                                        </td>
                                                                        <td style="width: 75%">
                                                                            <asp:DropDownList ID="ModuleManagement_updatesubmoduleDropdownlist" runat="server" Width="94%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 25%">
                                                                                        <asp:Label ID="ModuleManagement_updateSubModuleCodeLabel" runat="server" Text="Sub Module Code"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        <span class='mandatory'>*</span>
                                                                                    </td>
                                                                                    <td style="width: 32%">
                                                                                        <asp:TextBox ID="ModuleManagement_updateSubModuleCodeTextBox" runat="server" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 18%">
                                                                                        <asp:Label ID="ModuleManagement_updateSubModuleNameLabel" runat="server" Text="Sub Module Name"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        <span class='mandatory'>*</span>
                                                                                    </td>
                                                                                    <td style="width: 32%">
                                                                                        <asp:TextBox ID="ModuleManagement_updateSubModuleNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:HiddenField ID="ModuleManagement_updateRoleIsEditORSaveHiddenField" runat="server" />
                                                    <asp:Button ID="ModuleManagement_updateSubModuleSaveButton" OnClick="ModuleManagement_updateSubModuleSaveButton_Clicked"
                                                        runat="server" SkinID="sknButtonId" Text="Save" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ModuleManagement_updateSubModuleCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="ModuleManagement_updateSubModuleModalPopupExtender"
                            runat="server" PopupControlID="ModuleManagement_updateSubModulePanel" TargetControlID="ModuleManagement_updateSubModulehiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>