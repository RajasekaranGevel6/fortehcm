﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Options.aspx.cs
// File that represents the Options page that defines the user interface
// layout and functionalities for the Options page. This page helps in 
// configuring the admin settings such as complexity factor, computation
// factor, maximum recency factor, grid page size, default talent scout
// spheres, etc. This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the Options page that defines the user interface
    /// layout and functionalities for the Options page. This page helps in 
    /// configuring the admin settings such as complexity factor, computation
    /// factor, maximum recency factor, grid page size, default talent scout
    /// spheres, etc. This class inherits Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class Options : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus
                Page.Form.DefaultButton = Options_topSaveButton.UniqueID;
                Options_simpleTextBox.Focus();
                // Set page title
                Master.SetPageCaption("Options");
                if (!IsPostBack)
                {
                    // Clear all top and bottom label test
                    ClearAllLabelMessage();

                    // Load default values
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(Options_topErrorMessageLabel,
                    Options_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Options_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAllLabelMessage();
                // Validate the input.
                bool isValid = IsValidData();

                if (isValid == false)
                    return;

                // Update the admin settings.
                AdminSettings adminSettings = GetAdminSettings();
                new AdminBLManager().UpdateAdminSettings(adminSettings, base.userID);

                // Keep page size in session.
                Application[Constants.SessionConstants.GRID_PAGE_SIZE] = adminSettings.GridPageSize;

                List<AttributeDetail> existingProximity =
                    (List<AttributeDetail>)ViewState["PROXIMITY_EXISTING"];

                if (Options_proximityGridView.Rows.Count > 0)
                    UpdateProximityValues(Options_proximityGridView,
                        "Options_proximityFactorTextBox", existingProximity);

                AdminSettings proximitySettings = new AdminBLManager().GetAdminSettings();
                if (proximitySettings != null && proximitySettings.ProximityFactor != null
                    && proximitySettings.ProximityFactor.Count > 0)
                    ViewState["PROXIMITY_EXISTING"] = proximitySettings.ProximityFactor;

                base.ShowMessage(Options_topSuccessMessageLabel,
                    Options_bottomSuccessMessageLabel,
                    Resources.HCMResource.Options_SuccessfulSaveSettings);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(Options_topErrorMessageLabel,
                    Options_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the complexity factor 
        /// reset button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the complexity factor values such as simple, 
        /// medium and complex to 1.00, 1.50 and 2.00 respectively.
        /// </remarks>
        protected void Options_restoreComplexityLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Options_simpleTextBox.Text = Constants.
                    OptionsDefaultValuesConstants.SIMPLE_COMPLEXITY;
                Options_mediumTextBox.Text = Constants.
                    OptionsDefaultValuesConstants.MEDIUM_COMPLEXITY;
                Options_complexTextBox.Text = Constants.
                    OptionsDefaultValuesConstants.COMPLEX_COMPLEXITY;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(Options_topErrorMessageLabel,
                    Options_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the computation factor
        /// reset button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the computation factor value to 0.10.
        /// </remarks>
        protected void Options_restoreComputationLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Options_computationFactorTextBox.Text = Constants.
                    OptionsDefaultValuesConstants.COMPUTATION_FACTOR;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(Options_topErrorMessageLabel,
                    Options_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirects to the same page by loading the previously
        /// saved values.
        /// </remarks>
        protected void Options_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                // Show error messages to the user.
                base.ShowMessage(Options_topErrorMessageLabel,
                    Options_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that constructs the admin settings object by taking data 
        /// from the input fields.
        /// </summary>
        /// <returns>
        /// A <see cref="AdminSettings"/> that holds the admin settings.
        /// </returns>
        private AdminSettings GetAdminSettings()
        {
            // Instantiate admin settings object.
            AdminSettings adminSettings = new AdminSettings();

            // Assign the data to the corresponding fields.
            adminSettings.ComplexitySimple = Convert.ToDouble(Options_simpleTextBox.Text);
            adminSettings.ComplexityMedium = Convert.ToDouble(Options_mediumTextBox.Text);
            adminSettings.ComplexityComplex = Convert.ToDouble(Options_complexTextBox.Text);
            adminSettings.ComputationFactor = Convert.ToDouble(Options_computationFactorTextBox.Text);
            adminSettings.MaximumRecencyFactor = Convert.ToInt32(Options_maxRecencyFactorTextBox.Text);
            adminSettings.GridPageSize = Convert.ToInt32(Options_gridPageSizeTextBox.Text);
            adminSettings.NoOfSphere = Convert.ToInt32(Options_defaultNumberOfSpheresTextBox.Text);

            // Return admin settings object.
            return adminSettings;
        }

        /// <summary>
        /// Method that updates proximity factor values into database.
        /// </summary>
        /// <param name="proximityFactorGridView">
        /// A <see cref="GridView"/> that holds the gridview.
        /// </param>
        /// <param name="textBoxID">
        /// A <see cref="string"/> that holds textbox ID.
        /// </param>
        /// <param name="existingProximity">
        /// A <see cref="List<AttributeDetail>"/> that holds proximity factor.
        /// </param> 
        private void UpdateProximityValues(GridView proximityFactorGridView, string textBoxID,
            List<AttributeDetail> existingProximity)
        {
            for (int i = 0; i < proximityFactorGridView.Rows.Count; i++)
            {
                if (existingProximity[i].AttributeID !=
                    proximityFactorGridView.DataKeys[i].Value.ToString())
                    continue;

                if (existingProximity[i].AttributeValue ==
                   ((TextBox)proximityFactorGridView.Rows[i].FindControl(textBoxID)).Text)
                    continue;

                new AdminBLManager().UpdateProximityFactor(
                    Convert.ToDecimal(((TextBox)proximityFactorGridView.Rows[i].FindControl(textBoxID)).Text),
                    proximityFactorGridView.DataKeys[i].Value.ToString(),
                    base.userID);
            }
        }

        /// <summary>
        /// Method that clears messages.
        /// </summary>
        private void ClearAllLabelMessage()
        {

            Options_topErrorMessageLabel.Text = string.Empty;
            Options_bottomErrorMessageLabel.Text = string.Empty;
            Options_topSuccessMessageLabel.Text = string.Empty;
            Options_bottomSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            decimal isValidDecimal = 0;
            int isValidInteger = 0;
            if (decimal.TryParse(Options_simpleTextBox.Text.Trim(), out  isValidDecimal) == false)
            {
                base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                    Resources.HCMResource.Options_ComplexitySimpleCannotBeEmpty);
                isValidData = false;
            }

            if (decimal.TryParse(Options_mediumTextBox.Text.Trim(), out  isValidDecimal) == false)
            {
                base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                    Resources.HCMResource.Options_ComplexityMediumCannotBeEmpty);
                isValidData = false;
            }

            if (decimal.TryParse(Options_complexTextBox.Text.Trim(), out  isValidDecimal) == false)
            {
                base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                    Resources.HCMResource.Options_ComplexityComplexCannotBeEmpty);
                isValidData = false;
            }

            if (decimal.TryParse(Options_computationFactorTextBox.Text.Trim(), out  isValidDecimal) == false)
            {
                base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                    Resources.HCMResource.Options_ComputationFactorShouldNotBeEmpty);
                isValidData = false;
            }

            if (int.TryParse(Options_gridPageSizeTextBox.Text.Trim(), out  isValidInteger) == false)
            {
                base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                    Resources.HCMResource.Options_GridPageSizeMustHaveValue);
                isValidData = false;
            }
            else
            {
                if (Convert.ToInt32(Options_gridPageSizeTextBox.Text.Trim()) <= 0)
                {
                    base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                        "Grid page size must be between 1 and 999");
                    isValidData = false;
                }
            }

            if (int.TryParse(Options_defaultNumberOfSpheresTextBox.Text.Trim(), out  isValidInteger) == false)
            {
                base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                    Resources.HCMResource.Options_NoOfSphereShouldNotBeEmpty);
                isValidData = false;
            }

            if (int.TryParse(Options_maxRecencyFactorTextBox.Text.Trim(), out  isValidInteger) == false)
            {
                base.ShowMessage(Options_topErrorMessageLabel, Options_bottomErrorMessageLabel,
                    Resources.HCMResource.Options_MaximumRecencyFactorShouldNotBeEmpty);
                isValidData = false;
            }

            string errorMessage = string.Empty;
            foreach (GridViewRow row in Options_proximityGridView.Rows)
            {
                if (decimal.TryParse(((TextBox)row.FindControl("Options_proximityFactorTextBox")).Text.Trim(),
                    out  isValidDecimal))
                    continue;

                if (errorMessage == string.Empty)
                {
                    isValidData = false;
                    errorMessage = string.Format
                        ("'{0}' should not be empty or zero",
                         Options_proximityGridView.DataKeys[row.RowIndex].Values[1].ToString());
                }
                else
                {
                    errorMessage = errorMessage + "<br>" + string.Format
                       ("'{0}' should not be empty or zero",
                         Options_proximityGridView.DataKeys[row.RowIndex].Values[1].ToString());
                }
            }
            if (errorMessage != string.Empty)
            {
                ShowMessage(Options_topErrorMessageLabel,
                    Options_bottomErrorMessageLabel,
                    errorMessage);
                isValidData = false;
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Load current admin settings
            AdminSettings adminSettings = new AdminBLManager().GetAdminSettings();

            if (adminSettings == null)
                return;

            Options_simpleTextBox.Text = String.Format("{0:0.00}", adminSettings.ComplexitySimple);
            Options_mediumTextBox.Text = String.Format("{0:0.00}", adminSettings.ComplexityMedium);
            Options_complexTextBox.Text = String.Format("{0:0.00}", adminSettings.ComplexityComplex);
            Options_computationFactorTextBox.Text = String.Format("{0:0.00}", adminSettings.ComputationFactor);
            Options_maxRecencyFactorTextBox.Text = String.Format("{0}", adminSettings.MaximumRecencyFactor.ToString());
            Options_gridPageSizeTextBox.Text = String.Format("{0}", adminSettings.GridPageSize.ToString());
            Options_defaultNumberOfSpheresTextBox.Text = String.Format("{0}", adminSettings.NoOfSphere.ToString());

            if (Utility.IsNullOrEmpty(adminSettings.ProximityFactor))
                return;

            if (adminSettings.ProximityFactor.Count > 0)
            {
                Options_proximityGridView.DataSource = adminSettings.ProximityFactor;
                Options_proximityGridView.DataBind();
                ViewState["PROXIMITY_EXISTING"] = adminSettings.ProximityFactor;
            }
        }

        #endregion Protected Overridden Methods
    }
}