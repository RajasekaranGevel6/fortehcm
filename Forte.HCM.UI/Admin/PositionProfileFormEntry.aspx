﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PositionProfileFormEntry.aspx.cs"
    MasterPageFile="~/MasterPages/PositionProfileMaster.Master" Inherits="Forte.HCM.UI.Admin.PositionProfileFormEntry" %>

<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="../Segments/VerticalBackgroundRequirementControl.ascx" TagName="VerticalBackgroundRequirementControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Segments/TechnicalSkillRequirementControl.ascx" TagName="TechnicalSkillRequirementControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Segments/RoleRequirementControl.ascx" TagName="RoleRequirementControl"
    TagPrefix="uc4" %>
<%@ Register Src="../Segments/EducationRequirementControl.ascx" TagName="EducationRequirementControl"
    TagPrefix="uc5" %>
<%@ Register Src="../Segments/ClientPositionDetailsControl.ascx" TagName="ClientPositionDetailsControl"
    TagPrefix="uc6" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content runat="server" ID="PositionProfileFormEntry_bodyContent" ContentPlaceHolderID="PositionProfileMaster_body">
    <script type="text/javascript" language="javascript">

        // Java script to add the selected id to the hidden field
        function ShowPreviewForm(selectedGridViewID, sessionName)
        {
            var gridIDName = document.getElementById(selectedGridViewID);
            var rowCount = gridIDName.rows.length;
            for (var i = 0; i < rowCount; i++)
            {

                var row = gridIDName.rows[i];
                if (detectBrowser())
                {
                    chkbox = row.cells[0].childNodes[1];
                }
                else
                {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null == chkbox)
                {
                    return;
                }
                else
                {

                    return LoadPreviewForm(sessionName, '');
                }
            }
        }

        function AddIdToHiddenField(id, checkBoxId)
        {
            var checkBox = document.getElementById(checkBoxId);
            var hiddenField = document.getElementById("<%= PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.ClientID%>");

            if (checkBox.checked)
            {
                if (hiddenField.value == "")
                {
                    hiddenField.value = id + ",";
                }
                else
                {
                    hiddenField.value = hiddenField.value + id + ",";
                }
            }
            else
            {

                var string = hiddenField.value;
                if (string.search(id) != -1)
                {

                    hiddenField.value = string.replace(id, '');

                }

            }

        }

        //javascript to add the selected id to the hidden field
        function AddIdToSelectedHiddenField(id, checkBoxId, labelName)
        {
            var checkBox = document.getElementById(checkBoxId);
            var hiddenField = document.getElementById("<%= PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField.ClientID%>");
            var nameHiddenField = document.getElementById("<%= PositionProfileFormEntry_selectedSegmentsSelectedNamesHiddenField.ClientID %>");
            if (checkBox.checked)
            {
                if (hiddenField.value == "")
                {
                    hiddenField.value = id + ",";
                }
                else
                {
                    hiddenField.value = hiddenField.value + id + ",";
                }
                if (nameHiddenField.value == "")
                {
                    nameHiddenField.value = labelName + ",";
                }
                else
                {
                    nameHiddenField.value = nameHiddenField.value + labelName + ",";
                }
            }
            else
            {
                var string = hiddenField.value;
                if (string.search(id) != -1)
                {

                    hiddenField.value = string.replace(id, '');
                }


                var namestring = nameHiddenField.value;
                if (namestring.search(labelName) != -1)
                {

                    nameHiddenField.value = namestring.replace(labelName, '');
                }
            }
        }


        //javascript for drag and drop

        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //                //move the Single Question add to Testdraft.
        //                function singleqnsclick(QuestionID) {
        //                    var hiddenvalue = document.getElementById('<%=PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.ClientID %>');
        //                    hiddenvalue.value = QuestionID;
        //                    __doPostBack(buttonID, "OnClick");
        //                }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source)
        {
            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID))
            {
                CheckNoOfChecked(QuestionID);
            }
            else
            {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id)
        {
            var table = sourceTable;
            var testVar = document.getElementById('<%=PositionProfileFormEntry_availableSegmentsGridView.ClientID %>');
            //id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++)
            {
                if (id == i)
                {
                    var row = table.rows[i];
                    if (detectBrowser())
                    {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else
                    {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox)
                    {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;
                    }
                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable))
                {
                    CheckNoOfChecked(id);
                }
            }
        }


        //find the Browser details
        function detectBrowser()
        {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape")
            {
                return true;
            }
            else
            {
                return false; ;
            }
        }

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID)
        {
            var hiddenvalue = document.getElementById('<%=PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.ClientID %>');
            var postBackHiddenvalue = document.getElementById('<%=PositionProfileFormEntry_postbackHiddenField.ClientID %>');
            hiddenvalue.value = QuestionID;
            postBackHiddenvalue.value = "1";
            __doPostBack('PositionProfileFormEntry_selectedSegmentsMoveRightImageButton', "OnClick");
        }


        function CheckNoOfChecked(QuestionID)
        {

            var hiddenvalue = document.getElementById('<%=PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=PositionProfileFormEntry_availableSegmentsGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++)
            {
                var row = table.rows[i];
                if (detectBrowser())
                {
                    chkbox = row.cells[0].childNodes[1];
                }
                else
                {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked)
                {
                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=PositionProfileFormEntry_noOfSelectedDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move ' + numberOfQuestions + ' segment(s)';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser())
                    {
                        if (hiddenvalue.value == "")
                        {
                            hiddenvalue.value = row.childNodes[4].childNodes[1].defaultValue + ',';
                        }
                        else
                        {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[1].defaultValue + ',';
                        }
                    }
                    else
                    {
                        if (hiddenvalue.value == "")
                        {
                            hiddenvalue.value = row.childNodes[3].childNodes[0].value + ',';
                        }
                        else
                        {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[3].childNodes[0].value + ',';
                        }
                    }
                }
            }
        }


        function checkChecked(id)
        {

            var isChecked = false;

            var table = sourceTable;

            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++)
            {
                var row = table.rows[i];
                if (detectBrowser())
                {
                    chkbox = row.cells[0].childNodes[1];
                }
                else
                {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked)
                {
                    isChecked = true;
                }

                if (isChecked)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        function mouseMove(ev)
        {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject)
            {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser())
                {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else
                {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else
            {

            }
            return false;
        }
        function mouseCoords(ev)
        {
            if (ev != null)
            {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev)
        {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=PositionProfileFormEntry_selectedSegmentsGridView.ClientID %>');

            var buttonID = document.getElementById('<%=PositionProfileFormEntry_selectedSegmentsMoveRightImageButton.ClientID %>');
            var targPos = getPosition(curTarget);
            var scrolltargPos;
            if (detectBrowser())
            {
                scrolltargPos = targPos.y;
            }
            else
            {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
                    (mousePos.x >= targPos.x) &&
                    (mousePos.y >= scrolltargPos))
            {

                var postBackHiddenvalue = document.getElementById('<%=PositionProfileFormEntry_postbackHiddenField.ClientID %>');
                postBackHiddenvalue.value = "1";
                __doPostBack('PositionProfileFormEntry_selectedSegmentsMoveRightImageButton', "OnClick");

                dragObject.style.display = 'none';
            }
            else
            {
                if (dragObject != null)
                {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale()
        {
            var table = sourceTable;

            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++)
            {
                var row = table.rows[i];
                if (detectBrowser())
                {
                    chkbox = row.cells[0].childNodes[1];
                }
                else
                {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked)
                {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e)
        {
            var left = 0;
            var top = 0;

            if (e != null)
            {
                while (e.offsetParent)
                {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }
                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="3">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 52%" class="header_text_bold">
                            <asp:Literal ID="PositionProfileFormEntry_headerLiteral" runat="server" Text="Create Form"></asp:Literal>
                        </td>
                        <td width="48%" align="right">
                            <table width="50%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:Button ID="PositionProfileFormEntry_topCreatePositionProfileButton" runat="server"
                                            Text="Create Position Profile" Visible="false" SkinID="sknButtonId" OnClick="PositionProfileFormEntry_createPositionProfileButton_Click" />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Button ID="PositionProfileFormEntry_topSaveAsButton" runat="server" Text="Save As New"
                                            SkinID="sknButtonId" Visible="false" OnClick="PositionProfileFormEntry_topSaveAsButton_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="PositionProfileFormEntry_topSaveButton" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="PositionProfileFormEntry_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="PositionProfileFormEntry_topPreviewButton" runat="server" Text="Preview"
                                            SkinID="sknButtonId" OnClick="PositionProfileFormEntry_previewButton_Click" />
                                    </td>
                                    <td width="8%" align="left">
                                        <asp:LinkButton ID="PositionProfileFormEntry_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="PositionProfileFormEntry_topResetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="1%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="8%" align="left">
                                        <asp:LinkButton ID="PositionProfileFormEntry_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="PositionProfileFormEntry_topMessageUpdatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileFormEntry_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PositionProfileFormEntry_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                        <td style="width: 5%">
                            <asp:Label ID="PositionProfileFormEntry_nameLabel" runat="server" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                            <span class="mandatory">*</span>
                        </td>
                        <td style="width: 68%">
                            <div style="float: left; width: 93%">
                                <asp:TextBox ID="PositionProfileFormEntry_nameTextBox" runat="server" Width="575px"
                                    MaxLength="50" TabIndex="1"></asp:TextBox>
                            </div>
                            <div style="float: left; width: 5%">
                                <asp:ImageButton ID="PositionProfileFormEntry_nameHelpImageButton" runat="server"
                                    SkinID="sknHelpImageButton" ToolTip="Please enter form name " OnClientClick="javascript:return false;"/>
                            </div>
                        </td>
                        <td style="width: 8%">
                            <asp:Label ID="PositionProfileFormEntry_designedByLabel" runat="server" Text="Designed By"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td style="width: 18%">
                            <asp:TextBox ID="PositionProfileFormEntry_designedByTextBox" runat="server" Width="90%"
                                ReadOnly="True"></asp:TextBox>
                        </td>
                        <td style="width: 2%">
                            <asp:ImageButton ID="PositionProfileFormEntry_designedByIamgeButton" runat="server"
                                SkinID="sknbtnSearchicon" ToolTip="Click here to select the user" />
                            <asp:HiddenField ID="PositionProfileFormEntry_designedBydummyAuthorIdHidenField"
                                runat="server" />
                            <asp:HiddenField ID="PositionProfileFormEntry_designedByHiddenField" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="PositionProfileFormEntry_tagLabel" runat="server" Text="Tags" SkinID="sknLabelFieldHeaderText"></asp:Label>
                            <span id="PositionProfileFormEntry_tagSpan" runat="server" class="mandatory"></span>
                        </td>
                        <td style="width: 68%">
                            <div style="float: left; width: 93%">
                                <asp:TextBox ID="PositionProfileFormEntry_tagTextBox" runat="server" Width="575px"
                                    MaxLength="50" TabIndex="2"></asp:TextBox>
                            </div>
                            <div style="float: left; width: 5%">
                                <asp:ImageButton ID="PositionProfileFormEntry_tagHelpImageButton" runat="server" OnClientClick="javascript:return false;"
                                    SkinID="sknHelpImageButton" ToolTip="Please enter tags to form that represents additonal information related to the form that helps in search " />
                            </div>
                        </td>
                        <td colspan="3" id="PositionProfileFormEntry_globalTD" runat="server">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="PositionProfileFormEntry_isGlobalLabel" runat="server" Text="Global"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <div style="float: left; padding-right: 5px;">
                                            <asp:CheckBox ID="PositionProfileFormEntry_isGlobalCheckBox" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:ImageButton ID="PositionProfileFormEntry_isGlobalHelpImageButton" runat="server" OnClientClick="javascript:return false;"
                                                SkinID="sknHelpImageButton" ToolTip="Check here if the form is global" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_2">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 47%">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg_selectAll">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td style="width: 30%">
                                                            <table cellpadding="0" cellspacing="0" class="header_text_bold">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Literal ID="PositionProfileFormEntry_availableSegmentLiteral" runat="server"
                                                                            Text="Available Segments"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 45%">
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:LinkButton ID="PositionProfileFormEntry_selectAllLinkButton" runat="server"
                                                                SkinID="sknActionLinkButton" Text="Select All" OnClick="PositionProfileFormEntry_selectAllLinkButton_Click"
                                                                ToolTip="Click here to select all the available segments"></asp:LinkButton>
                                                            <span>&nbsp;|&nbsp;</span>
                                                        </td>
                                                        <td style="width: 10%" align="left">
                                                            <asp:LinkButton ID="PositionProfileFormEntry_unSelectAllLinkButton" runat="server"
                                                                SkinID="sknActionLinkButton" Text="Unselect All" OnClick="PositionProfileFormEntry_unSelectAllLinkButton_Click"
                                                                ToolTip="Click here to un select all the selected segments"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_form_bg">
                                        <div style="height: 280px; overflow: auto">
                                            <asp:UpdatePanel ID="PositionProfileFormEntry_availableSegmentsUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="PositionProfileFormEntry_availableSegmentsGridView" runat="server"
                                                        ShowHeader="false" OnRowCommand="PositionProfileFormEntry_availableSegmentsGridView_RowCommand"
                                                        OnRowDataBound="PositionProfileFormEntry_availableSegmentsGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="1%">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="PositionProfileFormEntry_availableSegmentsGridView_moveCheckBox"
                                                                        runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="40px">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="PositionProfileFormEntry_availableSegmentsGridView_moveImageButton"
                                                                        runat="server" SkinID="sknMoveRight_ArrowImageButton" CommandName="PreviewControl"
                                                                        ToolTip="Move segment to selected segments panel" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="1%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="PositionProfileFormEntry_availableSegmentsGridView_viewSegmentImageButton"
                                                                        runat="server" SkinID="sknPreviewImageButton" CommandName="PreviewControl" CommandArgument='<%# Eval("SegmentID") %>'
                                                                        ToolTip="Preview" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="PositionProfileFormEntry_availableSegmentsGridView_segmentIDHiddenField"
                                                                        runat="server" Value='<%# Eval("SegmentID") %>' />
                                                                    <asp:Label ID="PositionProfileFormEntry_availableSegmentsGridView_segmentNameLabel"
                                                                        runat="server" Text='<%# Eval("SegmentName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="PositionProfileFormEntry_availableSegmentsGridView_activeHiddenField"
                                                                        runat="server" Value='<%# Eval("IsActive") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="PositionProfileFormEntry_availableSegmentsSelectedIdHiddenField"
                                                        runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div id="moveDisplayDIV" style="display: none; background-color: Lime" runat="server">
                                                <asp:TextBox ID="PositionProfileFormEntry_noOfSelectedDIVTextBox" runat="server"
                                                    ReadOnly="True"></asp:TextBox>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    <asp:Literal ID="PositionProfileFormEntry_segmentsAvailableLiteral" runat="server"
                                                        Text="Segments"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 1%">
                            &nbsp;
                        </td>
                        <td style="width: 3%">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg" style="height: 357.5px" align="center">
                                        <asp:ImageButton ID="PositionProfileFormEntry_selectedSegmentsMoveRightImageButton"
                                            runat="server" SkinID="sknMultiRight_ArrowImageButton" OnClick="PositionProfileFormEntry_selectedSegmentsMoveRightImageButton_Click"
                                            ToolTip="Move segments to selected segments panel" />
                                        <div class="td_height_5">
                                        </div>
                                        <asp:ImageButton ID="PositionProfileFormEntry_selectedSegmentsMoveLeftImageButton"
                                            runat="server" SkinID="sknMultiLeft_ArrowImageButton" OnClick="PositionProfileFormEntry_selectedSegmentsMoveLeftImageButton_Click"
                                            ToolTip="Remove segments from selected segments panel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 1%">
                            &nbsp;
                        </td>
                        <td style="width: 48%">
                            <asp:UpdatePanel ID="PositionProfileFormEntry_selectedSegmentsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="header_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="header_text_bold">
                                                            <asp:Literal ID="PositionProfileFormEntry_selectedSegmentsLiteral" runat="server"
                                                                Text="Selected Segments"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_form_bg">
                                                <div style="height: 280px; overflow: auto">
                                                    <asp:GridView ID="PositionProfileFormEntry_selectedSegmentsGridView" runat="server"
                                                        ShowHeader="false" OnRowDataBound="PositionProfileFormEntry_selectedSegmentsGridView_RowDataBound"
                                                        OnRowCommand="PositionProfileFormEntry_selectedSegmentsGridView_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="PositionProfileFormEntry_selectedSegmentsGridView_moveCheckBox"
                                                                        runat="server" />
                                                                    <asp:ImageButton ID="PositionProfileFormEntry_selectedSegmentsGridView_moveImageButton"
                                                                        CommandArgument='<%# Eval("SegmentID") %>' runat="server" SkinID="sknDeleteImageButton"
                                                                        CommandName="DeleteSegment" ToolTip="Delete Segment" />
                                                                    <asp:ImageButton ID="PositionProfileFormEntry_selectedSegmentsGridView_moveDownSegmentImageButton"
                                                                        runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                        ToolTip="Move Segment Down" />
                                                                    <asp:ImageButton ID="PositionProfileFormEntry_selectedSegmentsGridView_moveUpImageButton"
                                                                        runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                        ToolTip="Move Segment Up" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="PositionProfileFormEntry_selectedSegmentsGridView_segmentIDHiddenField"
                                                                        runat="server" Value='<%# Eval("SegmentID") %>' />
                                                                    <asp:Label ID="PositionProfileFormEntry_selectedSegmentsGridView_segmentNameLabel"
                                                                        runat="server" Text='<%# Eval("SegmentName") %>'>                                                                     
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="PositionProfileFormEntry_selectedSegmentsGridView_displayOrderHiddenField"
                                                                        runat="server" Value='<%# Eval("DisplayOrder") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="PositionProfileFormEntry_selectedSegmentsGridView_displayLabel" runat="server"
                                                                        Text='<%# Eval("DisplayOrder") %>' Visible="false">                                                                     
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="PositionProfileFormEntry_selectedSegmentsSelectedIdsHiddenField"
                                                        runat="server" />
                                                    <asp:HiddenField ID="PositionProfileFormEntry_selectedSegmentsSelectedNamesHiddenField"
                                                        runat="server" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="header_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="header_text_bold">
                                                            <asp:Literal ID="PositionProfileFormEntry_selectedLiteral" runat="server" Text="Segments"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="PositionProfileFormEntry_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileFormEntry_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label><asp:Label ID="PositionProfileFormEntry_bottomErrorMessageLabel"
                                runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 52%">
                        </td>
                        <td width="48%" align="right">
                            <table width="50%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:Button ID="PositionProfileFormEntry_bottomCreatePositionProfileButton" runat="server"
                                            Text="Create Position Profile" Visible="false" SkinID="sknButtonId" OnClick="PositionProfileFormEntry_createPositionProfileButton_Click" />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Button ID="PositionProfileFormEntry_bottomSaveAsButton" runat="server" Text="Save As New"
                                            SkinID="sknButtonId" Visible="false" OnClick="PositionProfileFormEntry_topSaveAsButton_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="PositionProfileFormEntry_bottomSaveButton" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="PositionProfileFormEntry_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="PositionProfileFormEntry_bottomPreviewButton" runat="server" Text="Preview"
                                            SkinID="sknButtonId" OnClick="PositionProfileFormEntry_previewButton_Click" />
                                    </td>
                                    <td width="8%" align="left">
                                        <asp:LinkButton ID="PositionProfileFormEntry_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="PositionProfileFormEntry_topResetLinkButton_Click" />
                                    </td>
                                    <td width="1%" align="center">
                                        |
                                    </td>
                                    <td width="8%" align="left">
                                        <asp:LinkButton ID="PositionProfileFormEntry_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div style="display: none">
                    <asp:Button ID="PositionProfileFormEntry_hiddenPopupModalButton" runat="server" />
                </div>
                <asp:Panel ID="PositionProfileFormEntry_existingSegmentsPopupPanel" runat="server"
                    Style="display: none" CssClass="popupcontrol_confirm_remove">
                    <uc1:ConfirmMsgControl ID="PositionProfileFormEntry_ConfirmMsgControl" runat="server"
                        OnOkClick="PositionProfileFormEntry_ConfirmMsgControl_okClick" OnCancelClick="PositionProfileFormEntry_ConfirmMsgControl_cancelClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="PositionProfileFormEntry_ConfirmPopupExtender"
                    runat="server" PopupControlID="PositionProfileFormEntry_existingSegmentsPopupPanel"
                    TargetControlID="PositionProfileFormEntry_hiddenPopupModalButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="PositionProfileFormEntry_placeHolderUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="PositionProfileFormEntry_hiddenPlaceHolderButton" runat="server" />
                        </div>
                        <asp:Panel ID="PositionProfileFormEntry_placeHolderPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_segment_preview">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <table width="90%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Label ID="PositionProfileFormEntry_placeHoldercancelLiteral" runat="server"
                                                        Text="Segment Preview"></asp:Label>
                                                </td>
                                                <td style="width: 25%" align="right">
                                                    <asp:ImageButton ID="PositionProfileFormEntry_placeHolderCloseImageButton" runat="server"
                                                        SkinID="sknCloseImageButton" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:PlaceHolder ID="PositionProfileFormEntry_controlsPlaceHolder" runat="server">
                                                    </asp:PlaceHolder>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <asp:LinkButton ID="PositionProfileFormEntry_controlsPlaceHolderCancelButton" runat="server"
                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="PositionProfileFormEntry_placeHolderModalPopupExtender"
                            runat="server" PopupControlID="PositionProfileFormEntry_placeHolderPanel" TargetControlID="PositionProfileFormEntry_hiddenPlaceHolderButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="PositionProfileFormEntry_deleteSegmentUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="PositionProfileFormEntry_deleteSegmentHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="PositionProfileFormEntry_deleteSegmentPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc1:ConfirmMsgControl ID="PositionProfileFormEntry_deleteSegmentConfirmMsgControl"
                                runat="server" OnOkClick="PositionProfileFormEntry_DeleteSegmentConfirmMsgControl_okClick"
                                OnCancelClick="PositionProfileFormEntry_DeleteSegmentConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="PositionProfileFormEntry_deleteSegmentHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="PositionProfileFormEntry_deleteSegmentModalPopupExtender"
                            runat="server" PopupControlID="PositionProfileFormEntry_deleteSegmentPopupPanel"
                            TargetControlID="PositionProfileFormEntry_deleteSegmentHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="PositionProfileFormEntry_postbackHiddenField" runat="server"
                    Value="0" />
                <asp:HiddenField ID="PositionProfileFormEntry_updateFormHiddenField" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PositionProfileFormEntry_PreviewPopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_segment_Controls_preview">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align: center">
                        <tr>
                            <td class="td_height_20">
                                <div style="display: none">
                                    <asp:Button ID="PositionProfileFormEntry_previewPopupHiddenButton" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <table width="95%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 85%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Label ID="PositionProfileFormEntry_formPreviewPlaceHoldercancelLiteral" runat="server"
                                                Text="Form Preview"></asp:Label>
                                        </td>
                                        <td style="width: 15%" align="right">
                                            <asp:ImageButton ID="PositionProfileFormEntry_formPreviewPlaceHolderCloseImageButton"
                                                runat="server" SkinID="sknCloseImageButton" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_20" colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_20" colspan="2">
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; overflow: hidden" class="tab_body_bg">
                                                <tr>
                                                    <td align="left">
                                                        <input type="hidden" runat="server" id="IsDeleteClicked" />
                                                        <ajaxToolKit:TabContainer ID="PositionProfileFormEntry_tabContainer" runat="server"
                                                            ActiveTabIndex="0" AutoPostBack="true" SkinID="sknPositionProfileTabContainer"
                                                            OnActiveTabChanged="PositionProfileFormEntry_tabContainer_ActiveTabChanged">
                                                            <ajaxToolKit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                                                            </ajaxToolKit:TabPanel>
                                                        </ajaxToolKit:TabContainer>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%">
                                                        <uc2:VerticalBackgroundRequirementControl ID="PositionProfileFormEntry_verticalBackgroundRequirementControl"
                                                            runat="server" Visible="false" />
                                                        <uc3:TechnicalSkillRequirementControl ID="PositionProfileFormEntry_technicalSkillRequirementControl"
                                                            runat="server" Visible="false" />
                                                        <uc4:RoleRequirementControl ID="PositionProfileFormEntry_roleRequirementControl"
                                                            runat="server" Visible="false" />
                                                        <uc5:EducationRequirementControl ID="PositionProfileFormEntry_educationRequirementControl"
                                                            runat="server" Visible="false" />
                                                        <uc6:ClientPositionDetailsControl ID="PositionProfileFormEntry_clientPositionDetailsControl"
                                                            runat="server" Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_20">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:LinkButton ID="PositionProfileFormEntry_previewFormCancelLinkButton" runat="server"
                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="PositionProfileFormEntry_previewFormPopupExtender"
                    runat="server" PopupControlID="PositionProfileFormEntry_PreviewPopupPanel" TargetControlID="PositionProfileFormEntry_previewPopupHiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
