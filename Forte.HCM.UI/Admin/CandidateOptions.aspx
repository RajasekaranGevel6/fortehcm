<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master" 
    CodeBehind="CandidateOptions.aspx.cs" Inherits="Forte.HCM.UI.Admin.CandidateOptions"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master"  %>
<asp:Content ID="CandidateOptions_bodyContent" runat="server" ContentPlaceHolderID="SiteAdminMaster_body">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CandidateOptions_headerLiteral" runat="server" Text="Candidate Options"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="CandidateOptions_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="CandidateOptions_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CandidateOptions_topResetLinkButton" runat="server" Text="Reset"
                                            OnClick="CandidateOptions_resetLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CandidateOptions_topCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align" valign="middle">
                <asp:UpdatePanel ID="CandidateOptions_messageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CandidateOptions_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateOptions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CandidateOptions_selfAdminTestSettingsLabel" runat="server" Text="Self Admin Test Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_maxNoOfRetakesUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_maxNoOfRetakesLabel" Text="Max No Of Retakes" runat="server"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_maxNoOfRetakesHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the maximum number of retakes for a self admin test" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_maxNoOfRetakesTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_maxNoOfRetakesRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_maxNoOfRetakesRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_maxNoOfRetakesMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_maxNoOfRetakesTextBox" Mask="99"
                                                            MessageValidatorTip="true" InputDirection="LeftToRight" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_maxNoOfTestsUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_maxNoOfTestsLabel" runat="server" Text="Max No Of Tests (Per Month)"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_maxNoOfTestsHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the maximum number of self admin tests created per month" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_maxNoOfTestsTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_maxNoOfTestsRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_maxNoOfTestsRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_maxNoOfTestsMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_maxNoOfTestsTextBox" Mask="99"
                                                            MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AutoComplete="false" AcceptNegative="None"
                                                            ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_sessionExpiryDaysUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_sessionExpiryDaysLabel" Text="Session Expiry Period (In Days)"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_sessionExpiryDaysHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the number of days in which a candidate session will expire from the creation date" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_sessionExpiryDaysTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_sessionExpiryDaysRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_sessionExpiryDaysRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_sessionExpiryDaysMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_sessionExpiryDaysTextBox" Mask="99"
                                                            MessageValidatorTip="true" InputDirection="LeftToRight" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CandidateOptions_resumeSettingsLabel" runat="server" Text="Resume Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_resumeExpiryDaysUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_resumeExpiryDaysLabel" Text="Resume Expiry Period (In Days)"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_resumeExpiryDaysHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the resume expiry in number of days after upload" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_resumeExpiryDaysTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_resumeExpiryDaysRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_resumeExpiryDaysRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_resumeExpiryDaysMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_resumeExpiryDaysTextBox" Mask="9999"
                                                            MessageValidatorTip="true" InputDirection="LeftToRight" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CandidateOptions_gridPageSizeSettingsLabel" runat="server" Text="Grid Page Size Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_activitiesGridPageSizeUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_activitiesGridPageSizeLabel" Text="Activities Grid Page Size"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_activitiesGridPageSizeHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the grid page size for activities grid" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_activitiesGridPageSizeTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_activitiesGridPageSizeRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_activitiesGridPageSizeRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_activitiesGridPageSizeMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_activitiesGridPageSizeTextBox"
                                                            Mask="99" MessageValidatorTip="true" InputDirection="LeftToRight" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_searchTestGridPageSizeUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_searchTestGridPageSizeLabel" runat="server" Text="Search Test Grid Page Size"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_searchTestGridPageSizeHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the grid page size for search test grid" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_searchTestGridPageSizeTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_searchTestGridPageSizeRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_searchTestGridPageSizeRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_searchTestGridPageSizeMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_searchTestGridPageSizeTextBox"
                                                            Mask="99" MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AutoComplete="false" AcceptNegative="None"
                                                            ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                     <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="CandidateOptions_selfAdminTestQuestionSettingsLabel" runat="server" Text="Self Admin Test Question Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_minSelfAdminTestQuestionUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_minSelfAdminTestQuestionLabel" Text="Min Questions For Self Admin Test"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_minSelfAdminTestQuestionHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the min questions for self admin test" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_minSelfAdminTestQuestionTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_minSelfAdminTestQuestionRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_minSelfAdminTestQuestionRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_minSelfAdminTestQuestionMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_minSelfAdminTestQuestionTextBox"
                                                            Mask="99" MessageValidatorTip="true" InputDirection="LeftToRight" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="CandidateOptions_maxSelfAdminTestQuestionUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 25%;">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:Label ID="CandidateOptions_maxSelfAdminTestQuestionLabel" runat="server" Text="Max Questions For Self Admin Test"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="CandidateOptions_maxSelfAdminTestQuestionHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the max questions for self admin test" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="CandidateOptions_maxSelfAdminTestQuestionTextBox" runat="server" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="CandidateOptions_maxSelfAdminTestQuestionRestoreLinkButton" runat="server"
                                                                Text="Restore Defaults" ToolTip="Click here to restore the default value" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateOptions_maxSelfAdminTestQuestionRestoreLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <ajaxToolKit:MaskedEditExtender ID="CandidateOptions_maxSelfAdminTestQuestionMaskedEditExtender"
                                                            runat="server" TargetControlID="CandidateOptions_maxSelfAdminTestQuestionTextBox"
                                                            Mask="99" MessageValidatorTip="true" InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus"
                                                            OnInvalidCssClass="MaskedEditError" AutoComplete="false" AcceptNegative="None"
                                                            ErrorTooltipEnabled="True" />
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topResetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="msg_align" valign="middle">
                <asp:UpdatePanel ID="CandidateOptions_bottomMessageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="CandidateOptions_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateOptions_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="CandidateOptions_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="CandidateOptions_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="4" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="CandidateOptions_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                OnClick="CandidateOptions_saveButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="CandidateOptions_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="CandidateOptions_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="CandidateOptions_bottomCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
