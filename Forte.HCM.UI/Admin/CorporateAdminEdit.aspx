﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorporateAdminEdit.aspx.cs"
    Inherits="Forte.HCM.UI.Admin.CorporateAdminEdit" MasterPageFile="~/MasterPages/SiteAdminMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<asp:Content ID="CorporateAdminEdit_bodyContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CorporateAdminEdit_headerLiteral" runat="server" Text="Edit Corporate Admin"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="CorporateAdminEdit_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="CorporateAdminEdit_topSaveButton_Click" /> &nbsp;
                                    </td>
                                    
                                    <td>
                                        <asp:LinkButton ID="CorporateAdminEdit_showRolesLinkButton" runat="server" Text="Show Roles"
                                            SkinID="sknActionLinkButton" OnClick="CorporateAdminEdit_showRolesLinkButton_Click"></asp:LinkButton>  &nbsp;
                                    </td>
                                   
                                    <td align="center" class="link_button">
                                        | &nbsp;
                                    </td>
                                    
                                    <td>
                                        <asp:LinkButton ID="CorporateAdminEdit_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CorporateAdminEdit_topResetLinkButton_Click"></asp:LinkButton>&nbsp;
                                    </td>
                                     
                                    <td align="center" class="link_button">
                                       |&nbsp;
                                    </td>
                                      
                                    <td>
                                        <asp:LinkButton ID="CorporateAdminEdit_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="CorporateAdminEdit_isMaximizedHiddenField" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CorporateAdminEdit_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CorporateAdminEdit_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="CorporateAdminEdit_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div id="CorporateAdminEdit_SummaryDIV" runat="server">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="td_height_8">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="header_bg" align="center" colspan="4">
                                                        <asp:Literal ID="CorporateAdminEdit_corporateAdminDetailsLiteral" runat="server"
                                                            Text="Corporate Admin Details"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tab_body_bg">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="CorporateAdminEdit_editUserNameLabel" runat="server" Text="First Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class="mandatory">*</span>
                                                                </td>
                                                                <td style="width: 25%">
                                                                    <asp:TextBox ID="CorporateAdminEdit_editUserTextbox" runat="server" Width="175px"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="CorporateAdminEdit_editLastNameLabel" runat="server" Text="Last Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class="mandatory">*</span>
                                                                </td>
                                                                <td style="width: 35%">
                                                                    <asp:TextBox ID="CorporateAdminEdit_editUserLastnameTextbox" runat="server" Width="175px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="CorporateAdminEdit_editUserPhoneNumberLabel" runat="server" Text="Phone Number"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class="mandatory">*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="CorporateAdminEdit_editUserPhoneNumberTextbox" runat="server" Width="175px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="CorporateAdminEdit_editUserCompanyLabel" runat="server" Text="Company Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class="mandatory">*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="CorporateAdminEdit_editUserCompanyTextbox" runat="server" Width="175px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="CorporateAdminEdit_editUserTitleLabel" runat="server" Text="Title"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="CorporateAdminEdit_editUserTitleTextBox" runat="server" Width="175px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="CorporateAdminEdit_editUserNoOfUsersLabel" runat="server" Text="Number of Users"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:HiddenField ID="CorporateAdminEdit_usersGridView_noOfUsersHiddenField" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="CorporateAdminEdit_editUserNoOfUsersTextBox" runat="server" Width="175px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="CorporateAdminEdit_editUserActiveNoOfUsersLabel" runat="server" Text="Active Number of Users    "
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="CorporateAdminEdit_editUserActiveNoOfUserLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="TR_ROLES" visible="false">
                                                                <td>
                                                                    <asp:Label ID="CorporateAdminEdit_editUserRolesLabel" runat="server" Text="Roles"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class="mandatory">*</span>
                                                                </td>
                                                                <td class="checkbox_list_bg" align="left" colspan="4">
                                                                    <div>
                                                                        <asp:CheckBoxList ID="CorporateAdminEdit_editUserRolesCheckBoxList" runat="server"
                                                                            RepeatDirection="Horizontal" RepeatColumns="5" TextAlign="Right" DataTextField="RoleName"
                                                                            DataValueField="RoleID" Width="100%" TabIndex="5">
                                                                        </asp:CheckBoxList>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="header_bg">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Literal ID="CorporateAdminEdit_corporateUserLiteral" runat="server" Text="Corporate User Details"></asp:Literal>
                                                                                    </td>
                                                                                    <td align="right" style="width: 50%">
                                                                                        <span id="CorporateAdminEdit_UparrowSpan" runat="server" style="display: block;">
                                                                                            <asp:Image ID="CorporateAdminEdit_UpArrow" runat="server" SkinID="sknMinimizeImage" />
                                                                                        </span><span id="CorporateAdminEdit_DownarrowSpan" runat="server" style="display: none;">
                                                                                            <asp:Image ID="CorporateAdminEdit_DownArrow" runat="server" SkinID="sknMaximizeImage" />
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="grid_body_bg" colspan="2">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div style="height: 300px; overflow: auto;" runat="server" id="CorporateAdminEdit_testDiv">
                                                                                            <asp:GridView ID="CorporateAdminEdit_testGridView" runat="server" AllowSorting="True"
                                                                                                AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                                Width="100%">
                                                                                                <%--<RowStyle CssClass="grid_alternate_row" />
                                                                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                                <HeaderStyle CssClass="grid_header_row" />--%>
                                                                                                <Columns>
                                                                                                    <asp:BoundField HeaderText="User_ID" DataField="UserID" Visible="false" />
                                                                                                    <asp:BoundField HeaderText="User Name" DataField="UserEmail" />
                                                                                                    <asp:BoundField HeaderText="First Name" DataField="FirstName" />
                                                                                                    <asp:BoundField HeaderText="Last Name" DataField="LastName" />
                                                                                                    <asp:BoundField HeaderText="Active" DataField="ActiveUser" />
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CorporateAdminEdit_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CorporateAdminEdit_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="CorporateAdminEdit_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="CorporateAdminEdit_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="CorporateAdminEdit_topSaveButton_Click" />&nbsp;
                                    </td>
                                      
                                    <td>
                                        <asp:LinkButton ID="CorporateAdminEdit_bottomShowRolesLinkButton" runat="server"
                                            Text="Show Roles" SkinID="sknActionLinkButton" OnClick="CorporateAdminEdit_showRolesLinkButton_Click"></asp:LinkButton>&nbsp;
                                    </td>
                                      
                                    <td align="center" class="link_button">
                                        | &nbsp;
                                    </td>
                                     
                                    <td>
                                        <asp:LinkButton ID="CorporateAdminEdit_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CorporateAdminEdit_topResetLinkButton_Click"></asp:LinkButton> &nbsp;
                                        
                                    </td>
                                   
                                    <td align="center" class="link_button">
                                        |&nbsp;
                                    </td>
                                    
                                    <td>
                                        <asp:LinkButton ID="CorporateAdminEdit_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </table>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
