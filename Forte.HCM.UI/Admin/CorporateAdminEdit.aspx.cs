﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CorporateAdminEdit.cs
// File that represents the user interface to 
// edit the corporate admin information.

#endregion Header                                                              

#region Namespace                                                              

using System;
using System.Collections.Generic;
using Forte.HCM.DataObjects;
using Forte.HCM.BL;
using Forte.HCM.UI.Common;
using Forte.HCM.Trace;
using Forte.HCM.Support;

#endregion Namespace

namespace Forte.HCM.UI.Admin
{
    public partial class CorporateAdminEdit : PageBase
    {
        #region Private Variables                                              

        private short user_ID = 0;

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "335px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion

        #region Events                                                         

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt16(Request.QueryString["userID"]) != 0)
                user_ID = Convert.ToInt16(Request.QueryString["userID"]);

            ResetMessages();

            Master.SetPageCaption("Corporate Admin Edit");

            CheckAndSetExpandOrRestore();

            if (!IsPostBack)
            {
               
                LoadValues(user_ID);
                BindCorporateChildUsers(user_ID);
            }
            CorporateAdminEdit_UparrowSpan.Attributes.Add("onclick",
          "ExpandOrRestore('" +
          CorporateAdminEdit_testDiv.ClientID + "','" +
          CorporateAdminEdit_SummaryDIV.ClientID + "','" +
          CorporateAdminEdit_UparrowSpan.ClientID + "','" +
          CorporateAdminEdit_DownarrowSpan.ClientID + "','" +
          CorporateAdminEdit_isMaximizedHiddenField.ClientID + "','" +
          RESTORED_HEIGHT + "','" +
          EXPANDED_HEIGHT + "')");

            CorporateAdminEdit_DownarrowSpan.Attributes.Add("onclick",
            "ExpandOrRestore('" +
            CorporateAdminEdit_testDiv.ClientID + "','" +
            CorporateAdminEdit_SummaryDIV.ClientID + "','" +
            CorporateAdminEdit_UparrowSpan.ClientID + "','" +
            CorporateAdminEdit_DownarrowSpan.ClientID + "','" +
            CorporateAdminEdit_isMaximizedHiddenField.ClientID + "','" +
            RESTORED_HEIGHT + "','" +
            EXPANDED_HEIGHT + "')");

        }

        /// <summary>
        /// Handles the Click event of the CorporateAdminEdit_topResetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateAdminEdit_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateAdminEdit_topErrorMessageLabel,
            CorporateAdminEdit_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }
        }

        /// <summary>
        /// Handles the Click event of the CorporateAdminEdit_showRolesLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateAdminEdit_showRolesLinkButton_Click(object sender, EventArgs e)
        {
            if ((CorporateAdminEdit_showRolesLinkButton.Text == "Show Roles") &&
                (CorporateAdminEdit_bottomShowRolesLinkButton.Text == "Show Roles"))
            {
                TR_ROLES.Visible = true;

                CorporateAdminEdit_showRolesLinkButton.Text = "Hide Roles";

                CorporateAdminEdit_bottomShowRolesLinkButton.Text = "Hide Roles";
            }
            else
            {
                TR_ROLES.Visible = false;

                CorporateAdminEdit_showRolesLinkButton.Text = "Show Roles";

                CorporateAdminEdit_bottomShowRolesLinkButton.Text = "Show Roles";
            }
        }
        /// <summary>
        /// Handles the Click event of the CorporateAdminEdit_topSaveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateAdminEdit_topSaveButton_Click(object sender, EventArgs e)
        {
            ResetMessages();

            if (CorporateAdminEdit_editUserTextbox.Text.Length == 0)
            {
                base.ShowMessage(CorporateAdminEdit_topErrorMessageLabel,
           CorporateAdminEdit_bottomErrorMessageLabel,
                    "Enter first name");


                return;
            }
            if (CorporateAdminEdit_editUserCompanyTextbox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CorporateAdminEdit_topErrorMessageLabel,
          CorporateAdminEdit_bottomErrorMessageLabel,
                    "Enter company name");


                return;
            }
            if (CorporateAdminEdit_editUserLastnameTextbox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CorporateAdminEdit_topErrorMessageLabel,
          CorporateAdminEdit_bottomErrorMessageLabel,
                    "Enter Last name");


                return;
            }

            if (CorporateAdminEdit_editUserPhoneNumberTextbox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CorporateAdminEdit_topErrorMessageLabel,
          CorporateAdminEdit_bottomErrorMessageLabel,
                    "Enter Phone number name");


                return;
            }

            if (CorporateAdminEdit_editUserNoOfUsersTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CorporateAdminEdit_topErrorMessageLabel,
          CorporateAdminEdit_bottomErrorMessageLabel,
                    "Enter number of users");


                return;
            }


            //int id = int.Parse(CorporateAdminEdit_editUserHiddenField.Value);
            UserRegistrationInfo userEditInfo = new UserRegistrationInfo();
            userEditInfo.UserID = user_ID;
            userEditInfo.FirstName = CorporateAdminEdit_editUserTextbox.Text;
            userEditInfo.LastName = CorporateAdminEdit_editUserLastnameTextbox.Text;
            userEditInfo.Phone = CorporateAdminEdit_editUserPhoneNumberTextbox.Text;
            userEditInfo.Company = CorporateAdminEdit_editUserCompanyTextbox.Text;
            userEditInfo.Title = CorporateAdminEdit_editUserTitleTextBox.Text;
            if (CorporateAdminEdit_editUserNoOfUsersTextBox.Text != null)
            {
                if (Convert.ToInt16(CorporateAdminEdit_editUserNoOfUsersTextBox.Text) <
                    Convert.ToInt16(CorporateAdminEdit_editUserActiveNoOfUserLabel.Text))
                {
                    base.ShowMessage(CorporateAdminEdit_topErrorMessageLabel,
            CorporateAdminEdit_bottomErrorMessageLabel,
                        "Number of users cannot be less than active number of users. " +
                    "Please contact Tenant admin to deactivate user(s) ");

                    return;

                }
                else
                    userEditInfo.NoOfUsers = Convert.ToInt16(CorporateAdminEdit_editUserNoOfUsersTextBox.Text);
            }
            for (int j = 0; j < CorporateAdminEdit_editUserRolesCheckBoxList.Items.Count; j++)
            {
                if ((CorporateAdminEdit_editUserRolesCheckBoxList.Items[j].Selected == true) &&
                    (CorporateAdminEdit_editUserRolesCheckBoxList.Items[j].Enabled == true))
                {
                    userEditInfo.SelectedRoles = userEditInfo.SelectedRoles + CorporateAdminEdit_editUserRolesCheckBoxList.Items[j].Value + ",";

                }
            }
            if (userEditInfo.SelectedRoles != null)
                userEditInfo.SelectedRoles = userEditInfo.SelectedRoles.TrimEnd(',');

            else
                userEditInfo.SelectedRoles = userEditInfo.SelectedRoles;

            //if (CorporateAdminEdit_editUserIsActiveCheckBox.Checked == true)
            //    userEditInfo.IsActive = 1;
            //else
            //    userEditInfo.IsActive = 0;

            new UserRegistrationBLManager().UpdateUserInfo(userEditInfo);

            base.ShowMessage(CorporateAdminEdit_topSuccessMessageLabel,
            CorporateAdminEdit_bottomSuccessMessageLabel,
               "User information has been updated successfully");
        }


        #endregion

        #region Private Methods                                                
        /// <summary>
        /// Checks the and set expand or restore.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {

            if (!Utility.IsNullOrEmpty(CorporateAdminEdit_isMaximizedHiddenField.Value) &&
               CorporateAdminEdit_isMaximizedHiddenField.Value == "Y")
            {
                CorporateAdminEdit_SummaryDIV.Style["display"] = "none";
                CorporateAdminEdit_UparrowSpan.Style["display"] = "block";
                CorporateAdminEdit_DownarrowSpan.Style["display"] = "none";
                CorporateAdminEdit_testDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CorporateAdminEdit_SummaryDIV.Style["display"] = "block";
                CorporateAdminEdit_UparrowSpan.Style["display"] = "none";
                CorporateAdminEdit_DownarrowSpan.Style["display"] = "block";
                CorporateAdminEdit_testDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Binds the corporate child users.
        /// </summary>
        /// <param name="user_ID">The user_ ID.</param>
        private void BindCorporateChildUsers(short user_ID)
        {
            List<UserRegistrationInfo> userRegistrationInfo = null;
            try
            {
                userRegistrationInfo = new UserRegistrationBLManager().GetCorporateChileUsers(user_ID);

                CorporateAdminEdit_testGridView.DataSource = userRegistrationInfo;
                CorporateAdminEdit_testGridView.DataBind();
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }
      

        /// <summary>
        /// Loads the values.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        private void LoadValues(short userID)
        {

            UserRegistrationInfo userEditInfo = new AdminBLManager().GetSubscriptionUserDetails(userID);
            CorporateAdminEdit_editUserTextbox.Text = userEditInfo.FirstName;
            CorporateAdminEdit_editUserLastnameTextbox.Text = userEditInfo.LastName;
            CorporateAdminEdit_editUserPhoneNumberTextbox.Text = userEditInfo.Phone;
            CorporateAdminEdit_editUserCompanyTextbox.Text = userEditInfo.Company;
            CorporateAdminEdit_editUserTitleTextBox.Text = userEditInfo.Title;
            CorporateAdminEdit_editUserNoOfUsersTextBox.Text = userEditInfo.NoOfUsers.ToString();
            CorporateAdminEdit_usersGridView_noOfUsersHiddenField.Value = userEditInfo.NoOfUsers.ToString();
            CorporateAdminEdit_editUserActiveNoOfUserLabel.Text = "     "+userEditInfo.ActiveNoOfUsers.ToString();
           // CorporateAdminEdit_editUserIsActiveCheckBox.Checked = false;
            //if (userEditInfo.IsActive == 1)
            //    CorporateAdminEdit_editUserIsActiveCheckBox.Checked = true;
            //    CorporateAdminEdit_editUserHiddenField.Value = e.CommandArgument.ToString();
            BindSelectedRoles(userID);  
        }
        /// <summary>
        /// Binds the selected roles.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        private void BindSelectedRoles(short userID)
        {

            List<Roles> userEditInfo = new AdminBLManager().GetCorporateAdminAssignedRoles(userID);

            //CorporateAdminEdit_editUserRolesCheckBoxList.Items.Clear();
            CorporateAdminEdit_editUserRolesCheckBoxList.DataSource = null;
            CorporateAdminEdit_editUserRolesCheckBoxList.DataBind();
            CorporateAdminEdit_editUserRolesCheckBoxList.DataSource = userEditInfo;
            CorporateAdminEdit_editUserRolesCheckBoxList.DataBind();

            for (int i = 0; i < userEditInfo.Count; i++)
            {
                if (userEditInfo[i].IsSelected == "Y")
                {
                    CorporateAdminEdit_editUserRolesCheckBoxList.Items[i].Enabled = false;
                    CorporateAdminEdit_editUserRolesCheckBoxList.Items[i].Selected = true;
                }
            }
        }
       

        /// <summary>
        /// Resets the messages.
        /// </summary>
        private void ResetMessages()
        {
            CorporateAdminEdit_topSuccessMessageLabel.Text = "";
            CorporateAdminEdit_topErrorMessageLabel.Text = "";
            CorporateAdminEdit_bottomSuccessMessageLabel.Text = "";
            CorporateAdminEdit_bottomErrorMessageLabel.Text = "";
        }

        #endregion

        #region Protected Overridden Methods                                   

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
        
    }
}