﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ManageCertificate.cs
// File that represents the user interface for certificate details.
// This page will provide the feature for the users to add or
// modify the certificate information.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// managing certificates. This class provides the methods to  add/edit 
    /// the certificate information.
    /// </summary>
    /// <remarks>
    /// This class is inherited from the <see cref="PageBase"/> class.
    /// </remarks>
    public partial class ManageCertificates : PageBase
    {
        #region Private Members                                                

        /// <summary>
        /// Member that contains the boolean representation which will be used
        /// in certificate detail gridview radio button.
        /// </summary>
        private bool isChecked = false;

        #endregion Private Members                                             

        #region Event Handler                                                  

        /// <summary>
        /// Handler that will perform to load the certification details
        /// whenever the page gets loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadValues();
                }

                Master.SetPageCaption("Manage Certificates");
                Page.Form.DefaultButton = ManageCertificates_topRefreshButton.UniqueID;

                ManageCertificates_addCertificateLinkButton.Attributes.Add
                    ("onclick", "javascript:return ShowCertificatePopup('ADD');");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ManageCertificates_topErrorMessageLabel,
                    ManageCertificates_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the row is binding the information.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void ManageCertificates_certificatesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                HiddenField ManageCertificates_certificationIDHiddenFiled = (HiddenField)
                    e.Row.FindControl("ManageCertificates_certificationIDHiddenFiled");
                LinkButton ManageCertificates_editLinkButton = (LinkButton)
                    e.Row.FindControl("ManageCertificates_editLinkButton");
                HiddenField ManageCertificates_htmlTextHiddenField = (HiddenField)
                    e.Row.FindControl("ManageCertificates_htmlTextHiddenField");
                RadioButton ManageCertificates_certificatePreviewRadioButton = (RadioButton)
                    e.Row.FindControl("ManageCertificates_certificatePreviewRadioButton");

                ViewState["HTML_TEXT"] = ManageCertificates_htmlTextHiddenField.Value.Trim();

                // Make first row radio button status is checked and show the corresponding
                // certificate template.
                if (isChecked == false)
                {
                    isChecked = true;
                    ManageCertificates_certificatePreviewRadioButton.Checked = true;
                    ManageCertificates_certificateImage.ImageUrl
                        = @"~/Common/ImageHandler.ashx?ImageID="
                            + Convert.ToInt32(ManageCertificates_certificatePreviewRadioButton.GroupName.ToString())
                            + "&isThumb=0&source=CERTIFICATE_TEMPLATE";

                    ManageCertificates_certificateImage.Visible = true;
                }

                ManageCertificates_editLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowCertificatePopup('" +
                    ManageCertificates_certificationIDHiddenFiled.Value + "');");

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ManageCertificates_topErrorMessageLabel,
                    ManageCertificates_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will reload the updated certificate details.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ManageCertificates_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ManageCertificates_topErrorMessageLabel,
                    ManageCertificates_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the user clicks the reset linkbutton.
        /// It resets the page information as the page was loaded at first time.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that cotnains the event data.
        /// </param>
        protected void ManageCertificates_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ManageCertificates_topErrorMessageLabel,
                    ManageCertificates_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the radio button is clicked from the gridview.
        /// During that time, it will show the corresponding certificate format in the
        /// provided image control.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CertificateFormatRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                string CurentId = ((RadioButton)sender).ClientID;
                for (int i = 0; i < ManageCertificates_certificatesGridView.Rows.Count; i++)
                {
                    if (((RadioButton)ManageCertificates_certificatesGridView.Rows[i].
                        FindControl("ManageCertificates_certificatePreviewRadioButton")).ClientID == CurentId)
                        continue;
                    if (!(((RadioButton)ManageCertificates_certificatesGridView.Rows[i].
                        FindControl("ManageCertificates_certificatePreviewRadioButton")).Checked))
                        continue;
                    ((RadioButton)ManageCertificates_certificatesGridView.Rows[i]
                        .FindControl("ManageCertificates_certificatePreviewRadioButton")).Checked = false;
                }

                RadioButton radioButton = (RadioButton)sender;

                if (radioButton.Checked)
                {
                    ManageCertificates_certificateImage.ImageUrl
                        = @"~/Common/ImageHandler.ashx?ImageID="
                        + Convert.ToInt32(radioButton.GroupName.ToString()) + "&isThumb=0&source=CERTIFICATE_TEMPLATE";

                    ManageCertificates_certificateImage.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ManageCertificates_topErrorMessageLabel,
                     ManageCertificates_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler                                               

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            List<CertificationDetail> certificationDetail = new AdminBLManager().GetCertificateDetails();

            if (certificationDetail != null)
            {
                ManageCertificates_certificatesGridView.DataSource = certificationDetail;
                ManageCertificates_certificatesGridView.DataBind();
            }
            else
                base.ShowMessage(ManageCertificates_topErrorMessageLabel,
                    ManageCertificates_bottomErrorMessageLabel, "No data found to display");
        }

        #endregion Protected Overridden Methods                                
    }
}