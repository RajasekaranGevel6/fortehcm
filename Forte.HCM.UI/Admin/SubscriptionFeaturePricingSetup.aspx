﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionFeaturePricingSetup.aspx.cs"
    Inherits="Forte.HCM.UI.Admin.SubscriptionFeaturePricingSetup" MasterPageFile="~/MasterPages/SiteAdminMaster.Master" %>

<%@ Register Src="../CommonControls/AddFeatureSubscriptionType.ascx" TagName="AddFeatureSubscriptionType"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/SubscriptionPricingPreviewControl.ascx" TagName="SubscriptionPricingPreviewControl"
    TagPrefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteAdminMaster_body" runat="server">
    <asp:UpdatePanel ID="Forte_SubscriptionFeaturePricingSetup_UpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td>
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="header_bg" align="right">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" class="header_text_bold">
                                                <asp:Literal ID="Forte_SubscriptionFeaturePricingSetup_subsciptionPricingSetUpHeaderLiteral"
                                                    runat="server" Text="Subscription Feature Pricing Setup"></asp:Literal>
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_topSaveButton" runat="server"
                                                                Text="Save" SkinID="sknButtonId" OnClick="Forte_SubscriptionFeaturePricingSetup_SaveButton_Click" />&nbsp;
                                                            <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_topPublishButton" SkinID="sknButtonId"
                                                                runat="server" Text="Publish" OnClick="Forte_SubscriptionFeaturePricingSetup_PublishButton_Click"
                                                                Visible="false" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_SubscriptionFeaturePricingSetup_topResetLinkButton" runat="server"
                                                                Text="Reset" OnClick="ResetLinkButtonclick" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="Forte_SubscriptionFeaturePricingSetup_topCancelLinkButton" runat="server"
                                                                Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage" EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage" EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Literal ID="Forte_SubscriptionFeaturePricingSetup_subscriptionTypeHeaderLiteral"
                                                    runat="server" Text="Subscription Types"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <table width="60%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_subscriptionTypeHeadLabel" runat="server"
                                                                            SkinID="sknLabelFieldHeaderText" Text="Subscription Type"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 55%">
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:DropDownList ID="Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList"
                                                                                runat="server" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="Forte_SubscriptionFeaturePricingSetup_subscriptionTypeDropDownList_SelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="Forte_SubscriptionFeaturePricingSetup_subscriptionTypeHelpImageButton" OnClientClick="javascript:return false;"
                                                                                runat="server" SkinID="sknHelpImageButton" ToolTip="Select the subscription type" />
                                                                        </div>
                                                                    </td>
                                                                    <td style="width: 25%">
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_publishStatusLabel" runat="server"
                                                                                        SkinID="sknLabelFieldText"></asp:Label>&nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Image runat="server" ID="Forte_SubscriptionFeaturePricingSetup_publishStatusImage" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Literal ID="Forte_SubscriptionFeaturePricingSetup_featuresAndPricingHeadLiteral"
                                                    runat="server" Text="Feature & Pricing"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <div id="Forte_SubscriptionFeaturePricingSetup_featuresDiv" style="overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <input type="hidden" runat="server" id="Forte_SubscriptionFeaturePricing_deleteSubsriptionFeatureIdHidden" />
                                                                        <asp:GridView ID="Forte_SubscriptionFeaturePricingSetup_featureTypeGridView" runat="server"
                                                                            AutoGenerateColumns="false" SkinID="sknNewGridView" Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemStyle Width="8%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="Forte_SubscriptionFeaturePricingSetup_deleteFeatureTypeImageButton"
                                                                                            runat="server" ToolTip="Delete Pricing" SkinID="sknDeleteImageButton" CommandName="Delete Pricing"
                                                                                            CommandArgument='<%# Eval("SubscriptionFeatureId") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Image ID="Forte_SubscriptionFeaturePricidingSetUp_featureImage" runat="server"
                                                                                            SkinID="sknFeatureTypeImage" />
                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_rowNumberHeaderLabel" runat="server"
                                                                                            Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_rowNumberDotHeaderLabel" runat="server"
                                                                                            SkinID="sknLabelFieldText" Text="."></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <div style="float: left; width: 100%;">
                                                                                            <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                                <tr>
                                                                                                    <td style="height: 5px;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 30%" align="left">
                                                                                                        <%-- <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_featureNameHeaderLabel" runat="server"
                                                                                                            Text="Feature Name" SkinID="sknLabelFieldHeaderText"></asp:Label>--%>
                                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_featureNameLabel" runat="server"
                                                                                                            SkinID="sknLabelFieldText" Width="100%" Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_featureIdHiddenLabel" runat="server"
                                                                                                            Visible="false" Text='<%# Eval("SubscriptionFeatureId") %>'></asp:Label>
                                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_isNotAppliationLabel" runat="server"
                                                                                                            Text='<%# Eval("IsNotApplicable") %>' Visible="false"></asp:Label>
                                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_isUnlimitedLabel" runat="server"
                                                                                                            Text='<%# Eval("IsUnlimited") %>' Visible="false"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 40%">
                                                                                                        <asp:TextBox ID="Forte_SubscriptionFeaturePricingSetup_featureDefaultValueTextBox"
                                                                                                            runat="server" Width="70%" Text='<%# Eval("FeatureValue") %>'></asp:TextBox>
                                                                                                        &nbsp;
                                                                                                        <asp:LinkButton ID="Forte_SubscriptionFeaturePricingSetup_restoreDefaultValueLinkButton"
                                                                                                            runat="server" SkinID="sknActionLinkButton" Text="">
                                                                                                            <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_restoreDefaultValueLabel" runat="server"
                                                                                                                Text="Restore Default" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                        </asp:LinkButton>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 5%">
                                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_featureUnitHeaderLabel" runat="server"
                                                                                                            Text="Unit" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 25%">
                                                                                                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_featureUnitValue" runat="server"
                                                                                                            Text='<%# Eval("FeatureUnit") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_height_5">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="height: 8px;">
                                                                                                        <asp:CheckBox ID="Forte_SubscriptionFeaturePricingSetup_isUnlimitedCheckBox" runat="server"
                                                                                                            Text="Unlimited" />
                                                                                                        <asp:CheckBox ID="Forte_SubscriptionFeaturePricingSetup_isNotApplicableCheckBox"
                                                                                                            runat="server" Text="Not Applicable" />
                                                                                                        <input type="hidden" id="Forte_SubscriptionFeaturePricingSetup_defaultValueHidden"
                                                                                                            runat="server" value='<%# Eval("DefaultValue") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="height: 8px;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage" EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_SubscriptionFeaturePricingSetup_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage" EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg" align="right">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_bottonSaveButton" runat="server"
                                                    Text="Save" SkinID="sknButtonId" OnClick="Forte_SubscriptionFeaturePricingSetup_SaveButton_Click" />&nbsp;
                                                <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_bottomPublishButton" SkinID="sknButtonId"
                                                    runat="server" Text="Publish" OnClick="Forte_SubscriptionFeaturePricingSetup_PublishButton_Click"
                                                    Visible="false" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Forte_SubscriptionFeaturePricingSetup_bottonResetLinkButton"
                                                    runat="server" Text="Reset" OnClick="ResetLinkButtonclick" SkinID="sknActionLinkButton"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Forte_SubscriptionFeaturePricingSetup_bottonCancelLinkButton"
                                                    runat="server" Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="Forte_SubscriptionFeaturePricingSetup_pricingSubscriptioniddenDIV" runat="server"
                style="display: none">
                <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_pricingSubscriptionhiddenPopupModalButton"
                    runat="server" />
            </div>
            <div id="Forte_SubscriptionFeaturePricingSetup_deletePricingSubscriptionHiddenDIV"
                runat="server" style="display: none">
                <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_pricingSubscriptioniddenButton"
                    runat="server" />
            </div>
            <div id="Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControlHiddenDiv"
                runat="server" style="display: none">
                <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControlHiddenButton"
                    runat="server" />
            </div>
            <div id="Forte_SubscriptionFeaturePricingSetup_rePublishsubscriptionPricingPreviewControlHiddenDiv"
                runat="server" style="display: none">
                <asp:Button ID="Forte_SubscriptionFeaturePricingSetup_rePublishsubscriptionPricingPreviewControlButton"
                    runat="server" />
            </div>
            <table>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Forte_SubscriptionFeaturePricingSetup_pricingFeatureSubscriptionPanel"
                            runat="server" Style="display: none" CssClass="popupcontrol_subscription_types_popup">
                            <uc1:AddFeatureSubscriptionType ID="Forte_SubscriptionFeaturePricingSetup_pricingAddFeatureSubscriptionType"
                                runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeaturePricingSetup_pricingFeatureSubscriptionModalPopupExtender"
                runat="server" PopupControlID="Forte_SubscriptionFeaturePricingSetup_pricingFeatureSubscriptionPanel"
                TargetControlID="Forte_SubscriptionFeaturePricingSetup_pricingSubscriptionhiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingConfrimPopUpPanel"
                runat="server" Style="display: none" Height="210px" CssClass="popupcontrol_confirm">
                <uc2:ConfirmMsgControl ID="Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingconfirmPopupExtenderControl"
                    runat="server" Title="Warning" Type="YesNo" OnOkClick="Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingconfirmPopupExtenderControlokPopUpClick"
                    OnCancelClick="Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingconfirmPopupExtenderControlcancelPopUpClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingModalPopupExtender"
                runat="server" PopupControlID="Forte_SubscriptionFeaturePricingSetup_deleteSubscriptionPricingConfrimPopUpPanel"
                TargetControlID="Forte_SubscriptionFeaturePricingSetup_pricingSubscriptioniddenButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControlPanel"
                runat="server" Style="display: none" CssClass="popupcontrol_subscription_types"
                Height="330px">
                <uc3:SubscriptionPricingPreviewControl ID="Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControl"
                    runat="server" OnPublishClick="PreviewControlPublishClick" OnCancelClick="PreviewControlCancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControlModalPopupExtender"
                runat="server" PopupControlID="Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControlPanel"
                TargetControlID="Forte_SubscriptionFeaturePricingSetup_subscriptionPricingPreviewControlHiddenButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="Forte_SubscriptionFeaturePricingSetup_rePublishSubscriptionPricingConfrimPopUpPanel"
                runat="server" Style="display: none" Height="210px" CssClass="popupcontrol_confirm">
                <uc2:ConfirmMsgControl ID="Forte_SubscriptionFeaturePricingSetup_rePublishSubscriptionPricingConfirmMsgControl"
                    runat="server" Title="Warning" Type="YesNo" OnOkClick="Forte_SubscriptionFeaturePricingSetup_PublishButton_Click"
                    OnCancelClick="RePublishConfirmCancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="Forte_SubscriptionFeaturePricingSetup_rePublishSubscriptionPricingModalPopupExtender"
                runat="server" PopupControlID="Forte_SubscriptionFeaturePricingSetup_rePublishSubscriptionPricingConfrimPopUpPanel"
                TargetControlID="Forte_SubscriptionFeaturePricingSetup_rePublishsubscriptionPricingPreviewControlButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <script type="text/javascript">
                function Unlimited(NotApply, Unlimi, Text) {
                    if (document.getElementById(Unlimi).checked) {
                        document.getElementById(Text).value = "";
                        document.getElementById(Text).disabled = true;
                        if (document.getElementById(NotApply) != undefined || document.getElementById(NotApply) != null) {
                            document.getElementById(NotApply).checked = false;
                        }
                    }
                    else {
                        document.getElementById(Text).disabled = false;
                    }
                }
            </script>
            <script type="text/javascript">
                function NotApplicableClick(Notapply, Unlimi, Text) {
                    if (document.getElementById(Notapply).checked) {
                        document.getElementById(Unlimi).checked = false;
                        document.getElementById(Text).value = "";
                        document.getElementById(Text).disabled = true;
                    }
                    else {
                        document.getElementById(Text).value = "";
                        document.getElementById(Text).disabled = false;
                    }
                }

                function DeafultValue(Defaulthn, Text, Unlimi, NotApp) {
                    if (document.getElementById(Defaulthn).value == "UNLIMITED") {
                        document.getElementById(Unlimi).checked = true;
                        document.getElementById(Text).value = "";
                        document.getElementById(Text).disabled = true;
                    }
                    else {
                        document.getElementById(Text).value = document.getElementById(Defaulthn).value;
                        document.getElementById(Text).disabled = false;
                        document.getElementById(Unlimi).checked = false;
                    }
                    document.getElementById(NotApp).checked = false;
                    return false;
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
