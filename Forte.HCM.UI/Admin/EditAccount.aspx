﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditAccount.aspx.cs" MasterPageFile="~/MasterPages/CustomerAdminMaster.Master"
    Inherits="Forte.HCM.UI.Admin.EditAccount" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<asp:Content ID="EditAccount_bodyContent" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <asp:UpdatePanel ID="Forte_EditAccount_UpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td>
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="header_bg" align="right">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="50%" class="header_text_bold">
                                                <asp:Literal ID="Forte_EditAccount_editAccountHeaderLiteral" runat="server" Text="Edit Account"></asp:Literal>
                                            </td>
                                            <td width="50%" align="right">
                                                <asp:UpdatePanel ID="Forte_EditAccount_topButtonsUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="4">
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Forte_EditAccount_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                                        OnClick="EditAccount_SaveButtonClick" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="Forte_EditAccount_topUpgradeAccountButton" runat="server" Text="Upgrade Account"
                                                                        SkinID="sknButtonId" OnClick="EditAccount_UpgradeAccountButtonClick" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="Forte_EditAccount_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                        Text="Reset" OnClick="EditAccount_ResetLinkButtonClick" />
                                                                </td>
                                                                <td width="4%" align="center" class="link_button">
                                                                    |
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="Forte_EditAccount_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                        Text="Cancel" OnClick="ParentPageRedirect" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_EditAccount_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                            EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_EditAccount_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                            EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Literal ID="Forte_EditAccount_accountDetailsHeaderLiteral" runat="server" Text="Account Details"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_subscriptionTypeHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Subscription Type"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20px;">
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_subscriptionTypeLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_lastLoginHeaderLabel" runat="server" Text="Last Login"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_lastLoginLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        <input type="hidden" id="Forte_EditAccount_subscriptionRoleHidden" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_statusHeadLabel" runat="server" Text="Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_statusLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_subscribedOnHeadLabel" runat="server" Text="Subscribed On"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_subscribedOnLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_activatedOnHeadLabel" runat="server" Text="Activated On"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_activatedOnLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_noOfUsersHeadLabel" runat="server" Text="No Of Users"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_noOfUsersLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_titleHeadLabel" runat="server" Text="Title" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Forte_EditAccount_titleTextBox" MaxLength="50" Width="200px" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_companyHeadLable" runat="server" Text="Company"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Forte_EditAccount_companyTextBox" MaxLength="100" Width="200px"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_phoneHeadLabel" runat="server" Text="Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Forte_EditAccount_phoneTextBox" runat="server" MaxLength="20"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_firstNameHeadLabel" runat="server" Text="First Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Forte_EditAccount_firstNameTextBox" MaxLength="100" Width="200px"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_lastNameHeadLabel" runat="server" Text="Last Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Forte_EditAccount_lastNameTextBox" runat="server" Width="200px"
                                                                            MaxLength="100"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_isTrialHeadLabel" runat="server" Text="Trial" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_EditAccount_isTrialLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg" align="center">
                                                <asp:UpdatePanel ID="CreateTestSession_maxMinButtonUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                    <asp:Literal ID="Forte_CorporateUserManangement_usersResultsLiteral" runat="server"
                                                                        Text="Feature & Pricing"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <div id="Forte_EditAccount_featuresAndPricingDiv" style="overflow: auto; height: 200px;">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="Forte_EditAccount_featureAndPricingGridView" runat="server" AutoGenerateColumns="false"
                                                                            SkinID="sknSubsciptionAccountsGridView" Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Name">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_EditAccount_featureNameLabel" runat="server" Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Pricing">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_EditAccount_firstNameLabel" runat="server" Text='<%# Eval("FeatureValue") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_EditAccount_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                            EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_EditAccount_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                            EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" class="header_text_bold">
                                </td>
                                <td width="50%" align="right">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="4">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="Forte_EditAccount_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                            OnClick="EditAccount_SaveButtonClick" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="Forte_EditAccount_bottomUpgradeAccountButton" runat="server" Text="Upgrade Account"
                                                            SkinID="sknButtonId" OnClick="EditAccount_UpgradeAccountButtonClick" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="Forte_EditAccount_bottomResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                            Text="Reset" OnClick="EditAccount_ResetLinkButtonClick" />
                                                    </td>
                                                    <td width="4%" align="center" class="link_button">
                                                        |
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="Forte_EditAccount_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
