﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorporateUserManagement.aspx.cs"
    MasterPageFile="~/MasterPages/CustomerAdminMaster.Master" Inherits="Forte.HCM.UI.Admin.CorporateUserManagement" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<%@ Register Src="../CommonControls/CorporateUserSignUp.ascx" TagName="CorporateUserSignUp"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
 <%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="AddAssessorConfirmMsgControl"
    TagPrefix="uc3" %> 
<asp:Content ID="CorporateUserManagement_bodyContent" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <asp:UpdatePanel ID="Forte_CorporateUserManagement_UpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td>
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="header_bg" align="right">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="50%" class="header_text_bold">
                                                <asp:Literal ID="Forte_CorporateUserManagement_subsciptionPricingSetUpHeaderLiteral"
                                                    runat="server" Text="Corporate User Management"></asp:Literal>
                                            </td>
                                            <td width="50%" align="right">
                                                <asp:UpdatePanel ID="Forte_CorporateUserManagement_topButtonsUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="4">
                                                            <tr>
                                                                <td style="width: 50%;">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="Forte_CorporateUserManagement_topResetLinkButton" runat="server"
                                                                        SkinID="sknActionLinkButton" Text="Reset" OnClick="ResetButtonClick" />
                                                                </td>
                                                                <td width="4%" align="center" class="link_button">
                                                                    |
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="Forte_CorporateUserManagement_topCancelLinkButton" runat="server"
                                                                        SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_CorporateUserManagement_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage" EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_CorporateUserManagement_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage" EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width: 11%;" align="right">
                                                                        <asp:Label ID="Forte_CorporateUserManagement_TenantNameHeadLabel" runat="server"
                                                                            SkinID="sknLabelFieldHeaderText" Text="Company"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20px;">
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="Forte_CorporateUserManagement_TenantNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        <input type="hidden" id="Forte_CorporateUserManagement_userPhoneHidden" runat="server" />
                                                                        <input type="hidden" id="Forte_CorporateUserManagement_tenantIdHidden" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="Forte_CorporateUserManagement_tenantTitleHeadLabel" runat="server"
                                                                            SkinID="sknLabelFieldHeaderText" Text="Title"></asp:Label>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="Forte_CorporateUserManagement_tenantTitleFieldLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 15%;">
                                                                        <asp:Label ID="Forte_CorporateUserManagement_tenantNumberOfUserHeadLabel" runat="server"
                                                                            SkinID="sknLabelFieldHeaderText" Text="Number Of Users"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 20%;">
                                                                        <asp:Label ID="Forte_CorporateUserManagement_tenantNumberOfUserFieldLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg" align="center">
                                                <asp:UpdatePanel ID="CreateTestSession_maxMinButtonUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                    <asp:Literal ID="Forte_CorporateUserManangement_usersResultsLiteral" runat="server"
                                                                        Text="Users"></asp:Literal>
                                                                    <asp:Label ID="Forte_CorporateUserManangement_usersResultsHelpLabel" runat="server"
                                                                        SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                </td>
                                                                <td style="width: 50%" align="right" id="Forte_CorporateUserManangement_searchTestSessionResultsTR"
                                                                    runat="server">
                                                                    <span id="Forte_CorporateUserManangement_usersResultsUpSpan" runat="server" style="display: none;">
                                                                        <asp:Image ID="Forte_CorporateUserManangement_usersResultsUpImage" runat="server"
                                                                            SkinID="sknMinimizeImage" /></span><span id="Forte_CorporateUserManangement_usersResultsDownSpan"
                                                                                runat="server" style="display: none;"><asp:Image ID="Forte_CorporateUserManangement_usersResultsDownImage"
                                                                                    runat="server" SkinID="sknMaximizeImage" /></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <div id="Forte_CorporateUserManagement_featuresDiv" style="overflow: auto; height: 200px;">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <input type="hidden" runat="server" id="Forte_SubscriptionFeaturePricing_deleteUserIdHidden" />
                                                                        <asp:GridView ID="Forte_CorporateUserManagement_usersGridView" runat="server" AutoGenerateColumns="false"
                                                                            SkinID="sknWrapHeaderGrid" Width="100%" OnRowCommand="Forte_CorporateUserManagement_usersGridView_RowCommand"
                                                                            OnRowCreated="Forte_CorporateUserManagement_usersGridView_RowCreated" OnSorting="Forte_CorporateUserManagement_usersGridView_RowSorting">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemStyle Width="10%" />
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="Forte_CorporateUserManagement_emailActivationCodeImageButton"
                                                                                            runat="server" SkinID="sknMailImageButton" ToolTip="Email Activation Code" CommandArgument='<%# Eval("UserID") %>'
                                                                                            CommandName="EmailActivationCode" Visible='<%# IsUserNotActivated(Eval("Activated").ToString())%>' CssClass="showCursor" />
                                                                                        <asp:ImageButton ID="Forte_CorporateUserManagement_editfeatureTypeImageButton" runat="server"
                                                                                            ToolTip="Edit User" SkinID="sknEditImageButton" CommandName="Edit User" CommandArgument='<%# Eval("UserID") %>' />
                                                                                        <asp:ImageButton ID="Forte_CorporateUserManagement_deleteFeatureTypeImageButton"
                                                                                            runat="server" ToolTip="Delete User" SkinID="sknDeleteImageButton" CommandName="Delete User"
                                                                                            Visible="FAlse" CommandArgument='<%# Eval("UserID") %>' />
                                                                                        <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_activeOrDeactiveImageButton"
                                                                                            runat="server" SkinID="sknActiveImageButton" ToolTip="Activate User" Visible='<%# GetActiveIcon(Convert.ToInt16(Eval("IsActive"))) %>'
                                                                                            CommandName="ActivateUser" CommandArgument='<% #Eval("UserID") %>' />
                                                                                        <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_inActiveImageButton"
                                                                                            runat="server" SkinID="sknInactiveImageButton" ToolTip="Deactivate User" Visible='<%# GetInActiveIcon(Convert.ToInt16(Eval("IsActive"))) %>'
                                                                                            CommandName="DeactivateUser" CommandArgument='<% #Eval("UserID") %>' />
                                                                                         <asp:ImageButton ID="SearchUserInformation_resendPasswordImageButton" ToolTip="Resend Password" 
                                                                                                                runat="server" SkinID="sknPasswordResendImageButton" CommandName="ResendPassword" />
                                                                                        <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_createAssessorImageButton"
                                                                                                runat="server" CommandName="CreateAssessor" CommandArgument='<%# Eval("UserID") %>'
                                                                                                 Visible='<% # Convert.ToBoolean(IsNotAssessor(Convert.ToString(Eval("isAssessor"))))  %>'
                                                                                                SkinID="sknCreateCandidateUserNameImageButton"  ToolTip="Mark As An Assessor" />

                                                                                     <asp:ImageButton ID="CorporateSubscriptionManagement_usersGridView_editAssessorImageButton"
                                                                                        runat="server" CommandName="EditAssessor" CommandArgument='<%# Eval("UserID") %>'
                                                                                        Visible='<% # Convert.ToBoolean(IsAssessor(Convert.ToString(Eval("isAssessor"))))  %>'
                                                                                        SkinID="sknViewCandidateActivityLogImageButton" ToolTip="Edit Assessor" /> 

                                                                                        <asp:HiddenField ID="CorporateSubscriptionManagement_usersGridView_userEmailHiddenField" runat="server" Value='<%# Eval("UserEmail") %>' />

                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="User ID" SortExpression="EMAIL">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_userNameLabel" runat="server" Text='<%# Eval("UserEmail") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="First Name" SortExpression="FIRSTNAME">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_firstNameLabel" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Last Name" SortExpression="LASTNAME">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_LastNameLabel" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Account Active" SortExpression="ISACTIVE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_isActiveLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsActive"))) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Account Activated" SortExpression="ACTIVATED">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_activeLabel" runat="server" Text='<%# GetCharExpansion(Eval("Activated")) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Default Form Layout" SortExpression="FORMNAME">
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_userFormNameLabel" runat="server" Text='<%# Eval("UserFormName") %>'></asp:Label>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_userFormIdLabel" runat="server" Text='<%# Eval("FormId") %>'
                                                                                            Visible="false"></asp:Label>
                                                                                        <asp:Label ID="Forte_CorporateUserManagement_isActiveStatusLabel" runat="server"
                                                                                            Text='<%# Eval("IsActiveStatus") %>' Visible="false"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                    <td align="left">
                                                        <asp:HiddenField ID="Forte_CorporateUserManagement_createAssessorIdHiddenField" runat="server" />
                                                        <asp:Panel ID="Forte_CorporateUserManagement_createAssessorPopupPanel" runat="server" Style="display: none"
                                                            CssClass="client_confirm_message_box">
                                                            <uc3:AddAssessorConfirmMsgControl ID="Forte_CorporateUserManagement_createAssessorConfirmMsgControl"
                                                                runat="server" OnCancelClick="CorporateUserManagement_createAssessorConfirmMsgControl_CancelClick"
                                                                OnOkClick="CorporateUserManagement_createAssessorConfirmMsgControl_OkClick" />
                                                        </asp:Panel>
                                                        <ajaxToolKit:ModalPopupExtender ID="Forte_CorporateUserManagement_createAssessorModalPopupExtender"
                                                            runat="server" PopupControlID="Forte_CorporateUserManagement_createAssessorPopupPanel" TargetControlID="Forte_CorporateUserManagement_createAssessorIdHiddenField"
                                                            BackgroundCssClass="modalBackground">
                                                        </ajaxToolKit:ModalPopupExtender>
                                                    </td>
                                                </tr>
        
                                                    <tr>
                                                        <td align="left">
                                                            <asp:LinkButton ID="Forte_CorporateUserManagement_addNewUserLinkButton" runat="server"
                                                                Text="Add New User" SkinID="sknAddLinkButton" OnClick="Forte_CorporateUserManagement_addNewUserLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_CorporateUserManagement_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage" EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_CorporateUserManagement_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage" EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" class="header_text_bold">
                                </td>
                                <td width="50%" align="right">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="4">
                                                <tr>
                                                    <td style="width: 50%;">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="Forte_CorporateUserManagement_bottonResetLinkButton" runat="server"
                                                            SkinID="sknActionLinkButton" Text="Reset" OnClick="ResetButtonClick" />
                                                    </td>
                                                    <td width="4%" align="center" class="link_button">
                                                        |
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="Forte_CorporateUserManagement_bottonCancelLinkButton" runat="server"
                                                            SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="CorporateSubscriptionManagement_activateUserUpdatePanel" runat="server"
                            UpdateMode="Always">
                            <ContentTemplate>
                                <div style="display: none">
                                    <asp:Button ID="CorporateSubscriptionManagement_activateUserHiddenButton" runat="server" />
                                </div>
                                <asp:Panel ID="CorporateSubscriptionManagement_activateUserPanel" runat="server"
                                    Style="display: none" CssClass="popupcontrol_confirm_remove">
                                    <uc2:ConfirmMsgControl ID="CorporateSubscriptionManagement_activateUserConfirmMsgControl"
                                        runat="server" OnOkClick="CorporateSubscriptionManagement_activateUserConfirmMsgControl_OKClick"
                                        />
                                    <asp:HiddenField ID="CorporateSubscriptionManagement_activateUserHiddenField" runat="server" />
                                    <asp:HiddenField ID="CorporateSubscriptionManagement_activateStatusHiddenField" runat="server" />
                                </asp:Panel>
                                <ajaxToolKit:ModalPopupExtender ID="CorporateSubscriptionManagement_activateUserModalPopupExtender"
                                    runat="server" PopupControlID="CorporateSubscriptionManagement_activateUserPanel"
                                    TargetControlID="CorporateSubscriptionManagement_activateUserHiddenButton" BackgroundCssClass="modalBackground">
                                </ajaxToolKit:ModalPopupExtender>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <div id="Forte_CorporateUserManagement_hiddenDiv" style="display: none;">
                <asp:Button ID="Forte_CorporateUserManagement_addNewUserHiddenButton" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="Forte_CorporateUserManagement_AddNewUserModalPopupExtender"
                runat="server" PopupControlID="Forte_CorporateUserManagement_AddNewUserPanel"
                TargetControlID="Forte_CorporateUserManagement_addNewUserHiddenButton" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="Forte_CorporateUserManagement_AddNewUserPanel" runat="server" Style="display: none"
                Height="400px" CssClass="popupcontrol_subscription_types">
                <uc1:CorporateUserSignUp ID="Forte_CorporateUserManagement_CorporateUserSignUp" runat="server" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="Forte_CorporateUserManagement_deleteUserConfrimPopUpModalPopUpExtender"
                runat="server" PopupControlID="Forte_CorporateUserManagement_deleteUserConfrimPopUpPanel"
                TargetControlID="Forte_CorporateUserManagement_deleteUserhiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="Forte_CorporateUserManagement_deleteUserConfrimPopUpPanel" runat="server"
                Style="display: none" Height="210px" CssClass="popupcontrol_confirm">
                <uc2:ConfirmMsgControl ID="Forte_CorporateUserManagement_deleteUserconfirmPopupExtenderControl"
                    runat="server" Message="Are you sure you want to delete user?" Title="Warning"
                    Type="YesNo" OnOkClick="Forte_CorporateUserManagement_deleteUserokPopUpClick"
                    OnCancelClick="Forte_CorporateUserManagement_deleteUsercancelPopUpClick" />
            </asp:Panel>
            <div id="Forte_CorporateUserManagement_deleteUserhiddenDIV" runat="server" style="display: none">
                <asp:Button ID="Forte_CorporateUserManagement_deleteUserhiddenPopupModalButton" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
