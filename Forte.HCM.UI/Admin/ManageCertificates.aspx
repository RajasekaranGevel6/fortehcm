<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageCertificates.aspx.cs"
    MasterPageFile="~/MasterPages/OTMMaster.Master" Inherits="Forte.HCM.UI.Admin.ManageCertificates" %>

<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="ManageCertificates_contentID" ContentPlaceHolderID="OTMMaster_body"
    runat="server">

    <script language="javascript" type="text/javascript">

        // Function that helps to select default radio button and shows the certifiate.
        function CheckDefaultRadioButton(spanChk, htmlTextID, previewDivID) {
            debugger;
            
            Parent = document.getElementById("<%= ManageCertificates_certificatesGridView.ClientID %>");
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {
                if (items[i].id == spanChk && items[i].type == "radio") {
                    items[i].checked = true;
                    document.getElementById(previewDivID).innerHTML = document.getElementById(htmlTextID).value;
                }
            }
            return false;
        }

        // This method used to avoid multiple radio button selection
        function CheckOtherIsCheckedByGVID(spanChk, htmlTextID, previewDivID) {
            debugger;
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk;

            Parent = document.getElementById("<%= ManageCertificates_certificatesGridView.ClientID %>");
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {
                if (items[i].id == spanChk && items[i].type == "radio") {
                    items[i].checked = true;
                    document.getElementById(previewDivID).innerHTML = document.getElementById(htmlTextID).value;
                }
                else {
                    if (items[i].type == "radio") {
                        items[i].checked = false;
                    }
                }
            }
            return false;
        }
    </script>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ManageCertificates_headerLiteral" runat="server" Text="Manage Certificates"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ManageCertificates_topRefreshButton" runat="server" Text="Refresh"
                                            SkinID="sknButtonId" OnClick="ManageCertificates_refreshButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ManageCertificates_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="ManageCertificates_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ManageCertificates_topCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ManageCertificates_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ManageCertificates_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="ManageCertificates_certificatesGridviewHeaderLiteral" runat="server"
                                Text="Certificate Detail"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="2" cellspacing="2" border="0">
                                <tr>
                                    <td style="width: 49%; overflow: auto;" valign="top">
                                        <div id="Div1" style="height: 395px; overflow: auto;" runat="server">
                                            <asp:GridView ID="ManageCertificates_certificatesGridView" GridLines="Horizontal" BorderColor="white" BorderWidth="1px" runat="server" AutoGenerateColumns="false"
                                                OnRowDataBound="ManageCertificates_certificatesGridView_RowDataBound">
                                                <RowStyle CssClass="grid_alternate_row" />
<AlternatingRowStyle CssClass="grid_alternate_row" />
<HeaderStyle CssClass="grid_header_row" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Certificate Name">
                                                        <ItemTemplate>
                                                            <asp:RadioButton ID="ManageCertificates_certificatePreviewRadioButton" runat="server"
                                                                Text='<%# Eval("CertificateName") %>' OnCheckedChanged="CertificateFormatRadioButton_CheckedChanged"
                                                                AutoPostBack="true" GroupName='<%# Eval("CertificateID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Description" DataField="CertificateDesc" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ManageCertificates_editLinkButton" runat="server" Text="Edit" ToolTip="Edit Certificate"></asp:LinkButton>
                                                            <asp:HiddenField ID="ManageCertificates_certificationIDHiddenFiled" Value='<%# Eval("CertificateID") %>'
                                                                runat="server" />
                                                            <asp:HiddenField ID="ManageCertificates_htmlTextHiddenField" Value='<%# Eval("HtmlText") %>'
                                                                runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                    <td style="width: 50%;" valign="top">
                                        <div runat="server" id="ManageCertificates_previewDIV" style="width: 595px; height: 397px;
                                            overflow: auto;">
                                            <asp:Image ID="ManageCertificates_certificateImage" runat="server" Height="400px" Width="600px" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="ManageCertificates_addCertificateLinkButton" runat="server" Text="Add"
                                            SkinID="sknAddLinkButton" ToolTip="Add New Certificate">
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <%--<asp:UpdatePanel ID="ManageCertificates_bottomMessageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>--%>
                <asp:Label ID="ManageCertificates_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ManageCertificates_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ManageCertificates_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="ManageCertificates_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="4" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="ManageCertificates_bottomRefreshButton" runat="server" Text="Refresh"
                                SkinID="sknButtonId" OnClick="ManageCertificates_refreshButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="ManageCertificates_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="ManageCertificates_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="ManageCertificates_bottomCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
