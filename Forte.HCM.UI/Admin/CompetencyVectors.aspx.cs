﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CompetencyVectors.aspx.cs
// File that represents the Competency Vectors page that defines the admin interface
// layout and functionalities for the Competency Vectors.
// This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    // Class that represents the Competency Vectors page that defines the admin interface
    // layout and functionalities for the Competency Vectors.
    // This class inherits Forte.HCM.UI.Common.PageBase class.
    /// <summary>
    public partial class CompetencyVectors : PageBase
    {
        #region Event Handler                                                  

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CompetencyVectors_deleteVectorGroup_ConfirmMsgControl.OkClick +=
                   new ConfirmMsgControl.Button_Click(
                       CompetencyVectors_deleteVectorGroup_ConfirmMsgControl_OkClick);

                CompetencyVectors_deleteCompetencyVector_ConfirmMsgControl.OkClick +=
                    new Forte.HCM.UI.CommonControls.ConfirmMsgControl.Button_Click
                        (CompetencyVectors_deleteCompetencyVector_ConfirmMsgControl_OkClick);

                CompetencyVectors_deleteVectorParameter_ConfirmMsgControl.OkClick +=
                   new ConfirmMsgControl.Button_Click
                     (CompetencyVectors_deleteVectorParameter_ConfirmMsgControl_OkClick);

                CompetencyVectors_deleteVectorGroup_ConfirmMsgControl.CancelClick +=
                    new ConfirmMsgControl.Button_Click
                    (CompetencyVectors_deleteVectorGroup_ConfirmMsgControl_CancelClick);

                CompetencyVectors_deleteCompetencyVector_ConfirmMsgControl.CancelClick +=
                    new ConfirmMsgControl.Button_Click
                        (CompetencyVectors_deleteCompetencyVector_ConfirmMsgControl_CancelClick);

                CompetencyVectors_deleteVectorParameter_ConfirmMsgControl.CancelClick +=
                    new ConfirmMsgControl.Button_Click
                      (CompetencyVectors_deleteVectorParameter_ConfirmMsgControl_CancelClick);
                
                // Set page title
                Master.SetPageCaption("Competency Vectors");

                CompetencyVectors_newCompetencyVectorGroupTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + CompetencyVectors_newCompetencyVectorGroupLinkButton.ClientID + "','CVG','" 
                    + CompetencyVectors_browserHiddenField.ClientID + "');");

                CompetencyVectors_newCompetencyVectorTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + CompetencyVectors_newCompetencyVectorLinkButton.ClientID + "','CV','"
                    + CompetencyVectors_browserHiddenField.ClientID + "');");

                CompetencyVectors_newVectorParameterTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + CompetencyVectors_newVectorParameterLinkButton.ClientID + "','VP','"
                    + CompetencyVectors_browserHiddenField.ClientID + "');");

                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Utility.IsNullOrEmpty(CompetencyVectors_browserHiddenField.Value))
                {
                    ValidateEnterKey(CompetencyVectors_browserHiddenField.Value.Trim());
                    CompetencyVectors_browserHiddenField.Value = string.Empty;
                }

                if (!IsPostBack)
                {
                    // Clear all top and bottom label
                    ClearAllLabelMessage();
                    // Load default values
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, exp.Message);
            }
        }
        public void CompetencyVectors_deleteCompetencyVector_ConfirmMsgControl_CancelClick(object sender, EventArgs e)
        {
            ClearAllLabelMessage();
        }
        public void CompetencyVectors_deleteVectorGroup_ConfirmMsgControl_CancelClick(object sender, EventArgs e)
        {
            ClearAllLabelMessage();
        }
        public void CompetencyVectors_deleteVectorParameter_ConfirmMsgControl_CancelClick(object sender, EventArgs e)
        {
            ClearAllLabelMessage();
        }

        /// <summary>
        /// Handler method that will delete selected competency vector group.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        public void CompetencyVectors_deleteVectorGroup_ConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                ClearAllLabelMessage();
                if (CompetencyVectors_competencyVectorGroupsListBox.Items.FindByValue
                    (CompetencyVectors_competencyVectorGroupsListBox.SelectedValue) == null)
                {
                    ShowMessage(CompetencyVectors_topErrorMessageLabel,
                        CompetencyVectors_bottomErrorMessageLabel,
                        "Please select a competency vector group to delete");
                    return;
                }
                // Checks if a competency vector is associated with the Vector group
                if (CompetencyVectors_competencyVectorsListBox.Items.Count > 0)
                {
                    ShowMessage(CompetencyVectors_topErrorMessageLabel,
                      CompetencyVectors_bottomErrorMessageLabel,
                         string.Format("Competency vector group '{0}' cannot be deleted. It is associated with competency vector",
                      CompetencyVectors_competencyVectorGroupsListBox.SelectedItem.Text));

                    return;
                }

                new AdminBLManager().DeleteCompetencyVectorGroup(int.Parse
                        (CompetencyVectors_competencyVectorGroupsListBox.SelectedItem.Value));

                string vectorGroup = CompetencyVectors_competencyVectorGroupsListBox.SelectedItem.Text;

                string successMessage = string.Empty;
                successMessage = string.Format
                      ("Competency vector group '{0}' deleted successfully", vectorGroup);

                base.ShowMessage(CompetencyVectors_topSuccessMessageLabel,
                 CompetencyVectors_bottomSuccessMessageLabel,
                 successMessage);

                GetCompetencyVectorGroups();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CompetencyVectors_topErrorMessageLabel,
                  CompetencyVectors_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will delete selected competency vector.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        public void CompetencyVectors_deleteCompetencyVector_ConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                ClearAllLabelMessage();
                if (CompetencyVectors_competencyVectorsListBox.SelectedItem == null)
                {
                    ShowMessage(CompetencyVectors_topErrorMessageLabel,
                        CompetencyVectors_bottomErrorMessageLabel,
                        "Please select competency vector to delete");
                    return;
                }

                // Retrieve the Group ID.
                int groupID = int.Parse(CompetencyVectors_competencyVectorGroupsListBox.
                    SelectedValue);

                AdminBLManager adminBLManager = new AdminBLManager();

                // Declare message variables.
                string errorMessage = string.Empty;
                string successMessage = string.Empty;

                // Loop through the vector list box and delete the selected 
                // vectors
                for (int vectorIndex = 0; vectorIndex <
                    CompetencyVectors_competencyVectorsListBox.Items.Count; vectorIndex++)
                {
                    if (!CompetencyVectors_competencyVectorsListBox.Items[vectorIndex].Selected)
                        continue;

                    string vectorID = CompetencyVectors_competencyVectorsListBox.Items
                        [vectorIndex].Value;
                    string vectorName = CompetencyVectors_competencyVectorsListBox.Items
                        [vectorIndex].Text;

                    // Check if the vector is associated with candidate vector table

                    if (adminBLManager.IsVectorIDUsed(vectorID))
                    {
                        if (errorMessage == string.Empty)
                        {
                            errorMessage = string.Format
                                ("Competency vector '{0}' cannot be deleted. It is associated with candidate competency vector", vectorName);
                        }
                        else
                        {
                            errorMessage = errorMessage + "<br>" + string.Format
                                ("Competency vector  '{0}' cannot be deleted. It is associated with candidate competency vector", vectorName);
                        }
                    }
                    else
                    {
                        // Delete the selected vector.
                        new AdminBLManager().DeleteCompetencyVector(vectorID);

                        if (successMessage == string.Empty)
                        {
                            successMessage = string.Format
                                ("Competency vector '{0}' deleted successfully", vectorName);
                        }
                        else
                        {
                            successMessage = successMessage + "<br>" + string.Format
                                ("Competency vector '{0}' deleted successfully", vectorName);
                        }
                    }
                }

                GetCompetencyVectors(groupID);

                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                CompetencyVectors_bottomErrorMessageLabel,
                errorMessage);

                ShowMessage(CompetencyVectors_topSuccessMessageLabel,
                   CompetencyVectors_bottomSuccessMessageLabel,
                    successMessage);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will delete selected competency vector parameters.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        public void CompetencyVectors_deleteVectorParameter_ConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                ClearAllLabelMessage();
                if (CompetencyVectors_vectorParametersListBox.SelectedItem == null)
                {
                    ShowMessage(CompetencyVectors_topErrorMessageLabel,
                        CompetencyVectors_bottomErrorMessageLabel,
                        "Please select competency vector parameter to delete");
                    return;
                }

                AdminBLManager adminBLManager = new AdminBLManager();

                // Declare message variables.
                string errorMessage = string.Empty;
                string successMessage = string.Empty;

                // Loop through the parameters list box and delete the selected 
                // parameters.
                for (int parameterIndex = 0; parameterIndex <
                    CompetencyVectors_vectorParametersListBox.Items.Count; parameterIndex++)
                {
                    if (!CompetencyVectors_vectorParametersListBox.Items[parameterIndex].Selected)
                        continue;

                    string parameterID = CompetencyVectors_vectorParametersListBox.Items
                        [parameterIndex].Value;
                    string parameterName = CompetencyVectors_vectorParametersListBox.Items
                        [parameterIndex].Text;

                    // Check if the parameter is associated with candidate competency vector
                    if (adminBLManager.IsParameterIDUsed(parameterID))
                    {
                        if (errorMessage == string.Empty)
                        {
                            errorMessage = string.Format
                                ("Competency vector parameter '{0}' cannot be deleted. It is associated with candidate competency vector", parameterName);
                        }
                        else
                        {
                            errorMessage = errorMessage + "<br>" + string.Format
                                ("Competency vector parameter '{0}' cannot be deleted. It is associated with candidate competency vector", parameterName);
                        }
                    }
                    else
                    {
                        // Delete the selected parameter.
                        new AdminBLManager().DeleteCompetencyVectorParameter(parameterID);

                        if (successMessage == string.Empty)
                        {
                            successMessage = string.Format
                                ("Competency vector parameter '{0}' deleted successfully", parameterName);
                        }
                        else
                        {
                            successMessage = successMessage + "<br>" + string.Format
                                ("Competency vector parameter '{0}' deleted successfully", parameterName);
                        }
                    }
                }

                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                CompetencyVectors_bottomErrorMessageLabel,
                errorMessage);

                ShowMessage(CompetencyVectors_topSuccessMessageLabel,
                   CompetencyVectors_bottomSuccessMessageLabel,
                    successMessage);

                GetCompetencyVectorParameters();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will load competency vectors based on selected competency vector group.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CompetencyVectors_competencyVectorGroupsListBox_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            CompetencyVectors_competencyVectorsListBox.Items.Clear();
            GetCompetencyVectors(int.Parse(CompetencyVectors_competencyVectorGroupsListBox.SelectedItem.Value));
        }

        /// <summary>
        /// Handler method that will add new competency vector group.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CompetencyVectors_newCompetencyVectorGroupLinkButton_Click
            (object sender, EventArgs e)
        {
            ClearAllLabelMessage();
            if (CompetencyVectors_newCompetencyVectorGroupTextBox.Text.Trim().Length == 0)
            {
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    Resources.HCMResource.Competency_VectorGroupNameEmpty);
                return;
            }
            else
            {
                string[] prefixChars = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                bool isStartsWith = prefixChars.Any(prefix =>
                    CompetencyVectors_newCompetencyVectorGroupTextBox.Text.Trim().StartsWith(prefix));

                if (isStartsWith)
                {
                    ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    "Competency vector group should not start with number");
                    return;
                }

                string prefixString = "1234567890";
                bool isExists = (CompetencyVectors_newCompetencyVectorGroupTextBox.Text.Trim().Substring(0, 1).
                    Equals(prefixString.Contains(CompetencyVectors_newCompetencyVectorGroupTextBox.Text.Trim()))) 
                    ? true : false;
            }

            // Check if the new Vector group is already exist.
            if (FindByText(CompetencyVectors_competencyVectorGroupsListBox,
                CompetencyVectors_newCompetencyVectorGroupTextBox.Text.Trim()))
            {
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, string.Format(
                    "Competency vector group '{0}' already exist",
                    CompetencyVectors_newCompetencyVectorGroupTextBox.Text.Trim()));
                CompetencyVectors_newCompetencyVectorGroupTextBox.Focus();
                return;
            }

            try
            {
                CompetencyVectors_topErrorMessageLabel.Text = string.Empty;
                CompetencyVectors_bottomErrorMessageLabel.Text = string.Empty;

                CompetencyVectorGroup adminCompetencyVectorsDetail = new CompetencyVectorGroup();
                adminCompetencyVectorsDetail.GroupName = CompetencyVectors_newCompetencyVectorGroupTextBox.Text.Trim();

                adminCompetencyVectorsDetail.UserID = base.userID;
                new AdminBLManager().InsertCompetencyVectorGroup(adminCompetencyVectorsDetail);
                GetCompetencyVectorGroups();
                CompetencyVectors_newCompetencyVectorGroupTextBox.Text = string.Empty;

                base.ShowMessage(CompetencyVectors_topSuccessMessageLabel,
                    CompetencyVectors_bottomSuccessMessageLabel,
                     Resources.HCMResource.Competency_VectorGroupNameSuccessMessage);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will add new competency vectors.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CompetencyVectors_newCompetencyVectorLinkButton_Click
            (object sender, EventArgs e)
        {
            ClearAllLabelMessage();
            if (CompetencyVectors_competencyVectorGroupsListBox.SelectedItem == null)
            {
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    "Please select competency vector group");
                return;
            }

            if (CompetencyVectors_newCompetencyVectorTextBox.Text.Trim().Length == 0)
            {
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    Resources.HCMResource.Competency_VectorNameEmpty);
                return;
            }
            else
            {
                string[] prefixChars = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                bool isStartsWith = prefixChars.Any(prefix =>
                    CompetencyVectors_newCompetencyVectorTextBox.Text.Trim().StartsWith(prefix));

                if (isStartsWith)
                {
                    ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    "Competency vector should not start with number");
                    return;
                }
            }

            // Check if the new Vector is already exist.
            if (FindByText(CompetencyVectors_competencyVectorsListBox,
                CompetencyVectors_newCompetencyVectorTextBox.Text.Trim()))
            {
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, string.Format(
                    "Competency Vector '{0}' already exist",
                    CompetencyVectors_newCompetencyVectorTextBox.Text.Trim()));
                CompetencyVectors_competencyVectorsListBox.Focus();
                return;
            }
            try
            {
                CompetencyVector adminCompetencyVectorsDetail = new CompetencyVector();
                adminCompetencyVectorsDetail.GroupID = int.Parse(CompetencyVectors_competencyVectorGroupsListBox.SelectedItem.Value);
                adminCompetencyVectorsDetail.VectorType = CompetencyVectors_newCompetencyVectorTextBox.Text.Trim();
                adminCompetencyVectorsDetail.UserID = base.userID;
                new AdminBLManager().InsertCompetencyVector(adminCompetencyVectorsDetail);
                GetCompetencyVectors(int.Parse(CompetencyVectors_competencyVectorGroupsListBox.SelectedItem.Value));
                CompetencyVectors_newCompetencyVectorTextBox.Text = string.Empty;

                base.ShowMessage(CompetencyVectors_topSuccessMessageLabel,
                    CompetencyVectors_bottomSuccessMessageLabel,
                     Resources.HCMResource.Competency_VectorNameSuccessMessage);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will add new competency parameters.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CompetencyVectors_newVectorParameterLinkButton_Click
            (object sender, EventArgs e)
        {
            ClearAllLabelMessage();
            if (CompetencyVectors_newVectorParameterTextBox.Text.Trim().Length == 0)
            {
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    Resources.HCMResource.Competency_VectorParameterNameEmpty);
                return;
            }
            else
            {
                string[] prefixChars = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                bool isStartsWith = prefixChars.Any(prefix =>
                    CompetencyVectors_newVectorParameterTextBox.Text.Trim().StartsWith(prefix));

                if (isStartsWith)
                {
                    ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    "Vector parameter should not start with number");
                    return;
                }
            }

            // Check if the new Vector parameter is already exist.
            if (FindByText(CompetencyVectors_vectorParametersListBox,
                CompetencyVectors_newVectorParameterTextBox.Text.Trim()))
            {
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, string.Format(
                    "Competency Vector parameter '{0}' already exist",
                    CompetencyVectors_newVectorParameterTextBox.Text.Trim()));

                CompetencyVectors_vectorParametersListBox.Focus();
                return;
            }
            try
            {
                CompetencyVectors_topErrorMessageLabel.Text = string.Empty;
                CompetencyVectors_bottomErrorMessageLabel.Text = string.Empty;

                CompetencyVectorParameter adminCompetencyVectorsDetail = new CompetencyVectorParameter();
                adminCompetencyVectorsDetail.ParameterName = CompetencyVectors_newVectorParameterTextBox.Text.Trim();

                adminCompetencyVectorsDetail.UserID = base.userID;
                new AdminBLManager().InsertCompetencyVectorParameter(adminCompetencyVectorsDetail);
                GetCompetencyVectorParameters();
                CompetencyVectors_newVectorParameterTextBox.Text = string.Empty;

                base.ShowMessage(CompetencyVectors_topSuccessMessageLabel,
                   CompetencyVectors_bottomSuccessMessageLabel,
                    Resources.HCMResource.Competency_VectorParameterNameSuccessMessage);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                // Show error messages to the user.
                base.ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CompetencyVectors_bottomResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        protected void CompetencyVectors_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CompetencyVectors_topErrorMessageLabel,
                    CompetencyVectors_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        #endregion Event Handler                                               

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the competency vector group fields 
        /// </summary>
        private void GetCompetencyVectorGroups()
        {
            CompetencyVectors_competencyVectorGroupsListBox.Items.Clear();
            CompetencyVectors_competencyVectorsListBox.Items.Clear();
            List<CompetencyVectorGroup> competencyVectorGroupDetail = new AdminBLManager
                ().GetCompetencyVectorGroup();
            if (!Utility.IsNullOrEmpty(competencyVectorGroupDetail))
            {
                CompetencyVectors_competencyVectorGroupsListBox.DataSource = competencyVectorGroupDetail;
                CompetencyVectors_competencyVectorGroupsListBox.DataTextField = "GroupName";
                CompetencyVectors_competencyVectorGroupsListBox.DataValueField = "GroupID";
                CompetencyVectors_competencyVectorGroupsListBox.DataBind();
                CompetencyVectors_competencyVectorGroupsListBox.Items[0].Selected = true;
                GetCompetencyVectors(int.Parse(CompetencyVectors_competencyVectorGroupsListBox.Items[0].Value));
            }
        }

        /// <summary>
        /// Method that loads the competency vector fields 
        /// </summary>
        ///  /// <param name="GroupID">
        /// A <see cref="int"/> that holds the Group id of the selected vector group.
        /// </param>
        private void GetCompetencyVectors(int GroupID)
        {
            CompetencyVectors_competencyVectorsListBox.Items.Clear();
            List<CompetencyVector> competencyVectorDetail = new AdminBLManager().GetCompetencyVector(GroupID);
            if (!Utility.IsNullOrEmpty(competencyVectorDetail))
            {
                List<CompetencyVector> competencyVector = new
                    AdminBLManager().GetCompetencyVector(GroupID);
                CompetencyVectors_competencyVectorsListBox.DataSource = competencyVectorDetail;
                CompetencyVectors_competencyVectorsListBox.DataTextField = "VectorType";
                CompetencyVectors_competencyVectorsListBox.DataValueField = "VectorID";
                CompetencyVectors_competencyVectorsListBox.DataBind();
                CompetencyVectors_competencyVectorsListBox.Items[0].Selected = true;
            }
        }

        /// <summary>
        /// Method that loads the competency vector Parameter fields 
        /// clicked.
        /// </summary>        
        private void GetCompetencyVectorParameters()
        {
            CompetencyVectors_vectorParametersListBox.Items.Clear();
            List<CompetencyVectorParameter> competencyVectorParameterDetail = new AdminBLManager
                ().GetCompetencyVectorParameter();
            if (!Utility.IsNullOrEmpty(competencyVectorParameterDetail))
            {
                CompetencyVectors_vectorParametersListBox.DataSource = competencyVectorParameterDetail;
                CompetencyVectors_vectorParametersListBox.DataTextField = "ParameterName";
                CompetencyVectors_vectorParametersListBox.DataValueField = "ParameterID";
                CompetencyVectors_vectorParametersListBox.DataBind();
                CompetencyVectors_vectorParametersListBox.Items[0].Selected = true;
            }
        }

        /// <summary>
        /// Method that checks if the given text is found in the list box.
        /// </summary>
        /// <param name="listBox">
        /// A <see cref="ListBox"/> that holds the list box.
        /// </param>
        /// <param name="searchText">
        /// A <see cref="string"/> that holds the search text.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the found status. If found returns
        /// true else returns false.
        /// </returns>
        private bool FindByText(ListBox listBox, string searchText)
        {
            if (listBox == null)
                throw new Exception("List box object cannot be null");

            if (searchText == null)
                searchText = string.Empty;

            foreach (ListItem item in listBox.Items)
            {
                if (item.Text.ToUpper() == searchText.Trim().ToUpper())
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Method that clear all labels message
        /// clicked.
        /// </summary>    
        private void ClearAllLabelMessage()
        {
            // Clear messages.
            CompetencyVectors_bottomSuccessMessageLabel.Text = string.Empty;
            CompetencyVectors_bottomErrorMessageLabel.Text = string.Empty;
            CompetencyVectors_topSuccessMessageLabel.Text = string.Empty;
            CompetencyVectors_topErrorMessageLabel.Text = string.Empty;
        }


        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "CVG":
                    this.CompetencyVectors_newCompetencyVectorGroupLinkButton_Click(this, new EventArgs());
                    break;
                case "CV":
                    this.CompetencyVectors_newCompetencyVectorLinkButton_Click(this, new EventArgs());
                    break;
                case "VP":
                    this.CompetencyVectors_newVectorParameterLinkButton_Click(this, new EventArgs());
                    break;
            }
        }
        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            // Validate reason feild           
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CompetencyVectors_newCompetencyVectorGroupTextBox.Focus();
            GetCompetencyVectorGroups();
            GetCompetencyVectorParameters();
        }

        #endregion Protected Overridden Methods                                
    }
}
