﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ModuleManagement.aspx.cs
// File that represents the ModuleManagement class that defines the user 
// interface layout and functionalities for the ModuleManagement page. This 
// page helps in managing the modules and sub modules. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the ModuleManagement page. This page helps in managing the modules
    /// and sub modules. This class inherits the Forte.HCM.UI.Common.PageBase
    /// class.
    /// </summary>
    public partial class ModuleManagement : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //Set the page caption in the page
                Master.SetPageCaption("Module Management");

                ModuleManagement_topSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_bottomSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_topErrorMessageLabel.Text = string.Empty;
                ModuleManagement_bottomErrorMessageLabel.Text = string.Empty;

                ModuleManagement_modulePageNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                        (ModuleManagement_modulePageNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    //Load the default values
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "ROLECODE";

                    ViewState["SORT_FIELD1"] = "MODCODE";
                    ModuleManagement_showhide.Text = "Hide Description";
                    LoadValues();

                    GetSubModuleDetails(1);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of module section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void ModuleManagement_modulePageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["pagenumber"] = Convert.ToInt32(e.PageNumber);
                ModuleManagement_moduleGridView.EditIndex = -1;
                ModuleManagement_categoryTypePageNumberHidden.Value = e.PageNumber.ToString();
                GetModuleDetails(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ModuleManagement_resetLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the cancel edit action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ModuleManagement_moduleGridView_RowCancelingEdit
            (object sender, GridViewCancelEditEventArgs e)
        {
            try
            {

                ModuleManagement_topSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_bottomSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_topErrorMessageLabel.Text = string.Empty;
                ModuleManagement_bottomErrorMessageLabel.Text = string.Empty;

                if (ModuleManagement_moduleGridView.EditIndex != -1)
                {
                    base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                        ModuleManagement_bottomErrorMessageLabel, Resources.HCMResource.
                        ModuleManagement_PleaseUpdateModule);
                    return;
                }

                ModuleManagement_moduleGridView.EditIndex = -1;
                GetModuleDetails(Convert.ToInt32(ModuleManagement_categoryTypePageNumberHidden.Value));
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_editImageButton")).Visible = true;
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_deleteImageButton")).Visible = true;
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_cancelUpdateImageButton")).Visible = false;
                ModuleManagement_deleteHiddenField.Value = "";
                GetSubModuleDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when
        /// RowEditing event of the ModuleManagement_moduleGridView control
        /// takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The 
        /// <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_moduleGridView_RowEditing
            (object sender, GridViewEditEventArgs e)
        {

            ModuleManagement_topSuccessMessageLabel.Text = string.Empty;
            ModuleManagement_bottomSuccessMessageLabel.Text = string.Empty;
            ModuleManagement_topErrorMessageLabel.Text = string.Empty;
            ModuleManagement_bottomErrorMessageLabel.Text = string.Empty;

            ModuleManagement_moduleGridView.EditIndex = e.NewEditIndex;
            GetModuleDetails(Convert.ToInt32(ModuleManagement_categoryTypePageNumberHidden.Value));
            ((ImageButton)ModuleManagement_moduleGridView.Rows[e.NewEditIndex].
                FindControl("ModuleManagement_moduleGridView_UpdateImageButton")).Visible = true;
            ((ImageButton)ModuleManagement_moduleGridView.Rows[e.NewEditIndex].
                FindControl("ModuleManagement_moduleGridView_editImageButton")).Visible = false;
            ((ImageButton)ModuleManagement_moduleGridView.Rows[e.NewEditIndex].
                FindControl("ModuleManagement_moduleGridView_deleteImageButton")).Visible = false;
            ((ImageButton)ModuleManagement_moduleGridView.Rows[e.NewEditIndex].
               FindControl("ModuleManagement_moduleGridView_cancelUpdateImageButton")).Visible = true;
            ModuleManagement_deleteHiddenField.Value = ((HiddenField)ModuleManagement_moduleGridView.Rows[e.NewEditIndex].
                    FindControl("ModuleManagement_editModuleIDHiddenField")).Value;
            //((TextBox)ModuleManagement_moduleGridView.Rows[e.NewEditIndex].
            //  FindControl("ModuleManagement_moduleGridView_editcodeTextBox")).Focus();
            GetSubModuleDetails(1);
        }
        /// <summary>
        /// Handler method that will be called when
        /// RowUpdating event of the ModuleManagement_moduleGridView control
        /// takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_moduleGridView_RowUpdating
            (object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ModuleManagement_moduleGridView.EditIndex = -1;

                ModuleManagement_topSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_bottomSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_topErrorMessageLabel.Text = string.Empty;
                ModuleManagement_bottomErrorMessageLabel.Text = string.Empty;

                int rowAffected = 0;

                rowAffected = new AdminBLManager().UpdateModule
                    (((TextBox)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_editcodeTextBox")).Text,
                    ((TextBox)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_editmoduleNameTextBox")).Text,
                    Convert.ToInt32(ModuleManagement_deleteHiddenField.Value), base.userID);

                if (rowAffected == 0)
                    base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                     ModuleManagement_bottomSuccessMessageLabel, Resources.HCMResource.
                     ModuleManagement_ModuleUpdatedSuccessfully);
                else

                    base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                     ModuleManagement_bottomSuccessMessageLabel,
                     Resources.HCMResource.ModuleManagement_ModuleAlreadyExists);

                GetSubModuleDetails(1);


            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                GetModuleDetails(Convert.ToInt32(ModuleManagement_categoryTypePageNumberHidden.Value));
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_editImageButton")).Visible = true;
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_deleteImageButton")).Visible = true;
                ((ImageButton)ModuleManagement_moduleGridView.Rows[e.RowIndex].
                    FindControl("ModuleManagement_moduleGridView_cancelUpdateImageButton")).Visible = false;
                ModuleManagement_deleteHiddenField.Value = "";
                GetSubModuleDetails(1);
            }
        }
        /// <summary>
        /// Handler method that will be called when
        /// RowCommand event of the ModuleManagement_moduleGridView 
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_moduleGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ModuleManagement_topSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_bottomSuccessMessageLabel.Text = string.Empty;
                ModuleManagement_topErrorMessageLabel.Text = string.Empty;
                ModuleManagement_bottomErrorMessageLabel.Text = string.Empty;

                if (ModuleManagement_moduleGridView.EditIndex != -1)
                {
                    base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                        ModuleManagement_bottomErrorMessageLabel, Resources.HCMResource.
                        ModuleManagement_PleaseUpdateModule);
                    return;
                }

                if (e.CommandName != "DeleteModule")
                    return;

                ModuleManagement_deleteHiddenField.Value = e.CommandArgument.ToString();
                string result = string.Empty;
                result = new AdminBLManager().DeleteModuleResults(
               ((HiddenField)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
               FindControl("ModuleManagement_moduleIDHiddenField")).Value);
                if (result != "SUCCESS")
                {
                    ModuleManagement_topSuccessMessageLabel.ForeColor = Color.Red;
                    ModuleManagement_bottomSuccessMessageLabel.ForeColor = Color.Red;
                    base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                    ModuleManagement_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.ModuleManagement_ModuleCannotBeDeleted,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("ModuleManagement_moduleGridView_modcodeLabel")).Text));
                    return;
                }
                ModuleManagement_deletemoduleConfirmMsgControl.Message = string.Format
                    (Resources.HCMResource.ModuleManagement_moduleDeleteConfirmation,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("ModuleManagement_moduleGridView_modcodeLabel")).Text + '-' +
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("ModuleManagement_moduleGridView_modulenameLabel")).Text);
                ModuleManagement_deletemoduleConfirmMsgControl.Type = MessageBoxType.YesNo;
                ModuleManagement_deletemoduleConfirmMsgControl.Title = "Warning";
                ModuleManagement_deleteModuleModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when
        /// okClick event of the ModuleManagement_deleteSubModuleModuleConfirmMsgControl 
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_deleteSubModuleModuleConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                int modID = int.Parse(ModuleManagement_deleteRoleHiddenField.Value);

                new AdminBLManager().DeleteSubModule(modID);

                //Reset the page navigator
                //  ModuleManagement_rolesPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";
                GetModuleDetails(1);
                GetSubModuleDetails(1);
                base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                    ModuleManagement_bottomSuccessMessageLabel, Resources.HCMResource.
                       ModuleManagement_subModuleDeletedSuccessfully);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when
        /// cancelClick event of the ModuleManagement_deleteSubModuleConfirmMsgControl 
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_deleteSubModuleConfirmMsgControl_cancelClick
          (object sender, EventArgs e)
        {
            try
            {
                ModuleManagement_deleteRoleHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when
        /// okClick event of the ModuleManagement_deleteModuleConfirmMsgControl
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_deleteModuleConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                string modID = ModuleManagement_deleteHiddenField.Value;

                new AdminBLManager().DeleteModule(modID);

                //Reset the page navigator
                //  ModuleManagement_rolesPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";
                GetModuleDetails(1);
                GetSubModuleDetails(1);
                base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                    ModuleManagement_bottomSuccessMessageLabel, Resources.HCMResource.
                       ModuleManagement_ModuleDeletedSuccessfully);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when
        /// the cancelClick event of the ModuleManagement_deleteModuleConfirmMsgControl
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_deleteModuleConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                ModuleManagement_deleteHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
       
        /// <summary>
        /// Handler that will be called when close Clicked event
        /// of the ModuleManagement_updateModuleCloseLinkButton control
        /// takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_updateModuleCloseLinkButton_Clicked
            (object sender, EventArgs e)
        {
            try
            {
                ModuleManagement_deleteHiddenField.Value = string.Empty;
                ModuleManagement_updateModuleIsEditORSaveHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when save Clicked event 
        /// of the ModuleManagement_updateModuleCodeNameSaveButton
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_updateModuleCodeNameSaveButton_Clicked
          (object sender, EventArgs e)
        {
            ModuleManagement_updateModuleNameErrorLabel.Text = null;
            try
            {

                if (ModuleManagement_updateModuleCodeTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(ModuleManagement_updateModuleNameErrorLabel,
                        Resources.HCMResource.ModuleManagement_EnterModuleCode);


                    ModuleManagement_updateModuleNameModalPopupExtender.Show();

                    return;
                }

                if (ModuleManagement_updateModuleNameTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(ModuleManagement_updateModuleNameErrorLabel,
                        Resources.HCMResource.ModuleManagement_EnterModuleName);

                    ModuleManagement_updateModuleNameModalPopupExtender.Show();

                    return;
                }

                if (ModuleManagement_updateModuleIsEditORSaveHiddenField.Value.ToUpper() == "EDIT")
                {
                    int id = int.Parse(ModuleManagement_deleteHiddenField.Value);


                    new AuthenticationBLManager().UpdateRoleCategory(ModuleManagement_updateModuleCodeTextBox.Text,
                        ModuleManagement_updateModuleNameTextBox.Text, id, base.userID);

                    //base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                    //   ModuleManagement_bottomSuccessLabel,
                    //   Resources.HCMResource.ModuleManagement_RoleUpdatedSuccessfully);
                }
                if (ModuleManagement_updateModuleIsEditORSaveHiddenField.Value.ToUpper() == "ADDNEW")
                {

                    ModuleManagement_updateModuleCodeTextBox.Focus();
                    int rowsAffected = 0;


                    rowsAffected = new AdminBLManager().InsertNewModule(
                        ModuleManagement_updateModuleCodeTextBox.Text,
                        ModuleManagement_updateModuleNameTextBox.Text, base.userID);



                    if (rowsAffected == 0)
                    {

                        base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                      ModuleManagement_bottomSuccessMessageLabel,
                       Resources.HCMResource.RoleManagement_NewModuleInsertedSuccessfully);
                    }
                    else
                    {
                        base.ShowMessage(ModuleManagement_updateModuleNameErrorLabel,
                        Resources.HCMResource.ModuleManagement_ModuleAlreadyExists);

                        ModuleManagement_updateModuleNameModalPopupExtender.Show();
                        return;
                    }


                }

                //Reset the page navigator
                ModuleManagement_modulePageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD1"] = "ROLECODE";


                //Get the form details
                GetModuleDetails(1);
                GetSubModuleDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when save Clicked 
        /// event of the ModuleManagement_updateSubModuleSaveButton 
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_updateSubModuleSaveButton_Clicked
         (object sender, EventArgs e)
        {
            ModuleManagement_updateSubModuleNameErrorLabel.Text = null;
            try
            {

                if (ModuleManagement_updatesubmoduleDropdownlist.SelectedIndex == 0)
                {
                    base.ShowMessage(ModuleManagement_updateSubModuleNameErrorLabel,
                        Resources.HCMResource.ModuleManagement_EnterModuleCodeName);

                    ModuleManagement_updateSubModuleModalPopupExtender.Show();

                    return;
                }
                if (ModuleManagement_updateSubModuleCodeTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(ModuleManagement_updateSubModuleNameErrorLabel,
                        Resources.HCMResource.ModuleManagement_EnterSubModuleCode);

                    ModuleManagement_updateSubModuleModalPopupExtender.Show();

                    return;
                }
                if (ModuleManagement_updateSubModuleNameTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(ModuleManagement_updateSubModuleNameErrorLabel,
                        Resources.HCMResource.ModuleManagement_EnterSubModuleName);

                    ModuleManagement_updateSubModuleModalPopupExtender.Show();

                    return;
                }



                if (ModuleManagement_updateRoleIsEditORSaveHiddenField.Value.ToUpper() == "EDIT")
                {
                    int id = int.Parse(ModuleManagement_deleteHiddenField.Value);



                }
                if (ModuleManagement_updateRoleIsEditORSaveHiddenField.Value.ToUpper() == "ADDNEW")
                {


                    int rowsAffected = 0;
                    rowsAffected = new AdminBLManager().InsertNewSubModule(ModuleManagement_updateSubModuleCodeTextBox.Text,
                     Convert.ToInt32(ModuleManagement_updatesubmoduleDropdownlist.SelectedValue),
                     ModuleManagement_updateSubModuleNameTextBox.Text, base.userID);

                    if (rowsAffected == 0)
                        base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                       ModuleManagement_bottomSuccessMessageLabel,
                        Resources.HCMResource.ModuleManagement_NewSubModuleInsertedSuccessfully);
                    else
                    {
                        base.ShowMessage(ModuleManagement_updateSubModuleNameErrorLabel,
                       Resources.HCMResource.ModuleManagement_SubModuleAlreadyExists);

                        ModuleManagement_updateSubModuleModalPopupExtender.Show();
                        return;
                    }

                }

                //Reset the page navigator
                ModuleManagement_modulePageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";

                //Get the form details
                GetSubModuleDetails(1);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when add Click event
        /// of the ModuleManagement_moduleAddImageButton control
        /// takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_moduleAddImageButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                ModuleManagement_updateModuleNameErrorLabel.Text = null;
                if (ModuleManagement_moduleGridView.EditIndex != -1)
                {
                    base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                  ModuleManagement_bottomErrorMessageLabel, Resources.HCMResource.
                  ModuleManagement_PleaseUpdateModule);
                    return;
                }
                ModuleManagement_updateModuleIsEditORSaveHiddenField.Value = "ADDNEW";
                ModuleManagement_updateModuleLiteral.Text = "Add Module";
                ModuleManagement_updateModuleCodeTextBox.Focus();
                ModuleManagement_updateModuleCodeTextBox.Text = string.Empty;
                ModuleManagement_updateModuleNameTextBox.Text = string.Empty;
                ModuleManagement_updateModuleNameModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when Add Click 
        /// event of the ModuleManagement_rolesAddImageButton 
        /// controltakes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_submoduleAddImageButton_Click
            (object sender, EventArgs e)
        {
            try
            {

                ModuleManagement_updateSubModuleNameErrorLabel.Text = null;
                if (ModuleManagement_submoduleGridView.EditIndex != -1)
                {
                    base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                  ModuleManagement_bottomErrorMessageLabel, Resources.HCMResource.
                  ModuleManagement_PleaseUpdateModule);
                    return;
                }
                ModuleManagement_updateRoleIsEditORSaveHiddenField.Value = "ADDNEW";
                ModuleManagement_updateRoleLiteral.Text = "Add Sub Module";
                string sortOrder = ViewState["SORT_ORDER"].ToString();

                string sortExpression = ViewState["SORT_FIELD"].ToString();

                int totalPage = 0;



                List<ModuleManagementDetails> moduleDetails = new AdminBLManager().GetModules(
                    sortExpression, sortOrder, base.GridPageSize, 1, out totalPage);


                ModuleManagement_updatesubmoduleDropdownlist.DataSource = moduleDetails;

                ModuleManagement_updatesubmoduleDropdownlist.DataTextField = "ModeCodeName";
                ModuleManagement_updatesubmoduleDropdownlist.DataValueField = "ModID";

                ModuleManagement_updatesubmoduleDropdownlist.DataBind();
                ModuleManagement_updatesubmoduleDropdownlist.Items.Insert(0, new ListItem("--Select--"));
                ModuleManagement_updateSubModuleCodeTextBox.Text = string.Empty;
                ModuleManagement_updateSubModuleCodeTextBox.Text = string.Empty;
                ModuleManagement_updateSubModuleCodeTextBox.Focus();

                ModuleManagement_updateSubModuleModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when 
        /// RowCommand event of the ModuleManagement_submoduleGridView 
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_submoduleGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteSubModule")
                    return;
                ModuleManagement_deleteRoleHiddenField.Value = e.CommandArgument.ToString();
                ModuleManagement_deleteRolesConfirmMsgControl.Message = string.Format
                    (Resources.HCMResource.ModuleManagement_SubModuleDeleteConfirmation,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("ModuleManagement_submodulecodeHiddenLabel")).Text + '-' +
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("ModuleManagement_submodulenameHiddenLabel")).Text);

                ModuleManagement_deleteRolesConfirmMsgControl.Type = MessageBoxType.YesNo;
                ModuleManagement_deleteRolesConfirmMsgControl.Title = "Warning";
                ModuleManagement_deleteRoleFormModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when
        /// Sorting event of the ModuleManagement_moduleGridView 
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_moduleGridView_Sorting
          (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                if (ViewState["SORT_FIELD1"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD1"] = e.SortExpression;
                ModuleManagement_modulePageNavigator.Reset();
                GetModuleDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when 
        /// RowCreated event of the ModuleManagement_moduleGridView 
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> 
        /// instance containing the event data.</param>
        protected void ModuleManagement_moduleGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (ModuleManagement_moduleGridView,
                    (string)ViewState["SORT_FIELD1"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when
        /// SaveClick event of the ModuleManagement control
        /// takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_SaveClick(object sender, EventArgs e)
        {
            try
            {
                ModuleManagement_topErrorMessageLabel.Text = string.Empty;
                ModuleManagement_bottomErrorMessageLabel.Text = string.Empty;
                for (int i = 0; i < ModuleManagement_submoduleGridView.Rows.Count; i++)
                {
                    TextBox tb = (TextBox)ModuleManagement_submoduleGridView.Rows[i].FindControl
                          ("ModuleManagement_submoduleGridView_submodulecodeTextBox");

                    TextBox tb3 = (TextBox)ModuleManagement_submoduleGridView.Rows[i].FindControl
                         ("ModuleManagement_submoduleGridView_submodulenameTextBox");



                    if (tb.Text == string.Empty || tb3.Text == string.Empty)
                    {
                        base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, "Mandatory fields cannot be empty");
                        return;
                    }
                    for (int q = i; q < ModuleManagement_submoduleGridView.Rows.Count - 1; q++)
                    {
                        TextBox tb1 = (TextBox)ModuleManagement_submoduleGridView.Rows[q + 1].FindControl
                          ("ModuleManagement_submoduleGridView_submodulecodeTextBox");

                        if (tb.Text.Trim() == tb1.Text.Trim())
                        {
                            base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel,
                    string.Format(Resources.HCMResource.ModuleManagementSubModuleAlreadyExists,
                    tb.Text.Trim()));

                            return;
                        }

                    }
                }

                UpdateSubmodule();

                GetSubModuleDetails(1);

                base.ShowMessage(ModuleManagement_topSuccessMessageLabel,
                 ModuleManagement_bottomSuccessMessageLabel, Resources.HCMResource.
                 ModuleManagement_SubModuleUpdatedSuccessfully);
            }
            catch (Exception exception)
            {

                Logger.ExceptionLog(exception);
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called when
        /// showhideClick event of the ModuleManagement
        /// control takes place.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ModuleManagement_showhideClick(object sender, EventArgs e)
        {

            if (ModuleManagement_showhide.Text != "Hide Description")
            {
                foreach (GridViewRow gvr in ModuleManagement_submoduleGridView.Rows)
                {
                    TextBox tb = (TextBox)gvr.FindControl
                        ("ModuleManagement_roleGridView_roleJobDescriptionTextBox");

                    tb.Visible = true;
                }
                ModuleManagement_showhide.Text = "Hide Description";
                ModuleManagement_showhide.ToolTip = "Click to hide role description";
                return;
            }

            foreach (GridViewRow gvr in ModuleManagement_submoduleGridView.Rows)
            {
                TextBox tb = (TextBox)gvr.FindControl
                    ("ModuleManagement_roleGridView_roleJobDescriptionTextBox");

                tb.Visible = false;
            }
            ModuleManagement_showhide.ToolTip = "Click to show role description";
            ModuleManagement_showhide.Text = "Show Description";

        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that load the sub modules.
        /// </summary>
        private void LoadSubModules()
        {
            //Reset the page navigator
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD1"] = "SUBMODULECODE";

            //Get the form details
            GetSubModuleDetails(1);
        }
        /// <summary>
        /// Gets the sub module details.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void GetSubModuleDetails(int pageNumber)
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD1"].ToString();

            List<ModuleManagementDetails> subModuleList = new AdminBLManager().GetSubModules(
               sortExpression, sortOrder, pageNumber);

            ModuleManagement_submoduleGridView.DataSource = subModuleList;

            ModuleManagement_submoduleGridView.DataBind();

           // BindModuleDropdownList();

        }
        /// <summary>
        /// Binds the module dropdown list.
        /// </summary>
        private void BindModuleDropdownList()
        {
            int totalPage = 0;

            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            DropDownList ddlModule = null;

            List<ModuleManagementDetails> moduleList = new AdminBLManager().GetModule(
                sortExpression, sortOrder, base.GridPageSize, 1, out totalPage);

            for (int i = 0; i < ModuleManagement_submoduleGridView.Rows.Count; i++)
            {
                ddlModule = (DropDownList)ModuleManagement_submoduleGridView.Rows[i].
                    FindControl("ModuleManagement_updateSubmoduleDropdownList");

                ddlModule.DataSource = moduleList;
                ddlModule.DataTextField = "ModuleCodeName";
                ddlModule.DataValueField = "ModID";
                ddlModule.DataBind();
                try
                {

                    ddlModule.SelectedItem.Selected = false;

                    Label lbl = ((Label)ModuleManagement_submoduleGridView.Rows[i].
                        FindControl("RoleManagment_submoduleCodeName"));
                    for (int j = 0; j < ddlModule.Items.Count; j++)
                    {
                        if (lbl.Text.Trim() == ddlModule.Items[j].Text.Trim())
                        {
                            ddlModule.Items[j].Selected = true;
                            continue;
                        }
                    }
                }
                catch
                {

                }
                ddlModule = null;
            }

        }
        /// <summary>
        /// Gets the module details.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void GetModuleDetails(int pageNumber)
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD1"].ToString();

            int totalPage = 0;

            List<ModuleManagementDetails> moduleList = new AdminBLManager().
                GetModules(sortExpression, sortOrder, base.GridPageSize, pageNumber, out totalPage);

            ModuleManagement_moduleGridView.DataSource = moduleList;

            ModuleManagement_moduleGridView.DataBind();

            ModuleManagement_modulePageNavigator.Visible = true;

            ModuleManagement_modulePageNavigator.TotalRecords = totalPage;

            ModuleManagement_modulePageNavigator.PageSize = base.GridPageSize;

            ModuleManagement_moduleGridViewDIV.Style["display"] = "block";

            if (moduleList == null || moduleList.Count == 0)
            {
                base.ShowMessage(ModuleManagement_topErrorMessageLabel,
                    ModuleManagement_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);

                ModuleManagement_moduleGridViewDIV.Style["display"] = "none";
            }
        }
        /// <summary>
        /// Updates the submodule.
        /// </summary>
        private void UpdateSubmodule()
        {
            List<ModuleManagementDetails> submoduleDetails = null;
            ModuleManagementDetails subModuleDetail = null;

            for (int i = 0; i < ModuleManagement_submoduleGridView.Rows.Count; i++)
            {
                if (subModuleDetail == null)
                    subModuleDetail = new ModuleManagementDetails();

                subModuleDetail.SubModuleID = Convert.ToInt32(
                    ((HiddenField)ModuleManagement_submoduleGridView.Rows[i].
                    FindControl("ModuleManagement_submoduleidHiddenLabel")).Value);

                /* subModuleDetail.ModID =Convert.ToInt32( ((DropDownList)ModuleManagement_submoduleGridView.Rows[i].
                                  FindControl("ModuleManagement_updateSubmoduleDropdownList")).SelectedValue);*/


                subModuleDetail.SubModuleCode = ((TextBox)ModuleManagement_submoduleGridView.Rows[i].
                                 FindControl("ModuleManagement_submoduleGridView_submodulecodeTextBox")).Text.Trim();

                subModuleDetail.SubModuleName = ((TextBox)ModuleManagement_submoduleGridView.Rows[i].
                                FindControl("ModuleManagement_submoduleGridView_submodulenameTextBox")).Text.Trim();


                subModuleDetail.ModifiedBy = base.userID;

                if (submoduleDetails == null)
                    submoduleDetails = new List<ModuleManagementDetails>();

                submoduleDetails.Add(subModuleDetail);
                subModuleDetail = null;
            }


            new AdminBLManager().UpdateSubModuleDetails(submoduleDetails);

        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   
        
        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
           
            //Reset the page navigator
            ModuleManagement_modulePageNavigator.Reset();

            
            GetModuleDetails(1);
        }
        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }

        #endregion Protected Overridden Methods
    }
}