﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewAccount.cs
// File that represents the user interface to view the account
// details of the coroprate user admin. He/she can edit or
// upgrade or change his/her password from this page.

#endregion Header                                                              

#region Namespace                                                              

using System;
using System.Collections.Generic;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using Resources;

#endregion Namespace

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for ViewAccount page. This is used
    /// to view his/her account details. From this page user
    /// can navigate to edit his/her account or upgrade his/her corporate account
    /// or to change his/her password
    /// This class is inherited from Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ViewAccount : PageBase
    {

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(HCMResource.ViewAccount_PageTitle);
                if (IsPostBack)
                    return;
                LoadValues();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void ViewAccount_EditAccountButtonClick(object sender, EventArgs e)
        {
            Response.Redirect("EditAccount.aspx?m=0&s=1&parentpage=" + Constants.ParentPage.VIEW_ACCOUNT, false);
        }

        protected void ViewAccount_ResetButtonClick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void ViewAccount_UpgradeButtonClick(object sender, EventArgs e)
        {
            Response.Redirect("UpgradeAccount.aspx?m=0&s=2&parentpage=" + Constants.ParentPage.VIEW_ACCOUNT, false);
        }

        #endregion Events

        #region Private Methods                                                

        private void GetSubscriptionAndFeatureDetails(out UserRegistrationInfo userInfo, out SubscriptionTypes subscriptionTypes, out List<SubscriptionFeatures> subscriptionFeatures)
        {
            new Forte.HCM.BL.UserRegistrationBLManager().GetSubscriptionAccountDetailsUser(base.userID, base.tenantID,
                out userInfo, out subscriptionTypes, out subscriptionFeatures, null);
        }

        private void BindSubscriptionAndFeatureDetails()
        {
            UserRegistrationInfo userInfo = null;
            SubscriptionTypes subscriptionTypes = null;
            List<SubscriptionFeatures> subscriptionFeatures = null;
            try
            {
                GetSubscriptionAndFeatureDetails(out userInfo, out subscriptionTypes, out subscriptionFeatures);
                if ((userInfo == null || subscriptionTypes == null) && base.isSiteAdmin == false)
                {
                    RedirectToAccessDeniedPage();
                    return;
                }
                Forte_ViewAccount_subscriptionTypeLabel.Text = subscriptionTypes.SubscriptionName;
                Forte_ViewAccount_statusLabel.Text = userInfo.IsActive == 1 ? "Active" : "In-Active";
                Forte_ViewAccount_subscribedOnLabel.Text = Convert.ToDateTime(userInfo.CreatedDate).ToString("MM/dd/yyyy");
                Forte_ViewAccount_activatedOnLabel.Text = Convert.ToDateTime(userInfo.ActivatedOn).ToString("MM/dd/yyyy");
                Forte_ViewAccount_noOfUsersLabel.Text = userInfo.NumberOfUsers.ToString();
                Forte_ViewAccount_titleLabel.Text = userInfo.Title;
                Forte_ViewAccount_isTrialLabel.Text = userInfo.IsTrial == 1 ? "Yes" : "No";
                Forte_ViewAccount_companyLabel.Text = userInfo.Company;
                Forte_ViewAccount_phoneLabel.Text = userInfo.Phone;
                Forte_ViewAccount_firstNameLabel.Text = userInfo.FirstName;
                Forte_ViewAccount_lastNameLabel.Text = userInfo.LastName;
                Forte_ViewAccount_featureAndPricingGridView.DataSource = subscriptionFeatures;
                Forte_ViewAccount_featureAndPricingGridView.DataBind();
                Forte_ViewAccount_lastLoginLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
            }
            finally
            {
                if (userInfo != null) userInfo = null;
                if (subscriptionTypes != null) subscriptionTypes = null;
                if (subscriptionFeatures != null) subscriptionFeatures = null;
            }
        }

        /// <summary>
        /// A method that redirects to the unzuthorized page
        /// </summary>
        private void RedirectToAccessDeniedPage()
        {
            Response.Redirect("../Common/AccessDenied.aspx", false);
        }

        /// <summary>
        /// A Method that logs the exception message and 
        /// display's the error message to the user.
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception message
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            base.ShowMessage(Forte_ViewAccount_topErrorMessageLabel,
                Forte_ViewAccount_bottomErrorMessageLabel, exp.Message);
        }

        #endregion Private Methods

        #region Override Methods                                               

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            BindSubscriptionAndFeatureDetails();
        }

        #endregion Override Methods
    }
}