﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/CustomerAdminMaster.Master"
    CodeBehind="EnrollAssessor.aspx.cs" Inherits="Forte.HCM.UI.Admin.EnrollAssessor" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
 
<asp:Content ID="EnrollAssessor_content" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
        function SubmitPhoto() {
            document.getElementById('<%=EnrollAssessor_photoUploadedHiddenFiled.ClientID %>').value = 'Y';
            __doPostBack('', '');
        }

        //To validate the photo control
        function EnrollAssessor_assessorPhotoClientUploadComplete(sender, args) {
           
            /*var photosize=<%=Session["PHOTO_SIZE"] %>;
            if(photosize!=null)
            {
                if(parseInt(args.get_length())>parseInt(photosize))
                {
                     var errorlabelclientId ='<%=EnrollAssessor_topErrorMessageLabel.ClientID %>'; 
                     document.getElementById(errorlabelclientId).value="File size exceeds the maximum limit of" ;
                }
            }*/
             
            var handlerPage = '<%= Page.ResolveClientUrl("~/Common/CandidateImageHandler.ashx")%>';
            var queryString = '?source=ASSESSOR_CREA_PAGE&randomno=' + getRandomNumber();
            var src = handlerPage + queryString;
            var clientId = '<%=EnrollAssessor_assessorPhotoPreview.ClientID %>';
            document.getElementById(clientId).setAttribute("src", src);
            $get("<%=EnrollAssessor_photoClearLinkButton.ClientID %>").style.display = 'block';
        }

        function EnrollAssessor_assessorPhotoUploadError(sender, args) {
            
            return false;
        }

        function getRandomNumber() {
            var randomnumber = Math.random(10000);
            return randomnumber;
        }
        function ShowLoadingImage() {
            document.getElementById("<%=EnrollAssessor_newAssessorSkillTextBox.ClientID%>").className = "position_profile_client_bg";
        }
        function HideLoadingImage() {
            document.getElementById("<%=EnrollAssessor_newAssessorSkillTextBox.ClientID%>").className = "";
        }
  
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="EnrollAssessor_headerLiteral" runat="server" Text="Assessor Detail"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="EnrollAssessor_topSaveButton" runat="server" SkinID="sknButtonId"
                                            OnClick="EnrollAssessor_topSaveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollAssessor_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="EnrollAssessor_topResetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollAssessor_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
             <asp:UpdatePanel ID="EnrollAssessor_topMessageUpdatePanel" runat="server"  UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="EnrollAssessor_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="EnrollAssessor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                </ContentTemplate>
                   <Triggers>
                        <asp:AsyncPostBackTrigger ControlID ="EnrollAssessor_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID ="EnrollAssessor_bottomSaveButton" /> 
                        <asp:AsyncPostBackTrigger ControlID="EnrollAssessor_newAssessorSearchImageButton" />
                    </Triggers>
                </asp:UpdatePanel>   
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
            <asp:UpdatePanel ID="EnrollAssessor_bodyUpdatePanel" runat="server">
                    <ContentTemplate> 
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="td_height_10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="EnrollAssessor_internalDiv" runat="server"> 
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 50%" valign="top">
                                                        <table width="100%" cellpadding="1" cellspacing="2" border="0">
                                                            <tr>
                                                                <td colspan="3" align="left">
                                                                    <table cellpadding="0" cellspacing="0" width="100%" align="center" height="350px" >
                                                                        <tr>
                                                                            <td class="header_bg">
                                                                                <asp:Literal ID="Literal2" runat="server" Text="Assessor Details"></asp:Literal>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="grid_body_bg" valign="top">
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="350px">
                                                                                    <tr>
                                                                                            <td style="width: 14%">
                                                                                                <asp:Label ID="EnrollAssessor_newAssessorNameHeaderLabel" runat="server" Text="User"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td style="width: 1%;" align="right">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div>
                                                                                                   <asp:UpdatePanel ID="EnrollAssessor_newAssessorUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <asp:TextBox ID="EnrollAssessor_newAssessorUserNameTextBox" runat="server" MaxLength="100"
                                                                                                                Width="90%" AutoCompleteType="None" ReadOnly="True"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="EnrollAssessor_newAssessorUserIDhiddenField" runat="server" />
                                                                                                            &nbsp;
                                                                                                            <asp:ImageButton ID="EnrollAssessor_newAssessorSearchImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select assessor" 
                                                                                                                OnClick="EnrollAssessor_newAssessorSearchImageButton_Click" />
                                                                                                            <div id="EnrollAssessor_newAssessorUserEmailAvailableStatusDiv" runat="server" style="display: none;">
                                                                                                                <asp:Label ID="EnrollAssessor_newAssessorInValidEmailAvailableStatus" runat="server"
                                                                                                                    Width="100%" EnableViewState="false" ForeColor="Red"></asp:Label>
                                                                                                                <asp:Label ID="EnrollAssessor_newAssessorValidEmailAvailableStatus" runat="server"
                                                                                                                    Width="100%" EnableViewState="false" ForeColor="Green"></asp:Label>
                                                                                                            </div>
                                                                                                    </ContentTemplate>
                                                                                                    </asp:UpdatePanel> 
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                      
                                                                                   <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EnrollAssessor_newAssessorFirstNameHeaderLabel" runat="server" Text="First Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                            </td>
                                                                                            <td style="width: 43%">
                                                                                                <asp:TextBox ID="EnrollAssessor_newAssessorFirstNameTextBox" runat="server" MaxLength="100"
                                                                                                    Width="200px" ReadOnly="True"></asp:TextBox>
                                                                                            </td>
                                                                
                                                                                        </tr>
                                                                                         
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EnrollAssessor_newAssessorLastNameHeaderLabel" runat="server" Text="Last Name"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="EnrollAssessor_newAssessorLastNameTextBox" runat="server" MaxLength="100"
                                                                                                Width="200px" ReadOnly="True"></asp:TextBox>
                                                                                        </td> 
                                                                                        </tr>
                                                                                       
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EnrollAssessor_newAssessorAlternateEmailIDHeaderLabel" runat="server"
                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Alternate Email ID"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="EnrollAssessor_newAssessorAlternateEmailIDTextBox" runat="server"
                                                                                                    Width="200px"></asp:TextBox>
                                                                                            </td> 
                                                                                        </tr> 
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EnrollAssessor_newAssessorMobileHeaderLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                    Text="Mobile Phone"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="EnrollAssessor_newAssessorMobileNumberTextBox" runat="server" Width="200px"
                                                                                                    MaxLength="10">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                 
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EnrollAssessor_newAssessorPhoneHeaderLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                    Text="Home Phone"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="EnrollAssessor_newAssessorHomePhoneTextBox" runat="server" Width="200px"
                                                                                                    MaxLength="10">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                  
                                                                                        </tr>
                                                                    
                                                                                        <tr id="Tr1" runat="server">
                                                                                            <td>
                                                                                                <asp:Label ID="EnrollAssessor_newAssessorAdditionalInfoHeaderLabel" runat="server"
                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Additional Info"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="EnrollAssessor_newAssessorAdditionalTextBox" runat="server" Width="200px"
                                                                                                    Columns="2" TextMode="MultiLine" Height="50px"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EnrollAssessor_linkedInProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                    Text="LinkedID Profile"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td  >
                                                                                                <asp:TextBox ID="EnrollAssessor_linkedInProfileTextBox" runat="server" Columns="2"
                                                                                                    MaxLength="200" Width="90%"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                              <asp:UpdatePanel ID="EnrollAssessor_assessorPhotoUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>  
                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                <tr>
                                                                                                                    <td width="125px">
                                                                                                                        <asp:Label ID="EnrollAssessor_newAssessorSelectPhotoHeaderLabel" runat="server" Text="Select Photo"
                                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <ajaxToolKit:AsyncFileUpload ID="EnrollAssessor_assessorPhotoAsyncFileUpload" runat="server"
                                                                                                                            OnClientUploadComplete="EnrollAssessor_assessorPhotoClientUploadComplete" ClientIDMode="AutoID"
                                                                                                                            OnUploadedComplete="EnrollAssessor_assessorPhotoUploadedComplete" Width="220px" 
                                                                                                                            TabIndex="3" OnClientUploadError="EnrollAssessor_assessorPhotoUploadError" Visible="false"  />

                                                                                                                            <asp:FileUpload ID="EnrollAssessor_selectPhotoFileUpload" size="18"  type="file"  runat="server"  
                                                                                                                            onchange="javascript:SubmitPhoto()" Style="visibility: visible;" />
                                                                                                                            <asp:HiddenField runat="server" ID="EnrollAssessor_photoUploadedHiddenFiled" />
                                                                                                                    </td>
                                                                                                                    <td style="display:none">
                                                                                                                        <asp:ImageButton ID="EnrollAssessor_selectPhotoHelpImageButton" 
                                                                                                                            SkinID="sknHelpImageButton"
                                                                                                                            runat="server" OnClientClick="javascript:return false;" 
                                                                                                                            ToolTip="Click here to select the gif/png/jpg file that contains the photo"
                                                                                                                            ImageAlign="Middle" />
                                                                                                                    </td>
                                                                                                                    <td runat="server" style="display:none">
                                                                                                                        <asp:LinkButton ID="EnrollAssessor_photoClearLinkButton" runat="server"
                                                                                                                            SkinID="sknActionLinkButton" Text="Clear"
                                                                                                                            OnClick="EnrollAssessor_photoClearLinkButton_Click"
                                                                                                                            ToolTip="Click here to clear the photo"></asp:LinkButton>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                   </ContentTemplate>
                                                                                                </asp:UpdatePanel> 
                                                                                            </td> 
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3" align="center" style="padding-right:10px">
                                                                                                <asp:UpdatePanel ID="EnrollAssessor_assessorPreviewUpdatePanel" runat="server">
                                                                                                    <ContentTemplate>  
                                                                                                        <asp:Image runat="server" ID="EnrollAssessor_assessorPhotoPreview" Width="100px"
                                                                                                            Height="100px" ImageUrl="~/Common/CandidateImageHandler.ashx?source=ASSESSOR_CREA_PAGE" 
                                                                                                            AlternateText="Photo not available"/>
                                                                                                  </ContentTemplate>
                                                                                                </asp:UpdatePanel>  
                                                                                            </td>
                                                                                        </tr> 
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>  
                                                        </table>
                                                    </td>
                                                    <td style="width: 50%" valign="top">
                                                        <table width="100%" cellpadding="1" cellspacing="2">
                                                            <tr>
                                                                <td colspan="3" align="center">  
                                                                  <table cellpadding="0" cellspacing="0" width="100%" align="center" height="350px">
                                                                        <tr>
                                                                            <td class="header_bg">
                                                                                <asp:Literal ID="Literal1" runat="server" Text="Assessor Skills"></asp:Literal>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="grid_body_bg" valign="top">
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%"  height="350px">
                                                                                     <tr id="EnrollAssessor_rolesTR" runat="server">
                                                                                        <td>
                                                                                            <asp:Label ID="EnrollAssessor_newAssessorSkillHeaderLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Skill(s)"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <table cellpadding="0" cellspacing="0" border="0" >
                                                                                                <tr>
                                                                                                    <td> 
                                                                                                        <asp:TextBox ID="EnrollAssessor_newAssessorSkillTextBox"  Height="14px"
                                                                                                          runat="server" Width="150px"  AutoPostBack="true"  MaxLength="50" 
                                                                                                          style="font-size:100%;font-family: Tahoma, Arial, sans-serif;color:gray;font-style:normal;text-align:left;"
                                                                                                             OnTextChanged="EnrollAssessor_newAssessorSkillTextBox_TextChanged" >
                                                                                                         </asp:TextBox> 
                                                                                                     </td>
                                                                                                    <td style="padding-left:4px;display:none">
                                                                                                        <asp:ImageButton ID="EnrollAssesssor_NewSkillButton" runat="server" ImageAlign="AbsMiddle"
                                                                                                            SkinID="sknAddVectorGroupImageButton" Style="margin-left: 0px" ToolTip="Enter skill and press this button"
                                                                                                            Width="16px" OnClick="EnrollAssesssor_NewSkillButton_Click" />
                                                                                                    </td>
                                                                                                    <td style="padding-left:4px">
                                                                                                        <asp:ImageButton ID="EnrollAssesssor_SearchSkillButton" runat="server" ImageAlign="AbsMiddle"
                                                                                                         SkinID="sknbtnSearchIcon" Style="margin-left: 0px" ToolTip="Click here to select skill" Width="16px" />&nbsp;&nbsp;
                                                                                                    </td>
                                                                                                    <td style="padding-left:4px">
                                                                                                      <asp:ImageButton ID="EnrollAssessor_newAssessorSkillsImageButton" runat="server"
                                                                                                        ImageAlign="AbsMiddle" OnClientClick="javascript:return false;" SkinID="sknHelpImageButton"
                                                                                                        Style="margin-left: 0px" ToolTip="Enter multiple skills with comma separated"
                                                                                                        Width="16px" />  
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                             </td> 
                                                                                            </tr> 
                                                                                            <tr>
                                                                                                <td class="td_height_5"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_h_line" colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_5"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="3"> 
                                                                                                     <ajaxToolKit:TextBoxWatermarkExtender ID="EnrollAssessor_newAssessorTextBoxWatermarkExtender" runat="server"
                                                                                                        TargetControlID="EnrollAssessor_newAssessorSkillTextBox" WatermarkText="Enter/Select skill name" WatermarkCssClass="assessor_skill_water_mark"   >
                                                                                                    </ajaxToolKit:TextBoxWatermarkExtender>  
                                                                                                    <ajaxToolkit:AutoCompleteExtender ID="EnrollAssessor_SkillAutoCompleteExtender"
                                                                                                            runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetSkillList" 
                                                                                                            TargetControlID="EnrollAssessor_newAssessorSkillTextBox" MinimumPrefixLength="1" 
                                                                                                            CompletionListElementID="pnl"  CompletionListCssClass="position_profile_client_completion_list" 
                                                                                                            CompletionListItemCssClass="position_profile_client_completion_list_item"
                                                                                                             OnClientPopulating="ShowLoadingImage" OnClientPopulated="HideLoadingImage"  
                                                                                                            EnableCaching="true" CompletionSetCount="12" OnClientShowing="ShowLoadingImage" OnClientHiding="HideLoadingImage"
                                                                                                            CompletionListHighlightedItemCssClass="position_profile_client_completion_list_highlight">
                                                                                                    </ajaxToolkit:AutoCompleteExtender>

                                                                                                    <asp:Panel ID="pnl" runat="server">&nbsp;</asp:Panel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr> 
                                                                                                <td align="left" colspan="3" valign="top">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                                        <ContentTemplate>  
                                                                                                        <div style="overflow:auto;height:280px;"> 
                                                                                                                <asp:GridView ID="EnrollAssessor_skillGridView" runat="server" AutoGenerateColumns="False"
                                                                                                                    EnableModelValidation="True" OnRowDeleting="EnrollAssessor_skillGridView_RowDeleting">
                                                                                                                    <Columns>
                                                                                                                        <asp:BoundField DataField="Category" HeaderText="Category" ItemStyle-Width="100px" />
                                                                                                                        <asp:BoundField DataField="Skill" HeaderText="Skill" ItemStyle-Width="150px" />
                                                                                                                        <asp:CommandField ButtonType="Image" DeleteImageUrl="~/Images/delete.png" ShowDeleteButton="True"
                                                                                                                            ItemStyle-Width="50px" />
                                                                                                                        <asp:TemplateField>
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:HiddenField ID="EnrollAssessor_skillGridView_skillIDHiddenField" runat="server"
                                                                                                                                    Value='<%# DataBinder.Eval(Container.DataItem, "SkillID")%>' />
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                </asp:GridView>
                                                                                                            </div>
                                                                                                            <asp:HiddenField ID="EnrollAssessor_CaegoryIDHiddenField" runat="server" />
                                                                                                            <asp:HiddenField ID="EnrollAssessor_SkillIDHiddenField" runat="server" />
                                                                                                            <asp:HiddenField ID="EnrollAssessor_skillHiddenField" runat="server" />
                                                                                                     </ContentTemplate>
                                                                                                    </asp:UpdatePanel>  
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">&nbsp; </td>
                                                            </tr> 
                                                            <tr>
                                                                <td colspan="3" style="padding-top:5px;" valign="top">
                                                                  <asp:Panel ID="EnrollAssessor_newAssessorNonAvailabilityDatesPanel" runat="server" CssClass="popupcontrol_scheduler"
                                                                    Style="display: none" Width="440px" Height="400px">
                                                                        <table cellpadding="0" cellspacing="0" width="20%" align="center">
                                                                            <tr>
                                                                                <td class="td_height_5">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_header_text">
                                                                                     <asp:Literal ID="EnrollAssessor_newAssessorNonAvailabiltyTitleLiteral" runat="server" Text="Set Vacation Dates"></asp:Literal>
                                                                                </td>
                                                                                <td align="right" valign="middle" style="padding-top:5px">
                                                                                    <asp:ImageButton ID="EnrollAssessor_newAssessorImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">&nbsp;</td>
                                                                            </tr>
                                                                            
                                                                            <tr>
                                                                                <td class="header_bg" colspan="2">
                                                                                    <asp:Literal ID="EnrollAssessor_newAssessorNonAvailabiltyHeaderLiteral" runat="server"
                                                                                        Text="Calendar"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="EnrollAssessor_activationOptionTR" runat="server">
                                                                                <td style="width: 350px" colspan="2" align="center" class="grid_body_bg">
                                                                                     <asp:UpdatePanel ID="EnrollAssessor_calendarUpdatePanel" runat="server" UpdateMode="Conditional">
                                                                                        <ContentTemplate> 
                                                                                            <div align="center">
                                                                                                <asp:Calendar ID="EnrollAssessor_calendarNotAvailable" runat="server" BackColor="White"
                                                                                                BorderColor="Black" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black"
                                                                                                Height="250px" Width="340px" SelectionMode="Day" OtherMonthDayStyle-Font-Underline="false"
                                                                                                DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                                                DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                                                TodayDayStyle-Font-Underline="false" OnPreRender="EnrollAssessor_calendarNotAvailable_PreRender"
                                                                                                    OnSelectionChanged="EnrollAssessor_calendarNotAvailable_SelectionChanged" 
                                                                                                    onvisiblemonthchanged="EnrollAssessor_calendarNotAvailable_VisibleMonthChanged" >
                                                                                                <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                                                <SelectedDayStyle Font-Underline="false" BackColor="#FA9476" ForeColor="#000000">
                                                                                                </SelectedDayStyle>
                                                                                                <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                                                <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </asp:Calendar>
                                                                                            </div>
                                                                                       </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:AsyncPostBackTrigger ControlID="EnrollAssessor_newAssessorSearchImageButton"/>
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel> 
                                                                                </td>
                                                                            </tr>
                                                                            
                                                                             <tr>
                                                                                <td class="td_height_5">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2"> 
                                                                                    <asp:Button ID="EnrollAssessor_newAssessorOkButton" runat="server" SkinID="sknButtonId" Text="Save" />&nbsp;&nbsp;
                                                                                     <asp:LinkButton ID="EnrollAssessor_newAssessorCloseLinkButton" runat="server" SkinID="sknPopupLinkButton" Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                     <ajaxToolKit:ModalPopupExtender ID="EnrollAssessor_newAssessorModalPopupExtender"
                                                                        runat="server" BackgroundCssClass="modalBackground" PopupControlID="EnrollAssessor_newAssessorNonAvailabilityDatesPanel"
                                                                        TargetControlID="EnrollAssessor_calendarImageButton" >
                                                                    </ajaxToolKit:ModalPopupExtender>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                               
                                                <tr>
                                                    <td colspan="2" class="tab_body_bg" style="padding:5px">
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td height="20px">
                                                                    <asp:Label ID="EnrollAssessor_profileLastUpdatedLabel" runat="server" SkinID="sknLabelAssessorFieldText" Text="Your profile last updated on "></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="EnrollAssessor_nonAvailabilityDatesLabel" runat="server"  SkinID="sknLabelAssessorFieldText" Text="Set Vacation Dates"></asp:Label>
                                                                </td>
                                                                <td width="36%" align="left"> &nbsp;
                                                                    <asp:ImageButton ID="EnrollAssessor_calendarImageButton" runat="server" ImageAlign="Middle" SkinID="sknCalendarImageButton" ToolTip="Set Vacation Dates" />
                                                               </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                               </tr>
                                            </table> 
                                    </div>
                                </td>
                            </tr>
                        </table>
                 </ContentTemplate>
                </asp:UpdatePanel>  
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
      
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr id="EnrollAssessor_searchResultsTR" runat="server">
            <td class="header_bg" align="center">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" align="left" class="header_text_bold">
                            <asp:Literal ID="EnrollAssessor_searchResultsLiteral" runat="server" Text="Assessment Details"></asp:Literal>
                            &nbsp;<asp:Label ID="EnrollAssessor_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                        </td>
                        <td style="width: 50%" align="right">
                            <%--<span id="EnrollAssessor_searchResultsUpSpan" runat="server" style="display: none;">
                                <asp:Image ID="EnrollAssessor_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                            <span id="EnrollAssessor_searchResultsDownSpan" style="display: block;" runat="server">
                                <asp:Image ID="EnrollAssessor_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>--%>
                            <asp:HiddenField ID="EnrollAssessor_restoreHiddenField" runat="server" />
                            <asp:HiddenField ID="EnrollAssessor_isMaximizedHiddenField" runat="server" />
                        </td>
                    </tr>
                 </table>
            </td>
        </tr>
        <tr>
            <td  width="100%" >
            <asp:UpdatePanel runat="server" ID="EnrollAssessor_assessorEventsUpdatePanel">
                  <ContentTemplate> 
                        <table cellpadding="0" cellspacing="0" width="100%" align="left" runat="server" id="EnrollAssessor_assessorEventsTable">
                            <tr style="display:none">
                                <td class="header_bg">
                                    <asp:Literal ID="EnrollAssessor_assessorEventsLiteral" runat="server" Text="Assessor Events"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td class="grid_body_bg"> 
                                     <table cellpadding="2" cellspacing="2" width="100%">
                                        <tr>
                                                <td width="24%">
                                                    <asp:Label ID="EnrollAssessor_assessmentCompletedLabel" runat="server"  SkinID="sknLabelFieldTextBold" Text="Assessment Completed"></asp:Label>
                                                    &nbsp;<asp:LinkButton ID="EnrollAssessor_assessmentCompletedCountLinkButton" runat="server"
                                                        SkinID="sknAssessorActionLinkButton" CommandName="Select" OnCommand="EnrollAssessor_assessmentLinkButton_Command">0</asp:LinkButton>
                                                </td> 
                                                <td width="24%">
                                                    <asp:Label ID="EnrollAssessor_assessmentInProgressLabel" runat="server" Text="Assessment Inprogress" SkinID="sknLabelFieldTextBold" ></asp:Label>
                                                    &nbsp;<asp:LinkButton ID="EnrollAssessor_assessmentInprogressCountLinkButton" 
                                                        runat="server" SkinID="sknAssessorActionLinkButton" CommandName="Select" OnCommand="EnrollAssessor_assessmentLinkButton_Command">0</asp:LinkButton>
                                                </td> 
                                                <td width="24%">
                                                    <asp:Label ID="EnrollAssessor_assessmentInitiatedLabel" runat="server" Text="Assessment Initiated" SkinID="sknLabelFieldTextBold" ></asp:Label>
                                                    &nbsp;<asp:LinkButton ID="EnrollAssessor_assessmentInitiatedCountLinkButton" runat="server" SkinID="sknAssessorActionLinkButton" CommandName="Select"
                                                        OnCommand="EnrollAssessor_assessmentLinkButton_Command">0</asp:LinkButton>
                                                </td> 
                                      
                                                <td width="27%">
                                                    <asp:Label ID="EnrollAssessor_assessmentSchduledLabel" runat="server" Text="Candidate Scheduled" SkinID="sknLabelFieldTextBold" ></asp:Label>
                                                    &nbsp;<asp:LinkButton ID="EnrollAssessor_assessmentSchduledCountLinkButton" runat="server" SkinID="sknAssessorActionLinkButton" CommandName="Select"
                                                        OnCommand="EnrollAssessor_assessmentLinkButton_Command">0</asp:LinkButton>
                                                </td> 
                                         </tr> 
                                    </table> 
                                 </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                 </asp:UpdatePanel>   
              </td>
         </tr>

        <tr>
            <td align="center" class="grid_body_bg">
          <asp:UpdatePanel ID="EnrollAssessor_assessmentDetailsGridViewUpdatePanel" runat="server">
                 <ContentTemplate>  
                     <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                         <tr>
                             <td align="left">
                                 <div ID="EnrollAssessor_assessmentDetailsGridViewDiv" runat="server" 
                                     style="height: 200px; width: 100%; overflow: auto">
                                     <asp:GridView ID="EnrollAssessor_assessmentDetailsGridView" runat="server" 
                                         AllowSorting="True" AutoGenerateColumns="False" 
                                         OnRowCommand="EnrollAssessor_assessmentDetailsGridView_RowCommand" 
                                         OnRowCreated="EnrollAssessor_assessmentDetailsGridView_RowCreated" 
                                         OnRowDataBound="EnrollAssessor_assessmentDetailsGridView_RowDataBound" 
                                         OnSorting="EnrollAssessor_assessmentDetailsGridView_Sorting">
                                         <Columns>
                                             <asp:TemplateField HeaderText="">
                                                 <ItemTemplate>
                                                     <asp:HyperLink ID="EnrollAssessor_assessmentDetailsEmailButton" runat="server" 
                                                         ImageUrl="~/App_Themes/DefaultTheme/Images/mail_icon_to.png" 
                                                         NavigateUrl='<%# Bind("CandidateEamil", "mailto:{0}") %>' ToolTip="Send Mail"> 
                                                        </asp:HyperLink>
                                                     <asp:HyperLink ID="EnrollAssessor_assessmentDetailsSelectHyperLink" 
                                                         runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif" 
                                                         Target="_blank" ToolTip="View Interview Summary"> 
                                                        </asp:HyperLink>
                                                     <asp:HiddenField ID="EnrollAssessor_assessmentDetailsTestStatusHiddenField" 
                                                         runat="server" Value='<%# Eval("TestStatus") %>' />
                                                     <asp:HyperLink ID="EnrollAssessor_assessmentDetailsCandidateRatingSummaryHyperLink" 
                                                         runat="server" 
                                                         ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif" 
                                                         Target="_blank" ToolTip="Candidate Rating Summary"> 
                                                        </asp:HyperLink>
                                                     <asp:HyperLink ID="EnrollAssessor_assessmentDetailsViewTrackingDetailsHyperLink" 
                                                         runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/tracking_icon.gif" 
                                                         Target="_blank" ToolTip="Offline Interview Tracking Details"> 
                                                        </asp:HyperLink>
                                                 </ItemTemplate>
                                                 <ItemStyle Width="10%" />
                                             </asp:TemplateField>
                                             <asp:BoundField DataField="CandidateName" HeaderText="Candidate Name" 
                                                 SortExpression="CANDIDATE_NAME" />
                                             <asp:TemplateField HeaderText="Completed Date" SortExpression="COMPLETED_DATE">
                                                 <ItemTemplate>
                                                     <asp:Label ID="EnrollAssessor_assessmentDetailsCompletedDateLabel" 
                                                         runat="server" 
                                                         Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CompletedDate"))) %>'></asp:Label>
                                                 </ItemTemplate>
                                             </asp:TemplateField>
                                             <asp:BoundField DataField="InterviewName" HeaderText="Interview Name" 
                                                 SortExpression="INTERVIEW_NAME" />
                                             <asp:BoundField DataField="PositionProfileName" 
                                                 HeaderText="Position Profile Name" SortExpression="POSITION_PROFILE_NUMBER" />
                                             <asp:BoundField DataField="AssessmentStatus" HeaderText="Assessment Status" 
                                                 SortExpression="ASSESSMENT_STATUS" />
                                             <asp:BoundField DataField="TestStatus" HeaderText="Interview Status" 
                                                 SortExpression="TEST_STATUS" />
                                             <asp:TemplateField>
                                                 <ItemTemplate>
                                                     <asp:HiddenField ID="EnrollAssessor_assessmentDetailsGridView_CandidateInterviewSessionKey" 
                                                         runat="server" Value='<%# Eval("CandidateInterviewSessionKey") %>' />
                                                     <asp:HiddenField ID="EnrollAssessor_assessmentDetailsGridView_AssessorId" 
                                                         runat="server" Value='<%# Eval("AssessorId") %>' />
                                                     <asp:HiddenField ID="EnrollAssessor_assessmentDetailsGridView_InterviewSessionKey" 
                                                         runat="server" Value='<%# Eval("InterviewSessionKey") %>' />
                                                     <asp:HiddenField ID="EnrollAssessor_assessmentDetailsGridView_attemptIDHiddenField" 
                                                         runat="server" Value='<%# Eval("AttemptID") %>' />
                                                     <asp:HiddenField ID="EnrollAssessor_assessmentDetailsGridView_testKeyHiddenField" 
                                                         runat="server" Value='<%# Eval("InterviewTestKey") %>' />
                                                 </ItemTemplate>
                                             </asp:TemplateField>
                                         </Columns>
                                         <EmptyDataTemplate>
                                             <table style="width: 100%; height: 100%">
                                                 <tr>
                                                     <td style="height: 50px">
                                                     </td>
                                                 </tr>
                                                 <tr>
                                                     <td align="center" class="error_message_text_normal" style="height: 100%" 
                                                         valign="middle">
                                                         No assessment details available
                                                     </td>
                                                 </tr>
                                             </table>
                                         </EmptyDataTemplate>
                                     </asp:GridView>
                                 </div>
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <uc1:PageNavigator ID="EnrollAssessor_pagingNavigator" runat="server" />
                             </td>
                         </tr>
                     </table>
                     </ContentTemplate>
                 </asp:UpdatePanel> 
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
            <asp:UpdatePanel ID="EnrollAssessor_bottomMessageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>  
                        <asp:Label ID="EnrollAssessor_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="EnrollAssessor_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                   </ContentTemplate> 
                 <Triggers>
                        <asp:AsyncPostBackTrigger ControlID ="EnrollAssessor_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID ="EnrollAssessor_bottomSaveButton" /> 
                        <asp:AsyncPostBackTrigger ControlID="EnrollAssessor_newAssessorSearchImageButton" />
                    </Triggers> 
                </asp:UpdatePanel>  
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="EnrollAssessor_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                            OnClick="EnrollAssessor_topSaveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollAssessor_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="EnrollAssessor_topResetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollAssessor_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
