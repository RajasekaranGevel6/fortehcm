﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/CustomerAdminMaster.Master"
    CodeBehind="EnrollCustomer.aspx.cs" Inherits="Forte.HCM.UI.Admin.EnrollCustomer" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<asp:Content ID="EnrollCustomer_content" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <script type="text/javascript">
        function ToggleInternalOrSubscriptionUser() {
            var internalRB = document.getElementById('<%=EnrollCustomer_internalRadioButton.ClientID %>');
            var subscriptionRB = document.getElementById('<%=EnrollCustomer_subscriptionRadioButton.ClientID %>');
            var internalDIV = document.getElementById('<%=EnrollCustomer_internalDiv.ClientID %>');
            var subscriptionDIV = document.getElementById('<%=EnrollCustomer_subscriptionDiv.ClientID %>');
            if (internalRB.checked) {
                internalDIV.style["display"] = "block";
                subscriptionDIV.style["display"] = "none";
            }
            else {
                internalDIV.style["display"] = "none";
                subscriptionDIV.style["display"] = "block";
            }

            //Diable and check active check box
            DisableActiveCheckBox();
        }

        var Text = '<%= EnrollCustomer_subscriptionUserOtherBussinessTextBox.ClientID %>';
        var Div = '<%= EnrollCustomer_subscriptionUserTypeOfBussinessOtherBussinessDiv.ClientID %>';

        function ShowValue(CheckBoxch) {
            if (CheckBoxch) {
                document.getElementById(Text).value = "";
                document.getElementById(Div).style.display = "block";
            }
            else {
                document.getElementById(Text).value = "";
                document.getElementById(Div).style.display = "none";
            }
        }

        function DisableActiveCheckBox() {
            var subscriptionRB = document.getElementById('<%=EnrollCustomer_subscriptionRadioButton.ClientID %>');
            if (subscriptionRB.checked) {
                var activateImmediate = document.getElementById('<%=EnrollCustomer_subscriptionUserActivateImmediatelyRadioButton.ClientID %>');
                var activateLater = document.getElementById('<%=EnrollCustomer_subscriptionUserActivateLaterRadioButton.ClientID %>');
                var activeCheckBox = document.getElementById('<%=EnrollCustomer_activeCheckbox.ClientID %>');

                activeCheckBox.disabled = true;
                if (activateImmediate.checked) {
                    activeCheckBox.checked = true;
                }

                if (activateLater.checked) {
                    activeCheckBox.checked = false;
                }
            }
        }
        function dateSelectionChanged(sender, args) {
          
           
            let visibleDate = sender._visibleDate.getDateOnly().format("dd-MM-yyyy");
              
                // set the date back to the current date
            sender._textbox.set_Value(visibleDate);
            
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="EnrollCustomer_headerLiteral" runat="server" Text="Enroll New Tenant"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="EnrollCustomer_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="EnrollCustomer_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollCustomer_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="EnrollCustomer_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollCustomer_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EnrollCustomer_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EnrollCustomer_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="EnrollCustomer_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <asp:UpdatePanel ID="EnrollCustomer_bodyUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                           <%-- <tr runat="server" id="EnrollCustomer_topHeaderTR">
                                <td class="header_bg">
                                    <asp:Literal ID="EnrollCustomer_topHeaderLiteral" runat="server" Text="Select User Type"></asp:Literal>
                                </td>
                            </tr>--%>
                            <tr runat="server" id="EnrollCustomer_topHeaderBGTR">
                                <td class="header_bg">
                                    <table width="25%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="EnrollCustomer_internalRadioButton" runat="server" GroupName="11"
                                                    Checked="true" />
                                                <asp:Label ID="EnrollCustomer_internalLabel" runat="server" Text="Internal" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="EnrollCustomer_subscriptionRadioButton" runat="server" GroupName="11" />
                                                <asp:Label ID="EnrollCustomer_subscriptionLabel" runat="server" Text="Subscription"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="EnrollCustomer_internalDiv" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="header_bg">
                                                    <asp:Literal ID="EnrollCustomer_internalUserHeaderLiteral" runat="server" Text="Internal User Details"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <table width="100%" cellpadding="1" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_internalUserNameHeaderLabel" runat="server" Text="Email ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25px;" align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                <asp:UpdatePanel ID="EnrollCustomer_internalUserUpdatePanel" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="EnrollCustomer_internalUserUserNameTextBox" runat="server" MaxLength="100"
                                                                                            Width="200px" AutoCompleteType="None"></asp:TextBox>
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:LinkButton ID="EnrollCustomer_internalUserCheckUserEmailIdAvailableButton" CssClass="link_button_hcm"
                                                                                            runat="server" Text="Check" OnClick="EnrollCustomer_internalUserCheckUserEmailIdAvailableButton_Click"></asp:LinkButton>
                                                                                        <div id="EnrollCustomer_internalUserUserEmailAvailableStatusDiv" runat="server" style="display: none;">
                                                                                            <asp:Label ID="EnrollCustomer_internalUserInValidEmailAvailableStatus" runat="server"
                                                                                                Width="100%" EnableViewState="false" ForeColor="Red"></asp:Label>
                                                                                            <asp:Label ID="EnrollCustomer_internalUserValidEmailAvailableStatus" runat="server"
                                                                                                Width="100%" EnableViewState="false" ForeColor="Green"></asp:Label>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_internalUserFirstNameHeaderLabel" runat="server" Text="First Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_internalUserFirstNameTextBox" runat="server" MaxLength="100"
                                                                                Width="200px" onkeydown = "return (!(event.keyCode>=96) && event.keyCode!=32)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_internalUserLastNameHeaderLabel" runat="server" Text="Last Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_internalUserLastNameTextBox" runat="server" MaxLength="100"
                                                                                Width="200px" onkeydown = "return (!(event.keyCode>=96) && event.keyCode!=32)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_internalUserPhoneHeaderLabel" runat="server" Text="Phone Number"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_internalUserPhoneTextBox" runat="server" MaxLength="15"
                                                                                Width="200px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_internalUserExpiryDateLabel" runat="server" Text="Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <span class="mandatory">&nbsp;&nbsp;&nbsp; *</span>
                                                                        </td>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 165px">
                                                                                <tr>
                                                                                    <td style="width: 37%">
                                                                                        <asp:TextBox ID="EnrollCustomer_internalexpiryDateTextBox" runat="server" MaxLength="10"
                                                                                            AutoCompleteType="None" Text=""></asp:TextBox>
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <asp:ImageButton ID="EnrollCustomer_internalImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:MaskedEditExtender ID="EnrollCustomer_internalExpiredDateMaskedEditExtender"
                                                                                runat="server" TargetControlID="EnrollCustomer_internalexpiryDateTextBox" Mask="99/99/9999"
                                                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="EnrollCustomer_internalExpiredDateMaskedEditValidator"
                                                                                runat="server" ControlExtender="EnrollCustomer_internalExpiredDateMaskedEditExtender"
                                                                                ControlToValidate="EnrollCustomer_internalexpiryDateTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="EnrollCustomer_internalExpiredDateCalendarExtender" OnClientDateSelectionChanged="dateSelectionChanged"
                                                                                runat="server" TargetControlID="EnrollCustomer_internalexpiryDateTextBox" CssClass="MyCalendar"
                                                                                Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="EnrollCustomer_internalImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="EnrollCustomer_siteAdministratorLabel" runat="server" Text="Site Admin"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:CheckBox ID="EnrollCustomer_siteAdminCheckBox" runat="server" />
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="EnrollCustomer_siteAdminImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Selecting this check box will assign the user as a site administrator" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="EnrollCustomer_rolesTR" runat="server">
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_rolesLabel" runat="server" Text="Roles" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_rolesTextBox" ReadOnly="true" runat="server" Width="90%"
                                                                                Font-Size="X-Small"></asp:TextBox>
                                                                            <asp:Panel ID="EnrollCustomer_rolesPanel" runat="server" CssClass="popupControl_role">
                                                                                <asp:CheckBoxList ID="EnrollCustomer_rolesCheckBoxList" RepeatColumns="1" runat="server"
                                                                                    CellPadding="0" AutoPostBack="true" DataValueField="RoleID" DataTextField="RoleName"
                                                                                    OnSelectedIndexChanged="EnrollCustomer_rolesCheckBoxList_SelectedIndexChanged">
                                                                                </asp:CheckBoxList>
                                                                            </asp:Panel>
                                                                            <ajaxToolKit:PopupControlExtender ID="EnrollCustomer_rolesPopupControlExtender" runat="server"
                                                                                TargetControlID="EnrollCustomer_rolesTextBox" PopupControlID="EnrollCustomer_rolesPanel"
                                                                                OffsetX="2" OffsetY="2" Position="Bottom">
                                                                            </ajaxToolKit:PopupControlExtender>
                                                                            <asp:HiddenField ID="EnrollCustomer_rolesHiddenField" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="EnrollCustomer_activationOptionTR" runat="server">
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_activationOptionLabel" runat="server" Text="Activation Option"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div class="alignTop">
                                                                                <asp:RadioButton ID="EnrollCustomer_internalUserActivateImmediatelyRadioButton" runat="server"
                                                                                    GroupName="2" Checked="true" />
                                                                                <asp:Label ID="EnrollCustomer_internalUserActivateImmediatelyLabel" runat="server"
                                                                                    Text="Activate Immediately"></asp:Label>
                                                                            </div>
                                                                            <div class="alignBottom">
                                                                                <asp:RadioButton ID="EnrollCustomer_internalUserActivateAfterRadioButton" runat="server"
                                                                                    GroupName="2" />
                                                                                <asp:Label ID="EnrollCustomer_internalUserActivateAfterLabel" runat="server" Text="Activate after user confirms through activation email"></asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="EnrollCustomer_subscriptionDiv" runat="server" style="display: block">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="header_bg">
                                                    <asp:Literal ID="EnrollCustomer_subscriptionHeaderLiteral" runat="server" Text="Subscription User Details"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <table width="100%" cellpadding="1" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserUserIDabel" runat="server" Text="Email ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25px;" align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                <asp:UpdatePanel ID="EnrollCustomer_subscriptionUserUdatePanel1" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="EnrollCustomer_subscriptionUserUserIDTextBox" runat="server" MaxLength="100"
                                                                                            Width="200px" AutoCompleteType="None"></asp:TextBox>
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:LinkButton ID="EnrollCustomer_subscriptionUserUserCheckLinkButton" CssClass="link_button_hcm"
                                                                                            runat="server" Text="Check" OnClick="EnrollCustomer_subscriptionUserUserCheckLinkButton_Click"></asp:LinkButton>
                                                                                        <div id="EnrollCustomer_subscriptionUserUserEmailAvailableStatusDiv" runat="server"
                                                                                            style="display: none;">
                                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserInvalidEmailLabel" runat="server" Width="100%"
                                                                                                EnableViewState="false" ForeColor="Red"></asp:Label>
                                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserValidLabel" runat="server" Width="100%"
                                                                                                EnableViewState="false" ForeColor="Green"></asp:Label>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserFirstNameLabel" runat="server" Text="First Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_subscriptionUserFirstNameTextBox" runat="server"
                                                                                MaxLength="100" Width="200px" onkeydown = "return (!(event.keyCode>=96) && event.keyCode!=32)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserLastNameLabel" runat="server" Text="Last Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_subscriptionUserLastNameTextBox" runat="server" MaxLength="100"
                                                                                Width="200px" onkeydown = "return (!(event.keyCode>=96) && event.keyCode!=32)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserPhoneLabel" runat="server" Text="Phone Number"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_subscriptionUserPhoneTextBox" runat="server" MaxLength="15"
                                                                                Width="200px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserCompanyNameHeaderLabel" runat="server"
                                                                                Text="Company Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_subscriptionUserCompanyNameTextBox" runat="server"
                                                                                MaxLength="100" Width="200px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserTitleHeaderLable" runat="server" Text="Title"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EnrollCustomer_subscriptionUserTitleTextBox" runat="server" MaxLength="50"
                                                                                Width="200px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_expirydateLabel" runat="server" Text="Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <span class="mandatory">&nbsp;&nbsp;&nbsp; *</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="EnrollCustomer_subscriptionExpiryDateUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 165px">
                                                                                        <tr>
                                                                                            <td style="width: 37%">
                                                                                                <asp:TextBox ID="EnrollCustomer_expirydateTextbox" runat="server" MaxLength="10"
                                                                                                    AutoCompleteType="None" Text=""></asp:TextBox>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <asp:ImageButton ID="EnrollCustomer_expirydateImageButton" SkinID="sknCalendarImageButton"
                                                                                                    runat="server" ImageAlign="Middle" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="EnrollCustomer_expirydateMaskedEditExtender"
                                                                                        runat="server" TargetControlID="EnrollCustomer_expirydateTextbox" Mask="99/99/9999"
                                                                                        MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                        MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="EnrollCustomer_MaskedEditValidator" runat="server"
                                                                                        ControlExtender="EnrollCustomer_expirydateMaskedEditExtender" ControlToValidate="EnrollCustomer_expirydateTextbox"
                                                                                        EmptyValueMessage="Date is required" InvalidValueMessage="From date is invalid"
                                                                                        Display="None" TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                        ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="EnrollCustomer_expirydateCustomCalendarExtender" OnClientDateSelectionChanged="dateSelectionChanged"
                                                                                        runat="server" TargetControlID="EnrollCustomer_expirydateTextbox" CssClass="MyCalendar"
                                                                                        Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="EnrollCustomer_expirydateImageButton" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table width="100%" cellpadding="1" cellspacing="2">
                                                                    <tr>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserSubscriptionTypeLabel" runat="server"
                                                                                Text="Subscription Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%" align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 80%">
                                                                            <asp:UpdatePanel ID="EnrollCustomer_subscriptionUserSubscriptionTypeUpdatePanel"
                                                                                runat="server">
                                                                                <ContentTemplate>
                                                                                    <div style="float: left; padding-right: 5px; width: 91%">
                                                                                        <asp:DropDownList ID="EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList"
                                                                                            runat="server" Width="100%" OnSelectedIndexChanged="EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList_SelectedIndexChanged"
                                                                                            AutoPostBack="True">
                                                                                            <asp:ListItem Text="Free Subscription" Value="1"> </asp:ListItem>
                                                                                            <asp:ListItem Text="Standard Subscription" Value="2"></asp:ListItem>
                                                                                            <asp:ListItem Selected="True" Text="Corporate Subscription" Value="3"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="EnrollCustomer_subscriptionUserSubscriptionTypeLable" runat="server"
                                                                                            Visible="false" SkinID="sknLabelFieldText"></asp:Label>
                                                                                    </div>
                                                                                    <div style="float: left; width: 5%">
                                                                                        <asp:ImageButton ID="EnrollCustomer_subscriptionUserSubscriptionTypeHelpImageButton" OnClientClick="javascript:return false;"
                                                                                            SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" ToolTip="Please select a subscription type" />
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="EnrollCustomer_numberOfUsersTr" runat="server" visible="false">
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_numberOfUsersHeaderLabel" runat="server" Text="Number of Users"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 35%;">
                                                                                        <asp:TextBox ID="EnrollCustomer_numberOfUsersTextBox" runat="server" MaxLength="2"></asp:TextBox>
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <asp:ImageButton ID="EnrollCustomer_numberOfUsersHelpImageButton" runat="server"
                                                                                            SkinID="sknHelpImageButton" OnClientClick="javascript:return false;"/>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <input type="hidden" runat="server" id="EnrollCustomer_MaximumCorporateUsersHidden"
                                                                                value="0" />
                                                                            <input type="hidden" runat="server" id="EnrollCustomer_subscriptionIdHidden" value="0" />
                                                                            <input type="hidden" runat="server" id="EnrollCustomer_isTrialHidden" value="0" />
                                                                            <input type="hidden" runat="server" id="EnrollCustomer_trialDaysHidden" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUsersubscriptionBusinessTypeLabel" runat="server"
                                                                                Text="Type Of Business" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%" align="right">
                                                                            <span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 80%">
                                                                            <table width="100%" cellpadding="1" cellspacing="2">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div>
                                                                                            <asp:CheckBoxList ID="EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList"
                                                                                                runat="server" RepeatDirection="Horizontal" RepeatColumns="4" TextAlign="Right"
                                                                                                CellSpacing="5">
                                                                                            </asp:CheckBoxList>
                                                                                            &nbsp;
                                                                                            <asp:CheckBox ID="EnrollCustomer_subscriptionUserTypeOfBussinessOtherTypeOfBussinessCheckBox"
                                                                                                runat="server" Text="Other" />
                                                                                            &nbsp; &nbsp;
                                                                                            <div id="EnrollCustomer_subscriptionUserTypeOfBussinessOtherBussinessDiv" runat="server"
                                                                                                style="display: none;" class="alignBottom">
                                                                                                <asp:TextBox ID="EnrollCustomer_subscriptionUserOtherBussinessTextBox" runat="server"
                                                                                                    MaxLength="30"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_subscriptionUserActivationOptionLabel" runat="server"
                                                                                Text="Activation Option" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td id="td_activationtype" runat="server">
                                                                            <div class="alignTop">
                                                                                <asp:RadioButton ID="EnrollCustomer_subscriptionUserActivateImmediatelyRadioButton"
                                                                                    runat="server" GroupName="1" Checked="true" />
                                                                                <asp:Label ID="EnrollCustomer_subscriptionUserActivateImmediatelyLabel" runat="server"
                                                                                    Text="Activate Immediately"></asp:Label>
                                                                            </div>
                                                                            <div class="alignBottom">
                                                                                <asp:RadioButton ID="EnrollCustomer_subscriptionUserActivateLaterRadioButton" runat="server"
                                                                                    GroupName="1" />
                                                                                <asp:Label ID="EnrollCustomer_subscriptionUserActivateLaterLabel" runat="server"
                                                                                    Text="Activate after user confirms through activation email"></asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EnrollCustomer_activeLabel" runat="server" Text="Active" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        <asp:HiddenField ID="EnrollCustomer_activeHiddenField" runat="server" />
                                                                            <asp:CheckBox ID="EnrollCustomer_activeCheckbox" runat="server"/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EnrollCustomer_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EnrollCustomer_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="EnrollCustomer_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="EnrollCustomer_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="EnrollCustomer_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollCustomer_bottomResetLinkButton" runat="server" Text="Reset"
                                            OnClick="EnrollCustomer_resetLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="EnrollCustomer_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
