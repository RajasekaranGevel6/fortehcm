﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAccount.aspx.cs" MasterPageFile="~/MasterPages/CustomerAdminMaster.Master"
    Inherits="Forte.HCM.UI.Admin.ViewAccount" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<asp:Content ID="ViewAccount_bodyContent" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <asp:UpdatePanel ID="Forte_ViewAccount_UpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td>
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="header_bg" align="right">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="50%" class="header_text_bold">
                                                <asp:Literal ID="Forte_ViewAccount_subsciptionPricingSetUpHeaderLiteral" runat="server"
                                                    Text="View Account"></asp:Literal>
                                            </td>
                                            <td width="50%" align="right">
                                                <asp:UpdatePanel ID="Forte_ViewAccount_topButtonsUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="4">
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="Forte_ViewAccount_topEditAccountButton" runat="server" Text="Edit Account"
                                                                        SkinID="sknButtonId" OnClick="ViewAccount_EditAccountButtonClick" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="Forte_ViewAccount_topUpgradeAccountButton" runat="server" Text="Upgrade Account"
                                                                        SkinID="sknButtonId" OnClick="ViewAccount_UpgradeButtonClick" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="Forte_ViewAccount_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                        Text="Reset" OnClick="ViewAccount_ResetButtonClick" />
                                                                </td>
                                                                <td width="4%" align="center" class="link_button">
                                                                    |
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="Forte_ViewAccount_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                        Text="Cancel" OnClick="ParentPageRedirect" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_ViewAccount_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                            EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_ViewAccount_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                            EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Literal ID="Forte_ViewAccount_accountDetailsHeaderLiteral" runat="server" Text="Account Details"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_subscriptionTypeHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Subscription Type"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20px;">
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_subscriptionTypeLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_lastLoginHeaderLabel" runat="server" Text="Last Login"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_lastLoginLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_statusHeadLabel" runat="server" Text="Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_statusLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_subscribedOnHeadLabel" runat="server" Text="Subscribed On"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_subscribedOnLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_activatedOnHeadLabel" runat="server" Text="Activated On"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_activatedOnLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_noOfUsersHeadLabel" runat="server" Text="No Of Users"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_noOfUsersLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_titleHeadLabel" runat="server" Text="Title" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_titleLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_companyHeadLable" runat="server" Text="Company"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_companyLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_phoneHeadLabel" runat="server" Text="Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_phoneLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_firstNameHeadLabel" runat="server" Text="First Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_firstNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_lastNameHeadLabel" runat="server" Text="Last Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_lastNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_isTrialHeadLabel" runat="server" Text="Trial" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Forte_ViewAccount_isTrialLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg" align="center">
                                                <asp:UpdatePanel ID="CreateTestSession_maxMinButtonUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                    <asp:Literal ID="Forte_CorporateUserManangement_usersResultsLiteral" runat="server"
                                                                        Text="Feature & Pricing"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <div id="Forte_ViewAccount_featuresAndPricingDiv" style="overflow: auto; height: 200px;">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="Forte_ViewAccount_featureAndPricingGridView" runat="server" AutoGenerateColumns="false"
                                                                            SkinID="sknSubsciptionAccountsGridView" Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Name">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_ViewAccount_featureNameLabel" runat="server" Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Pricing">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Forte_ViewAccount_firstNameLabel" runat="server" Text='<%# Eval("FeatureValue") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="Forte_ViewAccount_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                            EnableViewState="false"></asp:Label>
                        <asp:Label ID="Forte_ViewAccount_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                            EnableViewState="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" class="header_text_bold">
                                </td>
                                <td width="50%" align="right">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="4">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="Forte_ViewAccount_bottomEditAccountButton" runat="server" Text="Edit Account"
                                                            SkinID="sknButtonId" OnClick="ViewAccount_EditAccountButtonClick" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="Forte_ViewAccount_bottomUpgradeAccountButton" runat="server" Text="Upgrade Account"
                                                            SkinID="sknButtonId" OnClick="ViewAccount_UpgradeButtonClick" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="Forte_ViewAccount_bottomResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                            Text="Reset" OnClick="ViewAccount_ResetButtonClick" />
                                                    </td>
                                                    <td width="4%" align="center" class="link_button">
                                                        |
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="Forte_ViewAccount_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
