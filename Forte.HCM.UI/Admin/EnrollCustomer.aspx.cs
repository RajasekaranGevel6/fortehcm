﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EnrollCustomer.aspx.cs
// File that represents the enrollCutomer class that defines the user 
// interface layout and functionalities for the customer enrollment page. This 
// page helps in managing the customer enrollment page. 
//
#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.Common;
using System.Configuration;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Resources;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using System.Threading;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    public partial class EnrollCustomer : PageBase
    {
        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page caption in the page
                Master.SetPageCaption("Enroll New Customer");

                Page.SetFocus(EnrollCustomer_topSaveButton.ClientID);

                if (EnrollCustomer_subscriptionRadioButton.Checked)
                {
                    EnrollCustomer_subscriptionDiv.Style["display"] = "block";
                    EnrollCustomer_internalDiv.Style["display"] = "none";
                }
                else
                {
                    EnrollCustomer_subscriptionDiv.Style["display"] = "none";
                    EnrollCustomer_internalDiv.Style["display"] = "block";
                }

                if (!IsPostBack)
                {
                    EnrollCustomer_expirydateTextbox.Text =
                         DateTime.Now.Date.AddDays(GetExpiryDate(1)).ToShortDateString();

                    EnrollCustomer_internalRadioButton.Attributes.Add("onclick",
                        "javascript:return ToggleInternalOrSubscriptionUser();");
                    EnrollCustomer_subscriptionRadioButton.Attributes.Add("onclick",
                        "javascript:return ToggleInternalOrSubscriptionUser();");

                    EnrollCustomer_subscriptionUserActivateImmediatelyRadioButton.Attributes.Add("onclick",
                        "javascript:return DisableActiveCheckBox();");

                    EnrollCustomer_subscriptionUserActivateLaterRadioButton.Attributes.Add("onclick",
                        "javascript:return DisableActiveCheckBox();");

                    int TotalRecords = 0;
                    EnrollCustomer_MaximumCorporateUsersHidden.Value = new UserRegistrationBLManager().GetMaximumCorporateUsers().ToString();
                    EnrollCustomer_numberOfUsersHelpImageButton.ToolTip = "Maximum number of users allowed is " +
                        EnrollCustomer_MaximumCorporateUsersHidden.Value;
                    EnrollCustomer_numberOfUsersTr.Visible = false;

                    EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.DataSource = new AdminBLManager().
                    GetBusinessTypes(string.Empty, "NAME", "A", 1, null, out TotalRecords);
                    EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.DataValueField = "BusinessTypeID";
                    EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.DataTextField = "BusinessTypeName";
                    EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.DataBind();
                    EnrollCustomer_subscriptionUserTypeOfBussinessOtherTypeOfBussinessCheckBox.Attributes.Add
                        ("onclick", "ShowValue(this.checked);");

                    //Assign the values for the roles check box list
                    EnrollCustomer_rolesCheckBoxList.DataSource = new AuthenticationBLManager().GetRoles();
                    EnrollCustomer_rolesCheckBoxList.DataBind();

                    if ((Request.QueryString["userID"] != null) && (Request.QueryString["usertype"] == null || Request.QueryString["usertype"] == ""))
                    {
                        //EnrollCustomer_topHeaderTR.Visible = false;
                        EnrollCustomer_topHeaderBGTR.Visible = false;
                        EnrollCustomer_rolesTR.Visible = false;
                        EnrollCustomer_activationOptionTR.Visible = false;
                        EnrollCustomer_internalUserCheckUserEmailIdAvailableButton.Visible = false;
                        EnrollCustomer_internalUserUserNameTextBox.Enabled = false;
                        EnrollCustomer_headerLiteral.Text = "Edit Internal User";
                        Master.SetPageCaption("Edit Internal User");
                        LoadInternalUserDetails(Request.QueryString["userID"]);
                    }
                    if (((Request.QueryString["userID"] != null) && (Request.QueryString["usertype"] != null)))
                    {
                       // EnrollCustomer_topHeaderTR.Visible = false;
                        EnrollCustomer_topHeaderBGTR.Visible = false;
                        EnrollCustomer_rolesTR.Visible = false;
                        EnrollCustomer_activationOptionTR.Visible = true;
                        EnrollCustomer_internalUserCheckUserEmailIdAvailableButton.Visible = false;
                        EnrollCustomer_internalUserUserNameTextBox.Enabled = false;
                        EnrollCustomer_headerLiteral.Text = "Edit " + Request.QueryString["usertype"] + " User";
                        Master.SetPageCaption("Edit " + Request.QueryString["usertype"] + " User");
                        if (Request.QueryString["usertype"] == "Free")
                            LoadFreeUserDetails(Request.QueryString["userID"]);
                        else if (Request.QueryString["usertype"] == "Standard")
                            LoadFreeUserDetails(Request.QueryString["userID"]);
                    }
                }
                EnrollCustomer_topErrorMessageLabel.Text = string.Empty;
                EnrollCustomer_bottomErrorMessageLabel.Text = string.Empty;
                EnrollCustomer_topSuccessMessageLabel.Text = string.Empty;
                EnrollCustomer_bottomSuccessMessageLabel.Text = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollCustomer_topErrorMessageLabel, exception.Message);
            }
        }

        private void LoadStandardUserDetails(string p)
        {
            throw new NotImplementedException();
        }

        private void LoadFreeUserDetails(string userID)
        {
            UserDetail userDetail = new CommonBLManager().GetSubscriptionUserDetail(int.Parse(userID));
            EnrollCustomer_subscriptionDiv.Style["display"] = "block";
            EnrollCustomer_internalDiv.Style["display"] = "none";
            EnrollCustomer_subscriptionUserUserIDTextBox.Enabled = false;
            EnrollCustomer_subscriptionUserSubscriptionTypeLable.Visible = true;
            EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList.Visible = false;
            EnrollCustomer_subscriptionUserActivationOptionLabel.Visible = false;
            EnrollCustomer_subscriptionUserSubscriptionTypeHelpImageButton.Visible = false;
            EnrollCustomer_subscriptionUserUserCheckLinkButton.Visible = false;

            EnrollCustomer_subscriptionUserUserIDTextBox.Text = userDetail.UserName;
            EnrollCustomer_subscriptionUserFirstNameTextBox.Text = userDetail.FirstName;
            EnrollCustomer_subscriptionUserLastNameTextBox.Text = userDetail.LastName;
            EnrollCustomer_subscriptionUserPhoneTextBox.Text = userDetail.FirstName;
            EnrollCustomer_subscriptionUserCompanyNameTextBox.Text = userDetail.Company;
            EnrollCustomer_subscriptionUserTitleTextBox.Text = userDetail.Title;

            EnrollCustomer_expirydateTextbox.Text = Convert.ToDateTime(userDetail.ExpiryDate).ToShortDateString();

            EnrollCustomer_subscriptionUserSubscriptionTypeLable.Text = userDetail.SubscriptionType;

            string[] businessType = userDetail.BussinessType.Split(',');
            for (int i = 0; i < businessType.Length; i++)
            {
                for (int j = 0; j < EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items.Count; j++)
                {
                    if (businessType[i] == EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items[j].Value)
                        EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items[j].Selected = true;
                    continue;
                }
            }
            
            td_activationtype.Visible = false;
            if (userDetail.IsActive == true)
                EnrollCustomer_activeHiddenField.Value = "Y";
            else
                EnrollCustomer_activeHiddenField.Value = "N";
            
            EnrollCustomer_activeCheckbox.Checked = userDetail.IsActive;
        }

        /// <summary>
        /// Handles the Click event of the EnrollCustomer_subscriptionUserUserCheckLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EnrollCustomer_subscriptionUserUserCheckLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim()))
                {
                    base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                        EnrollCustomer_topErrorMessageLabel, HCMResource.UserRegistraion_EmptyUserEmail);
                    return;
                }
                if (!IsValidEmailAddress(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim()))
                {
                    base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                        EnrollCustomer_topErrorMessageLabel, HCMResource.UserRegistration_InvalidEmailAddress);
                    return;
                }
                if (CheckForEmailAddressAvailability(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim()))
                    EnrollCustomer_subscriptionUserValidLabel.Text = HCMResource.UserRegistration_UserEmailAvailable;
                else
                    EnrollCustomer_subscriptionUserInvalidEmailLabel.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                EnrollCustomer_subscriptionUserUserEmailAvailableStatusDiv.Style.Add("display", "block");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                       EnrollCustomer_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the EnrollCustomer_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EnrollCustomer_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                      EnrollCustomer_topErrorMessageLabel, exception.Message);
            }

        }

        /// <summary>
        /// Handles the Click event of the EnrollCustomer_internalUserCheckUserEmailIdAvailableButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EnrollCustomer_internalUserCheckUserEmailIdAvailableButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(EnrollCustomer_internalUserUserNameTextBox.Text.Trim()))
                {
                    base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                        EnrollCustomer_topErrorMessageLabel, HCMResource.UserRegistraion_EmptyUserEmail);
                    return;
                }
                if (!IsValidEmailAddress(EnrollCustomer_internalUserUserNameTextBox.Text.Trim()))
                {
                    base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                        EnrollCustomer_topErrorMessageLabel, HCMResource.UserRegistration_InvalidEmailAddress);
                    return;
                }
                if (CheckForEmailAddressAvailability(EnrollCustomer_internalUserUserNameTextBox.Text.Trim()))
                    EnrollCustomer_internalUserValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailAvailable;
                else
                    EnrollCustomer_internalUserInValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                EnrollCustomer_internalUserUserEmailAvailableStatusDiv.Style.Add("display", "block");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                       EnrollCustomer_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the EnrollCustomer_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EnrollCustomer_saveButton_Click(object sender, EventArgs e)
        {
            if ((Request.QueryString["userID"] != null) && (Request.QueryString["usertype"] == null || Request.QueryString["usertype"] == ""))
            {
                UpdateInternalUser();

                EnrollCustomer_subscriptionDiv.Style["display"] = "none";
                EnrollCustomer_internalDiv.Style["display"] = "block";
                return;
            }
            else if (((Request.QueryString["userID"] != null) && (Request.QueryString["usertype"] != null)))
            {
                UpdateSubscriptionUsers();

                EnrollCustomer_subscriptionDiv.Style["display"] = "block";
                EnrollCustomer_internalDiv.Style["display"] = "none";
                return;
            }

            if (!IsValidData())
                return;

            IDbTransaction transaction = null;
            try
            {
                string confirmatioCode = string.Empty;
                string password = string.Empty;

                if (EnrollCustomer_subscriptionRadioButton.Checked)
                {
                    UserRegistrationInfo userRegistrationInfo = null;
                    RegistrationConfirmation registrationConfirmation = null;

                    userRegistrationInfo = new UserRegistrationInfo();
                    userRegistrationInfo.BussinessTypesIds = GetSelectedBussinessTypesFromList();
                    userRegistrationInfo.BussinessTypeOther = EnrollCustomer_subscriptionUserOtherBussinessTextBox.Text.Trim();
                    userRegistrationInfo.UserEmail = EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim();
                    password = RandomString(10);
                    userRegistrationInfo.Password = new EncryptAndDecrypt().EncryptString(password);
                    userRegistrationInfo.FirstName = EnrollCustomer_subscriptionUserFirstNameTextBox.Text.Trim();
                    userRegistrationInfo.LastName = EnrollCustomer_subscriptionUserLastNameTextBox.Text.Trim();
                    userRegistrationInfo.Phone = EnrollCustomer_subscriptionUserPhoneTextBox.Text.Trim();
                    userRegistrationInfo.Company = EnrollCustomer_subscriptionUserCompanyNameTextBox.Text.Trim();
                    userRegistrationInfo.Title = EnrollCustomer_subscriptionUserTitleTextBox.Text.Trim();
                    userRegistrationInfo.SubscriptionId = int.Parse(EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList.SelectedValue);
                    userRegistrationInfo.SubscriptionRole = Enum.GetName(typeof(SubscriptionRolesEnum), userRegistrationInfo.SubscriptionId);
                    if (Utility.IsNullOrEmpty(userRegistrationInfo.SubscriptionRole))
                        userRegistrationInfo.SubscriptionRole = "SR_OTHER";
                    userRegistrationInfo.IsTrial = 0;
                    if (EnrollCustomer_numberOfUsersTextBox.Text.Length != 0)
                        userRegistrationInfo.NumberOfUsers = Convert.ToInt16(EnrollCustomer_numberOfUsersTextBox.Text);
                    userRegistrationInfo.TrialDays = 0;
                    //if(EnrollCustomer_activeCheckbox.Checked==true)
                    //    userRegistrationInfo.IsActive=1;
                    //else
                    //     userRegistrationInfo.IsActive=0;                    
                    confirmatioCode = GetConfirmationCode();
                    registrationConfirmation = new RegistrationConfirmation();
                    registrationConfirmation.ConfirmationCode = confirmatioCode;
                    EnrollCustomer_MaskedEditValidator.Validate();
                    if (!EnrollCustomer_MaskedEditValidator.IsValid)
                    {
                        base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                           EnrollCustomer_topErrorMessageLabel, EnrollCustomer_MaskedEditValidator.ErrorMessage);
                        return;
                    }
                    if (Convert.ToDateTime(EnrollCustomer_expirydateTextbox.Text) < DateTime.Now.Date)
                    {
                        base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                         EnrollCustomer_topErrorMessageLabel, "Expiry date cannot be less than the current date");
                        return;
                    }
                    userRegistrationInfo.ExpiryDate = Convert.ToDateTime(EnrollCustomer_expirydateTextbox.Text);
                    
                    transaction = new TransactionManager().Transaction;
                    new UserRegistrationBLManager().InsertUser(null, userRegistrationInfo,
                        registrationConfirmation, transaction as DbTransaction);

                    //If the activate later radio button is checked activate later and send 
                    //mail
                    if (EnrollCustomer_subscriptionUserActivateLaterRadioButton.Checked)
                    {
                        transaction.Commit();

                        base.ShowMessage(EnrollCustomer_bottomSuccessMessageLabel,
                            EnrollCustomer_topSuccessMessageLabel, HCMResource.UserRegistration_UserInserted);

                        SendConfirmationCodeEmail(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim(),
                            password, confirmatioCode, EnrollCustomer_subscriptionUserFirstNameTextBox.Text.Trim(),
                            EnrollCustomer_subscriptionUserLastNameTextBox.Text.Trim());
                    }
                    else
                    {
                        //If the activate later radio button is not checked activate immediately and send
                        //mail
                        CheckAndActivateAccount(new EncryptAndDecrypt().EncryptString
                            (EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim()), confirmatioCode, transaction);

                        transaction.Commit();

                        base.ShowMessage(EnrollCustomer_bottomSuccessMessageLabel,
                           EnrollCustomer_topSuccessMessageLabel, HCMResource.UserRegistration_UserInserted);

                        SendActivatedUserEmail(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim(), password, confirmatioCode);
                    }

                    EnrollCustomer_subscriptionUserUserIDTextBox.Text = string.Empty;
                    EnrollCustomer_subscriptionUserFirstNameTextBox.Text = string.Empty;
                    EnrollCustomer_subscriptionUserLastNameTextBox.Text = string.Empty;
                    EnrollCustomer_subscriptionUserPhoneTextBox.Text = string.Empty;
                    EnrollCustomer_subscriptionUserCompanyNameTextBox.Text = string.Empty;
                    EnrollCustomer_subscriptionUserTitleTextBox.Text = string.Empty;
                    EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList.SelectedIndex = 0;
                    EnrollCustomer_subscriptionUserTypeOfBussinessOtherTypeOfBussinessCheckBox.Checked = false;
                    for (int ItemsCount = 0; ItemsCount < EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items.Count; ItemsCount++)
                        EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items[ItemsCount].Selected = false;
                }
                else
                {
                    UserRegistrationInfo userRegistrationInfo = null;
                    RegistrationConfirmation registrationConfirmation = null;

                    userRegistrationInfo = new UserRegistrationInfo();
                    userRegistrationInfo.UserEmail = EnrollCustomer_internalUserUserNameTextBox.Text.Trim();
                    password = RandomString(10);
                    userRegistrationInfo.Password = new EncryptAndDecrypt().EncryptString(password);
                    userRegistrationInfo.FirstName = EnrollCustomer_internalUserFirstNameTextBox.Text.Trim();
                    userRegistrationInfo.LastName = EnrollCustomer_internalUserLastNameTextBox.Text.Trim();
                    userRegistrationInfo.Phone = EnrollCustomer_internalUserPhoneTextBox.Text.Trim();
                    userRegistrationInfo.RoleIDs = EnrollCustomer_rolesHiddenField.Value;
                    userRegistrationInfo.IsSiteAdmin = EnrollCustomer_siteAdminCheckBox.Checked;
                    userRegistrationInfo.CreatedBy = base.userID.ToString();
                    registrationConfirmation = new RegistrationConfirmation();
                    confirmatioCode = GetConfirmationCode();
                    registrationConfirmation.ConfirmationCode = confirmatioCode;
                    transaction = new TransactionManager().Transaction;
                    int tenantUserID = int.Parse(ConfigurationManager.AppSettings["INTERNAL_USER_TENANT_ID"]);

                    EnrollCustomer_internalExpiredDateMaskedEditValidator.Validate();
                    if (!EnrollCustomer_internalExpiredDateMaskedEditValidator.IsValid)
                    {
                        base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                           EnrollCustomer_topErrorMessageLabel, EnrollCustomer_internalExpiredDateMaskedEditValidator.ErrorMessage);
                        return;
                    }
                    if (Convert.ToDateTime(EnrollCustomer_internalexpiryDateTextBox.Text) < DateTime.Now.Date)
                    {
                        base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                         EnrollCustomer_topErrorMessageLabel, "Expiry date cannot be less than the current date");
                        return;
                    }
                    userRegistrationInfo.ExpiryDate = Convert.ToDateTime(EnrollCustomer_internalexpiryDateTextBox.Text);
                    new AdminBLManager().InsertInternalUser(userRegistrationInfo, registrationConfirmation,
                        tenantUserID, transaction);

                    if (EnrollCustomer_internalUserActivateAfterRadioButton.Checked)
                    {
                        transaction.Commit();

                        base.ShowMessage(EnrollCustomer_bottomSuccessMessageLabel,
                            EnrollCustomer_topSuccessMessageLabel, HCMResource.UserRegistration_UserInserted);

                        SendConfirmationCodeEmail(EnrollCustomer_internalUserUserNameTextBox.Text.Trim(),
                            password, confirmatioCode, EnrollCustomer_internalUserFirstNameTextBox.Text.Trim(),
                            EnrollCustomer_internalUserLastNameTextBox.Text.Trim());
                    }
                    else
                    {
                        CheckAndActivateAccount(new EncryptAndDecrypt().EncryptString
                            (EnrollCustomer_internalUserUserNameTextBox.Text.Trim()), confirmatioCode, transaction);

                        transaction.Commit();

                        base.ShowMessage(EnrollCustomer_bottomSuccessMessageLabel,
                           EnrollCustomer_topSuccessMessageLabel, HCMResource.UserRegistration_UserInserted);

                        SendActivatedUserEmail(EnrollCustomer_internalUserUserNameTextBox.Text.Trim(), password, confirmatioCode);
                    }

                    EnrollCustomer_internalUserUserNameTextBox.Text = string.Empty;
                    EnrollCustomer_internalUserFirstNameTextBox.Text = string.Empty;
                    EnrollCustomer_internalUserLastNameTextBox.Text = string.Empty;
                    EnrollCustomer_internalUserPhoneTextBox.Text = string.Empty;
                    EnrollCustomer_siteAdminCheckBox.Checked = false;
                    //Assign the values for the roles check box list
                    EnrollCustomer_rolesCheckBoxList.DataSource = new AuthenticationBLManager().GetRoles();
                    EnrollCustomer_rolesCheckBoxList.DataBind();
                    EnrollCustomer_rolesHiddenField.Value = string.Empty;
                    EnrollCustomer_rolesTextBox.Text = string.Empty;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                       EnrollCustomer_topErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// method handles updatesubscription process.
        /// </summary>
        private void UpdateSubscriptionUsers()
        {
            UserDetail userDetail = new UserDetail();
            userDetail.UserID = Convert.ToInt32(Request.QueryString["userID"]);
            userDetail.UserName=EnrollCustomer_subscriptionUserUserIDTextBox.Text;
            userDetail.FirstName = EnrollCustomer_subscriptionUserFirstNameTextBox.Text;
            userDetail.LastName = EnrollCustomer_subscriptionUserLastNameTextBox.Text;
            userDetail.Phone = EnrollCustomer_subscriptionUserPhoneTextBox.Text;
            userDetail.Company = EnrollCustomer_subscriptionUserCompanyNameTextBox.Text;
            userDetail.Title = EnrollCustomer_subscriptionUserTitleTextBox.Text;
            userDetail.ExpiryDate = Convert.ToDateTime(EnrollCustomer_expirydateTextbox.Text);
            userDetail.SubscriptionType = EnrollCustomer_subscriptionUserSubscriptionTypeLable.Text;
            userDetail.BussinessType = GetSelectedBussinessTypesFromList();
            userDetail.IsActive = EnrollCustomer_activeCheckbox.Checked;
            new AuthenticationBLManager().UpdateSubscriptionUserDetails(userDetail, base.userID);

            if (!(EnrollCustomer_activeCheckbox.Checked == true && EnrollCustomer_activeHiddenField.Value == "Y")
            && !(EnrollCustomer_activeCheckbox.Checked == false && EnrollCustomer_activeHiddenField.Value == "N"))
                    SendActiveStatusChangedEmail(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim(),
                           EnrollCustomer_subscriptionUserFirstNameTextBox.Text.Trim(),
                           EnrollCustomer_subscriptionUserLastNameTextBox.Text.Trim(), 
                           EnrollCustomer_activeCheckbox.Checked);

            if (EnrollCustomer_activeCheckbox.Checked)
                EnrollCustomer_activeHiddenField.Value = "Y";
            else
                EnrollCustomer_activeHiddenField.Value = "N";

            ShowSuccessMessage("Subscription user updated successfully");
        }

      
        /// <summary>
        /// Handles the SelectedIndexChanged event of the EnrollCustomer_rolesCheckBoxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EnrollCustomer_rolesCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectRole();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EnrollCustomer_topErrorMessageLabel,
                    EnrollCustomer_bottomErrorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handler

        #region Private Methods

        /// <summary>
        /// Updates the internal user.
        /// </summary>
        private void UpdateInternalUser()
        {
            if (EnrollCustomer_internalUserFirstNameTextBox.Text.Trim().Length == 0 ||
                EnrollCustomer_internalUserLastNameTextBox.Text.Trim().Length == 0 ||
                 EnrollCustomer_internalUserPhoneTextBox.Text.Trim().Length == 0)
            {
                ShowErrorMessage(Resources.HCMResource.EnrollCustomer_PleaseEnterMandatoryInformation);
                return;
            }

            UserDetail userDetail = new UserDetail();

            userDetail.FirstName = EnrollCustomer_internalUserFirstNameTextBox.Text.Trim();
            userDetail.LastName = EnrollCustomer_internalUserLastNameTextBox.Text.Trim();
            userDetail.Phone = EnrollCustomer_internalUserPhoneTextBox.Text.Trim();
            userDetail.UserID = int.Parse(Request.QueryString["UserID"]);
            userDetail.UserType = EnrollCustomer_siteAdminCheckBox.Checked ? UserType.SiteAdmin : UserType.None;

            if (EnrollCustomer_internalUserActivateImmediatelyRadioButton.Checked)
                userDetail.IsActive = true;
            else
                userDetail.IsActive = false;

            if(EnrollCustomer_internalexpiryDateTextBox.Text!="")
                userDetail.ExpiryDate=Convert.ToDateTime(EnrollCustomer_internalexpiryDateTextBox.Text);

            new AuthenticationBLManager().UpdateInternalUserDetails(userDetail, base.userID);

            ShowSuccessMessage(Resources.HCMResource.EnrollCustomer_UpdatedSuccessfully);
        }

        /// <summary>
        /// Selects the role.
        /// </summary>
        private void SelectRole()
        {
            EnrollCustomer_rolesHiddenField.Value = string.Empty;

            string selectedRoleName = "";

            for (int i = 0; i < EnrollCustomer_rolesCheckBoxList.Items.Count; i++)
            {
                if (EnrollCustomer_rolesCheckBoxList.Items[i].Selected)
                {
                    EnrollCustomer_rolesHiddenField.Value += ", " + EnrollCustomer_rolesCheckBoxList.Items[i].Value;

                    selectedRoleName += ", " + EnrollCustomer_rolesCheckBoxList.Items[i].Text;
                }
            }

            if (selectedRoleName != null || selectedRoleName.Length != 0)
            {
                selectedRoleName = selectedRoleName.TrimStart(',', ' ');
                EnrollCustomer_rolesHiddenField.Value = EnrollCustomer_rolesHiddenField.Value.TrimStart(',', ' ');
            }

            EnrollCustomer_rolesTextBox.Text = selectedRoleName;
        }

        /// <summary>
        /// Gets the available subscriptions in the repository
        /// </summary>
        /// <returns>
        /// A List of subscriptions available in the repository
        /// </returns>
        private List<SubscriptionTypes> GetAvailableSubscriptionsList()
        {
            return new SubscriptionBLManager().GetAllSubscriptionTypes().ToList();
        }

        /// <summary>
        /// Checks for email address availability.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        private bool CheckForEmailAddressAvailability(string userName)
        {
            return new UserRegistrationBLManager().CheckUserEmailExists(userName, base.tenantID);
        }

        /// <summary>
        /// A Method that validates whether the user inputed username
        /// is valid email or not using regular expression
        /// </summary>
        /// <param name="strUserEmailId">
        /// A <see cref="System.String"/> that holds the email to validate
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the validity status
        /// </returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Shows the error message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ShowErrorMessage(string message)
        {
            base.ShowMessage(EnrollCustomer_bottomErrorMessageLabel,
                EnrollCustomer_topErrorMessageLabel, message);
        }

        /// <summary>
        /// Checks the and activate account.
        /// </summary>
        /// <param name="EncryptedUserName">Name of the encrypted user.</param>
        /// <param name="EncryptedConfirmationCode">The encrypted confirmation code.</param>
        private void CheckAndActivateAccount(string EncryptedUserName, string EncryptedConfirmationCode, IDbTransaction transaction)
        {
            try
            {
                CheckAndActivateUserAccount(DecryptString(ref EncryptedUserName), ref EncryptedConfirmationCode, transaction);
            }
            catch (FormatException formatExp)
            {
                Logger.ExceptionLog(formatExp);

                base.ShowMessage(EnrollCustomer_topErrorMessageLabel, EnrollCustomer_bottomErrorMessageLabel,
                    new Exception("In-valid user name").ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EnrollCustomer_topErrorMessageLabel, EnrollCustomer_bottomErrorMessageLabel,
                  exp.ToString());
            }
        }

        /// <summary>
        /// Checks the and activate user account.
        /// </summary>
        /// <param name="User_Email">The user_ email.</param>
        /// <param name="EncryptedConfirmationCode">The encrypted confirmation code.</param>
        /// <returns></returns>
        private string CheckAndActivateUserAccount(string User_Email, ref string EncryptedConfirmationCode, IDbTransaction transaction)
        {
            return new UserRegistrationBLManager().CheckForActivationCodeWithTransaction
                (User_Email, EncryptedConfirmationCode.Replace(' ', '+'), transaction);
        }

        /// <summary>
        /// Decrypts the string.
        /// </summary>
        /// <param name="EncryptedString">The encrypted string.</param>
        /// <returns></returns>
        private string DecryptString(ref string EncryptedString)
        {
            return new EncryptAndDecrypt().DecryptString(EncryptedString.Replace(' ', '+'));
        }

        /// <summary>
        /// A Method that shows the success message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// </param>
        private void ShowSuccessMessage(string Message)
        {
            base.ShowMessage(EnrollCustomer_topSuccessMessageLabel,
                EnrollCustomer_bottomSuccessMessageLabel, Message);
        }

        /// <summary>
        /// Method that sends the activation email to the created user.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="string"/> that holds the user email.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds the encrypted confirmation code 
        /// to activate his/her account.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        private void SendConfirmationCodeEmail(string userEmail, string password,
            string confirmationCode, string firstName, string lastName)
        {
            new EmailHandler().SendMail(userEmail,
                ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                HCMResource.UserRegistration_MailActivationCodeSubject,
                GetMailBody(new EncryptAndDecrypt().EncryptString(userEmail), confirmationCode, password, firstName, lastName));
        }

        /// <summary>
        /// A Method that sends mail when active status is changed
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="activeStatus">
        /// A <see cref="string"/> that holds the current active status.
        /// </param>
        private void SendActiveStatusChangedEmail(string userEmail, string firstName, 
            string lastName,bool activeStatus)
        {
            //ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
            new EmailHandler().SendMail(userEmail,
                ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                "ForteHCM - Account Status",
                GetActiveStatusChangedMailBody(new EncryptAndDecrypt().EncryptString(userEmail), 
                firstName, lastName, activeStatus));
        }

        /// <summary>
        /// Method that compose the body content of the email.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="string"/> that holds the user email.
        /// </param>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="activeStatus">
        /// A <see cref="string"/> that holds the current active status.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains the activation mail body.
        /// </returns>
        private string GetActiveStatusChangedMailBody(string userEmail, string firstName, 
            string lastName, bool activeStatus)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                string statusMsg = string.Empty;
                if (activeStatus == true)
                    statusMsg = "This is to inform you that your account has been activated.";
                else
                    statusMsg = "This is to inform you that your account has been deactivated.";

                sbBody.Append("Dear ");
                sbBody.Append(firstName);
                sbBody.Append(" ");
                sbBody.Append(lastName);
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(statusMsg);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// A Method that sends the activation code as email
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="System.String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        private void SendActivatedUserEmail(string userEmail, string password, string confirmationCode)
        {
            MailDetail mailDetails = new MailDetail();
            EmailHandler Email = new EmailHandler();
            Email.SendMail(userEmail, HCMResource.EnrollNewCustomer_NewUserActivated,
                GetActivatedUserMailBody(new EncryptAndDecrypt().EncryptString(userEmail), confirmationCode, password));
        }

        /// <summary>
        /// Method that compose the body content of the email.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="string"/> that holds the user email.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds the encrypted confirmation code 
        /// to activate his/her account.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains the activation mail body.
        /// </returns>
        private string GetMailBody(string userEmail, string confirmationCode,
            string password, string firstName, string lastName)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(firstName.ToLower()));
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This is to acknowledge that your request to create an account was processed successfully.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Your email ID is your user ID");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Your password is: " + password);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("To activate your account, please copy and paste the ");
                sbBody.Append("below URL in the browser or click on the link");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(WebConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    WebConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    userEmail, confirmationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// A Method that gets the body content of the email
        /// </summary>
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that contains the activation mail body
        /// </returns>
        private string GetActivatedUserMailBody(string UserEmail, string ConfirmationCode, string password)
        {
            StringBuilder sbBody = new StringBuilder();
            try
            {
                string displayName = EnrollCustomer_internalRadioButton.Checked ? 
                    EnrollCustomer_internalUserFirstNameTextBox.Text.Trim() :
                    EnrollCustomer_subscriptionUserFirstNameTextBox.Text.Trim();

                sbBody.Append("Dear ");
                sbBody.Append(Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(displayName.ToLower()));
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM! Your account has been created, and is now ready for use.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Your email ID is your user ID");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Your password is: " + password);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// Gets the selected bussiness types from list.
        /// </summary>
        /// <returns></returns>
        private string GetSelectedBussinessTypesFromList()
        {
            StringBuilder SelectedItems = new StringBuilder();
            for (int ItemsCount = 0; ItemsCount < EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items.Count; ItemsCount++)
                if (EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items[ItemsCount].Selected)
                    SelectedItems.Append(EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.Items[ItemsCount].Value + ",");
            return SelectedItems.ToString().TrimEnd(',');
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// Loads the internal user details.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        private void LoadInternalUserDetails(string userID)
        {
            UserDetail userDetail = new CommonBLManager().GetUserDetail(int.Parse(userID));

            if (userDetail == null)
                return;

            EnrollCustomer_internalUserUserNameTextBox.Text = userDetail.UserName;
            EnrollCustomer_internalUserFirstNameTextBox.Text = userDetail.FirstName;
            EnrollCustomer_internalUserLastNameTextBox.Text = userDetail.LastName;
            EnrollCustomer_internalUserPhoneTextBox.Text = userDetail.Phone;

            if (Convert.ToString(userDetail.ExpiryDate) != "1/1/0001 12:00:00 AM")
                EnrollCustomer_internalexpiryDateTextBox.Text = Convert.ToString(userDetail.ExpiryDate);
            else
                EnrollCustomer_internalexpiryDateTextBox.Text = "";

            EnrollCustomer_siteAdminCheckBox.Checked = userDetail.UserType == UserType.SiteAdmin ? true : false;
           
            if (userDetail.IsActive == true)
                EnrollCustomer_internalUserActivateImmediatelyRadioButton.Checked = true;                         
             else 
                EnrollCustomer_internalUserActivateAfterRadioButton.Checked = true;              
            
            
        }
        #endregion Private Methods

        #region Protected Override Methods
        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            bool isValid = true;

            if (EnrollCustomer_subscriptionRadioButton.Checked)
            {
                if (string.IsNullOrEmpty(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistraion_EmptyUserID);
                    isValid = false;
                }
                else if (!IsValidEmailAddress(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_InvalidEmailAddress);
                    isValid = false;
                }
                else if (!CheckForEmailAddressAvailability(EnrollCustomer_subscriptionUserUserIDTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_UserEmailNotAvailable);
                    isValid = false;
                }
                if (string.IsNullOrEmpty(EnrollCustomer_subscriptionUserFirstNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyFirstName);
                    isValid = false;
                }
                if (string.IsNullOrEmpty(EnrollCustomer_subscriptionUserLastNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyLastName);
                    isValid = false;
                }
                if (string.IsNullOrEmpty(EnrollCustomer_subscriptionUserPhoneTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyPhoneNo);
                    isValid = false;
                }
                if (string.IsNullOrEmpty(EnrollCustomer_subscriptionUserCompanyNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyCompanyName);
                    isValid = false;
                }
                if (!EnrollCustomer_subscriptionUserTypeOfBussinessOtherTypeOfBussinessCheckBox.Checked && EnrollCustomer_subscriptionUserTypeOfBussinessCheckBoxList.SelectedIndex < 0)
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyTypeOfBusiness);
                    isValid = false;
                }

                //if (UserRegistration_typeOfBussinessCheckBoxList.SelectedIndex == UserRegistration_typeOfBussinessCheckBoxList.Items.Count - 1)
                if (EnrollCustomer_subscriptionUserTypeOfBussinessOtherTypeOfBussinessCheckBox.Checked)
                {
                    if (string.IsNullOrEmpty(EnrollCustomer_subscriptionUserOtherBussinessTextBox.Text.Trim()))
                    {
                        ShowErrorMessage(HCMResource.UserRegistration_EmptyOtherTypeOfBussiness);
                        isValid = false;
                    }
                    EnrollCustomer_subscriptionUserTypeOfBussinessOtherBussinessDiv.Style.Add("display", "block");
                }
                else
                    EnrollCustomer_subscriptionUserTypeOfBussinessOtherBussinessDiv.Style.Add("display", "none");
            }
            else
            {
                EnrollCustomer_numberOfUsersTr.Visible = false;
                if (string.IsNullOrEmpty(EnrollCustomer_internalUserUserNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistraion_EmptyUserID);
                    isValid = false;
                }
                else if (!IsValidEmailAddress(EnrollCustomer_internalUserUserNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_InvalidEmailAddress);
                    isValid = false;
                }
                else if (!CheckForEmailAddressAvailability(EnrollCustomer_internalUserUserNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_UserEmailNotAvailable);
                    isValid = false;
                }
                if (string.IsNullOrEmpty(EnrollCustomer_internalUserFirstNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyFirstName);
                    isValid = false;
                }
                if (string.IsNullOrEmpty(EnrollCustomer_internalUserLastNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyLastName);
                    isValid = false;
                }
                if (string.IsNullOrEmpty(EnrollCustomer_internalUserPhoneTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyPhoneNo);
                    isValid = false;
                }
            }

            //if (EnrollCustomer_numberOfUsersTr.Visible == true)
            //{
            //    if (string.IsNullOrEmpty(EnrollCustomer_numberOfUsersTextBox.Text.Trim()))
            //    {
            //        ShowErrorMessage(HCMResource.UserRegistration_EmptyNumberOfUsers);
            //        isValid = false;
            //    }
            //    else
            //    {
            //        int Temp = 0;
            //        int.TryParse(EnrollCustomer_numberOfUsersTextBox.Text.Trim(), out Temp);
            //        if (Temp == 0)
            //        {
            //            ShowErrorMessage(HCMResource.UserRegistration_InvalidNumberOfUsers);
            //            isValid = false;
            //        }
            //        if (Temp > Convert.ToInt32(EnrollCustomer_MaximumCorporateUsersHidden.Value))
            //        {
            //            ShowErrorMessage(
            //                string.Format(HCMResource.UserRegistration_MaximumSystemUserExceed, EnrollCustomer_MaximumCorporateUsersHidden.Value));
            //            isValid = false;
            //        }
            //    }
            //}


            return isValid;
        }
        #endregion Protected Override Methods

        protected void EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Call javascript function to disable the active check box
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallDisableActiveCheckBox", "<script language='javascript'>DisableActiveCheckBox();</script>", false);

            EnrollCustomer_numberOfUsersTr.Visible = false;
            if (EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList.SelectedIndex == 2)
                EnrollCustomer_numberOfUsersTr.Visible = true;

            switch (EnrollCustomer_subscriptionUserSubscriptionTypeDropDownList.SelectedIndex)
            {
                case 0:
                    EnrollCustomer_expirydateTextbox.Text =
                        DateTime.Now.Date.AddDays(GetExpiryDate(1)).ToShortDateString();
                    break;
                case 1:
                    EnrollCustomer_expirydateTextbox.Text =
                        DateTime.Now.Date.AddDays(GetExpiryDate(2)).ToShortDateString();
                    break;
                case 2:
                    EnrollCustomer_expirydateTextbox.Text =
                        DateTime.Now.Date.AddDays(GetExpiryDate(3)).ToShortDateString();
                    break;
                default:
                    break;
            }
        }

        private double GetExpiryDate(int subscriptionId)
        {
            return new UserRegistrationBLManager().GetExpiryDate(subscriptionId);
        }
    }
}