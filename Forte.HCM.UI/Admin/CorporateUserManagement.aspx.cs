﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CorporateUserManagement.cs
// File that represents the user interface for the to create a new 
// corporate user and can edit user and can delete user.
// This will interact with the UserRegistrationBLManager to insert,
// edit,delete users details to the database.

#endregion Header                                                              

#region Namespace                                                              

using System;
using Resources;
using System.Web;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using System.Web.Configuration;
using Forte.HCM.Utilities;


#endregion Namespace

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for CorporateUserManagement page. This is used
    /// to create,edit,delete a corporate users. 
    /// This class is inherited from Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CorporateUserManagement : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user object details.
        /// </param>
        private delegate void AsyncTaskDelegate(UserDetail userDetail);

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="EntityType">
        /// A <see cref="entityType"/> that holds the entity type.
        /// </param>
        /// <param name="userid">
        /// A <see cref="string"/> that holds the user id object details.
        /// </param>
        private delegate void UserActiveStatusDelegate(EntityType entityType, string userid);

        #endregion Declaration

        #region Private Varibles                                               

        /// <summary>
        /// A <see cref="System.String"/> constant that holds the 
        /// view state name of the sort direction
        /// </summary>
        private const string SORTDIRECTION_VIEWSTATE = "GRIDVIEWSORT_DIRECTION";

        /// <summary>
        ///  A <see cref="System.String"/> constant that holds the 
        /// view state name of the sort expression
        /// </summary>
        private const string SORTEXPRESSION_VIEWSTATE = "GRIDVIEWSORT_EXPRESSION";

        #endregion Private Variables

        #region Enum                                                           

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(HCMResource.CorporateUserManagement_PageTitle);
                Forte_CorporateUserManagement_CorporateUserSignUp.CREATED_BY = base.userID;
                //Forte_CorporateUserManagement_CorporateUserSignUp.SelectedIndexChanged+=
                //    new CommonControls.CorporateUserSignUp.DropDown_selectedIndexChanged
                //        (Forte_CorporateUserManagement_CorporateUserSignUp_SelectedIndexChanged);

                Session["Tenant_id"] = base.tenantID;
                if (IsPostBack)
                    return;

                Forte_CorporateUserManagement_createAssessorConfirmMsgControl.Message = "Are you sure want to mark the user as an assessor?";
                Forte_CorporateUserManagement_createAssessorConfirmMsgControl.Type = MessageBoxType.YesNo;
                Forte_CorporateUserManagement_createAssessorConfirmMsgControl.Title = "Mark As An Assessor";

                LoadValues();


            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }        

        protected void ResetButtonClick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void ResetLinkButtonclick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        protected void Forte_CorporateUserManagement_addNewUserLinkButton_Click(object sender, EventArgs e)
        {
            if (CanAddNewUser())
            {
                UserRegistrationInfo userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.UserID = Convert.ToInt32(Forte_CorporateUserManagement_tenantIdHidden.Value);
                userRegistrationInfo.Company = Forte_CorporateUserManagement_TenantNameLabel.Text;
                userRegistrationInfo.Phone = Forte_CorporateUserManagement_userPhoneHidden.Value;
                userRegistrationInfo.Title = Forte_CorporateUserManagement_tenantTitleFieldLabel.Text;
                Forte_CorporateUserManagement_CorporateUserSignUp.InsertNewDataSource = userRegistrationInfo;
                Forte_CorporateUserManagement_AddNewUserModalPopupExtender.Show();
            }
            else
            {
                ShowErrorMessage(HCMResource.DeactivateUsersMessage);
            }
        }

        protected void Forte_CorporateUserManagement_usersGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex(Forte_CorporateUserManagement_usersGridView, ViewState[SORTEXPRESSION_VIEWSTATE].ToString());
                if (sortColumnIndex != -1)
                    AddSortImage(sortColumnIndex, e.Row, (SortType)ViewState[SORTDIRECTION_VIEWSTATE]);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_CorporateUserManagement_usersGridView_RowSorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState[SORTEXPRESSION_VIEWSTATE].ToString() == e.SortExpression)
                {
                    ViewState[SORTDIRECTION_VIEWSTATE] =
                        ((SortType)ViewState[SORTDIRECTION_VIEWSTATE]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState[SORTDIRECTION_VIEWSTATE] = SortType.Ascending;
                ViewState[SORTEXPRESSION_VIEWSTATE] = e.SortExpression;
                BindCorporateChildUsers();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_CorporateUserManagement_usersGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete User")
                {
                    Forte_SubscriptionFeaturePricing_deleteUserIdHidden.Value = e.CommandArgument.ToString();
                    Forte_CorporateUserManagement_deleteUserConfrimPopUpModalPopUpExtender.Show();
                }
                else if (e.CommandName == "Edit User")
                {
                    UserRegistrationInfo userRegistrationInfo = new UserRegistrationInfo();
                    userRegistrationInfo.UserID = Convert.ToInt32(e.CommandArgument);
                    userRegistrationInfo.UserEmail = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_CorporateUserManagement_userNameLabel")).Text;
                    userRegistrationInfo.FirstName = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_CorporateUserManagement_firstNameLabel")).Text;
                    userRegistrationInfo.LastName = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_CorporateUserManagement_LastNameLabel")).Text;
                    userRegistrationInfo.FormId = Convert.ToInt32(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_CorporateUserManagement_userFormIdLabel")).Text);
                    userRegistrationInfo.IsActiveStatus = Convert.ToInt16(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                           "Forte_CorporateUserManagement_isActiveStatusLabel")).Text);
                    userRegistrationInfo.IsActive = Convert.ToInt16(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                        "Forte_CorporateUserManagement_isActiveLabel")).Text == "Yes" ? 1 : 0);
                    userRegistrationInfo.IsCustomerAdmin = new UserRegistrationBLManager().GetCustomerAdmin(userRegistrationInfo.UserID);
                    userRegistrationInfo.CorporateRoles = new UserRegistrationBLManager().GetcorporateRoles(userRegistrationInfo.UserID);
                    Forte_CorporateUserManagement_CorporateUserSignUp.EditUserDataSource = userRegistrationInfo;
                    Forte_CorporateUserManagement_AddNewUserModalPopupExtender.Show();
                }
                else if (e.CommandName == "DeactivateUser")
                {
                    CorporateSubscriptionManagement_activateUserConfirmMsgControl.Message =
                        Resources.HCMResource.CorporateSubscriptionManagement_AreYouSureToDeactivateThisUser;
                    CorporateSubscriptionManagement_activateUserConfirmMsgControl.Type = MessageBoxType.YesNo;
                    CorporateSubscriptionManagement_activateUserConfirmMsgControl.Title = "Warning";
                    CorporateSubscriptionManagement_activateUserHiddenField.Value = e.CommandArgument.ToString();
                    CorporateSubscriptionManagement_activateStatusHiddenField.Value = "0";
                    CorporateSubscriptionManagement_activateUserModalPopupExtender.Show();
                }
                else if (e.CommandName == "ActivateUser")
                {

                    if (CanAddNewUser())
                    {
                        UserRegistrationInfo userRegistrationInfo = new UserRegistrationInfo();
                        userRegistrationInfo.UserID = Convert.ToInt32(e.CommandArgument);
                        userRegistrationInfo.UserEmail = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                               "Forte_CorporateUserManagement_userNameLabel")).Text;
                        userRegistrationInfo.FirstName = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                               "Forte_CorporateUserManagement_firstNameLabel")).Text;
                        userRegistrationInfo.LastName = ((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                               "Forte_CorporateUserManagement_LastNameLabel")).Text;
                        userRegistrationInfo.FormId = Convert.ToInt32(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                               "Forte_CorporateUserManagement_userFormIdLabel")).Text);
                        userRegistrationInfo.IsActiveStatus = Convert.ToInt16(((Label)((GridViewRow)((DataControlFieldCell)(((ImageButton)e.CommandSource).Parent)).Parent).FindControl(
                               "Forte_CorporateUserManagement_isActiveStatusLabel")).Text);
                        userRegistrationInfo.IsActive = 1;
                        userRegistrationInfo.IsCustomerAdmin = new UserRegistrationBLManager().GetCustomerAdmin(userRegistrationInfo.UserID);
                        userRegistrationInfo.CorporateRoles = new UserRegistrationBLManager().GetcorporateRoles(userRegistrationInfo.UserID);

                        new UserRegistrationBLManager().UpdateCorporateUserAdmin(userRegistrationInfo);

                        LoadValues();

                        base.ShowMessage(Forte_CorporateUserManagement_topSuccessMessageLabel,
                            "User activated successfully"); 

                        try
                        {
                            // Send mail to user on activation and deactivation.
                            UserActiveStatusDelegate taskActiveDelegate = new UserActiveStatusDelegate(SendUserActiveStatusMail);
                            IAsyncResult result = taskActiveDelegate.BeginInvoke(EntityType.UserActivated,
                                userID.ToString(),
                                new AsyncCallback(SendUserActiveStatusMailCallBack), taskActiveDelegate);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                            base.ShowMessage(Forte_CorporateUserManagement_topErrorMessageLabel,
                            Forte_CorporateUserManagement_topErrorMessageLabel, exp.Message);
                        }
                    } 
                    else
                    {
                        ShowErrorMessage(HCMResource.DeactivateUsersMessage);
                    }
                }
                else if (e.CommandName == "EmailActivationCode")
                {

                    try
                    {
                        // Compose and send activation email.
                        SendConfirmationCodeEmail(Convert.ToInt32(e.CommandArgument));

                        // Clear the messages.
                        ClearMessages();

                        base.ShowMessage(Forte_CorporateUserManagement_topSuccessMessageLabel,
                            Forte_CorporateUserManagement_bottomSuccessMessageLabel, "Activation code mailed successfully");
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                        base.ShowMessage(Forte_CorporateUserManagement_topErrorMessageLabel,
                            Forte_CorporateUserManagement_bottomErrorMessageLabel, "Unable to mail the activation code");
                    }
                }
                else if (e.CommandName == "EditAssessor")
                {
                    if (e.CommandArgument == null) return;

                    Response.Redirect("../Admin/EnrollAssessor.aspx?m=1&s=2&parentpage=EDIT_ASS&assessorid=" + e.CommandArgument.ToString(), false);    
                }
                else if (e.CommandName == "CreateAssessor")
                {
                    GridViewRow userGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);
                    
                    if (userGridViewRow == null) return;

                    ViewState["USER_ID"] = e.CommandArgument.ToString();

                    HiddenField CorporateSubscriptionManagement_usersGridView_userEmailHiddenField =
                        (HiddenField)userGridViewRow.FindControl("CorporateSubscriptionManagement_usersGridView_userEmailHiddenField");

                    if (CorporateSubscriptionManagement_usersGridView_userEmailHiddenField==null)
                        ViewState["USER_EMAIL"]=string.Empty;
                    else
                        ViewState["USER_EMAIL"] = CorporateSubscriptionManagement_usersGridView_userEmailHiddenField.Value.ToString();

                    Forte_CorporateUserManagement_createAssessorModalPopupExtender.Show();
                }
                else if (e.CommandName == "ResendPassword")
                {
                    GridViewRow userGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);

                    if (userGridViewRow == null) return;

                    string userEmail = ((HiddenField)userGridViewRow.
                        FindControl("CorporateSubscriptionManagement_usersGridView_userEmailHiddenField")).Value;

                    // Get user detail along with password.
                    UserDetail userDetail = new UserDetail();
                    userDetail = new UserRegistrationBLManager().GetPassword(userEmail.Trim());

                    if (userDetail == null)
                    {
                        base.ShowMessage(Forte_CorporateUserManagement_topErrorMessageLabel, "No such user name exist !");
                        return;
                    }

                    // Decrypt the password.
                    userDetail.Password = new EncryptAndDecrypt().DecryptString(userDetail.Password);

                    try
                    {
                        // Sent alert mail to user asynchronously.
                        AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(ResendPasswordToUser);
                        IAsyncResult result = taskDelegate.BeginInvoke(userDetail,
                            new AsyncCallback(ResendPasswordToUserCallBack), taskDelegate);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }

                    // Show a message to the user.
                    base.ShowMessage(Forte_CorporateUserManagement_topSuccessMessageLabel,
                        string.Format("Password has been sent to {0}", userEmail.Trim()));
                }
            }   
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        
        protected void CorporateSubscriptionManagement_activateUserConfirmMsgControl_OKClick(object sender, EventArgs e)
        {
            try
            {
                if (CorporateSubscriptionManagement_activateUserHiddenField.Value != null
                    && CorporateSubscriptionManagement_activateUserHiddenField.Value.Trim() != string.Empty)
                {
                    int userID = int.Parse(CorporateSubscriptionManagement_activateUserHiddenField.Value);
                    Int16 activateID = Int16.Parse(CorporateSubscriptionManagement_activateStatusHiddenField.Value);

                    // Activate/deactivate the user.
                    new AdminBLManager().ActivateUsers(userID, activateID, base.userID);

                    base.ShowMessage(Forte_CorporateUserManagement_topSuccessMessageLabel,
                        activateID == 1 ? "User activated successfully" : "User deactivated successfully");
                    
                    try
                    {
                        // Send mail to user on activation and deactivation.
                        UserActiveStatusDelegate taskActiveDelegate = new UserActiveStatusDelegate(SendUserActiveStatusMail);
                        IAsyncResult result = taskActiveDelegate.BeginInvoke(activateID == 1 ? EntityType.UserActivated : EntityType.UserDeactivated,
                            userID.ToString(),
                            new AsyncCallback(SendUserActiveStatusMailCallBack), taskActiveDelegate);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                        base.ShowMessage(Forte_CorporateUserManagement_topErrorMessageLabel,
                        Forte_CorporateUserManagement_topErrorMessageLabel, exp.Message);
                    }

                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(Forte_CorporateUserManagement_topErrorMessageLabel,
                           Forte_CorporateUserManagement_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        protected bool GetActiveIcon(int value)
        {
            return value == 0 ? true : false;
        }
        protected bool GetInActiveIcon(int value)
        {
            return value == 1 ? true : false;
        }


        protected void Forte_CorporateUserManagement_deleteUserokPopUpClick(object sender, EventArgs e)
        {
            try
            {
                DeleteCoporateUsers(Convert.ToInt32(Forte_SubscriptionFeaturePricing_deleteUserIdHidden.Value));
                Forte_SubscriptionFeaturePricing_deleteUserIdHidden.Value = "";
                BindCorporateChildUsers();
                ShowSuccessMessage(HCMResource.CorporateUserManagement_DeleteUserSuccessfull);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_CorporateUserManagement_deleteUsercancelPopUpClick(object sender, EventArgs e)
        {
            Forte_SubscriptionFeaturePricing_deleteUserIdHidden.Value = "";
        }



        protected void CorporateUserManagement_createAssessorConfirmMsgControl_CancelClick
      (object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);

                ShowErrorMessage(ex);
            }
        }

        protected void CorporateUserManagement_createAssessorConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try 
            { 
                if (ViewState["USER_ID"] == null) return;

                CreateNewAssessor(Convert.ToInt32(ViewState["USER_ID"]), ViewState["USER_EMAIL"].ToString());

                Response.Redirect("../Admin/EnrollAssessor.aspx?m=1&s=2&parentpage=EDIT_ASS&assessorid=" + ViewState["USER_ID"].ToString(), false);
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);

                ShowErrorMessage(ex);
            }
        }

        #endregion Events

        #region Private Methods                                                

        /// <summary>
        /// Method that send mail to user indicating that password has been resend
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user deail object
        /// detail.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void ResendPasswordToUser(UserDetail userDetail)
        {
            try
            {
                new EmailHandler().SendMail(EntityType.ResendPassword, userDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that send mail to user indicating that password has been resend
        /// </summary>
        /// <param name="EntityType">
        /// A <see cref="entityType"/> that holds the entity type object
        /// </param>
        /// <param name="userid">
        /// A <see cref="string"/> that holds the user id
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendUserActiveStatusMail(EntityType entityType, string userid)
        {
            try
            {
                new EmailHandler().SendMail(entityType, userid);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Method that clears the messages.
        /// </summary>
        private void ClearMessages()
        {
            Forte_CorporateUserManagement_topSuccessMessageLabel.Text = string.Empty;
            Forte_CorporateUserManagement_bottomSuccessMessageLabel.Text = string.Empty;
            Forte_CorporateUserManagement_topErrorMessageLabel.Text = string.Empty;
            Forte_CorporateUserManagement_bottomErrorMessageLabel.Text = string.Empty;
        }

        private void UpdateUser()
        {
            UserRegistrationInfo userRegistrationInfo = null;
            try
            {
                /* userRegistrationInfo = new UserRegistrationInfo();
                 userRegistrationInfo.UserEmail = Forte_CorporateUserSignUp_userIdTextBox.Text.Trim();
                 userRegistrationInfo.FirstName = Forte_CorporateUserSignUp_firstNameTextBox.Text.Trim();
                 userRegistrationInfo.LastName = Forte_CorporateUserSignUp_lastNameTextBox.Text.Trim();
                 userRegistrationInfo.ModifiedBy = CREATED_BY;
                 //userRegistrationInfo.FormId = Convert.ToInt32(Forte_CorporateUserSignUp_formNameDropDownList.SelectedValue);
                 userRegistrationInfo.UserID = Convert.ToInt32(Forte_CorporateSignUp_editUserTenantIdHidden.Value);
                 userRegistrationInfo.IsActive = 1;
                 if (Forte_CorporateUserSignUp_customerAdminCheckBox.Checked == false)
                     userRegistrationInfo.IsCustomerAdmin = false;
                 else
                     userRegistrationInfo.IsCustomerAdmin = true;
                 userRegistrationInfo.CorporateRoles = Forte_CorporateUserSignUp_rolesHiddenField.Value;

                 new UserRegistrationBLManager().UpdateCorporateUserAdmin(userRegistrationInfo);*/
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that deletes the user from the repository
        /// </summary>
        /// <param name="Tanent_User_Id">
        /// A <see cref="System.Int32"/> that holds the tenant User id
        /// of the user to be deleted
        /// </param>
        private void DeleteCoporateUsers(int Tanent_User_Id)
        {
            new UserRegistrationBLManager().DeleteTenantUser(Tanent_User_Id);
        }

        /// <summary>
        /// A Method that returns whether the user can add a new user
        /// to his corporate account.
        /// </summary>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds
        /// whether a new user can be added to his/her coporate account
        /// </returns>
        private bool CanAddNewUser()
        {

            UserRegistrationInfo userEditInfo = new AdminBLManager().GetCorporateUserDetails(base.userID);

            return Convert.ToInt32(userEditInfo.ActiveNoOfUsers) <
                Convert.ToInt32(Forte_CorporateUserManagement_tenantNumberOfUserFieldLabel.Text) ? true : false;
        }

        /// <summary>
        /// A method that redirects to the access denied page.
        /// </summary>
        private void RedirectToAccessDeniedPage()
        {
            Response.Redirect("../Common/AccessDenied.aspx", false);
        }

        /// <summary>
        /// A Method that checks and binds the logged in corporate account users
        /// </summary>
        private void BindCorporateChildUsers()
        {
            List<UserRegistrationInfo> userRegistrationInfo = null;

            try
            {
                userRegistrationInfo = new UserRegistrationBLManager().GetSelectedCorporateAccountUsers(base.tenantID,
                    ViewState[SORTEXPRESSION_VIEWSTATE].ToString(), (SortType)ViewState[SORTDIRECTION_VIEWSTATE]);                

                if ((userRegistrationInfo == null || userRegistrationInfo.Count == 0) && base.isSiteAdmin == false)
                {
                    RedirectToAccessDeniedPage();
                    return;
                }
                string strAdminRole = Enum.GetName(typeof(SubscriptionRolesEnum), 3);
                var CheckIsAdminUser = System.Linq.Enumerable.Where(userRegistrationInfo, p => p.SubscriptionRole == strAdminRole);
                if ((CheckIsAdminUser == null || CheckIsAdminUser.ToList().Count == 0) && base.isSiteAdmin == false)
                {
                    RedirectToAccessDeniedPage();
                    return;
                }
                var AdminUser = CheckIsAdminUser.ToList();

                if (AdminUser.Count > 0)
                {
                    Forte_CorporateUserManagement_tenantIdHidden.Value = AdminUser[0].TenantID.ToString();
                    Forte_CorporateUserManagement_userPhoneHidden.Value = AdminUser[0].Phone;
                    Forte_CorporateUserManagement_TenantNameLabel.Text = AdminUser[0].Company;
                    Forte_CorporateUserManagement_tenantTitleFieldLabel.Text = AdminUser[0].Title;
                    Forte_CorporateUserManagement_tenantNumberOfUserFieldLabel.Text = AdminUser[0].NumberOfUsers.ToString();
                    string strNormalUserRole = Enum.GetName(typeof(SubscriptionRolesEnum), 4);
                    var NormalUsers = System.Linq.Enumerable.Where(userRegistrationInfo, p => p.SubscriptionRole == strNormalUserRole);
                    Forte_CorporateUserManagement_usersGridView.DataSource = NormalUsers.ToList();
                    Forte_CorporateUserManagement_usersGridView.DataBind();
                    Forte_CorporateUserManagement_addNewUserLinkButton.Visible = true;
                }
                else
                {
                    Forte_CorporateUserManagement_addNewUserLinkButton.Visible = false;
                }
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// to be show to the user
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            base.ShowMessage(Forte_CorporateUserManagement_topErrorMessageLabel,
                Forte_CorporateUserManagement_bottomErrorMessageLabel, Message);
        }

        /// <summary>
        /// Method that compose and send the activation email.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="String"/> that holds the user email
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        private void SendConfirmationCodeEmail(int userID)
        {
            // Get user info detail.
            UserRegistrationInfo userInfo = new UserRegistrationBLManager().
            GetUserRegistrationInfo(userID);

            if (userInfo == null)
                throw new Exception("User does not exist");

            string strCC = string.Empty;

            strCC = ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"];
            // Assign User's Admin Corporate Email Id
            if (!string.IsNullOrEmpty(userInfo.UserCorporateAdminEmail))
                strCC = string.Concat(userInfo.UserCorporateAdminEmail, ",",
                    ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"]);

            // Send mail.
            new EmailHandler().SendMail
                (userInfo.UserEmail, strCC,
                HCMResource.UserRegistration_MailActivationCodeSubject,
                GetMailBody(userInfo));
        }

        /// <summary>
        /// Method that constructs the body content of the email.
        /// </summary>
        /// <param name="userInfo">
        /// A <see cref="UserRegistrationInfo"/> that holds the user details.
        /// </param>
        /// A <see cref="string"/> that contains the mail body.
        /// </returns>
        private string GetMailBody(UserRegistrationInfo userInfo)
        {
            string encryptedEmail = new Forte.HCM.Utilities.EncryptAndDecrypt().EncryptString(userInfo.UserEmail);

            StringBuilder sbBody = new StringBuilder();

            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(userInfo.FirstName);
                sbBody.Append(" ");
                sbBody.Append(userInfo.LastName);
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This is to acknowledge that your request to create an account was processed successfully.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("To Activate your account, please copy and paste the ");
                sbBody.Append("below URL in the browser or click on the link");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(ConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    ConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    ConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    encryptedEmail, userInfo.ActivationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }
        /// <summary>
        /// A Method that logs the error message
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the 
        /// exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        /// <summary>
        /// Creating a new assessor from existing listed users
        /// </summary>
        /// <param name="userId">
        ///     <see cref="System.String"/>         
        /// </param>
        /// <param name="userEmail">
        ///     <see cref="System.String"/>
        /// </param>
        private void CreateNewAssessor(int userId,string userEmail)
        {
            UserDetail userDetail = new UserDetail();

            userDetail = new CommonBLManager().GetUserDetail(userId);


            AssessorDetail assessorDetails = new AssessorDetail();

            assessorDetails.UserID = userId;
            assessorDetails.UserEmail = userEmail;
            assessorDetails.AlternateEmailID = string.Empty;
            assessorDetails.FirstName = string.Empty;
            assessorDetails.LastName = string.Empty;
            assessorDetails.Mobile = string.Empty;

            if (userDetail != null)
            {
                assessorDetails.UserEmail = userDetail.Email;
                assessorDetails.AlternateEmailID = userDetail.AltEmail;
                assessorDetails.FirstName = userDetail.FirstName;
                assessorDetails.LastName = userDetail.LastName;
                assessorDetails.Mobile = userDetail.Phone;
            }

            assessorDetails.Skill = string.Empty;
            assessorDetails.CreatedBy = base.userID;
            assessorDetails.NonAvailabilityDates = string.Empty;
            assessorDetails.Image = null;
            assessorDetails.HomePhone = string.Empty;
            assessorDetails.AdditionalInfo = string.Empty;
            assessorDetails.LinkedInProfile = string.Empty;

            //Save assessor details
            string retval = new AssessorBLManager().SaveAssessorDetails(assessorDetails);

            try
            {
                new EmailHandler().SendMail(userDetail.Email, ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"], "You have been marked as an assessor", AesssorMailBody(assessorDetails.FirstName, userId));
            }
            catch { }
        }

        private string AesssorMailBody(string  assessorName,int assessorId)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                string statusMsg = string.Empty;
                statusMsg = "This is to inform you that you have been marked as an assessor. You are invited to update your profile and skill details";
                 
                sbBody.Append("Dear ");
                sbBody.Append(assessorName);
                
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(statusMsg);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Click here to proceed   ");
                string strUri = WebConfigurationManager.AppSettings["DEFAULT_URL"] + "Admin/EnrollAssessor.aspx?m=1&s=2&parentpage=MENU&assessorid=" + assessorId;
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        #endregion Private Methods

        #region Asynchronous Method Handlers
        
        /// <summary>
        /// Handler method that acts as the callback method for send mail.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void ResendPasswordToUserCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Handler method that acts as the callback method for send mail.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendUserActiveStatusMailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                UserActiveStatusDelegate caller = (UserActiveStatusDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        #region Public Methods                                                 

        public string GetCharExpansion(object obj)
        {
            if (obj == null)
                return "No";
            else if (obj.ToString() == "Y")
                return "Yes";
            else 
                return "No";
        }

        public void ShowSuccessMessage(string Message)
        {
            base.ShowMessage(Forte_CorporateUserManagement_topSuccessMessageLabel,
                Forte_CorporateUserManagement_bottomSuccessMessageLabel, Message);
        }
        public void ShowErrMessage(string Message)
        {
            base.ShowMessage(Forte_CorporateUserManagement_topErrorMessageLabel,
                Forte_CorporateUserManagement_bottomErrorMessageLabel, Message);
        }

        public void RebindGridView()
        {
            BindCorporateChildUsers();
        }

        public void ShowSignUpPopUp()
        {
            Forte_CorporateUserManagement_AddNewUserModalPopupExtender.Show();
        }

        #endregion Public Methods

        #region Override Methods                                               

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            ViewState[SORTEXPRESSION_VIEWSTATE] = "EMAIL";
            ViewState[SORTDIRECTION_VIEWSTATE] = SortDirection.Ascending;
            BindCorporateChildUsers();
        }

        #endregion Override Methods

        #region Proctected Methods

        /// <summary>
        /// Method that retrieves the user activated status. This helps to show
        /// or hide the 'Email Activation Code' icon in the grid
        /// </summary>
        /// <param name="activated">
        /// A <see cref="string"/> that holds the activated flag.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of user activated
        /// status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for user activated status are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsUserNotActivated(string activated)
        {
            return (activated.Trim().ToUpper() == "N" ? true : false);
        }

        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="assessor">The assessor.</param>
        /// <returns></returns>
        protected bool IsAssessor(string assessor)
        {
           // return assessor == "N" ? true : false;             
            if (assessor == "Y")
                return true;
            else
                return false;
        }

        protected bool IsNotAssessor(string assessor)
        {
            //return assessor == "Y" ? true : false;
            if (assessor == "N") 
                return true;
            else
                return false;
        }

        #endregion Proctected Methods

    }

        
}