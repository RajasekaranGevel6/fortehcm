﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OfflineInterviewLog.aspx.cs
// File that represents the user interface layout and functionalities for the
// OfflineInterviewLog page. This page help to display the log information 
// (such as application, event, system, exception, desktop image) captured 
// during the offline interview session This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the OfflineInterviewLog page. This page help to display the log 
    /// information (such as application, event, system, exception, desktop 
    /// image) captured during the offline interview session. This class 
    /// inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class OfflineInterviewLog : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "335px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Private Variables                                              

        /// <summary>
        /// A <seealso cref="int"/> that holds the page number for desktop
        /// thumbnail image view.
        /// </summary>
        private int desktopThumbImagepageNo;

        /// <summary>
        /// A <seealso cref="int"/> that holds the page number for webcam
        /// thumbnail image view.
        /// </summary>
        private int webCamThumbImagepageNo;

        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Offline Interview Log");

                OfflineInterviewLog_topSuccessMessageLabel.Text = string.Empty;
                OfflineInterviewLog_bottomSuccessLabel.Text = string.Empty;
                OfflineInterviewLog_topErrorMessageLabel.Text = string.Empty;
                OfflineInterviewLog_bottomErrorLabel.Text = string.Empty;

                CheckAndSetExpandOrRestore();

                Page.Form.DefaultButton = OfflineInterviewLog_searchButton.UniqueID;

                if (!IsPostBack)
                {
                    desktopThumbImagepageNo = 1;
                    webCamThumbImagepageNo = 1;
                    Session["OfflineInterviewLogDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                    Session["OfflineInterviewLogWebCamImagePageNo"] = webCamThumbImagepageNo;
                }

                //Assign the on click attributes for the expand or restore button 
                OfflineInterviewLog_UparrowSpan.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    OfflineInterviewLog_DetailsDIV.ClientID + "','" +
                    OfflineInterviewLog_SummaryDIV.ClientID + "','" +
                    OfflineInterviewLog_UparrowSpan.ClientID + "','" +
                    OfflineInterviewLog_DownarrowSpan.ClientID + "','" +
                    OfflineInterviewLog_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");

                OfflineInterviewLog_DownarrowSpan.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    OfflineInterviewLog_DetailsDIV.ClientID + "','" +
                    OfflineInterviewLog_SummaryDIV.ClientID + "','" +
                    OfflineInterviewLog_UparrowSpan.ClientID + "','" +
                    OfflineInterviewLog_DownarrowSpan.ClientID + "','" +
                    OfflineInterviewLog_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");

                OfflineInterviewLog_bottomPagingNavigator.PageNumberClick +=
                   new PageNavigator.PageNumberClickEventHandler
                       (OfflineInterviewLog_bottomPagingNavigator_PageNumberClick);

                OfflineInterviewLog_stateExpandHiddenField.Value = "0";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewLog_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "DATE";

                OfflineInterviewLog_bottomPagingNavigator.Reset();

                LoadValues(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void OfflineInterviewLog_bottomPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["pagenumber"] = Convert.ToInt32(e.PageNumber);
                OfflineInterviewLog_resultsGridView.EditIndex = -1;
                OfflineInterviewLog_PageNumberHidden.Value = e.PageNumber.ToString();

                LoadValues(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset is clicked       
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewLog_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topSuccessMessageLabel,
                    OfflineInterviewLog_bottomSuccessLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void OfflineInterviewLog_resultsGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                OfflineInterviewLog_stateExpandHiddenField.Value = "0";
                OfflineInterviewLog_resultsExpandLinkButton.Text = "Expand All";
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                OfflineInterviewLog_bottomPagingNavigator.Reset();
                LoadValues(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void OfflineInterviewLog_resultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (OfflineInterviewLog_resultsGridView, (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void OfflineInterviewLog_resultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int deskTopPageIndex = 0;
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton OfflineInterviewLog_showDesktopImageButton = (ImageButton)e.Row.FindControl(
                    "OfflineInterviewLog_showDesktopImageButton");
                ImageButton OfflineInterviewLog_showWebcamImageButton = (ImageButton)e.Row.FindControl(
                    "OfflineInterviewLog_showWebcamImageButton");
                ImageButton OfflineInterviewLogviewLogFileImageButton = (ImageButton)e.Row.FindControl(
                    "OfflineInterviewLog_viewLogFileImageButton");
                HiddenField OfflineInterviewLog_LogIDHiddenField = (HiddenField)e.Row.FindControl(
                    "OfflineInterviewLog_LogIDHiddenField");

                LinkButton OfflineInterviewLog_systemNameLinkButton =
                    (LinkButton)e.Row.FindControl("OfflineInterviewLog_systemNameLinkButton");
                HtmlContainerControl OfflineInterviewLog_MessagedetailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("OfflineInterviewLog_MessagedetailsDiv");
                HtmlAnchor OfflineInterviewLog_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("OfflineInterviewLog_focusDownLink");

                OfflineInterviewLog_systemNameLinkButton.Attributes.Add("onclick", "return MessageDetails('" +
                OfflineInterviewLog_MessagedetailsDiv.ClientID + "','" + OfflineInterviewLog_focusDownLink.ClientID + "')");

                GridView OfflineInterviewLog_detailsGridView = (GridView)e.Row.FindControl(
                    "OfflineInterviewLog_detailsGridView");

                List<SystemMessageLogDetail> systemMessageLogs = new TrackingBLManager().
                    GetOfflineInterviewMessageLog(Convert.ToInt32(OfflineInterviewLog_LogIDHiddenField.Value));

                OfflineInterviewLog_detailsGridView.DataSource = systemMessageLogs;
                OfflineInterviewLog_detailsGridView.DataBind();

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                desktopThumbImagepageNo = (int)Session["OfflineInterviewLogDesktopThumbImagePageNo"];
                int.TryParse(e.Row.DataItem.ToString(), out deskTopPageIndex);

                OfflineInterviewLog_showDesktopImageButton.Attributes.Add("onclick", "javascript:return OpenScreenShot('OI','" +
                    (deskTopPageIndex + ((desktopThumbImagepageNo - 1) * base.GridPageSize)).ToString()
                    + "','SCREEN_IMG','" + OfflineInterviewLog_LogIDHiddenField.Value + "','LOG');");

                OfflineInterviewLog_showWebcamImageButton.Attributes.Add("onclick", "javascript:return OpenScreenShot('OI','" +
                    (deskTopPageIndex + ((desktopThumbImagepageNo - 1) * base.GridPageSize)).ToString()
                    + "','USER_IMG','" + OfflineInterviewLog_LogIDHiddenField.Value + "','LOG');");

                OfflineInterviewLogviewLogFileImageButton.Attributes.Add("onclick", "javascript:return OpenTrackingLogFile('OI','" +
                   OfflineInterviewLog_LogIDHiddenField.Value + "','" + OfflineInterviewLog_systemNameLinkButton.Text + "');");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void OfflineInterviewLog_resultsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "DeleteMessage")
                return;

            OfflineInterviewLog_LogIDHiddenFied.Value = e.CommandArgument.ToString();

            string result = string.Empty;

            OfflineInterviewLog_deleteMessageConfirmMsgControl.Message = string.Format
                (Resources.HCMResource.CyberProctorLogGridView_MessageDeleteConfirmation);

            OfflineInterviewLog_deleteMessageConfirmMsgControl.Type = MessageBoxType.YesNo;
            OfflineInterviewLog_deleteMessageConfirmMsgControl.Title = "Warning";
            OfflineInterviewLog_deleteFormModalPopupExtender.Show();
        }

        /// <summary>
        /// Handler method that will be called when the ok button is 
        /// clicked in the delete message confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewLog_deleteMessageConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                int logID = int.Parse(OfflineInterviewLog_LogIDHiddenFied.Value);

                new TrackingBLManager().DeleteOfflineInterviewLog(logID);

                //Reset the page navigator
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "DATE";

                // Get the update business details
                LoadValues(1);

                base.ShowMessage(OfflineInterviewLog_topSuccessMessageLabel,
                    OfflineInterviewLog_bottomSuccessLabel, "Log entry deleted successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the delete message confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewLog_deleteMessageConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                OfflineInterviewLog_LogIDHiddenFied.Value = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that sets the expand or restored state.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            if (!Utility.IsNullOrEmpty(OfflineInterviewLog_isMaximizedHiddenField.Value) &&
               OfflineInterviewLog_isMaximizedHiddenField.Value == "Y")
            {
                OfflineInterviewLog_SummaryDIV.Style["display"] = "none";
                OfflineInterviewLog_UparrowSpan.Style["display"] = "block";
                OfflineInterviewLog_DownarrowSpan.Style["display"] = "none";
                OfflineInterviewLog_DetailsDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                OfflineInterviewLog_SummaryDIV.Style["display"] = "block";
                OfflineInterviewLog_UparrowSpan.Style["display"] = "none";
                OfflineInterviewLog_DownarrowSpan.Style["display"] = "block";
                OfflineInterviewLog_DetailsDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        private void LoadValues(int pageNumber)
        {
            string systemName = OfflineInterviewLog_systemNameTextBox.Text.Trim();

            string macAddress = OfflineInterviewLog_macAddressTextBox.Text.Trim();

            string sortOrder = ViewState["SORT_ORDER"].ToString();
            string sortExpression = ViewState["SORT_FIELD"].ToString();

            int totalRecords = 0;

            List<SystemLogDetail> systemLogs = new TrackingBLManager().
                GetOfflineInterviewCommonLog(systemName, macAddress,
                OfflineInterviewLog_datePopupTextBox.Text.ToString(), sortOrder,
                sortExpression, pageNumber,
                base.GridPageSize, out totalRecords);

            if (systemLogs.Count != 0)
            {
                OfflineInterviewLog_resultsExpandLinkButton.Visible = true;

                OfflineInterviewLog_resultsGridView.DataSource = systemLogs;
                OfflineInterviewLog_resultsGridView.DataBind();

                OfflineInterviewLog_bottomPagingNavigator.Visible = true;

                OfflineInterviewLog_bottomPagingNavigator.TotalRecords = totalRecords;

                OfflineInterviewLog_bottomPagingNavigator.PageSize = base.GridPageSize;
            }
            else
            {
                OfflineInterviewLog_resultsGridView.DataSource = systemLogs;
                OfflineInterviewLog_resultsGridView.DataBind();
                OfflineInterviewLog_bottomPagingNavigator.Visible = true;

                OfflineInterviewLog_bottomPagingNavigator.TotalRecords = totalRecords;

                OfflineInterviewLog_bottomPagingNavigator.PageSize = base.GridPageSize;

                OfflineInterviewLog_resultsExpandLinkButton.Visible = false;
                base.ShowMessage(OfflineInterviewLog_topErrorMessageLabel,
                    OfflineInterviewLog_bottomErrorLabel, "No data found to display");
            }
        }
        
        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }

        #endregion Protected Overridden Methods

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the log file availability status. This
        /// helps to show or hide the view log file icon.
        /// </summary>
        /// <param name="logFileID">
        /// A <see cref="string"/> that holds the log file ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of log file 
        /// availability status. True represents available and false not 
        /// available.
        /// </returns>
        /// <remarks>
        /// The applicable return values are:
        /// 1. True - Available.
        /// 2. False - Not available.
        /// </remarks>
        protected bool IsLogFileAvailable(string logFileID)
        {
            return (logFileID.Trim() == "0" ? false : true);
        }

        /// <summary>
        /// Method that retrieves the image availability status. This
        /// helps to show or hide show/hide desktop and webcam image icon.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds status as string.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of given status. 
        /// True represents available and false not available.
        /// </returns>
        /// <remarks>
        /// The applicable return values are:
        /// 1. True - Available.
        /// 2. False - Not available.
        /// </remarks>
        protected bool IsImageAvailable(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }
        #endregion Protected Methods
    }
}