﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    CodeBehind="FeatureManagement.aspx.cs" Inherits="Forte.HCM.UI.Admin.FeatureManagement" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="FeatureManagement_Content" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
     <script language="javascript" type="text/javascript">


         function disableEnterKey(e) {
             var key;
             if (window.event)
                 key = window.event.keyCode;     //IE          
             else
                 key = e.which;     //firefox          
             if (key == 13)
                 return false;
             else
                 return true;
         } 

    </script>
    <asp:UpdatePanel ID="FeatureManagement_updatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                    <asp:Literal ID="FeatureManagement_headerLiteral" runat="server" Text="Feature Management"></asp:Literal>
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="FeatureManagement_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="FeatureManagement_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="FeatureManagement_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="FeatureManagement_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="FeatureManagement_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="TAB_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr id="FeatureManagement_expandAllTR" runat="server">
                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                        <asp:Label ID="FeatureManagement_featuresHeader" runat="server" Text="Features"> </asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <div id="FeatureManagement_featureSearchDIV" runat="server">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td colspan="2">

                                                                            <div id="FeatureManagement_featureGridViewDIV" style="height: 320px; display: block;
                                                                                overflow: auto;" runat="server" onkeypress="javascript:return disableEnterKey(event);">
                                                                                <asp:GridView ID="FeatureManagement_featureGridView" runat="server" AllowSorting="True"
                                                                                    AutoGenerateColumns="False" OnRowCancelingEdit="FeatureManagement_featureGridView_RowCancelingEdit"
                                                                                    OnRowCommand="FeatureManagement_featureGridView_RowCommand" OnRowCreated="FeatureManagement_featureGridView_RowCreated"
                                                                                    OnRowEditing="FeatureManagement_featureGridView_RowEditing" OnRowUpdating="FeatureManagement_featureGridView_RowUpdating"
                                                                                    OnSorting="FeatureManagement_featureGridView_Sorting">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderStyle-Width="12%" ItemStyle-Width="12%">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="FeatureManagement_featureGridView_editImageButton" runat="server"
                                                                                                    SkinID="sknEditVectorImageButton" ToolTip="Edit Feature" CommandArgument='<%# Eval("FeatureID") %>'
                                                                                                    CommandName="Edit" />
                                                                                                <asp:ImageButton ID="FeatureManagement_featureGridView_UpdateImageButton" runat="server"
                                                                                                    SkinID="sknEditSaveVectorImageButton" ToolTip="Update Feature" ValidationGroup="b"
                                                                                                    CommandArgument='<%# Eval("FeatureID") %>' CommandName="Update" Visible="false" />
                                                                                                <asp:ImageButton ID="FeatureManagement_featureGridView_deleteImageButton" runat="server"
                                                                                                    SkinID="sknDeleteImageButton" ToolTip="Delete Feature" CommandArgument='<%# Eval("FeatureID") %>'
                                                                                                    CommandName="DeleteFeature" />
                                                                                                <asp:ImageButton ID="FeatureManagement_featureGridView_cancelUpdateImageButton" runat="server"
                                                                                                    SkinID="sknDeleteVectorImageButton" ToolTip="Cancel Update" CommandArgument='<%# Eval("FeatureID") %>'
                                                                                                    CommandName="Cancel" Visible="false" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Module" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="25%" SortExpression="MODULE">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="FeatureManagement_moduleNameLabel" runat="server" Text='<%# Eval("Module") %>'></asp:Label>
                                                                                                <asp:HiddenField ID="FeatureManagement_moduleIDHiddenField" runat="server" Value='<%# Eval("FeatureID") %>' />
                                                                                                <asp:HiddenField ID="FeatureManagement_moduleCodeNameHiddenField" runat="server"
                                                                                                    Value='<%# Eval("Module") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Sub Module" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="25%" SortExpression="SUBMODULE">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="FeatureManagement_submoduleNameLabel" runat="server" Text='<%# Eval("SubModule") %>'></asp:Label>
                                                                                                <asp:HiddenField ID="FeatureManagement_submoduleIDHiddenField" runat="server" Value='<%# Eval("FeatureID") %>' />
                                                                                                <asp:HiddenField ID="FeatureManagement_subModuleCodeNameHiddenField" runat="server"
                                                                                                    Value='<%# Eval("SubModule") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Feature Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="35%" SortExpression="FEATURENAME">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="FeatureManagement_featuerGridView_modulenameLabel" runat="server"
                                                                                                    Text='<%# Eval("FeatureName") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:HiddenField ID="FeatureManagement_featureNameIDHiddenField" runat="server" Value='<%# Eval("FeatureID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:HiddenField ID="FeatureManagement_editfeaturenameIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("FeatureID") %>' />
                                                                                                <asp:TextBox ID="FeatureManagement_moduleGridView_editFeatureNameTextBox" runat="server"
                                                                                                    Width="75%" AutoCompleteType="None" Text='<%# Eval("FeatureName") %>' MaxLength="50"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="FeatureManagement_featureNameRequiredFieldValidator"
                                                                                                    runat="server" ControlToValidate="FeatureManagement_moduleGridView_editFeatureNameTextBox"
                                                                                                    ErrorMessage="Enter feature name" ValidationGroup="b" Display="None" SetFocusOnError="true"
                                                                                                    ForeColor="White">
                                                                                                </asp:RequiredFieldValidator>
                                                                                                <ajaxToolKit:ValidatorCalloutExtender ID="FeatureManagement_featureNameValidatorCalloutExtender"
                                                                                                    runat="server" TargetControlID="FeatureManagement_featureNameRequiredFieldValidator"
                                                                                                    PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                <asp:Label ID="FeatureManagement_featureNameHiddenLabel" runat="server" Visible="false"
                                                                                                    Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Feature URL" HeaderStyle-Width="55%" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-Width="55%" SortExpression="FEATUREURL">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="FeatureManagement_featuerURLGridView_modulenameLabel" runat="server"
                                                                                                    Text='<%# Eval("FeatureURL") %>'>
                                                                                                </asp:Label>
                                                                                                <asp:HiddenField ID="FeatureManagement_featureURLNameIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("FeatureID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:HiddenField ID="FeatureManagement_editfeaturenameURLIDHiddenField" runat="server"
                                                                                                    Value='<%# Eval("FeatureID") %>' />
                                                                                                <asp:TextBox ID="FeatureManagement_moduleGridView_editFeatureURLTextBox" runat="server"
                                                                                                    Width="100%" AutoCompleteType="None" Text='<%# Eval("FeatureURL") %>' MaxLength="50"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="FeatureManagement_featureURLRequiredFieldValidator"
                                                                                                    runat="server" ControlToValidate="FeatureManagement_moduleGridView_editFeatureURLTextBox"
                                                                                                    ErrorMessage="Enter feature URL" ValidationGroup="b" Display="None" SetFocusOnError="true"
                                                                                                    ForeColor="White">
                                                                                                </asp:RequiredFieldValidator>
                                                                                                <ajaxToolKit:ValidatorCalloutExtender ID="FeatureManagement_featureURLValidatorCalloutExtender"
                                                                                                    runat="server" TargetControlID="FeatureManagement_featureURLRequiredFieldValidator"
                                                                                                    PopupPosition="Left" CssClass="customCalloutStyle">
                                                                                                </ajaxToolKit:ValidatorCalloutExtender>
                                                                                                <asp:Label ID="FeatureManagement_featureURLHiddenLabel" runat="server" Visible="false"
                                                                                                    Text='<%# Eval("FeatureURL") %>'></asp:Label>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="FeatureManagement_featureAddImageButton" runat="server" SkinID="sknAddLinkButton"
                                                                                Text="Add" OnClick="FeatureManagement_featureAddImageButton_Click">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td align="right">
                                                                            <input type="hidden" runat="server" id="FeatureManagement_featurePageNumberHidden"
                                                                                value="1" />
                                                                            <uc1:PageNavigator ID="FeatureManagement_featurePageNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" class="header_text_bold">
                                                &nbsp;
                                            </td>
                                            <td style="width: 50%">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="FeatureManagement_resetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="FeatureManagement_resetLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                        <td align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="FeatureManagement_cancelLinkButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="FeatureManagement_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="FeatureManagement_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="display: none">
                            <asp:Button ID="FeatureManagement_deleteFeatureHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="FeatureManagement_deleteFeaturePopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove" DefaultButton="" >
                            <uc2:ConfirmMsgControl ID="FeatureManagement_deleteFeatureConfirmMsgControl" runat="server"
                                OnOkClick="FeatureManagement_deleteFeatureConfirmMsgControl_okClick" OnCancelClick="FeatureManagement_deleteFeatureConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="FeatureManagement_deleteHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="FeatureManagement_deleteFeatureModalPopupExtender"
                            runat="server" PopupControlID="FeatureManagement_deleteFeaturePopupPanel" TargetControlID="FeatureManagement_deleteFeatureHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="FeatureManagement_updateFeaturePanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_addfeature" DefaultButton="FeatureManagement_updateFeatureNameSaveButton">
                            <div style="display: none;">
                                <asp:Button ID="FeatureManagement_updateFeatureshiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="FeatureManagement_updateFeatureLiteral" runat="server" Text="Edit Feature"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="FeatureManagement_updateFeatureTopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="FeatureManagement_updateFeatureNameErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="FeatureManagement_updateModuleCodeLabel" runat="server" Text="Module"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="FeatureManagement_updateModuleDropdownList" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="FeatureManagement_updateModuleNameLabel" runat="server" Text="Sub Module"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="FeatureManagement_updateSubModuleDropdownList" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="FeatureManagement_updateFeatureName" runat="server" Text="Feature Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="FeatureManagement_updateFeatureNameTextBox" runat="server" Width="160px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="FeatureManagement_updateFeatureURL" runat="server" Text="Feature URL"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="FeatureManagement_updateFeatureURLTextBox" runat="server" Width="165px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:HiddenField ID="FeatureManagement_updateFeatureIsEditORSaveHiddenField" runat="server" />
                                                    <asp:Button ID="FeatureManagement_updateFeatureNameSaveButton" OnClick="FeatureManagement_updateFeatureNameSaveButton_Clicked"
                                                        runat="server" SkinID="sknButtonId" Text="Save" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="FeatureManagement_updateFeatureNameCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="FeatureManagement_updateFeaureeModalPopupExtender"
                            runat="server" PopupControlID="FeatureManagement_updateFeaturePanel" TargetControlID="FeatureManagement_updateFeatureshiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
