﻿#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    public partial class CorporateSubscriptionManagement : PageBase
    {

        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "400px";

        #endregion Private Constants

        #region Event Handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus.
                Page.Form.DefaultButton = CorporateSubscriptionManagement_topSearchButton.UniqueID;
                CorporateSubscriptionManagement_nameTextBox.Focus();

                // Subscribe to paging events.
                CorporateSubscriptionManagement_pageNavigator.PageNumberClick +=
                        new CommonControls.PageNavigator.PageNumberClickEventHandler
                            (CorporateSubscriptionManagement_pageNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    //Load the default values 
                    LoadValues();

                    //Set the page caption
                    Master.SetPageCaption("Corporate Subscription Management");

                    //Assign the sort order and sort field in the view state
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "NAME";

                    CorporateSubscriptionManagement_searchTR.Attributes.Add("onclick",
                      "ExpandOrRestore('" +
                      CorporateSubscriptionManagement_usersDiv.ClientID + "','" +
                      CorporateSubscriptionManagement_searchDiv.ClientID + "','" +
                      CorporateSubscriptionManagement_searchResultsUpSpan.ClientID + "','" +
                      CorporateSubscriptionManagement_searchResultsDownSpan.ClientID + "','" +
                      CorporateSubscriptionManagement_restoreHiddenField.ClientID + "','" +
                      RESTORED_HEIGHT + "','" +
                      EXPANDED_HEIGHT + "')");
                }

                CorporateSubscriptionManagement_topErrorMessageLabel.Text = string.Empty;
                CorporateSubscriptionManagement_bottomErrorLabel.Text = string.Empty;
                CorporateSubscriptionManagement_bottomSuccessLabel.Text = string.Empty;
                CorporateSubscriptionManagement_topSuccessMessageLabel.Text = string.Empty;
            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);

                Logger.ExceptionLog(exception);

            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the CorporateSubscriptionManagement_pageNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Forte.HCM.EventSupport.PageNumberEventArgs"/>
        /// instance containing the event data.</param>
        void CorporateSubscriptionManagement_pageNavigator_PageNumberClick(object sender,
            EventSupport.PageNumberEventArgs e)
        {
            try
            {

                //ViewState["SORT_ORDER"] = SortType.Ascending;
                //ViewState["SORT_FIELD"] = "NAME";
                GetCorporateSubscription(e.PageNumber);
                CorporateSubscriptionManagement_pageNavigator.Reset();

            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);

                Logger.ExceptionLog(exception);

            }
        }

        /// <summary>
        /// Handles the Click event of the CorporateSubscriptionManagement_topSearchButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Reset the page navigator
                CorporateSubscriptionManagement_pageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";
                GetCorporateSubscription(1);
            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);

                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Sorting event of the CorporateSubscriptionManagement_usersGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/>
        /// instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_usersGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Assign the sorting and sort order
            if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
            {
                ViewState["SORT_ORDER"] =
                    ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                    SortType.Descending : SortType.Ascending;
            }
            else
                ViewState["SORT_ORDER"] = SortType.Ascending;

            ViewState["SORT_FIELD"] = e.SortExpression;
            CorporateSubscriptionManagement_pageNavigator.Reset();
            GetCorporateSubscription(1);
        }


        /// <summary>
        /// Handles the RowCommand event of the CorporateSubscriptionManagement_usersGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/>
        /// instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_usersGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "DeleteUser":
                        CorporateSubscriptionManagement_deleteUserModalPopupExtender.Show();
                        CorporateSubscriptionManagement_deleteUserConfirmMsgControl.Message =
                            Resources.HCMResource.CorporateSubscriptionManagement_AreYouSureToDeleteThisUser;
                        CorporateSubscriptionManagement_deleteUserConfirmMsgControl.Type = MessageBoxType.YesNo;
                        CorporateSubscriptionManagement_deleteUserConfirmMsgControl.Title = "Warning";
                        CorporateSubscriptionManagement_deleteUserHiddenField.Value = e.CommandArgument.ToString();
                        break;

                    case "ActivateUser":
                        CorporateSubscriptionManagement_activateUserConfirmMsgControl.Type = MessageBoxType.YesNo;
                        CorporateSubscriptionManagement_activateUserConfirmMsgControl.Title = "Warning";
                        CorporateSubscriptionManagement_activateUserHiddenField.Value = e.CommandArgument.ToString();
                        CorporateSubscriptionManagement_activateStatusHiddenField.Value = "1";
                        CorporateSubscriptionManagement_activateUserConfirmMsgControl.Message =
                            Resources.HCMResource.CorporateSubscriptionManagement_AreYouSureToActivateThisUser;
                        CorporateSubscriptionManagement_activateUserModalPopupExtender.Show();
                        break;

                    case "DeactivateUser":
                        CorporateSubscriptionManagement_activateUserConfirmMsgControl.Message =
                            Resources.HCMResource.CorporateSubscriptionManagement_AreYouSureToDeactivateThisUser;
                        CorporateSubscriptionManagement_activateUserConfirmMsgControl.Type = MessageBoxType.YesNo;
                        CorporateSubscriptionManagement_activateUserConfirmMsgControl.Title = "Warning";
                        CorporateSubscriptionManagement_activateUserHiddenField.Value = e.CommandArgument.ToString();
                        CorporateSubscriptionManagement_activateStatusHiddenField.Value = "0";
                        CorporateSubscriptionManagement_activateUserModalPopupExtender.Show();
                        break;

                    case "ViewUser":
                        UserRegistrationInfo userInfo = new AdminBLManager().GetSubscriptionUserDetails(
                            Convert.ToInt16(e.CommandArgument.ToString()));
                        CorporateSubscriptionManagement_previewUserNameLabel.Text = userInfo.LastName;
                        CorporateSubscriptionManagement_previewUserCompanyNameLabel.Text = userInfo.Company;
                        CorporateSubscriptionManagement_previewUserCountryNameLabel.Text = userInfo.Country;
                        CorporateSubscriptionManagement_previewUserIsActiveCheckbox.Checked = false;
                        if (userInfo.IsActive == 1)
                            CorporateSubscriptionManagement_previewUserIsActiveCheckbox.Checked = true;

                        CorporateSubscriptionManagement_previewUserModalPopupExtender.Show();
                        break;
                    case "EditUser":

                        Response.Redirect("~/Admin/CorporateAdminEdit.aspx?m=1&s=0" +
                        "&userID=" + Convert.ToInt16(e.CommandArgument.ToString()) +
                        "&parentpage=COR_ADMIN", false);
                        /*UserRegistrationInfo userEditInfo = new AdminBLManager().GetSubscriptionUserDetails(
                           Convert.ToInt16(e.CommandArgument.ToString()));
                        CorporateSubscriptionManagement_editUserTextbox.Text = userEditInfo.FirstName;
                        CorporateSubscriptionManagement_editUserLastnameTextbox.Text = userEditInfo.LastName;
                        CorporateSubscriptionManagement_editUserPhoneNumberTextbox.Text = userEditInfo.Phone;
                        CorporateSubscriptionManagement_editUserCompanyTextbox.Text = userEditInfo.Company;
                        CorporateSubscriptionManagement_editUserTitleTextBox.Text = userEditInfo.Title;
                        CorporateSubscriptionManagement_editUserNoOfUsersTextBox.Text = userEditInfo.NoOfUsers.ToString();
                        CorporateSubscriptionManagement_usersGridView_noOfUsersHiddenField.Value = userEditInfo.NoOfUsers.ToString();
                        CorporateSubscriptionManagement_editUserIsActiveCheckBox.Checked = false;
                        if (userEditInfo.IsActive == 1)
                            CorporateSubscriptionManagement_editUserIsActiveCheckBox.Checked = true;
                        CorporateSubscriptionManagement_editUserHiddenField.Value = e.CommandArgument.ToString();
                        CorporateSubscriptionManagement_editUserLabel.Text = null;
                        BindSelectedRoles(Convert.ToInt16(e.CommandArgument.ToString()));
                        CorporateSubscriptionManagement_editUserModalPopupExtender.Show();*/
                        break;
                }

                // Reload the records again.

            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }

        }

        private void BindSelectedRoles(short userID)
        {

            List<Roles> userEditInfo = new AdminBLManager().GetCorporateAdminAssignedRoles(userID);
            CorporateSubscriptionManagement_editUserRolesCheckBoxList.DataSource = userEditInfo;
            CorporateSubscriptionManagement_editUserRolesCheckBoxList.DataBind();

            for (int i = 0; i < userEditInfo.Count; i++)
            {
                if (userEditInfo[i].IsSelected == "Y")
                {
                    CorporateSubscriptionManagement_editUserRolesCheckBoxList.Items[i].Enabled = false;
                    CorporateSubscriptionManagement_editUserRolesCheckBoxList.Items[i].Selected = true;
                }
            }
        }
        protected void CorporateSubscriptionManagement_editUserButton_Clicked
     (object sender, EventArgs e)
        {
            CorporateSubscriptionManagement_editUserLabel.Text = null;


            if (CorporateSubscriptionManagement_editUserTextbox.Text.Length == 0)
            {
                base.ShowMessage(CorporateSubscriptionManagement_editUserLabel,
                    "Enter user name");

                CorporateSubscriptionManagement_editUserModalPopupExtender.Show();

                return;
            }
            if (CorporateSubscriptionManagement_editUserCompanyTextbox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CorporateSubscriptionManagement_editUserLabel,
                    "Enter company name");

                CorporateSubscriptionManagement_editUserModalPopupExtender.Show();

                return;
            }
            

            int id = int.Parse(CorporateSubscriptionManagement_editUserHiddenField.Value);
            UserRegistrationInfo userEditInfo = new UserRegistrationInfo();
            userEditInfo.UserID = id;
            userEditInfo.FirstName = CorporateSubscriptionManagement_editUserTextbox.Text;
            userEditInfo.LastName = CorporateSubscriptionManagement_editUserLastnameTextbox.Text;
            userEditInfo.Phone = CorporateSubscriptionManagement_editUserPhoneNumberTextbox.Text;
            userEditInfo.Company = CorporateSubscriptionManagement_editUserCompanyTextbox.Text;
            userEditInfo.Title = CorporateSubscriptionManagement_editUserTitleTextBox.Text;
            if (CorporateSubscriptionManagement_editUserNoOfUsersTextBox.Text != null)
            {
                if (Convert.ToInt16(CorporateSubscriptionManagement_editUserNoOfUsersTextBox.Text)<
                    Convert.ToInt16(CorporateSubscriptionManagement_usersGridView_noOfUsersHiddenField.Value) )
                    
                {
                    base.ShowMessage(CorporateSubscriptionManagement_editUserLabel,
                        "Number of user cannot be decreased below  " 
                        + CorporateSubscriptionManagement_usersGridView_noOfUsersHiddenField.Value);
                    CorporateSubscriptionManagement_editUserModalPopupExtender.Show();
                    return;
                   
                }
                else
                userEditInfo.NoOfUsers = Convert.ToInt16(CorporateSubscriptionManagement_editUserNoOfUsersTextBox.Text);
            }
            for (int j = 0; j < CorporateSubscriptionManagement_editUserRolesCheckBoxList.Items.Count; j++)
            {
                if ((CorporateSubscriptionManagement_editUserRolesCheckBoxList.Items[j].Selected==true)&&
                    (CorporateSubscriptionManagement_editUserRolesCheckBoxList.Items[j].Enabled==true))
                {
                    userEditInfo.SelectedRoles = userEditInfo.SelectedRoles + CorporateSubscriptionManagement_editUserRolesCheckBoxList.Items[j].Value + ",";
                    
                }
            }
            userEditInfo.SelectedRoles = userEditInfo.SelectedRoles.TrimEnd(',');
            if (CorporateSubscriptionManagement_editUserIsActiveCheckBox.Checked == true)
                userEditInfo.IsActive = 1;
            else
                userEditInfo.IsActive = 0;

            new UserRegistrationBLManager().UpdateUserInfo(userEditInfo);

            base.ShowMessage(CorporateSubscriptionManagement_topSuccessMessageLabel,
            CorporateSubscriptionManagement_bottomSuccessLabel,
               "User information has been updated successfully");

            CorporateSubscriptionManagement_pageNavigator.Reset();
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD"] = "NAME";
            GetCorporateSubscription(1);
        }
        /// <summary>
        /// Handles the Click event of the CorporateSubscriptionManagement_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }
        }

        /// <summary>
        /// Handles the OKClick event of the CorporateSubscriptionManagement_deleteUserConfirmMsgControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_deleteUserConfirmMsgControl_OKClick(object sender, EventArgs e)
        {
            try
            {
                if (CorporateSubscriptionManagement_deleteUserHiddenField.Value != null
                    && CorporateSubscriptionManagement_deleteUserHiddenField.Value.Trim() != string.Empty)
                {
                    int userID = int.Parse(CorporateSubscriptionManagement_deleteUserHiddenField.Value);
                    //Delete the user
                    new UserRegistrationBLManager().DeleteTenantUser(userID);

                    base.ShowMessage(CorporateSubscriptionManagement_topSuccessMessageLabel, CorporateSubscriptionManagement_bottomSuccessLabel,
                        HCMResource.CorporateUserManagement_DeleteUserSuccessfull);

                    //Rebind the grid view
                    CorporateSubscriptionManagement_pageNavigator.Reset();
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "NAME";
                    GetCorporateSubscription(1);

                }

            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }
        }

        /// <summary>
        /// Handles the CancelClick event of the CorporateSubscriptionManagement_deleteUserConfirmMsgControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_deleteUserConfirmMsgControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                CorporateSubscriptionManagement_deleteUserHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }

        }

        /// <summary>
        /// Handles the OKClick event of the CorporateSubscriptionManagement_activateUserConfirmMsgControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_activateUserConfirmMsgControl_OKClick(object sender, EventArgs e)
        {
            try
            {
                if (CorporateSubscriptionManagement_activateUserHiddenField.Value != null
                    && CorporateSubscriptionManagement_activateUserHiddenField.Value.Trim() != string.Empty)
                {
                    int userID = int.Parse(CorporateSubscriptionManagement_activateUserHiddenField.Value);
                    Int16 activateID = Int16.Parse(CorporateSubscriptionManagement_activateStatusHiddenField.Value);

                    // Activate/deactivate the user.
                    new AdminBLManager().ActivateUsers(userID, activateID, base.userID);

                    base.ShowMessage(CorporateSubscriptionManagement_topSuccessMessageLabel,
                        activateID == 1 ? "User activated successfully" : "User deactivated successfully");

                    // Rebind the grid view.
                    CorporateSubscriptionManagement_pageNavigator.Reset();

                    try
                    {

                        // Send mail to user on activation and deactivation.
                        if (activateID == 1)
                            new EmailHandler().SendMail(EntityType.UserActivated, userID.ToString());
                        else
                            new EmailHandler().SendMail(EntityType.UserDeactivated, userID.ToString());
                    }
                    catch (Exception exp)
                    {
                        base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                            CorporateSubscriptionManagement_bottomErrorLabel, exp.Message);
                    }

                    if (ViewState["PAGE_NUMBER"] != null)
                    {
                        GetCorporateSubscription(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
                        CorporateSubscriptionManagement_pageNavigator.MoveToPage(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
                    }
                    else
                    {
                        GetCorporateSubscription(1);
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handles the CancelClick event of the 
        /// CorporateSubscriptionManagement_activateUserConfirmMsgControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_activateUserConfirmMsgControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                CorporateSubscriptionManagement_activateUserHiddenField.Value = string.Empty;
                CorporateSubscriptionManagement_activateStatusHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }

        }
        /// <summary>
        /// Handles the RowCreated event of the CorporateSubscriptionManagement_usersGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> 
        /// instance containing the event data.</param>
        protected void CorporateSubscriptionManagement_usersGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (CorporateSubscriptionManagement_usersGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }
        }

        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// Represents the method to get the coporate subscription details
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pageNumber
        /// </param>
        private void GetCorporateSubscription(int pageNumber)
        {
            ViewState["PAGE_NUMBER"] = pageNumber;

            UserRegistrationInfo user = new UserRegistrationInfo();

            user.FirstName = CorporateSubscriptionManagement_nameTextBox.Text.Trim();

            user.Title = CorporateSubscriptionManagement_titleTextBox.Text.Trim();

            user.Company = CorporateSubscriptionManagement_companyTextBox.Text.Trim();

            user.Country = CorporateSubscriptionManagement_countryTextBox.Text.Trim();

            user.IsActive = Convert.ToInt16(CorporateSubscriptionManagement_activeStatusDropDownList.SelectedValue);

            foreach (ListItem item in CorporateSubscriptionManagement_businessTypeCheckBoxList.Items)
            {
                if (item.Selected)
                {
                    user.BussinessTypesIds += item.Value + ",";
                }
            }
            if (!string.IsNullOrEmpty(user.BussinessTypesIds))
                user.BussinessTypesIds = user.BussinessTypesIds.TrimEnd(',');

            int totalPage = 0;

            List<UserRegistrationInfo> userInfo = new AdminBLManager().GetSubscriptionDetails(user, pageNumber,
                base.GridPageSize, out totalPage, ViewState["SORT_FIELD"].ToString(), ViewState["SORT_ORDER"].ToString());

            if (userInfo != null && userInfo.Count != 0)
            {
                CorporateSubscriptionManagement_usersDiv.Style["display"] = "block";
                CorporateSubscriptionManagement_usersGridView.DataSource = userInfo;
                CorporateSubscriptionManagement_usersGridView.DataBind();
                CorporateSubscriptionManagement_pageNavigator.TotalRecords = totalPage;
                CorporateSubscriptionManagement_pageNavigator.PageSize = base.GridPageSize;
            }

            else
            {
                CorporateSubscriptionManagement_usersDiv.Style["display"] = "none";
                base.ShowMessage(CorporateSubscriptionManagement_topErrorMessageLabel,
                    CorporateSubscriptionManagement_bottomErrorLabel,
                    Resources.HCMResource.Common_Empty_Grid);
                CorporateSubscriptionManagement_pageNavigator.TotalRecords = 0;
            }
        }


        /// <summary>
        /// Represents the method to get the string value
        /// </summary>
        /// <param name="value">
        /// A<see cref="int"/>that holds the value
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the string
        /// </returns>
        protected string GetValue(int value)
        {
            return value == 1 ? "Yes" : "No";
        }


        /// <summary>
        /// Gets the active icon.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// A<see cref="bool"/> that holds the corresponding bool value
        /// </returns>
        protected bool GetActiveIcon(int value)
        {
            return value == 0 ? true : false;
        }


        /// <summary>
        /// Gets the in active icon.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// A<see cref="bool"/> that holds the corresponding bool value
        /// </returns>
        protected bool GetInActiveIcon(int value)
        {
            return value == 1 ? true : false;
        }


        #endregion Private Methods

        #region Protected Overridden Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            int total = 0;

            CorporateSubscriptionManagement_businessTypeCheckBoxList.DataSource =
                new AdminBLManager().GetBusinessTypes(string.Empty, "NAME", "A", 1, null, out total);

            CorporateSubscriptionManagement_businessTypeCheckBoxList.DataBind();
        }

        #endregion Protected Overridden Methods
       
}
}
