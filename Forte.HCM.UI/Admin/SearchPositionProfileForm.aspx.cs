﻿#region Directives                                                   
using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Represents the class that holds the coding for the search 
    /// position profile form 
    /// </summary>
    public partial class SearchPositionProfileForm : PageBase
    {
        #region Private Constants                                    

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "310px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "400px";

        #endregion Private Constants

        #region Event Handler                                        
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Check and expand or restore
                CheckAndSetExpandOrRestore();

                //Assign the page number click event
                SearchPostionProfileForm_PageNavigator.PageNumberClick += new PageNavigator.
                    PageNumberClickEventHandler(SearchPostionProfileForm_PageNavigator_PageNumberClick);

                // Add handler to 'designed by' image button.
                SearchPositionProfileForm_designedByImageButton.Attributes.Add("onclick",
                     "javascript:return LoadUserForPositionProfileModule('"
                    + SearchPositionProfileForm_designedBydummyAuthorIdHidenField.ClientID + "','"
                    + SearchPositionProfileForm_designedByHiddenField.ClientID + "','"
                    + SearchPositionProfileForm_designedByTextBox.ClientID + "')");

                //Page.SetFocus(SearchPostionProfileForm_topSearchButton);
                Page.Form.DefaultButton = SearchPostionProfileForm_topSearchButton.UniqueID;

                if (!IsPostBack)
                {
                    //Assign the sort order and sort field in the view state
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "FORM_NAME";

                    //Clear the session for the preview popup
                    Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = null;

                    Page.Form.DefaultFocus = SearchPositionProfileForm_formNameTextBox.UniqueID;
                    SearchPositionProfileForm_formNameTextBox.Focus();

                    //Assign the on click attributes for the expand or restore button 
                    SearchPostionProfileForm_expandAllTR.Attributes.Add("onclick",
                      "ExpandOrRestore('" +
                      SearchPostionProfileForm_positionProfileSearchDiv.ClientID + "','" +
                      SearchPositionProfileForm_searchDiv.ClientID + "','" +
                      SearchPostionProfileForm_searchResultsUpSpan.ClientID + "','" +
                      SearchPostionProfileForm_searchResultsDownSpan.ClientID + "','" +
                      SearchPostionProfileForm_restoreHiddenField.ClientID + "','" +
                      RESTORED_HEIGHT + "','" +
                      EXPANDED_HEIGHT + "')");

                    Master.SetPageCaption("Search Form");

                    //Set the default userId 
                    SearchPositionProfileForm_designedByHiddenField.Value = base.userID.ToString();
                    SearchPositionProfileForm_designedByTextBox.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;

                    // Check if the page is launched from menu. If so, clear the existing
                    // session variables.
                    if ((!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                    }

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE_FORM] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE_FORM]
                            as FormDetail);

                    //Check and expand or restore
                    CheckAndSetExpandOrRestore();
                }
                //Empty all the message label
                SearchPositionProfileForm_topErrorMessageLabel.Text = string.Empty;
                SearchPositionProfileForm_topSuccessMessageLabel.Text = string.Empty;
                SearchPositionProfileForm_bottomErrorMessageLabel.Text = string.Empty;
                SearchPositionProfileForm_bottomSuccessMessageLabel.Text = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                    SearchPositionProfileForm_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the SearchPostionProfileForm_PageNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Forte.HCM.EventSupport.PageNumberEventArgs"/> 
        /// instance containing the event data.</param>
        void SearchPostionProfileForm_PageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            //Store the page number in view state
            ViewState["PAGE_NUMBER"] = e.PageNumber;
            //Get the form details and assign data for grid view
            GetFormDetails(e.PageNumber);
        }

        /// <summary>
        /// Handles the Click event of the SearchPostionProfileForm_topSearchButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void SearchPostionProfileForm_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Reset the page navigator
                SearchPostionProfileForm_PageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "FORM_NAME";
                ViewState["PAGE_NUMBER"] = 1;

                //Get the form details
                GetFormDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                    SearchPositionProfileForm_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCreated event of the SearchPostionProfileForm_positionProfileGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void SearchPostionProfileForm_positionProfileGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (SearchPostionProfileForm_positionProfileGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                SearchPositionProfileForm_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Sorting event of the SearchPostionProfileForm_positionProfileGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> 
        /// instance containing the event data.</param>
        protected void SearchPostionProfileForm_positionProfileGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                SearchPostionProfileForm_PageNavigator.Reset();
                GetFormDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                SearchPositionProfileForm_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the SearchPositionProfileForm_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SearchPositionProfileForm_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                SearchPositionProfileForm_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the SearchPostionProfileForm_positionProfileGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void SearchPostionProfileForm_positionProfileGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "editForm":
                        //If the the command is edit form , transfer the control to PositionProfileFormEntry.aspx
                        Response.Redirect("~/Admin/PositionProfileFormEntry.aspx?m=2&s=1" +
                          "&mode=edit&form_id=" + e.CommandArgument +
                          "&parentpage=" + Constants.ParentPage.SEARCH_POSITION_PROFILE_FORM, false);
                        break;
                    case "deleteForm":
                        SearchPositionProfileForm_deleteFormHiddenField.Value = e.CommandArgument.ToString();
                        ImageButton sourceButton = (ImageButton)e.CommandSource;

                        GridViewRow row = (GridViewRow)sourceButton.Parent.Parent;

                        HiddenField nameHiddenField = (HiddenField)row.FindControl("SearchPostionProfileForm_positionProfileGridView_formNameHiddenField");
                        SearchPositionProfileForm_deleteFormConfirmMsgControl.Message = string.Format(Resources.HCMResource.
                            SearchPositionProfileForm_AreYouSureWantToDeleteThisForm, nameHiddenField.Value.Trim());

                        SearchPositionProfileForm_deleteFormConfirmMsgControl.Type = MessageBoxType.YesNo;
                        SearchPositionProfileForm_deleteFormConfirmMsgControl.Title = "Warning";
                        SearchPositionProfileForm_deleteFormModalPopupExtender.Show();
                        break;
                    case "createNewForm":
                        Response.Redirect("~/Admin/PositionProfileFormEntry.aspx?m=2&s=1" +
                         "&mode=create&form_id=" + e.CommandArgument +
                         "&parentpage=" + Constants.ParentPage.SEARCH_POSITION_PROFILE_FORM, false);
                        break;
                    case "createPositionProfile":
                        Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0" +
                        "&" + Constants.PositionProfileConstants.FORM_ID_QUERYSTRING + "=" + e.CommandArgument +
                        "&parentpage=" + Constants.ParentPage.SEARCH_POSITION_PROFILE_FORM, false);
                        break;
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                    SearchPositionProfileForm_bottomErrorMessageLabel, exception.Message);
            }
        }


        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void SearchPositionProfileForm_DeleteSegmentConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                if (SearchPositionProfileForm_deleteFormHiddenField.Value.Trim().Length == 0)
                    return;

                int formID = int.Parse(SearchPositionProfileForm_deleteFormHiddenField.Value);

                new AdminBLManager().DeleteForm(formID);

                base.ShowMessage(SearchPositionProfileForm_bottomSuccessMessageLabel,
                    SearchPositionProfileForm_topSuccessMessageLabel,
                    Resources.HCMResource.SearchPositionProfileForm_FormDeletedSuccessfully);

                //Get the form details
                GetFormDetails(int.Parse(ViewState["PAGE_NUMBER"].ToString()));

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                if (exception.Message.Contains("FK_SUBSCRIPTION_OPTION_FORM_DEFAULT_FORM") ||
                    exception.Message.Contains("FK_POSITION_PROFILE_SETTINGS_FORM_DEFAULT_FORM"))
                {
                    base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                    SearchPositionProfileForm_bottomErrorMessageLabel,
                    Resources.HCMResource.SearchPositionProfileForm_CannotDeleteADefaultForm);
                }
                else
                {
                    base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                        SearchPositionProfileForm_bottomErrorMessageLabel, exception.Message);
                }
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the 
        /// SearchPostionProfileForm_positionProfileGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void SearchPostionProfileForm_positionProfileGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                //Add the mouse over attribute
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton viewFormImageButton = (ImageButton)e.Row.FindControl
                    ("SearchPostionProfileForm_positionProfileGridView_viewFormImageButton");

                if (viewFormImageButton != null)
                {
                    viewFormImageButton.Attributes.Add("onclick", "return LoadPreviewForm('','" +
                        viewFormImageButton.CommandArgument + "');");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                    SearchPositionProfileForm_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler

        #region Private Methods                                      

        /// <summary>
        /// Fills the search criteria.
        /// </summary>
        /// <param name="formDetail">The form detail.</param>
        private void FillSearchCriteria(FormDetail formDetail)
        {
            SearchPositionProfileForm_formNameTextBox.Text = formDetail.FormName.Trim();

            SearchPositionProfileForm_designedByHiddenField.Value = formDetail.SearchDesignedBy.ToString();

            SearchPositionProfileForm_designedByTextBox.Text = formDetail.CreatedByName;

            SearchPositionProfileForm_tagTextBox.Text = formDetail.Tags;
            SearchPositionProfileForm_globalCheckBox.Checked = formDetail.IsGlobalForm;

            SearchPostionProfileForm_restoreHiddenField.Value = formDetail.IsMaximized == true ? "Y" : "N";

            ViewState["SORT_FIELD"] = formDetail.SortExpression;

            ViewState["SORT_ORDER"] = formDetail.SortDirection;

            GetFormDetails(formDetail.CurrentPage);

            SearchPostionProfileForm_PageNavigator.MoveToPage(formDetail.CurrentPage);
        }

        /// <summary>
        /// Checks the and set expand or restore.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(SearchPostionProfileForm_restoreHiddenField.Value) &&
                 SearchPostionProfileForm_restoreHiddenField.Value == "Y")
            {
                SearchPositionProfileForm_searchDiv.Style["display"] = "none";
                SearchPostionProfileForm_searchResultsUpSpan.Style["display"] = "block";
                SearchPostionProfileForm_searchResultsDownSpan.Style["display"] = "none";
                SearchPostionProfileForm_positionProfileSearchDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchPositionProfileForm_searchDiv.Style["display"] = "block";
                SearchPostionProfileForm_searchResultsUpSpan.Style["display"] = "none";
                SearchPostionProfileForm_searchResultsDownSpan.Style["display"] = "block";
                SearchPostionProfileForm_positionProfileSearchDiv.Style["height"] = RESTORED_HEIGHT;
            }

            //If the page is redirected from child page expand or restore the grid
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE_FORM]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchPostionProfileForm_restoreHiddenField.Value))
                    ((FormDetail)Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE_FORM]).IsMaximized =
                        SearchPostionProfileForm_restoreHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// Gets the form details.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void GetFormDetails(int pageNumber)
        {
            FormDetail formDetail = new FormDetail();

            formDetail.FormName = SearchPositionProfileForm_formNameTextBox.Text.Trim();

            if (SearchPositionProfileForm_designedByHiddenField.Value.Trim().Length == 0)
            {
                formDetail.SearchDesignedBy = null;
            }
            else
            {
                formDetail.SearchDesignedBy = int.Parse(SearchPositionProfileForm_designedByHiddenField.Value);
            }

            formDetail.CreatedByName = SearchPositionProfileForm_designedByTextBox.Text.Trim();

            formDetail.Tags = SearchPositionProfileForm_tagTextBox.Text.Trim();

            formDetail.IsGlobalForm = SearchPositionProfileForm_globalCheckBox.Checked;
            formDetail.UserID = base.userID;
            formDetail.TenantID = base.tenantID;

            formDetail.CurrentPage = pageNumber;

            formDetail.SortDirection = (SortType)ViewState["SORT_ORDER"];

            formDetail.SortExpression = ViewState["SORT_FIELD"].ToString();

            formDetail.IsMaximized =
                   SearchPostionProfileForm_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

            //Save the search details in session
            Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE_FORM] = formDetail;

            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            int totalRecords = 0;

            List<FormDetail> formDetails = new AdminBLManager().GetPositionProfileForms(formDetail,
                sortOrder, sortExpression, pageNumber, base.GridPageSize, out totalRecords);

            if (formDetails.Count == 0)
            {
                base.ShowMessage(SearchPositionProfileForm_topErrorMessageLabel,
                    SearchPositionProfileForm_bottomErrorMessageLabel,
                    Resources.HCMResource.TestScheduler_NoDataFoundToDisplay);

                SearchPostionProfileForm_positionProfileSearchDiv.Visible = false;

                SearchPostionProfileForm_PageNavigator.Visible = false;

                return;
            }

            SearchPostionProfileForm_positionProfileSearchDiv.Visible = true;

            SearchPostionProfileForm_PageNavigator.Visible = true;

            SearchPostionProfileForm_positionProfileGridView.DataSource = formDetails;
            SearchPostionProfileForm_positionProfileGridView.DataBind();

            SearchPostionProfileForm_PageNavigator.TotalRecords = totalRecords;
            SearchPostionProfileForm_PageNavigator.PageSize = GridPageSize;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods

        #region Protected Methods

        /// <summary>
        /// Method that retrieves the allow edit/delete status.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the allow edit status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of allow edit 
        /// status. True represents allowed and false don't allow.
        /// </returns>
        /// <remarks>
        /// The applicable status for allow edit are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsAllowEdit(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        #endregion Protected Methods
    }
}