﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RoleManagement.aspx.cs
// File that represents the RoleManagement class that defines the user 
// interface layout and functionalities for the RoleManagement page. This 
// page helps in managing the role category and roles. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header   


#region Directives                                                             
using System;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.Trace;

#endregion Directives


namespace Forte.HCM.UI.Admin
{

    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the RoleManagement page. This page helps in managing the role category
    /// and roles. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class RoleManagement : PageBase
    {
        #region Private Constants                                              
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion

        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //Set the page caption in the page
                Master.SetPageCaption("Role Management");

                ClearMessage();

                RoleManagement_rolecategoryPageNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                        (RoleManagement_rolecategoryPageNavigator_PageNumberClick);
                if (!IsPostBack)
                {
                    //Load the default values
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "ROLECODE";
                    ViewState["SORT_FIELD1"] = "ROLECODE";
                    RoleManagement_showhide.Text = "Hide Description";
                    LoadValues();
                    GetRoleDetails(1);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }
      
        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of role category section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryPageNavigator_PageNumberClick
            (object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                ViewState["pagenumber"] = Convert.ToInt32(e.PageNumber);
                RoleManagement_rolecategoryGridView.EditIndex = -1;
                RoleManagement_categoryTypePageNumberHidden.Value = e.PageNumber.ToString();
                GetRoleCategoryDetails(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel edit action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryGridView_RowCancelingEdit
            (object sender, GridViewCancelEditEventArgs e)
        {
            try
            {

                ClearMessage();

                if (RoleManagement_rolecategoryGridView.EditIndex != -1)
                {
                    base.ShowMessage(RoleManagement_topErrorMessageLabel,
                        RoleManagement_bottomErrorLabel, Resources.HCMResource.
                        RoleManagement_PleaseUpdateTheRoleCategroy);
                    return;
                }

                RoleManagement_rolecategoryGridView.EditIndex = -1;
                GetRoleCategoryDetails(Convert.ToInt32(RoleManagement_categoryTypePageNumberHidden.Value));
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_editImageButton")).Visible = true;
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_deleteImageButton")).Visible = true;
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_cancelUpdateImageButton")).Visible = false;
                RoleManagement_deleteHiddenField.Value = "";
                GetRoleDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row editing action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryGridView_RowEditing
            (object sender, GridViewEditEventArgs e)
        {

            ClearMessage();

            RoleManagement_rolecategoryGridView.EditIndex = e.NewEditIndex;
            GetRoleCategoryDetails(Convert.ToInt32(RoleManagement_categoryTypePageNumberHidden.Value));
            ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.NewEditIndex].
                FindControl("RoleManagement_rolecategoryGridView_UpdateImageButton")).Visible = true;
            ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.NewEditIndex].
                FindControl("RoleManagement_rolecategoryGridView_editImageButton")).Visible = false;
            ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.NewEditIndex].
                FindControl("RoleManagement_rolecategoryGridView_deleteImageButton")).Visible = false;
            ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.NewEditIndex].
               FindControl("RoleManagement_rolecategoryGridView_cancelUpdateImageButton")).Visible = true;
            RoleManagement_deleteHiddenField.Value = ((HiddenField)RoleManagement_rolecategoryGridView.Rows[e.NewEditIndex].
                    FindControl("RoleManagement_editrolecategoryIDHiddenField")).Value;
            ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.NewEditIndex].
              FindControl("RoleManagement_rolecategoryGridView_UpdateImageButton")).Focus();

            
            GetRoleDetails(1);
        }

        /// <summary>
        /// Handler method that will be called when the row updating action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                RoleManagement_rolecategoryGridView.EditIndex = -1;

                ClearMessage();

                int rowAffected = 0;

                rowAffected = new AuthenticationBLManager().UpdateRoleCategory
                    (((TextBox)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_businessTypesGridView_editcodeTextBox")).Text,
                    ((TextBox)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_businessTypesGridView_editNameTextBox")).Text,
                    Convert.ToInt32(RoleManagement_deleteHiddenField.Value), base.userID);

                if (rowAffected == 0)
                    base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                   RoleManagement_bottomSuccessLabel, Resources.HCMResource.
                   RoleManagement_RoleCategoryTypeUpdatedSuccessfully);
                else
                    base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                     RoleManagement_bottomSuccessLabel,
                     Resources.HCMResource.RoleManagement_RoleCategoryAlreadyExists);

                GetRoleDetails(1);


            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exp.Message);
            }
            finally
            {
                GetRoleCategoryDetails(Convert.ToInt32(RoleManagement_categoryTypePageNumberHidden.Value));
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_UpdateImageButton")).Visible = false;
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_editImageButton")).Visible = true;
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_deleteImageButton")).Visible = true;
                ((ImageButton)RoleManagement_rolecategoryGridView.Rows[e.RowIndex].
                    FindControl("RoleManagement_rolecategoryGridView_cancelUpdateImageButton")).Visible = false;
                RoleManagement_deleteHiddenField.Value = "";
                GetRoleDetails(1);
            }
        }
        /// <summary>
        /// Handler method that will be called when the delete action with row command
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ClearMessage();

                if (RoleManagement_rolecategoryGridView.EditIndex != -1)
                {
                    base.ShowMessage(RoleManagement_topErrorMessageLabel,
                        RoleManagement_bottomErrorLabel, Resources.HCMResource.
                        RoleManagement_PleaseUpdateTheRoleCategroy);
                    return;
                }

                
                if (e.CommandName != "DeleteRoleName")
                    return;

                RoleManagement_deleteHiddenField.Value = e.CommandArgument.ToString();

                string result = string.Empty;

                result = new AuthenticationBLManager().DeleteRoleCategoryResult(
                    ((HiddenField)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("RoleManagement_rolecategoryIDHiddenField")).Value);

                if (result != "SUCCESS")
                {


                    RoleManagement_topSuccessMessageLabel.ForeColor = Color.Red;
                    RoleManagement_bottomSuccessLabel.ForeColor = Color.Red;
                    base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                    RoleManagement_bottomSuccessLabel,
                    string.Format(Resources.HCMResource.RoleManagement_RoleCategoryCannotBeDeleted,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("RoleManagement_rolecategoryGridView_rolecategotycodeLabel")).Text));
                    return;
                }

                RoleManagement_deleteBusinessConfirmMsgControl.Message = string.Format
                    (Resources.HCMResource.RoleManagement_RoleCategoryDeleteConfirmation,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("RoleManagement_rolecategoryGridView_rolecategotycodeLabel")).Text + '-' +
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("RoleManagement_rolecategoryGridView_rolecategotynameLabel")).Text);

                RoleManagement_deleteBusinessConfirmMsgControl.Type = MessageBoxType.YesNo;
                RoleManagement_deleteBusinessConfirmMsgControl.Title = "Warning";
                RoleManagement_deleteFormModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the OK button is clicked
        /// in the deleting the role.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagemen_deleteRolesConfirmMsgControl_okClick
        (object sender, EventArgs e)
        {
            try
            {
                int roleID = int.Parse(RoleManagement_deleteRoleHiddenField.Value);

                new AuthenticationBLManager().DeleteRoles(roleID);

                //Reset the page navigator
                //  RoleManagement_rolesPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";

                //Get the update business details
                GetRoleDetails(1);
                base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                    RoleManagement_bottomSuccessLabel, Resources.HCMResource.
                       RoleManagement_RoleDeletedSuccessfully);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the OK button is clicked
        /// in the deleting the role category.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagemen_deleteBusinessConfirmMsgControl_okClick
          (object sender, EventArgs e)
        {
            try
            {
                int roleID = int.Parse(RoleManagement_deleteHiddenField.Value);


                new AuthenticationBLManager().DeleteRoleCategory(roleID);

                //Reset the page navigator
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";

                //Get the update business details
                GetRoleCategoryDetails(1);
                GetRoleDetails(1);
                base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                    RoleManagement_bottomSuccessLabel, Resources.HCMResource.
                       RoleManagement_RoleCategoryDeletedSuccessfully);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Cancel button is clicked
        /// in the deleting the role.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagemen_deleteRolesConfirmMsgControl_cancelClick
          (object sender, EventArgs e)
        {
            try
            {
                //Remove the business ype id from the hidden field
                RoleManagement_deleteRoleHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method called when common ConfirmMessage Popup Extenter 
        /// Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void RoleManagemen_deleteBusinessConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                //Remove the business ype id from the hidden field
                RoleManagement_deleteHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method called when common ConfirmMessage Popup Extenter 
        /// Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void RoleManagement_updateRoleCategoryCloseLinkButton_Clicked
            (object sender, EventArgs e)
        {
            try
            {
                RoleManagement_deleteHiddenField.Value = string.Empty;
                RoleManagement_updateBusinessIsEditORSaveHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when save is clicked
        /// in the adding new role code.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_updateCategoryRoleCodeNameSaveButton_Clicked
          (object sender, EventArgs e)
        {
            RoleManagement_updateBusinessNameErrorLabel.Text = null;
            try
            {

                if (RoleManagement_updateRoleCategoryCodeTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(RoleManagement_updateBusinessNameErrorLabel,
                        Resources.HCMResource.RoleManagement_EnterRoleCatCode);


                    RoleManagement_updateRoleCategoryNameModalPopupExtender.Show();

                    return;
                }

                if (RoleManagement_updateRoleCategoryNameTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(RoleManagement_updateBusinessNameErrorLabel,
                        Resources.HCMResource.RoleManagement_EnterRoleCatName);

                    RoleManagement_updateRoleCategoryNameModalPopupExtender.Show();

                    return;
                }

                if (RoleManagement_updateBusinessIsEditORSaveHiddenField.Value.ToUpper() == "EDIT")
                {
                    int id = int.Parse(RoleManagement_deleteHiddenField.Value);


                    new AuthenticationBLManager().UpdateRoleCategory(RoleManagement_updateRoleCategoryCodeTextBox.Text,
                        RoleManagement_updateRoleCategoryNameTextBox.Text, id, base.userID);

                    base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                       RoleManagement_bottomSuccessLabel,
                       Resources.HCMResource.RoleManagement_RoleUpdatedSuccessfully);
                }
                if (RoleManagement_updateBusinessIsEditORSaveHiddenField.Value.ToUpper() == "ADDNEW")
                {

                    RoleManagement_updateRoleCategoryCodeTextBox.Focus();
                    int rowsAffected = 0;
                    rowsAffected = new AuthenticationBLManager().InsertNewCategoryRole(
                       RoleManagement_updateRoleCategoryCodeTextBox.Text,
                       RoleManagement_updateRoleCategoryNameTextBox.Text, base.userID);

                    if (rowsAffected == 0)
                    {

                        base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                      RoleManagement_bottomSuccessLabel,
                       Resources.HCMResource.RoleManagement_NewRoleCategoryInsertedSuccessfully);
                    }
                    else
                    {
                        base.ShowMessage(RoleManagement_updateBusinessNameErrorLabel,
                        Resources.HCMResource.RoleManagement_RoleAlreadyExists);

                        RoleManagement_updateRoleCategoryNameModalPopupExtender.Show();
                        return;
                    }
                }

                //Reset the page navigator
                RoleManagement_rolecategoryPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD1"] = "ROLECODE";

                //Get the form details
                GetRoleCategoryDetails(1);
                GetRoleDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save is clicked
        /// in adding new roles.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_updateRoleSaveButton_Clicked
         (object sender, EventArgs e)
        {
            RoleManagement_updateRoleCategoryNameErrorLabel.Text = null;
            try
            {

                if (RoleManagement_updateRoleCategoryListBox.SelectedIndex == 0)
                {
                    base.ShowMessage(RoleManagement_updateRoleCategoryNameErrorLabel,
                        Resources.HCMResource.RoleManagement_EnterRoleCategory);

                    RoleManagement_updateRoleModalPopupExtender.Show();

                    return;
                }
                if (RoleManagement_updateRoleCodeTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(RoleManagement_updateRoleCategoryNameErrorLabel,
                        Resources.HCMResource.RoleManagement_EnterRoleCode);

                    RoleManagement_updateRoleModalPopupExtender.Show();

                    return;
                }
                if (RoleManagement_updateRoleNameTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(RoleManagement_updateRoleCategoryNameErrorLabel,
                        Resources.HCMResource.RoleManagement_EnterRoleName);

                    RoleManagement_updateRoleModalPopupExtender.Show();

                    return;
                }


                if (RoleManagement_updateRoleJobDescriptionTextbox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(RoleManagement_updateRoleCategoryNameErrorLabel,
                        Resources.HCMResource.RoleManagement_EnterRoleJobDescription);

                    RoleManagement_updateRoleModalPopupExtender.Show();

                    return;
                }

                if (RoleManagement_updateRoleIsEditORSaveHiddenField.Value.ToUpper() == "EDIT")
                {
                    int id = int.Parse(RoleManagement_deleteHiddenField.Value);


                    new AuthenticationBLManager().UpdateRoleCategory(RoleManagement_updateRoleCategoryCodeTextBox.Text,
                        RoleManagement_updateRoleCategoryNameTextBox.Text, id, base.userID);

                    base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                       RoleManagement_bottomSuccessLabel,
                       Resources.HCMResource.RoleManagement_RoleCategoryTypeUpdatedSuccessfully);
                }
                if (RoleManagement_updateRoleIsEditORSaveHiddenField.Value.ToUpper() == "ADDNEW")
                {
                    int rowsAffected = 0;
                    rowsAffected = new AuthenticationBLManager().InsertNewRole(RoleManagement_updateRoleCodeTextBox.Text,
                        Convert.ToInt32(RoleManagement_updateRoleCategoryListBox.SelectedValue),
                        RoleManagement_updateRoleNameTextBox.Text, RoleManagement_updateRoleJobDescriptionTextbox.Text, base.userID);

                    if (rowsAffected == 0)
                        base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                      RoleManagement_bottomSuccessLabel,
                       Resources.HCMResource.RoleManagement_NewRoleInsertedSuccessfully);
                    else
                    {
                        base.ShowMessage(RoleManagement_updateRoleCategoryNameErrorLabel,
                       Resources.HCMResource.RoleManagement_RoleAlreadyExists);

                        RoleManagement_updateRoleModalPopupExtender.Show();
                        return;
                    }
                }

                //Reset the page navigator
                RoleManagement_rolecategoryPageNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "NAME";

                //Get the form details
                GetRoleDetails(1);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Add is clicked
        /// to add new role category.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryAddImageButton_Click(object sender, EventArgs e)
        {
            try
            {
                RoleManagement_updateBusinessNameErrorLabel.Text = null;
                if (RoleManagement_rolecategoryGridView.EditIndex != -1)
                {
                    base.ShowMessage(RoleManagement_topErrorMessageLabel,
                  RoleManagement_bottomErrorLabel, Resources.HCMResource.
                  RoleManagement_PleaseUpdateTheRole);
                    return;
                }
                RoleManagement_updateBusinessIsEditORSaveHiddenField.Value = "ADDNEW";
                RoleManagement_updateBusinessLiteral.Text = "Add Role Category";
                RoleManagement_updateRoleCategoryCodeTextBox.Focus();
                RoleManagement_updateRoleCategoryCodeTextBox.Text = string.Empty;
                RoleManagement_updateRoleCategoryNameTextBox.Text = string.Empty;
                RoleManagement_updateRoleCategoryNameModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Add is clicked
        /// to add new role .
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_rolesAddImageButton_Click(object sender, EventArgs e)
        {
            try
            {

                RoleManagement_updateRoleCategoryNameErrorLabel.Text = null;
                if (RoleManagement_rolesGridView.EditIndex != -1)
                {
                    base.ShowMessage(RoleManagement_topErrorMessageLabel,
                  RoleManagement_bottomErrorLabel, Resources.HCMResource.
                  RoleManagement_PleaseUpdateTheRoleCategory);
                    return;
                }
                RoleManagement_updateRoleIsEditORSaveHiddenField.Value = "ADDNEW";
                RoleManagement_updateRoleLiteral.Text = "Add Role";
                string sortOrder = ViewState["SORT_ORDER"].ToString();

                string sortExpression = ViewState["SORT_FIELD"].ToString();

                int totalPage = 0;

                List<RoleManagementDetail> roleCategoryList = new AuthenticationBLManager().GetRoleCategory(
                    sortExpression, sortOrder, base.GridPageSize, 1, out totalPage);

                RoleManagement_updateRoleCategoryListBox.DataSource = roleCategoryList;

                RoleManagement_updateRoleCategoryListBox.DataTextField = "RoleCodeName";
                RoleManagement_updateRoleCategoryListBox.DataValueField = "RoleID";

                RoleManagement_updateRoleCategoryListBox.DataBind();
                RoleManagement_updateRoleCategoryListBox.Items.Insert(0, new ListItem("--Select--"));
                RoleManagement_updateRoleCodeTextBox.Text = string.Empty;
                RoleManagement_updateRoleNameTextBox.Text = string.Empty;
                RoleManagement_updateRoleCodeTextBox.Focus();

                RoleManagement_updateRoleJobDescriptionTextbox.Text = string.Empty;
                RoleManagement_updateRoleModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the delete action with row command
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RoleManagement_roleGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteRoleName")
                    return;
                RoleManagement_deleteRoleHiddenField.Value = e.CommandArgument.ToString();
                RoleManagement_deleteRolesConfirmMsgControl.Message = string.Format
                    (Resources.HCMResource.RoleManagement_RoleDeleteConfirmation,
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("RoleManagement_rolecodeHiddenLabel")).Text + '-' +
                    ((Label)((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent).
                    FindControl("RoleManagement_rolenameHiddenLabel")).Text);

                RoleManagement_deleteRolesConfirmMsgControl.Type = MessageBoxType.YesNo;
                RoleManagement_deleteRolesConfirmMsgControl.Title = "Warning";
                RoleManagement_deleteRoleFormModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }
      
        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryGridView_Sorting
          (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                if (ViewState["SORT_FIELD1"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD1"] = e.SortExpression;
                RoleManagement_rolecategoryPageNavigator.Reset();
                GetRoleCategoryDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void RoleManagement_rolecategoryGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (RoleManagement_rolecategoryGridView,
                    (string)ViewState["SORT_FIELD1"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save is clicked
        /// to save the role  .
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_SaveClick(object sender, EventArgs e)
        {
            try
            {
                RoleManagement_topErrorMessageLabel.Text = string.Empty;
                RoleManagement_bottomErrorLabel.Text = string.Empty;
                for (int i = 0; i < RoleManagement_rolesGridView.Rows.Count; i++)
                {
                    TextBox tb = (TextBox)RoleManagement_rolesGridView.Rows[i].FindControl
                          ("RoleManagement_roleGridView_rolecodeTextBox");

                    TextBox tb3 = (TextBox)RoleManagement_rolesGridView.Rows[i].FindControl
                         ("RoleManagement_roleGridView_rolenameTextBox");

                    TextBox tb4 = (TextBox)RoleManagement_rolesGridView.Rows[i].FindControl
                         ("RoleManagement_roleGridView_roleJobDescriptionTextBox");

                    if (tb.Text == string.Empty || tb3.Text == string.Empty || tb4.Text == string.Empty)
                    {
                        base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, "Mandatory fields cannot be empty");
                        return;
                    }
                    for (int q = i; q < RoleManagement_rolesGridView.Rows.Count - 1; q++)
                    {
                        TextBox tb1 = (TextBox)RoleManagement_rolesGridView.Rows[q + 1].FindControl
                          ("RoleManagement_roleGridView_rolecodeTextBox");

                        if (tb.Text.Trim() == tb1.Text.Trim())
                        {
                            base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel,
                    string.Format(Resources.HCMResource.RoleManagementRoleAlreadyExists,
                    tb.Text.Trim()));

                            return;
                        }

                    }
                }

                UpdateRoleManangementRoles();
                GetRoleDetails(1);

                base.ShowMessage(RoleManagement_topSuccessMessageLabel,
                 RoleManagement_bottomSuccessLabel, Resources.HCMResource.
                 RoleManagement_RoleUpdatedSuccessfully);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when show or hide is clicked
        /// to show/ hide the jod description of roles  .
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void RoleManagement_showhideClick(object sender, EventArgs e)
        {

            if (RoleManagement_showhide.Text != "Hide Description")
            {
                foreach (GridViewRow gvr in RoleManagement_rolesGridView.Rows)
                {
                    TextBox tb = (TextBox)gvr.FindControl
                        ("RoleManagement_roleGridView_roleJobDescriptionTextBox");

                    tb.Visible = true;
                }
                RoleManagement_showhide.Text = "Hide Description";
                RoleManagement_showhide.ToolTip = "Click to hide role description";
                return;
            }

            foreach (GridViewRow gvr in RoleManagement_rolesGridView.Rows)
            {
                TextBox tb = (TextBox)gvr.FindControl
                    ("RoleManagement_roleGridView_roleJobDescriptionTextBox");

                tb.Visible = false;
            }
            RoleManagement_showhide.ToolTip = "Click to show role description";
            RoleManagement_showhide.Text = "Show Description";

        }


        #endregion Event Handlers

        #region private Methods                                                
        /// <summary>
        /// Method to load roles
        /// </summary>
        private void LoadRoles()
        {
            //Reset the page navigator
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD1"] = "ROLECODE";

            //Get the role details
            GetRoleDetails(1);
        }
        /// <summary>
        /// Method to get the role details
        /// </summary>
        /// <param name="pageNumber"></param>
        private void GetRoleDetails(int pageNumber)
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD1"].ToString();

          //  int totalPage = 0;

            List<RoleManagementDetail> rolesList = new AuthenticationBLManager().GetRoles(
                sortExpression, sortOrder, pageNumber);

            RoleManagement_rolesGridView.DataSource = rolesList;

            RoleManagement_rolesGridView.DataBind();

            BindRoleCategoryDropdownList();
        }
        /// <summary>
        /// Method to load role category in the dropdown list in roles
        /// </summary>
        private void BindRoleCategoryDropdownList()
        {
            int totalPage = 0;

            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            DropDownList ddlRoleCatgeory = null;

            List<RoleManagementDetail> roleCategoryList = new AuthenticationBLManager().GetRoleCategory(
          sortExpression, sortOrder, base.GridPageSize, 1, out totalPage);

            for (int i = 0; i < RoleManagement_rolesGridView.Rows.Count; i++)
            {
                ddlRoleCatgeory = (DropDownList)RoleManagement_rolesGridView.Rows[i].
                    FindControl("RoleManagement_updateRoleCategoryListBox");

                ddlRoleCatgeory.DataSource = roleCategoryList;
                ddlRoleCatgeory.DataTextField = "RoleCodeName";
                ddlRoleCatgeory.DataValueField = "RoleID";
                ddlRoleCatgeory.DataBind();
                try
                {

                    ddlRoleCatgeory.SelectedItem.Selected = false;

                    Label lbl = ((Label)RoleManagement_rolesGridView.Rows[i].
                        FindControl("RoleManagment_roleCodeName"));
                    for (int j = 0; j < ddlRoleCatgeory.Items.Count; j++)
                    {
                        if (lbl.Text.Trim() == ddlRoleCatgeory.Items[j].Text.Trim())
                        {
                            ddlRoleCatgeory.Items[j].Selected = true;
                            continue;
                        }
                    }
                }
                catch
                {

                }
                ddlRoleCatgeory = null;
            }

        }
        /// <summary>
        /// Method to get the role catgory details
        /// </summary>
        /// <param name="pageNumber"></param>
        private void GetRoleCategoryDetails(int pageNumber)
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD1"].ToString();

            int totalPage = 0;

            List<RoleManagementDetail> roleCategoryList = new AuthenticationBLManager().GetRoleCategory(
                sortExpression, sortOrder, base.GridPageSize, pageNumber, out totalPage);

            RoleManagement_rolecategoryGridView.DataSource = roleCategoryList;

            RoleManagement_rolecategoryGridView.DataBind();

            RoleManagement_rolecategoryPageNavigator.Visible = true;

            RoleManagement_rolecategoryPageNavigator.TotalRecords = totalPage;

            RoleManagement_rolecategoryPageNavigator.PageSize = base.GridPageSize;

            RoleManagement_rolecategoryGridViewDIV.Style["display"] = "block";

            if (roleCategoryList == null || roleCategoryList.Count == 0)
            {
                base.ShowMessage(RoleManagement_topErrorMessageLabel,
                    RoleManagement_bottomErrorLabel, Resources.HCMResource.Common_Empty_Grid);

                RoleManagement_rolecategoryGridViewDIV.Style["display"] = "none";
            }
        }
        /// <summary>
        /// Method to update the roles
        /// </summary>
        private void UpdateRoleManangementRoles()
        {
            List<RoleManagementDetail> roleDetails = null;
            RoleManagementDetail roleDetail = null;




            for (int i = 0; i < RoleManagement_rolesGridView.Rows.Count; i++)
            {
                if (roleDetail == null)
                    roleDetail = new RoleManagementDetail();

                roleDetail.RoleID = Convert.ToInt32(
                    ((HiddenField)RoleManagement_rolesGridView.Rows[i].
                    FindControl("RoleManagement_roleidHiddenLabel")).Value);

                roleDetail.RoleCodeName = ((DropDownList)RoleManagement_rolesGridView.Rows[i].
                                 FindControl("RoleManagement_updateRoleCategoryListBox")).SelectedValue;


                roleDetail.RoleCode = ((TextBox)RoleManagement_rolesGridView.Rows[i].
                                 FindControl("RoleManagement_roleGridView_rolecodeTextBox")).Text.Trim();

                roleDetail.RolesName = ((TextBox)RoleManagement_rolesGridView.Rows[i].
                                FindControl("RoleManagement_roleGridView_rolenameTextBox")).Text.Trim();

                roleDetail.RolesJobDesc = ((TextBox)RoleManagement_rolesGridView.Rows[i].
                                FindControl("RoleManagement_roleGridView_roleJobDescriptionTextBox")).Text.Trim();

                roleDetail.ModifiedBy = base.userID;

                if (roleDetails == null)
                    roleDetails = new List<RoleManagementDetail>();

                roleDetails.Add(roleDetail);
                roleDetail = null;
            }

            new AuthenticationBLManager().UpdateRoleDetails(roleDetails);
        }

        /// <summary>
        /// Clears the message.
        /// </summary>
        private void ClearMessage()
        {
            RoleManagement_topSuccessMessageLabel.Text = string.Empty;
            RoleManagement_bottomSuccessLabel.Text = string.Empty;
            RoleManagement_topErrorMessageLabel.Text = string.Empty;
            RoleManagement_bottomErrorLabel.Text = string.Empty;
        }

        #endregion

        #region Protected Overridden Methods                                   
        protected override void LoadValues()
        { 
            RoleManagement_rolecategoryPageNavigator.Reset();
            GetRoleCategoryDetails(1);
        }
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }
        #endregion Protected Overridden Methods
    }
}