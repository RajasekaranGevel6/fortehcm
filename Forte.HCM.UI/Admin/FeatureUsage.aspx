﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    CodeBehind="FeatureUsage.aspx.cs" Inherits="Forte.HCM.UI.Admin.FeatureUsage" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="FeatureUsage_bodyContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <asp:UpdatePanel ID="FeatureUsage_UpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Literal ID="FeatureUsage_headerLiteral" runat="server" Text="Feature Usage"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="FeatureUsage_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="FeatureUsage_topResetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="FeatureUsage_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="FeatureUsage_topMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="FeatureUsage_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="FeatureUsage_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <div id="FeatureUsage_SummaryDIV" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <div>
                                                                    <asp:UpdatePanel ID="FeatureUsage_selectRoleUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                <tr>
                                                                                    <td style="vertical-align: middle">
                                                                                        <div id="FeatureUsage_searchImageDIV" style="display: block; float: left; width: 100%"
                                                                                            runat="server">
                                                                                            <asp:UpdatePanel ID="FeatureUsage_searchImageDIV_updatePanel" runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <table width="100%" cellpadding="0" cellspacing="3" border="0" class="panel_inner_body_bg">
                                                                                                        <tr>
                                                                                                            <td style="width: 7%">
                                                                                                                <div>
                                                                                                                    <asp:Label ID="FeatureUsage_selectModule" runat="server" Text="User" SkinID="sknLabelFieldHeaderText">
                                                                                                                    </asp:Label>
                                                                                                                    <span class='mandatory'>*</span>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td style="width: 200px">
                                                                                                                <asp:TextBox ID="FeatureUsage_userIDTextBox" runat="server" ReadOnly="true" Width="195px"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="FeatureUsage_userIDHiddenField" runat="server" />
                                                                                                                <asp:HiddenField ID="FeatureUsage_browserHiddenField" runat="server" />
                                                                                                            </td>
                                                                                                            <td style="width: 22px">
                                                                                                                <asp:ImageButton ID="FeatureUsage_userSearchImage" runat="server" SkinID="sknbtnSearchicon"
                                                                                                                    ToolTip="Click here to select the user" Style="width: 18px" />&nbsp;
                                                                                                            </td>
                                                                                                            <td style="width: 22px">
                                                                                                                <div style="float: left; height: 16px;">
                                                                                                                    <asp:ImageButton ID="BatchQuestionEntry_helpImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" OnClientClick="javascript:return false;" ToolTip="Please select the user here" /></div>
                                                                                                            </td>
                                                                                                            <td style="width: 5%">
                                                                                                                <asp:Label ID="FeatureUsage_monthLable" runat="server" Text="Month" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 100px">
                                                                                                                <asp:DropDownList ID="FeatureUsage_monthDropDownList" runat="server" Width="100%">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                            <td style="width: 5%">
                                                                                                                <asp:Label ID="FeatureUsage_yearLable" runat="server" Text="Year" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 102px">
                                                                                                                <asp:DropDownList ID="FeatureUsage_yearDropDownList" runat="server" AutoPostBack="false"
                                                                                                                    Width="100%">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" colspan="2">
                                                                                        <asp:Button ID="FeatureUsage_showButton" runat="server" Text="Show Details" SkinID="sknButtonId"
                                                                                            OnClick="FeatureUsage_showButton_Click" Style="margin-left: 19px" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="header_bg">
                                                                <asp:Literal ID="Literal1" runat="server" Text="User Details"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="FeatureUsage_SubscriptionTypeCaption" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Subscription Type"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%">
                                                                            <asp:Label ID="FeatureUsage_SubscriptionTypeLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="FeatureUsage_SubscribedOnCation" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Subscribed On"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%">
                                                                            <asp:Label ID="FeatureUsage_SubscribedOnLable" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="FeatureUsage_CompanyCaption" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Company"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="FeatureUsage_CompanyLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="FeatureUsage_titleCaption" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Title"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="FeatureUsage_titleLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <caption>
                            </caption>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" style="width: 98%">
                                    <asp:Label ID="FeatureUsage_assignRolesLabel" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="width: 2%">
                                    <asp:HiddenField ID="FeatureUsage_restoreHiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="FeatureUsage_TR" runat="server">
                    <td align="center" class="header_bg">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" class="header_text_bold" style="width: 50%">
                                    <asp:Literal ID="FeatureUsage_questionDetailHeaderLiteral" runat="server" Text="Usage"></asp:Literal>
                                </td>
                                <td align="right" style="width: 50%">
                                    <span id="FeatureUsage_UparrowSpan" runat="server" style="display: block;">
                                        <asp:Image ID="FeatureUsage_UpArrow" runat="server" SkinID="sknMinimizeImage" />
                                    </span><span id="FeatureUsage_DownarrowSpan" runat="server" style="display: none;">
                                        <asp:Image ID="FeatureUsage_DownArrow" runat="server" SkinID="sknMaximizeImage" />
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="grid_body_bg">
                        <div id="FeatureUsage_DetailsDIV" runat="server" style="height: 350px; overflow: auto;">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="FeatureUsage_userFeaturesGridView" runat="server" AllowSorting="false"
                                            AutoGenerateColumns="false" OnRowCommand="FeatureUsage_userFeaturesGridView_RowCommand"
                                            OnRowDataBound="FeatureUsage_userFeaturesGridView_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Feature" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="35%">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="UserRoleMatrix_rolesGridView_viewDetails" runat="server" SkinID="sknViewCredits"
                                                            ToolTip="View Usage Details" CommandArgument='<%# Eval("FeatureID") %>' CommandName="POPUP" />
                                                        <asp:Label ID="UserRoleMatrix_rolesGridView_rolecategotycodeLabel" runat="server"
                                                            Text='<%# Eval("FeatureName") %>'>
                                                        </asp:Label>
                                                        <asp:HiddenField ID="UserRoleMatrix_rolenAMEGridViewIDHiddenField" runat="server"
                                                            Value='<%# Eval("FeatureID") %>' />
                                                        <asp:HiddenField ID="UserRoleMatrix_mmmYYYYGridViewIDHiddenField" runat="server"
                                                            Value='<%# Eval("MonthYear") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Limit" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="25%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="UserRoleMatrix_rolesGridView_rolecategotycodeLabel2" runat="server"
                                                            Text='<%# Eval("FeatureLimit") %>'>
                                                        </asp:Label>
                                                        <asp:HiddenField ID="UserRoleMatrix_rolenAMEGridViewIDHiddenField1" runat="server"
                                                            Value='<%# Eval("FeatureID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Used" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="25%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="UserRoleMatrix_rolesGridView_rolecategotycodeLabel1" runat="server"
                                                            Text='<%# Eval("FEATUREUSED") %>'>
                                                        </asp:Label>
                                                        <asp:HiddenField ID="UserRoleMatrix_rolenAMEGridViewIDHiddenField2" runat="server"
                                                            Value='<%# Eval("FeatureID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:HiddenField ID="FeatureUsage_checkedStatusHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                </tr>
                <caption>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    </tr>
                </caption>
            </table>
            </div> </caption> </td> </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="FeatureUsage_bottomMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="FeatureUsage_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            <asp:Label ID="FeatureUsage_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 72%" class="header_text_bold">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td style="width: 52%">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td style="width: 16%" align="right">
                                            <asp:LinkButton ID="FeatureUsage_bottomResetLinkButton" runat="server" Text="Reset"
                                                SkinID="sknActionLinkButton" OnClick=" FeatureUsage_topResetLinkButton_Click"></asp:LinkButton>
                                        </td>
                                        <td width="4%" align="center" class="link_button">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="FeatureUsage_bottomCancelLinkButton" runat="server" Text="Cancel"
                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="FeatureUsage_isMaximizedHiddenField" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="year_hd" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="FeatureUsage_message_updatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="FeatureUsage_bottomSuccessLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="FeatureUsage_bottomErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
