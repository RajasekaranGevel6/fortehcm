<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuestionSettings.aspx.cs"
    MasterPageFile="~/MasterPages/OTMMaster.Master" Inherits="Forte.HCM.UI.Admin.QuestionSettings" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="QuestionSettings_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <script type="text/javascript" language="javascript">

        // Method that will show the available subjects popup through which 
        // multiple subjects can be selected.
        function ShowSearchSubject() {

            // Clear error/success message label
            document.getElementById('<%=QuestionSettings_bottomSuccessMessageLabel.ClientID %>').innerHTML = '';
            document.getElementById('<%=QuestionSettings_bottomErrorMessageLabel.ClientID %>').innerHTML = '';
            document.getElementById('<%=QuestionSettings_topSuccessMessageLabel.ClientID %>').innerHTML = '';
            document.getElementById('<%=QuestionSettings_topErrorMessageLabel.ClientID %>').innerHTML = '';

            var categoryCtrl = document.getElementById
                ('<%=QuestionSettings_categoriesListBox.ClientID %>');
            var statusCtrl = document.getElementById
                ('<%=QuestionSettings_subjectAddedHiddenField.ClientID %>');

            // Reset the status control value to 'N'.
            statusCtrl.value = 'N';

            var index = categoryCtrl.selectedIndex;
            if (index < 0 || index >= categoryCtrl.length) {
                document.getElementById('<%=QuestionSettings_topErrorMessageLabel.ClientID %>').
                    innerHTML = 'Please select a category';

                document.getElementById('<%=QuestionSettings_bottomErrorMessageLabel.ClientID %>').
                    innerHTML = 'Please select a category';

                return false;
            }

            var categoryName = categoryCtrl.options[categoryCtrl.selectedIndex].text;
            var categoryID = categoryCtrl.options[categoryCtrl.selectedIndex].value

            var url = '../Popup/SearchSubject.aspx' +
                '?categoryid=' + categoryID +
                '&categoryname=' + categoryName +
                '&statusctrl=' + statusCtrl.id;
            var height = 550;
            var width = 600;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            // Launch the search subject popup.
            window.open(url, window.self, sModalFeature);
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="QuestionSettings_headerLiteral" runat="server" Text="Question Settings"></asp:Literal>
                            <asp:HiddenField ID="QuestionSettings_browserHiddenField" runat="server" />
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr align="right">
                                    <td>
                                        <asp:LinkButton ID="QuestionSettings_topResetLinkButton" Text="Reset" runat="server"
                                            SkinID="sknActionLinkButton" OnClick="QuestionSettings_resetButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="QuestionSettings_topCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="QuestionSettings_topSuccessUpdatePane">
                    <ContentTemplate>
                        <asp:Label ID="QuestionSettings_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label><br />
                        <asp:Label ID="QuestionSettings_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="header_text_bold">
                                                    <asp:Literal ID="QuestionSettings_categoriesLiteral" runat="server" Text="Categories"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="QuestionSettings_categoriesUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="2" cellspacing="3" border="0">
                                                    <tr>
                                                        <td style="width: 10%;">
                                                            <asp:ListBox ID="QuestionSettings_categoriesListBox" runat="server" Width="400px" Font-Size="Larger"
                                                                OnSelectedIndexChanged="QuestionSettings_categoriesListBox_SelectedIndexChanged"
                                                                AutoPostBack="True" SkinID="sknListTextBox"></asp:ListBox>
                                                        </td>
                                                        <td valign="bottom">
                                                            <div style="padding-left: 6px;">
                                                                <asp:LinkButton ID="QuestionSettings_deleteCategoryLinkButton" runat="server" Text="Delete Category"
                                                                    SkinID="sknActionLinkButton" OnClick="QuestionSettings_deleteCategoryLinkButton_Click"></asp:LinkButton>
                                                            </div>
                                                             <div style="padding-left: 6px;">
                                                                <asp:LinkButton ID="QuestionSettings_flushCategoryLinkButton" runat="server" Text=" Flush Category"
                                                                    SkinID="sknActionLinkButton" OnClick="QuestionSettings_flushCategoryLinkButton_Click"></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" cellpadding="2" cellspacing="3" border="0">
                                                                <tr>
                                                                    <td style="width: 12%">
                                                                        <asp:Label ID="QuestionSettings_newCategoryLabel" runat="server" Text="New Category"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                        <asp:TextBox ID="QuestionSettings_newCategoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:LinkButton ID="QuestionSettings_addNewCategoryLinkButton" runat="server" Text="Add New"
                                                                                SkinID="sknActionLinkButton" OnClick="QuestionSettings_addNewCategoryLinkButton_Click"></asp:LinkButton>
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="QuestionSettings_addNewCategoryImageButton" SkinID="sknHelpImageButton"
                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Helps to add new categroy" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:UpdatePanel ID="QuestionSettings_addSubjectsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="QuestionSettings_addSubjectsLiteral" runat="server" Text="Subjects"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="2" cellspacing="3">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <asp:ListBox ID="QuestionSettings_subjectsListBox" runat="server" Width="400px" SelectionMode="Multiple"
                                                                SkinID="sknListTextBox"></asp:ListBox>
                                                        </td>
                                                        <td>
                                                            <div style="float: left; padding-left: 6px;">
                                                                <asp:LinkButton ID="QuestionSettings_deleteSubjectLinkButton" runat="server" Text="Delete Subject"
                                                                    SkinID="sknActionLinkButton" OnClick="QuestionSettings_deleteSubjectLinkButton_Click"></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="50%" border="0">
                                                    <tr>
                                                        <td style="width: 10%">
                                                            <asp:Label ID="QuestionSettings_addSubjectLabel" runat="server" Text="New Subject"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:TextBox ID="QuestionSettings_newSubjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:LinkButton ID="QuestionSettings_addNewSubjectLinkButton" runat="server" Text="Add New"
                                                                    SkinID="sknActionLinkButton" OnClick="QuestionSettings_addNewSubjectLinkButton_Click"></asp:LinkButton>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="QuestionSettings_addNewSubjectImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Helps to add new subject under selected category" />
                                                            </div>
                                                        </td>
                                                        <td style="width: 55%" align="left">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:LinkButton ID="QuestionSettings_addAvailableSubjectsLinkButton" runat="server"
                                                                    Text="Add Available Subjects" SkinID="sknActionLinkButton" OnClick="QuestionSettings_addAvailableSubjectsLinkButton_Click"></asp:LinkButton>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="QuestionSettings_addAvailableSubjectsImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Helps to add existing subject of various categories under selected category"
                                                                    Height="16px" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <table width="100%">
                                <tr>
                                    <td style="width: 6%;">
                                        <asp:Literal ID="QuestionSettings_addTestAreaLiteral" runat="server" Text="Test Areas"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="QuestionSettings_testAreaUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="2" cellspacing="3">
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <asp:ListBox ID="QuestionSettings_testAreasListBox" runat="server" Width="400px"
                                                                SelectionMode="Multiple" SkinID="sknListTextBox"></asp:ListBox>
                                                        </td>
                                                        <td valign="bottom">
                                                            <div style="float: left; padding-left: 6px;">
                                                                <asp:LinkButton ID="QuestionSettings_deleteTestAreaLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                    Text="Delete Test Area" OnClick="QuestionSettings_deleteTestAreaLinkButton_Click"></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0">
                                            <tr>
                                                <td style="width: 11%">
                                                    <asp:Label ID="QuestionSettings_newTestAreaNameLabel" runat="server" Text="New Test Area "
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:TextBox ID="QuestionSettings_newTestAreaNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td style="width: 74%" align="left">
                                                    <div style="float: left; padding-right: 5px;">
                                                        <asp:LinkButton ID="QuestionSettings_addNewTestAreaLinkButton" runat="server" Text="Add New"
                                                            SkinID="sknActionLinkButton" OnClick="QuestionSettings_addNewTestAreaLinkButton_Click"></asp:LinkButton>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:ImageButton ID="QuestionSettings_addNewTestAreaImageButton" SkinID="sknHelpImageButton"
                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Helps to add new test area" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="QuestionSettings_bottomSuccessUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="QuestionSettings_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label><br />
                        <asp:Label ID="QuestionSettings_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:HiddenField ID="QuestionSettings_subjectAddedHiddenField" Value="N" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table align="right" cellpadding="0" cellspacing="4" border="0">
                    <tr>
                        <td>
                            <asp:LinkButton ID="QuestionSettings_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="QuestionSettings_resetButton_Click"></asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                            <asp:LinkButton ID="QuestionSettings_bottomCancelLinkButton" Text="Cancel" runat="server"
                                OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="QuestionSettings_modelPopupUpdatePanel">
                    <ContentTemplate>
                        <asp:Panel ID="QuestionSettings_deleteConfirmPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc3:ConfirmMsgControl ID="QuestionSettings_deleteConfirmMsgControl" runat="server"
                                OnOkClick="QuestionSettings_deleteConfirmOkClick" />
                        </asp:Panel>
                        <div id="QuestionSettings_hiddenDIV" runat="server" style="display: none">
                            <asp:Button ID="QuestionSettings_hiddenPopupModalButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="QuestionSettings_deleteConfirmPopupExtender"
                            runat="server" PopupControlID="QuestionSettings_deleteConfirmPanel" TargetControlID="QuestionSettings_hiddenPopupModalButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:HiddenField ID="QuestionSettings_deleteTypeHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
