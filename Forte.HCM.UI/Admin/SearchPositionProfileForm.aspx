﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    AutoEventWireup="true" CodeBehind="SearchPositionProfileForm.aspx.cs" Inherits="Forte.HCM.UI.Admin.SearchPositionProfileForm" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="SearchPositionProfileForm_Content" ContentPlaceHolderID="PositionProfileMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">

        function ClearDesignedBy()
        {
            document.getElementById("<%= SearchPositionProfileForm_designedBydummyAuthorIdHidenField.ClientID%>").value = '';
            document.getElementById("<%= SearchPositionProfileForm_designedByHiddenField.ClientID%>").value = '';
            document.getElementById("<%= SearchPositionProfileForm_designedByTextBox.ClientID%>").value = '';

            return false;
        }
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                            <asp:Literal ID="SearchPositionProfileForm_headerLiteral" runat="server" Text="Search Form"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="SearchPositionProfileForm_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchPositionProfileForm_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchPositionProfileForm_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SearchPositionProfileForm_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SearchPositionProfileForm_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchPositionProfileForm_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="SearchPositionProfileForm_searchDivUpdatePanel">
                                <ContentTemplate>
                                    <div id="SearchPositionProfileForm_searchDiv" runat="server">
                                        <table class="panel_bg" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="panel_inner_body_bg" border="0"
                                                        style="width: 100%">
                                                        <tr>
                                                            <td style="width: 5%">
                                                                <asp:Label ID="SearchPositionProfileForm_formNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Name"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="SearchPositionProfileForm_formNameTextBox" runat="server" Width="90%"
                                                                    MaxLength="500"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 4%">
                                                                <asp:Label ID="SearchPositionProfileForm_tagLabel" runat="server" Text="Tags" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="SearchPositionProfileForm_tagTextBox" runat="server" Width="90%"
                                                                    MaxLength="500"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 4%">
                                                                <asp:Label ID="SearchPositionProfileForm_globalLabel" runat="server" Text="Global"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 5%">
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:CheckBox ID="SearchPositionProfileForm_globalCheckBox" runat="server" Text="" />
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="SearchPositionProfileForm_globalHelpImageButton" runat="server" OnClientClick="javascript:return false;"
                                                                        SkinID="sknHelpImageButton" ToolTip="Check this to search for global forms" />
                                                                </div>
                                                            </td>
                                                            <td style="width: 10%" align="center">
                                                                <asp:Label ID="SearchPositionProfileForm_designedByLabel" runat="server" Text="Designed By"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:TextBox ID="SearchPositionProfileForm_designedByTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                </div>
                                                                <div style="float: left; height: 16px;">
                                                                    <asp:ImageButton ID="SearchPositionProfileForm_designedByImageButton" SkinID="sknbtnSearchicon"
                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the user" />
                                                                    <asp:HiddenField ID="SearchPositionProfileForm_designedBydummyAuthorIdHidenField"
                                                                        runat="server" />
                                                                    <asp:HiddenField ID="SearchPositionProfileForm_designedByHiddenField" runat="server" />
                                                                </div>
                                                                <div style="padding-top: 2px">
                                                                    <asp:LinkButton ID="SearchPositionProfileForm_clearDesignedByLinkButton" runat="server"
                                                                        Text="Clear" SkinID="sknActionLinkButton" OnClientClick="javascript:return ClearDesignedBy()"
                                                                        ToolTip="Click here to clear the 'Designed By' field" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5" colspan="6">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="td_padding_top_5">
                                                    <asp:Button ID="SearchPostionProfileForm_topSearchButton" runat="server" Text="Search"
                                                        SkinID="sknButtonId" OnClick="SearchPostionProfileForm_topSearchButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr id="SearchPostionProfileForm_searchResultsTR" runat="server">
                        <td class="header_bg">
                            <asp:UpdatePanel ID="SearchPostionProfileForm_expandAllUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr id="SearchPostionProfileForm_expandAllTR" runat="server">
                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                <asp:Literal ID="SearchPostionProfileForm_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                    ID="SearchPostionProfile_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                    Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                            </td>
                                            <td style="width: 48%" align="left">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="SearchPostionProfileForm_resultDraftExpandLinkButton" runat="server"
                                                                Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseRows(this);"
                                                                Visible="false"></asp:LinkButton>
                                                            <asp:HiddenField ID="SearchPostionProfile_stateExpandHiddenField" runat="server"
                                                                Value="0" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 2%" align="right">
                                                <span id="SearchPostionProfileForm_searchResultsUpSpan" runat="server" style="display: none;">
                                                    <asp:Image ID="SearchPostionProfileForm_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                        id="SearchPostionProfileForm_searchResultsDownSpan" runat="server" style="display: block;"><asp:Image
                                                            ID="SearchPostionProfileForm_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                <asp:HiddenField ID="SearchPostionProfileForm_restoreHiddenField" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel runat="server" ID="SearchPostionProfileForm_positionProfileGridViewUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 310px; overflow: auto;" runat="server" id="SearchPostionProfileForm_positionProfileSearchDiv"
                                                    visible="false">
                                                    <asp:GridView ID="SearchPostionProfileForm_positionProfileGridView" runat="server"
                                                        AllowSorting="true" OnRowCreated="SearchPostionProfileForm_positionProfileGridView_RowCreated"
                                                        OnSorting="SearchPostionProfileForm_positionProfileGridView_Sorting" OnRowCommand="SearchPostionProfileForm_positionProfileGridView_RowCommand"
                                                        OnRowDataBound="SearchPostionProfileForm_positionProfileGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="11%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="SearchPostionProfileForm_positionProfileGridView_editFormImageButton"
                                                                        runat="server" SkinID="sknEditFormImageButton" ToolTip="Edit Form" CommandArgument='<%# Eval("FormID") %>'
                                                                        CommandName="editForm" Visible='<%# IsAllowEdit(Eval("AllowEdit").ToString())%>' />
                                                                    <asp:ImageButton ID="SearchPostionProfileForm_positionProfileGridView_viewFormImageButton"
                                                                        runat="server" SkinID="sknViewFormImageButton" ToolTip="Preview Form" CommandArgument='<%# Eval("FormID") %>' />
                                                                    <asp:ImageButton ID="SearchPostionProfileForm_positionProfileGridView_deleteFormImageButton"
                                                                        runat="server" ToolTip="Delete Form" CommandArgument='<%# Eval("FormID") %>'
                                                                        SkinID="sknDeleteImageButton" CommandName="deleteForm" Visible='<%# IsAllowEdit(Eval("AllowEdit").ToString())%>' />
                                                                    <asp:ImageButton ID="SearchPostionProfileForm_positionProfileGridView_createNewFormImageButton"
                                                                        runat="server" SkinID="sknNewFormImageButton" ToolTip="Create New Form" CommandName="createNewForm"
                                                                        CommandArgument='<%# Eval("FormID") %>' />
                                                                    <asp:ImageButton ID="SearchPostionProfileForm_positionProfileGridView_createPositionProfileImageButton"
                                                                        runat="server" SkinID="sknNewPPImageButton" ToolTip="Create Position Profile"
                                                                        CommandName="createPositionProfile" CommandArgument='<%# Eval("FormID") %>' />
                                                                    <asp:HiddenField ID="SearchPostionProfileForm_positionProfileGridView_formIDHiddenField"
                                                                        runat="server" Value='<%# Eval("FormID") %>' />
                                                                    <asp:HiddenField ID="SearchPostionProfileForm_positionProfileGridView_formNameHiddenField"
                                                                        runat="server" Value='<%# Eval("FormName") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Name" DataField="FormName" SortExpression="FORM_NAME"
                                                                ItemStyle-Width="30%" />
                                                            <asp:BoundField HeaderText="Tags" DataField="Tags" SortExpression="FORM_TAG" ItemStyle-Width="28%" />
                                                            <asp:BoundField HeaderText="Global" DataField="GlobalFormText" SortExpression="GLOBAL"
                                                                ItemStyle-Width="10%" />
                                                            <asp:TemplateField HeaderText="Designed By" SortExpression="DESIGNED_BY">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchPostionProfileForm_positionProfileGridView_designedByLabel"
                                                                        runat="server" Text='<%# Eval("UserFullName") %>' ToolTip='<%# Eval("UserFullName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="SearchPostionProfileForm_PageNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchPostionProfileForm_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SearchPositionProfileForm_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SearchPositionProfileForm_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchPositionProfileForm_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 89%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 5%" align="right">
                                        <asp:LinkButton ID="SearchPositionProfileForm_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="SearchPositionProfileForm_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="1%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="5%" align="left">
                                        <asp:LinkButton ID="SearchPositionProfileForm_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchPositionProfileForm_deleteFormUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="SearchPositionProfileForm_deleteFormHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="SearchPositionProfileForm_deleteFormPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="SearchPositionProfileForm_deleteFormConfirmMsgControl"
                                runat="server" OnOkClick="SearchPositionProfileForm_DeleteSegmentConfirmMsgControl_okClick" />
                            <asp:HiddenField ID="SearchPositionProfileForm_deleteFormHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchPositionProfileForm_deleteFormModalPopupExtender"
                            runat="server" PopupControlID="SearchPositionProfileForm_deleteFormPopupPanel"
                            TargetControlID="SearchPositionProfileForm_deleteFormHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
